.class final Lcom/squareup/protos/client/cbms/DisputedPayment$ProtoAdapter_DisputedPayment;
.super Lcom/squareup/wire/ProtoAdapter;
.source "DisputedPayment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/cbms/DisputedPayment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_DisputedPayment"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/cbms/DisputedPayment;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 642
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/cbms/DisputedPayment;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/cbms/DisputedPayment;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 693
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;-><init>()V

    .line 694
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 695
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 751
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 749
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->unit_token(Ljava/lang/String;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;

    goto :goto_0

    .line 748
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->payment_card_pan_suffix(Ljava/lang/String;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;

    goto :goto_0

    .line 742
    :pswitch_2
    :try_start_0
    sget-object v4, Lcom/squareup/protos/common/instrument/InstrumentType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/common/instrument/InstrumentType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->payment_card_brand(Lcom/squareup/protos/common/instrument/InstrumentType;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 744
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 739
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->payment_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;

    goto :goto_0

    .line 738
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/common/time/YearMonthDay;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->expected_resolution_date(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;

    goto :goto_0

    .line 737
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/common/time/YearMonthDay;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->latest_reporting_date(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;

    goto :goto_0

    .line 736
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->payment_created_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;

    goto :goto_0

    .line 735
    :pswitch_7
    sget-object v3, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->resolution_decided_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;

    goto :goto_0

    .line 734
    :pswitch_8
    sget-object v3, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->submitted_to_bank_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;

    goto/16 :goto_0

    .line 728
    :pswitch_9
    :try_start_1
    sget-object v4, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->protection_state(Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v4

    .line 730
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 720
    :pswitch_a
    :try_start_2
    sget-object v4, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->resolution(Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;
    :try_end_2
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v4

    .line 722
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 717
    :pswitch_b
    iget-object v3, v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->information_request:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/cbms/InformationRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 716
    :pswitch_c
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->current_disputed_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;

    goto/16 :goto_0

    .line 715
    :pswitch_d
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->current_amount_held(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;

    goto/16 :goto_0

    .line 714
    :pswitch_e
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->bank_disputed_comment(Ljava/lang/String;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;

    goto/16 :goto_0

    .line 708
    :pswitch_f
    :try_start_3
    sget-object v4, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->dispute_reason(Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;
    :try_end_3
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    :catch_3
    move-exception v4

    .line 710
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 700
    :pswitch_10
    :try_start_4
    sget-object v4, Lcom/squareup/protos/client/cbms/ActionableStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/cbms/ActionableStatus;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->current_actionable_status(Lcom/squareup/protos/client/cbms/ActionableStatus;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;
    :try_end_4
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_4 .. :try_end_4} :catch_4

    goto/16 :goto_0

    :catch_4
    move-exception v4

    .line 702
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 697
    :pswitch_11
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->payment_token(Ljava/lang/String;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;

    goto/16 :goto_0

    .line 755
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 756
    invoke-virtual {v0}, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->build()Lcom/squareup/protos/client/cbms/DisputedPayment;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 640
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtoAdapter_DisputedPayment;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/cbms/DisputedPayment;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/cbms/DisputedPayment;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 670
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 671
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cbms/DisputedPayment;->unit_token:Ljava/lang/String;

    const/16 v2, 0x12

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 672
    sget-object v0, Lcom/squareup/protos/client/cbms/ActionableStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 673
    sget-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cbms/DisputedPayment;->dispute_reason:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 674
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cbms/DisputedPayment;->bank_disputed_comment:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 675
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_amount_held:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 676
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_disputed_amount:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 677
    sget-object v0, Lcom/squareup/protos/client/cbms/InformationRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/cbms/DisputedPayment;->information_request:Ljava/util/List;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 678
    sget-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cbms/DisputedPayment;->resolution:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 679
    sget-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cbms/DisputedPayment;->protection_state:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 680
    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cbms/DisputedPayment;->submitted_to_bank_at:Lcom/squareup/protos/common/time/DateTime;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 681
    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cbms/DisputedPayment;->resolution_decided_at:Lcom/squareup/protos/common/time/DateTime;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 682
    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_created_at:Lcom/squareup/protos/common/time/DateTime;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 683
    sget-object v0, Lcom/squareup/protos/common/time/YearMonthDay;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cbms/DisputedPayment;->latest_reporting_date:Lcom/squareup/protos/common/time/YearMonthDay;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 684
    sget-object v0, Lcom/squareup/protos/common/time/YearMonthDay;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cbms/DisputedPayment;->expected_resolution_date:Lcom/squareup/protos/common/time/YearMonthDay;

    const/16 v2, 0xe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 685
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_amount:Lcom/squareup/protos/common/Money;

    const/16 v2, 0xf

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 686
    sget-object v0, Lcom/squareup/protos/common/instrument/InstrumentType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_card_brand:Lcom/squareup/protos/common/instrument/InstrumentType;

    const/16 v2, 0x10

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 687
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_card_pan_suffix:Ljava/lang/String;

    const/16 v2, 0x11

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 688
    invoke-virtual {p2}, Lcom/squareup/protos/client/cbms/DisputedPayment;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 640
    check-cast p2, Lcom/squareup/protos/client/cbms/DisputedPayment;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtoAdapter_DisputedPayment;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/cbms/DisputedPayment;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/cbms/DisputedPayment;)I
    .locals 4

    .line 647
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->unit_token:Ljava/lang/String;

    const/16 v3, 0x12

    .line 648
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/cbms/ActionableStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

    const/4 v3, 0x2

    .line 649
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->dispute_reason:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    const/4 v3, 0x3

    .line 650
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->bank_disputed_comment:Ljava/lang/String;

    const/4 v3, 0x4

    .line 651
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_amount_held:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x5

    .line 652
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_disputed_amount:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x6

    .line 653
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/cbms/InformationRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 654
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->information_request:Ljava/util/List;

    const/4 v3, 0x7

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->resolution:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    const/16 v3, 0x8

    .line 655
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->protection_state:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    const/16 v3, 0x9

    .line 656
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->submitted_to_bank_at:Lcom/squareup/protos/common/time/DateTime;

    const/16 v3, 0xa

    .line 657
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->resolution_decided_at:Lcom/squareup/protos/common/time/DateTime;

    const/16 v3, 0xb

    .line 658
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_created_at:Lcom/squareup/protos/common/time/DateTime;

    const/16 v3, 0xc

    .line 659
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/time/YearMonthDay;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->latest_reporting_date:Lcom/squareup/protos/common/time/YearMonthDay;

    const/16 v3, 0xd

    .line 660
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/time/YearMonthDay;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->expected_resolution_date:Lcom/squareup/protos/common/time/YearMonthDay;

    const/16 v3, 0xe

    .line 661
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_amount:Lcom/squareup/protos/common/Money;

    const/16 v3, 0xf

    .line 662
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/instrument/InstrumentType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_card_brand:Lcom/squareup/protos/common/instrument/InstrumentType;

    const/16 v3, 0x10

    .line 663
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_card_pan_suffix:Ljava/lang/String;

    const/16 v3, 0x11

    .line 664
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 665
    invoke-virtual {p1}, Lcom/squareup/protos/client/cbms/DisputedPayment;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 640
    check-cast p1, Lcom/squareup/protos/client/cbms/DisputedPayment;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtoAdapter_DisputedPayment;->encodedSize(Lcom/squareup/protos/client/cbms/DisputedPayment;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/cbms/DisputedPayment;)Lcom/squareup/protos/client/cbms/DisputedPayment;
    .locals 2

    .line 761
    invoke-virtual {p1}, Lcom/squareup/protos/client/cbms/DisputedPayment;->newBuilder()Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;

    move-result-object p1

    .line 762
    iget-object v0, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->current_amount_held:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->current_amount_held:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->current_amount_held:Lcom/squareup/protos/common/Money;

    .line 763
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->current_disputed_amount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->current_disputed_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->current_disputed_amount:Lcom/squareup/protos/common/Money;

    .line 764
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->information_request:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/cbms/InformationRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 765
    iget-object v0, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->submitted_to_bank_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->submitted_to_bank_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/DateTime;

    iput-object v0, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->submitted_to_bank_at:Lcom/squareup/protos/common/time/DateTime;

    .line 766
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->resolution_decided_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->resolution_decided_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/DateTime;

    iput-object v0, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->resolution_decided_at:Lcom/squareup/protos/common/time/DateTime;

    .line 767
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->payment_created_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->payment_created_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/DateTime;

    iput-object v0, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->payment_created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 768
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->latest_reporting_date:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/common/time/YearMonthDay;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->latest_reporting_date:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/YearMonthDay;

    iput-object v0, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->latest_reporting_date:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 769
    :cond_5
    iget-object v0, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->expected_resolution_date:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/protos/common/time/YearMonthDay;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->expected_resolution_date:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/YearMonthDay;

    iput-object v0, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->expected_resolution_date:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 770
    :cond_6
    iget-object v0, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->payment_amount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->payment_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->payment_amount:Lcom/squareup/protos/common/Money;

    .line 771
    :cond_7
    invoke-virtual {p1}, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 772
    invoke-virtual {p1}, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->build()Lcom/squareup/protos/client/cbms/DisputedPayment;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 640
    check-cast p1, Lcom/squareup/protos/client/cbms/DisputedPayment;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtoAdapter_DisputedPayment;->redact(Lcom/squareup/protos/client/cbms/DisputedPayment;)Lcom/squareup/protos/client/cbms/DisputedPayment;

    move-result-object p1

    return-object p1
.end method
