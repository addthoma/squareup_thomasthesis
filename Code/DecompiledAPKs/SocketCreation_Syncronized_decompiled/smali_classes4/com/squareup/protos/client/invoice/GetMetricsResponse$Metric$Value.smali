.class public final Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;
.super Lcom/squareup/wire/Message;
.source "GetMetricsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Value"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value$ProtoAdapter_Value;,
        Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;",
        "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_VALUE_TYPE:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;

.field private static final serialVersionUID:J


# instance fields
.field public final money_value:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final value_type:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.GetMetricsResponse$Metric$ValueType#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 268
    new-instance v0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value$ProtoAdapter_Value;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value$ProtoAdapter_Value;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 272
    sget-object v0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;->MONEY_VALUE:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;

    sput-object v0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;->DEFAULT_VALUE_TYPE:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;Lcom/squareup/protos/common/Money;)V
    .locals 1

    .line 287
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;-><init>(Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V
    .locals 1

    .line 291
    sget-object v0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 292
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;->value_type:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;

    .line 293
    iput-object p2, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;->money_value:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 308
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 309
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;

    .line 310
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;->value_type:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;->value_type:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;

    .line 311
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;->money_value:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;->money_value:Lcom/squareup/protos/common/Money;

    .line 312
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 317
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 319
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 320
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;->value_type:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 321
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;->money_value:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 322
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value$Builder;
    .locals 2

    .line 298
    new-instance v0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value$Builder;-><init>()V

    .line 299
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;->value_type:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value$Builder;->value_type:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;

    .line 300
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;->money_value:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value$Builder;->money_value:Lcom/squareup/protos/common/Money;

    .line 301
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 267
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;->newBuilder()Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 329
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 330
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;->value_type:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;

    if-eqz v1, :cond_0

    const-string v1, ", value_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;->value_type:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 331
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;->money_value:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    const-string v1, ", money_value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;->money_value:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Value{"

    .line 332
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
