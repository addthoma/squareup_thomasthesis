.class public final Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListHistoryEventsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;",
        "Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public current_balance:Lcom/squareup/protos/common/Money;

.field public history_events:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/HistoryEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 100
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 101
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse$Builder;->history_events:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;
    .locals 4

    .line 120
    new-instance v0, Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse$Builder;->history_events:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse$Builder;->current_balance:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;-><init>(Ljava/util/List;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 95
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse$Builder;->build()Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;

    move-result-object v0

    return-object v0
.end method

.method public current_balance(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse$Builder;
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse$Builder;->current_balance:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public history_events(Ljava/util/List;)Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/HistoryEvent;",
            ">;)",
            "Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse$Builder;"
        }
    .end annotation

    .line 108
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 109
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse$Builder;->history_events:Ljava/util/List;

    return-object p0
.end method
