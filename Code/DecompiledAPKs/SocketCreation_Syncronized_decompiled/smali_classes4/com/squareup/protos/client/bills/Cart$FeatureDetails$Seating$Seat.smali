.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;
.super Lcom/squareup/wire/Message;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Seat"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$ProtoAdapter_Seat;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_ORDINAL:Ljava/lang/Integer;

.field public static final DEFAULT_SOURCE_OPEN_TICKET_CLIENT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_SOURCE_OPEN_TICKET_NAME:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final created_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final ordinal:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x2
    .end annotation
.end field

.field public final seat_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final source_open_ticket_client_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final source_open_ticket_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x6
    .end annotation
.end field

.field public final source_ticket_information:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$SourceTicketInformation#ADAPTER"
        tag = 0x7
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 4201
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$ProtoAdapter_Seat;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$ProtoAdapter_Seat;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 4205
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->DEFAULT_ORDINAL:Ljava/lang/Integer;

    .line 4207
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->DEFAULT_ENABLED:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;)V
    .locals 9

    .line 4280
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;-><init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;Lokio/ByteString;)V
    .locals 1

    .line 4286
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 4287
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->seat_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 4288
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->ordinal:Ljava/lang/Integer;

    .line 4289
    iput-object p3, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->enabled:Ljava/lang/Boolean;

    .line 4290
    iput-object p4, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 4291
    iput-object p5, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->source_open_ticket_client_id:Ljava/lang/String;

    .line 4292
    iput-object p6, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->source_open_ticket_name:Ljava/lang/String;

    .line 4293
    iput-object p7, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->source_ticket_information:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 4313
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 4314
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;

    .line 4315
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->seat_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->seat_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 4316
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->ordinal:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->ordinal:Ljava/lang/Integer;

    .line 4317
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->enabled:Ljava/lang/Boolean;

    .line 4318
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 4319
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->source_open_ticket_client_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->source_open_ticket_client_id:Ljava/lang/String;

    .line 4320
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->source_open_ticket_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->source_open_ticket_name:Ljava/lang/String;

    .line 4321
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->source_ticket_information:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->source_ticket_information:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;

    .line 4322
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 4327
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 4329
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 4330
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->seat_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4331
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->ordinal:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4332
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4333
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4334
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->source_open_ticket_client_id:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4335
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->source_open_ticket_name:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4336
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->source_ticket_information:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 4337
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;
    .locals 2

    .line 4298
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;-><init>()V

    .line 4299
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->seat_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->seat_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 4300
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->ordinal:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->ordinal:Ljava/lang/Integer;

    .line 4301
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->enabled:Ljava/lang/Boolean;

    .line 4302
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 4303
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->source_open_ticket_client_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->source_open_ticket_client_id:Ljava/lang/String;

    .line 4304
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->source_open_ticket_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->source_open_ticket_name:Ljava/lang/String;

    .line 4305
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->source_ticket_information:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->source_ticket_information:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;

    .line 4306
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 4200
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 4344
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4345
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->seat_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", seat_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->seat_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4346
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->ordinal:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", ordinal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->ordinal:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4347
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4348
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_3

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4349
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->source_open_ticket_client_id:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", source_open_ticket_client_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->source_open_ticket_client_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4350
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->source_open_ticket_name:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", source_open_ticket_name=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4351
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->source_ticket_information:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;

    if-eqz v1, :cond_6

    const-string v1, ", source_ticket_information="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->source_ticket_information:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Seat{"

    .line 4352
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
