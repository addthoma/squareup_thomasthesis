.class public final Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListManualMergeProposalRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;",
        "Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public contact_set:Lcom/squareup/protos/client/rolodex/ContactSet;

.field public duplicate_contact_token_set:Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 100
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;
    .locals 4

    .line 120
    new-instance v0, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$Builder;->duplicate_contact_token_set:Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$Builder;->contact_set:Lcom/squareup/protos/client/rolodex/ContactSet;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;-><init>(Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;Lcom/squareup/protos/client/rolodex/ContactSet;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 95
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;

    move-result-object v0

    return-object v0
.end method

.method public contact_set(Lcom/squareup/protos/client/rolodex/ContactSet;)Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$Builder;
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$Builder;->contact_set:Lcom/squareup/protos/client/rolodex/ContactSet;

    return-object p0
.end method

.method public duplicate_contact_token_set(Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;)Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 109
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$Builder;->duplicate_contact_token_set:Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;

    return-object p0
.end method
