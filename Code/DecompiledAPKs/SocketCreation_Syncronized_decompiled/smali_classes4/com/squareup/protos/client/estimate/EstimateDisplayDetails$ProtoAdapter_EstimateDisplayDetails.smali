.class final Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$ProtoAdapter_EstimateDisplayDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "EstimateDisplayDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_EstimateDisplayDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 510
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 551
    new-instance v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;-><init>()V

    .line 552
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 553
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 576
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 574
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->selected_package_token(Ljava/lang/String;)Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;

    goto :goto_0

    .line 573
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->is_archived(Ljava/lang/Boolean;)Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;

    goto :goto_0

    .line 572
    :pswitch_2
    iget-object v3, v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->event:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/invoice/InvoiceEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 571
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/client/invoice/InvoiceTimeline;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->estimate_timeline(Lcom/squareup/protos/client/invoice/InvoiceTimeline;)Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;

    goto :goto_0

    .line 570
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->canceled_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;

    goto :goto_0

    .line 569
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->linked_invoice_token(Ljava/lang/String;)Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;

    goto :goto_0

    .line 563
    :pswitch_6
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->display_state(Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;)Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 565
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 560
    :pswitch_7
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->accepted_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;

    goto :goto_0

    .line 559
    :pswitch_8
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->delivered_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;

    goto/16 :goto_0

    .line 558
    :pswitch_9
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->updated_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;

    goto/16 :goto_0

    .line 557
    :pswitch_a
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;

    goto/16 :goto_0

    .line 556
    :pswitch_b
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->sort_date(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;

    goto/16 :goto_0

    .line 555
    :pswitch_c
    sget-object v3, Lcom/squareup/protos/client/estimate/Estimate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/estimate/Estimate;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->estimate(Lcom/squareup/protos/client/estimate/Estimate;)Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;

    goto/16 :goto_0

    .line 580
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 581
    invoke-virtual {v0}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->build()Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 508
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$ProtoAdapter_EstimateDisplayDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 533
    sget-object v0, Lcom/squareup/protos/client/estimate/Estimate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->estimate:Lcom/squareup/protos/client/estimate/Estimate;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 534
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 535
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 536
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 537
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->delivered_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 538
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->accepted_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 539
    sget-object v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->display_state:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 540
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->linked_invoice_token:Ljava/lang/String;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 541
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->canceled_at:Lcom/squareup/protos/client/ISO8601Date;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 542
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->estimate_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 543
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->event:Ljava/util/List;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 544
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->is_archived:Ljava/lang/Boolean;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 545
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->selected_package_token:Ljava/lang/String;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 546
    invoke-virtual {p2}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 508
    check-cast p2, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$ProtoAdapter_EstimateDisplayDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;)I
    .locals 4

    .line 515
    sget-object v0, Lcom/squareup/protos/client/estimate/Estimate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->estimate:Lcom/squareup/protos/client/estimate/Estimate;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x2

    .line 516
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x3

    .line 517
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x4

    .line 518
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->delivered_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x5

    .line 519
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->accepted_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x6

    .line 520
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->display_state:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    const/4 v3, 0x7

    .line 521
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->linked_invoice_token:Ljava/lang/String;

    const/16 v3, 0x8

    .line 522
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->canceled_at:Lcom/squareup/protos/client/ISO8601Date;

    const/16 v3, 0x9

    .line 523
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceTimeline;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->estimate_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    const/16 v3, 0xa

    .line 524
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 525
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->event:Ljava/util/List;

    const/16 v3, 0xb

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->is_archived:Ljava/lang/Boolean;

    const/16 v3, 0xc

    .line 526
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->selected_package_token:Ljava/lang/String;

    const/16 v3, 0xd

    .line 527
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 528
    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 508
    check-cast p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$ProtoAdapter_EstimateDisplayDetails;->encodedSize(Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;)Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;
    .locals 2

    .line 586
    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->newBuilder()Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;

    move-result-object p1

    .line 587
    iget-object v0, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->estimate:Lcom/squareup/protos/client/estimate/Estimate;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/estimate/Estimate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->estimate:Lcom/squareup/protos/client/estimate/Estimate;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/estimate/Estimate;

    iput-object v0, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->estimate:Lcom/squareup/protos/client/estimate/Estimate;

    .line 588
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    .line 589
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 590
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 591
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->delivered_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->delivered_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->delivered_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 592
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->accepted_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->accepted_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->accepted_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 593
    :cond_5
    iget-object v0, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->canceled_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->canceled_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->canceled_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 594
    :cond_6
    iget-object v0, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->estimate_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->estimate_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    iput-object v0, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->estimate_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    .line 595
    :cond_7
    iget-object v0, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->event:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 596
    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 597
    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->build()Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 508
    check-cast p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$ProtoAdapter_EstimateDisplayDetails;->redact(Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;)Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;

    move-result-object p1

    return-object p1
.end method
