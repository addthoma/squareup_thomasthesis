.class public final enum Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;
.super Ljava/lang/Enum;
.source "CustomAttributeText.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/CustomAttributeText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Condition"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition$ProtoAdapter_Condition;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum EQUALS:Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 22
    new-instance v0, Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;->UNKNOWN:Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

    .line 24
    new-instance v0, Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

    const/4 v2, 0x1

    const-string v3, "EQUALS"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;->EQUALS:Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

    .line 21
    sget-object v3, Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;->UNKNOWN:Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;->EQUALS:Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;->$VALUES:[Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

    .line 26
    new-instance v0, Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition$ProtoAdapter_Condition;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition$ProtoAdapter_Condition;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 31
    iput p3, p0, Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;
    .locals 1

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 40
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;->EQUALS:Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

    return-object p0

    .line 39
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;->UNKNOWN:Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;
    .locals 1

    .line 21
    const-class v0, Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;
    .locals 1

    .line 21
    sget-object v0, Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;->$VALUES:[Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 47
    iget v0, p0, Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;->value:I

    return v0
.end method
