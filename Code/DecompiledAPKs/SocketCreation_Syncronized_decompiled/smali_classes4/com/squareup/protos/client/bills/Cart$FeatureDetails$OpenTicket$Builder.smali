.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public created_at:Lcom/squareup/protos/client/ISO8601Date;

.field public name:Ljava/lang/String;

.field public note:Ljava/lang/String;

.field public predefined_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

.field public products_opened_on:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$OpenTicketProductAssociation;",
            ">;"
        }
    .end annotation
.end field

.field public ticket_id_pair:Lcom/squareup/protos/client/IdPair;

.field public ticket_owner:Lcom/squareup/protos/client/CreatorDetails;

.field public unit_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1641
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 1642
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->products_opened_on:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;
    .locals 11

    .line 1717
    new-instance v10, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->note:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->ticket_owner:Lcom/squareup/protos/client/CreatorDetails;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->predefined_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->unit_token:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v8, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->products_opened_on:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/CreatorDetails;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Ljava/util/List;Lokio/ByteString;)V

    return-object v10
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1624
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    move-result-object v0

    return-object v0
.end method

.method public created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;
    .locals 0

    .line 1702
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;
    .locals 0

    .line 1649
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public note(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;
    .locals 0

    .line 1659
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->note:Ljava/lang/String;

    return-object p0
.end method

.method public predefined_ticket(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;
    .locals 0

    .line 1684
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->predefined_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    return-object p0
.end method

.method public products_opened_on(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$OpenTicketProductAssociation;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;"
        }
    .end annotation

    .line 1710
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1711
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->products_opened_on:Ljava/util/List;

    return-object p0
.end method

.method public ticket_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;
    .locals 0

    .line 1668
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public ticket_owner(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;
    .locals 0

    .line 1679
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->ticket_owner:Lcom/squareup/protos/client/CreatorDetails;

    return-object p0
.end method

.method public unit_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;
    .locals 0

    .line 1692
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method
