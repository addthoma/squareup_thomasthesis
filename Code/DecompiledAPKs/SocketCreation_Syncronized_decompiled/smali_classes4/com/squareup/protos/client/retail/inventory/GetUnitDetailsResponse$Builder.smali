.class public final Lcom/squareup/protos/client/retail/inventory/GetUnitDetailsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetUnitDetailsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/retail/inventory/GetUnitDetailsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/retail/inventory/GetUnitDetailsResponse;",
        "Lcom/squareup/protos/client/retail/inventory/GetUnitDetailsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public hours:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/retail/inventory/Hours;",
            ">;"
        }
    .end annotation
.end field

.field public time_zone:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 100
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 101
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/retail/inventory/GetUnitDetailsResponse$Builder;->hours:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/retail/inventory/GetUnitDetailsResponse;
    .locals 4

    .line 123
    new-instance v0, Lcom/squareup/protos/client/retail/inventory/GetUnitDetailsResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/retail/inventory/GetUnitDetailsResponse$Builder;->hours:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/retail/inventory/GetUnitDetailsResponse$Builder;->time_zone:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/retail/inventory/GetUnitDetailsResponse;-><init>(Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 95
    invoke-virtual {p0}, Lcom/squareup/protos/client/retail/inventory/GetUnitDetailsResponse$Builder;->build()Lcom/squareup/protos/client/retail/inventory/GetUnitDetailsResponse;

    move-result-object v0

    return-object v0
.end method

.method public hours(Ljava/util/List;)Lcom/squareup/protos/client/retail/inventory/GetUnitDetailsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/retail/inventory/Hours;",
            ">;)",
            "Lcom/squareup/protos/client/retail/inventory/GetUnitDetailsResponse$Builder;"
        }
    .end annotation

    .line 108
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 109
    iput-object p1, p0, Lcom/squareup/protos/client/retail/inventory/GetUnitDetailsResponse$Builder;->hours:Ljava/util/List;

    return-object p0
.end method

.method public time_zone(Ljava/lang/String;)Lcom/squareup/protos/client/retail/inventory/GetUnitDetailsResponse$Builder;
    .locals 0

    .line 117
    iput-object p1, p0, Lcom/squareup/protos/client/retail/inventory/GetUnitDetailsResponse$Builder;->time_zone:Ljava/lang/String;

    return-object p0
.end method
