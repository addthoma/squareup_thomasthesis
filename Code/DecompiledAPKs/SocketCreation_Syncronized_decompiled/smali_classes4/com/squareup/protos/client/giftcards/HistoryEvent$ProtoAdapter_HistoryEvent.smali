.class final Lcom/squareup/protos/client/giftcards/HistoryEvent$ProtoAdapter_HistoryEvent;
.super Lcom/squareup/wire/ProtoAdapter;
.source "HistoryEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/giftcards/HistoryEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_HistoryEvent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/giftcards/HistoryEvent;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 249
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/giftcards/HistoryEvent;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/giftcards/HistoryEvent;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 272
    new-instance v0, Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;-><init>()V

    .line 273
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 274
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 288
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 286
    :cond_0
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;

    goto :goto_0

    .line 285
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;->description(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;

    goto :goto_0

    .line 284
    :cond_2
    sget-object v3, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;->created_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;

    goto :goto_0

    .line 278
    :cond_3
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;->type(Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;)Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 280
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 292
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 293
    invoke-virtual {v0}, Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;->build()Lcom/squareup/protos/client/giftcards/HistoryEvent;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 247
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/giftcards/HistoryEvent$ProtoAdapter_HistoryEvent;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/giftcards/HistoryEvent;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/giftcards/HistoryEvent;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 263
    sget-object v0, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/giftcards/HistoryEvent;->type:Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 264
    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/giftcards/HistoryEvent;->created_at:Lcom/squareup/protos/common/time/DateTime;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 265
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/giftcards/HistoryEvent;->description:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 266
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/giftcards/HistoryEvent;->amount:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 267
    invoke-virtual {p2}, Lcom/squareup/protos/client/giftcards/HistoryEvent;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 247
    check-cast p2, Lcom/squareup/protos/client/giftcards/HistoryEvent;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/giftcards/HistoryEvent$ProtoAdapter_HistoryEvent;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/giftcards/HistoryEvent;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/giftcards/HistoryEvent;)I
    .locals 4

    .line 254
    sget-object v0, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/giftcards/HistoryEvent;->type:Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/giftcards/HistoryEvent;->created_at:Lcom/squareup/protos/common/time/DateTime;

    const/4 v3, 0x2

    .line 255
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/giftcards/HistoryEvent;->description:Ljava/lang/String;

    const/4 v3, 0x3

    .line 256
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/giftcards/HistoryEvent;->amount:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x4

    .line 257
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 258
    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/HistoryEvent;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 247
    check-cast p1, Lcom/squareup/protos/client/giftcards/HistoryEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/giftcards/HistoryEvent$ProtoAdapter_HistoryEvent;->encodedSize(Lcom/squareup/protos/client/giftcards/HistoryEvent;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/giftcards/HistoryEvent;)Lcom/squareup/protos/client/giftcards/HistoryEvent;
    .locals 2

    .line 298
    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/HistoryEvent;->newBuilder()Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;

    move-result-object p1

    .line 299
    iget-object v0, p1, Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/DateTime;

    iput-object v0, p1, Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 300
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;->amount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;->amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;->amount:Lcom/squareup/protos/common/Money;

    .line 301
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 302
    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;->build()Lcom/squareup/protos/client/giftcards/HistoryEvent;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 247
    check-cast p1, Lcom/squareup/protos/client/giftcards/HistoryEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/giftcards/HistoryEvent$ProtoAdapter_HistoryEvent;->redact(Lcom/squareup/protos/client/giftcards/HistoryEvent;)Lcom/squareup/protos/client/giftcards/HistoryEvent;

    move-result-object p1

    return-object p1
.end method
