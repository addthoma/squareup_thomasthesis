.class public final Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;
.super Lcom/squareup/wire/Message;
.source "AttributeSchema.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/AttributeSchema;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Attribute"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$ProtoAdapter_Attribute;,
        Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;,
        Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_FALLBACK_VALUE:Ljava/lang/String; = ""

.field public static final DEFAULT_IS_VISIBLE_IN_PROFILE:Ljava/lang/Boolean;

.field public static final DEFAULT_KEY:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

.field public static final DEFAULT_WAS_BOOLEAN_EVER_SET:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.AttributeSchema$Attribute$Data#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final fallback_value:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final is_visible_in_profile:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final key:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.AttributeSchema$Type#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final was_boolean_ever_set:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 575
    new-instance v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$ProtoAdapter_Attribute;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$ProtoAdapter_Attribute;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 583
    sget-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->UNKNOWN:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    sput-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->DEFAULT_TYPE:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    const/4 v0, 0x0

    .line 587
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->DEFAULT_IS_VISIBLE_IN_PROFILE:Ljava/lang/Boolean;

    .line 589
    sput-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->DEFAULT_WAS_BOOLEAN_EVER_SET:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 9

    .line 650
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 655
    sget-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 656
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->key:Ljava/lang/String;

    .line 657
    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->name:Ljava/lang/String;

    .line 658
    iput-object p3, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    .line 659
    iput-object p4, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->fallback_value:Ljava/lang/String;

    .line 660
    iput-object p5, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    .line 661
    iput-object p6, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->is_visible_in_profile:Ljava/lang/Boolean;

    .line 662
    iput-object p7, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->was_boolean_ever_set:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 682
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 683
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    .line 684
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->key:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->key:Ljava/lang/String;

    .line 685
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->name:Ljava/lang/String;

    .line 686
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    .line 687
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->fallback_value:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->fallback_value:Ljava/lang/String;

    .line 688
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    .line 689
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->is_visible_in_profile:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->is_visible_in_profile:Ljava/lang/Boolean;

    .line 690
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->was_boolean_ever_set:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->was_boolean_ever_set:Ljava/lang/Boolean;

    .line 691
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 696
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 698
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 699
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->key:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 700
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 701
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 702
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->fallback_value:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 703
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 704
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->is_visible_in_profile:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 705
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->was_boolean_ever_set:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 706
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;
    .locals 2

    .line 667
    new-instance v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;-><init>()V

    .line 668
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->key:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->key:Ljava/lang/String;

    .line 669
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->name:Ljava/lang/String;

    .line 670
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    .line 671
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->fallback_value:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->fallback_value:Ljava/lang/String;

    .line 672
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    .line 673
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->is_visible_in_profile:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->is_visible_in_profile:Ljava/lang/Boolean;

    .line 674
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->was_boolean_ever_set:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->was_boolean_ever_set:Ljava/lang/Boolean;

    .line 675
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 574
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->newBuilder()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 713
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 714
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->key:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 715
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 716
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    if-eqz v1, :cond_2

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 717
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->fallback_value:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", fallback_value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->fallback_value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 718
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    if-eqz v1, :cond_4

    const-string v1, ", data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 719
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->is_visible_in_profile:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", is_visible_in_profile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->is_visible_in_profile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 720
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->was_boolean_ever_set:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", was_boolean_ever_set="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->was_boolean_ever_set:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Attribute{"

    .line 721
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
