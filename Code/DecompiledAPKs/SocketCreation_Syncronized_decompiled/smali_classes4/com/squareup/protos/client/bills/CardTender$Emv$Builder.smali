.class public final Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CardTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardTender$Emv;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/CardTender$Emv;",
        "Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public buyer_selected_account_name:Ljava/lang/String;

.field public is_emv_capable_reader_present:Ljava/lang/Boolean;

.field public read_only_application_preferred_name:Ljava/lang/String;

.field public read_only_cardholder_verification_method_used:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

.field public read_only_chip_card_application_id:Ljava/lang/String;

.field public read_only_chip_card_preferred_language_code:Ljava/lang/String;

.field public read_only_encrypted_emv_data:Lokio/ByteString;

.field public read_only_plaintext_emv_data:Lokio/ByteString;

.field public write_only_hardware_serial_number:Ljava/lang/String;

.field public write_only_processing_type:Lcom/squareup/protos/client/bills/CardTender$Emv$ProcessingType;

.field public write_only_reader_firmware_version:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1613
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/CardTender$Emv;
    .locals 14

    .line 1719
    new-instance v13, Lcom/squareup/protos/client/bills/CardTender$Emv;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->read_only_encrypted_emv_data:Lokio/ByteString;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->write_only_processing_type:Lcom/squareup/protos/client/bills/CardTender$Emv$ProcessingType;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->read_only_chip_card_application_id:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->write_only_reader_firmware_version:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->write_only_hardware_serial_number:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->read_only_plaintext_emv_data:Lokio/ByteString;

    iget-object v7, p0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->is_emv_capable_reader_present:Ljava/lang/Boolean;

    iget-object v8, p0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->read_only_application_preferred_name:Ljava/lang/String;

    iget-object v9, p0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->read_only_cardholder_verification_method_used:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    iget-object v10, p0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->read_only_chip_card_preferred_language_code:Ljava/lang/String;

    iget-object v11, p0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->buyer_selected_account_name:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v12

    move-object v0, v13

    invoke-direct/range {v0 .. v12}, Lcom/squareup/protos/client/bills/CardTender$Emv;-><init>(Lokio/ByteString;Lcom/squareup/protos/client/bills/CardTender$Emv$ProcessingType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;Ljava/lang/Boolean;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v13
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1590
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->build()Lcom/squareup/protos/client/bills/CardTender$Emv;

    move-result-object v0

    return-object v0
.end method

.method public buyer_selected_account_name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;
    .locals 0

    .line 1713
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->buyer_selected_account_name:Ljava/lang/String;

    return-object p0
.end method

.method public is_emv_capable_reader_present(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;
    .locals 0

    .line 1675
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->is_emv_capable_reader_present:Ljava/lang/Boolean;

    return-object p0
.end method

.method public read_only_application_preferred_name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;
    .locals 0

    .line 1685
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->read_only_application_preferred_name:Ljava/lang/String;

    return-object p0
.end method

.method public read_only_cardholder_verification_method_used(Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;)Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;
    .locals 0

    .line 1695
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->read_only_cardholder_verification_method_used:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    return-object p0
.end method

.method public read_only_chip_card_application_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;
    .locals 0

    .line 1638
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->read_only_chip_card_application_id:Ljava/lang/String;

    return-object p0
.end method

.method public read_only_chip_card_preferred_language_code(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;
    .locals 0

    .line 1705
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->read_only_chip_card_preferred_language_code:Ljava/lang/String;

    return-object p0
.end method

.method public read_only_encrypted_emv_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;
    .locals 0

    .line 1621
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->read_only_encrypted_emv_data:Lokio/ByteString;

    return-object p0
.end method

.method public read_only_plaintext_emv_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;
    .locals 0

    .line 1665
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->read_only_plaintext_emv_data:Lokio/ByteString;

    return-object p0
.end method

.method public write_only_hardware_serial_number(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;
    .locals 0

    .line 1656
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->write_only_hardware_serial_number:Ljava/lang/String;

    return-object p0
.end method

.method public write_only_processing_type(Lcom/squareup/protos/client/bills/CardTender$Emv$ProcessingType;)Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;
    .locals 0

    .line 1629
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->write_only_processing_type:Lcom/squareup/protos/client/bills/CardTender$Emv$ProcessingType;

    return-object p0
.end method

.method public write_only_reader_firmware_version(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;
    .locals 0

    .line 1646
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->write_only_reader_firmware_version:Ljava/lang/String;

    return-object p0
.end method
