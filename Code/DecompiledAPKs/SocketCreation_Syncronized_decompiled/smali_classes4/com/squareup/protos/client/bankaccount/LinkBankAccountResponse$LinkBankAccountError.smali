.class public final enum Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;
.super Ljava/lang/Enum;
.source "LinkBankAccountResponse.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LinkBankAccountError"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError$ProtoAdapter_LinkBankAccountError;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DOES_NOT_SUPPORT_INSTANT_PAYOUT:Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

.field public static final enum HAS_EXISTING_PENDING_ACCOUNT:Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

.field public static final enum INCORRECT_PASSWORD:Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

.field public static final enum INVALID_BANK_ACCOUNT:Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

.field public static final enum INVALID_BANK_CODE:Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

.field public static final enum LINK_BANK_ACCOUNT_ERROR_DO_NOT_USE:Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 162
    new-instance v0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    const/4 v1, 0x0

    const-string v2, "LINK_BANK_ACCOUNT_ERROR_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;->LINK_BANK_ACCOUNT_ERROR_DO_NOT_USE:Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    .line 167
    new-instance v0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    const/4 v2, 0x1

    const-string v3, "HAS_EXISTING_PENDING_ACCOUNT"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;->HAS_EXISTING_PENDING_ACCOUNT:Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    .line 172
    new-instance v0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    const/4 v3, 0x2

    const-string v4, "INVALID_BANK_ACCOUNT"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;->INVALID_BANK_ACCOUNT:Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    .line 177
    new-instance v0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    const/4 v4, 0x3

    const-string v5, "INCORRECT_PASSWORD"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;->INCORRECT_PASSWORD:Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    .line 182
    new-instance v0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    const/4 v5, 0x4

    const-string v6, "INVALID_BANK_CODE"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;->INVALID_BANK_CODE:Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    .line 187
    new-instance v0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    const/4 v6, 0x5

    const-string v7, "DOES_NOT_SUPPORT_INSTANT_PAYOUT"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;->DOES_NOT_SUPPORT_INSTANT_PAYOUT:Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    .line 158
    sget-object v7, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;->LINK_BANK_ACCOUNT_ERROR_DO_NOT_USE:Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;->HAS_EXISTING_PENDING_ACCOUNT:Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;->INVALID_BANK_ACCOUNT:Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;->INCORRECT_PASSWORD:Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;->INVALID_BANK_CODE:Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;->DOES_NOT_SUPPORT_INSTANT_PAYOUT:Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;->$VALUES:[Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    .line 189
    new-instance v0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError$ProtoAdapter_LinkBankAccountError;

    invoke-direct {v0}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError$ProtoAdapter_LinkBankAccountError;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 193
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 194
    iput p3, p0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;
    .locals 1

    if-eqz p0, :cond_5

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 207
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;->DOES_NOT_SUPPORT_INSTANT_PAYOUT:Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    return-object p0

    .line 206
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;->INVALID_BANK_CODE:Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    return-object p0

    .line 205
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;->INCORRECT_PASSWORD:Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    return-object p0

    .line 204
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;->INVALID_BANK_ACCOUNT:Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    return-object p0

    .line 203
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;->HAS_EXISTING_PENDING_ACCOUNT:Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    return-object p0

    .line 202
    :cond_5
    sget-object p0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;->LINK_BANK_ACCOUNT_ERROR_DO_NOT_USE:Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;
    .locals 1

    .line 158
    const-class v0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;
    .locals 1

    .line 158
    sget-object v0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;->$VALUES:[Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 214
    iget v0, p0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;->value:I

    return v0
.end method
