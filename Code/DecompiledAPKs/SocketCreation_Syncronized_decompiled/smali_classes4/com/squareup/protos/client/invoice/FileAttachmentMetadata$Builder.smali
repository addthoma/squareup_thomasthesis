.class public final Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FileAttachmentMetadata.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
        "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public description:Ljava/lang/String;

.field public extension:Ljava/lang/String;

.field public invoice_token:Ljava/lang/String;

.field public mime_type:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public size_bytes:Ljava/lang/Integer;

.field public token:Ljava/lang/String;

.field public updated_at:Lcom/squareup/protos/common/time/DateTime;

.field public uploaded_at:Lcom/squareup/protos/common/time/DateTime;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 203
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;
    .locals 12

    .line 253
    new-instance v11, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->description:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->extension:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->mime_type:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->token:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->invoice_token:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->size_bytes:Ljava/lang/Integer;

    iget-object v8, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->uploaded_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v9, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->updated_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V

    return-object v11
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 184
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->build()Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    move-result-object v0

    return-object v0
.end method

.method public description(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;
    .locals 0

    .line 212
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public extension(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;
    .locals 0

    .line 217
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->extension:Ljava/lang/String;

    return-object p0
.end method

.method public invoice_token(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;
    .locals 0

    .line 232
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->invoice_token:Ljava/lang/String;

    return-object p0
.end method

.method public mime_type(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;
    .locals 0

    .line 222
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->mime_type:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;
    .locals 0

    .line 207
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public size_bytes(Ljava/lang/Integer;)Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;
    .locals 0

    .line 237
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->size_bytes:Ljava/lang/Integer;

    return-object p0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;
    .locals 0

    .line 227
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->token:Ljava/lang/String;

    return-object p0
.end method

.method public updated_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;
    .locals 0

    .line 247
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->updated_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public uploaded_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;
    .locals 0

    .line 242
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->uploaded_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method
