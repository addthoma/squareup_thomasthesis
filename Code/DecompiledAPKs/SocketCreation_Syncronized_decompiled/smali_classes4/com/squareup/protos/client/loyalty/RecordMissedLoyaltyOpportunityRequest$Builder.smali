.class public final Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RecordMissedLoyaltyOpportunityRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;",
        "Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bill_server_token:Ljava/lang/String;

.field public missed_loyalty_opportunity:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

.field public receipt_email_address:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;

.field public server_payment_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 137
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bill_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;
    .locals 0

    .line 161
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;->bill_server_token:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;
    .locals 7

    .line 172
    new-instance v6, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;->server_payment_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;->missed_loyalty_opportunity:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

    iget-object v3, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;->bill_server_token:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;->receipt_email_address:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;Ljava/lang/String;Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 128
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;

    move-result-object v0

    return-object v0
.end method

.method public missed_loyalty_opportunity(Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;)Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;
    .locals 0

    .line 153
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;->missed_loyalty_opportunity:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

    return-object p0
.end method

.method public receipt_email_address(Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;)Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;->receipt_email_address:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;

    return-object p0
.end method

.method public server_payment_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;->server_payment_token:Ljava/lang/String;

    return-object p0
.end method
