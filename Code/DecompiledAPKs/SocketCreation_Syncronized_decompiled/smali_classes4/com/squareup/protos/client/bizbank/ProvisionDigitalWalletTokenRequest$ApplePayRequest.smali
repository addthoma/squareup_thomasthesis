.class public final Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;
.super Lcom/squareup/wire/Message;
.source "ProvisionDigitalWalletTokenRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ApplePayRequest"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest$ProtoAdapter_ApplePayRequest;,
        Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;",
        "Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_NONCE:Lokio/ByteString;

.field public static final DEFAULT_NONCE_SIGNATURE:Lokio/ByteString;

.field private static final serialVersionUID:J


# instance fields
.field public final certificates:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        redacted = true
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lokio/ByteString;",
            ">;"
        }
    .end annotation
.end field

.field public final nonce:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        redacted = true
        tag = 0x2
    .end annotation
.end field

.field public final nonce_signature:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        redacted = true
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 212
    new-instance v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest$ProtoAdapter_ApplePayRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest$ProtoAdapter_ApplePayRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 216
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;->DEFAULT_NONCE:Lokio/ByteString;

    .line 218
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;->DEFAULT_NONCE_SIGNATURE:Lokio/ByteString;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lokio/ByteString;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lokio/ByteString;",
            ">;",
            "Lokio/ByteString;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 253
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;-><init>(Ljava/util/List;Lokio/ByteString;Lokio/ByteString;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lokio/ByteString;Lokio/ByteString;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lokio/ByteString;",
            ">;",
            "Lokio/ByteString;",
            "Lokio/ByteString;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 258
    sget-object v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p4, "certificates"

    .line 259
    invoke-static {p4, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;->certificates:Ljava/util/List;

    .line 260
    iput-object p2, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;->nonce:Lokio/ByteString;

    .line 261
    iput-object p3, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;->nonce_signature:Lokio/ByteString;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 277
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 278
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;

    .line 279
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;->certificates:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;->certificates:Ljava/util/List;

    .line 280
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;->nonce:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;->nonce:Lokio/ByteString;

    .line 281
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;->nonce_signature:Lokio/ByteString;

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;->nonce_signature:Lokio/ByteString;

    .line 282
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 287
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 289
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 290
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;->certificates:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 291
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;->nonce:Lokio/ByteString;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 292
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;->nonce_signature:Lokio/ByteString;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 293
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest$Builder;
    .locals 2

    .line 266
    new-instance v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest$Builder;-><init>()V

    .line 267
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;->certificates:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest$Builder;->certificates:Ljava/util/List;

    .line 268
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;->nonce:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest$Builder;->nonce:Lokio/ByteString;

    .line 269
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;->nonce_signature:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest$Builder;->nonce_signature:Lokio/ByteString;

    .line 270
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 211
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;->newBuilder()Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 300
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 301
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;->certificates:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", certificates=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 302
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;->nonce:Lokio/ByteString;

    if-eqz v1, :cond_1

    const-string v1, ", nonce=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;->nonce_signature:Lokio/ByteString;

    if-eqz v1, :cond_2

    const-string v1, ", nonce_signature=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ApplePayRequest{"

    .line 304
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
