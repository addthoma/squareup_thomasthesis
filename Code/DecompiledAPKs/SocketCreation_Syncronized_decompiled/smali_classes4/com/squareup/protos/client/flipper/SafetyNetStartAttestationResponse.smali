.class public final Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;
.super Lcom/squareup/wire/Message;
.source "SafetyNetStartAttestationResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse$ProtoAdapter_SafetyNetStartAttestationResponse;,
        Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;",
        "Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_API_KEY:Ljava/lang/String; = ""

.field public static final DEFAULT_NONCE:Lokio/ByteString;

.field private static final serialVersionUID:J


# instance fields
.field public final api_key:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final nonce:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse$ProtoAdapter_SafetyNetStartAttestationResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse$ProtoAdapter_SafetyNetStartAttestationResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 30
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;->DEFAULT_NONCE:Lokio/ByteString;

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Ljava/lang/String;)V
    .locals 1

    .line 58
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;-><init>(Lokio/ByteString;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 63
    sget-object v0, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 64
    iput-object p1, p0, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;->nonce:Lokio/ByteString;

    .line 65
    iput-object p2, p0, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;->api_key:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 80
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 81
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;

    .line 82
    invoke-virtual {p0}, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;->nonce:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;->nonce:Lokio/ByteString;

    .line 83
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;->api_key:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;->api_key:Ljava/lang/String;

    .line 84
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 89
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 91
    invoke-virtual {p0}, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 92
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;->nonce:Lokio/ByteString;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 93
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;->api_key:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 94
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse$Builder;
    .locals 2

    .line 70
    new-instance v0, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse$Builder;-><init>()V

    .line 71
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;->nonce:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse$Builder;->nonce:Lokio/ByteString;

    .line 72
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;->api_key:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse$Builder;->api_key:Ljava/lang/String;

    .line 73
    invoke-virtual {p0}, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;->newBuilder()Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;->nonce:Lokio/ByteString;

    if-eqz v1, :cond_0

    const-string v1, ", nonce="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;->nonce:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 103
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;->api_key:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", api_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;->api_key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SafetyNetStartAttestationResponse{"

    .line 104
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
