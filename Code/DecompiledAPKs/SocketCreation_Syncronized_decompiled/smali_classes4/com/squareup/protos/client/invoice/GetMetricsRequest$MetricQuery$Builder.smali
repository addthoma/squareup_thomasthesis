.class public final Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetMetricsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;",
        "Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

.field public metric_type:Lcom/squareup/protos/client/invoice/MetricType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 197
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;
    .locals 4

    .line 212
    new-instance v0, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;->metric_type:Lcom/squareup/protos/client/invoice/MetricType;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;-><init>(Lcom/squareup/protos/client/invoice/MetricType;Lcom/squareup/protos/common/time/DateTimeInterval;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 192
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;->build()Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;

    move-result-object v0

    return-object v0
.end method

.method public date_range(Lcom/squareup/protos/common/time/DateTimeInterval;)Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;
    .locals 0

    .line 206
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    return-object p0
.end method

.method public metric_type(Lcom/squareup/protos/client/invoice/MetricType;)Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;
    .locals 0

    .line 201
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;->metric_type:Lcom/squareup/protos/client/invoice/MetricType;

    return-object p0
.end method
