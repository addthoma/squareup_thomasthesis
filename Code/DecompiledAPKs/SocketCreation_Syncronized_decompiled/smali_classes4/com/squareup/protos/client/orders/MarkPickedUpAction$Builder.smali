.class public final Lcom/squareup/protos/client/orders/MarkPickedUpAction$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MarkPickedUpAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/orders/MarkPickedUpAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/orders/MarkPickedUpAction;",
        "Lcom/squareup/protos/client/orders/MarkPickedUpAction$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public picked_up_at:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 82
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/orders/MarkPickedUpAction;
    .locals 3

    .line 95
    new-instance v0, Lcom/squareup/protos/client/orders/MarkPickedUpAction;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/MarkPickedUpAction$Builder;->picked_up_at:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/orders/MarkPickedUpAction;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 79
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/MarkPickedUpAction$Builder;->build()Lcom/squareup/protos/client/orders/MarkPickedUpAction;

    move-result-object v0

    return-object v0
.end method

.method public picked_up_at(Ljava/lang/String;)Lcom/squareup/protos/client/orders/MarkPickedUpAction$Builder;
    .locals 0

    .line 89
    iput-object p1, p0, Lcom/squareup/protos/client/orders/MarkPickedUpAction$Builder;->picked_up_at:Ljava/lang/String;

    return-object p0
.end method
