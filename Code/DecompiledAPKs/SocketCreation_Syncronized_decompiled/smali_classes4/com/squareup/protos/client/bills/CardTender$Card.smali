.class public final Lcom/squareup/protos/client/bills/CardTender$Card;
.super Lcom/squareup/wire/Message;
.source "CardTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Card"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/CardTender$Card$ProtoAdapter_Card;,
        Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;,
        Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;,
        Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;,
        Lcom/squareup/protos/client/bills/CardTender$Card$Brand;,
        Lcom/squareup/protos/client/bills/CardTender$Card$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/CardTender$Card;",
        "Lcom/squareup/protos/client/bills/CardTender$Card$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/CardTender$Card;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field public static final DEFAULT_CHIP_CARD_FALLBACK_INDICATOR:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

.field public static final DEFAULT_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field public static final DEFAULT_FELICA_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

.field public static final DEFAULT_FELICA_MASKED_CARD_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_FELICA_SPRWID:Ljava/lang/String; = ""

.field public static final DEFAULT_PAN_SUFFIX:Ljava/lang/String; = ""

.field public static final DEFAULT_WRITE_ONLY_CAN_SWIPE_CHIP_CARD:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardTender$Card$Brand#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final chip_card_fallback_indicator:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardTender$Card$ChipCardFallbackIndicator#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardTender$Card$EntryMethod#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final felica_brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardTender$Card$FelicaBrand#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final felica_masked_card_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final felica_sprwid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final pan_suffix:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x3
    .end annotation
.end field

.field public final write_only_can_swipe_chip_card:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 223
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$ProtoAdapter_Card;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardTender$Card$ProtoAdapter_Card;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 227
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->UNKNOWN_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card;->DEFAULT_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 229
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->UNKNOWN_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card;->DEFAULT_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 233
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;->NONE:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card;->DEFAULT_CHIP_CARD_FALLBACK_INDICATOR:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    const/4 v0, 0x0

    .line 235
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card;->DEFAULT_WRITE_ONLY_CAN_SWIPE_CHIP_CARD:Ljava/lang/Boolean;

    .line 237
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->DEFAULT_FELICA_BRAND_DO_NOT_USE:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card;->DEFAULT_FELICA_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;Ljava/lang/Boolean;Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .line 310
    sget-object v9, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/client/bills/CardTender$Card;-><init>(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;Ljava/lang/Boolean;Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;Ljava/lang/Boolean;Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 317
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p9}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 318
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 319
    iput-object p2, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 320
    iput-object p3, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->pan_suffix:Ljava/lang/String;

    .line 321
    iput-object p4, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->chip_card_fallback_indicator:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    .line 322
    iput-object p5, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->write_only_can_swipe_chip_card:Ljava/lang/Boolean;

    .line 323
    iput-object p6, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    .line 324
    iput-object p7, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_masked_card_number:Ljava/lang/String;

    .line 325
    iput-object p8, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_sprwid:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 346
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/CardTender$Card;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 347
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/CardTender$Card;

    .line 348
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardTender$Card;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardTender$Card;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardTender$Card;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 349
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardTender$Card;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 350
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->pan_suffix:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardTender$Card;->pan_suffix:Ljava/lang/String;

    .line 351
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->chip_card_fallback_indicator:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardTender$Card;->chip_card_fallback_indicator:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    .line 352
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->write_only_can_swipe_chip_card:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardTender$Card;->write_only_can_swipe_chip_card:Ljava/lang/Boolean;

    .line 353
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    .line 354
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_masked_card_number:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_masked_card_number:Ljava/lang/String;

    .line 355
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_sprwid:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_sprwid:Ljava/lang/String;

    .line 356
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 361
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_8

    .line 363
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardTender$Card;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 364
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 365
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 366
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->pan_suffix:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 367
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->chip_card_fallback_indicator:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 368
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->write_only_can_swipe_chip_card:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 369
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 370
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_masked_card_number:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 371
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_sprwid:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_7
    add-int/2addr v0, v2

    .line 372
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_8
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/CardTender$Card$Builder;
    .locals 2

    .line 330
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;-><init>()V

    .line 331
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 332
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 333
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->pan_suffix:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->pan_suffix:Ljava/lang/String;

    .line 334
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->chip_card_fallback_indicator:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->chip_card_fallback_indicator:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    .line 335
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->write_only_can_swipe_chip_card:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->write_only_can_swipe_chip_card:Ljava/lang/Boolean;

    .line 336
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->felica_brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    .line 337
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_masked_card_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->felica_masked_card_number:Ljava/lang/String;

    .line 338
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_sprwid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->felica_sprwid:Ljava/lang/String;

    .line 339
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardTender$Card;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 222
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardTender$Card;->newBuilder()Lcom/squareup/protos/client/bills/CardTender$Card$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 379
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 380
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    if-eqz v1, :cond_0

    const-string v1, ", entry_method="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 381
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    if-eqz v1, :cond_1

    const-string v1, ", brand="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 382
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->pan_suffix:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", pan_suffix=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 383
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->chip_card_fallback_indicator:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    if-eqz v1, :cond_3

    const-string v1, ", chip_card_fallback_indicator="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->chip_card_fallback_indicator:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 384
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->write_only_can_swipe_chip_card:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", write_only_can_swipe_chip_card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->write_only_can_swipe_chip_card:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 385
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    if-eqz v1, :cond_5

    const-string v1, ", felica_brand="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 386
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_masked_card_number:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", felica_masked_card_number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_masked_card_number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 387
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_sprwid:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", felica_sprwid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_sprwid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Card{"

    .line 388
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
