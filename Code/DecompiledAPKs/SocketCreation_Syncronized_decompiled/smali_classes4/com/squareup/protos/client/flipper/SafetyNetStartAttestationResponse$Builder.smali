.class public final Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SafetyNetStartAttestationResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;",
        "Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public api_key:Ljava/lang/String;

.field public nonce:Lokio/ByteString;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 112
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public api_key(Ljava/lang/String;)Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse$Builder;
    .locals 0

    .line 132
    iput-object p1, p0, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse$Builder;->api_key:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;
    .locals 4

    .line 138
    new-instance v0, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse$Builder;->nonce:Lokio/ByteString;

    iget-object v2, p0, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse$Builder;->api_key:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;-><init>(Lokio/ByteString;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 107
    invoke-virtual {p0}, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse$Builder;->build()Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;

    move-result-object v0

    return-object v0
.end method

.method public nonce(Lokio/ByteString;)Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse$Builder;
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse$Builder;->nonce:Lokio/ByteString;

    return-object p0
.end method
