.class public final Lcom/squareup/protos/client/rolodex/ListEventsForContactRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListEventsForContactRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/ListEventsForContactRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/ListEventsForContactRequest;",
        "Lcom/squareup/protos/client/rolodex/ListEventsForContactRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public contact_token:Ljava/lang/String;

.field public list_option:Lcom/squareup/protos/client/rolodex/ListOption;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 102
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/ListEventsForContactRequest;
    .locals 4

    .line 123
    new-instance v0, Lcom/squareup/protos/client/rolodex/ListEventsForContactRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListEventsForContactRequest$Builder;->contact_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/ListEventsForContactRequest$Builder;->list_option:Lcom/squareup/protos/client/rolodex/ListOption;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/rolodex/ListEventsForContactRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/rolodex/ListOption;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 97
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ListEventsForContactRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/ListEventsForContactRequest;

    move-result-object v0

    return-object v0
.end method

.method public contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/ListEventsForContactRequest$Builder;
    .locals 0

    .line 109
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListEventsForContactRequest$Builder;->contact_token:Ljava/lang/String;

    return-object p0
.end method

.method public list_option(Lcom/squareup/protos/client/rolodex/ListOption;)Lcom/squareup/protos/client/rolodex/ListEventsForContactRequest$Builder;
    .locals 0

    .line 117
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListEventsForContactRequest$Builder;->list_option:Lcom/squareup/protos/client/rolodex/ListOption;

    return-object p0
.end method
