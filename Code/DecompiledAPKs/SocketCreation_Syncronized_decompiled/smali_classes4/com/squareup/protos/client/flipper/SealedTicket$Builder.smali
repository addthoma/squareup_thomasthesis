.class public final Lcom/squareup/protos/client/flipper/SealedTicket$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SealedTicket.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/flipper/SealedTicket;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/flipper/SealedTicket;",
        "Lcom/squareup/protos/client/flipper/SealedTicket$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public ciphertext:Lokio/ByteString;

.field public creation:Ljava/lang/Long;

.field public expiration:Ljava/lang/Long;

.field public key_index:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 155
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/flipper/SealedTicket;
    .locals 7

    .line 199
    new-instance v6, Lcom/squareup/protos/client/flipper/SealedTicket;

    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;->expiration:Ljava/lang/Long;

    iget-object v2, p0, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;->ciphertext:Lokio/ByteString;

    iget-object v3, p0, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;->creation:Ljava/lang/Long;

    iget-object v4, p0, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;->key_index:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/flipper/SealedTicket;-><init>(Ljava/lang/Long;Lokio/ByteString;Ljava/lang/Long;Ljava/lang/Long;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 146
    invoke-virtual {p0}, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;->build()Lcom/squareup/protos/client/flipper/SealedTicket;

    move-result-object v0

    return-object v0
.end method

.method public ciphertext(Lokio/ByteString;)Lcom/squareup/protos/client/flipper/SealedTicket$Builder;
    .locals 0

    .line 173
    iput-object p1, p0, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;->ciphertext:Lokio/ByteString;

    return-object p0
.end method

.method public creation(Ljava/lang/Long;)Lcom/squareup/protos/client/flipper/SealedTicket$Builder;
    .locals 0

    .line 184
    iput-object p1, p0, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;->creation:Ljava/lang/Long;

    return-object p0
.end method

.method public expiration(Ljava/lang/Long;)Lcom/squareup/protos/client/flipper/SealedTicket$Builder;
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;->expiration:Ljava/lang/Long;

    return-object p0
.end method

.method public key_index(Ljava/lang/Long;)Lcom/squareup/protos/client/flipper/SealedTicket$Builder;
    .locals 0

    .line 193
    iput-object p1, p0, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;->key_index:Ljava/lang/Long;

    return-object p0
.end method
