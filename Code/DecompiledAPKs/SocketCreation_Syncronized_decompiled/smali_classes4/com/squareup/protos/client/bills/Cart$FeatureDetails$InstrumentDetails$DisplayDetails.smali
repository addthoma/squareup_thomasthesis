.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;
.super Lcom/squareup/wire/Message;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DisplayDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails$ProtoAdapter_DisplayDetails;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field public static final DEFAULT_CARDHOLDER_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_LAST_FOUR:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardTender$Card$Brand#ADAPTER"
        redacted = true
        tag = 0x3
    .end annotation
.end field

.field public final cardholder_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x1
    .end annotation
.end field

.field public final expiry:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardData$KeyedCard$Expiry#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final last_four:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 2426
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails$ProtoAdapter_DisplayDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails$ProtoAdapter_DisplayDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 2434
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->UNKNOWN_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->DEFAULT_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;)V
    .locals 6

    .line 2465
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;Lokio/ByteString;)V
    .locals 1

    .line 2470
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 2471
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->cardholder_name:Ljava/lang/String;

    .line 2472
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->last_four:Ljava/lang/String;

    .line 2473
    iput-object p3, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 2474
    iput-object p4, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->expiry:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 2491
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 2492
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    .line 2493
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->cardholder_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->cardholder_name:Ljava/lang/String;

    .line 2494
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->last_four:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->last_four:Ljava/lang/String;

    .line 2495
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 2496
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->expiry:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->expiry:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    .line 2497
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 2502
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 2504
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 2505
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->cardholder_name:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2506
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->last_four:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2507
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2508
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->expiry:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 2509
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails$Builder;
    .locals 2

    .line 2479
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails$Builder;-><init>()V

    .line 2480
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->cardholder_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails$Builder;->cardholder_name:Ljava/lang/String;

    .line 2481
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->last_four:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails$Builder;->last_four:Ljava/lang/String;

    .line 2482
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails$Builder;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 2483
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->expiry:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails$Builder;->expiry:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    .line 2484
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 2425
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 2516
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2517
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->cardholder_name:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", cardholder_name=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2518
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->last_four:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", last_four=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2519
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    if-eqz v1, :cond_2

    const-string v1, ", brand=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2520
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->expiry:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    if-eqz v1, :cond_3

    const-string v1, ", expiry="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->expiry:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "DisplayDetails{"

    .line 2521
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
