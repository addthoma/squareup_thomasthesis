.class public final Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CreateLoyaltyAccountResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse;",
        "Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public conflicting_contacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation
.end field

.field public errors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;",
            ">;"
        }
    .end annotation
.end field

.field public loyalty_account:Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 138
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 139
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse$Builder;->conflicting_contacts:Ljava/util/List;

    .line 140
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse$Builder;->errors:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse;
    .locals 7

    .line 178
    new-instance v6, Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse$Builder;->conflicting_contacts:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse$Builder;->loyalty_account:Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    iget-object v4, p0, Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse$Builder;->errors:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Lcom/squareup/protos/client/loyalty/LoyaltyAccount;Ljava/util/List;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 129
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse$Builder;->build()Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse;

    move-result-object v0

    return-object v0
.end method

.method public conflicting_contacts(Ljava/util/List;)Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;)",
            "Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse$Builder;"
        }
    .end annotation

    .line 154
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 155
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse$Builder;->conflicting_contacts:Ljava/util/List;

    return-object p0
.end method

.method public errors(Ljava/util/List;)Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;",
            ">;)",
            "Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse$Builder;"
        }
    .end annotation

    .line 171
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 172
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse$Builder;->errors:Ljava/util/List;

    return-object p0
.end method

.method public loyalty_account(Lcom/squareup/protos/client/loyalty/LoyaltyAccount;)Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse$Builder;
    .locals 0

    .line 163
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse$Builder;->loyalty_account:Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse$Builder;
    .locals 0

    .line 144
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
