.class public final Lcom/squareup/protos/client/bills/ReturnCartDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReturnCartDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/ReturnCartDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/ReturnCartDetails;",
        "Lcom/squareup/protos/client/bills/ReturnCartDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cart:Lcom/squareup/protos/client/bills/Cart;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 82
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/ReturnCartDetails;
    .locals 3

    .line 97
    new-instance v0, Lcom/squareup/protos/client/bills/ReturnCartDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ReturnCartDetails$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/bills/ReturnCartDetails;-><init>(Lcom/squareup/protos/client/bills/Cart;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 79
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ReturnCartDetails$Builder;->build()Lcom/squareup/protos/client/bills/ReturnCartDetails;

    move-result-object v0

    return-object v0
.end method

.method public cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/bills/ReturnCartDetails$Builder;
    .locals 0

    .line 91
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ReturnCartDetails$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    return-object p0
.end method
