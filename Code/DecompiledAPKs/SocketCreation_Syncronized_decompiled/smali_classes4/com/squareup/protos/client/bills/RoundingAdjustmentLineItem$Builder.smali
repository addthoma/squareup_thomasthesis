.class public final Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RoundingAdjustmentLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;",
        "Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amounts:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;

.field public calculation_phase:Lcom/squareup/api/items/CalculationPhase;

.field public localized_name:Ljava/lang/String;

.field public rounding_adjustment_line_item_id_pair:Lcom/squareup/protos/client/IdPair;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 134
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public amounts(Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;)Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;
    .locals 0

    .line 143
    iput-object p1, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;
    .locals 7

    .line 168
    new-instance v6, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->rounding_adjustment_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->localized_name:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;-><init>(Lcom/squareup/api/items/CalculationPhase;Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 125
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->build()Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    move-result-object v0

    return-object v0
.end method

.method public calculation_phase(Lcom/squareup/api/items/CalculationPhase;)Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;
    .locals 0

    .line 138
    iput-object p1, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    return-object p0
.end method

.method public localized_name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;
    .locals 0

    .line 162
    iput-object p1, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->localized_name:Ljava/lang/String;

    return-object p0
.end method

.method public rounding_adjustment_line_item_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;
    .locals 0

    .line 152
    iput-object p1, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->rounding_adjustment_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method
