.class public final Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;
.super Lcom/squareup/wire/Message;
.source "ModifierOptionLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/ModifierOptionLineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DisplayDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$ProtoAdapter_DisplayDetails;,
        Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_COGS_OBJECT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_SORT_ORDER:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final cogs_object_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final sort_order:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 403
    new-instance v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$ProtoAdapter_DisplayDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$ProtoAdapter_DisplayDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 409
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->DEFAULT_SORT_ORDER:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 1

    .line 441
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 446
    sget-object v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 447
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->name:Ljava/lang/String;

    .line 448
    iput-object p2, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->sort_order:Ljava/lang/Integer;

    .line 449
    iput-object p3, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 465
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 466
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;

    .line 467
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->name:Ljava/lang/String;

    .line 468
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->sort_order:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->sort_order:Ljava/lang/Integer;

    .line 469
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    .line 470
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 475
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 477
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 478
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->name:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 479
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->sort_order:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 480
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 481
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$Builder;
    .locals 2

    .line 454
    new-instance v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$Builder;-><init>()V

    .line 455
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$Builder;->name:Ljava/lang/String;

    .line 456
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->sort_order:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$Builder;->sort_order:Ljava/lang/Integer;

    .line 457
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$Builder;->cogs_object_id:Ljava/lang/String;

    .line 458
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 402
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->newBuilder()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 488
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 489
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 490
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->sort_order:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", sort_order="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->sort_order:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 491
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", cogs_object_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "DisplayDetails{"

    .line 492
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
