.class public final Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ProvisionReaderResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;",
        "Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public dukpt_pin_iksn_txt:Ljava/lang/String;

.field public dukpt_pin_key_tr31:Ljava/lang/String;

.field public dukpt_sred_iksn_txt:Ljava/lang/String;

.field public dukpt_sred_key_tr31:Ljava/lang/String;

.field public hsm_cert:Ljava/lang/String;

.field public kbpk_bytes:Lokio/ByteString;

.field public kbpk_ciphertext:Ljava/lang/String;

.field public kbpk_sig_bytes:Lokio/ByteString;

.field public kbpk_signature:Ljava/lang/String;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 272
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;
    .locals 13

    .line 379
    new-instance v12, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->hsm_cert:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->kbpk_ciphertext:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->kbpk_signature:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->dukpt_sred_iksn_txt:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->dukpt_sred_key_tr31:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->kbpk_bytes:Lokio/ByteString;

    iget-object v8, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->kbpk_sig_bytes:Lokio/ByteString;

    iget-object v9, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->dukpt_pin_iksn_txt:Ljava/lang/String;

    iget-object v10, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->dukpt_pin_key_tr31:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v11

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;Lokio/ByteString;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v12
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 251
    invoke-virtual {p0}, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->build()Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;

    move-result-object v0

    return-object v0
.end method

.method public dukpt_pin_iksn_txt(Ljava/lang/String;)Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;
    .locals 0

    .line 362
    iput-object p1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->dukpt_pin_iksn_txt:Ljava/lang/String;

    return-object p0
.end method

.method public dukpt_pin_key_tr31(Ljava/lang/String;)Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;
    .locals 0

    .line 373
    iput-object p1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->dukpt_pin_key_tr31:Ljava/lang/String;

    return-object p0
.end method

.method public dukpt_sred_iksn_txt(Ljava/lang/String;)Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;
    .locals 0

    .line 320
    iput-object p1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->dukpt_sred_iksn_txt:Ljava/lang/String;

    return-object p0
.end method

.method public dukpt_sred_key_tr31(Ljava/lang/String;)Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;
    .locals 0

    .line 331
    iput-object p1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->dukpt_sred_key_tr31:Ljava/lang/String;

    return-object p0
.end method

.method public hsm_cert(Ljava/lang/String;)Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;
    .locals 0

    .line 285
    iput-object p1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->hsm_cert:Ljava/lang/String;

    return-object p0
.end method

.method public kbpk_bytes(Lokio/ByteString;)Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;
    .locals 0

    .line 342
    iput-object p1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->kbpk_bytes:Lokio/ByteString;

    return-object p0
.end method

.method public kbpk_ciphertext(Ljava/lang/String;)Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 298
    iput-object p1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->kbpk_ciphertext:Ljava/lang/String;

    return-object p0
.end method

.method public kbpk_sig_bytes(Lokio/ByteString;)Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;
    .locals 0

    .line 352
    iput-object p1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->kbpk_sig_bytes:Lokio/ByteString;

    return-object p0
.end method

.method public kbpk_signature(Ljava/lang/String;)Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 310
    iput-object p1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->kbpk_signature:Ljava/lang/String;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;
    .locals 0

    .line 276
    iput-object p1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
