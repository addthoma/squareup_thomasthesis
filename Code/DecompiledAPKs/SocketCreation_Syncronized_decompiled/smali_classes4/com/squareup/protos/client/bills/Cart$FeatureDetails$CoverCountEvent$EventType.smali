.class public final enum Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;
.super Ljava/lang/Enum;
.source "Cart.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EventType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType$ProtoAdapter_EventType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DO_NOT_USE:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

.field public static final enum MERGE:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

.field public static final enum SPLIT:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

.field public static final enum USER_SELECTED:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

.field public static final enum VOID:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 5348
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;->DO_NOT_USE:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    .line 5353
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    const/4 v2, 0x1

    const-string v3, "USER_SELECTED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;->USER_SELECTED:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    .line 5358
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    const/4 v3, 0x2

    const-string v4, "SPLIT"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;->SPLIT:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    .line 5363
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    const/4 v4, 0x3

    const-string v5, "MERGE"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;->MERGE:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    .line 5368
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    const/4 v5, 0x4

    const-string v6, "VOID"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;->VOID:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    .line 5347
    sget-object v6, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;->DO_NOT_USE:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;->USER_SELECTED:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;->SPLIT:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;->MERGE:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;->VOID:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;->$VALUES:[Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    .line 5370
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType$ProtoAdapter_EventType;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType$ProtoAdapter_EventType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 5374
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 5375
    iput p3, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;
    .locals 1

    if-eqz p0, :cond_4

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 5387
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;->VOID:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    return-object p0

    .line 5386
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;->MERGE:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    return-object p0

    .line 5385
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;->SPLIT:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    return-object p0

    .line 5384
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;->USER_SELECTED:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    return-object p0

    .line 5383
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;->DO_NOT_USE:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;
    .locals 1

    .line 5347
    const-class v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;
    .locals 1

    .line 5347
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;->$VALUES:[Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 5394
    iget v0, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;->value:I

    return v0
.end method
