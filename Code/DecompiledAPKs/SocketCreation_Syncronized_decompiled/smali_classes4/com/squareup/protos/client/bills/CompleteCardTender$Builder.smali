.class public final Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CompleteCardTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CompleteCardTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/CompleteCardTender;",
        "Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public delay_capture:Lcom/squareup/protos/client/bills/CardTender$DelayCapture;

.field public encrypted_reader_data:Lokio/ByteString;

.field public plaintext_reader_data:Lokio/ByteString;

.field public was_customer_present:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 140
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/CompleteCardTender;
    .locals 7

    .line 177
    new-instance v6, Lcom/squareup/protos/client/bills/CompleteCardTender;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;->was_customer_present:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;->delay_capture:Lcom/squareup/protos/client/bills/CardTender$DelayCapture;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;->encrypted_reader_data:Lokio/ByteString;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;->plaintext_reader_data:Lokio/ByteString;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bills/CompleteCardTender;-><init>(Ljava/lang/Boolean;Lcom/squareup/protos/client/bills/CardTender$DelayCapture;Lokio/ByteString;Lokio/ByteString;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 131
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;->build()Lcom/squareup/protos/client/bills/CompleteCardTender;

    move-result-object v0

    return-object v0
.end method

.method public delay_capture(Lcom/squareup/protos/client/bills/CardTender$DelayCapture;)Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;
    .locals 0

    .line 155
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;->delay_capture:Lcom/squareup/protos/client/bills/CardTender$DelayCapture;

    return-object p0
.end method

.method public encrypted_reader_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;
    .locals 0

    .line 163
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;->encrypted_reader_data:Lokio/ByteString;

    return-object p0
.end method

.method public plaintext_reader_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;->plaintext_reader_data:Lokio/ByteString;

    return-object p0
.end method

.method public was_customer_present(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;->was_customer_present:Ljava/lang/Boolean;

    return-object p0
.end method
