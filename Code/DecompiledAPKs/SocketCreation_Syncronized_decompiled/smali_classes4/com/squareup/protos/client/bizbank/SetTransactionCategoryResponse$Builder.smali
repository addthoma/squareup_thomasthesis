.class public final Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SetTransactionCategoryResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse;",
        "Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public is_personal_expense:Ljava/lang/Boolean;

.field public status:Lcom/squareup/protos/client/Status;

.field public transaction_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 121
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse;
    .locals 5

    .line 151
    new-instance v0, Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse$Builder;->transaction_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse$Builder;->is_personal_expense:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/lang/String;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 114
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse;

    move-result-object v0

    return-object v0
.end method

.method public is_personal_expense(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse$Builder;
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse$Builder;->is_personal_expense:Ljava/lang/Boolean;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse$Builder;
    .locals 0

    .line 128
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method

.method public transaction_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse$Builder;
    .locals 0

    .line 136
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse$Builder;->transaction_token:Ljava/lang/String;

    return-object p0
.end method
