.class final Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings$ProtoAdapter_ViewDepositsSettings;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ClientAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ViewDepositsSettings"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 3689
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3704
    new-instance v0, Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings$Builder;-><init>()V

    .line 3705
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 3706
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 3709
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 3713
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 3714
    invoke-virtual {v0}, Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings$Builder;->build()Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3687
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings$ProtoAdapter_ViewDepositsSettings;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3699
    invoke-virtual {p2}, Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3687
    check-cast p2, Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings$ProtoAdapter_ViewDepositsSettings;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;)I
    .locals 0

    .line 3694
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    return p1
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 3687
    check-cast p1, Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings$ProtoAdapter_ViewDepositsSettings;->encodedSize(Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;)Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;
    .locals 0

    .line 3719
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;->newBuilder()Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings$Builder;

    move-result-object p1

    .line 3720
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 3721
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings$Builder;->build()Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 3687
    check-cast p1, Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings$ProtoAdapter_ViewDepositsSettings;->redact(Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;)Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    move-result-object p1

    return-object p1
.end method
