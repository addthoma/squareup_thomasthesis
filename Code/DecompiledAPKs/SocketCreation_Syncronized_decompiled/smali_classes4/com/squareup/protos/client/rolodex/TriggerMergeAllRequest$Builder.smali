.class public final Lcom/squareup/protos/client/rolodex/TriggerMergeAllRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TriggerMergeAllRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/TriggerMergeAllRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/TriggerMergeAllRequest;",
        "Lcom/squareup/protos/client/rolodex/TriggerMergeAllRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public contact_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 85
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/TriggerMergeAllRequest;
    .locals 3

    .line 98
    new-instance v0, Lcom/squareup/protos/client/rolodex/TriggerMergeAllRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/TriggerMergeAllRequest$Builder;->contact_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/rolodex/TriggerMergeAllRequest;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 82
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/TriggerMergeAllRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/TriggerMergeAllRequest;

    move-result-object v0

    return-object v0
.end method

.method public contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/TriggerMergeAllRequest$Builder;
    .locals 0

    .line 92
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/TriggerMergeAllRequest$Builder;->contact_token:Ljava/lang/String;

    return-object p0
.end method
