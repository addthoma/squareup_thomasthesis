.class public final Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AdjustPunchesRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;",
        "Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public adjustment:Ljava/lang/Integer;

.field public balance_before_adjustment:Ljava/lang/Integer;

.field public creator_details:Lcom/squareup/protos/client/CreatorDetails;

.field public current_stars:Ljava/lang/Long;

.field public customer_identifier:Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

.field public decrement_reason:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;

.field public increment_reason:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

.field public loyalty_account_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 216
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public adjustment(Ljava/lang/Integer;)Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;
    .locals 0

    .line 224
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->adjustment:Ljava/lang/Integer;

    return-object p0
.end method

.method public balance_before_adjustment(Ljava/lang/Integer;)Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;
    .locals 0

    .line 253
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->balance_before_adjustment:Ljava/lang/Integer;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;
    .locals 11

    .line 283
    new-instance v10, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->adjustment:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->current_stars:Ljava/lang/Long;

    iget-object v3, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iget-object v4, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->balance_before_adjustment:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->customer_identifier:Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    iget-object v6, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->loyalty_account_token:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->decrement_reason:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;

    iget-object v8, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->increment_reason:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;-><init>(Ljava/lang/Integer;Ljava/lang/Long;Lcom/squareup/protos/client/CreatorDetails;Ljava/lang/Integer;Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;Ljava/lang/String;Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;Lokio/ByteString;)V

    return-object v10
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 199
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;

    move-result-object v0

    return-object v0
.end method

.method public creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;
    .locals 0

    .line 240
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    return-object p0
.end method

.method public current_stars(Ljava/lang/Long;)Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;
    .locals 0

    .line 232
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->current_stars:Ljava/lang/Long;

    return-object p0
.end method

.method public customer_identifier(Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;)Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;
    .locals 0

    .line 258
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->customer_identifier:Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    const/4 p1, 0x0

    .line 259
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->loyalty_account_token:Ljava/lang/String;

    return-object p0
.end method

.method public decrement_reason(Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;)Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;
    .locals 0

    .line 270
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->decrement_reason:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;

    const/4 p1, 0x0

    .line 271
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->increment_reason:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    return-object p0
.end method

.method public increment_reason(Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;)Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;
    .locals 0

    .line 276
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->increment_reason:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    const/4 p1, 0x0

    .line 277
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->decrement_reason:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;

    return-object p0
.end method

.method public loyalty_account_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;
    .locals 0

    .line 264
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->loyalty_account_token:Ljava/lang/String;

    const/4 p1, 0x0

    .line 265
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->customer_identifier:Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    return-object p0
.end method
