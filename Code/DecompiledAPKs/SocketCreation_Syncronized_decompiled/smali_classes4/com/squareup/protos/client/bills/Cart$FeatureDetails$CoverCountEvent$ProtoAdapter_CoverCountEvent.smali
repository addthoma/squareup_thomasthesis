.class final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$ProtoAdapter_CoverCountEvent;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CoverCountEvent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 5411
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5440
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;-><init>()V

    .line 5441
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 5442
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 5459
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 5457
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;

    goto :goto_0

    .line 5456
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;

    goto :goto_0

    .line 5455
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/client/CreatorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/CreatorDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;

    goto :goto_0

    .line 5454
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->is_confirmed(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;

    goto :goto_0

    .line 5453
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->count(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;

    goto :goto_0

    .line 5447
    :pswitch_5
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->event_type(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 5449
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 5444
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->event_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;

    goto :goto_0

    .line 5463
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 5464
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5409
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$ProtoAdapter_CoverCountEvent;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5428
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->event_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5429
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5430
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->count:Ljava/lang/Integer;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5431
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->is_confirmed:Ljava/lang/Boolean;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5432
    sget-object v0, Lcom/squareup/protos/client/CreatorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5433
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5434
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->ordinal:Ljava/lang/Integer;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5435
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5409
    check-cast p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$ProtoAdapter_CoverCountEvent;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;)I
    .locals 4

    .line 5416
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->event_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    const/4 v3, 0x2

    .line 5417
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->count:Ljava/lang/Integer;

    const/4 v3, 0x3

    .line 5418
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->is_confirmed:Ljava/lang/Boolean;

    const/4 v3, 0x4

    .line 5419
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/CreatorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    const/4 v3, 0x5

    .line 5420
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x6

    .line 5421
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->ordinal:Ljava/lang/Integer;

    const/4 v3, 0x7

    .line 5422
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5423
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 5409
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$ProtoAdapter_CoverCountEvent;->encodedSize(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;
    .locals 2

    .line 5469
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;

    move-result-object p1

    .line 5470
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/CreatorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/CreatorDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 5471
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 5472
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 5473
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 5409
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$ProtoAdapter_CoverCountEvent;->redact(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;

    move-result-object p1

    return-object p1
.end method
