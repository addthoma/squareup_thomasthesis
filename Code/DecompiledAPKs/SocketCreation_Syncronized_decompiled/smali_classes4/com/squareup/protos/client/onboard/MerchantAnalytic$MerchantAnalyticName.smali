.class public final enum Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;
.super Ljava/lang/Enum;
.source "MerchantAnalytic.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/onboard/MerchantAnalytic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MerchantAnalyticName"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName$ProtoAdapter_MerchantAnalyticName;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ESTIMATED_EMPLOYEE_COUNT:Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

.field public static final enum ESTIMATED_REVENUE:Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

.field public static final enum ESTIMATED_USAGE:Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

.field public static final enum PAYMENT_LOCATION:Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

.field public static final enum PAYMENT_TYPE:Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 119
    new-instance v0, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;->UNKNOWN:Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    .line 121
    new-instance v0, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    const/4 v2, 0x1

    const-string v3, "ESTIMATED_REVENUE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;->ESTIMATED_REVENUE:Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    .line 123
    new-instance v0, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    const/4 v3, 0x2

    const-string v4, "ESTIMATED_USAGE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;->ESTIMATED_USAGE:Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    .line 125
    new-instance v0, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    const/4 v4, 0x3

    const-string v5, "ESTIMATED_EMPLOYEE_COUNT"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;->ESTIMATED_EMPLOYEE_COUNT:Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    .line 127
    new-instance v0, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    const/4 v5, 0x4

    const-string v6, "PAYMENT_LOCATION"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;->PAYMENT_LOCATION:Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    .line 129
    new-instance v0, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    const/4 v6, 0x5

    const-string v7, "PAYMENT_TYPE"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;->PAYMENT_TYPE:Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    .line 118
    sget-object v7, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;->UNKNOWN:Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;->ESTIMATED_REVENUE:Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;->ESTIMATED_USAGE:Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;->ESTIMATED_EMPLOYEE_COUNT:Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;->PAYMENT_LOCATION:Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;->PAYMENT_TYPE:Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;->$VALUES:[Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    .line 131
    new-instance v0, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName$ProtoAdapter_MerchantAnalyticName;

    invoke-direct {v0}, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName$ProtoAdapter_MerchantAnalyticName;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 135
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 136
    iput p3, p0, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;
    .locals 1

    if-eqz p0, :cond_5

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 149
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;->PAYMENT_TYPE:Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    return-object p0

    .line 148
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;->PAYMENT_LOCATION:Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    return-object p0

    .line 147
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;->ESTIMATED_EMPLOYEE_COUNT:Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    return-object p0

    .line 146
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;->ESTIMATED_USAGE:Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    return-object p0

    .line 145
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;->ESTIMATED_REVENUE:Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    return-object p0

    .line 144
    :cond_5
    sget-object p0, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;->UNKNOWN:Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;
    .locals 1

    .line 118
    const-class v0, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;
    .locals 1

    .line 118
    sget-object v0, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;->$VALUES:[Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 156
    iget v0, p0, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;->value:I

    return v0
.end method
