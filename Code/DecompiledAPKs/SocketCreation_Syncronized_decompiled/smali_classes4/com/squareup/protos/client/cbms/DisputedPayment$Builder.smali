.class public final Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DisputedPayment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/cbms/DisputedPayment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/cbms/DisputedPayment;",
        "Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bank_disputed_comment:Ljava/lang/String;

.field public current_actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

.field public current_amount_held:Lcom/squareup/protos/common/Money;

.field public current_disputed_amount:Lcom/squareup/protos/common/Money;

.field public dispute_reason:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

.field public expected_resolution_date:Lcom/squareup/protos/common/time/YearMonthDay;

.field public information_request:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cbms/InformationRequest;",
            ">;"
        }
    .end annotation
.end field

.field public latest_reporting_date:Lcom/squareup/protos/common/time/YearMonthDay;

.field public payment_amount:Lcom/squareup/protos/common/Money;

.field public payment_card_brand:Lcom/squareup/protos/common/instrument/InstrumentType;

.field public payment_card_pan_suffix:Ljava/lang/String;

.field public payment_created_at:Lcom/squareup/protos/common/time/DateTime;

.field public payment_token:Ljava/lang/String;

.field public protection_state:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

.field public resolution:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

.field public resolution_decided_at:Lcom/squareup/protos/common/time/DateTime;

.field public submitted_to_bank_at:Lcom/squareup/protos/common/time/DateTime;

.field public unit_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 326
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 327
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->information_request:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public bank_disputed_comment(Ljava/lang/String;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;
    .locals 0

    .line 354
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->bank_disputed_comment:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/cbms/DisputedPayment;
    .locals 2

    .line 429
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/protos/client/cbms/DisputedPayment;-><init>(Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 289
    invoke-virtual {p0}, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->build()Lcom/squareup/protos/client/cbms/DisputedPayment;

    move-result-object v0

    return-object v0
.end method

.method public current_actionable_status(Lcom/squareup/protos/client/cbms/ActionableStatus;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;
    .locals 0

    .line 341
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->current_actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

    return-object p0
.end method

.method public current_amount_held(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;
    .locals 0

    .line 362
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->current_amount_held:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public current_disputed_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;
    .locals 0

    .line 367
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->current_disputed_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public dispute_reason(Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;
    .locals 0

    .line 346
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->dispute_reason:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    return-object p0
.end method

.method public expected_resolution_date(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;
    .locals 0

    .line 408
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->expected_resolution_date:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object p0
.end method

.method public information_request(Ljava/util/List;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cbms/InformationRequest;",
            ">;)",
            "Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;"
        }
    .end annotation

    .line 372
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 373
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->information_request:Ljava/util/List;

    return-object p0
.end method

.method public latest_reporting_date(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;
    .locals 0

    .line 403
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->latest_reporting_date:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object p0
.end method

.method public payment_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;
    .locals 0

    .line 413
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->payment_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public payment_card_brand(Lcom/squareup/protos/common/instrument/InstrumentType;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;
    .locals 0

    .line 418
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->payment_card_brand:Lcom/squareup/protos/common/instrument/InstrumentType;

    return-object p0
.end method

.method public payment_card_pan_suffix(Ljava/lang/String;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;
    .locals 0

    .line 423
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->payment_card_pan_suffix:Ljava/lang/String;

    return-object p0
.end method

.method public payment_created_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;
    .locals 0

    .line 398
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->payment_created_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public payment_token(Ljava/lang/String;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;
    .locals 0

    .line 331
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->payment_token:Ljava/lang/String;

    return-object p0
.end method

.method public protection_state(Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;
    .locals 0

    .line 383
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->protection_state:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    return-object p0
.end method

.method public resolution(Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;
    .locals 0

    .line 378
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->resolution:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    return-object p0
.end method

.method public resolution_decided_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;
    .locals 0

    .line 393
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->resolution_decided_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public submitted_to_bank_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;
    .locals 0

    .line 388
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->submitted_to_bank_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public unit_token(Ljava/lang/String;)Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;
    .locals 0

    .line 336
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method
