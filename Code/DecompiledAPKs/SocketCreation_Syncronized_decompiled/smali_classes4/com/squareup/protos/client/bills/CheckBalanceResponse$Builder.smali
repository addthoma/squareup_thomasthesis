.class public final Lcom/squareup/protos/client/bills/CheckBalanceResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CheckBalanceResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CheckBalanceResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/CheckBalanceResponse;",
        "Lcom/squareup/protos/client/bills/CheckBalanceResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public gift_card:Lcom/squareup/protos/client/giftcards/GiftCard;

.field public remaining_balance_money:Lcom/squareup/protos/common/Money;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 115
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/CheckBalanceResponse;
    .locals 5

    .line 143
    new-instance v0, Lcom/squareup/protos/client/bills/CheckBalanceResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CheckBalanceResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/CheckBalanceResponse$Builder;->remaining_balance_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/CheckBalanceResponse$Builder;->gift_card:Lcom/squareup/protos/client/giftcards/GiftCard;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bills/CheckBalanceResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/giftcards/GiftCard;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 108
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CheckBalanceResponse$Builder;->build()Lcom/squareup/protos/client/bills/CheckBalanceResponse;

    move-result-object v0

    return-object v0
.end method

.method public gift_card(Lcom/squareup/protos/client/giftcards/GiftCard;)Lcom/squareup/protos/client/bills/CheckBalanceResponse$Builder;
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CheckBalanceResponse$Builder;->gift_card:Lcom/squareup/protos/client/giftcards/GiftCard;

    return-object p0
.end method

.method public remaining_balance_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/CheckBalanceResponse$Builder;
    .locals 0

    .line 128
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CheckBalanceResponse$Builder;->remaining_balance_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/bills/CheckBalanceResponse$Builder;
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CheckBalanceResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
