.class final Lcom/squareup/protos/client/bills/Tender$Method$ProtoAdapter_Method;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Tender$Method;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Method"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Tender$Method;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1093
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Tender$Method;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Tender$Method;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1126
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;-><init>()V

    .line 1127
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1128
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 1140
    :pswitch_0
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1138
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/client/bills/SquareCapitalTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/SquareCapitalTender;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->square_capital_tender(Lcom/squareup/protos/client/bills/SquareCapitalTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    goto :goto_0

    .line 1137
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/client/bills/ExternalTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/ExternalTender;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->external_tender(Lcom/squareup/protos/client/bills/ExternalTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    goto :goto_0

    .line 1136
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/client/bills/ZeroAmountTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/ZeroAmountTender;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->zero_amount_tender(Lcom/squareup/protos/client/bills/ZeroAmountTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    goto :goto_0

    .line 1135
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/client/bills/GenericTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/GenericTender;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->read_only_generic_tender(Lcom/squareup/protos/client/bills/GenericTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    goto :goto_0

    .line 1134
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/client/bills/WalletTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/WalletTender;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->wallet_tender(Lcom/squareup/protos/client/bills/WalletTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    goto :goto_0

    .line 1133
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/client/bills/OtherTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/OtherTender;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->other_tender(Lcom/squareup/protos/client/bills/OtherTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    goto :goto_0

    .line 1132
    :pswitch_7
    sget-object v3, Lcom/squareup/protos/client/bills/NoSaleTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/NoSaleTender;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->no_sale_tender(Lcom/squareup/protos/client/bills/NoSaleTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    goto :goto_0

    .line 1131
    :pswitch_8
    sget-object v3, Lcom/squareup/protos/client/bills/CashTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/CashTender;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->cash_tender(Lcom/squareup/protos/client/bills/CashTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    goto :goto_0

    .line 1130
    :pswitch_9
    sget-object v3, Lcom/squareup/protos/client/bills/CardTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/CardTender;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->card_tender(Lcom/squareup/protos/client/bills/CardTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    goto :goto_0

    .line 1144
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1145
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->build()Lcom/squareup/protos/client/bills/Tender$Method;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1091
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Tender$Method$ProtoAdapter_Method;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Tender$Method;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Tender$Method;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1112
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1113
    sget-object v0, Lcom/squareup/protos/client/bills/CashTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$Method;->cash_tender:Lcom/squareup/protos/client/bills/CashTender;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1114
    sget-object v0, Lcom/squareup/protos/client/bills/NoSaleTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$Method;->no_sale_tender:Lcom/squareup/protos/client/bills/NoSaleTender;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1115
    sget-object v0, Lcom/squareup/protos/client/bills/OtherTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$Method;->other_tender:Lcom/squareup/protos/client/bills/OtherTender;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1116
    sget-object v0, Lcom/squareup/protos/client/bills/WalletTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$Method;->wallet_tender:Lcom/squareup/protos/client/bills/WalletTender;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1117
    sget-object v0, Lcom/squareup/protos/client/bills/GenericTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$Method;->read_only_generic_tender:Lcom/squareup/protos/client/bills/GenericTender;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1118
    sget-object v0, Lcom/squareup/protos/client/bills/ZeroAmountTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$Method;->zero_amount_tender:Lcom/squareup/protos/client/bills/ZeroAmountTender;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1119
    sget-object v0, Lcom/squareup/protos/client/bills/ExternalTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$Method;->external_tender:Lcom/squareup/protos/client/bills/ExternalTender;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1120
    sget-object v0, Lcom/squareup/protos/client/bills/SquareCapitalTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$Method;->square_capital_tender:Lcom/squareup/protos/client/bills/SquareCapitalTender;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1121
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Tender$Method;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1091
    check-cast p2, Lcom/squareup/protos/client/bills/Tender$Method;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Tender$Method$ProtoAdapter_Method;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Tender$Method;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Tender$Method;)I
    .locals 4

    .line 1098
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/CashTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender$Method;->cash_tender:Lcom/squareup/protos/client/bills/CashTender;

    const/4 v3, 0x3

    .line 1099
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/NoSaleTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender$Method;->no_sale_tender:Lcom/squareup/protos/client/bills/NoSaleTender;

    const/4 v3, 0x4

    .line 1100
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/OtherTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender$Method;->other_tender:Lcom/squareup/protos/client/bills/OtherTender;

    const/4 v3, 0x5

    .line 1101
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/WalletTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender$Method;->wallet_tender:Lcom/squareup/protos/client/bills/WalletTender;

    const/4 v3, 0x6

    .line 1102
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/GenericTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender$Method;->read_only_generic_tender:Lcom/squareup/protos/client/bills/GenericTender;

    const/4 v3, 0x7

    .line 1103
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/ZeroAmountTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender$Method;->zero_amount_tender:Lcom/squareup/protos/client/bills/ZeroAmountTender;

    const/16 v3, 0x8

    .line 1104
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/ExternalTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender$Method;->external_tender:Lcom/squareup/protos/client/bills/ExternalTender;

    const/16 v3, 0xa

    .line 1105
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/SquareCapitalTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender$Method;->square_capital_tender:Lcom/squareup/protos/client/bills/SquareCapitalTender;

    const/16 v3, 0xb

    .line 1106
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1107
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$Method;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1091
    check-cast p1, Lcom/squareup/protos/client/bills/Tender$Method;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Tender$Method$ProtoAdapter_Method;->encodedSize(Lcom/squareup/protos/client/bills/Tender$Method;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Tender$Method;)Lcom/squareup/protos/client/bills/Tender$Method;
    .locals 2

    .line 1150
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$Method;->newBuilder()Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    move-result-object p1

    .line 1151
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/CardTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/CardTender;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    .line 1152
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->cash_tender:Lcom/squareup/protos/client/bills/CashTender;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bills/CashTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->cash_tender:Lcom/squareup/protos/client/bills/CashTender;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/CashTender;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->cash_tender:Lcom/squareup/protos/client/bills/CashTender;

    .line 1153
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->no_sale_tender:Lcom/squareup/protos/client/bills/NoSaleTender;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/bills/NoSaleTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->no_sale_tender:Lcom/squareup/protos/client/bills/NoSaleTender;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/NoSaleTender;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->no_sale_tender:Lcom/squareup/protos/client/bills/NoSaleTender;

    .line 1154
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->other_tender:Lcom/squareup/protos/client/bills/OtherTender;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/client/bills/OtherTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->other_tender:Lcom/squareup/protos/client/bills/OtherTender;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/OtherTender;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->other_tender:Lcom/squareup/protos/client/bills/OtherTender;

    .line 1155
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->wallet_tender:Lcom/squareup/protos/client/bills/WalletTender;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/client/bills/WalletTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->wallet_tender:Lcom/squareup/protos/client/bills/WalletTender;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/WalletTender;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->wallet_tender:Lcom/squareup/protos/client/bills/WalletTender;

    .line 1156
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->read_only_generic_tender:Lcom/squareup/protos/client/bills/GenericTender;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/client/bills/GenericTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->read_only_generic_tender:Lcom/squareup/protos/client/bills/GenericTender;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/GenericTender;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->read_only_generic_tender:Lcom/squareup/protos/client/bills/GenericTender;

    .line 1157
    :cond_5
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->zero_amount_tender:Lcom/squareup/protos/client/bills/ZeroAmountTender;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/protos/client/bills/ZeroAmountTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->zero_amount_tender:Lcom/squareup/protos/client/bills/ZeroAmountTender;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/ZeroAmountTender;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->zero_amount_tender:Lcom/squareup/protos/client/bills/ZeroAmountTender;

    .line 1158
    :cond_6
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->external_tender:Lcom/squareup/protos/client/bills/ExternalTender;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/squareup/protos/client/bills/ExternalTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->external_tender:Lcom/squareup/protos/client/bills/ExternalTender;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/ExternalTender;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->external_tender:Lcom/squareup/protos/client/bills/ExternalTender;

    .line 1159
    :cond_7
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->square_capital_tender:Lcom/squareup/protos/client/bills/SquareCapitalTender;

    if-eqz v0, :cond_8

    sget-object v0, Lcom/squareup/protos/client/bills/SquareCapitalTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->square_capital_tender:Lcom/squareup/protos/client/bills/SquareCapitalTender;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/SquareCapitalTender;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->square_capital_tender:Lcom/squareup/protos/client/bills/SquareCapitalTender;

    .line 1160
    :cond_8
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1161
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->build()Lcom/squareup/protos/client/bills/Tender$Method;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1091
    check-cast p1, Lcom/squareup/protos/client/bills/Tender$Method;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Tender$Method$ProtoAdapter_Method;->redact(Lcom/squareup/protos/client/bills/Tender$Method;)Lcom/squareup/protos/client/bills/Tender$Method;

    move-result-object p1

    return-object p1
.end method
