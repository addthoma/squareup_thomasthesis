.class public final Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "WeeklyDepositSchedule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;",
        "Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public day_of_week_deposit_settings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;",
            ">;"
        }
    .end annotation
.end field

.field public effective_at:Lcom/squareup/protos/common/time/DateTime;

.field public paused_settlement:Ljava/lang/Boolean;

.field public settle_if_optional:Ljava/lang/Boolean;

.field public timezone:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 162
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 163
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;->day_of_week_deposit_settings:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;
    .locals 8

    .line 214
    new-instance v7, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;->effective_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v2, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;->timezone:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;->day_of_week_deposit_settings:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;->settle_if_optional:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;->paused_settlement:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;-><init>(Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 151
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;->build()Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    move-result-object v0

    return-object v0
.end method

.method public day_of_week_deposit_settings(Ljava/util/List;)Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;",
            ">;)",
            "Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;"
        }
    .end annotation

    .line 187
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 188
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;->day_of_week_deposit_settings:Ljava/util/List;

    return-object p0
.end method

.method public effective_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;
    .locals 0

    .line 170
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;->effective_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public paused_settlement(Ljava/lang/Boolean;)Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;
    .locals 0

    .line 208
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;->paused_settlement:Ljava/lang/Boolean;

    return-object p0
.end method

.method public settle_if_optional(Ljava/lang/Boolean;)Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;
    .locals 0

    .line 198
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;->settle_if_optional:Ljava/lang/Boolean;

    return-object p0
.end method

.method public timezone(Ljava/lang/String;)Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;
    .locals 0

    .line 178
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;->timezone:Ljava/lang/String;

    return-object p0
.end method
