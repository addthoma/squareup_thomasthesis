.class public final Lcom/squareup/protos/client/devicesettings/TenderSettings;
.super Lcom/squareup/wire/Message;
.source "TenderSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/devicesettings/TenderSettings$ProtoAdapter_TenderSettings;,
        Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;,
        Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;,
        Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/devicesettings/TenderSettings;",
        "Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final disabled_tender:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.devicesettings.TenderSettings$Tender#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            ">;"
        }
    .end annotation
.end field

.field public final primary_tender:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.devicesettings.TenderSettings$Tender#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            ">;"
        }
    .end annotation
.end field

.field public final secondary_tender:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.devicesettings.TenderSettings$Tender#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 28
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$ProtoAdapter_TenderSettings;

    invoke-direct {v0}, Lcom/squareup/protos/client/devicesettings/TenderSettings$ProtoAdapter_TenderSettings;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/devicesettings/TenderSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            ">;)V"
        }
    .end annotation

    .line 55
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/devicesettings/TenderSettings;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 60
    sget-object v0, Lcom/squareup/protos/client/devicesettings/TenderSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p4, "primary_tender"

    .line 61
    invoke-static {p4, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings;->primary_tender:Ljava/util/List;

    const-string p1, "secondary_tender"

    .line 62
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings;->secondary_tender:Ljava/util/List;

    const-string p1, "disabled_tender"

    .line 63
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings;->disabled_tender:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 79
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 80
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;

    .line 81
    invoke-virtual {p0}, Lcom/squareup/protos/client/devicesettings/TenderSettings;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/devicesettings/TenderSettings;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings;->primary_tender:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->primary_tender:Ljava/util/List;

    .line 82
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings;->secondary_tender:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->secondary_tender:Ljava/util/List;

    .line 83
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings;->disabled_tender:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->disabled_tender:Ljava/util/List;

    .line 84
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 89
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_0

    .line 91
    invoke-virtual {p0}, Lcom/squareup/protos/client/devicesettings/TenderSettings;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 92
    iget-object v1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings;->primary_tender:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 93
    iget-object v1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings;->secondary_tender:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 94
    iget-object v1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings;->disabled_tender:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 95
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_0
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;
    .locals 2

    .line 68
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;-><init>()V

    .line 69
    iget-object v1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings;->primary_tender:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->primary_tender:Ljava/util/List;

    .line 70
    iget-object v1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings;->secondary_tender:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->secondary_tender:Ljava/util/List;

    .line 71
    iget-object v1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings;->disabled_tender:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->disabled_tender:Ljava/util/List;

    .line 72
    invoke-virtual {p0}, Lcom/squareup/protos/client/devicesettings/TenderSettings;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/protos/client/devicesettings/TenderSettings;->newBuilder()Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings;->primary_tender:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", primary_tender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings;->primary_tender:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 104
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings;->secondary_tender:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", secondary_tender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings;->secondary_tender:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 105
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings;->disabled_tender:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", disabled_tender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings;->disabled_tender:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "TenderSettings{"

    .line 106
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
