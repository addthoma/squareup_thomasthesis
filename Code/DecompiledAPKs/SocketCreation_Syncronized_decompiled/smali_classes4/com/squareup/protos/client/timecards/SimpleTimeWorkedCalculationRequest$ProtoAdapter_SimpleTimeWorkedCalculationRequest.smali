.class final Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$ProtoAdapter_SimpleTimeWorkedCalculationRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "SimpleTimeWorkedCalculationRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SimpleTimeWorkedCalculationRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 243
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 269
    new-instance v0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;-><init>()V

    .line 270
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 271
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 279
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 277
    :cond_0
    sget-object v3, Lcom/squareup/protos/common/time/DateTimeInterval;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/DateTimeInterval;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->date_time_interval(Lcom/squareup/protos/common/time/DateTimeInterval;)Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;

    goto :goto_0

    .line 276
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->include_open_timecards(Ljava/lang/Boolean;)Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;

    goto :goto_0

    .line 275
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->next_cursor(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;

    goto :goto_0

    .line 274
    :cond_3
    sget-object v3, Lcom/squareup/protos/client/timecards/MerchantEmployeeRequestFilter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/timecards/MerchantEmployeeRequestFilter;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->request_filter(Lcom/squareup/protos/client/timecards/MerchantEmployeeRequestFilter;)Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;

    goto :goto_0

    .line 273
    :cond_4
    sget-object v3, Lcom/squareup/protos/client/timecards/DateRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/timecards/DateRange;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->date_range(Lcom/squareup/protos/client/timecards/DateRange;)Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;

    goto :goto_0

    .line 283
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 284
    invoke-virtual {v0}, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->build()Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 241
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$ProtoAdapter_SimpleTimeWorkedCalculationRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 259
    sget-object v0, Lcom/squareup/protos/client/timecards/DateRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->date_range:Lcom/squareup/protos/client/timecards/DateRange;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 260
    sget-object v0, Lcom/squareup/protos/common/time/DateTimeInterval;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->date_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 261
    sget-object v0, Lcom/squareup/protos/client/timecards/MerchantEmployeeRequestFilter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->request_filter:Lcom/squareup/protos/client/timecards/MerchantEmployeeRequestFilter;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 262
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->next_cursor:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 263
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->include_open_timecards:Ljava/lang/Boolean;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 264
    invoke-virtual {p2}, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 241
    check-cast p2, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$ProtoAdapter_SimpleTimeWorkedCalculationRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;)I
    .locals 4

    .line 248
    sget-object v0, Lcom/squareup/protos/client/timecards/DateRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->date_range:Lcom/squareup/protos/client/timecards/DateRange;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/common/time/DateTimeInterval;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->date_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

    const/4 v3, 0x5

    .line 249
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/timecards/MerchantEmployeeRequestFilter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->request_filter:Lcom/squareup/protos/client/timecards/MerchantEmployeeRequestFilter;

    const/4 v3, 0x2

    .line 250
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->next_cursor:Ljava/lang/String;

    const/4 v3, 0x3

    .line 251
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->include_open_timecards:Ljava/lang/Boolean;

    const/4 v3, 0x4

    .line 252
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 253
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 241
    check-cast p1, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$ProtoAdapter_SimpleTimeWorkedCalculationRequest;->encodedSize(Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;)Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;
    .locals 2

    .line 289
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->newBuilder()Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;

    move-result-object p1

    .line 290
    iget-object v0, p1, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->date_range:Lcom/squareup/protos/client/timecards/DateRange;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/timecards/DateRange;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->date_range:Lcom/squareup/protos/client/timecards/DateRange;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/timecards/DateRange;

    iput-object v0, p1, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->date_range:Lcom/squareup/protos/client/timecards/DateRange;

    .line 291
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->date_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/common/time/DateTimeInterval;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->date_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/DateTimeInterval;

    iput-object v0, p1, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->date_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 292
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->request_filter:Lcom/squareup/protos/client/timecards/MerchantEmployeeRequestFilter;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/timecards/MerchantEmployeeRequestFilter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->request_filter:Lcom/squareup/protos/client/timecards/MerchantEmployeeRequestFilter;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/timecards/MerchantEmployeeRequestFilter;

    iput-object v0, p1, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->request_filter:Lcom/squareup/protos/client/timecards/MerchantEmployeeRequestFilter;

    .line 293
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 294
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->build()Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 241
    check-cast p1, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$ProtoAdapter_SimpleTimeWorkedCalculationRequest;->redact(Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;)Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;

    move-result-object p1

    return-object p1
.end method
