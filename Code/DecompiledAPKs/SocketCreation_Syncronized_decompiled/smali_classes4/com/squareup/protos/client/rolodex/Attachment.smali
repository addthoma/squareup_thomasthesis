.class public final Lcom/squareup/protos/client/rolodex/Attachment;
.super Lcom/squareup/wire/Message;
.source "Attachment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/Attachment$ProtoAdapter_Attachment;,
        Lcom/squareup/protos/client/rolodex/Attachment$State;,
        Lcom/squareup/protos/client/rolodex/Attachment$FileType;,
        Lcom/squareup/protos/client/rolodex/Attachment$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/rolodex/Attachment;",
        "Lcom/squareup/protos/client/rolodex/Attachment$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/Attachment;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ATTACHMENT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_CONTACT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_CONTENT_TYPE:Ljava/lang/String; = ""

.field public static final DEFAULT_FILE_CONTENT:Lokio/ByteString;

.field public static final DEFAULT_FILE_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_FILE_SIZE_IN_BYTES:Ljava/lang/Long;

.field public static final DEFAULT_FILE_TYPE:Lcom/squareup/protos/client/rolodex/Attachment$FileType;

.field public static final DEFAULT_STATE:Lcom/squareup/protos/client/rolodex/Attachment$State;

.field private static final serialVersionUID:J


# instance fields
.field public final attachment_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final contact_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final content_type:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final created_at:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final file_content:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x8
    .end annotation
.end field

.field public final file_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final file_size_in_bytes:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0x7
    .end annotation
.end field

.field public final file_type:Lcom/squareup/protos/client/rolodex/Attachment$FileType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Attachment$FileType#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final state:Lcom/squareup/protos/client/rolodex/Attachment$State;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Attachment$State#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final updated_at:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0xa
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 27
    new-instance v0, Lcom/squareup/protos/client/rolodex/Attachment$ProtoAdapter_Attachment;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/Attachment$ProtoAdapter_Attachment;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Attachment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 37
    sget-object v0, Lcom/squareup/protos/client/rolodex/Attachment$FileType;->UNKNOWN_FILE_TYPE:Lcom/squareup/protos/client/rolodex/Attachment$FileType;

    sput-object v0, Lcom/squareup/protos/client/rolodex/Attachment;->DEFAULT_FILE_TYPE:Lcom/squareup/protos/client/rolodex/Attachment$FileType;

    .line 39
    sget-object v0, Lcom/squareup/protos/client/rolodex/Attachment$State;->PENDING:Lcom/squareup/protos/client/rolodex/Attachment$State;

    sput-object v0, Lcom/squareup/protos/client/rolodex/Attachment;->DEFAULT_STATE:Lcom/squareup/protos/client/rolodex/Attachment$State;

    const-wide/16 v0, 0x0

    .line 43
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/rolodex/Attachment;->DEFAULT_FILE_SIZE_IN_BYTES:Ljava/lang/Long;

    .line 45
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/client/rolodex/Attachment;->DEFAULT_FILE_CONTENT:Lokio/ByteString;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/Attachment$FileType;Lcom/squareup/protos/client/rolodex/Attachment$State;Ljava/lang/String;Ljava/lang/Long;Lokio/ByteString;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;)V
    .locals 12

    .line 130
    sget-object v11, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/protos/client/rolodex/Attachment;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/Attachment$FileType;Lcom/squareup/protos/client/rolodex/Attachment$State;Ljava/lang/String;Ljava/lang/Long;Lokio/ByteString;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/Attachment$FileType;Lcom/squareup/protos/client/rolodex/Attachment$State;Ljava/lang/String;Ljava/lang/Long;Lokio/ByteString;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V
    .locals 1

    .line 136
    sget-object v0, Lcom/squareup/protos/client/rolodex/Attachment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p11}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 137
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->attachment_token:Ljava/lang/String;

    .line 138
    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Attachment;->contact_token:Ljava/lang/String;

    .line 139
    iput-object p3, p0, Lcom/squareup/protos/client/rolodex/Attachment;->file_name:Ljava/lang/String;

    .line 140
    iput-object p4, p0, Lcom/squareup/protos/client/rolodex/Attachment;->file_type:Lcom/squareup/protos/client/rolodex/Attachment$FileType;

    .line 141
    iput-object p5, p0, Lcom/squareup/protos/client/rolodex/Attachment;->state:Lcom/squareup/protos/client/rolodex/Attachment$State;

    .line 142
    iput-object p6, p0, Lcom/squareup/protos/client/rolodex/Attachment;->content_type:Ljava/lang/String;

    .line 143
    iput-object p7, p0, Lcom/squareup/protos/client/rolodex/Attachment;->file_size_in_bytes:Ljava/lang/Long;

    .line 144
    iput-object p8, p0, Lcom/squareup/protos/client/rolodex/Attachment;->file_content:Lokio/ByteString;

    .line 145
    iput-object p9, p0, Lcom/squareup/protos/client/rolodex/Attachment;->created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 146
    iput-object p10, p0, Lcom/squareup/protos/client/rolodex/Attachment;->updated_at:Lcom/squareup/protos/common/time/DateTime;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 169
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/rolodex/Attachment;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 170
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/rolodex/Attachment;

    .line 171
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Attachment;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Attachment;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->attachment_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Attachment;->attachment_token:Ljava/lang/String;

    .line 172
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->contact_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Attachment;->contact_token:Ljava/lang/String;

    .line 173
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->file_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Attachment;->file_name:Ljava/lang/String;

    .line 174
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->file_type:Lcom/squareup/protos/client/rolodex/Attachment$FileType;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Attachment;->file_type:Lcom/squareup/protos/client/rolodex/Attachment$FileType;

    .line 175
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->state:Lcom/squareup/protos/client/rolodex/Attachment$State;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Attachment;->state:Lcom/squareup/protos/client/rolodex/Attachment$State;

    .line 176
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->content_type:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Attachment;->content_type:Ljava/lang/String;

    .line 177
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->file_size_in_bytes:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Attachment;->file_size_in_bytes:Ljava/lang/Long;

    .line 178
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->file_content:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Attachment;->file_content:Lokio/ByteString;

    .line 179
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->created_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Attachment;->created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 180
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->updated_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Attachment;->updated_at:Lcom/squareup/protos/common/time/DateTime;

    .line 181
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 186
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_a

    .line 188
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Attachment;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 189
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->attachment_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 190
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->contact_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 191
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->file_name:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 192
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->file_type:Lcom/squareup/protos/client/rolodex/Attachment$FileType;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Attachment$FileType;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 193
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->state:Lcom/squareup/protos/client/rolodex/Attachment$State;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Attachment$State;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 194
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->content_type:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 195
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->file_size_in_bytes:Ljava/lang/Long;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 196
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->file_content:Lokio/ByteString;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 197
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->created_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 198
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->updated_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v2

    :cond_9
    add-int/2addr v0, v2

    .line 199
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_a
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/rolodex/Attachment$Builder;
    .locals 2

    .line 151
    new-instance v0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/Attachment$Builder;-><init>()V

    .line 152
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->attachment_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->attachment_token:Ljava/lang/String;

    .line 153
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->contact_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->contact_token:Ljava/lang/String;

    .line 154
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->file_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->file_name:Ljava/lang/String;

    .line 155
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->file_type:Lcom/squareup/protos/client/rolodex/Attachment$FileType;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->file_type:Lcom/squareup/protos/client/rolodex/Attachment$FileType;

    .line 156
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->state:Lcom/squareup/protos/client/rolodex/Attachment$State;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->state:Lcom/squareup/protos/client/rolodex/Attachment$State;

    .line 157
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->content_type:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->content_type:Ljava/lang/String;

    .line 158
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->file_size_in_bytes:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->file_size_in_bytes:Ljava/lang/Long;

    .line 159
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->file_content:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->file_content:Lokio/ByteString;

    .line 160
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->created_at:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 161
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->updated_at:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->updated_at:Lcom/squareup/protos/common/time/DateTime;

    .line 162
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Attachment;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Attachment;->newBuilder()Lcom/squareup/protos/client/rolodex/Attachment$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 206
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 207
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->attachment_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", attachment_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->attachment_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 208
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->contact_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", contact_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->contact_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->file_name:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", file_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->file_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->file_type:Lcom/squareup/protos/client/rolodex/Attachment$FileType;

    if-eqz v1, :cond_3

    const-string v1, ", file_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->file_type:Lcom/squareup/protos/client/rolodex/Attachment$FileType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 211
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->state:Lcom/squareup/protos/client/rolodex/Attachment$State;

    if-eqz v1, :cond_4

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->state:Lcom/squareup/protos/client/rolodex/Attachment$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 212
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->content_type:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", content_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->content_type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->file_size_in_bytes:Ljava/lang/Long;

    if-eqz v1, :cond_6

    const-string v1, ", file_size_in_bytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->file_size_in_bytes:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 214
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->file_content:Lokio/ByteString;

    if-eqz v1, :cond_7

    const-string v1, ", file_content="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->file_content:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 215
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->created_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_8

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->created_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 216
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->updated_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_9

    const-string v1, ", updated_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment;->updated_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_9
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Attachment{"

    .line 217
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
