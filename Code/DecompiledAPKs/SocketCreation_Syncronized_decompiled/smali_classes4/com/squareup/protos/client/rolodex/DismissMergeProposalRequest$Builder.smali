.class public final Lcom/squareup/protos/client/rolodex/DismissMergeProposalRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DismissMergeProposalRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/DismissMergeProposalRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/DismissMergeProposalRequest;",
        "Lcom/squareup/protos/client/rolodex/DismissMergeProposalRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public dismissed:Ljava/lang/Boolean;

.field public duplicate_contact_token_set:Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 104
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/DismissMergeProposalRequest;
    .locals 4

    .line 126
    new-instance v0, Lcom/squareup/protos/client/rolodex/DismissMergeProposalRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/DismissMergeProposalRequest$Builder;->duplicate_contact_token_set:Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/DismissMergeProposalRequest$Builder;->dismissed:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/rolodex/DismissMergeProposalRequest;-><init>(Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 99
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/DismissMergeProposalRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/DismissMergeProposalRequest;

    move-result-object v0

    return-object v0
.end method

.method public dismissed(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/DismissMergeProposalRequest$Builder;
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/DismissMergeProposalRequest$Builder;->dismissed:Ljava/lang/Boolean;

    return-object p0
.end method

.method public duplicate_contact_token_set(Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;)Lcom/squareup/protos/client/rolodex/DismissMergeProposalRequest$Builder;
    .locals 0

    .line 112
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/DismissMergeProposalRequest$Builder;->duplicate_contact_token_set:Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;

    return-object p0
.end method
