.class public final Lcom/squareup/protos/client/bills/CompleteTender;
.super Lcom/squareup/wire/Message;
.source "CompleteTender.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/CompleteTender$ProtoAdapter_CompleteTender;,
        Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;,
        Lcom/squareup/protos/client/bills/CompleteTender$Amounts;,
        Lcom/squareup/protos/client/bills/CompleteTender$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/CompleteTender;",
        "Lcom/squareup/protos/client/bills/CompleteTender$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/CompleteTender;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final amounts:Lcom/squareup/protos/client/bills/CompleteTender$Amounts;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CompleteTender$Amounts#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final complete_details:Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CompleteTender$CompleteDetails#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Tender$CompleteTenderDetails#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final tender_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/client/bills/CompleteTender$ProtoAdapter_CompleteTender;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CompleteTender$ProtoAdapter_CompleteTender;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/CompleteTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/CompleteTender$Amounts;Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;)V
    .locals 6

    .line 59
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bills/CompleteTender;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/CompleteTender$Amounts;Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/CompleteTender$Amounts;Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;Lokio/ByteString;)V
    .locals 1

    .line 64
    sget-object v0, Lcom/squareup/protos/client/bills/CompleteTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 65
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CompleteTender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 66
    iput-object p2, p0, Lcom/squareup/protos/client/bills/CompleteTender;->amounts:Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    .line 67
    iput-object p3, p0, Lcom/squareup/protos/client/bills/CompleteTender;->complete_details:Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;

    .line 68
    iput-object p4, p0, Lcom/squareup/protos/client/bills/CompleteTender;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 85
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/CompleteTender;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 86
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/CompleteTender;

    .line 87
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CompleteTender;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CompleteTender;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CompleteTender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 88
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender;->amounts:Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CompleteTender;->amounts:Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    .line 89
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender;->complete_details:Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CompleteTender;->complete_details:Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;

    .line 90
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/CompleteTender;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    .line 91
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 96
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 98
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CompleteTender;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender;->amounts:Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender;->complete_details:Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 103
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/CompleteTender$Builder;
    .locals 2

    .line 73
    new-instance v0, Lcom/squareup/protos/client/bills/CompleteTender$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;-><init>()V

    .line 74
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 75
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender;->amounts:Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->amounts:Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    .line 76
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender;->complete_details:Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->complete_details:Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;

    .line 77
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    .line 78
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CompleteTender;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CompleteTender;->newBuilder()Lcom/squareup/protos/client/bills/CompleteTender$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", tender_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 112
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender;->amounts:Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    if-eqz v1, :cond_1

    const-string v1, ", amounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender;->amounts:Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 113
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender;->complete_details:Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;

    if-eqz v1, :cond_2

    const-string v1, ", complete_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender;->complete_details:Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 114
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    if-eqz v1, :cond_3

    const-string v1, ", extra_tender_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CompleteTender{"

    .line 115
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
