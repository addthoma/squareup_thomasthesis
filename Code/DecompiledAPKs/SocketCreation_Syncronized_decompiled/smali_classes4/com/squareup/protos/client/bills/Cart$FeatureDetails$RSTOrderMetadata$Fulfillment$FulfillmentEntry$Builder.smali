.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public fulfillment_entry_id:Ljava/lang/String;

.field public itemization_id_pair:Lcom/squareup/protos/client/IdPair;

.field public quantity:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 5939
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;
    .locals 5

    .line 5966
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;->fulfillment_entry_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;->quantity:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 5932
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;

    move-result-object v0

    return-object v0
.end method

.method public fulfillment_entry_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;
    .locals 0

    .line 5943
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;->fulfillment_entry_id:Ljava/lang/String;

    return-object p0
.end method

.method public itemization_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;
    .locals 0

    .line 5951
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public quantity(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;
    .locals 0

    .line 5960
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;->quantity:Ljava/lang/String;

    return-object p0
.end method
