.class final Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$ProtoAdapter_RecordMissedLoyaltyOpportunityRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "RecordMissedLoyaltyOpportunityRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_RecordMissedLoyaltyOpportunityRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 526
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 550
    new-instance v0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;-><init>()V

    .line 551
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 552
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 559
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 557
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;->receipt_email_address(Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;)Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;

    goto :goto_0

    .line 556
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;->bill_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;

    goto :goto_0

    .line 555
    :cond_2
    sget-object v3, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;->missed_loyalty_opportunity(Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;)Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;

    goto :goto_0

    .line 554
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;->server_payment_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;

    goto :goto_0

    .line 563
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 564
    invoke-virtual {v0}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 524
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$ProtoAdapter_RecordMissedLoyaltyOpportunityRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 541
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->server_payment_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 542
    sget-object v0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->missed_loyalty_opportunity:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 543
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->bill_server_token:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 544
    sget-object v0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->receipt_email_address:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 545
    invoke-virtual {p2}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 524
    check-cast p2, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$ProtoAdapter_RecordMissedLoyaltyOpportunityRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;)I
    .locals 4

    .line 531
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->server_payment_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->missed_loyalty_opportunity:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

    const/4 v3, 0x2

    .line 532
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->bill_server_token:Ljava/lang/String;

    const/4 v3, 0x3

    .line 533
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->receipt_email_address:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;

    const/4 v3, 0x4

    .line 534
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 535
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 524
    check-cast p1, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$ProtoAdapter_RecordMissedLoyaltyOpportunityRequest;->encodedSize(Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;)Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;
    .locals 2

    .line 570
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->newBuilder()Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;

    move-result-object p1

    .line 571
    iget-object v0, p1, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;->missed_loyalty_opportunity:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;->missed_loyalty_opportunity:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

    iput-object v0, p1, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;->missed_loyalty_opportunity:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

    .line 572
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;->receipt_email_address:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;->receipt_email_address:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;

    iput-object v0, p1, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;->receipt_email_address:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;

    .line 573
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 574
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 524
    check-cast p1, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$ProtoAdapter_RecordMissedLoyaltyOpportunityRequest;->redact(Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;)Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;

    move-result-object p1

    return-object p1
.end method
