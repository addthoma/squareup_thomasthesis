.class final Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$ProtoAdapter_CreateCardCustomizationRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CreateCardCustomizationRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CreateCardCustomizationRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 138
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 160
    new-instance v0, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;-><init>()V

    .line 161
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 162
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 168
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 166
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;->mime_type(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;

    goto :goto_0

    .line 165
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;->image_bytes(Lokio/ByteString;)Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;

    goto :goto_0

    .line 164
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;->idempotence_key(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;

    goto :goto_0

    .line 172
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 173
    invoke-virtual {v0}, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 136
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$ProtoAdapter_CreateCardCustomizationRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 152
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;->idempotence_key:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 153
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;->image_bytes:Lokio/ByteString;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 154
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;->mime_type:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 155
    invoke-virtual {p2}, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 136
    check-cast p2, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$ProtoAdapter_CreateCardCustomizationRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;)I
    .locals 4

    .line 143
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;->idempotence_key:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;->image_bytes:Lokio/ByteString;

    const/4 v3, 0x2

    .line 144
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;->mime_type:Ljava/lang/String;

    const/4 v3, 0x3

    .line 145
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 146
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 136
    check-cast p1, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$ProtoAdapter_CreateCardCustomizationRequest;->encodedSize(Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;)Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;
    .locals 1

    .line 178
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;->newBuilder()Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 179
    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;->image_bytes:Lokio/ByteString;

    .line 180
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 181
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 136
    check-cast p1, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$ProtoAdapter_CreateCardCustomizationRequest;->redact(Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;)Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;

    move-result-object p1

    return-object p1
.end method
