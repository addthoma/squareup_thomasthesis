.class public final Lcom/squareup/protos/client/invoice/InvoiceRefund;
.super Lcom/squareup/wire/Message;
.source "InvoiceRefund.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/invoice/InvoiceRefund$ProtoAdapter_InvoiceRefund;,
        Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/invoice/InvoiceRefund;",
        "Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/invoice/InvoiceRefund;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BRAND:Ljava/lang/String; = ""

.field public static final DEFAULT_LAST_FOUR:Ljava/lang/String; = ""

.field public static final DEFAULT_REASON:Ljava/lang/String; = ""

.field public static final DEFAULT_TENDER_TYPE:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

.field private static final serialVersionUID:J


# instance fields
.field public final brand:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final last_four:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x6
    .end annotation
.end field

.field public final reason:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final refunded_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final refunded_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.InvoiceTenderDetails$TenderType#ADAPTER"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceRefund$ProtoAdapter_InvoiceRefund;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/InvoiceRefund$ProtoAdapter_InvoiceRefund;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 28
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->CASH:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->DEFAULT_TENDER_TYPE:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .line 87
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/invoice/InvoiceRefund;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 93
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 94
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->refunded_money:Lcom/squareup/protos/common/Money;

    .line 95
    iput-object p2, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 96
    iput-object p3, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->reason:Ljava/lang/String;

    .line 97
    iput-object p4, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    .line 98
    iput-object p5, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->brand:Ljava/lang/String;

    .line 99
    iput-object p6, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->last_four:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 118
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/invoice/InvoiceRefund;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 119
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/invoice/InvoiceRefund;

    .line 120
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceRefund;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/InvoiceRefund;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->refunded_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceRefund;->refunded_money:Lcom/squareup/protos/common/Money;

    .line 121
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceRefund;->refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 122
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->reason:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceRefund;->reason:Ljava/lang/String;

    .line 123
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceRefund;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    .line 124
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->brand:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceRefund;->brand:Ljava/lang/String;

    .line 125
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->last_four:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceRefund;->last_four:Ljava/lang/String;

    .line 126
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 131
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 133
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceRefund;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 134
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->refunded_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 135
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 136
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->reason:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 137
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 138
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->brand:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 139
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->last_four:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 140
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;
    .locals 2

    .line 104
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;-><init>()V

    .line 105
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->refunded_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;->refunded_money:Lcom/squareup/protos/common/Money;

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;->refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->reason:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;->reason:Ljava/lang/String;

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->brand:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;->brand:Ljava/lang/String;

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->last_four:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;->last_four:Ljava/lang/String;

    .line 111
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceRefund;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceRefund;->newBuilder()Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 147
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 148
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->refunded_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_0

    const-string v1, ", refunded_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->refunded_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 149
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_1

    const-string v1, ", refunded_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 150
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->reason:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->reason:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    if-eqz v1, :cond_3

    const-string v1, ", tender_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 152
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->brand:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", brand="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->brand:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->last_four:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", last_four=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "InvoiceRefund{"

    .line 154
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
