.class public final Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BuyerSummary.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/BuyerSummary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/BuyerSummary;",
        "Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public average_transaction_frequency_millis:Ljava/lang/Long;

.field public first_visit:Lcom/squareup/protos/common/time/DateTime;

.field public last_visit:Lcom/squareup/protos/common/time/DateTime;

.field public total_spent:Lcom/squareup/protos/common/Money;

.field public transaction_count:Ljava/lang/Long;

.field public transaction_frequency_description:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 167
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public average_transaction_frequency_millis(Ljava/lang/Long;)Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;
    .locals 0

    .line 189
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;->average_transaction_frequency_millis:Ljava/lang/Long;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/rolodex/BuyerSummary;
    .locals 9

    .line 211
    new-instance v8, Lcom/squareup/protos/client/rolodex/BuyerSummary;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;->transaction_count:Ljava/lang/Long;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;->first_visit:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;->last_visit:Lcom/squareup/protos/common/time/DateTime;

    iget-object v4, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;->average_transaction_frequency_millis:Ljava/lang/Long;

    iget-object v5, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;->total_spent:Lcom/squareup/protos/common/Money;

    iget-object v6, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;->transaction_frequency_description:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/rolodex/BuyerSummary;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/Long;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 154
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;->build()Lcom/squareup/protos/client/rolodex/BuyerSummary;

    move-result-object v0

    return-object v0
.end method

.method public first_visit(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;
    .locals 0

    .line 176
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;->first_visit:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public last_visit(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;
    .locals 0

    .line 181
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;->last_visit:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public total_spent(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;
    .locals 0

    .line 197
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;->total_spent:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public transaction_count(Ljava/lang/Long;)Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;->transaction_count:Ljava/lang/Long;

    return-object p0
.end method

.method public transaction_frequency_description(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;
    .locals 0

    .line 205
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;->transaction_frequency_description:Ljava/lang/String;

    return-object p0
.end method
