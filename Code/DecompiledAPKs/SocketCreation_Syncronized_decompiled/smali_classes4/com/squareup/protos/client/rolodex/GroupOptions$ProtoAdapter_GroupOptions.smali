.class final Lcom/squareup/protos/client/rolodex/GroupOptions$ProtoAdapter_GroupOptions;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GroupOptions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/GroupOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GroupOptions"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/rolodex/GroupOptions;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 105
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/rolodex/GroupOptions;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/rolodex/GroupOptions;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 122
    new-instance v0, Lcom/squareup/protos/client/rolodex/GroupOptions$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/GroupOptions$Builder;-><init>()V

    .line 123
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 124
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 128
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 126
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/GroupOptions$Builder;->include_counts(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/GroupOptions$Builder;

    goto :goto_0

    .line 132
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/GroupOptions$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 133
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/GroupOptions$Builder;->build()Lcom/squareup/protos/client/rolodex/GroupOptions;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 103
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/GroupOptions$ProtoAdapter_GroupOptions;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/rolodex/GroupOptions;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/rolodex/GroupOptions;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 116
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/GroupOptions;->include_counts:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 117
    invoke-virtual {p2}, Lcom/squareup/protos/client/rolodex/GroupOptions;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 103
    check-cast p2, Lcom/squareup/protos/client/rolodex/GroupOptions;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/rolodex/GroupOptions$ProtoAdapter_GroupOptions;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/rolodex/GroupOptions;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/rolodex/GroupOptions;)I
    .locals 3

    .line 110
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/GroupOptions;->include_counts:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 111
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/GroupOptions;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 103
    check-cast p1, Lcom/squareup/protos/client/rolodex/GroupOptions;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/GroupOptions$ProtoAdapter_GroupOptions;->encodedSize(Lcom/squareup/protos/client/rolodex/GroupOptions;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/rolodex/GroupOptions;)Lcom/squareup/protos/client/rolodex/GroupOptions;
    .locals 0

    .line 138
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/GroupOptions;->newBuilder()Lcom/squareup/protos/client/rolodex/GroupOptions$Builder;

    move-result-object p1

    .line 139
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/GroupOptions$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 140
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/GroupOptions$Builder;->build()Lcom/squareup/protos/client/rolodex/GroupOptions;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 103
    check-cast p1, Lcom/squareup/protos/client/rolodex/GroupOptions;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/GroupOptions$ProtoAdapter_GroupOptions;->redact(Lcom/squareup/protos/client/rolodex/GroupOptions;)Lcom/squareup/protos/client/rolodex/GroupOptions;

    move-result-object p1

    return-object p1
.end method
