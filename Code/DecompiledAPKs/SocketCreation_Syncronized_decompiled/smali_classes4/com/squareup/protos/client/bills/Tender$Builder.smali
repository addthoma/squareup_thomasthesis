.class public final Lcom/squareup/protos/client/bills/Tender$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Tender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Tender;",
        "Lcom/squareup/protos/client/bills/Tender$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

.field public checkout_app_id:Ljava/lang/String;

.field public checkout_id:Ljava/lang/String;

.field public client_details:Lcom/squareup/protos/client/bills/ClientDetails;

.field public create_payment_note:Ljava/lang/String;

.field public creator_details:Lcom/squareup/protos/client/CreatorDetails;

.field public extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

.field public method:Lcom/squareup/protos/client/bills/Tender$Method;

.field public open_ticket_owner:Lcom/squareup/protos/client/CreatorDetails;

.field public peripheral_metadata:Lcom/squareup/protos/common/payment/PeripheralMetadata;

.field public read_only_buyer_profile:Lcom/squareup/protos/client/Buyer;

.field public read_only_contact_token:Ljava/lang/String;

.field public read_only_last_refundable_at:Lcom/squareup/protos/client/ISO8601Date;

.field public read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

.field public read_only_receipt_number:Ljava/lang/String;

.field public read_only_refund_card_presence_requirement:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

.field public read_only_sequential_tender_number:Ljava/lang/String;

.field public read_only_state:Lcom/squareup/protos/client/bills/Tender$State;

.field public reference_id:Ljava/lang/String;

.field public tender_id_pair:Lcom/squareup/protos/client/IdPair;

.field public tender_tag:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/TenderTag;",
            ">;"
        }
    .end annotation
.end field

.field public tendered_at:Lcom/squareup/protos/client/ISO8601Date;

.field public type:Lcom/squareup/protos/client/bills/Tender$Type;

.field public write_only_client_cash_drawer_shift_id:Ljava/lang/String;

.field public write_only_location:Lcom/squareup/protos/client/GeoLocation;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 500
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 501
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Tender$Builder;->tender_tag:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public amounts(Lcom/squareup/protos/client/bills/Tender$Amounts;)Lcom/squareup/protos/client/bills/Tender$Builder;
    .locals 0

    .line 543
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Builder;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/Tender;
    .locals 2

    .line 711
    new-instance v0, Lcom/squareup/protos/client/bills/Tender;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/protos/client/bills/Tender;-><init>(Lcom/squareup/protos/client/bills/Tender$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 449
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Tender$Builder;->build()Lcom/squareup/protos/client/bills/Tender;

    move-result-object v0

    return-object v0
.end method

.method public checkout_app_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$Builder;
    .locals 0

    .line 695
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Builder;->checkout_app_id:Ljava/lang/String;

    return-object p0
.end method

.method public checkout_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$Builder;
    .locals 0

    .line 671
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Builder;->checkout_id:Ljava/lang/String;

    return-object p0
.end method

.method public client_details(Lcom/squareup/protos/client/bills/ClientDetails;)Lcom/squareup/protos/client/bills/Tender$Builder;
    .locals 0

    .line 647
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Builder;->client_details:Lcom/squareup/protos/client/bills/ClientDetails;

    return-object p0
.end method

.method public create_payment_note(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$Builder;
    .locals 0

    .line 687
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Builder;->create_payment_note:Ljava/lang/String;

    return-object p0
.end method

.method public creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/bills/Tender$Builder;
    .locals 0

    .line 587
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    return-object p0
.end method

.method public extra_tender_details(Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;)Lcom/squareup/protos/client/bills/Tender$Builder;
    .locals 0

    .line 601
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Builder;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    return-object p0
.end method

.method public method(Lcom/squareup/protos/client/bills/Tender$Method;)Lcom/squareup/protos/client/bills/Tender$Builder;
    .locals 0

    .line 526
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Builder;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    return-object p0
.end method

.method public open_ticket_owner(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/bills/Tender$Builder;
    .locals 0

    .line 655
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Builder;->open_ticket_owner:Lcom/squareup/protos/client/CreatorDetails;

    return-object p0
.end method

.method public peripheral_metadata(Lcom/squareup/protos/common/payment/PeripheralMetadata;)Lcom/squareup/protos/client/bills/Tender$Builder;
    .locals 0

    .line 663
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Builder;->peripheral_metadata:Lcom/squareup/protos/common/payment/PeripheralMetadata;

    return-object p0
.end method

.method public read_only_buyer_profile(Lcom/squareup/protos/client/Buyer;)Lcom/squareup/protos/client/bills/Tender$Builder;
    .locals 0

    .line 562
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_buyer_profile:Lcom/squareup/protos/client/Buyer;

    return-object p0
.end method

.method public read_only_contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$Builder;
    .locals 0

    .line 705
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_contact_token:Ljava/lang/String;

    return-object p0
.end method

.method public read_only_last_refundable_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Tender$Builder;
    .locals 0

    .line 579
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_last_refundable_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public read_only_loyalty_details(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;)Lcom/squareup/protos/client/bills/Tender$Builder;
    .locals 0

    .line 642
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    return-object p0
.end method

.method public read_only_receipt_number(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$Builder;
    .locals 0

    .line 570
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_receipt_number:Ljava/lang/String;

    return-object p0
.end method

.method public read_only_refund_card_presence_requirement(Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;)Lcom/squareup/protos/client/bills/Tender$Builder;
    .locals 0

    .line 624
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_refund_card_presence_requirement:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    return-object p0
.end method

.method public read_only_sequential_tender_number(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$Builder;
    .locals 0

    .line 618
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_sequential_tender_number:Ljava/lang/String;

    return-object p0
.end method

.method public read_only_state(Lcom/squareup/protos/client/bills/Tender$State;)Lcom/squareup/protos/client/bills/Tender$Builder;
    .locals 0

    .line 521
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_state:Lcom/squareup/protos/client/bills/Tender$State;

    return-object p0
.end method

.method public reference_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$Builder;
    .locals 0

    .line 679
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Builder;->reference_id:Ljava/lang/String;

    return-object p0
.end method

.method public tender_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Tender$Builder;
    .locals 0

    .line 508
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public tender_tag(Ljava/util/List;)Lcom/squareup/protos/client/bills/Tender$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/TenderTag;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Tender$Builder;"
        }
    .end annotation

    .line 632
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 633
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Builder;->tender_tag:Ljava/util/List;

    return-object p0
.end method

.method public tendered_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Tender$Builder;
    .locals 0

    .line 538
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Builder;->tendered_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/client/bills/Tender$Type;)Lcom/squareup/protos/client/bills/Tender$Builder;
    .locals 0

    .line 513
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Builder;->type:Lcom/squareup/protos/client/bills/Tender$Type;

    return-object p0
.end method

.method public write_only_client_cash_drawer_shift_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$Builder;
    .locals 0

    .line 596
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Builder;->write_only_client_cash_drawer_shift_id:Ljava/lang/String;

    return-object p0
.end method

.method public write_only_location(Lcom/squareup/protos/client/GeoLocation;)Lcom/squareup/protos/client/bills/Tender$Builder;
    .locals 0

    .line 554
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Builder;->write_only_location:Lcom/squareup/protos/client/GeoLocation;

    return-object p0
.end method
