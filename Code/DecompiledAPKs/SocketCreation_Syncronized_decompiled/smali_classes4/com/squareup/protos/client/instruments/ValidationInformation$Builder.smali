.class public final Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ValidationInformation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/instruments/ValidationInformation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/instruments/ValidationInformation;",
        "Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cvv:Ljava/lang/String;

.field public expiration_date:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

.field public postal_code:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 126
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/instruments/ValidationInformation;
    .locals 5

    .line 154
    new-instance v0, Lcom/squareup/protos/client/instruments/ValidationInformation;

    iget-object v1, p0, Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;->postal_code:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;->cvv:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;->expiration_date:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/instruments/ValidationInformation;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 119
    invoke-virtual {p0}, Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;->build()Lcom/squareup/protos/client/instruments/ValidationInformation;

    move-result-object v0

    return-object v0
.end method

.method public cvv(Ljava/lang/String;)Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;->cvv:Ljava/lang/String;

    return-object p0
.end method

.method public expiration_date(Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;)Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 148
    iput-object p1, p0, Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;->expiration_date:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    return-object p0
.end method

.method public postal_code(Ljava/lang/String;)Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;->postal_code:Ljava/lang/String;

    return-object p0
.end method
