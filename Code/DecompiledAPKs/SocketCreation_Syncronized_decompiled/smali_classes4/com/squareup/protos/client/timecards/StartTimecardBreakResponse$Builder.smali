.class public final Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "StartTimecardBreakResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;",
        "Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public break_definition_outdated:Ljava/lang/Boolean;

.field public break_definitions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public timecard:Lcom/squareup/protos/client/timecards/Timecard;

.field public timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

.field public valid:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 140
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 141
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;->break_definitions:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public break_definition_outdated(Ljava/lang/Boolean;)Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;->break_definition_outdated:Ljava/lang/Boolean;

    return-object p0
.end method

.method public break_definitions(Ljava/util/List;)Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;",
            ">;)",
            "Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;"
        }
    .end annotation

    .line 155
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 156
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;->break_definitions:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;
    .locals 8

    .line 172
    new-instance v7, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v2, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    iget-object v3, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;->break_definitions:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;->valid:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;->break_definition_outdated:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;-><init>(Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/TimecardBreak;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 129
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;->build()Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;

    move-result-object v0

    return-object v0
.end method

.method public timecard(Lcom/squareup/protos/client/timecards/Timecard;)Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    return-object p0
.end method

.method public timecard_break(Lcom/squareup/protos/client/timecards/TimecardBreak;)Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;
    .locals 0

    .line 150
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    return-object p0
.end method

.method public valid(Ljava/lang/Boolean;)Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;
    .locals 0

    .line 161
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;->valid:Ljava/lang/Boolean;

    return-object p0
.end method
