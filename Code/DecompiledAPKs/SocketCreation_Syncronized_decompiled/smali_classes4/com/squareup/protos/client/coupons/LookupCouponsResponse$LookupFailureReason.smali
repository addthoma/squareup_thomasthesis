.class public final Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;
.super Lcom/squareup/wire/Message;
.source "LookupCouponsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/coupons/LookupCouponsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LookupFailureReason"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ProtoAdapter_LookupFailureReason;,
        Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;,
        Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;",
        "Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;

.field private static final serialVersionUID:J


# instance fields
.field public final expired_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final redeemed_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.coupons.LookupCouponsResponse$LookupFailureReason$ReasonType#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 124
    new-instance v0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ProtoAdapter_LookupFailureReason;

    invoke-direct {v0}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ProtoAdapter_LookupFailureReason;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 128
    sget-object v0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;->UNKNOWN:Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;

    sput-object v0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->DEFAULT_TYPE:Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;)V
    .locals 1

    .line 160
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;-><init>(Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lokio/ByteString;)V
    .locals 1

    .line 165
    sget-object v0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 166
    iput-object p1, p0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->type:Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;

    .line 167
    iput-object p2, p0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->expired_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 168
    iput-object p3, p0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->redeemed_at:Lcom/squareup/protos/client/ISO8601Date;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 184
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 185
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;

    .line 186
    invoke-virtual {p0}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->type:Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;

    iget-object v3, p1, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->type:Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;

    .line 187
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->expired_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->expired_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 188
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->redeemed_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object p1, p1, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->redeemed_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 189
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 194
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 196
    invoke-virtual {p0}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 197
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->type:Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 198
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->expired_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 199
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->redeemed_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 200
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;
    .locals 2

    .line 173
    new-instance v0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;-><init>()V

    .line 174
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->type:Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;

    iput-object v1, v0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;->type:Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;

    .line 175
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->expired_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;->expired_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 176
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->redeemed_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;->redeemed_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 177
    invoke-virtual {p0}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 123
    invoke-virtual {p0}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->newBuilder()Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 207
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 208
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->type:Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;

    if-eqz v1, :cond_0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->type:Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 209
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->expired_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_1

    const-string v1, ", expired_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->expired_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 210
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->redeemed_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_2

    const-string v1, ", redeemed_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->redeemed_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "LookupFailureReason{"

    .line 211
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
