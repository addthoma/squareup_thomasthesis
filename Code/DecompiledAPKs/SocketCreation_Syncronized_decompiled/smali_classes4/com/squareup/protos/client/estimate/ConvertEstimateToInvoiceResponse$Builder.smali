.class public final Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ConvertEstimateToInvoiceResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse;",
        "Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public estimate_display_details:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 93
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse;
    .locals 4

    .line 108
    new-instance v0, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse$Builder;->estimate_display_details:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 88
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse$Builder;->build()Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse;

    move-result-object v0

    return-object v0
.end method

.method public estimate_display_details(Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;)Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse$Builder;
    .locals 0

    .line 102
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse$Builder;->estimate_display_details:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse$Builder;
    .locals 0

    .line 97
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
