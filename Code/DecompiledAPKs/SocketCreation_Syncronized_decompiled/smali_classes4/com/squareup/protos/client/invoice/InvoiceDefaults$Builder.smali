.class public final Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InvoiceDefaults.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/InvoiceDefaults;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/InvoiceDefaults;",
        "Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public automatic_reminder_config:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
            ">;"
        }
    .end annotation
.end field

.field public automatic_reminders_enabled:Ljava/lang/Boolean;

.field public buyer_entered_instrument_enabled:Ljava/lang/Boolean;

.field public message:Ljava/lang/String;

.field public payment_request_defaults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;",
            ">;"
        }
    .end annotation
.end field

.field public relative_send_on:Ljava/lang/Integer;

.field public shipping_enabled:Ljava/lang/Boolean;

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 215
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 216
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->payment_request_defaults:Ljava/util/List;

    .line 217
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->automatic_reminder_config:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public automatic_reminder_config(Ljava/util/List;)Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
            ">;)",
            "Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;"
        }
    .end annotation

    .line 282
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 283
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->automatic_reminder_config:Ljava/util/List;

    return-object p0
.end method

.method public automatic_reminders_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;
    .locals 0

    .line 240
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->automatic_reminders_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/invoice/InvoiceDefaults;
    .locals 11

    .line 289
    new-instance v10, Lcom/squareup/protos/client/invoice/InvoiceDefaults;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->buyer_entered_instrument_enabled:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->shipping_enabled:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->automatic_reminders_enabled:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->relative_send_on:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->title:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->message:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->payment_request_defaults:Ljava/util/List;

    iget-object v8, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->automatic_reminder_config:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/client/invoice/InvoiceDefaults;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-object v10
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 198
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->build()Lcom/squareup/protos/client/invoice/InvoiceDefaults;

    move-result-object v0

    return-object v0
.end method

.method public buyer_entered_instrument_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;
    .locals 0

    .line 224
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->buyer_entered_instrument_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public message(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;
    .locals 0

    .line 264
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->message:Ljava/lang/String;

    return-object p0
.end method

.method public payment_request_defaults(Ljava/util/List;)Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;",
            ">;)",
            "Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;"
        }
    .end annotation

    .line 272
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 273
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->payment_request_defaults:Ljava/util/List;

    return-object p0
.end method

.method public relative_send_on(Ljava/lang/Integer;)Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;
    .locals 0

    .line 248
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->relative_send_on:Ljava/lang/Integer;

    return-object p0
.end method

.method public shipping_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;
    .locals 0

    .line 232
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->shipping_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public title(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;
    .locals 0

    .line 256
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->title:Ljava/lang/String;

    return-object p0
.end method
