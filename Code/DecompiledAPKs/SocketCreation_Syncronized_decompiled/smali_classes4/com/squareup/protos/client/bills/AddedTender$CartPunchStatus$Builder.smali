.class public final Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AddedTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;",
        "Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public current_punch_count:Ljava/lang/Long;

.field public earns_punch:Ljava/lang/Boolean;

.field public required_punch_count:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 880
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;
    .locals 5

    .line 909
    new-instance v0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus$Builder;->earns_punch:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus$Builder;->current_punch_count:Ljava/lang/Long;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus$Builder;->required_punch_count:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;-><init>(Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 873
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus$Builder;->build()Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;

    move-result-object v0

    return-object v0
.end method

.method public current_punch_count(Ljava/lang/Long;)Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus$Builder;
    .locals 0

    .line 895
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus$Builder;->current_punch_count:Ljava/lang/Long;

    return-object p0
.end method

.method public earns_punch(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus$Builder;
    .locals 0

    .line 887
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus$Builder;->earns_punch:Ljava/lang/Boolean;

    return-object p0
.end method

.method public required_punch_count(Ljava/lang/Long;)Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus$Builder;
    .locals 0

    .line 903
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus$Builder;->required_punch_count:Ljava/lang/Long;

    return-object p0
.end method
