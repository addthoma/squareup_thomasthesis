.class public final Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;
.super Lcom/squareup/wire/Message;
.source "SetDepositScheduleRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$ProtoAdapter_SetDepositScheduleRequest;,
        Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;",
        "Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ENABLE_DAILY_SAME_DAY_SETTLEMENT:Ljava/lang/Boolean;

.field public static final DEFAULT_PAUSE_SETTLEMENT:Ljava/lang/Boolean;

.field public static final DEFAULT_SETTLE_IF_OPTIONAL:Ljava/lang/Boolean;

.field public static final DEFAULT_TIMEZONE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final day_of_week_deposit_settings:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.depositsettings.DayOfWeekDepositSettings#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;",
            ">;"
        }
    .end annotation
.end field

.field public final enable_daily_same_day_settlement:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final pause_settlement:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final settle_if_optional:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final timezone:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$ProtoAdapter_SetDepositScheduleRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$ProtoAdapter_SetDepositScheduleRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 28
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->DEFAULT_ENABLE_DAILY_SAME_DAY_SETTLEMENT:Ljava/lang/Boolean;

    .line 30
    sput-object v0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->DEFAULT_SETTLE_IF_OPTIONAL:Ljava/lang/Boolean;

    .line 32
    sput-object v0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->DEFAULT_PAUSE_SETTLEMENT:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .line 92
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 99
    sget-object v0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 100
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->timezone:Ljava/lang/String;

    const-string p1, "day_of_week_deposit_settings"

    .line 101
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->day_of_week_deposit_settings:Ljava/util/List;

    .line 102
    iput-object p3, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->enable_daily_same_day_settlement:Ljava/lang/Boolean;

    .line 103
    iput-object p4, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->settle_if_optional:Ljava/lang/Boolean;

    .line 104
    iput-object p5, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->pause_settlement:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 122
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 123
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;

    .line 124
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->timezone:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->timezone:Ljava/lang/String;

    .line 125
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->day_of_week_deposit_settings:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->day_of_week_deposit_settings:Ljava/util/List;

    .line 126
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->enable_daily_same_day_settlement:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->enable_daily_same_day_settlement:Ljava/lang/Boolean;

    .line 127
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->settle_if_optional:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->settle_if_optional:Ljava/lang/Boolean;

    .line 128
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->pause_settlement:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->pause_settlement:Ljava/lang/Boolean;

    .line 129
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 134
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 136
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 137
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->timezone:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 138
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->day_of_week_deposit_settings:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 139
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->enable_daily_same_day_settlement:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 140
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->settle_if_optional:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 141
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->pause_settlement:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 142
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;
    .locals 2

    .line 109
    new-instance v0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;-><init>()V

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->timezone:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;->timezone:Ljava/lang/String;

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->day_of_week_deposit_settings:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;->day_of_week_deposit_settings:Ljava/util/List;

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->enable_daily_same_day_settlement:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;->enable_daily_same_day_settlement:Ljava/lang/Boolean;

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->settle_if_optional:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;->settle_if_optional:Ljava/lang/Boolean;

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->pause_settlement:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;->pause_settlement:Ljava/lang/Boolean;

    .line 115
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->newBuilder()Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 149
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 150
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->timezone:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", timezone="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->timezone:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->day_of_week_deposit_settings:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", day_of_week_deposit_settings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->day_of_week_deposit_settings:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 152
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->enable_daily_same_day_settlement:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", enable_daily_same_day_settlement="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->enable_daily_same_day_settlement:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 153
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->settle_if_optional:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", settle_if_optional="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->settle_if_optional:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 154
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->pause_settlement:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", pause_settlement="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;->pause_settlement:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SetDepositScheduleRequest{"

    .line 155
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
