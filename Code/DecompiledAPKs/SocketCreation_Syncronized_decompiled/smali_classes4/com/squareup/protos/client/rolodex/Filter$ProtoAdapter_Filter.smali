.class final Lcom/squareup/protos/client/rolodex/Filter$ProtoAdapter_Filter;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Filter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/Filter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Filter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/rolodex/Filter;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1486
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/rolodex/Filter;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/rolodex/Filter;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1595
    new-instance v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/Filter$Builder;-><init>()V

    .line 1596
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1597
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_d

    const/4 v4, 0x1

    if-eq v3, v4, :cond_c

    const/4 v4, 0x2

    if-eq v3, v4, :cond_b

    const/4 v4, 0x3

    if-eq v3, v4, :cond_a

    const/16 v4, 0x64

    if-eq v3, v4, :cond_9

    const/16 v4, 0x65

    if-eq v3, v4, :cond_8

    const/16 v4, 0x12c

    if-eq v3, v4, :cond_7

    const/16 v4, 0x12d

    if-eq v3, v4, :cond_6

    const/16 v4, 0x190

    if-eq v3, v4, :cond_5

    const/16 v4, 0x191

    if-eq v3, v4, :cond_4

    const/16 v4, 0x2bc

    if-eq v3, v4, :cond_3

    const/16 v4, 0x2bd

    if-eq v3, v4, :cond_2

    const/16 v4, 0x320

    if-eq v3, v4, :cond_1

    const/16 v4, 0x321

    if-eq v3, v4, :cond_0

    sparse-switch v3, :sswitch_data_0

    packed-switch v3, :pswitch_data_0

    packed-switch v3, :pswitch_data_1

    packed-switch v3, :pswitch_data_2

    .line 1661
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1641
    :pswitch_0
    iget-object v3, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->caen_attribute_options:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1640
    :pswitch_1
    iget-object v3, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->caen_attribute_values:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1639
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->caen_attribute_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto :goto_0

    .line 1629
    :pswitch_3
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cat_condition(Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 1631
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 1626
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cat_attribute_value(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto/16 :goto_0

    .line 1625
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cat_attribute_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto/16 :goto_0

    .line 1624
    :pswitch_6
    iget-object v3, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->r_y_days_options:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1623
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->r_y_days_field_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto/16 :goto_0

    .line 1622
    :pswitch_8
    sget-object v3, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/rolodex/Filter$Option;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->r_y_days(Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto/16 :goto_0

    .line 1621
    :pswitch_9
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->r_x_payments_field_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto/16 :goto_0

    .line 1620
    :pswitch_a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->r_x_payments(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto/16 :goto_0

    .line 1659
    :sswitch_0
    iget-object v3, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->iip_is_instant_profile_options:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1658
    :sswitch_1
    sget-object v3, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/rolodex/Filter$Option;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->iip_is_instant_profile(Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto/16 :goto_0

    .line 1657
    :sswitch_2
    iget-object v3, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cs_creation_source_types_options:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1656
    :sswitch_3
    iget-object v3, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cs_creation_source_types:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1655
    :sswitch_4
    iget-object v3, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hvl_locations_options:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1654
    :sswitch_5
    iget-object v3, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hvl_locations:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1653
    :sswitch_6
    iget-object v3, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hl_has_loyalty_options:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1652
    :sswitch_7
    sget-object v3, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/rolodex/Filter$Option;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hl_has_loyalty(Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto/16 :goto_0

    .line 1651
    :sswitch_8
    iget-object v3, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hcof_has_card_on_file_options:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1650
    :sswitch_9
    sget-object v3, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/rolodex/Filter$Option;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hcof_has_card_on_file(Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto/16 :goto_0

    .line 1649
    :sswitch_a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->sq_full_phone_number(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto/16 :goto_0

    .line 1648
    :sswitch_b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->sq_full_email_address(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto/16 :goto_0

    .line 1647
    :sswitch_c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->sq_search_query(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto/16 :goto_0

    .line 1646
    :sswitch_d
    iget-object v3, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->fb_sentiment_type_options:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1645
    :sswitch_e
    sget-object v3, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/rolodex/Filter$Option;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->fb_sentiment_type(Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto/16 :goto_0

    .line 1644
    :sswitch_f
    iget-object v3, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cab_attribute_options:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1643
    :sswitch_10
    sget-object v3, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/rolodex/Filter$Option;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cab_attribute_value(Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto/16 :goto_0

    .line 1642
    :sswitch_11
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cab_attribute_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto/16 :goto_0

    .line 1615
    :sswitch_12
    iget-object v3, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->xpilyd_y_days_options:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1614
    :sswitch_13
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->xpilyd_y_days_field_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto/16 :goto_0

    .line 1613
    :sswitch_14
    sget-object v3, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/rolodex/Filter$Option;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->xpilyd_y_days(Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto/16 :goto_0

    .line 1612
    :sswitch_15
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->xpilyd_x_payments_field_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto/16 :goto_0

    .line 1611
    :sswitch_16
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->xpilyd_x_payments(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto/16 :goto_0

    .line 1638
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cap_attribute_value(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto/16 :goto_0

    .line 1637
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cap_attribute_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto/16 :goto_0

    .line 1636
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cae_attribute_value(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto/16 :goto_0

    .line 1635
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cae_attribute_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto/16 :goto_0

    .line 1619
    :cond_4
    iget-object v3, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->npilyd_y_days_options:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1618
    :cond_5
    sget-object v3, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/rolodex/Filter$Option;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->npilyd_y_days(Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto/16 :goto_0

    .line 1617
    :cond_6
    iget-object v3, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->lpilyd_y_days_options:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1616
    :cond_7
    sget-object v3, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/rolodex/Filter$Option;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->lpilyd_y_days(Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto/16 :goto_0

    .line 1610
    :cond_8
    iget-object v3, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->mg_group_tokens:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1609
    :cond_9
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->group_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto/16 :goto_0

    .line 1608
    :cond_a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->allow_multiples_in_conjunction(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto/16 :goto_0

    .line 1607
    :cond_b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->display_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto/16 :goto_0

    .line 1601
    :cond_c
    :try_start_1
    sget-object v4, Lcom/squareup/protos/client/rolodex/Filter$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->type(Lcom/squareup/protos/client/rolodex/Filter$Type;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v4

    .line 1603
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 1665
    :cond_d
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1666
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->build()Lcom/squareup/protos/client/rolodex/Filter;

    move-result-object p1

    return-object p1

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_16
        0xc9 -> :sswitch_15
        0xca -> :sswitch_14
        0xcb -> :sswitch_13
        0xcc -> :sswitch_12
        0x3e8 -> :sswitch_11
        0x3e9 -> :sswitch_10
        0x3ea -> :sswitch_f
        0x44c -> :sswitch_e
        0x44d -> :sswitch_d
        0x4b0 -> :sswitch_c
        0x4b1 -> :sswitch_b
        0x4b2 -> :sswitch_a
        0x514 -> :sswitch_9
        0x515 -> :sswitch_8
        0x578 -> :sswitch_7
        0x579 -> :sswitch_6
        0x640 -> :sswitch_5
        0x641 -> :sswitch_4
        0x6a4 -> :sswitch_3
        0x6a5 -> :sswitch_2
        0x708 -> :sswitch_1
        0x709 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1f4
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x258
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x384
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1484
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/Filter$ProtoAdapter_Filter;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/rolodex/Filter;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/rolodex/Filter;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1543
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1544
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->display_name:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1545
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->allow_multiples_in_conjunction:Ljava/lang/Boolean;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1546
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->group_token:Ljava/lang/String;

    const/16 v2, 0x64

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1547
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->mg_group_tokens:Ljava/util/List;

    const/16 v2, 0x65

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1548
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_x_payments:Ljava/lang/Integer;

    const/16 v2, 0xc8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1549
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_x_payments_field_name:Ljava/lang/String;

    const/16 v2, 0xc9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1550
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    const/16 v2, 0xca

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1551
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days_field_name:Ljava/lang/String;

    const/16 v2, 0xcb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1552
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days_options:Ljava/util/List;

    const/16 v2, 0xcc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1553
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->lpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    const/16 v2, 0x12c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1554
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->lpilyd_y_days_options:Ljava/util/List;

    const/16 v2, 0x12d

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1555
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->npilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    const/16 v2, 0x190

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1556
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->npilyd_y_days_options:Ljava/util/List;

    const/16 v2, 0x191

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1557
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->r_x_payments:Ljava/lang/Integer;

    const/16 v2, 0x1f4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1558
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->r_x_payments_field_name:Ljava/lang/String;

    const/16 v2, 0x1f5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1559
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->r_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    const/16 v2, 0x1f6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1560
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->r_y_days_field_name:Ljava/lang/String;

    const/16 v2, 0x1f7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1561
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->r_y_days_options:Ljava/util/List;

    const/16 v2, 0x1f8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1562
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->cat_attribute_token:Ljava/lang/String;

    const/16 v2, 0x258

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1563
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->cat_attribute_value:Ljava/lang/String;

    const/16 v2, 0x259

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1564
    sget-object v0, Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->cat_condition:Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

    const/16 v2, 0x25a

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1565
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->cae_attribute_token:Ljava/lang/String;

    const/16 v2, 0x2bc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1566
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->cae_attribute_value:Ljava/lang/String;

    const/16 v2, 0x2bd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1567
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->cap_attribute_token:Ljava/lang/String;

    const/16 v2, 0x320

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1568
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->cap_attribute_value:Ljava/lang/String;

    const/16 v2, 0x321

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1569
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_token:Ljava/lang/String;

    const/16 v2, 0x384

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1570
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_values:Ljava/util/List;

    const/16 v2, 0x385

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1571
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_options:Ljava/util/List;

    const/16 v2, 0x386

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1572
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_token:Ljava/lang/String;

    const/16 v2, 0x3e8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1573
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_value:Lcom/squareup/protos/client/rolodex/Filter$Option;

    const/16 v2, 0x3e9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1574
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_options:Ljava/util/List;

    const/16 v2, 0x3ea

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1575
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->fb_sentiment_type:Lcom/squareup/protos/client/rolodex/Filter$Option;

    const/16 v2, 0x44c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1576
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->fb_sentiment_type_options:Ljava/util/List;

    const/16 v2, 0x44d

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1577
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->sq_search_query:Ljava/lang/String;

    const/16 v2, 0x4b0

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1578
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->sq_full_email_address:Ljava/lang/String;

    const/16 v2, 0x4b1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1579
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->sq_full_phone_number:Ljava/lang/String;

    const/16 v2, 0x4b2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1580
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->hcof_has_card_on_file:Lcom/squareup/protos/client/rolodex/Filter$Option;

    const/16 v2, 0x514

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1581
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->hcof_has_card_on_file_options:Ljava/util/List;

    const/16 v2, 0x515

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1582
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->hl_has_loyalty:Lcom/squareup/protos/client/rolodex/Filter$Option;

    const/16 v2, 0x578

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1583
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->hl_has_loyalty_options:Ljava/util/List;

    const/16 v2, 0x579

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1584
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->hvl_locations:Ljava/util/List;

    const/16 v2, 0x640

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1585
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->hvl_locations_options:Ljava/util/List;

    const/16 v2, 0x641

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1586
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->cs_creation_source_types:Ljava/util/List;

    const/16 v2, 0x6a4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1587
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->cs_creation_source_types_options:Ljava/util/List;

    const/16 v2, 0x6a5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1588
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->iip_is_instant_profile:Lcom/squareup/protos/client/rolodex/Filter$Option;

    const/16 v2, 0x708

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1589
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Filter;->iip_is_instant_profile_options:Ljava/util/List;

    const/16 v2, 0x709

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1590
    invoke-virtual {p2}, Lcom/squareup/protos/client/rolodex/Filter;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1484
    check-cast p2, Lcom/squareup/protos/client/rolodex/Filter;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/rolodex/Filter$ProtoAdapter_Filter;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/rolodex/Filter;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/rolodex/Filter;)I
    .locals 4

    .line 1491
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->display_name:Ljava/lang/String;

    const/4 v3, 0x2

    .line 1492
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->allow_multiples_in_conjunction:Ljava/lang/Boolean;

    const/4 v3, 0x3

    .line 1493
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->group_token:Ljava/lang/String;

    const/16 v3, 0x64

    .line 1494
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    .line 1495
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->mg_group_tokens:Ljava/util/List;

    const/16 v3, 0x65

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_x_payments:Ljava/lang/Integer;

    const/16 v3, 0xc8

    .line 1496
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_x_payments_field_name:Ljava/lang/String;

    const/16 v3, 0xc9

    .line 1497
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    const/16 v3, 0xca

    .line 1498
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days_field_name:Ljava/lang/String;

    const/16 v3, 0xcb

    .line 1499
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1500
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days_options:Ljava/util/List;

    const/16 v3, 0xcc

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->lpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    const/16 v3, 0x12c

    .line 1501
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1502
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->lpilyd_y_days_options:Ljava/util/List;

    const/16 v3, 0x12d

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->npilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    const/16 v3, 0x190

    .line 1503
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1504
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->npilyd_y_days_options:Ljava/util/List;

    const/16 v3, 0x191

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->r_x_payments:Ljava/lang/Integer;

    const/16 v3, 0x1f4

    .line 1505
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->r_x_payments_field_name:Ljava/lang/String;

    const/16 v3, 0x1f5

    .line 1506
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->r_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    const/16 v3, 0x1f6

    .line 1507
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->r_y_days_field_name:Ljava/lang/String;

    const/16 v3, 0x1f7

    .line 1508
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1509
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->r_y_days_options:Ljava/util/List;

    const/16 v3, 0x1f8

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->cat_attribute_token:Ljava/lang/String;

    const/16 v3, 0x258

    .line 1510
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->cat_attribute_value:Ljava/lang/String;

    const/16 v3, 0x259

    .line 1511
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->cat_condition:Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

    const/16 v3, 0x25a

    .line 1512
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->cae_attribute_token:Ljava/lang/String;

    const/16 v3, 0x2bc

    .line 1513
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->cae_attribute_value:Ljava/lang/String;

    const/16 v3, 0x2bd

    .line 1514
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->cap_attribute_token:Ljava/lang/String;

    const/16 v3, 0x320

    .line 1515
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->cap_attribute_value:Ljava/lang/String;

    const/16 v3, 0x321

    .line 1516
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_token:Ljava/lang/String;

    const/16 v3, 0x384

    .line 1517
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1518
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_values:Ljava/util/List;

    const/16 v3, 0x385

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1519
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_options:Ljava/util/List;

    const/16 v3, 0x386

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_token:Ljava/lang/String;

    const/16 v3, 0x3e8

    .line 1520
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_value:Lcom/squareup/protos/client/rolodex/Filter$Option;

    const/16 v3, 0x3e9

    .line 1521
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1522
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_options:Ljava/util/List;

    const/16 v3, 0x3ea

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->fb_sentiment_type:Lcom/squareup/protos/client/rolodex/Filter$Option;

    const/16 v3, 0x44c

    .line 1523
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1524
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->fb_sentiment_type_options:Ljava/util/List;

    const/16 v3, 0x44d

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->sq_search_query:Ljava/lang/String;

    const/16 v3, 0x4b0

    .line 1525
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->sq_full_email_address:Ljava/lang/String;

    const/16 v3, 0x4b1

    .line 1526
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->sq_full_phone_number:Ljava/lang/String;

    const/16 v3, 0x4b2

    .line 1527
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->hcof_has_card_on_file:Lcom/squareup/protos/client/rolodex/Filter$Option;

    const/16 v3, 0x514

    .line 1528
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1529
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->hcof_has_card_on_file_options:Ljava/util/List;

    const/16 v3, 0x515

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->hl_has_loyalty:Lcom/squareup/protos/client/rolodex/Filter$Option;

    const/16 v3, 0x578

    .line 1530
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1531
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->hl_has_loyalty_options:Ljava/util/List;

    const/16 v3, 0x579

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1532
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->hvl_locations:Ljava/util/List;

    const/16 v3, 0x640

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1533
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->hvl_locations_options:Ljava/util/List;

    const/16 v3, 0x641

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1534
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->cs_creation_source_types:Ljava/util/List;

    const/16 v3, 0x6a4

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1535
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->cs_creation_source_types_options:Ljava/util/List;

    const/16 v3, 0x6a5

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->iip_is_instant_profile:Lcom/squareup/protos/client/rolodex/Filter$Option;

    const/16 v3, 0x708

    .line 1536
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1537
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->iip_is_instant_profile_options:Ljava/util/List;

    const/16 v3, 0x709

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1538
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Filter;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1484
    check-cast p1, Lcom/squareup/protos/client/rolodex/Filter;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/Filter$ProtoAdapter_Filter;->encodedSize(Lcom/squareup/protos/client/rolodex/Filter;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/rolodex/Filter;)Lcom/squareup/protos/client/rolodex/Filter;
    .locals 2

    .line 1671
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Filter;->newBuilder()Lcom/squareup/protos/client/rolodex/Filter$Builder;

    move-result-object p1

    .line 1672
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->xpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->xpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Filter$Option;

    iput-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->xpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 1673
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->xpilyd_y_days_options:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1674
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->lpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->lpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Filter$Option;

    iput-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->lpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 1675
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->lpilyd_y_days_options:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1676
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->npilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->npilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Filter$Option;

    iput-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->npilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 1677
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->npilyd_y_days_options:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1678
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->r_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->r_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Filter$Option;

    iput-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->r_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 1679
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->r_y_days_options:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1680
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->caen_attribute_values:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1681
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->caen_attribute_options:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1682
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cab_attribute_value:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cab_attribute_value:Lcom/squareup/protos/client/rolodex/Filter$Option;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Filter$Option;

    iput-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cab_attribute_value:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 1683
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cab_attribute_options:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1684
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->fb_sentiment_type:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->fb_sentiment_type:Lcom/squareup/protos/client/rolodex/Filter$Option;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Filter$Option;

    iput-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->fb_sentiment_type:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 1685
    :cond_5
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->fb_sentiment_type_options:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1686
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hcof_has_card_on_file:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hcof_has_card_on_file:Lcom/squareup/protos/client/rolodex/Filter$Option;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Filter$Option;

    iput-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hcof_has_card_on_file:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 1687
    :cond_6
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hcof_has_card_on_file_options:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1688
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hl_has_loyalty:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hl_has_loyalty:Lcom/squareup/protos/client/rolodex/Filter$Option;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Filter$Option;

    iput-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hl_has_loyalty:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 1689
    :cond_7
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hl_has_loyalty_options:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1690
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hvl_locations:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1691
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hvl_locations_options:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1692
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cs_creation_source_types:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1693
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cs_creation_source_types_options:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1694
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->iip_is_instant_profile:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v0, :cond_8

    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->iip_is_instant_profile:Lcom/squareup/protos/client/rolodex/Filter$Option;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Filter$Option;

    iput-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->iip_is_instant_profile:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 1695
    :cond_8
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->iip_is_instant_profile_options:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1696
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1697
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->build()Lcom/squareup/protos/client/rolodex/Filter;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1484
    check-cast p1, Lcom/squareup/protos/client/rolodex/Filter;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/Filter$ProtoAdapter_Filter;->redact(Lcom/squareup/protos/client/rolodex/Filter;)Lcom/squareup/protos/client/rolodex/Filter;

    move-result-object p1

    return-object p1
.end method
