.class public final Lcom/squareup/protos/client/rolodex/Group;
.super Lcom/squareup/wire/Message;
.source "Group.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/Group$ProtoAdapter_Group;,
        Lcom/squareup/protos/client/rolodex/Group$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/rolodex/Group;",
        "Lcom/squareup/protos/client/rolodex/Group$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_AUDIENCE_TYPE:Lcom/squareup/protos/client/rolodex/AudienceType;

.field public static final DEFAULT_DISPLAY_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_GROUP_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_GROUP_TYPE:Lcom/squareup/protos/client/rolodex/GroupType;

.field public static final DEFAULT_MERCHANT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_NUM_CUSTOMERS:Ljava/lang/Long;

.field public static final DEFAULT_NUM_MANUAL_CONTACTS:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final audience_type:Lcom/squareup/protos/client/rolodex/AudienceType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.AudienceType#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final display_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final filters:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Filter#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x8
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation
.end field

.field public final group_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final group_type:Lcom/squareup/protos/client/rolodex/GroupType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.GroupType#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final merchant_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final num_customers:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x7
    .end annotation
.end field

.field public final num_manual_contacts:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 22
    new-instance v0, Lcom/squareup/protos/client/rolodex/Group$ProtoAdapter_Group;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/Group$ProtoAdapter_Group;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Group;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 32
    sget-object v0, Lcom/squareup/protos/client/rolodex/GroupType;->MANUAL_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    sput-object v0, Lcom/squareup/protos/client/rolodex/Group;->DEFAULT_GROUP_TYPE:Lcom/squareup/protos/client/rolodex/GroupType;

    .line 34
    sget-object v0, Lcom/squareup/protos/client/rolodex/AudienceType;->NONE:Lcom/squareup/protos/client/rolodex/AudienceType;

    sput-object v0, Lcom/squareup/protos/client/rolodex/Group;->DEFAULT_AUDIENCE_TYPE:Lcom/squareup/protos/client/rolodex/AudienceType;

    const-wide/16 v0, 0x0

    .line 36
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/rolodex/Group;->DEFAULT_NUM_MANUAL_CONTACTS:Ljava/lang/Long;

    .line 38
    sput-object v0, Lcom/squareup/protos/client/rolodex/Group;->DEFAULT_NUM_CUSTOMERS:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/GroupType;Lcom/squareup/protos/client/rolodex/AudienceType;Ljava/lang/Long;Ljava/lang/Long;Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/rolodex/GroupType;",
            "Lcom/squareup/protos/client/rolodex/AudienceType;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;)V"
        }
    .end annotation

    .line 108
    sget-object v9, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/client/rolodex/Group;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/GroupType;Lcom/squareup/protos/client/rolodex/AudienceType;Ljava/lang/Long;Ljava/lang/Long;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/GroupType;Lcom/squareup/protos/client/rolodex/AudienceType;Ljava/lang/Long;Ljava/lang/Long;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/rolodex/GroupType;",
            "Lcom/squareup/protos/client/rolodex/AudienceType;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 114
    sget-object v0, Lcom/squareup/protos/client/rolodex/Group;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p9}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 115
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Group;->group_token:Ljava/lang/String;

    .line 116
    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Group;->merchant_token:Ljava/lang/String;

    .line 117
    iput-object p3, p0, Lcom/squareup/protos/client/rolodex/Group;->display_name:Ljava/lang/String;

    .line 118
    iput-object p4, p0, Lcom/squareup/protos/client/rolodex/Group;->group_type:Lcom/squareup/protos/client/rolodex/GroupType;

    .line 119
    iput-object p5, p0, Lcom/squareup/protos/client/rolodex/Group;->audience_type:Lcom/squareup/protos/client/rolodex/AudienceType;

    .line 120
    iput-object p6, p0, Lcom/squareup/protos/client/rolodex/Group;->num_manual_contacts:Ljava/lang/Long;

    .line 121
    iput-object p7, p0, Lcom/squareup/protos/client/rolodex/Group;->num_customers:Ljava/lang/Long;

    const-string p1, "filters"

    .line 122
    invoke-static {p1, p8}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Group;->filters:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 143
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/rolodex/Group;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 144
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/rolodex/Group;

    .line 145
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Group;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Group;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->group_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Group;->group_token:Ljava/lang/String;

    .line 146
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->merchant_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Group;->merchant_token:Ljava/lang/String;

    .line 147
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->display_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Group;->display_name:Ljava/lang/String;

    .line 148
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->group_type:Lcom/squareup/protos/client/rolodex/GroupType;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Group;->group_type:Lcom/squareup/protos/client/rolodex/GroupType;

    .line 149
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->audience_type:Lcom/squareup/protos/client/rolodex/AudienceType;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Group;->audience_type:Lcom/squareup/protos/client/rolodex/AudienceType;

    .line 150
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->num_manual_contacts:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Group;->num_manual_contacts:Ljava/lang/Long;

    .line 151
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->num_customers:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Group;->num_customers:Ljava/lang/Long;

    .line 152
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->filters:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Group;->filters:Ljava/util/List;

    .line 153
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 158
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 160
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Group;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 161
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->group_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 162
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 163
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->display_name:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 164
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->group_type:Lcom/squareup/protos/client/rolodex/GroupType;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/GroupType;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 165
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->audience_type:Lcom/squareup/protos/client/rolodex/AudienceType;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/AudienceType;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 166
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->num_manual_contacts:Ljava/lang/Long;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 167
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->num_customers:Ljava/lang/Long;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 168
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->filters:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 169
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/rolodex/Group$Builder;
    .locals 2

    .line 127
    new-instance v0, Lcom/squareup/protos/client/rolodex/Group$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/Group$Builder;-><init>()V

    .line 128
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->group_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Group$Builder;->group_token:Ljava/lang/String;

    .line 129
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->merchant_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Group$Builder;->merchant_token:Ljava/lang/String;

    .line 130
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->display_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Group$Builder;->display_name:Ljava/lang/String;

    .line 131
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->group_type:Lcom/squareup/protos/client/rolodex/GroupType;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Group$Builder;->group_type:Lcom/squareup/protos/client/rolodex/GroupType;

    .line 132
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->audience_type:Lcom/squareup/protos/client/rolodex/AudienceType;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Group$Builder;->audience_type:Lcom/squareup/protos/client/rolodex/AudienceType;

    .line 133
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->num_manual_contacts:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Group$Builder;->num_manual_contacts:Ljava/lang/Long;

    .line 134
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->num_customers:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Group$Builder;->num_customers:Ljava/lang/Long;

    .line 135
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->filters:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Group$Builder;->filters:Ljava/util/List;

    .line 136
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Group;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/Group$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Group;->newBuilder()Lcom/squareup/protos/client/rolodex/Group$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 176
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 177
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->group_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", group_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->group_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 178
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", merchant_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->merchant_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->display_name:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", display_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->display_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->group_type:Lcom/squareup/protos/client/rolodex/GroupType;

    if-eqz v1, :cond_3

    const-string v1, ", group_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->group_type:Lcom/squareup/protos/client/rolodex/GroupType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 181
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->audience_type:Lcom/squareup/protos/client/rolodex/AudienceType;

    if-eqz v1, :cond_4

    const-string v1, ", audience_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->audience_type:Lcom/squareup/protos/client/rolodex/AudienceType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 182
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->num_manual_contacts:Ljava/lang/Long;

    if-eqz v1, :cond_5

    const-string v1, ", num_manual_contacts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->num_manual_contacts:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 183
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->num_customers:Ljava/lang/Long;

    if-eqz v1, :cond_6

    const-string v1, ", num_customers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->num_customers:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 184
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->filters:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, ", filters="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->filters:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_7
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Group{"

    .line 185
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
