.class public final Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UpdateLoyaltyAccountMappingResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;",
        "Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public conflicting_contacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation
.end field

.field public errors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;",
            ">;"
        }
    .end annotation
.end field

.field public loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

.field public status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 143
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 144
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;->errors:Ljava/util/List;

    .line 145
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;->conflicting_contacts:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;
    .locals 7

    .line 186
    new-instance v6, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    iget-object v2, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    iget-object v3, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;->errors:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;->conflicting_contacts:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;-><init>(Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 134
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;->build()Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;

    move-result-object v0

    return-object v0
.end method

.method public conflicting_contacts(Ljava/util/List;)Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;)",
            "Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;"
        }
    .end annotation

    .line 179
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 180
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;->conflicting_contacts:Ljava/util/List;

    return-object p0
.end method

.method public errors(Ljava/util/List;)Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;",
            ">;)",
            "Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;"
        }
    .end annotation

    .line 168
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 169
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;->errors:Ljava/util/List;

    return-object p0
.end method

.method public loyalty_account_mapping(Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;)Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;
    .locals 0

    .line 152
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;)Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;
    .locals 0

    .line 160
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    return-object p0
.end method
