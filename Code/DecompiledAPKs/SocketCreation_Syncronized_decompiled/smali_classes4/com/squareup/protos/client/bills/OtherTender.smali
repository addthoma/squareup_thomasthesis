.class public final Lcom/squareup/protos/client/bills/OtherTender;
.super Lcom/squareup/wire/Message;
.source "OtherTender.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/OtherTender$ProtoAdapter_OtherTender;,
        Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;,
        Lcom/squareup/protos/client/bills/OtherTender$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/OtherTender;",
        "Lcom/squareup/protos/client/bills/OtherTender$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/OtherTender;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_OTHER_TENDER_TYPE:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

.field public static final DEFAULT_TENDER_NOTE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final external_metadata:Lcom/squareup/protos/client/bills/ExternalMetadata;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.ExternalMetadata#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final other_tender_type:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.OtherTender$OtherTenderType#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.TranslatedName#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final tender_note:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/client/bills/OtherTender$ProtoAdapter_OtherTender;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/OtherTender$ProtoAdapter_OtherTender;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/OtherTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 26
    sget-object v0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->UNKNOWN:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    sput-object v0, Lcom/squareup/protos/client/bills/OtherTender;->DEFAULT_OTHER_TENDER_TYPE:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;Ljava/lang/String;Lcom/squareup/protos/client/bills/TranslatedName;Lcom/squareup/protos/client/bills/ExternalMetadata;)V
    .locals 6

    .line 69
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bills/OtherTender;-><init>(Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;Ljava/lang/String;Lcom/squareup/protos/client/bills/TranslatedName;Lcom/squareup/protos/client/bills/ExternalMetadata;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;Ljava/lang/String;Lcom/squareup/protos/client/bills/TranslatedName;Lcom/squareup/protos/client/bills/ExternalMetadata;Lokio/ByteString;)V
    .locals 1

    .line 75
    sget-object v0, Lcom/squareup/protos/client/bills/OtherTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 76
    iput-object p1, p0, Lcom/squareup/protos/client/bills/OtherTender;->other_tender_type:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    .line 77
    iput-object p2, p0, Lcom/squareup/protos/client/bills/OtherTender;->tender_note:Ljava/lang/String;

    .line 78
    iput-object p3, p0, Lcom/squareup/protos/client/bills/OtherTender;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    .line 79
    iput-object p4, p0, Lcom/squareup/protos/client/bills/OtherTender;->external_metadata:Lcom/squareup/protos/client/bills/ExternalMetadata;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 96
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/OtherTender;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 97
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/OtherTender;

    .line 98
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/OtherTender;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/OtherTender;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/OtherTender;->other_tender_type:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/OtherTender;->other_tender_type:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    .line 99
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/OtherTender;->tender_note:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/OtherTender;->tender_note:Ljava/lang/String;

    .line 100
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/OtherTender;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/OtherTender;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    .line 101
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/OtherTender;->external_metadata:Lcom/squareup/protos/client/bills/ExternalMetadata;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/OtherTender;->external_metadata:Lcom/squareup/protos/client/bills/ExternalMetadata;

    .line 102
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 107
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 109
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/OtherTender;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/client/bills/OtherTender;->other_tender_type:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/client/bills/OtherTender;->tender_note:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/client/bills/OtherTender;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/TranslatedName;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/client/bills/OtherTender;->external_metadata:Lcom/squareup/protos/client/bills/ExternalMetadata;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/ExternalMetadata;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 114
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/OtherTender$Builder;
    .locals 2

    .line 84
    new-instance v0, Lcom/squareup/protos/client/bills/OtherTender$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/OtherTender$Builder;-><init>()V

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/client/bills/OtherTender;->other_tender_type:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/OtherTender$Builder;->other_tender_type:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/client/bills/OtherTender;->tender_note:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/OtherTender$Builder;->tender_note:Ljava/lang/String;

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/client/bills/OtherTender;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/OtherTender$Builder;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    .line 88
    iget-object v1, p0, Lcom/squareup/protos/client/bills/OtherTender;->external_metadata:Lcom/squareup/protos/client/bills/ExternalMetadata;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/OtherTender$Builder;->external_metadata:Lcom/squareup/protos/client/bills/ExternalMetadata;

    .line 89
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/OtherTender;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/OtherTender$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/OtherTender;->newBuilder()Lcom/squareup/protos/client/bills/OtherTender$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 121
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 122
    iget-object v1, p0, Lcom/squareup/protos/client/bills/OtherTender;->other_tender_type:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    if-eqz v1, :cond_0

    const-string v1, ", other_tender_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/OtherTender;->other_tender_type:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 123
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/OtherTender;->tender_note:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", tender_note="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/OtherTender;->tender_note:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/OtherTender;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    if-eqz v1, :cond_2

    const-string v1, ", read_only_translated_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/OtherTender;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/OtherTender;->external_metadata:Lcom/squareup/protos/client/bills/ExternalMetadata;

    if-eqz v1, :cond_3

    const-string v1, ", external_metadata="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/OtherTender;->external_metadata:Lcom/squareup/protos/client/bills/ExternalMetadata;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "OtherTender{"

    .line 126
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
