.class final Lcom/squareup/protos/client/settlements/SettlementType$ProtoAdapter_SettlementType;
.super Lcom/squareup/wire/EnumAdapter;
.source "SettlementType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/settlements/SettlementType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SettlementType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/protos/client/settlements/SettlementType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 71
    const-class v0, Lcom/squareup/protos/client/settlements/SettlementType;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/protos/client/settlements/SettlementType;
    .locals 0

    .line 76
    invoke-static {p1}, Lcom/squareup/protos/client/settlements/SettlementType;->fromValue(I)Lcom/squareup/protos/client/settlements/SettlementType;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 69
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/settlements/SettlementType$ProtoAdapter_SettlementType;->fromValue(I)Lcom/squareup/protos/client/settlements/SettlementType;

    move-result-object p1

    return-object p1
.end method
