.class public final Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;
.super Lcom/squareup/wire/Message;
.source "DeleteLoyaltyAccountResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse$ProtoAdapter_DeleteLoyaltyAccountResponse;,
        Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;",
        "Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_STATUS:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

.field private static final serialVersionUID:J


# instance fields
.field public final errors:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.LoyaltyRequestError#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;",
            ">;"
        }
    .end annotation
.end field

.field public final loyalty_account:Lcom/squareup/protos/client/loyalty/LoyaltyAccount;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.LoyaltyAccount#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.LoyaltyRequestStatus#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse$ProtoAdapter_DeleteLoyaltyAccountResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse$ProtoAdapter_DeleteLoyaltyAccountResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 28
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->STATUS_INVALID:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    sput-object v0, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;->DEFAULT_STATUS:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/loyalty/LoyaltyAccount;Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/loyalty/LoyaltyAccount;",
            "Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;",
            ">;)V"
        }
    .end annotation

    .line 62
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;-><init>(Lcom/squareup/protos/client/loyalty/LoyaltyAccount;Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/loyalty/LoyaltyAccount;Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/loyalty/LoyaltyAccount;",
            "Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 67
    sget-object v0, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 68
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;->loyalty_account:Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    .line 69
    iput-object p2, p0, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    const-string p1, "errors"

    .line 70
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;->errors:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 86
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 87
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;

    .line 88
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;->loyalty_account:Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;->loyalty_account:Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    .line 89
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    .line 90
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;->errors:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;->errors:Ljava/util/List;

    .line 91
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 96
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 98
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;->loyalty_account:Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;->errors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 102
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse$Builder;
    .locals 2

    .line 75
    new-instance v0, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse$Builder;-><init>()V

    .line 76
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;->loyalty_account:Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse$Builder;->loyalty_account:Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    .line 77
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse$Builder;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    .line 78
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;->errors:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse$Builder;->errors:Ljava/util/List;

    .line 79
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;->newBuilder()Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 109
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;->loyalty_account:Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    if-eqz v1, :cond_0

    const-string v1, ", loyalty_account="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;->loyalty_account:Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 111
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    if-eqz v1, :cond_1

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 112
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;->errors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", errors="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;->errors:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "DeleteLoyaltyAccountResponse{"

    .line 113
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
