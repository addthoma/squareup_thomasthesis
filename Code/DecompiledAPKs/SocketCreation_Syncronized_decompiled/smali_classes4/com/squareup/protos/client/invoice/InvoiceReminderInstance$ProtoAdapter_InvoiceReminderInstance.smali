.class final Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$ProtoAdapter_InvoiceReminderInstance;
.super Lcom/squareup/wire/ProtoAdapter;
.source "InvoiceReminderInstance.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_InvoiceReminderInstance"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 295
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 322
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;-><init>()V

    .line 323
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 324
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 340
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 338
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->token(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;

    goto :goto_0

    .line 337
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->relative_days(Ljava/lang/Integer;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;

    goto :goto_0

    .line 336
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->custom_message(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;

    goto :goto_0

    .line 335
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/common/time/YearMonthDay;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->sent_on(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;

    goto :goto_0

    .line 329
    :pswitch_4
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->state(Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 331
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 326
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->reminder_config_token(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;

    goto :goto_0

    .line 344
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 345
    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->build()Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 293
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$ProtoAdapter_InvoiceReminderInstance;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 311
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->reminder_config_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 312
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->state:Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 313
    sget-object v0, Lcom/squareup/protos/common/time/YearMonthDay;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->sent_on:Lcom/squareup/protos/common/time/YearMonthDay;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 314
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->custom_message:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 315
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->relative_days:Ljava/lang/Integer;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 316
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->token:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 317
    invoke-virtual {p2}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 293
    check-cast p2, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$ProtoAdapter_InvoiceReminderInstance;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;)I
    .locals 4

    .line 300
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->reminder_config_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->state:Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;

    const/4 v3, 0x2

    .line 301
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/time/YearMonthDay;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->sent_on:Lcom/squareup/protos/common/time/YearMonthDay;

    const/4 v3, 0x3

    .line 302
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->custom_message:Ljava/lang/String;

    const/4 v3, 0x4

    .line 303
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->relative_days:Ljava/lang/Integer;

    const/4 v3, 0x5

    .line 304
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->token:Ljava/lang/String;

    const/4 v3, 0x6

    .line 305
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 306
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 293
    check-cast p1, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$ProtoAdapter_InvoiceReminderInstance;->encodedSize(Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;
    .locals 2

    .line 350
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->newBuilder()Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;

    move-result-object p1

    .line 351
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->sent_on:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/time/YearMonthDay;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->sent_on:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/YearMonthDay;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->sent_on:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 352
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 353
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->build()Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 293
    check-cast p1, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$ProtoAdapter_InvoiceReminderInstance;->redact(Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    move-result-object p1

    return-object p1
.end method
