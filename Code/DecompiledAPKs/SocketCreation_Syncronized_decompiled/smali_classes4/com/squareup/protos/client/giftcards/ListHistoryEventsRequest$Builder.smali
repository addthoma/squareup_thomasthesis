.class public final Lcom/squareup/protos/client/giftcards/ListHistoryEventsRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListHistoryEventsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/giftcards/ListHistoryEventsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/giftcards/ListHistoryEventsRequest;",
        "Lcom/squareup/protos/client/giftcards/ListHistoryEventsRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public gift_card_token:Ljava/lang/String;

.field public size:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 101
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/giftcards/ListHistoryEventsRequest;
    .locals 4

    .line 119
    new-instance v0, Lcom/squareup/protos/client/giftcards/ListHistoryEventsRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/ListHistoryEventsRequest$Builder;->gift_card_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/giftcards/ListHistoryEventsRequest$Builder;->size:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/giftcards/ListHistoryEventsRequest;-><init>(Ljava/lang/String;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 96
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/ListHistoryEventsRequest$Builder;->build()Lcom/squareup/protos/client/giftcards/ListHistoryEventsRequest;

    move-result-object v0

    return-object v0
.end method

.method public gift_card_token(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/ListHistoryEventsRequest$Builder;
    .locals 0

    .line 105
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/ListHistoryEventsRequest$Builder;->gift_card_token:Ljava/lang/String;

    return-object p0
.end method

.method public size(Ljava/lang/Integer;)Lcom/squareup/protos/client/giftcards/ListHistoryEventsRequest$Builder;
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/ListHistoryEventsRequest$Builder;->size:Ljava/lang/Integer;

    return-object p0
.end method
