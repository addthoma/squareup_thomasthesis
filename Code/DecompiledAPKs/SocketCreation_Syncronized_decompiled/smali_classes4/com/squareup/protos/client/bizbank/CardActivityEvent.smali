.class public final Lcom/squareup/protos/client/bizbank/CardActivityEvent;
.super Lcom/squareup/wire/Message;
.source "CardActivityEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bizbank/CardActivityEvent$ProtoAdapter_CardActivityEvent;,
        Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;,
        Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;,
        Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bizbank/CardActivityEvent;",
        "Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ACTIVITY_TYPE:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

.field public static final DEFAULT_DESCRIPTION:Ljava/lang/String; = ""

.field public static final DEFAULT_IMAGE_URL:Ljava/lang/String; = ""

.field public static final DEFAULT_IS_PERSONAL_EXPENSE:Ljava/lang/Boolean;

.field public static final DEFAULT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_TRANSACTION_STATE:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

.field public static final DEFAULT_TRANSACTION_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final activity_type:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bizbank.CardActivityEvent$ActivityType#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final image_url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field

.field public final is_personal_expense:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final occurred_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final transaction_state:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bizbank.CardActivityEvent$TransactionState#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final transaction_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ProtoAdapter_CardActivityEvent;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ProtoAdapter_CardActivityEvent;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 33
    sget-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->DO_NOT_USE:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    sput-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->DEFAULT_ACTIVITY_TYPE:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    .line 35
    sget-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->PENDING:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    sput-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->DEFAULT_TRANSACTION_STATE:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    const/4 v0, 0x0

    .line 37
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->DEFAULT_IS_PERSONAL_EXPENSE:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11

    .line 124
    sget-object v10, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/client/bizbank/CardActivityEvent;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 130
    sget-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p10}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 131
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->token:Ljava/lang/String;

    .line 132
    iput-object p2, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 133
    iput-object p3, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->description:Ljava/lang/String;

    .line 134
    iput-object p4, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->amount:Lcom/squareup/protos/common/Money;

    .line 135
    iput-object p5, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->activity_type:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    .line 136
    iput-object p6, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->transaction_state:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    .line 137
    iput-object p7, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->is_personal_expense:Ljava/lang/Boolean;

    .line 138
    iput-object p8, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->transaction_token:Ljava/lang/String;

    .line 139
    iput-object p9, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->image_url:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 161
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bizbank/CardActivityEvent;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 162
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bizbank/CardActivityEvent;

    .line 163
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->token:Ljava/lang/String;

    .line 164
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 165
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->description:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->description:Ljava/lang/String;

    .line 166
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->amount:Lcom/squareup/protos/common/Money;

    .line 167
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->activity_type:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->activity_type:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    .line 168
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->transaction_state:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->transaction_state:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    .line 169
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->is_personal_expense:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->is_personal_expense:Ljava/lang/Boolean;

    .line 170
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->transaction_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->transaction_token:Ljava/lang/String;

    .line 171
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->image_url:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->image_url:Ljava/lang/String;

    .line 172
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 177
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_9

    .line 179
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 180
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 181
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 182
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->description:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 183
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 184
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->activity_type:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 185
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->transaction_state:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 186
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->is_personal_expense:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 187
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->transaction_token:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 188
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->image_url:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_8
    add-int/2addr v0, v2

    .line 189
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_9
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;
    .locals 2

    .line 144
    new-instance v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;-><init>()V

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;->token:Ljava/lang/String;

    .line 146
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 147
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;->description:Ljava/lang/String;

    .line 148
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;->amount:Lcom/squareup/protos/common/Money;

    .line 149
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->activity_type:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;->activity_type:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    .line 150
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->transaction_state:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;->transaction_state:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    .line 151
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->is_personal_expense:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;->is_personal_expense:Ljava/lang/Boolean;

    .line 152
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->transaction_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;->transaction_token:Ljava/lang/String;

    .line 153
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->image_url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;->image_url:Ljava/lang/String;

    .line 154
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->newBuilder()Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 196
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 197
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_1

    const-string v1, ", occurred_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 199
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->description:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    const-string v1, ", amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 201
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->activity_type:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    if-eqz v1, :cond_4

    const-string v1, ", activity_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->activity_type:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 202
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->transaction_state:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    if-eqz v1, :cond_5

    const-string v1, ", transaction_state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->transaction_state:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 203
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->is_personal_expense:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", is_personal_expense="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->is_personal_expense:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 204
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->transaction_token:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", transaction_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->transaction_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->image_url:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", image_url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->image_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CardActivityEvent{"

    .line 206
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
