.class public final Lcom/squareup/protos/client/tickets/VectorClock$Clock$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "VectorClock.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/tickets/VectorClock$Clock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/tickets/VectorClock$Clock;",
        "Lcom/squareup/protos/client/tickets/VectorClock$Clock$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public name:Ljava/lang/String;

.field public version:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 171
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/tickets/VectorClock$Clock;
    .locals 4

    .line 186
    new-instance v0, Lcom/squareup/protos/client/tickets/VectorClock$Clock;

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/VectorClock$Clock$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/tickets/VectorClock$Clock$Builder;->version:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/tickets/VectorClock$Clock;-><init>(Ljava/lang/String;Ljava/lang/Long;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 166
    invoke-virtual {p0}, Lcom/squareup/protos/client/tickets/VectorClock$Clock$Builder;->build()Lcom/squareup/protos/client/tickets/VectorClock$Clock;

    move-result-object v0

    return-object v0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/client/tickets/VectorClock$Clock$Builder;
    .locals 0

    .line 175
    iput-object p1, p0, Lcom/squareup/protos/client/tickets/VectorClock$Clock$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public version(Ljava/lang/Long;)Lcom/squareup/protos/client/tickets/VectorClock$Clock$Builder;
    .locals 0

    .line 180
    iput-object p1, p0, Lcom/squareup/protos/client/tickets/VectorClock$Clock$Builder;->version:Ljava/lang/Long;

    return-object p0
.end method
