.class public final Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;
.super Lcom/squareup/wire/Message;
.source "DiscountLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/DiscountLineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DisplayDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$ProtoAdapter_DisplayDetails;,
        Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;",
        "Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_APPLICATION_METHOD:Lcom/squareup/api/items/Discount$ApplicationMethod;

.field public static final DEFAULT_COGS_OBJECT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_PERCENTAGE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final application_method:Lcom/squareup/api/items/Discount$ApplicationMethod;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Discount$ApplicationMethod#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final cogs_object_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final coupon_ids:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x5
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final percentage:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 786
    new-instance v0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$ProtoAdapter_DisplayDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$ProtoAdapter_DisplayDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 796
    sget-object v0, Lcom/squareup/api/items/Discount$ApplicationMethod;->MANUALLY_APPLIED:Lcom/squareup/api/items/Discount$ApplicationMethod;

    sput-object v0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->DEFAULT_APPLICATION_METHOD:Lcom/squareup/api/items/Discount$ApplicationMethod;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Discount$ApplicationMethod;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/api/items/Discount$ApplicationMethod;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 845
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Discount$ApplicationMethod;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Discount$ApplicationMethod;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/api/items/Discount$ApplicationMethod;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 851
    sget-object v0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 852
    iput-object p1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->name:Ljava/lang/String;

    .line 853
    iput-object p2, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->percentage:Ljava/lang/String;

    .line 854
    iput-object p3, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    .line 855
    iput-object p4, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->application_method:Lcom/squareup/api/items/Discount$ApplicationMethod;

    const-string p1, "coupon_ids"

    .line 856
    invoke-static {p1, p5}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->coupon_ids:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 874
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 875
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    .line 876
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->name:Ljava/lang/String;

    .line 877
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->percentage:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->percentage:Ljava/lang/String;

    .line 878
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    .line 879
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->application_method:Lcom/squareup/api/items/Discount$ApplicationMethod;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->application_method:Lcom/squareup/api/items/Discount$ApplicationMethod;

    .line 880
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->coupon_ids:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->coupon_ids:Ljava/util/List;

    .line 881
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 886
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 888
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 889
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->name:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 890
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->percentage:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 891
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 892
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->application_method:Lcom/squareup/api/items/Discount$ApplicationMethod;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/api/items/Discount$ApplicationMethod;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 893
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->coupon_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 894
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;
    .locals 2

    .line 861
    new-instance v0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;-><init>()V

    .line 862
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;->name:Ljava/lang/String;

    .line 863
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->percentage:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;->percentage:Ljava/lang/String;

    .line 864
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;->cogs_object_id:Ljava/lang/String;

    .line 865
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->application_method:Lcom/squareup/api/items/Discount$ApplicationMethod;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;->application_method:Lcom/squareup/api/items/Discount$ApplicationMethod;

    .line 866
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->coupon_ids:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;->coupon_ids:Ljava/util/List;

    .line 867
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 785
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->newBuilder()Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 901
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 902
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 903
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->percentage:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", percentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->percentage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 904
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", cogs_object_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 905
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->application_method:Lcom/squareup/api/items/Discount$ApplicationMethod;

    if-eqz v1, :cond_3

    const-string v1, ", application_method="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->application_method:Lcom/squareup/api/items/Discount$ApplicationMethod;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 906
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->coupon_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ", coupon_ids="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->coupon_ids:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "DisplayDetails{"

    .line 907
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
