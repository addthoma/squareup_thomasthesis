.class final Lcom/squareup/protos/client/timecards/CalculationTotal$ProtoAdapter_CalculationTotal;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CalculationTotal.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/CalculationTotal;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CalculationTotal"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/timecards/CalculationTotal;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 156
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/timecards/CalculationTotal;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/timecards/CalculationTotal;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 177
    new-instance v0, Lcom/squareup/protos/client/timecards/CalculationTotal$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/CalculationTotal$Builder;-><init>()V

    .line 178
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 179
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 185
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 183
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/CalculationTotal$Builder;->doubletime_seconds(Ljava/lang/Integer;)Lcom/squareup/protos/client/timecards/CalculationTotal$Builder;

    goto :goto_0

    .line 182
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/CalculationTotal$Builder;->overtime_seconds(Ljava/lang/Integer;)Lcom/squareup/protos/client/timecards/CalculationTotal$Builder;

    goto :goto_0

    .line 181
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/CalculationTotal$Builder;->regular_seconds(Ljava/lang/Integer;)Lcom/squareup/protos/client/timecards/CalculationTotal$Builder;

    goto :goto_0

    .line 189
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/timecards/CalculationTotal$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 190
    invoke-virtual {v0}, Lcom/squareup/protos/client/timecards/CalculationTotal$Builder;->build()Lcom/squareup/protos/client/timecards/CalculationTotal;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 154
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/CalculationTotal$ProtoAdapter_CalculationTotal;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/timecards/CalculationTotal;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/timecards/CalculationTotal;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 169
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/CalculationTotal;->regular_seconds:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 170
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/CalculationTotal;->overtime_seconds:Ljava/lang/Integer;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 171
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/CalculationTotal;->doubletime_seconds:Ljava/lang/Integer;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 172
    invoke-virtual {p2}, Lcom/squareup/protos/client/timecards/CalculationTotal;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 154
    check-cast p2, Lcom/squareup/protos/client/timecards/CalculationTotal;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/timecards/CalculationTotal$ProtoAdapter_CalculationTotal;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/timecards/CalculationTotal;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/timecards/CalculationTotal;)I
    .locals 4

    .line 161
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/CalculationTotal;->regular_seconds:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/CalculationTotal;->overtime_seconds:Ljava/lang/Integer;

    const/4 v3, 0x2

    .line 162
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/CalculationTotal;->doubletime_seconds:Ljava/lang/Integer;

    const/4 v3, 0x3

    .line 163
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 164
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/CalculationTotal;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 154
    check-cast p1, Lcom/squareup/protos/client/timecards/CalculationTotal;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/CalculationTotal$ProtoAdapter_CalculationTotal;->encodedSize(Lcom/squareup/protos/client/timecards/CalculationTotal;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/timecards/CalculationTotal;)Lcom/squareup/protos/client/timecards/CalculationTotal;
    .locals 0

    .line 195
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/CalculationTotal;->newBuilder()Lcom/squareup/protos/client/timecards/CalculationTotal$Builder;

    move-result-object p1

    .line 196
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/CalculationTotal$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 197
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/CalculationTotal$Builder;->build()Lcom/squareup/protos/client/timecards/CalculationTotal;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 154
    check-cast p1, Lcom/squareup/protos/client/timecards/CalculationTotal;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/CalculationTotal$ProtoAdapter_CalculationTotal;->redact(Lcom/squareup/protos/client/timecards/CalculationTotal;)Lcom/squareup/protos/client/timecards/CalculationTotal;

    move-result-object p1

    return-object p1
.end method
