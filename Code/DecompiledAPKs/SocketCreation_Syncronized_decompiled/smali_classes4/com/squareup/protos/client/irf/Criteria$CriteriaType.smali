.class public final enum Lcom/squareup/protos/client/irf/Criteria$CriteriaType;
.super Ljava/lang/Enum;
.source "Criteria.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/irf/Criteria;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CriteriaType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/irf/Criteria$CriteriaType$ProtoAdapter_CriteriaType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/irf/Criteria$CriteriaType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/irf/Criteria$CriteriaType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/irf/Criteria$CriteriaType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ANSWER_MATCH_EXACT:Lcom/squareup/protos/client/irf/Criteria$CriteriaType;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/irf/Criteria$CriteriaType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 137
    new-instance v0, Lcom/squareup/protos/client/irf/Criteria$CriteriaType;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/irf/Criteria$CriteriaType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/irf/Criteria$CriteriaType;->UNKNOWN:Lcom/squareup/protos/client/irf/Criteria$CriteriaType;

    .line 139
    new-instance v0, Lcom/squareup/protos/client/irf/Criteria$CriteriaType;

    const/4 v2, 0x1

    const-string v3, "ANSWER_MATCH_EXACT"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/irf/Criteria$CriteriaType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/irf/Criteria$CriteriaType;->ANSWER_MATCH_EXACT:Lcom/squareup/protos/client/irf/Criteria$CriteriaType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/protos/client/irf/Criteria$CriteriaType;

    .line 136
    sget-object v3, Lcom/squareup/protos/client/irf/Criteria$CriteriaType;->UNKNOWN:Lcom/squareup/protos/client/irf/Criteria$CriteriaType;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/protos/client/irf/Criteria$CriteriaType;->ANSWER_MATCH_EXACT:Lcom/squareup/protos/client/irf/Criteria$CriteriaType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/client/irf/Criteria$CriteriaType;->$VALUES:[Lcom/squareup/protos/client/irf/Criteria$CriteriaType;

    .line 141
    new-instance v0, Lcom/squareup/protos/client/irf/Criteria$CriteriaType$ProtoAdapter_CriteriaType;

    invoke-direct {v0}, Lcom/squareup/protos/client/irf/Criteria$CriteriaType$ProtoAdapter_CriteriaType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/irf/Criteria$CriteriaType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 145
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 146
    iput p3, p0, Lcom/squareup/protos/client/irf/Criteria$CriteriaType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/irf/Criteria$CriteriaType;
    .locals 1

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 155
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/irf/Criteria$CriteriaType;->ANSWER_MATCH_EXACT:Lcom/squareup/protos/client/irf/Criteria$CriteriaType;

    return-object p0

    .line 154
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/irf/Criteria$CriteriaType;->UNKNOWN:Lcom/squareup/protos/client/irf/Criteria$CriteriaType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/irf/Criteria$CriteriaType;
    .locals 1

    .line 136
    const-class v0, Lcom/squareup/protos/client/irf/Criteria$CriteriaType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/irf/Criteria$CriteriaType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/irf/Criteria$CriteriaType;
    .locals 1

    .line 136
    sget-object v0, Lcom/squareup/protos/client/irf/Criteria$CriteriaType;->$VALUES:[Lcom/squareup/protos/client/irf/Criteria$CriteriaType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/irf/Criteria$CriteriaType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/irf/Criteria$CriteriaType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 162
    iget v0, p0, Lcom/squareup/protos/client/irf/Criteria$CriteriaType;->value:I

    return v0
.end method
