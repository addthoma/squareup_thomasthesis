.class final Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$ProtoAdapter_ManualMergeContactsRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ManualMergeContactsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ManualMergeContactsRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 172
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 195
    new-instance v0, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;-><init>()V

    .line 196
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 197
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 204
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 202
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->loyaltyAccountMapping(Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;)Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;

    goto :goto_0

    .line 201
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/rolodex/ContactSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/rolodex/ContactSet;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->contact_set(Lcom/squareup/protos/client/rolodex/ContactSet;)Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;

    goto :goto_0

    .line 200
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->request_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;

    goto :goto_0

    .line 199
    :cond_3
    sget-object v3, Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->duplicate_contact_token_set(Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;)Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;

    goto :goto_0

    .line 208
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 209
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 170
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$ProtoAdapter_ManualMergeContactsRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 186
    sget-object v0, Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->duplicate_contact_token_set:Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 187
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->request_token:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 188
    sget-object v0, Lcom/squareup/protos/client/rolodex/ContactSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->contact_set:Lcom/squareup/protos/client/rolodex/ContactSet;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 189
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->loyaltyAccountMapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 190
    invoke-virtual {p2}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 170
    check-cast p2, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$ProtoAdapter_ManualMergeContactsRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;)I
    .locals 4

    .line 177
    sget-object v0, Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->duplicate_contact_token_set:Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->request_token:Ljava/lang/String;

    const/4 v3, 0x2

    .line 178
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/ContactSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->contact_set:Lcom/squareup/protos/client/rolodex/ContactSet;

    const/4 v3, 0x3

    .line 179
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->loyaltyAccountMapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    const/4 v3, 0x4

    .line 180
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 181
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 170
    check-cast p1, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$ProtoAdapter_ManualMergeContactsRequest;->encodedSize(Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;)Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;
    .locals 2

    .line 214
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;->newBuilder()Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;

    move-result-object p1

    .line 215
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->duplicate_contact_token_set:Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->duplicate_contact_token_set:Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;

    iput-object v0, p1, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->duplicate_contact_token_set:Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;

    .line 216
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->contact_set:Lcom/squareup/protos/client/rolodex/ContactSet;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/rolodex/ContactSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->contact_set:Lcom/squareup/protos/client/rolodex/ContactSet;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/ContactSet;

    iput-object v0, p1, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->contact_set:Lcom/squareup/protos/client/rolodex/ContactSet;

    .line 217
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->loyaltyAccountMapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->loyaltyAccountMapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    iput-object v0, p1, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->loyaltyAccountMapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    .line 218
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 219
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 170
    check-cast p1, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest$ProtoAdapter_ManualMergeContactsRequest;->redact(Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;)Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;

    move-result-object p1

    return-object p1
.end method
