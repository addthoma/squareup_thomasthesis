.class public final Lcom/squareup/protos/client/settlements/RequestParams$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RequestParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/settlements/RequestParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/settlements/RequestParams;",
        "Lcom/squareup/protos/client/settlements/RequestParams$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public merchant_token:Ljava/lang/String;

.field public subunit_merchant_token:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public tz_name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 129
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 130
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/settlements/RequestParams$Builder;->subunit_merchant_token:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/settlements/RequestParams;
    .locals 5

    .line 167
    new-instance v0, Lcom/squareup/protos/client/settlements/RequestParams;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/RequestParams$Builder;->merchant_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/settlements/RequestParams$Builder;->subunit_merchant_token:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/settlements/RequestParams$Builder;->tz_name:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/settlements/RequestParams;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 122
    invoke-virtual {p0}, Lcom/squareup/protos/client/settlements/RequestParams$Builder;->build()Lcom/squareup/protos/client/settlements/RequestParams;

    move-result-object v0

    return-object v0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/settlements/RequestParams$Builder;
    .locals 0

    .line 138
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/RequestParams$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public subunit_merchant_token(Ljava/util/List;)Lcom/squareup/protos/client/settlements/RequestParams$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/settlements/RequestParams$Builder;"
        }
    .end annotation

    .line 150
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 151
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/RequestParams$Builder;->subunit_merchant_token:Ljava/util/List;

    return-object p0
.end method

.method public tz_name(Ljava/lang/String;)Lcom/squareup/protos/client/settlements/RequestParams$Builder;
    .locals 0

    .line 161
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/RequestParams$Builder;->tz_name:Ljava/lang/String;

    return-object p0
.end method
