.class final Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet$ProtoAdapter_ViewOrdersApplet;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ClientAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ViewOrdersApplet"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1739
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1754
    new-instance v0, Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet$Builder;-><init>()V

    .line 1755
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1756
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 1759
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1763
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1764
    invoke-virtual {v0}, Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet$Builder;->build()Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1737
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet$ProtoAdapter_ViewOrdersApplet;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1749
    invoke-virtual {p2}, Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1737
    check-cast p2, Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet$ProtoAdapter_ViewOrdersApplet;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;)I
    .locals 0

    .line 1744
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    return p1
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1737
    check-cast p1, Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet$ProtoAdapter_ViewOrdersApplet;->encodedSize(Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;)Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;
    .locals 0

    .line 1769
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;->newBuilder()Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet$Builder;

    move-result-object p1

    .line 1770
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1771
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet$Builder;->build()Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1737
    check-cast p1, Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet$ProtoAdapter_ViewOrdersApplet;->redact(Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;)Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    move-result-object p1

    return-object p1
.end method
