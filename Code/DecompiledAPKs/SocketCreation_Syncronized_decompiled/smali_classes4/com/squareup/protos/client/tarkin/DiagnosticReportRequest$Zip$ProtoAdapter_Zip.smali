.class final Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$ProtoAdapter_Zip;
.super Lcom/squareup/wire/ProtoAdapter;
.source "DiagnosticReportRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Zip"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 789
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 808
    new-instance v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$Builder;-><init>()V

    .line 809
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 810
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 815
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 813
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$Builder;->archive(Lokio/ByteString;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$Builder;

    goto :goto_0

    .line 812
    :cond_1
    iget-object v3, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$Builder;->metadata:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 819
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 820
    invoke-virtual {v0}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$Builder;->build()Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 787
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$ProtoAdapter_Zip;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 801
    sget-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->metadata:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 802
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->archive:Lokio/ByteString;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 803
    invoke-virtual {p2}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 787
    check-cast p2, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$ProtoAdapter_Zip;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;)I
    .locals 4

    .line 794
    sget-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->metadata:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->archive:Lokio/ByteString;

    const/4 v3, 0x2

    .line 795
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 796
    invoke-virtual {p1}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 787
    check-cast p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$ProtoAdapter_Zip;->encodedSize(Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;
    .locals 2

    .line 825
    invoke-virtual {p1}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->newBuilder()Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$Builder;

    move-result-object p1

    .line 826
    iget-object v0, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$Builder;->metadata:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 827
    invoke-virtual {p1}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 828
    invoke-virtual {p1}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$Builder;->build()Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 787
    check-cast p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$ProtoAdapter_Zip;->redact(Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;

    move-result-object p1

    return-object p1
.end method
