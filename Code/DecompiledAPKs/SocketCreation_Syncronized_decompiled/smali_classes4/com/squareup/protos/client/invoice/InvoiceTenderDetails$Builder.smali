.class public final Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InvoiceTenderDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;",
        "Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public brand:Ljava/lang/String;

.field public last_four:Ljava/lang/String;

.field public tender_note:Ljava/lang/String;

.field public tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

.field public tendered_at:Lcom/squareup/protos/common/time/DateTime;

.field public tip_amount:Lcom/squareup/protos/common/Money;

.field public total_amount:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 182
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public brand(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;
    .locals 0

    .line 186
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;->brand:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;
    .locals 10

    .line 233
    new-instance v9, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;->brand:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;->last_four:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    iget-object v4, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;->tender_note:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;->tip_amount:Lcom/squareup/protos/common/Money;

    iget-object v6, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;->total_amount:Lcom/squareup/protos/common/Money;

    iget-object v7, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;->tendered_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 167
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;->build()Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;

    move-result-object v0

    return-object v0
.end method

.method public last_four(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;
    .locals 0

    .line 191
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;->last_four:Ljava/lang/String;

    return-object p0
.end method

.method public tender_note(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;
    .locals 0

    .line 201
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;->tender_note:Ljava/lang/String;

    return-object p0
.end method

.method public tender_type(Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;)Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;
    .locals 0

    .line 196
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    return-object p0
.end method

.method public tendered_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;
    .locals 0

    .line 227
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;->tendered_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public tip_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;
    .locals 0

    .line 209
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;->tip_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public total_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;
    .locals 0

    .line 217
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;->total_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
