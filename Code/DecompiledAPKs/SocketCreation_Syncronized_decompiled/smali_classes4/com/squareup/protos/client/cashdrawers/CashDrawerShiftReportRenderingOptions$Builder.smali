.class public final Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CashDrawerShiftReportRenderingOptions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;",
        "Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public show_expected_in_drawer_fields:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 81
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;
    .locals 3

    .line 91
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions$Builder;->show_expected_in_drawer_fields:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;-><init>(Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 78
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions$Builder;->build()Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;

    move-result-object v0

    return-object v0
.end method

.method public show_expected_in_drawer_fields(Ljava/lang/Boolean;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions$Builder;
    .locals 0

    .line 85
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions$Builder;->show_expected_in_drawer_fields:Ljava/lang/Boolean;

    return-object p0
.end method
