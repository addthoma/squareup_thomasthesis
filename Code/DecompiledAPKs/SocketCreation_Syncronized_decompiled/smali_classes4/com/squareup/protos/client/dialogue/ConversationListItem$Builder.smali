.class public final Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ConversationListItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/dialogue/ConversationListItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/dialogue/ConversationListItem;",
        "Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public display_name:Ljava/lang/String;

.field public last_comment_content:Ljava/lang/String;

.field public last_comment_created_at:Lcom/squareup/protos/common/time/DateTime;

.field public opened:Ljava/lang/Boolean;

.field public sentiment:Lcom/squareup/protos/client/dialogue/Sentiment;

.field public token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 183
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/dialogue/ConversationListItem;
    .locals 9

    .line 238
    new-instance v8, Lcom/squareup/protos/client/dialogue/ConversationListItem;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;->token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;->opened:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;->last_comment_created_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v4, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;->last_comment_content:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;->sentiment:Lcom/squareup/protos/client/dialogue/Sentiment;

    iget-object v6, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;->display_name:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/dialogue/ConversationListItem;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Lcom/squareup/protos/client/dialogue/Sentiment;Ljava/lang/String;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 170
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;->build()Lcom/squareup/protos/client/dialogue/ConversationListItem;

    move-result-object v0

    return-object v0
.end method

.method public display_name(Ljava/lang/String;)Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;
    .locals 0

    .line 232
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;->display_name:Ljava/lang/String;

    return-object p0
.end method

.method public last_comment_content(Ljava/lang/String;)Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;
    .locals 0

    .line 216
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;->last_comment_content:Ljava/lang/String;

    return-object p0
.end method

.method public last_comment_created_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;
    .locals 0

    .line 208
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;->last_comment_created_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public opened(Ljava/lang/Boolean;)Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;
    .locals 0

    .line 198
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;->opened:Ljava/lang/Boolean;

    return-object p0
.end method

.method public sentiment(Lcom/squareup/protos/client/dialogue/Sentiment;)Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;
    .locals 0

    .line 224
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;->sentiment:Lcom/squareup/protos/client/dialogue/Sentiment;

    return-object p0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;
    .locals 0

    .line 190
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;->token:Ljava/lang/String;

    return-object p0
.end method
