.class final Lcom/squareup/protos/client/bills/Tender$ProtoAdapter_Tender;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Tender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Tender"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Tender;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 2583
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Tender;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Tender;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2648
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$Builder;-><init>()V

    .line 2649
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 2650
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 2699
    :pswitch_0
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 2697
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$Builder;

    goto :goto_0

    .line 2696
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Builder;->checkout_app_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$Builder;

    goto :goto_0

    .line 2695
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Builder;->create_payment_note(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$Builder;

    goto :goto_0

    .line 2694
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Builder;->reference_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$Builder;

    goto :goto_0

    .line 2693
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Builder;->checkout_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$Builder;

    goto :goto_0

    .line 2692
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/common/payment/PeripheralMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/payment/PeripheralMetadata;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Builder;->peripheral_metadata(Lcom/squareup/protos/common/payment/PeripheralMetadata;)Lcom/squareup/protos/client/bills/Tender$Builder;

    goto :goto_0

    .line 2691
    :pswitch_7
    sget-object v3, Lcom/squareup/protos/client/CreatorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/CreatorDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Builder;->open_ticket_owner(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/bills/Tender$Builder;

    goto :goto_0

    .line 2690
    :pswitch_8
    sget-object v3, Lcom/squareup/protos/client/bills/ClientDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/ClientDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Builder;->client_details(Lcom/squareup/protos/client/bills/ClientDetails;)Lcom/squareup/protos/client/bills/Tender$Builder;

    goto :goto_0

    .line 2689
    :pswitch_9
    sget-object v3, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_loyalty_details(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;)Lcom/squareup/protos/client/bills/Tender$Builder;

    goto :goto_0

    .line 2688
    :pswitch_a
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Tender$Builder;->tender_tag:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bills/TenderTag;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2682
    :pswitch_b
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_refund_card_presence_requirement(Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;)Lcom/squareup/protos/client/bills/Tender$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v4

    .line 2684
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bills/Tender$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 2679
    :pswitch_c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_sequential_tender_number(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$Builder;

    goto/16 :goto_0

    .line 2678
    :pswitch_d
    sget-object v3, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Builder;->extra_tender_details(Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;)Lcom/squareup/protos/client/bills/Tender$Builder;

    goto/16 :goto_0

    .line 2677
    :pswitch_e
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Builder;->write_only_client_cash_drawer_shift_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$Builder;

    goto/16 :goto_0

    .line 2676
    :pswitch_f
    sget-object v3, Lcom/squareup/protos/client/CreatorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/CreatorDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Builder;->creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/bills/Tender$Builder;

    goto/16 :goto_0

    .line 2675
    :pswitch_10
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_last_refundable_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Tender$Builder;

    goto/16 :goto_0

    .line 2674
    :pswitch_11
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_receipt_number(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$Builder;

    goto/16 :goto_0

    .line 2673
    :pswitch_12
    sget-object v3, Lcom/squareup/protos/client/Buyer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/Buyer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_buyer_profile(Lcom/squareup/protos/client/Buyer;)Lcom/squareup/protos/client/bills/Tender$Builder;

    goto/16 :goto_0

    .line 2672
    :pswitch_13
    sget-object v3, Lcom/squareup/protos/client/GeoLocation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/GeoLocation;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Builder;->write_only_location(Lcom/squareup/protos/client/GeoLocation;)Lcom/squareup/protos/client/bills/Tender$Builder;

    goto/16 :goto_0

    .line 2671
    :pswitch_14
    sget-object v3, Lcom/squareup/protos/client/bills/Tender$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Tender$Amounts;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Builder;->amounts(Lcom/squareup/protos/client/bills/Tender$Amounts;)Lcom/squareup/protos/client/bills/Tender$Builder;

    goto/16 :goto_0

    .line 2670
    :pswitch_15
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Builder;->tendered_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Tender$Builder;

    goto/16 :goto_0

    .line 2669
    :pswitch_16
    sget-object v3, Lcom/squareup/protos/client/bills/Tender$Method;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Tender$Method;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Builder;->method(Lcom/squareup/protos/client/bills/Tender$Method;)Lcom/squareup/protos/client/bills/Tender$Builder;

    goto/16 :goto_0

    .line 2663
    :pswitch_17
    :try_start_1
    sget-object v4, Lcom/squareup/protos/client/bills/Tender$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/Tender$State;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_state(Lcom/squareup/protos/client/bills/Tender$State;)Lcom/squareup/protos/client/bills/Tender$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v4

    .line 2665
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bills/Tender$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 2655
    :pswitch_18
    :try_start_2
    sget-object v4, Lcom/squareup/protos/client/bills/Tender$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/Tender$Type;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bills/Tender$Builder;->type(Lcom/squareup/protos/client/bills/Tender$Type;)Lcom/squareup/protos/client/bills/Tender$Builder;
    :try_end_2
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v4

    .line 2657
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bills/Tender$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 2652
    :pswitch_19
    sget-object v3, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Builder;->tender_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Tender$Builder;

    goto/16 :goto_0

    .line 2703
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Tender$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 2704
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$Builder;->build()Lcom/squareup/protos/client/bills/Tender;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_0
        :pswitch_12
        :pswitch_0
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_0
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2581
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Tender$ProtoAdapter_Tender;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Tender;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Tender;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2618
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2619
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender;->type:Lcom/squareup/protos/client/bills/Tender$Type;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2620
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender;->read_only_state:Lcom/squareup/protos/client/bills/Tender$State;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2621
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$Method;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2622
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender;->tendered_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2623
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2624
    sget-object v0, Lcom/squareup/protos/client/GeoLocation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender;->write_only_location:Lcom/squareup/protos/client/GeoLocation;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2625
    sget-object v0, Lcom/squareup/protos/client/Buyer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender;->read_only_buyer_profile:Lcom/squareup/protos/client/Buyer;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2626
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender;->read_only_receipt_number:Ljava/lang/String;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2627
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender;->read_only_last_refundable_at:Lcom/squareup/protos/client/ISO8601Date;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2628
    sget-object v0, Lcom/squareup/protos/client/CreatorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2629
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender;->write_only_client_cash_drawer_shift_id:Ljava/lang/String;

    const/16 v2, 0xe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2630
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    const/16 v2, 0x10

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2631
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender;->read_only_sequential_tender_number:Ljava/lang/String;

    const/16 v2, 0x11

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2632
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender;->read_only_refund_card_presence_requirement:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    const/16 v2, 0x12

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2633
    sget-object v0, Lcom/squareup/protos/client/bills/TenderTag;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender;->tender_tag:Ljava/util/List;

    const/16 v2, 0x13

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2634
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender;->read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    const/16 v2, 0x14

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2635
    sget-object v0, Lcom/squareup/protos/client/bills/ClientDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender;->client_details:Lcom/squareup/protos/client/bills/ClientDetails;

    const/16 v2, 0x15

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2636
    sget-object v0, Lcom/squareup/protos/client/CreatorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender;->open_ticket_owner:Lcom/squareup/protos/client/CreatorDetails;

    const/16 v2, 0x16

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2637
    sget-object v0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender;->peripheral_metadata:Lcom/squareup/protos/common/payment/PeripheralMetadata;

    const/16 v2, 0x17

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2638
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender;->checkout_id:Ljava/lang/String;

    const/16 v2, 0x18

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2639
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender;->reference_id:Ljava/lang/String;

    const/16 v2, 0x19

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2640
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender;->create_payment_note:Ljava/lang/String;

    const/16 v2, 0x1a

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2641
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender;->checkout_app_id:Ljava/lang/String;

    const/16 v2, 0x1b

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2642
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender;->read_only_contact_token:Ljava/lang/String;

    const/16 v2, 0x1c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2643
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Tender;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2581
    check-cast p2, Lcom/squareup/protos/client/bills/Tender;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Tender$ProtoAdapter_Tender;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Tender;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Tender;)I
    .locals 4

    .line 2588
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender;->type:Lcom/squareup/protos/client/bills/Tender$Type;

    const/4 v3, 0x2

    .line 2589
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender;->read_only_state:Lcom/squareup/protos/client/bills/Tender$State;

    const/4 v3, 0x3

    .line 2590
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$Method;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    const/4 v3, 0x4

    .line 2591
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender;->tendered_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x5

    .line 2592
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    const/4 v3, 0x6

    .line 2593
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/GeoLocation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender;->write_only_location:Lcom/squareup/protos/client/GeoLocation;

    const/4 v3, 0x7

    .line 2594
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/Buyer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender;->read_only_buyer_profile:Lcom/squareup/protos/client/Buyer;

    const/16 v3, 0x9

    .line 2595
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender;->read_only_receipt_number:Ljava/lang/String;

    const/16 v3, 0xb

    .line 2596
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender;->read_only_last_refundable_at:Lcom/squareup/protos/client/ISO8601Date;

    const/16 v3, 0xc

    .line 2597
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/CreatorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    const/16 v3, 0xd

    .line 2598
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender;->write_only_client_cash_drawer_shift_id:Ljava/lang/String;

    const/16 v3, 0xe

    .line 2599
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    const/16 v3, 0x10

    .line 2600
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender;->read_only_sequential_tender_number:Ljava/lang/String;

    const/16 v3, 0x11

    .line 2601
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender;->read_only_refund_card_presence_requirement:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    const/16 v3, 0x12

    .line 2602
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/TenderTag;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 2603
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender;->tender_tag:Ljava/util/List;

    const/16 v3, 0x13

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender;->read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    const/16 v3, 0x14

    .line 2604
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/ClientDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender;->client_details:Lcom/squareup/protos/client/bills/ClientDetails;

    const/16 v3, 0x15

    .line 2605
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/CreatorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender;->open_ticket_owner:Lcom/squareup/protos/client/CreatorDetails;

    const/16 v3, 0x16

    .line 2606
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/payment/PeripheralMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender;->peripheral_metadata:Lcom/squareup/protos/common/payment/PeripheralMetadata;

    const/16 v3, 0x17

    .line 2607
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender;->checkout_id:Ljava/lang/String;

    const/16 v3, 0x18

    .line 2608
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender;->reference_id:Ljava/lang/String;

    const/16 v3, 0x19

    .line 2609
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender;->create_payment_note:Ljava/lang/String;

    const/16 v3, 0x1a

    .line 2610
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender;->checkout_app_id:Ljava/lang/String;

    const/16 v3, 0x1b

    .line 2611
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender;->read_only_contact_token:Ljava/lang/String;

    const/16 v3, 0x1c

    .line 2612
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2613
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 2581
    check-cast p1, Lcom/squareup/protos/client/bills/Tender;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Tender$ProtoAdapter_Tender;->encodedSize(Lcom/squareup/protos/client/bills/Tender;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Tender;)Lcom/squareup/protos/client/bills/Tender;
    .locals 2

    .line 2709
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender;->newBuilder()Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object p1

    .line 2710
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/IdPair;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 2711
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bills/Tender$Method;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Tender$Method;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    .line 2712
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->tendered_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->tendered_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->tendered_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 2713
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/client/bills/Tender$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Tender$Amounts;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    .line 2714
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->write_only_location:Lcom/squareup/protos/client/GeoLocation;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/client/GeoLocation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->write_only_location:Lcom/squareup/protos/client/GeoLocation;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/GeoLocation;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->write_only_location:Lcom/squareup/protos/client/GeoLocation;

    .line 2715
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_buyer_profile:Lcom/squareup/protos/client/Buyer;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/client/Buyer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_buyer_profile:Lcom/squareup/protos/client/Buyer;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/Buyer;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_buyer_profile:Lcom/squareup/protos/client/Buyer;

    .line 2716
    :cond_5
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_last_refundable_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_last_refundable_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_last_refundable_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 2717
    :cond_6
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/squareup/protos/client/CreatorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/CreatorDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 2718
    :cond_7
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    if-eqz v0, :cond_8

    sget-object v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    .line 2719
    :cond_8
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->tender_tag:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/bills/TenderTag;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 2720
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    if-eqz v0, :cond_9

    sget-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    .line 2721
    :cond_9
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->client_details:Lcom/squareup/protos/client/bills/ClientDetails;

    if-eqz v0, :cond_a

    sget-object v0, Lcom/squareup/protos/client/bills/ClientDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->client_details:Lcom/squareup/protos/client/bills/ClientDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/ClientDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->client_details:Lcom/squareup/protos/client/bills/ClientDetails;

    .line 2722
    :cond_a
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->open_ticket_owner:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v0, :cond_b

    sget-object v0, Lcom/squareup/protos/client/CreatorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->open_ticket_owner:Lcom/squareup/protos/client/CreatorDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/CreatorDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->open_ticket_owner:Lcom/squareup/protos/client/CreatorDetails;

    .line 2723
    :cond_b
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->peripheral_metadata:Lcom/squareup/protos/common/payment/PeripheralMetadata;

    if-eqz v0, :cond_c

    sget-object v0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->peripheral_metadata:Lcom/squareup/protos/common/payment/PeripheralMetadata;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/payment/PeripheralMetadata;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->peripheral_metadata:Lcom/squareup/protos/common/payment/PeripheralMetadata;

    .line 2724
    :cond_c
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 2725
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$Builder;->build()Lcom/squareup/protos/client/bills/Tender;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 2581
    check-cast p1, Lcom/squareup/protos/client/bills/Tender;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Tender$ProtoAdapter_Tender;->redact(Lcom/squareup/protos/client/bills/Tender;)Lcom/squareup/protos/client/bills/Tender;

    move-result-object p1

    return-object p1
.end method
