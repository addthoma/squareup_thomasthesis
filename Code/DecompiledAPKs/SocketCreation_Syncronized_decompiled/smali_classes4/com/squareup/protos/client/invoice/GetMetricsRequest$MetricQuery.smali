.class public final Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;
.super Lcom/squareup/wire/Message;
.source "GetMetricsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/GetMetricsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MetricQuery"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$ProtoAdapter_MetricQuery;,
        Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;",
        "Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_METRIC_TYPE:Lcom/squareup/protos/client/invoice/MetricType;

.field private static final serialVersionUID:J


# instance fields
.field public final date_range:Lcom/squareup/protos/common/time/DateTimeInterval;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTimeInterval#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final metric_type:Lcom/squareup/protos/client/invoice/MetricType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.MetricType#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 124
    new-instance v0, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$ProtoAdapter_MetricQuery;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$ProtoAdapter_MetricQuery;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 128
    sget-object v0, Lcom/squareup/protos/client/invoice/MetricType;->METRIC_TYPE_DO_NOT_USE:Lcom/squareup/protos/client/invoice/MetricType;

    sput-object v0, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;->DEFAULT_METRIC_TYPE:Lcom/squareup/protos/client/invoice/MetricType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/invoice/MetricType;Lcom/squareup/protos/common/time/DateTimeInterval;)V
    .locals 1

    .line 143
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;-><init>(Lcom/squareup/protos/client/invoice/MetricType;Lcom/squareup/protos/common/time/DateTimeInterval;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/invoice/MetricType;Lcom/squareup/protos/common/time/DateTimeInterval;Lokio/ByteString;)V
    .locals 1

    .line 148
    sget-object v0, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 149
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;->metric_type:Lcom/squareup/protos/client/invoice/MetricType;

    .line 150
    iput-object p2, p0, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 165
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 166
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;

    .line 167
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;->metric_type:Lcom/squareup/protos/client/invoice/MetricType;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;->metric_type:Lcom/squareup/protos/client/invoice/MetricType;

    .line 168
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 169
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 174
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 176
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 177
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;->metric_type:Lcom/squareup/protos/client/invoice/MetricType;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/MetricType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 178
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTimeInterval;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 179
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;
    .locals 2

    .line 155
    new-instance v0, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;-><init>()V

    .line 156
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;->metric_type:Lcom/squareup/protos/client/invoice/MetricType;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;->metric_type:Lcom/squareup/protos/client/invoice/MetricType;

    .line 157
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 158
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 123
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;->newBuilder()Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 186
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 187
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;->metric_type:Lcom/squareup/protos/client/invoice/MetricType;

    if-eqz v1, :cond_0

    const-string v1, ", metric_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;->metric_type:Lcom/squareup/protos/client/invoice/MetricType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 188
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    if-eqz v1, :cond_1

    const-string v1, ", date_range="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "MetricQuery{"

    .line 189
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
