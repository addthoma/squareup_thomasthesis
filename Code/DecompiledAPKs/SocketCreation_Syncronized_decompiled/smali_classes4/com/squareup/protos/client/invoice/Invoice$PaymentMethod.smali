.class public final enum Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;
.super Ljava/lang/Enum;
.source "Invoice.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/Invoice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PaymentMethod"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod$ProtoAdapter_PaymentMethod;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CARD_ON_FILE:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

.field public static final enum EMAIL:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

.field public static final enum SHARE_LINK:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

.field public static final enum SMS:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

.field public static final enum UNKNOWN_METHOD:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

.field public static final enum UNSELECTED:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 678
    new-instance v0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN_METHOD"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->UNKNOWN_METHOD:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    .line 680
    new-instance v0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    const/4 v2, 0x1

    const-string v3, "UNSELECTED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->UNSELECTED:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    .line 682
    new-instance v0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    const/4 v3, 0x2

    const-string v4, "EMAIL"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->EMAIL:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    .line 684
    new-instance v0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    const/4 v4, 0x3

    const-string v5, "SHARE_LINK"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->SHARE_LINK:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    .line 686
    new-instance v0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    const/4 v5, 0x4

    const-string v6, "CARD_ON_FILE"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->CARD_ON_FILE:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    .line 688
    new-instance v0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    const/4 v6, 0x5

    const-string v7, "SMS"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->SMS:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    .line 674
    sget-object v7, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->UNKNOWN_METHOD:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->UNSELECTED:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->EMAIL:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->SHARE_LINK:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->CARD_ON_FILE:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->SMS:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->$VALUES:[Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    .line 690
    new-instance v0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod$ProtoAdapter_PaymentMethod;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod$ProtoAdapter_PaymentMethod;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 694
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 695
    iput p3, p0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;
    .locals 1

    if-eqz p0, :cond_5

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 708
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->SMS:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    return-object p0

    .line 707
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->CARD_ON_FILE:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    return-object p0

    .line 706
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->SHARE_LINK:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    return-object p0

    .line 705
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->EMAIL:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    return-object p0

    .line 704
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->UNSELECTED:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    return-object p0

    .line 703
    :cond_5
    sget-object p0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->UNKNOWN_METHOD:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;
    .locals 1

    .line 674
    const-class v0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;
    .locals 1

    .line 674
    sget-object v0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->$VALUES:[Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 715
    iget v0, p0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->value:I

    return v0
.end method
