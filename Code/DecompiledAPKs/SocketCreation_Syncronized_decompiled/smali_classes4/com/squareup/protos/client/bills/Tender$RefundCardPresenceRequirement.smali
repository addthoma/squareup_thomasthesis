.class public final enum Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;
.super Ljava/lang/Enum;
.source "Tender.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Tender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RefundCardPresenceRequirement"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement$ProtoAdapter_RefundCardPresenceRequirement;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum REFUND_CARD_PRESENCE_NOT_REQUIRED:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

.field public static final enum REFUND_CARD_PRESENCE_REQUIRED:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 1971
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "REFUND_CARD_PRESENCE_NOT_REQUIRED"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;->REFUND_CARD_PRESENCE_NOT_REQUIRED:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    .line 1973
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    const/4 v3, 0x2

    const-string v4, "REFUND_CARD_PRESENCE_REQUIRED"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;->REFUND_CARD_PRESENCE_REQUIRED:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    new-array v0, v3, [Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    .line 1970
    sget-object v3, Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;->REFUND_CARD_PRESENCE_NOT_REQUIRED:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;->REFUND_CARD_PRESENCE_REQUIRED:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;->$VALUES:[Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    .line 1975
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement$ProtoAdapter_RefundCardPresenceRequirement;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement$ProtoAdapter_RefundCardPresenceRequirement;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 1979
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1980
    iput p3, p0, Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 1989
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;->REFUND_CARD_PRESENCE_REQUIRED:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    return-object p0

    .line 1988
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;->REFUND_CARD_PRESENCE_NOT_REQUIRED:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;
    .locals 1

    .line 1970
    const-class v0, Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;
    .locals 1

    .line 1970
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;->$VALUES:[Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 1996
    iget v0, p0, Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;->value:I

    return v0
.end method
