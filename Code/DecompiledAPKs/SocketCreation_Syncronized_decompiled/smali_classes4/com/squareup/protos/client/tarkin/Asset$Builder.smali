.class public final Lcom/squareup/protos/client/tarkin/Asset$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Asset.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/tarkin/Asset;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/tarkin/Asset;",
        "Lcom/squareup/protos/client/tarkin/Asset$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public block_index_table:Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexTable;

.field public encrypted_data:Lokio/ByteString;

.field public header:Lokio/ByteString;

.field public is_blocking:Ljava/lang/Boolean;

.field public requires_reboot:Lcom/squareup/protos/client/tarkin/Asset$Reboot;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 163
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public block_index_table(Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexTable;)Lcom/squareup/protos/client/tarkin/Asset$Builder;
    .locals 0

    .line 197
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/Asset$Builder;->block_index_table:Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexTable;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/tarkin/Asset;
    .locals 8

    .line 213
    new-instance v7, Lcom/squareup/protos/client/tarkin/Asset;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/Asset$Builder;->encrypted_data:Lokio/ByteString;

    iget-object v2, p0, Lcom/squareup/protos/client/tarkin/Asset$Builder;->is_blocking:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/client/tarkin/Asset$Builder;->header:Lokio/ByteString;

    iget-object v4, p0, Lcom/squareup/protos/client/tarkin/Asset$Builder;->block_index_table:Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexTable;

    iget-object v5, p0, Lcom/squareup/protos/client/tarkin/Asset$Builder;->requires_reboot:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/tarkin/Asset;-><init>(Lokio/ByteString;Ljava/lang/Boolean;Lokio/ByteString;Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexTable;Lcom/squareup/protos/client/tarkin/Asset$Reboot;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 152
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/Asset$Builder;->build()Lcom/squareup/protos/client/tarkin/Asset;

    move-result-object v0

    return-object v0
.end method

.method public encrypted_data(Lokio/ByteString;)Lcom/squareup/protos/client/tarkin/Asset$Builder;
    .locals 0

    .line 170
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/Asset$Builder;->encrypted_data:Lokio/ByteString;

    return-object p0
.end method

.method public header(Lokio/ByteString;)Lcom/squareup/protos/client/tarkin/Asset$Builder;
    .locals 0

    .line 186
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/Asset$Builder;->header:Lokio/ByteString;

    return-object p0
.end method

.method public is_blocking(Ljava/lang/Boolean;)Lcom/squareup/protos/client/tarkin/Asset$Builder;
    .locals 0

    .line 178
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/Asset$Builder;->is_blocking:Ljava/lang/Boolean;

    return-object p0
.end method

.method public requires_reboot(Lcom/squareup/protos/client/tarkin/Asset$Reboot;)Lcom/squareup/protos/client/tarkin/Asset$Builder;
    .locals 0

    .line 207
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/Asset$Builder;->requires_reboot:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    return-object p0
.end method
