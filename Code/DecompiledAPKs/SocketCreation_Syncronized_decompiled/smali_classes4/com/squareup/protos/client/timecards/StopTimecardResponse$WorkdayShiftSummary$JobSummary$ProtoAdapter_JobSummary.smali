.class final Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$ProtoAdapter_JobSummary;
.super Lcom/squareup/wire/ProtoAdapter;
.source "StopTimecardResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_JobSummary"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 448
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 473
    new-instance v0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;-><init>()V

    .line 474
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 475
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 483
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 481
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;->timecard_notes(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;

    goto :goto_0

    .line 480
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;->paid_seconds(Ljava/lang/Long;)Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;

    goto :goto_0

    .line 479
    :cond_2
    sget-object v3, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;->stop_zoned_date_time(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;

    goto :goto_0

    .line 478
    :cond_3
    sget-object v3, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;->start_zoned_date_time(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;

    goto :goto_0

    .line 477
    :cond_4
    sget-object v3, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;->job_info(Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;)Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;

    goto :goto_0

    .line 487
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 488
    invoke-virtual {v0}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;->build()Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 446
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$ProtoAdapter_JobSummary;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 463
    sget-object v0, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 464
    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->start_zoned_date_time:Lcom/squareup/protos/common/time/DateTime;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 465
    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->stop_zoned_date_time:Lcom/squareup/protos/common/time/DateTime;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 466
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->paid_seconds:Ljava/lang/Long;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 467
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->timecard_notes:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 468
    invoke-virtual {p2}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 446
    check-cast p2, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$ProtoAdapter_JobSummary;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;)I
    .locals 4

    .line 453
    sget-object v0, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->start_zoned_date_time:Lcom/squareup/protos/common/time/DateTime;

    const/4 v3, 0x2

    .line 454
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->stop_zoned_date_time:Lcom/squareup/protos/common/time/DateTime;

    const/4 v3, 0x3

    .line 455
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->paid_seconds:Ljava/lang/Long;

    const/4 v3, 0x4

    .line 456
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->timecard_notes:Ljava/lang/String;

    const/4 v3, 0x5

    .line 457
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 458
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 446
    check-cast p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$ProtoAdapter_JobSummary;->encodedSize(Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;)Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;
    .locals 2

    .line 493
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->newBuilder()Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;

    move-result-object p1

    .line 494
    iget-object v0, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;->job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;->job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    iput-object v0, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;->job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    .line 495
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;->start_zoned_date_time:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;->start_zoned_date_time:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/DateTime;

    iput-object v0, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;->start_zoned_date_time:Lcom/squareup/protos/common/time/DateTime;

    .line 496
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;->stop_zoned_date_time:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;->stop_zoned_date_time:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/DateTime;

    iput-object v0, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;->stop_zoned_date_time:Lcom/squareup/protos/common/time/DateTime;

    .line 497
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 498
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$Builder;->build()Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 446
    check-cast p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary$ProtoAdapter_JobSummary;->redact(Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;)Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;

    move-result-object p1

    return-object p1
.end method
