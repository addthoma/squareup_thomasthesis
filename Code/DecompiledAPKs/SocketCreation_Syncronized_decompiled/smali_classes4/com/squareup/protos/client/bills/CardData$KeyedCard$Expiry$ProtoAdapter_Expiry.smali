.class final Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$ProtoAdapter_Expiry;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CardData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Expiry"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 668
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 687
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;-><init>()V

    .line 688
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 689
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 694
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 692
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;->year(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;

    goto :goto_0

    .line 691
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;->month(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;

    goto :goto_0

    .line 698
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 699
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;->build()Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 666
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$ProtoAdapter_Expiry;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 680
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;->month:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 681
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;->year:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 682
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 666
    check-cast p2, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$ProtoAdapter_Expiry;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;)I
    .locals 4

    .line 673
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;->month:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;->year:Ljava/lang/String;

    const/4 v3, 0x2

    .line 674
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 675
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 666
    check-cast p1, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$ProtoAdapter_Expiry;->encodedSize(Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;
    .locals 0

    .line 704
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;->newBuilder()Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;

    move-result-object p1

    .line 705
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 706
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;->build()Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 666
    check-cast p1, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$ProtoAdapter_Expiry;->redact(Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    move-result-object p1

    return-object p1
.end method
