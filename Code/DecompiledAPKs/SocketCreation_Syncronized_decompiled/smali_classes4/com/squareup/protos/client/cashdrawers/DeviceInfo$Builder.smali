.class public final Lcom/squareup/protos/client/cashdrawers/DeviceInfo$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DeviceInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/cashdrawers/DeviceInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/cashdrawers/DeviceInfo;",
        "Lcom/squareup/protos/client/cashdrawers/DeviceInfo$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public device_id:Ljava/lang/String;

.field public device_name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 94
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/cashdrawers/DeviceInfo;
    .locals 4

    .line 109
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/DeviceInfo$Builder;->device_name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/cashdrawers/DeviceInfo$Builder;->device_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 89
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/DeviceInfo$Builder;->build()Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    move-result-object v0

    return-object v0
.end method

.method public device_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/DeviceInfo$Builder;
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/DeviceInfo$Builder;->device_id:Ljava/lang/String;

    return-object p0
.end method

.method public device_name(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/DeviceInfo$Builder;
    .locals 0

    .line 98
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/DeviceInfo$Builder;->device_name:Ljava/lang/String;

    return-object p0
.end method
