.class public final Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;
.super Lcom/squareup/wire/Message;
.source "UnitMetadataDisplayDetails.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$ProtoAdapter_UnitMetadataDisplayDetails;,
        Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;,
        Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;,
        Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;",
        "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_HAS_ESTIMATES:Ljava/lang/Boolean;

.field public static final DEFAULT_HAS_INVOICES:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final analytics:Lcom/squareup/protos/client/invoice/UnitAnalytics;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.UnitAnalytics#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final has_estimates:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final has_invoices:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final invoice_automatic_reminder_settings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final limits:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final unit_metadata:Lcom/squareup/protos/client/invoice/UnitMetadata;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.UnitMetadata#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$ProtoAdapter_UnitMetadataDisplayDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$ProtoAdapter_UnitMetadataDisplayDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 30
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->DEFAULT_HAS_INVOICES:Ljava/lang/Boolean;

    .line 32
    sput-object v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->DEFAULT_HAS_ESTIMATES:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/invoice/UnitMetadata;Ljava/lang/Boolean;Lcom/squareup/protos/client/invoice/UnitAnalytics;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;Ljava/lang/Boolean;)V
    .locals 8

    .line 91
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;-><init>(Lcom/squareup/protos/client/invoice/UnitMetadata;Ljava/lang/Boolean;Lcom/squareup/protos/client/invoice/UnitAnalytics;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/invoice/UnitMetadata;Ljava/lang/Boolean;Lcom/squareup/protos/client/invoice/UnitAnalytics;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 98
    sget-object v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 99
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->unit_metadata:Lcom/squareup/protos/client/invoice/UnitMetadata;

    .line 100
    iput-object p2, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->has_invoices:Ljava/lang/Boolean;

    .line 101
    iput-object p3, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->analytics:Lcom/squareup/protos/client/invoice/UnitAnalytics;

    .line 102
    iput-object p4, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->limits:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    .line 103
    iput-object p5, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->invoice_automatic_reminder_settings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    .line 104
    iput-object p6, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->has_estimates:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 123
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 124
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    .line 125
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->unit_metadata:Lcom/squareup/protos/client/invoice/UnitMetadata;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->unit_metadata:Lcom/squareup/protos/client/invoice/UnitMetadata;

    .line 126
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->has_invoices:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->has_invoices:Ljava/lang/Boolean;

    .line 127
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->analytics:Lcom/squareup/protos/client/invoice/UnitAnalytics;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->analytics:Lcom/squareup/protos/client/invoice/UnitAnalytics;

    .line 128
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->limits:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->limits:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    .line 129
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->invoice_automatic_reminder_settings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->invoice_automatic_reminder_settings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    .line 130
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->has_estimates:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->has_estimates:Ljava/lang/Boolean;

    .line 131
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 136
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 138
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 139
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->unit_metadata:Lcom/squareup/protos/client/invoice/UnitMetadata;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/UnitMetadata;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 140
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->has_invoices:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 141
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->analytics:Lcom/squareup/protos/client/invoice/UnitAnalytics;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/UnitAnalytics;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->limits:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->invoice_automatic_reminder_settings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->has_estimates:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 145
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;
    .locals 2

    .line 109
    new-instance v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;-><init>()V

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->unit_metadata:Lcom/squareup/protos/client/invoice/UnitMetadata;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->unit_metadata:Lcom/squareup/protos/client/invoice/UnitMetadata;

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->has_invoices:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->has_invoices:Ljava/lang/Boolean;

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->analytics:Lcom/squareup/protos/client/invoice/UnitAnalytics;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->analytics:Lcom/squareup/protos/client/invoice/UnitAnalytics;

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->limits:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->limits:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->invoice_automatic_reminder_settings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->invoice_automatic_reminder_settings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->has_estimates:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->has_estimates:Ljava/lang/Boolean;

    .line 116
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->newBuilder()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 152
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 153
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->unit_metadata:Lcom/squareup/protos/client/invoice/UnitMetadata;

    if-eqz v1, :cond_0

    const-string v1, ", unit_metadata="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->unit_metadata:Lcom/squareup/protos/client/invoice/UnitMetadata;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 154
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->has_invoices:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", has_invoices="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->has_invoices:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 155
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->analytics:Lcom/squareup/protos/client/invoice/UnitAnalytics;

    if-eqz v1, :cond_2

    const-string v1, ", analytics="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->analytics:Lcom/squareup/protos/client/invoice/UnitAnalytics;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 156
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->limits:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    if-eqz v1, :cond_3

    const-string v1, ", limits="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->limits:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 157
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->invoice_automatic_reminder_settings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    if-eqz v1, :cond_4

    const-string v1, ", invoice_automatic_reminder_settings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->invoice_automatic_reminder_settings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 158
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->has_estimates:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", has_estimates="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->has_estimates:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "UnitMetadataDisplayDetails{"

    .line 159
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
