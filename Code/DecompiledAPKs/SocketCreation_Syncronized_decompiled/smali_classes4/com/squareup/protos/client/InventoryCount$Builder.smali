.class public final Lcom/squareup/protos/client/InventoryCount$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InventoryCount.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/InventoryCount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/InventoryCount;",
        "Lcom/squareup/protos/client/InventoryCount$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public catalog_version:Ljava/lang/Long;

.field public current_count:Ljava/lang/Long;

.field public current_quantity_decimal:Ljava/lang/String;

.field public state:Lcom/squareup/protos/client/State;

.field public unit_token:Ljava/lang/String;

.field public variation_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 172
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/InventoryCount;
    .locals 9

    .line 218
    new-instance v8, Lcom/squareup/protos/client/InventoryCount;

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryCount$Builder;->unit_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/InventoryCount$Builder;->variation_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/InventoryCount$Builder;->state:Lcom/squareup/protos/client/State;

    iget-object v4, p0, Lcom/squareup/protos/client/InventoryCount$Builder;->catalog_version:Ljava/lang/Long;

    iget-object v5, p0, Lcom/squareup/protos/client/InventoryCount$Builder;->current_count:Ljava/lang/Long;

    iget-object v6, p0, Lcom/squareup/protos/client/InventoryCount$Builder;->current_quantity_decimal:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/InventoryCount;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/State;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 159
    invoke-virtual {p0}, Lcom/squareup/protos/client/InventoryCount$Builder;->build()Lcom/squareup/protos/client/InventoryCount;

    move-result-object v0

    return-object v0
.end method

.method public catalog_version(Ljava/lang/Long;)Lcom/squareup/protos/client/InventoryCount$Builder;
    .locals 0

    .line 194
    iput-object p1, p0, Lcom/squareup/protos/client/InventoryCount$Builder;->catalog_version:Ljava/lang/Long;

    return-object p0
.end method

.method public current_count(Ljava/lang/Long;)Lcom/squareup/protos/client/InventoryCount$Builder;
    .locals 0

    .line 202
    iput-object p1, p0, Lcom/squareup/protos/client/InventoryCount$Builder;->current_count:Ljava/lang/Long;

    return-object p0
.end method

.method public current_quantity_decimal(Ljava/lang/String;)Lcom/squareup/protos/client/InventoryCount$Builder;
    .locals 0

    .line 212
    iput-object p1, p0, Lcom/squareup/protos/client/InventoryCount$Builder;->current_quantity_decimal:Ljava/lang/String;

    return-object p0
.end method

.method public state(Lcom/squareup/protos/client/State;)Lcom/squareup/protos/client/InventoryCount$Builder;
    .locals 0

    .line 186
    iput-object p1, p0, Lcom/squareup/protos/client/InventoryCount$Builder;->state:Lcom/squareup/protos/client/State;

    return-object p0
.end method

.method public unit_token(Ljava/lang/String;)Lcom/squareup/protos/client/InventoryCount$Builder;
    .locals 0

    .line 176
    iput-object p1, p0, Lcom/squareup/protos/client/InventoryCount$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method

.method public variation_token(Ljava/lang/String;)Lcom/squareup/protos/client/InventoryCount$Builder;
    .locals 0

    .line 181
    iput-object p1, p0, Lcom/squareup/protos/client/InventoryCount$Builder;->variation_token:Ljava/lang/String;

    return-object p0
.end method
