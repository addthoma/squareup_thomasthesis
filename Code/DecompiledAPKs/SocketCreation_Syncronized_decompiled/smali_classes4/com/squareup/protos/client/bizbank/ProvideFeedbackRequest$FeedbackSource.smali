.class public final enum Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;
.super Ljava/lang/Enum;
.source "ProvideFeedbackRequest.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FeedbackSource"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource$ProtoAdapter_FeedbackSource;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum BALANCE_DEACTIVATION:Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;

.field public static final enum CARD_DEACTIVATION:Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;

.field public static final enum DO_NOT_USE:Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 138
    new-instance v0, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;->DO_NOT_USE:Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;

    .line 140
    new-instance v0, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;

    const/4 v2, 0x1

    const-string v3, "CARD_DEACTIVATION"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;->CARD_DEACTIVATION:Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;

    .line 142
    new-instance v0, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;

    const/4 v3, 0x2

    const-string v4, "BALANCE_DEACTIVATION"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;->BALANCE_DEACTIVATION:Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;

    .line 137
    sget-object v4, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;->DO_NOT_USE:Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;->CARD_DEACTIVATION:Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;->BALANCE_DEACTIVATION:Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;->$VALUES:[Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;

    .line 144
    new-instance v0, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource$ProtoAdapter_FeedbackSource;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource$ProtoAdapter_FeedbackSource;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 148
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 149
    iput p3, p0, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 159
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;->BALANCE_DEACTIVATION:Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;

    return-object p0

    .line 158
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;->CARD_DEACTIVATION:Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;

    return-object p0

    .line 157
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;->DO_NOT_USE:Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;
    .locals 1

    .line 137
    const-class v0, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;
    .locals 1

    .line 137
    sget-object v0, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;->$VALUES:[Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 166
    iget v0, p0, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;->value:I

    return v0
.end method
