.class public final Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListBreaksForOpenTimecardsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsResponse;",
        "Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public pagination_token:Ljava/lang/String;

.field public timecard_break:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/timecards/TimecardBreak;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 99
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 100
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsResponse$Builder;->timecard_break:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsResponse;
    .locals 4

    .line 119
    new-instance v0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsResponse$Builder;->timecard_break:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsResponse$Builder;->pagination_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsResponse;-><init>(Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsResponse$Builder;->build()Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsResponse;

    move-result-object v0

    return-object v0
.end method

.method public pagination_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsResponse$Builder;
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsResponse$Builder;->pagination_token:Ljava/lang/String;

    return-object p0
.end method

.method public timecard_break(Ljava/util/List;)Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/timecards/TimecardBreak;",
            ">;)",
            "Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsResponse$Builder;"
        }
    .end annotation

    .line 104
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 105
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsResponse$Builder;->timecard_break:Ljava/util/List;

    return-object p0
.end method
