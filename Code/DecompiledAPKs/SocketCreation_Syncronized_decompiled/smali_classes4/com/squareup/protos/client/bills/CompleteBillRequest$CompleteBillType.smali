.class public final enum Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;
.super Ljava/lang/Enum;
.source "CompleteBillRequest.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CompleteBillRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CompleteBillType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType$ProtoAdapter_CompleteBillType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum COMPLETE_BILL_CLIENT_INITIATED_TIMEOUT:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

.field public static final enum COMPLETE_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

.field public static final enum COMPLETE_BILL_OTHER:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

.field public static final enum DEFAULT_COMPLETE_BILL_TYPE_DO_NOT_USE:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 352
    new-instance v0, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "DEFAULT_COMPLETE_BILL_TYPE_DO_NOT_USE"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;->DEFAULT_COMPLETE_BILL_TYPE_DO_NOT_USE:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    .line 357
    new-instance v0, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    const/4 v3, 0x2

    const-string v4, "COMPLETE_BILL_HUMAN_INITIATED"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;->COMPLETE_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    .line 362
    new-instance v0, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    const/4 v4, 0x3

    const-string v5, "COMPLETE_BILL_OTHER"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;->COMPLETE_BILL_OTHER:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    .line 367
    new-instance v0, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    const/4 v5, 0x4

    const-string v6, "COMPLETE_BILL_CLIENT_INITIATED_TIMEOUT"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;->COMPLETE_BILL_CLIENT_INITIATED_TIMEOUT:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    new-array v0, v5, [Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    .line 351
    sget-object v5, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;->DEFAULT_COMPLETE_BILL_TYPE_DO_NOT_USE:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;->COMPLETE_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;->COMPLETE_BILL_OTHER:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;->COMPLETE_BILL_CLIENT_INITIATED_TIMEOUT:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;->$VALUES:[Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    .line 369
    new-instance v0, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType$ProtoAdapter_CompleteBillType;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType$ProtoAdapter_CompleteBillType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 373
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 374
    iput p3, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 385
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;->COMPLETE_BILL_CLIENT_INITIATED_TIMEOUT:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    return-object p0

    .line 384
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;->COMPLETE_BILL_OTHER:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    return-object p0

    .line 383
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;->COMPLETE_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    return-object p0

    .line 382
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;->DEFAULT_COMPLETE_BILL_TYPE_DO_NOT_USE:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;
    .locals 1

    .line 351
    const-class v0, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;
    .locals 1

    .line 351
    sget-object v0, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;->$VALUES:[Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 392
    iget v0, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;->value:I

    return v0
.end method
