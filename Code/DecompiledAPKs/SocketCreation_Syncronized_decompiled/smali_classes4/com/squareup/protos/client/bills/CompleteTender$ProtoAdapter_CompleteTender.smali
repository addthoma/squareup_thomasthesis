.class final Lcom/squareup/protos/client/bills/CompleteTender$ProtoAdapter_CompleteTender;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CompleteTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CompleteTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CompleteTender"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/CompleteTender;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 470
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/CompleteTender;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/CompleteTender;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 493
    new-instance v0, Lcom/squareup/protos/client/bills/CompleteTender$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;-><init>()V

    .line 494
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 495
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 502
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 500
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->extra_tender_details(Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;)Lcom/squareup/protos/client/bills/CompleteTender$Builder;

    goto :goto_0

    .line 499
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->complete_details(Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;)Lcom/squareup/protos/client/bills/CompleteTender$Builder;

    goto :goto_0

    .line 498
    :cond_2
    sget-object v3, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->amounts(Lcom/squareup/protos/client/bills/CompleteTender$Amounts;)Lcom/squareup/protos/client/bills/CompleteTender$Builder;

    goto :goto_0

    .line 497
    :cond_3
    sget-object v3, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->tender_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/CompleteTender$Builder;

    goto :goto_0

    .line 506
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 507
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->build()Lcom/squareup/protos/client/bills/CompleteTender;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 468
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CompleteTender$ProtoAdapter_CompleteTender;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/CompleteTender;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/CompleteTender;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 484
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CompleteTender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 485
    sget-object v0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CompleteTender;->amounts:Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 486
    sget-object v0, Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CompleteTender;->complete_details:Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 487
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CompleteTender;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 488
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/CompleteTender;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 468
    check-cast p2, Lcom/squareup/protos/client/bills/CompleteTender;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/CompleteTender$ProtoAdapter_CompleteTender;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/CompleteTender;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/CompleteTender;)I
    .locals 4

    .line 475
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CompleteTender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CompleteTender;->amounts:Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    const/4 v3, 0x2

    .line 476
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CompleteTender;->complete_details:Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;

    const/4 v3, 0x3

    .line 477
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CompleteTender;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    const/4 v3, 0x4

    .line 478
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 479
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CompleteTender;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 468
    check-cast p1, Lcom/squareup/protos/client/bills/CompleteTender;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CompleteTender$ProtoAdapter_CompleteTender;->encodedSize(Lcom/squareup/protos/client/bills/CompleteTender;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/CompleteTender;)Lcom/squareup/protos/client/bills/CompleteTender;
    .locals 2

    .line 512
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CompleteTender;->newBuilder()Lcom/squareup/protos/client/bills/CompleteTender$Builder;

    move-result-object p1

    .line 513
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/IdPair;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 514
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->amounts:Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->amounts:Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->amounts:Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    .line 515
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->complete_details:Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->complete_details:Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->complete_details:Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;

    .line 516
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    .line 517
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 518
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->build()Lcom/squareup/protos/client/bills/CompleteTender;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 468
    check-cast p1, Lcom/squareup/protos/client/bills/CompleteTender;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CompleteTender$ProtoAdapter_CompleteTender;->redact(Lcom/squareup/protos/client/bills/CompleteTender;)Lcom/squareup/protos/client/bills/CompleteTender;

    move-result-object p1

    return-object p1
.end method
