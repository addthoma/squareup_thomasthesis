.class public final Lcom/squareup/protos/client/InventoryCount;
.super Lcom/squareup/wire/Message;
.source "InventoryCount.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/InventoryCount$ProtoAdapter_InventoryCount;,
        Lcom/squareup/protos/client/InventoryCount$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/InventoryCount;",
        "Lcom/squareup/protos/client/InventoryCount$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/InventoryCount;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CATALOG_VERSION:Ljava/lang/Long;

.field public static final DEFAULT_CURRENT_COUNT:Ljava/lang/Long;

.field public static final DEFAULT_CURRENT_QUANTITY_DECIMAL:Ljava/lang/String; = ""

.field public static final DEFAULT_STATE:Lcom/squareup/protos/client/State;

.field public static final DEFAULT_UNIT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_VARIATION_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final catalog_version:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x5
    .end annotation
.end field

.field public final current_count:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x4
    .end annotation
.end field

.field public final current_quantity_decimal:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final state:Lcom/squareup/protos/client/State;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.State#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final unit_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final variation_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 24
    new-instance v0, Lcom/squareup/protos/client/InventoryCount$ProtoAdapter_InventoryCount;

    invoke-direct {v0}, Lcom/squareup/protos/client/InventoryCount$ProtoAdapter_InventoryCount;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/InventoryCount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 32
    sget-object v0, Lcom/squareup/protos/client/State;->DO_NOT_USE_STATE:Lcom/squareup/protos/client/State;

    sput-object v0, Lcom/squareup/protos/client/InventoryCount;->DEFAULT_STATE:Lcom/squareup/protos/client/State;

    const-wide/16 v0, 0x0

    .line 34
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/InventoryCount;->DEFAULT_CATALOG_VERSION:Ljava/lang/Long;

    .line 36
    sput-object v0, Lcom/squareup/protos/client/InventoryCount;->DEFAULT_CURRENT_COUNT:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/State;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;)V
    .locals 8

    .line 89
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/InventoryCount;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/State;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/State;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 95
    sget-object v0, Lcom/squareup/protos/client/InventoryCount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 96
    iput-object p1, p0, Lcom/squareup/protos/client/InventoryCount;->unit_token:Ljava/lang/String;

    .line 97
    iput-object p2, p0, Lcom/squareup/protos/client/InventoryCount;->variation_token:Ljava/lang/String;

    .line 98
    iput-object p3, p0, Lcom/squareup/protos/client/InventoryCount;->state:Lcom/squareup/protos/client/State;

    .line 99
    iput-object p4, p0, Lcom/squareup/protos/client/InventoryCount;->catalog_version:Ljava/lang/Long;

    .line 100
    iput-object p5, p0, Lcom/squareup/protos/client/InventoryCount;->current_count:Ljava/lang/Long;

    .line 101
    iput-object p6, p0, Lcom/squareup/protos/client/InventoryCount;->current_quantity_decimal:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 120
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/InventoryCount;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 121
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/InventoryCount;

    .line 122
    invoke-virtual {p0}, Lcom/squareup/protos/client/InventoryCount;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/InventoryCount;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryCount;->unit_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/InventoryCount;->unit_token:Ljava/lang/String;

    .line 123
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryCount;->variation_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/InventoryCount;->variation_token:Ljava/lang/String;

    .line 124
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryCount;->state:Lcom/squareup/protos/client/State;

    iget-object v3, p1, Lcom/squareup/protos/client/InventoryCount;->state:Lcom/squareup/protos/client/State;

    .line 125
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryCount;->catalog_version:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/InventoryCount;->catalog_version:Ljava/lang/Long;

    .line 126
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryCount;->current_count:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/InventoryCount;->current_count:Ljava/lang/Long;

    .line 127
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryCount;->current_quantity_decimal:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/InventoryCount;->current_quantity_decimal:Ljava/lang/String;

    .line 128
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 133
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 135
    invoke-virtual {p0}, Lcom/squareup/protos/client/InventoryCount;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 136
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryCount;->unit_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 137
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryCount;->variation_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 138
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryCount;->state:Lcom/squareup/protos/client/State;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/State;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 139
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryCount;->catalog_version:Ljava/lang/Long;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 140
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryCount;->current_count:Ljava/lang/Long;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 141
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryCount;->current_quantity_decimal:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 142
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/InventoryCount$Builder;
    .locals 2

    .line 106
    new-instance v0, Lcom/squareup/protos/client/InventoryCount$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/InventoryCount$Builder;-><init>()V

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryCount;->unit_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/InventoryCount$Builder;->unit_token:Ljava/lang/String;

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryCount;->variation_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/InventoryCount$Builder;->variation_token:Ljava/lang/String;

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryCount;->state:Lcom/squareup/protos/client/State;

    iput-object v1, v0, Lcom/squareup/protos/client/InventoryCount$Builder;->state:Lcom/squareup/protos/client/State;

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryCount;->catalog_version:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/InventoryCount$Builder;->catalog_version:Ljava/lang/Long;

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryCount;->current_count:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/InventoryCount$Builder;->current_count:Ljava/lang/Long;

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryCount;->current_quantity_decimal:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/InventoryCount$Builder;->current_quantity_decimal:Ljava/lang/String;

    .line 113
    invoke-virtual {p0}, Lcom/squareup/protos/client/InventoryCount;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/InventoryCount$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/client/InventoryCount;->newBuilder()Lcom/squareup/protos/client/InventoryCount$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 149
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 150
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryCount;->unit_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", unit_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryCount;->unit_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryCount;->variation_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", variation_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryCount;->variation_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryCount;->state:Lcom/squareup/protos/client/State;

    if-eqz v1, :cond_2

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryCount;->state:Lcom/squareup/protos/client/State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 153
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryCount;->catalog_version:Ljava/lang/Long;

    if-eqz v1, :cond_3

    const-string v1, ", catalog_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryCount;->catalog_version:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 154
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryCount;->current_count:Ljava/lang/Long;

    if-eqz v1, :cond_4

    const-string v1, ", current_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryCount;->current_count:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 155
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryCount;->current_quantity_decimal:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", current_quantity_decimal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryCount;->current_quantity_decimal:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "InventoryCount{"

    .line 156
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
