.class public final Lcom/squareup/protos/client/rolodex/QueryContext$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "QueryContext.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/QueryContext;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/QueryContext;",
        "Lcom/squareup/protos/client/rolodex/QueryContext$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public automatic:Ljava/lang/Boolean;

.field public filters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation
.end field

.field public full_email_address:Ljava/lang/String;

.field public full_phone_number:Ljava/lang/String;

.field public group_token:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public group_v2_token:Ljava/lang/String;

.field public limit:Ljava/lang/Integer;

.field public paging_key:Ljava/lang/String;

.field public payment_token:Ljava/lang/String;

.field public query:Ljava/lang/String;

.field public sort_type:Lcom/squareup/protos/client/rolodex/ListContactsSortType;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 272
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 273
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->group_token:Ljava/util/List;

    .line 274
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->filters:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public automatic(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/QueryContext$Builder;
    .locals 0

    .line 330
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->automatic:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/rolodex/QueryContext;
    .locals 14

    .line 372
    new-instance v13, Lcom/squareup/protos/client/rolodex/QueryContext;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->limit:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->paging_key:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->query:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->group_token:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->sort_type:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    iget-object v6, p0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->full_phone_number:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->automatic:Ljava/lang/Boolean;

    iget-object v8, p0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->full_email_address:Ljava/lang/String;

    iget-object v9, p0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->filters:Ljava/util/List;

    iget-object v10, p0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->group_v2_token:Ljava/lang/String;

    iget-object v11, p0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->payment_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v12

    move-object v0, v13

    invoke-direct/range {v0 .. v12}, Lcom/squareup/protos/client/rolodex/QueryContext;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/rolodex/ListContactsSortType;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v13
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 249
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->build()Lcom/squareup/protos/client/rolodex/QueryContext;

    move-result-object v0

    return-object v0
.end method

.method public filters(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/QueryContext$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/QueryContext$Builder;"
        }
    .end annotation

    .line 346
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 347
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->filters:Ljava/util/List;

    return-object p0
.end method

.method public full_email_address(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/QueryContext$Builder;
    .locals 0

    .line 338
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->full_email_address:Ljava/lang/String;

    return-object p0
.end method

.method public full_phone_number(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/QueryContext$Builder;
    .locals 0

    .line 322
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->full_phone_number:Ljava/lang/String;

    return-object p0
.end method

.method public group_token(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/QueryContext$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/QueryContext$Builder;"
        }
    .end annotation

    .line 305
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 306
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->group_token:Ljava/util/List;

    return-object p0
.end method

.method public group_v2_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/QueryContext$Builder;
    .locals 0

    .line 358
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->group_v2_token:Ljava/lang/String;

    return-object p0
.end method

.method public limit(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/QueryContext$Builder;
    .locals 0

    .line 281
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->limit:Ljava/lang/Integer;

    return-object p0
.end method

.method public paging_key(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/QueryContext$Builder;
    .locals 0

    .line 289
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->paging_key:Ljava/lang/String;

    return-object p0
.end method

.method public payment_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/QueryContext$Builder;
    .locals 0

    .line 366
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->payment_token:Ljava/lang/String;

    return-object p0
.end method

.method public query(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/QueryContext$Builder;
    .locals 0

    .line 297
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->query:Ljava/lang/String;

    return-object p0
.end method

.method public sort_type(Lcom/squareup/protos/client/rolodex/ListContactsSortType;)Lcom/squareup/protos/client/rolodex/QueryContext$Builder;
    .locals 0

    .line 314
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->sort_type:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    return-object p0
.end method
