.class public final Lcom/squareup/protos/client/bills/Tender$Method$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Tender$Method;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Tender$Method;",
        "Lcom/squareup/protos/client/bills/Tender$Method$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public card_tender:Lcom/squareup/protos/client/bills/CardTender;

.field public cash_tender:Lcom/squareup/protos/client/bills/CashTender;

.field public external_tender:Lcom/squareup/protos/client/bills/ExternalTender;

.field public no_sale_tender:Lcom/squareup/protos/client/bills/NoSaleTender;

.field public other_tender:Lcom/squareup/protos/client/bills/OtherTender;

.field public read_only_generic_tender:Lcom/squareup/protos/client/bills/GenericTender;

.field public square_capital_tender:Lcom/squareup/protos/client/bills/SquareCapitalTender;

.field public wallet_tender:Lcom/squareup/protos/client/bills/WalletTender;

.field public zero_amount_tender:Lcom/squareup/protos/client/bills/ZeroAmountTender;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1037
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Tender$Method;
    .locals 12

    .line 1087
    new-instance v11, Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->cash_tender:Lcom/squareup/protos/client/bills/CashTender;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->no_sale_tender:Lcom/squareup/protos/client/bills/NoSaleTender;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->other_tender:Lcom/squareup/protos/client/bills/OtherTender;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->wallet_tender:Lcom/squareup/protos/client/bills/WalletTender;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->read_only_generic_tender:Lcom/squareup/protos/client/bills/GenericTender;

    iget-object v7, p0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->zero_amount_tender:Lcom/squareup/protos/client/bills/ZeroAmountTender;

    iget-object v8, p0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->external_tender:Lcom/squareup/protos/client/bills/ExternalTender;

    iget-object v9, p0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->square_capital_tender:Lcom/squareup/protos/client/bills/SquareCapitalTender;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/client/bills/Tender$Method;-><init>(Lcom/squareup/protos/client/bills/CardTender;Lcom/squareup/protos/client/bills/CashTender;Lcom/squareup/protos/client/bills/NoSaleTender;Lcom/squareup/protos/client/bills/OtherTender;Lcom/squareup/protos/client/bills/WalletTender;Lcom/squareup/protos/client/bills/GenericTender;Lcom/squareup/protos/client/bills/ZeroAmountTender;Lcom/squareup/protos/client/bills/ExternalTender;Lcom/squareup/protos/client/bills/SquareCapitalTender;Lokio/ByteString;)V

    return-object v11
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1018
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->build()Lcom/squareup/protos/client/bills/Tender$Method;

    move-result-object v0

    return-object v0
.end method

.method public card_tender(Lcom/squareup/protos/client/bills/CardTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;
    .locals 0

    .line 1041
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    return-object p0
.end method

.method public cash_tender(Lcom/squareup/protos/client/bills/CashTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;
    .locals 0

    .line 1046
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->cash_tender:Lcom/squareup/protos/client/bills/CashTender;

    return-object p0
.end method

.method public external_tender(Lcom/squareup/protos/client/bills/ExternalTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;
    .locals 0

    .line 1076
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->external_tender:Lcom/squareup/protos/client/bills/ExternalTender;

    return-object p0
.end method

.method public no_sale_tender(Lcom/squareup/protos/client/bills/NoSaleTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;
    .locals 0

    .line 1051
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->no_sale_tender:Lcom/squareup/protos/client/bills/NoSaleTender;

    return-object p0
.end method

.method public other_tender(Lcom/squareup/protos/client/bills/OtherTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;
    .locals 0

    .line 1056
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->other_tender:Lcom/squareup/protos/client/bills/OtherTender;

    return-object p0
.end method

.method public read_only_generic_tender(Lcom/squareup/protos/client/bills/GenericTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;
    .locals 0

    .line 1066
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->read_only_generic_tender:Lcom/squareup/protos/client/bills/GenericTender;

    return-object p0
.end method

.method public square_capital_tender(Lcom/squareup/protos/client/bills/SquareCapitalTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;
    .locals 0

    .line 1081
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->square_capital_tender:Lcom/squareup/protos/client/bills/SquareCapitalTender;

    return-object p0
.end method

.method public wallet_tender(Lcom/squareup/protos/client/bills/WalletTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;
    .locals 0

    .line 1061
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->wallet_tender:Lcom/squareup/protos/client/bills/WalletTender;

    return-object p0
.end method

.method public zero_amount_tender(Lcom/squareup/protos/client/bills/ZeroAmountTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;
    .locals 0

    .line 1071
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->zero_amount_tender:Lcom/squareup/protos/client/bills/ZeroAmountTender;

    return-object p0
.end method
