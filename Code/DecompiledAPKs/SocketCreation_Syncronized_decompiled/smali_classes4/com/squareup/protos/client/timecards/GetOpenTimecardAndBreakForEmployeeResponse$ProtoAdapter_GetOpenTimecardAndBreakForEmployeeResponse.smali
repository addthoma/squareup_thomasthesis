.class final Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$ProtoAdapter_GetOpenTimecardAndBreakForEmployeeResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetOpenTimecardAndBreakForEmployeeResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GetOpenTimecardAndBreakForEmployeeResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 179
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 204
    new-instance v0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;-><init>()V

    .line 205
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 206
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 213
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 211
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;->clocked_in_current_workday(Ljava/lang/Boolean;)Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;

    goto :goto_0

    .line 210
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;->total_seconds_clocked_in_for_current_workday(Ljava/lang/Long;)Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;

    goto :goto_0

    .line 209
    :cond_2
    sget-object v3, Lcom/squareup/protos/client/timecards/TimecardBreak;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/timecards/TimecardBreak;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;->timecard_break(Lcom/squareup/protos/client/timecards/TimecardBreak;)Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;

    goto :goto_0

    .line 208
    :cond_3
    sget-object v3, Lcom/squareup/protos/client/timecards/Timecard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/timecards/Timecard;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;->timecard(Lcom/squareup/protos/client/timecards/Timecard;)Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;

    goto :goto_0

    .line 217
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 218
    invoke-virtual {v0}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;->build()Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 177
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$ProtoAdapter_GetOpenTimecardAndBreakForEmployeeResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 194
    sget-object v0, Lcom/squareup/protos/client/timecards/Timecard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 195
    sget-object v0, Lcom/squareup/protos/client/timecards/TimecardBreak;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 196
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->total_seconds_clocked_in_for_current_workday:Ljava/lang/Long;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 197
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->clocked_in_current_workday:Ljava/lang/Boolean;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 198
    invoke-virtual {p2}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 177
    check-cast p2, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$ProtoAdapter_GetOpenTimecardAndBreakForEmployeeResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;)I
    .locals 4

    .line 184
    sget-object v0, Lcom/squareup/protos/client/timecards/Timecard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/timecards/TimecardBreak;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    const/4 v3, 0x2

    .line 185
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->total_seconds_clocked_in_for_current_workday:Ljava/lang/Long;

    const/4 v3, 0x4

    .line 186
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->clocked_in_current_workday:Ljava/lang/Boolean;

    const/4 v3, 0x5

    .line 187
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 188
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 177
    check-cast p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$ProtoAdapter_GetOpenTimecardAndBreakForEmployeeResponse;->encodedSize(Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;)Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;
    .locals 2

    .line 224
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->newBuilder()Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;

    move-result-object p1

    .line 225
    iget-object v0, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/timecards/Timecard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/timecards/Timecard;

    iput-object v0, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    .line 226
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/timecards/TimecardBreak;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/timecards/TimecardBreak;

    iput-object v0, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    .line 227
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 228
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;->build()Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 177
    check-cast p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$ProtoAdapter_GetOpenTimecardAndBreakForEmployeeResponse;->redact(Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;)Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;

    move-result-object p1

    return-object p1
.end method
