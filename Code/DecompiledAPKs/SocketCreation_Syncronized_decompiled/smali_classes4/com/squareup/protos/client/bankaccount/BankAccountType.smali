.class public final enum Lcom/squareup/protos/client/bankaccount/BankAccountType;
.super Ljava/lang/Enum;
.source "BankAccountType.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bankaccount/BankAccountType$ProtoAdapter_BankAccountType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bankaccount/BankAccountType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bankaccount/BankAccountType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bankaccount/BankAccountType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum BANK_ACCOUNT_TYPE_DO_NOT_USE:Lcom/squareup/protos/client/bankaccount/BankAccountType;

.field public static final enum BUSINESS_CHECKING:Lcom/squareup/protos/client/bankaccount/BankAccountType;

.field public static final enum CHECKING:Lcom/squareup/protos/client/bankaccount/BankAccountType;

.field public static final enum OTHER:Lcom/squareup/protos/client/bankaccount/BankAccountType;

.field public static final enum SAVINGS:Lcom/squareup/protos/client/bankaccount/BankAccountType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 17
    new-instance v0, Lcom/squareup/protos/client/bankaccount/BankAccountType;

    const/4 v1, 0x0

    const-string v2, "BANK_ACCOUNT_TYPE_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bankaccount/BankAccountType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bankaccount/BankAccountType;->BANK_ACCOUNT_TYPE_DO_NOT_USE:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    .line 23
    new-instance v0, Lcom/squareup/protos/client/bankaccount/BankAccountType;

    const/4 v2, 0x1

    const-string v3, "CHECKING"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bankaccount/BankAccountType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bankaccount/BankAccountType;->CHECKING:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    .line 28
    new-instance v0, Lcom/squareup/protos/client/bankaccount/BankAccountType;

    const/4 v3, 0x2

    const-string v4, "BUSINESS_CHECKING"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bankaccount/BankAccountType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bankaccount/BankAccountType;->BUSINESS_CHECKING:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    .line 34
    new-instance v0, Lcom/squareup/protos/client/bankaccount/BankAccountType;

    const/4 v4, 0x3

    const-string v5, "SAVINGS"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/bankaccount/BankAccountType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bankaccount/BankAccountType;->SAVINGS:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    .line 39
    new-instance v0, Lcom/squareup/protos/client/bankaccount/BankAccountType;

    const/4 v5, 0x4

    const-string v6, "OTHER"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/bankaccount/BankAccountType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bankaccount/BankAccountType;->OTHER:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/protos/client/bankaccount/BankAccountType;

    .line 13
    sget-object v6, Lcom/squareup/protos/client/bankaccount/BankAccountType;->BANK_ACCOUNT_TYPE_DO_NOT_USE:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bankaccount/BankAccountType;->CHECKING:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bankaccount/BankAccountType;->BUSINESS_CHECKING:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bankaccount/BankAccountType;->SAVINGS:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/bankaccount/BankAccountType;->OTHER:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/protos/client/bankaccount/BankAccountType;->$VALUES:[Lcom/squareup/protos/client/bankaccount/BankAccountType;

    .line 41
    new-instance v0, Lcom/squareup/protos/client/bankaccount/BankAccountType$ProtoAdapter_BankAccountType;

    invoke-direct {v0}, Lcom/squareup/protos/client/bankaccount/BankAccountType$ProtoAdapter_BankAccountType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bankaccount/BankAccountType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 46
    iput p3, p0, Lcom/squareup/protos/client/bankaccount/BankAccountType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bankaccount/BankAccountType;
    .locals 1

    if-eqz p0, :cond_4

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 58
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bankaccount/BankAccountType;->OTHER:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    return-object p0

    .line 57
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bankaccount/BankAccountType;->SAVINGS:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    return-object p0

    .line 56
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bankaccount/BankAccountType;->BUSINESS_CHECKING:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    return-object p0

    .line 55
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/bankaccount/BankAccountType;->CHECKING:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    return-object p0

    .line 54
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/bankaccount/BankAccountType;->BANK_ACCOUNT_TYPE_DO_NOT_USE:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/BankAccountType;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/protos/client/bankaccount/BankAccountType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bankaccount/BankAccountType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bankaccount/BankAccountType;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/protos/client/bankaccount/BankAccountType;->$VALUES:[Lcom/squareup/protos/client/bankaccount/BankAccountType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bankaccount/BankAccountType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bankaccount/BankAccountType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 65
    iget v0, p0, Lcom/squareup/protos/client/bankaccount/BankAccountType;->value:I

    return v0
.end method
