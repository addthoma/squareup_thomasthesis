.class public final Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PutResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse;",
        "Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public object:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 89
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 90
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse$Builder;->object:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse;
    .locals 3

    .line 101
    new-instance v0, Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse$Builder;->object:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 86
    invoke-virtual {p0}, Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse$Builder;->build()Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse;

    move-result-object v0

    return-object v0
.end method

.method public object(Ljava/util/List;)Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;",
            ">;)",
            "Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse$Builder;"
        }
    .end annotation

    .line 94
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 95
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse$Builder;->object:Ljava/util/List;

    return-object p0
.end method
