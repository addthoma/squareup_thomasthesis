.class public final Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetUnitSettingsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;",
        "Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public invoice_defaults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceDefaults;",
            ">;"
        }
    .end annotation
.end field

.field public status:Lcom/squareup/protos/client/Status;

.field public unit_settings:Lcom/squareup/protos/client/invoice/UnitSettings;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 114
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 115
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;->invoice_defaults:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;
    .locals 5

    .line 142
    new-instance v0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;->unit_settings:Lcom/squareup/protos/client/invoice/UnitSettings;

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;->invoice_defaults:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/invoice/UnitSettings;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 107
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;->build()Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;

    move-result-object v0

    return-object v0
.end method

.method public invoice_defaults(Ljava/util/List;)Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceDefaults;",
            ">;)",
            "Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;"
        }
    .end annotation

    .line 135
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 136
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;->invoice_defaults:Ljava/util/List;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method

.method public unit_settings(Lcom/squareup/protos/client/invoice/UnitSettings;)Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;->unit_settings:Lcom/squareup/protos/client/invoice/UnitSettings;

    return-object p0
.end method
