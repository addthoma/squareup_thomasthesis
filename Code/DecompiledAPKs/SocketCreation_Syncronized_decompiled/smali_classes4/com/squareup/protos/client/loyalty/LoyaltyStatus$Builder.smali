.class public final Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LoyaltyStatus.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/LoyaltyStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/LoyaltyStatus;",
        "Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public balance:Ljava/lang/Integer;

.field public current_points:Ljava/lang/Long;

.field public early_computed_reason:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

.field public earned_points:Ljava/lang/Long;

.field public earned_reward:Ljava/lang/Boolean;

.field public lifetime_points:Ljava/lang/Long;

.field public new_rewards_earned:Ljava/lang/Integer;

.field public new_stars_earned:Ljava/lang/Long;

.field public obfuscated_phone_number:Ljava/lang/String;

.field public phone_token:Ljava/lang/String;

.field public previous_stars_earned:Ljava/lang/Long;

.field public previous_stars_remaining:Ljava/lang/Long;

.field public reward_name:Ljava/lang/String;

.field public reward_tiers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/RewardTier;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 347
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 348
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->reward_tiers:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public balance(Ljava/lang/Integer;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;
    .locals 0

    .line 475
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->balance:Ljava/lang/Integer;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/loyalty/LoyaltyStatus;
    .locals 18

    move-object/from16 v0, p0

    .line 481
    new-instance v17, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    iget-object v2, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->previous_stars_earned:Ljava/lang/Long;

    iget-object v3, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->previous_stars_remaining:Ljava/lang/Long;

    iget-object v4, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->new_stars_earned:Ljava/lang/Long;

    iget-object v5, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->earned_reward:Ljava/lang/Boolean;

    iget-object v6, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->reward_name:Ljava/lang/String;

    iget-object v7, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->new_rewards_earned:Ljava/lang/Integer;

    iget-object v8, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->obfuscated_phone_number:Ljava/lang/String;

    iget-object v9, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->phone_token:Ljava/lang/String;

    iget-object v10, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->current_points:Ljava/lang/Long;

    iget-object v11, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->earned_points:Ljava/lang/Long;

    iget-object v12, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->reward_tiers:Ljava/util/List;

    iget-object v13, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->lifetime_points:Ljava/lang/Long;

    iget-object v14, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->early_computed_reason:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    iget-object v15, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->balance:Ljava/lang/Integer;

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v16

    move-object/from16 v1, v17

    invoke-direct/range {v1 .. v16}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;-><init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/util/List;Ljava/lang/Long;Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v17
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 318
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->build()Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    move-result-object v0

    return-object v0
.end method

.method public current_points(Ljava/lang/Long;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;
    .locals 0

    .line 426
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->current_points:Ljava/lang/Long;

    return-object p0
.end method

.method public early_computed_reason(Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;
    .locals 0

    .line 467
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->early_computed_reason:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    return-object p0
.end method

.method public earned_points(Ljava/lang/Long;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;
    .locals 0

    .line 435
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->earned_points:Ljava/lang/Long;

    return-object p0
.end method

.method public earned_reward(Ljava/lang/Boolean;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;
    .locals 0

    .line 384
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->earned_reward:Ljava/lang/Boolean;

    return-object p0
.end method

.method public lifetime_points(Ljava/lang/Long;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;
    .locals 0

    .line 454
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->lifetime_points:Ljava/lang/Long;

    return-object p0
.end method

.method public new_rewards_earned(Ljava/lang/Integer;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;
    .locals 0

    .line 402
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->new_rewards_earned:Ljava/lang/Integer;

    return-object p0
.end method

.method public new_stars_earned(Ljava/lang/Long;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;
    .locals 0

    .line 375
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->new_stars_earned:Ljava/lang/Long;

    return-object p0
.end method

.method public obfuscated_phone_number(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;
    .locals 0

    .line 410
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->obfuscated_phone_number:Ljava/lang/String;

    return-object p0
.end method

.method public phone_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;
    .locals 0

    .line 418
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->phone_token:Ljava/lang/String;

    return-object p0
.end method

.method public previous_stars_earned(Ljava/lang/Long;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;
    .locals 0

    .line 356
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->previous_stars_earned:Ljava/lang/Long;

    return-object p0
.end method

.method public previous_stars_remaining(Ljava/lang/Long;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;
    .locals 0

    .line 365
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->previous_stars_remaining:Ljava/lang/Long;

    return-object p0
.end method

.method public reward_name(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;
    .locals 0

    .line 393
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->reward_name:Ljava/lang/String;

    return-object p0
.end method

.method public reward_tiers(Ljava/util/List;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/RewardTier;",
            ">;)",
            "Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;"
        }
    .end annotation

    .line 444
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 445
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->reward_tiers:Ljava/util/List;

    return-object p0
.end method
