.class public final Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;
.super Lcom/squareup/wire/Message;
.source "AssetUpdateResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$ProtoAdapter_AssetUpdateResponse;,
        Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;,
        Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;",
        "Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_APP_UPDATE:Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;

.field private static final serialVersionUID:J


# instance fields
.field public final app_update:Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.tarkin.AssetUpdateResponse$AppUpdate#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final assets:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.tarkin.Asset#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tarkin/Asset;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$ProtoAdapter_AssetUpdateResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$ProtoAdapter_AssetUpdateResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 27
    sget-object v0, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;->SUGGEST:Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;

    sput-object v0, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;->DEFAULT_APP_UPDATE:Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tarkin/Asset;",
            ">;",
            "Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;",
            ")V"
        }
    .end annotation

    .line 52
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;-><init>(Ljava/util/List;Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tarkin/Asset;",
            ">;",
            "Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 56
    sget-object v0, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p3, "assets"

    .line 57
    invoke-static {p3, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;->assets:Ljava/util/List;

    .line 58
    iput-object p2, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;->app_update:Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 73
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 74
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;

    .line 75
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;->assets:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;->assets:Ljava/util/List;

    .line 76
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;->app_update:Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;

    iget-object p1, p1, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;->app_update:Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;

    .line 77
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 82
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 84
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;->assets:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;->app_update:Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 87
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$Builder;
    .locals 2

    .line 63
    new-instance v0, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$Builder;-><init>()V

    .line 64
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;->assets:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$Builder;->assets:Ljava/util/List;

    .line 65
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;->app_update:Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$Builder;->app_update:Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;

    .line 66
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;->newBuilder()Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 95
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;->assets:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", assets="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;->assets:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 96
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;->app_update:Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;

    if-eqz v1, :cond_1

    const-string v1, ", app_update="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;->app_update:Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AssetUpdateResponse{"

    .line 97
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
