.class public final Lcom/squareup/protos/client/onboard/Step$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Step.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/onboard/Step;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/onboard/Step;",
        "Lcom/squareup/protos/client/onboard/Step$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public event:Lcom/squareup/protos/client/onboard/TerminalEvent;

.field public panel:Lcom/squareup/protos/client/onboard/Panel;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 95
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/onboard/Step;
    .locals 4

    .line 112
    new-instance v0, Lcom/squareup/protos/client/onboard/Step;

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/Step$Builder;->panel:Lcom/squareup/protos/client/onboard/Panel;

    iget-object v2, p0, Lcom/squareup/protos/client/onboard/Step$Builder;->event:Lcom/squareup/protos/client/onboard/TerminalEvent;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/onboard/Step;-><init>(Lcom/squareup/protos/client/onboard/Panel;Lcom/squareup/protos/client/onboard/TerminalEvent;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 90
    invoke-virtual {p0}, Lcom/squareup/protos/client/onboard/Step$Builder;->build()Lcom/squareup/protos/client/onboard/Step;

    move-result-object v0

    return-object v0
.end method

.method public event(Lcom/squareup/protos/client/onboard/TerminalEvent;)Lcom/squareup/protos/client/onboard/Step$Builder;
    .locals 0

    .line 105
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Step$Builder;->event:Lcom/squareup/protos/client/onboard/TerminalEvent;

    const/4 p1, 0x0

    .line 106
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Step$Builder;->panel:Lcom/squareup/protos/client/onboard/Panel;

    return-object p0
.end method

.method public panel(Lcom/squareup/protos/client/onboard/Panel;)Lcom/squareup/protos/client/onboard/Step$Builder;
    .locals 0

    .line 99
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Step$Builder;->panel:Lcom/squareup/protos/client/onboard/Panel;

    const/4 p1, 0x0

    .line 100
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Step$Builder;->event:Lcom/squareup/protos/client/onboard/TerminalEvent;

    return-object p0
.end method
