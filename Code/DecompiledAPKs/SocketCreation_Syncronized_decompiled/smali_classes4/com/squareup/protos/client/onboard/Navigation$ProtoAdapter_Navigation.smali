.class final Lcom/squareup/protos/client/onboard/Navigation$ProtoAdapter_Navigation;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Navigation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/onboard/Navigation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Navigation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/onboard/Navigation;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 180
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/onboard/Navigation;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/onboard/Navigation;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 205
    new-instance v0, Lcom/squareup/protos/client/onboard/Navigation$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/onboard/Navigation$Builder;-><init>()V

    .line 206
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 207
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 215
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 213
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/onboard/Navigation$Builder;->timeout_action(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Navigation$Builder;

    goto :goto_0

    .line 212
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/onboard/Navigation$Builder;->timeout_duration(Ljava/lang/Integer;)Lcom/squareup/protos/client/onboard/Navigation$Builder;

    goto :goto_0

    .line 211
    :cond_2
    sget-object v3, Lcom/squareup/protos/client/onboard/Dialog;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/onboard/Dialog;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/onboard/Navigation$Builder;->exit_dialog(Lcom/squareup/protos/client/onboard/Dialog;)Lcom/squareup/protos/client/onboard/Navigation$Builder;

    goto :goto_0

    .line 210
    :cond_3
    iget-object v3, v0, Lcom/squareup/protos/client/onboard/Navigation$Builder;->navigation_buttons:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/onboard/NavigationButton;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 209
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/onboard/Navigation$Builder;->can_go_back(Ljava/lang/Boolean;)Lcom/squareup/protos/client/onboard/Navigation$Builder;

    goto :goto_0

    .line 219
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/onboard/Navigation$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 220
    invoke-virtual {v0}, Lcom/squareup/protos/client/onboard/Navigation$Builder;->build()Lcom/squareup/protos/client/onboard/Navigation;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 178
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/onboard/Navigation$ProtoAdapter_Navigation;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/onboard/Navigation;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/onboard/Navigation;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 195
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/onboard/Navigation;->can_go_back:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 196
    sget-object v0, Lcom/squareup/protos/client/onboard/NavigationButton;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/onboard/Navigation;->navigation_buttons:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 197
    sget-object v0, Lcom/squareup/protos/client/onboard/Dialog;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/onboard/Navigation;->exit_dialog:Lcom/squareup/protos/client/onboard/Dialog;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 198
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/onboard/Navigation;->timeout_duration:Ljava/lang/Integer;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 199
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/onboard/Navigation;->timeout_action:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 200
    invoke-virtual {p2}, Lcom/squareup/protos/client/onboard/Navigation;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 178
    check-cast p2, Lcom/squareup/protos/client/onboard/Navigation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/onboard/Navigation$ProtoAdapter_Navigation;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/onboard/Navigation;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/onboard/Navigation;)I
    .locals 4

    .line 185
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/onboard/Navigation;->can_go_back:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/onboard/NavigationButton;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 186
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/onboard/Navigation;->navigation_buttons:Ljava/util/List;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/onboard/Dialog;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/onboard/Navigation;->exit_dialog:Lcom/squareup/protos/client/onboard/Dialog;

    const/4 v3, 0x3

    .line 187
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/onboard/Navigation;->timeout_duration:Ljava/lang/Integer;

    const/4 v3, 0x4

    .line 188
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/onboard/Navigation;->timeout_action:Ljava/lang/String;

    const/4 v3, 0x5

    .line 189
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 190
    invoke-virtual {p1}, Lcom/squareup/protos/client/onboard/Navigation;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 178
    check-cast p1, Lcom/squareup/protos/client/onboard/Navigation;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/onboard/Navigation$ProtoAdapter_Navigation;->encodedSize(Lcom/squareup/protos/client/onboard/Navigation;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/onboard/Navigation;)Lcom/squareup/protos/client/onboard/Navigation;
    .locals 2

    .line 225
    invoke-virtual {p1}, Lcom/squareup/protos/client/onboard/Navigation;->newBuilder()Lcom/squareup/protos/client/onboard/Navigation$Builder;

    move-result-object p1

    .line 226
    iget-object v0, p1, Lcom/squareup/protos/client/onboard/Navigation$Builder;->navigation_buttons:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/onboard/NavigationButton;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 227
    iget-object v0, p1, Lcom/squareup/protos/client/onboard/Navigation$Builder;->exit_dialog:Lcom/squareup/protos/client/onboard/Dialog;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/onboard/Dialog;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/onboard/Navigation$Builder;->exit_dialog:Lcom/squareup/protos/client/onboard/Dialog;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/onboard/Dialog;

    iput-object v0, p1, Lcom/squareup/protos/client/onboard/Navigation$Builder;->exit_dialog:Lcom/squareup/protos/client/onboard/Dialog;

    .line 228
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/onboard/Navigation$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 229
    invoke-virtual {p1}, Lcom/squareup/protos/client/onboard/Navigation$Builder;->build()Lcom/squareup/protos/client/onboard/Navigation;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 178
    check-cast p1, Lcom/squareup/protos/client/onboard/Navigation;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/onboard/Navigation$ProtoAdapter_Navigation;->redact(Lcom/squareup/protos/client/onboard/Navigation;)Lcom/squareup/protos/client/onboard/Navigation;

    move-result-object p1

    return-object p1
.end method
