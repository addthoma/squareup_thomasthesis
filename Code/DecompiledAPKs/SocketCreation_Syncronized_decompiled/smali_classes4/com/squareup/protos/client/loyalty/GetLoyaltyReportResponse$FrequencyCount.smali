.class public final Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;
.super Lcom/squareup/wire/Message;
.source "GetLoyaltyReportResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FrequencyCount"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount$ProtoAdapter_FrequencyCount;,
        Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_COUNT:Ljava/lang/Long;

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final count:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0x2
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 309
    new-instance v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount$ProtoAdapter_FrequencyCount;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount$ProtoAdapter_FrequencyCount;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 315
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->DEFAULT_COUNT:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Long;)V
    .locals 1

    .line 330
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;-><init>(Ljava/lang/String;Ljava/lang/Long;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Long;Lokio/ByteString;)V
    .locals 1

    .line 334
    sget-object v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 335
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->name:Ljava/lang/String;

    .line 336
    iput-object p2, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->count:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 351
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 352
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;

    .line 353
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->name:Ljava/lang/String;

    .line 354
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->count:Ljava/lang/Long;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->count:Ljava/lang/Long;

    .line 355
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 360
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 362
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 363
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->name:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 364
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->count:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 365
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount$Builder;
    .locals 2

    .line 341
    new-instance v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount$Builder;-><init>()V

    .line 342
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount$Builder;->name:Ljava/lang/String;

    .line 343
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->count:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount$Builder;->count:Ljava/lang/Long;

    .line 344
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 308
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->newBuilder()Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 372
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 373
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 374
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->count:Ljava/lang/Long;

    if-eqz v1, :cond_1

    const-string v1, ", count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->count:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "FrequencyCount{"

    .line 375
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
