.class final Lcom/squareup/protos/client/bills/CardTender$ProtoAdapter_CardTender;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CardTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CardTender"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/CardTender;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1969
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/CardTender;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/CardTender;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1996
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardTender$Builder;-><init>()V

    .line 1997
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1998
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_6

    const/4 v4, 0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x3

    if-eq v3, v4, :cond_4

    const/4 v4, 0x4

    if-eq v3, v4, :cond_3

    const/4 v4, 0x5

    if-eq v3, v4, :cond_2

    const/4 v4, 0x6

    if-eq v3, v4, :cond_1

    const/4 v4, 0x7

    if-eq v3, v4, :cond_0

    .line 2014
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 2012
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardTender$Builder;->allows_reauthorization_with_changes(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/CardTender$Builder;

    goto :goto_0

    .line 2006
    :cond_1
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/bills/CardTender$AccountType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/CardTender$AccountType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bills/CardTender$Builder;->account_type(Lcom/squareup/protos/client/bills/CardTender$AccountType;)Lcom/squareup/protos/client/bills/CardTender$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 2008
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bills/CardTender$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 2003
    :cond_2
    sget-object v3, Lcom/squareup/protos/client/bills/CardTender$Emv;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/CardTender$Emv;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardTender$Builder;->emv(Lcom/squareup/protos/client/bills/CardTender$Emv;)Lcom/squareup/protos/client/bills/CardTender$Builder;

    goto :goto_0

    .line 2002
    :cond_3
    sget-object v3, Lcom/squareup/protos/client/bills/CardTender$Authorization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/CardTender$Authorization;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardTender$Builder;->read_only_authorization(Lcom/squareup/protos/client/bills/CardTender$Authorization;)Lcom/squareup/protos/client/bills/CardTender$Builder;

    goto :goto_0

    .line 2001
    :cond_4
    sget-object v3, Lcom/squareup/protos/client/bills/CardTender$DelayCapture;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/CardTender$DelayCapture;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardTender$Builder;->read_only_delay_capture(Lcom/squareup/protos/client/bills/CardTender$DelayCapture;)Lcom/squareup/protos/client/bills/CardTender$Builder;

    goto :goto_0

    .line 2000
    :cond_5
    sget-object v3, Lcom/squareup/protos/client/bills/CardTender$Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/CardTender$Card;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardTender$Builder;->card(Lcom/squareup/protos/client/bills/CardTender$Card;)Lcom/squareup/protos/client/bills/CardTender$Builder;

    goto :goto_0

    .line 2018
    :cond_6
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/CardTender$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 2019
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CardTender$Builder;->build()Lcom/squareup/protos/client/bills/CardTender;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1967
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CardTender$ProtoAdapter_CardTender;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/CardTender;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/CardTender;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1985
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1986
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$DelayCapture;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardTender;->read_only_delay_capture:Lcom/squareup/protos/client/bills/CardTender$DelayCapture;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1987
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardTender;->read_only_authorization:Lcom/squareup/protos/client/bills/CardTender$Authorization;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1988
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Emv;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardTender;->emv:Lcom/squareup/protos/client/bills/CardTender$Emv;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1989
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$AccountType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardTender;->account_type:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1990
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardTender;->allows_reauthorization_with_changes:Ljava/lang/Boolean;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1991
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/CardTender;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1967
    check-cast p2, Lcom/squareup/protos/client/bills/CardTender;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/CardTender$ProtoAdapter_CardTender;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/CardTender;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/CardTender;)I
    .locals 4

    .line 1974
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$DelayCapture;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardTender;->read_only_delay_capture:Lcom/squareup/protos/client/bills/CardTender$DelayCapture;

    const/4 v3, 0x3

    .line 1975
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Authorization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardTender;->read_only_authorization:Lcom/squareup/protos/client/bills/CardTender$Authorization;

    const/4 v3, 0x4

    .line 1976
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Emv;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardTender;->emv:Lcom/squareup/protos/client/bills/CardTender$Emv;

    const/4 v3, 0x5

    .line 1977
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$AccountType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardTender;->account_type:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    const/4 v3, 0x6

    .line 1978
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardTender;->allows_reauthorization_with_changes:Ljava/lang/Boolean;

    const/4 v3, 0x7

    .line 1979
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1980
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardTender;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1967
    check-cast p1, Lcom/squareup/protos/client/bills/CardTender;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CardTender$ProtoAdapter_CardTender;->encodedSize(Lcom/squareup/protos/client/bills/CardTender;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/CardTender;)Lcom/squareup/protos/client/bills/CardTender;
    .locals 2

    .line 2024
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardTender;->newBuilder()Lcom/squareup/protos/client/bills/CardTender$Builder;

    move-result-object p1

    .line 2025
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CardTender$Builder;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CardTender$Builder;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/CardTender$Card;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CardTender$Builder;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    .line 2026
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CardTender$Builder;->read_only_delay_capture:Lcom/squareup/protos/client/bills/CardTender$DelayCapture;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$DelayCapture;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CardTender$Builder;->read_only_delay_capture:Lcom/squareup/protos/client/bills/CardTender$DelayCapture;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/CardTender$DelayCapture;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CardTender$Builder;->read_only_delay_capture:Lcom/squareup/protos/client/bills/CardTender$DelayCapture;

    .line 2027
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CardTender$Builder;->read_only_authorization:Lcom/squareup/protos/client/bills/CardTender$Authorization;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CardTender$Builder;->read_only_authorization:Lcom/squareup/protos/client/bills/CardTender$Authorization;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/CardTender$Authorization;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CardTender$Builder;->read_only_authorization:Lcom/squareup/protos/client/bills/CardTender$Authorization;

    .line 2028
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CardTender$Builder;->emv:Lcom/squareup/protos/client/bills/CardTender$Emv;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Emv;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CardTender$Builder;->emv:Lcom/squareup/protos/client/bills/CardTender$Emv;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/CardTender$Emv;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CardTender$Builder;->emv:Lcom/squareup/protos/client/bills/CardTender$Emv;

    .line 2029
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardTender$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 2030
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardTender$Builder;->build()Lcom/squareup/protos/client/bills/CardTender;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1967
    check-cast p1, Lcom/squareup/protos/client/bills/CardTender;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CardTender$ProtoAdapter_CardTender;->redact(Lcom/squareup/protos/client/bills/CardTender;)Lcom/squareup/protos/client/bills/CardTender;

    move-result-object p1

    return-object p1
.end method
