.class public final Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetUnitItemCountsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;",
        "Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public item_count:Ljava/lang/String;

.field public unit:Lcom/squareup/protos/client/retail/inventory/UnitProfile;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 174
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;
    .locals 4

    .line 189
    new-instance v0, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;

    iget-object v1, p0, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount$Builder;->item_count:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount$Builder;->unit:Lcom/squareup/protos/client/retail/inventory/UnitProfile;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/retail/inventory/UnitProfile;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 169
    invoke-virtual {p0}, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount$Builder;->build()Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;

    move-result-object v0

    return-object v0
.end method

.method public item_count(Ljava/lang/String;)Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount$Builder;
    .locals 0

    .line 178
    iput-object p1, p0, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount$Builder;->item_count:Ljava/lang/String;

    return-object p0
.end method

.method public unit(Lcom/squareup/protos/client/retail/inventory/UnitProfile;)Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount$Builder;
    .locals 0

    .line 183
    iput-object p1, p0, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount$Builder;->unit:Lcom/squareup/protos/client/retail/inventory/UnitProfile;

    return-object p0
.end method
