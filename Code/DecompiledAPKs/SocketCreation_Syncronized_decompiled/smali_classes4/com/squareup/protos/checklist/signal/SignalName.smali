.class public final enum Lcom/squareup/protos/checklist/signal/SignalName;
.super Ljava/lang/Enum;
.source "SignalName.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/checklist/signal/SignalName$ProtoAdapter_SignalName;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/checklist/signal/SignalName;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/checklist/signal/SignalName;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ADDED_AUTHORIZED_REPRESENTATIVE:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum ADDED_A_LOCATION:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum ADDED_MOBILE_STAFF:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum ADDED_MORE_NEXT_STEPS:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum ALLOWED_REACTIVATION:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum APPS_WITH_DEVICE_CODE_USAGES:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum BANK_ACCOUNT_LINK_PROGRESS:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum CARDLESS_FREE_TRIAL_FEATURES:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum CARD_PAYMENT_SUCCESS_COUNT:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum COMPLETED_ACTION_ITEMS:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum COMPLETED_PRODUCT_INTERESTS:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum CREATED_AN_ITEM:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum CREATED_BAR_CODE_LABEL:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum CUSTOMIZED_RECEIPTS:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum DISMISSED_ACTION_ITEMS:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum DISMISSED_ACTION_ITEM_TIMES:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum DOWNLOADED_THE_INVOICES_APP:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum ENABLED_ITEM_FOR_SALE:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum ENABLED_TWO_FACTOR_AUTH:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum ENABLE_PICKUP_AND_DELIVERY:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum ENGAGED_CUSTOMERS:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum HAS_A_DEVICE_USAGE:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum HAS_PAID_FOR_FEATURES:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum HAS_RETAIL_APP_USAGE:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum IMPORTED_INVENTORY:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum MANAGED_EMPLOYEES:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum NEXT_STEPS:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum PUBLISHED_ITEM:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum PUBLISHED_MERCHANT_PROFILE:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum RECOMMENDED_PRODUCTS:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum REFERRAL_ACTIVATED:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum REGISTER_ACTIVATION_STATE:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum REGISTER_ACTIVATION_STATE_HISTORY:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum REVIEWED_BUSINESS_DETAILS:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum SAVED_A_MENU_ITEM:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum SAW_SECURE_PROFILE:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum SENT_AN_INVOICE:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum SENT_A_DEVICE_CODE:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum SET_CLOSE_OF_DAY:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum SET_UP_AN_RST_GRID_LAYOUT:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum SET_UP_A_FLOOR_PLAN:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum SET_UP_TAXES:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum SIGNUP_BUSINESS_CATEGORY:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum SIGNUP_BUSINESS_SUB_CATEGORY:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum SIGNUP_EGIFT_CARD:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum SIGNUP_ESTIMATED_EMPLOYEES:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum SIGNUP_ESTIMATED_REVENUE:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum SIGNUP_ESTIMATED_USAGE:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum SIGNUP_VARIANT:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum STARTED_BIG_COMMERCE_RETAIL:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum SUBSCRIBED_FEATURES:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum TOOK_A_CARD_PAYMENT:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum TOOK_A_CASH_PAYMENT:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum TOOK_A_CNP_PAYMENT:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum UPDATED_SECURE_PROFILE:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum VIEWED_BIZ_FULFILLMENT:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum VIEWED_CUSTOMER_FEEDBACK:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum VIEWED_DISCOUNTS:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum VIEWED_INVOICE_SETTINGS:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum VIEWED_ONLINE_STORE_SELECTOR:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum VIEWED_ONLINE_STORE_SETUP:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum VIEWED_OTHER_SERVICES:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum VIEWED_REPORTS:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum VIEWED_SOFTWARE_OVERVIEW:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum VIEWED_STAND_PAGE:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum VIEWED_VENDORS:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum VIEWED_WEEBLY_OST:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum VISITED_SELLER_COMMUNITY:Lcom/squareup/protos/checklist/signal/SignalName;

.field public static final enum VISITED_SQUARE_SHOP:Lcom/squareup/protos/checklist/signal/SignalName;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 17
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const/4 v1, 0x1

    const-string v2, "VIEWED_REPORTS"

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_REPORTS:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 19
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const/4 v2, 0x2

    const-string v3, "TOOK_A_CASH_PAYMENT"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->TOOK_A_CASH_PAYMENT:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 21
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const/4 v3, 0x3

    const-string v4, "TOOK_A_CARD_PAYMENT"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->TOOK_A_CARD_PAYMENT:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 23
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const/4 v4, 0x4

    const-string v5, "SENT_AN_INVOICE"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->SENT_AN_INVOICE:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 25
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const/4 v5, 0x5

    const-string v6, "CREATED_AN_ITEM"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->CREATED_AN_ITEM:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 27
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const/4 v6, 0x6

    const-string v7, "BANK_ACCOUNT_LINK_PROGRESS"

    invoke-direct {v0, v7, v5, v6}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->BANK_ACCOUNT_LINK_PROGRESS:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 29
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const/4 v7, 0x7

    const-string v8, "TOOK_A_CNP_PAYMENT"

    invoke-direct {v0, v8, v6, v7}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->TOOK_A_CNP_PAYMENT:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 31
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v8, 0x8

    const-string v9, "NEXT_STEPS"

    invoke-direct {v0, v9, v7, v8}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->NEXT_STEPS:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 33
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v9, 0x9

    const-string v10, "ALLOWED_REACTIVATION"

    invoke-direct {v0, v10, v8, v9}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->ALLOWED_REACTIVATION:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 35
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v10, 0xa

    const-string v11, "HAS_A_DEVICE_USAGE"

    invoke-direct {v0, v11, v9, v10}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->HAS_A_DEVICE_USAGE:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 37
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v11, 0xb

    const-string v12, "PUBLISHED_MERCHANT_PROFILE"

    invoke-direct {v0, v12, v10, v11}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->PUBLISHED_MERCHANT_PROFILE:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 39
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v12, 0xc

    const-string v13, "VIEWED_STAND_PAGE"

    invoke-direct {v0, v13, v11, v12}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_STAND_PAGE:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 41
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v13, 0xd

    const-string v14, "ADDED_MOBILE_STAFF"

    invoke-direct {v0, v14, v12, v13}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->ADDED_MOBILE_STAFF:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 43
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v14, 0xe

    const-string v15, "ADDED_MORE_NEXT_STEPS"

    invoke-direct {v0, v15, v13, v14}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->ADDED_MORE_NEXT_STEPS:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 45
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v15, 0xf

    const-string v13, "DISMISSED_ACTION_ITEMS"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->DISMISSED_ACTION_ITEMS:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 47
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "CARD_PAYMENT_SUCCESS_COUNT"

    const/16 v14, 0x10

    invoke-direct {v0, v13, v15, v14}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->CARD_PAYMENT_SUCCESS_COUNT:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 49
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "PUBLISHED_ITEM"

    const/16 v15, 0x11

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->PUBLISHED_ITEM:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 51
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "ENABLED_ITEM_FOR_SALE"

    const/16 v14, 0x11

    const/16 v15, 0x12

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->ENABLED_ITEM_FOR_SALE:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 53
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "VIEWED_BIZ_FULFILLMENT"

    const/16 v14, 0x12

    const/16 v15, 0x13

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_BIZ_FULFILLMENT:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 55
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "REFERRAL_ACTIVATED"

    const/16 v14, 0x13

    const/16 v15, 0x14

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->REFERRAL_ACTIVATED:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 57
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "ENABLED_TWO_FACTOR_AUTH"

    const/16 v14, 0x14

    const/16 v15, 0x15

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->ENABLED_TWO_FACTOR_AUTH:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 59
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "SIGNUP_BUSINESS_CATEGORY"

    const/16 v14, 0x15

    const/16 v15, 0x16

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->SIGNUP_BUSINESS_CATEGORY:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 61
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "SIGNUP_BUSINESS_SUB_CATEGORY"

    const/16 v14, 0x16

    const/16 v15, 0x17

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->SIGNUP_BUSINESS_SUB_CATEGORY:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 63
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "SIGNUP_ESTIMATED_REVENUE"

    const/16 v14, 0x17

    const/16 v15, 0x18

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->SIGNUP_ESTIMATED_REVENUE:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 65
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "SIGNUP_ESTIMATED_EMPLOYEES"

    const/16 v14, 0x18

    const/16 v15, 0x19

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->SIGNUP_ESTIMATED_EMPLOYEES:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 67
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "SIGNUP_ESTIMATED_USAGE"

    const/16 v14, 0x19

    const/16 v15, 0x1a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->SIGNUP_ESTIMATED_USAGE:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 69
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "DISMISSED_ACTION_ITEM_TIMES"

    const/16 v14, 0x1a

    const/16 v15, 0x1b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->DISMISSED_ACTION_ITEM_TIMES:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 71
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "SENT_A_DEVICE_CODE"

    const/16 v14, 0x1b

    const/16 v15, 0x1c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->SENT_A_DEVICE_CODE:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 73
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "REVIEWED_BUSINESS_DETAILS"

    const/16 v14, 0x1c

    const/16 v15, 0x1d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->REVIEWED_BUSINESS_DETAILS:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 75
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "ADDED_A_LOCATION"

    const/16 v14, 0x1d

    const/16 v15, 0x1e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->ADDED_A_LOCATION:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 77
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "MANAGED_EMPLOYEES"

    const/16 v14, 0x1e

    const/16 v15, 0x1f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->MANAGED_EMPLOYEES:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 79
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "VIEWED_CUSTOMER_FEEDBACK"

    const/16 v14, 0x1f

    const/16 v15, 0x20

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_CUSTOMER_FEEDBACK:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 81
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "CUSTOMIZED_RECEIPTS"

    const/16 v14, 0x20

    const/16 v15, 0x21

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->CUSTOMIZED_RECEIPTS:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 83
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "VIEWED_OTHER_SERVICES"

    const/16 v14, 0x21

    const/16 v15, 0x22

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_OTHER_SERVICES:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 85
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "ENGAGED_CUSTOMERS"

    const/16 v14, 0x22

    const/16 v15, 0x23

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->ENGAGED_CUSTOMERS:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 87
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "VISITED_SELLER_COMMUNITY"

    const/16 v14, 0x23

    const/16 v15, 0x24

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->VISITED_SELLER_COMMUNITY:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 89
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "VISITED_SQUARE_SHOP"

    const/16 v14, 0x24

    const/16 v15, 0x25

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->VISITED_SQUARE_SHOP:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 91
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "RECOMMENDED_PRODUCTS"

    const/16 v14, 0x25

    const/16 v15, 0x26

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->RECOMMENDED_PRODUCTS:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 93
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "SUBSCRIBED_FEATURES"

    const/16 v14, 0x26

    const/16 v15, 0x27

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->SUBSCRIBED_FEATURES:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 95
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "CARDLESS_FREE_TRIAL_FEATURES"

    const/16 v14, 0x27

    const/16 v15, 0x28

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->CARDLESS_FREE_TRIAL_FEATURES:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 97
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "COMPLETED_PRODUCT_INTERESTS"

    const/16 v14, 0x28

    const/16 v15, 0x29

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->COMPLETED_PRODUCT_INTERESTS:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 99
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "SET_CLOSE_OF_DAY"

    const/16 v14, 0x29

    const/16 v15, 0x2a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->SET_CLOSE_OF_DAY:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 101
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "HAS_RETAIL_APP_USAGE"

    const/16 v14, 0x2a

    const/16 v15, 0x2b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->HAS_RETAIL_APP_USAGE:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 103
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "HAS_PAID_FOR_FEATURES"

    const/16 v14, 0x2b

    const/16 v15, 0x2c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->HAS_PAID_FOR_FEATURES:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 105
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "REGISTER_ACTIVATION_STATE_HISTORY"

    const/16 v14, 0x2c

    const/16 v15, 0x2d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->REGISTER_ACTIVATION_STATE_HISTORY:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 107
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "REGISTER_ACTIVATION_STATE"

    const/16 v14, 0x2d

    const/16 v15, 0x2e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->REGISTER_ACTIVATION_STATE:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 109
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "SAW_SECURE_PROFILE"

    const/16 v14, 0x2e

    const/16 v15, 0x2f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->SAW_SECURE_PROFILE:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 111
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "SET_UP_AN_RST_GRID_LAYOUT"

    const/16 v14, 0x2f

    const/16 v15, 0x30

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->SET_UP_AN_RST_GRID_LAYOUT:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 113
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "SET_UP_A_FLOOR_PLAN"

    const/16 v14, 0x30

    const/16 v15, 0x31

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->SET_UP_A_FLOOR_PLAN:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 115
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "SET_UP_TAXES"

    const/16 v14, 0x31

    const/16 v15, 0x32

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->SET_UP_TAXES:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 117
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "SAVED_A_MENU_ITEM"

    const/16 v14, 0x32

    const/16 v15, 0x33

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->SAVED_A_MENU_ITEM:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 119
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "COMPLETED_ACTION_ITEMS"

    const/16 v14, 0x33

    const/16 v15, 0x34

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->COMPLETED_ACTION_ITEMS:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 121
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "APPS_WITH_DEVICE_CODE_USAGES"

    const/16 v14, 0x34

    const/16 v15, 0x35

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->APPS_WITH_DEVICE_CODE_USAGES:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 123
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "SIGNUP_VARIANT"

    const/16 v14, 0x35

    const/16 v15, 0x36

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->SIGNUP_VARIANT:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 125
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "SIGNUP_EGIFT_CARD"

    const/16 v14, 0x36

    const/16 v15, 0x37

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->SIGNUP_EGIFT_CARD:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 127
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "VIEWED_SOFTWARE_OVERVIEW"

    const/16 v14, 0x37

    const/16 v15, 0x38

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_SOFTWARE_OVERVIEW:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 129
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "UPDATED_SECURE_PROFILE"

    const/16 v14, 0x38

    const/16 v15, 0x39

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->UPDATED_SECURE_PROFILE:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 131
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "ADDED_AUTHORIZED_REPRESENTATIVE"

    const/16 v14, 0x39

    const/16 v15, 0x3a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->ADDED_AUTHORIZED_REPRESENTATIVE:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 133
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "VIEWED_DISCOUNTS"

    const/16 v14, 0x3a

    const/16 v15, 0x3b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_DISCOUNTS:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 135
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "VIEWED_VENDORS"

    const/16 v14, 0x3b

    const/16 v15, 0x3c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_VENDORS:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 137
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "VIEWED_WEEBLY_OST"

    const/16 v14, 0x3c

    const/16 v15, 0x3d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_WEEBLY_OST:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 139
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "VIEWED_ONLINE_STORE_SETUP"

    const/16 v14, 0x3d

    const/16 v15, 0x3e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_ONLINE_STORE_SETUP:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 141
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "VIEWED_ONLINE_STORE_SELECTOR"

    const/16 v14, 0x3e

    const/16 v15, 0x3f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_ONLINE_STORE_SELECTOR:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 143
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "STARTED_BIG_COMMERCE_RETAIL"

    const/16 v14, 0x3f

    const/16 v15, 0x40

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->STARTED_BIG_COMMERCE_RETAIL:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 145
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "VIEWED_INVOICE_SETTINGS"

    const/16 v14, 0x40

    const/16 v15, 0x41

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_INVOICE_SETTINGS:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 147
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "DOWNLOADED_THE_INVOICES_APP"

    const/16 v14, 0x41

    const/16 v15, 0x42

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->DOWNLOADED_THE_INVOICES_APP:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 149
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "IMPORTED_INVENTORY"

    const/16 v14, 0x42

    const/16 v15, 0x43

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->IMPORTED_INVENTORY:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 151
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "CREATED_BAR_CODE_LABEL"

    const/16 v14, 0x43

    const/16 v15, 0x44

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->CREATED_BAR_CODE_LABEL:Lcom/squareup/protos/checklist/signal/SignalName;

    .line 153
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName;

    const-string v13, "ENABLE_PICKUP_AND_DELIVERY"

    const/16 v14, 0x44

    const/16 v15, 0x45

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/signal/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->ENABLE_PICKUP_AND_DELIVERY:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v0, 0x45

    new-array v0, v0, [Lcom/squareup/protos/checklist/signal/SignalName;

    .line 10
    sget-object v13, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_REPORTS:Lcom/squareup/protos/checklist/signal/SignalName;

    const/4 v14, 0x0

    aput-object v13, v0, v14

    sget-object v13, Lcom/squareup/protos/checklist/signal/SignalName;->TOOK_A_CASH_PAYMENT:Lcom/squareup/protos/checklist/signal/SignalName;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->TOOK_A_CARD_PAYMENT:Lcom/squareup/protos/checklist/signal/SignalName;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->SENT_AN_INVOICE:Lcom/squareup/protos/checklist/signal/SignalName;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->CREATED_AN_ITEM:Lcom/squareup/protos/checklist/signal/SignalName;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->BANK_ACCOUNT_LINK_PROGRESS:Lcom/squareup/protos/checklist/signal/SignalName;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->TOOK_A_CNP_PAYMENT:Lcom/squareup/protos/checklist/signal/SignalName;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->NEXT_STEPS:Lcom/squareup/protos/checklist/signal/SignalName;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->ALLOWED_REACTIVATION:Lcom/squareup/protos/checklist/signal/SignalName;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->HAS_A_DEVICE_USAGE:Lcom/squareup/protos/checklist/signal/SignalName;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->PUBLISHED_MERCHANT_PROFILE:Lcom/squareup/protos/checklist/signal/SignalName;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_STAND_PAGE:Lcom/squareup/protos/checklist/signal/SignalName;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->ADDED_MOBILE_STAFF:Lcom/squareup/protos/checklist/signal/SignalName;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->ADDED_MORE_NEXT_STEPS:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->DISMISSED_ACTION_ITEMS:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->CARD_PAYMENT_SUCCESS_COUNT:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->PUBLISHED_ITEM:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->ENABLED_ITEM_FOR_SALE:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_BIZ_FULFILLMENT:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->REFERRAL_ACTIVATED:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->ENABLED_TWO_FACTOR_AUTH:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->SIGNUP_BUSINESS_CATEGORY:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->SIGNUP_BUSINESS_SUB_CATEGORY:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->SIGNUP_ESTIMATED_REVENUE:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->SIGNUP_ESTIMATED_EMPLOYEES:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->SIGNUP_ESTIMATED_USAGE:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->DISMISSED_ACTION_ITEM_TIMES:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->SENT_A_DEVICE_CODE:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->REVIEWED_BUSINESS_DETAILS:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->ADDED_A_LOCATION:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->MANAGED_EMPLOYEES:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_CUSTOMER_FEEDBACK:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->CUSTOMIZED_RECEIPTS:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_OTHER_SERVICES:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->ENGAGED_CUSTOMERS:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->VISITED_SELLER_COMMUNITY:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->VISITED_SQUARE_SHOP:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->RECOMMENDED_PRODUCTS:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->SUBSCRIBED_FEATURES:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->CARDLESS_FREE_TRIAL_FEATURES:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->COMPLETED_PRODUCT_INTERESTS:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->SET_CLOSE_OF_DAY:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->HAS_RETAIL_APP_USAGE:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->HAS_PAID_FOR_FEATURES:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->REGISTER_ACTIVATION_STATE_HISTORY:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->REGISTER_ACTIVATION_STATE:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->SAW_SECURE_PROFILE:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->SET_UP_AN_RST_GRID_LAYOUT:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->SET_UP_A_FLOOR_PLAN:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->SET_UP_TAXES:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->SAVED_A_MENU_ITEM:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->COMPLETED_ACTION_ITEMS:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->APPS_WITH_DEVICE_CODE_USAGES:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->SIGNUP_VARIANT:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->SIGNUP_EGIFT_CARD:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_SOFTWARE_OVERVIEW:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->UPDATED_SECURE_PROFILE:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->ADDED_AUTHORIZED_REPRESENTATIVE:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_DISCOUNTS:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_VENDORS:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_WEEBLY_OST:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_ONLINE_STORE_SETUP:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_ONLINE_STORE_SELECTOR:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x3e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->STARTED_BIG_COMMERCE_RETAIL:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x3f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_INVOICE_SETTINGS:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x40

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->DOWNLOADED_THE_INVOICES_APP:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x41

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->IMPORTED_INVENTORY:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x42

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->CREATED_BAR_CODE_LABEL:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x43

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->ENABLE_PICKUP_AND_DELIVERY:Lcom/squareup/protos/checklist/signal/SignalName;

    const/16 v2, 0x44

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->$VALUES:[Lcom/squareup/protos/checklist/signal/SignalName;

    .line 155
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalName$ProtoAdapter_SignalName;

    invoke-direct {v0}, Lcom/squareup/protos/checklist/signal/SignalName$ProtoAdapter_SignalName;-><init>()V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 159
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 160
    iput p3, p0, Lcom/squareup/protos/checklist/signal/SignalName;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/checklist/signal/SignalName;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 236
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->ENABLE_PICKUP_AND_DELIVERY:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 235
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->CREATED_BAR_CODE_LABEL:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 234
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->IMPORTED_INVENTORY:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 233
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->DOWNLOADED_THE_INVOICES_APP:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 232
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_INVOICE_SETTINGS:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 231
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->STARTED_BIG_COMMERCE_RETAIL:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 230
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_ONLINE_STORE_SELECTOR:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 229
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_ONLINE_STORE_SETUP:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 228
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_WEEBLY_OST:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 227
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_VENDORS:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 226
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_DISCOUNTS:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 225
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->ADDED_AUTHORIZED_REPRESENTATIVE:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 224
    :pswitch_c
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->UPDATED_SECURE_PROFILE:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 223
    :pswitch_d
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_SOFTWARE_OVERVIEW:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 222
    :pswitch_e
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->SIGNUP_EGIFT_CARD:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 221
    :pswitch_f
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->SIGNUP_VARIANT:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 220
    :pswitch_10
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->APPS_WITH_DEVICE_CODE_USAGES:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 219
    :pswitch_11
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->COMPLETED_ACTION_ITEMS:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 218
    :pswitch_12
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->SAVED_A_MENU_ITEM:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 217
    :pswitch_13
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->SET_UP_TAXES:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 216
    :pswitch_14
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->SET_UP_A_FLOOR_PLAN:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 215
    :pswitch_15
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->SET_UP_AN_RST_GRID_LAYOUT:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 214
    :pswitch_16
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->SAW_SECURE_PROFILE:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 213
    :pswitch_17
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->REGISTER_ACTIVATION_STATE:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 212
    :pswitch_18
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->REGISTER_ACTIVATION_STATE_HISTORY:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 211
    :pswitch_19
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->HAS_PAID_FOR_FEATURES:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 210
    :pswitch_1a
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->HAS_RETAIL_APP_USAGE:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 209
    :pswitch_1b
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->SET_CLOSE_OF_DAY:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 208
    :pswitch_1c
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->COMPLETED_PRODUCT_INTERESTS:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 207
    :pswitch_1d
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->CARDLESS_FREE_TRIAL_FEATURES:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 206
    :pswitch_1e
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->SUBSCRIBED_FEATURES:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 205
    :pswitch_1f
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->RECOMMENDED_PRODUCTS:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 204
    :pswitch_20
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->VISITED_SQUARE_SHOP:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 203
    :pswitch_21
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->VISITED_SELLER_COMMUNITY:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 202
    :pswitch_22
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->ENGAGED_CUSTOMERS:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 201
    :pswitch_23
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_OTHER_SERVICES:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 200
    :pswitch_24
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->CUSTOMIZED_RECEIPTS:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 199
    :pswitch_25
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_CUSTOMER_FEEDBACK:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 198
    :pswitch_26
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->MANAGED_EMPLOYEES:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 197
    :pswitch_27
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->ADDED_A_LOCATION:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 196
    :pswitch_28
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->REVIEWED_BUSINESS_DETAILS:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 195
    :pswitch_29
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->SENT_A_DEVICE_CODE:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 194
    :pswitch_2a
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->DISMISSED_ACTION_ITEM_TIMES:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 193
    :pswitch_2b
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->SIGNUP_ESTIMATED_USAGE:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 192
    :pswitch_2c
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->SIGNUP_ESTIMATED_EMPLOYEES:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 191
    :pswitch_2d
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->SIGNUP_ESTIMATED_REVENUE:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 190
    :pswitch_2e
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->SIGNUP_BUSINESS_SUB_CATEGORY:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 189
    :pswitch_2f
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->SIGNUP_BUSINESS_CATEGORY:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 188
    :pswitch_30
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->ENABLED_TWO_FACTOR_AUTH:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 187
    :pswitch_31
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->REFERRAL_ACTIVATED:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 186
    :pswitch_32
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_BIZ_FULFILLMENT:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 185
    :pswitch_33
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->ENABLED_ITEM_FOR_SALE:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 184
    :pswitch_34
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->PUBLISHED_ITEM:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 183
    :pswitch_35
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->CARD_PAYMENT_SUCCESS_COUNT:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 182
    :pswitch_36
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->DISMISSED_ACTION_ITEMS:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 181
    :pswitch_37
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->ADDED_MORE_NEXT_STEPS:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 180
    :pswitch_38
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->ADDED_MOBILE_STAFF:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 179
    :pswitch_39
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_STAND_PAGE:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 178
    :pswitch_3a
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->PUBLISHED_MERCHANT_PROFILE:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 177
    :pswitch_3b
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->HAS_A_DEVICE_USAGE:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 176
    :pswitch_3c
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->ALLOWED_REACTIVATION:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 175
    :pswitch_3d
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->NEXT_STEPS:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 174
    :pswitch_3e
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->TOOK_A_CNP_PAYMENT:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 173
    :pswitch_3f
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->BANK_ACCOUNT_LINK_PROGRESS:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 172
    :pswitch_40
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->CREATED_AN_ITEM:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 171
    :pswitch_41
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->SENT_AN_INVOICE:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 170
    :pswitch_42
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->TOOK_A_CARD_PAYMENT:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 169
    :pswitch_43
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->TOOK_A_CASH_PAYMENT:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    .line 168
    :pswitch_44
    sget-object p0, Lcom/squareup/protos/checklist/signal/SignalName;->VIEWED_REPORTS:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_44
        :pswitch_43
        :pswitch_42
        :pswitch_41
        :pswitch_40
        :pswitch_3f
        :pswitch_3e
        :pswitch_3d
        :pswitch_3c
        :pswitch_3b
        :pswitch_3a
        :pswitch_39
        :pswitch_38
        :pswitch_37
        :pswitch_36
        :pswitch_35
        :pswitch_34
        :pswitch_33
        :pswitch_32
        :pswitch_31
        :pswitch_30
        :pswitch_2f
        :pswitch_2e
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/checklist/signal/SignalName;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/checklist/signal/SignalName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/checklist/signal/SignalName;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->$VALUES:[Lcom/squareup/protos/checklist/signal/SignalName;

    invoke-virtual {v0}, [Lcom/squareup/protos/checklist/signal/SignalName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/checklist/signal/SignalName;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 243
    iget v0, p0, Lcom/squareup/protos/checklist/signal/SignalName;->value:I

    return v0
.end method
