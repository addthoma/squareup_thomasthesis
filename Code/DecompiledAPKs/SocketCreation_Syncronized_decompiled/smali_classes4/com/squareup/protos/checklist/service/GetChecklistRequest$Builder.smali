.class public final Lcom/squareup/protos/checklist/service/GetChecklistRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetChecklistRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/checklist/service/GetChecklistRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/checklist/service/GetChecklistRequest;",
        "Lcom/squareup/protos/checklist/service/GetChecklistRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public is_mobile:Ljava/lang/Boolean;

.field public variant:Lcom/squareup/protos/checklist/service/Variant;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 95
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/checklist/service/GetChecklistRequest;
    .locals 4

    .line 110
    new-instance v0, Lcom/squareup/protos/checklist/service/GetChecklistRequest;

    iget-object v1, p0, Lcom/squareup/protos/checklist/service/GetChecklistRequest$Builder;->variant:Lcom/squareup/protos/checklist/service/Variant;

    iget-object v2, p0, Lcom/squareup/protos/checklist/service/GetChecklistRequest$Builder;->is_mobile:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/checklist/service/GetChecklistRequest;-><init>(Lcom/squareup/protos/checklist/service/Variant;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 90
    invoke-virtual {p0}, Lcom/squareup/protos/checklist/service/GetChecklistRequest$Builder;->build()Lcom/squareup/protos/checklist/service/GetChecklistRequest;

    move-result-object v0

    return-object v0
.end method

.method public is_mobile(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/service/GetChecklistRequest$Builder;
    .locals 0

    .line 104
    iput-object p1, p0, Lcom/squareup/protos/checklist/service/GetChecklistRequest$Builder;->is_mobile:Ljava/lang/Boolean;

    return-object p0
.end method

.method public variant(Lcom/squareup/protos/checklist/service/Variant;)Lcom/squareup/protos/checklist/service/GetChecklistRequest$Builder;
    .locals 0

    .line 99
    iput-object p1, p0, Lcom/squareup/protos/checklist/service/GetChecklistRequest$Builder;->variant:Lcom/squareup/protos/checklist/service/Variant;

    return-object p0
.end method
