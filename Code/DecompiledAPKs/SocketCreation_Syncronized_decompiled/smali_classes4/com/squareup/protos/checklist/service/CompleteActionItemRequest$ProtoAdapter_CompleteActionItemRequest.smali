.class final Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$ProtoAdapter_CompleteActionItemRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CompleteActionItemRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CompleteActionItemRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 96
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 113
    new-instance v0, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$Builder;-><init>()V

    .line 114
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 115
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 126
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 119
    :cond_0
    :try_start_0
    sget-object v4, Lcom/squareup/protos/checklist/common/ActionItemName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/checklist/common/ActionItemName;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$Builder;->name(Lcom/squareup/protos/checklist/common/ActionItemName;)Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 121
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 130
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 131
    invoke-virtual {v0}, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$Builder;->build()Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 94
    invoke-virtual {p0, p1}, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$ProtoAdapter_CompleteActionItemRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 107
    sget-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;->name:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 108
    invoke-virtual {p2}, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 94
    check-cast p2, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$ProtoAdapter_CompleteActionItemRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;)I
    .locals 3

    .line 101
    sget-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;->name:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 102
    invoke-virtual {p1}, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 94
    check-cast p1, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$ProtoAdapter_CompleteActionItemRequest;->encodedSize(Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;)Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;
    .locals 0

    .line 136
    invoke-virtual {p1}, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;->newBuilder()Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$Builder;

    move-result-object p1

    .line 137
    invoke-virtual {p1}, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 138
    invoke-virtual {p1}, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$Builder;->build()Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 94
    check-cast p1, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$ProtoAdapter_CompleteActionItemRequest;->redact(Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;)Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;

    move-result-object p1

    return-object p1
.end method
