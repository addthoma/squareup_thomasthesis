.class public final Lcom/squareup/protos/common/RGBAColor;
.super Lcom/squareup/wire/Message;
.source "RGBAColor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/common/RGBAColor$ProtoAdapter_RGBAColor;,
        Lcom/squareup/protos/common/RGBAColor$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/common/RGBAColor;",
        "Lcom/squareup/protos/common/RGBAColor$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/common/RGBAColor;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_A:Ljava/lang/Double;

.field public static final DEFAULT_B:Ljava/lang/Double;

.field public static final DEFAULT_G:Ljava/lang/Double;

.field public static final DEFAULT_R:Ljava/lang/Double;

.field private static final serialVersionUID:J


# instance fields
.field public final a:Ljava/lang/Double;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#DOUBLE"
        tag = 0x4
    .end annotation
.end field

.field public final b:Ljava/lang/Double;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#DOUBLE"
        tag = 0x3
    .end annotation
.end field

.field public final g:Ljava/lang/Double;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#DOUBLE"
        tag = 0x2
    .end annotation
.end field

.field public final r:Ljava/lang/Double;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#DOUBLE"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 24
    new-instance v0, Lcom/squareup/protos/common/RGBAColor$ProtoAdapter_RGBAColor;

    invoke-direct {v0}, Lcom/squareup/protos/common/RGBAColor$ProtoAdapter_RGBAColor;-><init>()V

    sput-object v0, Lcom/squareup/protos/common/RGBAColor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 28
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/common/RGBAColor;->DEFAULT_R:Ljava/lang/Double;

    .line 30
    sput-object v0, Lcom/squareup/protos/common/RGBAColor;->DEFAULT_G:Ljava/lang/Double;

    .line 32
    sput-object v0, Lcom/squareup/protos/common/RGBAColor;->DEFAULT_B:Ljava/lang/Double;

    const-wide v0, 0x406fe00000000000L    # 255.0

    .line 34
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/common/RGBAColor;->DEFAULT_A:Ljava/lang/Double;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;)V
    .locals 6

    .line 61
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/common/RGBAColor;-><init>(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Lokio/ByteString;)V
    .locals 1

    .line 65
    sget-object v0, Lcom/squareup/protos/common/RGBAColor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 66
    iput-object p1, p0, Lcom/squareup/protos/common/RGBAColor;->r:Ljava/lang/Double;

    .line 67
    iput-object p2, p0, Lcom/squareup/protos/common/RGBAColor;->g:Ljava/lang/Double;

    .line 68
    iput-object p3, p0, Lcom/squareup/protos/common/RGBAColor;->b:Ljava/lang/Double;

    .line 69
    iput-object p4, p0, Lcom/squareup/protos/common/RGBAColor;->a:Ljava/lang/Double;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 86
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/common/RGBAColor;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 87
    :cond_1
    check-cast p1, Lcom/squareup/protos/common/RGBAColor;

    .line 88
    invoke-virtual {p0}, Lcom/squareup/protos/common/RGBAColor;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/common/RGBAColor;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/RGBAColor;->r:Ljava/lang/Double;

    iget-object v3, p1, Lcom/squareup/protos/common/RGBAColor;->r:Ljava/lang/Double;

    .line 89
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/RGBAColor;->g:Ljava/lang/Double;

    iget-object v3, p1, Lcom/squareup/protos/common/RGBAColor;->g:Ljava/lang/Double;

    .line 90
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/RGBAColor;->b:Ljava/lang/Double;

    iget-object v3, p1, Lcom/squareup/protos/common/RGBAColor;->b:Ljava/lang/Double;

    .line 91
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/RGBAColor;->a:Ljava/lang/Double;

    iget-object p1, p1, Lcom/squareup/protos/common/RGBAColor;->a:Ljava/lang/Double;

    .line 92
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 97
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 99
    invoke-virtual {p0}, Lcom/squareup/protos/common/RGBAColor;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/common/RGBAColor;->r:Ljava/lang/Double;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Double;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/common/RGBAColor;->g:Ljava/lang/Double;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Double;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/common/RGBAColor;->b:Ljava/lang/Double;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Double;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/common/RGBAColor;->a:Ljava/lang/Double;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Double;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 104
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/common/RGBAColor$Builder;
    .locals 2

    .line 74
    new-instance v0, Lcom/squareup/protos/common/RGBAColor$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/RGBAColor$Builder;-><init>()V

    .line 75
    iget-object v1, p0, Lcom/squareup/protos/common/RGBAColor;->r:Ljava/lang/Double;

    iput-object v1, v0, Lcom/squareup/protos/common/RGBAColor$Builder;->r:Ljava/lang/Double;

    .line 76
    iget-object v1, p0, Lcom/squareup/protos/common/RGBAColor;->g:Ljava/lang/Double;

    iput-object v1, v0, Lcom/squareup/protos/common/RGBAColor$Builder;->g:Ljava/lang/Double;

    .line 77
    iget-object v1, p0, Lcom/squareup/protos/common/RGBAColor;->b:Ljava/lang/Double;

    iput-object v1, v0, Lcom/squareup/protos/common/RGBAColor$Builder;->b:Ljava/lang/Double;

    .line 78
    iget-object v1, p0, Lcom/squareup/protos/common/RGBAColor;->a:Ljava/lang/Double;

    iput-object v1, v0, Lcom/squareup/protos/common/RGBAColor$Builder;->a:Ljava/lang/Double;

    .line 79
    invoke-virtual {p0}, Lcom/squareup/protos/common/RGBAColor;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/RGBAColor$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/common/RGBAColor;->newBuilder()Lcom/squareup/protos/common/RGBAColor$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/common/RGBAColor;->r:Ljava/lang/Double;

    if-eqz v1, :cond_0

    const-string v1, ", r="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/RGBAColor;->r:Ljava/lang/Double;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 113
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/common/RGBAColor;->g:Ljava/lang/Double;

    if-eqz v1, :cond_1

    const-string v1, ", g="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/RGBAColor;->g:Ljava/lang/Double;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 114
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/common/RGBAColor;->b:Ljava/lang/Double;

    if-eqz v1, :cond_2

    const-string v1, ", b="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/RGBAColor;->b:Ljava/lang/Double;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 115
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/common/RGBAColor;->a:Ljava/lang/Double;

    if-eqz v1, :cond_3

    const-string v1, ", a="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/RGBAColor;->a:Ljava/lang/Double;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "RGBAColor{"

    .line 116
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
