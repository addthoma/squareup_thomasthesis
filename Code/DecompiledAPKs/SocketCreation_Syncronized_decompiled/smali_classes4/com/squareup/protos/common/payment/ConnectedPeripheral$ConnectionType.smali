.class public final enum Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;
.super Ljava/lang/Enum;
.source "ConnectedPeripheral.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/payment/ConnectedPeripheral;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ConnectionType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType$ProtoAdapter_ConnectionType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum AUDIO:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

.field public static final enum BLE:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

.field public static final enum BLUETOOTH:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

.field public static final enum NETWORK:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

.field public static final enum UNKNOWN:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

.field public static final enum USB:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 218
    new-instance v0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    const/4 v1, 0x0

    const-string v2, "NETWORK"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->NETWORK:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    .line 223
    new-instance v0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    const/4 v2, 0x1

    const-string v3, "USB"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->USB:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    .line 228
    new-instance v0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    const/4 v3, 0x2

    const-string v4, "BLUETOOTH"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->BLUETOOTH:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    .line 233
    new-instance v0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    const/4 v4, 0x3

    const-string v5, "BLE"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->BLE:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    .line 238
    new-instance v0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    const/4 v5, 0x4

    const-string v6, "AUDIO"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->AUDIO:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    .line 243
    new-instance v0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    const/4 v6, 0x5

    const-string v7, "UNKNOWN"

    const/16 v8, 0x64

    invoke-direct {v0, v7, v6, v8}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->UNKNOWN:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    .line 214
    sget-object v7, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->NETWORK:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->USB:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->BLUETOOTH:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->BLE:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->AUDIO:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->UNKNOWN:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->$VALUES:[Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    .line 245
    new-instance v0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType$ProtoAdapter_ConnectionType;

    invoke-direct {v0}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType$ProtoAdapter_ConnectionType;-><init>()V

    sput-object v0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 249
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 250
    iput p3, p0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;
    .locals 1

    if-eqz p0, :cond_5

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/16 v0, 0x64

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 263
    :cond_0
    sget-object p0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->UNKNOWN:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    return-object p0

    .line 262
    :cond_1
    sget-object p0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->AUDIO:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    return-object p0

    .line 261
    :cond_2
    sget-object p0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->BLE:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    return-object p0

    .line 260
    :cond_3
    sget-object p0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->BLUETOOTH:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    return-object p0

    .line 259
    :cond_4
    sget-object p0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->USB:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    return-object p0

    .line 258
    :cond_5
    sget-object p0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->NETWORK:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;
    .locals 1

    .line 214
    const-class v0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;
    .locals 1

    .line 214
    sget-object v0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->$VALUES:[Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    invoke-virtual {v0}, [Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 270
    iget v0, p0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->value:I

    return v0
.end method
