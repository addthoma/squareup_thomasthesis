.class public final Lcom/squareup/protos/common/payment/PeripheralMetadata;
.super Lcom/squareup/wire/Message;
.source "PeripheralMetadata.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/common/payment/PeripheralMetadata$ProtoAdapter_PeripheralMetadata;,
        Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/common/payment/PeripheralMetadata;",
        "Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/common/payment/PeripheralMetadata;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_APP_RELEASE:Ljava/lang/String; = ""

.field public static final DEFAULT_APP_VERSION:Ljava/lang/String; = ""

.field public static final DEFAULT_SOURCE_DEVICE_INSTALLATION_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_TRANSACTION_LOCAL_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_TRANSACTION_REMOTE_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final app_release:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final app_version:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final connected_peripherals:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.payment.ConnectedPeripheral#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x5
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/payment/ConnectedPeripheral;",
            ">;"
        }
    .end annotation
.end field

.field public final source_device_installation_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final transaction_local_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final transaction_remote_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final unknown_peripherals:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.payment.ConnectedPeripheral#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x6
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/payment/ConnectedPeripheral;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/common/payment/PeripheralMetadata$ProtoAdapter_PeripheralMetadata;

    invoke-direct {v0}, Lcom/squareup/protos/common/payment/PeripheralMetadata$ProtoAdapter_PeripheralMetadata;-><init>()V

    sput-object v0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/payment/ConnectedPeripheral;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/payment/ConnectedPeripheral;",
            ">;)V"
        }
    .end annotation

    .line 112
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/common/payment/PeripheralMetadata;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/payment/ConnectedPeripheral;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/payment/ConnectedPeripheral;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 119
    sget-object v0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 120
    iput-object p1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->app_release:Ljava/lang/String;

    .line 121
    iput-object p2, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->app_version:Ljava/lang/String;

    .line 122
    iput-object p3, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->transaction_remote_id:Ljava/lang/String;

    .line 123
    iput-object p4, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->transaction_local_id:Ljava/lang/String;

    .line 124
    iput-object p5, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->source_device_installation_id:Ljava/lang/String;

    const-string p1, "connected_peripherals"

    .line 125
    invoke-static {p1, p6}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->connected_peripherals:Ljava/util/List;

    const-string p1, "unknown_peripherals"

    .line 126
    invoke-static {p1, p7}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->unknown_peripherals:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 146
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/common/payment/PeripheralMetadata;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 147
    :cond_1
    check-cast p1, Lcom/squareup/protos/common/payment/PeripheralMetadata;

    .line 148
    invoke-virtual {p0}, Lcom/squareup/protos/common/payment/PeripheralMetadata;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/common/payment/PeripheralMetadata;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->app_release:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/common/payment/PeripheralMetadata;->app_release:Ljava/lang/String;

    .line 149
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->app_version:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/common/payment/PeripheralMetadata;->app_version:Ljava/lang/String;

    .line 150
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->transaction_remote_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/common/payment/PeripheralMetadata;->transaction_remote_id:Ljava/lang/String;

    .line 151
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->transaction_local_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/common/payment/PeripheralMetadata;->transaction_local_id:Ljava/lang/String;

    .line 152
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->source_device_installation_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/common/payment/PeripheralMetadata;->source_device_installation_id:Ljava/lang/String;

    .line 153
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->connected_peripherals:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/common/payment/PeripheralMetadata;->connected_peripherals:Ljava/util/List;

    .line 154
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->unknown_peripherals:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/common/payment/PeripheralMetadata;->unknown_peripherals:Ljava/util/List;

    .line 155
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 160
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 162
    invoke-virtual {p0}, Lcom/squareup/protos/common/payment/PeripheralMetadata;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 163
    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->app_release:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 164
    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->app_version:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 165
    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->transaction_remote_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 166
    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->transaction_local_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 167
    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->source_device_installation_id:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 168
    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->connected_peripherals:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 169
    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->unknown_peripherals:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 170
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;
    .locals 2

    .line 131
    new-instance v0, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;-><init>()V

    .line 132
    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->app_release:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->app_release:Ljava/lang/String;

    .line 133
    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->app_version:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->app_version:Ljava/lang/String;

    .line 134
    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->transaction_remote_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->transaction_remote_id:Ljava/lang/String;

    .line 135
    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->transaction_local_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->transaction_local_id:Ljava/lang/String;

    .line 136
    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->source_device_installation_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->source_device_installation_id:Ljava/lang/String;

    .line 137
    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->connected_peripherals:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->connected_peripherals:Ljava/util/List;

    .line 138
    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->unknown_peripherals:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->unknown_peripherals:Ljava/util/List;

    .line 139
    invoke-virtual {p0}, Lcom/squareup/protos/common/payment/PeripheralMetadata;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/common/payment/PeripheralMetadata;->newBuilder()Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 177
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 178
    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->app_release:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", app_release="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->app_release:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->app_version:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", app_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->app_version:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->transaction_remote_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", transaction_remote_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->transaction_remote_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->transaction_local_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", transaction_local_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->transaction_local_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 182
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->source_device_installation_id:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", source_device_installation_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->source_device_installation_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->connected_peripherals:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, ", connected_peripherals="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->connected_peripherals:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 184
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->unknown_peripherals:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, ", unknown_peripherals="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata;->unknown_peripherals:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "PeripheralMetadata{"

    .line 185
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
