.class public final Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PeripheralMetadata.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/payment/PeripheralMetadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/common/payment/PeripheralMetadata;",
        "Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public app_release:Ljava/lang/String;

.field public app_version:Ljava/lang/String;

.field public connected_peripherals:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/payment/ConnectedPeripheral;",
            ">;"
        }
    .end annotation
.end field

.field public source_device_installation_id:Ljava/lang/String;

.field public transaction_local_id:Ljava/lang/String;

.field public transaction_remote_id:Ljava/lang/String;

.field public unknown_peripherals:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/payment/ConnectedPeripheral;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 203
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 204
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->connected_peripherals:Ljava/util/List;

    .line 205
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->unknown_peripherals:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public app_release(Ljava/lang/String;)Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;
    .locals 0

    .line 212
    iput-object p1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->app_release:Ljava/lang/String;

    return-object p0
.end method

.method public app_version(Ljava/lang/String;)Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;
    .locals 0

    .line 220
    iput-object p1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->app_version:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/common/payment/PeripheralMetadata;
    .locals 10

    .line 273
    new-instance v9, Lcom/squareup/protos/common/payment/PeripheralMetadata;

    iget-object v1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->app_release:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->app_version:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->transaction_remote_id:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->transaction_local_id:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->source_device_installation_id:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->connected_peripherals:Ljava/util/List;

    iget-object v7, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->unknown_peripherals:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/common/payment/PeripheralMetadata;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 188
    invoke-virtual {p0}, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->build()Lcom/squareup/protos/common/payment/PeripheralMetadata;

    move-result-object v0

    return-object v0
.end method

.method public connected_peripherals(Ljava/util/List;)Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/payment/ConnectedPeripheral;",
            ">;)",
            "Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;"
        }
    .end annotation

    .line 255
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 256
    iput-object p1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->connected_peripherals:Ljava/util/List;

    return-object p0
.end method

.method public source_device_installation_id(Ljava/lang/String;)Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;
    .locals 0

    .line 246
    iput-object p1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->source_device_installation_id:Ljava/lang/String;

    return-object p0
.end method

.method public transaction_local_id(Ljava/lang/String;)Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;
    .locals 0

    .line 238
    iput-object p1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->transaction_local_id:Ljava/lang/String;

    return-object p0
.end method

.method public transaction_remote_id(Ljava/lang/String;)Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;
    .locals 0

    .line 230
    iput-object p1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->transaction_remote_id:Ljava/lang/String;

    return-object p0
.end method

.method public unknown_peripherals(Ljava/util/List;)Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/payment/ConnectedPeripheral;",
            ">;)",
            "Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;"
        }
    .end annotation

    .line 266
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 267
    iput-object p1, p0, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->unknown_peripherals:Ljava/util/List;

    return-object p0
.end method
