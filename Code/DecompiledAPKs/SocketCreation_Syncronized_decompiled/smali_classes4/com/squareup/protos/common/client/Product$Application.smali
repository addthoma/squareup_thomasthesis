.class public final enum Lcom/squareup/protos/common/client/Product$Application;
.super Ljava/lang/Enum;
.source "Product.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/client/Product;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Application"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/common/client/Product$Application$ProtoAdapter_Application;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/common/client/Product$Application;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/common/client/Product$Application;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/common/client/Product$Application;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum APPOINTMENTS:Lcom/squareup/protos/common/client/Product$Application;

.field public static final enum CASH:Lcom/squareup/protos/common/client/Product$Application;

.field public static final enum DASHBOARD:Lcom/squareup/protos/common/client/Product$Application;

.field public static final enum LEGACY_REGISTER:Lcom/squareup/protos/common/client/Product$Application;

.field public static final enum REGISTER:Lcom/squareup/protos/common/client/Product$Application;

.field public static final enum TAXIDRIVER:Lcom/squareup/protos/common/client/Product$Application;

.field public static final enum WALLET:Lcom/squareup/protos/common/client/Product$Application;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 203
    new-instance v0, Lcom/squareup/protos/common/client/Product$Application;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "REGISTER"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/common/client/Product$Application;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/client/Product$Application;->REGISTER:Lcom/squareup/protos/common/client/Product$Application;

    .line 209
    new-instance v0, Lcom/squareup/protos/common/client/Product$Application;

    const/4 v3, 0x2

    const-string v4, "LEGACY_REGISTER"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/common/client/Product$Application;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/client/Product$Application;->LEGACY_REGISTER:Lcom/squareup/protos/common/client/Product$Application;

    .line 215
    new-instance v0, Lcom/squareup/protos/common/client/Product$Application;

    const/4 v4, 0x3

    const-string v5, "WALLET"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/common/client/Product$Application;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/client/Product$Application;->WALLET:Lcom/squareup/protos/common/client/Product$Application;

    .line 217
    new-instance v0, Lcom/squareup/protos/common/client/Product$Application;

    const/4 v5, 0x4

    const-string v6, "TAXIDRIVER"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/protos/common/client/Product$Application;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/client/Product$Application;->TAXIDRIVER:Lcom/squareup/protos/common/client/Product$Application;

    .line 219
    new-instance v0, Lcom/squareup/protos/common/client/Product$Application;

    const/4 v6, 0x5

    const-string v7, "DASHBOARD"

    invoke-direct {v0, v7, v5, v6}, Lcom/squareup/protos/common/client/Product$Application;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/client/Product$Application;->DASHBOARD:Lcom/squareup/protos/common/client/Product$Application;

    .line 221
    new-instance v0, Lcom/squareup/protos/common/client/Product$Application;

    const/4 v7, 0x6

    const-string v8, "CASH"

    invoke-direct {v0, v8, v6, v7}, Lcom/squareup/protos/common/client/Product$Application;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/client/Product$Application;->CASH:Lcom/squareup/protos/common/client/Product$Application;

    .line 223
    new-instance v0, Lcom/squareup/protos/common/client/Product$Application;

    const/4 v8, 0x7

    const-string v9, "APPOINTMENTS"

    invoke-direct {v0, v9, v7, v8}, Lcom/squareup/protos/common/client/Product$Application;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/client/Product$Application;->APPOINTMENTS:Lcom/squareup/protos/common/client/Product$Application;

    new-array v0, v8, [Lcom/squareup/protos/common/client/Product$Application;

    .line 199
    sget-object v8, Lcom/squareup/protos/common/client/Product$Application;->REGISTER:Lcom/squareup/protos/common/client/Product$Application;

    aput-object v8, v0, v1

    sget-object v1, Lcom/squareup/protos/common/client/Product$Application;->LEGACY_REGISTER:Lcom/squareup/protos/common/client/Product$Application;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/client/Product$Application;->WALLET:Lcom/squareup/protos/common/client/Product$Application;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/common/client/Product$Application;->TAXIDRIVER:Lcom/squareup/protos/common/client/Product$Application;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/common/client/Product$Application;->DASHBOARD:Lcom/squareup/protos/common/client/Product$Application;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/common/client/Product$Application;->CASH:Lcom/squareup/protos/common/client/Product$Application;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/common/client/Product$Application;->APPOINTMENTS:Lcom/squareup/protos/common/client/Product$Application;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/protos/common/client/Product$Application;->$VALUES:[Lcom/squareup/protos/common/client/Product$Application;

    .line 225
    new-instance v0, Lcom/squareup/protos/common/client/Product$Application$ProtoAdapter_Application;

    invoke-direct {v0}, Lcom/squareup/protos/common/client/Product$Application$ProtoAdapter_Application;-><init>()V

    sput-object v0, Lcom/squareup/protos/common/client/Product$Application;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 229
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 230
    iput p3, p0, Lcom/squareup/protos/common/client/Product$Application;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/common/client/Product$Application;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 244
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/common/client/Product$Application;->APPOINTMENTS:Lcom/squareup/protos/common/client/Product$Application;

    return-object p0

    .line 243
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/common/client/Product$Application;->CASH:Lcom/squareup/protos/common/client/Product$Application;

    return-object p0

    .line 242
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/common/client/Product$Application;->DASHBOARD:Lcom/squareup/protos/common/client/Product$Application;

    return-object p0

    .line 241
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/common/client/Product$Application;->TAXIDRIVER:Lcom/squareup/protos/common/client/Product$Application;

    return-object p0

    .line 240
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/common/client/Product$Application;->WALLET:Lcom/squareup/protos/common/client/Product$Application;

    return-object p0

    .line 239
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/common/client/Product$Application;->LEGACY_REGISTER:Lcom/squareup/protos/common/client/Product$Application;

    return-object p0

    .line 238
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/common/client/Product$Application;->REGISTER:Lcom/squareup/protos/common/client/Product$Application;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/common/client/Product$Application;
    .locals 1

    .line 199
    const-class v0, Lcom/squareup/protos/common/client/Product$Application;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/common/client/Product$Application;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/common/client/Product$Application;
    .locals 1

    .line 199
    sget-object v0, Lcom/squareup/protos/common/client/Product$Application;->$VALUES:[Lcom/squareup/protos/common/client/Product$Application;

    invoke-virtual {v0}, [Lcom/squareup/protos/common/client/Product$Application;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/common/client/Product$Application;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 251
    iget v0, p0, Lcom/squareup/protos/common/client/Product$Application;->value:I

    return v0
.end method
