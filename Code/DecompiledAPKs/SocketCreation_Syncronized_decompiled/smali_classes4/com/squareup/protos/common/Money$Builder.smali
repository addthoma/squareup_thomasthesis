.class public final Lcom/squareup/protos/common/Money$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Money.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/Money;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/common/Money;",
        "Lcom/squareup/protos/common/Money$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amount:Ljava/lang/Long;

.field public currency_code:Lcom/squareup/protos/common/CurrencyCode;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 108
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public amount(Ljava/lang/Long;)Lcom/squareup/protos/common/Money$Builder;
    .locals 0

    .line 117
    iput-object p1, p0, Lcom/squareup/protos/common/Money$Builder;->amount:Ljava/lang/Long;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/common/Money;
    .locals 4

    .line 132
    new-instance v0, Lcom/squareup/protos/common/Money;

    iget-object v1, p0, Lcom/squareup/protos/common/Money$Builder;->amount:Ljava/lang/Long;

    iget-object v2, p0, Lcom/squareup/protos/common/Money$Builder;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 103
    invoke-virtual {p0}, Lcom/squareup/protos/common/Money$Builder;->build()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public currency_code(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money$Builder;
    .locals 0

    .line 126
    iput-object p1, p0, Lcom/squareup/protos/common/Money$Builder;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    return-object p0
.end method
