.class public final Lcom/squareup/protos/common/tipping/TipOption$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TipOption.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/tipping/TipOption;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/common/tipping/TipOption;",
        "Lcom/squareup/protos/common/tipping/TipOption$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public is_remaining_balance:Ljava/lang/Boolean;

.field public label:Ljava/lang/String;

.field public percentage:Ljava/lang/Double;

.field public tip_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 151
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/common/tipping/TipOption;
    .locals 7

    .line 193
    new-instance v6, Lcom/squareup/protos/common/tipping/TipOption;

    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TipOption$Builder;->label:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/common/tipping/TipOption$Builder;->tip_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p0, Lcom/squareup/protos/common/tipping/TipOption$Builder;->percentage:Ljava/lang/Double;

    iget-object v4, p0, Lcom/squareup/protos/common/tipping/TipOption$Builder;->is_remaining_balance:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/common/tipping/TipOption;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/Double;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 142
    invoke-virtual {p0}, Lcom/squareup/protos/common/tipping/TipOption$Builder;->build()Lcom/squareup/protos/common/tipping/TipOption;

    move-result-object v0

    return-object v0
.end method

.method public is_remaining_balance(Ljava/lang/Boolean;)Lcom/squareup/protos/common/tipping/TipOption$Builder;
    .locals 0

    .line 187
    iput-object p1, p0, Lcom/squareup/protos/common/tipping/TipOption$Builder;->is_remaining_balance:Ljava/lang/Boolean;

    return-object p0
.end method

.method public label(Ljava/lang/String;)Lcom/squareup/protos/common/tipping/TipOption$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 163
    iput-object p1, p0, Lcom/squareup/protos/common/tipping/TipOption$Builder;->label:Ljava/lang/String;

    return-object p0
.end method

.method public percentage(Ljava/lang/Double;)Lcom/squareup/protos/common/tipping/TipOption$Builder;
    .locals 0

    .line 179
    iput-object p1, p0, Lcom/squareup/protos/common/tipping/TipOption$Builder;->percentage:Ljava/lang/Double;

    return-object p0
.end method

.method public tip_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/tipping/TipOption$Builder;
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/squareup/protos/common/tipping/TipOption$Builder;->tip_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
