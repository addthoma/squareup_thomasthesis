.class public final Lcom/squareup/protos/common/location/GeoLocation$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GeoLocation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/location/GeoLocation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/common/location/GeoLocation;",
        "Lcom/squareup/protos/common/location/GeoLocation$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public altitude_meters:Ljava/lang/Double;

.field public latitude:Ljava/lang/Double;

.field public longitude:Ljava/lang/Double;

.field public uncertainty_meters:Ljava/lang/Double;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 130
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public altitude_meters(Ljava/lang/Double;)Lcom/squareup/protos/common/location/GeoLocation$Builder;
    .locals 0

    .line 144
    iput-object p1, p0, Lcom/squareup/protos/common/location/GeoLocation$Builder;->altitude_meters:Ljava/lang/Double;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/common/location/GeoLocation;
    .locals 7

    .line 158
    new-instance v6, Lcom/squareup/protos/common/location/GeoLocation;

    iget-object v1, p0, Lcom/squareup/protos/common/location/GeoLocation$Builder;->latitude:Ljava/lang/Double;

    iget-object v2, p0, Lcom/squareup/protos/common/location/GeoLocation$Builder;->longitude:Ljava/lang/Double;

    iget-object v3, p0, Lcom/squareup/protos/common/location/GeoLocation$Builder;->altitude_meters:Ljava/lang/Double;

    iget-object v4, p0, Lcom/squareup/protos/common/location/GeoLocation$Builder;->uncertainty_meters:Ljava/lang/Double;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/common/location/GeoLocation;-><init>(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 121
    invoke-virtual {p0}, Lcom/squareup/protos/common/location/GeoLocation$Builder;->build()Lcom/squareup/protos/common/location/GeoLocation;

    move-result-object v0

    return-object v0
.end method

.method public latitude(Ljava/lang/Double;)Lcom/squareup/protos/common/location/GeoLocation$Builder;
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/squareup/protos/common/location/GeoLocation$Builder;->latitude:Ljava/lang/Double;

    return-object p0
.end method

.method public longitude(Ljava/lang/Double;)Lcom/squareup/protos/common/location/GeoLocation$Builder;
    .locals 0

    .line 139
    iput-object p1, p0, Lcom/squareup/protos/common/location/GeoLocation$Builder;->longitude:Ljava/lang/Double;

    return-object p0
.end method

.method public uncertainty_meters(Ljava/lang/Double;)Lcom/squareup/protos/common/location/GeoLocation$Builder;
    .locals 0

    .line 152
    iput-object p1, p0, Lcom/squareup/protos/common/location/GeoLocation$Builder;->uncertainty_meters:Ljava/lang/Double;

    return-object p0
.end method
