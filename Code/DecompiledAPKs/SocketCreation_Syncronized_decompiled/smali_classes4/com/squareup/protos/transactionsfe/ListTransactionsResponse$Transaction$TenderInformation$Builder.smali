.class public final Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListTransactionsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field public entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field public felica_brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

.field public tender_type:Lcom/squareup/protos/client/bills/Tender$Type;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 703
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;
    .locals 7

    .line 740
    new-instance v6, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    iget-object v2, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    iget-object v3, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;->felica_brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    iget-object v4, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;-><init>(Lcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 694
    invoke-virtual {p0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;->build()Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;

    move-result-object v0

    return-object v0
.end method

.method public card_brand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;
    .locals 0

    .line 718
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-object p0
.end method

.method public entry_method(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;
    .locals 0

    .line 734
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object p0
.end method

.method public felica_brand(Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;
    .locals 0

    .line 726
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;->felica_brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    return-object p0
.end method

.method public tender_type(Lcom/squareup/protos/client/bills/Tender$Type;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;
    .locals 0

    .line 710
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    return-object p0
.end method
