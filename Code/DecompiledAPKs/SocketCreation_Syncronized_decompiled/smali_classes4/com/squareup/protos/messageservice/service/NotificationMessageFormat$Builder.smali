.class public final Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "NotificationMessageFormat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;",
        "Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public aggregatable:Ljava/lang/Boolean;

.field public aggregated_body:Ljava/lang/String;

.field public aggregated_browser_dialog_body:Ljava/lang/String;

.field public aggregated_client_action:Lcom/squareup/protos/client/ClientAction;

.field public aggregated_primary_cta:Ljava/lang/String;

.field public aggregated_title:Ljava/lang/String;

.field public aggregated_url:Ljava/lang/String;

.field public body:Ljava/lang/String;

.field public browser_dialog_body:Ljava/lang/String;

.field public client_action:Lcom/squareup/protos/client/ClientAction;

.field public primary_cta:Ljava/lang/String;

.field public title:Ljava/lang/String;

.field public url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 307
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public aggregatable(Ljava/lang/Boolean;)Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;
    .locals 0

    .line 365
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->aggregatable:Ljava/lang/Boolean;

    return-object p0
.end method

.method public aggregated_body(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;
    .locals 0

    .line 381
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->aggregated_body:Ljava/lang/String;

    return-object p0
.end method

.method public aggregated_browser_dialog_body(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;
    .locals 0

    .line 413
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->aggregated_browser_dialog_body:Ljava/lang/String;

    return-object p0
.end method

.method public aggregated_client_action(Lcom/squareup/protos/client/ClientAction;)Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;
    .locals 0

    .line 405
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->aggregated_client_action:Lcom/squareup/protos/client/ClientAction;

    return-object p0
.end method

.method public aggregated_primary_cta(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;
    .locals 0

    .line 389
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->aggregated_primary_cta:Ljava/lang/String;

    return-object p0
.end method

.method public aggregated_title(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;
    .locals 0

    .line 373
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->aggregated_title:Ljava/lang/String;

    return-object p0
.end method

.method public aggregated_url(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;
    .locals 0

    .line 397
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->aggregated_url:Ljava/lang/String;

    return-object p0
.end method

.method public body(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;
    .locals 0

    .line 322
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->body:Ljava/lang/String;

    return-object p0
.end method

.method public browser_dialog_body(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;
    .locals 0

    .line 357
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->browser_dialog_body:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;
    .locals 17

    move-object/from16 v0, p0

    .line 419
    new-instance v16, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

    iget-object v2, v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->title:Ljava/lang/String;

    iget-object v3, v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->body:Ljava/lang/String;

    iget-object v4, v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->primary_cta:Ljava/lang/String;

    iget-object v5, v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->url:Ljava/lang/String;

    iget-object v6, v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->client_action:Lcom/squareup/protos/client/ClientAction;

    iget-object v7, v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->browser_dialog_body:Ljava/lang/String;

    iget-object v8, v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->aggregatable:Ljava/lang/Boolean;

    iget-object v9, v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->aggregated_title:Ljava/lang/String;

    iget-object v10, v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->aggregated_body:Ljava/lang/String;

    iget-object v11, v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->aggregated_primary_cta:Ljava/lang/String;

    iget-object v12, v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->aggregated_url:Ljava/lang/String;

    iget-object v13, v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->aggregated_client_action:Lcom/squareup/protos/client/ClientAction;

    iget-object v14, v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->aggregated_browser_dialog_body:Ljava/lang/String;

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v15

    move-object/from16 v1, v16

    invoke-direct/range {v1 .. v15}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/ClientAction;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/ClientAction;Ljava/lang/String;Lokio/ByteString;)V

    return-object v16
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 280
    invoke-virtual {p0}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->build()Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

    move-result-object v0

    return-object v0
.end method

.method public client_action(Lcom/squareup/protos/client/ClientAction;)Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;
    .locals 0

    .line 349
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->client_action:Lcom/squareup/protos/client/ClientAction;

    return-object p0
.end method

.method public primary_cta(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;
    .locals 0

    .line 331
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->primary_cta:Ljava/lang/String;

    return-object p0
.end method

.method public title(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;
    .locals 0

    .line 314
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->title:Ljava/lang/String;

    return-object p0
.end method

.method public url(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;
    .locals 0

    .line 340
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->url:Ljava/lang/String;

    return-object p0
.end method
