.class final Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$ProtoAdapter_NotificationMessageFormat;
.super Lcom/squareup/wire/ProtoAdapter;
.source "NotificationMessageFormat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_NotificationMessageFormat"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 425
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 466
    new-instance v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;-><init>()V

    .line 467
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 468
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 484
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 482
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->aggregated_browser_dialog_body(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;

    goto :goto_0

    .line 481
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/client/ClientAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->aggregated_client_action(Lcom/squareup/protos/client/ClientAction;)Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;

    goto :goto_0

    .line 480
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->aggregated_url(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;

    goto :goto_0

    .line 479
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->aggregated_primary_cta(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;

    goto :goto_0

    .line 478
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->aggregated_body(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;

    goto :goto_0

    .line 477
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->aggregated_title(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;

    goto :goto_0

    .line 476
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->aggregatable(Ljava/lang/Boolean;)Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;

    goto :goto_0

    .line 475
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->browser_dialog_body(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;

    goto :goto_0

    .line 474
    :pswitch_8
    sget-object v3, Lcom/squareup/protos/client/ClientAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ClientAction;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->client_action(Lcom/squareup/protos/client/ClientAction;)Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;

    goto :goto_0

    .line 473
    :pswitch_9
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->url(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;

    goto/16 :goto_0

    .line 472
    :pswitch_a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->primary_cta(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;

    goto/16 :goto_0

    .line 471
    :pswitch_b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->body(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;

    goto/16 :goto_0

    .line 470
    :pswitch_c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->title(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;

    goto/16 :goto_0

    .line 488
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 489
    invoke-virtual {v0}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->build()Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 423
    invoke-virtual {p0, p1}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$ProtoAdapter_NotificationMessageFormat;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 448
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->title:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 449
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->body:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 450
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->primary_cta:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 451
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->url:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 452
    sget-object v0, Lcom/squareup/protos/client/ClientAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->client_action:Lcom/squareup/protos/client/ClientAction;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 453
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->browser_dialog_body:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 454
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregatable:Ljava/lang/Boolean;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 455
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_title:Ljava/lang/String;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 456
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_body:Ljava/lang/String;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 457
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_primary_cta:Ljava/lang/String;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 458
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_url:Ljava/lang/String;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 459
    sget-object v0, Lcom/squareup/protos/client/ClientAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_client_action:Lcom/squareup/protos/client/ClientAction;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 460
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_browser_dialog_body:Ljava/lang/String;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 461
    invoke-virtual {p2}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 423
    check-cast p2, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$ProtoAdapter_NotificationMessageFormat;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;)I
    .locals 4

    .line 430
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->title:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->body:Ljava/lang/String;

    const/4 v3, 0x2

    .line 431
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->primary_cta:Ljava/lang/String;

    const/4 v3, 0x3

    .line 432
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->url:Ljava/lang/String;

    const/4 v3, 0x4

    .line 433
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ClientAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->client_action:Lcom/squareup/protos/client/ClientAction;

    const/4 v3, 0x5

    .line 434
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->browser_dialog_body:Ljava/lang/String;

    const/4 v3, 0x6

    .line 435
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregatable:Ljava/lang/Boolean;

    const/4 v3, 0x7

    .line 436
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_title:Ljava/lang/String;

    const/16 v3, 0x8

    .line 437
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_body:Ljava/lang/String;

    const/16 v3, 0x9

    .line 438
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_primary_cta:Ljava/lang/String;

    const/16 v3, 0xa

    .line 439
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_url:Ljava/lang/String;

    const/16 v3, 0xb

    .line 440
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ClientAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_client_action:Lcom/squareup/protos/client/ClientAction;

    const/16 v3, 0xc

    .line 441
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_browser_dialog_body:Ljava/lang/String;

    const/16 v3, 0xd

    .line 442
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 443
    invoke-virtual {p1}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 423
    check-cast p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$ProtoAdapter_NotificationMessageFormat;->encodedSize(Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;)Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;
    .locals 2

    .line 494
    invoke-virtual {p1}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->newBuilder()Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;

    move-result-object p1

    .line 495
    iget-object v0, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->client_action:Lcom/squareup/protos/client/ClientAction;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/ClientAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->client_action:Lcom/squareup/protos/client/ClientAction;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction;

    iput-object v0, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->client_action:Lcom/squareup/protos/client/ClientAction;

    .line 496
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->aggregated_client_action:Lcom/squareup/protos/client/ClientAction;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/ClientAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->aggregated_client_action:Lcom/squareup/protos/client/ClientAction;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ClientAction;

    iput-object v0, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->aggregated_client_action:Lcom/squareup/protos/client/ClientAction;

    .line 497
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 498
    invoke-virtual {p1}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->build()Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 423
    check-cast p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$ProtoAdapter_NotificationMessageFormat;->redact(Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;)Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

    move-result-object p1

    return-object p1
.end method
