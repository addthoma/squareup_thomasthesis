.class public final Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MessageUnit.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/messageservice/service/MessageUnit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/messageservice/service/MessageUnit;",
        "Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public created_at:Ljava/lang/Long;

.field public default_format:Lcom/squareup/protos/messageservice/service/DefaultFormat;

.field public message_class:Lcom/squareup/protos/messageservice/service/MessageClass;

.field public message_format_id:Ljava/lang/String;

.field public message_id:Ljava/lang/String;

.field public message_unit_token:Ljava/lang/String;

.field public notification_message_format:Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

.field public request_token:Ljava/lang/String;

.field public state:Lcom/squareup/protos/messageservice/service/State;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 225
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/messageservice/service/MessageUnit;
    .locals 12

    .line 299
    new-instance v11, Lcom/squareup/protos/messageservice/service/MessageUnit;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->request_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->message_format_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->state:Lcom/squareup/protos/messageservice/service/State;

    iget-object v4, p0, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->message_unit_token:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->message_class:Lcom/squareup/protos/messageservice/service/MessageClass;

    iget-object v6, p0, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->created_at:Ljava/lang/Long;

    iget-object v7, p0, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->message_id:Ljava/lang/String;

    iget-object v8, p0, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->default_format:Lcom/squareup/protos/messageservice/service/DefaultFormat;

    iget-object v9, p0, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->notification_message_format:Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/messageservice/service/MessageUnit;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/messageservice/service/State;Ljava/lang/String;Lcom/squareup/protos/messageservice/service/MessageClass;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/messageservice/service/DefaultFormat;Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;Lokio/ByteString;)V

    return-object v11
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 206
    invoke-virtual {p0}, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->build()Lcom/squareup/protos/messageservice/service/MessageUnit;

    move-result-object v0

    return-object v0
.end method

.method public created_at(Ljava/lang/Long;)Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;
    .locals 0

    .line 272
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->created_at:Ljava/lang/Long;

    return-object p0
.end method

.method public default_format(Lcom/squareup/protos/messageservice/service/DefaultFormat;)Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;
    .locals 0

    .line 285
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->default_format:Lcom/squareup/protos/messageservice/service/DefaultFormat;

    const/4 p1, 0x0

    .line 286
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->notification_message_format:Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

    return-object p0
.end method

.method public message_class(Lcom/squareup/protos/messageservice/service/MessageClass;)Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;
    .locals 0

    .line 264
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->message_class:Lcom/squareup/protos/messageservice/service/MessageClass;

    return-object p0
.end method

.method public message_format_id(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;
    .locals 0

    .line 240
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->message_format_id:Ljava/lang/String;

    return-object p0
.end method

.method public message_id(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;
    .locals 0

    .line 280
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->message_id:Ljava/lang/String;

    return-object p0
.end method

.method public message_unit_token(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;
    .locals 0

    .line 256
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->message_unit_token:Ljava/lang/String;

    return-object p0
.end method

.method public notification_message_format(Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;)Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;
    .locals 0

    .line 292
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->notification_message_format:Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

    const/4 p1, 0x0

    .line 293
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->default_format:Lcom/squareup/protos/messageservice/service/DefaultFormat;

    return-object p0
.end method

.method public request_token(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;
    .locals 0

    .line 232
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->request_token:Ljava/lang/String;

    return-object p0
.end method

.method public state(Lcom/squareup/protos/messageservice/service/State;)Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;
    .locals 0

    .line 248
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->state:Lcom/squareup/protos/messageservice/service/State;

    return-object p0
.end method
