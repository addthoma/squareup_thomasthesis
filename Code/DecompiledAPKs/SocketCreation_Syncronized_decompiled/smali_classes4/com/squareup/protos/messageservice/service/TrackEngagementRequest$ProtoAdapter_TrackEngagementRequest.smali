.class final Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$ProtoAdapter_TrackEngagementRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "TrackEngagementRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_TrackEngagementRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 210
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 235
    new-instance v0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;-><init>()V

    .line 236
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 237
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 252
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 250
    :cond_0
    sget-object v3, Lcom/squareup/protos/messageservice/service/Entity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/messageservice/service/Entity;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->entity(Lcom/squareup/protos/messageservice/service/Entity;)Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;

    goto :goto_0

    .line 249
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->click_url(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;

    goto :goto_0

    .line 243
    :cond_2
    :try_start_0
    sget-object v4, Lcom/squareup/protos/messageservice/service/Action;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/messageservice/service/Action;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->action(Lcom/squareup/protos/messageservice/service/Action;)Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 245
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 240
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->message_id(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;

    goto :goto_0

    .line 239
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->request_token(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;

    goto :goto_0

    .line 256
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 257
    invoke-virtual {v0}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->build()Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 208
    invoke-virtual {p0, p1}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$ProtoAdapter_TrackEngagementRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 225
    sget-object v0, Lcom/squareup/protos/messageservice/service/Action;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->action:Lcom/squareup/protos/messageservice/service/Action;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 226
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->click_url:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 227
    sget-object v0, Lcom/squareup/protos/messageservice/service/Entity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->entity:Lcom/squareup/protos/messageservice/service/Entity;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 228
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->request_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 229
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->message_id:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 230
    invoke-virtual {p2}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 208
    check-cast p2, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$ProtoAdapter_TrackEngagementRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;)I
    .locals 4

    .line 215
    sget-object v0, Lcom/squareup/protos/messageservice/service/Action;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->action:Lcom/squareup/protos/messageservice/service/Action;

    const/4 v2, 0x3

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->click_url:Ljava/lang/String;

    const/4 v3, 0x4

    .line 216
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/messageservice/service/Entity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->entity:Lcom/squareup/protos/messageservice/service/Entity;

    const/4 v3, 0x5

    .line 217
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->request_token:Ljava/lang/String;

    const/4 v3, 0x1

    .line 218
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->message_id:Ljava/lang/String;

    const/4 v3, 0x2

    .line 219
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 220
    invoke-virtual {p1}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 208
    check-cast p1, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$ProtoAdapter_TrackEngagementRequest;->encodedSize(Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;)Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;
    .locals 2

    .line 262
    invoke-virtual {p1}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->newBuilder()Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;

    move-result-object p1

    .line 263
    iget-object v0, p1, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->entity:Lcom/squareup/protos/messageservice/service/Entity;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/messageservice/service/Entity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->entity:Lcom/squareup/protos/messageservice/service/Entity;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/messageservice/service/Entity;

    iput-object v0, p1, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->entity:Lcom/squareup/protos/messageservice/service/Entity;

    .line 264
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 265
    invoke-virtual {p1}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->build()Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 208
    check-cast p1, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$ProtoAdapter_TrackEngagementRequest;->redact(Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;)Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;

    move-result-object p1

    return-object p1
.end method
