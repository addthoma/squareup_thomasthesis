.class public final Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Protected.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;",
        "Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public category:Ljava/lang/String;

.field public token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 314
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;
    .locals 4

    .line 335
    new-instance v0, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

    iget-object v1, p0, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory$Builder;->category:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory$Builder;->token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 309
    invoke-virtual {p0}, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory$Builder;->build()Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

    move-result-object v0

    return-object v0
.end method

.method public category(Ljava/lang/String;)Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory$Builder;
    .locals 0

    .line 321
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory$Builder;->category:Ljava/lang/String;

    return-object p0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory$Builder;
    .locals 0

    .line 329
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory$Builder;->token:Ljava/lang/String;

    return-object p0
.end method
