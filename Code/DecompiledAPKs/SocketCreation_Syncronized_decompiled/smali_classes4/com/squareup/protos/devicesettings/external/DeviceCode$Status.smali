.class public final enum Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;
.super Ljava/lang/Enum;
.source "DeviceCode.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/devicesettings/external/DeviceCode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/devicesettings/external/DeviceCode$Status$ProtoAdapter_Status;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum PAIRED:Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;

.field public static final enum UNPAIRED:Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 425
    new-instance v0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "UNPAIRED"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;->UNPAIRED:Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;

    .line 432
    new-instance v0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;

    const/4 v3, 0x2

    const-string v4, "PAIRED"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;->PAIRED:Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;

    new-array v0, v3, [Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;

    .line 419
    sget-object v3, Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;->UNPAIRED:Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;->PAIRED:Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;->$VALUES:[Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;

    .line 434
    new-instance v0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Status$ProtoAdapter_Status;

    invoke-direct {v0}, Lcom/squareup/protos/devicesettings/external/DeviceCode$Status$ProtoAdapter_Status;-><init>()V

    sput-object v0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 438
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 439
    iput p3, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 448
    :cond_0
    sget-object p0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;->PAIRED:Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;

    return-object p0

    .line 447
    :cond_1
    sget-object p0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;->UNPAIRED:Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;
    .locals 1

    .line 419
    const-class v0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;
    .locals 1

    .line 419
    sget-object v0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;->$VALUES:[Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;

    invoke-virtual {v0}, [Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 455
    iget v0, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;->value:I

    return v0
.end method
