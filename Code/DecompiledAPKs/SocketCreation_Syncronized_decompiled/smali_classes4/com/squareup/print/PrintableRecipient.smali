.class public interface abstract Lcom/squareup/print/PrintableRecipient;
.super Ljava/lang/Object;
.source "PrintableRecipient.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\n\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\u000cH&R\u0014\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0005R\u0014\u0010\u0008\u001a\u0004\u0018\u00010\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\u0005\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/print/PrintableRecipient;",
        "",
        "recipientAddress",
        "",
        "getRecipientAddress",
        "()Ljava/lang/String;",
        "recipientName",
        "getRecipientName",
        "recipientPhoneNumber",
        "getRecipientPhoneNumber",
        "recipientLabel",
        "res",
        "Lcom/squareup/util/Res;",
        "print_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getRecipientAddress()Ljava/lang/String;
.end method

.method public abstract getRecipientName()Ljava/lang/String;
.end method

.method public abstract getRecipientPhoneNumber()Ljava/lang/String;
.end method

.method public abstract recipientLabel(Lcom/squareup/util/Res;)Ljava/lang/String;
.end method
