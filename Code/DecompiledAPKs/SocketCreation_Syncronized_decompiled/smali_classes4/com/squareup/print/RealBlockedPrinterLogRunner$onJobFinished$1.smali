.class final Lcom/squareup/print/RealBlockedPrinterLogRunner$onJobFinished$1;
.super Ljava/lang/Object;
.source "RealBlockedPrinterLogRunner.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/print/RealBlockedPrinterLogRunner;->onJobFinished(Lcom/squareup/print/PrintJob;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $printJob:Lcom/squareup/print/PrintJob;

.field final synthetic this$0:Lcom/squareup/print/RealBlockedPrinterLogRunner;


# direct methods
.method constructor <init>(Lcom/squareup/print/RealBlockedPrinterLogRunner;Lcom/squareup/print/PrintJob;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner$onJobFinished$1;->this$0:Lcom/squareup/print/RealBlockedPrinterLogRunner;

    iput-object p2, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner$onJobFinished$1;->$printJob:Lcom/squareup/print/PrintJob;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .line 33
    iget-object v0, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner$onJobFinished$1;->this$0:Lcom/squareup/print/RealBlockedPrinterLogRunner;

    iget-object v1, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner$onJobFinished$1;->$printJob:Lcom/squareup/print/PrintJob;

    invoke-virtual {v0, v1}, Lcom/squareup/print/RealBlockedPrinterLogRunner;->getUniqueLogId$hardware_release(Lcom/squareup/print/PrintJob;)Lcom/squareup/print/RealBlockedPrinterLogRunner$UniqueLogId;

    move-result-object v0

    .line 34
    iget-object v1, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner$onJobFinished$1;->this$0:Lcom/squareup/print/RealBlockedPrinterLogRunner;

    invoke-static {v1}, Lcom/squareup/print/RealBlockedPrinterLogRunner;->access$getJobToRunnableMap$p(Lcom/squareup/print/RealBlockedPrinterLogRunner;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "onJobFinished: not in jobs map!"

    .line 36
    invoke-static {v1, v0}, Ltimber/log/Timber;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 38
    :cond_0
    iget-object v1, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner$onJobFinished$1;->this$0:Lcom/squareup/print/RealBlockedPrinterLogRunner;

    invoke-static {v1}, Lcom/squareup/print/RealBlockedPrinterLogRunner;->access$getMainThread$p(Lcom/squareup/print/RealBlockedPrinterLogRunner;)Lcom/squareup/thread/executor/MainThread;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method
