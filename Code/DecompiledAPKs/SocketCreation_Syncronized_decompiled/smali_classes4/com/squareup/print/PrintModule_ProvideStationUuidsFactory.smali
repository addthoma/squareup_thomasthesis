.class public final Lcom/squareup/print/PrintModule_ProvideStationUuidsFactory;
.super Ljava/lang/Object;
.source "PrintModule_ProvideStationUuidsFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/f2prateek/rx/preferences2/Preference<",
        "Ljava/util/Set<",
        "Ljava/lang/String;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field private final adapterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/StringSetPreferenceAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private final preferencesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/StringSetPreferenceAdapter;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/print/PrintModule_ProvideStationUuidsFactory;->preferencesProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/print/PrintModule_ProvideStationUuidsFactory;->adapterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/print/PrintModule_ProvideStationUuidsFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/StringSetPreferenceAdapter;",
            ">;)",
            "Lcom/squareup/print/PrintModule_ProvideStationUuidsFactory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/print/PrintModule_ProvideStationUuidsFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/print/PrintModule_ProvideStationUuidsFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideStationUuids(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/squareup/settings/StringSetPreferenceAdapter;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            "Lcom/squareup/settings/StringSetPreferenceAdapter;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 44
    invoke-static {p0, p1}, Lcom/squareup/print/PrintModule;->provideStationUuids(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/squareup/settings/StringSetPreferenceAdapter;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/f2prateek/rx/preferences2/Preference;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/squareup/print/PrintModule_ProvideStationUuidsFactory;->preferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    iget-object v1, p0, Lcom/squareup/print/PrintModule_ProvideStationUuidsFactory;->adapterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/StringSetPreferenceAdapter;

    invoke-static {v0, v1}, Lcom/squareup/print/PrintModule_ProvideStationUuidsFactory;->provideStationUuids(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/squareup/settings/StringSetPreferenceAdapter;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/print/PrintModule_ProvideStationUuidsFactory;->get()Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    return-object v0
.end method
