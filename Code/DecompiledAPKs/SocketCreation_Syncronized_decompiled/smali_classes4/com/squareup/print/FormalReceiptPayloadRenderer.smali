.class public Lcom/squareup/print/FormalReceiptPayloadRenderer;
.super Ljava/lang/Object;
.source "FormalReceiptPayloadRenderer.java"

# interfaces
.implements Lcom/squareup/print/ReceiptPayloadRenderer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public renderBitmap(Lcom/squareup/print/payload/ReceiptPayload;Lcom/squareup/print/ThermalBitmapBuilder;)Landroid/graphics/Bitmap;
    .locals 5

    .line 16
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getHeader()Lcom/squareup/print/sections/HeaderSection;

    move-result-object v0

    .line 18
    iget-object v1, v0, Lcom/squareup/print/sections/HeaderSection;->logoUrl:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 19
    iget-object v1, v0, Lcom/squareup/print/sections/HeaderSection;->logoUrl:Ljava/lang/String;

    sget-object v2, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->MEDIUM:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    invoke-virtual {p2, v1, v2}, Lcom/squareup/print/ThermalBitmapBuilder;->logo(Ljava/lang/String;Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 23
    :cond_0
    iget-object v1, v0, Lcom/squareup/print/sections/HeaderSection;->businessName:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 24
    iget-object v1, v0, Lcom/squareup/print/sections/HeaderSection;->businessName:Ljava/lang/String;

    invoke-virtual {p2, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthHeadlineText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 25
    invoke-virtual {p2}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 29
    :cond_1
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 30
    iget-object v2, v0, Lcom/squareup/print/sections/HeaderSection;->addressLines:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/squareup/print/util/PrintRendererUtils;->appendAsLinesIfNotBlankList(Landroid/text/SpannableStringBuilder;Ljava/util/List;)V

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/CharSequence;

    const/4 v3, 0x0

    .line 31
    iget-object v4, v0, Lcom/squareup/print/sections/HeaderSection;->phoneNumber:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, v0, Lcom/squareup/print/sections/HeaderSection;->website:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, v0, Lcom/squareup/print/sections/HeaderSection;->twitter:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, v0, Lcom/squareup/print/sections/HeaderSection;->businessAbn:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/squareup/print/util/PrintRendererUtils;->appendAsLinesIfNotBlank(Landroid/text/SpannableStringBuilder;[Ljava/lang/CharSequence;)V

    .line 39
    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 40
    invoke-virtual {p2, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 41
    invoke-virtual {p2}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 45
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getCodes()Lcom/squareup/print/sections/CodesSection;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/print/sections/CodesSection;->receiptNumber:Ljava/lang/String;

    invoke-virtual {p2, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 46
    invoke-virtual {p2}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 49
    iget-object v1, v0, Lcom/squareup/print/sections/HeaderSection;->formalReceiptTitle:Ljava/lang/String;

    invoke-virtual {p2, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthCenteredHeadlineText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 50
    invoke-virtual {p2}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 53
    iget-object v1, v0, Lcom/squareup/print/sections/HeaderSection;->formalReceiptName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {p2, v2, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsRightExpandsLeftOverflows(Ljava/lang/CharSequence;Ljava/lang/String;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 54
    invoke-virtual {p2}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 55
    invoke-virtual {p2}, Lcom/squareup/print/ThermalBitmapBuilder;->lightDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 58
    invoke-virtual {p2}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 59
    invoke-virtual {p2}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 60
    invoke-virtual {p2}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 63
    invoke-virtual {p2}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 64
    iget-object v0, v0, Lcom/squareup/print/sections/HeaderSection;->date:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 65
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getTender()Lcom/squareup/print/sections/TenderSection;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/print/sections/TenderSection;->amountActuallyTenderedShort:Lcom/squareup/print/payload/LabelAmountPair;

    iget-object v1, v1, Lcom/squareup/print/payload/LabelAmountPair;->label:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 64
    invoke-virtual {p2, v0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsOrCollapse(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 66
    invoke-virtual {p2}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 69
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getSubtotalSection()Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 70
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getTotalSection()Lcom/squareup/print/sections/TotalSection;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/squareup/print/sections/TotalSection;->renderBitmapFormalReceipt(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 73
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getMultipleTaxBreakdown()Lcom/squareup/print/sections/MultipleTaxBreakdownSection;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 74
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getMultipleTaxBreakdown()Lcom/squareup/print/sections/MultipleTaxBreakdownSection;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/squareup/print/sections/MultipleTaxBreakdownSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 78
    :cond_3
    invoke-virtual {p2}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 79
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getFooter()Lcom/squareup/print/sections/FooterSection;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/print/sections/FooterSection;->formalReceiptItemListHeader:Ljava/lang/String;

    invoke-virtual {p2, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 83
    invoke-virtual {p2}, Lcom/squareup/print/ThermalBitmapBuilder;->largeSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 84
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getFooter()Lcom/squareup/print/sections/FooterSection;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/print/sections/FooterSection;->formalReceiptItemListConfirmation:Ljava/lang/String;

    invoke-virtual {p2, p1}, Lcom/squareup/print/ThermalBitmapBuilder;->centeredText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 85
    invoke-virtual {p2}, Lcom/squareup/print/ThermalBitmapBuilder;->largeSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 88
    invoke-virtual {p2}, Lcom/squareup/print/ThermalBitmapBuilder;->largeSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 89
    invoke-virtual {p2}, Lcom/squareup/print/ThermalBitmapBuilder;->largeSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 90
    invoke-virtual {p2}, Lcom/squareup/print/ThermalBitmapBuilder;->largeSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 91
    invoke-virtual {p2}, Lcom/squareup/print/ThermalBitmapBuilder;->largeSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 92
    invoke-virtual {p2}, Lcom/squareup/print/ThermalBitmapBuilder;->largeSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 94
    invoke-virtual {p2}, Lcom/squareup/print/ThermalBitmapBuilder;->render()Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method
