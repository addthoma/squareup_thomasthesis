.class public abstract Lcom/squareup/print/RegisterPrintModule;
.super Ljava/lang/Object;
.source "RegisterPrintModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/RegisterPrintModule$Prod;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideHardwarePrinterExecutor(Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/print/HardwarePrinterExecutor;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "hardware-printers-executor"

    .line 57
    invoke-static {v0}, Lcom/squareup/thread/Threads;->namedThreadFactory(Ljava/lang/String;)Ljava/util/concurrent/ThreadFactory;

    move-result-object v0

    .line 58
    new-instance v1, Lcom/squareup/print/RegisterHardwarePrinterExecutor;

    invoke-static {v0}, Lcom/squareup/thread/executor/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-direct {v1, v0, p0}, Lcom/squareup/print/RegisterHardwarePrinterExecutor;-><init>(Ljava/util/concurrent/ExecutorService;Lcom/squareup/thread/executor/MainThread;)V

    return-object v1
.end method

.method static provideSettingFactory(Lcom/google/gson/Gson;Landroid/content/SharedPreferences;)Lcom/squareup/settings/GsonLocalSettingFactory;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/GsonLocalSettingFactory<",
            "Lcom/squareup/print/PrinterStationConfiguration;",
            ">;"
        }
    .end annotation

    .line 88
    new-instance v0, Lcom/squareup/settings/GsonLocalSettingFactory;

    const-class v1, Lcom/squareup/print/PrinterStationConfiguration;

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/settings/GsonLocalSettingFactory;-><init>(Lcom/google/gson/Gson;Landroid/content/SharedPreferences;Ljava/lang/Class;)V

    return-object v0
.end method

.method static provideSqlitePrintJobQueue(Landroid/app/Application;Ljava/io/File;Lcom/google/gson/Gson;)Lcom/squareup/print/PrintJobQueue;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 64
    new-instance v0, Lcom/squareup/print/SqlitePrintJobQueue;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/print/SqlitePrintJobQueue;-><init>(Landroid/content/Context;Ljava/io/File;Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method static provideStubPayloadFactory(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/permissions/EmployeeManagement;)Lcom/squareup/print/payload/StubPayload$Factory;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 82
    new-instance v0, Lcom/squareup/print/payload/StubPayload$Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/print/payload/StubPayload$Factory;-><init>(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/permissions/EmployeeManagement;)V

    return-object v0
.end method


# virtual methods
.method abstract provideAnalyticsListener(Lcom/squareup/print/RegisterPrintStatusLogger;)Lcom/squareup/print/PrintSpooler$PrintJobStatusListener;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract provideOldBinder(Lcom/squareup/print/RegisterPrintTargetRouter;)Lcom/squareup/print/PrintTargetRouter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract providePrintSettings(Lcom/squareup/print/RealPrintSettings;)Lcom/squareup/print/PrintSettings;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract providePrinterStationFactory(Lcom/squareup/print/RealPrinterStationFactory;)Lcom/squareup/print/PrinterStationFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract providePrinterStations(Lcom/squareup/print/RealPrinterStations;)Lcom/squareup/print/PrinterStations;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract providePrintoutDispatcher(Lcom/squareup/print/RealPrintoutDispatcher;)Lcom/squareup/print/PrintoutDispatcher;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideQueueExecutor(Lcom/squareup/print/FileThreadPrintQueueExecutor;)Lcom/squareup/print/PrintQueueExecutor;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideTicketNumbering(Lcom/squareup/print/RealTicketAutoIdentifiers;)Lcom/squareup/print/TicketAutoIdentifiers;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
