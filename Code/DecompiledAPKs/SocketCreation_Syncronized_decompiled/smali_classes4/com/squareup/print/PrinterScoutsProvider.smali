.class public interface abstract Lcom/squareup/print/PrinterScoutsProvider;
.super Ljava/lang/Object;
.source "PrinterScoutsProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/PrinterScoutsProvider$NoPrinterScouts;
    }
.end annotation


# virtual methods
.method public abstract availableScouts()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/print/PrinterScout;",
            ">;"
        }
    .end annotation
.end method
