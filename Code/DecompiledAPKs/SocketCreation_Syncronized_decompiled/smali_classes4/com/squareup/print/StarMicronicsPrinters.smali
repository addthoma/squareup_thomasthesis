.class public final Lcom/squareup/print/StarMicronicsPrinters;
.super Ljava/lang/Object;
.source "StarMicronicsPrinters.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;
    }
.end annotation


# static fields
.field public static final BOLD_END_CONTROL_CHARACTERS:Ljava/lang/String; = "\u001bF"

.field public static final BOLD_START_CONTROL_CHARACTERS:Ljava/lang/String; = "\u001bE"

.field public static final DISABLE_WIDE_CHARACTERS_BYTES:Ljava/lang/String; = "\u001bW0"

.field public static final ENABLE_WIDE_CHARACTERS_BYTES:Ljava/lang/String; = "\u001bW1"

.field public static final FEED_AND_CUT_BYTES:Ljava/lang/String; = "\u001bd\u0002"

.field public static final IMPACT_PRINTER_THREE_INCH_WIDE_CHARACTER_LINE_WIDTH:I = 0x15

.field public static final RED_END_CONTROL_CHARACTERS:Ljava/lang/String; = "\u001b5"

.field public static final RED_START_CONTROL_CHARACTERS:Ljava/lang/String; = "\u001b4"

.field public static final UNPRINTABLE_CHARACTER_REPLACEMENT:Ljava/lang/String; = "?"

.field private static final rasterWhitelist:[Ljava/lang/String;

.field private static final textWhitelist:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const-string v0, "TSP"

    const-string v1, "FVP"

    const-string v2, "TUP"

    const-string v3, "MCP"

    const-string v4, "MC-P"

    .line 14
    filled-new-array {v0, v1, v2, v3, v4}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/print/StarMicronicsPrinters;->rasterWhitelist:[Ljava/lang/String;

    const-string v0, "SP"

    .line 17
    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/print/StarMicronicsPrinters;->textWhitelist:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static modelPaperWidth(Ljava/lang/String;)Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;
    .locals 1

    const-string v0, "model"

    .line 94
    invoke-static {p0, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 95
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    const-string v0, "MCP21"

    .line 96
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "MC-PRINT2"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    .line 99
    :cond_0
    sget-object p0, Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;->THREE_INCH:Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;

    return-object p0

    .line 97
    :cond_1
    :goto_0
    sget-object p0, Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;->TWO_INCH:Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;

    return-object p0
.end method

.method static modelSupportsRasterMode(Ljava/lang/String;)Z
    .locals 5

    const-string v0, "model"

    .line 63
    invoke-static {p0, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 64
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    .line 65
    sget-object v0, Lcom/squareup/print/StarMicronicsPrinters;->rasterWhitelist:[Ljava/lang/String;

    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_1

    aget-object v4, v0, v3

    .line 66
    invoke-virtual {p0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return v2
.end method

.method static modelSupportsTextMode(Ljava/lang/String;)Z
    .locals 5

    const-string v0, "model"

    .line 74
    invoke-static {p0, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 75
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 76
    invoke-static {p0}, Lcom/squareup/print/StarMicronicsPrinters;->modelSupportsRasterMode(Ljava/lang/String;)Z

    move-result p0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz p0, :cond_1

    const-string p0, "TSP1"

    .line 78
    invoke-virtual {v0, p0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    return v1

    :cond_0
    return v2

    .line 84
    :cond_1
    sget-object p0, Lcom/squareup/print/StarMicronicsPrinters;->textWhitelist:[Ljava/lang/String;

    array-length v3, p0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, p0, v1

    .line 85
    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    return v2

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    const-string p0, "KDS"

    .line 90
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method

.method static useStarExtensionLib(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "model"

    .line 106
    invoke-static {p0, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 107
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    const-string v0, "MCP"

    .line 108
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "MC-P"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method
