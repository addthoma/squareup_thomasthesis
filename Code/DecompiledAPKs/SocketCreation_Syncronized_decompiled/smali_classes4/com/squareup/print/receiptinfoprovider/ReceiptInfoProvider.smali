.class public interface abstract Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;
.super Ljava/lang/Object;
.source "ReceiptInfoProvider.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider$NoBarcodeReceiptInfoProvider;,
        Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider$NoBarcodeReceiptInfoProviderModule;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001:\u0002\u0007\u0008J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0003H&J\u0008\u0010\u0005\u001a\u00020\u0006H&\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;",
        "",
        "formatBarcodeForReceiptNumber",
        "",
        "receiptNumber",
        "shouldPrintBarcodes",
        "",
        "NoBarcodeReceiptInfoProvider",
        "NoBarcodeReceiptInfoProviderModule",
        "print_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract formatBarcodeForReceiptNumber(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract shouldPrintBarcodes()Z
.end method
