.class public Lcom/squareup/print/LocalTenderCache;
.super Ljava/lang/Object;
.source "LocalTenderCache.java"


# instance fields
.field private receiptNumber:Ljava/lang/String;

.field private tenderClientId:Ljava/lang/String;

.field private tenderServerId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized clear()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    .line 21
    :try_start_0
    iput-object v0, p0, Lcom/squareup/print/LocalTenderCache;->receiptNumber:Ljava/lang/String;

    .line 22
    iput-object v0, p0, Lcom/squareup/print/LocalTenderCache;->tenderClientId:Ljava/lang/String;

    .line 23
    iput-object v0, p0, Lcom/squareup/print/LocalTenderCache;->tenderServerId:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 24
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getLastReceiptNumber(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    monitor-enter p0

    if-eqz p1, :cond_1

    .line 34
    :try_start_0
    iget-object v0, p0, Lcom/squareup/print/LocalTenderCache;->tenderClientId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    .line 37
    :cond_0
    iget-object p1, p0, Lcom/squareup/print/LocalTenderCache;->receiptNumber:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    .line 35
    monitor-exit p0

    return-object p1
.end method

.method public declared-synchronized getLastTenderServerId(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    monitor-enter p0

    if-eqz p1, :cond_1

    .line 41
    :try_start_0
    iget-object v0, p0, Lcom/squareup/print/LocalTenderCache;->tenderClientId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    .line 44
    :cond_0
    iget-object p1, p0, Lcom/squareup/print/LocalTenderCache;->tenderServerId:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    .line 42
    monitor-exit p0

    return-object p1
.end method

.method public declared-synchronized setLast(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    monitor-enter p0

    .line 28
    :try_start_0
    iput-object p1, p0, Lcom/squareup/print/LocalTenderCache;->tenderClientId:Ljava/lang/String;

    .line 29
    iput-object p2, p0, Lcom/squareup/print/LocalTenderCache;->receiptNumber:Ljava/lang/String;

    .line 30
    iput-object p3, p0, Lcom/squareup/print/LocalTenderCache;->tenderServerId:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 31
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
