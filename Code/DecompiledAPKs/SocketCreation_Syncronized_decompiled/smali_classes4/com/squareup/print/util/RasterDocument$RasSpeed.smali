.class public final enum Lcom/squareup/print/util/RasterDocument$RasSpeed;
.super Ljava/lang/Enum;
.source "RasterDocument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/util/RasterDocument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RasSpeed"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/print/util/RasterDocument$RasSpeed;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/print/util/RasterDocument$RasSpeed;

.field public static final enum FULL:Lcom/squareup/print/util/RasterDocument$RasSpeed;

.field public static final enum LOW:Lcom/squareup/print/util/RasterDocument$RasSpeed;

.field public static final enum MEDIUM:Lcom/squareup/print/util/RasterDocument$RasSpeed;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 18
    new-instance v0, Lcom/squareup/print/util/RasterDocument$RasSpeed;

    const/4 v1, 0x0

    const-string v2, "FULL"

    invoke-direct {v0, v2, v1}, Lcom/squareup/print/util/RasterDocument$RasSpeed;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/util/RasterDocument$RasSpeed;->FULL:Lcom/squareup/print/util/RasterDocument$RasSpeed;

    .line 19
    new-instance v0, Lcom/squareup/print/util/RasterDocument$RasSpeed;

    const/4 v2, 0x1

    const-string v3, "MEDIUM"

    invoke-direct {v0, v3, v2}, Lcom/squareup/print/util/RasterDocument$RasSpeed;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/util/RasterDocument$RasSpeed;->MEDIUM:Lcom/squareup/print/util/RasterDocument$RasSpeed;

    .line 20
    new-instance v0, Lcom/squareup/print/util/RasterDocument$RasSpeed;

    const/4 v3, 0x2

    const-string v4, "LOW"

    invoke-direct {v0, v4, v3}, Lcom/squareup/print/util/RasterDocument$RasSpeed;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/util/RasterDocument$RasSpeed;->LOW:Lcom/squareup/print/util/RasterDocument$RasSpeed;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/print/util/RasterDocument$RasSpeed;

    .line 17
    sget-object v4, Lcom/squareup/print/util/RasterDocument$RasSpeed;->FULL:Lcom/squareup/print/util/RasterDocument$RasSpeed;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/print/util/RasterDocument$RasSpeed;->MEDIUM:Lcom/squareup/print/util/RasterDocument$RasSpeed;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/print/util/RasterDocument$RasSpeed;->LOW:Lcom/squareup/print/util/RasterDocument$RasSpeed;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/print/util/RasterDocument$RasSpeed;->$VALUES:[Lcom/squareup/print/util/RasterDocument$RasSpeed;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/print/util/RasterDocument$RasSpeed;
    .locals 1

    .line 17
    const-class v0, Lcom/squareup/print/util/RasterDocument$RasSpeed;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/print/util/RasterDocument$RasSpeed;

    return-object p0
.end method

.method public static values()[Lcom/squareup/print/util/RasterDocument$RasSpeed;
    .locals 1

    .line 17
    sget-object v0, Lcom/squareup/print/util/RasterDocument$RasSpeed;->$VALUES:[Lcom/squareup/print/util/RasterDocument$RasSpeed;

    invoke-virtual {v0}, [Lcom/squareup/print/util/RasterDocument$RasSpeed;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/print/util/RasterDocument$RasSpeed;

    return-object v0
.end method
