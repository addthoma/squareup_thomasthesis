.class public Lcom/squareup/print/util/RasterDocument;
.super Ljava/lang/Object;
.source "RasterDocument.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/util/RasterDocument$RasPageEndMode;,
        Lcom/squareup/print/util/RasterDocument$RasSpeed;,
        Lcom/squareup/print/util/RasterDocument$RasTopMargin;
    }
.end annotation


# instance fields
.field mEOTMode:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

.field mFFMode:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

.field mLeftMargin:I

.field mPageLength:I

.field mRightMargin:I

.field mSpeed:Lcom/squareup/print/util/RasterDocument$RasSpeed;

.field mTopMargin:Lcom/squareup/print/util/RasterDocument$RasTopMargin;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    sget-object v0, Lcom/squareup/print/util/RasterDocument$RasTopMargin;->DEFAULT:Lcom/squareup/print/util/RasterDocument$RasTopMargin;

    iput-object v0, p0, Lcom/squareup/print/util/RasterDocument;->mTopMargin:Lcom/squareup/print/util/RasterDocument$RasTopMargin;

    .line 37
    sget-object v0, Lcom/squareup/print/util/RasterDocument$RasSpeed;->FULL:Lcom/squareup/print/util/RasterDocument$RasSpeed;

    iput-object v0, p0, Lcom/squareup/print/util/RasterDocument;->mSpeed:Lcom/squareup/print/util/RasterDocument$RasSpeed;

    .line 38
    sget-object v0, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;->DEFAULT:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    iput-object v0, p0, Lcom/squareup/print/util/RasterDocument;->mFFMode:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    .line 39
    sget-object v0, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;->DEFAULT:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    iput-object v0, p0, Lcom/squareup/print/util/RasterDocument;->mEOTMode:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    const/4 v0, 0x0

    .line 40
    iput v0, p0, Lcom/squareup/print/util/RasterDocument;->mPageLength:I

    .line 41
    iput v0, p0, Lcom/squareup/print/util/RasterDocument;->mLeftMargin:I

    .line 42
    iput v0, p0, Lcom/squareup/print/util/RasterDocument;->mRightMargin:I

    return-void
.end method

.method public constructor <init>(Lcom/squareup/print/util/RasterDocument$RasSpeed;Lcom/squareup/print/util/RasterDocument$RasPageEndMode;Lcom/squareup/print/util/RasterDocument$RasPageEndMode;Lcom/squareup/print/util/RasterDocument$RasTopMargin;III)V
    .locals 1

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    sget-object v0, Lcom/squareup/print/util/RasterDocument$RasTopMargin;->DEFAULT:Lcom/squareup/print/util/RasterDocument$RasTopMargin;

    iput-object v0, p0, Lcom/squareup/print/util/RasterDocument;->mTopMargin:Lcom/squareup/print/util/RasterDocument$RasTopMargin;

    .line 37
    sget-object v0, Lcom/squareup/print/util/RasterDocument$RasSpeed;->FULL:Lcom/squareup/print/util/RasterDocument$RasSpeed;

    iput-object v0, p0, Lcom/squareup/print/util/RasterDocument;->mSpeed:Lcom/squareup/print/util/RasterDocument$RasSpeed;

    .line 38
    sget-object v0, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;->DEFAULT:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    iput-object v0, p0, Lcom/squareup/print/util/RasterDocument;->mFFMode:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    .line 39
    sget-object v0, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;->DEFAULT:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    iput-object v0, p0, Lcom/squareup/print/util/RasterDocument;->mEOTMode:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    const/4 v0, 0x0

    .line 40
    iput v0, p0, Lcom/squareup/print/util/RasterDocument;->mPageLength:I

    .line 41
    iput v0, p0, Lcom/squareup/print/util/RasterDocument;->mLeftMargin:I

    .line 42
    iput v0, p0, Lcom/squareup/print/util/RasterDocument;->mRightMargin:I

    .line 50
    iput-object p1, p0, Lcom/squareup/print/util/RasterDocument;->mSpeed:Lcom/squareup/print/util/RasterDocument$RasSpeed;

    .line 51
    iput-object p3, p0, Lcom/squareup/print/util/RasterDocument;->mEOTMode:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    .line 52
    iput-object p2, p0, Lcom/squareup/print/util/RasterDocument;->mFFMode:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    .line 53
    iput-object p4, p0, Lcom/squareup/print/util/RasterDocument;->mTopMargin:Lcom/squareup/print/util/RasterDocument$RasTopMargin;

    .line 54
    iput p5, p0, Lcom/squareup/print/util/RasterDocument;->mPageLength:I

    .line 55
    iput p6, p0, Lcom/squareup/print/util/RasterDocument;->mLeftMargin:I

    .line 56
    iput p7, p0, Lcom/squareup/print/util/RasterDocument;->mRightMargin:I

    return-void
.end method

.method private endPageModeCommandValue(Lcom/squareup/print/util/RasterDocument$RasPageEndMode;)Ljava/lang/String;
    .locals 1

    .line 178
    sget-object v0, Lcom/squareup/print/util/RasterDocument$1;->$SwitchMap$com$squareup$print$util$RasterDocument$RasPageEndMode:[I

    invoke-virtual {p1}, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const-string v0, "0"

    packed-switch p1, :pswitch_data_0

    return-object v0

    :pswitch_0
    const-string p1, "37"

    return-object p1

    :pswitch_1
    const-string p1, "36"

    return-object p1

    :pswitch_2
    const-string p1, "13"

    return-object p1

    :pswitch_3
    const-string p1, "12"

    return-object p1

    :pswitch_4
    const-string p1, "9"

    return-object p1

    :pswitch_5
    const-string p1, "8"

    return-object p1

    :pswitch_6
    const-string p1, "3"

    return-object p1

    :pswitch_7
    const-string p1, "2"

    return-object p1

    :pswitch_8
    const-string p1, "1"

    return-object p1

    :pswitch_9
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private leftMarginCommand(I)Ljava/lang/String;
    .locals 2

    .line 166
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\u001b*rml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\u0000"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private pageLengthCommand(I)Ljava/lang/String;
    .locals 2

    .line 174
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\u001b*rP"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\u0000"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private rightMarginCommand(I)Ljava/lang/String;
    .locals 2

    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\u001b*rmr"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\u0000"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private setEndOfDocModeCommand(Lcom/squareup/print/util/RasterDocument$RasPageEndMode;)Ljava/lang/String;
    .locals 2

    .line 209
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\u001b*rE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0, p1}, Lcom/squareup/print/util/RasterDocument;->endPageModeCommandValue(Lcom/squareup/print/util/RasterDocument$RasPageEndMode;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\u0000"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private setEndOfPageModeCommand(Lcom/squareup/print/util/RasterDocument$RasPageEndMode;)Ljava/lang/String;
    .locals 2

    .line 205
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\u001b*rF"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0, p1}, Lcom/squareup/print/util/RasterDocument;->endPageModeCommandValue(Lcom/squareup/print/util/RasterDocument$RasPageEndMode;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\u0000"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private speedCommand(Lcom/squareup/print/util/RasterDocument$RasSpeed;)Ljava/lang/String;
    .locals 2

    .line 153
    sget-object v0, Lcom/squareup/print/util/RasterDocument$1;->$SwitchMap$com$squareup$print$util$RasterDocument$RasSpeed:[I

    invoke-virtual {p1}, Lcom/squareup/print/util/RasterDocument$RasSpeed;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    const-string v1, "\u001b*rQ0\u0000"

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    return-object v1

    :cond_0
    const-string p1, "\u001b*rQ2\u0000"

    return-object p1

    :cond_1
    const-string p1, "\u001b*rQ1\u0000"

    return-object p1

    :cond_2
    return-object v1
.end method

.method private topMarginCommand(Lcom/squareup/print/util/RasterDocument$RasTopMargin;)Ljava/lang/String;
    .locals 2

    .line 138
    sget-object v0, Lcom/squareup/print/util/RasterDocument$1;->$SwitchMap$com$squareup$print$util$RasterDocument$RasTopMargin:[I

    invoke-virtual {p1}, Lcom/squareup/print/util/RasterDocument$RasTopMargin;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    const-string v1, "\u001b*rT0\u0000"

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    return-object v1

    :cond_0
    const-string p1, "\u001b*rT2\u0000"

    return-object p1

    :cond_1
    const-string p1, "\u001b*rT1\u0000"

    return-object p1

    :cond_2
    return-object v1
.end method


# virtual methods
.method public beginDocumentCommandData()[B
    .locals 2

    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\u001b*rA"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/util/RasterDocument;->mTopMargin:Lcom/squareup/print/util/RasterDocument$RasTopMargin;

    invoke-direct {p0, v1}, Lcom/squareup/print/util/RasterDocument;->topMarginCommand(Lcom/squareup/print/util/RasterDocument$RasTopMargin;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 118
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/print/util/RasterDocument;->getPrintSpeed()Lcom/squareup/print/util/RasterDocument$RasSpeed;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/print/util/RasterDocument;->speedCommand(Lcom/squareup/print/util/RasterDocument$RasSpeed;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 119
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/squareup/print/util/RasterDocument;->mPageLength:I

    invoke-direct {p0, v0}, Lcom/squareup/print/util/RasterDocument;->pageLengthCommand(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 120
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/squareup/print/util/RasterDocument;->mLeftMargin:I

    invoke-direct {p0, v0}, Lcom/squareup/print/util/RasterDocument;->leftMarginCommand(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 121
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/squareup/print/util/RasterDocument;->mRightMargin:I

    invoke-direct {p0, v0}, Lcom/squareup/print/util/RasterDocument;->rightMarginCommand(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 122
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/print/util/RasterDocument;->getEndOfDocumentBehaviour()Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/print/util/RasterDocument;->setEndOfPageModeCommand(Lcom/squareup/print/util/RasterDocument$RasPageEndMode;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 123
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/print/util/RasterDocument;->getEndOfDocumentBehaviour()Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/print/util/RasterDocument;->setEndOfDocModeCommand(Lcom/squareup/print/util/RasterDocument$RasPageEndMode;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 124
    sget-object v1, Lcom/squareup/util/Strings;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    return-object v0
.end method

.method public endDocumentCommandData()[B
    .locals 2

    .line 128
    sget-object v0, Lcom/squareup/util/Strings;->US_ASCII:Ljava/nio/charset/Charset;

    const-string v1, "\u001b*rB"

    invoke-virtual {v1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    return-object v0
.end method

.method public getEndOfDocumentBehaviour()Lcom/squareup/print/util/RasterDocument$RasPageEndMode;
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/print/util/RasterDocument;->mEOTMode:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    return-object v0
.end method

.method public getEndOfPageBehaviour()Lcom/squareup/print/util/RasterDocument$RasPageEndMode;
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/squareup/print/util/RasterDocument;->mFFMode:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    return-object v0
.end method

.method public getLeftMargin()I
    .locals 1

    .line 92
    iget v0, p0, Lcom/squareup/print/util/RasterDocument;->mLeftMargin:I

    return v0
.end method

.method public getPageLength()I
    .locals 1

    .line 108
    iget v0, p0, Lcom/squareup/print/util/RasterDocument;->mPageLength:I

    return v0
.end method

.method public getPrintSpeed()Lcom/squareup/print/util/RasterDocument$RasSpeed;
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/print/util/RasterDocument;->mSpeed:Lcom/squareup/print/util/RasterDocument$RasSpeed;

    return-object v0
.end method

.method public getRightMargin()I
    .locals 1

    .line 100
    iget v0, p0, Lcom/squareup/print/util/RasterDocument;->mRightMargin:I

    return v0
.end method

.method public getTopMargin()Lcom/squareup/print/util/RasterDocument$RasTopMargin;
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/print/util/RasterDocument;->mTopMargin:Lcom/squareup/print/util/RasterDocument$RasTopMargin;

    return-object v0
.end method

.method public pageBreakCommandData()[B
    .locals 2

    .line 132
    sget-object v0, Lcom/squareup/util/Strings;->US_ASCII:Ljava/nio/charset/Charset;

    const-string v1, "\u001b\u000c\u0000"

    invoke-virtual {v1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    return-object v0
.end method

.method public setEndOfDocumentBehaviour(Lcom/squareup/print/util/RasterDocument$RasPageEndMode;)V
    .locals 0

    .line 88
    iput-object p1, p0, Lcom/squareup/print/util/RasterDocument;->mEOTMode:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    return-void
.end method

.method public setEndOfPageBehaviour(Lcom/squareup/print/util/RasterDocument$RasPageEndMode;)V
    .locals 0

    .line 80
    iput-object p1, p0, Lcom/squareup/print/util/RasterDocument;->mFFMode:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    return-void
.end method

.method public setLeftMargin(I)V
    .locals 0

    .line 96
    iput p1, p0, Lcom/squareup/print/util/RasterDocument;->mLeftMargin:I

    return-void
.end method

.method public setPageLength(I)V
    .locals 0

    .line 112
    iput p1, p0, Lcom/squareup/print/util/RasterDocument;->mPageLength:I

    return-void
.end method

.method public setPrintSpeed(Lcom/squareup/print/util/RasterDocument$RasSpeed;)V
    .locals 0

    .line 72
    iput-object p1, p0, Lcom/squareup/print/util/RasterDocument;->mSpeed:Lcom/squareup/print/util/RasterDocument$RasSpeed;

    return-void
.end method

.method public setRightMargin(I)V
    .locals 0

    .line 104
    iput p1, p0, Lcom/squareup/print/util/RasterDocument;->mRightMargin:I

    return-void
.end method

.method public setTopMargin(Lcom/squareup/print/util/RasterDocument$RasTopMargin;)V
    .locals 0

    .line 64
    iput-object p1, p0, Lcom/squareup/print/util/RasterDocument;->mTopMargin:Lcom/squareup/print/util/RasterDocument$RasTopMargin;

    return-void
.end method
