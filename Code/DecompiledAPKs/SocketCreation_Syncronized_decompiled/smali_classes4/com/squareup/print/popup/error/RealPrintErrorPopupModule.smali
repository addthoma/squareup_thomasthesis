.class public abstract Lcom/squareup/print/popup/error/RealPrintErrorPopupModule;
.super Ljava/lang/Object;
.source "RealPrintErrorPopupModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindPrintErrorPopupViewBinder(Lcom/squareup/ui/main/RealPrintErrorPopupViewBinder;)Lcom/squareup/ui/main/PrintErrorPopupViewBinder;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
