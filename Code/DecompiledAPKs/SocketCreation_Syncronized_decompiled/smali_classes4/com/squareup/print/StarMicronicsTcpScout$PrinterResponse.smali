.class Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;
.super Ljava/lang/Object;
.source "StarMicronicsTcpScout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/StarMicronicsTcpScout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PrinterResponse"
.end annotation


# instance fields
.field final hardwareInfo:Lcom/squareup/print/HardwarePrinter$HardwareInfo;

.field final id:Ljava/lang/String;

.field final lastSeenTime:J

.field final macAddress:Ljava/lang/String;

.field final manufacturer:Ljava/lang/String;

.field final model:Ljava/lang/String;

.field final networkIpAddress:Ljava/lang/String;

.field final printerReportedIpAddress:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;[BJ)V
    .locals 7

    .line 332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 333
    iput-object p1, p0, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;->networkIpAddress:Ljava/lang/String;

    .line 335
    sget-object p1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/Object;

    const/16 v2, 0x58

    aget-byte v2, p2, v2

    and-int/lit16 v2, v2, 0xff

    .line 336
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/16 v2, 0x59

    aget-byte v2, p2, v2

    and-int/lit16 v2, v2, 0xff

    .line 337
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const/16 v2, 0x5a

    aget-byte v2, p2, v2

    and-int/lit16 v2, v2, 0xff

    .line 338
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x2

    aput-object v2, v1, v5

    const/16 v2, 0x5b

    aget-byte v2, p2, v2

    and-int/lit16 v2, v2, 0xff

    .line 339
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v6, 0x3

    aput-object v2, v1, v6

    const-string v2, "%d.%d.%d.%d"

    .line 335
    invoke-static {p1, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;->printerReportedIpAddress:Ljava/lang/String;

    .line 341
    sget-object p1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/16 v2, 0x4e

    aget-byte v2, p2, v2

    and-int/lit16 v2, v2, 0xff

    .line 342
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const/16 v2, 0x4f

    aget-byte v2, p2, v2

    and-int/lit16 v2, v2, 0xff

    .line 343
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/16 v2, 0x50

    aget-byte v2, p2, v2

    and-int/lit16 v2, v2, 0xff

    .line 344
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    const/16 v2, 0x51

    aget-byte v2, p2, v2

    and-int/lit16 v2, v2, 0xff

    .line 345
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    const/16 v2, 0x52

    aget-byte v2, p2, v2

    and-int/lit16 v2, v2, 0xff

    .line 346
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    const/16 v0, 0x53

    aget-byte v0, p2, v0

    and-int/lit16 v0, v0, 0xff

    .line 347
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v2, 0x5

    aput-object v0, v1, v2

    const-string v0, "%02x:%02x:%02x:%02x:%02x:%02x"

    .line 341
    invoke-static {p1, v0, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;->macAddress:Ljava/lang/String;

    .line 349
    new-instance p1, Ljava/lang/String;

    sget-object v0, Lcom/squareup/util/Strings;->US_ASCII:Ljava/nio/charset/Charset;

    const/16 v1, 0xcc

    const/16 v2, 0x40

    invoke-direct {p1, p2, v1, v2, v0}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    .line 350
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;->model:Ljava/lang/String;

    .line 352
    iget-object p1, p0, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;->model:Ljava/lang/String;

    const-string p2, "KDS"

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "Fresh"

    goto :goto_0

    :cond_0
    const-string p1, "Star"

    :goto_0
    iput-object p1, p0, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;->manufacturer:Ljava/lang/String;

    .line 354
    new-instance p1, Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    iget-object v1, p0, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;->manufacturer:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;->model:Ljava/lang/String;

    sget-object v3, Lcom/squareup/print/ConnectionType;->TCP:Lcom/squareup/print/ConnectionType;

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;->macAddress:Ljava/lang/String;

    move-object v0, p1

    invoke-direct/range {v0 .. v6}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/print/ConnectionType;ZZLjava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;->hardwareInfo:Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    .line 356
    iget-object p1, p0, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;->hardwareInfo:Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->getId()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;->id:Ljava/lang/String;

    .line 358
    iput-wide p3, p0, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;->lastSeenTime:J

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .line 362
    iget-object v0, p0, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;->id:Ljava/lang/String;

    return-object v0
.end method
