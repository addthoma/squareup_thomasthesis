.class public Lcom/squareup/print/payload/StubPayload;
.super Lcom/squareup/print/PrintablePayload;
.source "StubPayload.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/payload/StubPayload$Factory;
    }
.end annotation


# instance fields
.field public final businessName:Ljava/lang/String;

.field public final date:Ljava/lang/String;

.field public final employeeFirstName:Ljava/lang/String;

.field public final ticketName:Ljava/lang/String;

.field public final time:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 53
    invoke-direct {p0}, Lcom/squareup/print/PrintablePayload;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/squareup/print/payload/StubPayload;->ticketName:Ljava/lang/String;

    .line 55
    iput-object p2, p0, Lcom/squareup/print/payload/StubPayload;->businessName:Ljava/lang/String;

    .line 56
    iput-object p3, p0, Lcom/squareup/print/payload/StubPayload;->date:Ljava/lang/String;

    .line 57
    iput-object p4, p0, Lcom/squareup/print/payload/StubPayload;->time:Ljava/lang/String;

    .line 58
    iput-object p5, p0, Lcom/squareup/print/payload/StubPayload;->employeeFirstName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_c

    .line 63
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_5

    .line 65
    :cond_1
    check-cast p1, Lcom/squareup/print/payload/StubPayload;

    .line 67
    iget-object v2, p0, Lcom/squareup/print/payload/StubPayload;->ticketName:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v3, p1, Lcom/squareup/print/payload/StubPayload;->ticketName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0

    :cond_2
    iget-object v2, p1, Lcom/squareup/print/payload/StubPayload;->ticketName:Ljava/lang/String;

    if-eqz v2, :cond_3

    :goto_0
    return v1

    .line 70
    :cond_3
    iget-object v2, p0, Lcom/squareup/print/payload/StubPayload;->businessName:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v3, p1, Lcom/squareup/print/payload/StubPayload;->businessName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    goto :goto_1

    :cond_4
    iget-object v2, p1, Lcom/squareup/print/payload/StubPayload;->businessName:Ljava/lang/String;

    if-eqz v2, :cond_5

    :goto_1
    return v1

    .line 74
    :cond_5
    iget-object v2, p0, Lcom/squareup/print/payload/StubPayload;->employeeFirstName:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v3, p1, Lcom/squareup/print/payload/StubPayload;->employeeFirstName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    goto :goto_2

    :cond_6
    iget-object v2, p1, Lcom/squareup/print/payload/StubPayload;->employeeFirstName:Ljava/lang/String;

    if-eqz v2, :cond_7

    :goto_2
    return v1

    .line 78
    :cond_7
    iget-object v2, p0, Lcom/squareup/print/payload/StubPayload;->date:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v3, p1, Lcom/squareup/print/payload/StubPayload;->date:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    goto :goto_3

    :cond_8
    iget-object v2, p1, Lcom/squareup/print/payload/StubPayload;->date:Ljava/lang/String;

    if-eqz v2, :cond_9

    :goto_3
    return v1

    .line 79
    :cond_9
    iget-object v2, p0, Lcom/squareup/print/payload/StubPayload;->time:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/print/payload/StubPayload;->time:Ljava/lang/String;

    if-eqz v2, :cond_a

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_b

    goto :goto_4

    :cond_a
    if-eqz p1, :cond_b

    :goto_4
    return v1

    :cond_b
    return v0

    :cond_c
    :goto_5
    return v1
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterActionName;
    .locals 1

    .line 104
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_STUB_PAPER:Lcom/squareup/analytics/RegisterActionName;

    return-object v0
.end method

.method public getAnalyticsPrintJobType()Ljava/lang/String;
    .locals 1

    const-string v0, "ticket_stub"

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .line 85
    iget-object v0, p0, Lcom/squareup/print/payload/StubPayload;->ticketName:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 86
    iget-object v2, p0, Lcom/squareup/print/payload/StubPayload;->businessName:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 87
    iget-object v2, p0, Lcom/squareup/print/payload/StubPayload;->employeeFirstName:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 88
    iget-object v2, p0, Lcom/squareup/print/payload/StubPayload;->date:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 89
    iget-object v2, p0, Lcom/squareup/print/payload/StubPayload;->time:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;Z)Landroid/graphics/Bitmap;
    .locals 0

    .line 95
    new-instance p2, Lcom/squareup/print/StubRenderer;

    invoke-direct {p2, p1}, Lcom/squareup/print/StubRenderer;-><init>(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 96
    invoke-virtual {p2, p0}, Lcom/squareup/print/StubRenderer;->renderBitmap(Lcom/squareup/print/payload/StubPayload;)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method
