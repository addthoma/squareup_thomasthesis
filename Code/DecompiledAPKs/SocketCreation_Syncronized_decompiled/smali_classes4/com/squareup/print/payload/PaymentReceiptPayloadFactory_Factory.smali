.class public final Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;
.super Ljava/lang/Object;
.source "PaymentReceiptPayloadFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final localeOverrideFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/locale/LocaleOverrideFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final paperSignatureSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final percentageFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;"
        }
    .end annotation
.end field

.field private final phoneNumbersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final receiptInfoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final taxFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;"
        }
    .end annotation
.end field

.field private final tipSectionFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/papersig/TipSectionFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final voidCompSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/papersig/TipSectionFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/locale/LocaleOverrideFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;",
            ">;)V"
        }
    .end annotation

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p2, p0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p3, p0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;->tipSectionFactoryProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p4, p0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;->paperSignatureSettingsProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p5, p0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p6, p0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;->voidCompSettingsProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p7, p0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;->percentageFormatterProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p8, p0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;->taxFormatterProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p9, p0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;->localeOverrideFactoryProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p10, p0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;->phoneNumbersProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p11, p0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;->localeProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p12, p0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 78
    iput-object p13, p0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;->receiptInfoProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/papersig/TipSectionFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/locale/LocaleOverrideFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;",
            ">;)",
            "Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;"
        }
    .end annotation

    .line 98
    new-instance v14, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v14
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/print/papersig/TipSectionFactory;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/text/PhoneNumberHelper;Ljavax/inject/Provider;Lcom/squareup/text/Formatter;Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;)Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/print/papersig/TipSectionFactory;",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/locale/LocaleOverrideFactory;",
            "Lcom/squareup/text/PhoneNumberHelper;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;",
            ")",
            "Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;"
        }
    .end annotation

    .line 108
    new-instance v14, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/print/papersig/TipSectionFactory;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/text/PhoneNumberHelper;Ljavax/inject/Provider;Lcom/squareup/text/Formatter;Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;)V

    return-object v14
.end method


# virtual methods
.method public get()Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;
    .locals 14

    .line 83
    iget-object v0, p0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;->tipSectionFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/print/papersig/TipSectionFactory;

    iget-object v0, p0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;->paperSignatureSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/papersignature/PaperSignatureSettings;

    iget-object v0, p0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v0, p0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;->voidCompSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    iget-object v0, p0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;->percentageFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;->taxFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;->localeOverrideFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/locale/LocaleOverrideFactory;

    iget-object v0, p0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;->phoneNumbersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/text/PhoneNumberHelper;

    iget-object v11, p0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;->localeProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;->receiptInfoProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;

    invoke-static/range {v1 .. v13}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/print/papersig/TipSectionFactory;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/text/PhoneNumberHelper;Ljavax/inject/Provider;Lcom/squareup/text/Formatter;Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;)Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory_Factory;->get()Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;

    move-result-object v0

    return-object v0
.end method
