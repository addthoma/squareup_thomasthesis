.class public Lcom/squareup/print/payload/TestReceiptPayloadFactory;
.super Lcom/squareup/print/payload/ReceiptPayloadFactory;
.source "TestReceiptPayloadFactory.java"


# instance fields
.field private final formatter:Lcom/squareup/print/ReceiptFormatter;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/text/Formatter;Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;)V
    .locals 15
    .param p9    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/money/ForTaxPercentage;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/print/ReceiptFormatter;",
            "Lcom/squareup/settings/server/Features;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/PhoneNumberHelper;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v14, p0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p4

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    move-object/from16 v9, p1

    move-object/from16 v10, p10

    move-object/from16 v11, p5

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    .line 56
    invoke-direct/range {v0 .. v13}, Lcom/squareup/print/payload/ReceiptPayloadFactory;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/print/papersig/TipSectionFactory;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;Ljavax/inject/Provider;Lcom/squareup/text/Formatter;Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;)V

    move-object/from16 v0, p1

    .line 59
    iput-object v0, v14, Lcom/squareup/print/payload/TestReceiptPayloadFactory;->res:Lcom/squareup/util/Res;

    move-object/from16 v0, p3

    .line 60
    iput-object v0, v14, Lcom/squareup/print/payload/TestReceiptPayloadFactory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    return-void
.end method


# virtual methods
.method public createPayload(Ljava/lang/String;Lcom/squareup/payment/Order;)Lcom/squareup/print/payload/ReceiptPayload;
    .locals 42

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 64
    invoke-static/range {p2 .. p2}, Lcom/squareup/util/TaxBreakdown;->fromOrder(Lcom/squareup/payment/Order;)Lcom/squareup/util/TaxBreakdown;

    move-result-object v9

    if-nez v1, :cond_0

    .line 68
    iget-object v1, v0, Lcom/squareup/print/payload/TestReceiptPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/print/R$string;->buyer_printed_receipt_test_title:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 70
    :cond_0
    iget-object v2, v0, Lcom/squareup/print/payload/TestReceiptPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/print/R$string;->buyer_printed_receipt_test_title_with_printer_name:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    const-string v3, "printer_nickname"

    .line 71
    invoke-virtual {v2, v3, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 72
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 73
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    move-object v5, v1

    .line 77
    iget-object v1, v0, Lcom/squareup/print/payload/TestReceiptPayloadFactory;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/payment/Order;->getEmployeeToken()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/permissions/EmployeeManagement;->getEmployeeByToken(Ljava/lang/String;)Lcom/squareup/permissions/Employee;

    move-result-object v13

    .line 79
    iget-object v10, v0, Lcom/squareup/print/payload/TestReceiptPayloadFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v11, v0, Lcom/squareup/print/payload/TestReceiptPayloadFactory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    new-instance v12, Ljava/util/Date;

    invoke-direct {v12}, Ljava/util/Date;-><init>()V

    sget-object v14, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    const/4 v15, 0x0

    const/16 v16, 0x0

    iget-object v1, v0, Lcom/squareup/print/payload/TestReceiptPayloadFactory;->features:Lcom/squareup/settings/server/Features;

    move-object/from16 v17, v1

    .line 80
    invoke-static/range {v10 .. v17}, Lcom/squareup/print/sections/HeaderSection;->createSection(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/print/ReceiptFormatter;Ljava/util/Date;Lcom/squareup/permissions/Employee;Lcom/squareup/print/payload/ReceiptPayload$RenderType;ZZLcom/squareup/settings/server/Features;)Lcom/squareup/print/sections/HeaderSection;

    move-result-object v18

    .line 83
    iget-object v2, v0, Lcom/squareup/print/payload/TestReceiptPayloadFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v3, v0, Lcom/squareup/print/payload/TestReceiptPayloadFactory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v4, p2

    .line 84
    invoke-static/range {v2 .. v8}, Lcom/squareup/print/sections/CodesSection;->fromOrder(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/print/sections/CodesSection;

    move-result-object v19

    .line 86
    new-instance v10, Lcom/squareup/print/sections/EmvSection;

    const/4 v11, 0x0

    invoke-direct {v10, v11, v11, v11}, Lcom/squareup/print/sections/EmvSection;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    iget-object v1, v0, Lcom/squareup/print/payload/TestReceiptPayloadFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v2, v0, Lcom/squareup/print/payload/TestReceiptPayloadFactory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    iget-object v5, v0, Lcom/squareup/print/payload/TestReceiptPayloadFactory;->features:Lcom/squareup/settings/server/Features;

    iget-object v3, v0, Lcom/squareup/print/payload/TestReceiptPayloadFactory;->localeProvider:Ljavax/inject/Provider;

    .line 90
    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    move-object v6, v3

    check-cast v6, Ljava/util/Locale;

    iget-object v3, v0, Lcom/squareup/print/payload/TestReceiptPayloadFactory;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v3}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isCompEnabled()Z

    move-result v7

    iget-object v8, v0, Lcom/squareup/print/payload/TestReceiptPayloadFactory;->res:Lcom/squareup/util/Res;

    move-object/from16 v3, p2

    move-object v4, v9

    .line 89
    invoke-static/range {v1 .. v8}, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->fromOrder(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/util/TaxBreakdown;Lcom/squareup/settings/server/Features;Ljava/util/Locale;ZLcom/squareup/util/Res;)Lcom/squareup/print/sections/ItemsAndDiscountsSection;

    move-result-object v21

    .line 92
    iget-object v1, v0, Lcom/squareup/print/payload/TestReceiptPayloadFactory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v2, v0, Lcom/squareup/print/payload/TestReceiptPayloadFactory;->features:Lcom/squareup/settings/server/Features;

    sget-object v5, Lcom/squareup/settings/server/Features$Feature;->CAN_SEE_AUTO_GRATUITY:Lcom/squareup/settings/server/Features$Feature;

    .line 93
    invoke-interface {v2, v5}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v5

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object/from16 v2, p2

    .line 92
    invoke-static/range {v1 .. v7}, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->fromOrder(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZZZ)Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

    move-result-object v22

    .line 97
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/payment/Order;->hasReturn()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 98
    iget-object v1, v0, Lcom/squareup/print/payload/TestReceiptPayloadFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v2, v0, Lcom/squareup/print/payload/TestReceiptPayloadFactory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    .line 99
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/payment/Order;->getReturnCart()Lcom/squareup/checkout/ReturnCart;

    move-result-object v3

    iget-object v5, v0, Lcom/squareup/print/payload/TestReceiptPayloadFactory;->features:Lcom/squareup/settings/server/Features;

    iget-object v4, v0, Lcom/squareup/print/payload/TestReceiptPayloadFactory;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    .line 100
    invoke-virtual {v4}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isCompEnabled()Z

    move-result v6

    iget-object v7, v0, Lcom/squareup/print/payload/TestReceiptPayloadFactory;->res:Lcom/squareup/util/Res;

    move-object v4, v9

    .line 98
    invoke-static/range {v1 .. v7}, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->fromReturnCart(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/checkout/ReturnCart;Lcom/squareup/util/TaxBreakdown;Lcom/squareup/settings/server/Features;ZLcom/squareup/util/Res;)Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;

    move-result-object v1

    .line 102
    iget-object v2, v0, Lcom/squareup/print/payload/TestReceiptPayloadFactory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    .line 104
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/payment/Order;->getReturnCart()Lcom/squareup/checkout/ReturnCart;

    move-result-object v3

    .line 102
    invoke-static {v2, v3}, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->fromReturnCart(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/checkout/ReturnCart;)Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;

    move-result-object v2

    move-object/from16 v26, v1

    move-object/from16 v27, v2

    goto :goto_1

    :cond_1
    move-object/from16 v26, v11

    move-object/from16 v27, v26

    .line 108
    :goto_1
    iget-object v1, v0, Lcom/squareup/print/payload/TestReceiptPayloadFactory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    .line 109
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/payment/Order;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v2

    iget-object v3, v0, Lcom/squareup/print/payload/TestReceiptPayloadFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object/from16 v4, p2

    invoke-static {v1, v4, v9, v2, v3}, Lcom/squareup/print/sections/TotalSection;->fromOrder(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/util/TaxBreakdown;Lcom/squareup/protos/common/Money;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/print/sections/TotalSection;

    move-result-object v23

    .line 111
    iget-object v1, v0, Lcom/squareup/print/payload/TestReceiptPayloadFactory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    const/16 v36, 0x0

    const/16 v37, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    const/16 v40, 0x0

    const/16 v41, 0x0

    move-object/from16 v28, v1

    move-object/from16 v29, p2

    .line 112
    invoke-static/range {v28 .. v41}, Lcom/squareup/print/sections/TenderSection;->fromOrder(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZLcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/print/payload/LabelAmountPair;)Lcom/squareup/print/sections/TenderSection;

    move-result-object v24

    .line 115
    new-instance v1, Lcom/squareup/print/sections/RefundsSection;

    move-object/from16 v25, v1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/print/sections/RefundsSection;-><init>(Ljava/util/List;)V

    .line 116
    iget-object v11, v0, Lcom/squareup/print/payload/TestReceiptPayloadFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v12, v0, Lcom/squareup/print/payload/TestReceiptPayloadFactory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    iget-object v14, v0, Lcom/squareup/print/payload/TestReceiptPayloadFactory;->features:Lcom/squareup/settings/server/Features;

    const/4 v15, 0x1

    const/16 v16, 0x0

    move-object/from16 v13, p2

    .line 117
    invoke-static/range {v11 .. v16}, Lcom/squareup/print/sections/FooterSection;->fromOrder(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/settings/server/Features;ZZ)Lcom/squareup/print/sections/FooterSection;

    move-result-object v31

    .line 119
    new-instance v1, Lcom/squareup/print/payload/ReceiptPayload;

    move-object/from16 v17, v1

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v32, 0x0

    sget-object v33, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    sget-object v34, Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;->ITEMIZED_RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;

    move-object/from16 v20, v10

    invoke-direct/range {v17 .. v34}, Lcom/squareup/print/payload/ReceiptPayload;-><init>(Lcom/squareup/print/sections/HeaderSection;Lcom/squareup/print/sections/CodesSection;Lcom/squareup/print/sections/EmvSection;Lcom/squareup/print/sections/ItemsAndDiscountsSection;Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;Lcom/squareup/print/sections/TotalSection;Lcom/squareup/print/sections/TenderSection;Lcom/squareup/print/sections/RefundsSection;Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;Lcom/squareup/print/papersig/TipSections;Lcom/squareup/print/papersig/SignatureSection;Lcom/squareup/print/sections/MultipleTaxBreakdownSection;Lcom/squareup/print/sections/FooterSection;Lcom/squareup/print/sections/BarcodeSection;Lcom/squareup/print/payload/ReceiptPayload$RenderType;Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;)V

    return-object v1
.end method
