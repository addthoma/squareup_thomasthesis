.class public final Lcom/squareup/print/PrintModule_ProvideStarMicronicsUsbScoutFactory;
.super Ljava/lang/Object;
.source "PrintModule_ProvideStarMicronicsUsbScoutFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/print/StarMicronicsUsbScout;",
        ">;"
    }
.end annotation


# instance fields
.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final starMironicsPrinterFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/StarMicronicsPrinter$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final usbManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hardware/usb/UsbManager;",
            ">;"
        }
    .end annotation
.end field

.field private final usbPortMapperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/usb/UsbPortMapper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hardware/usb/UsbManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/usb/UsbPortMapper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/StarMicronicsPrinter$Factory;",
            ">;)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/print/PrintModule_ProvideStarMicronicsUsbScoutFactory;->contextProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p2, p0, Lcom/squareup/print/PrintModule_ProvideStarMicronicsUsbScoutFactory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p3, p0, Lcom/squareup/print/PrintModule_ProvideStarMicronicsUsbScoutFactory;->usbManagerProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p4, p0, Lcom/squareup/print/PrintModule_ProvideStarMicronicsUsbScoutFactory;->featuresProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p5, p0, Lcom/squareup/print/PrintModule_ProvideStarMicronicsUsbScoutFactory;->usbPortMapperProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p6, p0, Lcom/squareup/print/PrintModule_ProvideStarMicronicsUsbScoutFactory;->starMironicsPrinterFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/print/PrintModule_ProvideStarMicronicsUsbScoutFactory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hardware/usb/UsbManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/usb/UsbPortMapper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/StarMicronicsPrinter$Factory;",
            ">;)",
            "Lcom/squareup/print/PrintModule_ProvideStarMicronicsUsbScoutFactory;"
        }
    .end annotation

    .line 56
    new-instance v7, Lcom/squareup/print/PrintModule_ProvideStarMicronicsUsbScoutFactory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/print/PrintModule_ProvideStarMicronicsUsbScoutFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static provideStarMicronicsUsbScout(Landroid/app/Application;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/settings/server/Features;Lcom/squareup/usb/UsbPortMapper;Lcom/squareup/print/StarMicronicsPrinter$Factory;)Lcom/squareup/print/StarMicronicsUsbScout;
    .locals 0

    .line 62
    invoke-static/range {p0 .. p5}, Lcom/squareup/print/PrintModule;->provideStarMicronicsUsbScout(Landroid/app/Application;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/settings/server/Features;Lcom/squareup/usb/UsbPortMapper;Lcom/squareup/print/StarMicronicsPrinter$Factory;)Lcom/squareup/print/StarMicronicsUsbScout;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/print/StarMicronicsUsbScout;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/print/StarMicronicsUsbScout;
    .locals 7

    .line 48
    iget-object v0, p0, Lcom/squareup/print/PrintModule_ProvideStarMicronicsUsbScoutFactory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/app/Application;

    iget-object v0, p0, Lcom/squareup/print/PrintModule_ProvideStarMicronicsUsbScoutFactory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/print/PrintModule_ProvideStarMicronicsUsbScoutFactory;->usbManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/hardware/usb/UsbManager;

    iget-object v0, p0, Lcom/squareup/print/PrintModule_ProvideStarMicronicsUsbScoutFactory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/print/PrintModule_ProvideStarMicronicsUsbScoutFactory;->usbPortMapperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/usb/UsbPortMapper;

    iget-object v0, p0, Lcom/squareup/print/PrintModule_ProvideStarMicronicsUsbScoutFactory;->starMironicsPrinterFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/print/StarMicronicsPrinter$Factory;

    invoke-static/range {v1 .. v6}, Lcom/squareup/print/PrintModule_ProvideStarMicronicsUsbScoutFactory;->provideStarMicronicsUsbScout(Landroid/app/Application;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/settings/server/Features;Lcom/squareup/usb/UsbPortMapper;Lcom/squareup/print/StarMicronicsPrinter$Factory;)Lcom/squareup/print/StarMicronicsUsbScout;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/print/PrintModule_ProvideStarMicronicsUsbScoutFactory;->get()Lcom/squareup/print/StarMicronicsUsbScout;

    move-result-object v0

    return-object v0
.end method
