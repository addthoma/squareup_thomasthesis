.class public Lcom/squareup/print/papersig/TipSectionFactory;
.super Ljava/lang/Object;
.source "TipSectionFactory.java"


# instance fields
.field private final historicalTippingCalculator:Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;

.field private final paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;Lcom/squareup/payment/Transaction;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/print/papersig/TipSectionFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 35
    iput-object p2, p0, Lcom/squareup/print/papersig/TipSectionFactory;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    .line 36
    iput-object p4, p0, Lcom/squareup/print/papersig/TipSectionFactory;->transaction:Lcom/squareup/payment/Transaction;

    .line 37
    iput-object p3, p0, Lcom/squareup/print/papersig/TipSectionFactory;->historicalTippingCalculator:Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;

    return-void
.end method

.method private createQuickTipSectionForTender(Lcom/squareup/payment/AcceptsTips;Lcom/squareup/protos/common/Money;Lcom/squareup/print/ReceiptFormatter;)Lcom/squareup/print/papersig/QuickTipSection;
    .locals 6

    .line 93
    invoke-interface {p1}, Lcom/squareup/payment/AcceptsTips;->getTipOptions()Ljava/util/List;

    move-result-object v1

    invoke-interface {p1}, Lcom/squareup/payment/AcceptsTips;->getCustomTipMaxMoney()Lcom/squareup/protos/common/Money;

    move-result-object v2

    iget-object p1, p0, Lcom/squareup/print/papersig/TipSectionFactory;->transaction:Lcom/squareup/payment/Transaction;

    .line 94
    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getTipSettings()Lcom/squareup/settings/server/TipSettings;

    move-result-object v4

    move-object v0, p0

    move-object v3, p2

    move-object v5, p3

    .line 93
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/print/papersig/TipSectionFactory;->createQuickTipSection(Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/settings/server/TipSettings;Lcom/squareup/print/ReceiptFormatter;)Lcom/squareup/print/papersig/QuickTipSection;

    move-result-object p1

    return-object p1
.end method

.method private createQuickTipSectionForTenderHistory(Lcom/squareup/billhistory/model/TenderHistory;Lcom/squareup/print/ReceiptFormatter;)Lcom/squareup/print/papersig/QuickTipSection;
    .locals 7

    .line 99
    iget-object v0, p0, Lcom/squareup/print/papersig/TipSectionFactory;->historicalTippingCalculator:Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;

    invoke-virtual {v0, p1}, Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;->calculateTipOptions(Lcom/squareup/billhistory/model/TenderHistory;)Ljava/util/List;

    move-result-object v2

    .line 100
    iget-object v0, p0, Lcom/squareup/print/papersig/TipSectionFactory;->historicalTippingCalculator:Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;

    invoke-virtual {v0, p1}, Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;->getCustomTipMaxMoney(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 101
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/TenderHistory;->amountExcludingTip()Lcom/squareup/protos/common/Money;

    move-result-object v4

    .line 102
    iget-object p1, p0, Lcom/squareup/print/papersig/TipSectionFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getTipSettings()Lcom/squareup/settings/server/TipSettings;

    move-result-object v5

    move-object v1, p0

    move-object v6, p2

    invoke-virtual/range {v1 .. v6}, Lcom/squareup/print/papersig/TipSectionFactory;->createQuickTipSection(Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/settings/server/TipSettings;Lcom/squareup/print/ReceiptFormatter;)Lcom/squareup/print/papersig/QuickTipSection;

    move-result-object p1

    return-object p1
.end method

.method private createTraditionalTipSection(Lcom/squareup/print/ReceiptFormatter;Z)Lcom/squareup/print/papersig/TraditionalTipSection;
    .locals 1

    .line 87
    new-instance v0, Lcom/squareup/print/papersig/TraditionalTipSection;

    invoke-virtual {p1, p2}, Lcom/squareup/print/ReceiptFormatter;->traditionalTipLabel(Z)Ljava/lang/String;

    move-result-object p2

    .line 88
    invoke-virtual {p1}, Lcom/squareup/print/ReceiptFormatter;->traditionalTipTotalLabel()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p2, p1}, Lcom/squareup/print/papersig/TraditionalTipSection;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private formatTipOptions(Ljava/util/List;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/print/ReceiptFormatter;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/print/ReceiptFormatter;",
            ")V"
        }
    .end annotation

    .line 137
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/tipping/TipOption;

    .line 138
    iget-object v1, v0, Lcom/squareup/protos/common/tipping/TipOption;->tip_money:Lcom/squareup/protos/common/Money;

    invoke-static {p2, v1}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 140
    iget-object v2, v0, Lcom/squareup/protos/common/tipping/TipOption;->percentage:Ljava/lang/Double;

    if-eqz v2, :cond_0

    .line 141
    iget-object v2, v0, Lcom/squareup/protos/common/tipping/TipOption;->percentage:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/squareup/util/Percentage;->fromDouble(D)Lcom/squareup/util/Percentage;

    move-result-object v2

    invoke-virtual {p5, v2}, Lcom/squareup/print/ReceiptFormatter;->tipPercentage(Lcom/squareup/util/Percentage;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 142
    iget-object v0, v0, Lcom/squareup/protos/common/tipping/TipOption;->tip_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {p5, v0, v1}, Lcom/squareup/print/ReceiptFormatter;->calculatedOption(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 144
    :cond_0
    iget-object v0, v0, Lcom/squareup/protos/common/tipping/TipOption;->tip_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {p5, v0}, Lcom/squareup/print/ReceiptFormatter;->formatMoney(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    invoke-virtual {p5, v1}, Lcom/squareup/print/ReceiptFormatter;->calculatedOption(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method createQuickTipSection(Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/settings/server/TipSettings;Lcom/squareup/print/ReceiptFormatter;)Lcom/squareup/print/papersig/QuickTipSection;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/settings/server/TipSettings;",
            "Lcom/squareup/print/ReceiptFormatter;",
            ")",
            "Lcom/squareup/print/papersig/QuickTipSection;"
        }
    .end annotation

    .line 109
    new-instance v6, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 110
    new-instance v7, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v7, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 113
    invoke-static {p4, p2}, Lcom/squareup/tipping/TippingCalculator;->shouldAskForCustomAmount(Lcom/squareup/settings/server/TipSettings;Lcom/squareup/protos/common/Money;)Z

    move-result p2

    const/4 p4, 0x0

    if-eqz p2, :cond_0

    .line 116
    invoke-virtual {p5}, Lcom/squareup/print/ReceiptFormatter;->quickTipCustomTipLabel()Ljava/lang/String;

    move-result-object v0

    .line 117
    invoke-virtual {p5}, Lcom/squareup/print/ReceiptFormatter;->quickTipCustomTotalLabel()Ljava/lang/String;

    move-result-object v1

    move-object v8, v0

    move-object v9, v1

    goto :goto_0

    :cond_0
    move-object v8, p4

    move-object v9, v8

    .line 120
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p2, :cond_1

    return-object p4

    :cond_1
    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, v6

    move-object v4, v7

    move-object v5, p5

    .line 124
    invoke-direct/range {v0 .. v5}, Lcom/squareup/print/papersig/TipSectionFactory;->formatTipOptions(Ljava/util/List;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/print/ReceiptFormatter;)V

    if-nez p2, :cond_2

    .line 127
    invoke-virtual {p5}, Lcom/squareup/print/ReceiptFormatter;->quickTipNoTipLabel()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v6, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string p1, ""

    .line 128
    invoke-interface {v7, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 131
    :cond_2
    new-instance p1, Lcom/squareup/print/papersig/QuickTipSection;

    invoke-virtual {p5}, Lcom/squareup/print/ReceiptFormatter;->quickTipTitle()Ljava/lang/String;

    move-result-object v1

    move-object v0, p1

    move-object v2, v6

    move-object v3, v7

    move-object v4, v8

    move-object v5, v9

    invoke-direct/range {v0 .. v5}, Lcom/squareup/print/papersig/QuickTipSection;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    return-object p1
.end method

.method public createTipSectionForTender(Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/print/ReceiptFormatter;)Lcom/squareup/print/papersig/TipSections;
    .locals 3

    .line 43
    iget-object v0, p0, Lcom/squareup/print/papersig/TipSectionFactory;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    invoke-virtual {v0}, Lcom/squareup/papersignature/PaperSignatureSettings;->isSignOnPrintedReceiptEnabled()Z

    move-result v0

    .line 44
    instance-of v1, p1, Lcom/squareup/payment/AcceptsTips;

    const/4 v2, 0x0

    if-eqz v0, :cond_3

    if-nez v1, :cond_0

    goto :goto_0

    .line 49
    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/squareup/payment/AcceptsTips;

    .line 50
    invoke-interface {v0}, Lcom/squareup/payment/AcceptsTips;->tipOnPrintedReceipt()Z

    move-result v1

    if-nez v1, :cond_1

    return-object v2

    .line 54
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/payment/tender/BaseTender;->hasAutoGratuity()Z

    move-result v1

    .line 55
    iget-object v2, p0, Lcom/squareup/print/papersig/TipSectionFactory;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    invoke-virtual {v2}, Lcom/squareup/papersignature/PaperSignatureSettings;->isQuickTipEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    if-nez v1, :cond_2

    .line 56
    new-instance v1, Lcom/squareup/print/papersig/TipSections;

    .line 57
    invoke-virtual {p1}, Lcom/squareup/payment/tender/BaseTender;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/print/papersig/TipSectionFactory;->createQuickTipSectionForTender(Lcom/squareup/payment/AcceptsTips;Lcom/squareup/protos/common/Money;Lcom/squareup/print/ReceiptFormatter;)Lcom/squareup/print/papersig/QuickTipSection;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/print/papersig/TipSections;-><init>(Lcom/squareup/print/papersig/QuickTipSection;)V

    return-object v1

    .line 59
    :cond_2
    new-instance p1, Lcom/squareup/print/papersig/TipSections;

    invoke-direct {p0, p2, v1}, Lcom/squareup/print/papersig/TipSectionFactory;->createTraditionalTipSection(Lcom/squareup/print/ReceiptFormatter;Z)Lcom/squareup/print/papersig/TraditionalTipSection;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/print/papersig/TipSections;-><init>(Lcom/squareup/print/papersig/TraditionalTipSection;)V

    return-object p1

    :cond_3
    :goto_0
    return-object v2
.end method

.method public createTipSectionForTenderHistory(Lcom/squareup/billhistory/model/TenderHistory;Lcom/squareup/print/ReceiptFormatter;)Lcom/squareup/print/papersig/TipSections;
    .locals 2

    .line 66
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/TenderHistory;->shouldPrintTipEntryInput()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 70
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/TenderHistory;->hasAutoGratuity()Z

    move-result v0

    .line 71
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/TenderHistory;->hasReceiptDisplayDetails()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 72
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/TenderHistory;->shouldDisplayQuickTipOptions()Z

    move-result v1

    if-eqz v1, :cond_1

    if-nez v0, :cond_1

    .line 73
    new-instance v0, Lcom/squareup/print/papersig/TipSections;

    invoke-direct {p0, p1, p2}, Lcom/squareup/print/papersig/TipSectionFactory;->createQuickTipSectionForTenderHistory(Lcom/squareup/billhistory/model/TenderHistory;Lcom/squareup/print/ReceiptFormatter;)Lcom/squareup/print/papersig/QuickTipSection;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/print/papersig/TipSections;-><init>(Lcom/squareup/print/papersig/QuickTipSection;)V

    return-object v0

    .line 75
    :cond_1
    new-instance p1, Lcom/squareup/print/papersig/TipSections;

    invoke-direct {p0, p2, v0}, Lcom/squareup/print/papersig/TipSectionFactory;->createTraditionalTipSection(Lcom/squareup/print/ReceiptFormatter;Z)Lcom/squareup/print/papersig/TraditionalTipSection;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/print/papersig/TipSections;-><init>(Lcom/squareup/print/papersig/TraditionalTipSection;)V

    return-object p1

    .line 79
    :cond_2
    iget-object v1, p0, Lcom/squareup/print/papersig/TipSectionFactory;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    invoke-virtual {v1}, Lcom/squareup/papersignature/PaperSignatureSettings;->isQuickTipEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    if-nez v0, :cond_3

    .line 80
    new-instance v0, Lcom/squareup/print/papersig/TipSections;

    invoke-direct {p0, p1, p2}, Lcom/squareup/print/papersig/TipSectionFactory;->createQuickTipSectionForTenderHistory(Lcom/squareup/billhistory/model/TenderHistory;Lcom/squareup/print/ReceiptFormatter;)Lcom/squareup/print/papersig/QuickTipSection;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/print/papersig/TipSections;-><init>(Lcom/squareup/print/papersig/QuickTipSection;)V

    return-object v0

    .line 82
    :cond_3
    new-instance p1, Lcom/squareup/print/papersig/TipSections;

    invoke-direct {p0, p2, v0}, Lcom/squareup/print/papersig/TipSectionFactory;->createTraditionalTipSection(Lcom/squareup/print/ReceiptFormatter;Z)Lcom/squareup/print/papersig/TraditionalTipSection;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/print/papersig/TipSections;-><init>(Lcom/squareup/print/papersig/TraditionalTipSection;)V

    return-object p1
.end method
