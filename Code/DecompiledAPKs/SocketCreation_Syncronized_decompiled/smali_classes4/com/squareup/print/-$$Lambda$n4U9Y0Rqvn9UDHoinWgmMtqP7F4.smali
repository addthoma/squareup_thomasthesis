.class public final synthetic Lcom/squareup/print/-$$Lambda$n4U9Y0Rqvn9UDHoinWgmMtqP7F4;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lcom/squareup/print/PrintQueueExecutor$PrintTask;


# static fields
.field public static final synthetic INSTANCE:Lcom/squareup/print/-$$Lambda$n4U9Y0Rqvn9UDHoinWgmMtqP7F4;


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/print/-$$Lambda$n4U9Y0Rqvn9UDHoinWgmMtqP7F4;

    invoke-direct {v0}, Lcom/squareup/print/-$$Lambda$n4U9Y0Rqvn9UDHoinWgmMtqP7F4;-><init>()V

    sput-object v0, Lcom/squareup/print/-$$Lambda$n4U9Y0Rqvn9UDHoinWgmMtqP7F4;->INSTANCE:Lcom/squareup/print/-$$Lambda$n4U9Y0Rqvn9UDHoinWgmMtqP7F4;

    return-void
.end method

.method private synthetic constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final perform(Lcom/squareup/print/PrintJobQueue;)Ljava/lang/Object;
    .locals 0

    invoke-interface {p1}, Lcom/squareup/print/PrintJobQueue;->retrieveAllPrintJobs()Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
