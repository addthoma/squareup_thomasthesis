.class public final Lcom/squareup/print/PrintModule_ProvideStarMicronicsTcpScoutFactory;
.super Ljava/lang/Object;
.source "PrintModule_ProvideStarMicronicsTcpScoutFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/print/StarMicronicsTcpScout;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadEnforcerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final socketProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/SocketProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final starMironicsPrinterFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/StarMicronicsPrinter$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/SocketProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/StarMicronicsPrinter$Factory;",
            ">;)V"
        }
    .end annotation

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/print/PrintModule_ProvideStarMicronicsTcpScoutFactory;->contextProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p2, p0, Lcom/squareup/print/PrintModule_ProvideStarMicronicsTcpScoutFactory;->socketProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p3, p0, Lcom/squareup/print/PrintModule_ProvideStarMicronicsTcpScoutFactory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p4, p0, Lcom/squareup/print/PrintModule_ProvideStarMicronicsTcpScoutFactory;->mainThreadEnforcerProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p5, p0, Lcom/squareup/print/PrintModule_ProvideStarMicronicsTcpScoutFactory;->clockProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p6, p0, Lcom/squareup/print/PrintModule_ProvideStarMicronicsTcpScoutFactory;->featuresProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p7, p0, Lcom/squareup/print/PrintModule_ProvideStarMicronicsTcpScoutFactory;->analyticsProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p8, p0, Lcom/squareup/print/PrintModule_ProvideStarMicronicsTcpScoutFactory;->starMironicsPrinterFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/print/PrintModule_ProvideStarMicronicsTcpScoutFactory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/SocketProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/StarMicronicsPrinter$Factory;",
            ">;)",
            "Lcom/squareup/print/PrintModule_ProvideStarMicronicsTcpScoutFactory;"
        }
    .end annotation

    .line 65
    new-instance v9, Lcom/squareup/print/PrintModule_ProvideStarMicronicsTcpScoutFactory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/print/PrintModule_ProvideStarMicronicsTcpScoutFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static provideStarMicronicsTcpScout(Landroid/app/Application;Lcom/squareup/print/SocketProvider;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/util/Clock;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lcom/squareup/print/StarMicronicsPrinter$Factory;)Lcom/squareup/print/StarMicronicsTcpScout;
    .locals 0

    .line 72
    invoke-static/range {p0 .. p7}, Lcom/squareup/print/PrintModule;->provideStarMicronicsTcpScout(Landroid/app/Application;Lcom/squareup/print/SocketProvider;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/util/Clock;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lcom/squareup/print/StarMicronicsPrinter$Factory;)Lcom/squareup/print/StarMicronicsTcpScout;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/print/StarMicronicsTcpScout;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/print/StarMicronicsTcpScout;
    .locals 9

    .line 56
    iget-object v0, p0, Lcom/squareup/print/PrintModule_ProvideStarMicronicsTcpScoutFactory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/app/Application;

    iget-object v0, p0, Lcom/squareup/print/PrintModule_ProvideStarMicronicsTcpScoutFactory;->socketProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/print/SocketProvider;

    iget-object v0, p0, Lcom/squareup/print/PrintModule_ProvideStarMicronicsTcpScoutFactory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/print/PrintModule_ProvideStarMicronicsTcpScoutFactory;->mainThreadEnforcerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iget-object v0, p0, Lcom/squareup/print/PrintModule_ProvideStarMicronicsTcpScoutFactory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/util/Clock;

    iget-object v0, p0, Lcom/squareup/print/PrintModule_ProvideStarMicronicsTcpScoutFactory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/print/PrintModule_ProvideStarMicronicsTcpScoutFactory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/print/PrintModule_ProvideStarMicronicsTcpScoutFactory;->starMironicsPrinterFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/print/StarMicronicsPrinter$Factory;

    invoke-static/range {v1 .. v8}, Lcom/squareup/print/PrintModule_ProvideStarMicronicsTcpScoutFactory;->provideStarMicronicsTcpScout(Landroid/app/Application;Lcom/squareup/print/SocketProvider;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/util/Clock;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lcom/squareup/print/StarMicronicsPrinter$Factory;)Lcom/squareup/print/StarMicronicsTcpScout;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/print/PrintModule_ProvideStarMicronicsTcpScoutFactory;->get()Lcom/squareup/print/StarMicronicsTcpScout;

    move-result-object v0

    return-object v0
.end method
