.class public Lcom/squareup/print/ThermalBitmapBuilder$Factory;
.super Ljava/lang/Object;
.source "ThermalBitmapBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/ThermalBitmapBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# instance fields
.field private final application:Landroid/app/Application;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final mainScheduler:Lrx/Scheduler;

.field private final picasso:Lcom/squareup/picasso/Picasso;

.field private final thumbor:Lcom/squareup/pollexor/Thumbor;


# direct methods
.method constructor <init>(Landroid/app/Application;Lcom/squareup/settings/server/Features;Lcom/squareup/picasso/Picasso;Lcom/squareup/pollexor/Thumbor;Lrx/Scheduler;)V
    .locals 0
    .param p5    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object p1, p0, Lcom/squareup/print/ThermalBitmapBuilder$Factory;->application:Landroid/app/Application;

    .line 76
    iput-object p2, p0, Lcom/squareup/print/ThermalBitmapBuilder$Factory;->features:Lcom/squareup/settings/server/Features;

    .line 77
    iput-object p3, p0, Lcom/squareup/print/ThermalBitmapBuilder$Factory;->picasso:Lcom/squareup/picasso/Picasso;

    .line 78
    iput-object p4, p0, Lcom/squareup/print/ThermalBitmapBuilder$Factory;->thumbor:Lcom/squareup/pollexor/Thumbor;

    .line 79
    iput-object p5, p0, Lcom/squareup/print/ThermalBitmapBuilder$Factory;->mainScheduler:Lrx/Scheduler;

    return-void
.end method


# virtual methods
.method public get(Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 9

    .line 83
    new-instance v8, Lcom/squareup/print/ThermalBitmapBuilder;

    iget-object v1, p0, Lcom/squareup/print/ThermalBitmapBuilder$Factory;->application:Landroid/app/Application;

    iget-object v2, p0, Lcom/squareup/print/ThermalBitmapBuilder$Factory;->features:Lcom/squareup/settings/server/Features;

    iget-object v3, p0, Lcom/squareup/print/ThermalBitmapBuilder$Factory;->picasso:Lcom/squareup/picasso/Picasso;

    iget-object v4, p0, Lcom/squareup/print/ThermalBitmapBuilder$Factory;->thumbor:Lcom/squareup/pollexor/Thumbor;

    iget-object v5, p0, Lcom/squareup/print/ThermalBitmapBuilder$Factory;->mainScheduler:Lrx/Scheduler;

    const/4 v7, 0x0

    move-object v0, v8

    move-object v6, p1

    invoke-direct/range {v0 .. v7}, Lcom/squareup/print/ThermalBitmapBuilder;-><init>(Landroid/content/Context;Lcom/squareup/settings/server/Features;Lcom/squareup/picasso/Picasso;Lcom/squareup/pollexor/Thumbor;Lrx/Scheduler;Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;Lcom/squareup/print/ThermalBitmapBuilder$1;)V

    return-object v8
.end method
