.class public final Lcom/squareup/print/VoidTicketPrintout;
.super Lcom/squareup/print/Printout;
.source "Printout.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0018\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0007H\u00c6\u0003J\u000f\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\n0\tH\u00c6\u0003J7\u0010\u001c\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u000e\u0008\u0002\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tH\u00c6\u0001J\u0013\u0010\u001d\u001a\u00020\u001e2\u0008\u0010\u001f\u001a\u0004\u0018\u00010 H\u00d6\u0003J\t\u0010!\u001a\u00020\"H\u00d6\u0001J\t\u0010#\u001a\u00020\u0005H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0014\u0010\u0012\u001a\u00020\u0013X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0017\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/print/VoidTicketPrintout;",
        "Lcom/squareup/print/Printout;",
        "orderSnapshot",
        "Lcom/squareup/payment/OrderSnapshot;",
        "orderDisplayName",
        "",
        "openTicket",
        "Lcom/squareup/tickets/OpenTicket;",
        "voidedItems",
        "",
        "Lcom/squareup/checkout/CartItem;",
        "(Lcom/squareup/payment/OrderSnapshot;Ljava/lang/String;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;)V",
        "getOpenTicket",
        "()Lcom/squareup/tickets/OpenTicket;",
        "getOrderDisplayName",
        "()Ljava/lang/String;",
        "getOrderSnapshot",
        "()Lcom/squareup/payment/OrderSnapshot;",
        "type",
        "Lcom/squareup/print/PrintoutType;",
        "getType",
        "()Lcom/squareup/print/PrintoutType;",
        "getVoidedItems",
        "()Ljava/util/List;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "print_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final openTicket:Lcom/squareup/tickets/OpenTicket;

.field private final orderDisplayName:Ljava/lang/String;

.field private final orderSnapshot:Lcom/squareup/payment/OrderSnapshot;

.field private final type:Lcom/squareup/print/PrintoutType;

.field private final voidedItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/payment/OrderSnapshot;Ljava/lang/String;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/OrderSnapshot;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/OpenTicket;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/CartItem;",
            ">;)V"
        }
    .end annotation

    const-string v0, "orderSnapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderDisplayName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "openTicket"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "voidedItems"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 57
    invoke-direct {p0, v0}, Lcom/squareup/print/Printout;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/print/VoidTicketPrintout;->orderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    iput-object p2, p0, Lcom/squareup/print/VoidTicketPrintout;->orderDisplayName:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/print/VoidTicketPrintout;->openTicket:Lcom/squareup/tickets/OpenTicket;

    iput-object p4, p0, Lcom/squareup/print/VoidTicketPrintout;->voidedItems:Ljava/util/List;

    .line 58
    sget-object p1, Lcom/squareup/print/PrintoutType;->VOID_TICKET:Lcom/squareup/print/PrintoutType;

    iput-object p1, p0, Lcom/squareup/print/VoidTicketPrintout;->type:Lcom/squareup/print/PrintoutType;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/print/VoidTicketPrintout;Lcom/squareup/payment/OrderSnapshot;Ljava/lang/String;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/print/VoidTicketPrintout;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/print/VoidTicketPrintout;->orderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/print/VoidTicketPrintout;->orderDisplayName:Ljava/lang/String;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/print/VoidTicketPrintout;->openTicket:Lcom/squareup/tickets/OpenTicket;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/print/VoidTicketPrintout;->voidedItems:Ljava/util/List;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/print/VoidTicketPrintout;->copy(Lcom/squareup/payment/OrderSnapshot;Ljava/lang/String;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;)Lcom/squareup/print/VoidTicketPrintout;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/payment/OrderSnapshot;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/VoidTicketPrintout;->orderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/VoidTicketPrintout;->orderDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Lcom/squareup/tickets/OpenTicket;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/VoidTicketPrintout;->openTicket:Lcom/squareup/tickets/OpenTicket;

    return-object v0
.end method

.method public final component4()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/print/VoidTicketPrintout;->voidedItems:Ljava/util/List;

    return-object v0
.end method

.method public final copy(Lcom/squareup/payment/OrderSnapshot;Ljava/lang/String;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;)Lcom/squareup/print/VoidTicketPrintout;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/OrderSnapshot;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/OpenTicket;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/CartItem;",
            ">;)",
            "Lcom/squareup/print/VoidTicketPrintout;"
        }
    .end annotation

    const-string v0, "orderSnapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderDisplayName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "openTicket"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "voidedItems"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/print/VoidTicketPrintout;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/print/VoidTicketPrintout;-><init>(Lcom/squareup/payment/OrderSnapshot;Ljava/lang/String;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/print/VoidTicketPrintout;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/print/VoidTicketPrintout;

    iget-object v0, p0, Lcom/squareup/print/VoidTicketPrintout;->orderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    iget-object v1, p1, Lcom/squareup/print/VoidTicketPrintout;->orderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/VoidTicketPrintout;->orderDisplayName:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/print/VoidTicketPrintout;->orderDisplayName:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/VoidTicketPrintout;->openTicket:Lcom/squareup/tickets/OpenTicket;

    iget-object v1, p1, Lcom/squareup/print/VoidTicketPrintout;->openTicket:Lcom/squareup/tickets/OpenTicket;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/VoidTicketPrintout;->voidedItems:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/print/VoidTicketPrintout;->voidedItems:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getOpenTicket()Lcom/squareup/tickets/OpenTicket;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/squareup/print/VoidTicketPrintout;->openTicket:Lcom/squareup/tickets/OpenTicket;

    return-object v0
.end method

.method public final getOrderDisplayName()Ljava/lang/String;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/print/VoidTicketPrintout;->orderDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public final getOrderSnapshot()Lcom/squareup/payment/OrderSnapshot;
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/print/VoidTicketPrintout;->orderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    return-object v0
.end method

.method public getType()Lcom/squareup/print/PrintoutType;
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/print/VoidTicketPrintout;->type:Lcom/squareup/print/PrintoutType;

    return-object v0
.end method

.method public final getVoidedItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    .line 56
    iget-object v0, p0, Lcom/squareup/print/VoidTicketPrintout;->voidedItems:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/print/VoidTicketPrintout;->orderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/print/VoidTicketPrintout;->orderDisplayName:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/print/VoidTicketPrintout;->openTicket:Lcom/squareup/tickets/OpenTicket;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/print/VoidTicketPrintout;->voidedItems:Ljava/util/List;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "VoidTicketPrintout(orderSnapshot="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/VoidTicketPrintout;->orderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", orderDisplayName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/VoidTicketPrintout;->orderDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", openTicket="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/VoidTicketPrintout;->openTicket:Lcom/squareup/tickets/OpenTicket;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", voidedItems="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/VoidTicketPrintout;->voidedItems:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
