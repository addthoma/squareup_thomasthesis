.class public final Lcom/squareup/print/SingleQuantityPrintableOrderItem;
.super Ljava/lang/Object;
.source "SingleQuantityPrintableOrderItem.kt"

# interfaces
.implements Lcom/squareup/print/PrintableOrderItem;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u000b\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0001\u00a2\u0006\u0002\u0010\u0003J\u0011\u00107\u001a\u00020\u00052\u0006\u00108\u001a\u000209H\u0096\u0001J\u0011\u0010:\u001a\u00020\u001d2\u0006\u00108\u001a\u000209H\u0096\u0001J\u0010\u0010;\u001a\u00020\u00012\u0006\u0010\"\u001a\u00020#H\u0016J\u0011\u0010<\u001a\u00020\u00052\u0006\u00108\u001a\u000209H\u0096\u0001R\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0007R\u0014\u0010\u0008\u001a\u0004\u0018\u00010\tX\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u000bR\u0014\u0010\u000c\u001a\u0004\u0018\u00010\rX\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000fR\u0012\u0010\u0010\u001a\u00020\u0011X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0012R\u0012\u0010\u0013\u001a\u00020\u0011X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\u0013\u0010\u0012R\u0012\u0010\u0014\u001a\u00020\u0011X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\u0012R\u0012\u0010\u0015\u001a\u00020\u0011X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\u0015\u0010\u0012R\u0012\u0010\u0016\u001a\u00020\u0011X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\u0016\u0010\u0012R\u0012\u0010\u0017\u001a\u00020\u0011X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\u0017\u0010\u0012R\u0014\u0010\u0018\u001a\u0004\u0018\u00010\u0005X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\u0019\u0010\u0007R\u0014\u0010\u001a\u001a\u0004\u0018\u00010\u0005X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\u001b\u0010\u0007R\u0014\u0010\u001c\u001a\u00020\u001d8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001e\u0010\u001fR\u0014\u0010 \u001a\u00020\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008!\u0010\u0007R\u0014\u0010\"\u001a\u0004\u0018\u00010#X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008$\u0010%R\u0018\u0010&\u001a\u0008\u0012\u0004\u0012\u00020(0\'X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008)\u0010*R\u0014\u0010+\u001a\u0004\u0018\u00010\u0005X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008,\u0010\u0007R\u0012\u0010-\u001a\u00020\u0011X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008.\u0010\u0012R\u0012\u0010/\u001a\u000200X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u00081\u00102R\u0014\u00103\u001a\u0004\u0018\u000100X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u00084\u00102R\u0012\u00105\u001a\u000200X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u00086\u00102\u00a8\u0006="
    }
    d2 = {
        "Lcom/squareup/print/SingleQuantityPrintableOrderItem;",
        "Lcom/squareup/print/PrintableOrderItem;",
        "delegate",
        "(Lcom/squareup/print/PrintableOrderItem;)V",
        "categoryId",
        "",
        "getCategoryId",
        "()Ljava/lang/String;",
        "destination",
        "Lcom/squareup/checkout/OrderDestination;",
        "getDestination",
        "()Lcom/squareup/checkout/OrderDestination;",
        "idPair",
        "Lcom/squareup/protos/client/IdPair;",
        "getIdPair",
        "()Lcom/squareup/protos/client/IdPair;",
        "isComped",
        "",
        "()Z",
        "isDownConvertedCustomAmount",
        "isTaxed",
        "isUncategorized",
        "isUnitPriced",
        "isVoided",
        "itemName",
        "getItemName",
        "notes",
        "getNotes",
        "quantityAsInt",
        "",
        "getQuantityAsInt",
        "()I",
        "quantityAsString",
        "getQuantityAsString",
        "selectedDiningOption",
        "Lcom/squareup/checkout/DiningOption;",
        "getSelectedDiningOption",
        "()Lcom/squareup/checkout/DiningOption;",
        "selectedModifiers",
        "",
        "Lcom/squareup/print/PrintableOrderItemModifier;",
        "getSelectedModifiers",
        "()Ljava/util/List;",
        "selectedVariationDisplayName",
        "getSelectedVariationDisplayName",
        "shouldShowVariationName",
        "getShouldShowVariationName",
        "total",
        "Lcom/squareup/protos/common/Money;",
        "getTotal",
        "()Lcom/squareup/protos/common/Money;",
        "unitPrice",
        "getUnitPrice",
        "unitPriceWithModifiers",
        "getUnitPriceWithModifiers",
        "quantityEntrySuffix",
        "res",
        "Lcom/squareup/util/Res;",
        "quantityPrecision",
        "setSelectedDiningOption",
        "unitAbbreviation",
        "print_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/print/PrintableOrderItem;


# direct methods
.method public constructor <init>(Lcom/squareup/print/PrintableOrderItem;)V
    .locals 1

    const-string v0, "delegate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/print/SingleQuantityPrintableOrderItem;->$$delegate_0:Lcom/squareup/print/PrintableOrderItem;

    return-void
.end method


# virtual methods
.method public getCategoryId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/SingleQuantityPrintableOrderItem;->$$delegate_0:Lcom/squareup/print/PrintableOrderItem;

    invoke-interface {v0}, Lcom/squareup/print/PrintableOrderItem;->getCategoryId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDestination()Lcom/squareup/checkout/OrderDestination;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/SingleQuantityPrintableOrderItem;->$$delegate_0:Lcom/squareup/print/PrintableOrderItem;

    invoke-interface {v0}, Lcom/squareup/print/PrintableOrderItem;->getDestination()Lcom/squareup/checkout/OrderDestination;

    move-result-object v0

    return-object v0
.end method

.method public getIdPair()Lcom/squareup/protos/client/IdPair;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/SingleQuantityPrintableOrderItem;->$$delegate_0:Lcom/squareup/print/PrintableOrderItem;

    invoke-interface {v0}, Lcom/squareup/print/PrintableOrderItem;->getIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    return-object v0
.end method

.method public getItemName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/SingleQuantityPrintableOrderItem;->$$delegate_0:Lcom/squareup/print/PrintableOrderItem;

    invoke-interface {v0}, Lcom/squareup/print/PrintableOrderItem;->getItemName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNotes()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/SingleQuantityPrintableOrderItem;->$$delegate_0:Lcom/squareup/print/PrintableOrderItem;

    invoke-interface {v0}, Lcom/squareup/print/PrintableOrderItem;->getNotes()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getQuantityAsInt()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getQuantityAsString()Ljava/lang/String;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/print/SingleQuantityPrintableOrderItem;->getQuantityAsInt()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedDiningOption()Lcom/squareup/checkout/DiningOption;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/SingleQuantityPrintableOrderItem;->$$delegate_0:Lcom/squareup/print/PrintableOrderItem;

    invoke-interface {v0}, Lcom/squareup/print/PrintableOrderItem;->getSelectedDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getSelectedDiningOption()Lcom/squareup/itemsorter/SortableDiningOption;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/print/SingleQuantityPrintableOrderItem;->getSelectedDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v0

    check-cast v0, Lcom/squareup/itemsorter/SortableDiningOption;

    return-object v0
.end method

.method public getSelectedModifiers()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintableOrderItemModifier;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/print/SingleQuantityPrintableOrderItem;->$$delegate_0:Lcom/squareup/print/PrintableOrderItem;

    invoke-interface {v0}, Lcom/squareup/print/PrintableOrderItem;->getSelectedModifiers()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedVariationDisplayName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/SingleQuantityPrintableOrderItem;->$$delegate_0:Lcom/squareup/print/PrintableOrderItem;

    invoke-interface {v0}, Lcom/squareup/print/PrintableOrderItem;->getSelectedVariationDisplayName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getShouldShowVariationName()Z
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/SingleQuantityPrintableOrderItem;->$$delegate_0:Lcom/squareup/print/PrintableOrderItem;

    invoke-interface {v0}, Lcom/squareup/print/PrintableOrderItem;->getShouldShowVariationName()Z

    move-result v0

    return v0
.end method

.method public getTotal()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/SingleQuantityPrintableOrderItem;->$$delegate_0:Lcom/squareup/print/PrintableOrderItem;

    invoke-interface {v0}, Lcom/squareup/print/PrintableOrderItem;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getUnitPrice()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/SingleQuantityPrintableOrderItem;->$$delegate_0:Lcom/squareup/print/PrintableOrderItem;

    invoke-interface {v0}, Lcom/squareup/print/PrintableOrderItem;->getUnitPrice()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getUnitPriceWithModifiers()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/SingleQuantityPrintableOrderItem;->$$delegate_0:Lcom/squareup/print/PrintableOrderItem;

    invoke-interface {v0}, Lcom/squareup/print/PrintableOrderItem;->getUnitPriceWithModifiers()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public isComped()Z
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/SingleQuantityPrintableOrderItem;->$$delegate_0:Lcom/squareup/print/PrintableOrderItem;

    invoke-interface {v0}, Lcom/squareup/print/PrintableOrderItem;->isComped()Z

    move-result v0

    return v0
.end method

.method public isDownConvertedCustomAmount()Z
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/SingleQuantityPrintableOrderItem;->$$delegate_0:Lcom/squareup/print/PrintableOrderItem;

    invoke-interface {v0}, Lcom/squareup/print/PrintableOrderItem;->isDownConvertedCustomAmount()Z

    move-result v0

    return v0
.end method

.method public isTaxed()Z
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/SingleQuantityPrintableOrderItem;->$$delegate_0:Lcom/squareup/print/PrintableOrderItem;

    invoke-interface {v0}, Lcom/squareup/print/PrintableOrderItem;->isTaxed()Z

    move-result v0

    return v0
.end method

.method public isUncategorized()Z
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/SingleQuantityPrintableOrderItem;->$$delegate_0:Lcom/squareup/print/PrintableOrderItem;

    invoke-interface {v0}, Lcom/squareup/print/PrintableOrderItem;->isUncategorized()Z

    move-result v0

    return v0
.end method

.method public isUnitPriced()Z
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/SingleQuantityPrintableOrderItem;->$$delegate_0:Lcom/squareup/print/PrintableOrderItem;

    invoke-interface {v0}, Lcom/squareup/print/PrintableOrderItem;->isUnitPriced()Z

    move-result v0

    return v0
.end method

.method public isVoided()Z
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/SingleQuantityPrintableOrderItem;->$$delegate_0:Lcom/squareup/print/PrintableOrderItem;

    invoke-interface {v0}, Lcom/squareup/print/PrintableOrderItem;->isVoided()Z

    move-result v0

    return v0
.end method

.method public quantityEntrySuffix(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/print/SingleQuantityPrintableOrderItem;->$$delegate_0:Lcom/squareup/print/PrintableOrderItem;

    invoke-interface {v0, p1}, Lcom/squareup/print/PrintableOrderItem;->quantityEntrySuffix(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public quantityPrecision(Lcom/squareup/util/Res;)I
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/print/SingleQuantityPrintableOrderItem;->$$delegate_0:Lcom/squareup/print/PrintableOrderItem;

    invoke-interface {v0, p1}, Lcom/squareup/print/PrintableOrderItem;->quantityPrecision(Lcom/squareup/util/Res;)I

    move-result p1

    return p1
.end method

.method public bridge synthetic setSelectedDiningOption(Lcom/squareup/itemsorter/SortableDiningOption;)Lcom/squareup/itemsorter/SortableItem;
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/checkout/DiningOption;

    invoke-virtual {p0, p1}, Lcom/squareup/print/SingleQuantityPrintableOrderItem;->setSelectedDiningOption(Lcom/squareup/checkout/DiningOption;)Lcom/squareup/print/PrintableOrderItem;

    move-result-object p1

    check-cast p1, Lcom/squareup/itemsorter/SortableItem;

    return-object p1
.end method

.method public setSelectedDiningOption(Lcom/squareup/checkout/DiningOption;)Lcom/squareup/print/PrintableOrderItem;
    .locals 1

    const-string v0, "selectedDiningOption"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    move-object p1, p0

    check-cast p1, Lcom/squareup/print/PrintableOrderItem;

    return-object p1
.end method

.method public unitAbbreviation(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/print/SingleQuantityPrintableOrderItem;->$$delegate_0:Lcom/squareup/print/PrintableOrderItem;

    invoke-interface {v0, p1}, Lcom/squareup/print/PrintableOrderItem;->unitAbbreviation(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
