.class public Lcom/squareup/print/SqlitePrintJobQueue;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SqlitePrintJobQueue.java"

# interfaces
.implements Lcom/squareup/print/PrintJobQueue;


# static fields
.field private static final ALL_FAILED_JOBS:Ljava/lang/String;

.field private static final ALL_JOBS:Ljava/lang/String;

.field private static final CREATE_TABLE:Ljava/lang/String;

.field private static final CREATE_TARGET_ID_INDEX:Ljava/lang/String;

.field private static final DATABASE_NAME:Ljava/lang/String; = "PrintJobQueueDb"

.field private static final DATABASE_VERSION:I = 0x1

.field private static final GET_HIGHEST_PRIORITY_FOR_TARGET:Ljava/lang/String;

.field private static final GET_JOB_PRIORITY:Ljava/lang/String;

.field private static final GET_JOB_STATE:Ljava/lang/String;

.field private static final GET_STATE_OF_JOBS_IN_ID_LIST:Lcom/squareup/phrase/Phrase;

.field private static final HEAD_JOB_BY_TARGET:Ljava/lang/String;

.field private static final ID_IN_ID_LIST:Lcom/squareup/phrase/Phrase;

.field private static final INDEX_TARGET_ID:Ljava/lang/String; = "idx_target_id"

.field private static final JOB_GSON:Ljava/lang/String; = "JOB_GSON"

.field private static final JOB_ID:Ljava/lang/String; = "JOB_ID"

.field private static final JOB_ID_IS:Ljava/lang/String; = "JOB_ID IS ?"

.field private static final JOB_PRIORITY:Ljava/lang/String; = "JOB_PRIORITY"

.field private static final JOB_STATE:Ljava/lang/String; = "JOB_STATE"

.field private static final JOB_UPDATED:Ljava/lang/String; = "JOB_UPDATED"

.field static final MIN_JOB_PRIORITY:Ljava/lang/Long;

.field private static final PRINT_JOB_TABLE:Ljava/lang/String; = "PRINT_JOB_TABLE"

.field private static final TARGET_ID:Ljava/lang/String; = "TARGET_ID"


# instance fields
.field private final gson:Lcom/google/gson/Gson;

.field private final iso8601:Ljava/text/SimpleDateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const-wide/16 v0, 0x0

    .line 38
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/print/SqlitePrintJobQueue;->MIN_JOB_PRIORITY:Ljava/lang/Long;

    const-string v0, "CREATE TABLE {table} ( {job_id} TEXT PRIMARY KEY, {target_id} TEXT NOT NULL, {job_state} TEXT NOT NULL, {job_priority} INTEGER NOT NULL, {job_updated} TEXT NOT NULL, {job_gson} TEXT NOT NULL )"

    .line 74
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "table"

    const-string v2, "PRINT_JOB_TABLE"

    .line 84
    invoke-virtual {v0, v1, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v3, "job_id"

    const-string v4, "JOB_ID"

    .line 85
    invoke-virtual {v0, v3, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v5, "target_id"

    const-string v6, "TARGET_ID"

    .line 86
    invoke-virtual {v0, v5, v6}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v7, "job_state"

    const-string v8, "JOB_STATE"

    .line 87
    invoke-virtual {v0, v7, v8}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v9, "job_priority"

    const-string v10, "JOB_PRIORITY"

    .line 88
    invoke-virtual {v0, v9, v10}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v11, "JOB_UPDATED"

    const-string v12, "job_updated"

    .line 89
    invoke-virtual {v0, v12, v11}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v13, "JOB_GSON"

    const-string v14, "job_gson"

    .line 90
    invoke-virtual {v0, v14, v13}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 92
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/print/SqlitePrintJobQueue;->CREATE_TABLE:Ljava/lang/String;

    const-string v0, "CREATE INDEX {idx} ON {table} ({target_id})"

    .line 94
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v15, "idx"

    move-object/from16 v16, v3

    const-string v3, "idx_target_id"

    .line 96
    invoke-virtual {v0, v15, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 97
    invoke-virtual {v0, v1, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 98
    invoke-virtual {v0, v5, v6}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 99
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 100
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/print/SqlitePrintJobQueue;->CREATE_TARGET_ID_INDEX:Ljava/lang/String;

    const-string v0, "SELECT main.{target_id}, main.{job_gson}, main.{job_priority} FROM {table_main} AS main WHERE main.{job_state_main} = \"{enqueued_main}\" AND main.{job_priority_main} = (       SELECT min(sub.{job_priority_sub})       FROM {table_sub} AS sub       WHERE sub.{target_id_sub} = main.{target_id_main}             AND sub.{job_state_sub} = \"{enqueued_sub}\" )"

    .line 102
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 111
    invoke-virtual {v0, v5, v6}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 112
    invoke-virtual {v0, v14, v13}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 113
    invoke-virtual {v0, v9, v10}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v3, "table_main"

    .line 114
    invoke-virtual {v0, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v3, "job_state_main"

    .line 115
    invoke-virtual {v0, v3, v8}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    sget-object v3, Lcom/squareup/print/PrintJobQueue$JobState;->ENQUEUED:Lcom/squareup/print/PrintJobQueue$JobState;

    .line 116
    invoke-virtual {v3}, Lcom/squareup/print/PrintJobQueue$JobState;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, "enqueued_main"

    invoke-virtual {v0, v5, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v3, "job_priority_main"

    .line 117
    invoke-virtual {v0, v3, v10}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v3, "job_priority_sub"

    .line 118
    invoke-virtual {v0, v3, v10}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v3, "table_sub"

    .line 119
    invoke-virtual {v0, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v3, "target_id_sub"

    .line 120
    invoke-virtual {v0, v3, v6}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v3, "target_id_main"

    .line 121
    invoke-virtual {v0, v3, v6}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v3, "job_state_sub"

    .line 122
    invoke-virtual {v0, v3, v8}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    sget-object v3, Lcom/squareup/print/PrintJobQueue$JobState;->ENQUEUED:Lcom/squareup/print/PrintJobQueue$JobState;

    .line 123
    invoke-virtual {v3}, Lcom/squareup/print/PrintJobQueue$JobState;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, "enqueued_sub"

    invoke-virtual {v0, v5, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 124
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 125
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/print/SqlitePrintJobQueue;->HEAD_JOB_BY_TARGET:Ljava/lang/String;

    const-string v0, "SELECT {job_gson} FROM {table} WHERE {job_state} IS \"{failed}\" ORDER BY {job_updated} ASC"

    .line 127
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 132
    invoke-virtual {v0, v14, v13}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 133
    invoke-virtual {v0, v1, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 134
    invoke-virtual {v0, v7, v8}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    sget-object v3, Lcom/squareup/print/PrintJobQueue$JobState;->FAILED:Lcom/squareup/print/PrintJobQueue$JobState;

    .line 135
    invoke-virtual {v3}, Lcom/squareup/print/PrintJobQueue$JobState;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, "failed"

    invoke-virtual {v0, v5, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 136
    invoke-virtual {v0, v12, v11}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 137
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 138
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/print/SqlitePrintJobQueue;->ALL_FAILED_JOBS:Ljava/lang/String;

    const-string v0, "SELECT {job_gson} FROM {table} ORDER BY {job_updated} ASC"

    .line 140
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 144
    invoke-virtual {v0, v14, v13}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 145
    invoke-virtual {v0, v1, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 146
    invoke-virtual {v0, v12, v11}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 147
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 148
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/print/SqlitePrintJobQueue;->ALL_JOBS:Ljava/lang/String;

    const-string v0, "SELECT max({job_priority}) FROM {table} WHERE {job_target} IS ?"

    .line 150
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 154
    invoke-virtual {v0, v9, v10}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 155
    invoke-virtual {v0, v1, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v3, "job_target"

    .line 156
    invoke-virtual {v0, v3, v6}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 157
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 158
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/print/SqlitePrintJobQueue;->GET_HIGHEST_PRIORITY_FOR_TARGET:Ljava/lang/String;

    const-string v0, "SELECT {job_state} FROM {table} WHERE {job_id} IS ?"

    .line 160
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 164
    invoke-virtual {v0, v7, v8}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 165
    invoke-virtual {v0, v1, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    move-object/from16 v3, v16

    .line 166
    invoke-virtual {v0, v3, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 167
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 168
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/print/SqlitePrintJobQueue;->GET_JOB_STATE:Ljava/lang/String;

    const-string v0, "SELECT {job_priority} FROM {table} WHERE {job_id} IS ?"

    .line 170
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 174
    invoke-virtual {v0, v9, v10}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 175
    invoke-virtual {v0, v1, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 176
    invoke-virtual {v0, v3, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 177
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 178
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/print/SqlitePrintJobQueue;->GET_JOB_PRIORITY:Ljava/lang/String;

    const-string v0, "SELECT {job_state}, {print_job_id} FROM {table} WHERE {job_id} IN ({id_list})"

    .line 182
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 186
    invoke-virtual {v0, v7, v8}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v5, "print_job_id"

    .line 187
    invoke-virtual {v0, v5, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 188
    invoke-virtual {v0, v1, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 189
    invoke-virtual {v0, v3, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    sput-object v0, Lcom/squareup/print/SqlitePrintJobQueue;->GET_STATE_OF_JOBS_IN_ID_LIST:Lcom/squareup/phrase/Phrase;

    const-string/jumbo v0, "{id} IN ({id_list})"

    .line 191
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "id"

    .line 193
    invoke-virtual {v0, v1, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    sput-object v0, Lcom/squareup/print/SqlitePrintJobQueue;->ID_IN_ID_LIST:Lcom/squareup/phrase/Phrase;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/io/File;Lcom/google/gson/Gson;)V
    .locals 2

    .line 199
    new-instance v0, Ljava/io/File;

    const-string v1, "PrintJobQueueDb.db"

    invoke-direct {v0, p2, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 200
    iput-object p3, p0, Lcom/squareup/print/SqlitePrintJobQueue;->gson:Lcom/google/gson/Gson;

    .line 202
    new-instance p1, Ljava/text/SimpleDateFormat;

    sget-object p2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string/jumbo p3, "yyyy-MM-dd\'T\'HH:mm:ss.SSSZ"

    invoke-direct {p1, p3, p2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object p1, p0, Lcom/squareup/print/SqlitePrintJobQueue;->iso8601:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method private asArray(Ljava/lang/String;)[Ljava/lang/String;
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    return-object v0
.end method

.method private asUuidArray(Lcom/squareup/print/PrintJob;)[Ljava/lang/String;
    .locals 0

    .line 507
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/print/SqlitePrintJobQueue;->asArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private asUuidArray(Ljava/util/Collection;)[Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/print/PrintJob;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 512
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    .line 514
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/print/PrintJob;

    .line 515
    invoke-virtual {v2}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private assertJobInState(Ljava/lang/String;Lcom/squareup/print/PrintJobQueue$JobState;)V
    .locals 0

    .line 452
    invoke-direct {p0, p1}, Lcom/squareup/print/SqlitePrintJobQueue;->asArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1, p2}, Lcom/squareup/print/SqlitePrintJobQueue;->assertJobsInState([Ljava/lang/String;Lcom/squareup/print/PrintJobQueue$JobState;)V

    return-void
.end method

.method private assertJobsInState([Ljava/lang/String;Lcom/squareup/print/PrintJobQueue$JobState;)V
    .locals 4

    .line 429
    invoke-virtual {p0}, Lcom/squareup/print/SqlitePrintJobQueue;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 430
    sget-object v1, Lcom/squareup/print/SqlitePrintJobQueue;->GET_STATE_OF_JOBS_IN_ID_LIST:Lcom/squareup/phrase/Phrase;

    array-length v2, p1

    .line 431
    invoke-direct {p0, v2}, Lcom/squareup/print/SqlitePrintJobQueue;->buildArgs(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "id_list"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 432
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 433
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 434
    invoke-virtual {v0, v1, p1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    const-string v0, "JOB_STATE"

    .line 435
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const-string v1, "JOB_ID"

    .line 436
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 437
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 438
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_1

    .line 439
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 440
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/print/PrintJobQueue$JobState;->valueOf(Ljava/lang/String;)Lcom/squareup/print/PrintJobQueue$JobState;

    move-result-object v3

    if-ne v3, p2, :cond_0

    .line 445
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .line 442
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PrintJob with id "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " is not "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 443
    invoke-virtual {p2}, Lcom/squareup/print/PrintJobQueue$JobState;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, ", is instead "

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 447
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    return-void
.end method

.method private buildArgs(I)Ljava/lang/String;
    .locals 2

    const-string v0, "?"

    const-string v1, ", "

    .line 502
    invoke-static {v0, v1, p1}, Lcom/squareup/util/Strings;->joinRepeated(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private buildJobContentValues(Lcom/squareup/print/PrintJob;Lcom/squareup/print/PrintJobQueue$JobState;Z)Landroid/content/ContentValues;
    .locals 3

    .line 356
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 357
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "JOB_ID"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getTargetId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "TARGET_ID"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    invoke-virtual {p2}, Lcom/squareup/print/PrintJobQueue$JobState;->toString()Ljava/lang/String;

    move-result-object p2

    const-string v1, "JOB_STATE"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p3, :cond_0

    .line 362
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getTargetId()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/squareup/print/SqlitePrintJobQueue;->getHighestJobPriorityForTarget(Ljava/lang/String;)J

    move-result-wide p2

    const-wide/16 v1, 0x1

    add-long/2addr p2, v1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    const-string p3, "JOB_PRIORITY"

    invoke-virtual {v0, p3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 364
    :cond_0
    iget-object p2, p0, Lcom/squareup/print/SqlitePrintJobQueue;->gson:Lcom/google/gson/Gson;

    invoke-virtual {p2, p1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "JOB_GSON"

    invoke-virtual {v0, p2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    iget-object p1, p0, Lcom/squareup/print/SqlitePrintJobQueue;->iso8601:Ljava/text/SimpleDateFormat;

    new-instance p2, Ljava/util/Date;

    invoke-direct {p2}, Ljava/util/Date;-><init>()V

    invoke-virtual {p1, p2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "JOB_UPDATED"

    invoke-virtual {v0, p2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private buildPrintJobsListFromCursor(Landroid/database/Cursor;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintJob;",
            ">;"
        }
    .end annotation

    const-string v0, "JOB_GSON"

    .line 403
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 405
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    if-ltz v0, :cond_1

    .line 407
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 408
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x0

    .line 411
    :try_start_0
    iget-object v3, p0, Lcom/squareup/print/SqlitePrintJobQueue;->gson:Lcom/google/gson/Gson;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-class v5, Lcom/squareup/print/PrintJob;

    invoke-virtual {v3, v4, v5}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/print/PrintJob;
    :try_end_0
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v3

    goto :goto_1

    :catch_0
    move-exception v3

    .line 413
    new-instance v4, Lcom/google/gson/JsonSyntaxException;

    const-string v5, "Failed to read json: JOB_GSON"

    invoke-direct {v4, v5, v3}, Lcom/google/gson/JsonSyntaxException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v4}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    :goto_1
    if-eqz v2, :cond_0

    .line 416
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 418
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .line 421
    :cond_1
    new-instance v0, Lcom/google/gson/JsonSyntaxException;

    const-string v2, "JOB_GSON index not found."

    invoke-direct {v0, v2}, Lcom/google/gson/JsonSyntaxException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    .line 423
    :cond_2
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    return-object v1
.end method

.method private buildPrintJobsMapFromCursor(Landroid/database/Cursor;)Ljava/util/LinkedHashMap;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/print/PrintJob;",
            ">;"
        }
    .end annotation

    const-string v0, "JOB_GSON"

    .line 374
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 376
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/LinkedHashMap;-><init>(I)V

    if-ltz v0, :cond_1

    .line 378
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 379
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x0

    .line 382
    :try_start_0
    iget-object v3, p0, Lcom/squareup/print/SqlitePrintJobQueue;->gson:Lcom/google/gson/Gson;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-class v5, Lcom/squareup/print/PrintJob;

    invoke-virtual {v3, v4, v5}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/print/PrintJob;
    :try_end_0
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v3

    goto :goto_1

    :catch_0
    move-exception v3

    .line 384
    new-instance v4, Lcom/google/gson/JsonSyntaxException;

    const-string v5, "Failed to read json: JOB_GSON"

    invoke-direct {v4, v5, v3}, Lcom/google/gson/JsonSyntaxException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v4}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    :goto_1
    if-eqz v2, :cond_0

    .line 387
    invoke-virtual {v2}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .line 392
    :cond_1
    new-instance v0, Lcom/google/gson/JsonSyntaxException;

    const-string v2, "JOB_GSON index not found."

    invoke-direct {v0, v2}, Lcom/google/gson/JsonSyntaxException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    .line 394
    :cond_2
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    return-object v1
.end method

.method private deleteJob(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 497
    invoke-direct {p0, p1}, Lcom/squareup/print/SqlitePrintJobQueue;->asArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1, p2}, Lcom/squareup/print/SqlitePrintJobQueue;->deleteJobs([Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private deleteJobs([Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 461
    array-length v2, p1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const-string v4, "[PrintJobQueue_deleteJobs] Job Count: %d with reason: %s"

    invoke-static {v4, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 462
    array-length v1, p1

    if-lez v1, :cond_1

    new-array v1, v2, [Ljava/lang/Object;

    const-string v4, ", "

    .line 463
    invoke-static {p1, v4}, Lcom/squareup/util/Strings;->join([Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    const-string v4, "[PrintJobQueue_deleteJobs] Deleting the following IDs: %s"

    invoke-static {v4, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 465
    sget-object v1, Lcom/squareup/print/SqlitePrintJobQueue;->ID_IN_ID_LIST:Lcom/squareup/phrase/Phrase;

    array-length v4, p1

    .line 468
    invoke-direct {p0, v4}, Lcom/squareup/print/SqlitePrintJobQueue;->buildArgs(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "id_list"

    invoke-virtual {v1, v5, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 469
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 470
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 474
    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v1}, Lcom/squareup/util/Strings;->trim(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "1"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 479
    invoke-virtual {p0}, Lcom/squareup/print/SqlitePrintJobQueue;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, "PRINT_JOB_TABLE"

    .line 480
    invoke-virtual {v4, v5, v1, p1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    new-array v0, v0, [Ljava/lang/Object;

    .line 481
    array-length v4, p1

    .line 482
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v2

    const-string v2, "[PrintJobQueue_deleteJobs] Expecting to delete %d jobs; actually deleted %d rows"

    .line 481
    invoke-static {v2, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 483
    array-length v0, p1

    if-eq v1, v0, :cond_1

    .line 485
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Did not delete "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length p1, p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " print jobs like expected! Only deleted "

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ". Reason for deleting print jobs: "

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 475
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Invalid WHERE string, would delete all rows: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public enqueueNewJob(Lcom/squareup/print/PrintJob;)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v2, "Enqueueing new print job: %s"

    .line 217
    invoke-static {v2, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 218
    invoke-virtual {p0}, Lcom/squareup/print/SqlitePrintJobQueue;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 219
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 221
    :try_start_0
    sget-object v2, Lcom/squareup/print/PrintJobQueue$JobState;->ENQUEUED:Lcom/squareup/print/PrintJobQueue$JobState;

    invoke-direct {p0, p1, v2, v1}, Lcom/squareup/print/SqlitePrintJobQueue;->buildJobContentValues(Lcom/squareup/print/PrintJob;Lcom/squareup/print/PrintJobQueue$JobState;Z)Landroid/content/ContentValues;

    move-result-object p1

    const-string v1, "PRINT_JOB_TABLE"

    const/4 v2, 0x0

    .line 222
    invoke-virtual {v0, v1, v2, p1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 223
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return-void

    :catchall_0
    move-exception p1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 226
    throw p1
.end method

.method getHighestJobPriorityForTarget(Ljava/lang/String;)J
    .locals 2

    .line 307
    invoke-virtual {p0}, Lcom/squareup/print/SqlitePrintJobQueue;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 308
    sget-object v1, Lcom/squareup/print/SqlitePrintJobQueue;->GET_HIGHEST_PRIORITY_FOR_TARGET:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/squareup/print/SqlitePrintJobQueue;->asArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    .line 311
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_0

    .line 312
    sget-object p1, Lcom/squareup/print/SqlitePrintJobQueue;->MIN_JOB_PRIORITY:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0

    :cond_0
    const/4 v0, 0x0

    .line 315
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 316
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    return-wide v0
.end method

.method getPriorityForJob(Ljava/lang/String;)J
    .locals 2

    .line 342
    invoke-virtual {p0}, Lcom/squareup/print/SqlitePrintJobQueue;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 343
    sget-object v1, Lcom/squareup/print/SqlitePrintJobQueue;->GET_JOB_PRIORITY:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/squareup/print/SqlitePrintJobQueue;->asArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    .line 345
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    return-wide v0

    :cond_0
    const-string v0, "JOB_PRIORITY"

    .line 348
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 349
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    return-wide v0
.end method

.method getStateForJob(Ljava/lang/String;)Lcom/squareup/print/PrintJobQueue$JobState;
    .locals 2

    .line 325
    invoke-virtual {p0}, Lcom/squareup/print/SqlitePrintJobQueue;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 326
    sget-object v1, Lcom/squareup/print/SqlitePrintJobQueue;->GET_JOB_STATE:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/squareup/print/SqlitePrintJobQueue;->asArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    .line 328
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    const-string v0, "JOB_STATE"

    .line 331
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 332
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/print/PrintJobQueue$JobState;->valueOf(Ljava/lang/String;)Lcom/squareup/print/PrintJobQueue$JobState;

    move-result-object v0

    .line 333
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    return-object v0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .line 206
    sget-object v0, Lcom/squareup/print/SqlitePrintJobQueue;->CREATE_TABLE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 207
    sget-object v0, Lcom/squareup/print/SqlitePrintJobQueue;->CREATE_TARGET_ID_INDEX:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    .line 213
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public reenqueueFailedJob(Lcom/squareup/print/PrintJob;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 273
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "[reenqueueFailedJob] Job Id: %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 274
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/squareup/print/PrintJobQueue$JobState;->FAILED:Lcom/squareup/print/PrintJobQueue$JobState;

    invoke-direct {p0, v0, v1}, Lcom/squareup/print/SqlitePrintJobQueue;->assertJobInState(Ljava/lang/String;Lcom/squareup/print/PrintJobQueue$JobState;)V

    .line 275
    invoke-virtual {p0}, Lcom/squareup/print/SqlitePrintJobQueue;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 276
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 278
    :try_start_0
    sget-object v1, Lcom/squareup/print/PrintJobQueue$JobState;->ENQUEUED:Lcom/squareup/print/PrintJobQueue$JobState;

    invoke-direct {p0, p1, v1, v2}, Lcom/squareup/print/SqlitePrintJobQueue;->buildJobContentValues(Lcom/squareup/print/PrintJob;Lcom/squareup/print/PrintJobQueue$JobState;Z)Landroid/content/ContentValues;

    move-result-object v1

    const-string v2, "PRINT_JOB_TABLE"

    const-string v3, "JOB_ID IS ?"

    .line 279
    invoke-direct {p0, p1}, Lcom/squareup/print/SqlitePrintJobQueue;->asUuidArray(Lcom/squareup/print/PrintJob;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 280
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 282
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 284
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object p1

    sget-object v0, Lcom/squareup/print/PrintJobQueue$JobState;->ENQUEUED:Lcom/squareup/print/PrintJobQueue$JobState;

    invoke-direct {p0, p1, v0}, Lcom/squareup/print/SqlitePrintJobQueue;->assertJobInState(Ljava/lang/String;Lcom/squareup/print/PrintJobQueue$JobState;)V

    return-void

    :catchall_0
    move-exception p1

    .line 282
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 283
    throw p1
.end method

.method public removeFailedJobs(Ljava/util/Collection;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/print/PrintJob;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 266
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const-string v1, "[removeFailedfulJobs] Job Count: %s with reason: %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 267
    invoke-direct {p0, p1}, Lcom/squareup/print/SqlitePrintJobQueue;->asUuidArray(Ljava/util/Collection;)[Ljava/lang/String;

    move-result-object p1

    .line 268
    sget-object v0, Lcom/squareup/print/PrintJobQueue$JobState;->FAILED:Lcom/squareup/print/PrintJobQueue$JobState;

    invoke-direct {p0, p1, v0}, Lcom/squareup/print/SqlitePrintJobQueue;->assertJobsInState([Ljava/lang/String;Lcom/squareup/print/PrintJobQueue$JobState;)V

    .line 269
    invoke-direct {p0, p1, p2}, Lcom/squareup/print/SqlitePrintJobQueue;->deleteJobs([Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public removeSuccessfulJob(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "[removeSuccessfulJob] Job Id: %s"

    .line 260
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 261
    sget-object v0, Lcom/squareup/print/PrintJobQueue$JobState;->ENQUEUED:Lcom/squareup/print/PrintJobQueue$JobState;

    invoke-direct {p0, p1, v0}, Lcom/squareup/print/SqlitePrintJobQueue;->assertJobInState(Ljava/lang/String;Lcom/squareup/print/PrintJobQueue$JobState;)V

    const-string v0, "removeSuccessfulJob"

    .line 262
    invoke-direct {p0, p1, v0}, Lcom/squareup/print/SqlitePrintJobQueue;->deleteJob(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public retrieveAllFailedPrintJobs()Ljava/util/LinkedHashMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/print/PrintJob;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "[retrieveAllFailedPrintJobs]"

    .line 253
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 254
    invoke-virtual {p0}, Lcom/squareup/print/SqlitePrintJobQueue;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 255
    sget-object v1, Lcom/squareup/print/SqlitePrintJobQueue;->ALL_FAILED_JOBS:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 256
    invoke-direct {p0, v0}, Lcom/squareup/print/SqlitePrintJobQueue;->buildPrintJobsMapFromCursor(Landroid/database/Cursor;)Ljava/util/LinkedHashMap;

    move-result-object v0

    return-object v0
.end method

.method public retrieveAllPrintJobs()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintJob;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "[retrieveAllPrintJobs]"

    .line 246
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 247
    invoke-virtual {p0}, Lcom/squareup/print/SqlitePrintJobQueue;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 248
    sget-object v1, Lcom/squareup/print/SqlitePrintJobQueue;->ALL_JOBS:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 249
    invoke-direct {p0, v0}, Lcom/squareup/print/SqlitePrintJobQueue;->buildPrintJobsListFromCursor(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public retrieveHeadPrintJobPerPrintTarget()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/print/PrintJob;",
            ">;"
        }
    .end annotation

    .line 230
    invoke-virtual {p0}, Lcom/squareup/print/SqlitePrintJobQueue;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 231
    sget-object v1, Lcom/squareup/print/SqlitePrintJobQueue;->HEAD_JOB_BY_TARGET:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    const-string v1, "JOB_GSON"

    .line 232
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 234
    new-instance v2, Ljava/util/HashMap;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/HashMap;-><init>(I)V

    .line 235
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 236
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_0

    .line 237
    iget-object v3, p0, Lcom/squareup/print/SqlitePrintJobQueue;->gson:Lcom/google/gson/Gson;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-class v5, Lcom/squareup/print/PrintJob;

    invoke-virtual {v3, v4, v5}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/print/PrintJob;

    .line 238
    invoke-virtual {v3}, Lcom/squareup/print/PrintJob;->getTargetId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .line 241
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 242
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public updateJobAsFailed(Lcom/squareup/print/PrintJob;)V
    .locals 5

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 288
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "[updateJobAsFailed] Job Id: %s"

    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 289
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/squareup/print/PrintJobQueue$JobState;->ENQUEUED:Lcom/squareup/print/PrintJobQueue$JobState;

    invoke-direct {p0, v1, v2}, Lcom/squareup/print/SqlitePrintJobQueue;->assertJobInState(Ljava/lang/String;Lcom/squareup/print/PrintJobQueue$JobState;)V

    .line 290
    invoke-virtual {p0}, Lcom/squareup/print/SqlitePrintJobQueue;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 291
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 293
    :try_start_0
    sget-object v2, Lcom/squareup/print/PrintJobQueue$JobState;->FAILED:Lcom/squareup/print/PrintJobQueue$JobState;

    invoke-direct {p0, p1, v2, v0}, Lcom/squareup/print/SqlitePrintJobQueue;->buildJobContentValues(Lcom/squareup/print/PrintJob;Lcom/squareup/print/PrintJobQueue$JobState;Z)Landroid/content/ContentValues;

    move-result-object v0

    const-string v2, "PRINT_JOB_TABLE"

    const-string v3, "JOB_ID IS ?"

    .line 294
    invoke-direct {p0, p1}, Lcom/squareup/print/SqlitePrintJobQueue;->asUuidArray(Lcom/squareup/print/PrintJob;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 295
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 297
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 299
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object p1

    sget-object v0, Lcom/squareup/print/PrintJobQueue$JobState;->FAILED:Lcom/squareup/print/PrintJobQueue$JobState;

    invoke-direct {p0, p1, v0}, Lcom/squareup/print/SqlitePrintJobQueue;->assertJobInState(Ljava/lang/String;Lcom/squareup/print/PrintJobQueue$JobState;)V

    return-void

    :catchall_0
    move-exception p1

    .line 297
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 298
    throw p1
.end method
