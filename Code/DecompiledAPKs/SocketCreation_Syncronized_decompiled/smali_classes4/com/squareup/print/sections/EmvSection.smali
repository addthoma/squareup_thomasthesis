.class public Lcom/squareup/print/sections/EmvSection;
.super Ljava/lang/Object;
.source "EmvSection.java"


# instance fields
.field public final applicationId:Ljava/lang/String;

.field public final applicationPreferredName:Ljava/lang/String;

.field public final cardholderVerificationMethodUsedDescription:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/squareup/print/sections/EmvSection;->applicationPreferredName:Ljava/lang/String;

    .line 19
    iput-object p2, p0, Lcom/squareup/print/sections/EmvSection;->applicationId:Ljava/lang/String;

    .line 20
    iput-object p3, p0, Lcom/squareup/print/sections/EmvSection;->cardholderVerificationMethodUsedDescription:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_8

    .line 25
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_3

    .line 27
    :cond_1
    check-cast p1, Lcom/squareup/print/sections/EmvSection;

    .line 29
    iget-object v2, p0, Lcom/squareup/print/sections/EmvSection;->applicationPreferredName:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v3, p1, Lcom/squareup/print/sections/EmvSection;->applicationPreferredName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0

    :cond_2
    iget-object v2, p1, Lcom/squareup/print/sections/EmvSection;->applicationPreferredName:Ljava/lang/String;

    if-eqz v2, :cond_3

    :goto_0
    return v1

    .line 33
    :cond_3
    iget-object v2, p0, Lcom/squareup/print/sections/EmvSection;->applicationId:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v3, p1, Lcom/squareup/print/sections/EmvSection;->applicationId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    goto :goto_1

    :cond_4
    iget-object v2, p1, Lcom/squareup/print/sections/EmvSection;->applicationId:Ljava/lang/String;

    if-eqz v2, :cond_5

    :goto_1
    return v1

    .line 37
    :cond_5
    iget-object v2, p0, Lcom/squareup/print/sections/EmvSection;->cardholderVerificationMethodUsedDescription:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object p1, p1, Lcom/squareup/print/sections/EmvSection;->cardholderVerificationMethodUsedDescription:Ljava/lang/String;

    .line 38
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_7

    goto :goto_2

    :cond_6
    iget-object p1, p1, Lcom/squareup/print/sections/EmvSection;->cardholderVerificationMethodUsedDescription:Ljava/lang/String;

    if-eqz p1, :cond_7

    :goto_2
    return v1

    :cond_7
    return v0

    :cond_8
    :goto_3
    return v1
.end method

.method public hashCode()I
    .locals 3

    .line 48
    iget-object v0, p0, Lcom/squareup/print/sections/EmvSection;->applicationPreferredName:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 49
    iget-object v2, p0, Lcom/squareup/print/sections/EmvSection;->applicationId:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 50
    iget-object v2, p0, Lcom/squareup/print/sections/EmvSection;->cardholderVerificationMethodUsedDescription:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 51
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    .line 56
    iget-object v1, p0, Lcom/squareup/print/sections/EmvSection;->applicationPreferredName:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/print/sections/EmvSection;->applicationId:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/print/sections/EmvSection;->cardholderVerificationMethodUsedDescription:Ljava/lang/String;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/print/util/PrintRendererUtils;->removeBlanks([Ljava/lang/CharSequence;)Ljava/util/List;

    move-result-object v0

    .line 58
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    .line 61
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->lightDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 62
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->smallSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 63
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 64
    invoke-static {v1, v0}, Lcom/squareup/print/util/PrintRendererUtils;->appendAsLinesIfNotBlankList(Landroid/text/SpannableStringBuilder;Ljava/util/List;)V

    .line 65
    invoke-virtual {p1, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 66
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->smallSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    return-void
.end method
