.class public final Lcom/squareup/print/sections/BarcodeSection;
.super Ljava/lang/Object;
.source "BarcodeSection.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\u0008\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u000c\u001a\u00020\rH\u00d6\u0001J\u000e\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011J\t\u0010\u0012\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/print/sections/BarcodeSection;",
        "",
        "barcodeString",
        "",
        "(Ljava/lang/String;)V",
        "getBarcodeString",
        "()Ljava/lang/String;",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "renderBitmap",
        "",
        "builder",
        "Lcom/squareup/print/ThermalBitmapBuilder;",
        "toString",
        "print_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final barcodeString:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const-string v0, "barcodeString"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/print/sections/BarcodeSection;->barcodeString:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/print/sections/BarcodeSection;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/print/sections/BarcodeSection;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/print/sections/BarcodeSection;->barcodeString:Ljava/lang/String;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/print/sections/BarcodeSection;->copy(Ljava/lang/String;)Lcom/squareup/print/sections/BarcodeSection;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/sections/BarcodeSection;->barcodeString:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;)Lcom/squareup/print/sections/BarcodeSection;
    .locals 1

    const-string v0, "barcodeString"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/print/sections/BarcodeSection;

    invoke-direct {v0, p1}, Lcom/squareup/print/sections/BarcodeSection;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/print/sections/BarcodeSection;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/print/sections/BarcodeSection;

    iget-object v0, p0, Lcom/squareup/print/sections/BarcodeSection;->barcodeString:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/print/sections/BarcodeSection;->barcodeString:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBarcodeString()Ljava/lang/String;
    .locals 1

    .line 5
    iget-object v0, p0, Lcom/squareup/print/sections/BarcodeSection;->barcodeString:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/sections/BarcodeSection;->barcodeString:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V
    .locals 1

    const-string v0, "builder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 8
    iget-object v0, p0, Lcom/squareup/print/sections/BarcodeSection;->barcodeString:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->barcode(Ljava/lang/String;)Lcom/squareup/print/ThermalBitmapBuilder;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BarcodeSection(barcodeString="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/sections/BarcodeSection;->barcodeString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
