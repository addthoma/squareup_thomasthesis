.class public Lcom/squareup/print/sections/HeaderSection$Builder;
.super Ljava/lang/Object;
.source "HeaderSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/sections/HeaderSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field public addressLines:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public businessAbn:Ljava/lang/String;

.field public businessName:Ljava/lang/String;

.field public date:Ljava/lang/String;

.field public eMoneyReprintLabel:Ljava/lang/String;

.field public employee:Ljava/lang/String;

.field public formalReceiptName:Ljava/lang/String;

.field public formalReceiptTitle:Ljava/lang/String;

.field public logoUrl:Ljava/lang/String;

.field public phoneNumber:Ljava/lang/String;

.field public renderType:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

.field public time:Ljava/lang/String;

.field public transactionType:Ljava/lang/String;

.field public twitter:Ljava/lang/String;

.field public website:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/print/sections/HeaderSection;
    .locals 18

    move-object/from16 v0, p0

    .line 82
    new-instance v17, Lcom/squareup/print/sections/HeaderSection;

    iget-object v2, v0, Lcom/squareup/print/sections/HeaderSection$Builder;->eMoneyReprintLabel:Ljava/lang/String;

    iget-object v3, v0, Lcom/squareup/print/sections/HeaderSection$Builder;->logoUrl:Ljava/lang/String;

    iget-object v4, v0, Lcom/squareup/print/sections/HeaderSection$Builder;->date:Ljava/lang/String;

    iget-object v5, v0, Lcom/squareup/print/sections/HeaderSection$Builder;->time:Ljava/lang/String;

    iget-object v6, v0, Lcom/squareup/print/sections/HeaderSection$Builder;->employee:Ljava/lang/String;

    iget-object v7, v0, Lcom/squareup/print/sections/HeaderSection$Builder;->businessName:Ljava/lang/String;

    iget-object v8, v0, Lcom/squareup/print/sections/HeaderSection$Builder;->addressLines:Ljava/util/List;

    iget-object v9, v0, Lcom/squareup/print/sections/HeaderSection$Builder;->phoneNumber:Ljava/lang/String;

    iget-object v10, v0, Lcom/squareup/print/sections/HeaderSection$Builder;->twitter:Ljava/lang/String;

    iget-object v11, v0, Lcom/squareup/print/sections/HeaderSection$Builder;->website:Ljava/lang/String;

    iget-object v12, v0, Lcom/squareup/print/sections/HeaderSection$Builder;->businessAbn:Ljava/lang/String;

    iget-object v13, v0, Lcom/squareup/print/sections/HeaderSection$Builder;->formalReceiptTitle:Ljava/lang/String;

    iget-object v14, v0, Lcom/squareup/print/sections/HeaderSection$Builder;->formalReceiptName:Ljava/lang/String;

    iget-object v15, v0, Lcom/squareup/print/sections/HeaderSection$Builder;->renderType:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    iget-object v1, v0, Lcom/squareup/print/sections/HeaderSection$Builder;->transactionType:Ljava/lang/String;

    move-object/from16 v16, v1

    move-object/from16 v1, v17

    invoke-direct/range {v1 .. v16}, Lcom/squareup/print/sections/HeaderSection;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/print/payload/ReceiptPayload$RenderType;Ljava/lang/String;)V

    return-object v17
.end method
