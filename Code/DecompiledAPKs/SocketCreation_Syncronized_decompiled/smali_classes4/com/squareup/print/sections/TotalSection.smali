.class public Lcom/squareup/print/sections/TotalSection;
.super Ljava/lang/Object;
.source "TotalSection.java"


# instance fields
.field public final formalReceiptTotal:Lcom/squareup/print/payload/LabelAmountPair;

.field public final inclusiveTaxTotal:Ljava/lang/String;

.field public final total:Lcom/squareup/print/payload/LabelAmountPair;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;)V
    .locals 0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/squareup/print/sections/TotalSection;->inclusiveTaxTotal:Ljava/lang/String;

    .line 52
    iput-object p2, p0, Lcom/squareup/print/sections/TotalSection;->total:Lcom/squareup/print/payload/LabelAmountPair;

    .line 53
    iput-object p3, p0, Lcom/squareup/print/sections/TotalSection;->formalReceiptTotal:Lcom/squareup/print/payload/LabelAmountPair;

    return-void
.end method

.method public static fromOrder(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/util/TaxBreakdown;Lcom/squareup/protos/common/Money;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/print/sections/TotalSection;
    .locals 0

    .line 28
    invoke-static {p4}, Lcom/squareup/print/sections/SectionUtils;->canShowTaxBreakDownTable(Lcom/squareup/settings/server/AccountStatusSettings;)Z

    move-result p4

    .line 27
    invoke-static {p0, p1, p2, p4}, Lcom/squareup/print/sections/SectionUtils;->createInclusiveTaxes(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/util/TaxBreakdown;Z)Ljava/lang/String;

    move-result-object p1

    .line 30
    new-instance p2, Lcom/squareup/print/sections/TotalSection;

    .line 32
    invoke-virtual {p0, p3}, Lcom/squareup/print/ReceiptFormatter;->total(Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object p4

    .line 33
    invoke-virtual {p0, p3}, Lcom/squareup/print/ReceiptFormatter;->formalReceiptTotal(Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object p0

    invoke-direct {p2, p1, p4, p0}, Lcom/squareup/print/sections/TotalSection;-><init>(Ljava/lang/String;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;)V

    return-object p2
.end method

.method private renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;Z)V
    .locals 0

    .line 78
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 79
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    if-eqz p2, :cond_0

    .line 82
    iget-object p2, p0, Lcom/squareup/print/sections/TotalSection;->formalReceiptTotal:Lcom/squareup/print/payload/LabelAmountPair;

    invoke-virtual {p1, p2}, Lcom/squareup/print/ThermalBitmapBuilder;->boldTitleAndBigAmount(Lcom/squareup/print/payload/LabelAmountPair;)Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_0

    .line 84
    :cond_0
    iget-object p2, p0, Lcom/squareup/print/sections/TotalSection;->total:Lcom/squareup/print/payload/LabelAmountPair;

    invoke-virtual {p1, p2}, Lcom/squareup/print/ThermalBitmapBuilder;->boldTitleAndBigAmount(Lcom/squareup/print/payload/LabelAmountPair;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 87
    :goto_0
    iget-object p2, p0, Lcom/squareup/print/sections/TotalSection;->inclusiveTaxTotal:Ljava/lang/String;

    if-eqz p2, :cond_1

    .line 88
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 89
    iget-object p2, p0, Lcom/squareup/print/sections/TotalSection;->inclusiveTaxTotal:Ljava/lang/String;

    invoke-virtual {p1, p2}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    :cond_1
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 58
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 59
    :cond_1
    check-cast p1, Lcom/squareup/print/sections/TotalSection;

    .line 60
    iget-object v2, p0, Lcom/squareup/print/sections/TotalSection;->inclusiveTaxTotal:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/print/sections/TotalSection;->inclusiveTaxTotal:Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/print/sections/TotalSection;->total:Lcom/squareup/print/payload/LabelAmountPair;

    iget-object v3, p1, Lcom/squareup/print/sections/TotalSection;->total:Lcom/squareup/print/payload/LabelAmountPair;

    .line 61
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/print/sections/TotalSection;->formalReceiptTotal:Lcom/squareup/print/payload/LabelAmountPair;

    iget-object p1, p1, Lcom/squareup/print/sections/TotalSection;->formalReceiptTotal:Lcom/squareup/print/payload/LabelAmountPair;

    .line 62
    invoke-static {v2, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 66
    iget-object v1, p0, Lcom/squareup/print/sections/TotalSection;->inclusiveTaxTotal:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/print/sections/TotalSection;->total:Lcom/squareup/print/payload/LabelAmountPair;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/print/sections/TotalSection;->formalReceiptTotal:Lcom/squareup/print/payload/LabelAmountPair;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V
    .locals 1

    const/4 v0, 0x0

    .line 70
    invoke-direct {p0, p1, v0}, Lcom/squareup/print/sections/TotalSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;Z)V

    return-void
.end method

.method public renderBitmapFormalReceipt(Lcom/squareup/print/ThermalBitmapBuilder;)V
    .locals 1

    const/4 v0, 0x1

    .line 74
    invoke-direct {p0, p1, v0}, Lcom/squareup/print/sections/TotalSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;Z)V

    return-void
.end method
