.class public final Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;
.super Ljava/lang/Object;
.source "MultipleTaxBreakdownSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/sections/MultipleTaxBreakdownSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VatRow"
.end annotation


# instance fields
.field public final bold:Z

.field public final gross:Ljava/lang/String;

.field public final name:Ljava/lang/String;

.field public final net:Ljava/lang/String;

.field public final vat:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-boolean p1, p0, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->bold:Z

    .line 34
    iput-object p2, p0, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->name:Ljava/lang/String;

    .line 35
    iput-object p3, p0, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->net:Ljava/lang/String;

    .line 36
    iput-object p4, p0, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->vat:Ljava/lang/String;

    .line 37
    iput-object p5, p0, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->gross:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_b

    .line 43
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_4

    .line 45
    :cond_1
    check-cast p1, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;

    .line 47
    iget-boolean v2, p0, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->bold:Z

    iget-boolean v3, p1, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->bold:Z

    if-eq v2, v3, :cond_2

    return v1

    .line 48
    :cond_2
    iget-object v2, p0, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->name:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v3, p1, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    goto :goto_0

    :cond_3
    iget-object v2, p1, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->name:Ljava/lang/String;

    if-eqz v2, :cond_4

    :goto_0
    return v1

    .line 49
    :cond_4
    iget-object v2, p0, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->net:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v3, p1, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->net:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    goto :goto_1

    :cond_5
    iget-object v2, p1, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->net:Ljava/lang/String;

    if-eqz v2, :cond_6

    :goto_1
    return v1

    .line 50
    :cond_6
    iget-object v2, p0, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->vat:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v3, p1, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->vat:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    goto :goto_2

    :cond_7
    iget-object v2, p1, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->vat:Ljava/lang/String;

    if-eqz v2, :cond_8

    :goto_2
    return v1

    .line 51
    :cond_8
    iget-object v2, p0, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->gross:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->gross:Ljava/lang/String;

    if-eqz v2, :cond_9

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_a

    goto :goto_3

    :cond_9
    if-eqz p1, :cond_a

    :goto_3
    return v1

    :cond_a
    return v0

    :cond_b
    :goto_4
    return v1
.end method

.method public hashCode()I
    .locals 3

    .line 57
    iget-boolean v0, p0, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->bold:Z

    mul-int/lit8 v0, v0, 0x1f

    .line 58
    iget-object v1, p0, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->name:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 59
    iget-object v1, p0, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->net:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 60
    iget-object v1, p0, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->vat:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 61
    iget-object v1, p0, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->gross:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "VatRow{bold="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->bold:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", name=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", net=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->net:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", vat=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->vat:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", gross=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->gross:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
