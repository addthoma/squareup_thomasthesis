.class public Lcom/squareup/print/sections/HeaderSection;
.super Ljava/lang/Object;
.source "HeaderSection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/sections/HeaderSection$Builder;
    }
.end annotation


# instance fields
.field public final addressLines:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final businessAbn:Ljava/lang/String;

.field public final businessName:Ljava/lang/String;

.field public final date:Ljava/lang/String;

.field public final eMoneyReprintLabel:Ljava/lang/String;

.field public final employee:Ljava/lang/String;

.field public final formalReceiptName:Ljava/lang/String;

.field public final formalReceiptTitle:Ljava/lang/String;

.field public final logoUrl:Ljava/lang/String;

.field public final phoneNumber:Ljava/lang/String;

.field public final renderType:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

.field public final time:Ljava/lang/String;

.field public final transactionType:Ljava/lang/String;

.field public final twitter:Ljava/lang/String;

.field public final website:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/print/payload/ReceiptPayload$RenderType;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/print/payload/ReceiptPayload$RenderType;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    iput-object p1, p0, Lcom/squareup/print/sections/HeaderSection;->eMoneyReprintLabel:Ljava/lang/String;

    .line 114
    iput-object p2, p0, Lcom/squareup/print/sections/HeaderSection;->logoUrl:Ljava/lang/String;

    .line 115
    iput-object p6, p0, Lcom/squareup/print/sections/HeaderSection;->businessName:Ljava/lang/String;

    .line 116
    iput-object p7, p0, Lcom/squareup/print/sections/HeaderSection;->addressLines:Ljava/util/List;

    .line 117
    iput-object p8, p0, Lcom/squareup/print/sections/HeaderSection;->phoneNumber:Ljava/lang/String;

    .line 118
    iput-object p9, p0, Lcom/squareup/print/sections/HeaderSection;->twitter:Ljava/lang/String;

    .line 119
    iput-object p10, p0, Lcom/squareup/print/sections/HeaderSection;->website:Ljava/lang/String;

    .line 120
    iput-object p11, p0, Lcom/squareup/print/sections/HeaderSection;->businessAbn:Ljava/lang/String;

    .line 121
    iput-object p3, p0, Lcom/squareup/print/sections/HeaderSection;->date:Ljava/lang/String;

    .line 122
    iput-object p4, p0, Lcom/squareup/print/sections/HeaderSection;->time:Ljava/lang/String;

    .line 123
    iput-object p5, p0, Lcom/squareup/print/sections/HeaderSection;->employee:Ljava/lang/String;

    .line 124
    iput-object p12, p0, Lcom/squareup/print/sections/HeaderSection;->formalReceiptTitle:Ljava/lang/String;

    .line 125
    iput-object p13, p0, Lcom/squareup/print/sections/HeaderSection;->formalReceiptName:Ljava/lang/String;

    .line 126
    iput-object p14, p0, Lcom/squareup/print/sections/HeaderSection;->renderType:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    .line 127
    iput-object p15, p0, Lcom/squareup/print/sections/HeaderSection;->transactionType:Ljava/lang/String;

    return-void
.end method

.method public static createSection(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/print/ReceiptFormatter;Ljava/util/Date;Lcom/squareup/permissions/Employee;Lcom/squareup/print/payload/ReceiptPayload$RenderType;ZZLcom/squareup/settings/server/Features;)Lcom/squareup/print/sections/HeaderSection;
    .locals 1

    .line 32
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object p0

    .line 34
    new-instance v0, Lcom/squareup/print/sections/HeaderSection$Builder;

    invoke-direct {v0}, Lcom/squareup/print/sections/HeaderSection$Builder;-><init>()V

    if-eqz p6, :cond_0

    .line 37
    invoke-virtual {p1}, Lcom/squareup/print/ReceiptFormatter;->getEMoneyReprintReceiptLabel()Ljava/lang/String;

    move-result-object p6

    iput-object p6, v0, Lcom/squareup/print/sections/HeaderSection$Builder;->eMoneyReprintLabel:Ljava/lang/String;

    .line 40
    :cond_0
    sget-object p6, Lcom/squareup/settings/server/Features$Feature;->PRINT_MERCHANT_LOGO_ON_RECEIPTS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p7, p6}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p6

    if-eqz p6, :cond_1

    .line 41
    invoke-virtual {p0}, Lcom/squareup/settings/server/UserSettings;->getPrintedReceiptImageUrl()Ljava/lang/String;

    move-result-object p6

    iput-object p6, v0, Lcom/squareup/print/sections/HeaderSection$Builder;->logoUrl:Ljava/lang/String;

    .line 44
    :cond_1
    invoke-virtual {p1, p0}, Lcom/squareup/print/ReceiptFormatter;->addressLines(Lcom/squareup/settings/server/UserSettings;)Ljava/util/List;

    move-result-object p6

    iput-object p6, v0, Lcom/squareup/print/sections/HeaderSection$Builder;->addressLines:Ljava/util/List;

    .line 45
    invoke-virtual {p0}, Lcom/squareup/settings/server/UserSettings;->getBusinessName()Ljava/lang/String;

    move-result-object p6

    iput-object p6, v0, Lcom/squareup/print/sections/HeaderSection$Builder;->businessName:Ljava/lang/String;

    .line 46
    invoke-virtual {p0}, Lcom/squareup/settings/server/UserSettings;->getPhone()Ljava/lang/String;

    move-result-object p6

    invoke-virtual {p1, p6}, Lcom/squareup/print/ReceiptFormatter;->phoneNumberOrNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p6

    iput-object p6, v0, Lcom/squareup/print/sections/HeaderSection$Builder;->phoneNumber:Ljava/lang/String;

    .line 47
    invoke-virtual {p0}, Lcom/squareup/settings/server/UserSettings;->getWebsite()Ljava/lang/String;

    move-result-object p6

    iput-object p6, v0, Lcom/squareup/print/sections/HeaderSection$Builder;->website:Ljava/lang/String;

    .line 48
    invoke-virtual {p0}, Lcom/squareup/settings/server/UserSettings;->getTwitter()Ljava/lang/String;

    move-result-object p6

    invoke-virtual {p1, p6}, Lcom/squareup/print/ReceiptFormatter;->twitterOrNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p6

    iput-object p6, v0, Lcom/squareup/print/sections/HeaderSection$Builder;->twitter:Ljava/lang/String;

    .line 49
    invoke-virtual {p1, p0}, Lcom/squareup/print/ReceiptFormatter;->businessAbn(Lcom/squareup/settings/server/UserSettings;)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/squareup/print/sections/HeaderSection$Builder;->businessAbn:Ljava/lang/String;

    .line 51
    invoke-virtual {p1, p2}, Lcom/squareup/print/ReceiptFormatter;->getDateDetailString(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/squareup/print/sections/HeaderSection$Builder;->date:Ljava/lang/String;

    .line 52
    invoke-virtual {p1, p2}, Lcom/squareup/print/ReceiptFormatter;->getTimeDetailStringWithoutSeconds(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/squareup/print/sections/HeaderSection$Builder;->time:Ljava/lang/String;

    .line 53
    invoke-virtual {p1, p3}, Lcom/squareup/print/ReceiptFormatter;->employee(Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/squareup/print/sections/HeaderSection$Builder;->employee:Ljava/lang/String;

    .line 54
    invoke-virtual {p1}, Lcom/squareup/print/ReceiptFormatter;->getFormalReceiptName()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/squareup/print/sections/HeaderSection$Builder;->formalReceiptName:Ljava/lang/String;

    .line 55
    invoke-virtual {p1}, Lcom/squareup/print/ReceiptFormatter;->getFormalReceiptTitle()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lcom/squareup/print/sections/HeaderSection$Builder;->formalReceiptTitle:Ljava/lang/String;

    .line 56
    iput-object p4, v0, Lcom/squareup/print/sections/HeaderSection$Builder;->renderType:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    if-eqz p5, :cond_2

    .line 58
    invoke-virtual {p1}, Lcom/squareup/print/ReceiptFormatter;->getRefundTransactionTypeHeader()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 59
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/print/ReceiptFormatter;->getPurchaseTransactionTypeHeader()Ljava/lang/String;

    move-result-object p0

    :goto_0
    iput-object p0, v0, Lcom/squareup/print/sections/HeaderSection$Builder;->transactionType:Ljava/lang/String;

    .line 61
    invoke-virtual {v0}, Lcom/squareup/print/sections/HeaderSection$Builder;->build()Lcom/squareup/print/sections/HeaderSection;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_20

    .line 133
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto/16 :goto_f

    .line 135
    :cond_1
    check-cast p1, Lcom/squareup/print/sections/HeaderSection;

    .line 137
    iget-object v2, p0, Lcom/squareup/print/sections/HeaderSection;->eMoneyReprintLabel:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v3, p1, Lcom/squareup/print/sections/HeaderSection;->eMoneyReprintLabel:Ljava/lang/String;

    .line 138
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0

    :cond_2
    iget-object v2, p1, Lcom/squareup/print/sections/HeaderSection;->eMoneyReprintLabel:Ljava/lang/String;

    if-eqz v2, :cond_3

    :goto_0
    return v1

    .line 142
    :cond_3
    iget-object v2, p0, Lcom/squareup/print/sections/HeaderSection;->logoUrl:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v3, p1, Lcom/squareup/print/sections/HeaderSection;->logoUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    goto :goto_1

    :cond_4
    iget-object v2, p1, Lcom/squareup/print/sections/HeaderSection;->logoUrl:Ljava/lang/String;

    if-eqz v2, :cond_5

    :goto_1
    return v1

    .line 143
    :cond_5
    iget-object v2, p0, Lcom/squareup/print/sections/HeaderSection;->date:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v3, p1, Lcom/squareup/print/sections/HeaderSection;->date:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    goto :goto_2

    :cond_6
    iget-object v2, p1, Lcom/squareup/print/sections/HeaderSection;->date:Ljava/lang/String;

    if-eqz v2, :cond_7

    :goto_2
    return v1

    .line 144
    :cond_7
    iget-object v2, p0, Lcom/squareup/print/sections/HeaderSection;->time:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v3, p1, Lcom/squareup/print/sections/HeaderSection;->time:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    goto :goto_3

    :cond_8
    iget-object v2, p1, Lcom/squareup/print/sections/HeaderSection;->time:Ljava/lang/String;

    if-eqz v2, :cond_9

    :goto_3
    return v1

    .line 145
    :cond_9
    iget-object v2, p0, Lcom/squareup/print/sections/HeaderSection;->employee:Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v3, p1, Lcom/squareup/print/sections/HeaderSection;->employee:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    goto :goto_4

    :cond_a
    iget-object v2, p1, Lcom/squareup/print/sections/HeaderSection;->employee:Ljava/lang/String;

    if-eqz v2, :cond_b

    :goto_4
    return v1

    .line 146
    :cond_b
    iget-object v2, p0, Lcom/squareup/print/sections/HeaderSection;->formalReceiptTitle:Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v3, p1, Lcom/squareup/print/sections/HeaderSection;->formalReceiptTitle:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    goto :goto_5

    :cond_c
    iget-object v2, p1, Lcom/squareup/print/sections/HeaderSection;->formalReceiptTitle:Ljava/lang/String;

    if-eqz v2, :cond_d

    :goto_5
    return v1

    .line 150
    :cond_d
    iget-object v2, p0, Lcom/squareup/print/sections/HeaderSection;->formalReceiptName:Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v3, p1, Lcom/squareup/print/sections/HeaderSection;->formalReceiptName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    goto :goto_6

    :cond_e
    iget-object v2, p1, Lcom/squareup/print/sections/HeaderSection;->formalReceiptName:Ljava/lang/String;

    if-eqz v2, :cond_f

    :goto_6
    return v1

    .line 154
    :cond_f
    iget-object v2, p0, Lcom/squareup/print/sections/HeaderSection;->businessName:Ljava/lang/String;

    if-eqz v2, :cond_10

    iget-object v3, p1, Lcom/squareup/print/sections/HeaderSection;->businessName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    goto :goto_7

    :cond_10
    iget-object v2, p1, Lcom/squareup/print/sections/HeaderSection;->businessName:Ljava/lang/String;

    if-eqz v2, :cond_11

    :goto_7
    return v1

    .line 157
    :cond_11
    iget-object v2, p0, Lcom/squareup/print/sections/HeaderSection;->addressLines:Ljava/util/List;

    if-eqz v2, :cond_12

    iget-object v3, p1, Lcom/squareup/print/sections/HeaderSection;->addressLines:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    goto :goto_8

    :cond_12
    iget-object v2, p1, Lcom/squareup/print/sections/HeaderSection;->addressLines:Ljava/util/List;

    if-eqz v2, :cond_13

    :goto_8
    return v1

    .line 160
    :cond_13
    iget-object v2, p0, Lcom/squareup/print/sections/HeaderSection;->phoneNumber:Ljava/lang/String;

    if-eqz v2, :cond_14

    iget-object v3, p1, Lcom/squareup/print/sections/HeaderSection;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    goto :goto_9

    :cond_14
    iget-object v2, p1, Lcom/squareup/print/sections/HeaderSection;->phoneNumber:Ljava/lang/String;

    if-eqz v2, :cond_15

    :goto_9
    return v1

    .line 163
    :cond_15
    iget-object v2, p0, Lcom/squareup/print/sections/HeaderSection;->twitter:Ljava/lang/String;

    if-eqz v2, :cond_16

    iget-object v3, p1, Lcom/squareup/print/sections/HeaderSection;->twitter:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    goto :goto_a

    :cond_16
    iget-object v2, p1, Lcom/squareup/print/sections/HeaderSection;->twitter:Ljava/lang/String;

    if-eqz v2, :cond_17

    :goto_a
    return v1

    .line 164
    :cond_17
    iget-object v2, p0, Lcom/squareup/print/sections/HeaderSection;->website:Ljava/lang/String;

    if-eqz v2, :cond_18

    iget-object v3, p1, Lcom/squareup/print/sections/HeaderSection;->website:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_19

    goto :goto_b

    :cond_18
    iget-object v2, p1, Lcom/squareup/print/sections/HeaderSection;->website:Ljava/lang/String;

    if-eqz v2, :cond_19

    :goto_b
    return v1

    .line 165
    :cond_19
    iget-object v2, p0, Lcom/squareup/print/sections/HeaderSection;->businessAbn:Ljava/lang/String;

    if-eqz v2, :cond_1a

    iget-object v3, p1, Lcom/squareup/print/sections/HeaderSection;->businessAbn:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1b

    goto :goto_c

    :cond_1a
    iget-object v2, p1, Lcom/squareup/print/sections/HeaderSection;->businessAbn:Ljava/lang/String;

    if-eqz v2, :cond_1b

    :goto_c
    return v1

    .line 168
    :cond_1b
    iget-object v2, p0, Lcom/squareup/print/sections/HeaderSection;->renderType:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    if-eqz v2, :cond_1c

    iget-object v3, p1, Lcom/squareup/print/sections/HeaderSection;->renderType:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    invoke-virtual {v2, v3}, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1d

    goto :goto_d

    :cond_1c
    iget-object v2, p1, Lcom/squareup/print/sections/HeaderSection;->renderType:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    if-eqz v2, :cond_1d

    :goto_d
    return v1

    .line 172
    :cond_1d
    iget-object v2, p0, Lcom/squareup/print/sections/HeaderSection;->transactionType:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/print/sections/HeaderSection;->transactionType:Ljava/lang/String;

    if-eqz v2, :cond_1e

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1f

    goto :goto_e

    :cond_1e
    if-eqz p1, :cond_1f

    :goto_e
    return v1

    :cond_1f
    return v0

    :cond_20
    :goto_f
    return v1
.end method

.method public hashCode()I
    .locals 3

    .line 181
    iget-object v0, p0, Lcom/squareup/print/sections/HeaderSection;->eMoneyReprintLabel:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 182
    iget-object v2, p0, Lcom/squareup/print/sections/HeaderSection;->logoUrl:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 183
    iget-object v2, p0, Lcom/squareup/print/sections/HeaderSection;->date:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 184
    iget-object v2, p0, Lcom/squareup/print/sections/HeaderSection;->time:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 185
    iget-object v2, p0, Lcom/squareup/print/sections/HeaderSection;->employee:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 186
    iget-object v2, p0, Lcom/squareup/print/sections/HeaderSection;->formalReceiptTitle:Ljava/lang/String;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 187
    iget-object v2, p0, Lcom/squareup/print/sections/HeaderSection;->formalReceiptName:Ljava/lang/String;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_6
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 188
    iget-object v2, p0, Lcom/squareup/print/sections/HeaderSection;->businessName:Ljava/lang/String;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_7
    const/4 v2, 0x0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 189
    iget-object v2, p0, Lcom/squareup/print/sections/HeaderSection;->addressLines:Ljava/util/List;

    if-eqz v2, :cond_8

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    goto :goto_8

    :cond_8
    const/4 v2, 0x0

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 190
    iget-object v2, p0, Lcom/squareup/print/sections/HeaderSection;->phoneNumber:Ljava/lang/String;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_9

    :cond_9
    const/4 v2, 0x0

    :goto_9
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 191
    iget-object v2, p0, Lcom/squareup/print/sections/HeaderSection;->twitter:Ljava/lang/String;

    if-eqz v2, :cond_a

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_a

    :cond_a
    const/4 v2, 0x0

    :goto_a
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 192
    iget-object v2, p0, Lcom/squareup/print/sections/HeaderSection;->website:Ljava/lang/String;

    if-eqz v2, :cond_b

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_b

    :cond_b
    const/4 v2, 0x0

    :goto_b
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 193
    iget-object v2, p0, Lcom/squareup/print/sections/HeaderSection;->businessAbn:Ljava/lang/String;

    if-eqz v2, :cond_c

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_c

    :cond_c
    const/4 v2, 0x0

    :goto_c
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 194
    iget-object v2, p0, Lcom/squareup/print/sections/HeaderSection;->renderType:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    if-eqz v2, :cond_d

    invoke-virtual {v2}, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->hashCode()I

    move-result v2

    goto :goto_d

    :cond_d
    const/4 v2, 0x0

    :goto_d
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 195
    iget-object v2, p0, Lcom/squareup/print/sections/HeaderSection;->transactionType:Ljava/lang/String;

    if-eqz v2, :cond_e

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_e
    add-int/2addr v0, v1

    return v0
.end method

.method public renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V
    .locals 8

    .line 200
    iget-object v0, p0, Lcom/squareup/print/sections/HeaderSection;->eMoneyReprintLabel:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/squareup/print/sections/HeaderSection;->eMoneyReprintLabel:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->eMoneyReprintHeader(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 204
    :cond_0
    iget-object v0, p0, Lcom/squareup/print/sections/HeaderSection;->logoUrl:Ljava/lang/String;

    sget-object v1, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->EXTRA_LARGE:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->logo(Ljava/lang/String;Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 206
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    const/4 v1, 0x3

    new-array v2, v1, [Ljava/lang/CharSequence;

    .line 207
    iget-object v3, p0, Lcom/squareup/print/sections/HeaderSection;->date:Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/squareup/print/sections/HeaderSection;->time:Ljava/lang/String;

    const/4 v5, 0x1

    aput-object v3, v2, v5

    iget-object v3, p0, Lcom/squareup/print/sections/HeaderSection;->employee:Ljava/lang/String;

    const/4 v6, 0x2

    aput-object v3, v2, v6

    invoke-static {v0, v2}, Lcom/squareup/print/util/PrintRendererUtils;->appendAsLinesIfNotBlank(Landroid/text/SpannableStringBuilder;[Ljava/lang/CharSequence;)V

    .line 208
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 210
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 211
    iget-object v3, p0, Lcom/squareup/print/sections/HeaderSection;->addressLines:Ljava/util/List;

    invoke-static {v2, v3}, Lcom/squareup/print/util/PrintRendererUtils;->appendAsLinesIfNotBlankList(Landroid/text/SpannableStringBuilder;Ljava/util/List;)V

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/CharSequence;

    .line 212
    iget-object v7, p0, Lcom/squareup/print/sections/HeaderSection;->phoneNumber:Ljava/lang/String;

    aput-object v7, v3, v4

    iget-object v4, p0, Lcom/squareup/print/sections/HeaderSection;->website:Ljava/lang/String;

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/squareup/print/sections/HeaderSection;->twitter:Ljava/lang/String;

    aput-object v4, v3, v6

    iget-object v4, p0, Lcom/squareup/print/sections/HeaderSection;->businessAbn:Ljava/lang/String;

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Lcom/squareup/print/util/PrintRendererUtils;->appendAsLinesIfNotBlank(Landroid/text/SpannableStringBuilder;[Ljava/lang/CharSequence;)V

    .line 214
    iget-object v1, p0, Lcom/squareup/print/sections/HeaderSection;->businessName:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/2addr v1, v5

    .line 215
    invoke-static {v2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v3

    xor-int/2addr v3, v5

    .line 217
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->hasNarrowContent()Z

    move-result v4

    if-eqz v4, :cond_3

    if-eqz v1, :cond_1

    .line 219
    iget-object v1, p0, Lcom/squareup/print/sections/HeaderSection;->businessName:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthHeadlineText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 220
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    :cond_1
    if-eqz v3, :cond_2

    .line 223
    invoke-virtual {p1, v2}, Lcom/squareup/print/ThermalBitmapBuilder;->singleColumn(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 224
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 227
    :cond_2
    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->singleColumn(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_0

    :cond_3
    if-eqz v3, :cond_5

    if-eqz v1, :cond_4

    .line 231
    iget-object v1, p0, Lcom/squareup/print/sections/HeaderSection;->businessName:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthHeadlineText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 232
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 234
    :cond_4
    invoke-virtual {p1, v2, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsRightExpandsLeftOverflows(Ljava/lang/CharSequence;Ljava/lang/String;)Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_0

    .line 237
    :cond_5
    iget-object v1, p0, Lcom/squareup/print/sections/HeaderSection;->businessName:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsLeftHeadlineOrCollapseRightAlignPadded(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 241
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->smallSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 243
    iget-object v0, p0, Lcom/squareup/print/sections/HeaderSection;->renderType:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    sget-object v1, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    if-eq v0, v1, :cond_6

    iget-object v0, p0, Lcom/squareup/print/sections/HeaderSection;->renderType:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    sget-object v1, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->FORMAL_RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    if-ne v0, v1, :cond_7

    .line 244
    :cond_6
    iget-object v0, p0, Lcom/squareup/print/sections/HeaderSection;->transactionType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->transactionTypeBlock(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 247
    :cond_7
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    return-void
.end method
