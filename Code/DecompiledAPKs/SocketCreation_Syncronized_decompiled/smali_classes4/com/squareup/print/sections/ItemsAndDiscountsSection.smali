.class public Lcom/squareup/print/sections/ItemsAndDiscountsSection;
.super Ljava/lang/Object;
.source "ItemsAndDiscountsSection.java"


# instance fields
.field public final comps:Lcom/squareup/print/payload/LabelAmountPair;

.field public final discount:Lcom/squareup/print/payload/LabelAmountPair;

.field public final discounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/print/sections/ItemSection;",
            ">;"
        }
    .end annotation
.end field

.field public final items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/print/sections/ItemSection;",
            ">;"
        }
    .end annotation
.end field

.field public final sectionHeader:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/print/sections/ItemSection;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/print/sections/ItemSection;",
            ">;",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219
    iput-object p1, p0, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->items:Ljava/util/List;

    .line 220
    iput-object p2, p0, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->discounts:Ljava/util/List;

    .line 221
    iput-object p3, p0, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->discount:Lcom/squareup/print/payload/LabelAmountPair;

    .line 222
    iput-object p4, p0, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->comps:Lcom/squareup/print/payload/LabelAmountPair;

    .line 223
    iput-object p5, p0, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->sectionHeader:Ljava/lang/String;

    return-void
.end method

.method private static annotateUntaxedItems(Lcom/squareup/settings/server/AccountStatusSettings;Ljava/util/List;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;)Z"
        }
    .end annotation

    .line 126
    invoke-static {p0}, Lcom/squareup/print/sections/SectionUtils;->isAustralia(Lcom/squareup/settings/server/AccountStatusSettings;)Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-static {p1}, Lcom/squareup/print/sections/SectionUtils;->hasBothTaxedAndUntaxedItems(Ljava/util/List;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static annotateVatItemsForMultipleTaxBreakdown(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/TaxBreakdown;)Z
    .locals 0

    .line 131
    invoke-static {p0}, Lcom/squareup/print/sections/SectionUtils;->canShowTaxBreakDownTable(Lcom/squareup/settings/server/AccountStatusSettings;)Z

    move-result p0

    if-eqz p0, :cond_0

    iget-object p0, p1, Lcom/squareup/util/TaxBreakdown;->taxBreakdownType:Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;

    sget-object p1, Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;->TABLE:Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;

    if-ne p0, p1, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static buildAllDiscounts(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;)Lcom/squareup/print/payload/LabelAmountPair;
    .locals 5

    .line 135
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getOrderAdjustments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1, v1}, Lcom/squareup/print/sections/SectionUtils;->calculateTotalNegativeDiscount(Ljava/util/List;ZZ)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    .line 137
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 138
    invoke-virtual {p0, p1}, Lcom/squareup/print/ReceiptFormatter;->discount(Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method private static buildCompDiscounts(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;)Lcom/squareup/print/payload/LabelAmountPair;
    .locals 5

    .line 153
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getOrderAdjustments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/squareup/print/sections/SectionUtils;->calculateTotalNegativeDiscount(Ljava/util/List;ZZ)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    .line 155
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 156
    invoke-virtual {p0, p1}, Lcom/squareup/print/ReceiptFormatter;->comp(Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method private static buildItemizedCart(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/util/TaxBreakdown;Ljava/util/List;ZZZZLcom/squareup/util/Res;Z)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/ReceiptFormatter;",
            "Lcom/squareup/util/TaxBreakdown;",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintableOrderItem;",
            ">;ZZZZ",
            "Lcom/squareup/util/Res;",
            "Z)",
            "Ljava/util/List<",
            "Lcom/squareup/print/sections/ItemSection;",
            ">;"
        }
    .end annotation

    .line 178
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 180
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v5, v2

    check-cast v5, Lcom/squareup/print/PrintableOrderItem;

    .line 181
    invoke-interface {v5}, Lcom/squareup/print/PrintableOrderItem;->isVoided()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v10, 0x0

    move-object v3, p0

    move-object v4, p1

    move/from16 v6, p3

    move/from16 v7, p4

    move/from16 v8, p5

    move/from16 v9, p6

    move-object/from16 v11, p7

    move/from16 v12, p8

    .line 182
    invoke-static/range {v3 .. v12}, Lcom/squareup/print/sections/ItemSection;->createItemSection(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/util/TaxBreakdown;Lcom/squareup/print/PrintableOrderItem;ZZZZLjava/lang/String;Lcom/squareup/util/Res;Z)Lcom/squareup/print/sections/ItemSection;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private static buildItemizedCartWithDiningOptions(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/util/TaxBreakdown;Ljava/util/List;ZZZZLjava/util/Locale;Lcom/squareup/util/Res;Z)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/ReceiptFormatter;",
            "Lcom/squareup/util/TaxBreakdown;",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintableOrderItem;",
            ">;ZZZZ",
            "Ljava/util/Locale;",
            "Lcom/squareup/util/Res;",
            "Z)",
            "Ljava/util/List<",
            "Lcom/squareup/print/sections/ItemSection;",
            ">;"
        }
    .end annotation

    .line 195
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 197
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v5, v2

    check-cast v5, Lcom/squareup/print/PrintableOrderItem;

    .line 198
    invoke-interface {v5}, Lcom/squareup/print/PrintableOrderItem;->getSelectedDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v2

    if-nez v2, :cond_1

    const/4 v2, 0x0

    move-object/from16 v13, p7

    goto :goto_1

    .line 199
    :cond_1
    invoke-interface {v5}, Lcom/squareup/print/PrintableOrderItem;->getSelectedDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/checkout/DiningOption;->getName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v13, p7

    invoke-virtual {v2, v13}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    move-object v10, v2

    .line 201
    invoke-interface {v5}, Lcom/squareup/print/PrintableOrderItem;->isVoided()Z

    move-result v2

    if-nez v2, :cond_0

    move-object v3, p0

    move-object v4, p1

    move/from16 v6, p3

    move/from16 v7, p4

    move/from16 v8, p5

    move/from16 v9, p6

    move-object/from16 v11, p8

    move/from16 v12, p9

    .line 202
    invoke-static/range {v3 .. v12}, Lcom/squareup/print/sections/ItemSection;->createItemSection(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/util/TaxBreakdown;Lcom/squareup/print/PrintableOrderItem;ZZZZLjava/lang/String;Lcom/squareup/util/Res;Z)Lcom/squareup/print/sections/ItemSection;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method private static buildItemizedDiscounts(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Z)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/ReceiptFormatter;",
            "Lcom/squareup/payment/Order;",
            "Z)",
            "Ljava/util/List<",
            "Lcom/squareup/print/sections/ItemSection;",
            ">;"
        }
    .end annotation

    .line 163
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 166
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getOrderAdjustments()Ljava/util/List;

    move-result-object p1

    const/4 v1, 0x1

    invoke-static {p1, v1, p2}, Lcom/squareup/print/sections/SectionUtils;->findAdjustments(Ljava/util/List;ZZ)Ljava/util/List;

    move-result-object p1

    .line 167
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/payment/OrderAdjustment;

    .line 168
    invoke-static {p0, p2}, Lcom/squareup/print/sections/ItemSection;->createItemSection(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/OrderAdjustment;)Lcom/squareup/print/sections/ItemSection;

    move-result-object p2

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private static buildNoncompDiscounts(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;)Lcom/squareup/print/payload/LabelAmountPair;
    .locals 5

    .line 144
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getOrderAdjustments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/squareup/print/sections/SectionUtils;->calculateTotalNegativeDiscount(Ljava/util/List;ZZ)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    .line 146
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 147
    invoke-virtual {p0, p1}, Lcom/squareup/print/ReceiptFormatter;->discount(Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public static fromOrder(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/util/TaxBreakdown;Lcom/squareup/settings/server/Features;Ljava/util/Locale;ZLcom/squareup/util/Res;)Lcom/squareup/print/sections/ItemsAndDiscountsSection;
    .locals 10

    move-object v1, p4

    move-object v3, p2

    .line 52
    invoke-static {p2, p4}, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->showDiningOptions(Lcom/squareup/payment/Order;Lcom/squareup/settings/server/Features;)Z

    move-result v6

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_SEE_SEATING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p4, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v9

    move-object v0, p0

    move-object v2, p1

    move-object v4, p3

    move/from16 v5, p6

    move-object v7, p5

    move-object/from16 v8, p7

    .line 51
    invoke-static/range {v0 .. v9}, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->fromOrder(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/util/TaxBreakdown;ZZLjava/util/Locale;Lcom/squareup/util/Res;Z)Lcom/squareup/print/sections/ItemsAndDiscountsSection;

    move-result-object v0

    return-object v0
.end method

.method private static fromOrder(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/util/TaxBreakdown;ZZLjava/util/Locale;Lcom/squareup/util/Res;Z)Lcom/squareup/print/sections/ItemsAndDiscountsSection;
    .locals 11

    move-object v0, p0

    .line 63
    invoke-virtual {p3}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v1

    .line 66
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 67
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/CartItem;

    .line 68
    invoke-virtual {v3}, Lcom/squareup/checkout/CartItem;->isInteresting()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 69
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    if-eqz p6, :cond_2

    .line 75
    invoke-virtual {p3}, Lcom/squareup/payment/Order;->getDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v1

    move-object/from16 v7, p7

    invoke-static {v2, v1, v7}, Lcom/squareup/itemsorter/ItemSorter;->sortItemsByDiningOption(Ljava/util/List;Lcom/squareup/itemsorter/SortableDiningOption;Ljava/util/Locale;)Ljava/util/List;

    move-result-object v2

    goto :goto_1

    :cond_2
    move-object/from16 v7, p7

    .line 78
    :goto_1
    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REDUCE_PRINTING_WASTE:Lcom/squareup/settings/server/Features$Feature;

    move-object v3, p1

    invoke-interface {p1, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 80
    invoke-static {v2}, Lcom/squareup/print/sections/CoalescedItem;->coalesce(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 83
    :cond_3
    invoke-static {p1}, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->showItemizedDiscounts(Lcom/squareup/settings/server/Features;)Z

    move-result v10

    .line 84
    invoke-static {p0, v2}, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->annotateUntaxedItems(Lcom/squareup/settings/server/AccountStatusSettings;Ljava/util/List;)Z

    move-result v3

    move-object v1, p4

    .line 86
    invoke-static {p0, p4}, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->annotateVatItemsForMultipleTaxBreakdown(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/TaxBreakdown;)Z

    move-result v4

    .line 87
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->isHideModifiersOnReceiptsEnabled()Z

    move-result v5

    if-eqz p6, :cond_4

    .line 90
    invoke-static {v2}, Lcom/squareup/print/PrintablePaymentOrder;->convertCartItemsToItems(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    move-object v0, p2

    move-object v1, p4

    move/from16 v6, p5

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move/from16 v9, p9

    .line 89
    invoke-static/range {v0 .. v9}, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->buildItemizedCartWithDiningOptions(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/util/TaxBreakdown;Ljava/util/List;ZZZZLjava/util/Locale;Lcom/squareup/util/Res;Z)Ljava/util/List;

    move-result-object v0

    goto :goto_2

    .line 94
    :cond_4
    invoke-static {v2}, Lcom/squareup/print/PrintablePaymentOrder;->convertCartItemsToItems(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    move-object v0, p2

    move-object v1, p4

    move/from16 v6, p5

    move-object/from16 v7, p8

    move/from16 v8, p9

    .line 93
    invoke-static/range {v0 .. v8}, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->buildItemizedCart(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/util/TaxBreakdown;Ljava/util/List;ZZZZLcom/squareup/util/Res;Z)Ljava/util/List;

    move-result-object v0

    :goto_2
    const/4 v1, 0x0

    if-eqz v10, :cond_5

    xor-int/lit8 v2, p5, 0x1

    move-object v3, p2

    move-object v4, p3

    .line 102
    invoke-static {p2, p3, v2}, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->buildItemizedDiscounts(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Z)Ljava/util/List;

    move-result-object v2

    move-object v5, v1

    goto :goto_3

    :cond_5
    move-object v3, p2

    move-object v4, p3

    .line 105
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    if-eqz p5, :cond_6

    .line 107
    invoke-static {p2, p3}, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->buildNoncompDiscounts(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object v5

    goto :goto_3

    .line 108
    :cond_6
    invoke-static {p2, p3}, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->buildAllDiscounts(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object v5

    :goto_3
    if-eqz p5, :cond_7

    .line 111
    invoke-static {p2, p3}, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->buildCompDiscounts(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object v3

    goto :goto_4

    :cond_7
    move-object v3, v1

    .line 113
    :goto_4
    invoke-virtual {p3}, Lcom/squareup/payment/Order;->hasReturn()Z

    move-result v4

    if-eqz v4, :cond_8

    sget v1, Lcom/squareup/print/R$string;->receipt_header_purchase:I

    move-object/from16 v4, p8

    .line 114
    invoke-interface {v4, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 117
    :cond_8
    new-instance v4, Lcom/squareup/print/sections/ItemsAndDiscountsSection;

    move-object p0, v4

    move-object p1, v0

    move-object p2, v2

    move-object p3, v5

    move-object p4, v3

    move-object/from16 p5, v1

    invoke-direct/range {p0 .. p5}, Lcom/squareup/print/sections/ItemsAndDiscountsSection;-><init>(Ljava/util/List;Ljava/util/List;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;Ljava/lang/String;)V

    return-object v4
.end method

.method private renderItemSection(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/print/sections/ItemSection;)V
    .locals 4

    .line 302
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/CharSequence;

    .line 303
    iget-object v2, p2, Lcom/squareup/print/sections/ItemSection;->baseAmountInputs:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p2, Lcom/squareup/print/sections/ItemSection;->variationAndModifiers:Ljava/lang/String;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/print/util/PrintRendererUtils;->appendAsLinesIfNotBlank(Landroid/text/SpannableStringBuilder;[Ljava/lang/CharSequence;)V

    .line 307
    iget-object v1, p2, Lcom/squareup/print/sections/ItemSection;->nameAndQuantity:Ljava/lang/String;

    iget-object v2, p2, Lcom/squareup/print/sections/ItemSection;->totalPrice:Ljava/lang/String;

    invoke-virtual {p1, v1, v2, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->multilineTitleAmountAndText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 309
    iget-object v0, p2, Lcom/squareup/print/sections/ItemSection;->note:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 310
    iget-object p2, p2, Lcom/squareup/print/sections/ItemSection;->note:Ljava/lang/String;

    invoke-virtual {p1, p2}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthItalicText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    :cond_0
    return-void
.end method

.method private static showDiningOptions(Lcom/squareup/payment/Order;Lcom/squareup/settings/server/Features;)Z
    .locals 1

    .line 56
    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->DINING_OPTIONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static showItemizedDiscounts(Lcom/squareup/settings/server/Features;)Z
    .locals 1

    .line 121
    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->PRINT_ITEMIZED_DISCOUNTS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p0, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p0

    return p0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_8

    .line 228
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_2

    .line 230
    :cond_1
    check-cast p1, Lcom/squareup/print/sections/ItemsAndDiscountsSection;

    .line 232
    iget-object v2, p0, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->items:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->items:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    return v1

    .line 233
    :cond_2
    iget-object v2, p0, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->discounts:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->discounts:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    return v1

    .line 234
    :cond_3
    iget-object v2, p0, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->discount:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v2, :cond_4

    iget-object v3, p1, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->discount:Lcom/squareup/print/payload/LabelAmountPair;

    invoke-virtual {v2, v3}, Lcom/squareup/print/payload/LabelAmountPair;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    goto :goto_0

    :cond_4
    iget-object v2, p1, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->discount:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v2, :cond_5

    :goto_0
    return v1

    .line 235
    :cond_5
    iget-object v2, p0, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->comps:Lcom/squareup/print/payload/LabelAmountPair;

    iget-object p1, p1, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->comps:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v2, :cond_6

    invoke-virtual {v2, p1}, Lcom/squareup/print/payload/LabelAmountPair;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_7

    goto :goto_1

    :cond_6
    if-eqz p1, :cond_7

    :goto_1
    return v1

    :cond_7
    return v0

    :cond_8
    :goto_2
    return v1
.end method

.method public hashCode()I
    .locals 3

    .line 241
    iget-object v0, p0, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    .line 242
    iget-object v1, p0, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->discounts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 243
    iget-object v1, p0, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->discount:Lcom/squareup/print/payload/LabelAmountPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/print/payload/LabelAmountPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 244
    iget-object v1, p0, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->comps:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/print/payload/LabelAmountPair;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    return v0
.end method

.method public renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V
    .locals 6

    .line 251
    iget-object v0, p0, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->sectionHeader:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 252
    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->appendSectionHeader(Ljava/lang/CharSequence;)V

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/print/sections/ItemSection;

    .line 255
    iget-object v5, v4, Lcom/squareup/print/sections/ItemSection;->diningOptionName:Ljava/lang/String;

    if-eqz v5, :cond_2

    .line 256
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    if-nez v3, :cond_1

    .line 258
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 260
    :cond_1
    invoke-virtual {p1, v5, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->diningOptionHeader(Ljava/lang/CharSequence;Z)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 261
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->smallSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_1

    :cond_2
    if-eqz v3, :cond_3

    .line 264
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->lightDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 265
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_1

    .line 267
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 270
    :goto_1
    invoke-direct {p0, p1, v4}, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->renderItemSection(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/print/sections/ItemSection;)V

    const/4 v3, 0x0

    move-object v2, v5

    goto :goto_0

    .line 276
    :cond_4
    iget-object v0, p0, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->discounts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 277
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 278
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->lightDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 281
    :cond_5
    iget-object v0, p0, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->discounts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/sections/ItemSection;

    .line 282
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 283
    invoke-direct {p0, p1, v1}, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->renderItemSection(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/print/sections/ItemSection;)V

    goto :goto_2

    .line 286
    :cond_6
    iget-object v0, p0, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->discount:Lcom/squareup/print/payload/LabelAmountPair;

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->comps:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v0, :cond_8

    .line 287
    :cond_7
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 290
    :cond_8
    iget-object v0, p0, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->discount:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v0, :cond_9

    .line 291
    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->titleAndAmount(Lcom/squareup/print/payload/LabelAmountPair;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 294
    :cond_9
    iget-object v0, p0, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->comps:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v0, :cond_a

    .line 295
    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->titleAndAmount(Lcom/squareup/print/payload/LabelAmountPair;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 298
    :cond_a
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    return-void
.end method
