.class public interface abstract Lcom/squareup/print/PrintoutDispatcher;
.super Ljava/lang/Object;
.source "PrintoutDispatcher.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J(\u0010\u0002\u001a\u000c\u0012\u0008\u0012\u00060\u0004j\u0002`\u00050\u00032\u0006\u0010\u0006\u001a\u00020\u00072\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tH&J(\u0010\u000b\u001a\u000c\u0012\u0008\u0012\u00060\u0004j\u0002`\u00050\u00032\u0006\u0010\u0006\u001a\u00020\u000c2\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tH&J(\u0010\r\u001a\u000c\u0012\u0008\u0012\u00060\u0004j\u0002`\u00050\u00032\u0006\u0010\u0006\u001a\u00020\u000e2\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tH&J(\u0010\u000f\u001a\u000c\u0012\u0008\u0012\u00060\u0004j\u0002`\u00050\u00032\u0006\u0010\u0006\u001a\u00020\u00102\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tH&\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/print/PrintoutDispatcher;",
        "",
        "dispatchAuthSlip",
        "Lio/reactivex/Single;",
        "Lcom/squareup/print/PrintJob$PrintAttempt$Result;",
        "Lcom/squareup/print/PrintJobResult;",
        "printout",
        "Lcom/squareup/print/AuthSlipPrintout;",
        "target",
        "",
        "Lcom/squareup/print/PrinterStation;",
        "dispatchOrderTicket",
        "Lcom/squareup/print/TicketStubPrintout;",
        "dispatchReceipt",
        "Lcom/squareup/print/ReceiptPrintout;",
        "dispatchVoidTicket",
        "Lcom/squareup/print/VoidTicketPrintout;",
        "print_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract dispatchAuthSlip(Lcom/squareup/print/AuthSlipPrintout;Ljava/util/Collection;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/AuthSlipPrintout;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/print/PrinterStation;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/print/PrintJob$PrintAttempt$Result;",
            ">;"
        }
    .end annotation
.end method

.method public abstract dispatchOrderTicket(Lcom/squareup/print/TicketStubPrintout;Ljava/util/Collection;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/TicketStubPrintout;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/print/PrinterStation;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/print/PrintJob$PrintAttempt$Result;",
            ">;"
        }
    .end annotation
.end method

.method public abstract dispatchReceipt(Lcom/squareup/print/ReceiptPrintout;Ljava/util/Collection;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/ReceiptPrintout;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/print/PrinterStation;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/print/PrintJob$PrintAttempt$Result;",
            ">;"
        }
    .end annotation
.end method

.method public abstract dispatchVoidTicket(Lcom/squareup/print/VoidTicketPrintout;Ljava/util/Collection;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/VoidTicketPrintout;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/print/PrinterStation;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/print/PrintJob$PrintAttempt$Result;",
            ">;"
        }
    .end annotation
.end method
