.class public Lcom/squareup/print/PrinterEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "PrinterEvent.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/PrinterEvent$AttemptCountPrinterEvent;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0016\u0008\u0016\u0018\u00002\u00020\u0001:\u0001\"Bc\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\n\u0012\u0010\u0008\u0002\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\u000c\u0012\n\u0008\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0010R\u0013\u0010\u0011\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0013\u0010\u0008\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0013R\u0015\u0010\u0015\u001a\u0004\u0018\u00010\n\u00a2\u0006\n\n\u0002\u0010\u0018\u001a\u0004\u0008\u0016\u0010\u0017R\u0015\u0010\t\u001a\u0004\u0018\u00010\n\u00a2\u0006\n\n\u0002\u0010\u0018\u001a\u0004\u0008\t\u0010\u0017R\u0013\u0010\u000f\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u0013R\u0013\u0010\u001a\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u0013R\u0013\u0010\u001c\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u0013R\u0013\u0010\u001e\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010\u0013R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010\u0013R\u0013\u0010\u000e\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008!\u0010\u0013\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/print/PrinterEvent;",
        "Lcom/squareup/eventstream/v1/EventStreamEvent;",
        "name",
        "Lcom/squareup/eventstream/v1/EventStream$Name;",
        "value",
        "",
        "printer",
        "Lcom/squareup/print/HardwarePrinter$HardwareInfo;",
        "detail",
        "isUnsupported",
        "",
        "stations",
        "",
        "Lcom/squareup/print/Station;",
        "targetId",
        "jobId",
        "(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V",
        "connectionType",
        "getConnectionType",
        "()Ljava/lang/String;",
        "getDetail",
        "hasUniqueId",
        "getHasUniqueId",
        "()Ljava/lang/Boolean;",
        "Ljava/lang/Boolean;",
        "getJobId",
        "manufacturerName",
        "getManufacturerName",
        "modelName",
        "getModelName",
        "printerId",
        "getPrinterId",
        "getStations",
        "getTargetId",
        "AttemptCountPrinterEvent",
        "hardware_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final connectionType:Ljava/lang/String;

.field private final detail:Ljava/lang/String;

.field private final hasUniqueId:Ljava/lang/Boolean;

.field private final isUnsupported:Ljava/lang/Boolean;

.field private final jobId:Ljava/lang/String;

.field private final manufacturerName:Ljava/lang/String;

.field private final modelName:Ljava/lang/String;

.field private final printerId:Ljava/lang/String;

.field private final stations:Ljava/lang/String;

.field private final targetId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/eventstream/v1/EventStream$Name;",
            "Ljava/lang/String;",
            "Lcom/squareup/print/HardwarePrinter$HardwareInfo;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Lcom/squareup/print/Station;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "value"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0, p1, p2}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    iput-object p4, p0, Lcom/squareup/print/PrinterEvent;->detail:Ljava/lang/String;

    iput-object p5, p0, Lcom/squareup/print/PrinterEvent;->isUnsupported:Ljava/lang/Boolean;

    iput-object p7, p0, Lcom/squareup/print/PrinterEvent;->targetId:Ljava/lang/String;

    iput-object p8, p0, Lcom/squareup/print/PrinterEvent;->jobId:Ljava/lang/String;

    const/4 p1, 0x0

    if-eqz p3, :cond_0

    .line 27
    iget-object p2, p3, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->connectionType:Lcom/squareup/print/ConnectionType;

    if-eqz p2, :cond_0

    iget-object p2, p2, Lcom/squareup/print/ConnectionType;->analyticsName:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object p2, p1

    :goto_0
    iput-object p2, p0, Lcom/squareup/print/PrinterEvent;->connectionType:Ljava/lang/String;

    if-eqz p3, :cond_1

    .line 28
    iget-object p2, p3, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->manufacturer:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object p2, p1

    :goto_1
    iput-object p2, p0, Lcom/squareup/print/PrinterEvent;->manufacturerName:Ljava/lang/String;

    if-eqz p3, :cond_2

    .line 29
    iget-object p2, p3, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->model:Ljava/lang/String;

    goto :goto_2

    :cond_2
    move-object p2, p1

    :goto_2
    iput-object p2, p0, Lcom/squareup/print/PrinterEvent;->modelName:Ljava/lang/String;

    if-eqz p3, :cond_3

    .line 30
    invoke-virtual {p3}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->hasUniqueAttribute()Z

    move-result p2

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    goto :goto_3

    :cond_3
    move-object p2, p1

    :goto_3
    iput-object p2, p0, Lcom/squareup/print/PrinterEvent;->hasUniqueId:Ljava/lang/Boolean;

    if-eqz p3, :cond_4

    .line 32
    invoke-virtual {p3}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->getId()Ljava/lang/String;

    move-result-object p2

    goto :goto_4

    :cond_4
    move-object p2, p1

    :goto_4
    if-eqz p2, :cond_5

    invoke-virtual {p3}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->getId()Ljava/lang/String;

    move-result-object p2

    const-string p4, "Unknown Printer"

    invoke-static {p2, p4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    xor-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_5

    invoke-virtual {p3}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->getId()Ljava/lang/String;

    move-result-object p2

    goto :goto_5

    :cond_5
    iget-object p2, p0, Lcom/squareup/print/PrinterEvent;->targetId:Ljava/lang/String;

    :goto_5
    iput-object p2, p0, Lcom/squareup/print/PrinterEvent;->printerId:Ljava/lang/String;

    if-eqz p6, :cond_6

    .line 33
    invoke-virtual {p6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_6
    iput-object p1, p0, Lcom/squareup/print/PrinterEvent;->stations:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 12

    move/from16 v0, p9

    and-int/lit8 v1, v0, 0x4

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 20
    move-object v1, v2

    check-cast v1, Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-object v6, v1

    goto :goto_0

    :cond_0
    move-object v6, p3

    :goto_0
    and-int/lit8 v1, v0, 0x8

    if-eqz v1, :cond_1

    .line 21
    move-object v1, v2

    check-cast v1, Ljava/lang/String;

    move-object v7, v1

    goto :goto_1

    :cond_1
    move-object/from16 v7, p4

    :goto_1
    and-int/lit8 v1, v0, 0x10

    if-eqz v1, :cond_2

    .line 22
    move-object v1, v2

    check-cast v1, Ljava/lang/Boolean;

    move-object v8, v1

    goto :goto_2

    :cond_2
    move-object/from16 v8, p5

    :goto_2
    and-int/lit8 v1, v0, 0x20

    if-eqz v1, :cond_3

    .line 23
    move-object v1, v2

    check-cast v1, Ljava/util/List;

    move-object v9, v1

    goto :goto_3

    :cond_3
    move-object/from16 v9, p6

    :goto_3
    and-int/lit8 v1, v0, 0x40

    if-eqz v1, :cond_4

    .line 24
    move-object v1, v2

    check-cast v1, Ljava/lang/String;

    move-object v10, v1

    goto :goto_4

    :cond_4
    move-object/from16 v10, p7

    :goto_4
    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_5

    .line 25
    move-object v0, v2

    check-cast v0, Ljava/lang/String;

    move-object v11, v0

    goto :goto_5

    :cond_5
    move-object/from16 v11, p8

    :goto_5
    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v3 .. v11}, Lcom/squareup/print/PrinterEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getConnectionType()Ljava/lang/String;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/print/PrinterEvent;->connectionType:Ljava/lang/String;

    return-object v0
.end method

.method public final getDetail()Ljava/lang/String;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/print/PrinterEvent;->detail:Ljava/lang/String;

    return-object v0
.end method

.method public final getHasUniqueId()Ljava/lang/Boolean;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/print/PrinterEvent;->hasUniqueId:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final getJobId()Ljava/lang/String;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/print/PrinterEvent;->jobId:Ljava/lang/String;

    return-object v0
.end method

.method public final getManufacturerName()Ljava/lang/String;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/print/PrinterEvent;->manufacturerName:Ljava/lang/String;

    return-object v0
.end method

.method public final getModelName()Ljava/lang/String;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/print/PrinterEvent;->modelName:Ljava/lang/String;

    return-object v0
.end method

.method public final getPrinterId()Ljava/lang/String;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/print/PrinterEvent;->printerId:Ljava/lang/String;

    return-object v0
.end method

.method public final getStations()Ljava/lang/String;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/print/PrinterEvent;->stations:Ljava/lang/String;

    return-object v0
.end method

.method public final getTargetId()Ljava/lang/String;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/print/PrinterEvent;->targetId:Ljava/lang/String;

    return-object v0
.end method

.method public final isUnsupported()Ljava/lang/Boolean;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/print/PrinterEvent;->isUnsupported:Ljava/lang/Boolean;

    return-object v0
.end method
