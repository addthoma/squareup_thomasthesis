.class public final Lcom/squareup/print/RegisterPrintModule_ProvideStubPayloadFactoryFactory;
.super Ljava/lang/Object;
.source "RegisterPrintModule_ProvideStubPayloadFactoryFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/print/payload/StubPayload$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final formatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/ReceiptFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/ReceiptFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/print/RegisterPrintModule_ProvideStubPayloadFactoryFactory;->formatterProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/print/RegisterPrintModule_ProvideStubPayloadFactoryFactory;->settingsProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/print/RegisterPrintModule_ProvideStubPayloadFactoryFactory;->employeeManagementProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/print/RegisterPrintModule_ProvideStubPayloadFactoryFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/ReceiptFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;)",
            "Lcom/squareup/print/RegisterPrintModule_ProvideStubPayloadFactoryFactory;"
        }
    .end annotation

    .line 44
    new-instance v0, Lcom/squareup/print/RegisterPrintModule_ProvideStubPayloadFactoryFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/print/RegisterPrintModule_ProvideStubPayloadFactoryFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideStubPayloadFactory(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/permissions/EmployeeManagement;)Lcom/squareup/print/payload/StubPayload$Factory;
    .locals 0

    .line 49
    invoke-static {p0, p1, p2}, Lcom/squareup/print/RegisterPrintModule;->provideStubPayloadFactory(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/permissions/EmployeeManagement;)Lcom/squareup/print/payload/StubPayload$Factory;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/print/payload/StubPayload$Factory;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/print/payload/StubPayload$Factory;
    .locals 3

    .line 37
    iget-object v0, p0, Lcom/squareup/print/RegisterPrintModule_ProvideStubPayloadFactoryFactory;->formatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/print/ReceiptFormatter;

    iget-object v1, p0, Lcom/squareup/print/RegisterPrintModule_ProvideStubPayloadFactoryFactory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v2, p0, Lcom/squareup/print/RegisterPrintModule_ProvideStubPayloadFactoryFactory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/permissions/EmployeeManagement;

    invoke-static {v0, v1, v2}, Lcom/squareup/print/RegisterPrintModule_ProvideStubPayloadFactoryFactory;->provideStubPayloadFactory(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/permissions/EmployeeManagement;)Lcom/squareup/print/payload/StubPayload$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/print/RegisterPrintModule_ProvideStubPayloadFactoryFactory;->get()Lcom/squareup/print/payload/StubPayload$Factory;

    move-result-object v0

    return-object v0
.end method
