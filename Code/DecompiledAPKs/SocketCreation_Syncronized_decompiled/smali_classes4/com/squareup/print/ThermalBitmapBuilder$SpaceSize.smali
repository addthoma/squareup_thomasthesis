.class public final enum Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;
.super Ljava/lang/Enum;
.source "ThermalBitmapBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/ThermalBitmapBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SpaceSize"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

.field public static final enum EXTRA_LARGE:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

.field public static final enum LARGE:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

.field public static final enum MEDIUM:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

.field public static final enum SMALL:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

.field public static final enum TINY:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;


# instance fields
.field public final sizePx:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 97
    new-instance v0, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    const/4 v1, 0x0

    const-string v2, "EXTRA_LARGE"

    const/16 v3, 0x37

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->EXTRA_LARGE:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    .line 98
    new-instance v0, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    const/4 v2, 0x1

    const-string v3, "LARGE"

    const/16 v4, 0x2d

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->LARGE:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    .line 99
    new-instance v0, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    const/4 v3, 0x2

    const-string v4, "MEDIUM"

    const/16 v5, 0x23

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->MEDIUM:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    .line 100
    new-instance v0, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    const/4 v4, 0x3

    const-string v5, "SMALL"

    const/16 v6, 0x19

    invoke-direct {v0, v5, v4, v6}, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->SMALL:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    .line 101
    new-instance v0, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    const/4 v5, 0x4

    const-string v6, "TINY"

    const/16 v7, 0xa

    invoke-direct {v0, v6, v5, v7}, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->TINY:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    .line 96
    sget-object v6, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->EXTRA_LARGE:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->LARGE:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->MEDIUM:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->SMALL:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->TINY:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->$VALUES:[Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 105
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 106
    iput p3, p0, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->sizePx:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;
    .locals 1

    .line 96
    const-class v0, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    return-object p0
.end method

.method public static values()[Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;
    .locals 1

    .line 96
    sget-object v0, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->$VALUES:[Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    invoke-virtual {v0}, [Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    return-object v0
.end method
