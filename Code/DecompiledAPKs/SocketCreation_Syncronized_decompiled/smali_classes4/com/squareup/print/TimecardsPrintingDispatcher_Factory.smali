.class public final Lcom/squareup/print/TimecardsPrintingDispatcher_Factory;
.super Ljava/lang/Object;
.source "TimecardsPrintingDispatcher_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/print/TimecardsPrintingDispatcher;",
        ">;"
    }
.end annotation


# instance fields
.field private final printSpoolerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSpooler;",
            ">;"
        }
    .end annotation
.end field

.field private final printerStationsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final timecardsSummaryPayloadFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/payload/TimecardsSummaryPayloadFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSpooler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/payload/TimecardsSummaryPayloadFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/print/TimecardsPrintingDispatcher_Factory;->printSpoolerProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/print/TimecardsPrintingDispatcher_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/print/TimecardsPrintingDispatcher_Factory;->timecardsSummaryPayloadFactoryProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/squareup/print/TimecardsPrintingDispatcher_Factory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/print/TimecardsPrintingDispatcher_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSpooler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/payload/TimecardsSummaryPayloadFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/print/TimecardsPrintingDispatcher_Factory;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/print/TimecardsPrintingDispatcher_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/print/TimecardsPrintingDispatcher_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/PrinterStations;Lcom/squareup/print/payload/TimecardsSummaryPayloadFactory;Lcom/squareup/util/Res;)Lcom/squareup/print/TimecardsPrintingDispatcher;
    .locals 1

    .line 52
    new-instance v0, Lcom/squareup/print/TimecardsPrintingDispatcher;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/print/TimecardsPrintingDispatcher;-><init>(Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/PrinterStations;Lcom/squareup/print/payload/TimecardsSummaryPayloadFactory;Lcom/squareup/util/Res;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/print/TimecardsPrintingDispatcher;
    .locals 4

    .line 38
    iget-object v0, p0, Lcom/squareup/print/TimecardsPrintingDispatcher_Factory;->printSpoolerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/print/PrintSpooler;

    iget-object v1, p0, Lcom/squareup/print/TimecardsPrintingDispatcher_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/PrinterStations;

    iget-object v2, p0, Lcom/squareup/print/TimecardsPrintingDispatcher_Factory;->timecardsSummaryPayloadFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/print/payload/TimecardsSummaryPayloadFactory;

    iget-object v3, p0, Lcom/squareup/print/TimecardsPrintingDispatcher_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/util/Res;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/print/TimecardsPrintingDispatcher_Factory;->newInstance(Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/PrinterStations;Lcom/squareup/print/payload/TimecardsSummaryPayloadFactory;Lcom/squareup/util/Res;)Lcom/squareup/print/TimecardsPrintingDispatcher;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/print/TimecardsPrintingDispatcher_Factory;->get()Lcom/squareup/print/TimecardsPrintingDispatcher;

    move-result-object v0

    return-object v0
.end method
