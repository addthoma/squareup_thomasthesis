.class public Lcom/squareup/print/RealTicketAutoIdentifiers;
.super Ljava/lang/Object;
.source "RealTicketAutoIdentifiers.java"

# interfaces
.implements Lcom/squareup/badbus/BadBusRegistrant;
.implements Lcom/squareup/print/TicketAutoIdentifiers;


# instance fields
.field private canceled:Z

.field private final identifierService:Lcom/squareup/server/TicketIdentifierService;

.field private inFlight:Z

.field private final printerStations:Lcom/squareup/print/PrinterStations;

.field private final ticketAutoNumberingEnabled:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method constructor <init>(Lcom/squareup/payment/Transaction;Lcom/squareup/server/TicketIdentifierService;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/print/PrinterStations;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/server/TicketIdentifierService;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/print/PrinterStations;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p2, p0, Lcom/squareup/print/RealTicketAutoIdentifiers;->identifierService:Lcom/squareup/server/TicketIdentifierService;

    .line 49
    iput-object p3, p0, Lcom/squareup/print/RealTicketAutoIdentifiers;->ticketAutoNumberingEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    .line 50
    iput-object p1, p0, Lcom/squareup/print/RealTicketAutoIdentifiers;->transaction:Lcom/squareup/payment/Transaction;

    .line 51
    iput-object p4, p0, Lcom/squareup/print/RealTicketAutoIdentifiers;->printerStations:Lcom/squareup/print/PrinterStations;

    return-void
.end method

.method static synthetic access$002(Lcom/squareup/print/RealTicketAutoIdentifiers;Z)Z
    .locals 0

    .line 35
    iput-boolean p1, p0, Lcom/squareup/print/RealTicketAutoIdentifiers;->inFlight:Z

    return p1
.end method

.method static synthetic access$100(Lcom/squareup/print/RealTicketAutoIdentifiers;)Z
    .locals 0

    .line 35
    iget-boolean p0, p0, Lcom/squareup/print/RealTicketAutoIdentifiers;->canceled:Z

    return p0
.end method

.method static synthetic access$200(Lcom/squareup/print/RealTicketAutoIdentifiers;I)V
    .locals 0

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/print/RealTicketAutoIdentifiers;->onResponseReceived(I)V

    return-void
.end method

.method private static cartHasOnlyExactlyOneInterestingItem(Lcom/squareup/payment/Transaction;)Z
    .locals 4

    .line 86
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object p0

    .line 88
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object p0

    .line 90
    invoke-static {p0}, Lcom/squareup/util/SquareCollections;->emptyIfNull(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eq v0, v1, :cond_2

    const/4 v3, 0x2

    if-eq v0, v3, :cond_0

    return v2

    .line 94
    :cond_0
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->isInteresting()Z

    move-result v0

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/checkout/CartItem;

    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->isInteresting()Z

    move-result p0

    if-eq v0, p0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 92
    :cond_2
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/checkout/CartItem;

    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->isInteresting()Z

    move-result p0

    return p0
.end method

.method static isNewTransaction(Lcom/squareup/payment/Transaction;Lcom/squareup/payment/OrderEntryEvents$CartChanged;)Z
    .locals 1

    .line 80
    iget-object v0, p1, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->status:Lcom/squareup/payment/OrderEntryEvents$ItemStatus;

    if-eqz v0, :cond_0

    iget-object p1, p1, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->status:Lcom/squareup/payment/OrderEntryEvents$ItemStatus;

    iget-object p1, p1, Lcom/squareup/payment/OrderEntryEvents$ItemStatus;->changeType:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    sget-object v0, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;->ADD:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    if-ne p1, v0, :cond_0

    .line 82
    invoke-static {p0}, Lcom/squareup/print/RealTicketAutoIdentifiers;->cartHasOnlyExactlyOneInterestingItem(Lcom/squareup/payment/Transaction;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static synthetic lambda$9HFbpOSLRz-AXfEg_WlsmZe6-xk(Lcom/squareup/print/RealTicketAutoIdentifiers;Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/print/RealTicketAutoIdentifiers;->onCartChanged(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V

    return-void
.end method

.method private needsNewIdentifier()Z
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/squareup/print/RealTicketAutoIdentifiers;->ticketAutoNumberingEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/RealTicketAutoIdentifiers;->transaction:Lcom/squareup/payment/Transaction;

    .line 102
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/RealTicketAutoIdentifiers;->transaction:Lcom/squareup/payment/Transaction;

    .line 103
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getOrderTicketName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/RealTicketAutoIdentifiers;->printerStations:Lcom/squareup/print/PrinterStations;

    .line 104
    invoke-interface {v0}, Lcom/squareup/print/PrinterStations;->hasEnabledKitchenPrintingStations()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private onCartChanged(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/print/RealTicketAutoIdentifiers;->transaction:Lcom/squareup/payment/Transaction;

    invoke-static {v0, p1}, Lcom/squareup/print/RealTicketAutoIdentifiers;->isNewTransaction(Lcom/squareup/payment/Transaction;Lcom/squareup/payment/OrderEntryEvents$CartChanged;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 60
    invoke-virtual {p0}, Lcom/squareup/print/RealTicketAutoIdentifiers;->requestTicketIdentifierIfNecessary()V

    :cond_0
    return-void
.end method

.method private onResponseReceived(I)V
    .locals 1

    .line 131
    invoke-direct {p0}, Lcom/squareup/print/RealTicketAutoIdentifiers;->needsNewIdentifier()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 133
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    .line 134
    iget-object v0, p0, Lcom/squareup/print/RealTicketAutoIdentifiers;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Transaction;->setOrderTicketName(Ljava/lang/String;)V

    return-void
.end method

.method private requestNextTicketIdentifier()V
    .locals 3

    .line 108
    iget-boolean v0, p0, Lcom/squareup/print/RealTicketAutoIdentifiers;->inFlight:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    new-array v0, v1, [Ljava/lang/Object;

    const-string v1, "[RealTicketAutoIdentifiers] Request already in flight."

    .line 109
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_0
    new-array v0, v1, [Ljava/lang/Object;

    const-string v2, "[RealTicketAutoIdentifiers] Requesting next ticket identifier from server\u2026"

    .line 113
    invoke-static {v2, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 115
    iput-boolean v1, p0, Lcom/squareup/print/RealTicketAutoIdentifiers;->canceled:Z

    const/4 v0, 0x1

    .line 116
    iput-boolean v0, p0, Lcom/squareup/print/RealTicketAutoIdentifiers;->inFlight:Z

    .line 118
    iget-object v0, p0, Lcom/squareup/print/RealTicketAutoIdentifiers;->identifierService:Lcom/squareup/server/TicketIdentifierService;

    new-instance v1, Lcom/squareup/print/RealTicketAutoIdentifiers$1;

    const-string v2, "RealTicketAutoIdentifiers"

    invoke-direct {v1, p0, v2}, Lcom/squareup/print/RealTicketAutoIdentifiers$1;-><init>(Lcom/squareup/print/RealTicketAutoIdentifiers;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/server/TicketIdentifierService;->getTicketIdentifier(Lcom/squareup/server/SquareCallback;)V

    return-void
.end method


# virtual methods
.method public cancelTicketIdentifierRequest()V
    .locals 2

    .line 65
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "[RealTicketAutoIdentifiers] Canceling request."

    .line 67
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x1

    .line 68
    iput-boolean v0, p0, Lcom/squareup/print/RealTicketAutoIdentifiers;->canceled:Z

    return-void
.end method

.method public registerOnBus(Lcom/squareup/badbus/BadBus;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 55
    const-class v0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    invoke-virtual {p1, v0}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/print/-$$Lambda$RealTicketAutoIdentifiers$9HFbpOSLRz-AXfEg_WlsmZe6-xk;

    invoke-direct {v0, p0}, Lcom/squareup/print/-$$Lambda$RealTicketAutoIdentifiers$9HFbpOSLRz-AXfEg_WlsmZe6-xk;-><init>(Lcom/squareup/print/RealTicketAutoIdentifiers;)V

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public requestTicketIdentifierIfNecessary()V
    .locals 1

    .line 72
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 74
    invoke-direct {p0}, Lcom/squareup/print/RealTicketAutoIdentifiers;->needsNewIdentifier()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    invoke-direct {p0}, Lcom/squareup/print/RealTicketAutoIdentifiers;->requestNextTicketIdentifier()V

    :cond_0
    return-void
.end method
