.class public final Lcom/squareup/print/RealPrintSettings;
.super Ljava/lang/Object;
.source "PrintSettings.kt"

# interfaces
.implements Lcom/squareup/print/PrintSettings;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPrintSettings.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PrintSettings.kt\ncom/squareup/print/RealPrintSettings\n*L\n1#1,270:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u001e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010 \n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u001e\u0010\u0013\u001a\u00020\u00142\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u0012\u0010\u0019\u001a\u00020\u00142\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u000bH\u0016J\u0010\u0010\u001b\u001a\u00020\u00142\u0006\u0010\u001a\u001a\u00020\u000bH\u0016J\u0010\u0010\u001c\u001a\u00020\u00142\u0006\u0010\u001d\u001a\u00020\u001eH\u0016J\u0012\u0010\u001f\u001a\u00020\u00142\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u000bH\u0016J\u0010\u0010 \u001a\u00020\u00142\u0006\u0010\u001d\u001a\u00020\u001eH\u0016J\u0010\u0010!\u001a\u00020\u00142\u0006\u0010\u001d\u001a\u00020\u001eH\u0016J\u0010\u0010\"\u001a\u00020\u00142\u0006\u0010\u001a\u001a\u00020\u000bH\u0016J&\u0010#\u001a\u00020\u00142\u000c\u0010$\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010%\u001a\u00020\u0014H\u0016J.\u0010&\u001a\u00020\u00142\u000c\u0010$\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010%\u001a\u00020\u00142\u0006\u0010\'\u001a\u00020(H\u0016J\u001e\u0010)\u001a\u00020\u00142\u000c\u0010$\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J&\u0010*\u001a\u00020\u00142\u000c\u0010$\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\'\u001a\u00020(H\u0016J$\u0010+\u001a\u00020\u00142\u000c\u0010$\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u00162\u000c\u0010,\u001a\u0008\u0012\u0004\u0012\u00020\u000c0-H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R0\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000c0\n8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0014\n\u0000\u0012\u0004\u0008\r\u0010\u000e\u001a\u0004\u0008\u000f\u0010\u0010\"\u0004\u0008\u0011\u0010\u0012R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006."
    }
    d2 = {
        "Lcom/squareup/print/RealPrintSettings;",
        "Lcom/squareup/print/PrintSettings;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "paperSignatureSettings",
        "Lcom/squareup/papersignature/PaperSignatureSettings;",
        "voidCompSettings",
        "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
        "(Lcom/squareup/settings/server/Features;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/tickets/voidcomp/VoidCompSettings;)V",
        "ticketItemClassifier",
        "Lcom/squareup/util/SquareCollections$Classifier;",
        "Lcom/squareup/print/PrinterStation;",
        "Lcom/squareup/checkout/CartItem;",
        "ticketItemClassifier$annotations",
        "()V",
        "getTicketItemClassifier",
        "()Lcom/squareup/util/SquareCollections$Classifier;",
        "setTicketItemClassifier",
        "(Lcom/squareup/util/SquareCollections$Classifier;)V",
        "canDispatchStubForOrder",
        "",
        "targetStations",
        "",
        "order",
        "Lcom/squareup/payment/Order;",
        "canPrintATicketForEachItem",
        "printerStation",
        "canPrintCompactTickets",
        "isFormalReceiptPrintingAvailable",
        "receipt",
        "Lcom/squareup/payment/PaymentReceipt;",
        "shouldPrintATicketForEachItem",
        "shouldPrintAuthSlip",
        "shouldPrintAuthSlipCopy",
        "shouldPrintCompactTickets",
        "shouldPrintTicketForOrder",
        "targetStation",
        "isPrintingSavedTicket",
        "shouldPrintTicketForPayment",
        "payment",
        "Lcom/squareup/payment/Payment;",
        "shouldPrintTicketStubForOrder",
        "shouldPrintTicketStubForPayment",
        "shouldPrintVoidTicketForTransaction",
        "voidedItems",
        "",
        "print_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

.field private ticketItemClassifier:Lcom/squareup/util/SquareCollections$Classifier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/util/SquareCollections$Classifier<",
            "Lcom/squareup/print/PrinterStation;",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation
.end field

.field private final voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/tickets/voidcomp/VoidCompSettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paperSignatureSettings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "voidCompSettings"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/print/RealPrintSettings;->features:Lcom/squareup/settings/server/Features;

    iput-object p2, p0, Lcom/squareup/print/RealPrintSettings;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    iput-object p3, p0, Lcom/squareup/print/RealPrintSettings;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    .line 151
    sget-object p1, Lcom/squareup/print/RealPrintSettings$ticketItemClassifier$1;->INSTANCE:Lcom/squareup/print/RealPrintSettings$ticketItemClassifier$1;

    check-cast p1, Lcom/squareup/util/SquareCollections$Classifier;

    iput-object p1, p0, Lcom/squareup/print/RealPrintSettings;->ticketItemClassifier:Lcom/squareup/util/SquareCollections$Classifier;

    return-void
.end method

.method private final canDispatchStubForOrder(Ljava/util/Collection;Lcom/squareup/payment/Order;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/print/PrinterStation;",
            ">;",
            "Lcom/squareup/payment/Order;",
            ")Z"
        }
    .end annotation

    .line 260
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/print/PrinterStation;

    .line 261
    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getNotVoidedUnlockedItems()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 262
    iget-object v3, p0, Lcom/squareup/print/RealPrintSettings;->ticketItemClassifier:Lcom/squareup/util/SquareCollections$Classifier;

    invoke-interface {v3, v0, v2}, Lcom/squareup/util/SquareCollections$Classifier;->classContains(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_2
    const/4 p1, 0x0

    return p1
.end method

.method public static synthetic ticketItemClassifier$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public canPrintATicketForEachItem(Lcom/squareup/print/PrinterStation;)Z
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_1

    .line 234
    invoke-virtual {p1}, Lcom/squareup/print/PrinterStation;->hasAutomaticPaperCutter()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/print/RealPrintSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->CAN_PRINT_SINGLE_TICKET_PER_ITEM:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-ne p1, v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public canPrintCompactTickets(Lcom/squareup/print/PrinterStation;)Z
    .locals 1

    const-string v0, "printerStation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 244
    iget-object p1, p0, Lcom/squareup/print/RealPrintSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_PRINT_COMPACT_TICKETS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    return p1
.end method

.method public final getTicketItemClassifier()Lcom/squareup/util/SquareCollections$Classifier;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/util/SquareCollections$Classifier<",
            "Lcom/squareup/print/PrinterStation;",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    .line 150
    iget-object v0, p0, Lcom/squareup/print/RealPrintSettings;->ticketItemClassifier:Lcom/squareup/util/SquareCollections$Classifier;

    return-object v0
.end method

.method public isFormalReceiptPrintingAvailable(Lcom/squareup/payment/PaymentReceipt;)Z
    .locals 5

    const-string v0, "receipt"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 225
    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object p1

    .line 226
    instance-of v0, p1, Lcom/squareup/payment/InvoicePayment;

    .line 227
    instance-of v1, p1, Lcom/squareup/payment/BillPayment;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    check-cast p1, Lcom/squareup/payment/BillPayment;

    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->isSingleTender()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 228
    :goto_0
    iget-object v1, p0, Lcom/squareup/print/RealPrintSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v4, Lcom/squareup/settings/server/Features$Feature;->RECEIPTS_JP_FORMAL_PRINTED_RECEIPTS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v4}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-nez v0, :cond_2

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :cond_2
    :goto_1
    return v2
.end method

.method public final setTicketItemClassifier(Lcom/squareup/util/SquareCollections$Classifier;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/SquareCollections$Classifier<",
            "Lcom/squareup/print/PrinterStation;",
            "Lcom/squareup/checkout/CartItem;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    iput-object p1, p0, Lcom/squareup/print/RealPrintSettings;->ticketItemClassifier:Lcom/squareup/util/SquareCollections$Classifier;

    return-void
.end method

.method public shouldPrintATicketForEachItem(Lcom/squareup/print/PrinterStation;)Z
    .locals 2

    .line 239
    invoke-virtual {p0, p1}, Lcom/squareup/print/RealPrintSettings;->canPrintATicketForEachItem(Lcom/squareup/print/PrinterStation;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/print/PrinterStation;->isPrintATicketForEachItemEnabled()Z

    move-result p1

    if-ne p1, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public shouldPrintAuthSlip(Lcom/squareup/payment/PaymentReceipt;)Z
    .locals 2

    const-string v0, "receipt"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 215
    iget-object v0, p0, Lcom/squareup/print/RealPrintSettings;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    invoke-virtual {v0}, Lcom/squareup/papersignature/PaperSignatureSettings;->shouldPrintReceiptToSign()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/receipt/PrintedReceiptSettings;->INSTANCE:Lcom/squareup/receipt/PrintedReceiptSettings;

    .line 216
    invoke-static {p1}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->getTenderForPrinting(Lcom/squareup/payment/PaymentReceipt;)Lcom/squareup/payment/tender/BaseTender;

    move-result-object p1

    const-string v1, "getTenderForPrinting(receipt)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/receipt/PrintedReceiptSettings;->tenderRequiresPrintedReceipt(Lcom/squareup/payment/tender/BaseTender;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public shouldPrintAuthSlipCopy(Lcom/squareup/payment/PaymentReceipt;)Z
    .locals 1

    const-string v0, "receipt"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 220
    invoke-virtual {p0, p1}, Lcom/squareup/print/RealPrintSettings;->shouldPrintAuthSlip(Lcom/squareup/payment/PaymentReceipt;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/print/RealPrintSettings;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    invoke-virtual {p1}, Lcom/squareup/papersignature/PaperSignatureSettings;->isPrintAdditionalAuthSlipEnabled()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public shouldPrintCompactTickets(Lcom/squareup/print/PrinterStation;)Z
    .locals 1

    const-string v0, "printerStation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 248
    invoke-virtual {p0, p1}, Lcom/squareup/print/RealPrintSettings;->canPrintCompactTickets(Lcom/squareup/print/PrinterStation;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/print/PrinterStation;->isPrintCompactTicketsEnabled()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public shouldPrintTicketForOrder(Ljava/util/Collection;Lcom/squareup/payment/Order;Z)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/print/PrinterStation;",
            ">;",
            "Lcom/squareup/payment/Order;",
            "Z)Z"
        }
    .end annotation

    const-string p3, "targetStation"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "order"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 184
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p3

    if-eqz p3, :cond_0

    const/4 p1, 0x0

    return p1

    .line 187
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getNotVoidedUnlockedItems()Ljava/util/List;

    move-result-object p2

    .line 188
    check-cast p2, Ljava/util/Collection;

    iget-object p3, p0, Lcom/squareup/print/RealPrintSettings;->ticketItemClassifier:Lcom/squareup/util/SquareCollections$Classifier;

    invoke-static {p1, p2, p3}, Lcom/squareup/util/SquareCollections;->classify(Ljava/util/Collection;Ljava/util/Collection;Lcom/squareup/util/SquareCollections$Classifier;)Ljava/util/Map;

    move-result-object p1

    const-string p2, "classify(targetStation, \u2026nt, ticketItemClassifier)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method public shouldPrintTicketForPayment(Ljava/util/Collection;Lcom/squareup/payment/Order;ZLcom/squareup/payment/Payment;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/print/PrinterStation;",
            ">;",
            "Lcom/squareup/payment/Order;",
            "Z",
            "Lcom/squareup/payment/Payment;",
            ")Z"
        }
    .end annotation

    const-string p3, "targetStation"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "order"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "payment"

    invoke-static {p4, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 197
    invoke-virtual {p4}, Lcom/squareup/payment/Payment;->isComplete()Z

    move-result p3

    if-eqz p3, :cond_1

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p3

    if-eqz p3, :cond_0

    goto :goto_0

    .line 200
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getNotVoidedUnlockedItems()Ljava/util/List;

    move-result-object p2

    .line 201
    check-cast p2, Ljava/util/Collection;

    iget-object p3, p0, Lcom/squareup/print/RealPrintSettings;->ticketItemClassifier:Lcom/squareup/util/SquareCollections$Classifier;

    invoke-static {p1, p2, p3}, Lcom/squareup/util/SquareCollections;->classify(Ljava/util/Collection;Ljava/util/Collection;Lcom/squareup/util/SquareCollections$Classifier;)Ljava/util/Map;

    move-result-object p1

    const-string p2, "classify(targetStation, \u2026nt, ticketItemClassifier)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public shouldPrintTicketStubForOrder(Ljava/util/Collection;Lcom/squareup/payment/Order;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/print/PrinterStation;",
            ">;",
            "Lcom/squareup/payment/Order;",
            ")Z"
        }
    .end annotation

    const-string v0, "targetStation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "order"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 163
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 164
    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getNotVoidedUnlockedItems()Ljava/util/List;

    move-result-object v0

    const-string v2, "order.notVoidedUnlockedItems"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 165
    invoke-direct {p0, p1, p2}, Lcom/squareup/print/RealPrintSettings;->canDispatchStubForOrder(Ljava/util/Collection;Lcom/squareup/payment/Order;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public shouldPrintTicketStubForPayment(Ljava/util/Collection;Lcom/squareup/payment/Order;Lcom/squareup/payment/Payment;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/print/PrinterStation;",
            ">;",
            "Lcom/squareup/payment/Order;",
            "Lcom/squareup/payment/Payment;",
            ")Z"
        }
    .end annotation

    const-string v0, "targetStation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "order"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "payment"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 174
    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getNotVoidedUnlockedItems()Ljava/util/List;

    move-result-object v0

    const-string v2, "order.notVoidedUnlockedItems"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    invoke-virtual {p3}, Lcom/squareup/payment/Payment;->isComplete()Z

    move-result p3

    if-eqz p3, :cond_0

    .line 176
    invoke-direct {p0, p1, p2}, Lcom/squareup/print/RealPrintSettings;->canDispatchStubForOrder(Ljava/util/Collection;Lcom/squareup/payment/Order;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public shouldPrintVoidTicketForTransaction(Ljava/util/Collection;Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/print/PrinterStation;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/CartItem;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "targetStation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "voidedItems"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 208
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/print/RealPrintSettings;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isVoidEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 211
    :cond_0
    check-cast p2, Ljava/util/Collection;

    iget-object v0, p0, Lcom/squareup/print/RealPrintSettings;->ticketItemClassifier:Lcom/squareup/util/SquareCollections$Classifier;

    invoke-static {p1, p2, v0}, Lcom/squareup/util/SquareCollections;->classify(Ljava/util/Collection;Ljava/util/Collection;Lcom/squareup/util/SquareCollections$Classifier;)Ljava/util/Map;

    move-result-object p1

    const-string p2, "classify(targetStation, \u2026ms, ticketItemClassifier)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return p1
.end method
