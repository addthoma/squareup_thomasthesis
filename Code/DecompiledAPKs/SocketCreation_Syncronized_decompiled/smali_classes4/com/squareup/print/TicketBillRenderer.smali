.class public Lcom/squareup/print/TicketBillRenderer;
.super Ljava/lang/Object;
.source "TicketBillRenderer.java"


# instance fields
.field private final builder:Lcom/squareup/print/ThermalBitmapBuilder;


# direct methods
.method public constructor <init>(Lcom/squareup/print/ThermalBitmapBuilder;)V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/squareup/print/TicketBillRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    return-void
.end method


# virtual methods
.method public final renderBitmap(Lcom/squareup/print/payload/TicketBillPayload;)Landroid/graphics/Bitmap;
    .locals 2

    .line 15
    iget-object v0, p1, Lcom/squareup/print/payload/TicketBillPayload;->header:Lcom/squareup/print/sections/HeaderSection;

    iget-object v1, p0, Lcom/squareup/print/TicketBillRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {v0, v1}, Lcom/squareup/print/sections/HeaderSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 16
    iget-object v0, p1, Lcom/squareup/print/payload/TicketBillPayload;->codes:Lcom/squareup/print/sections/CodesSection;

    iget-object v1, p0, Lcom/squareup/print/TicketBillRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {v0, v1}, Lcom/squareup/print/sections/CodesSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 17
    iget-object v0, p1, Lcom/squareup/print/payload/TicketBillPayload;->itemsAndDiscounts:Lcom/squareup/print/sections/ItemsAndDiscountsSection;

    iget-object v1, p0, Lcom/squareup/print/TicketBillRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {v0, v1}, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 18
    iget-object v0, p1, Lcom/squareup/print/payload/TicketBillPayload;->subtotalAndAdjustments:Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

    iget-object v1, p0, Lcom/squareup/print/TicketBillRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {v0, v1}, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 19
    iget-object v0, p1, Lcom/squareup/print/payload/TicketBillPayload;->total:Lcom/squareup/print/sections/TotalSection;

    iget-object v1, p0, Lcom/squareup/print/TicketBillRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {v0, v1}, Lcom/squareup/print/sections/TotalSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 20
    iget-object p1, p1, Lcom/squareup/print/payload/TicketBillPayload;->footer:Lcom/squareup/print/sections/FooterSection;

    iget-object v0, p0, Lcom/squareup/print/TicketBillRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {p1, v0}, Lcom/squareup/print/sections/FooterSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 22
    iget-object p1, p0, Lcom/squareup/print/TicketBillRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->render()Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method
