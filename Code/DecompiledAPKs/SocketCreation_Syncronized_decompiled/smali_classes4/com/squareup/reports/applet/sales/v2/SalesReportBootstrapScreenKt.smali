.class public final Lcom/squareup/reports/applet/sales/v2/SalesReportBootstrapScreenKt;
.super Ljava/lang/Object;
.source "SalesReportBootstrapScreen.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u000c\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "getSalesReportWorkflowRunner",
        "Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner;",
        "Lmortar/MortarScope;",
        "reports-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$getSalesReportWorkflowRunner(Lmortar/MortarScope;)Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/reports/applet/sales/v2/SalesReportBootstrapScreenKt;->getSalesReportWorkflowRunner(Lmortar/MortarScope;)Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner;

    move-result-object p0

    return-object p0
.end method

.method private static final getSalesReportWorkflowRunner(Lmortar/MortarScope;)Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner;
    .locals 1

    .line 30
    sget-object v0, Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner;->Companion:Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner$Companion;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner$Companion;->getNAME()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/container/PosWorkflowRunnerKt;->getWorkflowRunner(Lmortar/MortarScope;Ljava/lang/String;)Lcom/squareup/container/PosWorkflowRunner;

    move-result-object p0

    check-cast p0, Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner;

    return-object p0
.end method
