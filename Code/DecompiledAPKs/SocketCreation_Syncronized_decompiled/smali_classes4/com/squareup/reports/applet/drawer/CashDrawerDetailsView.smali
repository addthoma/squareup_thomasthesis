.class public Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;
.super Landroid/widget/LinearLayout;
.source "CashDrawerDetailsView.java"


# instance fields
.field private final actualInDrawerContainer:Landroid/view/ViewGroup;

.field private final actualInDrawerEditText:Lcom/squareup/widgets/SelectableEditText;

.field private final actualInDrawerHint:Landroid/view/View;

.field private final actualInDrawerRow:Lcom/squareup/ui/account/view/LineRow;

.field private final cashRefunds:Lcom/squareup/ui/account/view/LineRow;

.field private final cashSales:Lcom/squareup/ui/account/view/LineRow;

.field private final difference:Lcom/squareup/ui/account/view/LineRow;

.field private final drawerDescription:Lcom/squareup/ui/account/view/LineRow;

.field private final endOfDrawer:Lcom/squareup/ui/account/view/LineRow;

.field private final expectedInDrawer:Lcom/squareup/ui/account/view/LineRow;

.field private final paidInOut:Lcom/squareup/ui/account/view/LineRow;

.field private presenter:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;

.field private final saveButton:Lcom/squareup/ui/ConfirmButton;

.field private final startOfDrawer:Lcom/squareup/ui/account/view/LineRow;

.field private final startingCash:Lcom/squareup/ui/account/view/LineRow;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 41
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    sget p2, Lcom/squareup/reports/applet/R$layout;->cash_drawer_details:I

    invoke-static {p1, p2, p0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 45
    sget p1, Lcom/squareup/reports/applet/R$id;->drawer_description:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/account/view/LineRow;

    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->drawerDescription:Lcom/squareup/ui/account/view/LineRow;

    .line 46
    sget p1, Lcom/squareup/reports/applet/R$id;->start_of_drawer:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/account/view/LineRow;

    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->startOfDrawer:Lcom/squareup/ui/account/view/LineRow;

    .line 47
    sget p1, Lcom/squareup/reports/applet/R$id;->end_of_drawer:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/account/view/LineRow;

    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->endOfDrawer:Lcom/squareup/ui/account/view/LineRow;

    .line 48
    sget p1, Lcom/squareup/reports/applet/R$id;->starting_cash:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/account/view/LineRow;

    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->startingCash:Lcom/squareup/ui/account/view/LineRow;

    .line 49
    sget p1, Lcom/squareup/reports/applet/R$id;->cash_sales:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/account/view/LineRow;

    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->cashSales:Lcom/squareup/ui/account/view/LineRow;

    .line 50
    sget p1, Lcom/squareup/reports/applet/R$id;->cash_refunds:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/account/view/LineRow;

    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->cashRefunds:Lcom/squareup/ui/account/view/LineRow;

    .line 51
    sget p1, Lcom/squareup/reports/applet/R$id;->paid_in_out:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/account/view/LineRow;

    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->paidInOut:Lcom/squareup/ui/account/view/LineRow;

    .line 52
    sget p1, Lcom/squareup/reports/applet/R$id;->expected_in_drawer:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/account/view/LineRow;

    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->expectedInDrawer:Lcom/squareup/ui/account/view/LineRow;

    .line 53
    sget p1, Lcom/squareup/reports/applet/R$id;->actual_in_drawer:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/account/view/LineRow;

    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->actualInDrawerRow:Lcom/squareup/ui/account/view/LineRow;

    .line 54
    sget p1, Lcom/squareup/reports/applet/R$id;->actual_in_drawer_container:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->actualInDrawerContainer:Landroid/view/ViewGroup;

    .line 55
    sget p1, Lcom/squareup/reports/applet/R$id;->actual_in_drawer_edit_text:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/SelectableEditText;

    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->actualInDrawerEditText:Lcom/squareup/widgets/SelectableEditText;

    .line 56
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->actualInDrawerEditText:Lcom/squareup/widgets/SelectableEditText;

    new-instance p2, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView$1;

    invoke-direct {p2, p0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView$1;-><init>(Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;)V

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 63
    sget p1, Lcom/squareup/reports/applet/R$id;->actual_in_drawer_hint:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->actualInDrawerHint:Landroid/view/View;

    .line 64
    sget p1, Lcom/squareup/reports/applet/R$id;->difference:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/account/view/LineRow;

    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->difference:Lcom/squareup/ui/account/view/LineRow;

    .line 65
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->difference:Lcom/squareup/ui/account/view/LineRow;

    sget-object p2, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/LineRow;->setValueWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 66
    sget p1, Lcom/squareup/reports/applet/R$id;->save_button:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ConfirmButton;

    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->saveButton:Lcom/squareup/ui/ConfirmButton;

    .line 67
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->saveButton:Lcom/squareup/ui/ConfirmButton;

    sget-object p2, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/ConfirmButton;->setButtonWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 68
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->saveButton:Lcom/squareup/ui/ConfirmButton;

    new-instance p2, Lcom/squareup/reports/applet/drawer/-$$Lambda$CashDrawerDetailsView$RNmqdgRODFdJFCNBGm_pYB9IAGE;

    invoke-direct {p2, p0}, Lcom/squareup/reports/applet/drawer/-$$Lambda$CashDrawerDetailsView$RNmqdgRODFdJFCNBGm_pYB9IAGE;-><init>(Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;)V

    invoke-virtual {p1, p2}, Lcom/squareup/ui/ConfirmButton;->setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;)Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->presenter:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;

    return-object p0
.end method


# virtual methods
.method public dropPresenter()V
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->presenter:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->dropView(Ljava/lang/Object;)V

    const/4 v0, 0x0

    .line 86
    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->presenter:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;

    return-void
.end method

.method public getActualInDrawerEditTextString()Ljava/lang/String;
    .locals 1

    .line 146
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->actualInDrawerEditText:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hideCashReports()V
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->cashSales:Lcom/squareup/ui/account/view/LineRow;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setVisibility(I)V

    .line 131
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->cashRefunds:Lcom/squareup/ui/account/view/LineRow;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setVisibility(I)V

    .line 132
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->paidInOut:Lcom/squareup/ui/account/view/LineRow;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setVisibility(I)V

    .line 133
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->expectedInDrawer:Lcom/squareup/ui/account/view/LineRow;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setVisibility(I)V

    return-void
.end method

.method public hideDifferences()V
    .locals 2

    .line 167
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->difference:Lcom/squareup/ui/account/view/LineRow;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setVisibility(I)V

    return-void
.end method

.method public synthetic lambda$new$0$CashDrawerDetailsView()V
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->presenter:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->closeEndedDrawer()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->presenter:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;

    if-eqz v0, :cond_0

    .line 73
    invoke-virtual {v0, p0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->dropView(Ljava/lang/Object;)V

    .line 75
    :cond_0
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public setActualInDrawerEditText(Ljava/lang/String;)V
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->actualInDrawerEditText:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setAndConfigurePresenter(Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;)V
    .locals 1

    .line 79
    invoke-virtual {p1, p0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->takeView(Ljava/lang/Object;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->actualInDrawerEditText:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p1, v0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->configurePriceLocalHelper(Lcom/squareup/widgets/SelectableEditText;)V

    .line 81
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->presenter:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;

    return-void
.end method

.method public setAndShowActualInDrawer(Ljava/lang/String;)V
    .locals 1

    .line 137
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->actualInDrawerRow:Lcom/squareup/ui/account/view/LineRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    .line 138
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->actualInDrawerRow:Lcom/squareup/ui/account/view/LineRow;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/LineRow;->setVisibility(I)V

    return-void
.end method

.method public setAndShowCashRefunds(Ljava/lang/String;)V
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->cashRefunds:Lcom/squareup/ui/account/view/LineRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    .line 116
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->cashRefunds:Lcom/squareup/ui/account/view/LineRow;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/LineRow;->setVisibility(I)V

    return-void
.end method

.method public setAndShowCashSales(Ljava/lang/String;)V
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->cashSales:Lcom/squareup/ui/account/view/LineRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    .line 111
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->cashSales:Lcom/squareup/ui/account/view/LineRow;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/LineRow;->setVisibility(I)V

    return-void
.end method

.method public setAndShowDescription(Ljava/lang/String;)V
    .locals 1

    .line 90
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->drawerDescription:Lcom/squareup/ui/account/view/LineRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    .line 91
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->drawerDescription:Lcom/squareup/ui/account/view/LineRow;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setAndShowDifference(Ljava/lang/String;)V
    .locals 1

    .line 162
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->difference:Lcom/squareup/ui/account/view/LineRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    .line 163
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->difference:Lcom/squareup/ui/account/view/LineRow;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/LineRow;->setVisibility(I)V

    return-void
.end method

.method public setAndShowEndOfDrawer(Ljava/lang/CharSequence;)V
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->endOfDrawer:Lcom/squareup/ui/account/view/LineRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    .line 101
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->endOfDrawer:Lcom/squareup/ui/account/view/LineRow;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/LineRow;->setVisibility(I)V

    return-void
.end method

.method public setAndShowExpectedInDrawer(Ljava/lang/String;)V
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->expectedInDrawer:Lcom/squareup/ui/account/view/LineRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    .line 126
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->expectedInDrawer:Lcom/squareup/ui/account/view/LineRow;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/LineRow;->setVisibility(I)V

    return-void
.end method

.method public setAndShowPaidInOut(Ljava/lang/String;)V
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->paidInOut:Lcom/squareup/ui/account/view/LineRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    .line 121
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->paidInOut:Lcom/squareup/ui/account/view/LineRow;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/LineRow;->setVisibility(I)V

    return-void
.end method

.method public setAndShowStartOfDrawer(Ljava/lang/CharSequence;)V
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->startOfDrawer:Lcom/squareup/ui/account/view/LineRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    .line 96
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->startOfDrawer:Lcom/squareup/ui/account/view/LineRow;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/LineRow;->setVisibility(I)V

    return-void
.end method

.method public setAndShowStartingCash(Ljava/lang/String;)V
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->startingCash:Lcom/squareup/ui/account/view/LineRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    .line 106
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->startingCash:Lcom/squareup/ui/account/view/LineRow;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/LineRow;->setVisibility(I)V

    return-void
.end method

.method public showActualInDrawerContainer(Z)V
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->actualInDrawerContainer:Landroid/view/ViewGroup;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public showActualInDrawerHint(Z)V
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->actualInDrawerHint:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public showDifference(Z)V
    .locals 1

    .line 158
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->difference:Lcom/squareup/ui/account/view/LineRow;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public showSaveButton(Z)V
    .locals 1

    .line 171
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->saveButton:Lcom/squareup/ui/ConfirmButton;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method
