.class public final Lcom/squareup/reports/applet/disputes/DisputesBootstrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "DisputesBootstrapScreen.kt"

# interfaces
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDisputesBootstrapScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DisputesBootstrapScreen.kt\ncom/squareup/reports/applet/disputes/DisputesBootstrapScreen\n+ 2 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,28:1\n24#2,4:29\n*E\n*S KotlinDebug\n*F\n+ 1 DisputesBootstrapScreen.kt\ncom/squareup/reports/applet/disputes/DisputesBootstrapScreen\n*L\n26#1,4:29\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u00012\u00020\u0002B\u0011\u0012\n\u0008\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0005J\u000b\u0010\u000e\u001a\u0004\u0018\u00010\u0004H\u00c6\u0003J\u0015\u0010\u000f\u001a\u00020\u00002\n\u0008\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0004H\u00c6\u0001J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\u0008\u0010\u0014\u001a\u00020\u0015H\u0016J\t\u0010\u0016\u001a\u00020\u0004H\u00d6\u0001R\u0016\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00000\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0013\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0018\u0010\n\u001a\u0006\u0012\u0002\u0008\u00030\u000b8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\r\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/reports/applet/disputes/DisputesBootstrapScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "Lcom/squareup/container/layer/InSection;",
        "paymentToken",
        "",
        "(Ljava/lang/String;)V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "getPaymentToken",
        "()Ljava/lang/String;",
        "section",
        "Ljava/lang/Class;",
        "getSection",
        "()Ljava/lang/Class;",
        "component1",
        "copy",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "Lcom/squareup/reports/applet/disputes/DisputesScope;",
        "toString",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/reports/applet/disputes/DisputesBootstrapScreen;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentToken:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, v0}, Lcom/squareup/reports/applet/disputes/DisputesBootstrapScreen;-><init>(Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/reports/applet/disputes/DisputesBootstrapScreen;->paymentToken:Ljava/lang/String;

    .line 29
    new-instance p1, Lcom/squareup/reports/applet/disputes/DisputesBootstrapScreen$$special$$inlined$pathCreator$1;

    invoke-direct {p1}, Lcom/squareup/reports/applet/disputes/DisputesBootstrapScreen$$special$$inlined$pathCreator$1;-><init>()V

    check-cast p1, Landroid/os/Parcelable$Creator;

    .line 32
    iput-object p1, p0, Lcom/squareup/reports/applet/disputes/DisputesBootstrapScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 15
    check-cast p1, Ljava/lang/String;

    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/reports/applet/disputes/DisputesBootstrapScreen;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/reports/applet/disputes/DisputesBootstrapScreen;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/reports/applet/disputes/DisputesBootstrapScreen;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/reports/applet/disputes/DisputesBootstrapScreen;->paymentToken:Ljava/lang/String;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/reports/applet/disputes/DisputesBootstrapScreen;->copy(Ljava/lang/String;)Lcom/squareup/reports/applet/disputes/DisputesBootstrapScreen;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/reports/applet/disputes/DisputesBootstrapScreen;->paymentToken:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;)Lcom/squareup/reports/applet/disputes/DisputesBootstrapScreen;
    .locals 1

    new-instance v0, Lcom/squareup/reports/applet/disputes/DisputesBootstrapScreen;

    invoke-direct {v0, p1}, Lcom/squareup/reports/applet/disputes/DisputesBootstrapScreen;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public doRegister(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    sget-object v0, Lcom/squareup/disputes/DisputesWorkflowRunner;->Companion:Lcom/squareup/disputes/DisputesWorkflowRunner$Companion;

    iget-object v1, p0, Lcom/squareup/reports/applet/disputes/DisputesBootstrapScreen;->paymentToken:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/disputes/DisputesWorkflowRunner$Companion;->startNewWorkflow(Lmortar/MortarScope;Ljava/lang/String;)V

    return-void
.end method

.method public getParentKey()Lcom/squareup/reports/applet/disputes/DisputesScope;
    .locals 1

    .line 20
    sget-object v0, Lcom/squareup/reports/applet/disputes/DisputesScope;->INSTANCE:Lcom/squareup/reports/applet/disputes/DisputesScope;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/reports/applet/disputes/DisputesBootstrapScreen;->getParentKey()Lcom/squareup/reports/applet/disputes/DisputesScope;

    move-result-object v0

    return-object v0
.end method

.method public final getPaymentToken()Ljava/lang/String;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/reports/applet/disputes/DisputesBootstrapScreen;->paymentToken:Ljava/lang/String;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 18
    const-class v0, Lcom/squareup/reports/applet/disputes/DisputesSection;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DisputesBootstrapScreen(paymentToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/reports/applet/disputes/DisputesBootstrapScreen;->paymentToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
