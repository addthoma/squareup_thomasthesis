.class public final Lcom/squareup/reports/applet/ReportsAppletScopeRunner_Factory;
.super Ljava/lang/Object;
.source "ReportsAppletScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/reports/applet/ReportsAppletScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final reportConfigBuilderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private final reportConfigSubjectProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfig;",
            ">;>;"
        }
    .end annotation
.end field

.field private final reportsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/ReportsApplet;",
            ">;"
        }
    .end annotation
.end field

.field private final salesReportDetailLevelHolderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/ReportsApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfig;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/reports/applet/ReportsAppletScopeRunner_Factory;->reportsProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/squareup/reports/applet/ReportsAppletScopeRunner_Factory;->reportConfigSubjectProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/reports/applet/ReportsAppletScopeRunner_Factory;->reportConfigBuilderProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/reports/applet/ReportsAppletScopeRunner_Factory;->salesReportDetailLevelHolderProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/reports/applet/ReportsAppletScopeRunner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/ReportsApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfig;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;",
            ">;)",
            "Lcom/squareup/reports/applet/ReportsAppletScopeRunner_Factory;"
        }
    .end annotation

    .line 47
    new-instance v0, Lcom/squareup/reports/applet/ReportsAppletScopeRunner_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/reports/applet/ReportsAppletScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/reports/applet/ReportsApplet;Lrx/subjects/BehaviorSubject;Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;)Lcom/squareup/reports/applet/ReportsAppletScopeRunner;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/reports/applet/ReportsApplet;",
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfig;",
            ">;",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;",
            "Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;",
            ")",
            "Lcom/squareup/reports/applet/ReportsAppletScopeRunner;"
        }
    .end annotation

    .line 53
    new-instance v0, Lcom/squareup/reports/applet/ReportsAppletScopeRunner;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/reports/applet/ReportsAppletScopeRunner;-><init>(Lcom/squareup/reports/applet/ReportsApplet;Lrx/subjects/BehaviorSubject;Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/reports/applet/ReportsAppletScopeRunner;
    .locals 4

    .line 40
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsAppletScopeRunner_Factory;->reportsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/ReportsApplet;

    iget-object v1, p0, Lcom/squareup/reports/applet/ReportsAppletScopeRunner_Factory;->reportConfigSubjectProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lrx/subjects/BehaviorSubject;

    iget-object v2, p0, Lcom/squareup/reports/applet/ReportsAppletScopeRunner_Factory;->reportConfigBuilderProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;

    iget-object v3, p0, Lcom/squareup/reports/applet/ReportsAppletScopeRunner_Factory;->salesReportDetailLevelHolderProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/reports/applet/ReportsAppletScopeRunner_Factory;->newInstance(Lcom/squareup/reports/applet/ReportsApplet;Lrx/subjects/BehaviorSubject;Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;)Lcom/squareup/reports/applet/ReportsAppletScopeRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/reports/applet/ReportsAppletScopeRunner_Factory;->get()Lcom/squareup/reports/applet/ReportsAppletScopeRunner;

    move-result-object v0

    return-object v0
.end method
