.class public final Lcom/squareup/reports/applet/disputes/DisputesSection$1;
.super Lcom/squareup/applet/SectionAccess;
.source "DisputesSection.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/reports/applet/disputes/DisputesSection;-><init>(Lcom/squareup/disputes/api/HandlesDisputes;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016J\u000e\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "com/squareup/reports/applet/disputes/DisputesSection$1",
        "Lcom/squareup/applet/SectionAccess;",
        "determineVisibility",
        "",
        "getPermissions",
        "",
        "Lcom/squareup/permissions/Permission;",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $handlesDisputes:Lcom/squareup/disputes/api/HandlesDisputes;


# direct methods
.method constructor <init>(Lcom/squareup/disputes/api/HandlesDisputes;)V
    .locals 0

    .line 15
    iput-object p1, p0, Lcom/squareup/reports/applet/disputes/DisputesSection$1;->$handlesDisputes:Lcom/squareup/disputes/api/HandlesDisputes;

    invoke-direct {p0}, Lcom/squareup/applet/SectionAccess;-><init>()V

    return-void
.end method


# virtual methods
.method public determineVisibility()Z
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/reports/applet/disputes/DisputesSection$1;->$handlesDisputes:Lcom/squareup/disputes/api/HandlesDisputes;

    invoke-interface {v0}, Lcom/squareup/disputes/api/HandlesDisputes;->isVisible()Z

    move-result v0

    return v0
.end method

.method public getPermissions()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    .line 17
    sget-object v0, Lcom/squareup/permissions/Permission;->MANAGE_DISPUTES:Lcom/squareup/permissions/Permission;

    invoke-static {v0}, Lkotlin/collections/SetsKt;->setOf(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
