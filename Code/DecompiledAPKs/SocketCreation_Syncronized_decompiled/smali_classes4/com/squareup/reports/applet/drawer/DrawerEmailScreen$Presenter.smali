.class public Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;
.super Lcom/squareup/reports/applet/BaseEmailCardPresenter;
.source "DrawerEmailScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/drawer/DrawerEmailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation


# static fields
.field private static final EMAIL_SENT:Ljava/lang/String; = "cash_drawer_email_sent"


# instance fields
.field private final cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

.field private final cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

.field private emailSent:Z

.field private final flow:Lflow/Flow;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/cashmanagement/CashDrawerShiftManager;Lcom/squareup/cashmanagement/CashManagementSettings;Lcom/squareup/connectivity/ConnectivityMonitor;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 0
    .param p6    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p7    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 58
    invoke-direct {p0, p6, p7}, Lcom/squareup/reports/applet/BaseEmailCardPresenter;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    const/4 p6, 0x0

    .line 51
    iput-boolean p6, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;->emailSent:Z

    .line 59
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;->flow:Lflow/Flow;

    .line 60
    iput-object p2, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 61
    iput-object p3, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    .line 62
    iput-object p4, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    .line 63
    iput-object p5, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    return-void
.end method

.method private goBack()V
    .locals 4

    .line 103
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method private screen()Lcom/squareup/reports/applet/drawer/DrawerEmailScreen;
    .locals 1

    .line 99
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/BaseEmailCardView;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/BaseEmailCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lflow/path/Path;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen;

    return-object v0
.end method

.method private showEmailMessage(Z)V
    .locals 7

    .line 107
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-interface {v0}, Lcom/squareup/connectivity/ConnectivityMonitor;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 108
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/reports/applet/BaseEmailCardView;

    sget v2, Lcom/squareup/billhistoryui/R$string;->no_internet_connection:I

    sget v3, Lcom/squareup/reports/applet/R$string;->drawer_history_email_message_offline:I

    const/4 v4, 0x1

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move v6, p1

    invoke-virtual/range {v1 .. v6}, Lcom/squareup/reports/applet/BaseEmailCardView;->showEmailMessage(IIZLcom/squareup/glyph/GlyphTypeface$Glyph;Z)V

    goto :goto_0

    .line 111
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/BaseEmailCardView;

    sget v1, Lcom/squareup/reports/applet/R$string;->drawer_history_email_message:I

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_ENVELOPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/reports/applet/BaseEmailCardView;->showEmailMessage(ILcom/squareup/glyph/GlyphTypeface$Glyph;Z)V

    :goto_0
    return-void
.end method


# virtual methods
.method public getActionBarConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 4

    .line 77
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/reports/applet/R$string;->drawer_history_email_drawer_report:I

    .line 78
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/reports/applet/drawer/-$$Lambda$A8qxokjk8JXpb54wfDDlt_Vu5ws;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/drawer/-$$Lambda$A8qxokjk8JXpb54wfDDlt_Vu5ws;-><init>(Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;)V

    .line 79
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultEmailRecipient()Ljava/lang/String;
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/cashmanagement/CashManagementSettings;->getEmailRecipient()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onBackPressed()Z
    .locals 1

    .line 88
    invoke-direct {p0}, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;->goBack()V

    const/4 v0, 0x1

    return v0
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 67
    invoke-super {p0, p1}, Lcom/squareup/reports/applet/BaseEmailCardPresenter;->onLoad(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "cash_drawer_email_sent"

    .line 69
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;->emailSent:Z

    .line 70
    iget-boolean p1, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;->emailSent:Z

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 71
    invoke-direct {p0, p1}, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;->showEmailMessage(Z)V

    :cond_0
    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 84
    iget-boolean v0, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;->emailSent:Z

    const-string v1, "cash_drawer_email_sent"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public sendEmail(Ljava/lang/String;)V
    .locals 3

    const/4 v0, 0x1

    .line 93
    iput-boolean v0, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;->emailSent:Z

    .line 94
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    invoke-direct {p0}, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;->screen()Lcom/squareup/reports/applet/drawer/DrawerEmailScreen;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen;->access$000(Lcom/squareup/reports/applet/drawer/DrawerEmailScreen;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p1}, Lcom/squareup/cashmanagement/CashDrawerShiftManager;->emailCashDrawerShiftReport(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    invoke-direct {p0, v0}, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;->showEmailMessage(Z)V

    return-void
.end method
