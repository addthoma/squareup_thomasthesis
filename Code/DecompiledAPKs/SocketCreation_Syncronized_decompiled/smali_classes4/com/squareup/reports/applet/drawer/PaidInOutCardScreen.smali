.class public final Lcom/squareup/reports/applet/drawer/PaidInOutCardScreen;
.super Lcom/squareup/reports/applet/InReportsAppletScope;
.source "PaidInOutCardScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/reports/applet/drawer/PaidInOutCardScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/drawer/PaidInOutCardScreen$Component;,
        Lcom/squareup/reports/applet/drawer/PaidInOutCardScreen$Module;,
        Lcom/squareup/reports/applet/drawer/PaidInOutCardScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/reports/applet/drawer/PaidInOutCardScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/reports/applet/drawer/PaidInOutCardScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/reports/applet/drawer/PaidInOutCardScreen;

    invoke-direct {v0}, Lcom/squareup/reports/applet/drawer/PaidInOutCardScreen;-><init>()V

    sput-object v0, Lcom/squareup/reports/applet/drawer/PaidInOutCardScreen;->INSTANCE:Lcom/squareup/reports/applet/drawer/PaidInOutCardScreen;

    .line 87
    sget-object v0, Lcom/squareup/reports/applet/drawer/PaidInOutCardScreen;->INSTANCE:Lcom/squareup/reports/applet/drawer/PaidInOutCardScreen;

    .line 88
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/reports/applet/drawer/PaidInOutCardScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 41
    invoke-direct {p0}, Lcom/squareup/reports/applet/InReportsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 45
    const-class v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 91
    sget v0, Lcom/squareup/reports/applet/R$layout;->paid_in_out_main_view:I

    return v0
.end method
