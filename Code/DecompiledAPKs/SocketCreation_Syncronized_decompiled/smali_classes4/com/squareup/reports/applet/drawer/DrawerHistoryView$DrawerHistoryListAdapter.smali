.class Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;
.super Landroid/widget/BaseAdapter;
.source "DrawerHistoryView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/drawer/DrawerHistoryView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DrawerHistoryListAdapter"
.end annotation


# static fields
.field private static final DIVIDER_TYPE:I = 0x1

.field private static final HEADER_TYPE:I = 0x0

.field private static final SHIFT_TYPE:I = 0x2


# instance fields
.field private closedCursor:Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

.field final synthetic this$0:Lcom/squareup/reports/applet/drawer/DrawerHistoryView;

.field private unclosedCursor:Lcom/squareup/cashmanagement/CashDrawerShiftCursor;


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/drawer/DrawerHistoryView;Lcom/squareup/cashmanagement/CashDrawerShiftCursor;Lcom/squareup/cashmanagement/CashDrawerShiftCursor;)V
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->this$0:Lcom/squareup/reports/applet/drawer/DrawerHistoryView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 135
    iput-object p2, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->unclosedCursor:Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

    .line 136
    iput-object p3, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->closedCursor:Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

    return-void
.end method

.method private bindHeaderRow(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    if-nez p2, :cond_0

    .line 240
    sget p2, Lcom/squareup/reports/applet/R$layout;->section_title_row:I

    .line 241
    invoke-static {p2, p3}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    check-cast p2, Landroid/widget/TextView;

    .line 243
    iget-object p3, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->this$0:Lcom/squareup/reports/applet/drawer/DrawerHistoryView;

    invoke-static {p3}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->access$000(Lcom/squareup/reports/applet/drawer/DrawerHistoryView;)I

    move-result p3

    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->this$0:Lcom/squareup/reports/applet/drawer/DrawerHistoryView;

    invoke-static {v0}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->access$000(Lcom/squareup/reports/applet/drawer/DrawerHistoryView;)I

    move-result v0

    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->this$0:Lcom/squareup/reports/applet/drawer/DrawerHistoryView;

    invoke-static {v1}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->access$000(Lcom/squareup/reports/applet/drawer/DrawerHistoryView;)I

    move-result v1

    iget-object v2, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->this$0:Lcom/squareup/reports/applet/drawer/DrawerHistoryView;

    invoke-static {v2}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->access$100(Lcom/squareup/reports/applet/drawer/DrawerHistoryView;)I

    move-result v2

    invoke-virtual {p2, p3, v0, v1, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 244
    invoke-direct {p0}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->shouldShowUnclosed()Z

    move-result p3

    if-eqz p3, :cond_1

    if-nez p1, :cond_1

    .line 245
    sget p1, Lcom/squareup/reports/applet/R$string;->drawer_history_unclosed_drawers_uppercase:I

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 247
    :cond_1
    sget p1, Lcom/squareup/reports/applet/R$string;->drawer_history_previous_drawers_uppercase:I

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    return-object p2
.end method

.method private bindShiftRow(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    if-nez p2, :cond_0

    .line 222
    sget p2, Lcom/squareup/reports/applet/R$layout;->drawer_history_row:I

    .line 223
    invoke-static {p2, p3}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    check-cast p2, Landroid/widget/LinearLayout;

    .line 226
    invoke-direct {p0, p1}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->getShiftFromPosition(I)Lcom/squareup/cashmanagement/CashDrawerShiftRow;

    move-result-object p1

    .line 227
    sget p3, Lcom/squareup/reports/applet/R$id;->drawer_date:I

    invoke-static {p2, p3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    .line 228
    sget v0, Lcom/squareup/reports/applet/R$id;->drawer_description:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 229
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->this$0:Lcom/squareup/reports/applet/drawer/DrawerHistoryView;

    iget-object v1, v1, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->presenter:Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;

    invoke-virtual {v1, p1}, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->getDateString(Lcom/squareup/cashmanagement/CashDrawerShiftRow;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 230
    invoke-interface {p1}, Lcom/squareup/cashmanagement/CashDrawerShiftRow;->getDescription()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 231
    invoke-interface {p1}, Lcom/squareup/cashmanagement/CashDrawerShiftRow;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    return-object p2
.end method

.method private getShiftFromPosition(I)Lcom/squareup/cashmanagement/CashDrawerShiftRow;
    .locals 0

    .line 236
    invoke-virtual {p0, p1}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cashmanagement/CashDrawerShiftRow;

    return-object p1
.end method

.method private shouldShowClosed()Z
    .locals 1

    .line 257
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->closedCursor:Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

    invoke-virtual {v0}, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private shouldShowUnclosed()Z
    .locals 1

    .line 253
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->unclosedCursor:Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

    invoke-virtual {v0}, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .line 146
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->unclosedCursor:Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

    invoke-virtual {v0}, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;->getCount()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->closedCursor:Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

    invoke-virtual {v1}, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;->getCount()I

    move-result v1

    add-int/2addr v0, v1

    .line 147
    invoke-direct {p0}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->shouldShowUnclosed()Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    .line 151
    :cond_0
    invoke-direct {p0}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->shouldShowClosed()Z

    move-result v1

    if-eqz v1, :cond_1

    add-int/lit8 v0, v0, 0x1

    .line 155
    :cond_1
    invoke-direct {p0}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->shouldShowClosed()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->shouldShowUnclosed()Z

    move-result v1

    if-eqz v1, :cond_2

    add-int/lit8 v0, v0, 0x1

    :cond_2
    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 3

    .line 165
    invoke-direct {p0}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->shouldShowUnclosed()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    add-int/lit8 v0, p1, -0x1

    .line 166
    iget-object v2, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->unclosedCursor:Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

    invoke-virtual {v2}, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 167
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->unclosedCursor:Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

    invoke-virtual {p1, v0}, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->unclosedCursor:Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

    invoke-virtual {v0}, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    add-int/2addr v1, v0

    .line 173
    :cond_1
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->closedCursor:Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

    sub-int/2addr p1, v1

    invoke-virtual {v0, p1}, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 184
    :cond_0
    invoke-direct {p0}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->shouldShowUnclosed()Z

    move-result v1

    const/4 v2, 0x2

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->shouldShowClosed()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 185
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->unclosedCursor:Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

    invoke-virtual {v1}, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;->getCount()I

    move-result v1

    const/4 v3, 0x1

    add-int/2addr v1, v3

    if-ne p1, v1, :cond_1

    return v3

    .line 188
    :cond_1
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->unclosedCursor:Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

    invoke-virtual {v1}, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;->getCount()I

    move-result v1

    add-int/2addr v1, v2

    if-ne p1, v1, :cond_2

    return v0

    :cond_2
    return v2
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .line 205
    invoke-virtual {p0, p1}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->getItemViewType(I)I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 215
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->bindShiftRow(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1

    .line 217
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Bad viewType"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    if-nez p2, :cond_2

    .line 211
    new-instance p2, Lcom/squareup/marin/widgets/HorizontalRuleView;

    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->this$0:Lcom/squareup/reports/applet/drawer/DrawerHistoryView;

    invoke-virtual {p1}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->getContext()Landroid/content/Context;

    move-result-object p1

    iget-object p3, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->this$0:Lcom/squareup/reports/applet/drawer/DrawerHistoryView;

    invoke-static {p3}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->access$000(Lcom/squareup/reports/applet/drawer/DrawerHistoryView;)I

    move-result p3

    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->this$0:Lcom/squareup/reports/applet/drawer/DrawerHistoryView;

    invoke-static {v0}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->access$100(Lcom/squareup/reports/applet/drawer/DrawerHistoryView;)I

    move-result v0

    invoke-direct {p2, p1, p3, v0}, Lcom/squareup/marin/widgets/HorizontalRuleView;-><init>(Landroid/content/Context;II)V

    :cond_2
    return-object p2

    .line 208
    :cond_3
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->bindHeaderRow(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .line 196
    invoke-virtual {p0, p1}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->getItemViewType(I)I

    move-result p1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method setCursors(Lcom/squareup/cashmanagement/CashDrawerShiftCursor;Lcom/squareup/cashmanagement/CashDrawerShiftCursor;)V
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->unclosedCursor:Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

    .line 141
    iput-object p2, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->closedCursor:Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

    .line 142
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->notifyDataSetChanged()V

    return-void
.end method
