.class public Lcom/squareup/reports/applet/drawer/PaidInOutView;
.super Landroid/widget/LinearLayout;
.source "PaidInOutView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/drawer/PaidInOutView$Component;
    }
.end annotation


# instance fields
.field presenter:Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const-class p2, Lcom/squareup/reports/applet/drawer/PaidInOutView$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/reports/applet/drawer/PaidInOutView$Component;

    invoke-interface {p1, p0}, Lcom/squareup/reports/applet/drawer/PaidInOutView$Component;->inject(Lcom/squareup/reports/applet/drawer/PaidInOutView;)V

    return-void
.end method


# virtual methods
.method public hideKeyboard()V
    .locals 0

    .line 52
    invoke-static {p0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutView;->presenter:Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->dropView(Ljava/lang/Object;)V

    .line 48
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 30
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 32
    sget v0, Lcom/squareup/reports/applet/R$id;->recycler:I

    invoke-virtual {p0, v0}, Lcom/squareup/reports/applet/drawer/PaidInOutView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 33
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 35
    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/PaidInOutView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 36
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;->setOrientation(I)V

    .line 37
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/PaidInOutView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 39
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutView;->presenter:Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->createAdapter()Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;

    move-result-object v0

    .line 40
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/PaidInOutView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 41
    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;->update()V

    .line 43
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutView;->presenter:Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method
