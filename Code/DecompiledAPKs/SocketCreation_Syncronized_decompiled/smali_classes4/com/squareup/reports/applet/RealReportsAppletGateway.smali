.class public final Lcom/squareup/reports/applet/RealReportsAppletGateway;
.super Ljava/lang/Object;
.source "RealReportsAppletGateway.kt"

# interfaces
.implements Lcom/squareup/reports/applet/ReportsAppletGateway;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016J\u0008\u0010\u0007\u001a\u00020\u0006H\u0016J\u0008\u0010\u0008\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/reports/applet/RealReportsAppletGateway;",
        "Lcom/squareup/reports/applet/ReportsAppletGateway;",
        "reportsApplet",
        "Lcom/squareup/reports/applet/ReportsApplet;",
        "(Lcom/squareup/reports/applet/ReportsApplet;)V",
        "activate",
        "",
        "activateCurrentDrawerReport",
        "activateDisputesReport",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final reportsApplet:Lcom/squareup/reports/applet/ReportsApplet;


# direct methods
.method public constructor <init>(Lcom/squareup/reports/applet/ReportsApplet;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "reportsApplet"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/reports/applet/RealReportsAppletGateway;->reportsApplet:Lcom/squareup/reports/applet/ReportsApplet;

    return-void
.end method


# virtual methods
.method public activate()V
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/reports/applet/RealReportsAppletGateway;->reportsApplet:Lcom/squareup/reports/applet/ReportsApplet;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/ReportsApplet;->activate()V

    return-void
.end method

.method public activateCurrentDrawerReport()V
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/reports/applet/RealReportsAppletGateway;->reportsApplet:Lcom/squareup/reports/applet/ReportsApplet;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/ReportsApplet;->activateCurrentDrawerReport()V

    return-void
.end method

.method public activateDisputesReport()V
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/reports/applet/RealReportsAppletGateway;->reportsApplet:Lcom/squareup/reports/applet/ReportsApplet;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/ReportsApplet;->activateDisputesReport()V

    return-void
.end method
