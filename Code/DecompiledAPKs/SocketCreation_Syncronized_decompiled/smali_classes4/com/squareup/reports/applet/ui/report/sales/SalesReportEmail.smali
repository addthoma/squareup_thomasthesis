.class public Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail;
.super Lcom/squareup/queue/RpcThreadTask;
.source "SalesReportEmail.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/queue/RpcThreadTask<",
        "Lcom/squareup/server/SimpleResponse;",
        "Lcom/squareup/reports/applet/ui/report/sales/ReportLoggedInComponent;",
        ">;"
    }
.end annotation


# instance fields
.field transient reportEmailService:Lcom/squareup/server/reporting/ReportEmailService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final request:Lcom/squareup/server/reporting/ReportEmailBody;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .line 18
    invoke-direct {p0}, Lcom/squareup/queue/RpcThreadTask;-><init>()V

    .line 19
    new-instance v0, Lcom/squareup/server/reporting/ReportEmailBody;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/server/reporting/ReportEmailBody;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail;->request:Lcom/squareup/server/reporting/ReportEmailBody;

    return-void
.end method


# virtual methods
.method protected callOnRpcThread()Lcom/squareup/server/SimpleResponse;
    .locals 2

    .line 28
    iget-object v0, p0, Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail;->reportEmailService:Lcom/squareup/server/reporting/ReportEmailService;

    iget-object v1, p0, Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail;->request:Lcom/squareup/server/reporting/ReportEmailBody;

    invoke-interface {v0, v1}, Lcom/squareup/server/reporting/ReportEmailService;->emailReport(Lcom/squareup/server/reporting/ReportEmailBody;)Lcom/squareup/server/SimpleResponse;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic callOnRpcThread()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail;->callOnRpcThread()Lcom/squareup/server/SimpleResponse;

    move-result-object v0

    return-object v0
.end method

.method protected handleResponseOnMainThread(Lcom/squareup/server/SimpleResponse;)Lcom/squareup/server/SimpleResponse;
    .locals 0

    return-object p1
.end method

.method protected bridge synthetic handleResponseOnMainThread(Ljava/lang/Object;)Lcom/squareup/server/SimpleResponse;
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/server/SimpleResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail;->handleResponseOnMainThread(Lcom/squareup/server/SimpleResponse;)Lcom/squareup/server/SimpleResponse;

    move-result-object p1

    return-object p1
.end method

.method public inject(Lcom/squareup/reports/applet/ui/report/sales/ReportLoggedInComponent;)V
    .locals 0

    .line 37
    invoke-interface {p1, p0}, Lcom/squareup/reports/applet/ui/report/sales/ReportLoggedInComponent;->inject(Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/reports/applet/ui/report/sales/ReportLoggedInComponent;

    invoke-virtual {p0, p1}, Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail;->inject(Lcom/squareup/reports/applet/ui/report/sales/ReportLoggedInComponent;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail;->request:Lcom/squareup/server/reporting/ReportEmailBody;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
