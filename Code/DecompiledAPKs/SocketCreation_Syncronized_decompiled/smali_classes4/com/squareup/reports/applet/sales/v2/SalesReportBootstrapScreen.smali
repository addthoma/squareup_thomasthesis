.class public final Lcom/squareup/reports/applet/sales/v2/SalesReportBootstrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "SalesReportBootstrapScreen.kt"

# interfaces
.implements Lcom/squareup/container/layer/InSection;
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/sales/v2/SalesReportBootstrapScreen$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u000cH\u00d6\u0001J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0008\u0010\u0011\u001a\u00020\u0012H\u0016J\u0019\u0010\u0013\u001a\u00020\u000e2\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u000cH\u00d6\u0001R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0018\u0010\u0007\u001a\u0006\u0012\u0002\u0008\u00030\u00088VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/reports/applet/sales/v2/SalesReportBootstrapScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "Lcom/squareup/container/layer/InSection;",
        "Landroid/os/Parcelable;",
        "detailLevel",
        "Lcom/squareup/api/salesreport/DetailLevel;",
        "(Lcom/squareup/api/salesreport/DetailLevel;)V",
        "section",
        "Ljava/lang/Class;",
        "getSection",
        "()Ljava/lang/Class;",
        "describeContents",
        "",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "",
        "writeToParcel",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final detailLevel:Lcom/squareup/api/salesreport/DetailLevel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/reports/applet/sales/v2/SalesReportBootstrapScreen$Creator;

    invoke-direct {v0}, Lcom/squareup/reports/applet/sales/v2/SalesReportBootstrapScreen$Creator;-><init>()V

    sput-object v0, Lcom/squareup/reports/applet/sales/v2/SalesReportBootstrapScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/salesreport/DetailLevel;)V
    .locals 1

    const-string v0, "detailLevel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v2/SalesReportBootstrapScreen;->detailLevel:Lcom/squareup/api/salesreport/DetailLevel;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public doRegister(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-static {p1}, Lcom/squareup/reports/applet/sales/v2/SalesReportBootstrapScreenKt;->access$getSalesReportWorkflowRunner(Lmortar/MortarScope;)Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner;

    move-result-object p1

    .line 25
    new-instance v0, Lcom/squareup/salesreport/SalesReportProps;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v2/SalesReportBootstrapScreen;->detailLevel:Lcom/squareup/api/salesreport/DetailLevel;

    invoke-direct {v0, v1}, Lcom/squareup/salesreport/SalesReportProps;-><init>(Lcom/squareup/api/salesreport/DetailLevel;)V

    invoke-interface {p1, v0}, Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner;->startWorkflow(Lcom/squareup/salesreport/SalesReportProps;)V

    return-void
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 21
    sget-object v0, Lcom/squareup/reports/applet/sales/SalesReportScope;->INSTANCE:Lcom/squareup/reports/applet/sales/SalesReportScope;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 19
    const-class v0, Lcom/squareup/reports/applet/sales/SalesReportSection;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v2/SalesReportBootstrapScreen;->detailLevel:Lcom/squareup/api/salesreport/DetailLevel;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
