.class public final Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;
.super Lcom/squareup/container/DynamicPropsWorkflowV2Runner;
.source "RealSalesReportWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner<",
        "Lcom/squareup/salesreport/SalesReportProps;",
        "Lcom/squareup/salesreport/SalesReportOutput;",
        ">;",
        "Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u00012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002B\'\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0014J\u0010\u0010\u0014\u001a\u00020\u00112\u0006\u0010\u0015\u001a\u00020\u0003H\u0016R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u00020\u0008X\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;",
        "Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner;",
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner;",
        "Lcom/squareup/salesreport/SalesReportProps;",
        "Lcom/squareup/salesreport/SalesReportOutput;",
        "viewFactory",
        "Lcom/squareup/salesreport/SalesReportViewFactory;",
        "workflow",
        "Lcom/squareup/salesreport/SalesReportWorkflow;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "feeTutorial",
        "Lcom/squareup/feetutorial/FeeTutorial;",
        "(Lcom/squareup/salesreport/SalesReportViewFactory;Lcom/squareup/salesreport/SalesReportWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/feetutorial/FeeTutorial;)V",
        "getWorkflow",
        "()Lcom/squareup/salesreport/SalesReportWorkflow;",
        "onEnterScope",
        "",
        "newScope",
        "Lmortar/MortarScope;",
        "startWorkflow",
        "salesReportProps",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final container:Lcom/squareup/ui/main/PosContainer;

.field private final feeTutorial:Lcom/squareup/feetutorial/FeeTutorial;

.field private final workflow:Lcom/squareup/salesreport/SalesReportWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/salesreport/SalesReportViewFactory;Lcom/squareup/salesreport/SalesReportWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/feetutorial/FeeTutorial;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "viewFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "feeTutorial"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    sget-object v0, Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner;->Companion:Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner$Companion;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner$Companion;->getNAME()Ljava/lang/String;

    move-result-object v2

    .line 26
    invoke-interface {p3}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v3

    .line 27
    move-object v4, p1

    check-cast v4, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x18

    const/4 v8, 0x0

    move-object v1, p0

    .line 24
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;ZLkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;->workflow:Lcom/squareup/salesreport/SalesReportWorkflow;

    iput-object p3, p0, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    iput-object p4, p0, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;->feeTutorial:Lcom/squareup/feetutorial/FeeTutorial;

    return-void
.end method

.method public static final synthetic access$getContainer$p(Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;)Lcom/squareup/ui/main/PosContainer;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    return-object p0
.end method

.method public static final synthetic access$getFeeTutorial$p(Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;)Lcom/squareup/feetutorial/FeeTutorial;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;->feeTutorial:Lcom/squareup/feetutorial/FeeTutorial;

    return-object p0
.end method


# virtual methods
.method protected getWorkflow()Lcom/squareup/salesreport/SalesReportWorkflow;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;->workflow:Lcom/squareup/salesreport/SalesReportWorkflow;

    return-object v0
.end method

.method public bridge synthetic getWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;->getWorkflow()Lcom/squareup/salesreport/SalesReportWorkflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "newScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-super {p0, p1}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;->onEnterScope(Lmortar/MortarScope;)V

    .line 32
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    .line 33
    new-instance v1, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner$onEnterScope$1;

    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    invoke-direct {v1, v2}, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/main/PosContainer;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 35
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    .line 36
    new-instance v1, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner$onEnterScope$2;-><init>(Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public startWorkflow(Lcom/squareup/salesreport/SalesReportProps;)V
    .locals 1

    const-string v0, "salesReportProps"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-virtual {p0, p1}, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;->setProps(Ljava/lang/Object;)V

    .line 46
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;->ensureWorkflow()V

    return-void
.end method
