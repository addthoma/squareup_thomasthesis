.class public final Lcom/squareup/reports/applet/ReportsApplet$getHistoryFactory$1;
.super Ljava/lang/Object;
.source "ReportsApplet.kt"

# interfaces
.implements Lcom/squareup/ui/main/HistoryFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/reports/applet/ReportsApplet;->getHistoryFactory(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/applet/AppletSection;)Lcom/squareup/ui/main/HistoryFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReportsApplet.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ReportsApplet.kt\ncom/squareup/reports/applet/ReportsApplet$getHistoryFactory$1\n*L\n1#1,136:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000#\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u001a\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0003H\u0016J\u0010\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\u0008H\u0016\u00a8\u0006\n"
    }
    d2 = {
        "com/squareup/reports/applet/ReportsApplet$getHistoryFactory$1",
        "Lcom/squareup/ui/main/HistoryFactory;",
        "createHistory",
        "Lflow/History;",
        "home",
        "Lcom/squareup/ui/main/Home;",
        "currentHistory",
        "getPermissions",
        "",
        "Lcom/squareup/permissions/Permission;",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/container/ContainerTreeKey;

.field final synthetic $section:Lcom/squareup/applet/AppletSection;

.field final synthetic this$0:Lcom/squareup/reports/applet/ReportsApplet;


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/ReportsApplet;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/applet/AppletSection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/container/ContainerTreeKey;",
            "Lcom/squareup/applet/AppletSection;",
            ")V"
        }
    .end annotation

    .line 112
    iput-object p1, p0, Lcom/squareup/reports/applet/ReportsApplet$getHistoryFactory$1;->this$0:Lcom/squareup/reports/applet/ReportsApplet;

    iput-object p2, p0, Lcom/squareup/reports/applet/ReportsApplet$getHistoryFactory$1;->$screen:Lcom/squareup/container/ContainerTreeKey;

    iput-object p3, p0, Lcom/squareup/reports/applet/ReportsApplet$getHistoryFactory$1;->$section:Lcom/squareup/applet/AppletSection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createHistory(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;
    .locals 1

    const-string v0, "home"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsApplet$getHistoryFactory$1;->this$0:Lcom/squareup/reports/applet/ReportsApplet;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/reports/applet/ReportsApplet;->createHistory(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;

    move-result-object p1

    const-string p2, "this@ReportsApplet.creat\u2026ory(home, currentHistory)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    invoke-virtual {p1}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p1

    .line 119
    iget-object p2, p0, Lcom/squareup/reports/applet/ReportsApplet$getHistoryFactory$1;->$screen:Lcom/squareup/container/ContainerTreeKey;

    invoke-virtual {p1, p2}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    move-result-object p1

    .line 120
    invoke-virtual {p1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    const-string p2, "base.buildUpon()\n       \u2026een)\n            .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public getPermissions()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    .line 124
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsApplet$getHistoryFactory$1;->$section:Lcom/squareup/applet/AppletSection;

    invoke-virtual {v0}, Lcom/squareup/applet/AppletSection;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object v0

    const-string v1, "section.getAccessControl()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/applet/SectionAccess;->getPermissions()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 126
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Ljava/util/Set;

    return-object v0
.end method
