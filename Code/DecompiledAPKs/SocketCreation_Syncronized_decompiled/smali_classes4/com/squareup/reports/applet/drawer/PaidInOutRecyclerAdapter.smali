.class public Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "PaidInOutRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final PAID_IN_OUT_HISTORY_ROW:I = 0x1

.field private static final STATIC_BOTTOM_ROW_COUNT:I = 0x1

.field private static final STATIC_TOP_ROW_CONTENT:I = 0x0

.field private static final STATIC_TOP_ROW_COUNT:I = 0x1

.field private static final TOTAL_PAID_IN_OUT_ROW:I = 0x2


# instance fields
.field private final dateFormatter:Ljava/text/DateFormat;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final presenter:Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;

.field private final priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;Lcom/squareup/money/PriceLocaleHelper;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;",
            "Lcom/squareup/money/PriceLocaleHelper;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .line 49
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;->presenter:Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;

    .line 51
    iput-object p2, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    .line 52
    iput-object p3, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;->dateFormatter:Ljava/text/DateFormat;

    .line 53
    iput-object p4, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 54
    iput-object p5, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;)Ljava/lang/String;
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;->formattedEmployeeName(Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private formatName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 244
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/reports/applet/R$string;->paid_in_out_employee_name:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "name"

    .line 245
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 246
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 247
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private formattedEmployeeName(Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;)Ljava/lang/String;
    .locals 3

    .line 228
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;->employee_first_name:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;->employee_last_name:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/reports/applet/R$string;->paid_in_out_employee_full_name:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;->employee_first_name:Ljava/lang/String;

    const-string v2, "first_name"

    .line 230
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;->employee_last_name:Ljava/lang/String;

    const-string v1, "last_name"

    .line 231
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 232
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 233
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 234
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;->employee_first_name:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 235
    iget-object p1, p1, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;->employee_first_name:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;->formatName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 236
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;->employee_last_name:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 237
    iget-object p1, p1, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;->employee_last_name:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;->formatName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_2
    const-string p1, ""

    return-object p1
.end method


# virtual methods
.method public eventAdded()V
    .locals 2

    .line 110
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;->presenter:Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->updateEvents()V

    const/4 v0, 0x1

    .line 112
    invoke-virtual {p0, v0}, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;->notifyItemInserted(I)V

    .line 114
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;->getItemCount()I

    move-result v1

    sub-int/2addr v1, v0

    invoke-virtual {p0, v1}, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;->notifyItemChanged(I)V

    return-void
.end method

.method public getItemCount()I
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;->presenter:Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->getSize()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getItemViewType(I)I
    .locals 2

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 98
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;->getItemCount()I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    if-ne p1, v0, :cond_1

    const/4 p1, 0x2

    return p1

    :cond_1
    return v1
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;->onBindViewHolder(Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;I)V
    .locals 2

    .line 77
    invoke-virtual {p1}, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->getItemViewType()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 p2, 0x2

    if-eq v0, p2, :cond_0

    goto :goto_0

    .line 86
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->bindTotalPaidInOutRow()V

    goto :goto_0

    :cond_1
    sub-int/2addr p2, v1

    .line 83
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;->presenter:Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;

    invoke-virtual {v0, p2}, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->getEvent(I)Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->bindPaidInOutHistoryRow(Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;)V

    goto :goto_0

    .line 79
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->bindStaticTopRowContent()V

    :goto_0
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 31
    invoke-virtual {p0, p1, p2}, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;
    .locals 7

    .line 58
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz p2, :cond_2

    const/4 v2, 0x1

    if-eq p2, v2, :cond_1

    const/4 v2, 0x2

    if-ne p2, v2, :cond_0

    .line 68
    sget p2, Lcom/squareup/reports/applet/R$layout;->paid_in_out_total_row:I

    invoke-virtual {v0, p2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    goto :goto_0

    .line 71
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo p2, "viewType not supported"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 65
    :cond_1
    sget p2, Lcom/squareup/reports/applet/R$layout;->paid_in_out_event_row:I

    invoke-virtual {v0, p2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    goto :goto_0

    .line 62
    :cond_2
    sget p2, Lcom/squareup/reports/applet/R$layout;->add_paid_in_out_view:I

    invoke-virtual {v0, p2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    :goto_0
    move-object v2, p1

    .line 73
    new-instance p1, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;

    iget-object v3, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;->presenter:Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;

    iget-object v4, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v5, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;->dateFormatter:Ljava/text/DateFormat;

    iget-object v6, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;->moneyFormatter:Lcom/squareup/text/Formatter;

    move-object v0, p1

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;-><init>(Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;Landroid/view/View;Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;Lcom/squareup/money/PriceLocaleHelper;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;)V

    return-object p1
.end method

.method public update()V
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;->presenter:Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->updateEvents()V

    .line 106
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;->notifyDataSetChanged()V

    return-void
.end method
