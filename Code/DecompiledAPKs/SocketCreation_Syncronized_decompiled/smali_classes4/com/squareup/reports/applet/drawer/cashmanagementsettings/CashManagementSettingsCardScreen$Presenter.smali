.class Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;
.super Lcom/squareup/settings/ui/SettingsCardPresenter;
.source "CashManagementSettingsCardScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/settings/ui/SettingsCardPresenter<",
        "Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardView;",
        ">;"
    }
.end annotation


# instance fields
.field private cashManagementInitiallyEnabled:Z

.field private final cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

.field private final flow:Lflow/Flow;

.field private final helper:Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/settings/ui/SettingsCardPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/cashmanagement/CashManagementSettings;Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 57
    invoke-direct {p0, p1}, Lcom/squareup/settings/ui/SettingsCardPresenter;-><init>(Lcom/squareup/settings/ui/SettingsCardPresenter$CoreParameters;)V

    .line 58
    invoke-virtual {p1}, Lcom/squareup/settings/ui/SettingsCardPresenter$CoreParameters;->getFlow()Lflow/Flow;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->flow:Lflow/Flow;

    .line 59
    iput-object p2, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 60
    iput-object p3, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    .line 61
    iput-object p4, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->helper:Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;

    return-void
.end method

.method private backToPreviousScreen()V
    .locals 4

    .line 124
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->screenForAssertion()Ljava/lang/Class;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public enableAutoEmailToggled(Z)V
    .locals 2

    .line 108
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->helper:Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;

    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->enableAutoEmailToggled(Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;Z)V

    return-void
.end method

.method public enableCashManagementToggled(Z)V
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    invoke-virtual {v0, p1}, Lcom/squareup/cashmanagement/CashManagementSettings;->enableCashManagementToggled(Z)V

    return-void
.end method

.method public getActionbarText()Ljava/lang/String;
    .locals 2

    .line 95
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/settings/cashmanagement/R$string;->cash_management:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->helper:Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;

    invoke-virtual {v0}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v0

    return-object v0
.end method

.method public getMoneyFormatter()Lcom/squareup/text/Formatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 112
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->helper:Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;

    invoke-virtual {v0}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v0

    return-object v0
.end method

.method public getPriceLocaleHelper()Lcom/squareup/money/PriceLocaleHelper;
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->helper:Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;

    invoke-virtual {v0}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->getPriceLocaleHelper()Lcom/squareup/money/PriceLocaleHelper;

    move-result-object v0

    return-object v0
.end method

.method public getPrimaryButtonText()Ljava/lang/String;
    .locals 2

    .line 99
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/settings/cashmanagement/R$string;->cash_management_settings_save:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$0$CashManagementSettingsCardScreen$Presenter(Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 70
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->cashManagementInitiallyEnabled:Z

    return-void
.end method

.method public synthetic lambda$onLoad$1$CashManagementSettingsCardScreen$Presenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 68
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/cashmanagement/CashManagementSettings;->cashManagementEnabled()Lio/reactivex/Observable;

    move-result-object v0

    const/4 v1, 0x0

    .line 69
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->first(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/-$$Lambda$CashManagementSettingsCardScreen$Presenter$45GVGhOV6keFijfOfiz8W5gLTUA;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/-$$Lambda$CashManagementSettingsCardScreen$Presenter$45GVGhOV6keFijfOfiz8W5gLTUA;-><init>(Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;)V

    .line 70
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onLoad$2$CashManagementSettingsCardScreen$Presenter(Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 73
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/cashmanagement/CashManagementSettings;->cashManagementEnabled()Lio/reactivex/Observable;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/-$$Lambda$vJ3JwfGLGDMwiv3ikKp6svsw7qs;

    invoke-direct {v1, p1}, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/-$$Lambda$vJ3JwfGLGDMwiv3ikKp6svsw7qs;-><init>(Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardView;)V

    .line 74
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 65
    invoke-super {p0, p1}, Lcom/squareup/settings/ui/SettingsCardPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 66
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardView;

    .line 67
    new-instance v0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/-$$Lambda$CashManagementSettingsCardScreen$Presenter$2IpXMmqS-SwDKpfEfi9wog9Cgck;

    invoke-direct {v0, p0}, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/-$$Lambda$CashManagementSettingsCardScreen$Presenter$2IpXMmqS-SwDKpfEfi9wog9Cgck;-><init>(Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 72
    new-instance v0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/-$$Lambda$CashManagementSettingsCardScreen$Presenter$bchGVhUek-GstNxq798_eP0LSGM;

    invoke-direct {v0, p0, p1}, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/-$$Lambda$CashManagementSettingsCardScreen$Presenter$bchGVhUek-GstNxq798_eP0LSGM;-><init>(Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 75
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->helper:Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;

    invoke-virtual {v0, p1}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->onLoad(Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;)V

    return-void
.end method

.method protected onUpPressed()Z
    .locals 3

    .line 79
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    iget-boolean v1, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->cashManagementInitiallyEnabled:Z

    invoke-virtual {v0, v1}, Lcom/squareup/cashmanagement/CashManagementSettings;->setCashManagementEnabled(Z)V

    .line 80
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->helper:Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;

    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;

    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->hasView()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->handleBackPressed(Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;Z)Z

    .line 81
    invoke-direct {p0}, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->backToPreviousScreen()V

    const/4 v0, 0x1

    return v0
.end method

.method protected saveSettings()V
    .locals 2

    .line 90
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->helper:Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;

    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;

    invoke-virtual {v0, v1}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->saveSettings(Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;)V

    .line 91
    invoke-direct {p0}, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;->backToPreviousScreen()V

    return-void
.end method

.method public screenForAssertion()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;"
        }
    .end annotation

    .line 86
    const-class v0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen;

    return-object v0
.end method
