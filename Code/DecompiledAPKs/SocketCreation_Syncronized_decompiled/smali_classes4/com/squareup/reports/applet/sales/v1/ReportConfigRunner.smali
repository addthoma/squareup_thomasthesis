.class public Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;
.super Ljava/lang/Object;
.source "ReportConfigRunner.java"

# interfaces
.implements Lmortar/bundler/Bundler;


# static fields
.field private static final CURRENT_CONFIG_KEY:Ljava/lang/String; = "CURRENT_CONFIG"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final builder:Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;

.field private final configInEditSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfig;",
            ">;"
        }
    .end annotation
.end field

.field private final configSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfig;",
            ">;"
        }
    .end annotation
.end field

.field private final flow:Lflow/Flow;


# direct methods
.method public constructor <init>(Lflow/Flow;Lrx/subjects/BehaviorSubject;Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Flow;",
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfig;",
            ">;",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;",
            "Lcom/squareup/analytics/Analytics;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->flow:Lflow/Flow;

    .line 37
    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->configSubject:Lrx/subjects/BehaviorSubject;

    .line 38
    iput-object p3, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->builder:Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;

    .line 39
    invoke-virtual {p2}, Lrx/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Lrx/subjects/BehaviorSubject;->create(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->configInEditSubject:Lrx/subjects/BehaviorSubject;

    .line 40
    iput-object p4, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method


# virtual methods
.method public apply()V
    .locals 2

    .line 121
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->configSubject:Lrx/subjects/BehaviorSubject;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->configInEditSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v1}, Lrx/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 122
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->closeCard(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public cancel()V
    .locals 2

    .line 126
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->closeCard(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public clickEmployeeFilter()V
    .locals 2

    .line 100
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterScreen;->INSTANCE:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public configInEdit()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfig;",
            ">;"
        }
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->configInEditSubject:Lrx/subjects/BehaviorSubject;

    return-object v0
.end method

.method public daySelected(III)V
    .locals 8

    .line 48
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, v7

    move v1, p1

    move v2, p2

    move v3, p3

    .line 49
    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 51
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object p1

    .line 52
    iget-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->configInEditSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {p2}, Lrx/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    iget-object p2, p2, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->startTime:Ljava/util/Date;

    invoke-virtual {p1, p2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 53
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object p2

    .line 54
    iget-object p3, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->configInEditSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {p3}, Lrx/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    iget-object p3, p3, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->endTime:Ljava/util/Date;

    invoke-virtual {p2, p3}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 56
    invoke-virtual {v7}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p3

    invoke-static {p3}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p3

    .line 57
    invoke-virtual {p1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 60
    invoke-static {p1, p2}, Lcom/squareup/util/Times;->sameDate(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result p2

    if-eqz p2, :cond_2

    .line 61
    invoke-static {p1, v7}, Lcom/squareup/util/Times;->sameDate(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result p2

    if-eqz p2, :cond_0

    return-void

    .line 65
    :cond_0
    invoke-virtual {v7, p1}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 66
    iget-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->configInEditSubject:Lrx/subjects/BehaviorSubject;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->builder:Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;

    .line 67
    invoke-virtual {p2}, Lrx/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    invoke-virtual {v1, v2, v7, p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->dayRange(Lcom/squareup/reports/applet/sales/v1/ReportConfig;Ljava/util/Calendar;Ljava/util/Calendar;)Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    move-result-object p1

    .line 66
    invoke-virtual {p2, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 68
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance p2, Lcom/squareup/reports/applet/sales/v1/SelectCustomTimeFrameEvent;

    invoke-direct {p2, p3, v0}, Lcom/squareup/reports/applet/sales/v1/SelectCustomTimeFrameEvent;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, p2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    .line 70
    :cond_1
    iget-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->configInEditSubject:Lrx/subjects/BehaviorSubject;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->builder:Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;

    .line 71
    invoke-virtual {p2}, Lrx/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    invoke-virtual {v1, v2, p1, v7}, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->dayRange(Lcom/squareup/reports/applet/sales/v1/ReportConfig;Ljava/util/Calendar;Ljava/util/Calendar;)Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    move-result-object p1

    .line 70
    invoke-virtual {p2, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 72
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance p2, Lcom/squareup/reports/applet/sales/v1/SelectCustomTimeFrameEvent;

    invoke-direct {p2, v0, p3}, Lcom/squareup/reports/applet/sales/v1/SelectCustomTimeFrameEvent;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, p2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    .line 75
    :cond_2
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->configInEditSubject:Lrx/subjects/BehaviorSubject;

    iget-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->builder:Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;

    invoke-virtual {p1}, Lrx/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    invoke-virtual {p2, v0, v7}, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->singleDay(Lcom/squareup/reports/applet/sales/v1/ReportConfig;Ljava/util/Calendar;)Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 76
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance p2, Lcom/squareup/reports/applet/sales/v1/SelectCustomTimeFrameEvent;

    invoke-direct {p2, p3, p3}, Lcom/squareup/reports/applet/sales/v1/SelectCustomTimeFrameEvent;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, p2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :goto_0
    return-void
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 130
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    .line 137
    :cond_0
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->configInEditSubject:Lrx/subjects/BehaviorSubject;

    const-string v1, "CURRENT_CONFIG"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 141
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->configInEditSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lrx/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    const-string v1, "CURRENT_CONFIG"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method

.method public selectEndTime()V
    .locals 2

    .line 108
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->flow:Lflow/Flow;

    invoke-static {}, Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;->forEndTime()Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public selectStartTime()V
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->flow:Lflow/Flow;

    invoke-static {}, Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;->forStartTime()Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public setAllDayChecked(Z)V
    .locals 3

    .line 81
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->configInEditSubject:Lrx/subjects/BehaviorSubject;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->builder:Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;

    invoke-virtual {v0}, Lrx/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    invoke-virtual {v1, v2, p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->setAllDayChecked(Lcom/squareup/reports/applet/sales/v1/ReportConfig;Z)Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    move-result-object p1

    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public setDeviceFilterChecked(Z)V
    .locals 3

    .line 90
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->configInEditSubject:Lrx/subjects/BehaviorSubject;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->builder:Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;

    .line 91
    invoke-virtual {v0}, Lrx/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    invoke-virtual {v1, v2, p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->setDeviceFilterChecked(Lcom/squareup/reports/applet/sales/v1/ReportConfig;Z)Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    move-result-object p1

    .line 90
    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public setEmployeeFilter(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 95
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->configInEditSubject:Lrx/subjects/BehaviorSubject;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->builder:Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;

    .line 96
    invoke-virtual {v0}, Lrx/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    invoke-virtual {v1, v2, p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->setEmployeeFilter(Lcom/squareup/reports/applet/sales/v1/ReportConfig;Ljava/util/List;)Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    move-result-object p1

    .line 95
    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public setItemDetailsChecked(Z)V
    .locals 3

    .line 85
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->configInEditSubject:Lrx/subjects/BehaviorSubject;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->builder:Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;

    .line 86
    invoke-virtual {v0}, Lrx/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    invoke-virtual {v1, v2, p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->setItemDetailsChecked(Lcom/squareup/reports/applet/sales/v1/ReportConfig;Z)Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    move-result-object p1

    .line 85
    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public updateEndTime(II)V
    .locals 3

    .line 117
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->configInEditSubject:Lrx/subjects/BehaviorSubject;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->builder:Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;

    invoke-virtual {v0}, Lrx/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    invoke-virtual {v1, v2, p1, p2}, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->changeEndTime(Lcom/squareup/reports/applet/sales/v1/ReportConfig;II)Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    move-result-object p1

    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public updateStartTime(II)V
    .locals 3

    .line 112
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->configInEditSubject:Lrx/subjects/BehaviorSubject;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->builder:Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;

    .line 113
    invoke-virtual {v0}, Lrx/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    invoke-virtual {v1, v2, p1, p2}, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->changeStartTime(Lcom/squareup/reports/applet/sales/v1/ReportConfig;II)Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    move-result-object p1

    .line 112
    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method
