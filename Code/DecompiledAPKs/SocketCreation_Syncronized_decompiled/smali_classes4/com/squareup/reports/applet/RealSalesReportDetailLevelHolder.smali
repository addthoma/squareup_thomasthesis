.class public final Lcom/squareup/reports/applet/RealSalesReportDetailLevelHolder;
.super Ljava/lang/Object;
.source "RealSalesReportDetailLevelHolder.kt"

# interfaces
.implements Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\t\u001a\u00020\nH\u0016J\u0012\u0010\u000b\u001a\u00020\u000c2\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0016J\u0008\u0010\u000f\u001a\u00020\u000cH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/reports/applet/RealSalesReportDetailLevelHolder;",
        "Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "employeeManagement",
        "Lcom/squareup/permissions/EmployeeManagement;",
        "(Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/EmployeeManagement;)V",
        "pinnedEmployeeHasDetailedPermission",
        "",
        "getDetailLevel",
        "Lcom/squareup/api/salesreport/DetailLevel;",
        "grantPermissionToPinnedEmployee",
        "",
        "employeeToken",
        "",
        "revokePermissionForPinnedEmployee",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final features:Lcom/squareup/settings/server/Features;

.field private pinnedEmployeeHasDetailedPermission:Z


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/EmployeeManagement;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "employeeManagement"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/reports/applet/RealSalesReportDetailLevelHolder;->features:Lcom/squareup/settings/server/Features;

    iput-object p2, p0, Lcom/squareup/reports/applet/RealSalesReportDetailLevelHolder;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    return-void
.end method


# virtual methods
.method public getDetailLevel()Lcom/squareup/api/salesreport/DetailLevel;
    .locals 2

    .line 41
    iget-object v0, p0, Lcom/squareup/reports/applet/RealSalesReportDetailLevelHolder;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_VIEW_DETAILED_SALES_REPORTS_PERMISSION:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 42
    iget-object v0, p0, Lcom/squareup/reports/applet/RealSalesReportDetailLevelHolder;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    sget-object v1, Lcom/squareup/permissions/Permission;->DETAILED_SALES_REPORTS:Lcom/squareup/permissions/Permission;

    invoke-interface {v0, v1}, Lcom/squareup/permissions/EmployeeManagement;->hasPermission(Lcom/squareup/permissions/Permission;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/squareup/reports/applet/RealSalesReportDetailLevelHolder;->pinnedEmployeeHasDetailedPermission:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 47
    :cond_0
    sget-object v0, Lcom/squareup/api/salesreport/DetailLevel$Basic;->INSTANCE:Lcom/squareup/api/salesreport/DetailLevel$Basic;

    check-cast v0, Lcom/squareup/api/salesreport/DetailLevel;

    goto :goto_1

    .line 45
    :cond_1
    :goto_0
    sget-object v0, Lcom/squareup/api/salesreport/DetailLevel$Detailed;->INSTANCE:Lcom/squareup/api/salesreport/DetailLevel$Detailed;

    check-cast v0, Lcom/squareup/api/salesreport/DetailLevel;

    :goto_1
    return-object v0
.end method

.method public grantPermissionToPinnedEmployee(Ljava/lang/String;)V
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/reports/applet/RealSalesReportDetailLevelHolder;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0, p1}, Lcom/squareup/permissions/EmployeeManagement;->getEmployeeByToken(Ljava/lang/String;)Lcom/squareup/permissions/Employee;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 31
    sget-object v0, Lcom/squareup/permissions/Permission;->DETAILED_SALES_REPORTS:Lcom/squareup/permissions/Permission;

    invoke-static {v0}, Lkotlin/collections/SetsKt;->setOf(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/permissions/Employee;->hasAnyPermission(Ljava/util/Set;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/squareup/reports/applet/RealSalesReportDetailLevelHolder;->pinnedEmployeeHasDetailedPermission:Z

    return-void
.end method

.method public revokePermissionForPinnedEmployee()V
    .locals 1

    const/4 v0, 0x0

    .line 25
    iput-boolean v0, p0, Lcom/squareup/reports/applet/RealSalesReportDetailLevelHolder;->pinnedEmployeeHasDetailedPermission:Z

    return-void
.end method
