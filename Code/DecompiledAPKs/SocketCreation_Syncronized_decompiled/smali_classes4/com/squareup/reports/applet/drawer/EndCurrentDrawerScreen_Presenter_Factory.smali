.class public final Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "EndCurrentDrawerScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final cashDrawerShiftManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManager;",
            ">;"
        }
    .end annotation
.end field

.field private final cashManagementExtraPermissionsHolderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;",
            ">;"
        }
    .end annotation
.end field

.field private final detailsPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final reportsAppletSectionsListPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;",
            ">;)V"
        }
    .end annotation

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p2, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p3, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen_Presenter_Factory;->cashDrawerShiftManagerProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p4, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen_Presenter_Factory;->reportsAppletSectionsListPresenterProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p5, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen_Presenter_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p6, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p7, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen_Presenter_Factory;->detailsPresenterProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p8, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen_Presenter_Factory;->cashManagementExtraPermissionsHolderProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen_Presenter_Factory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;",
            ">;)",
            "Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen_Presenter_Factory;"
        }
    .end annotation

    .line 66
    new-instance v9, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen_Presenter_Factory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/cashmanagement/CashDrawerShiftManager;Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/analytics/Analytics;Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;
    .locals 10

    .line 74
    new-instance v9, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;-><init>(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/cashmanagement/CashDrawerShiftManager;Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/analytics/Analytics;Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)V

    return-object v9
.end method


# virtual methods
.method public get()Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;
    .locals 9

    .line 57
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen_Presenter_Factory;->cashDrawerShiftManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen_Presenter_Factory;->reportsAppletSectionsListPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;

    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen_Presenter_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen_Presenter_Factory;->detailsPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;

    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen_Presenter_Factory;->cashManagementExtraPermissionsHolderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;

    invoke-static/range {v1 .. v8}, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen_Presenter_Factory;->newInstance(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/cashmanagement/CashDrawerShiftManager;Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/analytics/Analytics;Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen_Presenter_Factory;->get()Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
