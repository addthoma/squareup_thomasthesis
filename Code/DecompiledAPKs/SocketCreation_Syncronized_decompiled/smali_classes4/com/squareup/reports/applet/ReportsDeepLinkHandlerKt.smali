.class public final Lcom/squareup/reports/applet/ReportsDeepLinkHandlerKt;
.super Ljava/lang/Object;
.source "ReportsDeepLinkHandler.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReportsDeepLinkHandler.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ReportsDeepLinkHandler.kt\ncom/squareup/reports/applet/ReportsDeepLinkHandlerKt\n*L\n1#1,105:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\t\"\u001a\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u00020\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\"\u0018\u0010\u0005\u001a\u00020\u0006*\u00020\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0007\"\u0018\u0010\u0008\u001a\u00020\u0006*\u00020\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\u0007\"\u0018\u0010\t\u001a\u00020\u0006*\u00020\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u0007\"\u0018\u0010\u000b\u001a\u00020\u0006*\u00020\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\u0007\"\u0018\u0010\r\u001a\u00020\u0006*\u00020\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u0007\u00a8\u0006\u000f"
    }
    d2 = {
        "disputePaymentToken",
        "",
        "Landroid/net/Uri;",
        "getDisputePaymentToken",
        "(Landroid/net/Uri;)Ljava/lang/String;",
        "isNavigation",
        "",
        "(Landroid/net/Uri;)Z",
        "isReports",
        "navigateToDisputes",
        "getNavigateToDisputes",
        "navigateToSales",
        "getNavigateToSales",
        "navigateToTopReports",
        "getNavigateToTopReports",
        "reports-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$getDisputePaymentToken$p(Landroid/net/Uri;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/reports/applet/ReportsDeepLinkHandlerKt;->getDisputePaymentToken(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getNavigateToDisputes$p(Landroid/net/Uri;)Z
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/reports/applet/ReportsDeepLinkHandlerKt;->getNavigateToDisputes(Landroid/net/Uri;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$getNavigateToSales$p(Landroid/net/Uri;)Z
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/reports/applet/ReportsDeepLinkHandlerKt;->getNavigateToSales(Landroid/net/Uri;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$getNavigateToTopReports$p(Landroid/net/Uri;)Z
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/reports/applet/ReportsDeepLinkHandlerKt;->getNavigateToTopReports(Landroid/net/Uri;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$isReports$p(Landroid/net/Uri;)Z
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/reports/applet/ReportsDeepLinkHandlerKt;->isReports(Landroid/net/Uri;)Z

    move-result p0

    return p0
.end method

.method private static final getDisputePaymentToken(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    const-string v0, "paymentToken"

    .line 104
    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static final getNavigateToDisputes(Landroid/net/Uri;)Z
    .locals 1

    .line 101
    invoke-static {p0}, Lcom/squareup/reports/applet/ReportsDeepLinkHandlerKt;->isNavigation(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "navigationID"

    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    const-string v0, "disputes"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static final getNavigateToSales(Landroid/net/Uri;)Z
    .locals 1

    .line 97
    invoke-static {p0}, Lcom/squareup/reports/applet/ReportsDeepLinkHandlerKt;->isNavigation(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "navigationID"

    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    const-string v0, "sales"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static final getNavigateToTopReports(Landroid/net/Uri;)Z
    .locals 3

    .line 93
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_2

    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object p0

    const-string v0, "/"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    :cond_2
    const/4 v1, 0x1

    :cond_3
    return v1
.end method

.method private static final isNavigation(Landroid/net/Uri;)Z
    .locals 1

    .line 90
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object p0

    const-string v0, "/navigate"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method

.method private static final isReports(Landroid/net/Uri;)Z
    .locals 1

    .line 88
    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object p0

    const-string v0, "reports"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method
