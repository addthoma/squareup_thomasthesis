.class public final Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;
.super Ljava/lang/Object;
.source "ReportConfigEmployeeFilterRunner.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterScreen;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B)\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0001\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0006\u0010\u0012\u001a\u00020\u0013J\u0006\u0010\u0014\u001a\u00020\u0013J\u000e\u0010\u0015\u001a\u00020\u00132\u0006\u0010\u0016\u001a\u00020\u0017J\u0006\u0010\u0018\u001a\u00020\u0013J\u0010\u0010\u0019\u001a\u00020\u00132\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0008\u0010\u001c\u001a\u00020\u0013H\u0016J\u000c\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u001f0\u001eR\u000e\u0010\u000b\u001a\u00020\u000cX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\r\u001a\u0014\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00110\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;",
        "Lmortar/Scoped;",
        "reportConfigRunner",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;",
        "flow",
        "Lflow/Flow;",
        "employeeFilterReactor",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor;",
        "mainThread",
        "Lrx/Scheduler;",
        "(Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;Lflow/Flow;Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor;Lrx/Scheduler;)V",
        "SCREEN_DATA_DEBOUNCE_MS",
        "",
        "employeeFilterWorkflow",
        "Lcom/squareup/workflow/rx1/Workflow;",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState;",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterEvent;",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterResult;",
        "onClickAllEmployees",
        "",
        "onClickBackArrow",
        "onClickEmployee",
        "employeeToken",
        "",
        "onClickFilterByEmployee",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "screenData",
        "Lrx/Observable;",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final SCREEN_DATA_DEBOUNCE_MS:J

.field private final employeeFilterReactor:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor;

.field private employeeFilterWorkflow:Lcom/squareup/workflow/rx1/Workflow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/rx1/Workflow<",
            "+",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState;",
            "-",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterEvent;",
            "+",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterResult;",
            ">;"
        }
    .end annotation
.end field

.field private final flow:Lflow/Flow;

.field private final mainThread:Lrx/Scheduler;

.field private final reportConfigRunner:Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;Lflow/Flow;Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor;Lrx/Scheduler;)V
    .locals 1
    .param p4    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "reportConfigRunner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "employeeFilterReactor"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainThread"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;->reportConfigRunner:Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;

    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;->flow:Lflow/Flow;

    iput-object p3, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;->employeeFilterReactor:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor;

    iput-object p4, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;->mainThread:Lrx/Scheduler;

    const-wide/16 p1, 0x1f4

    .line 42
    iput-wide p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;->SCREEN_DATA_DEBOUNCE_MS:J

    return-void
.end method

.method public static final synthetic access$getFlow$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;)Lflow/Flow;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;->flow:Lflow/Flow;

    return-object p0
.end method

.method public static final synthetic access$getMainThread$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;)Lrx/Scheduler;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;->mainThread:Lrx/Scheduler;

    return-object p0
.end method

.method public static final synthetic access$getReportConfigRunner$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;)Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;->reportConfigRunner:Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;

    return-object p0
.end method

.method public static final synthetic access$getSCREEN_DATA_DEBOUNCE_MS$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;)J
    .locals 2

    .line 35
    iget-wide v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;->SCREEN_DATA_DEBOUNCE_MS:J

    return-wide v0
.end method


# virtual methods
.method public final onClickAllEmployees()V
    .locals 2

    .line 90
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;->employeeFilterWorkflow:Lcom/squareup/workflow/rx1/Workflow;

    if-nez v0, :cond_0

    const-string v1, "employeeFilterWorkflow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget-object v1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterEvent$SelectAllEmployees;->INSTANCE:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterEvent$SelectAllEmployees;

    invoke-interface {v0, v1}, Lcom/squareup/workflow/rx1/Workflow;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method public final onClickBackArrow()V
    .locals 2

    .line 98
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;->employeeFilterWorkflow:Lcom/squareup/workflow/rx1/Workflow;

    if-nez v0, :cond_0

    const-string v1, "employeeFilterWorkflow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget-object v1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterEvent$Commit;->INSTANCE:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterEvent$Commit;

    invoke-interface {v0, v1}, Lcom/squareup/workflow/rx1/Workflow;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method public final onClickEmployee(Ljava/lang/String;)V
    .locals 2

    const-string v0, "employeeToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;->employeeFilterWorkflow:Lcom/squareup/workflow/rx1/Workflow;

    if-nez v0, :cond_0

    const-string v1, "employeeFilterWorkflow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterEvent$SelectEmployee;

    invoke-direct {v1, p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterEvent$SelectEmployee;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/rx1/Workflow;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method public final onClickFilterByEmployee()V
    .locals 2

    .line 94
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;->employeeFilterWorkflow:Lcom/squareup/workflow/rx1/Workflow;

    if-nez v0, :cond_0

    const-string v1, "employeeFilterWorkflow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget-object v1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterEvent$SelectFilterByEmployee;->INSTANCE:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterEvent$SelectFilterByEmployee;

    invoke-interface {v0, v1}, Lcom/squareup/workflow/rx1/Workflow;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;->employeeFilterReactor:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor;

    check-cast v0, Lcom/squareup/workflow/rx1/Reactor;

    sget-object v1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState$InitialState;->INSTANCE:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState$InitialState;

    invoke-static {v0, v1}, Lcom/squareup/workflow/rx1/ReactorKt;->startWorkflow(Lcom/squareup/workflow/rx1/Reactor;Ljava/lang/Object;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;->employeeFilterWorkflow:Lcom/squareup/workflow/rx1/Workflow;

    .line 49
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;->employeeFilterWorkflow:Lcom/squareup/workflow/rx1/Workflow;

    if-nez v0, :cond_0

    const-string v1, "employeeFilterWorkflow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0}, Lcom/squareup/workflow/rx1/Workflow;->getResult()Lrx/Single;

    move-result-object v0

    .line 50
    new-instance v1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner$onEnterScope$1;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Single;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "employeeFilterWorkflow.r\u2026FilterScreen>()\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-static {v0, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnExit(Lrx/Subscription;Lmortar/MortarScope;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public final screenData()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;",
            ">;"
        }
    .end annotation

    .line 69
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;->employeeFilterWorkflow:Lcom/squareup/workflow/rx1/Workflow;

    if-nez v0, :cond_0

    const-string v1, "employeeFilterWorkflow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0}, Lcom/squareup/workflow/rx1/Workflow;->getState()Lrx/Observable;

    move-result-object v0

    .line 70
    new-instance v1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner$screenData$1;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner$screenData$1;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->debounce(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 78
    sget-object v1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner$screenData$2;->INSTANCE:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner$screenData$2;

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    const-string v1, "employeeFilterWorkflow.s\u2026es)\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
