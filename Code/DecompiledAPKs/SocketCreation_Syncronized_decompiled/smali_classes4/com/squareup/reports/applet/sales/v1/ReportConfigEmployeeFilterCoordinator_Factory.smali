.class public final Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator_Factory;
.super Ljava/lang/Object;
.source "ReportConfigEmployeeFilterCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;",
            ">;)",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator_Factory;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;)Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;
    .locals 1

    .line 35
    new-instance v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;

    invoke-direct {v0, p0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;

    invoke-static {v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator_Factory;->newInstance(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;)Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator_Factory;->get()Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;

    move-result-object v0

    return-object v0
.end method
