.class public final Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner_Factory;
.super Ljava/lang/Object;
.source "ReportConfigEmployeeFilterRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final employeeFilterReactorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final reportConfigRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner_Factory;->reportConfigRunnerProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner_Factory;->employeeFilterReactorProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;)",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner_Factory;"
        }
    .end annotation

    .line 45
    new-instance v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;Lflow/Flow;Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor;Lrx/Scheduler;)Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;
    .locals 1

    .line 50
    new-instance v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;Lflow/Flow;Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor;Lrx/Scheduler;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;
    .locals 4

    .line 38
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner_Factory;->reportConfigRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflow/Flow;

    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner_Factory;->employeeFilterReactorProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor;

    iget-object v3, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lrx/Scheduler;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner_Factory;->newInstance(Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;Lflow/Flow;Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor;Lrx/Scheduler;)Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner_Factory;->get()Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;

    move-result-object v0

    return-object v0
.end method
