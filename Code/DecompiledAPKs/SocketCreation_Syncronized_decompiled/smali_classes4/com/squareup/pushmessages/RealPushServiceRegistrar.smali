.class public final Lcom/squareup/pushmessages/RealPushServiceRegistrar;
.super Ljava/lang/Object;
.source "RealPushServiceRegistrar.kt"

# interfaces
.implements Lcom/squareup/pushmessages/PushServiceRegistrar;
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/LoggedInScope;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/pushmessages/RealPushServiceRegistrar$UpdateRegistrationResult;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealPushServiceRegistrar.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealPushServiceRegistrar.kt\ncom/squareup/pushmessages/RealPushServiceRegistrar\n*L\n1#1,196:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000|\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u0002:\u0001,BY\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u000e\u0008\u0001\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u0012\u0008\u0008\u0001\u0010\n\u001a\u00020\u000b\u0012\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0008\u0008\u0001\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u00a2\u0006\u0002\u0010\u0015J\u0008\u0010\u0018\u001a\u00020\u0019H\u0002J\u0008\u0010\u001a\u001a\u00020\u001bH\u0002J\u0008\u0010\u001c\u001a\u00020\tH\u0016J\u0010\u0010\u001d\u001a\u00020\u00192\u0006\u0010\u001e\u001a\u00020\u001fH\u0016J\u0008\u0010 \u001a\u00020\u0019H\u0016J\u0008\u0010!\u001a\u00020\u001bH\u0002J\n\u0010\"\u001a\u0004\u0018\u00010#H\u0002J\u0012\u0010$\u001a\u00020\u00192\u0008\u0010\"\u001a\u0004\u0018\u00010#H\u0002J\u0008\u0010%\u001a\u00020\u0019H\u0003J\u000c\u0010&\u001a\u00020\u0019*\u00020\'H\u0002J\u0014\u0010(\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020*0)*\u00020+H\u0002R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0016\u001a\u00020\t8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0016\u0010\u0017R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/pushmessages/RealPushServiceRegistrar;",
        "Lcom/squareup/pushmessages/PushServiceRegistrar;",
        "Lmortar/Scoped;",
        "pushService",
        "Lcom/squareup/push/PushService;",
        "eventBus",
        "Lcom/squareup/badbus/BadEventSink;",
        "pushServiceEnabledPreference",
        "Lcom/f2prateek/rx/preferences2/Preference;",
        "",
        "appVersion",
        "",
        "pushServiceRegistrationSetting",
        "Lcom/squareup/settings/LocalSetting;",
        "Lcom/squareup/pushmessages/PushServiceRegistrationSettingValue;",
        "registerService",
        "Lcom/squareup/pushmessages/register/RegisterPushNotificationService;",
        "ioScheduler",
        "Lio/reactivex/Scheduler;",
        "pushServiceAvailability",
        "Lcom/squareup/push/PushServiceAvailability;",
        "(Lcom/squareup/push/PushService;Lcom/squareup/badbus/BadEventSink;Lcom/f2prateek/rx/preferences2/Preference;ILcom/squareup/settings/LocalSetting;Lcom/squareup/pushmessages/register/RegisterPushNotificationService;Lio/reactivex/Scheduler;Lcom/squareup/push/PushServiceAvailability;)V",
        "isRegistered",
        "()Z",
        "disablePushService",
        "",
        "enablePushService",
        "Lio/reactivex/disposables/Disposable;",
        "getPushServiceEnabled",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "register",
        "registrationToken",
        "",
        "storeRegistrationToken",
        "unregister",
        "onServerRegistrationUpdateResponse",
        "Lcom/squareup/pushmessages/RealPushServiceRegistrar$UpdateRegistrationResult$ServerRegistration;",
        "toServerRegistrationUpdateResult",
        "Lio/reactivex/Single;",
        "Lcom/squareup/pushmessages/RealPushServiceRegistrar$UpdateRegistrationResult;",
        "Lcom/squareup/push/PushServiceRegistration;",
        "UpdateRegistrationResult",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final appVersion:I

.field private final eventBus:Lcom/squareup/badbus/BadEventSink;

.field private final ioScheduler:Lio/reactivex/Scheduler;

.field private final pushService:Lcom/squareup/push/PushService;

.field private final pushServiceAvailability:Lcom/squareup/push/PushServiceAvailability;

.field private final pushServiceEnabledPreference:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final pushServiceRegistrationSetting:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/pushmessages/PushServiceRegistrationSettingValue;",
            ">;"
        }
    .end annotation
.end field

.field private final registerService:Lcom/squareup/pushmessages/register/RegisterPushNotificationService;


# direct methods
.method public constructor <init>(Lcom/squareup/push/PushService;Lcom/squareup/badbus/BadEventSink;Lcom/f2prateek/rx/preferences2/Preference;ILcom/squareup/settings/LocalSetting;Lcom/squareup/pushmessages/register/RegisterPushNotificationService;Lio/reactivex/Scheduler;Lcom/squareup/push/PushServiceAvailability;)V
    .locals 1
    .param p3    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/pushmessages/PushServiceEnabled;
        .end annotation
    .end param
    .param p4    # I
        .annotation runtime Lcom/squareup/util/PosSdkVersionCode;
        .end annotation
    .end param
    .param p7    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Rpc;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/push/PushService;",
            "Lcom/squareup/badbus/BadEventSink;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;I",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/pushmessages/PushServiceRegistrationSettingValue;",
            ">;",
            "Lcom/squareup/pushmessages/register/RegisterPushNotificationService;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/push/PushServiceAvailability;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "pushService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventBus"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pushServiceEnabledPreference"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pushServiceRegistrationSetting"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "registerService"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ioScheduler"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pushServiceAvailability"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->pushService:Lcom/squareup/push/PushService;

    iput-object p2, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->eventBus:Lcom/squareup/badbus/BadEventSink;

    iput-object p3, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->pushServiceEnabledPreference:Lcom/f2prateek/rx/preferences2/Preference;

    iput p4, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->appVersion:I

    iput-object p5, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->pushServiceRegistrationSetting:Lcom/squareup/settings/LocalSetting;

    iput-object p6, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->registerService:Lcom/squareup/pushmessages/register/RegisterPushNotificationService;

    iput-object p7, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->ioScheduler:Lio/reactivex/Scheduler;

    iput-object p8, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->pushServiceAvailability:Lcom/squareup/push/PushServiceAvailability;

    return-void
.end method

.method public static final synthetic access$getPushServiceEnabledPreference$p(Lcom/squareup/pushmessages/RealPushServiceRegistrar;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->pushServiceEnabledPreference:Lcom/f2prateek/rx/preferences2/Preference;

    return-object p0
.end method

.method public static final synthetic access$onServerRegistrationUpdateResponse(Lcom/squareup/pushmessages/RealPushServiceRegistrar;Lcom/squareup/pushmessages/RealPushServiceRegistrar$UpdateRegistrationResult$ServerRegistration;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1}, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->onServerRegistrationUpdateResponse(Lcom/squareup/pushmessages/RealPushServiceRegistrar$UpdateRegistrationResult$ServerRegistration;)V

    return-void
.end method

.method public static final synthetic access$storeRegistrationToken(Lcom/squareup/pushmessages/RealPushServiceRegistrar;Ljava/lang/String;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1}, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->storeRegistrationToken(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$toServerRegistrationUpdateResult(Lcom/squareup/pushmessages/RealPushServiceRegistrar;Lcom/squareup/push/PushServiceRegistration;)Lio/reactivex/Single;
    .locals 0

    .line 34
    invoke-direct {p0, p1}, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->toServerRegistrationUpdateResult(Lcom/squareup/push/PushServiceRegistration;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method private final disablePushService()V
    .locals 3

    .line 81
    invoke-virtual {p0}, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->getPushServiceEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->pushServiceEnabledPreference:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    .line 83
    invoke-direct {p0}, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->unregister()V

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->eventBus:Lcom/squareup/badbus/BadEventSink;

    new-instance v2, Lcom/squareup/pushmessages/FcmStateChangedEvent;

    invoke-direct {v2, v1}, Lcom/squareup/pushmessages/FcmStateChangedEvent;-><init>(Z)V

    invoke-interface {v0, v2}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    return-void
.end method

.method private final enablePushService()Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 64
    iget-object v0, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->pushServiceAvailability:Lcom/squareup/push/PushServiceAvailability;

    invoke-interface {v0}, Lcom/squareup/push/PushServiceAvailability;->isAvailable()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    new-array v0, v1, [Ljava/lang/Object;

    const-string v1, "[PUSH] The push services (e.g. GCM) is not available."

    .line 65
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 66
    invoke-static {}, Lio/reactivex/disposables/Disposables;->disposed()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "Disposables.disposed()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 68
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->getPushServiceEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    new-array v0, v1, [Ljava/lang/Object;

    const-string v1, "[PUSH] Already enabled. Registering to receive push token updates."

    .line 69
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 70
    iget-object v0, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->eventBus:Lcom/squareup/badbus/BadEventSink;

    new-instance v1, Lcom/squareup/pushmessages/FcmStateChangedEvent;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lcom/squareup/pushmessages/FcmStateChangedEvent;-><init>(Z)V

    invoke-interface {v0, v1}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    .line 77
    :cond_1
    invoke-direct {p0}, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->register()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method private final isRegistered()Z
    .locals 1

    .line 146
    invoke-direct {p0}, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->registrationToken()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final onServerRegistrationUpdateResponse(Lcom/squareup/pushmessages/RealPushServiceRegistrar$UpdateRegistrationResult$ServerRegistration;)V
    .locals 2

    .line 169
    invoke-virtual {p1}, Lcom/squareup/pushmessages/RealPushServiceRegistrar$UpdateRegistrationResult$ServerRegistration;->getResponse()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object v0

    .line 170
    instance-of v1, v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v1, :cond_0

    .line 171
    iget-object p1, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->pushServiceEnabledPreference:Lcom/f2prateek/rx/preferences2/Preference;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string v1, "[PUSH] Successfully registered with third-party server."

    .line 172
    invoke-static {v1, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 174
    iget-object p1, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->eventBus:Lcom/squareup/badbus/BadEventSink;

    new-instance v1, Lcom/squareup/pushmessages/FcmStateChangedEvent;

    invoke-direct {v1, v0}, Lcom/squareup/pushmessages/FcmStateChangedEvent;-><init>(Z)V

    invoke-interface {p1, v1}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 176
    :cond_0
    instance-of v0, v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/squareup/pushmessages/RealPushServiceRegistrar$UpdateRegistrationResult$ServerRegistration;->getResponse()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p1

    const-string v0, "pushService-registration"

    invoke-static {p1, v0}, Lcom/squareup/receiving/SuccessOrFailureLogger;->logFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private final register()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 89
    iget-object v0, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->pushService:Lcom/squareup/push/PushService;

    invoke-interface {v0}, Lcom/squareup/push/PushService;->enable()Lio/reactivex/Observable;

    move-result-object v0

    .line 90
    iget-object v1, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->ioScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 91
    sget-object v1, Lcom/squareup/pushmessages/RealPushServiceRegistrar$register$1;->INSTANCE:Lcom/squareup/pushmessages/RealPushServiceRegistrar$register$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 92
    new-instance v1, Lcom/squareup/pushmessages/RealPushServiceRegistrar$register$2;

    invoke-direct {v1, p0}, Lcom/squareup/pushmessages/RealPushServiceRegistrar$register$2;-><init>(Lcom/squareup/pushmessages/RealPushServiceRegistrar;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 93
    new-instance v1, Lcom/squareup/pushmessages/RealPushServiceRegistrar$register$3;

    invoke-direct {v1, p0}, Lcom/squareup/pushmessages/RealPushServiceRegistrar$register$3;-><init>(Lcom/squareup/pushmessages/RealPushServiceRegistrar;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "pushService.enable()\n   \u2026e()\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final registrationToken()Ljava/lang/String;
    .locals 4

    .line 124
    iget-object v0, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->pushServiceRegistrationSetting:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/pushmessages/PushServiceRegistrationSettingValue;

    iget-object v0, v0, Lcom/squareup/pushmessages/PushServiceRegistrationSettingValue;->token:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 126
    iget-object v2, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->pushServiceRegistrationSetting:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v2}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/pushmessages/PushServiceRegistrationSettingValue;

    iget v2, v2, Lcom/squareup/pushmessages/PushServiceRegistrationSettingValue;->appVersion:I

    .line 131
    iget v3, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->appVersion:I

    if-eq v2, v3, :cond_0

    .line 132
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[PUSH] App version changed. Registered: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", Current: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->appVersion:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v1

    :cond_0
    return-object v0

    .line 125
    :cond_1
    check-cast v1, Ljava/lang/String;

    return-object v1
.end method

.method private final storeRegistrationToken(Ljava/lang/String;)V
    .locals 3

    .line 140
    iget-object v0, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->pushServiceRegistrationSetting:Lcom/squareup/settings/LocalSetting;

    .line 141
    new-instance v1, Lcom/squareup/pushmessages/PushServiceRegistrationSettingValue;

    iget v2, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->appVersion:I

    invoke-direct {v1, p1, v2}, Lcom/squareup/pushmessages/PushServiceRegistrationSettingValue;-><init>(Ljava/lang/String;I)V

    .line 140
    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private final toServerRegistrationUpdateResult(Lcom/squareup/push/PushServiceRegistration;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/push/PushServiceRegistration;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/pushmessages/RealPushServiceRegistrar$UpdateRegistrationResult;",
            ">;"
        }
    .end annotation

    .line 151
    instance-of v0, p1, Lcom/squareup/push/PushServiceRegistration$Error;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/pushmessages/RealPushServiceRegistrar$UpdateRegistrationResult$PushServiceRegistrationError;

    check-cast p1, Lcom/squareup/push/PushServiceRegistration$Error;

    invoke-virtual {p1}, Lcom/squareup/push/PushServiceRegistration$Error;->getThrowable()Ljava/lang/Throwable;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/pushmessages/RealPushServiceRegistrar$UpdateRegistrationResult$PushServiceRegistrationError;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v0}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(PushServiceR\u2026strationError(throwable))"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 152
    :cond_0
    instance-of v0, p1, Lcom/squareup/push/PushServiceRegistration$Success;

    if-eqz v0, :cond_1

    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[PUSH] Retrieved push service registration id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    check-cast p1, Lcom/squareup/push/PushServiceRegistration$Success;

    invoke-virtual {p1}, Lcom/squareup/push/PushServiceRegistration$Success;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 158
    invoke-virtual {p1}, Lcom/squareup/push/PushServiceRegistration$Success;->getToken()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->storeRegistrationToken(Ljava/lang/String;)V

    .line 160
    iget-object v0, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->registerService:Lcom/squareup/pushmessages/register/RegisterPushNotificationService;

    .line 161
    new-instance v1, Lcom/squareup/pushmessages/register/UpdateRegistrationBody;

    invoke-virtual {p1}, Lcom/squareup/push/PushServiceRegistration$Success;->getToken()Ljava/lang/String;

    move-result-object p1

    iget-object v2, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->pushService:Lcom/squareup/push/PushService;

    invoke-interface {v2}, Lcom/squareup/push/PushService;->getPushGateway()Lcom/squareup/push/PushGateway;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Lcom/squareup/pushmessages/register/UpdateRegistrationBody;-><init>(Ljava/lang/String;Lcom/squareup/push/PushGateway;)V

    invoke-interface {v0, v1}, Lcom/squareup/pushmessages/register/RegisterPushNotificationService;->updateRegistrationId(Lcom/squareup/pushmessages/register/UpdateRegistrationBody;)Lcom/squareup/server/SimpleStandardResponse;

    move-result-object p1

    .line 162
    invoke-virtual {p1}, Lcom/squareup/server/SimpleStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 163
    sget-object v0, Lcom/squareup/pushmessages/RealPushServiceRegistrar$toServerRegistrationUpdateResult$1;->INSTANCE:Lcom/squareup/pushmessages/RealPushServiceRegistrar$toServerRegistrationUpdateResult$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "registerService\n        \u2026 ServerRegistration(it) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final unregister()V
    .locals 3

    .line 104
    iget-object v0, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->pushService:Lcom/squareup/push/PushService;

    invoke-interface {v0}, Lcom/squareup/push/PushService;->disable()Lio/reactivex/Completable;

    move-result-object v0

    .line 105
    iget-object v1, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->ioScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Completable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Completable;

    move-result-object v0

    .line 106
    new-instance v1, Lcom/squareup/pushmessages/RealPushServiceRegistrar$unregister$1;

    invoke-direct {v1, p0}, Lcom/squareup/pushmessages/RealPushServiceRegistrar$unregister$1;-><init>(Lcom/squareup/pushmessages/RealPushServiceRegistrar;)V

    check-cast v1, Lio/reactivex/functions/Action;

    .line 114
    sget-object v2, Lcom/squareup/pushmessages/RealPushServiceRegistrar$unregister$2;->INSTANCE:Lcom/squareup/pushmessages/RealPushServiceRegistrar$unregister$2;

    check-cast v2, Lio/reactivex/functions/Consumer;

    .line 106
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Completable;->subscribe(Lio/reactivex/functions/Action;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method


# virtual methods
.method public getPushServiceEnabled()Z
    .locals 3

    .line 54
    invoke-direct {p0}, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->isRegistered()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->pushServiceEnabledPreference:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-direct {p0}, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->enablePushService()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    .line 50
    invoke-direct {p0}, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->disablePushService()V

    return-void
.end method
