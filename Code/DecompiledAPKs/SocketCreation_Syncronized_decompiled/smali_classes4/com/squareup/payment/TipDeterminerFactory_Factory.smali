.class public final Lcom/squareup/payment/TipDeterminerFactory_Factory;
.super Ljava/lang/Object;
.source "TipDeterminerFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/TipDeterminerFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final paperSignatureSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderInEditProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/payment/TipDeterminerFactory_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/payment/TipDeterminerFactory_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/payment/TipDeterminerFactory_Factory;->paperSignatureSettingsProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/squareup/payment/TipDeterminerFactory_Factory;->transactionProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/TipDeterminerFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;)",
            "Lcom/squareup/payment/TipDeterminerFactory_Factory;"
        }
    .end annotation

    .line 45
    new-instance v0, Lcom/squareup/payment/TipDeterminerFactory_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/payment/TipDeterminerFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/payment/Transaction;)Lcom/squareup/payment/TipDeterminerFactory;
    .locals 1

    .line 50
    new-instance v0, Lcom/squareup/payment/TipDeterminerFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/payment/TipDeterminerFactory;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/payment/Transaction;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/payment/TipDeterminerFactory;
    .locals 4

    .line 38
    iget-object v0, p0, Lcom/squareup/payment/TipDeterminerFactory_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    iget-object v1, p0, Lcom/squareup/payment/TipDeterminerFactory_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/TenderInEdit;

    iget-object v2, p0, Lcom/squareup/payment/TipDeterminerFactory_Factory;->paperSignatureSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/papersignature/PaperSignatureSettings;

    iget-object v3, p0, Lcom/squareup/payment/TipDeterminerFactory_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/payment/Transaction;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/payment/TipDeterminerFactory_Factory;->newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/payment/Transaction;)Lcom/squareup/payment/TipDeterminerFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/payment/TipDeterminerFactory_Factory;->get()Lcom/squareup/payment/TipDeterminerFactory;

    move-result-object v0

    return-object v0
.end method
