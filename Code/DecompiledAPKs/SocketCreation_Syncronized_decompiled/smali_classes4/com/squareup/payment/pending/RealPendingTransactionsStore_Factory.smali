.class public final Lcom/squareup/payment/pending/RealPendingTransactionsStore_Factory;
.super Ljava/lang/Object;
.source "RealPendingTransactionsStore_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/pending/RealPendingTransactionsStore;",
        ">;"
    }
.end annotation


# instance fields
.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final countSmootherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;",
            ">;"
        }
    .end annotation
.end field

.field private final forwardedPaymentManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/ForwardedPaymentManager;",
            ">;"
        }
    .end annotation
.end field

.field private final localPaymentsMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field

.field private final pendingCapturesMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/sqlite/PendingCapturesMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final storedPaymentsMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/ForwardedPaymentManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/sqlite/PendingCapturesMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;",
            ">;)V"
        }
    .end annotation

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p2, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p3, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore_Factory;->resProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p4, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore_Factory;->forwardedPaymentManagerProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p5, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore_Factory;->storedPaymentsMonitorProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p6, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore_Factory;->pendingCapturesMonitorProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p7, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore_Factory;->localPaymentsMonitorProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p8, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p9, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore_Factory;->countSmootherProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/pending/RealPendingTransactionsStore_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/ForwardedPaymentManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/sqlite/PendingCapturesMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;",
            ">;)",
            "Lcom/squareup/payment/pending/RealPendingTransactionsStore_Factory;"
        }
    .end annotation

    .line 74
    new-instance v10, Lcom/squareup/payment/pending/RealPendingTransactionsStore_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/payment/pending/RealPendingTransactionsStore_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Lcom/squareup/thread/enforcer/ThreadEnforcer;Lio/reactivex/Scheduler;Lcom/squareup/util/Res;Lcom/squareup/payment/offline/ForwardedPaymentManager;Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;Lcom/squareup/queue/sqlite/PendingCapturesMonitor;Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;)Lcom/squareup/payment/pending/RealPendingTransactionsStore;
    .locals 11

    .line 82
    new-instance v10, Lcom/squareup/payment/pending/RealPendingTransactionsStore;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/payment/pending/RealPendingTransactionsStore;-><init>(Lcom/squareup/thread/enforcer/ThreadEnforcer;Lio/reactivex/Scheduler;Lcom/squareup/util/Res;Lcom/squareup/payment/offline/ForwardedPaymentManager;Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;Lcom/squareup/queue/sqlite/PendingCapturesMonitor;Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/payment/pending/RealPendingTransactionsStore;
    .locals 10

    .line 63
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore_Factory;->forwardedPaymentManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/payment/offline/ForwardedPaymentManager;

    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore_Factory;->storedPaymentsMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;

    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore_Factory;->pendingCapturesMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/queue/sqlite/PendingCapturesMonitor;

    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore_Factory;->localPaymentsMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;

    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore_Factory;->countSmootherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;

    invoke-static/range {v1 .. v9}, Lcom/squareup/payment/pending/RealPendingTransactionsStore_Factory;->newInstance(Lcom/squareup/thread/enforcer/ThreadEnforcer;Lio/reactivex/Scheduler;Lcom/squareup/util/Res;Lcom/squareup/payment/offline/ForwardedPaymentManager;Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;Lcom/squareup/queue/sqlite/PendingCapturesMonitor;Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;)Lcom/squareup/payment/pending/RealPendingTransactionsStore;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/payment/pending/RealPendingTransactionsStore_Factory;->get()Lcom/squareup/payment/pending/RealPendingTransactionsStore;

    move-result-object v0

    return-object v0
.end method
