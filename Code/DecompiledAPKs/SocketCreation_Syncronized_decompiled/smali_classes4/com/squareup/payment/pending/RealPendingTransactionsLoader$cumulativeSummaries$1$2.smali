.class final Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1$2;
.super Ljava/lang/Object;
.source "RealPendingTransactionsLoader.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1;->apply(Lcom/squareup/payment/pending/RequestType;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Ljava/util/List<",
        "+",
        "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u001a\u0010\u0002\u001a\u0016\u0012\u0004\u0012\u00020\u0004 \u0005*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1;


# direct methods
.method constructor <init>(Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1$2;->this$0:Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 60
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1$2;->accept(Ljava/util/List;)V

    return-void
.end method

.method public final accept(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;",
            ">;)V"
        }
    .end annotation

    .line 104
    iget-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1$2;->this$0:Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1;

    iget-object p1, p1, Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1;->this$0:Lcom/squareup/payment/pending/RealPendingTransactionsLoader;

    invoke-static {p1}, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->access$getLoadingState$p(Lcom/squareup/payment/pending/RealPendingTransactionsLoader;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    sget-object v0, Lcom/squareup/transactionhistory/TransactionsLoaderState;->LOADED:Lcom/squareup/transactionhistory/TransactionsLoaderState;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 105
    iget-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1$2;->this$0:Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1;

    iget-object p1, p1, Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1;->this$0:Lcom/squareup/payment/pending/RealPendingTransactionsLoader;

    invoke-static {p1}, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->access$getLoadingSequenceState$p(Lcom/squareup/payment/pending/RealPendingTransactionsLoader;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    sget-object v0, Lcom/squareup/transactionhistory/LoadingSequenceState;->FINISHED:Lcom/squareup/transactionhistory/LoadingSequenceState;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
