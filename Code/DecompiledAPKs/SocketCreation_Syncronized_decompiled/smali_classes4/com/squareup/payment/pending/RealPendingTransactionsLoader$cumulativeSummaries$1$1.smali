.class final Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1$1;
.super Ljava/lang/Object;
.source "RealPendingTransactionsLoader.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1;->apply(Lcom/squareup/payment/pending/RequestType;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;",
        "summaries",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1;


# direct methods
.method constructor <init>(Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1$1;->this$0:Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 60
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1$1;->apply(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final apply(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;",
            ">;"
        }
    .end annotation

    const-string v0, "summaries"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1$1;->this$0:Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1;

    iget-object v0, v0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1;->this$0:Lcom/squareup/payment/pending/RealPendingTransactionsLoader;

    invoke-static {v0}, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->access$getComparator$p(Lcom/squareup/payment/pending/RealPendingTransactionsLoader;)Ljava/util/Comparator;

    move-result-object v0

    invoke-static {}, Lcom/squareup/payment/pending/RealPendingTransactionsLoaderKt;->getNO_OP_COMPARATOR()Ljava/util/Comparator;

    move-result-object v1

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 101
    :cond_0
    check-cast p1, Ljava/lang/Iterable;

    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1$1;->this$0:Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1;

    iget-object v0, v0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1;->this$0:Lcom/squareup/payment/pending/RealPendingTransactionsLoader;

    invoke-static {v0}, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->access$getComparator$p(Lcom/squareup/payment/pending/RealPendingTransactionsLoader;)Ljava/util/Comparator;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p1

    :goto_0
    return-object p1
.end method
