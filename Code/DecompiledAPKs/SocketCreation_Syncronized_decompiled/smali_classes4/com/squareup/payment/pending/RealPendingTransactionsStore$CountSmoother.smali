.class public Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;
.super Ljava/lang/Object;
.source "RealPendingTransactionsStore.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/LoggedInScope;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/pending/RealPendingTransactionsStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CountSmoother"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealPendingTransactionsStore.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealPendingTransactionsStore.kt\ncom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother\n+ 2 RxKotlin.kt\ncom/squareup/util/rx2/Observables\n*L\n1#1,497:1\n148#2,4:498\n148#2,4:502\n*E\n*S KotlinDebug\n*F\n+ 1 RealPendingTransactionsStore.kt\ncom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother\n*L\n393#1,4:498\n408#1,4:502\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u001e\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0008\u0003\u0008\u0017\u0018\u00002\u00020\u0001B1\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJB\u0010\u001a\u001a\u00020\u000f2\u000c\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u001c2\u000c\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u001c2\u000c\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u001f2\u0006\u0010 \u001a\u00020\u000f2\u0006\u0010!\u001a\u00020\u000fH\u0002R\"\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e8\u0016X\u0097\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u0010\u0010\u0011\u001a\u0004\u0008\u0012\u0010\u0013R\"\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e8\u0016X\u0097\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u0015\u0010\u0011\u001a\u0004\u0008\u0016\u0010\u0013R\u0014\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;",
        "",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "forwardedPaymentManager",
        "Lcom/squareup/payment/offline/ForwardedPaymentManager;",
        "storedPaymentsMonitor",
        "Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;",
        "pendingCapturesMonitor",
        "Lcom/squareup/queue/sqlite/PendingCapturesMonitor;",
        "localPaymentsMonitor",
        "Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;",
        "(Lio/reactivex/Scheduler;Lcom/squareup/payment/offline/ForwardedPaymentManager;Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;Lcom/squareup/queue/sqlite/PendingCapturesMonitor;Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;)V",
        "allPendingTransactionsCount",
        "Lio/reactivex/Observable;",
        "",
        "allPendingTransactionsCount$annotations",
        "()V",
        "getAllPendingTransactionsCount",
        "()Lio/reactivex/Observable;",
        "ripenedAllPendingTransactionsCount",
        "ripenedAllPendingTransactionsCount$annotations",
        "getRipenedAllPendingTransactionsCount",
        "yetToBeForwardedStoredTransactionIds",
        "Ljava/util/LinkedHashSet;",
        "",
        "smoothedCount",
        "forwardedTransactionIds",
        "",
        "processedForwardedTransactionIds",
        "storedTransactionIds",
        "",
        "pendingCaptureTransactionsCount",
        "localPaymentTransactionsCount",
        "payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final allPendingTransactionsCount:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final ripenedAllPendingTransactionsCount:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final yetToBeForwardedStoredTransactionIds:Ljava/util/LinkedHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Scheduler;Lcom/squareup/payment/offline/ForwardedPaymentManager;Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;Lcom/squareup/queue/sqlite/PendingCapturesMonitor;Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;)V
    .locals 21
    .param p1    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "mainScheduler"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "forwardedPaymentManager"

    move-object/from16 v3, p2

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "storedPaymentsMonitor"

    move-object/from16 v4, p3

    invoke-static {v4, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "pendingCapturesMonitor"

    move-object/from16 v5, p4

    invoke-static {v5, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "localPaymentsMonitor"

    move-object/from16 v6, p5

    invoke-static {v6, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 384
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 393
    sget-object v2, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 394
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->allForwardedPaymentIds()Lio/reactivex/Observable;

    move-result-object v2

    const-string v7, "forwardedPaymentManager.allForwardedPaymentIds()"

    invoke-static {v2, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 395
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->onProcessedForwardedPaymentIds()Lio/reactivex/Observable;

    move-result-object v8

    .line 398
    invoke-static {}, Lcom/squareup/payment/pending/RealPendingTransactionsStoreKt;->access$getEMPTY_PROCESSED_FORWARDED_TRANSACTION_IDS$p()Ljava/util/List;

    move-result-object v9

    invoke-virtual {v8, v9}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v8

    const-string v9, "forwardedPaymentManager.\u2026ORWARDED_TRANSACTION_IDS)"

    invoke-static {v8, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 399
    invoke-interface/range {p3 .. p3}, Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;->allDistinctStoredPaymentIds()Lrx/Observable;

    move-result-object v10

    const-string v11, "storedPaymentsMonitor.al\u2026istinctStoredPaymentIds()"

    invoke-static {v10, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v10}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v10

    .line 400
    invoke-virtual {v10, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v10

    const-string v12, "storedPaymentsMonitor.al\u2026.observeOn(mainScheduler)"

    invoke-static {v10, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 401
    invoke-interface/range {p4 .. p4}, Lcom/squareup/queue/sqlite/PendingCapturesMonitor;->pendingCapturesCount()Lrx/Observable;

    move-result-object v13

    const-string v14, "pendingCapturesMonitor.pendingCapturesCount()"

    invoke-static {v13, v14}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v13}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v13

    .line 402
    invoke-interface/range {p5 .. p5}, Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;->localPaymentsCount()Lrx/Observable;

    move-result-object v14

    const-string v15, "localPaymentsMonitor.localPaymentsCount()"

    invoke-static {v14, v15}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v14}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v14

    .line 499
    move-object v15, v2

    check-cast v15, Lio/reactivex/ObservableSource;

    move-object/from16 v16, v8

    check-cast v16, Lio/reactivex/ObservableSource;

    move-object/from16 v17, v10

    check-cast v17, Lio/reactivex/ObservableSource;

    move-object/from16 v18, v13

    check-cast v18, Lio/reactivex/ObservableSource;

    move-object/from16 v19, v14

    check-cast v19, Lio/reactivex/ObservableSource;

    .line 500
    new-instance v2, Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother$$special$$inlined$combineLatest$1;

    invoke-direct {v2, v0}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother$$special$$inlined$combineLatest$1;-><init>(Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;)V

    move-object/from16 v20, v2

    check-cast v20, Lio/reactivex/functions/Function5;

    .line 498
    invoke-static/range {v15 .. v20}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function5;)Lio/reactivex/Observable;

    move-result-object v2

    const-string v8, "Observable.combineLatest\u2026t1, t2, t3, t4, t5) }\n  )"

    invoke-static {v2, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 405
    invoke-virtual {v2}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v2

    const-string v10, "Observables.combineLates\u2026  .distinctUntilChanged()"

    invoke-static {v2, v10}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v2, v0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;->allPendingTransactionsCount:Lio/reactivex/Observable;

    .line 408
    sget-object v2, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 409
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->allForwardedPaymentIds()Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {v2, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 410
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->onProcessedForwardedPaymentIds()Lio/reactivex/Observable;

    move-result-object v3

    .line 411
    invoke-static {}, Lcom/squareup/payment/pending/RealPendingTransactionsStoreKt;->access$getEMPTY_PROCESSED_FORWARDED_TRANSACTION_IDS$p()Ljava/util/List;

    move-result-object v7

    invoke-virtual {v3, v7}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v3

    invoke-static {v3, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 412
    invoke-interface/range {p3 .. p3}, Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;->allDistinctStoredPaymentIds()Lrx/Observable;

    move-result-object v4

    invoke-static {v4, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v4}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v4

    .line 413
    invoke-virtual {v4, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {v1, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 414
    invoke-interface/range {p4 .. p4}, Lcom/squareup/queue/sqlite/PendingCapturesMonitor;->ripenedPendingCapturesCount()Lrx/Observable;

    move-result-object v4

    const-string v5, "pendingCapturesMonitor.r\u2026nedPendingCapturesCount()"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v4}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v4

    .line 415
    invoke-interface/range {p5 .. p5}, Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;->ripenedLocalPaymentsCount()Lrx/Observable;

    move-result-object v5

    const-string v6, "localPaymentsMonitor.ripenedLocalPaymentsCount()"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v5}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v5

    .line 503
    move-object v11, v2

    check-cast v11, Lio/reactivex/ObservableSource;

    move-object v12, v3

    check-cast v12, Lio/reactivex/ObservableSource;

    move-object v13, v1

    check-cast v13, Lio/reactivex/ObservableSource;

    move-object v14, v4

    check-cast v14, Lio/reactivex/ObservableSource;

    move-object v15, v5

    check-cast v15, Lio/reactivex/ObservableSource;

    .line 504
    new-instance v1, Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother$$special$$inlined$combineLatest$2;

    invoke-direct {v1, v0}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother$$special$$inlined$combineLatest$2;-><init>(Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;)V

    move-object/from16 v16, v1

    check-cast v16, Lio/reactivex/functions/Function5;

    .line 502
    invoke-static/range {v11 .. v16}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function5;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {v1, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 418
    invoke-virtual {v1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {v1, v10}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, v0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;->ripenedAllPendingTransactionsCount:Lio/reactivex/Observable;

    .line 424
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v1, v0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;->yetToBeForwardedStoredTransactionIds:Ljava/util/LinkedHashSet;

    return-void
.end method

.method public static final synthetic access$smoothedCount(Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/List;II)I
    .locals 0

    .line 384
    invoke-direct/range {p0 .. p5}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;->smoothedCount(Ljava/util/Collection;Ljava/util/Collection;Ljava/util/List;II)I

    move-result p0

    return p0
.end method

.method public static synthetic allPendingTransactionsCount$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic ripenedAllPendingTransactionsCount$annotations()V
    .locals 0

    return-void
.end method

.method private final smoothedCount(Ljava/util/Collection;Ljava/util/Collection;Ljava/util/List;II)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;II)I"
        }
    .end annotation

    .line 434
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;->yetToBeForwardedStoredTransactionIds:Ljava/util/LinkedHashSet;

    check-cast v0, Ljava/util/Collection;

    check-cast p3, Ljava/lang/Iterable;

    invoke-static {v0, p3}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    .line 438
    iget-object p3, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;->yetToBeForwardedStoredTransactionIds:Ljava/util/LinkedHashSet;

    check-cast p3, Ljava/util/Collection;

    move-object v0, p1

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {p3, v0}, Lkotlin/collections/CollectionsKt;->removeAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    .line 443
    iget-object p3, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;->yetToBeForwardedStoredTransactionIds:Ljava/util/LinkedHashSet;

    check-cast p3, Ljava/util/Collection;

    check-cast p2, Ljava/lang/Iterable;

    invoke-static {p3, p2}, Lkotlin/collections/CollectionsKt;->removeAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    .line 445
    iget-object p2, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;->yetToBeForwardedStoredTransactionIds:Ljava/util/LinkedHashSet;

    invoke-virtual {p2}, Ljava/util/LinkedHashSet;->size()I

    move-result p2

    .line 446
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result p1

    add-int/2addr p2, p1

    add-int/2addr p2, p4

    add-int/2addr p2, p5

    return p2
.end method


# virtual methods
.method public getAllPendingTransactionsCount()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 392
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;->allPendingTransactionsCount:Lio/reactivex/Observable;

    return-object v0
.end method

.method public getRipenedAllPendingTransactionsCount()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 407
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;->ripenedAllPendingTransactionsCount:Lio/reactivex/Observable;

    return-object v0
.end method
