.class final Lcom/squareup/payment/pending/RealPendingTransactionsStore$onProcessedForwardedTransactionsAsBillHistory$1;
.super Ljava/lang/Object;
.source "RealPendingTransactionsStore.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/payment/pending/RealPendingTransactionsStore;->onProcessedForwardedTransactionsAsBillHistory()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealPendingTransactionsStore.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealPendingTransactionsStore.kt\ncom/squareup/payment/pending/RealPendingTransactionsStore$onProcessedForwardedTransactionsAsBillHistory$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,497:1\n1360#2:498\n1429#2,3:499\n*E\n*S KotlinDebug\n*F\n+ 1 RealPendingTransactionsStore.kt\ncom/squareup/payment/pending/RealPendingTransactionsStore$onProcessedForwardedTransactionsAsBillHistory$1\n*L\n307#1:498\n307#1,3:499\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u001f\n\u0002\u0018\u0002\n\u0002\u0010\u001e\n\u0000\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012(\u0010\u0004\u001a$\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00060\u0006 \u0003*\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00060\u00060\u00070\u0005H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/billhistory/model/BillHistory;",
        "kotlin.jvm.PlatformType",
        "list",
        "",
        "Lcom/squareup/payment/offline/ForwardedPayment;",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/payment/pending/RealPendingTransactionsStore;


# direct methods
.method constructor <init>(Lcom/squareup/payment/pending/RealPendingTransactionsStore;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$onProcessedForwardedTransactionsAsBillHistory$1;->this$0:Lcom/squareup/payment/pending/RealPendingTransactionsStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 54
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$onProcessedForwardedTransactionsAsBillHistory$1;->apply(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final apply(Ljava/util/Collection;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/payment/offline/ForwardedPayment;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;"
        }
    .end annotation

    const-string v0, "list"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 307
    check-cast p1, Ljava/lang/Iterable;

    .line 498
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 499
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 500
    check-cast v1, Lcom/squareup/payment/offline/ForwardedPayment;

    .line 307
    iget-object v2, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$onProcessedForwardedTransactionsAsBillHistory$1;->this$0:Lcom/squareup/payment/pending/RealPendingTransactionsStore;

    invoke-static {v2}, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->access$getRes$p(Lcom/squareup/payment/pending/RealPendingTransactionsStore;)Lcom/squareup/util/Res;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/payment/offline/ForwardedPayment;->asBill(Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 501
    :cond_0
    check-cast v0, Ljava/util/List;

    return-object v0
.end method
