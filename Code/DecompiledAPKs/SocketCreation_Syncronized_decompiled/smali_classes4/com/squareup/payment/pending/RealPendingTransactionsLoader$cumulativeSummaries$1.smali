.class final Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1;
.super Ljava/lang/Object;
.source "RealPendingTransactionsLoader.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/payment/pending/RealPendingTransactionsLoader;-><init>(Lcom/squareup/thread/enforcer/ThreadEnforcer;Lio/reactivex/Scheduler;Lcom/squareup/payment/pending/PendingTransactionsStore;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/ObservableSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a>\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0003 \u0004*\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00020\u0002 \u0004*\u001e\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0003 \u0004*\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "",
        "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;",
        "kotlin.jvm.PlatformType",
        "requestType",
        "Lcom/squareup/payment/pending/RequestType;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $transactionsStore:Lcom/squareup/payment/pending/PendingTransactionsStore;

.field final synthetic this$0:Lcom/squareup/payment/pending/RealPendingTransactionsLoader;


# direct methods
.method constructor <init>(Lcom/squareup/payment/pending/RealPendingTransactionsLoader;Lcom/squareup/payment/pending/PendingTransactionsStore;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1;->this$0:Lcom/squareup/payment/pending/RealPendingTransactionsLoader;

    iput-object p2, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1;->$transactionsStore:Lcom/squareup/payment/pending/PendingTransactionsStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/payment/pending/RequestType;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/pending/RequestType;",
            ")",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;",
            ">;>;"
        }
    .end annotation

    const-string v0, "requestType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    sget-object v0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/payment/pending/RequestType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v1, 0x2

    if-eq p1, v1, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 95
    iget-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1;->this$0:Lcom/squareup/payment/pending/RealPendingTransactionsLoader;

    invoke-static {p1}, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->access$getLoadingState$p(Lcom/squareup/payment/pending/RealPendingTransactionsLoader;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    sget-object v0, Lcom/squareup/transactionhistory/TransactionsLoaderState;->LOADING_INITIAL_REQUEST:Lcom/squareup/transactionhistory/TransactionsLoaderState;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 96
    iget-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1;->this$0:Lcom/squareup/payment/pending/RealPendingTransactionsLoader;

    invoke-static {p1}, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->access$getLoadingSequenceState$p(Lcom/squareup/payment/pending/RealPendingTransactionsLoader;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    sget-object v0, Lcom/squareup/transactionhistory/LoadingSequenceState;->ACTIVE:Lcom/squareup/transactionhistory/LoadingSequenceState;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 98
    iget-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1;->$transactionsStore:Lcom/squareup/payment/pending/PendingTransactionsStore;

    invoke-interface {p1}, Lcom/squareup/payment/pending/PendingTransactionsStore;->allTransactionSummaries()Lio/reactivex/Observable;

    move-result-object p1

    .line 99
    new-instance v0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1$1;

    invoke-direct {v0, p0}, Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1$1;-><init>(Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 103
    new-instance v0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1$2;

    invoke-direct {v0, p0}, Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1$2;-><init>(Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1;)V

    check-cast v0, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p1

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 88
    :cond_1
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    .line 91
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object p1

    .line 92
    invoke-virtual {p1}, Lio/reactivex/observables/ConnectableObservable;->refCount()Lio/reactivex/Observable;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/payment/pending/RequestType;

    invoke-virtual {p0, p1}, Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1;->apply(Lcom/squareup/payment/pending/RequestType;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
