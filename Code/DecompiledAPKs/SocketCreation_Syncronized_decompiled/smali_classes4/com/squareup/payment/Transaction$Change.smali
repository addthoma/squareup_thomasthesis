.class final enum Lcom/squareup/payment/Transaction$Change;
.super Ljava/lang/Enum;
.source "Transaction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/Transaction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "Change"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/payment/Transaction$Change;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/payment/Transaction$Change;

.field public static final enum ADD:Lcom/squareup/payment/Transaction$Change;

.field public static final enum DELETE:Lcom/squareup/payment/Transaction$Change;

.field public static final enum NO_OP:Lcom/squareup/payment/Transaction$Change;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 260
    new-instance v0, Lcom/squareup/payment/Transaction$Change;

    const/4 v1, 0x0

    const-string v2, "ADD"

    invoke-direct {v0, v2, v1}, Lcom/squareup/payment/Transaction$Change;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/Transaction$Change;->ADD:Lcom/squareup/payment/Transaction$Change;

    new-instance v0, Lcom/squareup/payment/Transaction$Change;

    const/4 v2, 0x1

    const-string v3, "DELETE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/payment/Transaction$Change;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/Transaction$Change;->DELETE:Lcom/squareup/payment/Transaction$Change;

    new-instance v0, Lcom/squareup/payment/Transaction$Change;

    const/4 v3, 0x2

    const-string v4, "NO_OP"

    invoke-direct {v0, v4, v3}, Lcom/squareup/payment/Transaction$Change;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/Transaction$Change;->NO_OP:Lcom/squareup/payment/Transaction$Change;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/payment/Transaction$Change;

    .line 259
    sget-object v4, Lcom/squareup/payment/Transaction$Change;->ADD:Lcom/squareup/payment/Transaction$Change;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/payment/Transaction$Change;->DELETE:Lcom/squareup/payment/Transaction$Change;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/payment/Transaction$Change;->NO_OP:Lcom/squareup/payment/Transaction$Change;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/payment/Transaction$Change;->$VALUES:[Lcom/squareup/payment/Transaction$Change;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 259
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/payment/Transaction$Change;
    .locals 1

    .line 259
    const-class v0, Lcom/squareup/payment/Transaction$Change;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/payment/Transaction$Change;

    return-object p0
.end method

.method public static values()[Lcom/squareup/payment/Transaction$Change;
    .locals 1

    .line 259
    sget-object v0, Lcom/squareup/payment/Transaction$Change;->$VALUES:[Lcom/squareup/payment/Transaction$Change;

    invoke-virtual {v0}, [Lcom/squareup/payment/Transaction$Change;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/payment/Transaction$Change;

    return-object v0
.end method
