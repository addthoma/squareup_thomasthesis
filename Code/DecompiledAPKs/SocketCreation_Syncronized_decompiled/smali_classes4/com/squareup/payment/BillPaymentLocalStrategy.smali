.class public Lcom/squareup/payment/BillPaymentLocalStrategy;
.super Ljava/lang/Object;
.source "BillPaymentLocalStrategy.java"

# interfaces
.implements Lcom/squareup/payment/BillPayment$BillPaymentStrategy;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;
    }
.end annotation


# instance fields
.field private addTendersRequest:Lcom/squareup/protos/client/bills/AddTendersRequest;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final localPaymentsQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

.field private final merchantToken:Ljava/lang/String;

.field private final taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;


# direct methods
.method private constructor <init>(Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;)V
    .locals 1

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    invoke-static {p1}, Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;->access$000(Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;)Lcom/squareup/queue/retrofit/RetrofitQueue;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/BillPaymentLocalStrategy;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    .line 49
    invoke-static {p1}, Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;->access$100(Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;)Lcom/squareup/queue/retrofit/RetrofitQueue;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/BillPaymentLocalStrategy;->localPaymentsQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    .line 50
    invoke-static {p1}, Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;->access$200(Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/BillPaymentLocalStrategy;->merchantToken:Ljava/lang/String;

    .line 51
    invoke-static {p1}, Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;->access$300(Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;)Lcom/squareup/settings/server/Features;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/BillPaymentLocalStrategy;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;Lcom/squareup/payment/BillPaymentLocalStrategy$1;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1}, Lcom/squareup/payment/BillPaymentLocalStrategy;-><init>(Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;)V

    return-void
.end method

.method private requiresSynchronousAuthorization(Lcom/squareup/protos/client/bills/AddTendersRequest;)Z
    .locals 4

    .line 160
    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/Itemization;

    .line 161
    iget-object v3, v1, Lcom/squareup/protos/client/bills/Itemization;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    if-eqz v3, :cond_0

    iget-object v3, v1, Lcom/squareup/protos/client/bills/Itemization;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item:Lcom/squareup/api/items/Item;

    if-eqz v3, :cond_0

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item:Lcom/squareup/api/items/Item;

    iget-object v1, v1, Lcom/squareup/api/items/Item;->type:Lcom/squareup/api/items/Item$Type;

    sget-object v3, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    if-ne v1, v3, :cond_0

    return v2

    .line 168
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    if-eqz v0, :cond_2

    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->invoice:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;

    if-eqz p1, :cond_2

    return v2

    :cond_2
    const/4 p1, 0x0

    return p1
.end method


# virtual methods
.method public addTenders(Lcom/squareup/protos/client/bills/AddTendersRequest;)Lcom/squareup/protos/client/bills/AddTendersResponse;
    .locals 4

    .line 68
    iget-object v0, p0, Lcom/squareup/payment/BillPaymentLocalStrategy;->addTendersRequest:Lcom/squareup/protos/client/bills/AddTendersRequest;

    if-nez v0, :cond_2

    .line 72
    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddTendersRequest;->add_tender:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 79
    invoke-direct {p0, p1}, Lcom/squareup/payment/BillPaymentLocalStrategy;->requiresSynchronousAuthorization(Lcom/squareup/protos/client/bills/AddTendersRequest;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 83
    iput-object p1, p0, Lcom/squareup/payment/BillPaymentLocalStrategy;->addTendersRequest:Lcom/squareup/protos/client/bills/AddTendersRequest;

    .line 85
    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddTendersRequest;->add_tender:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/AddTender;

    .line 86
    new-instance v2, Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;-><init>()V

    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 87
    invoke-virtual {v2, p1}, Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;->bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;

    move-result-object p1

    new-instance v2, Lcom/squareup/protos/client/Status$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/Status$Builder;-><init>()V

    .line 88
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/Status$Builder;->success(Ljava/lang/Boolean;)Lcom/squareup/protos/client/Status$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/protos/client/Status$Builder;->build()Lcom/squareup/protos/client/Status;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;->status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;

    move-result-object p1

    new-instance v2, Lcom/squareup/protos/client/bills/AddedTender$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/bills/AddedTender$Builder;-><init>()V

    new-instance v3, Lcom/squareup/protos/client/Status$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/client/Status$Builder;-><init>()V

    .line 90
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/squareup/protos/client/Status$Builder;->success(Ljava/lang/Boolean;)Lcom/squareup/protos/client/Status$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/client/Status$Builder;->build()Lcom/squareup/protos/client/Status;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/squareup/protos/client/bills/AddedTender$Builder;->status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/bills/AddedTender$Builder;

    move-result-object v1

    iget-object v0, v0, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    .line 91
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/bills/AddedTender$Builder;->tender(Lcom/squareup/protos/client/bills/Tender;)Lcom/squareup/protos/client/bills/AddedTender$Builder;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/AddedTender$Builder;->build()Lcom/squareup/protos/client/bills/AddedTender;

    move-result-object v0

    .line 89
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;->tender(Ljava/util/List;)Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;

    move-result-object p1

    .line 93
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;->build()Lcom/squareup/protos/client/bills/AddTendersResponse;

    move-result-object p1

    return-object p1

    .line 80
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Payment cannot be processed in the background"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 73
    :cond_1
    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddTendersRequest;->add_tender:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    .line 74
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected exactly one tender, but request contained "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "addTenders already called"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public canExitStrategy()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public doCapture(Lcom/squareup/payment/BillPayment;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    .line 98
    iget-object v0, p0, Lcom/squareup/payment/BillPaymentLocalStrategy;->addTendersRequest:Lcom/squareup/protos/client/bills/AddTendersRequest;

    if-eqz v0, :cond_2

    .line 102
    invoke-virtual {p1, p2, v0}, Lcom/squareup/payment/BillPayment;->getLocalAddAndCaptureTask(ZLcom/squareup/protos/client/bills/AddTendersRequest;)Lcom/squareup/queue/LocalPaymentsQueueTask;

    move-result-object p1

    .line 104
    iget-object p2, p0, Lcom/squareup/payment/BillPaymentLocalStrategy;->features:Lcom/squareup/settings/server/Features;

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_SEPARATE_QUEUE_FOR_LOCAL_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p2, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p2

    const/4 v0, 0x1

    if-eqz p2, :cond_0

    .line 106
    invoke-interface {p1, v0}, Lcom/squareup/queue/LocalPaymentsQueueTask;->setAddedToTheLocalPaymentsQueue(Z)V

    const/4 p2, 0x0

    .line 107
    invoke-interface {p1, p2}, Lcom/squareup/queue/LocalPaymentsQueueTask;->setOnTheTaskQueue(Z)V

    .line 108
    iget-object p2, p0, Lcom/squareup/payment/BillPaymentLocalStrategy;->localPaymentsQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    invoke-interface {p2, p1}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    .line 113
    :cond_0
    iget-object p2, p0, Lcom/squareup/payment/BillPaymentLocalStrategy;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DO_NOT_USE_TASK_QUEUE_FOR_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p2, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p2

    if-nez p2, :cond_1

    .line 114
    invoke-interface {p1, v0}, Lcom/squareup/queue/LocalPaymentsQueueTask;->setOnTheTaskQueue(Z)V

    .line 115
    iget-object p2, p0, Lcom/squareup/payment/BillPaymentLocalStrategy;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    invoke-interface {p2, p1}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    :cond_1
    return-void

    .line 99
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Attempt to capture without adding tender first"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public enqueueAttachContactTask(Lcom/squareup/payment/tender/BaseTender;)V
    .locals 3

    .line 148
    invoke-virtual {p1}, Lcom/squareup/payment/tender/BaseTender;->getCustomerId()Ljava/lang/String;

    move-result-object p1

    .line 149
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/squareup/payment/BillPaymentLocalStrategy;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    new-instance v1, Lcom/squareup/queue/crm/AttachContactPostPaymentTask;

    iget-object v2, p0, Lcom/squareup/payment/BillPaymentLocalStrategy;->merchantToken:Ljava/lang/String;

    invoke-direct {v1, p1, v2}, Lcom/squareup/queue/crm/AttachContactPostPaymentTask;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public enqueueLastReceipt(Lcom/squareup/payment/BillPayment;Lcom/squareup/payment/tender/BaseTender;)V
    .locals 2

    .line 120
    invoke-virtual {p2}, Lcom/squareup/payment/tender/BaseTender;->getReceiptDestinationType()Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 125
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/payment/tender/BaseTender;->getReceiptDestination()Ljava/lang/String;

    move-result-object p2

    .line 126
    sget-object v0, Lcom/squareup/payment/BillPaymentLocalStrategy$1;->$SwitchMap$com$squareup$payment$tender$BaseTender$ReceiptDestinationType:[I

    invoke-virtual {p1}, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_5

    const/4 v1, 0x2

    if-eq v0, v1, :cond_4

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 p2, 0x5

    if-ne v0, p2, :cond_1

    .line 140
    iget-object p1, p0, Lcom/squareup/payment/BillPaymentLocalStrategy;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    new-instance p2, Lcom/squareup/queue/EnqueueDeclineReceipt;

    invoke-direct {p2}, Lcom/squareup/queue/EnqueueDeclineReceipt;-><init>()V

    invoke-interface {p1, p2}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 143
    :cond_1
    new-instance p2, Ljava/lang/RuntimeException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unrecognized receipt type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 137
    :cond_2
    iget-object p1, p0, Lcom/squareup/payment/BillPaymentLocalStrategy;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    new-instance v0, Lcom/squareup/queue/EnqueueEmailReceiptById;

    invoke-direct {v0, p2}, Lcom/squareup/queue/EnqueueEmailReceiptById;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 134
    :cond_3
    iget-object p1, p0, Lcom/squareup/payment/BillPaymentLocalStrategy;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    new-instance v0, Lcom/squareup/queue/EnqueueSmsReceiptById;

    invoke-direct {v0, p2}, Lcom/squareup/queue/EnqueueSmsReceiptById;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 131
    :cond_4
    iget-object p1, p0, Lcom/squareup/payment/BillPaymentLocalStrategy;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    new-instance v0, Lcom/squareup/queue/EnqueueEmailReceipt;

    const-string v1, "address"

    invoke-static {p2, v1}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    invoke-direct {v0, p2}, Lcom/squareup/queue/EnqueueEmailReceipt;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 128
    :cond_5
    iget-object p1, p0, Lcom/squareup/payment/BillPaymentLocalStrategy;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    new-instance v0, Lcom/squareup/queue/EnqueueSmsReceipt;

    const-string v1, "phone"

    invoke-static {p2, v1}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    invoke-direct {v0, p2}, Lcom/squareup/queue/EnqueueSmsReceipt;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public hasOfflineTenders()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onDropTender(Lcom/squareup/payment/tender/BaseTender;)V
    .locals 1

    .line 64
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Background tenders cannot be dropped."

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
