.class public Lcom/squareup/payment/PaymentCapturer;
.super Ljava/lang/Object;
.source "PaymentCapturer.java"


# instance fields
.field private final autoCaptureControl:Lcom/squareup/autocapture/AutoCaptureControl;

.field private final completedCapture:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentAccuracyLogger:Lcom/squareup/payment/PaymentAccuracyLogger;

.field private final printerStations:Lcom/squareup/print/PrinterStations;

.field private final ticketAutoIdentifiers:Lcom/squareup/print/TicketAutoIdentifiers;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method constructor <init>(Lcom/squareup/payment/Transaction;Lcom/squareup/print/PrinterStations;Lcom/squareup/autocapture/AutoCaptureControl;Lcom/squareup/print/TicketAutoIdentifiers;Lcom/squareup/settings/LocalSetting;Lcom/squareup/payment/PaymentAccuracyLogger;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/print/PrinterStations;",
            "Lcom/squareup/autocapture/AutoCaptureControl;",
            "Lcom/squareup/print/TicketAutoIdentifiers;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/payment/PaymentAccuracyLogger;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/payment/PaymentCapturer;->transaction:Lcom/squareup/payment/Transaction;

    .line 26
    iput-object p2, p0, Lcom/squareup/payment/PaymentCapturer;->printerStations:Lcom/squareup/print/PrinterStations;

    .line 27
    iput-object p3, p0, Lcom/squareup/payment/PaymentCapturer;->autoCaptureControl:Lcom/squareup/autocapture/AutoCaptureControl;

    .line 28
    iput-object p4, p0, Lcom/squareup/payment/PaymentCapturer;->ticketAutoIdentifiers:Lcom/squareup/print/TicketAutoIdentifiers;

    .line 29
    iput-object p5, p0, Lcom/squareup/payment/PaymentCapturer;->completedCapture:Lcom/squareup/settings/LocalSetting;

    .line 30
    iput-object p6, p0, Lcom/squareup/payment/PaymentCapturer;->paymentAccuracyLogger:Lcom/squareup/payment/PaymentAccuracyLogger;

    return-void
.end method


# virtual methods
.method public capture(Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    .line 42
    iget-object v0, p0, Lcom/squareup/payment/PaymentCapturer;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/payment/PaymentCapturer;->printerStations:Lcom/squareup/print/PrinterStations;

    invoke-interface {v0}, Lcom/squareup/print/PrinterStations;->isTicketAutoNumberingEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/squareup/payment/PaymentCapturer;->ticketAutoIdentifiers:Lcom/squareup/print/TicketAutoIdentifiers;

    invoke-interface {v0}, Lcom/squareup/print/TicketAutoIdentifiers;->cancelTicketIdentifierRequest()V

    .line 45
    iget-object v0, p0, Lcom/squareup/payment/PaymentCapturer;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getDefaultOrderName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Transaction;->setOrderTicketName(Ljava/lang/String;)V

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/PaymentCapturer;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->ensurePaymentOrderTicketNameUpToDate()V

    .line 50
    iget-object v0, p0, Lcom/squareup/payment/PaymentCapturer;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->saveGrandTotalToDisplayOnBuyerDisconnect()V

    .line 52
    iget-object v0, p0, Lcom/squareup/payment/PaymentCapturer;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requirePayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    .line 53
    invoke-virtual {v0, p1}, Lcom/squareup/payment/Payment;->capture(Z)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 56
    iget-object p1, p0, Lcom/squareup/payment/PaymentCapturer;->autoCaptureControl:Lcom/squareup/autocapture/AutoCaptureControl;

    invoke-virtual {p1}, Lcom/squareup/autocapture/AutoCaptureControl;->stop()V

    .line 57
    iget-object p1, p0, Lcom/squareup/payment/PaymentCapturer;->completedCapture:Lcom/squareup/settings/LocalSetting;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 59
    :cond_1
    iget-object p1, p0, Lcom/squareup/payment/PaymentCapturer;->paymentAccuracyLogger:Lcom/squareup/payment/PaymentAccuracyLogger;

    invoke-virtual {p1, v0}, Lcom/squareup/payment/PaymentAccuracyLogger;->logReceiptAccuracyEvent(Lcom/squareup/payment/Payment;)V

    return-void
.end method
