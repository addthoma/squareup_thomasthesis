.class public final Lcom/squareup/payment/ledger/LedgerDiagnosticsReporter_Factory;
.super Ljava/lang/Object;
.source "LedgerDiagnosticsReporter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/ledger/LedgerDiagnosticsReporter;",
        ">;"
    }
.end annotation


# instance fields
.field private final ohSnapLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionLedgerManagerFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/payment/ledger/LedgerDiagnosticsReporter_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/payment/ledger/LedgerDiagnosticsReporter_Factory;->transactionLedgerManagerFactoryProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/payment/ledger/LedgerDiagnosticsReporter_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/ledger/LedgerDiagnosticsReporter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;)",
            "Lcom/squareup/payment/ledger/LedgerDiagnosticsReporter_Factory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/payment/ledger/LedgerDiagnosticsReporter_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/payment/ledger/LedgerDiagnosticsReporter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;Lcom/squareup/log/OhSnapLogger;)Lcom/squareup/payment/ledger/LedgerDiagnosticsReporter;
    .locals 1

    .line 46
    new-instance v0, Lcom/squareup/payment/ledger/LedgerDiagnosticsReporter;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/payment/ledger/LedgerDiagnosticsReporter;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;Lcom/squareup/log/OhSnapLogger;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/payment/ledger/LedgerDiagnosticsReporter;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/payment/ledger/LedgerDiagnosticsReporter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, p0, Lcom/squareup/payment/ledger/LedgerDiagnosticsReporter_Factory;->transactionLedgerManagerFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;

    iget-object v2, p0, Lcom/squareup/payment/ledger/LedgerDiagnosticsReporter_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/log/OhSnapLogger;

    invoke-static {v0, v1, v2}, Lcom/squareup/payment/ledger/LedgerDiagnosticsReporter_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;Lcom/squareup/log/OhSnapLogger;)Lcom/squareup/payment/ledger/LedgerDiagnosticsReporter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/payment/ledger/LedgerDiagnosticsReporter_Factory;->get()Lcom/squareup/payment/ledger/LedgerDiagnosticsReporter;

    move-result-object v0

    return-object v0
.end method
