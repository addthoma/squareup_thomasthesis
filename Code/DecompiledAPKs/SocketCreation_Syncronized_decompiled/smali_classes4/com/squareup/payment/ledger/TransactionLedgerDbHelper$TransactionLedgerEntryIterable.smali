.class public interface abstract Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$TransactionLedgerEntryIterable;
.super Ljava/lang/Object;
.source "TransactionLedgerDbHelper.java"

# interfaces
.implements Ljava/lang/Iterable;
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TransactionLedgerEntryIterable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable<",
        "Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;",
        ">;",
        "Ljava/io/Closeable;"
    }
.end annotation
