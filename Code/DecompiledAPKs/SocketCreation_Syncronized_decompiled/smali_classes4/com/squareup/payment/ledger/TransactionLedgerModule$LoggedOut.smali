.class public Lcom/squareup/payment/ledger/TransactionLedgerModule$LoggedOut;
.super Ljava/lang/Object;
.source "TransactionLedgerModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/ledger/TransactionLedgerModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LoggedOut"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$provideFactory$0(Landroid/app/Application;Lcom/google/gson/Gson;Ljava/io/File;Lcom/squareup/util/Clock;Ljava/util/concurrent/Executor;Lcom/squareup/settings/server/Features;Lcom/squareup/payment/ledger/TransactionLedgerUploader;Ljava/lang/String;)Lcom/squareup/payment/ledger/TransactionLedgerManager;
    .locals 10

    .line 27
    new-instance v9, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p7

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    move-object/from16 v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;-><init>(Landroid/app/Application;Lcom/google/gson/Gson;Ljava/io/File;Ljava/lang/String;Lcom/squareup/util/Clock;Ljava/util/concurrent/Executor;Lcom/squareup/settings/server/Features;Lcom/squareup/payment/ledger/TransactionLedgerUploader;)V

    return-object v9
.end method

.method static provideFactory(Landroid/app/Application;Lcom/squareup/util/Clock;Ljava/io/File;Ljava/util/concurrent/Executor;Lcom/google/gson/Gson;Lcom/squareup/settings/server/Features;Lcom/squareup/payment/ledger/TransactionLedgerUploader;)Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;
    .locals 9
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .param p4    # Lcom/google/gson/Gson;
        .annotation runtime Lcom/squareup/gson/WireGson;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 27
    new-instance v8, Lcom/squareup/payment/ledger/-$$Lambda$TransactionLedgerModule$LoggedOut$X1FblijWje72z2yfSBY9nAivxzY;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p4

    move-object v3, p2

    move-object v4, p1

    move-object v5, p3

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/payment/ledger/-$$Lambda$TransactionLedgerModule$LoggedOut$X1FblijWje72z2yfSBY9nAivxzY;-><init>(Landroid/app/Application;Lcom/google/gson/Gson;Ljava/io/File;Lcom/squareup/util/Clock;Ljava/util/concurrent/Executor;Lcom/squareup/settings/server/Features;Lcom/squareup/payment/ledger/TransactionLedgerUploader;)V

    return-object v8
.end method
