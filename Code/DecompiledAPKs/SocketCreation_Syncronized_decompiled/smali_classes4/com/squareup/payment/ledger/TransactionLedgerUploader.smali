.class public Lcom/squareup/payment/ledger/TransactionLedgerUploader;
.super Ljava/lang/Object;
.source "TransactionLedgerUploader.java"


# static fields
.field private static final MAX_UPLOAD_TRIES:I = 0x3

.field private static final RETRY_DELAY_SECONDS:I = 0x2

.field private static final TRANSACTION_LEDGER_ENDPOINT:Ljava/lang/String; = "1.0/transaction-ledger/upload"


# instance fields
.field private final client:Lokhttp3/OkHttpClient;

.field private final ioScheduler:Lio/reactivex/Scheduler;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final res:Lcom/squareup/util/Res;

.field private final server:Lcom/squareup/http/Server;

.field private final transactionLedgerService:Lcom/squareup/server/transaction_ledger/TransactionLedgerService;


# direct methods
.method constructor <init>(Lcom/squareup/server/transaction_ledger/TransactionLedgerService;Lcom/squareup/util/Res;Lcom/squareup/http/Server;Lokhttp3/OkHttpClient;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;)V
    .locals 0
    .param p5    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p6    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Rpc;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/squareup/payment/ledger/TransactionLedgerUploader;->transactionLedgerService:Lcom/squareup/server/transaction_ledger/TransactionLedgerService;

    .line 58
    iput-object p2, p0, Lcom/squareup/payment/ledger/TransactionLedgerUploader;->res:Lcom/squareup/util/Res;

    .line 59
    iput-object p3, p0, Lcom/squareup/payment/ledger/TransactionLedgerUploader;->server:Lcom/squareup/http/Server;

    .line 60
    iput-object p4, p0, Lcom/squareup/payment/ledger/TransactionLedgerUploader;->client:Lokhttp3/OkHttpClient;

    .line 61
    iput-object p5, p0, Lcom/squareup/payment/ledger/TransactionLedgerUploader;->mainScheduler:Lio/reactivex/Scheduler;

    .line 62
    iput-object p6, p0, Lcom/squareup/payment/ledger/TransactionLedgerUploader;->ioScheduler:Lio/reactivex/Scheduler;

    return-void
.end method

.method private createUploadLedgerRequest(Ljava/util/List;Lokio/ByteString;)Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;",
            ">;",
            "Lokio/ByteString;",
            ")",
            "Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;"
        }
    .end annotation

    .line 104
    new-instance v0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;-><init>()V

    .line 105
    invoke-direct {p0}, Lcom/squareup/payment/ledger/TransactionLedgerUploader;->randomId()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;->id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;

    move-result-object v0

    .line 106
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;->entries(Ljava/util/List;)Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;

    move-result-object p1

    .line 107
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;->diagnostics_data(Lokio/ByteString;)Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;

    move-result-object p1

    .line 108
    invoke-virtual {p1}, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;->build()Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;

    move-result-object p1

    return-object p1
.end method

.method private doUploadLedgerStream(Lcom/squareup/payment/ledger/UploadTransactionLedgerRequestBody;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/ledger/UploadTransactionLedgerRequestBody;",
            ")",
            "Lio/reactivex/Observable<",
            "Lokhttp3/Response;",
            ">;"
        }
    .end annotation

    .line 118
    new-instance v0, Lcom/squareup/payment/ledger/-$$Lambda$TransactionLedgerUploader$_ewA60n6ieQHA_KXhYVExVsnK1Q;

    invoke-direct {v0, p0, p1}, Lcom/squareup/payment/ledger/-$$Lambda$TransactionLedgerUploader$_ewA60n6ieQHA_KXhYVExVsnK1Q;-><init>(Lcom/squareup/payment/ledger/TransactionLedgerUploader;Lcom/squareup/payment/ledger/UploadTransactionLedgerRequestBody;)V

    invoke-static {v0}, Lio/reactivex/Observable;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerUploader;->ioScheduler:Lio/reactivex/Scheduler;

    .line 128
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerUploader;->mainScheduler:Lio/reactivex/Scheduler;

    .line 129
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method static synthetic lambda$uploadLedger$0(Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerResponse;)Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 80
    new-instance v0, Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v1, v1, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    .line 81
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 82
    invoke-virtual {p0}, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerResponse;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, v1, p0}, Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;-><init>(ZLjava/lang/String;)V

    return-object v0
.end method

.method static synthetic lambda$uploadLedger$1(Lokhttp3/Response;)Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 95
    :try_start_0
    new-instance v0, Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;

    invoke-virtual {p0}, Lokhttp3/Response;->isSuccessful()Z

    move-result v1

    invoke-virtual {p0}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object p0

    invoke-virtual {p0}, Lokhttp3/ResponseBody;->string()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, v1, p0}, Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;-><init>(ZLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception p0

    .line 97
    invoke-static {p0}, Lrx/exceptions/Exceptions;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object p0

    throw p0
.end method

.method private randomId()Lcom/squareup/protos/client/IdPair;
    .locals 2

    .line 138
    new-instance v0, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/IdPair$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    return-object v0
.end method

.method private uploadEmptyLedger()Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;",
            ">;"
        }
    .end annotation

    .line 133
    iget-object v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerUploader;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/transaction/R$string;->upload_transaction_ledger_empty:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 134
    new-instance v1, Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v0}, Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;-><init>(ZLjava/lang/String;)V

    invoke-static {v1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public synthetic lambda$doUploadLedgerStream$2$TransactionLedgerUploader(Lcom/squareup/payment/ledger/UploadTransactionLedgerRequestBody;)Lokhttp3/Response;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 119
    new-instance v0, Lokhttp3/Request$Builder;

    invoke-direct {v0}, Lokhttp3/Request$Builder;-><init>()V

    .line 120
    invoke-virtual {p0}, Lcom/squareup/payment/ledger/TransactionLedgerUploader;->transactionLedgerEndpoint()Lokhttp3/HttpUrl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/Request$Builder;->url(Lokhttp3/HttpUrl;)Lokhttp3/Request$Builder;

    move-result-object v0

    .line 121
    invoke-virtual {v0, p1}, Lokhttp3/Request$Builder;->post(Lokhttp3/RequestBody;)Lokhttp3/Request$Builder;

    move-result-object p1

    .line 122
    invoke-virtual {p1}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object p1

    .line 124
    :try_start_0
    iget-object v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerUploader;->client:Lokhttp3/OkHttpClient;

    invoke-virtual {v0, p1}, Lokhttp3/OkHttpClient;->newCall(Lokhttp3/Request;)Lokhttp3/Call;

    move-result-object p1

    invoke-interface {p1}, Lokhttp3/Call;->execute()Lokhttp3/Response;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 126
    invoke-static {p1}, Lrx/exceptions/Exceptions;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object p1

    throw p1
.end method

.method transactionLedgerEndpoint()Lokhttp3/HttpUrl;
    .locals 2

    .line 112
    iget-object v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerUploader;->server:Lcom/squareup/http/Server;

    invoke-virtual {v0}, Lcom/squareup/http/Server;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lokhttp3/HttpUrl;->parse(Ljava/lang/String;)Lokhttp3/HttpUrl;

    move-result-object v0

    const-string v1, "1.0/transaction-ledger/upload"

    .line 113
    invoke-virtual {v0, v1}, Lokhttp3/HttpUrl;->resolve(Ljava/lang/String;)Lokhttp3/HttpUrl;

    move-result-object v0

    return-object v0
.end method

.method public uploadLedger(Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$TransactionLedgerEntryIterable;)Lio/reactivex/Single;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$TransactionLedgerEntryIterable;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;",
            ">;"
        }
    .end annotation

    .line 88
    new-instance v0, Lcom/squareup/payment/ledger/UploadTransactionLedgerRequestBody;

    .line 89
    invoke-direct {p0}, Lcom/squareup/payment/ledger/TransactionLedgerUploader;->randomId()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/squareup/payment/ledger/UploadTransactionLedgerRequestBody;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$TransactionLedgerEntryIterable;)V

    .line 90
    invoke-direct {p0, v0}, Lcom/squareup/payment/ledger/TransactionLedgerUploader;->doUploadLedgerStream(Lcom/squareup/payment/ledger/UploadTransactionLedgerRequestBody;)Lio/reactivex/Observable;

    move-result-object p1

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, Lcom/squareup/payment/ledger/TransactionLedgerUploader;->mainScheduler:Lio/reactivex/Scheduler;

    const/4 v2, 0x3

    const-wide/16 v3, 0x2

    .line 91
    invoke-static {v2, v3, v4, v0, v1}, Lcom/squareup/util/rx2/Rx2RetryStrategies;->exponentialBackoffThenError(IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/functions/Function;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->retryWhen(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/payment/ledger/-$$Lambda$TransactionLedgerUploader$4CCbW1uvYmbec4nmwmS9nazZZg4;->INSTANCE:Lcom/squareup/payment/ledger/-$$Lambda$TransactionLedgerUploader$4CCbW1uvYmbec4nmwmS9nazZZg4;

    .line 93
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;

    const/4 v1, 0x0

    const-string v2, "Failed to upload client ledger"

    invoke-direct {v0, v1, v2}, Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;-><init>(ZLjava/lang/String;)V

    .line 99
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->single(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public uploadLedger(Ljava/util/List;Lokio/ByteString;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;",
            ">;",
            "Lokio/ByteString;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 72
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    invoke-direct {p0}, Lcom/squareup/payment/ledger/TransactionLedgerUploader;->uploadEmptyLedger()Lio/reactivex/Single;

    move-result-object p1

    return-object p1

    .line 76
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/payment/ledger/TransactionLedgerUploader;->createUploadLedgerRequest(Ljava/util/List;Lokio/ByteString;)Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;

    move-result-object p1

    .line 77
    iget-object p2, p0, Lcom/squareup/payment/ledger/TransactionLedgerUploader;->transactionLedgerService:Lcom/squareup/server/transaction_ledger/TransactionLedgerService;

    invoke-interface {p2, p1}, Lcom/squareup/server/transaction_ledger/TransactionLedgerService;->uploadTransactionLedger(Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;)Lrx/Observable;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    const/4 p2, 0x3

    const-wide/16 v0, 0x2

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lcom/squareup/payment/ledger/TransactionLedgerUploader;->mainScheduler:Lio/reactivex/Scheduler;

    .line 78
    invoke-static {p2, v0, v1, v2, v3}, Lcom/squareup/util/rx2/Rx2RetryStrategies;->exponentialBackoffThenError(IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/functions/Function;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->retryWhen(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    sget-object p2, Lcom/squareup/payment/ledger/-$$Lambda$TransactionLedgerUploader$CuphCLbrBsEMA5mKKtcznQuVFsY;->INSTANCE:Lcom/squareup/payment/ledger/-$$Lambda$TransactionLedgerUploader$CuphCLbrBsEMA5mKKtcznQuVFsY;

    .line 80
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 83
    invoke-virtual {p1}, Lio/reactivex/Observable;->singleOrError()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
