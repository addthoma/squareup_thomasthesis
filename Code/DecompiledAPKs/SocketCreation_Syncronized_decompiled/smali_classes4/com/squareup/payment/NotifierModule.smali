.class public abstract Lcom/squareup/payment/NotifierModule;
.super Ljava/lang/Object;
.source "NotifierModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideAutoCaptureNotifier(Lcom/squareup/payment/AutoCaptureStatusBarNotifier;)Lcom/squareup/notifications/AutoCaptureNotifier;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideAutoVoidNotifier(Lcom/squareup/payment/AutoVoidStatusBarNotifier;)Lcom/squareup/notifications/AutoVoidNotifier;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract providePaymentIncompleteNotifier(Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier;)Lcom/squareup/notifications/PaymentIncompleteNotifier;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideStoredPaymentNotifier(Lcom/squareup/payment/offline/StoredPaymentStatusBarNotifier;)Lcom/squareup/notifications/StoredPaymentNotifier;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
