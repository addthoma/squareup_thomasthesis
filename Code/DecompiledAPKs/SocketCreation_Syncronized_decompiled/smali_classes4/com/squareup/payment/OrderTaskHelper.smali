.class public final Lcom/squareup/payment/OrderTaskHelper;
.super Ljava/lang/Object;
.source "OrderTaskHelper.java"


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public static orderSnapshotForTask(Lcom/squareup/queue/PaymentTask;)Lcom/squareup/payment/OrderSnapshot;
    .locals 9

    .line 15
    new-instance v8, Lcom/squareup/payment/OrderSnapshot;

    invoke-interface {p0}, Lcom/squareup/queue/PaymentTask;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 16
    invoke-interface {p0}, Lcom/squareup/queue/PaymentTask;->getItemizations()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/server/payment/AdjusterMessages;->fromItemizationMessages(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/checkout/CartItem;->of(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 17
    invoke-interface {p0}, Lcom/squareup/queue/PaymentTask;->getAdjustments()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/server/payment/AdjusterMessages;->fromAdjustmentMessages(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/payment/OrderAdjustment;->of(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    new-instance v4, Lcom/squareup/protos/client/IdPair;

    .line 18
    invoke-interface {p0}, Lcom/squareup/queue/PaymentTask;->getTicketId()Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x0

    invoke-direct {v4, v0, p0}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v6

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/payment/OrderSnapshot;-><init>(Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/IdPair;Lcom/squareup/checkout/DiningOption;Ljava/util/Map;Lcom/squareup/checkout/ReturnCart;)V

    return-object v8
.end method
