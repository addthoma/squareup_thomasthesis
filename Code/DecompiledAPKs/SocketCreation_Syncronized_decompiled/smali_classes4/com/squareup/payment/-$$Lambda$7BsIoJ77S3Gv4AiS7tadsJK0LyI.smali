.class public final synthetic Lcom/squareup/payment/-$$Lambda$7BsIoJ77S3Gv4AiS7tadsJK0LyI;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# instance fields
.field private final synthetic f$0:Lcom/squareup/payment/Transaction;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/payment/Transaction;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/payment/-$$Lambda$7BsIoJ77S3Gv4AiS7tadsJK0LyI;->f$0:Lcom/squareup/payment/Transaction;

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/payment/-$$Lambda$7BsIoJ77S3Gv4AiS7tadsJK0LyI;->f$0:Lcom/squareup/payment/Transaction;

    check-cast p1, Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Transaction;->onTaxItemRelationshipsChanged(Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;)V

    return-void
.end method
