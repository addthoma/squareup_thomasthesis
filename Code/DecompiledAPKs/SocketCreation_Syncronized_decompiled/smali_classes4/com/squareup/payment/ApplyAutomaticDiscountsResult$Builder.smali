.class public Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;
.super Ljava/lang/Object;
.source "ApplyAutomaticDiscountsResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/ApplyAutomaticDiscountsResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private added:I

.field private removed:I

.field private replacements:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;>;"
        }
    .end annotation
.end field

.field private split:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 71
    iput v0, p0, Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;->added:I

    .line 72
    iput v0, p0, Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;->removed:I

    .line 73
    iput v0, p0, Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;->split:I

    .line 74
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;->replacements:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public addReplacement(Ljava/lang/String;Lcom/squareup/checkout/CartItem;)Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;
    .locals 2

    .line 93
    iget-object v0, p0, Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;->replacements:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;->replacements:Ljava/util/Map;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;->replacements:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public build()Lcom/squareup/payment/ApplyAutomaticDiscountsResult;
    .locals 5

    .line 101
    new-instance v0, Lcom/squareup/payment/ApplyAutomaticDiscountsResult;

    iget v1, p0, Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;->added:I

    iget v2, p0, Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;->removed:I

    iget v3, p0, Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;->split:I

    iget-object v4, p0, Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;->replacements:Ljava/util/Map;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/payment/ApplyAutomaticDiscountsResult;-><init>(IIILjava/util/Map;)V

    return-object v0
.end method

.method public incrementAdded()Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;
    .locals 1

    .line 78
    iget v0, p0, Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;->added:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;->added:I

    return-object p0
.end method

.method public incrementRemoved()Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;
    .locals 1

    .line 83
    iget v0, p0, Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;->removed:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;->removed:I

    return-object p0
.end method

.method public incrementSplit()Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;
    .locals 1

    .line 88
    iget v0, p0, Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;->split:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;->split:I

    return-object p0
.end method
