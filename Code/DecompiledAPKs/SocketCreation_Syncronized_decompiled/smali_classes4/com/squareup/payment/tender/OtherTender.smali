.class public Lcom/squareup/payment/tender/OtherTender;
.super Lcom/squareup/payment/tender/BaseLocalTender;
.source "OtherTender.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/tender/OtherTender$Builder;
    }
.end annotation


# instance fields
.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final note:Ljava/lang/String;

.field private final res:Lcom/squareup/util/Res;

.field private final tip:Lcom/squareup/protos/common/Money;

.field private final tipPercentage:Lcom/squareup/util/Percentage;

.field private final type:Lcom/squareup/server/account/protos/OtherTenderType;


# direct methods
.method private constructor <init>(Lcom/squareup/payment/tender/OtherTender$Builder;)V
    .locals 2

    .line 33
    invoke-direct {p0, p1}, Lcom/squareup/payment/tender/BaseLocalTender;-><init>(Lcom/squareup/payment/tender/BaseLocalTender$Builder;)V

    .line 34
    invoke-static {p1}, Lcom/squareup/payment/tender/OtherTender$Builder;->access$000(Lcom/squareup/payment/tender/OtherTender$Builder;)Lcom/squareup/util/Percentage;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/tender/OtherTender;->tipPercentage:Lcom/squareup/util/Percentage;

    .line 35
    invoke-static {p1}, Lcom/squareup/payment/tender/OtherTender$Builder;->access$100(Lcom/squareup/payment/tender/OtherTender$Builder;)Lcom/squareup/server/account/protos/OtherTenderType;

    move-result-object v0

    const-string v1, "type"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/OtherTenderType;

    iput-object v0, p0, Lcom/squareup/payment/tender/OtherTender;->type:Lcom/squareup/server/account/protos/OtherTenderType;

    .line 36
    invoke-static {p1}, Lcom/squareup/payment/tender/OtherTender$Builder;->access$200(Lcom/squareup/payment/tender/OtherTender$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/tender/OtherTender;->note:Ljava/lang/String;

    .line 37
    iget-object v0, p1, Lcom/squareup/payment/tender/OtherTender$Builder;->res:Lcom/squareup/util/Res;

    iput-object v0, p0, Lcom/squareup/payment/tender/OtherTender;->res:Lcom/squareup/util/Res;

    .line 38
    invoke-static {p1}, Lcom/squareup/payment/tender/OtherTender$Builder;->access$300(Lcom/squareup/payment/tender/OtherTender$Builder;)Lcom/squareup/text/Formatter;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/tender/OtherTender;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 39
    invoke-static {p1}, Lcom/squareup/payment/tender/OtherTender$Builder;->access$400(Lcom/squareup/payment/tender/OtherTender$Builder;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/tender/OtherTender;->tip:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/payment/tender/OtherTender$Builder;Lcom/squareup/payment/tender/OtherTender$1;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1}, Lcom/squareup/payment/tender/OtherTender;-><init>(Lcom/squareup/payment/tender/OtherTender$Builder;)V

    return-void
.end method


# virtual methods
.method public getDeclinedMessage()Ljava/lang/String;
    .locals 3

    .line 69
    iget-object v0, p0, Lcom/squareup/payment/tender/OtherTender;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/transaction/R$string;->split_tender_other_declined:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 70
    invoke-virtual {p0}, Lcom/squareup/payment/tender/OtherTender;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "type"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 71
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getMethod()Lcom/squareup/protos/client/bills/Tender$Method;
    .locals 3

    .line 49
    iget-object v0, p0, Lcom/squareup/payment/tender/OtherTender;->type:Lcom/squareup/server/account/protos/OtherTenderType;

    iget-object v0, v0, Lcom/squareup/server/account/protos/OtherTenderType;->tender_type:Ljava/lang/Integer;

    .line 50
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->fromValue(I)Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    move-result-object v0

    .line 52
    new-instance v1, Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;-><init>()V

    new-instance v2, Lcom/squareup/protos/client/bills/OtherTender$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/bills/OtherTender$Builder;-><init>()V

    .line 54
    invoke-virtual {v2, v0}, Lcom/squareup/protos/client/bills/OtherTender$Builder;->other_tender_type(Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;)Lcom/squareup/protos/client/bills/OtherTender$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/payment/tender/OtherTender;->note:Ljava/lang/String;

    .line 55
    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/bills/OtherTender$Builder;->tender_note(Ljava/lang/String;)Lcom/squareup/protos/client/bills/OtherTender$Builder;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/OtherTender$Builder;->build()Lcom/squareup/protos/client/bills/OtherTender;

    move-result-object v0

    .line 52
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->other_tender(Lcom/squareup/protos/client/bills/OtherTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    move-result-object v0

    .line 57
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->build()Lcom/squareup/protos/client/bills/Tender$Method;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/payment/tender/OtherTender;->type:Lcom/squareup/server/account/protos/OtherTenderType;

    iget-object v0, v0, Lcom/squareup/server/account/protos/OtherTenderType;->tender_name:Ljava/lang/String;

    return-object v0
.end method

.method public getOtherTenderType()Lcom/squareup/server/account/protos/OtherTenderType;
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/squareup/payment/tender/OtherTender;->type:Lcom/squareup/server/account/protos/OtherTenderType;

    return-object v0
.end method

.method public getReceiptTenderName(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 0

    .line 99
    invoke-virtual {p0}, Lcom/squareup/payment/tender/OtherTender;->getName()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getShortTenderMessage()Ljava/lang/String;
    .locals 1

    .line 61
    invoke-virtual {p0}, Lcom/squareup/payment/tender/OtherTender;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTenderTypeForLogging()Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;
    .locals 1

    .line 103
    sget-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->OTHER:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    return-object v0
.end method

.method public getTenderTypeGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 65
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->OTHER_TENDER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object v0
.end method

.method public getTip()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 107
    iget-object v0, p0, Lcom/squareup/payment/tender/OtherTender;->tip:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getTipPercentage()Lcom/squareup/util/Percentage;
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/payment/tender/OtherTender;->tipPercentage:Lcom/squareup/util/Percentage;

    return-object v0
.end method

.method public requireTenderForHistory()Lcom/squareup/protos/client/bills/Tender;
    .locals 5

    .line 75
    invoke-super {p0}, Lcom/squareup/payment/tender/BaseLocalTender;->requireTenderForHistory()Lcom/squareup/protos/client/bills/Tender;

    move-result-object v0

    .line 77
    iget-object v1, v0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Tender$Method;->other_tender:Lcom/squareup/protos/client/bills/OtherTender;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Tender$Method;->other_tender:Lcom/squareup/protos/client/bills/OtherTender;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/OtherTender;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    if-nez v1, :cond_2

    .line 80
    :cond_0
    iget-object v1, v0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    if-nez v1, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    iget-object v1, v0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Tender$Method;->other_tender:Lcom/squareup/protos/client/bills/OtherTender;

    .line 83
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender;->newBuilder()Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object v2

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    .line 84
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$Method;->newBuilder()Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    move-result-object v0

    .line 85
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/OtherTender;->newBuilder()Lcom/squareup/protos/client/bills/OtherTender$Builder;

    move-result-object v1

    new-instance v3, Lcom/squareup/protos/client/bills/TranslatedName$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/client/bills/TranslatedName$Builder;-><init>()V

    .line 88
    invoke-virtual {p0}, Lcom/squareup/payment/tender/OtherTender;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/protos/client/bills/TranslatedName$Builder;->localized_name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/TranslatedName$Builder;

    move-result-object v3

    .line 89
    invoke-virtual {v3}, Lcom/squareup/protos/client/bills/TranslatedName$Builder;->build()Lcom/squareup/protos/client/bills/TranslatedName;

    move-result-object v3

    .line 86
    invoke-virtual {v1, v3}, Lcom/squareup/protos/client/bills/OtherTender$Builder;->read_only_translated_name(Lcom/squareup/protos/client/bills/TranslatedName;)Lcom/squareup/protos/client/bills/OtherTender$Builder;

    move-result-object v1

    .line 90
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/OtherTender$Builder;->build()Lcom/squareup/protos/client/bills/OtherTender;

    move-result-object v1

    .line 85
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->other_tender(Lcom/squareup/protos/client/bills/OtherTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->build()Lcom/squareup/protos/client/bills/Tender$Method;

    move-result-object v0

    .line 84
    invoke-virtual {v2, v0}, Lcom/squareup/protos/client/bills/Tender$Builder;->method(Lcom/squareup/protos/client/bills/Tender$Method;)Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$Builder;->build()Lcom/squareup/protos/client/bills/Tender;

    move-result-object v0

    :cond_2
    return-object v0
.end method

.method public supportsPaperSigAndTip()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
