.class public Lcom/squareup/payment/tender/ZeroTender;
.super Lcom/squareup/payment/tender/BaseLocalTender;
.source "ZeroTender.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/tender/ZeroTender$Builder;
    }
.end annotation


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;


# direct methods
.method private constructor <init>(Lcom/squareup/payment/tender/ZeroTender$Builder;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/squareup/payment/tender/BaseLocalTender;-><init>(Lcom/squareup/payment/tender/BaseLocalTender$Builder;)V

    .line 26
    invoke-static {p1}, Lcom/squareup/payment/tender/ZeroTender$Builder;->access$000(Lcom/squareup/payment/tender/ZeroTender$Builder;)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/tender/ZeroTender;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/payment/tender/ZeroTender$Builder;Lcom/squareup/payment/tender/ZeroTender$1;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/squareup/payment/tender/ZeroTender;-><init>(Lcom/squareup/payment/tender/ZeroTender$Builder;)V

    return-void
.end method


# virtual methods
.method public getDeclinedMessage()Ljava/lang/String;
    .locals 1

    .line 56
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method getMethod()Lcom/squareup/protos/client/bills/Tender$Method;
    .locals 2

    .line 30
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;-><init>()V

    new-instance v1, Lcom/squareup/protos/client/bills/ZeroAmountTender$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/ZeroAmountTender$Builder;-><init>()V

    .line 31
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/ZeroAmountTender$Builder;->build()Lcom/squareup/protos/client/bills/ZeroAmountTender;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->zero_amount_tender(Lcom/squareup/protos/client/bills/ZeroAmountTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    move-result-object v0

    .line 32
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->build()Lcom/squareup/protos/client/bills/Tender$Method;

    move-result-object v0

    return-object v0
.end method

.method public getReceiptTenderName(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public getShortTenderMessage()Ljava/lang/String;
    .locals 2

    .line 45
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "ZeroTender should never need a tender message. It should be converted to a concrete tender type."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getTenderTypeForLogging()Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;
    .locals 1

    .line 41
    sget-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->ZERO_AMOUNT:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    return-object v0
.end method

.method public getTenderTypeGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 2

    .line 50
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "ZeroTender should never need a glyph. It should be converted to a concrete tender type."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getTip()Lcom/squareup/protos/common/Money;
    .locals 3

    .line 60
    iget-object v0, p0, Lcom/squareup/payment/tender/ZeroTender;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public supportsPaperSigAndTip()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
