.class public abstract Lcom/squareup/payment/tender/MagStripeTenderBuilder;
.super Lcom/squareup/payment/tender/BaseCardTender$Builder;
.source "MagStripeTenderBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/tender/MagStripeTenderBuilder$CreditCardTenderBuilder;,
        Lcom/squareup/payment/tender/MagStripeTenderBuilder$GiftCardTenderBuilder;
    }
.end annotation


# instance fields
.field protected final canSwipeChipCard:Z

.field protected card:Lcom/squareup/Card;

.field protected final cardConverter:Lcom/squareup/payment/CardConverter;

.field protected partialCard:Lcom/squareup/register/widgets/card/PartialCard;


# direct methods
.method constructor <init>(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/settings/server/SwipeChipCardsSettings;Ljava/lang/String;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1, p3}, Lcom/squareup/payment/tender/BaseCardTender$Builder;-><init>(Lcom/squareup/payment/tender/TenderFactory;Ljava/lang/String;)V

    .line 22
    iget-object p3, p1, Lcom/squareup/payment/tender/TenderFactory;->cardConverter:Lcom/squareup/payment/CardConverter;

    iput-object p3, p0, Lcom/squareup/payment/tender/MagStripeTenderBuilder;->cardConverter:Lcom/squareup/payment/CardConverter;

    .line 24
    invoke-virtual {p2}, Lcom/squareup/settings/server/SwipeChipCardsSettings;->isEnabledBlocking()Z

    move-result p2

    if-eqz p2, :cond_0

    iget-object p1, p1, Lcom/squareup/payment/tender/TenderFactory;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    .line 25
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderHubUtils;->canSwipeChipCardsWithConnectedReaders()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/squareup/payment/tender/MagStripeTenderBuilder;->canSwipeChipCard:Z

    return-void
.end method


# virtual methods
.method public getCard()Lcom/squareup/Card;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/payment/tender/MagStripeTenderBuilder;->card:Lcom/squareup/Card;

    return-object v0
.end method

.method public getPartialCard()Lcom/squareup/register/widgets/card/PartialCard;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/payment/tender/MagStripeTenderBuilder;->partialCard:Lcom/squareup/register/widgets/card/PartialCard;

    return-object v0
.end method

.method public hasCard()Z
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/payment/tender/MagStripeTenderBuilder;->card:Lcom/squareup/Card;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasPartialCard()Z
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/payment/tender/MagStripeTenderBuilder;->partialCard:Lcom/squareup/register/widgets/card/PartialCard;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public abstract isGiftCard()Z
.end method

.method public setCard(Lcom/squareup/Card;)Lcom/squareup/payment/tender/MagStripeTenderBuilder;
    .locals 1

    const/4 v0, 0x0

    .line 36
    invoke-virtual {p0, p1, v0}, Lcom/squareup/payment/tender/MagStripeTenderBuilder;->setValidAndPartialCard(Lcom/squareup/Card;Lcom/squareup/register/widgets/card/PartialCard;)Lcom/squareup/payment/tender/MagStripeTenderBuilder;

    move-result-object p1

    return-object p1
.end method

.method public setValidAndPartialCard(Lcom/squareup/Card;Lcom/squareup/register/widgets/card/PartialCard;)Lcom/squareup/payment/tender/MagStripeTenderBuilder;
    .locals 0

    .line 52
    iput-object p1, p0, Lcom/squareup/payment/tender/MagStripeTenderBuilder;->card:Lcom/squareup/Card;

    .line 53
    iput-object p2, p0, Lcom/squareup/payment/tender/MagStripeTenderBuilder;->partialCard:Lcom/squareup/register/widgets/card/PartialCard;

    return-object p0
.end method
