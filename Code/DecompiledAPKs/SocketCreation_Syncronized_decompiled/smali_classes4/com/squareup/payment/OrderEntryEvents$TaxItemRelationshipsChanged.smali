.class public Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;
.super Lcom/squareup/log/OhSnapLoggable$JavaOhSnapLoggable;
.source "OrderEntryEvents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/OrderEntryEvents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TaxItemRelationshipsChanged"
.end annotation


# instance fields
.field public final edits:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final feeId:Ljava/lang/String;

.field public final type:Lcom/squareup/payment/OrderEntryEvents$TaxItemChangeType;


# direct methods
.method private constructor <init>(Ljava/lang/String;Lcom/squareup/payment/OrderEntryEvents$TaxItemChangeType;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/payment/OrderEntryEvents$TaxItemChangeType;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .line 82
    invoke-direct {p0}, Lcom/squareup/log/OhSnapLoggable$JavaOhSnapLoggable;-><init>()V

    .line 83
    iput-object p1, p0, Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;->feeId:Ljava/lang/String;

    .line 84
    iput-object p2, p0, Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;->type:Lcom/squareup/payment/OrderEntryEvents$TaxItemChangeType;

    .line 85
    iput-object p3, p0, Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;->edits:Ljava/util/Map;

    return-void
.end method

.method public static edits(Ljava/lang/String;Ljava/util/Map;)Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;"
        }
    .end annotation

    .line 78
    new-instance v0, Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;

    sget-object v1, Lcom/squareup/payment/OrderEntryEvents$TaxItemChangeType;->EDITS:Lcom/squareup/payment/OrderEntryEvents$TaxItemChangeType;

    invoke-direct {v0, p0, v1, p1}, Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;-><init>(Ljava/lang/String;Lcom/squareup/payment/OrderEntryEvents$TaxItemChangeType;Ljava/util/Map;)V

    return-object v0
.end method

.method public static globalAdd(Ljava/lang/String;)Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;
    .locals 3

    .line 74
    new-instance v0, Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;

    sget-object v1, Lcom/squareup/payment/OrderEntryEvents$TaxItemChangeType;->GLOBAL_ADD:Lcom/squareup/payment/OrderEntryEvents$TaxItemChangeType;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;-><init>(Ljava/lang/String;Lcom/squareup/payment/OrderEntryEvents$TaxItemChangeType;Ljava/util/Map;)V

    return-object v0
.end method
