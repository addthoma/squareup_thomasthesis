.class Lcom/squareup/payment/BillPaymentOfflineStrategy;
.super Ljava/lang/Object;
.source "BillPaymentOfflineStrategy.java"

# interfaces
.implements Lcom/squareup/payment/BillPayment$BillPaymentStrategy;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;
    }
.end annotation


# static fields
.field private static final MAX_GPS_UNCERTAINTY_METERS:D = 10000.0

.field private static final STATUS_SUCCESS:Lcom/squareup/protos/client/Status;


# instance fields
.field private final backgroundCaptor:Lcom/squareup/payment/BackgroundCaptor;

.field private billInFlight:Lcom/squareup/payment/offline/BillInFlight;

.field private final locationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation
.end field

.field private final offlineTenderClientIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentAccuracyLogger:Lcom/squareup/payment/PaymentAccuracyLogger;

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

.field private final storeAndForwardAnalytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

.field private final storeAndForwardPaymentService:Lcom/squareup/payment/offline/StoreAndForwardPaymentService;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 41
    new-instance v0, Lcom/squareup/protos/client/Status$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/Status$Builder;-><init>()V

    const/4 v1, 0x1

    .line 42
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/Status$Builder;->success(Ljava/lang/Boolean;)Lcom/squareup/protos/client/Status$Builder;

    move-result-object v0

    .line 43
    invoke-virtual {v0}, Lcom/squareup/protos/client/Status$Builder;->build()Lcom/squareup/protos/client/Status;

    move-result-object v0

    sput-object v0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->STATUS_SUCCESS:Lcom/squareup/protos/client/Status;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;)V
    .locals 1

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->offlineTenderClientIds:Ljava/util/Set;

    .line 60
    invoke-static {p1}, Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;->access$000(Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;)Lcom/squareup/settings/server/AccountStatusSettings;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 61
    invoke-static {p1}, Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;->access$100(Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;)Lcom/squareup/util/Res;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->res:Lcom/squareup/util/Res;

    .line 62
    invoke-static {p1}, Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;->access$200(Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;)Ldagger/Lazy;

    move-result-object v0

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/Transaction;

    iput-object v0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->transaction:Lcom/squareup/payment/Transaction;

    .line 63
    invoke-static {p1}, Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;->access$300(Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->locationProvider:Ljavax/inject/Provider;

    .line 64
    invoke-static {p1}, Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;->access$400(Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;)Lcom/squareup/analytics/StoreAndForwardAnalytics;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->storeAndForwardAnalytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    .line 65
    invoke-static {p1}, Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;->access$500(Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;)Lcom/squareup/payment/PaymentAccuracyLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->paymentAccuracyLogger:Lcom/squareup/payment/PaymentAccuracyLogger;

    .line 66
    invoke-static {p1}, Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;->access$600(Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;)Lcom/squareup/payment/offline/StoreAndForwardPaymentService;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->storeAndForwardPaymentService:Lcom/squareup/payment/offline/StoreAndForwardPaymentService;

    .line 67
    invoke-static {p1}, Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;->access$700(Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;)Lcom/squareup/payment/BackgroundCaptor;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->backgroundCaptor:Lcom/squareup/payment/BackgroundCaptor;

    .line 68
    invoke-static {p1}, Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;->access$800(Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;)Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;Lcom/squareup/payment/BillPaymentOfflineStrategy$1;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Lcom/squareup/payment/BillPaymentOfflineStrategy;-><init>(Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;)V

    return-void
.end method

.method private declined(I)Lcom/squareup/protos/client/Status;
    .locals 3

    .line 231
    new-instance v0, Lcom/squareup/protos/client/Status$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/Status$Builder;-><init>()V

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/Status$Builder;->success(Ljava/lang/Boolean;)Lcom/squareup/protos/client/Status$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/transaction/R$string;->offline_mode_declined:I

    .line 232
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/Status$Builder;->localized_title(Ljava/lang/String;)Lcom/squareup/protos/client/Status$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->res:Lcom/squareup/util/Res;

    .line 233
    invoke-interface {v1, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/Status$Builder;->localized_description(Ljava/lang/String;)Lcom/squareup/protos/client/Status$Builder;

    move-result-object p1

    .line 234
    invoke-virtual {p1}, Lcom/squareup/protos/client/Status$Builder;->build()Lcom/squareup/protos/client/Status;

    move-result-object p1

    return-object p1
.end method

.method private getMerchantCountryCode()Ljava/lang/String;
    .locals 1

    .line 252
    iget-object v0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getCountryCodeOrNull()Lcom/squareup/CountryCode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/CountryCode;->name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getOfflineAddTenderStatus(Lcom/squareup/protos/client/bills/AddTender;)Lcom/squareup/protos/client/Status;
    .locals 1

    .line 191
    invoke-direct {p0, p1}, Lcom/squareup/payment/BillPaymentOfflineStrategy;->hasCardNotPresentCard(Lcom/squareup/protos/client/bills/AddTender;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    sget p1, Lcom/squareup/transaction/R$string;->offline_mode_cnp_card_not_accepted:I

    invoke-direct {p0, p1}, Lcom/squareup/payment/BillPaymentOfflineStrategy;->declined(I)Lcom/squareup/protos/client/Status;

    move-result-object p1

    return-object p1

    .line 193
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/payment/BillPaymentOfflineStrategy;->hasNonWhitelistedOfflineCardBrand(Lcom/squareup/protos/client/bills/AddTender;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 196
    sget p1, Lcom/squareup/transaction/R$string;->offline_mode_non_whitelisted_brand:I

    invoke-direct {p0, p1}, Lcom/squareup/payment/BillPaymentOfflineStrategy;->declined(I)Lcom/squareup/protos/client/Status;

    move-result-object p1

    return-object p1

    .line 197
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/payment/BillPaymentOfflineStrategy;->hasCreditCard(Lcom/squareup/protos/client/bills/AddTender;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/squareup/payment/BillPaymentOfflineStrategy;->inCorrectMerchantCountry()Z

    move-result v0

    if-nez v0, :cond_2

    .line 198
    sget p1, Lcom/squareup/transaction/R$string;->offline_mode_invalid_location:I

    invoke-direct {p0, p1}, Lcom/squareup/payment/BillPaymentOfflineStrategy;->declined(I)Lcom/squareup/protos/client/Status;

    move-result-object p1

    return-object p1

    .line 199
    :cond_2
    invoke-direct {p0, p1}, Lcom/squareup/payment/BillPaymentOfflineStrategy;->hasCreditCard(Lcom/squareup/protos/client/bills/AddTender;)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->isOfflineTransactionLimitExceeded()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 200
    sget p1, Lcom/squareup/transaction/R$string;->offline_mode_transaction_limit_message:I

    invoke-direct {p0, p1}, Lcom/squareup/payment/BillPaymentOfflineStrategy;->declined(I)Lcom/squareup/protos/client/Status;

    move-result-object p1

    return-object p1

    .line 202
    :cond_3
    sget-object p1, Lcom/squareup/payment/BillPaymentOfflineStrategy;->STATUS_SUCCESS:Lcom/squareup/protos/client/Status;

    return-object p1
.end method

.method private hasCardNotPresentCard(Lcom/squareup/protos/client/bills/AddTender;)Z
    .locals 1

    .line 207
    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    if-eqz v0, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/CardTender$Card;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->KEYED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private hasCreditCard(Lcom/squareup/protos/client/bills/AddTender;)Z
    .locals 1

    .line 226
    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    if-eqz v0, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private hasNonWhitelistedOfflineCardBrand(Lcom/squareup/protos/client/bills/AddTender;)Z
    .locals 1

    .line 214
    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    if-eqz v0, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/CardTender$Card;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 218
    invoke-static {p1}, Lcom/squareup/card/CardBrands;->toCardBrand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/Card$Brand;

    move-result-object p1

    .line 217
    invoke-direct {p0, p1}, Lcom/squareup/payment/BillPaymentOfflineStrategy;->isSupportedCardBrandOffline(Lcom/squareup/Card$Brand;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private inCorrectMerchantCountry()Z
    .locals 9

    .line 238
    invoke-direct {p0}, Lcom/squareup/payment/BillPaymentOfflineStrategy;->getMerchantCountryCode()Ljava/lang/String;

    move-result-object v0

    .line 239
    iget-object v1, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->locationProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/Location;

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    .line 240
    invoke-direct {p0, v0, v1}, Lcom/squareup/payment/BillPaymentOfflineStrategy;->inMerchantCountry(Ljava/lang/String;Landroid/location/Location;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    .line 245
    :cond_0
    iget-object v3, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->storeAndForwardAnalytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    .line 246
    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v4, v6

    .line 247
    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v2, 0x2

    invoke-virtual {v1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v4, v2

    const-string v1, "latitude=%f,longitude=%f,accuracy=%f]"

    .line 246
    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 245
    invoke-virtual {v3, v0, v1}, Lcom/squareup/analytics/StoreAndForwardAnalytics;->logOutOfCountry(Ljava/lang/String;Ljava/lang/String;)V

    return v6

    :cond_1
    :goto_0
    return v2
.end method

.method private inMerchantCountry(Ljava/lang/String;Landroid/location/Location;)Z
    .locals 9

    .line 256
    invoke-virtual {p2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    invoke-virtual {p2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    .line 257
    invoke-virtual {p2}, Landroid/location/Location;->getAccuracy()F

    move-result p2

    float-to-double v5, p2

    const-wide v7, 0x40c3880000000000L    # 10000.0

    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->min(DD)D

    move-result-wide v5

    move-object v0, p1

    .line 256
    invoke-static/range {v0 .. v6}, Lcom/squareup/util/CountryChecker;->inCountry(Ljava/lang/String;DDD)Z

    move-result p1

    return p1
.end method

.method private isSupportedCardBrandOffline(Lcom/squareup/Card$Brand;)Z
    .locals 1

    .line 222
    iget-object v0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/Card$Brand;->getJsonRepresentation()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/settings/server/PaymentSettings;->isSupportedCardBrandOffline(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method


# virtual methods
.method public addTenders(Lcom/squareup/protos/client/bills/AddTendersRequest;)Lcom/squareup/protos/client/bills/AddTendersResponse;
    .locals 10

    .line 93
    sget-object v0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->STATUS_SUCCESS:Lcom/squareup/protos/client/Status;

    .line 94
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 95
    iget-object v2, p1, Lcom/squareup/protos/client/bills/AddTendersRequest;->add_tender:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/AddTender;

    .line 96
    invoke-direct {p0, v3}, Lcom/squareup/payment/BillPaymentOfflineStrategy;->getOfflineAddTenderStatus(Lcom/squareup/protos/client/bills/AddTender;)Lcom/squareup/protos/client/Status;

    move-result-object v4

    .line 97
    iget-object v5, v4, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 98
    iget-object v5, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->offlineTenderClientIds:Ljava/util/Set;

    monitor-enter v5

    .line 99
    :try_start_0
    iget-object v6, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->offlineTenderClientIds:Ljava/util/Set;

    iget-object v7, v3, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v7, v7, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v7, v7, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v6, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 100
    monitor-exit v5

    goto :goto_1

    :catchall_0
    move-exception p1

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    .line 101
    :cond_0
    iget-object v5, v0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_1

    move-object v0, v4

    .line 105
    :cond_1
    :goto_1
    iget-object v5, v3, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    .line 106
    iget-object v6, v3, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v6, v6, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v6, v6, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    if-eqz v6, :cond_2

    .line 107
    iget-object v5, v3, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v5, v5, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    iget-object v5, v5, Lcom/squareup/protos/client/bills/Tender$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    .line 108
    iget-object v6, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 109
    invoke-virtual {v6}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/squareup/settings/server/PaymentSettings;->getSkipSignatureMaxCents()J

    move-result-wide v6

    iget-object v8, v5, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v6, v7, v8}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v6

    .line 111
    iget-object v7, v3, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    invoke-virtual {v7}, Lcom/squareup/protos/client/bills/Tender;->newBuilder()Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object v7

    iget-object v8, v3, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v8, v8, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    .line 112
    invoke-virtual {v8}, Lcom/squareup/protos/client/bills/Tender$Method;->newBuilder()Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    move-result-object v8

    iget-object v3, v3, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    .line 113
    invoke-virtual {v3}, Lcom/squareup/protos/client/bills/CardTender;->newBuilder()Lcom/squareup/protos/client/bills/CardTender$Builder;

    move-result-object v3

    new-instance v9, Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;

    invoke-direct {v9}, Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;-><init>()V

    .line 115
    invoke-virtual {v9, v5}, Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;->authorized_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;

    move-result-object v5

    .line 116
    invoke-virtual {v5, v6}, Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;->signature_optional_max_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;

    move-result-object v5

    .line 117
    invoke-virtual {v5}, Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;->build()Lcom/squareup/protos/client/bills/CardTender$Authorization;

    move-result-object v5

    .line 114
    invoke-virtual {v3, v5}, Lcom/squareup/protos/client/bills/CardTender$Builder;->read_only_authorization(Lcom/squareup/protos/client/bills/CardTender$Authorization;)Lcom/squareup/protos/client/bills/CardTender$Builder;

    move-result-object v3

    .line 118
    invoke-virtual {v3}, Lcom/squareup/protos/client/bills/CardTender$Builder;->build()Lcom/squareup/protos/client/bills/CardTender;

    move-result-object v3

    .line 113
    invoke-virtual {v8, v3}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->card_tender(Lcom/squareup/protos/client/bills/CardTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    move-result-object v3

    .line 119
    invoke-virtual {v3}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->build()Lcom/squareup/protos/client/bills/Tender$Method;

    move-result-object v3

    .line 112
    invoke-virtual {v7, v3}, Lcom/squareup/protos/client/bills/Tender$Builder;->method(Lcom/squareup/protos/client/bills/Tender$Method;)Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object v3

    .line 120
    invoke-virtual {v3}, Lcom/squareup/protos/client/bills/Tender$Builder;->build()Lcom/squareup/protos/client/bills/Tender;

    move-result-object v5

    .line 123
    :cond_2
    new-instance v3, Lcom/squareup/protos/client/bills/AddedTender$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/client/bills/AddedTender$Builder;-><init>()V

    .line 124
    invoke-virtual {v3, v4}, Lcom/squareup/protos/client/bills/AddedTender$Builder;->status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/bills/AddedTender$Builder;

    move-result-object v3

    .line 125
    invoke-virtual {v3, v5}, Lcom/squareup/protos/client/bills/AddedTender$Builder;->tender(Lcom/squareup/protos/client/bills/Tender;)Lcom/squareup/protos/client/bills/AddedTender$Builder;

    move-result-object v3

    const/4 v4, 0x0

    .line 126
    invoke-virtual {v3, v4}, Lcom/squareup/protos/client/bills/AddedTender$Builder;->receipt_details(Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;)Lcom/squareup/protos/client/bills/AddedTender$Builder;

    move-result-object v3

    .line 127
    invoke-virtual {v3, v4}, Lcom/squareup/protos/client/bills/AddedTender$Builder;->remaining_balance_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/AddedTender$Builder;

    move-result-object v3

    .line 128
    invoke-virtual {v3}, Lcom/squareup/protos/client/bills/AddedTender$Builder;->build()Lcom/squareup/protos/client/bills/AddedTender;

    move-result-object v3

    .line 123
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 131
    :cond_3
    iget-object v2, v0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 132
    iget-object v2, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->storeAndForwardAnalytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    invoke-virtual {v2}, Lcom/squareup/analytics/StoreAndForwardAnalytics;->logAuthorized()V

    .line 133
    iget-object v2, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->paymentAccuracyLogger:Lcom/squareup/payment/PaymentAccuracyLogger;

    iget-object v3, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/payment/PaymentAccuracyLogger;->logStoredTransaction(Lcom/squareup/payment/BillPayment;)V

    goto :goto_2

    .line 135
    :cond_4
    iget-object v2, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->storeAndForwardAnalytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    invoke-virtual {v2}, Lcom/squareup/analytics/StoreAndForwardAnalytics;->logAuthorizeFailed()V

    .line 138
    :goto_2
    new-instance v2, Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;-><init>()V

    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 139
    invoke-virtual {v2, p1}, Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;->bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;

    move-result-object p1

    .line 140
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;->status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;

    move-result-object p1

    .line 141
    invoke-virtual {p1, v1}, Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;->tender(Ljava/util/List;)Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;

    move-result-object p1

    .line 142
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;->build()Lcom/squareup/protos/client/bills/AddTendersResponse;

    move-result-object p1

    return-object p1
.end method

.method public canExitStrategy()Z
    .locals 1

    .line 73
    invoke-virtual {p0}, Lcom/squareup/payment/BillPaymentOfflineStrategy;->hasOfflineTenders()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public doCapture(Lcom/squareup/payment/BillPayment;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    .line 147
    invoke-virtual {p0}, Lcom/squareup/payment/BillPaymentOfflineStrategy;->hasOfflineTenders()Z

    move-result p2

    if-eqz p2, :cond_0

    const/4 p2, 0x1

    .line 152
    invoke-virtual {p1, p2}, Lcom/squareup/payment/BillPayment;->createCompleteBillWithKeyException(Z)Lcom/squareup/queue/bills/CompleteBill;

    .line 154
    new-instance p2, Lcom/squareup/payment/offline/BillInFlight;

    invoke-direct {p2, p1}, Lcom/squareup/payment/offline/BillInFlight;-><init>(Lcom/squareup/payment/BillPayment;)V

    iput-object p2, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->billInFlight:Lcom/squareup/payment/offline/BillInFlight;

    .line 157
    iget-object p2, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    invoke-interface {p2}, Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;->skipReceiptScreenForFastCheckout()Z

    move-result p2

    .line 158
    iget-object v0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->storeAndForwardPaymentService:Lcom/squareup/payment/offline/StoreAndForwardPaymentService;

    iget-object v1, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->billInFlight:Lcom/squareup/payment/offline/BillInFlight;

    invoke-virtual {v0, v1, p2}, Lcom/squareup/payment/offline/StoreAndForwardPaymentService;->enqueuePayment(Lcom/squareup/payment/offline/BillInFlight;Z)V

    .line 159
    iget-object p2, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->storeAndForwardAnalytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    invoke-virtual {p2}, Lcom/squareup/analytics/StoreAndForwardAnalytics;->logPaymentEnqueued()V

    .line 160
    iget-object p2, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->paymentAccuracyLogger:Lcom/squareup/payment/PaymentAccuracyLogger;

    invoke-virtual {p2, p1}, Lcom/squareup/payment/PaymentAccuracyLogger;->logStoredTransaction(Lcom/squareup/payment/BillPayment;)V

    .line 163
    iget-object p1, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->backgroundCaptor:Lcom/squareup/payment/BackgroundCaptor;

    invoke-virtual {p1}, Lcom/squareup/payment/BackgroundCaptor;->stopAutoCapture()V

    return-void

    .line 148
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Cannot perform enqueueStoreAndForwardTask with no offline tenders"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public enqueueAttachContactTask(Lcom/squareup/payment/tender/BaseTender;)V
    .locals 0

    return-void
.end method

.method public enqueueLastReceipt(Lcom/squareup/payment/BillPayment;Lcom/squareup/payment/tender/BaseTender;)V
    .locals 1

    .line 167
    invoke-virtual {p0}, Lcom/squareup/payment/BillPaymentOfflineStrategy;->hasOfflineTenders()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 173
    iget-object p2, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->billInFlight:Lcom/squareup/payment/offline/BillInFlight;

    invoke-virtual {p2, p1}, Lcom/squareup/payment/offline/BillInFlight;->withLastOfflineReceipt(Lcom/squareup/payment/BillPayment;)Lcom/squareup/payment/offline/BillInFlight;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->billInFlight:Lcom/squareup/payment/offline/BillInFlight;

    .line 177
    :try_start_0
    iget-object p1, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->storeAndForwardPaymentService:Lcom/squareup/payment/offline/StoreAndForwardPaymentService;

    iget-object p2, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->billInFlight:Lcom/squareup/payment/offline/BillInFlight;

    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, Lcom/squareup/payment/offline/StoreAndForwardPaymentService;->enqueuePayment(Lcom/squareup/payment/offline/BillInFlight;Z)V
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void

    .line 168
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Cannot enqueue last offline receipt with no offline tenders"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method getOfflineTendersCount()I
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->offlineTenderClientIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method

.method public hasOfflineTenders()Z
    .locals 2

    .line 77
    iget-object v0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->offlineTenderClientIds:Ljava/util/Set;

    monitor-enter v0

    .line 78
    :try_start_0
    iget-object v1, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->offlineTenderClientIds:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    .line 79
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onDropTender(Lcom/squareup/payment/tender/BaseTender;)V
    .locals 2

    .line 87
    iget-object v0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->offlineTenderClientIds:Ljava/util/Set;

    monitor-enter v0

    .line 88
    :try_start_0
    iget-object v1, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy;->offlineTenderClientIds:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/squareup/payment/tender/BaseTender;->getClientId()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 89
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method
