.class public Lcom/squareup/payment/OrderSnapshot;
.super Lcom/squareup/payment/Order;
.source "OrderSnapshot.java"


# instance fields
.field private final adjuster:Lcom/squareup/calc/Adjuster;

.field private final orderAdjustmentsSnapshot:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderAdjustment;",
            ">;"
        }
    .end annotation
.end field

.field private final total:J


# direct methods
.method constructor <init>(Lcom/squareup/payment/Order;)V
    .locals 2

    .line 65
    invoke-direct {p0, p1}, Lcom/squareup/payment/Order;-><init>(Lcom/squareup/payment/Order;)V

    .line 66
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/OrderSnapshot;->adjuster:Lcom/squareup/calc/Adjuster;

    const/4 p1, 0x0

    .line 67
    iput-object p1, p0, Lcom/squareup/payment/OrderSnapshot;->orderAdjustmentsSnapshot:Ljava/util/List;

    const-wide/16 v0, -0x1

    .line 68
    iput-wide v0, p0, Lcom/squareup/payment/OrderSnapshot;->total:J

    return-void
.end method

.method constructor <init>(Lcom/squareup/protos/client/bills/Bill;Lcom/squareup/protos/common/Money;Lcom/squareup/util/Res;ZZ)V
    .locals 10

    .line 100
    sget-object v0, Lcom/squareup/payment/OrderVariationNames;->INSTANCE:Lcom/squareup/payment/OrderVariationNames;

    .line 107
    invoke-static {p1}, Lcom/squareup/payment/OrderSnapshot;->getSeatsFromBill(Lcom/squareup/protos/client/bills/Bill;)Ljava/util/List;

    move-result-object v1

    .line 102
    invoke-static {p1, p3, v0, p4, v1}, Lcom/squareup/checkout/CartItem;->fromBillHistory(Lcom/squareup/protos/client/bills/Bill;Lcom/squareup/util/Res;Lcom/squareup/checkout/OrderVariationNamer;ZLjava/util/List;)Ljava/util/List;

    move-result-object v4

    .line 109
    invoke-static {p1, p5}, Lcom/squareup/payment/OrderAdjustment;->of(Lcom/squareup/protos/client/bills/Bill;Z)Ljava/util/List;

    move-result-object v5

    .line 111
    invoke-static {p1}, Lcom/squareup/payment/OrderSnapshot;->cartDiningOption(Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/checkout/DiningOption;

    move-result-object v7

    .line 112
    invoke-static {p1}, Lcom/squareup/payment/OrderSnapshot;->getSurchargesFromBill(Lcom/squareup/protos/client/bills/Bill;)Ljava/util/Map;

    move-result-object v8

    sget-object p4, Lcom/squareup/payment/OrderVariationNames;->INSTANCE:Lcom/squareup/payment/OrderVariationNames;

    .line 113
    invoke-static {p1, p3, p4, p5}, Lcom/squareup/checkout/ReturnCart;->fromBill(Lcom/squareup/protos/client/bills/Bill;Lcom/squareup/util/Res;Lcom/squareup/checkout/OrderVariationNamer;Z)Lcom/squareup/checkout/ReturnCart;

    move-result-object v9

    const/4 v6, 0x0

    move-object v2, p0

    move-object v3, p2

    .line 100
    invoke-direct/range {v2 .. v9}, Lcom/squareup/payment/OrderSnapshot;-><init>(Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/IdPair;Lcom/squareup/checkout/DiningOption;Ljava/util/Map;Lcom/squareup/checkout/ReturnCart;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/IdPair;Lcom/squareup/checkout/DiningOption;Ljava/util/Map;Lcom/squareup/checkout/ReturnCart;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderAdjustment;",
            ">;",
            "Lcom/squareup/protos/client/IdPair;",
            "Lcom/squareup/checkout/DiningOption;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Surcharge;",
            ">;",
            "Lcom/squareup/checkout/ReturnCart;",
            ")V"
        }
    .end annotation

    .line 87
    iget-object v1, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    move-object v0, p0

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/payment/Order;-><init>(Lcom/squareup/protos/common/CurrencyCode;Ljava/util/List;Lcom/squareup/protos/client/IdPair;Lcom/squareup/checkout/DiningOption;Ljava/util/Map;Lcom/squareup/checkout/ReturnCart;)V

    .line 88
    invoke-static {p3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/payment/OrderSnapshot;->orderAdjustmentsSnapshot:Ljava/util/List;

    .line 89
    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/squareup/payment/OrderSnapshot;->total:J

    const/4 p1, 0x0

    .line 90
    iput-object p1, p0, Lcom/squareup/payment/OrderSnapshot;->adjuster:Lcom/squareup/calc/Adjuster;

    return-void
.end method

.method private static cartDiningOption(Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/checkout/DiningOption;
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$LineItems;->default_dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    if-nez v0, :cond_0

    goto :goto_0

    .line 133
    :cond_0
    iget-object p0, p0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->default_dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    invoke-static {p0}, Lcom/squareup/checkout/DiningOption;->of(Lcom/squareup/protos/client/bills/DiningOptionLineItem;)Lcom/squareup/checkout/DiningOption;

    move-result-object p0

    return-object p0

    :cond_1
    :goto_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public static forBill(Lcom/squareup/protos/client/bills/Bill;Lcom/squareup/util/Res;ZZ)Lcom/squareup/payment/Order;
    .locals 7

    .line 40
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->amount_details:Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->amount_details:Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->net:Lcom/squareup/protos/client/bills/Cart$Amounts;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->amount_details:Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->net:Lcom/squareup/protos/client/bills/Cart$Amounts;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    :goto_0
    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    move-object v3, v0

    .line 43
    new-instance v0, Lcom/squareup/payment/OrderSnapshot;

    move-object v1, v0

    move-object v2, p0

    move-object v4, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/squareup/payment/OrderSnapshot;-><init>(Lcom/squareup/protos/client/bills/Bill;Lcom/squareup/protos/common/Money;Lcom/squareup/util/Res;ZZ)V

    return-object v0
.end method

.method private static getSeatsFromBill(Lcom/squareup/protos/client/bills/Bill;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Bill;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;"
        }
    .end annotation

    .line 118
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    invoke-static {p0}, Lcom/squareup/checkout/OrderDestination;->seatsFromFeatureDetails(Lcom/squareup/protos/client/bills/Cart$FeatureDetails;)Ljava/util/List;

    move-result-object p0

    goto :goto_0

    .line 119
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method private static getSurchargesFromBill(Lcom/squareup/protos/client/bills/Bill;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Bill;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Surcharge;",
            ">;"
        }
    .end annotation

    .line 123
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$LineItems;->surcharge:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 124
    iget-object p0, p0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->surcharge:Ljava/util/List;

    invoke-static {p0}, Lcom/squareup/checkout/Surcharge;->fromProto(Ljava/util/List;)Ljava/util/Map;

    move-result-object p0

    return-object p0

    .line 126
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method protected getAdjuster()Lcom/squareup/calc/Adjuster;
    .locals 2

    .line 137
    invoke-virtual {p0}, Lcom/squareup/payment/OrderSnapshot;->isRehydrated()Z

    move-result v0

    if-nez v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/squareup/payment/OrderSnapshot;->adjuster:Lcom/squareup/calc/Adjuster;

    return-object v0

    .line 141
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Rehydrated snapshots cannot recalculate."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getAdjustmentAmountsByIdForItem(Lcom/squareup/checkout/CartItem;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/CartItem;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 199
    invoke-virtual {p0}, Lcom/squareup/payment/OrderSnapshot;->isRehydrated()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 200
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->itemizedAdjustmentAmountsFromTransactionsHistoryById:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 206
    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->itemizedAdjustmentAmountsFromTransactionsHistoryById:Ljava/util/Map;

    return-object p1

    .line 203
    :cond_0
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Please fix how ItemAdjustments in CartItems are built in deserialized instances"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 208
    :cond_1
    invoke-super {p0, p1}, Lcom/squareup/payment/Order;->getAdjustmentAmountsByIdForItem(Lcom/squareup/checkout/CartItem;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public getAdjustmentsForRequest()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/value/Adjustment;",
            ">;"
        }
    .end annotation

    .line 164
    invoke-virtual {p0}, Lcom/squareup/payment/OrderSnapshot;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v0

    .line 165
    invoke-virtual {p0}, Lcom/squareup/payment/OrderSnapshot;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/calc/Adjuster;->calculate()V

    .line 168
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 170
    invoke-virtual {p0}, Lcom/squareup/payment/OrderSnapshot;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/calc/Adjuster;->getCollectedAmountPerDiscount()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 171
    invoke-virtual {p0}, Lcom/squareup/payment/OrderSnapshot;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/calc/Adjuster;->getAppliedDiscounts()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/checkout/Discount;

    .line 172
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/math/BigDecimal;

    invoke-virtual {v3}, Ljava/math/BigDecimal;->longValueExact()J

    move-result-wide v5

    invoke-static {v5, v6, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 173
    invoke-virtual {v4, v3}, Lcom/squareup/checkout/Discount;->asRequestAdjustment(Lcom/squareup/protos/common/Money;)Lcom/squareup/server/payment/value/Adjustment;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 177
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/OrderSnapshot;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/calc/Adjuster;->getCollectedAmountPerTax()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 178
    invoke-virtual {p0}, Lcom/squareup/payment/OrderSnapshot;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/calc/Adjuster;->getAppliedTaxes()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/checkout/Tax;

    .line 179
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/math/BigDecimal;

    invoke-virtual {v3}, Ljava/math/BigDecimal;->longValueExact()J

    move-result-wide v5

    invoke-static {v5, v6, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 180
    invoke-virtual {v4, v3}, Lcom/squareup/checkout/Tax;->asRequestAdjustment(Lcom/squareup/protos/common/Money;)Lcom/squareup/server/payment/value/Adjustment;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    return-object v1
.end method

.method protected getAmountDueLong()J
    .locals 2

    .line 213
    invoke-virtual {p0}, Lcom/squareup/payment/OrderSnapshot;->isRehydrated()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/squareup/payment/OrderSnapshot;->total:J

    goto :goto_0

    :cond_0
    invoke-super {p0}, Lcom/squareup/payment/Order;->getAmountDueLong()J

    move-result-wide v0

    :goto_0
    return-wide v0
.end method

.method public getItemizationsForRequest()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/Itemization;",
            ">;"
        }
    .end annotation

    .line 186
    invoke-virtual {p0}, Lcom/squareup/payment/OrderSnapshot;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v0

    .line 187
    invoke-virtual {v0}, Lcom/squareup/calc/Adjuster;->calculate()V

    .line 189
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 190
    invoke-virtual {p0}, Lcom/squareup/payment/OrderSnapshot;->getItems()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/CartItem;

    .line 191
    invoke-virtual {v0, v3}, Lcom/squareup/calc/Adjuster;->getAdjustedItemFor(Lcom/squareup/calc/order/Item;)Lcom/squareup/calc/AdjustedItem;

    move-result-object v4

    .line 192
    invoke-virtual {p0}, Lcom/squareup/payment/OrderSnapshot;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v5

    .line 191
    invoke-virtual {v3, v4, v5}, Lcom/squareup/checkout/CartItem;->buildItemization(Lcom/squareup/calc/AdjustedItem;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/server/payment/Itemization;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public getOrderAdjustments()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderAdjustment;",
            ">;"
        }
    .end annotation

    .line 147
    invoke-virtual {p0}, Lcom/squareup/payment/OrderSnapshot;->isRehydrated()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/payment/OrderSnapshot;->orderAdjustmentsSnapshot:Ljava/util/List;

    goto :goto_0

    .line 149
    :cond_0
    invoke-super {p0}, Lcom/squareup/payment/Order;->getOrderAdjustments()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public invalidate()V
    .locals 2

    .line 217
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot invalidate a SnapShot."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isRehydrated()Z
    .locals 1

    .line 227
    iget-object v0, p0, Lcom/squareup/payment/OrderSnapshot;->orderAdjustmentsSnapshot:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public newSnapshotWithCoupon(Lcom/squareup/protos/client/coupons/Coupon;Z)Lcom/squareup/payment/OrderSnapshot;
    .locals 1

    .line 158
    invoke-static {p0}, Lcom/squareup/payment/Order$Builder;->fromOrder(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order$Builder;->build()Lcom/squareup/payment/Order;

    move-result-object v0

    .line 159
    invoke-virtual {v0, p1, p2}, Lcom/squareup/payment/Order;->addCoupon(Lcom/squareup/protos/client/coupons/Coupon;Z)V

    .line 160
    invoke-virtual {v0}, Lcom/squareup/payment/Order;->snapshot()Lcom/squareup/payment/OrderSnapshot;

    move-result-object p1

    return-object p1
.end method
