.class interface abstract Lcom/squareup/payment/BillPayment$BillPaymentStrategy;
.super Ljava/lang/Object;
.source "BillPayment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/BillPayment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "BillPaymentStrategy"
.end annotation


# virtual methods
.method public abstract addTenders(Lcom/squareup/protos/client/bills/AddTendersRequest;)Lcom/squareup/protos/client/bills/AddTendersResponse;
.end method

.method public abstract canExitStrategy()Z
.end method

.method public abstract doCapture(Lcom/squareup/payment/BillPayment;Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation
.end method

.method public abstract enqueueAttachContactTask(Lcom/squareup/payment/tender/BaseTender;)V
.end method

.method public abstract enqueueLastReceipt(Lcom/squareup/payment/BillPayment;Lcom/squareup/payment/tender/BaseTender;)V
.end method

.method public abstract hasOfflineTenders()Z
.end method

.method public abstract onDropTender(Lcom/squareup/payment/tender/BaseTender;)V
.end method
