.class public final Lcom/squareup/payment/AddedTenderEvent;
.super Ljava/lang/Object;
.source "AddedTenderEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0018\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0019\u001a\u00020\u000e2\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001b\u001a\u00020\u001cH\u00d6\u0001J\t\u0010\u001d\u001a\u00020\u001eH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0010\u0010\u0006\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0013\u0010\r\u001a\u00020\u000e8G\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000fR\u0010\u0010\u0010\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0011\u001a\u00020\u0012\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/payment/AddedTenderEvent;",
        "",
        "addedTender",
        "Lcom/squareup/protos/client/bills/AddedTender;",
        "cart",
        "Lcom/squareup/protos/client/bills/Cart;",
        "billIdPair",
        "Lcom/squareup/protos/client/IdPair;",
        "(Lcom/squareup/protos/client/bills/AddedTender;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/IdPair;)V",
        "getAddedTender",
        "()Lcom/squareup/protos/client/bills/AddedTender;",
        "getCart",
        "()Lcom/squareup/protos/client/bills/Cart;",
        "hasServerId",
        "",
        "()Z",
        "tenderIdPair",
        "tenderType",
        "Lcom/squareup/protos/client/bills/Tender$Type;",
        "getTenderType",
        "()Lcom/squareup/protos/client/bills/Tender$Type;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final addedTender:Lcom/squareup/protos/client/bills/AddedTender;

.field public final billIdPair:Lcom/squareup/protos/client/IdPair;

.field private final cart:Lcom/squareup/protos/client/bills/Cart;

.field private final hasServerId:Z

.field public final tenderIdPair:Lcom/squareup/protos/client/IdPair;

.field private final tenderType:Lcom/squareup/protos/client/bills/Tender$Type;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/bills/AddedTender;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/IdPair;)V
    .locals 1

    const-string v0, "addedTender"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cart"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "billIdPair"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/payment/AddedTenderEvent;->addedTender:Lcom/squareup/protos/client/bills/AddedTender;

    iput-object p2, p0, Lcom/squareup/payment/AddedTenderEvent;->cart:Lcom/squareup/protos/client/bills/Cart;

    iput-object p3, p0, Lcom/squareup/payment/AddedTenderEvent;->billIdPair:Lcom/squareup/protos/client/IdPair;

    .line 16
    iget-object p1, p0, Lcom/squareup/payment/AddedTenderEvent;->addedTender:Lcom/squareup/protos/client/bills/AddedTender;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddedTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    iput-boolean p1, p0, Lcom/squareup/payment/AddedTenderEvent;->hasServerId:Z

    .line 18
    iget-object p1, p0, Lcom/squareup/payment/AddedTenderEvent;->addedTender:Lcom/squareup/protos/client/bills/AddedTender;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddedTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    if-nez p1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    iput-object p1, p0, Lcom/squareup/payment/AddedTenderEvent;->tenderIdPair:Lcom/squareup/protos/client/IdPair;

    .line 20
    iget-object p1, p0, Lcom/squareup/payment/AddedTenderEvent;->addedTender:Lcom/squareup/protos/client/bills/AddedTender;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddedTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Tender;->type:Lcom/squareup/protos/client/bills/Tender$Type;

    if-nez p1, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    iput-object p1, p0, Lcom/squareup/payment/AddedTenderEvent;->tenderType:Lcom/squareup/protos/client/bills/Tender$Type;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/payment/AddedTenderEvent;Lcom/squareup/protos/client/bills/AddedTender;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/IdPair;ILjava/lang/Object;)Lcom/squareup/payment/AddedTenderEvent;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/payment/AddedTenderEvent;->addedTender:Lcom/squareup/protos/client/bills/AddedTender;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/payment/AddedTenderEvent;->cart:Lcom/squareup/protos/client/bills/Cart;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/payment/AddedTenderEvent;->billIdPair:Lcom/squareup/protos/client/IdPair;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/payment/AddedTenderEvent;->copy(Lcom/squareup/protos/client/bills/AddedTender;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/IdPair;)Lcom/squareup/payment/AddedTenderEvent;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/protos/client/bills/AddedTender;
    .locals 1

    iget-object v0, p0, Lcom/squareup/payment/AddedTenderEvent;->addedTender:Lcom/squareup/protos/client/bills/AddedTender;

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/client/bills/Cart;
    .locals 1

    iget-object v0, p0, Lcom/squareup/payment/AddedTenderEvent;->cart:Lcom/squareup/protos/client/bills/Cart;

    return-object v0
.end method

.method public final component3()Lcom/squareup/protos/client/IdPair;
    .locals 1

    iget-object v0, p0, Lcom/squareup/payment/AddedTenderEvent;->billIdPair:Lcom/squareup/protos/client/IdPair;

    return-object v0
.end method

.method public final copy(Lcom/squareup/protos/client/bills/AddedTender;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/IdPair;)Lcom/squareup/payment/AddedTenderEvent;
    .locals 1

    const-string v0, "addedTender"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cart"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "billIdPair"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/payment/AddedTenderEvent;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/payment/AddedTenderEvent;-><init>(Lcom/squareup/protos/client/bills/AddedTender;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/IdPair;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/payment/AddedTenderEvent;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/payment/AddedTenderEvent;

    iget-object v0, p0, Lcom/squareup/payment/AddedTenderEvent;->addedTender:Lcom/squareup/protos/client/bills/AddedTender;

    iget-object v1, p1, Lcom/squareup/payment/AddedTenderEvent;->addedTender:Lcom/squareup/protos/client/bills/AddedTender;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/payment/AddedTenderEvent;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v1, p1, Lcom/squareup/payment/AddedTenderEvent;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/payment/AddedTenderEvent;->billIdPair:Lcom/squareup/protos/client/IdPair;

    iget-object p1, p1, Lcom/squareup/payment/AddedTenderEvent;->billIdPair:Lcom/squareup/protos/client/IdPair;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAddedTender()Lcom/squareup/protos/client/bills/AddedTender;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/payment/AddedTenderEvent;->addedTender:Lcom/squareup/protos/client/bills/AddedTender;

    return-object v0
.end method

.method public final getCart()Lcom/squareup/protos/client/bills/Cart;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/payment/AddedTenderEvent;->cart:Lcom/squareup/protos/client/bills/Cart;

    return-object v0
.end method

.method public final getTenderType()Lcom/squareup/protos/client/bills/Tender$Type;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/payment/AddedTenderEvent;->tenderType:Lcom/squareup/protos/client/bills/Tender$Type;

    return-object v0
.end method

.method public final hasServerId()Z
    .locals 1

    .line 16
    iget-boolean v0, p0, Lcom/squareup/payment/AddedTenderEvent;->hasServerId:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/payment/AddedTenderEvent;->addedTender:Lcom/squareup/protos/client/bills/AddedTender;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/payment/AddedTenderEvent;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/payment/AddedTenderEvent;->billIdPair:Lcom/squareup/protos/client/IdPair;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AddedTenderEvent(addedTender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/payment/AddedTenderEvent;->addedTender:Lcom/squareup/protos/client/bills/AddedTender;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", cart="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/payment/AddedTenderEvent;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", billIdPair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/payment/AddedTenderEvent;->billIdPair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
