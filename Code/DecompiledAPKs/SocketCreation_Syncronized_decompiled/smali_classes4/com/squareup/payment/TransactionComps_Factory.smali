.class public final Lcom/squareup/payment/TransactionComps_Factory;
.super Ljava/lang/Object;
.source "TransactionComps_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/TransactionComps;",
        ">;"
    }
.end annotation


# instance fields
.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final voidCompSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/payment/TransactionComps_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/payment/TransactionComps_Factory;->resProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/payment/TransactionComps_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/squareup/payment/TransactionComps_Factory;->voidCompSettingsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/TransactionComps_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;)",
            "Lcom/squareup/payment/TransactionComps_Factory;"
        }
    .end annotation

    .line 45
    new-instance v0, Lcom/squareup/payment/TransactionComps_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/payment/TransactionComps_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/util/Res;Lcom/squareup/payment/Transaction;Lcom/squareup/tickets/voidcomp/VoidCompSettings;)Lcom/squareup/payment/TransactionComps;
    .locals 1

    .line 50
    new-instance v0, Lcom/squareup/payment/TransactionComps;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/payment/TransactionComps;-><init>(Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/util/Res;Lcom/squareup/payment/Transaction;Lcom/squareup/tickets/voidcomp/VoidCompSettings;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/payment/TransactionComps;
    .locals 4

    .line 38
    iget-object v0, p0, Lcom/squareup/payment/TransactionComps_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v1, p0, Lcom/squareup/payment/TransactionComps_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/payment/TransactionComps_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/payment/Transaction;

    iget-object v3, p0, Lcom/squareup/payment/TransactionComps_Factory;->voidCompSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/payment/TransactionComps_Factory;->newInstance(Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/util/Res;Lcom/squareup/payment/Transaction;Lcom/squareup/tickets/voidcomp/VoidCompSettings;)Lcom/squareup/payment/TransactionComps;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/payment/TransactionComps_Factory;->get()Lcom/squareup/payment/TransactionComps;

    move-result-object v0

    return-object v0
.end method
