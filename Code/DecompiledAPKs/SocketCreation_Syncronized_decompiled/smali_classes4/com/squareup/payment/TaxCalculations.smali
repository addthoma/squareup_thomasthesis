.class public Lcom/squareup/payment/TaxCalculations;
.super Ljava/lang/Object;
.source "TaxCalculations.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static calculateAfterApplicationItemizationsTotalAmount(Lcom/squareup/payment/Order;Lcom/squareup/checkout/Tax;)J
    .locals 8

    .line 48
    invoke-static {p0, p1}, Lcom/squareup/payment/TaxCalculations;->getTotalAmountOfItemizationsHavingTax(Lcom/squareup/payment/Order;Lcom/squareup/checkout/Tax;)J

    move-result-wide v0

    .line 51
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/CartItem;

    .line 52
    invoke-virtual {p0, v3}, Lcom/squareup/payment/Order;->getAdjustmentAmountsByIdForItem(Lcom/squareup/checkout/CartItem;)Ljava/util/Map;

    move-result-object v4

    .line 55
    iget-object v5, p1, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    goto :goto_0

    .line 59
    :cond_1
    invoke-virtual {v3}, Lcom/squareup/checkout/CartItem;->appliedTaxes()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/checkout/Tax;

    .line 60
    iget-object v6, v5, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    iget-object v7, p1, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 61
    invoke-virtual {v5}, Lcom/squareup/checkout/Tax;->phase()Lcom/squareup/calc/constants/CalculationPhase;

    move-result-object v6

    invoke-virtual {p1}, Lcom/squareup/checkout/Tax;->phase()Lcom/squareup/calc/constants/CalculationPhase;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/squareup/calc/constants/CalculationPhase;->compareTo(Ljava/lang/Enum;)I

    move-result v6

    if-ltz v6, :cond_2

    .line 62
    iget-object v5, v5, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    sub-long/2addr v0, v5

    goto :goto_1

    :cond_3
    return-wide v0
.end method

.method public static calculateAppliedToAmount(Lcom/squareup/payment/Order;Lcom/squareup/checkout/Tax;)J
    .locals 8

    .line 19
    invoke-static {p0, p1}, Lcom/squareup/payment/TaxCalculations;->getTotalAmountOfItemizationsHavingTax(Lcom/squareup/payment/Order;Lcom/squareup/checkout/Tax;)J

    move-result-wide v0

    .line 22
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/CartItem;

    .line 23
    invoke-virtual {p0, v3}, Lcom/squareup/payment/Order;->getAdjustmentAmountsByIdForItem(Lcom/squareup/checkout/CartItem;)Ljava/util/Map;

    move-result-object v4

    .line 26
    iget-object v5, p1, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    goto :goto_0

    .line 30
    :cond_1
    invoke-virtual {v3}, Lcom/squareup/checkout/CartItem;->appliedTaxes()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/checkout/Tax;

    .line 31
    invoke-virtual {v5}, Lcom/squareup/checkout/Tax;->phase()Lcom/squareup/calc/constants/CalculationPhase;

    move-result-object v6

    invoke-virtual {p1}, Lcom/squareup/checkout/Tax;->phase()Lcom/squareup/calc/constants/CalculationPhase;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/squareup/calc/constants/CalculationPhase;->compareTo(Ljava/lang/Enum;)I

    move-result v6

    if-ltz v6, :cond_2

    .line 32
    iget-object v5, v5, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    sub-long/2addr v0, v5

    goto :goto_1

    :cond_3
    return-wide v0
.end method

.method protected static getTotalAmountOfItemizationsHavingTax(Lcom/squareup/payment/Order;Lcom/squareup/checkout/Tax;)J
    .locals 8

    .line 76
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const-wide/16 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/CartItem;

    const/4 v4, 0x0

    .line 78
    invoke-virtual {v3}, Lcom/squareup/checkout/CartItem;->appliedTaxes()Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/checkout/Tax;

    .line 79
    iget-object v6, v6, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    iget-object v7, p1, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v4, 0x1

    :cond_2
    if-eqz v4, :cond_0

    .line 86
    invoke-virtual {p0, v3}, Lcom/squareup/payment/Order;->getAdjustedItemFor(Lcom/squareup/checkout/CartItem;)Lcom/squareup/calc/AdjustedItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/calc/AdjustedItem;->getAdjustedTotal()J

    move-result-wide v3

    add-long/2addr v1, v3

    goto :goto_0

    :cond_3
    return-wide v1
.end method
