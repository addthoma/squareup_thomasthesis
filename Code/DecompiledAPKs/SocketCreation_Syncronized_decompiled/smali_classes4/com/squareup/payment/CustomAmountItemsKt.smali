.class public final Lcom/squareup/payment/CustomAmountItemsKt;
.super Ljava/lang/Object;
.source "CustomAmountItems.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCustomAmountItems.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CustomAmountItems.kt\ncom/squareup/payment/CustomAmountItemsKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,58:1\n704#2:59\n777#2,2:60\n1099#2,2:62\n1127#2,4:64\n*E\n*S KotlinDebug\n*F\n+ 1 CustomAmountItems.kt\ncom/squareup/payment/CustomAmountItemsKt\n*L\n52#1:59\n52#1,2:60\n56#1,2:62\n56#1,4:64\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001aN\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u00052\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u000c0\u00052\u0008\u0010\r\u001a\u0004\u0018\u00010\u000e\u001a<\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005*\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u00052\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t2\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0007\u00a8\u0006\u0010"
    }
    d2 = {
        "buildCustomAmountItem",
        "Lcom/squareup/checkout/CartItem;",
        "price",
        "Lcom/squareup/protos/common/Money;",
        "availableTaxes",
        "",
        "",
        "Lcom/squareup/checkout/Tax;",
        "availableTaxRules",
        "",
        "Lcom/squareup/payment/OrderTaxRule;",
        "cartAppliedDiscounts",
        "Lcom/squareup/checkout/Discount;",
        "cartLevelDiningOption",
        "Lcom/squareup/checkout/DiningOption;",
        "taxesThatApplyToCustomAmounts",
        "transaction_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final buildCustomAmountItem(Lcom/squareup/protos/common/Money;Ljava/util/Map;Ljava/util/List;Ljava/util/Map;Lcom/squareup/checkout/DiningOption;)Lcom/squareup/checkout/CartItem;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/squareup/checkout/Tax;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/payment/OrderTaxRule;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;",
            "Lcom/squareup/checkout/DiningOption;",
            ")",
            "Lcom/squareup/checkout/CartItem;"
        }
    .end annotation

    const-string v0, "price"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "availableTaxes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "availableTaxRules"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cartAppliedDiscounts"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-static {p1, p2, p4}, Lcom/squareup/payment/CustomAmountItemsKt;->taxesThatApplyToCustomAmounts(Ljava/util/Map;Ljava/util/List;Lcom/squareup/checkout/DiningOption;)Ljava/util/Map;

    move-result-object p1

    .line 26
    new-instance p2, Lcom/squareup/checkout/CartItem$Builder;

    invoke-direct {p2}, Lcom/squareup/checkout/CartItem$Builder;-><init>()V

    .line 28
    new-instance p4, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;

    invoke-direct {p4}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;-><init>()V

    .line 30
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;-><init>()V

    .line 31
    sget-object v1, Lcom/squareup/checkout/OrderVariation;->CUSTOM_ITEM_VARIATION:Lcom/squareup/checkout/OrderVariation;

    const-string v2, "CUSTOM_ITEM_VARIATION"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/checkout/OrderVariation;->getItemVariation()Lcom/squareup/api/items/ItemVariation;

    move-result-object v1

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;->item_variation(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;

    move-result-object v0

    .line 32
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    move-result-object v0

    .line 29
    invoke-virtual {p4, v0}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->available_options(Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;

    move-result-object p4

    .line 34
    invoke-virtual {p4}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    move-result-object p4

    .line 27
    invoke-virtual {p2, p4}, Lcom/squareup/checkout/CartItem$Builder;->backingDetails(Lcom/squareup/protos/client/bills/Itemization$BackingDetails;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p2

    .line 36
    invoke-virtual {p2, p3}, Lcom/squareup/checkout/CartItem$Builder;->appliedDiscounts(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p2

    .line 37
    sget-object p3, Lcom/squareup/checkout/OrderVariation;->CUSTOM_ITEM_VARIATION:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {p2, p3}, Lcom/squareup/checkout/CartItem$Builder;->selectedVariation(Lcom/squareup/checkout/OrderVariation;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p2

    .line 38
    invoke-virtual {p2, p1}, Lcom/squareup/checkout/CartItem$Builder;->appliedTaxes(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    .line 39
    invoke-virtual {p1, p0}, Lcom/squareup/checkout/CartItem$Builder;->variablePrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p0

    .line 40
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p0

    const-string p1, "CartItem.Builder()\n     \u2026ice(price)\n      .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final taxesThatApplyToCustomAmounts(Ljava/util/Map;Ljava/util/List;Lcom/squareup/checkout/DiningOption;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/squareup/checkout/Tax;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/payment/OrderTaxRule;",
            ">;",
            "Lcom/squareup/checkout/DiningOption;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$taxesThatApplyToCustomAmounts"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "availableTaxRules"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p0

    check-cast p0, Ljava/lang/Iterable;

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 60
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/checkout/Tax;

    .line 54
    iget-boolean v3, v2, Lcom/squareup/checkout/Tax;->enabled:Z

    if-eqz v3, :cond_1

    iget-boolean v3, v2, Lcom/squareup/checkout/Tax;->appliesToCustomAmounts:Z

    if-eqz v3, :cond_1

    invoke-static {v2, p1, p2}, Lcom/squareup/payment/TransactionTaxesKt;->isDisabledForCustomItem(Lcom/squareup/checkout/Tax;Ljava/util/List;Lcom/squareup/checkout/DiningOption;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 61
    :cond_2
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    const/16 p0, 0xa

    .line 62
    invoke-static {v0, p0}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result p0

    invoke-static {p0}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result p0

    const/16 p1, 0x10

    invoke-static {p0, p1}, Lkotlin/ranges/RangesKt;->coerceAtLeast(II)I

    move-result p0

    .line 63
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1, p0}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast p1, Ljava/util/Map;

    .line 64
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_2
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    .line 65
    move-object v0, p2

    check-cast v0, Lcom/squareup/checkout/Tax;

    .line 56
    iget-object v0, v0, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {p1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_3
    return-object p1
.end method
