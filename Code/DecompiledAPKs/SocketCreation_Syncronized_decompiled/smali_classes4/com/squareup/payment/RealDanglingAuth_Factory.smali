.class public final Lcom/squareup/payment/RealDanglingAuth_Factory;
.super Ljava/lang/Object;
.source "RealDanglingAuth_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/RealDanglingAuth;",
        ">;"
    }
.end annotation


# instance fields
.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final lastAuthKeyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistent/AtomicSyncedValue<",
            "Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field private final ohSnapLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentAccuracyLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentAccuracyLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final serverClockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/ServerClock;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final tasksProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionLedgerManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;"
        }
    .end annotation
.end field

.field private final voidMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/VoidMonitor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/ServerClock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistent/AtomicSyncedValue<",
            "Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentAccuracyLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/VoidMonitor;",
            ">;)V"
        }
    .end annotation

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/squareup/payment/RealDanglingAuth_Factory;->serverClockProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p2, p0, Lcom/squareup/payment/RealDanglingAuth_Factory;->lastAuthKeyProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p3, p0, Lcom/squareup/payment/RealDanglingAuth_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p4, p0, Lcom/squareup/payment/RealDanglingAuth_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p5, p0, Lcom/squareup/payment/RealDanglingAuth_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p6, p0, Lcom/squareup/payment/RealDanglingAuth_Factory;->tasksProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p7, p0, Lcom/squareup/payment/RealDanglingAuth_Factory;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p8, p0, Lcom/squareup/payment/RealDanglingAuth_Factory;->paymentAccuracyLoggerProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p9, p0, Lcom/squareup/payment/RealDanglingAuth_Factory;->voidMonitorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/RealDanglingAuth_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/ServerClock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistent/AtomicSyncedValue<",
            "Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentAccuracyLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/VoidMonitor;",
            ">;)",
            "Lcom/squareup/payment/RealDanglingAuth_Factory;"
        }
    .end annotation

    .line 71
    new-instance v10, Lcom/squareup/payment/RealDanglingAuth_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/payment/RealDanglingAuth_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Lcom/squareup/account/ServerClock;Lcom/squareup/persistent/AtomicSyncedValue;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/settings/server/AccountStatusSettings;Ljavax/inject/Provider;Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/payment/PaymentAccuracyLogger;Lcom/squareup/payment/VoidMonitor;)Lcom/squareup/payment/RealDanglingAuth;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/account/ServerClock;",
            "Lcom/squareup/persistent/AtomicSyncedValue<",
            "Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/log/OhSnapLogger;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            "Lcom/squareup/payment/PaymentAccuracyLogger;",
            "Lcom/squareup/payment/VoidMonitor;",
            ")",
            "Lcom/squareup/payment/RealDanglingAuth;"
        }
    .end annotation

    .line 79
    new-instance v10, Lcom/squareup/payment/RealDanglingAuth;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/payment/RealDanglingAuth;-><init>(Lcom/squareup/account/ServerClock;Lcom/squareup/persistent/AtomicSyncedValue;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/settings/server/AccountStatusSettings;Ljavax/inject/Provider;Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/payment/PaymentAccuracyLogger;Lcom/squareup/payment/VoidMonitor;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/payment/RealDanglingAuth;
    .locals 10

    .line 61
    iget-object v0, p0, Lcom/squareup/payment/RealDanglingAuth_Factory;->serverClockProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/account/ServerClock;

    iget-object v0, p0, Lcom/squareup/payment/RealDanglingAuth_Factory;->lastAuthKeyProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/persistent/AtomicSyncedValue;

    iget-object v0, p0, Lcom/squareup/payment/RealDanglingAuth_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v0, p0, Lcom/squareup/payment/RealDanglingAuth_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/log/OhSnapLogger;

    iget-object v0, p0, Lcom/squareup/payment/RealDanglingAuth_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v6, p0, Lcom/squareup/payment/RealDanglingAuth_Factory;->tasksProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/payment/RealDanglingAuth_Factory;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/payment/ledger/TransactionLedgerManager;

    iget-object v0, p0, Lcom/squareup/payment/RealDanglingAuth_Factory;->paymentAccuracyLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/payment/PaymentAccuracyLogger;

    iget-object v0, p0, Lcom/squareup/payment/RealDanglingAuth_Factory;->voidMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/payment/VoidMonitor;

    invoke-static/range {v1 .. v9}, Lcom/squareup/payment/RealDanglingAuth_Factory;->newInstance(Lcom/squareup/account/ServerClock;Lcom/squareup/persistent/AtomicSyncedValue;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/settings/server/AccountStatusSettings;Ljavax/inject/Provider;Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/payment/PaymentAccuracyLogger;Lcom/squareup/payment/VoidMonitor;)Lcom/squareup/payment/RealDanglingAuth;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/payment/RealDanglingAuth_Factory;->get()Lcom/squareup/payment/RealDanglingAuth;

    move-result-object v0

    return-object v0
.end method
