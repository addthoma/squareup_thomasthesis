.class Lcom/squareup/payment/offline/StoreAndForwardTask$2$1;
.super Lcom/squareup/payment/offline/StoreAndForwardTask$ErrorCallbackRunnable;
.source "StoreAndForwardTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/payment/offline/StoreAndForwardTask$2;->doRun(Lcom/squareup/server/SquareCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/payment/offline/StoreAndForwardTask$2;

.field final synthetic val$batch:Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;


# direct methods
.method constructor <init>(Lcom/squareup/payment/offline/StoreAndForwardTask$2;Lcom/squareup/server/SquareCallback;Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;)V
    .locals 0

    .line 161
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$2$1;->this$1:Lcom/squareup/payment/offline/StoreAndForwardTask$2;

    iput-object p3, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$2$1;->val$batch:Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;

    iget-object p1, p1, Lcom/squareup/payment/offline/StoreAndForwardTask$2;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    invoke-direct {p0, p1, p2}, Lcom/squareup/payment/offline/StoreAndForwardTask$ErrorCallbackRunnable;-><init>(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/server/SquareCallback;)V

    return-void
.end method


# virtual methods
.method protected doRun(Lcom/squareup/server/SquareCallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    .line 164
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$2$1;->val$batch:Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;

    invoke-virtual {v0}, Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "QueueBert: nothing to do"

    .line 165
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 166
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$2$1;->this$1:Lcom/squareup/payment/offline/StoreAndForwardTask$2;

    iget-object v0, v0, Lcom/squareup/payment/offline/StoreAndForwardTask$2;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    invoke-static {v0}, Lcom/squareup/payment/offline/StoreAndForwardTask;->access$200(Lcom/squareup/payment/offline/StoreAndForwardTask;)Lcom/squareup/payment/offline/StoreAndForwardTask$UserData;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/offline/StoreAndForwardTask$UserData;->getTransactionLedgerManager()Lcom/squareup/payment/ledger/TransactionLedgerManager;

    move-result-object v0

    const-string v1, "Batch is empty"

    .line 167
    invoke-interface {v0, v1}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logStoreAndForwardTaskStatus(Ljava/lang/String;)V

    .line 168
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$2$1;->this$1:Lcom/squareup/payment/offline/StoreAndForwardTask$2;

    iget-object v0, v0, Lcom/squareup/payment/offline/StoreAndForwardTask$2;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    iget-object v0, v0, Lcom/squareup/payment/offline/StoreAndForwardTask;->storedPaymentNotifier:Lcom/squareup/notifications/StoredPaymentNotifier;

    invoke-interface {v0}, Lcom/squareup/notifications/StoredPaymentNotifier;->hideNotification()V

    .line 169
    new-instance v0, Lcom/squareup/server/SimpleResponse;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V

    invoke-virtual {p1, v0}, Lcom/squareup/server/SquareCallback;->call(Ljava/lang/Object;)V

    return-void

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$2$1;->this$1:Lcom/squareup/payment/offline/StoreAndForwardTask$2;

    iget-object v0, v0, Lcom/squareup/payment/offline/StoreAndForwardTask$2;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    invoke-static {v0}, Lcom/squareup/payment/offline/StoreAndForwardTask;->access$200(Lcom/squareup/payment/offline/StoreAndForwardTask;)Lcom/squareup/payment/offline/StoreAndForwardTask$UserData;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/offline/StoreAndForwardTask$UserData;->getTransactionLedgerManager()Lcom/squareup/payment/ledger/TransactionLedgerManager;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Batch contains "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$2$1;->val$batch:Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;

    .line 175
    invoke-static {v2}, Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;->access$300(Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " stored payments"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 174
    invoke-interface {v0, v1}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logStoreAndForwardTaskStatus(Ljava/lang/String;)V

    .line 176
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$2$1;->this$1:Lcom/squareup/payment/offline/StoreAndForwardTask$2;

    iget-object v0, v0, Lcom/squareup/payment/offline/StoreAndForwardTask$2;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    iget-object v1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$2$1;->val$batch:Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;

    iget-object v2, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$2$1;->this$1:Lcom/squareup/payment/offline/StoreAndForwardTask$2;

    iget-object v2, v2, Lcom/squareup/payment/offline/StoreAndForwardTask$2;->val$storedPayments:Lcom/squareup/queue/StoredPaymentsQueue;

    invoke-static {v0, v1, v2, p1}, Lcom/squareup/payment/offline/StoreAndForwardTask;->access$400(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;Lcom/squareup/queue/StoredPaymentsQueue;Lcom/squareup/server/SquareCallback;)V

    return-void
.end method
