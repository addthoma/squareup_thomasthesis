.class public Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;
.super Lmortar/ViewPresenter;
.source "EnqueueStoredPaymentRunner.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# instance fields
.field private final badBus:Lcom/squareup/badbus/BadBus;

.field private final fileThread:Ljava/util/concurrent/Executor;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private runningJobsCount:I

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field public final showsProgress:Lcom/squareup/flowlegacy/NoResultPopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/flowlegacy/NoResultPopupPresenter<",
            "Lcom/squareup/caller/ProgressPopup$Progress;",
            ">;"
        }
    .end annotation
.end field

.field private final storeAndForwardQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

.field private final storedPayments:Lcom/squareup/queue/StoredPaymentsQueue;

.field private final transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

.field private final userId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/badbus/BadBus;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/settings/server/AccountStatusSettings;Ljava/lang/String;Lcom/squareup/queue/StoredPaymentsQueue;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/payment/ledger/TransactionLedgerManager;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 52
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 30
    new-instance v0, Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-direct {v0}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->showsProgress:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    .line 53
    iput-object p1, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->badBus:Lcom/squareup/badbus/BadBus;

    .line 54
    iput-object p2, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->fileThread:Ljava/util/concurrent/Executor;

    .line 55
    iput-object p3, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 56
    iput-object p4, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 57
    iput-object p5, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->userId:Ljava/lang/String;

    .line 58
    iput-object p6, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->storedPayments:Lcom/squareup/queue/StoredPaymentsQueue;

    .line 59
    iput-object p7, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->storeAndForwardQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    .line 60
    iput-object p8, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    return-void
.end method

.method public static synthetic lambda$LnS6GhgwHt6vgavNblKHp4fWAYM(Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;Lcom/squareup/payment/offline/StoreAndForwardPaymentService$EnqueueStoredPayment;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->onEnqueueStoredPayment(Lcom/squareup/payment/offline/StoreAndForwardPaymentService$EnqueueStoredPayment;)V

    return-void
.end method

.method private onEnqueueStoredPayment(Lcom/squareup/payment/offline/StoreAndForwardPaymentService$EnqueueStoredPayment;)V
    .locals 0

    .line 85
    iget-object p1, p1, Lcom/squareup/payment/offline/StoreAndForwardPaymentService$EnqueueStoredPayment;->payment:Lcom/squareup/payment/offline/StoredPayment;

    invoke-virtual {p0, p1}, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->run(Lcom/squareup/payment/offline/StoredPayment;)V

    return-void
.end method


# virtual methods
.method public done(Lcom/squareup/payment/offline/StoredPayment;)V
    .locals 1

    .line 142
    iget v0, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->runningJobsCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->runningJobsCount:I

    .line 143
    iget v0, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->runningJobsCount:I

    if-nez v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->showsProgress:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-virtual {v0}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->dismiss()V

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    invoke-interface {v0, p1}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logStoreAndForwardPaymentEnqueued(Lcom/squareup/payment/offline/StoredPayment;)V

    return-void
.end method

.method public enqueueTaskForDanglingStoredPayment()V
    .locals 3

    .line 116
    iget-object v0, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->storeAndForwardQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    invoke-interface {v0}, Lcom/squareup/queue/retrofit/RetrofitQueue;->size()I

    move-result v0

    if-lez v0, :cond_0

    return-void

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getName()Ljava/lang/String;

    move-result-object v0

    .line 123
    iget-object v1, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->fileThread:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/squareup/payment/offline/-$$Lambda$EnqueueStoredPaymentRunner$venLteNP1wcWaV_RYRZu_IK1ZnM;

    invoke-direct {v2, p0, v0}, Lcom/squareup/payment/offline/-$$Lambda$EnqueueStoredPaymentRunner$venLteNP1wcWaV_RYRZu_IK1ZnM;-><init>(Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic lambda$enqueueTaskForDanglingStoredPayment$3$EnqueueStoredPaymentRunner(Ljava/lang/String;)V
    .locals 3

    .line 124
    iget-object v0, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->storedPayments:Lcom/squareup/queue/StoredPaymentsQueue;

    invoke-virtual {v0}, Lcom/squareup/queue/StoredPaymentsQueue;->size()I

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 130
    :cond_0
    iget-object v1, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v2, Lcom/squareup/payment/offline/-$$Lambda$EnqueueStoredPaymentRunner$8nKMHyDymx7byipsxXWnnEAmmwo;

    invoke-direct {v2, p0, p1, v0}, Lcom/squareup/payment/offline/-$$Lambda$EnqueueStoredPaymentRunner$8nKMHyDymx7byipsxXWnnEAmmwo;-><init>(Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;Ljava/lang/String;I)V

    invoke-interface {v1, v2}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic lambda$null$0$EnqueueStoredPaymentRunner(Ljava/lang/String;Lcom/squareup/payment/offline/StoredPayment;)V
    .locals 3

    .line 108
    iget-object v0, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->storeAndForwardQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    new-instance v1, Lcom/squareup/payment/offline/StoreAndForwardTask;

    iget-object v2, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->userId:Ljava/lang/String;

    invoke-direct {v1, v2, p1}, Lcom/squareup/payment/offline/StoreAndForwardTask;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    .line 110
    invoke-virtual {p0, p2}, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->done(Lcom/squareup/payment/offline/StoredPayment;)V

    return-void
.end method

.method public synthetic lambda$null$2$EnqueueStoredPaymentRunner(Ljava/lang/String;I)V
    .locals 3

    .line 132
    iget-object v0, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->storeAndForwardQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    new-instance v1, Lcom/squareup/payment/offline/StoreAndForwardTask;

    iget-object v2, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->userId:Ljava/lang/String;

    invoke-direct {v1, v2, p1}, Lcom/squareup/payment/offline/StoreAndForwardTask;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    .line 135
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Dangling stored payments"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " dangling payments"

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$run$1$EnqueueStoredPaymentRunner(Lcom/squareup/payment/offline/StoredPayment;Ljava/lang/String;)V
    .locals 2

    .line 102
    iget-object v0, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->storedPayments:Lcom/squareup/queue/StoredPaymentsQueue;

    invoke-virtual {v0, p1}, Lcom/squareup/queue/StoredPaymentsQueue;->add(Lcom/squareup/payment/offline/StoredPayment;)V

    .line 104
    iget-object v0, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/payment/offline/-$$Lambda$EnqueueStoredPaymentRunner$6ZQm3wzlSGxuiCR4Y0LjGpgJ8ZM;

    invoke-direct {v1, p0, p2, p1}, Lcom/squareup/payment/offline/-$$Lambda$EnqueueStoredPaymentRunner$6ZQm3wzlSGxuiCR4Y0LjGpgJ8ZM;-><init>(Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;Ljava/lang/String;Lcom/squareup/payment/offline/StoredPayment;)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 64
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->badBus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/payment/offline/StoreAndForwardPaymentService$EnqueueStoredPayment;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/payment/offline/-$$Lambda$EnqueueStoredPaymentRunner$LnS6GhgwHt6vgavNblKHp4fWAYM;

    invoke-direct {v1, p0}, Lcom/squareup/payment/offline/-$$Lambda$EnqueueStoredPaymentRunner$LnS6GhgwHt6vgavNblKHp4fWAYM;-><init>(Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;)V

    .line 66
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 65
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 0

    .line 70
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    if-nez p1, :cond_0

    .line 74
    invoke-virtual {p0}, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->enqueueTaskForDanglingStoredPayment()V

    .line 77
    :cond_0
    iget p1, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->runningJobsCount:I

    if-nez p1, :cond_1

    .line 80
    iget-object p1, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->showsProgress:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-virtual {p1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->dismiss()V

    :cond_1
    return-void
.end method

.method run(Lcom/squareup/payment/offline/StoredPayment;)V
    .locals 3

    .line 89
    iget v0, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->runningJobsCount:I

    const/4 v1, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->runningJobsCount:I

    .line 91
    iget v0, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->runningJobsCount:I

    if-ne v0, v1, :cond_0

    .line 94
    iget-object v0, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->showsProgress:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    new-instance v1, Lcom/squareup/caller/ProgressPopup$Progress;

    const-string v2, ""

    invoke-direct {v1, v2}, Lcom/squareup/caller/ProgressPopup$Progress;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getName()Ljava/lang/String;

    move-result-object v0

    .line 100
    iget-object v1, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->fileThread:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/squareup/payment/offline/-$$Lambda$EnqueueStoredPaymentRunner$C3U536MuhbAr9AO8kBHZm-yGBFo;

    invoke-direct {v2, p0, p1, v0}, Lcom/squareup/payment/offline/-$$Lambda$EnqueueStoredPaymentRunner$C3U536MuhbAr9AO8kBHZm-yGBFo;-><init>(Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;Lcom/squareup/payment/offline/StoredPayment;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
