.class public Lcom/squareup/payment/offline/StoredPaymentStatusBarNotifier;
.super Ljava/lang/Object;
.source "StoredPaymentStatusBarNotifier.java"

# interfaces
.implements Lcom/squareup/notifications/StoredPaymentNotifier;


# instance fields
.field private final application:Landroid/app/Application;

.field private final notificationManager:Landroid/app/NotificationManager;

.field private final notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Landroid/app/Application;Lcom/squareup/util/Res;Landroid/app/NotificationManager;Lcom/squareup/notification/NotificationWrapper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/payment/offline/StoredPaymentStatusBarNotifier;->application:Landroid/app/Application;

    .line 30
    iput-object p2, p0, Lcom/squareup/payment/offline/StoredPaymentStatusBarNotifier;->res:Lcom/squareup/util/Res;

    .line 31
    iput-object p3, p0, Lcom/squareup/payment/offline/StoredPaymentStatusBarNotifier;->notificationManager:Landroid/app/NotificationManager;

    .line 32
    iput-object p4, p0, Lcom/squareup/payment/offline/StoredPaymentStatusBarNotifier;->notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

    return-void
.end method


# virtual methods
.method getTitle(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 56
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    iget-object p1, p0, Lcom/squareup/payment/offline/StoredPaymentStatusBarNotifier;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/transaction/R$string;->processing_payments_expiring:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPaymentStatusBarNotifier;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/transaction/R$string;->processing_payments_expiring_by_name:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "name"

    .line 61
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 62
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 63
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public hideNotification()V
    .locals 2

    .line 68
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPaymentStatusBarNotifier;->notificationManager:Landroid/app/NotificationManager;

    sget v1, Lcom/squareup/transaction/R$id;->notification_payment_logged_out:I

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    return-void
.end method

.method public showNotification(Ljava/lang/String;)V
    .locals 4

    .line 36
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPaymentStatusBarNotifier;->application:Landroid/app/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 37
    iget-object v1, p0, Lcom/squareup/payment/offline/StoredPaymentStatusBarNotifier;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/cardreader/R$string;->processing_payments_expiring_text:I

    .line 38
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 40
    iget-object v2, p0, Lcom/squareup/payment/offline/StoredPaymentStatusBarNotifier;->notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

    sget-object v3, Lcom/squareup/notification/Channels;->PAYMENTS:Lcom/squareup/notification/Channels;

    invoke-virtual {v2, v0, v3}, Lcom/squareup/notification/NotificationWrapper;->getNotificationBuilder(Landroid/content/Context;Lcom/squareup/notification/Channel;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    const/4 v2, 0x2

    .line 41
    invoke-virtual {v0, v2}, Landroidx/core/app/NotificationCompat$Builder;->setPriority(I)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    .line 42
    invoke-virtual {p0, p1}, Lcom/squareup/payment/offline/StoredPaymentStatusBarNotifier;->getTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroidx/core/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    .line 43
    invoke-virtual {p1, v1}, Landroidx/core/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    new-instance v0, Landroidx/core/app/NotificationCompat$BigTextStyle;

    invoke-direct {v0}, Landroidx/core/app/NotificationCompat$BigTextStyle;-><init>()V

    .line 44
    invoke-virtual {v0, v1}, Landroidx/core/app/NotificationCompat$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$BigTextStyle;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroidx/core/app/NotificationCompat$Builder;->setStyle(Landroidx/core/app/NotificationCompat$Style;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    const-wide/16 v0, 0x0

    .line 45
    invoke-virtual {p1, v0, v1}, Landroidx/core/app/NotificationCompat$Builder;->setWhen(J)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    .line 46
    invoke-virtual {p1}, Landroidx/core/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object p1

    .line 48
    iget v0, p1, Landroid/app/Notification;->flags:I

    or-int/lit8 v0, v0, 0x22

    iput v0, p1, Landroid/app/Notification;->flags:I

    .line 50
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPaymentStatusBarNotifier;->notificationManager:Landroid/app/NotificationManager;

    sget v1, Lcom/squareup/transaction/R$id;->notification_payment_logged_out:I

    invoke-virtual {v0, v1, p1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method
