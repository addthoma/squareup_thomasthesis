.class public final Lcom/squareup/payment/offline/StoreAndForwardPaymentService_Factory;
.super Ljava/lang/Object;
.source "StoreAndForwardPaymentService_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/offline/StoreAndForwardPaymentService;",
        ">;"
    }
.end annotation


# instance fields
.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;"
        }
    .end annotation
.end field

.field private final gsonProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field

.field private final merchantKeyManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/MerchantKeyManager;",
            ">;"
        }
    .end annotation
.end field

.field private final preferencesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private final queueBertPublicKeyManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/QueueBertPublicKeyManager;",
            ">;"
        }
    .end annotation
.end field

.field private final sessionTokenProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final squareHeadersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/SquareHeaders;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionLedgerManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/QueueBertPublicKeyManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/MerchantKeyManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/SquareHeaders;",
            ">;)V"
        }
    .end annotation

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService_Factory;->busProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p2, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p3, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService_Factory;->preferencesProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p4, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService_Factory;->queueBertPublicKeyManagerProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p5, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService_Factory;->merchantKeyManagerProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p6, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService_Factory;->sessionTokenProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p7, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService_Factory;->gsonProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p8, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService_Factory;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p9, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService_Factory;->squareHeadersProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/offline/StoreAndForwardPaymentService_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/QueueBertPublicKeyManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/MerchantKeyManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/SquareHeaders;",
            ">;)",
            "Lcom/squareup/payment/offline/StoreAndForwardPaymentService_Factory;"
        }
    .end annotation

    .line 72
    new-instance v10, Lcom/squareup/payment/offline/StoreAndForwardPaymentService_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/payment/offline/StoreAndForwardPaymentService_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Lcom/squareup/badbus/BadEventSink;Lcom/squareup/settings/server/AccountStatusSettings;Landroid/content/SharedPreferences;Lcom/squareup/payment/offline/QueueBertPublicKeyManager;Ljava/lang/Object;Ljavax/inject/Provider;Lcom/google/gson/Gson;Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/http/SquareHeaders;)Lcom/squareup/payment/offline/StoreAndForwardPaymentService;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/badbus/BadEventSink;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Landroid/content/SharedPreferences;",
            "Lcom/squareup/payment/offline/QueueBertPublicKeyManager;",
            "Ljava/lang/Object;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/gson/Gson;",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            "Lcom/squareup/http/SquareHeaders;",
            ")",
            "Lcom/squareup/payment/offline/StoreAndForwardPaymentService;"
        }
    .end annotation

    .line 80
    new-instance v10, Lcom/squareup/payment/offline/StoreAndForwardPaymentService;

    move-object v5, p4

    check-cast v5, Lcom/squareup/payment/offline/MerchantKeyManager;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/payment/offline/StoreAndForwardPaymentService;-><init>(Lcom/squareup/badbus/BadEventSink;Lcom/squareup/settings/server/AccountStatusSettings;Landroid/content/SharedPreferences;Lcom/squareup/payment/offline/QueueBertPublicKeyManager;Lcom/squareup/payment/offline/MerchantKeyManager;Ljavax/inject/Provider;Lcom/google/gson/Gson;Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/http/SquareHeaders;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/payment/offline/StoreAndForwardPaymentService;
    .locals 10

    .line 61
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService_Factory;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/badbus/BadEventSink;

    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService_Factory;->preferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/content/SharedPreferences;

    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService_Factory;->queueBertPublicKeyManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;

    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService_Factory;->merchantKeyManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService_Factory;->sessionTokenProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService_Factory;->gsonProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/google/gson/Gson;

    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService_Factory;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/payment/ledger/TransactionLedgerManager;

    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardPaymentService_Factory;->squareHeadersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/http/SquareHeaders;

    invoke-static/range {v1 .. v9}, Lcom/squareup/payment/offline/StoreAndForwardPaymentService_Factory;->newInstance(Lcom/squareup/badbus/BadEventSink;Lcom/squareup/settings/server/AccountStatusSettings;Landroid/content/SharedPreferences;Lcom/squareup/payment/offline/QueueBertPublicKeyManager;Ljava/lang/Object;Ljavax/inject/Provider;Lcom/google/gson/Gson;Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/http/SquareHeaders;)Lcom/squareup/payment/offline/StoreAndForwardPaymentService;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/payment/offline/StoreAndForwardPaymentService_Factory;->get()Lcom/squareup/payment/offline/StoreAndForwardPaymentService;

    move-result-object v0

    return-object v0
.end method
