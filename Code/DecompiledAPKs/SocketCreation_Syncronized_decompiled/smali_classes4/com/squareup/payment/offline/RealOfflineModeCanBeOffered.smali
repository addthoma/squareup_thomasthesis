.class public final Lcom/squareup/payment/offline/RealOfflineModeCanBeOffered;
.super Ljava/lang/Object;
.source "RealOfflineModeCanBeOffered.kt"

# interfaces
.implements Lcom/squareup/payment/offline/OfflineModeCanBeOffered;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/payment/offline/RealOfflineModeCanBeOffered;",
        "Lcom/squareup/payment/offline/OfflineModeCanBeOffered;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;)V",
        "value",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "settings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/payment/offline/RealOfflineModeCanBeOffered;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p2, p0, Lcom/squareup/payment/offline/RealOfflineModeCanBeOffered;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public value()Z
    .locals 2

    .line 15
    iget-object v0, p0, Lcom/squareup/payment/offline/RealOfflineModeCanBeOffered;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->canOptInToStoreAndForwardPayments()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17
    iget-object v0, p0, Lcom/squareup/payment/offline/RealOfflineModeCanBeOffered;->features:Lcom/squareup/settings/server/Features;

    .line 18
    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DEVICE_SETTINGS:Lcom/squareup/settings/server/Features$Feature;

    .line 17
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
