.class public Lcom/squareup/payment/offline/StoreAndForwardJobCreator;
.super Lcom/squareup/backgroundjob/BackgroundJobCreator;
.source "StoreAndForwardJobCreator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/offline/StoreAndForwardJobCreator$EnqueueStoreAndForwardTaskJob;
    }
.end annotation


# instance fields
.field private final application:Landroid/app/Application;

.field private final crossSessionStoreAndForwardQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

.field private final jobNotificationManager:Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Lcom/squareup/queue/retrofit/RetrofitQueue;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 45
    invoke-direct {p0, p3}, Lcom/squareup/backgroundjob/BackgroundJobCreator;-><init>(Lcom/squareup/backgroundjob/BackgroundJobManager;)V

    .line 46
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardJobCreator;->application:Landroid/app/Application;

    .line 47
    iput-object p2, p0, Lcom/squareup/payment/offline/StoreAndForwardJobCreator;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 48
    iput-object p4, p0, Lcom/squareup/payment/offline/StoreAndForwardJobCreator;->jobNotificationManager:Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;

    .line 49
    iput-object p5, p0, Lcom/squareup/payment/offline/StoreAndForwardJobCreator;->crossSessionStoreAndForwardQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/String;)Lcom/evernote/android/job/Job;
    .locals 4

    .line 53
    sget-object v0, Lcom/squareup/payment/offline/StoreAndForwardJobCreator$EnqueueStoreAndForwardTaskJob;->ENQUEUE_TAG:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 54
    new-instance p1, Lcom/squareup/payment/offline/StoreAndForwardJobCreator$EnqueueStoreAndForwardTaskJob;

    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardJobCreator;->application:Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/payment/offline/StoreAndForwardJobCreator;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v2, p0, Lcom/squareup/payment/offline/StoreAndForwardJobCreator;->jobNotificationManager:Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;

    iget-object v3, p0, Lcom/squareup/payment/offline/StoreAndForwardJobCreator;->crossSessionStoreAndForwardQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    invoke-direct {p1, v0, v1, v2, v3}, Lcom/squareup/payment/offline/StoreAndForwardJobCreator$EnqueueStoreAndForwardTaskJob;-><init>(Landroid/app/Application;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Lcom/squareup/queue/retrofit/RetrofitQueue;)V

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method
