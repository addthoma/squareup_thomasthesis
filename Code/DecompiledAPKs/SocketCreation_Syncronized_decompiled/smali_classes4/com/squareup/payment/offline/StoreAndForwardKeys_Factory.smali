.class public final Lcom/squareup/payment/offline/StoreAndForwardKeys_Factory;
.super Ljava/lang/Object;
.source "StoreAndForwardKeys_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/offline/StoreAndForwardKeys;",
        ">;"
    }
.end annotation


# instance fields
.field private final merchantKeyManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/MerchantKeyManager;",
            ">;"
        }
    .end annotation
.end field

.field private final queueBertPublicKeyManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/QueueBertPublicKeyManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/QueueBertPublicKeyManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/MerchantKeyManager;",
            ">;)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardKeys_Factory;->queueBertPublicKeyManagerProvider:Ljavax/inject/Provider;

    .line 20
    iput-object p2, p0, Lcom/squareup/payment/offline/StoreAndForwardKeys_Factory;->merchantKeyManagerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/offline/StoreAndForwardKeys_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/QueueBertPublicKeyManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/MerchantKeyManager;",
            ">;)",
            "Lcom/squareup/payment/offline/StoreAndForwardKeys_Factory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/payment/offline/StoreAndForwardKeys_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/payment/offline/StoreAndForwardKeys_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/payment/offline/QueueBertPublicKeyManager;Ljava/lang/Object;)Lcom/squareup/payment/offline/StoreAndForwardKeys;
    .locals 1

    .line 36
    new-instance v0, Lcom/squareup/payment/offline/StoreAndForwardKeys;

    check-cast p1, Lcom/squareup/payment/offline/MerchantKeyManager;

    invoke-direct {v0, p0, p1}, Lcom/squareup/payment/offline/StoreAndForwardKeys;-><init>(Lcom/squareup/payment/offline/QueueBertPublicKeyManager;Lcom/squareup/payment/offline/MerchantKeyManager;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/payment/offline/StoreAndForwardKeys;
    .locals 2

    .line 25
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardKeys_Factory;->queueBertPublicKeyManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;

    iget-object v1, p0, Lcom/squareup/payment/offline/StoreAndForwardKeys_Factory;->merchantKeyManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/payment/offline/StoreAndForwardKeys_Factory;->newInstance(Lcom/squareup/payment/offline/QueueBertPublicKeyManager;Ljava/lang/Object;)Lcom/squareup/payment/offline/StoreAndForwardKeys;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/payment/offline/StoreAndForwardKeys_Factory;->get()Lcom/squareup/payment/offline/StoreAndForwardKeys;

    move-result-object v0

    return-object v0
.end method
