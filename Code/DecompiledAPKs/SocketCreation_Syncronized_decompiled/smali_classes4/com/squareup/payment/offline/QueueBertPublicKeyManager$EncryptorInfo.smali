.class Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;
.super Ljava/lang/Object;
.source "QueueBertPublicKeyManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/offline/QueueBertPublicKeyManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "EncryptorInfo"
.end annotation


# instance fields
.field encryptor:Lcom/squareup/encryption/JweEncryptor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/encryption/JweEncryptor<",
            "Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;",
            ">;"
        }
    .end annotation
.end field

.field publicKey:Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;


# direct methods
.method constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
