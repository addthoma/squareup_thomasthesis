.class final Lcom/squareup/payment/DefaultCartSort$Companion$Comparator$1;
.super Ljava/lang/Object;
.source "DefaultCartSort.kt"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/DefaultCartSort;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/squareup/checkout/CartItem;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "lhs",
        "Lcom/squareup/checkout/CartItem;",
        "rhs",
        "compare"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/payment/DefaultCartSort$Companion$Comparator$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/payment/DefaultCartSort$Companion$Comparator$1;

    invoke-direct {v0}, Lcom/squareup/payment/DefaultCartSort$Companion$Comparator$1;-><init>()V

    sput-object v0, Lcom/squareup/payment/DefaultCartSort$Companion$Comparator$1;->INSTANCE:Lcom/squareup/payment/DefaultCartSort$Companion$Comparator$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Lcom/squareup/checkout/CartItem;Lcom/squareup/checkout/CartItem;)I
    .locals 8

    const-string v0, "lhs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rhs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    sget-object v1, Lcom/squareup/payment/DefaultCartSort;->Companion:Lcom/squareup/payment/DefaultCartSort$Companion;

    .line 12
    iget-object v2, p1, Lcom/squareup/checkout/CartItem;->createdAt:Ljava/util/Date;

    iget-object v3, p1, Lcom/squareup/checkout/CartItem;->itemName:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->isVoided()Z

    move-result v4

    iget-object v5, p2, Lcom/squareup/checkout/CartItem;->createdAt:Ljava/util/Date;

    iget-object v6, p2, Lcom/squareup/checkout/CartItem;->itemName:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/squareup/checkout/CartItem;->isVoided()Z

    move-result v7

    .line 11
    invoke-static/range {v1 .. v7}, Lcom/squareup/payment/DefaultCartSort$Companion;->access$sortCompare(Lcom/squareup/payment/DefaultCartSort$Companion;Ljava/util/Date;Ljava/lang/String;ZLjava/util/Date;Ljava/lang/String;Z)I

    move-result p1

    return p1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 8
    check-cast p1, Lcom/squareup/checkout/CartItem;

    check-cast p2, Lcom/squareup/checkout/CartItem;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/payment/DefaultCartSort$Companion$Comparator$1;->compare(Lcom/squareup/checkout/CartItem;Lcom/squareup/checkout/CartItem;)I

    move-result p1

    return p1
.end method
