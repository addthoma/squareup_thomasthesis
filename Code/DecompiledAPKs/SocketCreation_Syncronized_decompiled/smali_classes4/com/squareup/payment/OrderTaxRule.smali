.class public Lcom/squareup/payment/OrderTaxRule;
.super Ljava/lang/Object;
.source "OrderTaxRule.java"


# instance fields
.field private final appliesToCustomAmounts:Z

.field private final condition:Lcom/squareup/api/items/TaxRule$Condition;

.field private final diningOptionIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final enablingActionType:Lcom/squareup/api/items/EnablingActionType;

.field private hashCode:I

.field private final id:Ljava/lang/String;

.field private final itemIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final itemSetConfiguration:Lcom/squareup/api/items/ItemSetConfiguration;

.field private final name:Ljava/lang/String;

.field private final taxIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final taxSetConfiguration:Lcom/squareup/api/items/TaxSetConfiguration;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/EnablingActionType;Lcom/squareup/api/items/TaxRule$Condition;Lcom/squareup/api/items/TaxSetConfiguration;Lcom/squareup/api/items/ItemSetConfiguration;)V
    .locals 0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/squareup/payment/OrderTaxRule;->id:Ljava/lang/String;

    .line 59
    iput-object p2, p0, Lcom/squareup/payment/OrderTaxRule;->name:Ljava/lang/String;

    .line 60
    iput-object p3, p0, Lcom/squareup/payment/OrderTaxRule;->enablingActionType:Lcom/squareup/api/items/EnablingActionType;

    .line 61
    iput-object p4, p0, Lcom/squareup/payment/OrderTaxRule;->condition:Lcom/squareup/api/items/TaxRule$Condition;

    .line 62
    iput-object p5, p0, Lcom/squareup/payment/OrderTaxRule;->taxSetConfiguration:Lcom/squareup/api/items/TaxSetConfiguration;

    .line 63
    iput-object p6, p0, Lcom/squareup/payment/OrderTaxRule;->itemSetConfiguration:Lcom/squareup/api/items/ItemSetConfiguration;

    .line 64
    new-instance p1, Ljava/util/LinkedHashSet;

    invoke-direct {p1}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object p1, p0, Lcom/squareup/payment/OrderTaxRule;->diningOptionIds:Ljava/util/Set;

    .line 65
    new-instance p1, Ljava/util/LinkedHashSet;

    invoke-direct {p1}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object p1, p0, Lcom/squareup/payment/OrderTaxRule;->taxIds:Ljava/util/Set;

    .line 66
    new-instance p1, Ljava/util/LinkedHashSet;

    invoke-direct {p1}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object p1, p0, Lcom/squareup/payment/OrderTaxRule;->itemIds:Ljava/util/Set;

    .line 67
    iget-object p1, p6, Lcom/squareup/api/items/ItemSetConfiguration;->applies_to_custom_amounts:Ljava/lang/Boolean;

    const/4 p2, 0x0

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/payment/OrderTaxRule;->appliesToCustomAmounts:Z

    .line 69
    sget-object p1, Lcom/squareup/api/items/EnablingActionType;->DISABLE:Lcom/squareup/api/items/EnablingActionType;

    if-ne p3, p1, :cond_4

    .line 72
    iget-object p1, p4, Lcom/squareup/api/items/TaxRule$Condition;->dining_option_predicate:Lcom/squareup/api/items/ObjectPredicate;

    if-eqz p1, :cond_3

    .line 75
    iget-object p1, p5, Lcom/squareup/api/items/TaxSetConfiguration;->filter:Lcom/squareup/api/items/ObjectFilter;

    sget-object p2, Lcom/squareup/api/items/ObjectFilter;->INCLUDE:Lcom/squareup/api/items/ObjectFilter;

    if-eq p1, p2, :cond_0

    iget-object p1, p5, Lcom/squareup/api/items/TaxSetConfiguration;->filter:Lcom/squareup/api/items/ObjectFilter;

    sget-object p2, Lcom/squareup/api/items/ObjectFilter;->ALL:Lcom/squareup/api/items/ObjectFilter;

    if-ne p1, p2, :cond_1

    :cond_0
    iget-object p1, p6, Lcom/squareup/api/items/ItemSetConfiguration;->filter:Lcom/squareup/api/items/ObjectFilter;

    sget-object p2, Lcom/squareup/api/items/ObjectFilter;->INCLUDE:Lcom/squareup/api/items/ObjectFilter;

    if-eq p1, p2, :cond_2

    iget-object p1, p6, Lcom/squareup/api/items/ItemSetConfiguration;->filter:Lcom/squareup/api/items/ObjectFilter;

    sget-object p2, Lcom/squareup/api/items/ObjectFilter;->ALL:Lcom/squareup/api/items/ObjectFilter;

    if-ne p1, p2, :cond_1

    goto :goto_0

    .line 77
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The ObjectFilter only supports INCLUDE and ALL."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    :goto_0
    return-void

    .line 73
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "dining_option_predicate is required."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 70
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Register only supports DISABLE TaxRules."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private initialize()V
    .locals 3

    .line 116
    iget-object v0, p0, Lcom/squareup/payment/OrderTaxRule;->condition:Lcom/squareup/api/items/TaxRule$Condition;

    iget-object v0, v0, Lcom/squareup/api/items/TaxRule$Condition;->dining_option_predicate:Lcom/squareup/api/items/ObjectPredicate;

    iget-object v0, v0, Lcom/squareup/api/items/ObjectPredicate;->object_id:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/sync/ObjectId;

    .line 117
    iget-object v2, p0, Lcom/squareup/payment/OrderTaxRule;->diningOptionIds:Ljava/util/Set;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/OrderTaxRule;->taxSetConfiguration:Lcom/squareup/api/items/TaxSetConfiguration;

    iget-object v0, v0, Lcom/squareup/api/items/TaxSetConfiguration;->filter:Lcom/squareup/api/items/ObjectFilter;

    sget-object v1, Lcom/squareup/api/items/ObjectFilter;->INCLUDE:Lcom/squareup/api/items/ObjectFilter;

    if-ne v0, v1, :cond_1

    .line 121
    iget-object v0, p0, Lcom/squareup/payment/OrderTaxRule;->taxSetConfiguration:Lcom/squareup/api/items/TaxSetConfiguration;

    iget-object v0, v0, Lcom/squareup/api/items/TaxSetConfiguration;->tax:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/sync/ObjectId;

    .line 122
    iget-object v2, p0, Lcom/squareup/payment/OrderTaxRule;->taxIds:Ljava/util/Set;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 126
    :cond_1
    iget-object v0, p0, Lcom/squareup/payment/OrderTaxRule;->itemSetConfiguration:Lcom/squareup/api/items/ItemSetConfiguration;

    iget-object v0, v0, Lcom/squareup/api/items/ItemSetConfiguration;->filter:Lcom/squareup/api/items/ObjectFilter;

    sget-object v1, Lcom/squareup/api/items/ObjectFilter;->INCLUDE:Lcom/squareup/api/items/ObjectFilter;

    if-ne v0, v1, :cond_2

    .line 127
    iget-object v0, p0, Lcom/squareup/payment/OrderTaxRule;->itemSetConfiguration:Lcom/squareup/api/items/ItemSetConfiguration;

    iget-object v0, v0, Lcom/squareup/api/items/ItemSetConfiguration;->item:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/sync/ObjectId;

    .line 128
    iget-object v2, p0, Lcom/squareup/payment/OrderTaxRule;->itemIds:Ljava/util/Set;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    return-void
.end method

.method public static listOf(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTaxRule;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderTaxRule;",
            ">;"
        }
    .end annotation

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 26
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogTaxRule;

    .line 27
    invoke-static {v1}, Lcom/squareup/payment/OrderTaxRule;->of(Lcom/squareup/shared/catalog/models/CatalogTaxRule;)Lcom/squareup/payment/OrderTaxRule;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static of(Lcom/squareup/shared/catalog/models/CatalogTaxRule;)Lcom/squareup/payment/OrderTaxRule;
    .locals 8

    .line 33
    new-instance v7, Lcom/squareup/payment/OrderTaxRule;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTaxRule;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTaxRule;->getName()Ljava/lang/String;

    move-result-object v2

    .line 34
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTaxRule;->getEnablingActionType()Lcom/squareup/api/items/EnablingActionType;

    move-result-object v3

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTaxRule;->getCondition()Lcom/squareup/api/items/TaxRule$Condition;

    move-result-object v4

    .line 35
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTaxRule;->getTaxSetConfiguration()Lcom/squareup/api/items/TaxSetConfiguration;

    move-result-object v5

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTaxRule;->getItemSetConfiguration()Lcom/squareup/api/items/ItemSetConfiguration;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/payment/OrderTaxRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/EnablingActionType;Lcom/squareup/api/items/TaxRule$Condition;Lcom/squareup/api/items/TaxSetConfiguration;Lcom/squareup/api/items/ItemSetConfiguration;)V

    return-object v7
.end method


# virtual methods
.method public doesTaxRuleApply(Ljava/lang/String;ZLcom/squareup/checkout/Tax;Lcom/squareup/checkout/DiningOption;)Z
    .locals 3

    if-eqz p1, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    .line 84
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The item id of a custom amount item should be null."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    if-nez p1, :cond_3

    if-eqz p2, :cond_2

    goto :goto_1

    .line 87
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The item id of a regular item should not be null."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 90
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/squareup/payment/OrderTaxRule;->diningOptionIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 91
    invoke-direct {p0}, Lcom/squareup/payment/OrderTaxRule;->initialize()V

    .line 94
    :cond_4
    iget-object v0, p0, Lcom/squareup/payment/OrderTaxRule;->itemSetConfiguration:Lcom/squareup/api/items/ItemSetConfiguration;

    iget-object v0, v0, Lcom/squareup/api/items/ItemSetConfiguration;->filter:Lcom/squareup/api/items/ObjectFilter;

    sget-object v1, Lcom/squareup/api/items/ObjectFilter;->ALL:Lcom/squareup/api/items/ObjectFilter;

    const/4 v2, 0x0

    if-eq v0, v1, :cond_6

    if-eqz p2, :cond_5

    iget-boolean p1, p0, Lcom/squareup/payment/OrderTaxRule;->appliesToCustomAmounts:Z

    if-nez p1, :cond_6

    goto :goto_2

    :cond_5
    iget-object p2, p0, Lcom/squareup/payment/OrderTaxRule;->itemIds:Ljava/util/Set;

    .line 95
    invoke-interface {p2, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_6

    :goto_2
    return v2

    .line 97
    :cond_6
    iget-object p1, p0, Lcom/squareup/payment/OrderTaxRule;->taxSetConfiguration:Lcom/squareup/api/items/TaxSetConfiguration;

    iget-object p1, p1, Lcom/squareup/api/items/TaxSetConfiguration;->filter:Lcom/squareup/api/items/ObjectFilter;

    sget-object p2, Lcom/squareup/api/items/ObjectFilter;->ALL:Lcom/squareup/api/items/ObjectFilter;

    if-eq p1, p2, :cond_7

    iget-object p1, p0, Lcom/squareup/payment/OrderTaxRule;->taxIds:Ljava/util/Set;

    iget-object p2, p3, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {p1, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_7

    return v2

    .line 100
    :cond_7
    iget-object p1, p0, Lcom/squareup/payment/OrderTaxRule;->diningOptionIds:Ljava/util/Set;

    invoke-virtual {p4}, Lcom/squareup/checkout/DiningOption;->getId()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 137
    :cond_0
    instance-of v1, p1, Lcom/squareup/payment/OrderTaxRule;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 141
    :cond_1
    check-cast p1, Lcom/squareup/payment/OrderTaxRule;

    .line 142
    iget-object v1, p0, Lcom/squareup/payment/OrderTaxRule;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/payment/OrderTaxRule;->id:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/payment/OrderTaxRule;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/payment/OrderTaxRule;->name:Ljava/lang/String;

    .line 143
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/payment/OrderTaxRule;->enablingActionType:Lcom/squareup/api/items/EnablingActionType;

    iget-object v3, p1, Lcom/squareup/payment/OrderTaxRule;->enablingActionType:Lcom/squareup/api/items/EnablingActionType;

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/squareup/payment/OrderTaxRule;->condition:Lcom/squareup/api/items/TaxRule$Condition;

    iget-object v3, p1, Lcom/squareup/payment/OrderTaxRule;->condition:Lcom/squareup/api/items/TaxRule$Condition;

    .line 145
    invoke-virtual {v1, v3}, Lcom/squareup/api/items/TaxRule$Condition;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/payment/OrderTaxRule;->taxSetConfiguration:Lcom/squareup/api/items/TaxSetConfiguration;

    iget-object v3, p1, Lcom/squareup/payment/OrderTaxRule;->taxSetConfiguration:Lcom/squareup/api/items/TaxSetConfiguration;

    .line 146
    invoke-virtual {v1, v3}, Lcom/squareup/api/items/TaxSetConfiguration;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/payment/OrderTaxRule;->itemSetConfiguration:Lcom/squareup/api/items/ItemSetConfiguration;

    iget-object p1, p1, Lcom/squareup/payment/OrderTaxRule;->itemSetConfiguration:Lcom/squareup/api/items/ItemSetConfiguration;

    .line 147
    invoke-virtual {v1, p1}, Lcom/squareup/api/items/ItemSetConfiguration;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getEnablingActionType()Lcom/squareup/api/items/EnablingActionType;
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/squareup/payment/OrderTaxRule;->enablingActionType:Lcom/squareup/api/items/EnablingActionType;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/payment/OrderTaxRule;->name:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .line 151
    iget v0, p0, Lcom/squareup/payment/OrderTaxRule;->hashCode:I

    if-nez v0, :cond_0

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 152
    iget-object v2, p0, Lcom/squareup/payment/OrderTaxRule;->id:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/squareup/payment/OrderTaxRule;->name:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/squareup/payment/OrderTaxRule;->enablingActionType:Lcom/squareup/api/items/EnablingActionType;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/squareup/payment/OrderTaxRule;->condition:Lcom/squareup/api/items/TaxRule$Condition;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/squareup/payment/OrderTaxRule;->taxSetConfiguration:Lcom/squareup/api/items/TaxSetConfiguration;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/squareup/payment/OrderTaxRule;->itemSetConfiguration:Lcom/squareup/api/items/ItemSetConfiguration;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/squareup/payment/OrderTaxRule;->hashCode:I

    .line 155
    :cond_0
    iget v0, p0, Lcom/squareup/payment/OrderTaxRule;->hashCode:I

    return v0
.end method
