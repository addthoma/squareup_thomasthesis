.class public final Lcom/squareup/payment/PaymentReceipt_RealFactory_Factory;
.super Ljava/lang/Object;
.source "PaymentReceipt_RealFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/PaymentReceipt$RealFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final cartProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/payment/PaymentReceipt_RealFactory_Factory;->cartProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/payment/PaymentReceipt_RealFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;)",
            "Lcom/squareup/payment/PaymentReceipt_RealFactory_Factory;"
        }
    .end annotation

    .line 28
    new-instance v0, Lcom/squareup/payment/PaymentReceipt_RealFactory_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/payment/PaymentReceipt_RealFactory_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljavax/inject/Provider;)Lcom/squareup/payment/PaymentReceipt$RealFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;)",
            "Lcom/squareup/payment/PaymentReceipt$RealFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/payment/PaymentReceipt$RealFactory;

    invoke-direct {v0, p0}, Lcom/squareup/payment/PaymentReceipt$RealFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/payment/PaymentReceipt$RealFactory;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt_RealFactory_Factory;->cartProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/squareup/payment/PaymentReceipt_RealFactory_Factory;->newInstance(Ljavax/inject/Provider;)Lcom/squareup/payment/PaymentReceipt$RealFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/payment/PaymentReceipt_RealFactory_Factory;->get()Lcom/squareup/payment/PaymentReceipt$RealFactory;

    move-result-object v0

    return-object v0
.end method
