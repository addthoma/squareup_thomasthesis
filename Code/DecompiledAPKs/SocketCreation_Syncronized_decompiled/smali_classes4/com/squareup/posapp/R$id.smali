.class public final Lcom/squareup/posapp/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/posapp/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final ALT:I = 0x7f0a0000

.field public static final AUTOMATIC_PAYMENT_COF:I = 0x7f0a0001

.field public static final BACKSPACE:I = 0x7f0a0002

.field public static final BACK_ARROW:I = 0x7f0a0003

.field public static final BACK_ARROW_LARGE:I = 0x7f0a0004

.field public static final BATTERY_CHARGING:I = 0x7f0a0005

.field public static final BATTERY_DEAD:I = 0x7f0a0006

.field public static final BATTERY_FULL:I = 0x7f0a0007

.field public static final BATTERY_HIGH:I = 0x7f0a0008

.field public static final BATTERY_LOW:I = 0x7f0a0009

.field public static final BATTERY_MID:I = 0x7f0a000a

.field public static final BATTERY_OUTLINE:I = 0x7f0a000b

.field public static final BATTERY_TINY_CHARGING:I = 0x7f0a000c

.field public static final BATTERY_TINY_DEAD:I = 0x7f0a000d

.field public static final BATTERY_TINY_FULL:I = 0x7f0a000e

.field public static final BATTERY_TINY_HIGH:I = 0x7f0a000f

.field public static final BATTERY_TINY_LOW:I = 0x7f0a0010

.field public static final BATTERY_TINY_MID:I = 0x7f0a0011

.field public static final BATTERY_TINY_OUTLINE:I = 0x7f0a0012

.field public static final BIRTHDAY_CAKE:I = 0x7f0a0013

.field public static final BOTTOM:I = 0x7f0a0014

.field public static final BOTTOM_END:I = 0x7f0a0015

.field public static final BOTTOM_START:I = 0x7f0a0016

.field public static final BOX_DOLLAR_SIGN:I = 0x7f0a0017

.field public static final BOX_EQUALS:I = 0x7f0a0018

.field public static final BOX_PENNIES:I = 0x7f0a0019

.field public static final BOX_PERCENT:I = 0x7f0a001a

.field public static final BOX_PLUS:I = 0x7f0a001b

.field public static final BRIEFCASE:I = 0x7f0a001c

.field public static final BRIGHTNESS_HIGH:I = 0x7f0a001d

.field public static final BRIGHTNESS_LOW:I = 0x7f0a001e

.field public static final BURGER:I = 0x7f0a001f

.field public static final BURGER_SETTINGS:I = 0x7f0a0020

.field public static final CARD_AMEX:I = 0x7f0a0021

.field public static final CARD_BACK:I = 0x7f0a0022

.field public static final CARD_CHIP:I = 0x7f0a0023

.field public static final CARD_CUP:I = 0x7f0a0024

.field public static final CARD_DISCOVER:I = 0x7f0a0025

.field public static final CARD_DISCOVER_DINERS:I = 0x7f0a0026

.field public static final CARD_INTERAC:I = 0x7f0a0027

.field public static final CARD_JCB:I = 0x7f0a0028

.field public static final CARD_MC:I = 0x7f0a0029

.field public static final CARD_VISA:I = 0x7f0a002a

.field public static final CHECK:I = 0x7f0a002b

.field public static final CHECK_X2:I = 0x7f0a002c

.field public static final CIRCLE_CARD:I = 0x7f0a002d

.field public static final CIRCLE_CARD_CHIP:I = 0x7f0a002e

.field public static final CIRCLE_CHECK:I = 0x7f0a002f

.field public static final CIRCLE_CHECKLIST:I = 0x7f0a0030

.field public static final CIRCLE_CLOCK:I = 0x7f0a0031

.field public static final CIRCLE_CONTACTLESS:I = 0x7f0a0032

.field public static final CIRCLE_CONTACTS:I = 0x7f0a0033

.field public static final CIRCLE_EMPLOYEE_MANAGEMENT:I = 0x7f0a0034

.field public static final CIRCLE_ENVELOPE:I = 0x7f0a0035

.field public static final CIRCLE_EXCLAMATION:I = 0x7f0a0036

.field public static final CIRCLE_GIFT_CARD:I = 0x7f0a0037

.field public static final CIRCLE_HEART:I = 0x7f0a0038

.field public static final CIRCLE_INVOICE:I = 0x7f0a0039

.field public static final CIRCLE_LIGHTNING:I = 0x7f0a003a

.field public static final CIRCLE_LOCATION:I = 0x7f0a003b

.field public static final CIRCLE_LOCK:I = 0x7f0a003c

.field public static final CIRCLE_MICROPHONE:I = 0x7f0a003d

.field public static final CIRCLE_OPEN_TICKETS:I = 0x7f0a003e

.field public static final CIRCLE_PAYROLL:I = 0x7f0a003f

.field public static final CIRCLE_PHONE:I = 0x7f0a0040

.field public static final CIRCLE_PLAY:I = 0x7f0a0041

.field public static final CIRCLE_PRINTER:I = 0x7f0a0042

.field public static final CIRCLE_RECEIPT:I = 0x7f0a0043

.field public static final CIRCLE_REPORTS_CUSTOMIZE:I = 0x7f0a0044

.field public static final CIRCLE_REWARDS:I = 0x7f0a0045

.field public static final CIRCLE_SMS:I = 0x7f0a0046

.field public static final CIRCLE_STACK:I = 0x7f0a0047

.field public static final CIRCLE_STAR:I = 0x7f0a0048

.field public static final CIRCLE_STORAGE:I = 0x7f0a0049

.field public static final CIRCLE_SWIPE_ERROR:I = 0x7f0a004a

.field public static final CIRCLE_TAG:I = 0x7f0a004b

.field public static final CIRCLE_TIMECARDS:I = 0x7f0a004c

.field public static final CIRCLE_WARNING:I = 0x7f0a004d

.field public static final CIRCLE_X:I = 0x7f0a004e

.field public static final CLOCK:I = 0x7f0a004f

.field public static final CLOCK_SKEW:I = 0x7f0a0050

.field public static final CONTACTLESS:I = 0x7f0a0051

.field public static final CONTACTS:I = 0x7f0a0052

.field public static final CTRL:I = 0x7f0a0053

.field public static final CUSTOMER:I = 0x7f0a0054

.field public static final CUSTOMERS:I = 0x7f0a0055

.field public static final CUSTOMER_ADD:I = 0x7f0a0056

.field public static final CUSTOMER_CIRCLE:I = 0x7f0a0057

.field public static final CUSTOMER_GROUP:I = 0x7f0a0058

.field public static final CUSTOM_AMOUNT:I = 0x7f0a0059

.field public static final DOLLAR_BILL:I = 0x7f0a005a

.field public static final DOWN_CARET:I = 0x7f0a005b

.field public static final DRAG_HANDLE_BURGER:I = 0x7f0a005c

.field public static final DRAG_N_DROP:I = 0x7f0a005d

.field public static final ENVELOPE:I = 0x7f0a005e

.field public static final FUNCTION:I = 0x7f0a005f

.field public static final GIFT_CARD_LARGE:I = 0x7f0a0060

.field public static final GIFT_CARD_MEDIUM:I = 0x7f0a0061

.field public static final GIFT_CARD_SMALL:I = 0x7f0a0062

.field public static final HELP_CENTER:I = 0x7f0a0063

.field public static final HUD_BARCODE_SCANNER_CONNECTED:I = 0x7f0a0064

.field public static final HUD_BARCODE_SCANNER_DISCONNECTED:I = 0x7f0a0065

.field public static final HUD_CARD:I = 0x7f0a0066

.field public static final HUD_CASH_DRAWER_CONNECTED:I = 0x7f0a0067

.field public static final HUD_CASH_DRAWER_DISCONNECTED:I = 0x7f0a0068

.field public static final HUD_CHIP_CARD_NOT_USABLE:I = 0x7f0a0069

.field public static final HUD_CHIP_CARD_USABLE:I = 0x7f0a006a

.field public static final HUD_CONTACTLESS:I = 0x7f0a006b

.field public static final HUD_LOGOUT:I = 0x7f0a006c

.field public static final HUD_OVERLAY_BATTERY_DEAD:I = 0x7f0a006d

.field public static final HUD_OVERLAY_BATTERY_FULL:I = 0x7f0a006e

.field public static final HUD_OVERLAY_BATTERY_HIGH:I = 0x7f0a006f

.field public static final HUD_OVERLAY_BATTERY_LOW:I = 0x7f0a0070

.field public static final HUD_OVERLAY_BATTERY_MID:I = 0x7f0a0071

.field public static final HUD_PRINTER_CONNECTED:I = 0x7f0a0072

.field public static final HUD_PRINTER_DISCONNECTED:I = 0x7f0a0073

.field public static final HUD_R12:I = 0x7f0a0074

.field public static final HUD_R12_BATTERY_CHARGING:I = 0x7f0a0075

.field public static final HUD_R12_BATTERY_OUTLINE:I = 0x7f0a0076

.field public static final HUD_R12_DISCONNECTED:I = 0x7f0a0077

.field public static final HUD_R4_READER:I = 0x7f0a0078

.field public static final HUD_R6_BATTERY_CHARGING:I = 0x7f0a0079

.field public static final HUD_R6_BATTERY_OUTLINE:I = 0x7f0a007a

.field public static final HUD_R6_READER:I = 0x7f0a007b

.field public static final HUD_READER_DISCONNECTED:I = 0x7f0a007c

.field public static final HUD_REWARDS:I = 0x7f0a007d

.field public static final HUD_STORE:I = 0x7f0a007e

.field public static final HUD_SWIPE:I = 0x7f0a007f

.field public static final HUD_WARNING:I = 0x7f0a0080

.field public static final INVOICE:I = 0x7f0a0081

.field public static final INVOICES:I = 0x7f0a0082

.field public static final ITEMS:I = 0x7f0a0083

.field public static final KEYBOARD_ALPHA:I = 0x7f0a0084

.field public static final KEYBOARD_NUMBERS:I = 0x7f0a0085

.field public static final LEFT:I = 0x7f0a0086

.field public static final LEFT_CARET:I = 0x7f0a0087

.field public static final LOCATION:I = 0x7f0a0088

.field public static final LOCATION_CIRCLE:I = 0x7f0a0089

.field public static final LOCATION_PIN:I = 0x7f0a008a

.field public static final LOGOTYPE_EN:I = 0x7f0a008b

.field public static final LOGOTYPE_EN_WORLD:I = 0x7f0a008c

.field public static final LOGOTYPE_ES:I = 0x7f0a008d

.field public static final LOGOTYPE_ES_WORLD:I = 0x7f0a008e

.field public static final LOGOTYPE_FR:I = 0x7f0a008f

.field public static final LOGOTYPE_FR_WORLD:I = 0x7f0a0090

.field public static final LOGOTYPE_JA:I = 0x7f0a0091

.field public static final LOGOTYPE_JA_WORLD:I = 0x7f0a0092

.field public static final MEMO:I = 0x7f0a0093

.field public static final META:I = 0x7f0a0094

.field public static final MICROPHONE:I = 0x7f0a0095

.field public static final MINUS:I = 0x7f0a0096

.field public static final NAVIGATION_1:I = 0x7f0a0097

.field public static final NAVIGATION_2:I = 0x7f0a0098

.field public static final NAVIGATION_3:I = 0x7f0a0099

.field public static final NAVIGATION_4:I = 0x7f0a009a

.field public static final NAVIGATION_5:I = 0x7f0a009b

.field public static final NAVIGATION_6:I = 0x7f0a009c

.field public static final NAVIGATION_7:I = 0x7f0a009d

.field public static final NAVIGATION_KEYPAD:I = 0x7f0a009e

.field public static final NAVIGATION_LIBRARY:I = 0x7f0a009f

.field public static final NOTE:I = 0x7f0a00a0

.field public static final NO_CIRCLE:I = 0x7f0a00a1

.field public static final OTHER_TENDER:I = 0x7f0a00a2

.field public static final PERSON:I = 0x7f0a00a3

.field public static final PHONE:I = 0x7f0a00a4

.field public static final PHONE_RECEIVER:I = 0x7f0a00a5

.field public static final PIN_CARD_AMEX:I = 0x7f0a00a6

.field public static final PIN_CARD_GENERIC:I = 0x7f0a00a7

.field public static final PIN_CARD_MASTER_CARD:I = 0x7f0a00a8

.field public static final PIN_CARD_VISA:I = 0x7f0a00a9

.field public static final PLUS:I = 0x7f0a00aa

.field public static final PLUS_CIRCLE:I = 0x7f0a00ab

.field public static final PRINTER:I = 0x7f0a00ac

.field public static final READER_MEDIUM:I = 0x7f0a00ad

.field public static final READER_SMALL:I = 0x7f0a00ae

.field public static final RECEIPT:I = 0x7f0a00af

.field public static final REDEMPTION_CODE:I = 0x7f0a00b0

.field public static final REFERRAL:I = 0x7f0a00b1

.field public static final REFUNDED:I = 0x7f0a00b2

.field public static final REGISTER:I = 0x7f0a00b3

.field public static final RELOAD:I = 0x7f0a00b4

.field public static final REPORTS:I = 0x7f0a00b5

.field public static final REWARDS:I = 0x7f0a00b6

.field public static final REWARDS_LARGE:I = 0x7f0a00b7

.field public static final REWARDS_MEDIUM:I = 0x7f0a00b8

.field public static final REWARD_TROPHY:I = 0x7f0a00b9

.field public static final RIBBON:I = 0x7f0a00ba

.field public static final RIGHT:I = 0x7f0a00bb

.field public static final RIGHT_CARET:I = 0x7f0a00bc

.field public static final SALES_HISTORY:I = 0x7f0a00bd

.field public static final SAVE_CARD:I = 0x7f0a00be

.field public static final SEARCH:I = 0x7f0a00bf

.field public static final SETTINGS:I = 0x7f0a00c0

.field public static final SHIFT:I = 0x7f0a00c1

.field public static final SORT:I = 0x7f0a00c2

.field public static final SPEECH_BUBBLE:I = 0x7f0a00c3

.field public static final SPLIT_TENDER:I = 0x7f0a00c4

.field public static final SPLIT_TENDER_CASH_DOLLAR:I = 0x7f0a00c5

.field public static final SPLIT_TENDER_CASH_YEN:I = 0x7f0a00c6

.field public static final SPLIT_TENDER_CHIP:I = 0x7f0a00c7

.field public static final SPLIT_TENDER_OTHER:I = 0x7f0a00c8

.field public static final SQUARE_LOGO:I = 0x7f0a00c9

.field public static final SQUARE_LOGO_SW600:I = 0x7f0a00ca

.field public static final SQUARE_LOGO_SW720:I = 0x7f0a00cb

.field public static final SQUARE_WALLET_TENDER:I = 0x7f0a00cc

.field public static final STACK_HUGE_ADD:I = 0x7f0a00cd

.field public static final STACK_LARGE:I = 0x7f0a00ce

.field public static final STACK_MEDIUM:I = 0x7f0a00cf

.field public static final STACK_SMALL:I = 0x7f0a00d0

.field public static final STOPWATCH:I = 0x7f0a00d1

.field public static final STORAGE:I = 0x7f0a00d2

.field public static final SWITCHER_CUSTOMER:I = 0x7f0a00d3

.field public static final SWITCHER_DEVICE:I = 0x7f0a00d4

.field public static final SWITCHER_HELP:I = 0x7f0a00d5

.field public static final SWITCHER_ITEM:I = 0x7f0a00d6

.field public static final SWITCHER_REGISTER:I = 0x7f0a00d7

.field public static final SWITCHER_REPORT:I = 0x7f0a00d8

.field public static final SWITCHER_SETTINGS:I = 0x7f0a00d9

.field public static final SWITCHER_TRANSACTION:I = 0x7f0a00da

.field public static final SYM:I = 0x7f0a00db

.field public static final TAG_LARGE:I = 0x7f0a00dc

.field public static final TAG_MEDIUM:I = 0x7f0a00dd

.field public static final TAG_SMALL:I = 0x7f0a00de

.field public static final TAG_TINY:I = 0x7f0a00df

.field public static final TOP:I = 0x7f0a00e0

.field public static final TOP_END:I = 0x7f0a00e1

.field public static final TOP_START:I = 0x7f0a00e2

.field public static final UNKNOWN_TENDER:I = 0x7f0a00e3

.field public static final USER_LARGE:I = 0x7f0a00e4

.field public static final VOLUME_HIGH:I = 0x7f0a00e5

.field public static final VOLUME_LOW:I = 0x7f0a00e6

.field public static final WARNING_SMALL:I = 0x7f0a00e7

.field public static final X:I = 0x7f0a00e8

.field public static final X2_ETHERNET:I = 0x7f0a00e9

.field public static final X2_ETHERNET_ERROR:I = 0x7f0a00ea

.field public static final X2_INFO:I = 0x7f0a00eb

.field public static final X2_NETWORK_ERROR:I = 0x7f0a00ec

.field public static final X2_WIFI_1:I = 0x7f0a00ed

.field public static final X2_WIFI_2:I = 0x7f0a00ee

.field public static final X2_WIFI_3:I = 0x7f0a00ef

.field public static final X2_WIFI_4:I = 0x7f0a00f0

.field public static final X2_WIFI_ERROR:I = 0x7f0a00f1

.field public static final X2_WIFI_LOCK:I = 0x7f0a00f2

.field public static final X_LARGE:I = 0x7f0a00f3

.field public static final YEN_BILL:I = 0x7f0a00f4

.field public static final about_device_setting_container:I = 0x7f0a00f5

.field public static final about_device_settings_location:I = 0x7f0a00f6

.field public static final about_device_settings_phone:I = 0x7f0a00f7

.field public static final about_device_settings_storage:I = 0x7f0a00f8

.field public static final about_setting_body:I = 0x7f0a00f9

.field public static final about_setting_glyph:I = 0x7f0a00fa

.field public static final about_setting_text_container:I = 0x7f0a00fb

.field public static final about_setting_title:I = 0x7f0a00fc

.field public static final accept_button:I = 0x7f0a00fd

.field public static final accept_legal:I = 0x7f0a00fe

.field public static final accept_legal_parent:I = 0x7f0a00ff

.field public static final access_denied_glyph_message:I = 0x7f0a0100

.field public static final accessibility_action_clickable_span:I = 0x7f0a0101

.field public static final accessibility_button:I = 0x7f0a0102

.field public static final accessibility_custom_action_0:I = 0x7f0a0103

.field public static final accessibility_custom_action_1:I = 0x7f0a0104

.field public static final accessibility_custom_action_10:I = 0x7f0a0105

.field public static final accessibility_custom_action_11:I = 0x7f0a0106

.field public static final accessibility_custom_action_12:I = 0x7f0a0107

.field public static final accessibility_custom_action_13:I = 0x7f0a0108

.field public static final accessibility_custom_action_14:I = 0x7f0a0109

.field public static final accessibility_custom_action_15:I = 0x7f0a010a

.field public static final accessibility_custom_action_16:I = 0x7f0a010b

.field public static final accessibility_custom_action_17:I = 0x7f0a010c

.field public static final accessibility_custom_action_18:I = 0x7f0a010d

.field public static final accessibility_custom_action_19:I = 0x7f0a010e

.field public static final accessibility_custom_action_2:I = 0x7f0a010f

.field public static final accessibility_custom_action_20:I = 0x7f0a0110

.field public static final accessibility_custom_action_21:I = 0x7f0a0111

.field public static final accessibility_custom_action_22:I = 0x7f0a0112

.field public static final accessibility_custom_action_23:I = 0x7f0a0113

.field public static final accessibility_custom_action_24:I = 0x7f0a0114

.field public static final accessibility_custom_action_25:I = 0x7f0a0115

.field public static final accessibility_custom_action_26:I = 0x7f0a0116

.field public static final accessibility_custom_action_27:I = 0x7f0a0117

.field public static final accessibility_custom_action_28:I = 0x7f0a0118

.field public static final accessibility_custom_action_29:I = 0x7f0a0119

.field public static final accessibility_custom_action_3:I = 0x7f0a011a

.field public static final accessibility_custom_action_30:I = 0x7f0a011b

.field public static final accessibility_custom_action_31:I = 0x7f0a011c

.field public static final accessibility_custom_action_4:I = 0x7f0a011d

.field public static final accessibility_custom_action_5:I = 0x7f0a011e

.field public static final accessibility_custom_action_6:I = 0x7f0a011f

.field public static final accessibility_custom_action_7:I = 0x7f0a0120

.field public static final accessibility_custom_action_8:I = 0x7f0a0121

.field public static final accessibility_custom_action_9:I = 0x7f0a0122

.field public static final accessible_keypad_bounding_box:I = 0x7f0a0123

.field public static final accessible_keypad_button:I = 0x7f0a0124

.field public static final accessible_keypad_title:I = 0x7f0a0125

.field public static final accessible_pin_tutorial_done_layout:I = 0x7f0a0126

.field public static final accessible_pin_tutorial_step_layout:I = 0x7f0a0127

.field public static final accessory:I = 0x7f0a0128

.field public static final account_error_message:I = 0x7f0a0129

.field public static final account_holder_field:I = 0x7f0a012a

.field public static final account_holder_name:I = 0x7f0a012b

.field public static final account_name:I = 0x7f0a012c

.field public static final account_number:I = 0x7f0a012d

.field public static final account_number_field:I = 0x7f0a012e

.field public static final account_type:I = 0x7f0a012f

.field public static final account_type_field:I = 0x7f0a0130

.field public static final action0:I = 0x7f0a0131

.field public static final actionIcon:I = 0x7f0a0132

.field public static final actionLink:I = 0x7f0a0133

.field public static final action_amount:I = 0x7f0a0134

.field public static final action_bar:I = 0x7f0a0135

.field public static final action_bar_activity_content:I = 0x7f0a0136

.field public static final action_bar_container:I = 0x7f0a0137

.field public static final action_bar_guideline:I = 0x7f0a0138

.field public static final action_bar_root:I = 0x7f0a0139

.field public static final action_bar_spinner:I = 0x7f0a013a

.field public static final action_bar_subtitle:I = 0x7f0a013b

.field public static final action_bar_title:I = 0x7f0a013c

.field public static final action_bar_with_two_action_icons:I = 0x7f0a013d

.field public static final action_bottom_sheet_dialog_container:I = 0x7f0a013e

.field public static final action_button:I = 0x7f0a013f

.field public static final action_container:I = 0x7f0a0140

.field public static final action_context_bar:I = 0x7f0a0141

.field public static final action_description:I = 0x7f0a0142

.field public static final action_divider:I = 0x7f0a0143

.field public static final action_icon:I = 0x7f0a0144

.field public static final action_icon_2:I = 0x7f0a0145

.field public static final action_image:I = 0x7f0a0146

.field public static final action_menu_divider:I = 0x7f0a0147

.field public static final action_menu_presenter:I = 0x7f0a0148

.field public static final action_mode_bar:I = 0x7f0a0149

.field public static final action_mode_bar_stub:I = 0x7f0a014a

.field public static final action_mode_close_button:I = 0x7f0a014b

.field public static final action_text:I = 0x7f0a014c

.field public static final actionbar_custom_view_container:I = 0x7f0a014d

.field public static final actions:I = 0x7f0a014e

.field public static final activation_content:I = 0x7f0a014f

.field public static final active_sales_row:I = 0x7f0a0150

.field public static final activity_applet_bulk_settle_badge:I = 0x7f0a0151

.field public static final activity_applet_bulk_settle_button_glyph:I = 0x7f0a0152

.field public static final activity_applet_issue_refund_view:I = 0x7f0a0153

.field public static final activity_applet_issue_refund_view_glyph_view:I = 0x7f0a0154

.field public static final activity_applet_list_center_message_panel:I = 0x7f0a0155

.field public static final activity_applet_list_top_message_panel:I = 0x7f0a0156

.field public static final activity_applet_list_top_message_panel_deprecated:I = 0x7f0a0157

.field public static final activity_applet_list_top_message_panel_message:I = 0x7f0a0158

.field public static final activity_applet_list_top_message_panel_tap_state_message:I = 0x7f0a0159

.field public static final activity_applet_list_top_message_panel_title:I = 0x7f0a015a

.field public static final activity_applet_receipt_content:I = 0x7f0a015b

.field public static final activity_applet_receipt_message_panel:I = 0x7f0a015c

.field public static final activity_applet_restock_on_itemized_refund_view:I = 0x7f0a015d

.field public static final activity_applet_search_clickable_text:I = 0x7f0a015e

.field public static final activity_applet_search_message:I = 0x7f0a015f

.field public static final activity_chooser_view_content:I = 0x7f0a0160

.field public static final activity_memory_leak_tag:I = 0x7f0a0161

.field public static final actual_in_drawer:I = 0x7f0a0162

.field public static final actual_in_drawer_container:I = 0x7f0a0163

.field public static final actual_in_drawer_edit_text:I = 0x7f0a0164

.field public static final actual_in_drawer_hint:I = 0x7f0a0165

.field public static final add:I = 0x7f0a0166

.field public static final add_amount_button:I = 0x7f0a0167

.field public static final add_amount_help_text:I = 0x7f0a0168

.field public static final add_amount_input_field:I = 0x7f0a0169

.field public static final add_amount_source:I = 0x7f0a016a

.field public static final add_another_payment_button:I = 0x7f0a016b

.field public static final add_coupon_row:I = 0x7f0a016c

.field public static final add_money_button:I = 0x7f0a016d

.field public static final add_money_instrument:I = 0x7f0a016e

.field public static final add_money_section:I = 0x7f0a016f

.field public static final add_payment_button:I = 0x7f0a0170

.field public static final add_quick_amount:I = 0x7f0a0171

.field public static final add_tax_button:I = 0x7f0a0172

.field public static final add_tax_row:I = 0x7f0a0173

.field public static final add_to_google_pay:I = 0x7f0a0174

.field public static final additional_auth_slip_container:I = 0x7f0a0175

.field public static final additional_feedback:I = 0x7f0a0176

.field public static final additional_info_view:I = 0x7f0a0177

.field public static final address:I = 0x7f0a0178

.field public static final address_apartment:I = 0x7f0a0179

.field public static final address_city:I = 0x7f0a017a

.field public static final address_city_picker:I = 0x7f0a017b

.field public static final address_city_picker_scrubber:I = 0x7f0a017c

.field public static final address_city_scrubber:I = 0x7f0a017d

.field public static final address_city_state_row:I = 0x7f0a017e

.field public static final address_postal:I = 0x7f0a017f

.field public static final address_postal_scrubber:I = 0x7f0a0180

.field public static final address_selector_container:I = 0x7f0a0181

.field public static final address_selector_is_recommended:I = 0x7f0a0182

.field public static final address_selector_name:I = 0x7f0a0183

.field public static final address_selector_radio_button:I = 0x7f0a0184

.field public static final address_selector_text_container:I = 0x7f0a0185

.field public static final address_state:I = 0x7f0a0186

.field public static final address_state_scrubber:I = 0x7f0a0187

.field public static final address_street:I = 0x7f0a0188

.field public static final aditional_auth_slip:I = 0x7f0a0189

.field public static final adjust_height:I = 0x7f0a018a

.field public static final adjust_inventory_select_reason_help_text:I = 0x7f0a018b

.field public static final adjust_points_amount:I = 0x7f0a018c

.field public static final adjust_points_content:I = 0x7f0a018d

.field public static final adjust_points_reason_1:I = 0x7f0a018e

.field public static final adjust_points_reason_2:I = 0x7f0a018f

.field public static final adjust_points_reason_3:I = 0x7f0a0190

.field public static final adjust_points_reason_title:I = 0x7f0a0191

.field public static final adjust_stock_container:I = 0x7f0a0192

.field public static final adjust_stock_reason_section:I = 0x7f0a0193

.field public static final adjust_stock_specify_number_field:I = 0x7f0a0194

.field public static final adjust_stock_specify_number_help_text_inventory_tracking:I = 0x7f0a0195

.field public static final adjust_stock_specify_number_help_text_stock_count:I = 0x7f0a0196

.field public static final adjust_stock_unit_cost_field:I = 0x7f0a0197

.field public static final adjust_width:I = 0x7f0a0198

.field public static final admin_attach_screenshot_button:I = 0x7f0a0199

.field public static final admin_attachment_imageview:I = 0x7f0a019a

.field public static final admin_attachment_message_layout:I = 0x7f0a019b

.field public static final admin_attachment_request_text:I = 0x7f0a019c

.field public static final admin_date_text:I = 0x7f0a019d

.field public static final admin_image_attachment_message_container:I = 0x7f0a019e

.field public static final admin_image_message_layout:I = 0x7f0a019f

.field public static final admin_message:I = 0x7f0a01a0

.field public static final admin_message_container:I = 0x7f0a01a1

.field public static final admin_message_text:I = 0x7f0a01a2

.field public static final admin_review_message_layout:I = 0x7f0a01a3

.field public static final admin_suggestion_message:I = 0x7f0a01a4

.field public static final admin_suggestion_message_layout:I = 0x7f0a01a5

.field public static final admin_text_message_layout:I = 0x7f0a01a6

.field public static final advanced_modifier_instruction:I = 0x7f0a01a7

.field public static final agent_screenshot_request_message_layout:I = 0x7f0a01a8

.field public static final agent_typing_container:I = 0x7f0a01a9

.field public static final agent_typing_indicator:I = 0x7f0a01aa

.field public static final agreement_label:I = 0x7f0a01ab

.field public static final agreement_label_signature_buttons_container:I = 0x7f0a01ac

.field public static final alertTitle:I = 0x7f0a01ad

.field public static final all:I = 0x7f0a01ae

.field public static final all_employees:I = 0x7f0a01af

.field public static final allow_automatic_payment:I = 0x7f0a01b0

.field public static final allow_automatic_payment_toggle:I = 0x7f0a01b1

.field public static final allow_save_cof_helper:I = 0x7f0a01b2

.field public static final allow_year_check:I = 0x7f0a01b3

.field public static final always:I = 0x7f0a01b4

.field public static final always_skip_signature:I = 0x7f0a01b5

.field public static final am_pm_picker:I = 0x7f0a01b6

.field public static final amount:I = 0x7f0a01b7

.field public static final amount_button:I = 0x7f0a01b8

.field public static final amount_cell_label:I = 0x7f0a01b9

.field public static final amount_cell_percentage_value:I = 0x7f0a01ba

.field public static final amount_cell_value:I = 0x7f0a01bb

.field public static final amount_container:I = 0x7f0a01bc

.field public static final amount_field:I = 0x7f0a01bd

.field public static final amount_hint:I = 0x7f0a01be

.field public static final amount_value:I = 0x7f0a01bf

.field public static final amounts_panel:I = 0x7f0a01c0

.field public static final ampm_picker:I = 0x7f0a01c1

.field public static final animator:I = 0x7f0a01c2

.field public static final announcements_list_animator:I = 0x7f0a01c3

.field public static final answer_row:I = 0x7f0a01c4

.field public static final apartment:I = 0x7f0a01c5

.field public static final applet_badge:I = 0x7f0a01c6

.field public static final applet_sections_list:I = 0x7f0a01c7

.field public static final applet_title:I = 0x7f0a01c8

.field public static final applets_bottom_container:I = 0x7f0a01c9

.field public static final applets_drawer_glyph:I = 0x7f0a01ca

.field public static final applets_drawer_items_applet:I = 0x7f0a01cb

.field public static final applets_drawer_order_entry_applet:I = 0x7f0a01cc

.field public static final applets_list_container:I = 0x7f0a01cd

.field public static final applets_main_list:I = 0x7f0a01ce

.field public static final arrow:I = 0x7f0a01cf

.field public static final assigned_employees:I = 0x7f0a01d0

.field public static final async:I = 0x7f0a01d1

.field public static final attachment_date:I = 0x7f0a01d2

.field public static final attachment_file_name:I = 0x7f0a01d3

.field public static final attachment_file_size:I = 0x7f0a01d4

.field public static final attachment_icon:I = 0x7f0a01d5

.field public static final attribute_pair_key:I = 0x7f0a01d6

.field public static final attribute_pair_value:I = 0x7f0a01d7

.field public static final authentication_failed_body:I = 0x7f0a01d8

.field public static final authentication_failed_title:I = 0x7f0a01d9

.field public static final authentication_key:I = 0x7f0a01da

.field public static final auto:I = 0x7f0a01db

.field public static final auto_complete_row_text:I = 0x7f0a01dc

.field public static final auto_email_enable_row:I = 0x7f0a01dd

.field public static final auto_print_enable_row:I = 0x7f0a01de

.field public static final automatic_itemized_receipt_switch:I = 0x7f0a01df

.field public static final automatic_payment_glyph:I = 0x7f0a01e0

.field public static final automatic_payment_subtitle:I = 0x7f0a01e1

.field public static final automatic_payment_title:I = 0x7f0a01e2

.field public static final automatic_reminders_container:I = 0x7f0a01e3

.field public static final automatic_reminders_explanation:I = 0x7f0a01e4

.field public static final automatic_ticket_name_hint:I = 0x7f0a01e5

.field public static final automatic_ticket_names:I = 0x7f0a01e6

.field public static final available_balance:I = 0x7f0a01e7

.field public static final available_balance_title:I = 0x7f0a01e8

.field public static final available_options_search_bar:I = 0x7f0a01e9

.field public static final back:I = 0x7f0a01ea

.field public static final back_handler:I = 0x7f0a01eb

.field public static final back_stack_body:I = 0x7f0a01ec

.field public static final background_stars:I = 0x7f0a01ed

.field public static final backspace_glyph:I = 0x7f0a01ee

.field public static final badge:I = 0x7f0a01ef

.field public static final badge_view:I = 0x7f0a01f0

.field public static final badged_glyph_block:I = 0x7f0a01f1

.field public static final badged_icon_block:I = 0x7f0a01f2

.field public static final balance:I = 0x7f0a01f3

.field public static final balance_activities_child_container:I = 0x7f0a01f4

.field public static final balance_activities_list:I = 0x7f0a01f5

.field public static final balance_activities_list_container:I = 0x7f0a01f6

.field public static final balance_activities_loading:I = 0x7f0a01f7

.field public static final balance_activities_pull_to_refresh:I = 0x7f0a01f8

.field public static final balance_activities_title:I = 0x7f0a01f9

.field public static final balance_activity_action_bar:I = 0x7f0a01fa

.field public static final balance_activity_business_loading:I = 0x7f0a01fb

.field public static final balance_activity_description_list_state:I = 0x7f0a01fc

.field public static final balance_activity_details_amount:I = 0x7f0a01fd

.field public static final balance_activity_details_checkable_expense_type:I = 0x7f0a01fe

.field public static final balance_activity_details_expense_type_business:I = 0x7f0a01ff

.field public static final balance_activity_details_expense_type_personal:I = 0x7f0a0200

.field public static final balance_activity_details_expense_type_section:I = 0x7f0a0201

.field public static final balance_activity_details_expense_type_title:I = 0x7f0a0202

.field public static final balance_activity_details_footnote:I = 0x7f0a0203

.field public static final balance_activity_details_image:I = 0x7f0a0204

.field public static final balance_activity_details_list:I = 0x7f0a0205

.field public static final balance_activity_details_loaded_container:I = 0x7f0a0206

.field public static final balance_activity_details_main_description:I = 0x7f0a0207

.field public static final balance_activity_details_retry_button:I = 0x7f0a0208

.field public static final balance_activity_empty_text:I = 0x7f0a0209

.field public static final balance_activity_personal_loading:I = 0x7f0a020a

.field public static final balance_activity_retry:I = 0x7f0a020b

.field public static final balance_activity_tab_layout:I = 0x7f0a020c

.field public static final balance_activity_text:I = 0x7f0a020d

.field public static final balance_amount:I = 0x7f0a020e

.field public static final balance_card:I = 0x7f0a020f

.field public static final balance_due_date:I = 0x7f0a0210

.field public static final balance_empty_card_activity:I = 0x7f0a0211

.field public static final balance_error_card_activity:I = 0x7f0a0212

.field public static final balance_spinner:I = 0x7f0a0213

.field public static final balance_title:I = 0x7f0a0214

.field public static final balance_transactions:I = 0x7f0a0215

.field public static final bank_account_settings_view:I = 0x7f0a0216

.field public static final banner:I = 0x7f0a0217

.field public static final banner_close_button:I = 0x7f0a0218

.field public static final banner_download_app:I = 0x7f0a0219

.field public static final banner_icon:I = 0x7f0a021a

.field public static final banner_message:I = 0x7f0a021b

.field public static final banner_title:I = 0x7f0a021c

.field public static final bar_chart:I = 0x7f0a021d

.field public static final barcode_scanners_list:I = 0x7f0a021e

.field public static final barrier:I = 0x7f0a021f

.field public static final battery_image:I = 0x7f0a0220

.field public static final battery_message:I = 0x7f0a0221

.field public static final beginning:I = 0x7f0a0222

.field public static final below_first_row:I = 0x7f0a0223

.field public static final below_first_row_divider:I = 0x7f0a0224

.field public static final below_second_row:I = 0x7f0a0225

.field public static final below_second_row_divider:I = 0x7f0a0226

.field public static final below_third_row:I = 0x7f0a0227

.field public static final below_third_row_divider:I = 0x7f0a0228

.field public static final big_text_header:I = 0x7f0a0229

.field public static final bill_details_loading_panel:I = 0x7f0a022a

.field public static final bill_history_detail_empty_view_container:I = 0x7f0a022b

.field public static final bill_history_loyalty_section_row_subtitle:I = 0x7f0a022c

.field public static final bill_history_loyalty_section_row_title:I = 0x7f0a022d

.field public static final bill_history_loyalty_section_row_value:I = 0x7f0a022e

.field public static final bill_history_loyalty_section_title:I = 0x7f0a022f

.field public static final bill_history_view:I = 0x7f0a0230

.field public static final bill_history_view_content:I = 0x7f0a0231

.field public static final bill_history_view_loading_panel:I = 0x7f0a0232

.field public static final bill_to_row:I = 0x7f0a0233

.field public static final birth_date:I = 0x7f0a0234

.field public static final birth_date_header:I = 0x7f0a0235

.field public static final blank_icon:I = 0x7f0a0236

.field public static final blank_layout:I = 0x7f0a0237

.field public static final ble_actions:I = 0x7f0a0238

.field public static final blocking:I = 0x7f0a0239

.field public static final body:I = 0x7f0a023a

.field public static final body2:I = 0x7f0a023b

.field public static final bookable_by_customers_online:I = 0x7f0a023c

.field public static final boolean_attribute:I = 0x7f0a023d

.field public static final border_layout:I = 0x7f0a023e

.field public static final bottom:I = 0x7f0a023f

.field public static final bottom_buttons_container:I = 0x7f0a0240

.field public static final bottom_divider:I = 0x7f0a0241

.field public static final bottom_error_text:I = 0x7f0a0242

.field public static final bottom_horizontal_divider:I = 0x7f0a0243

.field public static final bottom_line:I = 0x7f0a0244

.field public static final bottom_margin_guide:I = 0x7f0a0245

.field public static final bottom_panel:I = 0x7f0a0246

.field public static final bottom_text_view:I = 0x7f0a0247

.field public static final bug_reporter_description:I = 0x7f0a0248

.field public static final bug_reporter_name:I = 0x7f0a0249

.field public static final bug_reporter_summary:I = 0x7f0a024a

.field public static final bulk_settle_content:I = 0x7f0a024b

.field public static final bulk_settle_focused_spacer:I = 0x7f0a024c

.field public static final bulk_settle_glyph_message:I = 0x7f0a024d

.field public static final bulk_settle_header:I = 0x7f0a024e

.field public static final bulk_settle_loading:I = 0x7f0a024f

.field public static final bulk_settle_primary_button:I = 0x7f0a0250

.field public static final bulk_settle_quicktip_editor:I = 0x7f0a0251

.field public static final bulk_settle_search:I = 0x7f0a0252

.field public static final bulk_settle_sort_amount:I = 0x7f0a0253

.field public static final bulk_settle_sort_employee:I = 0x7f0a0254

.field public static final bulk_settle_sort_options:I = 0x7f0a0255

.field public static final bulk_settle_sort_time:I = 0x7f0a0256

.field public static final bulk_settle_tender_amount:I = 0x7f0a0257

.field public static final bulk_settle_tender_employee_name:I = 0x7f0a0258

.field public static final bulk_settle_tender_receipt_number:I = 0x7f0a0259

.field public static final bulk_settle_tender_row_total:I = 0x7f0a025a

.field public static final bulk_settle_tender_time_with_employee_name:I = 0x7f0a025b

.field public static final bulk_settle_tender_time_with_no_employee_name:I = 0x7f0a025c

.field public static final bulk_settle_tenders_list:I = 0x7f0a025d

.field public static final bulk_settle_tenders_list_container:I = 0x7f0a025e

.field public static final bulk_settle_time_and_employee_name_container:I = 0x7f0a025f

.field public static final bulk_settle_up_button:I = 0x7f0a0260

.field public static final bulk_settle_up_text:I = 0x7f0a0261

.field public static final bulk_settle_view_all_button:I = 0x7f0a0262

.field public static final bulk_settle_view_all_container:I = 0x7f0a0263

.field public static final business_address_different_as_home:I = 0x7f0a0264

.field public static final business_address_edit_view:I = 0x7f0a0265

.field public static final business_address_help_text:I = 0x7f0a0266

.field public static final business_address_label:I = 0x7f0a0267

.field public static final business_address_mobile_business:I = 0x7f0a0268

.field public static final business_address_mobile_business_label:I = 0x7f0a0269

.field public static final business_address_same_as_home:I = 0x7f0a026a

.field public static final business_address_same_as_home_group:I = 0x7f0a026b

.field public static final business_address_section:I = 0x7f0a026c

.field public static final business_address_view:I = 0x7f0a026d

.field public static final business_checking:I = 0x7f0a026e

.field public static final business_info_business_address:I = 0x7f0a026f

.field public static final business_info_container:I = 0x7f0a0270

.field public static final business_name:I = 0x7f0a0271

.field public static final button:I = 0x7f0a0272

.field public static final button1:I = 0x7f0a0273

.field public static final button2:I = 0x7f0a0274

.field public static final buttonContainer:I = 0x7f0a0275

.field public static final buttonPanel:I = 0x7f0a0276

.field public static final button_container:I = 0x7f0a0277

.field public static final button_containers:I = 0x7f0a0278

.field public static final button_retry:I = 0x7f0a0279

.field public static final buttons_container:I = 0x7f0a027a

.field public static final buttons_separator:I = 0x7f0a027b

.field public static final buyer_action_bar:I = 0x7f0a027c

.field public static final buyer_action_container:I = 0x7f0a027d

.field public static final buyer_action_options:I = 0x7f0a027e

.field public static final buyer_actionbar_call_to_action:I = 0x7f0a027f

.field public static final buyer_actionbar_customer_add_card_button:I = 0x7f0a0280

.field public static final buyer_actionbar_customer_button:I = 0x7f0a0281

.field public static final buyer_actionbar_left_flank:I = 0x7f0a0282

.field public static final buyer_actionbar_name:I = 0x7f0a0283

.field public static final buyer_actionbar_new_sale_button:I = 0x7f0a0284

.field public static final buyer_actionbar_subtitle:I = 0x7f0a0285

.field public static final buyer_actionbar_total:I = 0x7f0a0286

.field public static final buyer_actionbar_up_glyph:I = 0x7f0a0287

.field public static final buyer_cart_view:I = 0x7f0a0288

.field public static final buyer_email:I = 0x7f0a0289

.field public static final buyer_floating_content:I = 0x7f0a028a

.field public static final buyer_language_button:I = 0x7f0a028b

.field public static final buyer_loyalty_account_container:I = 0x7f0a028c

.field public static final buyer_name:I = 0x7f0a028d

.field public static final buyer_select_canada_english:I = 0x7f0a028e

.field public static final buyer_select_canada_french:I = 0x7f0a028f

.field public static final buyer_select_japanese:I = 0x7f0a0290

.field public static final buyer_select_spanish:I = 0x7f0a0291

.field public static final buyer_send_receipt_digital_hint:I = 0x7f0a0292

.field public static final buyer_ticket_name:I = 0x7f0a0293

.field public static final buyer_up_button:I = 0x7f0a0294

.field public static final calendar_grid:I = 0x7f0a0295

.field public static final calendar_view:I = 0x7f0a0296

.field public static final call_to_action:I = 0x7f0a0297

.field public static final cancel_action:I = 0x7f0a0298

.field public static final cancel_button:I = 0x7f0a0299

.field public static final cancel_invoice_button:I = 0x7f0a029a

.field public static final cancel_square_card_choice:I = 0x7f0a029b

.field public static final cancel_square_card_message:I = 0x7f0a029c

.field public static final cancel_verification_button:I = 0x7f0a029d

.field public static final canceled_card_feedback:I = 0x7f0a029e

.field public static final canceled_card_submit_feedback:I = 0x7f0a029f

.field public static final cancellation_fee_row:I = 0x7f0a02a0

.field public static final cant_scan_barcode:I = 0x7f0a02a1

.field public static final canvas:I = 0x7f0a02a2

.field public static final capital_application_pending_body_message:I = 0x7f0a02a3

.field public static final capital_application_pending_body_title:I = 0x7f0a02a4

.field public static final capital_application_pending_hero_message:I = 0x7f0a02a5

.field public static final capital_application_pending_hero_title:I = 0x7f0a02a6

.field public static final capital_application_pending_icon:I = 0x7f0a02a7

.field public static final capital_error_message_view:I = 0x7f0a02a8

.field public static final capital_loading_bar:I = 0x7f0a02a9

.field public static final capital_no_offer_icon:I = 0x7f0a02aa

.field public static final capital_no_offer_message:I = 0x7f0a02ab

.field public static final capital_no_offer_title:I = 0x7f0a02ac

.field public static final capital_offer_choose_action:I = 0x7f0a02ad

.field public static final capital_offer_hero_amount:I = 0x7f0a02ae

.field public static final capital_offer_hero_icon:I = 0x7f0a02af

.field public static final capital_offer_hero_text:I = 0x7f0a02b0

.field public static final card:I = 0x7f0a02b1

.field public static final card_activation_confirm_code_title:I = 0x7f0a02b2

.field public static final card_brand:I = 0x7f0a02b3

.field public static final card_container:I = 0x7f0a02b4

.field public static final card_details_container:I = 0x7f0a02b5

.field public static final card_disabled:I = 0x7f0a02b6

.field public static final card_drawing:I = 0x7f0a02b7

.field public static final card_editor:I = 0x7f0a02b8

.field public static final card_glyph:I = 0x7f0a02b9

.field public static final card_on_file_card_expired_text:I = 0x7f0a02ba

.field public static final card_on_file_card_name_number:I = 0x7f0a02bb

.field public static final card_on_file_charge_button:I = 0x7f0a02bc

.field public static final card_ordered_deposits_info_title:I = 0x7f0a02bd

.field public static final card_payments_title:I = 0x7f0a02be

.field public static final card_present_refund_message_panel:I = 0x7f0a02bf

.field public static final card_present_refund_view:I = 0x7f0a02c0

.field public static final card_preview:I = 0x7f0a02c1

.field public static final card_preview_chip:I = 0x7f0a02c2

.field public static final card_preview_constraint_container:I = 0x7f0a02c3

.field public static final card_preview_logo:I = 0x7f0a02c4

.field public static final card_preview_magstripe:I = 0x7f0a02c5

.field public static final card_preview_square_logo:I = 0x7f0a02c6

.field public static final card_reader_details_help_message_row:I = 0x7f0a02c7

.field public static final card_row:I = 0x7f0a02c8

.field public static final card_slot:I = 0x7f0a02c9

.field public static final card_suspended_message:I = 0x7f0a02ca

.field public static final cart_container:I = 0x7f0a02cb

.field public static final cart_diff_highlight:I = 0x7f0a02cc

.field public static final cart_entry_image:I = 0x7f0a02cd

.field public static final cart_entry_row_amount:I = 0x7f0a02ce

.field public static final cart_entry_row_name_and_quantity:I = 0x7f0a02cf

.field public static final cart_entry_row_sub_label:I = 0x7f0a02d0

.field public static final cart_header:I = 0x7f0a02d1

.field public static final cart_header_current_sale:I = 0x7f0a02d2

.field public static final cart_header_current_sale_label:I = 0x7f0a02d3

.field public static final cart_header_flyout:I = 0x7f0a02d4

.field public static final cart_header_no_sale:I = 0x7f0a02d5

.field public static final cart_header_sale_quantity:I = 0x7f0a02d6

.field public static final cart_list:I = 0x7f0a02d7

.field public static final cart_menu_add_customer:I = 0x7f0a02d8

.field public static final cart_menu_arrow_button:I = 0x7f0a02d9

.field public static final cart_menu_clear_new_items:I = 0x7f0a02da

.field public static final cart_menu_drop_down_container:I = 0x7f0a02db

.field public static final cart_menu_edit_ticket:I = 0x7f0a02dc

.field public static final cart_menu_merge_ticket:I = 0x7f0a02dd

.field public static final cart_menu_move_ticket:I = 0x7f0a02de

.field public static final cart_menu_print_bill:I = 0x7f0a02df

.field public static final cart_menu_reprint_ticket:I = 0x7f0a02e0

.field public static final cart_menu_split_ticket:I = 0x7f0a02e1

.field public static final cart_menu_transfer_ticket:I = 0x7f0a02e2

.field public static final cart_menu_view_customer:I = 0x7f0a02e3

.field public static final cart_recycler:I = 0x7f0a02e4

.field public static final cart_recycler_view:I = 0x7f0a02e5

.field public static final cart_ticket_animation_overlay:I = 0x7f0a02e6

.field public static final cart_title:I = 0x7f0a02e7

.field public static final cash_drawer_details:I = 0x7f0a02e8

.field public static final cash_drawer_settings_list:I = 0x7f0a02e9

.field public static final cash_drawer_warning:I = 0x7f0a02ea

.field public static final cash_management_email_recipient:I = 0x7f0a02eb

.field public static final cash_management_enable_row:I = 0x7f0a02ec

.field public static final cash_management_settings_disabled_state_message:I = 0x7f0a02ed

.field public static final cash_option_custom:I = 0x7f0a02ee

.field public static final cash_out_reason:I = 0x7f0a02ef

.field public static final cash_refunds:I = 0x7f0a02f0

.field public static final cash_sales:I = 0x7f0a02f1

.field public static final categories_list_layout:I = 0x7f0a02f2

.field public static final categories_progress:I = 0x7f0a02f3

.field public static final category_drop_down_arrow:I = 0x7f0a02f4

.field public static final category_drop_down_title:I = 0x7f0a02f5

.field public static final category_list:I = 0x7f0a02f6

.field public static final category_name:I = 0x7f0a02f7

.field public static final category_row_delete:I = 0x7f0a02f8

.field public static final category_row_name:I = 0x7f0a02f9

.field public static final center:I = 0x7f0a02fa

.field public static final center_horizontal:I = 0x7f0a02fb

.field public static final center_vertical:I = 0x7f0a02fc

.field public static final chains:I = 0x7f0a02fd

.field public static final challenge_button:I = 0x7f0a02fe

.field public static final challenge_summary:I = 0x7f0a02ff

.field public static final change:I = 0x7f0a0300

.field public static final change_account_button:I = 0x7f0a0301

.field public static final change_bank_account_button:I = 0x7f0a0302

.field public static final change_glyph:I = 0x7f0a0303

.field public static final changes_unavailable_header:I = 0x7f0a0304

.field public static final changes_unavailable_subtext:I = 0x7f0a0305

.field public static final charge_and_continue_button:I = 0x7f0a0306

.field public static final charge_and_ticket_buttons:I = 0x7f0a0307

.field public static final charge_and_ticket_saved_alert:I = 0x7f0a0308

.field public static final charge_and_tickets_buttons_confirm_overlay:I = 0x7f0a0309

.field public static final charge_and_tickets_buttons_container:I = 0x7f0a030a

.field public static final charge_button_confirm_overlay:I = 0x7f0a030b

.field public static final charge_button_container:I = 0x7f0a030c

.field public static final charge_button_subtitle:I = 0x7f0a030d

.field public static final charge_button_title:I = 0x7f0a030e

.field public static final check:I = 0x7f0a030f

.field public static final check_bank_account_info_view:I = 0x7f0a0310

.field public static final check_compatibility_reader_button:I = 0x7f0a0311

.field public static final check_password_view:I = 0x7f0a0312

.field public static final check_row_check:I = 0x7f0a0313

.field public static final check_row_title:I = 0x7f0a0314

.field public static final checkable:I = 0x7f0a0315

.field public static final checkable_row:I = 0x7f0a0316

.field public static final checkable_ticket_group:I = 0x7f0a0317

.field public static final checkbox:I = 0x7f0a0318

.field public static final checked:I = 0x7f0a0319

.field public static final checking:I = 0x7f0a031a

.field public static final checkout_applet_main_view:I = 0x7f0a031b

.field public static final checkout_link:I = 0x7f0a031c

.field public static final checkout_link_container:I = 0x7f0a031d

.field public static final checkout_link_header:I = 0x7f0a031e

.field public static final checkout_link_list_screen_container:I = 0x7f0a031f

.field public static final checkout_link_screen_container:I = 0x7f0a0320

.field public static final checkout_link_screen_name_label:I = 0x7f0a0321

.field public static final checkout_link_settings_help_text:I = 0x7f0a0322

.field public static final checkout_link_share_help_text:I = 0x7f0a0323

.field public static final checkout_link_toggle:I = 0x7f0a0324

.field public static final checkoutlink_deactivate_confirmation_dialog:I = 0x7f0a0325

.field public static final checkoutlink_delete_confirmation_dialog:I = 0x7f0a0326

.field public static final checkoutlink_help_text:I = 0x7f0a0327

.field public static final checkoutlink_url:I = 0x7f0a0328

.field public static final chevron:I = 0x7f0a0329

.field public static final chevron_block:I = 0x7f0a032a

.field public static final chip:I = 0x7f0a032b

.field public static final chip_group:I = 0x7f0a032c

.field public static final choose_card_on_file_cards:I = 0x7f0a032d

.field public static final choose_card_on_file_cards_label:I = 0x7f0a032e

.field public static final choose_card_on_file_help_text:I = 0x7f0a032f

.field public static final choose_cof_customer_contact_list:I = 0x7f0a0330

.field public static final choose_cof_customer_search_box:I = 0x7f0a0331

.field public static final choose_cof_customer_search_message:I = 0x7f0a0332

.field public static final choose_cof_customer_search_progress_bar:I = 0x7f0a0333

.field public static final choose_days:I = 0x7f0a0334

.field public static final choose_days_offset:I = 0x7f0a0335

.field public static final choose_gift_card_on_file:I = 0x7f0a0336

.field public static final choose_interval_unit:I = 0x7f0a0337

.field public static final choose_photo:I = 0x7f0a0338

.field public static final choose_reward_item_container:I = 0x7f0a0339

.field public static final chronometer:I = 0x7f0a033a

.field public static final circle:I = 0x7f0a033b

.field public static final circle_initials_text:I = 0x7f0a033c

.field public static final clear:I = 0x7f0a033d

.field public static final clear_balance_button:I = 0x7f0a033e

.field public static final clear_glyph:I = 0x7f0a033f

.field public static final clear_text:I = 0x7f0a0340

.field public static final clickRemove:I = 0x7f0a0341

.field public static final clip_horizontal:I = 0x7f0a0342

.field public static final clip_vertical:I = 0x7f0a0343

.field public static final clock_in_button:I = 0x7f0a0344

.field public static final clock_in_clock_glyph:I = 0x7f0a0345

.field public static final clock_in_out_back:I = 0x7f0a0346

.field public static final clock_in_out_button:I = 0x7f0a0347

.field public static final clock_in_pad:I = 0x7f0a0348

.field public static final clock_in_pad_container:I = 0x7f0a0349

.field public static final clock_in_progress_bar:I = 0x7f0a034a

.field public static final clock_in_server_error:I = 0x7f0a034b

.field public static final clock_in_sheet_message:I = 0x7f0a034c

.field public static final clock_in_sheet_progress_bar:I = 0x7f0a034d

.field public static final clock_in_sheet_success:I = 0x7f0a034e

.field public static final clock_in_sheet_title:I = 0x7f0a034f

.field public static final clock_in_star_group:I = 0x7f0a0350

.field public static final clock_in_success:I = 0x7f0a0351

.field public static final clock_in_title:I = 0x7f0a0352

.field public static final clock_out_success_container:I = 0x7f0a0353

.field public static final clock_out_success_hours:I = 0x7f0a0354

.field public static final clock_out_success_minutes:I = 0x7f0a0355

.field public static final clock_settings_button:I = 0x7f0a0356

.field public static final clock_skew_glyph:I = 0x7f0a0357

.field public static final clock_skew_message:I = 0x7f0a0358

.field public static final clock_skew_title:I = 0x7f0a0359

.field public static final clockout_confirmation_notes_button:I = 0x7f0a035a

.field public static final clockout_summary_view_notes_button:I = 0x7f0a035b

.field public static final close:I = 0x7f0a035c

.field public static final close_of_day_pickers:I = 0x7f0a035d

.field public static final close_of_day_row:I = 0x7f0a035e

.field public static final closed:I = 0x7f0a035f

.field public static final cnp_disabled_view:I = 0x7f0a0360

.field public static final collapse:I = 0x7f0a0361

.field public static final collapseActionView:I = 0x7f0a0362

.field public static final collapse_toolbar:I = 0x7f0a0363

.field public static final collected:I = 0x7f0a0364

.field public static final colon:I = 0x7f0a0365

.field public static final color_grid:I = 0x7f0a0366

.field public static final community_reward_amount_label:I = 0x7f0a0367

.field public static final community_reward_amount_value:I = 0x7f0a0368

.field public static final community_reward_description:I = 0x7f0a0369

.field public static final community_reward_description_link:I = 0x7f0a036a

.field public static final community_reward_instant_discount_label:I = 0x7f0a036b

.field public static final community_reward_instant_discount_value:I = 0x7f0a036c

.field public static final community_reward_total_label:I = 0x7f0a036d

.field public static final community_reward_total_value:I = 0x7f0a036e

.field public static final compare_label:I = 0x7f0a036f

.field public static final compare_value:I = 0x7f0a0370

.field public static final comparison_range_label:I = 0x7f0a0371

.field public static final complete:I = 0x7f0a0372

.field public static final components_recycler:I = 0x7f0a0373

.field public static final conditional_taxes_help_text:I = 0x7f0a0374

.field public static final config_all_day:I = 0x7f0a0375

.field public static final config_all_devices:I = 0x7f0a0376

.field public static final config_device_filter:I = 0x7f0a0377

.field public static final config_device_filter_container:I = 0x7f0a0378

.field public static final config_employee_filter:I = 0x7f0a0379

.field public static final config_employee_filter_container:I = 0x7f0a037a

.field public static final config_end_time_row:I = 0x7f0a037b

.field public static final config_item_details:I = 0x7f0a037c

.field public static final config_start_time_row:I = 0x7f0a037d

.field public static final config_this_device:I = 0x7f0a037e

.field public static final configure_item_checkable_row:I = 0x7f0a037f

.field public static final configure_item_description:I = 0x7f0a0380

.field public static final configure_item_detail_view:I = 0x7f0a0381

.field public static final configure_item_duration:I = 0x7f0a0382

.field public static final configure_item_duration_container:I = 0x7f0a0383

.field public static final configure_item_final_duration:I = 0x7f0a0384

.field public static final configure_item_gap_duration:I = 0x7f0a0385

.field public static final configure_item_gap_time_container:I = 0x7f0a0386

.field public static final configure_item_gap_time_toggle:I = 0x7f0a0387

.field public static final configure_item_initial_duration:I = 0x7f0a0388

.field public static final configure_item_note:I = 0x7f0a0389

.field public static final confirm_and_pay_button:I = 0x7f0a038a

.field public static final confirm_button:I = 0x7f0a038b

.field public static final confirm_email:I = 0x7f0a038c

.field public static final confirm_transfer_view:I = 0x7f0a038d

.field public static final confirmation_glyph_message:I = 0x7f0a038e

.field public static final confirmation_text:I = 0x7f0a038f

.field public static final connect_reader_button:I = 0x7f0a0390

.field public static final connect_reader_wirelessly:I = 0x7f0a0391

.field public static final connected_scales_container:I = 0x7f0a0392

.field public static final connected_scales_layout:I = 0x7f0a0393

.field public static final contact_container:I = 0x7f0a0394

.field public static final contact_email:I = 0x7f0a0395

.field public static final contact_name:I = 0x7f0a0396

.field public static final contact_phone:I = 0x7f0a0397

.field public static final contact_support:I = 0x7f0a0398

.field public static final contact_us_button:I = 0x7f0a0399

.field public static final contact_us_hint_text:I = 0x7f0a039a

.field public static final contact_us_view:I = 0x7f0a039b

.field public static final contactless_learn_more_view:I = 0x7f0a039c

.field public static final contacts_empty_view:I = 0x7f0a039d

.field public static final contacts_header:I = 0x7f0a039e

.field public static final contacts_list:I = 0x7f0a039f

.field public static final contacts_search:I = 0x7f0a03a0

.field public static final container:I = 0x7f0a03a1

.field public static final container_layout:I = 0x7f0a03a2

.field public static final content:I = 0x7f0a03a3

.field public static final contentPanel:I = 0x7f0a03a4

.field public static final content_cont:I = 0x7f0a03a5

.field public static final content_frame:I = 0x7f0a03a6

.field public static final content_view:I = 0x7f0a03a7

.field public static final contents:I = 0x7f0a03a8

.field public static final continue_button:I = 0x7f0a03a9

.field public static final conversation_closed_view:I = 0x7f0a03aa

.field public static final conversation_redacted_view:I = 0x7f0a03ab

.field public static final conversations_divider:I = 0x7f0a03ac

.field public static final coordinator:I = 0x7f0a03ad

.field public static final copy_code:I = 0x7f0a03ae

.field public static final copy_link:I = 0x7f0a03af

.field public static final count_row:I = 0x7f0a03b0

.field public static final counter_view_minus_icon:I = 0x7f0a03b1

.field public static final counter_view_number_label:I = 0x7f0a03b2

.field public static final counter_view_plus_icon:I = 0x7f0a03b3

.field public static final country_picker:I = 0x7f0a03b4

.field public static final coupon_business_name:I = 0x7f0a03b5

.field public static final coupon_content:I = 0x7f0a03b6

.field public static final coupon_empty_label:I = 0x7f0a03b7

.field public static final coupon_reason:I = 0x7f0a03b8

.field public static final coupon_recycler_view:I = 0x7f0a03b9

.field public static final coupon_redeem_cancel:I = 0x7f0a03ba

.field public static final coupon_redeem_claim_button:I = 0x7f0a03bb

.field public static final coupon_redeem_title:I = 0x7f0a03bc

.field public static final coupon_ribbon:I = 0x7f0a03bd

.field public static final coupon_value:I = 0x7f0a03be

.field public static final create_account:I = 0x7f0a03bf

.field public static final create_account_account:I = 0x7f0a03c0

.field public static final create_account_action_bar:I = 0x7f0a03c1

.field public static final create_account_container:I = 0x7f0a03c2

.field public static final create_discount_button:I = 0x7f0a03c3

.field public static final create_item_button:I = 0x7f0a03c4

.field public static final create_link_button:I = 0x7f0a03c5

.field public static final create_link_screen_container:I = 0x7f0a03c6

.field public static final create_links_help_text_container:I = 0x7f0a03c7

.field public static final create_passcode_action_bar:I = 0x7f0a03c8

.field public static final create_passcode_container:I = 0x7f0a03c9

.field public static final create_passcode_description:I = 0x7f0a03ca

.field public static final create_passcode_digit_group:I = 0x7f0a03cb

.field public static final create_passcode_header:I = 0x7f0a03cc

.field public static final create_passcode_pin_pad:I = 0x7f0a03cd

.field public static final create_passcode_progress_bar:I = 0x7f0a03ce

.field public static final create_passcode_progress_bar_spinner:I = 0x7f0a03cf

.field public static final create_passcode_progress_bar_title:I = 0x7f0a03d0

.field public static final create_passcode_star_group:I = 0x7f0a03d1

.field public static final create_passcode_success_action_bar:I = 0x7f0a03d2

.field public static final create_passcode_success_button:I = 0x7f0a03d3

.field public static final create_passcode_success_container:I = 0x7f0a03d4

.field public static final create_passcode_success_description:I = 0x7f0a03d5

.field public static final create_passcode_success_glyph:I = 0x7f0a03d6

.field public static final create_passcode_success_progress_bar:I = 0x7f0a03d7

.field public static final create_passcode_success_progress_bar_spinner:I = 0x7f0a03d8

.field public static final create_passcode_success_progress_bar_title:I = 0x7f0a03d9

.field public static final create_ticket_group_button:I = 0x7f0a03da

.field public static final create_variation_button:I = 0x7f0a03db

.field public static final crm_action_create_manual_group:I = 0x7f0a03dc

.field public static final crm_activity_list:I = 0x7f0a03dd

.field public static final crm_activity_list_section:I = 0x7f0a03de

.field public static final crm_activity_progress_bar:I = 0x7f0a03df

.field public static final crm_add_coupon_button:I = 0x7f0a03e0

.field public static final crm_add_coupon_row_title:I = 0x7f0a03e1

.field public static final crm_add_filter:I = 0x7f0a03e2

.field public static final crm_add_from_address_book:I = 0x7f0a03e3

.field public static final crm_add_points_button:I = 0x7f0a03e4

.field public static final crm_additional_info_section:I = 0x7f0a03e5

.field public static final crm_address:I = 0x7f0a03e6

.field public static final crm_address_label:I = 0x7f0a03e7

.field public static final crm_address_view_stub:I = 0x7f0a03e8

.field public static final crm_all_frequent_items_header:I = 0x7f0a03e9

.field public static final crm_all_frequent_items_rows:I = 0x7f0a03ea

.field public static final crm_all_frequent_items_view:I = 0x7f0a03eb

.field public static final crm_all_notes_rows:I = 0x7f0a03ec

.field public static final crm_applet_conversation_container:I = 0x7f0a03ed

.field public static final crm_appointments_list:I = 0x7f0a03ee

.field public static final crm_bill_history_button:I = 0x7f0a03ef

.field public static final crm_bill_history_view:I = 0x7f0a03f0

.field public static final crm_birthday_field:I = 0x7f0a03f1

.field public static final crm_birthday_picker:I = 0x7f0a03f2

.field public static final crm_birthday_picker_date_picker:I = 0x7f0a03f3

.field public static final crm_birthday_picker_provide_year:I = 0x7f0a03f4

.field public static final crm_birthday_picker_provide_year_check:I = 0x7f0a03f5

.field public static final crm_bottom_button:I = 0x7f0a03f6

.field public static final crm_bottom_button_row:I = 0x7f0a03f7

.field public static final crm_buyer_summary_section:I = 0x7f0a03f8

.field public static final crm_card_input_row_editor:I = 0x7f0a03f9

.field public static final crm_cardonfile_authorize_add_button:I = 0x7f0a03fa

.field public static final crm_cardonfile_card_glyph:I = 0x7f0a03fb

.field public static final crm_cardonfile_card_name:I = 0x7f0a03fc

.field public static final crm_cardonfile_card_status:I = 0x7f0a03fd

.field public static final crm_cardonfile_customeremail_message:I = 0x7f0a03fe

.field public static final crm_cardonfile_customeremail_title:I = 0x7f0a03ff

.field public static final crm_cardonfile_email:I = 0x7f0a0400

.field public static final crm_cardonfile_email_disclaimer:I = 0x7f0a0401

.field public static final crm_cardonfile_next_button:I = 0x7f0a0402

.field public static final crm_cardonfile_postal_disclaimer:I = 0x7f0a0403

.field public static final crm_cardonfile_postal_keyboard_switch:I = 0x7f0a0404

.field public static final crm_cardonfile_postalcode:I = 0x7f0a0405

.field public static final crm_cardonfile_postalcode_message:I = 0x7f0a0406

.field public static final crm_cardonfile_postalcode_title:I = 0x7f0a0407

.field public static final crm_cardonfile_unlink_card_button:I = 0x7f0a0408

.field public static final crm_choose_customer_progress_bar:I = 0x7f0a0409

.field public static final crm_choose_customer_recycler_view:I = 0x7f0a040a

.field public static final crm_choose_customer_screen:I = 0x7f0a040b

.field public static final crm_choose_customer_search_message:I = 0x7f0a040c

.field public static final crm_company_field:I = 0x7f0a040d

.field public static final crm_contact_edit:I = 0x7f0a040e

.field public static final crm_contact_list:I = 0x7f0a040f

.field public static final crm_contact_list_bottom_row_message:I = 0x7f0a0410

.field public static final crm_contact_list_bottom_row_progress:I = 0x7f0a0411

.field public static final crm_contact_list_v2:I = 0x7f0a0412

.field public static final crm_content_container:I = 0x7f0a0413

.field public static final crm_conversation:I = 0x7f0a0414

.field public static final crm_conversation_card_screen:I = 0x7f0a0415

.field public static final crm_conversation_rows:I = 0x7f0a0416

.field public static final crm_coupon_check_row:I = 0x7f0a0417

.field public static final crm_coupon_container:I = 0x7f0a0418

.field public static final crm_coupon_row:I = 0x7f0a0419

.field public static final crm_create_customer_from_search_term_button:I = 0x7f0a041a

.field public static final crm_create_group:I = 0x7f0a041b

.field public static final crm_create_new_customer:I = 0x7f0a041c

.field public static final crm_create_note:I = 0x7f0a041d

.field public static final crm_create_note_reminder:I = 0x7f0a041e

.field public static final crm_current_balance_calculation:I = 0x7f0a041f

.field public static final crm_custom_date_attr_datepicker:I = 0x7f0a0420

.field public static final crm_customer_card_row_view:I = 0x7f0a0421

.field public static final crm_customer_check_circle:I = 0x7f0a0422

.field public static final crm_customer_contact_info:I = 0x7f0a0423

.field public static final crm_customer_contact_line_pii_wrapper:I = 0x7f0a0424

.field public static final crm_customer_display_name:I = 0x7f0a0425

.field public static final crm_customer_display_name_pii_wrapper:I = 0x7f0a0426

.field public static final crm_customer_initial_circle:I = 0x7f0a0427

.field public static final crm_customer_initial_circle_container:I = 0x7f0a0428

.field public static final crm_customer_lookup:I = 0x7f0a0429

.field public static final crm_customer_management_in_cart_toggle:I = 0x7f0a042a

.field public static final crm_customer_management_post_transaction_label:I = 0x7f0a042b

.field public static final crm_customer_management_post_transaction_save_card_label:I = 0x7f0a042c

.field public static final crm_customer_management_post_transaction_save_card_toggle:I = 0x7f0a042d

.field public static final crm_customer_management_post_transaction_toggle:I = 0x7f0a042e

.field public static final crm_customer_management_save_card_divider:I = 0x7f0a042f

.field public static final crm_customer_management_save_card_label:I = 0x7f0a0430

.field public static final crm_customer_management_save_card_post_transaction_container:I = 0x7f0a0431

.field public static final crm_customer_management_save_card_post_transaction_divider:I = 0x7f0a0432

.field public static final crm_customer_management_save_card_toggle:I = 0x7f0a0433

.field public static final crm_customer_name_pii_wrapper:I = 0x7f0a0434

.field public static final crm_customer_status_line:I = 0x7f0a0435

.field public static final crm_delete_group:I = 0x7f0a0436

.field public static final crm_delete_note_button:I = 0x7f0a0437

.field public static final crm_drop_down_container:I = 0x7f0a0438

.field public static final crm_edit_attributes:I = 0x7f0a0439

.field public static final crm_email_collection_settings_screen_hint:I = 0x7f0a043a

.field public static final crm_email_collection_settings_screen_toggle:I = 0x7f0a043b

.field public static final crm_email_field:I = 0x7f0a043c

.field public static final crm_email_input_box:I = 0x7f0a043d

.field public static final crm_empty_filter_content:I = 0x7f0a043e

.field public static final crm_empty_filter_content_stub:I = 0x7f0a043f

.field public static final crm_enum_options:I = 0x7f0a0440

.field public static final crm_enum_recycler_view:I = 0x7f0a0441

.field public static final crm_errors_bar:I = 0x7f0a0442

.field public static final crm_expiring_points:I = 0x7f0a0443

.field public static final crm_expiring_points_row:I = 0x7f0a0444

.field public static final crm_filter_bubble_name:I = 0x7f0a0445

.field public static final crm_filter_bubble_name_container:I = 0x7f0a0446

.field public static final crm_filter_bubble_x:I = 0x7f0a0447

.field public static final crm_filter_layout_linearlayout:I = 0x7f0a0448

.field public static final crm_filter_list:I = 0x7f0a0449

.field public static final crm_filter_search_box:I = 0x7f0a044a

.field public static final crm_filter_search_empty:I = 0x7f0a044b

.field public static final crm_filters_layout:I = 0x7f0a044c

.field public static final crm_first_name_field:I = 0x7f0a044d

.field public static final crm_footer:I = 0x7f0a044e

.field public static final crm_frequent_items_section_header:I = 0x7f0a044f

.field public static final crm_frequent_items_section_rows:I = 0x7f0a0450

.field public static final crm_frequent_items_section_view_all_button:I = 0x7f0a0451

.field public static final crm_group_edit:I = 0x7f0a0452

.field public static final crm_group_list:I = 0x7f0a0453

.field public static final crm_group_name:I = 0x7f0a0454

.field public static final crm_group_name_description:I = 0x7f0a0455

.field public static final crm_group_name_field:I = 0x7f0a0456

.field public static final crm_group_name_input:I = 0x7f0a0457

.field public static final crm_group_name_label:I = 0x7f0a0458

.field public static final crm_group_row_name:I = 0x7f0a0459

.field public static final crm_groups:I = 0x7f0a045a

.field public static final crm_groups_container:I = 0x7f0a045b

.field public static final crm_groups_create_new:I = 0x7f0a045c

.field public static final crm_groups_list:I = 0x7f0a045d

.field public static final crm_groups_message:I = 0x7f0a045e

.field public static final crm_groups_progress_bar:I = 0x7f0a045f

.field public static final crm_groups_recycler_view:I = 0x7f0a0460

.field public static final crm_groups_scroll_view:I = 0x7f0a0461

.field public static final crm_initial_circle_frame_layout:I = 0x7f0a0462

.field public static final crm_initial_circle_text:I = 0x7f0a0463

.field public static final crm_invoice_list:I = 0x7f0a0464

.field public static final crm_invoice_progress_bar:I = 0x7f0a0465

.field public static final crm_invoices_summary_section:I = 0x7f0a0466

.field public static final crm_last_name_field:I = 0x7f0a0467

.field public static final crm_line_data_row_info:I = 0x7f0a0468

.field public static final crm_line_data_row_title:I = 0x7f0a0469

.field public static final crm_list_header_row:I = 0x7f0a046a

.field public static final crm_list_header_row_text:I = 0x7f0a046b

.field public static final crm_loyalty_phone_check:I = 0x7f0a046c

.field public static final crm_loyalty_phone_number:I = 0x7f0a046d

.field public static final crm_manage_coupons:I = 0x7f0a046e

.field public static final crm_master_contact_list:I = 0x7f0a046f

.field public static final crm_master_create_customer_button:I = 0x7f0a0470

.field public static final crm_master_customer_lookup:I = 0x7f0a0471

.field public static final crm_master_drop_down:I = 0x7f0a0472

.field public static final crm_master_empty_directory_message_phone:I = 0x7f0a0473

.field public static final crm_master_menu_add_to_manual_group:I = 0x7f0a0474

.field public static final crm_master_menu_create_customer:I = 0x7f0a0475

.field public static final crm_master_menu_default:I = 0x7f0a0476

.field public static final crm_master_menu_delete_customers:I = 0x7f0a0477

.field public static final crm_master_menu_edit_group:I = 0x7f0a0478

.field public static final crm_master_menu_filter_list:I = 0x7f0a0479

.field public static final crm_master_menu_merge_customers:I = 0x7f0a047a

.field public static final crm_master_menu_remove_from_manual_group:I = 0x7f0a047b

.field public static final crm_master_menu_resolve_duplicates:I = 0x7f0a047c

.field public static final crm_master_menu_view_feedback:I = 0x7f0a047d

.field public static final crm_master_menu_view_groups:I = 0x7f0a047e

.field public static final crm_master_multiselect_button_blue:I = 0x7f0a047f

.field public static final crm_master_multiselect_button_red:I = 0x7f0a0480

.field public static final crm_master_progress:I = 0x7f0a0481

.field public static final crm_master_warning_message:I = 0x7f0a0482

.field public static final crm_merge_proposal_included_check:I = 0x7f0a0483

.field public static final crm_merge_proposal_list:I = 0x7f0a0484

.field public static final crm_merge_proposal_new_contact_title:I = 0x7f0a0485

.field public static final crm_merge_proposal_title_row:I = 0x7f0a0486

.field public static final crm_message:I = 0x7f0a0487

.field public static final crm_message_creator_timestamp:I = 0x7f0a0488

.field public static final crm_message_divider:I = 0x7f0a0489

.field public static final crm_message_edit:I = 0x7f0a048a

.field public static final crm_message_input:I = 0x7f0a048b

.field public static final crm_message_left:I = 0x7f0a048c

.field public static final crm_message_right:I = 0x7f0a048d

.field public static final crm_messages_list_empty_list:I = 0x7f0a048e

.field public static final crm_messages_list_view:I = 0x7f0a048f

.field public static final crm_minimum_visits:I = 0x7f0a0490

.field public static final crm_minimum_visits_header:I = 0x7f0a0491

.field public static final crm_multi_option_filter_content:I = 0x7f0a0492

.field public static final crm_multi_option_filter_content_stub:I = 0x7f0a0493

.field public static final crm_multiselect_actionbar:I = 0x7f0a0494

.field public static final crm_multiselect_cancel:I = 0x7f0a0495

.field public static final crm_multiselect_dropdown:I = 0x7f0a0496

.field public static final crm_multiselect_dropdown_container:I = 0x7f0a0497

.field public static final crm_multiselect_dropdown_view:I = 0x7f0a0498

.field public static final crm_multiselect_menu_deselect_all:I = 0x7f0a0499

.field public static final crm_multiselect_menu_select_all:I = 0x7f0a049a

.field public static final crm_multiselect_title:I = 0x7f0a049b

.field public static final crm_name_first_input_box:I = 0x7f0a049c

.field public static final crm_name_first_input_scrubber:I = 0x7f0a049d

.field public static final crm_name_second_input_box:I = 0x7f0a049e

.field public static final crm_name_second_input_scrubber:I = 0x7f0a049f

.field public static final crm_no_customer_selected_row1:I = 0x7f0a04a0

.field public static final crm_no_customer_selected_row2:I = 0x7f0a04a1

.field public static final crm_no_customer_selected_screen:I = 0x7f0a04a2

.field public static final crm_note_field:I = 0x7f0a04a3

.field public static final crm_note_message_left:I = 0x7f0a04a4

.field public static final crm_note_message_right:I = 0x7f0a04a5

.field public static final crm_note_row_contents:I = 0x7f0a04a6

.field public static final crm_note_row_creator_timestamp:I = 0x7f0a04a7

.field public static final crm_note_row_note:I = 0x7f0a04a8

.field public static final crm_note_row_reminder:I = 0x7f0a04a9

.field public static final crm_notes_all_notes:I = 0x7f0a04aa

.field public static final crm_notes_rows:I = 0x7f0a04ab

.field public static final crm_option_list:I = 0x7f0a04ac

.field public static final crm_past_appointments_section:I = 0x7f0a04ad

.field public static final crm_personal_information_section:I = 0x7f0a04ae

.field public static final crm_phone_field:I = 0x7f0a04af

.field public static final crm_points_balance_title:I = 0x7f0a04b0

.field public static final crm_profile_action_add_card_on_file:I = 0x7f0a04b1

.field public static final crm_profile_action_add_note:I = 0x7f0a04b2

.field public static final crm_profile_action_delete_customer:I = 0x7f0a04b3

.field public static final crm_profile_action_edit:I = 0x7f0a04b4

.field public static final crm_profile_action_exposed_action:I = 0x7f0a04b5

.field public static final crm_profile_action_merge_customer:I = 0x7f0a04b6

.field public static final crm_profile_action_new_sale:I = 0x7f0a04b7

.field public static final crm_profile_action_send_message:I = 0x7f0a04b8

.field public static final crm_profile_action_upload_file:I = 0x7f0a04b9

.field public static final crm_profile_attachments_image_preview_image:I = 0x7f0a04ba

.field public static final crm_profile_attachments_image_preview_progress_bar:I = 0x7f0a04bb

.field public static final crm_profile_attachments_list:I = 0x7f0a04bc

.field public static final crm_profile_attachments_progress_bar:I = 0x7f0a04bd

.field public static final crm_profile_attachments_upload_progress_bar:I = 0x7f0a04be

.field public static final crm_profile_attachments_upload_subtitle:I = 0x7f0a04bf

.field public static final crm_profile_attachments_upload_title:I = 0x7f0a04c0

.field public static final crm_profile_container:I = 0x7f0a04c1

.field public static final crm_profile_footer_add_or_remove_button:I = 0x7f0a04c2

.field public static final crm_profile_footer_button_container:I = 0x7f0a04c3

.field public static final crm_profile_footer_message_button:I = 0x7f0a04c4

.field public static final crm_profile_section_header_action:I = 0x7f0a04c5

.field public static final crm_profile_section_header_divider:I = 0x7f0a04c6

.field public static final crm_profile_section_header_dropdown:I = 0x7f0a04c7

.field public static final crm_profile_section_header_right_container:I = 0x7f0a04c8

.field public static final crm_profile_section_header_title:I = 0x7f0a04c9

.field public static final crm_progress:I = 0x7f0a04ca

.field public static final crm_progress_bar:I = 0x7f0a04cb

.field public static final crm_progress_text:I = 0x7f0a04cc

.field public static final crm_recent_contacts:I = 0x7f0a04cd

.field public static final crm_recent_progress_bar:I = 0x7f0a04ce

.field public static final crm_recent_title:I = 0x7f0a04cf

.field public static final crm_recycler_view:I = 0x7f0a04d0

.field public static final crm_redeem_reward_button:I = 0x7f0a04d1

.field public static final crm_redeem_reward_layout:I = 0x7f0a04d2

.field public static final crm_redeem_reward_subtitle:I = 0x7f0a04d3

.field public static final crm_redeem_reward_title:I = 0x7f0a04d4

.field public static final crm_reference_id_field:I = 0x7f0a04d5

.field public static final crm_reminder_one_month:I = 0x7f0a04d6

.field public static final crm_reminder_one_week:I = 0x7f0a04d7

.field public static final crm_reminder_tomorrow:I = 0x7f0a04d8

.field public static final crm_remove_add_coupon_row:I = 0x7f0a04d9

.field public static final crm_remove_all_filters:I = 0x7f0a04da

.field public static final crm_remove_filter_button:I = 0x7f0a04db

.field public static final crm_remove_points_button:I = 0x7f0a04dc

.field public static final crm_remove_reminder:I = 0x7f0a04dd

.field public static final crm_review_for_merging_container:I = 0x7f0a04de

.field public static final crm_reward_code_description:I = 0x7f0a04df

.field public static final crm_reward_code_digits:I = 0x7f0a04e0

.field public static final crm_reward_not_found_layout:I = 0x7f0a04e1

.field public static final crm_save_card_name_number:I = 0x7f0a04e2

.field public static final crm_save_card_remove_button:I = 0x7f0a04e3

.field public static final crm_save_filters:I = 0x7f0a04e4

.field public static final crm_savecard_new_customer:I = 0x7f0a04e5

.field public static final crm_scroll_view:I = 0x7f0a04e6

.field public static final crm_search_box:I = 0x7f0a04e7

.field public static final crm_search_contacts:I = 0x7f0a04e8

.field public static final crm_search_create_new_button:I = 0x7f0a04e9

.field public static final crm_search_message:I = 0x7f0a04ea

.field public static final crm_search_progress_bar:I = 0x7f0a04eb

.field public static final crm_search_reward_again:I = 0x7f0a04ec

.field public static final crm_section_card_on_file:I = 0x7f0a04ed

.field public static final crm_section_files:I = 0x7f0a04ee

.field public static final crm_section_frequent_items:I = 0x7f0a04ef

.field public static final crm_section_header:I = 0x7f0a04f0

.field public static final crm_section_loyalty:I = 0x7f0a04f1

.field public static final crm_section_notes:I = 0x7f0a04f2

.field public static final crm_section_rewards:I = 0x7f0a04f3

.field public static final crm_section_rows:I = 0x7f0a04f4

.field public static final crm_see_all_loyalty_tiers:I = 0x7f0a04f5

.field public static final crm_see_all_loyalty_tiers_list:I = 0x7f0a04f6

.field public static final crm_select_loyalty_hint:I = 0x7f0a04f7

.field public static final crm_send_message_button:I = 0x7f0a04f8

.field public static final crm_send_message_warning:I = 0x7f0a04f9

.field public static final crm_simple_section_cta_button:I = 0x7f0a04fa

.field public static final crm_simple_section_header:I = 0x7f0a04fb

.field public static final crm_simple_section_rows:I = 0x7f0a04fc

.field public static final crm_single_option_filter_content:I = 0x7f0a04fd

.field public static final crm_single_option_filter_content_stub:I = 0x7f0a04fe

.field public static final crm_single_text_filter_content:I = 0x7f0a04ff

.field public static final crm_single_text_filter_content_stub:I = 0x7f0a0500

.field public static final crm_summary_title:I = 0x7f0a0501

.field public static final crm_summary_value:I = 0x7f0a0502

.field public static final crm_text_field:I = 0x7f0a0503

.field public static final crm_time_period_header:I = 0x7f0a0504

.field public static final crm_time_period_list:I = 0x7f0a0505

.field public static final crm_title:I = 0x7f0a0506

.field public static final crm_upcoming_appointments_section:I = 0x7f0a0507

.field public static final crm_update_customer_root:I = 0x7f0a0508

.field public static final crm_v2_all_customers_list:I = 0x7f0a0509

.field public static final crm_v2_view_group:I = 0x7f0a050a

.field public static final crm_v2_view_groups_list:I = 0x7f0a050b

.field public static final crm_view_all_coupons_and_rewards:I = 0x7f0a050c

.field public static final crm_view_attribute_title:I = 0x7f0a050d

.field public static final crm_view_attribute_value:I = 0x7f0a050e

.field public static final crm_view_customer:I = 0x7f0a050f

.field public static final crm_view_customer_drop_down:I = 0x7f0a0510

.field public static final crm_view_note_body:I = 0x7f0a0511

.field public static final crm_view_note_creator_timestamp:I = 0x7f0a0512

.field public static final crm_view_note_reminder:I = 0x7f0a0513

.field public static final crm_visit_frequency_filter_content:I = 0x7f0a0514

.field public static final crm_visit_frequency_filter_content_stub:I = 0x7f0a0515

.field public static final csat_dislike_msg:I = 0x7f0a0516

.field public static final csat_like_msg:I = 0x7f0a0517

.field public static final csat_message:I = 0x7f0a0518

.field public static final csat_msg_container:I = 0x7f0a0519

.field public static final csat_view_layout:I = 0x7f0a051a

.field public static final current_balance_title:I = 0x7f0a051b

.field public static final current_drawer_progress_bar:I = 0x7f0a051c

.field public static final current_drawer_start_drawer_button:I = 0x7f0a051d

.field public static final current_drawer_starting_cash:I = 0x7f0a051e

.field public static final current_label:I = 0x7f0a051f

.field public static final current_plan_value:I = 0x7f0a0520

.field public static final current_plan_value_image:I = 0x7f0a0521

.field public static final current_plan_value_text:I = 0x7f0a0522

.field public static final current_value:I = 0x7f0a0523

.field public static final custom:I = 0x7f0a0524

.field public static final customPanel:I = 0x7f0a0525

.field public static final custom_amount_link_radio_btn:I = 0x7f0a0526

.field public static final custom_tax_enabled_row:I = 0x7f0a0527

.field public static final custom_ticket_button:I = 0x7f0a0528

.field public static final custom_ticket_name_hint:I = 0x7f0a0529

.field public static final custom_ticket_names:I = 0x7f0a052a

.field public static final custom_tip:I = 0x7f0a052b

.field public static final custom_tip_amount:I = 0x7f0a052c

.field public static final custom_tip_amount_submit:I = 0x7f0a052d

.field public static final custom_tip_button:I = 0x7f0a052e

.field public static final custom_tip_row:I = 0x7f0a052f

.field public static final custom_tip_row_entry:I = 0x7f0a0530

.field public static final customer_add_card_button:I = 0x7f0a0531

.field public static final customer_all_appointments_view:I = 0x7f0a0532

.field public static final customer_button:I = 0x7f0a0533

.field public static final customer_cards_on_file:I = 0x7f0a0534

.field public static final customer_checkout_divider:I = 0x7f0a0535

.field public static final customer_checkout_settings_group:I = 0x7f0a0536

.field public static final customer_image:I = 0x7f0a0537

.field public static final customer_info_title:I = 0x7f0a0538

.field public static final customer_invoice_view:I = 0x7f0a0539

.field public static final customer_row:I = 0x7f0a053a

.field public static final customer_row_container:I = 0x7f0a053b

.field public static final customer_unit_row:I = 0x7f0a053c

.field public static final customers_applet_customer_messages_list_view:I = 0x7f0a053d

.field public static final customers_applet_customer_messages_progress_bar:I = 0x7f0a053e

.field public static final customize_report_date_picker:I = 0x7f0a053f

.field public static final cut:I = 0x7f0a0540

.field public static final cvv_editor:I = 0x7f0a0541

.field public static final cvv_input:I = 0x7f0a0542

.field public static final dark:I = 0x7f0a0543

.field public static final dashboard_link:I = 0x7f0a0544

.field public static final data_recycler_view:I = 0x7f0a0545

.field public static final date:I = 0x7f0a0546

.field public static final date_options:I = 0x7f0a0547

.field public static final date_picker:I = 0x7f0a0548

.field public static final date_picker_actions:I = 0x7f0a0549

.field public static final date_picker_month_pager:I = 0x7f0a054a

.field public static final date_picker_month_title:I = 0x7f0a054b

.field public static final date_picker_next_month:I = 0x7f0a054c

.field public static final date_picker_prev_month:I = 0x7f0a054d

.field public static final date_range_checkable_group:I = 0x7f0a054e

.field public static final date_range_label:I = 0x7f0a054f

.field public static final date_range_row_last_month:I = 0x7f0a0550

.field public static final date_range_row_last_year:I = 0x7f0a0551

.field public static final date_range_row_this_month:I = 0x7f0a0552

.field public static final date_range_row_this_year:I = 0x7f0a0553

.field public static final date_range_section:I = 0x7f0a0554

.field public static final date_time:I = 0x7f0a0555

.field public static final day_names_header_row:I = 0x7f0a0556

.field public static final day_of_week_picker:I = 0x7f0a0557

.field public static final day_picker:I = 0x7f0a0558

.field public static final day_view_adapter_class:I = 0x7f0a0559

.field public static final deactivate_button:I = 0x7f0a055a

.field public static final deadline_row:I = 0x7f0a055b

.field public static final debit_card_input_editor:I = 0x7f0a055c

.field public static final decor_content_parent:I = 0x7f0a055d

.field public static final default_activity_button:I = 0x7f0a055e

.field public static final default_starting_cash:I = 0x7f0a055f

.field public static final delete:I = 0x7f0a0560

.field public static final delete_button:I = 0x7f0a0561

.field public static final delete_item:I = 0x7f0a0562

.field public static final delete_message:I = 0x7f0a0563

.field public static final delete_tax_button:I = 0x7f0a0564

.field public static final delete_ticket_group_button:I = 0x7f0a0565

.field public static final delivery_method_container:I = 0x7f0a0566

.field public static final delivery_method_options:I = 0x7f0a0567

.field public static final deposit_account:I = 0x7f0a0568

.field public static final deposit_hint:I = 0x7f0a0569

.field public static final deposit_methods:I = 0x7f0a056a

.field public static final deposit_options_hint:I = 0x7f0a056b

.field public static final deposit_schedule:I = 0x7f0a056c

.field public static final deposit_schedule_automatic:I = 0x7f0a056d

.field public static final deposit_schedule_checkable_group:I = 0x7f0a056e

.field public static final deposit_schedule_error_message:I = 0x7f0a056f

.field public static final deposit_schedule_hint:I = 0x7f0a0570

.field public static final deposit_schedule_manual:I = 0x7f0a0571

.field public static final deposit_schedule_rows:I = 0x7f0a0572

.field public static final deposit_schedule_title:I = 0x7f0a0573

.field public static final deposit_speed_checkable_group:I = 0x7f0a0574

.field public static final deposit_speed_custom:I = 0x7f0a0575

.field public static final deposit_speed_hint:I = 0x7f0a0576

.field public static final deposit_speed_next_business_day:I = 0x7f0a0577

.field public static final deposit_speed_one_to_two_business_days:I = 0x7f0a0578

.field public static final deposit_speed_same_day:I = 0x7f0a0579

.field public static final deposit_speed_title:I = 0x7f0a057a

.field public static final deposit_title:I = 0x7f0a057b

.field public static final deposits_report_detail_bank_or_card:I = 0x7f0a057c

.field public static final deposits_report_detail_card_payments_title:I = 0x7f0a057d

.field public static final deposits_report_detail_collected:I = 0x7f0a057e

.field public static final deposits_report_detail_date_time:I = 0x7f0a057f

.field public static final deposits_report_detail_deposit_number:I = 0x7f0a0580

.field public static final deposits_report_detail_error_section:I = 0x7f0a0581

.field public static final deposits_report_detail_net_total:I = 0x7f0a0582

.field public static final deposits_report_detail_root_view:I = 0x7f0a0583

.field public static final deposits_report_detail_spinner:I = 0x7f0a0584

.field public static final deposits_report_detail_toggles:I = 0x7f0a0585

.field public static final deposits_report_detail_view:I = 0x7f0a0586

.field public static final deposits_report_title:I = 0x7f0a0587

.field public static final deposits_report_view:I = 0x7f0a0588

.field public static final description:I = 0x7f0a0589

.field public static final description_row:I = 0x7f0a058a

.field public static final design_bottom_sheet:I = 0x7f0a058b

.field public static final design_menu_item_action_area:I = 0x7f0a058c

.field public static final design_menu_item_action_area_stub:I = 0x7f0a058d

.field public static final design_menu_item_text:I = 0x7f0a058e

.field public static final design_navigation_view:I = 0x7f0a058f

.field public static final desired_deposit:I = 0x7f0a0590

.field public static final destructive:I = 0x7f0a0591

.field public static final detail:I = 0x7f0a0592

.field public static final detail_confirmation_button:I = 0x7f0a0593

.field public static final detail_confirmation_glyph:I = 0x7f0a0594

.field public static final detail_confirmation_helper_text:I = 0x7f0a0595

.field public static final detail_confirmation_message:I = 0x7f0a0596

.field public static final detail_confirmation_title:I = 0x7f0a0597

.field public static final detail_invoice_file_attachments_container:I = 0x7f0a0598

.field public static final detail_list_view:I = 0x7f0a0599

.field public static final detail_list_wrapper:I = 0x7f0a059a

.field public static final detail_progress:I = 0x7f0a059b

.field public static final detail_search:I = 0x7f0a059c

.field public static final detail_searchable_list:I = 0x7f0a059d

.field public static final detail_searchable_list_convert_item_button:I = 0x7f0a059e

.field public static final detail_searchable_list_create_button:I = 0x7f0a059f

.field public static final detail_searchable_list_create_button_container:I = 0x7f0a05a0

.field public static final detail_searchable_list_create_from_search_button:I = 0x7f0a05a1

.field public static final detail_searchable_list_empty_view:I = 0x7f0a05a2

.field public static final detail_searchable_list_empty_view_subtitle:I = 0x7f0a05a3

.field public static final detail_searchable_list_empty_view_title:I = 0x7f0a05a4

.field public static final detail_searchable_list_loading_progress:I = 0x7f0a05a5

.field public static final detail_searchable_list_no_results_group:I = 0x7f0a05a6

.field public static final detail_searchable_list_recycler_view:I = 0x7f0a05a7

.field public static final detail_searchable_list_search_bar:I = 0x7f0a05a8

.field public static final detail_searchable_list_unit_number_limit_help_text_under_create_button:I = 0x7f0a05a9

.field public static final detail_searchable_list_unit_number_limit_help_text_under_create_from_search_button:I = 0x7f0a05aa

.field public static final detail_searchable_list_view_add_button:I = 0x7f0a05ab

.field public static final details:I = 0x7f0a05ac

.field public static final details_fragment_container:I = 0x7f0a05ad

.field public static final details_label:I = 0x7f0a05ae

.field public static final device_code_field:I = 0x7f0a05af

.field public static final device_code_link:I = 0x7f0a05b0

.field public static final device_contactless_help:I = 0x7f0a05b1

.field public static final device_name_field:I = 0x7f0a05b2

.field public static final device_name_layout:I = 0x7f0a05b3

.field public static final dialog_button:I = 0x7f0a05b4

.field public static final dialog_icon:I = 0x7f0a05b5

.field public static final difference:I = 0x7f0a05b6

.field public static final digit:I = 0x7f0a05b7

.field public static final digit_5:I = 0x7f0a05b8

.field public static final dimensions:I = 0x7f0a05b9

.field public static final dining_option:I = 0x7f0a05ba

.field public static final dining_option_app_bar:I = 0x7f0a05bb

.field public static final dipped_card_spinner_glyph:I = 0x7f0a05bc

.field public static final dipped_card_spinner_title:I = 0x7f0a05bd

.field public static final direct:I = 0x7f0a05be

.field public static final direct_debit_guarantee:I = 0x7f0a05bf

.field public static final direct_debit_logo:I = 0x7f0a05c0

.field public static final disableHome:I = 0x7f0a05c1

.field public static final disabled_text_box:I = 0x7f0a05c2

.field public static final disclosure:I = 0x7f0a05c3

.field public static final discount_amount:I = 0x7f0a05c4

.field public static final discount_bundle_list:I = 0x7f0a05c5

.field public static final discount_bundle_title:I = 0x7f0a05c6

.field public static final discount_delete:I = 0x7f0a05c7

.field public static final discount_icon:I = 0x7f0a05c8

.field public static final discount_modify_tax_basis:I = 0x7f0a05c9

.field public static final discount_name:I = 0x7f0a05ca

.field public static final discount_name_display:I = 0x7f0a05cb

.field public static final discount_options_section:I = 0x7f0a05cc

.field public static final discount_options_title:I = 0x7f0a05cd

.field public static final discount_percentage:I = 0x7f0a05ce

.field public static final discount_switch:I = 0x7f0a05cf

.field public static final discount_switch_amount:I = 0x7f0a05d0

.field public static final discount_switch_percentage:I = 0x7f0a05d1

.field public static final discount_tag_imageview:I = 0x7f0a05d2

.field public static final discount_text:I = 0x7f0a05d3

.field public static final discount_value_container:I = 0x7f0a05d4

.field public static final discounted_items_section:I = 0x7f0a05d5

.field public static final dismiss_button:I = 0x7f0a05d6

.field public static final display:I = 0x7f0a05d7

.field public static final display2:I = 0x7f0a05d8

.field public static final display_amount:I = 0x7f0a05d9

.field public static final display_price_container:I = 0x7f0a05da

.field public static final display_price_row:I = 0x7f0a05db

.field public static final dispute_row:I = 0x7f0a05dc

.field public static final disputed_amount_row:I = 0x7f0a05dd

.field public static final disputed_row:I = 0x7f0a05de

.field public static final disputes_error:I = 0x7f0a05df

.field public static final disputes_error_container:I = 0x7f0a05e0

.field public static final disputes_list:I = 0x7f0a05e1

.field public static final disputes_summary_section:I = 0x7f0a05e2

.field public static final divider:I = 0x7f0a05e3

.field public static final donation_link_radio_btn:I = 0x7f0a05e4

.field public static final done_button:I = 0x7f0a05e5

.field public static final done_editing:I = 0x7f0a05e6

.field public static final download_attachment_progressbar:I = 0x7f0a05e7

.field public static final download_button:I = 0x7f0a05e8

.field public static final download_item:I = 0x7f0a05e9

.field public static final download_progressbar_container:I = 0x7f0a05ea

.field public static final draft_amount:I = 0x7f0a05eb

.field public static final draft_container:I = 0x7f0a05ec

.field public static final draft_header:I = 0x7f0a05ed

.field public static final dragHandle:I = 0x7f0a05ee

.field public static final drag_handle:I = 0x7f0a05ef

.field public static final draggable_detail_list_view:I = 0x7f0a05f0

.field public static final draggable_item_variation_row_drag_handle:I = 0x7f0a05f1

.field public static final draggable_item_variation_row_price:I = 0x7f0a05f2

.field public static final draggable_item_variation_row_price_sku:I = 0x7f0a05f3

.field public static final draggable_item_variation_row_sku:I = 0x7f0a05f4

.field public static final draggable_item_variation_row_stock_count:I = 0x7f0a05f5

.field public static final draggable_service_variation_row_duration:I = 0x7f0a05f6

.field public static final draggable_service_variation_row_price:I = 0x7f0a05f7

.field public static final draggable_variation_row_details_container:I = 0x7f0a05f8

.field public static final draggable_variation_row_name:I = 0x7f0a05f9

.field public static final draggable_variation_row_top_divider:I = 0x7f0a05fa

.field public static final drawer_button:I = 0x7f0a05fb

.field public static final drawer_date:I = 0x7f0a05fc

.field public static final drawer_description:I = 0x7f0a05fd

.field public static final drawer_details_container:I = 0x7f0a05fe

.field public static final drawer_history_list:I = 0x7f0a05ff

.field public static final drawer_history_list_container:I = 0x7f0a0600

.field public static final drawer_history_null_container:I = 0x7f0a0601

.field public static final drawer_history_null_state_subtitle:I = 0x7f0a0602

.field public static final drawer_history_progress:I = 0x7f0a0603

.field public static final drawer_view:I = 0x7f0a0604

.field public static final drawing_mode:I = 0x7f0a0605

.field public static final dropdown_menu:I = 0x7f0a0606

.field public static final due_date:I = 0x7f0a0607

.field public static final due_date_row:I = 0x7f0a0608

.field public static final duplicate_sku_result_variation_list:I = 0x7f0a0609

.field public static final earned_points_container:I = 0x7f0a060a

.field public static final earned_rewards_container:I = 0x7f0a060b

.field public static final edit:I = 0x7f0a060c

.field public static final edit_button:I = 0x7f0a060d

.field public static final edit_catalog_object_label_abbreviation:I = 0x7f0a060e

.field public static final edit_catalog_object_label_animator:I = 0x7f0a060f

.field public static final edit_catalog_object_label_image:I = 0x7f0a0610

.field public static final edit_catalog_object_label_name:I = 0x7f0a0611

.field public static final edit_catalog_object_label_progress:I = 0x7f0a0612

.field public static final edit_category_label_color_grid:I = 0x7f0a0613

.field public static final edit_category_label_edit_label_color_header:I = 0x7f0a0614

.field public static final edit_category_name:I = 0x7f0a0615

.field public static final edit_category_static_top_content_tile:I = 0x7f0a0616

.field public static final edit_category_title:I = 0x7f0a0617

.field public static final edit_category_top_image_tile:I = 0x7f0a0618

.field public static final edit_category_top_text_tile:I = 0x7f0a0619

.field public static final edit_category_view:I = 0x7f0a061a

.field public static final edit_discount_container:I = 0x7f0a061b

.field public static final edit_discount_view:I = 0x7f0a061c

.field public static final edit_frame:I = 0x7f0a061d

.field public static final edit_invoice_1_container:I = 0x7f0a061e

.field public static final edit_invoice_2_container:I = 0x7f0a061f

.field public static final edit_invoice_bottom_button:I = 0x7f0a0620

.field public static final edit_invoice_file_attachment_header:I = 0x7f0a0621

.field public static final edit_invoice_file_attachments_container:I = 0x7f0a0622

.field public static final edit_invoice_frequency:I = 0x7f0a0623

.field public static final edit_invoice_progress_bar:I = 0x7f0a0624

.field public static final edit_invoice_scroll_view:I = 0x7f0a0625

.field public static final edit_item_description:I = 0x7f0a0626

.field public static final edit_item_details_item_name:I = 0x7f0a0627

.field public static final edit_item_edit_details_section:I = 0x7f0a0628

.field public static final edit_item_item_option_and_values_row_phone_option_and_values_row:I = 0x7f0a0629

.field public static final edit_item_item_option_and_values_row_tablet_option_row:I = 0x7f0a062a

.field public static final edit_item_item_option_and_values_row_tablet_values_row:I = 0x7f0a062b

.field public static final edit_item_label_top_view:I = 0x7f0a062c

.field public static final edit_item_label_view:I = 0x7f0a062d

.field public static final edit_item_main_view_default_variation_container:I = 0x7f0a062e

.field public static final edit_item_main_view_errors_bar:I = 0x7f0a062f

.field public static final edit_item_main_view_item_name_and_category:I = 0x7f0a0630

.field public static final edit_item_main_view_manage_option_button:I = 0x7f0a0631

.field public static final edit_item_main_view_manage_option_help_text:I = 0x7f0a0632

.field public static final edit_item_main_view_option_section:I = 0x7f0a0633

.field public static final edit_item_main_view_options_container:I = 0x7f0a0634

.field public static final edit_item_main_view_price_input_field:I = 0x7f0a0635

.field public static final edit_item_main_view_price_sku_container:I = 0x7f0a0636

.field public static final edit_item_main_view_recycler_view:I = 0x7f0a0637

.field public static final edit_item_main_view_sku_input_field:I = 0x7f0a0638

.field public static final edit_item_main_view_stock_count_row:I = 0x7f0a0639

.field public static final edit_item_main_view_unit_type:I = 0x7f0a063a

.field public static final edit_item_main_view_unit_type_help_text:I = 0x7f0a063b

.field public static final edit_item_main_view_variation_overlay:I = 0x7f0a063c

.field public static final edit_item_price_help_text:I = 0x7f0a063d

.field public static final edit_item_read_only_details_section:I = 0x7f0a063e

.field public static final edit_item_save_button:I = 0x7f0a063f

.field public static final edit_item_unit_type_selection:I = 0x7f0a0640

.field public static final edit_item_variation_errors_bar:I = 0x7f0a0641

.field public static final edit_item_variation_help_text:I = 0x7f0a0642

.field public static final edit_item_variation_name_input_field:I = 0x7f0a0643

.field public static final edit_item_variation_name_readonly_field:I = 0x7f0a0644

.field public static final edit_item_variation_price_input_field:I = 0x7f0a0645

.field public static final edit_item_variation_remove_variation_button:I = 0x7f0a0646

.field public static final edit_item_variation_sku_input_field:I = 0x7f0a0647

.field public static final edit_item_variation_stock_count_row:I = 0x7f0a0648

.field public static final edit_item_variation_unit_type:I = 0x7f0a0649

.field public static final edit_item_variation_unit_type_help_text:I = 0x7f0a064a

.field public static final edit_item_variation_view:I = 0x7f0a064b

.field public static final edit_label_color_header:I = 0x7f0a064c

.field public static final edit_label_image_tile:I = 0x7f0a064d

.field public static final edit_label_text_tile:I = 0x7f0a064e

.field public static final edit_notes:I = 0x7f0a064f

.field public static final edit_option_recycler:I = 0x7f0a0650

.field public static final edit_option_value_color_field:I = 0x7f0a0651

.field public static final edit_option_value_color_grid:I = 0x7f0a0652

.field public static final edit_option_value_delete_button:I = 0x7f0a0653

.field public static final edit_option_value_name_field:I = 0x7f0a0654

.field public static final edit_option_value_name_label:I = 0x7f0a0655

.field public static final edit_payment_request_due_row:I = 0x7f0a0656

.field public static final edit_payment_request_header_subtitle:I = 0x7f0a0657

.field public static final edit_payment_request_header_title:I = 0x7f0a0658

.field public static final edit_payment_request_reminder_row:I = 0x7f0a0659

.field public static final edit_photo_animator:I = 0x7f0a065a

.field public static final edit_photo_error_message:I = 0x7f0a065b

.field public static final edit_photo_hint:I = 0x7f0a065c

.field public static final edit_photo_image:I = 0x7f0a065d

.field public static final edit_photo_progress:I = 0x7f0a065e

.field public static final edit_quantity_row:I = 0x7f0a065f

.field public static final edit_query:I = 0x7f0a0660

.field public static final edit_service_after_appointment_row:I = 0x7f0a0661

.field public static final edit_service_duration_extra_time_toggle:I = 0x7f0a0662

.field public static final edit_service_duration_row:I = 0x7f0a0663

.field public static final edit_service_final_duration:I = 0x7f0a0664

.field public static final edit_service_gap_duration:I = 0x7f0a0665

.field public static final edit_service_gap_time_toggle:I = 0x7f0a0666

.field public static final edit_service_initial_duration:I = 0x7f0a0667

.field public static final edit_service_main_view_default_variation_container:I = 0x7f0a0668

.field public static final edit_service_price_type_helper_text:I = 0x7f0a0669

.field public static final edit_service_variation_after_appointment_row:I = 0x7f0a066a

.field public static final edit_service_variation_assigned_employees:I = 0x7f0a066b

.field public static final edit_service_variation_bookable_by_customers_online:I = 0x7f0a066c

.field public static final edit_service_variation_cancellation_fee_row:I = 0x7f0a066d

.field public static final edit_service_variation_display_price_container:I = 0x7f0a066e

.field public static final edit_service_variation_display_price_row:I = 0x7f0a066f

.field public static final edit_service_variation_duration_extra_time_toggle:I = 0x7f0a0670

.field public static final edit_service_variation_duration_row:I = 0x7f0a0671

.field public static final edit_service_variation_extra_time_helper_text:I = 0x7f0a0672

.field public static final edit_service_variation_final_duration:I = 0x7f0a0673

.field public static final edit_service_variation_gap_duration:I = 0x7f0a0674

.field public static final edit_service_variation_gap_time_toggle:I = 0x7f0a0675

.field public static final edit_service_variation_initial_duration:I = 0x7f0a0676

.field public static final edit_service_variation_name_input_field:I = 0x7f0a0677

.field public static final edit_service_variation_price_input_field:I = 0x7f0a0678

.field public static final edit_service_variation_remove_variation_button:I = 0x7f0a0679

.field public static final edit_service_variation_unit_type:I = 0x7f0a067a

.field public static final edit_service_variaton_price_type_row:I = 0x7f0a067b

.field public static final edit_split_ticket_view:I = 0x7f0a067c

.field public static final edit_text_password_contents:I = 0x7f0a067d

.field public static final edit_ticket_recycler_view:I = 0x7f0a067e

.field public static final edit_unit_abbreviation:I = 0x7f0a067f

.field public static final edit_unit_abbreviation_label:I = 0x7f0a0680

.field public static final edit_unit_custom_unit_fields:I = 0x7f0a0681

.field public static final edit_unit_delete:I = 0x7f0a0682

.field public static final edit_unit_details:I = 0x7f0a0683

.field public static final edit_unit_name:I = 0x7f0a0684

.field public static final edit_unit_precision_0:I = 0x7f0a0685

.field public static final edit_unit_precision_1:I = 0x7f0a0686

.field public static final edit_unit_precision_2:I = 0x7f0a0687

.field public static final edit_unit_precision_3:I = 0x7f0a0688

.field public static final edit_unit_precision_4:I = 0x7f0a0689

.field public static final edit_unit_precision_5:I = 0x7f0a068a

.field public static final edit_unit_precision_help_text:I = 0x7f0a068b

.field public static final edit_unit_quantity_row:I = 0x7f0a068c

.field public static final edit_unit_save_spinner:I = 0x7f0a068d

.field public static final edit_unit_scroll_view:I = 0x7f0a068e

.field public static final edit_unit_unit_label:I = 0x7f0a068f

.field public static final editor:I = 0x7f0a0690

.field public static final editor_wrapper:I = 0x7f0a0691

.field public static final egiftcard_actionbarbutton:I = 0x7f0a0692

.field public static final egiftcard_amount_0:I = 0x7f0a0693

.field public static final egiftcard_amount_1:I = 0x7f0a0694

.field public static final egiftcard_amount_2:I = 0x7f0a0695

.field public static final egiftcard_amount_3:I = 0x7f0a0696

.field public static final egiftcard_amount_custom:I = 0x7f0a0697

.field public static final egiftcard_choose_email_edit_text:I = 0x7f0a0698

.field public static final egiftcard_choosedesign_recyclerview:I = 0x7f0a0699

.field public static final egiftcard_custom_amount_edit_text:I = 0x7f0a069a

.field public static final egiftcard_custom_amount_hint:I = 0x7f0a069b

.field public static final egiftcard_design_checkmark:I = 0x7f0a069c

.field public static final egiftcard_design_delete_button:I = 0x7f0a069d

.field public static final egiftcard_design_image:I = 0x7f0a069e

.field public static final egiftcard_design_image_frame:I = 0x7f0a069f

.field public static final egiftcard_design_settings_add:I = 0x7f0a06a0

.field public static final egiftcard_designs_image:I = 0x7f0a06a1

.field public static final egiftcard_designs_image_frame:I = 0x7f0a06a2

.field public static final egiftcard_designs_image_loading:I = 0x7f0a06a3

.field public static final egiftcard_designs_not_valid_image:I = 0x7f0a06a4

.field public static final egiftcard_designs_upload_custom:I = 0x7f0a06a5

.field public static final egiftcard_designs_upload_custom_not_available:I = 0x7f0a06a6

.field public static final egiftcard_done:I = 0x7f0a06a7

.field public static final egiftcard_progress_bar:I = 0x7f0a06a8

.field public static final egiftcard_registered_body:I = 0x7f0a06a9

.field public static final egiftcard_registered_title:I = 0x7f0a06aa

.field public static final egiftcard_return_to_cart_actionbar:I = 0x7f0a06ab

.field public static final egiftcard_settings_design_recyclerview:I = 0x7f0a06ac

.field public static final egiftcard_spinner_glyph:I = 0x7f0a06ad

.field public static final egiftcard_x2_seller_body:I = 0x7f0a06ae

.field public static final ein_form:I = 0x7f0a06af

.field public static final ein_radios:I = 0x7f0a06b0

.field public static final ein_text:I = 0x7f0a06b1

.field public static final email:I = 0x7f0a06b2

.field public static final email_attribute:I = 0x7f0a06b3

.field public static final email_button:I = 0x7f0a06b4

.field public static final email_collection_all_done_container:I = 0x7f0a06b5

.field public static final email_collection_all_done_message:I = 0x7f0a06b6

.field public static final email_collection_customer:I = 0x7f0a06b7

.field public static final email_collection_email:I = 0x7f0a06b8

.field public static final email_collection_enroll_container:I = 0x7f0a06b9

.field public static final email_collection_new_sale:I = 0x7f0a06ba

.field public static final email_collection_no_thanks:I = 0x7f0a06bb

.field public static final email_collection_prompt:I = 0x7f0a06bc

.field public static final email_collection_submit_button:I = 0x7f0a06bd

.field public static final email_disclaimer:I = 0x7f0a06be

.field public static final email_edit:I = 0x7f0a06bf

.field public static final email_field:I = 0x7f0a06c0

.field public static final email_items_selected:I = 0x7f0a06c1

.field public static final email_message:I = 0x7f0a06c2

.field public static final email_option:I = 0x7f0a06c3

.field public static final email_receipt_field:I = 0x7f0a06c4

.field public static final email_recipient:I = 0x7f0a06c5

.field public static final email_recipient_container:I = 0x7f0a06c6

.field public static final email_report_address_view:I = 0x7f0a06c7

.field public static final email_report_animator:I = 0x7f0a06c8

.field public static final email_report_message_view:I = 0x7f0a06c9

.field public static final email_report_view:I = 0x7f0a06ca

.field public static final email_send_button:I = 0x7f0a06cb

.field public static final email_suggestion:I = 0x7f0a06cc

.field public static final email_suggestion_box:I = 0x7f0a06cd

.field public static final email_support_ledger_row:I = 0x7f0a06ce

.field public static final emails_container:I = 0x7f0a06cf

.field public static final emoney_brand_selection_subtitle:I = 0x7f0a06d0

.field public static final emoney_payment_selection_container:I = 0x7f0a06d1

.field public static final employee:I = 0x7f0a06d2

.field public static final employee_jobs_button:I = 0x7f0a06d3

.field public static final employee_jobs_list:I = 0x7f0a06d4

.field public static final employee_jobs_list_modal:I = 0x7f0a06d5

.field public static final employee_jobs_list_modal_message:I = 0x7f0a06d6

.field public static final employee_jobs_list_modal_scrollable_view:I = 0x7f0a06d7

.field public static final employee_jobs_list_modal_title:I = 0x7f0a06d8

.field public static final employee_jobs_list_scroll_view:I = 0x7f0a06d9

.field public static final employee_list_header:I = 0x7f0a06da

.field public static final employee_lock_button:I = 0x7f0a06db

.field public static final employee_lock_button_glyph:I = 0x7f0a06dc

.field public static final employee_lock_button_text:I = 0x7f0a06dd

.field public static final employee_management_enabled_toggle:I = 0x7f0a06de

.field public static final employee_management_enabled_toggle_description:I = 0x7f0a06df

.field public static final employee_management_learn_more:I = 0x7f0a06e0

.field public static final employee_management_mode_toggle_guest:I = 0x7f0a06e1

.field public static final employee_management_timeout_option_toggle_1m:I = 0x7f0a06e2

.field public static final employee_management_timeout_option_toggle_30s:I = 0x7f0a06e3

.field public static final employee_management_timeout_option_toggle_5m:I = 0x7f0a06e4

.field public static final employee_management_timeout_option_toggle_never:I = 0x7f0a06e5

.field public static final employee_management_track_time_toggle:I = 0x7f0a06e6

.field public static final employee_management_track_time_toggle_description:I = 0x7f0a06e7

.field public static final employee_management_transaction_lock_mode_toggle:I = 0x7f0a06e8

.field public static final employee_recycler:I = 0x7f0a06e9

.field public static final employee_selection_animator:I = 0x7f0a06ea

.field public static final employee_selection_container:I = 0x7f0a06eb

.field public static final employee_selection_list_animator:I = 0x7f0a06ec

.field public static final employee_selection_no_search_results:I = 0x7f0a06ed

.field public static final employee_selection_progress_bar:I = 0x7f0a06ee

.field public static final employee_selection_recycler_view:I = 0x7f0a06ef

.field public static final employee_selection_search:I = 0x7f0a06f0

.field public static final employees_group:I = 0x7f0a06f1

.field public static final empty:I = 0x7f0a06f2

.field public static final empty_message:I = 0x7f0a06f3

.field public static final empty_space_10:I = 0x7f0a06f4

.field public static final empty_title:I = 0x7f0a06f5

.field public static final empty_view:I = 0x7f0a06f6

.field public static final emv_application_wrapper:I = 0x7f0a06f7

.field public static final emv_liability_hint:I = 0x7f0a06f8

.field public static final emv_options_container:I = 0x7f0a06f9

.field public static final enable_automatic_reminders:I = 0x7f0a06fa

.field public static final enable_checkout_links_container:I = 0x7f0a06fb

.field public static final enable_checkout_links_loading_screen_container:I = 0x7f0a06fc

.field public static final enable_checkout_links_message_view:I = 0x7f0a06fd

.field public static final enable_checkout_links_result_container:I = 0x7f0a06fe

.field public static final enable_checkout_links_result_message_view:I = 0x7f0a06ff

.field public static final enable_device_settings_learn_more:I = 0x7f0a0700

.field public static final enable_device_settings_location_button:I = 0x7f0a0701

.field public static final enable_device_settings_phone_button:I = 0x7f0a0702

.field public static final enable_device_settings_storage_button:I = 0x7f0a0703

.field public static final enable_store_and_forward:I = 0x7f0a0704

.field public static final enable_swipe_chip_cards:I = 0x7f0a0705

.field public static final enabling_checkout_links_container:I = 0x7f0a0706

.field public static final end:I = 0x7f0a0707

.field public static final end_break_confirmation_notes_button:I = 0x7f0a0708

.field public static final end_drawer_button:I = 0x7f0a0709

.field public static final end_drawer_card_end_button:I = 0x7f0a070a

.field public static final end_drawer_details:I = 0x7f0a070b

.field public static final end_drawer_message:I = 0x7f0a070c

.field public static final end_drawer_message_container:I = 0x7f0a070d

.field public static final end_of_drawer:I = 0x7f0a070e

.field public static final end_padder:I = 0x7f0a070f

.field public static final ends_date_calendar_view:I = 0x7f0a0710

.field public static final ends_never:I = 0x7f0a0711

.field public static final ends_number:I = 0x7f0a0712

.field public static final ends_options:I = 0x7f0a0713

.field public static final ends_set_date:I = 0x7f0a0714

.field public static final enterAlways:I = 0x7f0a0715

.field public static final enterAlwaysCollapsed:I = 0x7f0a0716

.field public static final enter_card_number_or_swipe:I = 0x7f0a0717

.field public static final enter_passcode_to_unlock_header:I = 0x7f0a0718

.field public static final enum_attribute:I = 0x7f0a0719

.field public static final error:I = 0x7f0a071a

.field public static final errorMessageLabel:I = 0x7f0a071b

.field public static final errorReplyTextView:I = 0x7f0a071c

.field public static final error_container:I = 0x7f0a071d

.field public static final error_message:I = 0x7f0a071e

.field public static final error_message_panel:I = 0x7f0a071f

.field public static final error_message_view:I = 0x7f0a0720

.field public static final error_msg:I = 0x7f0a0721

.field public static final error_retry_row_affected:I = 0x7f0a0722

.field public static final error_retry_row_button:I = 0x7f0a0723

.field public static final error_retry_row_message:I = 0x7f0a0724

.field public static final error_retry_row_spinner:I = 0x7f0a0725

.field public static final error_retry_row_text_container:I = 0x7f0a0726

.field public static final error_retry_row_title:I = 0x7f0a0727

.field public static final error_screen_container:I = 0x7f0a0728

.field public static final error_section:I = 0x7f0a0729

.field public static final estimated_completion_date:I = 0x7f0a072a

.field public static final event_detailed_full:I = 0x7f0a072b

.field public static final event_detailed_truncated:I = 0x7f0a072c

.field public static final event_subtitle:I = 0x7f0a072d

.field public static final event_title:I = 0x7f0a072e

.field public static final exclude_row:I = 0x7f0a072f

.field public static final exitUntilCollapsed:I = 0x7f0a0730

.field public static final expand:I = 0x7f0a0731

.field public static final expand_activities_button:I = 0x7f0a0732

.field public static final expanded_menu:I = 0x7f0a0733

.field public static final expected_in_drawer:I = 0x7f0a0734

.field public static final expense_type_business:I = 0x7f0a0735

.field public static final expense_type_personal:I = 0x7f0a0736

.field public static final expiration_and_cvc:I = 0x7f0a0737

.field public static final expiration_editor:I = 0x7f0a0738

.field public static final expiration_input:I = 0x7f0a0739

.field public static final expiring_points_list:I = 0x7f0a073a

.field public static final expiring_points_policy:I = 0x7f0a073b

.field public static final export_report_view:I = 0x7f0a073c

.field public static final extra_time_helper_text:I = 0x7f0a073d

.field public static final extras:I = 0x7f0a073e

.field public static final fade:I = 0x7f0a073f

.field public static final failed_glyph_message:I = 0x7f0a0740

.field public static final failure_view:I = 0x7f0a0741

.field public static final fake_up_button:I = 0x7f0a0742

.field public static final faq_content_view:I = 0x7f0a0743

.field public static final faq_fragment_container:I = 0x7f0a0744

.field public static final favorite_grid_empty_tile:I = 0x7f0a0745

.field public static final favorite_view_position_tag:I = 0x7f0a0746

.field public static final favorite_view_type_tag:I = 0x7f0a0747

.field public static final fee:I = 0x7f0a0748

.field public static final feedback_title:I = 0x7f0a0749

.field public static final fees_help:I = 0x7f0a074a

.field public static final fetch_bank_account_error:I = 0x7f0a074b

.field public static final fetch_bank_account_spinner:I = 0x7f0a074c

.field public static final file_attachment_header:I = 0x7f0a074d

.field public static final file_category_row:I = 0x7f0a074e

.field public static final file_name:I = 0x7f0a074f

.field public static final fill:I = 0x7f0a0750

.field public static final fill_horizontal:I = 0x7f0a0751

.field public static final fill_vertical:I = 0x7f0a0752

.field public static final filled:I = 0x7f0a0753

.field public static final filter_by_employee:I = 0x7f0a0754

.field public static final filter_chip:I = 0x7f0a0755

.field public static final fine_print:I = 0x7f0a0756

.field public static final firmware_update_notification_service:I = 0x7f0a0757

.field public static final first:I = 0x7f0a0758

.field public static final first_bank_account:I = 0x7f0a0759

.field public static final first_button:I = 0x7f0a075a

.field public static final fitToContents:I = 0x7f0a075b

.field public static final fixed:I = 0x7f0a075c

.field public static final fixed_amount_button:I = 0x7f0a075d

.field public static final fixed_amount_text:I = 0x7f0a075e

.field public static final fixed_button:I = 0x7f0a075f

.field public static final fixed_price_override_button:I = 0x7f0a0760

.field public static final fixed_price_override_container:I = 0x7f0a0761

.field public static final fixed_text_box:I = 0x7f0a0762

.field public static final flingRemove:I = 0x7f0a0763

.field public static final flow_fragment_container:I = 0x7f0a0764

.field public static final flow_list:I = 0x7f0a0765

.field public static final focus_grabber:I = 0x7f0a0766

.field public static final font_bottom_chip:I = 0x7f0a0767

.field public static final font_bottom_signature:I = 0x7f0a0768

.field public static final font_top_chip:I = 0x7f0a0769

.field public static final font_top_signature:I = 0x7f0a076a

.field public static final footer:I = 0x7f0a076b

.field public static final footer_message:I = 0x7f0a076c

.field public static final forever:I = 0x7f0a076d

.field public static final forget_reader_button:I = 0x7f0a076e

.field public static final forgot_password_link:I = 0x7f0a076f

.field public static final fourth_button:I = 0x7f0a0770

.field public static final fragment_container:I = 0x7f0a0771

.field public static final fragment_container_view_tag:I = 0x7f0a0772

.field public static final frame:I = 0x7f0a0773

.field public static final freeze_notification:I = 0x7f0a0774

.field public static final frequency_options:I = 0x7f0a0775

.field public static final frequently_asked_questions_content:I = 0x7f0a0776

.field public static final frequently_asked_questions_header:I = 0x7f0a0777

.field public static final front:I = 0x7f0a0778

.field public static final front_bottom_logo:I = 0x7f0a0779

.field public static final front_bottom_margin:I = 0x7f0a077a

.field public static final front_bottom_name:I = 0x7f0a077b

.field public static final front_business_name:I = 0x7f0a077c

.field public static final front_left_chip:I = 0x7f0a077d

.field public static final front_left_name:I = 0x7f0a077e

.field public static final front_right_margin:I = 0x7f0a077f

.field public static final front_top_margin:I = 0x7f0a0780

.field public static final full_ssn:I = 0x7f0a0781

.field public static final gc_add_value_button:I = 0x7f0a0782

.field public static final gc_current_balance_label:I = 0x7f0a0783

.field public static final gc_current_balance_value:I = 0x7f0a0784

.field public static final gc_history_event_container:I = 0x7f0a0785

.field public static final gc_history_row_amount:I = 0x7f0a0786

.field public static final gc_history_row_date:I = 0x7f0a0787

.field public static final gc_history_row_description:I = 0x7f0a0788

.field public static final get_help_with_card:I = 0x7f0a0789

.field public static final ghost_view:I = 0x7f0a078a

.field public static final ghost_view_holder:I = 0x7f0a078b

.field public static final gift_card_amount:I = 0x7f0a078c

.field public static final gift_card_balance_contents:I = 0x7f0a078d

.field public static final gift_card_brand_with_unmasked_digits:I = 0x7f0a078e

.field public static final gift_card_clear_balance_button:I = 0x7f0a078f

.field public static final gift_card_clear_balance_contents:I = 0x7f0a0790

.field public static final gift_card_clear_balance_progress_bar:I = 0x7f0a0791

.field public static final gift_card_clear_reason_other_text:I = 0x7f0a0792

.field public static final gift_card_clear_reason_title:I = 0x7f0a0793

.field public static final gift_card_custom_amount_button:I = 0x7f0a0794

.field public static final gift_card_custom_amount_container:I = 0x7f0a0795

.field public static final gift_card_custom_amount_field:I = 0x7f0a0796

.field public static final gift_card_figure:I = 0x7f0a0797

.field public static final gift_card_history_contents:I = 0x7f0a0798

.field public static final gift_card_number:I = 0x7f0a0799

.field public static final gift_card_on_file:I = 0x7f0a079a

.field public static final gift_card_on_file_container:I = 0x7f0a079b

.field public static final gift_card_order_hint:I = 0x7f0a079c

.field public static final gift_card_preset_amount_container:I = 0x7f0a079d

.field public static final gift_card_progress_bar:I = 0x7f0a079e

.field public static final gift_card_purchase_hint:I = 0x7f0a079f

.field public static final gift_card_refund_amount:I = 0x7f0a07a0

.field public static final gift_card_type_check_balance:I = 0x7f0a07a1

.field public static final gift_card_type_container:I = 0x7f0a07a2

.field public static final gift_card_type_egc:I = 0x7f0a07a3

.field public static final gift_card_type_physical:I = 0x7f0a07a4

.field public static final giftcards_settings_custom_policy:I = 0x7f0a07a5

.field public static final giftcards_settings_designs_row:I = 0x7f0a07a6

.field public static final giftcards_settings_egift_max:I = 0x7f0a07a7

.field public static final giftcards_settings_egift_min:I = 0x7f0a07a8

.field public static final giftcards_settings_egiftcard_content:I = 0x7f0a07a9

.field public static final giftcards_settings_enable_sell_egift_in_pos:I = 0x7f0a07aa

.field public static final giftcards_settings_loading:I = 0x7f0a07ab

.field public static final giftcards_settings_plastic_description:I = 0x7f0a07ac

.field public static final giftcards_settings_scroll_view:I = 0x7f0a07ad

.field public static final glyph:I = 0x7f0a07ae

.field public static final glyph_container:I = 0x7f0a07af

.field public static final glyph_edit_text_field:I = 0x7f0a07b0

.field public static final glyph_message:I = 0x7f0a07b1

.field public static final glyph_message_button:I = 0x7f0a07b2

.field public static final glyph_message_glyph:I = 0x7f0a07b3

.field public static final glyph_message_message:I = 0x7f0a07b4

.field public static final glyph_message_title:I = 0x7f0a07b5

.field public static final glyph_subtitle:I = 0x7f0a07b6

.field public static final glyph_title:I = 0x7f0a07b7

.field public static final gone:I = 0x7f0a07b8

.field public static final google_auth_button:I = 0x7f0a07b9

.field public static final google_pay_loading:I = 0x7f0a07ba

.field public static final google_pay_message:I = 0x7f0a07bb

.field public static final gravity:I = 0x7f0a07bc

.field public static final gross:I = 0x7f0a07bd

.field public static final group_divider:I = 0x7f0a07be

.field public static final groups:I = 0x7f0a07bf

.field public static final guest_unlock:I = 0x7f0a07c0

.field public static final guideline:I = 0x7f0a07c1

.field public static final guideline0:I = 0x7f0a07c2

.field public static final guideline1:I = 0x7f0a07c3

.field public static final hairline:I = 0x7f0a07c4

.field public static final hardware_printer_list_view:I = 0x7f0a07c5

.field public static final hardware_printer_select:I = 0x7f0a07c6

.field public static final has_ein:I = 0x7f0a07c7

.field public static final header:I = 0x7f0a07c8

.field public static final header_container:I = 0x7f0a07c9

.field public static final header_row:I = 0x7f0a07ca

.field public static final header_text_view:I = 0x7f0a07cb

.field public static final heading:I = 0x7f0a07cc

.field public static final heading2:I = 0x7f0a07cd

.field public static final headline:I = 0x7f0a07ce

.field public static final height:I = 0x7f0a07cf

.field public static final held_amount_description:I = 0x7f0a07d0

.field public static final held_amount_row:I = 0x7f0a07d1

.field public static final help_text:I = 0x7f0a07d2

.field public static final helper_text:I = 0x7f0a07d3

.field public static final helpful_button:I = 0x7f0a07d4

.field public static final hex_color:I = 0x7f0a07d5

.field public static final hide_button:I = 0x7f0a07d6

.field public static final hide_on_click_layout:I = 0x7f0a07d7

.field public static final hideable:I = 0x7f0a07d8

.field public static final high_contrast_keypad_button:I = 0x7f0a07d9

.field public static final hint:I = 0x7f0a07da

.field public static final history_action_button:I = 0x7f0a07db

.field public static final history_loading_layout_view:I = 0x7f0a07dc

.field public static final history_row_title:I = 0x7f0a07dd

.field public static final holder_name:I = 0x7f0a07de

.field public static final home:I = 0x7f0a07df

.field public static final homeAsUp:I = 0x7f0a07e0

.field public static final home_actionbar_view:I = 0x7f0a07e1

.field public static final home_navigation_bar_edit_view:I = 0x7f0a07e2

.field public static final home_navigation_bar_sale_view:I = 0x7f0a07e3

.field public static final home_panel_keypad:I = 0x7f0a07e4

.field public static final hour_minute_divider:I = 0x7f0a07e5

.field public static final hour_picker:I = 0x7f0a07e6

.field public static final hs__action_done:I = 0x7f0a07e7

.field public static final hs__attach_screenshot:I = 0x7f0a07e8

.field public static final hs__bottom_sheet:I = 0x7f0a07e9

.field public static final hs__bottomsheet_coordinator:I = 0x7f0a07ea

.field public static final hs__collapsed_picker_header_text:I = 0x7f0a07eb

.field public static final hs__confirmation:I = 0x7f0a07ec

.field public static final hs__contact_us:I = 0x7f0a07ed

.field public static final hs__conversationDetail:I = 0x7f0a07ee

.field public static final hs__conversationDetailWrapper:I = 0x7f0a07ef

.field public static final hs__conversation_cardview_container:I = 0x7f0a07f0

.field public static final hs__conversation_icon:I = 0x7f0a07f1

.field public static final hs__csat_option:I = 0x7f0a07f2

.field public static final hs__email:I = 0x7f0a07f3

.field public static final hs__emailWrapper:I = 0x7f0a07f4

.field public static final hs__empty_picker_view:I = 0x7f0a07f5

.field public static final hs__expanded_picker_header_text:I = 0x7f0a07f6

.field public static final hs__messageText:I = 0x7f0a07f7

.field public static final hs__messagesList:I = 0x7f0a07f8

.field public static final hs__new_conversation:I = 0x7f0a07f9

.field public static final hs__new_conversation_btn:I = 0x7f0a07fa

.field public static final hs__new_conversation_footer_reason:I = 0x7f0a07fb

.field public static final hs__notification_badge:I = 0x7f0a07fc

.field public static final hs__notification_badge_padding:I = 0x7f0a07fd

.field public static final hs__option:I = 0x7f0a07fe

.field public static final hs__optionsList:I = 0x7f0a07ff

.field public static final hs__picker_action_back:I = 0x7f0a0800

.field public static final hs__picker_action_clear:I = 0x7f0a0801

.field public static final hs__picker_action_collapse:I = 0x7f0a0802

.field public static final hs__picker_action_expand:I = 0x7f0a0803

.field public static final hs__picker_action_search:I = 0x7f0a0804

.field public static final hs__picker_collapsed_header:I = 0x7f0a0805

.field public static final hs__picker_collapsed_shadow:I = 0x7f0a0806

.field public static final hs__picker_expanded_header:I = 0x7f0a0807

.field public static final hs__picker_expanded_shadow:I = 0x7f0a0808

.field public static final hs__picker_header_container:I = 0x7f0a0809

.field public static final hs__picker_header_search:I = 0x7f0a080a

.field public static final hs__screenshot:I = 0x7f0a080b

.field public static final hs__search:I = 0x7f0a080c

.field public static final hs__sendMessageBtn:I = 0x7f0a080d

.field public static final hs__start_new_conversation:I = 0x7f0a080e

.field public static final hs__username:I = 0x7f0a080f

.field public static final hs__usernameWrapper:I = 0x7f0a0810

.field public static final hs_download_foreground_view:I = 0x7f0a0811

.field public static final hs_logo:I = 0x7f0a0812

.field public static final hud_glyph:I = 0x7f0a0813

.field public static final hud_message_text:I = 0x7f0a0814

.field public static final hud_progress:I = 0x7f0a0815

.field public static final hud_text:I = 0x7f0a0816

.field public static final hud_vector:I = 0x7f0a0817

.field public static final icon:I = 0x7f0a0818

.field public static final icon_glyph:I = 0x7f0a0819

.field public static final icon_group:I = 0x7f0a081a

.field public static final icon_image:I = 0x7f0a081b

.field public static final icon_only:I = 0x7f0a081c

.field public static final icon_text:I = 0x7f0a081d

.field public static final identify_reader_button:I = 0x7f0a081e

.field public static final ifRoom:I = 0x7f0a081f

.field public static final if_enabled:I = 0x7f0a0820

.field public static final image:I = 0x7f0a0821

.field public static final image_container:I = 0x7f0a0822

.field public static final image_icon_animator:I = 0x7f0a0823

.field public static final image_reader:I = 0x7f0a0824

.field public static final image_tile:I = 0x7f0a0825

.field public static final image_tile_help_text:I = 0x7f0a0826

.field public static final image_tile_radio:I = 0x7f0a0827

.field public static final imageview_container:I = 0x7f0a0828

.field public static final in_progress_drawer_container:I = 0x7f0a0829

.field public static final include_on_ticket_container:I = 0x7f0a082a

.field public static final include_row:I = 0x7f0a082b

.field public static final income_radios:I = 0x7f0a082c

.field public static final info:I = 0x7f0a082d

.field public static final info_bar_button:I = 0x7f0a082e

.field public static final info_bar_message:I = 0x7f0a082f

.field public static final info_icon:I = 0x7f0a0830

.field public static final initial_button:I = 0x7f0a0831

.field public static final inline_amount:I = 0x7f0a0832

.field public static final inline_button:I = 0x7f0a0833

.field public static final input_field:I = 0x7f0a0834

.field public static final installments_enter_card_option:I = 0x7f0a0835

.field public static final installments_helper_text:I = 0x7f0a0836

.field public static final installments_link_option:I = 0x7f0a0837

.field public static final installments_link_options_subtitle:I = 0x7f0a0838

.field public static final installments_link_options_title:I = 0x7f0a0839

.field public static final installments_qr_container:I = 0x7f0a083a

.field public static final installments_qr_error_glyph:I = 0x7f0a083b

.field public static final installments_qr_hint:I = 0x7f0a083c

.field public static final installments_qr_image:I = 0x7f0a083d

.field public static final installments_qr_spinner:I = 0x7f0a083e

.field public static final installments_qr_title:I = 0x7f0a083f

.field public static final installments_scan_code_button:I = 0x7f0a0840

.field public static final installments_send_button:I = 0x7f0a0841

.field public static final installments_sms_hint:I = 0x7f0a0842

.field public static final installments_sms_input:I = 0x7f0a0843

.field public static final installments_sms_sent_hint:I = 0x7f0a0844

.field public static final installments_sms_sent_title:I = 0x7f0a0845

.field public static final installments_sms_title:I = 0x7f0a0846

.field public static final installments_text_link:I = 0x7f0a0847

.field public static final installments_text_me_button:I = 0x7f0a0848

.field public static final instant_deposit_button:I = 0x7f0a0849

.field public static final instant_deposit_container:I = 0x7f0a084a

.field public static final instant_deposit_error:I = 0x7f0a084b

.field public static final instant_deposit_hint:I = 0x7f0a084c

.field public static final instant_deposit_progress:I = 0x7f0a084d

.field public static final instant_deposits_done_button:I = 0x7f0a084e

.field public static final instant_deposits_glyph_message:I = 0x7f0a084f

.field public static final instant_deposits_learn_more_button:I = 0x7f0a0850

.field public static final instant_deposits_link_debit_card_glyph_message:I = 0x7f0a0851

.field public static final instant_deposits_link_debit_card_learn_more_button:I = 0x7f0a0852

.field public static final instant_deposits_link_debit_card_ok_button:I = 0x7f0a0853

.field public static final instant_deposits_link_debit_card_progress:I = 0x7f0a0854

.field public static final instant_deposits_message_container:I = 0x7f0a0855

.field public static final instant_deposits_progress:I = 0x7f0a0856

.field public static final instant_transfer_fee:I = 0x7f0a0857

.field public static final instant_transfer_fee_section:I = 0x7f0a0858

.field public static final instant_transfers_divider:I = 0x7f0a0859

.field public static final instant_transfers_hint:I = 0x7f0a085a

.field public static final instant_transfers_instrument:I = 0x7f0a085b

.field public static final instant_transfers_section:I = 0x7f0a085c

.field public static final instant_transfers_set_up_button:I = 0x7f0a085d

.field public static final instant_transfers_toggle:I = 0x7f0a085e

.field public static final institution_number_field:I = 0x7f0a085f

.field public static final instructional_text:I = 0x7f0a0860

.field public static final instructions:I = 0x7f0a0861

.field public static final intent_how_custom:I = 0x7f0a0862

.field public static final intent_how_custom_message:I = 0x7f0a0863

.field public static final intent_how_preset:I = 0x7f0a0864

.field public static final intent_how_preset_message:I = 0x7f0a0865

.field public static final internal_preserved_label:I = 0x7f0a0866

.field public static final internal_title:I = 0x7f0a0867

.field public static final interval_day:I = 0x7f0a0868

.field public static final interval_month:I = 0x7f0a0869

.field public static final interval_options:I = 0x7f0a086a

.field public static final interval_week:I = 0x7f0a086b

.field public static final interval_year:I = 0x7f0a086c

.field public static final invalid_gift_card:I = 0x7f0a086d

.field public static final inventory_adjustment_save_spinner:I = 0x7f0a086e

.field public static final inventory_adjustment_save_spinner_text:I = 0x7f0a086f

.field public static final invisible:I = 0x7f0a0870

.field public static final invoice_add_payment_schedule_button:I = 0x7f0a0871

.field public static final invoice_add_reminder:I = 0x7f0a0872

.field public static final invoice_additional_emails_container:I = 0x7f0a0873

.field public static final invoice_allow_buyer_save_cof:I = 0x7f0a0874

.field public static final invoice_attach_file:I = 0x7f0a0875

.field public static final invoice_attach_file_divider:I = 0x7f0a0876

.field public static final invoice_attachment_progress_bar:I = 0x7f0a0877

.field public static final invoice_automatic_reminders_list_container:I = 0x7f0a0878

.field public static final invoice_automatic_reminders_toggle:I = 0x7f0a0879

.field public static final invoice_bill_history_view:I = 0x7f0a087a

.field public static final invoice_bottom_divider:I = 0x7f0a087b

.field public static final invoice_custom_message:I = 0x7f0a087c

.field public static final invoice_customer:I = 0x7f0a087d

.field public static final invoice_date_options:I = 0x7f0a087e

.field public static final invoice_date_row:I = 0x7f0a087f

.field public static final invoice_delivery:I = 0x7f0a0880

.field public static final invoice_detail_animator:I = 0x7f0a0881

.field public static final invoice_detail_content:I = 0x7f0a0882

.field public static final invoice_detail_display_state:I = 0x7f0a0883

.field public static final invoice_detail_error_message:I = 0x7f0a0884

.field public static final invoice_detail_header:I = 0x7f0a0885

.field public static final invoice_detail_legacy_status:I = 0x7f0a0886

.field public static final invoice_detail_line_items_container:I = 0x7f0a0887

.field public static final invoice_detail_line_items_divider:I = 0x7f0a0888

.field public static final invoice_detail_share_link:I = 0x7f0a0889

.field public static final invoice_detail_timeline_content:I = 0x7f0a088a

.field public static final invoice_details_id:I = 0x7f0a088b

.field public static final invoice_details_message:I = 0x7f0a088c

.field public static final invoice_details_title:I = 0x7f0a088d

.field public static final invoice_due_date:I = 0x7f0a088e

.field public static final invoice_edit_action:I = 0x7f0a088f

.field public static final invoice_edit_add_line_item:I = 0x7f0a0890

.field public static final invoice_edit_line_items_container:I = 0x7f0a0891

.field public static final invoice_edit_payment_schedule_button:I = 0x7f0a0892

.field public static final invoice_edit_request_deposit:I = 0x7f0a0893

.field public static final invoice_end_helper:I = 0x7f0a0894

.field public static final invoice_event_bullet:I = 0x7f0a0895

.field public static final invoice_event_bullet_highlight:I = 0x7f0a0896

.field public static final invoice_event_line:I = 0x7f0a0897

.field public static final invoice_filter_container:I = 0x7f0a0898

.field public static final invoice_filter_drop_down_arrow:I = 0x7f0a0899

.field public static final invoice_filter_dropdown:I = 0x7f0a089a

.field public static final invoice_filter_listview:I = 0x7f0a089b

.field public static final invoice_filter_options_container:I = 0x7f0a089c

.field public static final invoice_filter_title:I = 0x7f0a089d

.field public static final invoice_frequency_helper:I = 0x7f0a089e

.field public static final invoice_frequency_row:I = 0x7f0a089f

.field public static final invoice_help_text:I = 0x7f0a08a0

.field public static final invoice_history_animator:I = 0x7f0a08a1

.field public static final invoice_history_progress_bar:I = 0x7f0a08a2

.field public static final invoice_id_row:I = 0x7f0a08a3

.field public static final invoice_item_search:I = 0x7f0a08a4

.field public static final invoice_list:I = 0x7f0a08a5

.field public static final invoice_message:I = 0x7f0a08a6

.field public static final invoice_name:I = 0x7f0a08a7

.field public static final invoice_number:I = 0x7f0a08a8

.field public static final invoice_options_title:I = 0x7f0a08a9

.field public static final invoice_payment_schedule_container:I = 0x7f0a08aa

.field public static final invoice_payment_schedule_divider:I = 0x7f0a08ab

.field public static final invoice_payment_schedule_list:I = 0x7f0a08ac

.field public static final invoice_payments_container:I = 0x7f0a08ad

.field public static final invoice_payments_divider:I = 0x7f0a08ae

.field public static final invoice_preview_action:I = 0x7f0a08af

.field public static final invoice_read_only_detail:I = 0x7f0a08b0

.field public static final invoice_recurring:I = 0x7f0a08b1

.field public static final invoice_recurring_period_helper:I = 0x7f0a08b2

.field public static final invoice_reminder_confirmation_message:I = 0x7f0a08b3

.field public static final invoice_reminder_custom_message:I = 0x7f0a08b4

.field public static final invoice_reminder_custom_message_title:I = 0x7f0a08b5

.field public static final invoice_remove_reminder:I = 0x7f0a08b6

.field public static final invoice_repeat_helper:I = 0x7f0a08b7

.field public static final invoice_request_shipping_address:I = 0x7f0a08b8

.field public static final invoice_search:I = 0x7f0a08b9

.field public static final invoice_search_activator:I = 0x7f0a08ba

.field public static final invoice_section_container:I = 0x7f0a08bb

.field public static final invoice_section_header:I = 0x7f0a08bc

.field public static final invoice_send_date:I = 0x7f0a08bd

.field public static final invoice_timeline_button:I = 0x7f0a08be

.field public static final invoice_tippable:I = 0x7f0a08bf

.field public static final invoice_top_divider:I = 0x7f0a08c0

.field public static final invoices_app_feedback:I = 0x7f0a08c1

.field public static final invoices_list_error_message:I = 0x7f0a08c2

.field public static final issue_publish_id_label:I = 0x7f0a08c3

.field public static final issue_receipt_subtitle:I = 0x7f0a08c4

.field public static final issue_refund_button:I = 0x7f0a08c5

.field public static final issue_refund_errors_bar:I = 0x7f0a08c6

.field public static final issue_refund_view:I = 0x7f0a08c7

.field public static final italic:I = 0x7f0a08c8

.field public static final item:I = 0x7f0a08c9

.field public static final item_card_empty_note:I = 0x7f0a08ca

.field public static final item_card_empty_note_glyph:I = 0x7f0a08cb

.field public static final item_card_empty_note_message:I = 0x7f0a08cc

.field public static final item_card_empty_note_title:I = 0x7f0a08cd

.field public static final item_card_empty_open_items_button:I = 0x7f0a08ce

.field public static final item_category:I = 0x7f0a08cf

.field public static final item_category_selection_list:I = 0x7f0a08d0

.field public static final item_comp_button_row:I = 0x7f0a08d1

.field public static final item_description:I = 0x7f0a08d2

.field public static final item_editing_read_only_category:I = 0x7f0a08d3

.field public static final item_editing_read_only_description:I = 0x7f0a08d4

.field public static final item_editing_read_only_name:I = 0x7f0a08d5

.field public static final item_editing_save_spinner:I = 0x7f0a08d6

.field public static final item_editing_save_spinner_text:I = 0x7f0a08d7

.field public static final item_image:I = 0x7f0a08d8

.field public static final item_image_tile:I = 0x7f0a08d9

.field public static final item_list:I = 0x7f0a08da

.field public static final item_list_create_item:I = 0x7f0a08db

.field public static final item_list_noho_row:I = 0x7f0a08dc

.field public static final item_name:I = 0x7f0a08dd

.field public static final item_per_unit_price_and_quantity:I = 0x7f0a08de

.field public static final item_price:I = 0x7f0a08df

.field public static final item_quantity:I = 0x7f0a08e0

.field public static final item_remove_button_row:I = 0x7f0a08e1

.field public static final item_text_tile:I = 0x7f0a08e2

.field public static final item_touch_helper_previous_elevation:I = 0x7f0a08e3

.field public static final item_uncomp_button_row:I = 0x7f0a08e4

.field public static final item_variation_message:I = 0x7f0a08e5

.field public static final item_void_button_row:I = 0x7f0a08e6

.field public static final itemization_radios:I = 0x7f0a08e7

.field public static final items:I = 0x7f0a08e8

.field public static final items_button:I = 0x7f0a08e9

.field public static final items_container:I = 0x7f0a08ea

.field public static final items_recyclerview:I = 0x7f0a08eb

.field public static final items_section:I = 0x7f0a08ec

.field public static final items_taxes_and_discounts_help:I = 0x7f0a08ed

.field public static final items_title:I = 0x7f0a08ee

.field public static final jail_loading_percentage_circle:I = 0x7f0a08ef

.field public static final jail_loading_percentage_text:I = 0x7f0a08f0

.field public static final jail_loading_text:I = 0x7f0a08f1

.field public static final jail_progress_container:I = 0x7f0a08f2

.field public static final jail_view:I = 0x7f0a08f3

.field public static final jedi_banner:I = 0x7f0a08f4

.field public static final jedi_error:I = 0x7f0a08f5

.field public static final jedi_panel:I = 0x7f0a08f6

.field public static final jedi_progress_bar:I = 0x7f0a08f7

.field public static final jedi_search_bar:I = 0x7f0a08f8

.field public static final keypad:I = 0x7f0a08f9

.field public static final keypad_0:I = 0x7f0a08fa

.field public static final keypad_1:I = 0x7f0a08fb

.field public static final keypad_2:I = 0x7f0a08fc

.field public static final keypad_3:I = 0x7f0a08fd

.field public static final keypad_4:I = 0x7f0a08fe

.field public static final keypad_5:I = 0x7f0a08ff

.field public static final keypad_6:I = 0x7f0a0900

.field public static final keypad_7:I = 0x7f0a0901

.field public static final keypad_8:I = 0x7f0a0902

.field public static final keypad_9:I = 0x7f0a0903

.field public static final keypad_delete:I = 0x7f0a0904

.field public static final keypad_digit_0:I = 0x7f0a0905

.field public static final keypad_digits_1_through_9:I = 0x7f0a0906

.field public static final keypad_keys:I = 0x7f0a0907

.field public static final keypad_landscape_shim:I = 0x7f0a0908

.field public static final keypad_mode_selection_title:I = 0x7f0a0909

.field public static final keypad_screen_view:I = 0x7f0a090a

.field public static final keypad_title:I = 0x7f0a090b

.field public static final keypad_view_container:I = 0x7f0a090c

.field public static final label:I = 0x7f0a090d

.field public static final label2:I = 0x7f0a090e

.field public static final labeled:I = 0x7f0a090f

.field public static final large:I = 0x7f0a0910

.field public static final largeLabel:I = 0x7f0a0911

.field public static final last:I = 0x7f0a0912

.field public static final last_name:I = 0x7f0a0913

.field public static final later_button:I = 0x7f0a0914

.field public static final layout_content:I = 0x7f0a0915

.field public static final layout_runner_coordinator_binding:I = 0x7f0a0916

.field public static final layout_runner_hints:I = 0x7f0a0917

.field public static final learn_container:I = 0x7f0a0918

.field public static final learn_more:I = 0x7f0a0919

.field public static final learn_more_content:I = 0x7f0a091a

.field public static final learn_more_header:I = 0x7f0a091b

.field public static final learn_more_magstripe:I = 0x7f0a091c

.field public static final learn_more_r12:I = 0x7f0a091d

.field public static final learn_parent:I = 0x7f0a091e

.field public static final learn_space:I = 0x7f0a091f

.field public static final left:I = 0x7f0a0920

.field public static final left_margin_guide:I = 0x7f0a0921

.field public static final legacy_clock_in_out_button:I = 0x7f0a0922

.field public static final legacy_enter_passcode_to_unlock_header:I = 0x7f0a0923

.field public static final legacy_guest_unlock:I = 0x7f0a0924

.field public static final legacy_signature_available_options:I = 0x7f0a0925

.field public static final legal:I = 0x7f0a0926

.field public static final legal_message:I = 0x7f0a0927

.field public static final libraries:I = 0x7f0a0928

.field public static final libraries_row:I = 0x7f0a0929

.field public static final library_back_button:I = 0x7f0a092a

.field public static final library_bar_search:I = 0x7f0a092b

.field public static final library_default_container:I = 0x7f0a092c

.field public static final library_empty_note_glyph_message:I = 0x7f0a092d

.field public static final library_list:I = 0x7f0a092e

.field public static final library_list_view:I = 0x7f0a092f

.field public static final library_search:I = 0x7f0a0930

.field public static final library_search_activator:I = 0x7f0a0931

.field public static final library_search_container:I = 0x7f0a0932

.field public static final library_search_exit:I = 0x7f0a0933

.field public static final library_title_bar:I = 0x7f0a0934

.field public static final license_group_header:I = 0x7f0a0935

.field public static final license_group_list:I = 0x7f0a0936

.field public static final licenses_text:I = 0x7f0a0937

.field public static final light:I = 0x7f0a0938

.field public static final like_status:I = 0x7f0a0939

.field public static final line1:I = 0x7f0a093a

.field public static final line3:I = 0x7f0a093b

.field public static final line_chart:I = 0x7f0a093c

.field public static final line_items_divider:I = 0x7f0a093d

.field public static final line_items_section:I = 0x7f0a093e

.field public static final line_items_title:I = 0x7f0a093f

.field public static final line_row_glyph_view:I = 0x7f0a0940

.field public static final link:I = 0x7f0a0941

.field public static final link_name:I = 0x7f0a0942

.field public static final link_price:I = 0x7f0a0943

.field public static final link_purpose_label:I = 0x7f0a0944

.field public static final link_type_checkable_group:I = 0x7f0a0945

.field public static final link_updated_status_message:I = 0x7f0a0946

.field public static final linked_bank_account_section:I = 0x7f0a0947

.field public static final links_header:I = 0x7f0a0948

.field public static final list:I = 0x7f0a0949

.field public static final listMode:I = 0x7f0a094a

.field public static final list_fragment_container:I = 0x7f0a094b

.field public static final list_item:I = 0x7f0a094c

.field public static final load_more_message:I = 0x7f0a094d

.field public static final load_more_spinner:I = 0x7f0a094e

.field public static final loading_circle:I = 0x7f0a094f

.field public static final loading_error_state_view:I = 0x7f0a0950

.field public static final loading_error_tap_to_retry:I = 0x7f0a0951

.field public static final loading_instruments:I = 0x7f0a0952

.field public static final loading_panel:I = 0x7f0a0953

.field public static final loading_progressbar:I = 0x7f0a0954

.field public static final loading_screen_container:I = 0x7f0a0955

.field public static final loading_state_container:I = 0x7f0a0956

.field public static final loading_state_view:I = 0x7f0a0957

.field public static final loading_view:I = 0x7f0a0958

.field public static final location_computer:I = 0x7f0a0959

.field public static final location_invoice:I = 0x7f0a095a

.field public static final location_label:I = 0x7f0a095b

.field public static final location_phone:I = 0x7f0a095c

.field public static final location_radios:I = 0x7f0a095d

.field public static final location_settings_button:I = 0x7f0a095e

.field public static final location_tablet:I = 0x7f0a095f

.field public static final location_website:I = 0x7f0a0960

.field public static final logo_guideline_top:I = 0x7f0a0961

.field public static final logo_image:I = 0x7f0a0962

.field public static final lost_card_reason:I = 0x7f0a0963

.field public static final lost_square_card_choice:I = 0x7f0a0964

.field public static final loyalty_all_done_container:I = 0x7f0a0965

.field public static final loyalty_all_done_glyph:I = 0x7f0a0966

.field public static final loyalty_all_done_text:I = 0x7f0a0967

.field public static final loyalty_cash_app_banner:I = 0x7f0a0968

.field public static final loyalty_cash_app_text:I = 0x7f0a0969

.field public static final loyalty_claim_your_star:I = 0x7f0a096a

.field public static final loyalty_claim_your_star_help:I = 0x7f0a096b

.field public static final loyalty_congratulations_points_amount:I = 0x7f0a096c

.field public static final loyalty_congratulations_points_term:I = 0x7f0a096d

.field public static final loyalty_congratulations_rewards_amount:I = 0x7f0a096e

.field public static final loyalty_congratulations_rewards_term:I = 0x7f0a096f

.field public static final loyalty_congratulations_title:I = 0x7f0a0970

.field public static final loyalty_container:I = 0x7f0a0971

.field public static final loyalty_customer:I = 0x7f0a0972

.field public static final loyalty_customer_add_card_button:I = 0x7f0a0973

.field public static final loyalty_dropdown_adjust_status:I = 0x7f0a0974

.field public static final loyalty_dropdown_copy_phone:I = 0x7f0a0975

.field public static final loyalty_dropdown_delete_account:I = 0x7f0a0976

.field public static final loyalty_dropdown_edit_phone:I = 0x7f0a0977

.field public static final loyalty_dropdown_send_status:I = 0x7f0a0978

.field public static final loyalty_dropdown_transfer_account:I = 0x7f0a0979

.field public static final loyalty_dropdown_view_expiring_points:I = 0x7f0a097a

.field public static final loyalty_enroll_container:I = 0x7f0a097b

.field public static final loyalty_join:I = 0x7f0a097c

.field public static final loyalty_new_sale:I = 0x7f0a097d

.field public static final loyalty_no_thanks:I = 0x7f0a097e

.field public static final loyalty_phone_edit:I = 0x7f0a097f

.field public static final loyalty_phone_number:I = 0x7f0a0980

.field public static final loyalty_phone_update_hint:I = 0x7f0a0981

.field public static final loyalty_program_name:I = 0x7f0a0982

.field public static final loyalty_report_average_spend_label:I = 0x7f0a0983

.field public static final loyalty_report_average_visits_count_label:I = 0x7f0a0984

.field public static final loyalty_report_error_message_view:I = 0x7f0a0985

.field public static final loyalty_report_initial_circle:I = 0x7f0a0986

.field public static final loyalty_report_loyalty_customers_count_label:I = 0x7f0a0987

.field public static final loyalty_report_progress_bar:I = 0x7f0a0988

.field public static final loyalty_report_recycler_view:I = 0x7f0a0989

.field public static final loyalty_report_row_icon_initials_title_label:I = 0x7f0a098a

.field public static final loyalty_report_row_icon_initials_value_label:I = 0x7f0a098b

.field public static final loyalty_report_row_title_label:I = 0x7f0a098c

.field public static final loyalty_report_row_value_label:I = 0x7f0a098d

.field public static final loyalty_report_summary_label:I = 0x7f0a098e

.field public static final loyalty_report_summary_loyalty_customers_label:I = 0x7f0a098f

.field public static final loyalty_report_view_in_dashboard_button:I = 0x7f0a0990

.field public static final loyalty_reward_container:I = 0x7f0a0991

.field public static final loyalty_reward_requirement:I = 0x7f0a0992

.field public static final loyalty_reward_status:I = 0x7f0a0993

.field public static final loyalty_rewards_recycler_view:I = 0x7f0a0994

.field public static final loyalty_section_header:I = 0x7f0a0995

.field public static final loyalty_section_points:I = 0x7f0a0996

.field public static final loyalty_section_rows:I = 0x7f0a0997

.field public static final loyalty_section_see_all_rewards:I = 0x7f0a0998

.field public static final loyalty_settings_enable_loyalty:I = 0x7f0a0999

.field public static final loyalty_settings_enabled_content:I = 0x7f0a099a

.field public static final loyalty_settings_front_of_transaction_section:I = 0x7f0a099b

.field public static final loyalty_settings_front_of_transaction_toggle:I = 0x7f0a099c

.field public static final loyalty_settings_show_nonqualifying:I = 0x7f0a099d

.field public static final loyalty_settings_timeout_30:I = 0x7f0a099e

.field public static final loyalty_settings_timeout_60:I = 0x7f0a099f

.field public static final loyalty_settings_timeout_90:I = 0x7f0a09a0

.field public static final loyalty_tier_requirement:I = 0x7f0a09a1

.field public static final loyalty_tier_status:I = 0x7f0a09a2

.field public static final loyalty_tour_description:I = 0x7f0a09a3

.field public static final loyalty_tour_image:I = 0x7f0a09a4

.field public static final loyalty_tour_title:I = 0x7f0a09a5

.field public static final loyalty_view_reward_tiers:I = 0x7f0a09a6

.field public static final magstripe_learn_more_view:I = 0x7f0a09a7

.field public static final manage_coupons_and_rewards_list:I = 0x7f0a09a8

.field public static final manual_option:I = 0x7f0a09a9

.field public static final marin_action_bar_background:I = 0x7f0a09aa

.field public static final market_date_picker:I = 0x7f0a09ab

.field public static final masked:I = 0x7f0a09ac

.field public static final master:I = 0x7f0a09ad

.field public static final master_detail_ticket_view_action_bar_edit_button:I = 0x7f0a09ae

.field public static final master_detail_ticket_view_action_bar_merge_transaction_ticket_button:I = 0x7f0a09af

.field public static final master_detail_ticket_view_action_bar_new_ticket_button:I = 0x7f0a09b0

.field public static final master_detail_ticket_view_action_bar_title:I = 0x7f0a09b1

.field public static final master_detail_ticket_view_action_bar_up_button_glyph:I = 0x7f0a09b2

.field public static final master_detail_ticket_view_edit_bar:I = 0x7f0a09b3

.field public static final master_detail_ticket_view_edit_bar_bulk_delete_button:I = 0x7f0a09b4

.field public static final master_detail_ticket_view_edit_bar_bulk_merge_button:I = 0x7f0a09b5

.field public static final master_detail_ticket_view_edit_bar_bulk_move_button:I = 0x7f0a09b6

.field public static final master_detail_ticket_view_edit_bar_bulk_reprint_ticket_button:I = 0x7f0a09b7

.field public static final master_detail_ticket_view_edit_bar_bulk_transfer_button:I = 0x7f0a09b8

.field public static final master_detail_ticket_view_edit_bar_bulk_void_button:I = 0x7f0a09b9

.field public static final master_detail_ticket_view_edit_bar_cancel_button:I = 0x7f0a09ba

.field public static final master_detail_ticket_view_edit_bar_dismiss_button:I = 0x7f0a09bb

.field public static final master_detail_ticket_view_edit_bar_title:I = 0x7f0a09bc

.field public static final master_detail_ticket_view_sort_type_menu_container:I = 0x7f0a09bd

.field public static final master_detail_ticket_view_sort_type_menu_drop_down:I = 0x7f0a09be

.field public static final master_group_list_view:I = 0x7f0a09bf

.field public static final master_group_list_view_log_out_button:I = 0x7f0a09c0

.field public static final master_group_list_view_log_out_button_divider:I = 0x7f0a09c1

.field public static final master_group_list_view_log_out_button_padder:I = 0x7f0a09c2

.field public static final master_group_list_view_progress_bar:I = 0x7f0a09c3

.field public static final master_group_list_view_recycler_view:I = 0x7f0a09c4

.field public static final master_group_row:I = 0x7f0a09c5

.field public static final media_actions:I = 0x7f0a09c6

.field public static final medium:I = 0x7f0a09c7

.field public static final merchant_category_container:I = 0x7f0a09c8

.field public static final merchant_image:I = 0x7f0a09c9

.field public static final merchant_profile_animator:I = 0x7f0a09ca

.field public static final merchant_profile_content:I = 0x7f0a09cb

.field public static final merchant_profile_content_progress:I = 0x7f0a09cc

.field public static final merchant_profile_error:I = 0x7f0a09cd

.field public static final merchant_profile_id_for_state_restoration:I = 0x7f0a09ce

.field public static final merchant_profile_retry:I = 0x7f0a09cf

.field public static final merge_ticket_row_checkbox:I = 0x7f0a09d0

.field public static final merge_ticket_row_name:I = 0x7f0a09d1

.field public static final merge_ticket_view_recycler_view:I = 0x7f0a09d2

.field public static final message:I = 0x7f0a09d3

.field public static final message_attachment_disabled:I = 0x7f0a09d4

.field public static final message_button:I = 0x7f0a09d5

.field public static final message_container:I = 0x7f0a09d6

.field public static final message_container_bottom:I = 0x7f0a09d7

.field public static final message_text:I = 0x7f0a09d8

.field public static final messages_layout:I = 0x7f0a09d9

.field public static final messaging_notification_bubble:I = 0x7f0a09da

.field public static final middle:I = 0x7f0a09db

.field public static final middle_guideline:I = 0x7f0a09dc

.field public static final mini:I = 0x7f0a09dd

.field public static final minute_picker:I = 0x7f0a09de

.field public static final mobile_business_help_text:I = 0x7f0a09df

.field public static final mobile_payment_pad:I = 0x7f0a09e0

.field public static final mobile_phone_number_field:I = 0x7f0a09e1

.field public static final models_adhoc_id:I = 0x7f0a09e2

.field public static final modified_warning:I = 0x7f0a09e3

.field public static final modifier_card_progress:I = 0x7f0a09e4

.field public static final modifier_list_container:I = 0x7f0a09e5

.field public static final modifier_list_header:I = 0x7f0a09e6

.field public static final modifier_option_delete:I = 0x7f0a09e7

.field public static final modifier_option_drag_handle:I = 0x7f0a09e8

.field public static final modifier_option_name:I = 0x7f0a09e9

.field public static final modifier_option_price:I = 0x7f0a09ea

.field public static final modifier_set_apply_to_items:I = 0x7f0a09eb

.field public static final modifiers_label:I = 0x7f0a09ec

.field public static final month_grid:I = 0x7f0a09ed

.field public static final month_navigation_bar:I = 0x7f0a09ee

.field public static final month_navigation_fragment_toggle:I = 0x7f0a09ef

.field public static final month_navigation_next:I = 0x7f0a09f0

.field public static final month_navigation_previous:I = 0x7f0a09f1

.field public static final month_picker:I = 0x7f0a09f2

.field public static final month_title:I = 0x7f0a09f3

.field public static final more_options:I = 0x7f0a09f4

.field public static final move_ticket_list_view:I = 0x7f0a09f5

.field public static final move_ticket_view_recycler_view:I = 0x7f0a09f6

.field public static final mtrl_calendar_day_selector_frame:I = 0x7f0a09f7

.field public static final mtrl_calendar_days_of_week:I = 0x7f0a09f8

.field public static final mtrl_calendar_frame:I = 0x7f0a09f9

.field public static final mtrl_calendar_main_pane:I = 0x7f0a09fa

.field public static final mtrl_calendar_months:I = 0x7f0a09fb

.field public static final mtrl_calendar_selection_frame:I = 0x7f0a09fc

.field public static final mtrl_calendar_text_input_frame:I = 0x7f0a09fd

.field public static final mtrl_calendar_year_selector_frame:I = 0x7f0a09fe

.field public static final mtrl_card_checked_layer_id:I = 0x7f0a09ff

.field public static final mtrl_child_content_container:I = 0x7f0a0a00

.field public static final mtrl_internal_children_alpha_tag:I = 0x7f0a0a01

.field public static final mtrl_picker_fullscreen:I = 0x7f0a0a02

.field public static final mtrl_picker_header:I = 0x7f0a0a03

.field public static final mtrl_picker_header_selection_text:I = 0x7f0a0a04

.field public static final mtrl_picker_header_title_and_selection:I = 0x7f0a0a05

.field public static final mtrl_picker_header_toggle:I = 0x7f0a0a06

.field public static final mtrl_picker_text_input_date:I = 0x7f0a0a07

.field public static final mtrl_picker_text_input_range_end:I = 0x7f0a0a08

.field public static final mtrl_picker_text_input_range_start:I = 0x7f0a0a09

.field public static final mtrl_picker_title_text:I = 0x7f0a0a0a

.field public static final multiply:I = 0x7f0a0a0b

.field public static final name:I = 0x7f0a0a0c

.field public static final name_field:I = 0x7f0a0a0d

.field public static final name_on_card:I = 0x7f0a0a0e

.field public static final nav_bar_content:I = 0x7f0a0a0f

.field public static final navigation_header_container:I = 0x7f0a0a10

.field public static final net:I = 0x7f0a0a11

.field public static final net_total:I = 0x7f0a0a12

.field public static final networkErrorFooter:I = 0x7f0a0a13

.field public static final networkErrorFooterText:I = 0x7f0a0a14

.field public static final networkErrorFooterTryAgainText:I = 0x7f0a0a15

.field public static final networkErrorIcon:I = 0x7f0a0a16

.field public static final networkErrorProgressBar:I = 0x7f0a0a17

.field public static final network_button:I = 0x7f0a0a18

.field public static final never:I = 0x7f0a0a19

.field public static final never_received_square_card_choice:I = 0x7f0a0a1a

.field public static final never_skip_signature:I = 0x7f0a0a1b

.field public static final new_category_button:I = 0x7f0a0a1c

.field public static final new_category_name_view:I = 0x7f0a0a1d

.field public static final new_sale:I = 0x7f0a0a1e

.field public static final new_station:I = 0x7f0a0a1f

.field public static final new_ticket_no_predefined_tickets_message:I = 0x7f0a0a20

.field public static final new_ticket_no_predefined_tickets_title:I = 0x7f0a0a21

.field public static final next_business_day_button:I = 0x7f0a0a22

.field public static final next_button_wrapper:I = 0x7f0a0a23

.field public static final nfc_prompt:I = 0x7f0a0a24

.field public static final nfc_tap_handler:I = 0x7f0a0a25

.field public static final noScroll:I = 0x7f0a0a26

.field public static final no_cards_on_file:I = 0x7f0a0a27

.field public static final no_connected_scales_container:I = 0x7f0a0a28

.field public static final no_connected_scales_layout:I = 0x7f0a0a29

.field public static final no_ein:I = 0x7f0a0a2a

.field public static final no_faqs_view:I = 0x7f0a0a2b

.field public static final no_internet_glyph:I = 0x7f0a0a2c

.field public static final no_receipt_button:I = 0x7f0a0a2d

.field public static final no_search_results:I = 0x7f0a0a2e

.field public static final no_signature_tooltip:I = 0x7f0a0a2f

.field public static final no_thanks_button:I = 0x7f0a0a30

.field public static final no_tip_button:I = 0x7f0a0a31

.field public static final noho_buyer_action_bar_left_button:I = 0x7f0a0a32

.field public static final noho_buyer_action_bar_left_glyph_button:I = 0x7f0a0a33

.field public static final noho_buyer_action_bar_right_button:I = 0x7f0a0a34

.field public static final noho_buyer_action_bar_subtitle:I = 0x7f0a0a35

.field public static final noho_buyer_action_bar_ticket_name:I = 0x7f0a0a36

.field public static final noho_buyer_action_bar_title:I = 0x7f0a0a37

.field public static final noho_buyer_action_bar_up_button:I = 0x7f0a0a38

.field public static final noho_buyer_action_container:I = 0x7f0a0a39

.field public static final noho_buyer_action_container_call_to_action:I = 0x7f0a0a3a

.field public static final noho_message_help:I = 0x7f0a0a3b

.field public static final noho_message_image:I = 0x7f0a0a3c

.field public static final noho_message_link_text:I = 0x7f0a0a3d

.field public static final noho_message_message:I = 0x7f0a0a3e

.field public static final noho_message_primary_button:I = 0x7f0a0a3f

.field public static final noho_message_secondary_button:I = 0x7f0a0a40

.field public static final noho_message_title:I = 0x7f0a0a41

.field public static final noho_title_value_row_title:I = 0x7f0a0a42

.field public static final noho_title_value_row_value:I = 0x7f0a0a43

.field public static final non_stable_action_bar:I = 0x7f0a0a44

.field public static final none:I = 0x7f0a0a45

.field public static final normal:I = 0x7f0a0a46

.field public static final note:I = 0x7f0a0a47

.field public static final note_button:I = 0x7f0a0a48

.field public static final notes_section_header:I = 0x7f0a0a49

.field public static final notification_auto_capture:I = 0x7f0a0a4a

.field public static final notification_auto_void:I = 0x7f0a0a4b

.field public static final notification_background:I = 0x7f0a0a4c

.field public static final notification_center_account_button:I = 0x7f0a0a4d

.field public static final notification_center_action_bar:I = 0x7f0a0a4e

.field public static final notification_center_content_animator:I = 0x7f0a0a4f

.field public static final notification_center_content_container:I = 0x7f0a0a50

.field public static final notification_center_error_description:I = 0x7f0a0a51

.field public static final notification_center_error_title:I = 0x7f0a0a52

.field public static final notification_center_loading_animator:I = 0x7f0a0a53

.field public static final notification_center_no_notifications_message_view:I = 0x7f0a0a54

.field public static final notification_center_progress_bar:I = 0x7f0a0a55

.field public static final notification_center_recycler_view:I = 0x7f0a0a56

.field public static final notification_center_view:I = 0x7f0a0a57

.field public static final notification_center_whats_new_button:I = 0x7f0a0a58

.field public static final notification_check_miryo:I = 0x7f0a0a59

.field public static final notification_error_action_bar:I = 0x7f0a0a5a

.field public static final notification_error_message:I = 0x7f0a0a5b

.field public static final notification_foregrounded_service:I = 0x7f0a0a5c

.field public static final notification_incomplete_transaction:I = 0x7f0a0a5d

.field public static final notification_loading_action_bar:I = 0x7f0a0a5e

.field public static final notification_loading_progress_bar:I = 0x7f0a0a5f

.field public static final notification_main_column:I = 0x7f0a0a60

.field public static final notification_main_column_container:I = 0x7f0a0a61

.field public static final notification_out_of_disk_space:I = 0x7f0a0a62

.field public static final notification_payment:I = 0x7f0a0a63

.field public static final notification_payment_logged_out:I = 0x7f0a0a64

.field public static final notification_preferences_email_row:I = 0x7f0a0a65

.field public static final notification_preferences_email_value_row:I = 0x7f0a0a66

.field public static final notification_preferences_pref_card_declines:I = 0x7f0a0a67

.field public static final notification_profile_error:I = 0x7f0a0a68

.field public static final notification_read_only_storage:I = 0x7f0a0a69

.field public static final notification_row_container:I = 0x7f0a0a6a

.field public static final notification_row_content:I = 0x7f0a0a6b

.field public static final notification_row_dot:I = 0x7f0a0a6c

.field public static final notification_row_open_in_browser_icon:I = 0x7f0a0a6d

.field public static final notification_row_right_icon:I = 0x7f0a0a6e

.field public static final notification_row_title:I = 0x7f0a0a6f

.field public static final notification_support_messages:I = 0x7f0a0a70

.field public static final notify_recipients:I = 0x7f0a0a71

.field public static final ntep_certification_number:I = 0x7f0a0a72

.field public static final ntep_company:I = 0x7f0a0a73

.field public static final ntep_model:I = 0x7f0a0a74

.field public static final ntep_row:I = 0x7f0a0a75

.field public static final ntep_version:I = 0x7f0a0a76

.field public static final null_state_container:I = 0x7f0a0a77

.field public static final null_state_image:I = 0x7f0a0a78

.field public static final null_state_message:I = 0x7f0a0a79

.field public static final null_state_subtitle:I = 0x7f0a0a7a

.field public static final null_state_title:I = 0x7f0a0a7b

.field public static final number:I = 0x7f0a0a7c

.field public static final number_attribute:I = 0x7f0a0a7d

.field public static final number_box:I = 0x7f0a0a7e

.field public static final number_last_digits:I = 0x7f0a0a7f

.field public static final off:I = 0x7f0a0a80

.field public static final offline_error_view:I = 0x7f0a0a81

.field public static final offline_payments_hint:I = 0x7f0a0a82

.field public static final on:I = 0x7f0a0a83

.field public static final onDown:I = 0x7f0a0a84

.field public static final onLongPress:I = 0x7f0a0a85

.field public static final onMove:I = 0x7f0a0a86

.field public static final onboarding_address:I = 0x7f0a0a87

.field public static final onboarding_address_title:I = 0x7f0a0a88

.field public static final onboarding_bank_account:I = 0x7f0a0a89

.field public static final onboarding_bank_account_title:I = 0x7f0a0a8a

.field public static final onboarding_button_destructive:I = 0x7f0a0a8b

.field public static final onboarding_button_primary:I = 0x7f0a0a8c

.field public static final onboarding_button_root:I = 0x7f0a0a8d

.field public static final onboarding_button_secondary:I = 0x7f0a0a8e

.field public static final onboarding_date_picker:I = 0x7f0a0a8f

.field public static final onboarding_date_title:I = 0x7f0a0a90

.field public static final onboarding_first_name:I = 0x7f0a0a91

.field public static final onboarding_hardware_disclaimer:I = 0x7f0a0a92

.field public static final onboarding_hardware_image:I = 0x7f0a0a93

.field public static final onboarding_hardware_items:I = 0x7f0a0a94

.field public static final onboarding_hardware_subtitle:I = 0x7f0a0a95

.field public static final onboarding_hardware_title:I = 0x7f0a0a96

.field public static final onboarding_image:I = 0x7f0a0a97

.field public static final onboarding_last_name:I = 0x7f0a0a98

.field public static final onboarding_multi_list:I = 0x7f0a0a99

.field public static final onboarding_multi_list_title:I = 0x7f0a0a9a

.field public static final onboarding_person_name_title:I = 0x7f0a0a9b

.field public static final onboarding_prompt_view:I = 0x7f0a0a9c

.field public static final onboarding_spinner:I = 0x7f0a0a9d

.field public static final onboarding_text_field:I = 0x7f0a0a9e

.field public static final onboarding_text_title:I = 0x7f0a0a9f

.field public static final one_third:I = 0x7f0a0aa0

.field public static final one_third_vertical_divider:I = 0x7f0a0aa1

.field public static final one_time:I = 0x7f0a0aa2

.field public static final online_checkout_checkoutlink_help_text:I = 0x7f0a0aa3

.field public static final online_checkout_checkoutlink_qr_code:I = 0x7f0a0aa4

.field public static final online_checkout_checkoutlink_url:I = 0x7f0a0aa5

.field public static final online_checkout_checkoutlinks_screen_container:I = 0x7f0a0aa6

.field public static final online_checkout_error_message_view:I = 0x7f0a0aa7

.field public static final online_checkout_error_screen_container:I = 0x7f0a0aa8

.field public static final online_checkout_loading_screen_container:I = 0x7f0a0aa9

.field public static final online_checkout_pay_link_name_container:I = 0x7f0a0aaa

.field public static final online_checkout_share_checkoutlink:I = 0x7f0a0aab

.field public static final onoff:I = 0x7f0a0aac

.field public static final open:I = 0x7f0a0aad

.field public static final open_in_browser_dialog:I = 0x7f0a0aae

.field public static final open_in_browser_dialog_cancel_button:I = 0x7f0a0aaf

.field public static final open_in_browser_dialog_description:I = 0x7f0a0ab0

.field public static final open_in_browser_dialog_open_button:I = 0x7f0a0ab1

.field public static final open_in_browser_dialog_title:I = 0x7f0a0ab2

.field public static final open_tickets_as_home_screen_toggle:I = 0x7f0a0ab3

.field public static final open_tickets_as_home_screen_toggle_hint:I = 0x7f0a0ab4

.field public static final open_tickets_new_ticket_button:I = 0x7f0a0ab5

.field public static final open_tickets_recycler_view:I = 0x7f0a0ab6

.field public static final open_tickets_toggle:I = 0x7f0a0ab7

.field public static final open_tickets_toggle_hint:I = 0x7f0a0ab8

.field public static final opt_in_message:I = 0x7f0a0ab9

.field public static final option_after_date:I = 0x7f0a0aba

.field public static final option_before_date:I = 0x7f0a0abb

.field public static final option_list_item_layout:I = 0x7f0a0abc

.field public static final option_on_date:I = 0x7f0a0abd

.field public static final option_text:I = 0x7f0a0abe

.field public static final option_value_search_bar:I = 0x7f0a0abf

.field public static final options:I = 0x7f0a0ac0

.field public static final options_container:I = 0x7f0a0ac1

.field public static final options_header:I = 0x7f0a0ac2

.field public static final options_message_view:I = 0x7f0a0ac3

.field public static final order_button:I = 0x7f0a0ac4

.field public static final order_confirmed_help:I = 0x7f0a0ac5

.field public static final order_confirmed_submessage:I = 0x7f0a0ac6

.field public static final order_contactless_reader_button:I = 0x7f0a0ac7

.field public static final order_count:I = 0x7f0a0ac8

.field public static final order_date:I = 0x7f0a0ac9

.field public static final order_done:I = 0x7f0a0aca

.field public static final order_entry_page_id:I = 0x7f0a0acb

.field public static final order_entry_view_container:I = 0x7f0a0acc

.field public static final order_entry_view_pager:I = 0x7f0a0acd

.field public static final order_glyph:I = 0x7f0a0ace

.field public static final order_glyph_message:I = 0x7f0a0acf

.field public static final order_history_row:I = 0x7f0a0ad0

.field public static final order_legacy_reader_row:I = 0x7f0a0ad1

.field public static final order_list:I = 0x7f0a0ad2

.field public static final order_magstripe_reader_button:I = 0x7f0a0ad3

.field public static final order_message:I = 0x7f0a0ad4

.field public static final order_message_for_no_title:I = 0x7f0a0ad5

.field public static final order_message_for_title:I = 0x7f0a0ad6

.field public static final order_more:I = 0x7f0a0ad7

.field public static final order_name_cardholder_name_status:I = 0x7f0a0ad8

.field public static final order_name_edit_text:I = 0x7f0a0ad9

.field public static final order_name_fetch_progress:I = 0x7f0a0ada

.field public static final order_name_next_button:I = 0x7f0a0adb

.field public static final order_number:I = 0x7f0a0adc

.field public static final order_reader:I = 0x7f0a0add

.field public static final order_reader_all_hardware:I = 0x7f0a0ade

.field public static final order_reader_contactless:I = 0x7f0a0adf

.field public static final order_reader_magstripe:I = 0x7f0a0ae0

.field public static final order_reader_row:I = 0x7f0a0ae1

.field public static final order_square_card:I = 0x7f0a0ae2

.field public static final order_square_card_business_account_name_row:I = 0x7f0a0ae3

.field public static final order_square_card_business_name_continue_button:I = 0x7f0a0ae4

.field public static final order_square_card_business_name_help_message:I = 0x7f0a0ae5

.field public static final order_square_card_business_name_row:I = 0x7f0a0ae6

.field public static final order_square_card_business_name_warning:I = 0x7f0a0ae7

.field public static final order_square_card_cardholder_agreement:I = 0x7f0a0ae8

.field public static final order_square_card_fetching_business_info_message:I = 0x7f0a0ae9

.field public static final order_square_card_message:I = 0x7f0a0aea

.field public static final order_square_card_title:I = 0x7f0a0aeb

.field public static final order_status:I = 0x7f0a0aec

.field public static final order_ticket_autonumber_switch:I = 0x7f0a0aed

.field public static final order_ticket_custom_name_switch:I = 0x7f0a0aee

.field public static final order_ticket_name_field:I = 0x7f0a0aef

.field public static final order_ticket_next:I = 0x7f0a0af0

.field public static final order_title:I = 0x7f0a0af1

.field public static final order_total:I = 0x7f0a0af2

.field public static final order_track:I = 0x7f0a0af3

.field public static final ordered_square_card_activate_trigger:I = 0x7f0a0af4

.field public static final ordered_square_card_message:I = 0x7f0a0af5

.field public static final orderhub_add_note_animator:I = 0x7f0a0af6

.field public static final orderhub_add_note_card:I = 0x7f0a0af7

.field public static final orderhub_add_note_spinner:I = 0x7f0a0af8

.field public static final orderhub_add_tracking_animator:I = 0x7f0a0af9

.field public static final orderhub_add_tracking_carrier_recycler_view:I = 0x7f0a0afa

.field public static final orderhub_add_tracking_delete_button:I = 0x7f0a0afb

.field public static final orderhub_add_tracking_number:I = 0x7f0a0afc

.field public static final orderhub_add_tracking_other_carrier:I = 0x7f0a0afd

.field public static final orderhub_add_tracking_scrollview:I = 0x7f0a0afe

.field public static final orderhub_alert_settings_enabled:I = 0x7f0a0aff

.field public static final orderhub_alert_settings_explanation:I = 0x7f0a0b00

.field public static final orderhub_alert_settings_frequency_1:I = 0x7f0a0b01

.field public static final orderhub_alert_settings_frequency_2:I = 0x7f0a0b02

.field public static final orderhub_alert_settings_frequency_3:I = 0x7f0a0b03

.field public static final orderhub_alert_settings_frequency_4:I = 0x7f0a0b04

.field public static final orderhub_alert_settings_frequency_container:I = 0x7f0a0b05

.field public static final orderhub_bill_history_progress_bar:I = 0x7f0a0b06

.field public static final orderhub_bill_history_view:I = 0x7f0a0b07

.field public static final orderhub_delete_note_button:I = 0x7f0a0b08

.field public static final orderhub_detail_container:I = 0x7f0a0b09

.field public static final orderhub_detail_customer_name:I = 0x7f0a0b0a

.field public static final orderhub_detail_filter_row:I = 0x7f0a0b0b

.field public static final orderhub_detail_filters_container:I = 0x7f0a0b0c

.field public static final orderhub_detail_filters_dropdown_arrow:I = 0x7f0a0b0d

.field public static final orderhub_detail_filters_dropdown_container:I = 0x7f0a0b0e

.field public static final orderhub_detail_filters_recycler_view:I = 0x7f0a0b0f

.field public static final orderhub_detail_filters_title:I = 0x7f0a0b10

.field public static final orderhub_detail_header_row:I = 0x7f0a0b11

.field public static final orderhub_detail_icon:I = 0x7f0a0b12

.field public static final orderhub_detail_message_view:I = 0x7f0a0b13

.field public static final orderhub_detail_order_display_state:I = 0x7f0a0b14

.field public static final orderhub_detail_order_id:I = 0x7f0a0b15

.field public static final orderhub_detail_order_placed_at:I = 0x7f0a0b16

.field public static final orderhub_detail_order_quick_action:I = 0x7f0a0b17

.field public static final orderhub_detail_order_quick_action_failure:I = 0x7f0a0b18

.field public static final orderhub_detail_order_row:I = 0x7f0a0b19

.field public static final orderhub_detail_order_title:I = 0x7f0a0b1a

.field public static final orderhub_detail_orders_not_syncing_message:I = 0x7f0a0b1b

.field public static final orderhub_detail_orders_not_syncing_title:I = 0x7f0a0b1c

.field public static final orderhub_detail_orders_recycler_view:I = 0x7f0a0b1d

.field public static final orderhub_detail_search:I = 0x7f0a0b1e

.field public static final orderhub_detail_search_error_message:I = 0x7f0a0b1f

.field public static final orderhub_detail_search_instructions:I = 0x7f0a0b20

.field public static final orderhub_detail_view:I = 0x7f0a0b21

.field public static final orderhub_filter_recycler_view:I = 0x7f0a0b22

.field public static final orderhub_fulfillments_section:I = 0x7f0a0b23

.field public static final orderhub_item_selection_items_list:I = 0x7f0a0b24

.field public static final orderhub_items_section:I = 0x7f0a0b25

.field public static final orderhub_items_section_header:I = 0x7f0a0b26

.field public static final orderhub_master_filter_row:I = 0x7f0a0b27

.field public static final orderhub_note_characters_remaining:I = 0x7f0a0b28

.field public static final orderhub_note_edit_text:I = 0x7f0a0b29

.field public static final orderhub_order_add_tracking:I = 0x7f0a0b2a

.field public static final orderhub_order_adjust_fulfillment_time_button:I = 0x7f0a0b2b

.field public static final orderhub_order_adjust_pickup_time_recycler_view:I = 0x7f0a0b2c

.field public static final orderhub_order_buttons_container:I = 0x7f0a0b2d

.field public static final orderhub_order_cancelation_reason_recycler_view:I = 0x7f0a0b2e

.field public static final orderhub_order_cancellation_spinner_container:I = 0x7f0a0b2f

.field public static final orderhub_order_cancellation_spinner_message:I = 0x7f0a0b30

.field public static final orderhub_order_cancellation_spinner_refund_selection:I = 0x7f0a0b31

.field public static final orderhub_order_container:I = 0x7f0a0b32

.field public static final orderhub_order_created_date:I = 0x7f0a0b33

.field public static final orderhub_order_customer_container:I = 0x7f0a0b34

.field public static final orderhub_order_customer_email:I = 0x7f0a0b35

.field public static final orderhub_order_customer_header:I = 0x7f0a0b36

.field public static final orderhub_order_customer_name:I = 0x7f0a0b37

.field public static final orderhub_order_customer_phone:I = 0x7f0a0b38

.field public static final orderhub_order_detail_animator:I = 0x7f0a0b39

.field public static final orderhub_order_detail_scrollview:I = 0x7f0a0b3a

.field public static final orderhub_order_detail_spinner:I = 0x7f0a0b3b

.field public static final orderhub_order_fulfillment_time:I = 0x7f0a0b3c

.field public static final orderhub_order_fulfillment_time_container:I = 0x7f0a0b3d

.field public static final orderhub_order_fulfillment_time_header:I = 0x7f0a0b3e

.field public static final orderhub_order_id:I = 0x7f0a0b3f

.field public static final orderhub_order_id_container:I = 0x7f0a0b40

.field public static final orderhub_order_id_header:I = 0x7f0a0b41

.field public static final orderhub_order_item_name_description_section:I = 0x7f0a0b42

.field public static final orderhub_order_item_row:I = 0x7f0a0b43

.field public static final orderhub_order_item_row_item_name:I = 0x7f0a0b44

.field public static final orderhub_order_item_row_item_price:I = 0x7f0a0b45

.field public static final orderhub_order_item_row_item_quantity:I = 0x7f0a0b46

.field public static final orderhub_order_item_row_item_subtitle:I = 0x7f0a0b47

.field public static final orderhub_order_items_container:I = 0x7f0a0b48

.field public static final orderhub_order_items_header:I = 0x7f0a0b49

.field public static final orderhub_order_items_header_detail:I = 0x7f0a0b4a

.field public static final orderhub_order_note:I = 0x7f0a0b4b

.field public static final orderhub_order_note_container:I = 0x7f0a0b4c

.field public static final orderhub_order_price_container:I = 0x7f0a0b4d

.field public static final orderhub_order_price_row_label:I = 0x7f0a0b4e

.field public static final orderhub_order_price_row_value:I = 0x7f0a0b4f

.field public static final orderhub_order_primary_action:I = 0x7f0a0b50

.field public static final orderhub_order_recipient_address_body:I = 0x7f0a0b51

.field public static final orderhub_order_recipient_address_container:I = 0x7f0a0b52

.field public static final orderhub_order_recipient_address_header:I = 0x7f0a0b53

.field public static final orderhub_order_recipient_address_name:I = 0x7f0a0b54

.field public static final orderhub_order_secondary_action:I = 0x7f0a0b55

.field public static final orderhub_order_secondary_action_button_container:I = 0x7f0a0b56

.field public static final orderhub_order_shipment_method:I = 0x7f0a0b57

.field public static final orderhub_order_shipment_method_container:I = 0x7f0a0b58

.field public static final orderhub_order_shipment_method_header:I = 0x7f0a0b59

.field public static final orderhub_order_shipping_spinner_container:I = 0x7f0a0b5a

.field public static final orderhub_order_source_icon:I = 0x7f0a0b5b

.field public static final orderhub_order_spacing_before_price:I = 0x7f0a0b5c

.field public static final orderhub_order_spacing_before_tenders:I = 0x7f0a0b5d

.field public static final orderhub_order_status:I = 0x7f0a0b5e

.field public static final orderhub_order_status_view:I = 0x7f0a0b5f

.field public static final orderhub_order_tender_amount:I = 0x7f0a0b60

.field public static final orderhub_order_tender_details:I = 0x7f0a0b61

.field public static final orderhub_order_tenders_container:I = 0x7f0a0b62

.field public static final orderhub_order_tracking_container:I = 0x7f0a0b63

.field public static final orderhub_order_tracking_edit:I = 0x7f0a0b64

.field public static final orderhub_order_tracking_info:I = 0x7f0a0b65

.field public static final orderhub_order_tracking_label:I = 0x7f0a0b66

.field public static final orderhub_order_tracking_spinner:I = 0x7f0a0b67

.field public static final orderhub_printing_settings_enabled:I = 0x7f0a0b68

.field public static final orderhub_printing_settings_explanation:I = 0x7f0a0b69

.field public static final orderhub_quick_actions_settings_enabled:I = 0x7f0a0b6a

.field public static final orderhub_quick_actions_settings_explanation:I = 0x7f0a0b6b

.field public static final orderhub_recycler_title_row:I = 0x7f0a0b6c

.field public static final orderhub_reveal_search_icon:I = 0x7f0a0b6d

.field public static final orderhub_search_results_spinner:I = 0x7f0a0b6e

.field public static final organization:I = 0x7f0a0b6f

.field public static final other_payment_type_helper_text:I = 0x7f0a0b70

.field public static final other_reason:I = 0x7f0a0b71

.field public static final other_tender_disclaimer:I = 0x7f0a0b72

.field public static final outline:I = 0x7f0a0b73

.field public static final outstanding_amount:I = 0x7f0a0b74

.field public static final outstanding_container:I = 0x7f0a0b75

.field public static final outstanding_header:I = 0x7f0a0b76

.field public static final overlay:I = 0x7f0a0b77

.field public static final overlay_bottom:I = 0x7f0a0b78

.field public static final overlay_middle:I = 0x7f0a0b79

.field public static final overlay_top:I = 0x7f0a0b7a

.field public static final overview_header:I = 0x7f0a0b7b

.field public static final overview_row:I = 0x7f0a0b7c

.field public static final owner_address:I = 0x7f0a0b7d

.field public static final owner_birthdate:I = 0x7f0a0b7e

.field public static final owner_name:I = 0x7f0a0b7f

.field public static final owner_ssn:I = 0x7f0a0b80

.field public static final packed:I = 0x7f0a0b81

.field public static final page_description:I = 0x7f0a0b82

.field public static final page_indicator:I = 0x7f0a0b83

.field public static final page_label:I = 0x7f0a0b84

.field public static final paged_tutorial_views:I = 0x7f0a0b85

.field public static final pager_tabs:I = 0x7f0a0b86

.field public static final paid_amount:I = 0x7f0a0b87

.field public static final paid_container:I = 0x7f0a0b88

.field public static final paid_header:I = 0x7f0a0b89

.field public static final paid_in_button:I = 0x7f0a0b8a

.field public static final paid_in_out:I = 0x7f0a0b8b

.field public static final paid_in_out_row:I = 0x7f0a0b8c

.field public static final paid_in_out_total_row:I = 0x7f0a0b8d

.field public static final paid_out_button:I = 0x7f0a0b8e

.field public static final pairing_header_content:I = 0x7f0a0b8f

.field public static final pairing_help_devices:I = 0x7f0a0b90

.field public static final pairing_help_support:I = 0x7f0a0b91

.field public static final pairing_help_video:I = 0x7f0a0b92

.field public static final pairing_list:I = 0x7f0a0b93

.field public static final pairing_retrying_help:I = 0x7f0a0b94

.field public static final pairing_screen_content_container:I = 0x7f0a0b95

.field public static final pairing_screen_content_help:I = 0x7f0a0b96

.field public static final pairing_screen_help:I = 0x7f0a0b97

.field public static final pairing_screen_reader_r12:I = 0x7f0a0b98

.field public static final pairing_screen_reference:I = 0x7f0a0b99

.field public static final pan:I = 0x7f0a0b9a

.field public static final panel_components:I = 0x7f0a0b9b

.field public static final panel_error:I = 0x7f0a0b9c

.field public static final panel_error_button:I = 0x7f0a0b9d

.field public static final panel_error_message:I = 0x7f0a0b9e

.field public static final panel_error_support:I = 0x7f0a0b9f

.field public static final panel_error_title:I = 0x7f0a0ba0

.field public static final panel_message:I = 0x7f0a0ba1

.field public static final panel_primary_buttons:I = 0x7f0a0ba2

.field public static final panel_root:I = 0x7f0a0ba3

.field public static final panel_success:I = 0x7f0a0ba4

.field public static final paper_signature_available_options:I = 0x7f0a0ba5

.field public static final paper_signature_divider:I = 0x7f0a0ba6

.field public static final paper_signature_group:I = 0x7f0a0ba7

.field public static final paper_signature_receipt_options:I = 0x7f0a0ba8

.field public static final paragraph:I = 0x7f0a0ba9

.field public static final paragraph_text:I = 0x7f0a0baa

.field public static final paragraph_title:I = 0x7f0a0bab

.field public static final parallax:I = 0x7f0a0bac

.field public static final parent:I = 0x7f0a0bad

.field public static final parentPanel:I = 0x7f0a0bae

.field public static final parent_matrix:I = 0x7f0a0baf

.field public static final parent_panel:I = 0x7f0a0bb0

.field public static final partial_auth_message:I = 0x7f0a0bb1

.field public static final passcode_employee_management_container:I = 0x7f0a0bb2

.field public static final passcode_employee_management_options:I = 0x7f0a0bb3

.field public static final passcode_settings_action_bar:I = 0x7f0a0bb4

.field public static final passcode_settings_scroll_view:I = 0x7f0a0bb5

.field public static final passcode_settings_timeout_action_bar:I = 0x7f0a0bb6

.field public static final passcode_settings_timeout_radio_group:I = 0x7f0a0bb7

.field public static final passcode_title:I = 0x7f0a0bb8

.field public static final passcode_unlock:I = 0x7f0a0bb9

.field public static final passcode_unlock_merchant_image:I = 0x7f0a0bba

.field public static final passcode_unlock_pad:I = 0x7f0a0bbb

.field public static final passcodes_settings_after_each_sale_check:I = 0x7f0a0bbc

.field public static final passcodes_settings_after_logout_check:I = 0x7f0a0bbd

.field public static final passcodes_settings_after_logout_description:I = 0x7f0a0bbe

.field public static final passcodes_settings_after_timeout:I = 0x7f0a0bbf

.field public static final passcodes_settings_back_out_of_sale_check:I = 0x7f0a0bc0

.field public static final passcodes_settings_create_or_edit_passcode:I = 0x7f0a0bc1

.field public static final passcodes_settings_create_or_edit_passcode_success:I = 0x7f0a0bc2

.field public static final passcodes_settings_enable_section:I = 0x7f0a0bc3

.field public static final passcodes_settings_enable_switch:I = 0x7f0a0bc4

.field public static final passcodes_settings_enable_switch_description:I = 0x7f0a0bc5

.field public static final passcodes_settings_require_passcode_section:I = 0x7f0a0bc6

.field public static final passcodes_settings_team_passcode:I = 0x7f0a0bc7

.field public static final passcodes_settings_team_passcode_description:I = 0x7f0a0bc8

.field public static final passcodes_settings_team_passcode_section:I = 0x7f0a0bc9

.field public static final passcodes_settings_team_passcode_switch:I = 0x7f0a0bca

.field public static final passcodes_settings_team_permissions:I = 0x7f0a0bcb

.field public static final passcodes_settings_timeout:I = 0x7f0a0bcc

.field public static final passcodes_settings_timeout_1_minute_radio:I = 0x7f0a0bcd

.field public static final passcodes_settings_timeout_30_seconds_radio:I = 0x7f0a0bce

.field public static final passcodes_settings_timeout_5_minutes_radio:I = 0x7f0a0bcf

.field public static final passcodes_settings_timeout_description:I = 0x7f0a0bd0

.field public static final passcodes_settings_timeout_never_radio:I = 0x7f0a0bd1

.field public static final password:I = 0x7f0a0bd2

.field public static final password_field:I = 0x7f0a0bd3

.field public static final password_show:I = 0x7f0a0bd4

.field public static final password_toggle:I = 0x7f0a0bd5

.field public static final pay_card_screen_helper_text:I = 0x7f0a0bd6

.field public static final pay_card_screen_pan_warning:I = 0x7f0a0bd7

.field public static final pay_cash_amount:I = 0x7f0a0bd8

.field public static final pay_cash_tender_button:I = 0x7f0a0bd9

.field public static final pay_credit_card_tender_button:I = 0x7f0a0bda

.field public static final pay_link_amount_label:I = 0x7f0a0bdb

.field public static final pay_link_get_link_button:I = 0x7f0a0bdc

.field public static final pay_link_name:I = 0x7f0a0bdd

.field public static final pay_other_record_payment_button:I = 0x7f0a0bde

.field public static final pay_third_party_amount:I = 0x7f0a0bdf

.field public static final pay_third_party_card_view:I = 0x7f0a0be0

.field public static final pay_third_party_helper_text:I = 0x7f0a0be1

.field public static final pay_third_party_tender_button:I = 0x7f0a0be2

.field public static final payment_amount_options_container:I = 0x7f0a0be3

.field public static final payment_list:I = 0x7f0a0be4

.field public static final payment_note:I = 0x7f0a0be5

.field public static final payment_pad_landscape_tablet:I = 0x7f0a0be6

.field public static final payment_pad_left_half:I = 0x7f0a0be7

.field public static final payment_pad_portrait:I = 0x7f0a0be8

.field public static final payment_pad_portrait_amount:I = 0x7f0a0be9

.field public static final payment_pad_right_half:I = 0x7f0a0bea

.field public static final payment_pad_tabs:I = 0x7f0a0beb

.field public static final payment_plan_section_helper:I = 0x7f0a0bec

.field public static final payment_processing_action_container:I = 0x7f0a0bed

.field public static final payment_processing_action_hint:I = 0x7f0a0bee

.field public static final payment_processing_primary_button:I = 0x7f0a0bef

.field public static final payment_processing_secondary_button:I = 0x7f0a0bf0

.field public static final payment_prompt_view:I = 0x7f0a0bf1

.field public static final payment_request_summary_text:I = 0x7f0a0bf2

.field public static final payment_schedule_list:I = 0x7f0a0bf3

.field public static final payment_type_action_bar:I = 0x7f0a0bf4

.field public static final payment_type_button:I = 0x7f0a0bf5

.field public static final payment_type_container:I = 0x7f0a0bf6

.field public static final payment_type_helper_text:I = 0x7f0a0bf7

.field public static final payment_type_icon:I = 0x7f0a0bf8

.field public static final payment_type_image:I = 0x7f0a0bf9

.field public static final payment_type_more_view:I = 0x7f0a0bfa

.field public static final payment_type_options_scroll_view:I = 0x7f0a0bfb

.field public static final payment_type_status_header:I = 0x7f0a0bfc

.field public static final payment_type_subtitle:I = 0x7f0a0bfd

.field public static final payment_type_text:I = 0x7f0a0bfe

.field public static final payment_type_title:I = 0x7f0a0bff

.field public static final payment_type_view:I = 0x7f0a0c00

.field public static final payment_types_settings_header_title:I = 0x7f0a0c01

.field public static final payment_types_settings_info_message:I = 0x7f0a0c02

.field public static final payment_types_settings_instructions:I = 0x7f0a0c03

.field public static final payment_types_settings_preview:I = 0x7f0a0c04

.field public static final payment_types_settings_recycler_view:I = 0x7f0a0c05

.field public static final payroll_learn_more:I = 0x7f0a0c06

.field public static final pdf_icon:I = 0x7f0a0c07

.field public static final peekHeight:I = 0x7f0a0c08

.field public static final per_item_discounts:I = 0x7f0a0c09

.field public static final percent:I = 0x7f0a0c0a

.field public static final percent_character:I = 0x7f0a0c0b

.field public static final percentage_button:I = 0x7f0a0c0c

.field public static final percentage_hint:I = 0x7f0a0c0d

.field public static final percentage_paid:I = 0x7f0a0c0e

.field public static final percentage_text:I = 0x7f0a0c0f

.field public static final percentage_text_box:I = 0x7f0a0c10

.field public static final permission_actionbar:I = 0x7f0a0c11

.field public static final permission_cancel:I = 0x7f0a0c12

.field public static final permission_denied_body:I = 0x7f0a0c13

.field public static final permission_denied_button:I = 0x7f0a0c14

.field public static final permission_denied_glyph:I = 0x7f0a0c15

.field public static final permission_denied_title:I = 0x7f0a0c16

.field public static final permission_enable:I = 0x7f0a0c17

.field public static final permission_explanation:I = 0x7f0a0c18

.field public static final permission_glyph:I = 0x7f0a0c19

.field public static final permission_passcode_pad:I = 0x7f0a0c1a

.field public static final permission_title:I = 0x7f0a0c1b

.field public static final personal_info_birth_date:I = 0x7f0a0c1c

.field public static final personal_info_first_name:I = 0x7f0a0c1d

.field public static final personal_info_last_4_header:I = 0x7f0a0c1e

.field public static final personal_info_last_name:I = 0x7f0a0c1f

.field public static final personal_info_last_ssn:I = 0x7f0a0c20

.field public static final personal_info_personal_address:I = 0x7f0a0c21

.field public static final personal_info_phone_number:I = 0x7f0a0c22

.field public static final personal_info_view:I = 0x7f0a0c23

.field public static final phone_attribute:I = 0x7f0a0c24

.field public static final phone_number:I = 0x7f0a0c25

.field public static final phone_payment_pad_charge_and_tickets_buttons:I = 0x7f0a0c26

.field public static final phone_ticket_row:I = 0x7f0a0c27

.field public static final photo_action_view:I = 0x7f0a0c28

.field public static final photo_label_header:I = 0x7f0a0c29

.field public static final pie_chart:I = 0x7f0a0c2a

.field public static final pie_chart_complete_label:I = 0x7f0a0c2b

.field public static final pin:I = 0x7f0a0c2c

.field public static final pin_card_info:I = 0x7f0a0c2d

.field public static final pin_message:I = 0x7f0a0c2e

.field public static final pin_title:I = 0x7f0a0c2f

.field public static final plan_action_button:I = 0x7f0a0c30

.field public static final plan_amount_outstanding_image:I = 0x7f0a0c31

.field public static final plan_amount_outstanding_text:I = 0x7f0a0c32

.field public static final plan_amount_outstanding_value:I = 0x7f0a0c33

.field public static final plan_payment_amount_image:I = 0x7f0a0c34

.field public static final plan_payment_amount_section:I = 0x7f0a0c35

.field public static final plan_payment_amount_sub_text:I = 0x7f0a0c36

.field public static final plan_payment_amount_text:I = 0x7f0a0c37

.field public static final plan_payment_amount_value:I = 0x7f0a0c38

.field public static final please_sign_here:I = 0x7f0a0c39

.field public static final points_amount:I = 0x7f0a0c3a

.field public static final popup_actions_container:I = 0x7f0a0c3b

.field public static final portable_library_list:I = 0x7f0a0c3c

.field public static final pos_api_progress_bar:I = 0x7f0a0c3d

.field public static final post_receipt_glyph:I = 0x7f0a0c3e

.field public static final postal:I = 0x7f0a0c3f

.field public static final postal_keyboard_switch:I = 0x7f0a0c40

.field public static final postal_pii_scrubber:I = 0x7f0a0c41

.field public static final precision_group:I = 0x7f0a0c42

.field public static final precision_label:I = 0x7f0a0c43

.field public static final predefined_ticket_groups_header:I = 0x7f0a0c44

.field public static final predefined_ticket_row:I = 0x7f0a0c45

.field public static final predefined_tickets_recycler_view:I = 0x7f0a0c46

.field public static final predefined_tickets_toggle:I = 0x7f0a0c47

.field public static final predefined_tickets_toggle_hint:I = 0x7f0a0c48

.field public static final preserved_label:I = 0x7f0a0c49

.field public static final preview_footer_row:I = 0x7f0a0c4a

.field public static final preview_invoice_progress_bar:I = 0x7f0a0c4b

.field public static final preview_save_draft:I = 0x7f0a0c4c

.field public static final preview_web_progress_bar:I = 0x7f0a0c4d

.field public static final preview_webview:I = 0x7f0a0c4e

.field public static final price:I = 0x7f0a0c4f

.field public static final price_button:I = 0x7f0a0c50

.field public static final price_edit_text:I = 0x7f0a0c51

.field public static final price_field:I = 0x7f0a0c52

.field public static final price_header:I = 0x7f0a0c53

.field public static final price_input_field:I = 0x7f0a0c54

.field public static final price_row:I = 0x7f0a0c55

.field public static final price_type_group:I = 0x7f0a0c56

.field public static final price_type_row:I = 0x7f0a0c57

.field public static final pricing_rule_help_text:I = 0x7f0a0c58

.field public static final primary:I = 0x7f0a0c59

.field public static final primary_action_container:I = 0x7f0a0c5a

.field public static final primary_button:I = 0x7f0a0c5b

.field public static final primary_container:I = 0x7f0a0c5c

.field public static final primary_header:I = 0x7f0a0c5d

.field public static final primary_institution_number:I = 0x7f0a0c5e

.field public static final primary_institution_number_field:I = 0x7f0a0c5f

.field public static final primary_value:I = 0x7f0a0c60

.field public static final print_a_ticket_for_each_item_switch:I = 0x7f0a0c61

.field public static final print_button:I = 0x7f0a0c62

.field public static final print_compact_tickets_switch:I = 0x7f0a0c63

.field public static final print_error_message:I = 0x7f0a0c64

.field public static final print_error_recycler:I = 0x7f0a0c65

.field public static final print_formal_receipt_button:I = 0x7f0a0c66

.field public static final print_gift_receipt_button:I = 0x7f0a0c67

.field public static final print_items_selected:I = 0x7f0a0c68

.field public static final print_order_ticket_stubs_switch:I = 0x7f0a0c69

.field public static final print_order_ticket_stubs_unavailable:I = 0x7f0a0c6a

.field public static final print_order_tickets_message:I = 0x7f0a0c6b

.field public static final print_order_tickets_no_categories_message:I = 0x7f0a0c6c

.field public static final print_order_tickets_switch:I = 0x7f0a0c6d

.field public static final print_receipt_button:I = 0x7f0a0c6e

.field public static final print_receipts_hint:I = 0x7f0a0c6f

.field public static final print_receipts_switch:I = 0x7f0a0c70

.field public static final print_receipts_unavailable:I = 0x7f0a0c71

.field public static final print_report_animator:I = 0x7f0a0c72

.field public static final print_report_configure_view:I = 0x7f0a0c73

.field public static final print_report_message_view:I = 0x7f0a0c74

.field public static final printer_station_name:I = 0x7f0a0c75

.field public static final printer_station_top_divider:I = 0x7f0a0c76

.field public static final printer_stations_list:I = 0x7f0a0c77

.field public static final privacy_policy_row:I = 0x7f0a0c78

.field public static final profile_section_header:I = 0x7f0a0c79

.field public static final profile_section_rows:I = 0x7f0a0c7a

.field public static final progress:I = 0x7f0a0c7b

.field public static final progress_bar:I = 0x7f0a0c7c

.field public static final progress_bar_load_invoice:I = 0x7f0a0c7d

.field public static final progress_circular:I = 0x7f0a0c7e

.field public static final progress_container:I = 0x7f0a0c7f

.field public static final progress_description_text_view:I = 0x7f0a0c80

.field public static final progress_horizontal:I = 0x7f0a0c81

.field public static final progress_loading_invoice:I = 0x7f0a0c82

.field public static final progressbar:I = 0x7f0a0c83

.field public static final prompt_rate_on_play_store:I = 0x7f0a0c84

.field public static final protection_row:I = 0x7f0a0c85

.field public static final public_profile_address:I = 0x7f0a0c86

.field public static final public_profile_address_edit_button:I = 0x7f0a0c87

.field public static final public_profile_address_help_text:I = 0x7f0a0c88

.field public static final public_profile_address_save_button:I = 0x7f0a0c89

.field public static final public_profile_business_name:I = 0x7f0a0c8a

.field public static final public_profile_description:I = 0x7f0a0c8b

.field public static final public_profile_edit_address:I = 0x7f0a0c8c

.field public static final public_profile_edit_mobile_business:I = 0x7f0a0c8d

.field public static final public_profile_email:I = 0x7f0a0c8e

.field public static final public_profile_email_suggestion_box:I = 0x7f0a0c8f

.field public static final public_profile_facebook:I = 0x7f0a0c90

.field public static final public_profile_featured:I = 0x7f0a0c91

.field public static final public_profile_logo_container:I = 0x7f0a0c92

.field public static final public_profile_logo_image:I = 0x7f0a0c93

.field public static final public_profile_logo_message:I = 0x7f0a0c94

.field public static final public_profile_logo_text:I = 0x7f0a0c95

.field public static final public_profile_mobile_business:I = 0x7f0a0c96

.field public static final public_profile_phone:I = 0x7f0a0c97

.field public static final public_profile_photo_on_receipt:I = 0x7f0a0c98

.field public static final public_profile_twitter:I = 0x7f0a0c99

.field public static final public_profile_website:I = 0x7f0a0c9a

.field public static final qr_code:I = 0x7f0a0c9b

.field public static final quantity_decrease:I = 0x7f0a0c9c

.field public static final quantity_footer_text:I = 0x7f0a0c9d

.field public static final quantity_increase:I = 0x7f0a0c9e

.field public static final quantity_section_header:I = 0x7f0a0c9f

.field public static final quantity_text:I = 0x7f0a0ca0

.field public static final question_divider:I = 0x7f0a0ca1

.field public static final question_footer:I = 0x7f0a0ca2

.field public static final question_footer_message:I = 0x7f0a0ca3

.field public static final question_list:I = 0x7f0a0ca4

.field public static final question_radios:I = 0x7f0a0ca5

.field public static final question_title:I = 0x7f0a0ca6

.field public static final quick_action_spinner:I = 0x7f0a0ca7

.field public static final quick_amount_value:I = 0x7f0a0ca8

.field public static final quick_amounts:I = 0x7f0a0ca9

.field public static final quick_amounts_auto:I = 0x7f0a0caa

.field public static final quick_amounts_checkable:I = 0x7f0a0cab

.field public static final quick_amounts_off:I = 0x7f0a0cac

.field public static final quick_amounts_preview_amounts:I = 0x7f0a0cad

.field public static final quick_amounts_preview_padlock:I = 0x7f0a0cae

.field public static final quick_amounts_preview_root:I = 0x7f0a0caf

.field public static final quick_amounts_set:I = 0x7f0a0cb0

.field public static final quick_amounts_settings:I = 0x7f0a0cb1

.field public static final quick_amounts_switch:I = 0x7f0a0cb2

.field public static final quick_tip:I = 0x7f0a0cb3

.field public static final quick_tip_hint:I = 0x7f0a0cb4

.field public static final r12_education_cable:I = 0x7f0a0cb5

.field public static final r12_education_charge:I = 0x7f0a0cb6

.field public static final r12_education_chip_card:I = 0x7f0a0cb7

.field public static final r12_education_dip:I = 0x7f0a0cb8

.field public static final r12_education_dip_matte_container:I = 0x7f0a0cb9

.field public static final r12_education_dots_green:I = 0x7f0a0cba

.field public static final r12_education_dots_orange:I = 0x7f0a0cbb

.field public static final r12_education_image:I = 0x7f0a0cbc

.field public static final r12_education_learn_even_more:I = 0x7f0a0cbd

.field public static final r12_education_mag_card:I = 0x7f0a0cbe

.field public static final r12_education_matte:I = 0x7f0a0cbf

.field public static final r12_education_message:I = 0x7f0a0cc0

.field public static final r12_education_next:I = 0x7f0a0cc1

.field public static final r12_education_pager:I = 0x7f0a0cc2

.field public static final r12_education_pager_indicator:I = 0x7f0a0cc3

.field public static final r12_education_phone_apple_pay:I = 0x7f0a0cc4

.field public static final r12_education_phone_r4:I = 0x7f0a0cc5

.field public static final r12_education_r12:I = 0x7f0a0cc6

.field public static final r12_education_start_selling:I = 0x7f0a0cc7

.field public static final r12_education_sticker:I = 0x7f0a0cc8

.field public static final r12_education_swipe:I = 0x7f0a0cc9

.field public static final r12_education_tap:I = 0x7f0a0cca

.field public static final r12_education_tap_card_with_hand:I = 0x7f0a0ccb

.field public static final r12_education_title:I = 0x7f0a0ccc

.field public static final r12_education_video:I = 0x7f0a0ccd

.field public static final r12_education_video_play:I = 0x7f0a0cce

.field public static final r12_education_video_preview:I = 0x7f0a0ccf

.field public static final r12_education_welcome:I = 0x7f0a0cd0

.field public static final r12_education_x_phone:I = 0x7f0a0cd1

.field public static final r12_education_x_tablet:I = 0x7f0a0cd2

.field public static final r6_first_time_video_done:I = 0x7f0a0cd3

.field public static final r6_first_time_video_fallback_background:I = 0x7f0a0cd4

.field public static final r6_first_time_video_layout_container:I = 0x7f0a0cd5

.field public static final r6_first_time_video_surface_view:I = 0x7f0a0cd6

.field public static final r6_first_time_video_text_container:I = 0x7f0a0cd7

.field public static final r6_first_time_video_text_layer:I = 0x7f0a0cd8

.field public static final radio:I = 0x7f0a0cd9

.field public static final radio_group:I = 0x7f0a0cda

.field public static final radios:I = 0x7f0a0cdb

.field public static final rate_content:I = 0x7f0a0cdc

.field public static final rate_heading:I = 0x7f0a0cdd

.field public static final rate_image:I = 0x7f0a0cde

.field public static final rate_on_play_store_button:I = 0x7f0a0cdf

.field public static final rate_subtitle:I = 0x7f0a0ce0

.field public static final rate_title:I = 0x7f0a0ce1

.field public static final ratingBar:I = 0x7f0a0ce2

.field public static final read_only_details_header:I = 0x7f0a0ce3

.field public static final reader:I = 0x7f0a0ce4

.field public static final reader_accepts_row:I = 0x7f0a0ce5

.field public static final reader_battery_glyph:I = 0x7f0a0ce6

.field public static final reader_battery_text:I = 0x7f0a0ce7

.field public static final reader_connection_row:I = 0x7f0a0ce8

.field public static final reader_detail_name_row:I = 0x7f0a0ce9

.field public static final reader_detail_name_row_border:I = 0x7f0a0cea

.field public static final reader_detail_name_row_helper:I = 0x7f0a0ceb

.field public static final reader_detail_name_row_title:I = 0x7f0a0cec

.field public static final reader_firmware_row:I = 0x7f0a0ced

.field public static final reader_list:I = 0x7f0a0cee

.field public static final reader_message_bar_current_text_view:I = 0x7f0a0cef

.field public static final reader_message_bar_previous_text_view:I = 0x7f0a0cf0

.field public static final reader_message_bar_view:I = 0x7f0a0cf1

.field public static final reader_serial_number_row:I = 0x7f0a0cf2

.field public static final reader_status_advice:I = 0x7f0a0cf3

.field public static final reader_status_headline:I = 0x7f0a0cf4

.field public static final reader_status_row:I = 0x7f0a0cf5

.field public static final reader_warning_bottom_default_button:I = 0x7f0a0cf6

.field public static final reader_warning_glyph_text:I = 0x7f0a0cf7

.field public static final reader_warning_important_message:I = 0x7f0a0cf8

.field public static final reader_warning_top_alternative_button:I = 0x7f0a0cf9

.field public static final reason_checkable_group:I = 0x7f0a0cfa

.field public static final reason_list:I = 0x7f0a0cfb

.field public static final reason_row:I = 0x7f0a0cfc

.field public static final reasons:I = 0x7f0a0cfd

.field public static final receipt_content:I = 0x7f0a0cfe

.field public static final receipt_detail_add_tip_button:I = 0x7f0a0cff

.field public static final receipt_detail_items_container:I = 0x7f0a0d00

.field public static final receipt_detail_quicktip_editor:I = 0x7f0a0d01

.field public static final receipt_detail_settle_tips_section:I = 0x7f0a0d02

.field public static final receipt_detail_tax_breakdown_container:I = 0x7f0a0d03

.field public static final receipt_detail_tender_tip_title:I = 0x7f0a0d04

.field public static final receipt_detail_total_container:I = 0x7f0a0d05

.field public static final receipt_details_view_animator:I = 0x7f0a0d06

.field public static final receipt_digital_hint:I = 0x7f0a0d07

.field public static final receipt_frame:I = 0x7f0a0d08

.field public static final receipt_list_row_amount:I = 0x7f0a0d09

.field public static final receipt_list_row_color_strip:I = 0x7f0a0d0a

.field public static final receipt_list_row_subtitle:I = 0x7f0a0d0b

.field public static final receipt_list_row_thumbnail:I = 0x7f0a0d0c

.field public static final receipt_list_row_title_and_quantity:I = 0x7f0a0d0d

.field public static final receipt_list_row_title_and_subtitle_container:I = 0x7f0a0d0e

.field public static final receipt_options:I = 0x7f0a0d0f

.field public static final receipt_row:I = 0x7f0a0d10

.field public static final receipt_subtitle:I = 0x7f0a0d11

.field public static final receipt_title:I = 0x7f0a0d12

.field public static final receipt_top_panel:I = 0x7f0a0d13

.field public static final recent_activity_container:I = 0x7f0a0d14

.field public static final recent_activity_empty:I = 0x7f0a0d15

.field public static final recent_activty_title:I = 0x7f0a0d16

.field public static final record_payment_amount:I = 0x7f0a0d17

.field public static final record_payment_method:I = 0x7f0a0d18

.field public static final record_payment_method_container:I = 0x7f0a0d19

.field public static final record_payment_note:I = 0x7f0a0d1a

.field public static final record_payment_send_receipt:I = 0x7f0a0d1b

.field public static final recurrence_end:I = 0x7f0a0d1c

.field public static final recurrence_options:I = 0x7f0a0d1d

.field public static final recurring:I = 0x7f0a0d1e

.field public static final recycler:I = 0x7f0a0d1f

.field public static final recyclerView:I = 0x7f0a0d20

.field public static final recycler_view:I = 0x7f0a0d21

.field public static final redeem_reward_tiers_list:I = 0x7f0a0d22

.field public static final redeem_reward_tiers_list_title:I = 0x7f0a0d23

.field public static final redeem_rewards_cart_points_content:I = 0x7f0a0d24

.field public static final redeem_rewards_cart_points_title:I = 0x7f0a0d25

.field public static final redeem_rewards_cart_points_value:I = 0x7f0a0d26

.field public static final redeem_rewards_contact_points_content:I = 0x7f0a0d27

.field public static final redeem_rewards_contact_points_title:I = 0x7f0a0d28

.field public static final redeem_rewards_contact_points_value:I = 0x7f0a0d29

.field public static final redeem_rewards_container:I = 0x7f0a0d2a

.field public static final redeem_rewards_content:I = 0x7f0a0d2b

.field public static final redeem_rewards_coupons_list:I = 0x7f0a0d2c

.field public static final redeem_rewards_coupons_list_title:I = 0x7f0a0d2d

.field public static final redeem_rewards_loyalty_rewards_subtitle:I = 0x7f0a0d2e

.field public static final redeem_rewards_loyalty_rewards_title:I = 0x7f0a0d2f

.field public static final redeem_rewards_points_content:I = 0x7f0a0d30

.field public static final redeem_rewards_points_message:I = 0x7f0a0d31

.field public static final redeem_rewards_workflow_choose_customer:I = 0x7f0a0d32

.field public static final reference:I = 0x7f0a0d33

.field public static final referral_button:I = 0x7f0a0d34

.field public static final referral_glyph:I = 0x7f0a0d35

.field public static final referral_link:I = 0x7f0a0d36

.field public static final referral_message:I = 0x7f0a0d37

.field public static final referral_title:I = 0x7f0a0d38

.field public static final refund_amount_editor:I = 0x7f0a0d39

.field public static final refund_amount_help:I = 0x7f0a0d3a

.field public static final refund_amount_max:I = 0x7f0a0d3b

.field public static final refund_amount_remaining:I = 0x7f0a0d3c

.field public static final refund_cards_required:I = 0x7f0a0d3d

.field public static final refund_error_action_button:I = 0x7f0a0d3e

.field public static final refund_legal:I = 0x7f0a0d3f

.field public static final refund_past_deadline:I = 0x7f0a0d40

.field public static final refund_policy:I = 0x7f0a0d41

.field public static final refund_policy_popup_title:I = 0x7f0a0d42

.field public static final refund_policy_x_button:I = 0x7f0a0d43

.field public static final refund_tax_information:I = 0x7f0a0d44

.field public static final refund_title:I = 0x7f0a0d45

.field public static final refund_to_gc_button:I = 0x7f0a0d46

.field public static final refund_to_gc_title:I = 0x7f0a0d47

.field public static final refund_to_gift_card_helper_text:I = 0x7f0a0d48

.field public static final refund_total:I = 0x7f0a0d49

.field public static final refund_type_radio_group:I = 0x7f0a0d4a

.field public static final refunded_items:I = 0x7f0a0d4b

.field public static final refunded_items_container:I = 0x7f0a0d4c

.field public static final refunded_items_title:I = 0x7f0a0d4d

.field public static final refunds_container:I = 0x7f0a0d4e

.field public static final regular:I = 0x7f0a0d4f

.field public static final related_bill_container:I = 0x7f0a0d50

.field public static final related_bill_employee:I = 0x7f0a0d51

.field public static final related_bill_timestamp:I = 0x7f0a0d52

.field public static final relativeLayout1:I = 0x7f0a0d53

.field public static final remaining_balance:I = 0x7f0a0d54

.field public static final remember_this_device:I = 0x7f0a0d55

.field public static final reminder_date:I = 0x7f0a0d56

.field public static final reminder_time:I = 0x7f0a0d57

.field public static final remove_attachment_button:I = 0x7f0a0d58

.field public static final remove_payment_request:I = 0x7f0a0d59

.field public static final remove_payment_request_button:I = 0x7f0a0d5a

.field public static final remove_printer_station:I = 0x7f0a0d5b

.field public static final rename_edit_text:I = 0x7f0a0d5c

.field public static final rename_item:I = 0x7f0a0d5d

.field public static final repeat_every:I = 0x7f0a0d5e

.field public static final replyBoxLabelLayout:I = 0x7f0a0d5f

.field public static final replyBoxViewStub:I = 0x7f0a0d60

.field public static final replyFieldLabel:I = 0x7f0a0d61

.field public static final report_config_employee_filter_group:I = 0x7f0a0d62

.field public static final report_config_employee_filter_progress_bar:I = 0x7f0a0d63

.field public static final report_config_employee_filter_view:I = 0x7f0a0d64

.field public static final report_config_employee_filter_view_animator:I = 0x7f0a0d65

.field public static final report_config_employee_row:I = 0x7f0a0d66

.field public static final report_config_sheet_view:I = 0x7f0a0d67

.field public static final report_content_frame:I = 0x7f0a0d68

.field public static final report_issue:I = 0x7f0a0d69

.field public static final reprint_ticket_button:I = 0x7f0a0d6a

.field public static final request_deposit_toggle:I = 0x7f0a0d6b

.field public static final request_type_radio_group:I = 0x7f0a0d6c

.field public static final resend_email_button:I = 0x7f0a0d6d

.field public static final reset_card_pin:I = 0x7f0a0d6e

.field public static final reset_card_reason:I = 0x7f0a0d6f

.field public static final resolution_accepted_button:I = 0x7f0a0d70

.field public static final resolution_question_text:I = 0x7f0a0d71

.field public static final resolution_rejected_button:I = 0x7f0a0d72

.field public static final responsive_scroll_view:I = 0x7f0a0d73

.field public static final restart_button:I = 0x7f0a0d74

.field public static final restockable_items:I = 0x7f0a0d75

.field public static final retry_button:I = 0x7f0a0d76

.field public static final review_request_button:I = 0x7f0a0d77

.field public static final review_request_date:I = 0x7f0a0d78

.field public static final review_request_message:I = 0x7f0a0d79

.field public static final review_request_message_container:I = 0x7f0a0d7a

.field public static final review_variations_to_delete_recycler:I = 0x7f0a0d7b

.field public static final reward_row:I = 0x7f0a0d7c

.field public static final reward_row_action:I = 0x7f0a0d7d

.field public static final reward_row_action_layout:I = 0x7f0a0d7e

.field public static final reward_row_action_text:I = 0x7f0a0d7f

.field public static final reward_row_counter_view:I = 0x7f0a0d80

.field public static final reward_row_icon:I = 0x7f0a0d81

.field public static final reward_row_points_circle:I = 0x7f0a0d82

.field public static final reward_row_subtitle:I = 0x7f0a0d83

.field public static final reward_row_title:I = 0x7f0a0d84

.field public static final reward_tier_container:I = 0x7f0a0d85

.field public static final reward_tiers_brand_image:I = 0x7f0a0d86

.field public static final rewards_empty_label:I = 0x7f0a0d87

.field public static final rewards_points_brand_image:I = 0x7f0a0d88

.field public static final rewards_section_bottom_button:I = 0x7f0a0d89

.field public static final rewards_section_coupon_rows:I = 0x7f0a0d8a

.field public static final rewards_section_header:I = 0x7f0a0d8b

.field public static final ribbon_image:I = 0x7f0a0d8c

.field public static final right:I = 0x7f0a0d8d

.field public static final right_barrier:I = 0x7f0a0d8e

.field public static final right_icon:I = 0x7f0a0d8f

.field public static final right_keypad_container:I = 0x7f0a0d90

.field public static final right_margin_guide:I = 0x7f0a0d91

.field public static final right_side:I = 0x7f0a0d92

.field public static final rl:I = 0x7f0a0d93

.field public static final root:I = 0x7f0a0d94

.field public static final root_body_container:I = 0x7f0a0d95

.field public static final root_card_container:I = 0x7f0a0d96

.field public static final root_card_over_sheet_container:I = 0x7f0a0d97

.field public static final root_fullsheet_container:I = 0x7f0a0d98

.field public static final root_master_container:I = 0x7f0a0d99

.field public static final root_message_bars:I = 0x7f0a0d9a

.field public static final root_view:I = 0x7f0a0d9b

.field public static final rounded:I = 0x7f0a0d9c

.field public static final routing_number_field:I = 0x7f0a0d9d

.field public static final row:I = 0x7f0a0d9e

.field public static final rowKey:I = 0x7f0a0d9f

.field public static final rowValue:I = 0x7f0a0da0

.field public static final row_container:I = 0x7f0a0da1

.field public static final sale_frame:I = 0x7f0a0da2

.field public static final sale_frame_animating_content:I = 0x7f0a0da3

.field public static final sales_category_caret:I = 0x7f0a0da4

.field public static final sales_category_name:I = 0x7f0a0da5

.field public static final sales_category_price:I = 0x7f0a0da6

.field public static final sales_category_quantity:I = 0x7f0a0da7

.field public static final sales_category_subrow_name:I = 0x7f0a0da8

.field public static final sales_category_subrow_price:I = 0x7f0a0da9

.field public static final sales_category_subrow_quantity:I = 0x7f0a0daa

.field public static final sales_chart:I = 0x7f0a0dab

.field public static final sales_chart_header:I = 0x7f0a0dac

.field public static final sales_chart_highlight:I = 0x7f0a0dad

.field public static final sales_details_caret:I = 0x7f0a0dae

.field public static final sales_details_row_name:I = 0x7f0a0daf

.field public static final sales_details_row_sub_value:I = 0x7f0a0db0

.field public static final sales_details_row_two_col_name:I = 0x7f0a0db1

.field public static final sales_details_row_two_col_value:I = 0x7f0a0db2

.field public static final sales_details_row_value:I = 0x7f0a0db3

.field public static final sales_discount_name:I = 0x7f0a0db4

.field public static final sales_discount_price:I = 0x7f0a0db5

.field public static final sales_discount_quantity:I = 0x7f0a0db6

.field public static final sales_history_view:I = 0x7f0a0db7

.field public static final sales_report_animator:I = 0x7f0a0db8

.field public static final sales_report_chart_animator:I = 0x7f0a0db9

.field public static final sales_report_chart_view:I = 0x7f0a0dba

.field public static final sales_report_container:I = 0x7f0a0dbb

.field public static final sales_report_customize_date_range:I = 0x7f0a0dbc

.field public static final sales_report_details_empty_message_view:I = 0x7f0a0dbd

.field public static final sales_report_details_failure_message_view:I = 0x7f0a0dbe

.field public static final sales_report_error_state:I = 0x7f0a0dbf

.field public static final sales_report_fee_amount:I = 0x7f0a0dc0

.field public static final sales_report_fee_name:I = 0x7f0a0dc1

.field public static final sales_report_no_transactions_for_time_period:I = 0x7f0a0dc2

.field public static final sales_report_overview_average_sales:I = 0x7f0a0dc3

.field public static final sales_report_overview_cover_count:I = 0x7f0a0dc4

.field public static final sales_report_overview_cover_count_holder:I = 0x7f0a0dc5

.field public static final sales_report_overview_cover_count_subtext:I = 0x7f0a0dc6

.field public static final sales_report_overview_gross_sales:I = 0x7f0a0dc7

.field public static final sales_report_overview_sales_count:I = 0x7f0a0dc8

.field public static final sales_report_payment_method_bar:I = 0x7f0a0dc9

.field public static final sales_report_payment_method_empty_bar:I = 0x7f0a0dca

.field public static final sales_report_payment_method_name:I = 0x7f0a0dcb

.field public static final sales_report_payment_method_percentage:I = 0x7f0a0dcc

.field public static final sales_report_payment_method_sales:I = 0x7f0a0dcd

.field public static final sales_report_payment_method_total_name:I = 0x7f0a0dce

.field public static final sales_report_payment_method_total_sales:I = 0x7f0a0dcf

.field public static final sales_report_progress_bar:I = 0x7f0a0dd0

.field public static final sales_report_recycler_view:I = 0x7f0a0dd1

.field public static final sales_report_row:I = 0x7f0a0dd2

.field public static final sales_report_section_header_button:I = 0x7f0a0dd3

.field public static final sales_report_section_header_title:I = 0x7f0a0dd4

.field public static final sales_report_section_header_with_image_button:I = 0x7f0a0dd5

.field public static final sales_report_section_header_with_image_title:I = 0x7f0a0dd6

.field public static final sales_report_subsection_header_label_1:I = 0x7f0a0dd7

.field public static final sales_report_subsection_header_label_2:I = 0x7f0a0dd8

.field public static final sales_report_subsection_header_label_3:I = 0x7f0a0dd9

.field public static final sales_report_tab_1:I = 0x7f0a0dda

.field public static final sales_report_tab_2:I = 0x7f0a0ddb

.field public static final sales_report_time_selector_1d:I = 0x7f0a0ddc

.field public static final sales_report_time_selector_1m:I = 0x7f0a0ddd

.field public static final sales_report_time_selector_1w:I = 0x7f0a0dde

.field public static final sales_report_time_selector_1y:I = 0x7f0a0ddf

.field public static final sales_report_time_selector_3m:I = 0x7f0a0de0

.field public static final sales_report_time_selector_container:I = 0x7f0a0de1

.field public static final sales_report_top_bar_compare_icon:I = 0x7f0a0de2

.field public static final sales_report_top_bar_compare_icon_spacer:I = 0x7f0a0de3

.field public static final sales_report_top_bar_container:I = 0x7f0a0de4

.field public static final sales_report_top_bar_customize_icon:I = 0x7f0a0de5

.field public static final sales_report_top_bar_subtitle:I = 0x7f0a0de6

.field public static final sales_report_top_bar_title:I = 0x7f0a0de7

.field public static final sales_report_top_bar_title_container:I = 0x7f0a0de8

.field public static final sales_report_two_col_payment_method_bar:I = 0x7f0a0de9

.field public static final sales_report_two_col_payment_method_empty_bar:I = 0x7f0a0dea

.field public static final sales_report_two_col_payment_method_name:I = 0x7f0a0deb

.field public static final sales_report_two_col_payment_method_sales:I = 0x7f0a0dec

.field public static final sales_report_v2:I = 0x7f0a0ded

.field public static final sales_report_v2_barrier_between_row_one_and_two:I = 0x7f0a0dee

.field public static final sales_report_v2_barrier_between_row_two_and_three:I = 0x7f0a0def

.field public static final sales_report_v2_bottom_barrier:I = 0x7f0a0df0

.field public static final sales_report_v2_bottom_line:I = 0x7f0a0df1

.field public static final sales_report_v2_line_between_row_one_and_two:I = 0x7f0a0df2

.field public static final sales_report_v2_line_between_row_two_and_three:I = 0x7f0a0df3

.field public static final sales_report_v2_middle_barrier:I = 0x7f0a0df4

.field public static final sales_report_v2_middle_line:I = 0x7f0a0df5

.field public static final sales_report_v2_overview_average_sale:I = 0x7f0a0df6

.field public static final sales_report_v2_overview_discount:I = 0x7f0a0df7

.field public static final sales_report_v2_overview_gross_sales:I = 0x7f0a0df8

.field public static final sales_report_v2_overview_net_sales:I = 0x7f0a0df9

.field public static final sales_report_v2_overview_number_of_sales:I = 0x7f0a0dfa

.field public static final sales_report_v2_overview_refunds:I = 0x7f0a0dfb

.field public static final same_day_button:I = 0x7f0a0dfc

.field public static final same_day_deposit_description:I = 0x7f0a0dfd

.field public static final sample_image_tile:I = 0x7f0a0dfe

.field public static final sample_text_tile:I = 0x7f0a0dff

.field public static final save_button:I = 0x7f0a0e00

.field public static final save_default_message:I = 0x7f0a0e01

.field public static final save_link_screen_container:I = 0x7f0a0e02

.field public static final save_message_as_default_toggle:I = 0x7f0a0e03

.field public static final save_non_transition_alpha:I = 0x7f0a0e04

.field public static final save_overlay_view:I = 0x7f0a0e05

.field public static final save_spinner_container:I = 0x7f0a0e06

.field public static final savings:I = 0x7f0a0e07

.field public static final scale:I = 0x7f0a0e08

.field public static final schedule_section:I = 0x7f0a0e09

.field public static final screen:I = 0x7f0a0e0a

.field public static final screen_container:I = 0x7f0a0e0b

.field public static final screenshot_loading_indicator:I = 0x7f0a0e0c

.field public static final screenshot_preview:I = 0x7f0a0e0d

.field public static final screenshot_view_container:I = 0x7f0a0e0e

.field public static final scroll:I = 0x7f0a0e0f

.field public static final scrollIndicatorDown:I = 0x7f0a0e10

.field public static final scrollIndicatorUp:I = 0x7f0a0e11

.field public static final scrollView:I = 0x7f0a0e12

.field public static final scroll_content:I = 0x7f0a0e13

.field public static final scroll_indicator:I = 0x7f0a0e14

.field public static final scroll_jump_button:I = 0x7f0a0e15

.field public static final scroll_view:I = 0x7f0a0e16

.field public static final scrollable:I = 0x7f0a0e17

.field public static final scrubber:I = 0x7f0a0e18

.field public static final search_badge:I = 0x7f0a0e19

.field public static final search_bar:I = 0x7f0a0e1a

.field public static final search_button:I = 0x7f0a0e1b

.field public static final search_close_btn:I = 0x7f0a0e1c

.field public static final search_edit_frame:I = 0x7f0a0e1d

.field public static final search_field:I = 0x7f0a0e1e

.field public static final search_go_btn:I = 0x7f0a0e1f

.field public static final search_list:I = 0x7f0a0e20

.field public static final search_list_footer_divider:I = 0x7f0a0e21

.field public static final search_mag_icon:I = 0x7f0a0e22

.field public static final search_plate:I = 0x7f0a0e23

.field public static final search_result:I = 0x7f0a0e24

.field public static final search_result_message:I = 0x7f0a0e25

.field public static final search_src_text:I = 0x7f0a0e26

.field public static final search_voice_btn:I = 0x7f0a0e27

.field public static final second:I = 0x7f0a0e28

.field public static final second_bank_account:I = 0x7f0a0e29

.field public static final second_button:I = 0x7f0a0e2a

.field public static final second_row_container:I = 0x7f0a0e2b

.field public static final secondary:I = 0x7f0a0e2c

.field public static final secondary_action_container:I = 0x7f0a0e2d

.field public static final secondary_button:I = 0x7f0a0e2e

.field public static final secondary_container:I = 0x7f0a0e2f

.field public static final secondary_header:I = 0x7f0a0e30

.field public static final secondary_institution_number:I = 0x7f0a0e31

.field public static final secondary_institution_number_field:I = 0x7f0a0e32

.field public static final secondary_value:I = 0x7f0a0e33

.field public static final section_button_subtitle:I = 0x7f0a0e34

.field public static final section_button_text:I = 0x7f0a0e35

.field public static final section_checkablegroup:I = 0x7f0a0e36

.field public static final section_container:I = 0x7f0a0e37

.field public static final section_header:I = 0x7f0a0e38

.field public static final section_header_row:I = 0x7f0a0e39

.field public static final section_list:I = 0x7f0a0e3a

.field public static final section_pager:I = 0x7f0a0e3b

.field public static final section_title:I = 0x7f0a0e3c

.field public static final secure_touch_accessible_layout:I = 0x7f0a0e3d

.field public static final select_address_1:I = 0x7f0a0e3e

.field public static final select_address_2:I = 0x7f0a0e3f

.field public static final select_address_order_button:I = 0x7f0a0e40

.field public static final select_dialog_listview:I = 0x7f0a0e41

.field public static final select_gift_receipt_tender_view:I = 0x7f0a0e42

.field public static final select_method_transaction_total:I = 0x7f0a0e43

.field public static final select_option_value_recycler:I = 0x7f0a0e44

.field public static final select_option_values_add_value:I = 0x7f0a0e45

.field public static final select_option_values_for_variation_recycler:I = 0x7f0a0e46

.field public static final select_option_values_for_variation_view:I = 0x7f0a0e47

.field public static final select_option_values_view:I = 0x7f0a0e48

.field public static final select_options_recycler:I = 0x7f0a0e49

.field public static final select_options_view:I = 0x7f0a0e4a

.field public static final select_payment_container:I = 0x7f0a0e4b

.field public static final select_payment_up_button:I = 0x7f0a0e4c

.field public static final select_question_view:I = 0x7f0a0e4d

.field public static final select_receipt_tender_view:I = 0x7f0a0e4e

.field public static final select_refund_tenders_container:I = 0x7f0a0e4f

.field public static final select_single_value_for_variation_recycler:I = 0x7f0a0e50

.field public static final select_single_value_for_variation_view:I = 0x7f0a0e51

.field public static final select_variations_to_create_recycler:I = 0x7f0a0e52

.field public static final select_variations_to_create_view:I = 0x7f0a0e53

.field public static final selectable_color:I = 0x7f0a0e54

.field public static final selectable_label:I = 0x7f0a0e55

.field public static final selectable_option_skip:I = 0x7f0a0e56

.field public static final selectable_option_text:I = 0x7f0a0e57

.field public static final selectable_options_container:I = 0x7f0a0e58

.field public static final selectable_unit_list:I = 0x7f0a0e59

.field public static final selectable_value:I = 0x7f0a0e5a

.field public static final selected:I = 0x7f0a0e5b

.field public static final seller_agreement_row:I = 0x7f0a0e5c

.field public static final seller_cart_footer_banner:I = 0x7f0a0e5d

.field public static final seller_cart_footer_banner_text:I = 0x7f0a0e5e

.field public static final seller_cash_received_continue:I = 0x7f0a0e5f

.field public static final seller_cash_received_subtitle:I = 0x7f0a0e60

.field public static final seller_cash_received_title:I = 0x7f0a0e61

.field public static final send_anyway_button:I = 0x7f0a0e62

.field public static final send_button:I = 0x7f0a0e63

.field public static final send_buyer_loyalty_status_button:I = 0x7f0a0e64

.field public static final send_diagnostic_row:I = 0x7f0a0e65

.field public static final send_email_button:I = 0x7f0a0e66

.field public static final send_email_container:I = 0x7f0a0e67

.field public static final send_receipt_button:I = 0x7f0a0e68

.field public static final set_default_message_helper:I = 0x7f0a0e69

.field public static final settings_applet_section_id_for_state_restoration:I = 0x7f0a0e6a

.field public static final settings_description:I = 0x7f0a0e6b

.field public static final settings_section_banner:I = 0x7f0a0e6c

.field public static final setup_guide_dialog_icon:I = 0x7f0a0e6d

.field public static final setup_guide_dialog_message:I = 0x7f0a0e6e

.field public static final setup_guide_dialog_primary:I = 0x7f0a0e6f

.field public static final setup_guide_dialog_secondary:I = 0x7f0a0e70

.field public static final setup_guide_dialog_title:I = 0x7f0a0e71

.field public static final setup_guide_intro_card:I = 0x7f0a0e72

.field public static final shadow_generator:I = 0x7f0a0e73

.field public static final share_checkoutlink:I = 0x7f0a0e74

.field public static final share_link_email_sent_helper:I = 0x7f0a0e75

.field public static final shared_item_message:I = 0x7f0a0e76

.field public static final shared_setting_message_view:I = 0x7f0a0e77

.field public static final sheet:I = 0x7f0a0e78

.field public static final sheet_payment_flow:I = 0x7f0a0e79

.field public static final shift_description:I = 0x7f0a0e7a

.field public static final shipping_address:I = 0x7f0a0e7b

.field public static final shipping_name:I = 0x7f0a0e7c

.field public static final shortcut:I = 0x7f0a0e7d

.field public static final showCustom:I = 0x7f0a0e7e

.field public static final showHome:I = 0x7f0a0e7f

.field public static final showTitle:I = 0x7f0a0e80

.field public static final show_result_view:I = 0x7f0a0e81

.field public static final sign_clear_button:I = 0x7f0a0e82

.field public static final sign_done_button:I = 0x7f0a0e83

.field public static final sign_in:I = 0x7f0a0e84

.field public static final sign_line:I = 0x7f0a0e85

.field public static final sign_line_x:I = 0x7f0a0e86

.field public static final sign_on_device:I = 0x7f0a0e87

.field public static final sign_on_device_hint:I = 0x7f0a0e88

.field public static final sign_on_printed_receipt:I = 0x7f0a0e89

.field public static final sign_out_button:I = 0x7f0a0e8a

.field public static final sign_skip_under_amount:I = 0x7f0a0e8b

.field public static final sign_skip_under_amount_tooltip:I = 0x7f0a0e8c

.field public static final sign_x:I = 0x7f0a0e8d

.field public static final signature_available_options:I = 0x7f0a0e8e

.field public static final signature_buttons:I = 0x7f0a0e8f

.field public static final signature_canvas:I = 0x7f0a0e90

.field public static final signature_hint:I = 0x7f0a0e91

.field public static final signature_on_card:I = 0x7f0a0e92

.field public static final signature_outline:I = 0x7f0a0e93

.field public static final signature_pad:I = 0x7f0a0e94

.field public static final signature_view:I = 0x7f0a0e95

.field public static final simpleIcon:I = 0x7f0a0e96

.field public static final single_picker_view_picker:I = 0x7f0a0e97

.field public static final single_question_container:I = 0x7f0a0e98

.field public static final skipBubbleTextView:I = 0x7f0a0e99

.field public static final skipCollapsed:I = 0x7f0a0e9a

.field public static final skipOuterBubble:I = 0x7f0a0e9b

.field public static final skip_enrollment_link:I = 0x7f0a0e9c

.field public static final skip_itemized_cart:I = 0x7f0a0e9d

.field public static final skip_itemized_cart_hint:I = 0x7f0a0e9e

.field public static final skip_payment_type_hint:I = 0x7f0a0e9f

.field public static final skip_payment_type_selection:I = 0x7f0a0ea0

.field public static final skip_receipt_divider:I = 0x7f0a0ea1

.field public static final skip_receipt_screen:I = 0x7f0a0ea2

.field public static final skip_receipt_screen_hint:I = 0x7f0a0ea3

.field public static final sku_not_found_glyph:I = 0x7f0a0ea4

.field public static final sku_not_found_ok_button:I = 0x7f0a0ea5

.field public static final sku_not_found_title:I = 0x7f0a0ea6

.field public static final slide:I = 0x7f0a0ea7

.field public static final small:I = 0x7f0a0ea8

.field public static final smallLabel:I = 0x7f0a0ea9

.field public static final sms_button:I = 0x7f0a0eaa

.field public static final sms_container:I = 0x7f0a0eab

.field public static final sms_disclaimer:I = 0x7f0a0eac

.field public static final sms_marketing_accept_button:I = 0x7f0a0ead

.field public static final sms_marketing_decline_button:I = 0x7f0a0eae

.field public static final sms_marketing_disclaimer:I = 0x7f0a0eaf

.field public static final sms_marketing_title:I = 0x7f0a0eb0

.field public static final sms_receipt_field:I = 0x7f0a0eb1

.field public static final sms_send_button:I = 0x7f0a0eb2

.field public static final snackbar_action:I = 0x7f0a0eb3

.field public static final snackbar_text:I = 0x7f0a0eb4

.field public static final snap:I = 0x7f0a0eb5

.field public static final snapMargins:I = 0x7f0a0eb6

.field public static final sort_code:I = 0x7f0a0eb7

.field public static final spacer:I = 0x7f0a0eb8

.field public static final speed_test_primary_button:I = 0x7f0a0eb9

.field public static final speed_test_secondary_button:I = 0x7f0a0eba

.field public static final speedtest_up_button:I = 0x7f0a0ebb

.field public static final spinner:I = 0x7f0a0ebc

.field public static final spinner_glyph:I = 0x7f0a0ebd

.field public static final spinner_item_selected:I = 0x7f0a0ebe

.field public static final spinner_message:I = 0x7f0a0ebf

.field public static final spinner_progress_bar:I = 0x7f0a0ec0

.field public static final spinner_title:I = 0x7f0a0ec1

.field public static final splash_image_accept_payments:I = 0x7f0a0ec2

.field public static final splash_image_build_trust:I = 0x7f0a0ec3

.field public static final splash_image_reports:I = 0x7f0a0ec4

.field public static final splash_image_sell_in_minutes:I = 0x7f0a0ec5

.field public static final splash_image_square_logo:I = 0x7f0a0ec6

.field public static final splash_page_icon:I = 0x7f0a0ec7

.field public static final splash_page_image:I = 0x7f0a0ec8

.field public static final splash_page_text:I = 0x7f0a0ec9

.field public static final splash_pager:I = 0x7f0a0eca

.field public static final splash_screen_gradient:I = 0x7f0a0ecb

.field public static final splash_view:I = 0x7f0a0ecc

.field public static final split_action_bar:I = 0x7f0a0ecd

.field public static final split_balance_toggle:I = 0x7f0a0ece

.field public static final split_tender_amount:I = 0x7f0a0ecf

.field public static final split_tender_amount_recycler_view:I = 0x7f0a0ed0

.field public static final split_tender_amount_subtitle:I = 0x7f0a0ed1

.field public static final split_tender_button:I = 0x7f0a0ed2

.field public static final split_tender_card_on_file_tender_list:I = 0x7f0a0ed3

.field public static final split_tender_cash_tender_list:I = 0x7f0a0ed4

.field public static final split_tender_completed_payments_cancel_button:I = 0x7f0a0ed5

.field public static final split_tender_completed_payments_line_row:I = 0x7f0a0ed6

.field public static final split_tender_completed_payments_title:I = 0x7f0a0ed7

.field public static final split_tender_confirm_button:I = 0x7f0a0ed8

.field public static final split_tender_contactless_tender_list:I = 0x7f0a0ed9

.field public static final split_tender_credit_card_tender_list:I = 0x7f0a0eda

.field public static final split_tender_custom_split:I = 0x7f0a0edb

.field public static final split_tender_even_split_custom:I = 0x7f0a0edc

.field public static final split_tender_even_split_four:I = 0x7f0a0edd

.field public static final split_tender_even_split_three:I = 0x7f0a0ede

.field public static final split_tender_even_split_two:I = 0x7f0a0edf

.field public static final split_tender_gift_card_tender_list:I = 0x7f0a0ee0

.field public static final split_tender_other_tender_list:I = 0x7f0a0ee1

.field public static final split_tender_third_party_card_tender_list:I = 0x7f0a0ee2

.field public static final split_tender_transaction_total:I = 0x7f0a0ee3

.field public static final split_ticket_check_box:I = 0x7f0a0ee4

.field public static final split_ticket_container:I = 0x7f0a0ee5

.field public static final split_ticket_drop_down_caret:I = 0x7f0a0ee6

.field public static final split_ticket_drop_down_container:I = 0x7f0a0ee7

.field public static final split_ticket_footer_button:I = 0x7f0a0ee8

.field public static final split_ticket_header:I = 0x7f0a0ee9

.field public static final split_ticket_item:I = 0x7f0a0eea

.field public static final split_ticket_item_row:I = 0x7f0a0eeb

.field public static final split_ticket_menu_add_customer:I = 0x7f0a0eec

.field public static final split_ticket_menu_edit_ticket:I = 0x7f0a0eed

.field public static final split_ticket_menu_print_bill:I = 0x7f0a0eee

.field public static final split_ticket_menu_view:I = 0x7f0a0eef

.field public static final split_ticket_menu_view_customer:I = 0x7f0a0ef0

.field public static final split_ticket_recycler_view:I = 0x7f0a0ef1

.field public static final split_ticket_scroll_view:I = 0x7f0a0ef2

.field public static final split_ticket_tax_total:I = 0x7f0a0ef3

.field public static final split_ticket_ticket_name:I = 0x7f0a0ef4

.field public static final spoc_version_row:I = 0x7f0a0ef5

.field public static final spread:I = 0x7f0a0ef6

.field public static final spread_inside:I = 0x7f0a0ef7

.field public static final square_card:I = 0x7f0a0ef8

.field public static final square_card_activation_code_confirmation_continue_button:I = 0x7f0a0ef9

.field public static final square_card_activation_code_confirmation_help:I = 0x7f0a0efa

.field public static final square_card_activation_code_confirmation_input:I = 0x7f0a0efb

.field public static final square_card_activation_code_help_message:I = 0x7f0a0efc

.field public static final square_card_activation_complete_continue_button:I = 0x7f0a0efd

.field public static final square_card_activation_complete_message:I = 0x7f0a0efe

.field public static final square_card_activation_create_pin_message:I = 0x7f0a0eff

.field public static final square_card_activation_pin_confirmation_entry:I = 0x7f0a0f00

.field public static final square_card_activation_pin_entry:I = 0x7f0a0f01

.field public static final square_card_activation_submit_pin:I = 0x7f0a0f02

.field public static final square_card_confirm_address:I = 0x7f0a0f03

.field public static final square_card_confirm_address_button:I = 0x7f0a0f04

.field public static final square_card_confirm_address_message:I = 0x7f0a0f05

.field public static final square_card_confirmation_card_confirmation_continue_button:I = 0x7f0a0f06

.field public static final square_card_enter_phone_number:I = 0x7f0a0f07

.field public static final square_card_enter_phone_number_submit:I = 0x7f0a0f08

.field public static final square_card_instant_deposit_confirm:I = 0x7f0a0f09

.field public static final square_card_instant_deposit_help:I = 0x7f0a0f0a

.field public static final square_card_instant_deposit_result:I = 0x7f0a0f0b

.field public static final square_card_instant_deposit_result_container:I = 0x7f0a0f0c

.field public static final square_card_instant_deposit_spinner_holder:I = 0x7f0a0f0d

.field public static final square_card_instant_deposit_upsell:I = 0x7f0a0f0e

.field public static final square_card_ordered_deposits_info:I = 0x7f0a0f0f

.field public static final square_card_ordered_deposits_info_continue:I = 0x7f0a0f10

.field public static final square_card_primary_action_button:I = 0x7f0a0f11

.field public static final square_card_progress_result:I = 0x7f0a0f12

.field public static final square_card_progress_spinner_holder:I = 0x7f0a0f13

.field public static final square_card_progress_spinner_message:I = 0x7f0a0f14

.field public static final square_card_upsell_message:I = 0x7f0a0f15

.field public static final square_card_upsell_preview:I = 0x7f0a0f16

.field public static final square_card_upsell_title:I = 0x7f0a0f17

.field public static final square_card_validation_entry_view:I = 0x7f0a0f18

.field public static final src_atop:I = 0x7f0a0f19

.field public static final src_in:I = 0x7f0a0f1a

.field public static final src_over:I = 0x7f0a0f1b

.field public static final stable_action_bar:I = 0x7f0a0f1c

.field public static final stamp_view:I = 0x7f0a0f1d

.field public static final stamps_loading_view:I = 0x7f0a0f1e

.field public static final stamps_mode:I = 0x7f0a0f1f

.field public static final stamps_recycler_view:I = 0x7f0a0f20

.field public static final stamps_selected_indicator:I = 0x7f0a0f21

.field public static final standard:I = 0x7f0a0f22

.field public static final standard_units_list:I = 0x7f0a0f23

.field public static final standard_units_list_create_custom_unit_button:I = 0x7f0a0f24

.field public static final standard_units_list_no_search_result_help_text:I = 0x7f0a0f25

.field public static final standard_units_list_search_bar:I = 0x7f0a0f26

.field public static final standard_units_list_view:I = 0x7f0a0f27

.field public static final star_group:I = 0x7f0a0f28

.field public static final stars:I = 0x7f0a0f29

.field public static final start:I = 0x7f0a0f2a

.field public static final start_break_or_clockout_notes_button:I = 0x7f0a0f2b

.field public static final start_drawer_button:I = 0x7f0a0f2c

.field public static final start_drawer_container:I = 0x7f0a0f2d

.field public static final start_drawer_starting_cash:I = 0x7f0a0f2e

.field public static final start_of_drawer:I = 0x7f0a0f2f

.field public static final starting_balance:I = 0x7f0a0f30

.field public static final starting_cash:I = 0x7f0a0f31

.field public static final state:I = 0x7f0a0f32

.field public static final status:I = 0x7f0a0f33

.field public static final status_bar_latest_event_content:I = 0x7f0a0f34

.field public static final status_bar_overlay:I = 0x7f0a0f35

.field public static final status_container:I = 0x7f0a0f36

.field public static final status_description:I = 0x7f0a0f37

.field public static final status_message:I = 0x7f0a0f38

.field public static final status_title:I = 0x7f0a0f39

.field public static final sticky_bar:I = 0x7f0a0f3a

.field public static final stock_count_row_label:I = 0x7f0a0f3b

.field public static final stock_count_row_progress_spinner:I = 0x7f0a0f3c

.field public static final stock_count_row_stock_field:I = 0x7f0a0f3d

.field public static final stock_count_row_view_stock_count_field:I = 0x7f0a0f3e

.field public static final stolen_square_card_choice:I = 0x7f0a0f3f

.field public static final store_and_forward_section_view:I = 0x7f0a0f40

.field public static final street:I = 0x7f0a0f41

.field public static final stretch:I = 0x7f0a0f42

.field public static final subValue:I = 0x7f0a0f43

.field public static final submenuarrow:I = 0x7f0a0f44

.field public static final submit:I = 0x7f0a0f45

.field public static final submit_area:I = 0x7f0a0f46

.field public static final submit_button:I = 0x7f0a0f47

.field public static final subtext:I = 0x7f0a0f48

.field public static final subtitle:I = 0x7f0a0f49

.field public static final success_continue_button:I = 0x7f0a0f4a

.field public static final success_glyph_message:I = 0x7f0a0f4b

.field public static final success_notes_button:I = 0x7f0a0f4c

.field public static final success_view:I = 0x7f0a0f4d

.field public static final suggestionsListStub:I = 0x7f0a0f4e

.field public static final summary_title:I = 0x7f0a0f4f

.field public static final support_fragment_container:I = 0x7f0a0f50

.field public static final svg_view:I = 0x7f0a0f51

.field public static final swipe_insert_tap_prompt:I = 0x7f0a0f52

.field public static final swipe_only_warning:I = 0x7f0a0f53

.field public static final swipe_refresh:I = 0x7f0a0f54

.field public static final swiped_card:I = 0x7f0a0f55

.field public static final swiped_card_container:I = 0x7f0a0f56

.field public static final swiped_gift_card:I = 0x7f0a0f57

.field public static final switch_language_button:I = 0x7f0a0f58

.field public static final switch_type:I = 0x7f0a0f59

.field public static final system:I = 0x7f0a0f5a

.field public static final system_message:I = 0x7f0a0f5b

.field public static final t2_body:I = 0x7f0a0f5c

.field public static final t2_screen:I = 0x7f0a0f5d

.field public static final tabMode:I = 0x7f0a0f5e

.field public static final tab_item_all:I = 0x7f0a0f5f

.field public static final tab_item_card_spend:I = 0x7f0a0f60

.field public static final tab_item_transfers:I = 0x7f0a0f61

.field public static final tab_keypad:I = 0x7f0a0f62

.field public static final tab_library:I = 0x7f0a0f63

.field public static final tag_accessibility_actions:I = 0x7f0a0f64

.field public static final tag_accessibility_clickable_spans:I = 0x7f0a0f65

.field public static final tag_accessibility_heading:I = 0x7f0a0f66

.field public static final tag_accessibility_pane_title:I = 0x7f0a0f67

.field public static final tag_attached_disposables:I = 0x7f0a0f68

.field public static final tag_attached_subscriptions:I = 0x7f0a0f69

.field public static final tag_screen_reader_focusable:I = 0x7f0a0f6a

.field public static final tag_transition_group:I = 0x7f0a0f6b

.field public static final tag_unhandled_key_event_manager:I = 0x7f0a0f6c

.field public static final tag_unhandled_key_listeners:I = 0x7f0a0f6d

.field public static final take_photo:I = 0x7f0a0f6e

.field public static final tap_to_preview_container:I = 0x7f0a0f6f

.field public static final tax:I = 0x7f0a0f70

.field public static final tax_applicable_items_row:I = 0x7f0a0f71

.field public static final tax_applicable_list:I = 0x7f0a0f72

.field public static final tax_applicable_progress_bar:I = 0x7f0a0f73

.field public static final tax_applicable_services_row:I = 0x7f0a0f74

.field public static final tax_basis_help_text:I = 0x7f0a0f75

.field public static final tax_breakdown:I = 0x7f0a0f76

.field public static final tax_detail_content:I = 0x7f0a0f77

.field public static final tax_detail_progress_bar:I = 0x7f0a0f78

.field public static final tax_enabled_row:I = 0x7f0a0f79

.field public static final tax_fee_type_container:I = 0x7f0a0f7a

.field public static final tax_item_pricing_row:I = 0x7f0a0f7b

.field public static final tax_name_row:I = 0x7f0a0f7c

.field public static final tax_percentage_row:I = 0x7f0a0f7d

.field public static final tax_type_row:I = 0x7f0a0f7e

.field public static final taxes_container:I = 0x7f0a0f7f

.field public static final taxes_header:I = 0x7f0a0f80

.field public static final taxes_list_view:I = 0x7f0a0f81

.field public static final taxes_progress_bar:I = 0x7f0a0f82

.field public static final tender_completed_row:I = 0x7f0a0f83

.field public static final tender_employee:I = 0x7f0a0f84

.field public static final tender_employee_deprecated:I = 0x7f0a0f85

.field public static final tender_name:I = 0x7f0a0f86

.field public static final tender_section_tax_breakdown_container:I = 0x7f0a0f87

.field public static final tender_section_total_container:I = 0x7f0a0f88

.field public static final tender_timestamp:I = 0x7f0a0f89

.field public static final tender_type:I = 0x7f0a0f8a

.field public static final tenders:I = 0x7f0a0f8b

.field public static final tenders_container:I = 0x7f0a0f8c

.field public static final tertiary:I = 0x7f0a0f8d

.field public static final tertiary_container:I = 0x7f0a0f8e

.field public static final tertiary_header:I = 0x7f0a0f8f

.field public static final tertiary_value:I = 0x7f0a0f90

.field public static final test_checkbox_android_button_tint:I = 0x7f0a0f91

.field public static final test_checkbox_app_button_tint:I = 0x7f0a0f92

.field public static final test_print:I = 0x7f0a0f93

.field public static final text:I = 0x7f0a0f94

.field public static final text2:I = 0x7f0a0f95

.field public static final textEnd:I = 0x7f0a0f96

.field public static final textIcon:I = 0x7f0a0f97

.field public static final textSpacerNoButtons:I = 0x7f0a0f98

.field public static final textSpacerNoTitle:I = 0x7f0a0f99

.field public static final textStart:I = 0x7f0a0f9a

.field public static final textViewFailureMessage:I = 0x7f0a0f9b

.field public static final textViewLoadingText:I = 0x7f0a0f9c

.field public static final text_attribute:I = 0x7f0a0f9d

.field public static final text_container:I = 0x7f0a0f9e

.field public static final text_field:I = 0x7f0a0f9f

.field public static final text_input_end_icon:I = 0x7f0a0fa0

.field public static final text_input_start_icon:I = 0x7f0a0fa1

.field public static final text_tile:I = 0x7f0a0fa2

.field public static final text_tile_color:I = 0x7f0a0fa3

.field public static final text_tile_icon:I = 0x7f0a0fa4

.field public static final text_tile_name:I = 0x7f0a0fa5

.field public static final text_tile_radio:I = 0x7f0a0fa6

.field public static final textinput_counter:I = 0x7f0a0fa7

.field public static final textinput_error:I = 0x7f0a0fa8

.field public static final textinput_helper_text:I = 0x7f0a0fa9

.field public static final third_button:I = 0x7f0a0faa

.field public static final thumbnail:I = 0x7f0a0fab

.field public static final ticket_card_not_stored_hint:I = 0x7f0a0fac

.field public static final ticket_check_box:I = 0x7f0a0fad

.field public static final ticket_comp:I = 0x7f0a0fae

.field public static final ticket_convert_to_custom_ticket:I = 0x7f0a0faf

.field public static final ticket_count_row:I = 0x7f0a0fb0

.field public static final ticket_delete:I = 0x7f0a0fb1

.field public static final ticket_detail_view:I = 0x7f0a0fb2

.field public static final ticket_error_container:I = 0x7f0a0fb3

.field public static final ticket_error_message:I = 0x7f0a0fb4

.field public static final ticket_error_title:I = 0x7f0a0fb5

.field public static final ticket_list_no_tickets:I = 0x7f0a0fb6

.field public static final ticket_list_row:I = 0x7f0a0fb7

.field public static final ticket_list_text_row_text:I = 0x7f0a0fb8

.field public static final ticket_list_view:I = 0x7f0a0fb9

.field public static final ticket_list_view_progress:I = 0x7f0a0fba

.field public static final ticket_list_view_recycler_view:I = 0x7f0a0fbb

.field public static final ticket_list_view_search_bar:I = 0x7f0a0fbc

.field public static final ticket_name:I = 0x7f0a0fbd

.field public static final ticket_name_method:I = 0x7f0a0fbe

.field public static final ticket_name_options_container:I = 0x7f0a0fbf

.field public static final ticket_note:I = 0x7f0a0fc0

.field public static final ticket_row_amount:I = 0x7f0a0fc1

.field public static final ticket_row_block_employee:I = 0x7f0a0fc2

.field public static final ticket_row_employee:I = 0x7f0a0fc3

.field public static final ticket_row_name:I = 0x7f0a0fc4

.field public static final ticket_row_time:I = 0x7f0a0fc5

.field public static final ticket_row_time_with_employee:I = 0x7f0a0fc6

.field public static final ticket_sort_amount:I = 0x7f0a0fc7

.field public static final ticket_sort_employee:I = 0x7f0a0fc8

.field public static final ticket_sort_name:I = 0x7f0a0fc9

.field public static final ticket_sort_recent:I = 0x7f0a0fca

.field public static final ticket_template_delete:I = 0x7f0a0fcb

.field public static final ticket_template_drag_handle:I = 0x7f0a0fcc

.field public static final ticket_template_name:I = 0x7f0a0fcd

.field public static final ticket_template_search:I = 0x7f0a0fce

.field public static final ticket_transfer_employee_name:I = 0x7f0a0fcf

.field public static final ticket_transfer_employee_radiobox:I = 0x7f0a0fd0

.field public static final ticket_uncomp:I = 0x7f0a0fd1

.field public static final ticket_void:I = 0x7f0a0fd2

.field public static final tickets_button_confirm_overlay:I = 0x7f0a0fd3

.field public static final tickets_button_container:I = 0x7f0a0fd4

.field public static final tickets_button_subtitle:I = 0x7f0a0fd5

.field public static final tickets_button_title:I = 0x7f0a0fd6

.field public static final tickets_transfer_employee_progress:I = 0x7f0a0fd7

.field public static final tickets_transfer_employee_recycler_view:I = 0x7f0a0fd8

.field public static final tickets_transfer_employee_search:I = 0x7f0a0fd9

.field public static final tier_desc:I = 0x7f0a0fda

.field public static final tile_choice:I = 0x7f0a0fdb

.field public static final tile_divider:I = 0x7f0a0fdc

.field public static final tile_help_text:I = 0x7f0a0fdd

.field public static final time:I = 0x7f0a0fde

.field public static final timePicker:I = 0x7f0a0fdf

.field public static final time_picker:I = 0x7f0a0fe0

.field public static final time_picker_layout:I = 0x7f0a0fe1

.field public static final time_tracking_settings_action_bar:I = 0x7f0a0fe2

.field public static final time_tracking_settings_enable_switch:I = 0x7f0a0fe3

.field public static final time_tracking_settings_enable_switch_description:I = 0x7f0a0fe4

.field public static final time_zone_tag:I = 0x7f0a0fe5

.field public static final timecards_action_bar:I = 0x7f0a0fe6

.field public static final timecards_action_bar_primary_button:I = 0x7f0a0fe7

.field public static final timecards_add_or_edit_notes:I = 0x7f0a0fe8

.field public static final timecards_add_or_edit_notes_action_bar:I = 0x7f0a0fe9

.field public static final timecards_add_or_edit_notes_text_view:I = 0x7f0a0fea

.field public static final timecards_clocked_in_green_indicator:I = 0x7f0a0feb

.field public static final timecards_clockin_confirmation:I = 0x7f0a0fec

.field public static final timecards_clockin_confirmation_button:I = 0x7f0a0fed

.field public static final timecards_clockout_button:I = 0x7f0a0fee

.field public static final timecards_clockout_confirmation:I = 0x7f0a0fef

.field public static final timecards_clockout_confirmation_button:I = 0x7f0a0ff0

.field public static final timecards_clockout_summary:I = 0x7f0a0ff1

.field public static final timecards_clockout_summary_data_cell:I = 0x7f0a0ff2

.field public static final timecards_clockout_summary_header_cell:I = 0x7f0a0ff3

.field public static final timecards_clockout_summary_line_separator:I = 0x7f0a0ff4

.field public static final timecards_clockout_summary_placeholder:I = 0x7f0a0ff5

.field public static final timecards_clockout_summary_scroll_view:I = 0x7f0a0ff6

.field public static final timecards_clockout_summary_table:I = 0x7f0a0ff7

.field public static final timecards_container:I = 0x7f0a0ff8

.field public static final timecards_current_time:I = 0x7f0a0ff9

.field public static final timecards_end_break_confirmation:I = 0x7f0a0ffa

.field public static final timecards_end_break_confirmation_button:I = 0x7f0a0ffb

.field public static final timecards_error:I = 0x7f0a0ffc

.field public static final timecards_error_glyph:I = 0x7f0a0ffd

.field public static final timecards_error_message:I = 0x7f0a0ffe

.field public static final timecards_error_title:I = 0x7f0a0fff

.field public static final timecards_header:I = 0x7f0a1000

.field public static final timecards_header_container:I = 0x7f0a1001

.field public static final timecards_list_breaks:I = 0x7f0a1002

.field public static final timecards_list_breaks_button:I = 0x7f0a1003

.field public static final timecards_list_breaks_button_break_duration:I = 0x7f0a1004

.field public static final timecards_list_breaks_button_break_name:I = 0x7f0a1005

.field public static final timecards_loading_progress_bar:I = 0x7f0a1006

.field public static final timecards_modal:I = 0x7f0a1007

.field public static final timecards_modal_clock_glyph:I = 0x7f0a1008

.field public static final timecards_modal_failure:I = 0x7f0a1009

.field public static final timecards_modal_message:I = 0x7f0a100a

.field public static final timecards_modal_primary_button:I = 0x7f0a100b

.field public static final timecards_modal_progress_bar:I = 0x7f0a100c

.field public static final timecards_modal_secondary_button:I = 0x7f0a100d

.field public static final timecards_modal_success:I = 0x7f0a100e

.field public static final timecards_modal_title:I = 0x7f0a100f

.field public static final timecards_primary_button:I = 0x7f0a1010

.field public static final timecards_progress_bar:I = 0x7f0a1011

.field public static final timecards_progress_bar_spinner:I = 0x7f0a1012

.field public static final timecards_progress_bar_title:I = 0x7f0a1013

.field public static final timecards_secondary_button:I = 0x7f0a1014

.field public static final timecards_start_break_button:I = 0x7f0a1015

.field public static final timecards_start_break_or_clockout:I = 0x7f0a1016

.field public static final timecards_sub_header:I = 0x7f0a1017

.field public static final timecards_success:I = 0x7f0a1018

.field public static final timecards_success_button_container:I = 0x7f0a1019

.field public static final timecards_success_button_separator:I = 0x7f0a101a

.field public static final timecards_success_buttons_view:I = 0x7f0a101b

.field public static final timecards_success_buttons_view_summary:I = 0x7f0a101c

.field public static final timecards_success_container:I = 0x7f0a101d

.field public static final timecards_success_continue_button_modal:I = 0x7f0a101e

.field public static final timecards_success_glyph:I = 0x7f0a101f

.field public static final timecards_success_glyph_modal:I = 0x7f0a1020

.field public static final timecards_success_message:I = 0x7f0a1021

.field public static final timecards_success_message_modal:I = 0x7f0a1022

.field public static final timecards_success_modal:I = 0x7f0a1023

.field public static final timecards_success_title:I = 0x7f0a1024

.field public static final timecards_success_title_modal:I = 0x7f0a1025

.field public static final timecards_switch_jobs_button:I = 0x7f0a1026

.field public static final timecards_title_text:I = 0x7f0a1027

.field public static final timecards_title_text_primary:I = 0x7f0a1028

.field public static final timecards_title_text_secondary:I = 0x7f0a1029

.field public static final timecards_up_button:I = 0x7f0a102a

.field public static final timecards_up_button_container:I = 0x7f0a102b

.field public static final timecards_up_button_text:I = 0x7f0a102c

.field public static final timecards_view_notes:I = 0x7f0a102d

.field public static final timecards_view_notes_action_bar:I = 0x7f0a102e

.field public static final timecards_view_notes_container:I = 0x7f0a102f

.field public static final timeline_content:I = 0x7f0a1030

.field public static final tip_amount_editor:I = 0x7f0a1031

.field public static final tip_amount_first:I = 0x7f0a1032

.field public static final tip_amount_second:I = 0x7f0a1033

.field public static final tip_amount_third:I = 0x7f0a1034

.field public static final tip_animator:I = 0x7f0a1035

.field public static final tip_animator_container:I = 0x7f0a1036

.field public static final tip_bar:I = 0x7f0a1037

.field public static final tip_option_custom:I = 0x7f0a1038

.field public static final tip_options_view:I = 0x7f0a1039

.field public static final tip_post_taxes_toggle:I = 0x7f0a103a

.field public static final tip_pre_taxes_toggle:I = 0x7f0a103b

.field public static final tip_separate_screen_row:I = 0x7f0a103c

.field public static final tip_smart_row:I = 0x7f0a103d

.field public static final tips_collect_row:I = 0x7f0a103e

.field public static final title:I = 0x7f0a103f

.field public static final titleDivider:I = 0x7f0a1040

.field public static final titleDividerNoCustom:I = 0x7f0a1041

.field public static final titleDividerTop:I = 0x7f0a1042

.field public static final title_and_glyph:I = 0x7f0a1043

.field public static final title_container:I = 0x7f0a1044

.field public static final title_row:I = 0x7f0a1045

.field public static final title_template:I = 0x7f0a1046

.field public static final title_text:I = 0x7f0a1047

.field public static final title_text_view:I = 0x7f0a1048

.field public static final toggle:I = 0x7f0a1049

.field public static final toggle_button_row:I = 0x7f0a104a

.field public static final toggle_card_state:I = 0x7f0a104b

.field public static final toggle_show_card_details:I = 0x7f0a104c

.field public static final toolbar:I = 0x7f0a104d

.field public static final top:I = 0x7f0a104e

.field public static final topPanel:I = 0x7f0a104f

.field public static final top_horizontal_divider:I = 0x7f0a1050

.field public static final top_line:I = 0x7f0a1051

.field public static final top_margin_guide:I = 0x7f0a1052

.field public static final top_panel:I = 0x7f0a1053

.field public static final top_text_guide:I = 0x7f0a1054

.field public static final top_text_guide_smaller:I = 0x7f0a1055

.field public static final top_text_view:I = 0x7f0a1056

.field public static final total:I = 0x7f0a1057

.field public static final total_title:I = 0x7f0a1058

.field public static final touch_outside:I = 0x7f0a1059

.field public static final touch_region:I = 0x7f0a105a

.field public static final tour_call_to_action:I = 0x7f0a105b

.field public static final tour_close_button:I = 0x7f0a105c

.field public static final tour_help_button:I = 0x7f0a105d

.field public static final tour_image:I = 0x7f0a105e

.field public static final tour_next_button:I = 0x7f0a105f

.field public static final tour_pager:I = 0x7f0a1060

.field public static final tour_subtitle:I = 0x7f0a1061

.field public static final tour_title:I = 0x7f0a1062

.field public static final traditional_receipt:I = 0x7f0a1063

.field public static final transaction_amount_detail:I = 0x7f0a1064

.field public static final transaction_amount_row:I = 0x7f0a1065

.field public static final transaction_date:I = 0x7f0a1066

.field public static final transaction_date_row:I = 0x7f0a1067

.field public static final transaction_description:I = 0x7f0a1068

.field public static final transaction_detail_view:I = 0x7f0a1069

.field public static final transaction_expense_type:I = 0x7f0a106a

.field public static final transaction_limit:I = 0x7f0a106b

.field public static final transaction_limit_hint:I = 0x7f0a106c

.field public static final transaction_limit_section:I = 0x7f0a106d

.field public static final transaction_load_more_retry:I = 0x7f0a106e

.field public static final transaction_time:I = 0x7f0a106f

.field public static final transaction_total:I = 0x7f0a1070

.field public static final transaction_type_text:I = 0x7f0a1071

.field public static final transactions:I = 0x7f0a1072

.field public static final transactions_history_animator:I = 0x7f0a1073

.field public static final transactions_history_list:I = 0x7f0a1074

.field public static final transactions_history_search_bar:I = 0x7f0a1075

.field public static final transfer_arrives_speed:I = 0x7f0a1076

.field public static final transfer_result_message:I = 0x7f0a1077

.field public static final transfer_result_spinner:I = 0x7f0a1078

.field public static final transfer_result_view:I = 0x7f0a1079

.field public static final transfer_speed_instant:I = 0x7f0a107a

.field public static final transfer_speed_instant_fee_rate:I = 0x7f0a107b

.field public static final transfer_speed_instant_group:I = 0x7f0a107c

.field public static final transfer_speed_instant_text:I = 0x7f0a107d

.field public static final transfer_speed_standard:I = 0x7f0a107e

.field public static final transfer_speed_standard_fee_rate:I = 0x7f0a107f

.field public static final transfer_to:I = 0x7f0a1080

.field public static final transfer_to_bank_button:I = 0x7f0a1081

.field public static final transfer_to_bank_view:I = 0x7f0a1082

.field public static final transferring_to_bank:I = 0x7f0a1083

.field public static final transit_number_field:I = 0x7f0a1084

.field public static final transition_current_scene:I = 0x7f0a1085

.field public static final transition_layout_save:I = 0x7f0a1086

.field public static final transition_position:I = 0x7f0a1087

.field public static final transition_scene_layoutid_cache:I = 0x7f0a1088

.field public static final transition_transform:I = 0x7f0a1089

.field public static final trash_stamp:I = 0x7f0a108a

.field public static final triangle:I = 0x7f0a108b

.field public static final troubleshooting_content:I = 0x7f0a108c

.field public static final troubleshooting_header:I = 0x7f0a108d

.field public static final troubleshooting_progress_bar:I = 0x7f0a108e

.field public static final tutorial2_bar:I = 0x7f0a108f

.field public static final tutorial2_baseline:I = 0x7f0a1090

.field public static final tutorial2_bottom_barrier:I = 0x7f0a1091

.field public static final tutorial2_bottom_space:I = 0x7f0a1092

.field public static final tutorial2_cancel:I = 0x7f0a1093

.field public static final tutorial2_content:I = 0x7f0a1094

.field public static final tutorial2_primary_button:I = 0x7f0a1095

.field public static final tutorial2_progress_text:I = 0x7f0a1096

.field public static final tutorial2_secondary_button:I = 0x7f0a1097

.field public static final tutorial2_tooltip:I = 0x7f0a1098

.field public static final tutorial2_top_space:I = 0x7f0a1099

.field public static final tutorial_arrow_bottom:I = 0x7f0a109a

.field public static final tutorial_arrow_top:I = 0x7f0a109b

.field public static final tutorial_bar:I = 0x7f0a109c

.field public static final tutorial_bar_cancel:I = 0x7f0a109d

.field public static final tutorial_bar_content:I = 0x7f0a109e

.field public static final tutorial_bars:I = 0x7f0a109f

.field public static final tutorial_dialog_content:I = 0x7f0a10a0

.field public static final tutorial_dialog_icon:I = 0x7f0a10a1

.field public static final tutorial_dialog_primary:I = 0x7f0a10a2

.field public static final tutorial_dialog_secondary:I = 0x7f0a10a3

.field public static final tutorial_dialog_title:I = 0x7f0a10a4

.field public static final tutorial_pager:I = 0x7f0a10a5

.field public static final tutorial_screen_image_view_template:I = 0x7f0a10a6

.field public static final tutorial_tooltip_content_root:I = 0x7f0a10a7

.field public static final tutorial_video_link_r12:I = 0x7f0a10a8

.field public static final tutorials_content:I = 0x7f0a10a9

.field public static final two_thirds:I = 0x7f0a10aa

.field public static final two_thirds_vertical_divider:I = 0x7f0a10ab

.field public static final unchecked:I = 0x7f0a10ac

.field public static final uncorrectable_warning:I = 0x7f0a10ad

.field public static final under_amount_skip_signature:I = 0x7f0a10ae

.field public static final underline:I = 0x7f0a10af

.field public static final undo:I = 0x7f0a10b0

.field public static final unhelpful_button:I = 0x7f0a10b1

.field public static final uniform:I = 0x7f0a10b2

.field public static final unit_number_limit_help_text:I = 0x7f0a10b3

.field public static final unit_selection_create_unit_button:I = 0x7f0a10b4

.field public static final unit_selection_help_text:I = 0x7f0a10b5

.field public static final unit_type_row:I = 0x7f0a10b6

.field public static final unknown_attribute:I = 0x7f0a10b7

.field public static final unlabeled:I = 0x7f0a10b8

.field public static final unread_indicator_red_dot:I = 0x7f0a10b9

.field public static final unread_indicator_red_dot_image_view:I = 0x7f0a10ba

.field public static final unsupported_text_view:I = 0x7f0a10bb

.field public static final unverified_address:I = 0x7f0a10bc

.field public static final unverified_address_order_free_card:I = 0x7f0a10bd

.field public static final unverified_address_re_enter_address:I = 0x7f0a10be

.field public static final up:I = 0x7f0a10bf

.field public static final up_button:I = 0x7f0a10c0

.field public static final up_button_glyph:I = 0x7f0a10c1

.field public static final up_text:I = 0x7f0a10c2

.field public static final update_existing_variations_recycler:I = 0x7f0a10c3

.field public static final update_loyalty_phone_view_root:I = 0x7f0a10c4

.field public static final updated_employee_management_container:I = 0x7f0a10c5

.field public static final updated_employee_management_passcode_description:I = 0x7f0a10c6

.field public static final updated_employee_management_passcode_toggle_always:I = 0x7f0a10c7

.field public static final updated_employee_management_passcode_toggle_never:I = 0x7f0a10c8

.field public static final updated_employee_management_passcode_toggle_restricted:I = 0x7f0a10c9

.field public static final updated_employee_management_timeout_toggle_1m:I = 0x7f0a10ca

.field public static final updated_employee_management_timeout_toggle_30s:I = 0x7f0a10cb

.field public static final updated_employee_management_timeout_toggle_5m:I = 0x7f0a10cc

.field public static final updated_employee_management_timeout_toggle_container:I = 0x7f0a10cd

.field public static final updated_employee_management_timeout_toggle_never:I = 0x7f0a10ce

.field public static final updated_employee_management_transaction_lock_mode_toggle:I = 0x7f0a10cf

.field public static final upload_attachment_progressbar:I = 0x7f0a10d0

.field public static final upload_photo:I = 0x7f0a10d1

.field public static final upload_progress_bar:I = 0x7f0a10d2

.field public static final upload_support_ledger_row:I = 0x7f0a10d3

.field public static final uploaded_time_stamp:I = 0x7f0a10d4

.field public static final uploading_text:I = 0x7f0a10d5

.field public static final useLogo:I = 0x7f0a10d6

.field public static final user_attachment_imageview:I = 0x7f0a10d7

.field public static final user_date_text:I = 0x7f0a10d8

.field public static final user_image_message_layout:I = 0x7f0a10d9

.field public static final user_message_container:I = 0x7f0a10da

.field public static final user_message_retry_button:I = 0x7f0a10db

.field public static final user_message_text:I = 0x7f0a10dc

.field public static final user_text_message_layout:I = 0x7f0a10dd

.field public static final value:I = 0x7f0a10de

.field public static final value_container:I = 0x7f0a10df

.field public static final value_subtitle:I = 0x7f0a10e0

.field public static final values:I = 0x7f0a10e1

.field public static final valuesBarrier:I = 0x7f0a10e2

.field public static final variable_button:I = 0x7f0a10e3

.field public static final variation_header:I = 0x7f0a10e4

.field public static final vector:I = 0x7f0a10e5

.field public static final vector_image:I = 0x7f0a10e6

.field public static final verification_code_field:I = 0x7f0a10e7

.field public static final verification_hint:I = 0x7f0a10e8

.field public static final verify_card_change_failure_message:I = 0x7f0a10e9

.field public static final verify_card_change_success_message:I = 0x7f0a10ea

.field public static final version_row:I = 0x7f0a10eb

.field public static final vertical_divider:I = 0x7f0a10ec

.field public static final view_all_activity:I = 0x7f0a10ed

.field public static final view_all_button:I = 0x7f0a10ee

.field public static final view_all_coupons_and_rewards_list:I = 0x7f0a10ef

.field public static final view_all_coupons_and_rewards_row:I = 0x7f0a10f0

.field public static final view_back_handler:I = 0x7f0a10f1

.field public static final view_billing_address:I = 0x7f0a10f2

.field public static final view_billing_address_layout:I = 0x7f0a10f3

.field public static final view_contents:I = 0x7f0a10f4

.field public static final view_faqs_load_error:I = 0x7f0a10f5

.field public static final view_faqs_loading:I = 0x7f0a10f6

.field public static final view_no_faqs:I = 0x7f0a10f7

.field public static final view_notes_entry:I = 0x7f0a10f8

.field public static final view_notes_entry_job_title:I = 0x7f0a10f9

.field public static final view_notes_entry_note_content:I = 0x7f0a10fa

.field public static final view_notifications:I = 0x7f0a10fb

.field public static final view_offset_helper:I = 0x7f0a10fc

.field public static final view_pager:I = 0x7f0a10fd

.field public static final view_pager_container:I = 0x7f0a10fe

.field public static final view_pager_content_container:I = 0x7f0a10ff

.field public static final view_persistence_stack_key:I = 0x7f0a1100

.field public static final view_rewards_screen:I = 0x7f0a1101

.field public static final view_rewards_screen_content:I = 0x7f0a1102

.field public static final view_rewards_screen_progress_bar:I = 0x7f0a1103

.field public static final view_show_rendering_function:I = 0x7f0a1104

.field public static final view_stack:I = 0x7f0a1105

.field public static final view_submission:I = 0x7f0a1106

.field public static final visible:I = 0x7f0a1107

.field public static final visible_removing_fragment_view_tag:I = 0x7f0a1108

.field public static final void_comp_cancel_button:I = 0x7f0a1109

.field public static final void_comp_cancel_button_text:I = 0x7f0a110a

.field public static final void_comp_help_text:I = 0x7f0a110b

.field public static final void_comp_primary_button:I = 0x7f0a110c

.field public static final void_comp_reason_header:I = 0x7f0a110d

.field public static final void_comp_reason_radios:I = 0x7f0a110e

.field public static final warning:I = 0x7f0a110f

.field public static final warning_banner_row_message_view:I = 0x7f0a1110

.field public static final warning_message:I = 0x7f0a1111

.field public static final web_back_button:I = 0x7f0a1112

.field public static final web_close_button:I = 0x7f0a1113

.field public static final web_forward_button:I = 0x7f0a1114

.field public static final web_progress_bar:I = 0x7f0a1115

.field public static final web_reload_button:I = 0x7f0a1116

.field public static final web_view:I = 0x7f0a1117

.field public static final webview:I = 0x7f0a1118

.field public static final weekend_balance_button:I = 0x7f0a1119

.field public static final weekend_balance_hint:I = 0x7f0a111a

.field public static final why_message:I = 0x7f0a111b

.field public static final wide:I = 0x7f0a111c

.field public static final width:I = 0x7f0a111d

.field public static final withText:I = 0x7f0a111e

.field public static final workflow_back_handler:I = 0x7f0a111f

.field public static final workflow_back_stack_container:I = 0x7f0a1120

.field public static final workflow_back_stack_key:I = 0x7f0a1121

.field public static final workflow_layout:I = 0x7f0a1122

.field public static final workflow_text_helper:I = 0x7f0a1123

.field public static final wrap:I = 0x7f0a1124

.field public static final wrap_content:I = 0x7f0a1125

.field public static final x_button:I = 0x7f0a1126

.field public static final xable_text_field:I = 0x7f0a1127

.field public static final year_picker:I = 0x7f0a1128


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
