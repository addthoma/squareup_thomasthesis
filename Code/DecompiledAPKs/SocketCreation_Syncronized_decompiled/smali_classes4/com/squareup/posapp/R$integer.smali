.class public final Lcom/squareup/posapp/R$integer;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/posapp/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "integer"
.end annotation


# static fields
.field public static final abc_config_activityDefaultDur:I = 0x7f0b0000

.field public static final abc_config_activityShortDur:I = 0x7f0b0001

.field public static final activation_row_anim_duration:I = 0x7f0b0002

.field public static final add_or_edit_notes_max_length:I = 0x7f0b0003

.field public static final app_bar_elevation_anim_duration:I = 0x7f0b0004

.field public static final bottom_sheet_slide_duration:I = 0x7f0b0005

.field public static final cancel_button_image_alpha:I = 0x7f0b0006

.field public static final cancel_card_feedback_lines:I = 0x7f0b0007

.field public static final cancel_card_feedback_max_length:I = 0x7f0b0008

.field public static final card_slide_time:I = 0x7f0b0009

.field public static final cart_header_flyout_duration:I = 0x7f0b000a

.field public static final center_horizontal:I = 0x7f0b000b

.field public static final color_picker_column_count:I = 0x7f0b000c

.field public static final config_activityShortDur:I = 0x7f0b000d

.field public static final config_tooltipAnimTime:I = 0x7f0b000e

.field public static final configure_item_note_max_length:I = 0x7f0b000f

.field public static final configure_item_note_max_visible_lines:I = 0x7f0b0010

.field public static final crm_company_max_length:I = 0x7f0b0011

.field public static final crm_coupon_max_price:I = 0x7f0b0012

.field public static final crm_customer_input_help_text_gravity:I = 0x7f0b0013

.field public static final crm_email_max_length:I = 0x7f0b0014

.field public static final crm_group_max_length:I = 0x7f0b0015

.field public static final crm_name_max_length:I = 0x7f0b0016

.field public static final crm_note_max_length:I = 0x7f0b0017

.field public static final crm_personal_info_note_max_length:I = 0x7f0b0018

.field public static final crm_personal_info_note_max_lines:I = 0x7f0b0019

.field public static final crm_phone_max_length:I = 0x7f0b001a

.field public static final crm_reference_id_max_length:I = 0x7f0b001b

.field public static final crm_text_attribute_max_length:I = 0x7f0b001c

.field public static final default_animation_duration:I = 0x7f0b001d

.field public static final default_title_indicator_footer_indicator_style:I = 0x7f0b001e

.field public static final default_underline_indicator_fade_delay:I = 0x7f0b001f

.field public static final default_underline_indicator_fade_length:I = 0x7f0b0020

.field public static final design_snackbar_text_max_lines:I = 0x7f0b0021

.field public static final design_tab_indicator_anim_duration_ms:I = 0x7f0b0022

.field public static final edit_item_description_length:I = 0x7f0b0023

.field public static final equalizing_text_view_max_additional_lines:I = 0x7f0b0024

.field public static final google_play_services_version:I = 0x7f0b0025

.field public static final hide_password_duration:I = 0x7f0b0026

.field public static final hs__chat_max_lines:I = 0x7f0b0027

.field public static final hs__conversation_detail_lines:I = 0x7f0b0028

.field public static final hs__faqs_list_item_max_lines:I = 0x7f0b0029

.field public static final hs__issue_description_max_lines:I = 0x7f0b002a

.field public static final hs__issue_description_min_chars:I = 0x7f0b002b

.field public static final hs_animation_duration:I = 0x7f0b002c

.field public static final invoice_field_standard_max_length:I = 0x7f0b002d

.field public static final invoice_number_standard_max_length:I = 0x7f0b002e

.field public static final item_attribute_field_label_layout_weight:I = 0x7f0b002f

.field public static final landing_reader_click_anim_duration:I = 0x7f0b0030

.field public static final landing_reader_click_start_delay:I = 0x7f0b0031

.field public static final landing_reader_insert_anim_duration:I = 0x7f0b0032

.field public static final left:I = 0x7f0b0033

.field public static final marin_spinner_tranisition_time:I = 0x7f0b0034

.field public static final maximum_length_of_option_value_names:I = 0x7f0b0035

.field public static final merchant_profile_description_length:I = 0x7f0b0036

.field public static final mtrl_badge_max_character_count:I = 0x7f0b0037

.field public static final mtrl_btn_anim_delay_ms:I = 0x7f0b0038

.field public static final mtrl_btn_anim_duration_ms:I = 0x7f0b0039

.field public static final mtrl_calendar_header_orientation:I = 0x7f0b003a

.field public static final mtrl_calendar_selection_text_lines:I = 0x7f0b003b

.field public static final mtrl_calendar_year_selector_span:I = 0x7f0b003c

.field public static final mtrl_card_anim_delay_ms:I = 0x7f0b003d

.field public static final mtrl_card_anim_duration_ms:I = 0x7f0b003e

.field public static final mtrl_chip_anim_duration:I = 0x7f0b003f

.field public static final mtrl_tab_indicator_anim_duration_ms:I = 0x7f0b0040

.field public static final multiline_edit_text_max_lines:I = 0x7f0b0041

.field public static final noho_glyph_visibility:I = 0x7f0b0042

.field public static final noho_message_image_visibility:I = 0x7f0b0043

.field public static final order_square_card_stamp_gallery_grid_count:I = 0x7f0b0044

.field public static final power_of_ten_for_compact_numbers:I = 0x7f0b0045

.field public static final reader_jack_first_segment_x:I = 0x7f0b0046

.field public static final reader_jack_first_segment_y:I = 0x7f0b0047

.field public static final receipt_auto_finish_delay_millis:I = 0x7f0b0048

.field public static final record_payment_note_max_lines:I = 0x7f0b0049

.field public static final reward_code_length:I = 0x7f0b004a

.field public static final search_field_max_length:I = 0x7f0b004b

.field public static final search_max_length:I = 0x7f0b004c

.field public static final setup_dialog_primary_angle:I = 0x7f0b004d

.field public static final setup_guide_gradient_angle:I = 0x7f0b004e

.field public static final show_password_duration:I = 0x7f0b004f

.field public static final square_card_preview_max_text_size:I = 0x7f0b0050

.field public static final square_card_preview_min_text_size:I = 0x7f0b0051

.field public static final square_card_preview_scale_increment:I = 0x7f0b0052

.field public static final status_bar_notification_info_maxnum:I = 0x7f0b0053

.field public static final tax_precision:I = 0x7f0b0054

.field public static final ticket_name_max_length:I = 0x7f0b0055

.field public static final ticket_note_max_length:I = 0x7f0b0056

.field public static final tutorial_dialog_title_lines:I = 0x7f0b0057

.field public static final view_gone:I = 0x7f0b0058

.field public static final view_invisible:I = 0x7f0b0059

.field public static final view_visible:I = 0x7f0b005a


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18569
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
