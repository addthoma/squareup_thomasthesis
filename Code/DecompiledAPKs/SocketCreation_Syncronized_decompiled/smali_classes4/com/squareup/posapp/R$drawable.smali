.class public final Lcom/squareup/posapp/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/posapp/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final abc_ab_share_pack_mtrl_alpha:I = 0x7f080020

.field public static final abc_action_bar_item_background_material:I = 0x7f080021

.field public static final abc_btn_borderless_material:I = 0x7f080022

.field public static final abc_btn_check_material:I = 0x7f080023

.field public static final abc_btn_check_material_anim:I = 0x7f080024

.field public static final abc_btn_check_to_on_mtrl_000:I = 0x7f080025

.field public static final abc_btn_check_to_on_mtrl_015:I = 0x7f080026

.field public static final abc_btn_colored_material:I = 0x7f080027

.field public static final abc_btn_default_mtrl_shape:I = 0x7f080028

.field public static final abc_btn_radio_material:I = 0x7f080029

.field public static final abc_btn_radio_material_anim:I = 0x7f08002a

.field public static final abc_btn_radio_to_on_mtrl_000:I = 0x7f08002b

.field public static final abc_btn_radio_to_on_mtrl_015:I = 0x7f08002c

.field public static final abc_btn_switch_to_on_mtrl_00001:I = 0x7f08002d

.field public static final abc_btn_switch_to_on_mtrl_00012:I = 0x7f08002e

.field public static final abc_cab_background_internal_bg:I = 0x7f08002f

.field public static final abc_cab_background_top_material:I = 0x7f080030

.field public static final abc_cab_background_top_mtrl_alpha:I = 0x7f080031

.field public static final abc_control_background_material:I = 0x7f080032

.field public static final abc_dialog_material_background:I = 0x7f080033

.field public static final abc_edit_text_material:I = 0x7f080034

.field public static final abc_ic_ab_back_material:I = 0x7f080035

.field public static final abc_ic_arrow_drop_right_black_24dp:I = 0x7f080036

.field public static final abc_ic_clear_material:I = 0x7f080037

.field public static final abc_ic_commit_search_api_mtrl_alpha:I = 0x7f080038

.field public static final abc_ic_go_search_api_material:I = 0x7f080039

.field public static final abc_ic_menu_copy_mtrl_am_alpha:I = 0x7f08003a

.field public static final abc_ic_menu_cut_mtrl_alpha:I = 0x7f08003b

.field public static final abc_ic_menu_overflow_material:I = 0x7f08003c

.field public static final abc_ic_menu_paste_mtrl_am_alpha:I = 0x7f08003d

.field public static final abc_ic_menu_selectall_mtrl_alpha:I = 0x7f08003e

.field public static final abc_ic_menu_share_mtrl_alpha:I = 0x7f08003f

.field public static final abc_ic_search_api_material:I = 0x7f080040

.field public static final abc_ic_star_black_16dp:I = 0x7f080041

.field public static final abc_ic_star_black_36dp:I = 0x7f080042

.field public static final abc_ic_star_black_48dp:I = 0x7f080043

.field public static final abc_ic_star_half_black_16dp:I = 0x7f080044

.field public static final abc_ic_star_half_black_36dp:I = 0x7f080045

.field public static final abc_ic_star_half_black_48dp:I = 0x7f080046

.field public static final abc_ic_voice_search_api_material:I = 0x7f080047

.field public static final abc_item_background_holo_dark:I = 0x7f080048

.field public static final abc_item_background_holo_light:I = 0x7f080049

.field public static final abc_list_divider_material:I = 0x7f08004a

.field public static final abc_list_divider_mtrl_alpha:I = 0x7f08004b

.field public static final abc_list_focused_holo:I = 0x7f08004c

.field public static final abc_list_longpressed_holo:I = 0x7f08004d

.field public static final abc_list_pressed_holo_dark:I = 0x7f08004e

.field public static final abc_list_pressed_holo_light:I = 0x7f08004f

.field public static final abc_list_selector_background_transition_holo_dark:I = 0x7f080050

.field public static final abc_list_selector_background_transition_holo_light:I = 0x7f080051

.field public static final abc_list_selector_disabled_holo_dark:I = 0x7f080052

.field public static final abc_list_selector_disabled_holo_light:I = 0x7f080053

.field public static final abc_list_selector_holo_dark:I = 0x7f080054

.field public static final abc_list_selector_holo_light:I = 0x7f080055

.field public static final abc_menu_hardkey_panel_mtrl_mult:I = 0x7f080056

.field public static final abc_popup_background_mtrl_mult:I = 0x7f080057

.field public static final abc_ratingbar_indicator_material:I = 0x7f080058

.field public static final abc_ratingbar_material:I = 0x7f080059

.field public static final abc_ratingbar_small_material:I = 0x7f08005a

.field public static final abc_scrubber_control_off_mtrl_alpha:I = 0x7f08005b

.field public static final abc_scrubber_control_to_pressed_mtrl_000:I = 0x7f08005c

.field public static final abc_scrubber_control_to_pressed_mtrl_005:I = 0x7f08005d

.field public static final abc_scrubber_primary_mtrl_alpha:I = 0x7f08005e

.field public static final abc_scrubber_track_mtrl_alpha:I = 0x7f08005f

.field public static final abc_seekbar_thumb_material:I = 0x7f080060

.field public static final abc_seekbar_tick_mark_material:I = 0x7f080061

.field public static final abc_seekbar_track_material:I = 0x7f080062

.field public static final abc_spinner_mtrl_am_alpha:I = 0x7f080063

.field public static final abc_spinner_textfield_background_material:I = 0x7f080064

.field public static final abc_switch_thumb_material:I = 0x7f080065

.field public static final abc_switch_track_mtrl_alpha:I = 0x7f080066

.field public static final abc_tab_indicator_material:I = 0x7f080067

.field public static final abc_tab_indicator_mtrl_alpha:I = 0x7f080068

.field public static final abc_text_cursor_material:I = 0x7f080069

.field public static final abc_text_select_handle_left_mtrl_dark:I = 0x7f08006a

.field public static final abc_text_select_handle_left_mtrl_light:I = 0x7f08006b

.field public static final abc_text_select_handle_middle_mtrl_dark:I = 0x7f08006c

.field public static final abc_text_select_handle_middle_mtrl_light:I = 0x7f08006d

.field public static final abc_text_select_handle_right_mtrl_dark:I = 0x7f08006e

.field public static final abc_text_select_handle_right_mtrl_light:I = 0x7f08006f

.field public static final abc_textfield_activated_mtrl_alpha:I = 0x7f080070

.field public static final abc_textfield_default_mtrl_alpha:I = 0x7f080071

.field public static final abc_textfield_search_activated_mtrl_alpha:I = 0x7f080072

.field public static final abc_textfield_search_default_mtrl_alpha:I = 0x7f080073

.field public static final abc_textfield_search_material:I = 0x7f080074

.field public static final abc_vector_test:I = 0x7f080075

.field public static final add_attachment_row_icon:I = 0x7f080076

.field public static final add_customer_row_icon:I = 0x7f080077

.field public static final add_item_row_icon:I = 0x7f080078

.field public static final add_recipient_row_icon:I = 0x7f080079

.field public static final add_row_icon:I = 0x7f08007a

.field public static final adjust_inventory_background_no_background_edit_row:I = 0x7f08007b

.field public static final adjust_inventory_background_no_border:I = 0x7f08007c

.field public static final adjust_inventory_background_selected_edit_row:I = 0x7f08007d

.field public static final all_edges:I = 0x7f08007e

.field public static final amount_border:I = 0x7f08007f

.field public static final amount_border_focused:I = 0x7f080080

.field public static final avd_hide_password:I = 0x7f080081

.field public static final avd_show_password:I = 0x7f080082

.field public static final background_star:I = 0x7f080083

.field public static final badge_expiring:I = 0x7f080084

.field public static final badge_expiring_selected:I = 0x7f080085

.field public static final badge_offline_payment_failure:I = 0x7f080086

.field public static final badge_offline_payment_failure_selected:I = 0x7f080087

.field public static final badge_pending:I = 0x7f080088

.field public static final badge_pending_actionbar:I = 0x7f080089

.field public static final badge_pending_actionbar_pressed:I = 0x7f08008a

.field public static final badge_refund:I = 0x7f08008b

.field public static final badge_refund_selected:I = 0x7f08008c

.field public static final balance_activity_details_pill_background:I = 0x7f08008d

.field public static final balance_white_phone:I = 0x7f08008e

.field public static final bank_logo_ally:I = 0x7f08008f

.field public static final bank_logo_bankofamerica:I = 0x7f080090

.field public static final bank_logo_bankofthewest:I = 0x7f080091

.field public static final bank_logo_bnymellon:I = 0x7f080092

.field public static final bank_logo_branchbankingandtrust:I = 0x7f080093

.field public static final bank_logo_capitalone:I = 0x7f080094

.field public static final bank_logo_chase:I = 0x7f080095

.field public static final bank_logo_citibank:I = 0x7f080096

.field public static final bank_logo_citizensfinancialbank:I = 0x7f080097

.field public static final bank_logo_compass:I = 0x7f080098

.field public static final bank_logo_etrade:I = 0x7f080099

.field public static final bank_logo_fifththird:I = 0x7f08009a

.field public static final bank_logo_harris:I = 0x7f08009b

.field public static final bank_logo_hsbc:I = 0x7f08009c

.field public static final bank_logo_huntington:I = 0x7f08009d

.field public static final bank_logo_ing:I = 0x7f08009e

.field public static final bank_logo_key:I = 0x7f08009f

.field public static final bank_logo_manufacturersandtraderstrust:I = 0x7f0800a0

.field public static final bank_logo_northerntrust:I = 0x7f0800a1

.field public static final bank_logo_pnc:I = 0x7f0800a2

.field public static final bank_logo_rbc:I = 0x7f0800a3

.field public static final bank_logo_rbscitizens:I = 0x7f0800a4

.field public static final bank_logo_regions:I = 0x7f0800a5

.field public static final bank_logo_sovereign:I = 0x7f0800a6

.field public static final bank_logo_statestreetbankandtrust:I = 0x7f0800a7

.field public static final bank_logo_suntrust:I = 0x7f0800a8

.field public static final bank_logo_td:I = 0x7f0800a9

.field public static final bank_logo_union:I = 0x7f0800aa

.field public static final bank_logo_us:I = 0x7f0800ab

.field public static final bank_logo_usaa:I = 0x7f0800ac

.field public static final bank_logo_wellsfargo:I = 0x7f0800ad

.field public static final bell:I = 0x7f0800ae

.field public static final bill_details_empty_state_icon:I = 0x7f0800af

.field public static final bill_details_tender_customer_circle:I = 0x7f0800b0

.field public static final blank_file:I = 0x7f0800b1

.field public static final blue_star_empty:I = 0x7f0800b2

.field public static final blue_star_filled:I = 0x7f0800b3

.field public static final blue_star_selector:I = 0x7f0800b4

.field public static final bottom_light_gray_border_white_background_1px:I = 0x7f0800b5

.field public static final brands_caviar_24:I = 0x7f0800b6

.field public static final brands_deliveroo_24:I = 0x7f0800b7

.field public static final brands_doordash_24:I = 0x7f0800b8

.field public static final brands_instagram_24:I = 0x7f0800b9

.field public static final brands_just_eat_24:I = 0x7f0800ba

.field public static final brands_postmates_24:I = 0x7f0800bb

.field public static final brands_square_24:I = 0x7f0800bc

.field public static final brands_uber_eats_24:I = 0x7f0800bd

.field public static final brands_weebly_24:I = 0x7f0800be

.field public static final btn_checkbox_checked_mtrl:I = 0x7f0800bf

.field public static final btn_checkbox_checked_to_unchecked_mtrl_animation:I = 0x7f0800c0

.field public static final btn_checkbox_unchecked_mtrl:I = 0x7f0800c1

.field public static final btn_checkbox_unchecked_to_checked_mtrl_animation:I = 0x7f0800c2

.field public static final btn_radio_off_mtrl:I = 0x7f0800c3

.field public static final btn_radio_off_to_on_mtrl_animation:I = 0x7f0800c4

.field public static final btn_radio_on_mtrl:I = 0x7f0800c5

.field public static final btn_radio_on_to_off_mtrl_animation:I = 0x7f0800c6

.field public static final buildings_building_columns_80:I = 0x7f0800c7

.field public static final buyer_facing_button_pressed:I = 0x7f0800c8

.field public static final buyer_language_icon:I = 0x7f0800c9

.field public static final buyer_language_icon_white:I = 0x7f0800ca

.field public static final buyer_language_selector:I = 0x7f0800cb

.field public static final buyer_language_selector_white:I = 0x7f0800cc

.field public static final calendar_bg_selector:I = 0x7f0800cd

.field public static final capital_offer_letter:I = 0x7f0800ce

.field public static final card_chip:I = 0x7f0800cf

.field public static final card_chip_disabled:I = 0x7f0800d0

.field public static final card_slot:I = 0x7f0800d1

.field public static final card_swipe_icon:I = 0x7f0800d2

.field public static final cart_entry_discount_icon:I = 0x7f0800d3

.field public static final cart_entry_tax_icon:I = 0x7f0800d4

.field public static final cart_entry_total_icon:I = 0x7f0800d5

.field public static final cart_link:I = 0x7f0800d6

.field public static final check_selector:I = 0x7f0800d7

.field public static final chip_card_2_120:I = 0x7f0800d8

.field public static final circle_alert_24:I = 0x7f0800d9

.field public static final circle_alert_96:I = 0x7f0800da

.field public static final circle_arrow_left_24:I = 0x7f0800db

.field public static final circle_calendar_month_today:I = 0x7f0800dc

.field public static final circle_check_24:I = 0x7f0800dd

.field public static final circle_check_96:I = 0x7f0800de

.field public static final circle_check_white_96:I = 0x7f0800df

.field public static final circle_circle_arrows_left_right_40:I = 0x7f0800e0

.field public static final circle_circle_check_80:I = 0x7f0800e1

.field public static final circle_filled_check_24:I = 0x7f0800e2

.field public static final circle_hand_fingers_snapping:I = 0x7f0800e3

.field public static final circle_minus_24:I = 0x7f0800e4

.field public static final circle_plus_24:I = 0x7f0800e5

.field public static final circle_question_24:I = 0x7f0800e6

.field public static final common_full_open_on_phone:I = 0x7f0800e7

.field public static final common_google_signin_btn_icon_dark:I = 0x7f0800e8

.field public static final common_google_signin_btn_icon_dark_focused:I = 0x7f0800e9

.field public static final common_google_signin_btn_icon_dark_normal:I = 0x7f0800ea

.field public static final common_google_signin_btn_icon_dark_normal_background:I = 0x7f0800eb

.field public static final common_google_signin_btn_icon_disabled:I = 0x7f0800ec

.field public static final common_google_signin_btn_icon_light:I = 0x7f0800ed

.field public static final common_google_signin_btn_icon_light_focused:I = 0x7f0800ee

.field public static final common_google_signin_btn_icon_light_normal:I = 0x7f0800ef

.field public static final common_google_signin_btn_icon_light_normal_background:I = 0x7f0800f0

.field public static final common_google_signin_btn_text_dark:I = 0x7f0800f1

.field public static final common_google_signin_btn_text_dark_focused:I = 0x7f0800f2

.field public static final common_google_signin_btn_text_dark_normal:I = 0x7f0800f3

.field public static final common_google_signin_btn_text_dark_normal_background:I = 0x7f0800f4

.field public static final common_google_signin_btn_text_disabled:I = 0x7f0800f5

.field public static final common_google_signin_btn_text_light:I = 0x7f0800f6

.field public static final common_google_signin_btn_text_light_focused:I = 0x7f0800f7

.field public static final common_google_signin_btn_text_light_normal:I = 0x7f0800f8

.field public static final common_google_signin_btn_text_light_normal_background:I = 0x7f0800f9

.field public static final configure_item_edit_background_no_border:I = 0x7f0800fa

.field public static final contactless:I = 0x7f0800fb

.field public static final conversation_bubble_left:I = 0x7f0800fc

.field public static final conversation_bubble_right:I = 0x7f0800fd

.field public static final coupon_icon_selected:I = 0x7f0800fe

.field public static final coupon_icon_selector:I = 0x7f0800ff

.field public static final coupon_icon_unselected:I = 0x7f080100

.field public static final create_passcode_digit_style:I = 0x7f080101

.field public static final crm_action_arrow:I = 0x7f080102

.field public static final crm_action_close:I = 0x7f080103

.field public static final crm_action_overflow:I = 0x7f080104

.field public static final crm_appointment_cancelled:I = 0x7f080105

.field public static final crm_appointment_no_show:I = 0x7f080106

.field public static final crm_card_divider:I = 0x7f080107

.field public static final crm_file_document:I = 0x7f080108

.field public static final crm_file_image:I = 0x7f080109

.field public static final crm_initial_circle_bg:I = 0x7f08010a

.field public static final crm_merge_proposal_border:I = 0x7f08010b

.field public static final crm_payment_amex:I = 0x7f08010c

.field public static final crm_payment_dinersclub:I = 0x7f08010d

.field public static final crm_payment_discover:I = 0x7f08010e

.field public static final crm_payment_generic:I = 0x7f08010f

.field public static final crm_payment_giftcard:I = 0x7f080110

.field public static final crm_payment_interac:I = 0x7f080111

.field public static final crm_payment_jcb:I = 0x7f080112

.field public static final crm_payment_mastercard:I = 0x7f080113

.field public static final crm_payment_unionpay:I = 0x7f080114

.field public static final crm_payment_visa:I = 0x7f080115

.field public static final crossed_out_bell:I = 0x7f080116

.field public static final customer_circle_background:I = 0x7f080117

.field public static final date_picker_weekday_cell_selector:I = 0x7f080118

.field public static final design_bottom_navigation_item_background:I = 0x7f080119

.field public static final design_fab_background:I = 0x7f08011a

.field public static final design_ic_visibility:I = 0x7f08011b

.field public static final design_ic_visibility_off:I = 0x7f08011c

.field public static final design_password_eye:I = 0x7f08011d

.field public static final design_snackbar_background:I = 0x7f08011e

.field public static final details_row_icon:I = 0x7f08011f

.field public static final device_cash_drawer_80:I = 0x7f080120

.field public static final device_laptop_items_40:I = 0x7f080121

.field public static final diagram_box_arrow_cycle_40:I = 0x7f080122

.field public static final diagram_box_copy_40:I = 0x7f080123

.field public static final diagram_box_graph_bar_40:I = 0x7f080124

.field public static final diagram_box_graph_bar_callout_40:I = 0x7f080125

.field public static final diagram_box_stacked_plus_40:I = 0x7f080126

.field public static final digit_divider_clear_small_gap:I = 0x7f080127

.field public static final direct_debit_logo:I = 0x7f080128

.field public static final documents_envelope_80:I = 0x7f080129

.field public static final documents_time_card_40:I = 0x7f08012a

.field public static final due_row_icon:I = 0x7f08012b

.field public static final empty_cell:I = 0x7f08012c

.field public static final expandable_view_background:I = 0x7f08012d

.field public static final expandable_view_collapse:I = 0x7f08012e

.field public static final expandable_view_expand:I = 0x7f08012f

.field public static final fastscroll_label_right_holo_light:I = 0x7f080130

.field public static final fastscroll_thumb_default_holo:I = 0x7f080131

.field public static final fastscroll_thumb_holo:I = 0x7f080132

.field public static final fastscroll_thumb_pressed_holo:I = 0x7f080133

.field public static final fastscroll_track_default_holo_light:I = 0x7f080134

.field public static final fastscroll_track_holo_light:I = 0x7f080135

.field public static final fastscroll_track_pressed_holo_light:I = 0x7f080136

.field public static final favorites_96:I = 0x7f080137

.field public static final flag_ae:I = 0x7f080138

.field public static final flag_ag:I = 0x7f080139

.field public static final flag_ai:I = 0x7f08013a

.field public static final flag_al:I = 0x7f08013b

.field public static final flag_am:I = 0x7f08013c

.field public static final flag_ao:I = 0x7f08013d

.field public static final flag_ar:I = 0x7f08013e

.field public static final flag_at:I = 0x7f08013f

.field public static final flag_au:I = 0x7f080140

.field public static final flag_aw:I = 0x7f080141

.field public static final flag_az:I = 0x7f080142

.field public static final flag_ba:I = 0x7f080143

.field public static final flag_bb:I = 0x7f080144

.field public static final flag_bd:I = 0x7f080145

.field public static final flag_be:I = 0x7f080146

.field public static final flag_bf:I = 0x7f080147

.field public static final flag_bg:I = 0x7f080148

.field public static final flag_bh:I = 0x7f080149

.field public static final flag_bj:I = 0x7f08014a

.field public static final flag_bm:I = 0x7f08014b

.field public static final flag_bn:I = 0x7f08014c

.field public static final flag_bo:I = 0x7f08014d

.field public static final flag_br:I = 0x7f08014e

.field public static final flag_bs:I = 0x7f08014f

.field public static final flag_bt:I = 0x7f080150

.field public static final flag_bw:I = 0x7f080151

.field public static final flag_by:I = 0x7f080152

.field public static final flag_bz:I = 0x7f080153

.field public static final flag_ca:I = 0x7f080154

.field public static final flag_cg:I = 0x7f080155

.field public static final flag_ch:I = 0x7f080156

.field public static final flag_ci:I = 0x7f080157

.field public static final flag_cl:I = 0x7f080158

.field public static final flag_cm:I = 0x7f080159

.field public static final flag_cn:I = 0x7f08015a

.field public static final flag_co:I = 0x7f08015b

.field public static final flag_cr:I = 0x7f08015c

.field public static final flag_cv:I = 0x7f08015d

.field public static final flag_cy:I = 0x7f08015e

.field public static final flag_cz:I = 0x7f08015f

.field public static final flag_de:I = 0x7f080160

.field public static final flag_dk:I = 0x7f080161

.field public static final flag_dm:I = 0x7f080162

.field public static final flag_do:I = 0x7f080163

.field public static final flag_dz:I = 0x7f080164

.field public static final flag_ec:I = 0x7f080165

.field public static final flag_ee:I = 0x7f080166

.field public static final flag_eg:I = 0x7f080167

.field public static final flag_es:I = 0x7f080168

.field public static final flag_fi:I = 0x7f080169

.field public static final flag_fj:I = 0x7f08016a

.field public static final flag_fm:I = 0x7f08016b

.field public static final flag_fr:I = 0x7f08016c

.field public static final flag_ga:I = 0x7f08016d

.field public static final flag_gb:I = 0x7f08016e

.field public static final flag_gd:I = 0x7f08016f

.field public static final flag_gh:I = 0x7f080170

.field public static final flag_gm:I = 0x7f080171

.field public static final flag_gr:I = 0x7f080172

.field public static final flag_gt:I = 0x7f080173

.field public static final flag_gu:I = 0x7f080174

.field public static final flag_gw:I = 0x7f080175

.field public static final flag_gy:I = 0x7f080176

.field public static final flag_hk:I = 0x7f080177

.field public static final flag_hn:I = 0x7f080178

.field public static final flag_hr:I = 0x7f080179

.field public static final flag_ht:I = 0x7f08017a

.field public static final flag_hu:I = 0x7f08017b

.field public static final flag_id:I = 0x7f08017c

.field public static final flag_ie:I = 0x7f08017d

.field public static final flag_il:I = 0x7f08017e

.field public static final flag_in:I = 0x7f08017f

.field public static final flag_is:I = 0x7f080180

.field public static final flag_it:I = 0x7f080181

.field public static final flag_jm:I = 0x7f080182

.field public static final flag_jo:I = 0x7f080183

.field public static final flag_jp:I = 0x7f080184

.field public static final flag_ke:I = 0x7f080185

.field public static final flag_kg:I = 0x7f080186

.field public static final flag_kh:I = 0x7f080187

.field public static final flag_kn:I = 0x7f080188

.field public static final flag_kr:I = 0x7f080189

.field public static final flag_kw:I = 0x7f08018a

.field public static final flag_ky:I = 0x7f08018b

.field public static final flag_kz:I = 0x7f08018c

.field public static final flag_la:I = 0x7f08018d

.field public static final flag_lb:I = 0x7f08018e

.field public static final flag_lc:I = 0x7f08018f

.field public static final flag_li:I = 0x7f080190

.field public static final flag_lk:I = 0x7f080191

.field public static final flag_lr:I = 0x7f080192

.field public static final flag_ls:I = 0x7f080193

.field public static final flag_lt:I = 0x7f080194

.field public static final flag_lu:I = 0x7f080195

.field public static final flag_lv:I = 0x7f080196

.field public static final flag_ly:I = 0x7f080197

.field public static final flag_ma:I = 0x7f080198

.field public static final flag_md:I = 0x7f080199

.field public static final flag_mg:I = 0x7f08019a

.field public static final flag_mk:I = 0x7f08019b

.field public static final flag_ml:I = 0x7f08019c

.field public static final flag_mm:I = 0x7f08019d

.field public static final flag_mn:I = 0x7f08019e

.field public static final flag_mo:I = 0x7f08019f

.field public static final flag_mp:I = 0x7f0801a0

.field public static final flag_mr:I = 0x7f0801a1

.field public static final flag_ms:I = 0x7f0801a2

.field public static final flag_mt:I = 0x7f0801a3

.field public static final flag_mu:I = 0x7f0801a4

.field public static final flag_mw:I = 0x7f0801a5

.field public static final flag_mx:I = 0x7f0801a6

.field public static final flag_my:I = 0x7f0801a7

.field public static final flag_mz:I = 0x7f0801a8

.field public static final flag_na:I = 0x7f0801a9

.field public static final flag_ne:I = 0x7f0801aa

.field public static final flag_ng:I = 0x7f0801ab

.field public static final flag_ni:I = 0x7f0801ac

.field public static final flag_nl:I = 0x7f0801ad

.field public static final flag_no:I = 0x7f0801ae

.field public static final flag_np:I = 0x7f0801af

.field public static final flag_nz:I = 0x7f0801b0

.field public static final flag_om:I = 0x7f0801b1

.field public static final flag_pa:I = 0x7f0801b2

.field public static final flag_pe:I = 0x7f0801b3

.field public static final flag_pg:I = 0x7f0801b4

.field public static final flag_ph:I = 0x7f0801b5

.field public static final flag_pk:I = 0x7f0801b6

.field public static final flag_pl:I = 0x7f0801b7

.field public static final flag_pr:I = 0x7f0801b8

.field public static final flag_pt:I = 0x7f0801b9

.field public static final flag_pw:I = 0x7f0801ba

.field public static final flag_py:I = 0x7f0801bb

.field public static final flag_qa:I = 0x7f0801bc

.field public static final flag_ro:I = 0x7f0801bd

.field public static final flag_rs:I = 0x7f0801be

.field public static final flag_ru:I = 0x7f0801bf

.field public static final flag_rw:I = 0x7f0801c0

.field public static final flag_sa:I = 0x7f0801c1

.field public static final flag_sb:I = 0x7f0801c2

.field public static final flag_sc:I = 0x7f0801c3

.field public static final flag_se:I = 0x7f0801c4

.field public static final flag_sg:I = 0x7f0801c5

.field public static final flag_si:I = 0x7f0801c6

.field public static final flag_sk:I = 0x7f0801c7

.field public static final flag_sl:I = 0x7f0801c8

.field public static final flag_sn:I = 0x7f0801c9

.field public static final flag_sr:I = 0x7f0801ca

.field public static final flag_st:I = 0x7f0801cb

.field public static final flag_sv:I = 0x7f0801cc

.field public static final flag_sz:I = 0x7f0801cd

.field public static final flag_tc:I = 0x7f0801ce

.field public static final flag_td:I = 0x7f0801cf

.field public static final flag_tg:I = 0x7f0801d0

.field public static final flag_th:I = 0x7f0801d1

.field public static final flag_tj:I = 0x7f0801d2

.field public static final flag_tm:I = 0x7f0801d3

.field public static final flag_tn:I = 0x7f0801d4

.field public static final flag_to:I = 0x7f0801d5

.field public static final flag_tr:I = 0x7f0801d6

.field public static final flag_tt:I = 0x7f0801d7

.field public static final flag_tv:I = 0x7f0801d8

.field public static final flag_tw:I = 0x7f0801d9

.field public static final flag_tz:I = 0x7f0801da

.field public static final flag_ua:I = 0x7f0801db

.field public static final flag_ug:I = 0x7f0801dc

.field public static final flag_us:I = 0x7f0801dd

.field public static final flag_uy:I = 0x7f0801de

.field public static final flag_uz:I = 0x7f0801df

.field public static final flag_vc:I = 0x7f0801e0

.field public static final flag_ve:I = 0x7f0801e1

.field public static final flag_vg:I = 0x7f0801e2

.field public static final flag_vi:I = 0x7f0801e3

.field public static final flag_vn:I = 0x7f0801e4

.field public static final flag_ye:I = 0x7f0801e5

.field public static final flag_za:I = 0x7f0801e6

.field public static final flag_zm:I = 0x7f0801e7

.field public static final flag_zw:I = 0x7f0801e8

.field public static final flyby_shadow:I = 0x7f0801e9

.field public static final flying_stars_background_gradient:I = 0x7f0801ea

.field public static final giftcard_card_magstripe_swipe:I = 0x7f0801eb

.field public static final googleg_disabled_color_18:I = 0x7f0801ec

.field public static final googleg_standard_color_18:I = 0x7f0801ed

.field public static final hand_coin:I = 0x7f0801ee

.field public static final hand_coin_pound:I = 0x7f0801ef

.field public static final hand_coin_yen:I = 0x7f0801f0

.field public static final hardware_sq_hardware_reader_magstripe_card_swiping_40:I = 0x7f0801f1

.field public static final horizontal_radio_button_dark_selector:I = 0x7f0801f2

.field public static final horizontal_radio_button_dark_text_selector:I = 0x7f0801f3

.field public static final horizontal_radio_button_selector:I = 0x7f0801f4

.field public static final how_custom:I = 0x7f0801f5

.field public static final how_preset:I = 0x7f0801f6

.field public static final hs___star_filled:I = 0x7f0801f7

.field public static final hs___star_hollow:I = 0x7f0801f8

.field public static final hs__action_download:I = 0x7f0801f9

.field public static final hs__action_download_background:I = 0x7f0801fa

.field public static final hs__action_no:I = 0x7f0801fb

.field public static final hs__action_search:I = 0x7f0801fc

.field public static final hs__action_yes:I = 0x7f0801fd

.field public static final hs__actionbar_compat_shadow:I = 0x7f0801fe

.field public static final hs__admin_image_background:I = 0x7f0801ff

.field public static final hs__attach_screenshot_action_button:I = 0x7f080200

.field public static final hs__attachment_icon:I = 0x7f080201

.field public static final hs__attachment_progressbar_background:I = 0x7f080202

.field public static final hs__back:I = 0x7f080203

.field public static final hs__button_with_border:I = 0x7f080204

.field public static final hs__chat_bubble_admin:I = 0x7f080205

.field public static final hs__chat_bubble_rounded:I = 0x7f080206

.field public static final hs__chat_bubble_user:I = 0x7f080207

.field public static final hs__chat_notif:I = 0x7f080208

.field public static final hs__circle:I = 0x7f080209

.field public static final hs__circle_shape_scroll_jump:I = 0x7f08020a

.field public static final hs__close:I = 0x7f08020b

.field public static final hs__collapse:I = 0x7f08020c

.field public static final hs__disclosure:I = 0x7f08020d

.field public static final hs__error_icon:I = 0x7f08020e

.field public static final hs__expand:I = 0x7f08020f

.field public static final hs__logo:I = 0x7f080210

.field public static final hs__network_error:I = 0x7f080211

.field public static final hs__picker_search:I = 0x7f080212

.field public static final hs__pill:I = 0x7f080213

.field public static final hs__pill_small:I = 0x7f080214

.field public static final hs__placeholder_image:I = 0x7f080215

.field public static final hs__rating_bar:I = 0x7f080216

.field public static final hs__ratingbar_full_empty:I = 0x7f080217

.field public static final hs__ratingbar_full_filled:I = 0x7f080218

.field public static final hs__report_issue:I = 0x7f080219

.field public static final hs__ring:I = 0x7f08021a

.field public static final hs__rounded_corner_filled_rectangle_user_option_selection:I = 0x7f08021b

.field public static final hs__rounded_corner_rectangle:I = 0x7f08021c

.field public static final hs__rounded_corner_rectangle_user_option_selection:I = 0x7f08021d

.field public static final hs__screenshot_clear:I = 0x7f08021e

.field public static final hs__scroll_jump_indicator:I = 0x7f08021f

.field public static final hs__search_on_conversation_done:I = 0x7f080220

.field public static final hs__send:I = 0x7f080221

.field public static final hs__skip_pill_background:I = 0x7f080222

.field public static final hs_action_retry:I = 0x7f080223

.field public static final hud_background:I = 0x7f080224

.field public static final ic_bottom_sheet_drag_indicator:I = 0x7f080225

.field public static final ic_calendar_black_24dp:I = 0x7f080226

.field public static final ic_clear_black_24dp:I = 0x7f080227

.field public static final ic_edit_black_24dp:I = 0x7f080228

.field public static final ic_keyboard_arrow_left_black_24dp:I = 0x7f080229

.field public static final ic_keyboard_arrow_right_black_24dp:I = 0x7f08022a

.field public static final ic_logo:I = 0x7f08022b

.field public static final ic_menu_arrow_down_black_24dp:I = 0x7f08022c

.field public static final ic_menu_arrow_up_black_24dp:I = 0x7f08022d

.field public static final ic_mtrl_checked_circle:I = 0x7f08022e

.field public static final ic_mtrl_chip_checked_black:I = 0x7f08022f

.field public static final ic_mtrl_chip_checked_circle:I = 0x7f080230

.field public static final ic_mtrl_chip_close_circle:I = 0x7f080231

.field public static final ic_pos_adaptive:I = 0x7f080232

.field public static final ic_pos_adaptive_bg:I = 0x7f080233

.field public static final ic_scale:I = 0x7f080234

.field public static final icon_accessibility:I = 0x7f080235

.field public static final icon_arrow_left:I = 0x7f080236

.field public static final icon_arrow_left_40:I = 0x7f080237

.field public static final icon_arrows_up_down:I = 0x7f080238

.field public static final icon_audio_reader_120:I = 0x7f080239

.field public static final icon_audio_reader_charging_battery_120:I = 0x7f08023a

.field public static final icon_audio_reader_empty_battery_120:I = 0x7f08023b

.field public static final icon_audio_reader_full_battery_120:I = 0x7f08023c

.field public static final icon_audio_reader_high_battery_120:I = 0x7f08023d

.field public static final icon_audio_reader_low_battery_120:I = 0x7f08023e

.field public static final icon_audio_reader_mid_battery_120:I = 0x7f08023f

.field public static final icon_audio_reader_slash_120:I = 0x7f080240

.field public static final icon_blue_dot_12:I = 0x7f080241

.field public static final icon_blue_dot_24:I = 0x7f080242

.field public static final icon_blue_dot_8:I = 0x7f080243

.field public static final icon_check:I = 0x7f080244

.field public static final icon_check_thick:I = 0x7f080245

.field public static final icon_checklist_40:I = 0x7f080246

.field public static final icon_chevron_down:I = 0x7f080247

.field public static final icon_chevron_left:I = 0x7f080248

.field public static final icon_chevron_right:I = 0x7f080249

.field public static final icon_chevron_up:I = 0x7f08024a

.field public static final icon_circle_solid_24:I = 0x7f08024b

.field public static final icon_compare_24:I = 0x7f08024c

.field public static final icon_contactless_animated_border_336:I = 0x7f08024d

.field public static final icon_copy:I = 0x7f08024e

.field public static final icon_delete:I = 0x7f08024f

.field public static final icon_delete_24:I = 0x7f080250

.field public static final icon_document_128:I = 0x7f080251

.field public static final icon_dotted_cloud_120:I = 0x7f080252

.field public static final icon_export_24:I = 0x7f080253

.field public static final icon_keypad_24:I = 0x7f080254

.field public static final icon_keypad_selected_24:I = 0x7f080255

.field public static final icon_library_24:I = 0x7f080256

.field public static final icon_library_selected_24:I = 0x7f080257

.field public static final icon_loyalty_star_brand:I = 0x7f080258

.field public static final icon_magnifying_glass:I = 0x7f080259

.field public static final icon_marketing_shopping_cart_24:I = 0x7f08025a

.field public static final icon_menu_3:I = 0x7f08025b

.field public static final icon_menu_3_40:I = 0x7f08025c

.field public static final icon_menu_4:I = 0x7f08025d

.field public static final icon_open_in_browser_16:I = 0x7f08025e

.field public static final icon_r12_reader_120:I = 0x7f08025f

.field public static final icon_r12_reader_charging_battery_120:I = 0x7f080260

.field public static final icon_r12_reader_empty_battery_120:I = 0x7f080261

.field public static final icon_r12_reader_full_battery_120:I = 0x7f080262

.field public static final icon_r12_reader_high_battery_120:I = 0x7f080263

.field public static final icon_r12_reader_low_battery_120:I = 0x7f080264

.field public static final icon_r12_reader_mid_battery_120:I = 0x7f080265

.field public static final icon_r12_reader_no_icon_144:I = 0x7f080266

.field public static final icon_r12_reader_slash_120:I = 0x7f080267

.field public static final icon_scale_connected:I = 0x7f080268

.field public static final icon_scale_disconnected:I = 0x7f080269

.field public static final icon_settings:I = 0x7f08026a

.field public static final icon_tree_36:I = 0x7f08026b

.field public static final icon_triangle_warning_24:I = 0x7f08026c

.field public static final icon_triangle_warning_40:I = 0x7f08026d

.field public static final icon_x:I = 0x7f08026e

.field public static final icon_x_40:I = 0x7f08026f

.field public static final icon_x_thick:I = 0x7f080270

.field public static final info_box_background:I = 0x7f080271

.field public static final info_box_icon:I = 0x7f080272

.field public static final installments_qr_code_background:I = 0x7f080273

.field public static final instant_deposit_upsell_banner_background_selector:I = 0x7f080274

.field public static final instant_transfer_button_background_blue:I = 0x7f080275

.field public static final instant_transfer_button_background_white:I = 0x7f080276

.field public static final interac_tap:I = 0x7f080277

.field public static final invoice_calendar_background_selector:I = 0x7f080278

.field public static final invoice_calendar_divider:I = 0x7f080279

.field public static final invoice_new_feature_background_selector:I = 0x7f08027a

.field public static final invoice_timeline_bullet:I = 0x7f08027b

.field public static final invoice_timeline_bullet_highlight:I = 0x7f08027c

.field public static final invoices_app_color:I = 0x7f08027d

.field public static final invoices_app_icon:I = 0x7f08027e

.field public static final keypad_selector:I = 0x7f08027f

.field public static final landing_reader:I = 0x7f080280

.field public static final library_list_create_new_plus_icon:I = 0x7f080281

.field public static final lightning_bolt:I = 0x7f080282

.field public static final location_background:I = 0x7f080283

.field public static final location_button_background_default:I = 0x7f080284

.field public static final location_button_background_pressed:I = 0x7f080285

.field public static final location_button_background_selector:I = 0x7f080286

.field public static final logo_dialog:I = 0x7f080287

.field public static final loyalty_action_bar_button_background:I = 0x7f080288

.field public static final loyalty_cash_logo:I = 0x7f080289

.field public static final loyalty_enrollment_button_background:I = 0x7f08028a

.field public static final magnifying_glass:I = 0x7f08028b

.field public static final marin_app_loading:I = 0x7f08028c

.field public static final marin_app_loading_light:I = 0x7f08028d

.field public static final marin_app_loading_light_large:I = 0x7f08028e

.field public static final marin_app_loading_logo:I = 0x7f08028f

.field public static final marin_app_loading_logo_large_light:I = 0x7f080290

.field public static final marin_app_loading_logo_light:I = 0x7f080291

.field public static final marin_badge_background:I = 0x7f080292

.field public static final marin_badge_background_fatal:I = 0x7f080293

.field public static final marin_badge_background_red:I = 0x7f080294

.field public static final marin_badge_fatal_border_dark_gray:I = 0x7f080295

.field public static final marin_badge_fatal_border_dark_gray_pressed:I = 0x7f080296

.field public static final marin_badge_fatal_border_ultra_light_gray:I = 0x7f080297

.field public static final marin_badge_fatal_border_ultra_light_gray_pressed:I = 0x7f080298

.field public static final marin_badge_rounded_blue_border_dark_gray_2dp:I = 0x7f080299

.field public static final marin_badge_rounded_blue_border_dark_gray_pressed_2dp:I = 0x7f08029a

.field public static final marin_badge_rounded_blue_border_ultra_light_gray_2dp:I = 0x7f08029b

.field public static final marin_badge_rounded_blue_border_ultra_light_gray_pressed_2dp:I = 0x7f08029c

.field public static final marin_badge_rounded_red_border_dark_gray_2dp:I = 0x7f08029d

.field public static final marin_badge_rounded_red_border_dark_gray_pressed_2dp:I = 0x7f08029e

.field public static final marin_badge_rounded_red_border_ultra_light_gray_2dp:I = 0x7f08029f

.field public static final marin_badge_rounded_red_border_ultra_light_gray_pressed_2dp:I = 0x7f0802a0

.field public static final marin_barely_dark_translucent_pressed:I = 0x7f0802a1

.field public static final marin_black_tranpsarent_fifty_border_white_1dp:I = 0x7f0802a2

.field public static final marin_black_tranpsarent_seventyfive_border_white_1dp:I = 0x7f0802a3

.field public static final marin_black_transparent_forty:I = 0x7f0802a4

.field public static final marin_black_transparent_thirty:I = 0x7f0802a5

.field public static final marin_blue:I = 0x7f0802a6

.field public static final marin_blue_border_right_dark_blue_1px:I = 0x7f0802a7

.field public static final marin_blue_disabled:I = 0x7f0802a8

.field public static final marin_blue_pressed:I = 0x7f0802a9

.field public static final marin_blue_pressed_border_right_dark_blue_1px:I = 0x7f0802aa

.field public static final marin_blue_selected:I = 0x7f0802ab

.field public static final marin_blue_selected_pressed:I = 0x7f0802ac

.field public static final marin_button_check_off:I = 0x7f0802ad

.field public static final marin_button_check_off_disabled:I = 0x7f0802ae

.field public static final marin_button_check_off_focused:I = 0x7f0802af

.field public static final marin_button_check_off_pressed:I = 0x7f0802b0

.field public static final marin_button_check_on:I = 0x7f0802b1

.field public static final marin_button_check_on_disabled:I = 0x7f0802b2

.field public static final marin_button_check_on_focused:I = 0x7f0802b3

.field public static final marin_button_check_on_pressed:I = 0x7f0802b4

.field public static final marin_button_radio_off:I = 0x7f0802b5

.field public static final marin_button_radio_off_disabled:I = 0x7f0802b6

.field public static final marin_button_radio_off_focused:I = 0x7f0802b7

.field public static final marin_button_radio_off_focused_red:I = 0x7f0802b8

.field public static final marin_button_radio_off_pressed:I = 0x7f0802b9

.field public static final marin_button_radio_on:I = 0x7f0802ba

.field public static final marin_button_radio_on_disabled:I = 0x7f0802bb

.field public static final marin_button_radio_on_focused:I = 0x7f0802bc

.field public static final marin_button_radio_on_focused_red:I = 0x7f0802bd

.field public static final marin_button_radio_on_pressed:I = 0x7f0802be

.field public static final marin_button_radio_on_pressed_red:I = 0x7f0802bf

.field public static final marin_button_radio_on_red:I = 0x7f0802c0

.field public static final marin_cart_quantity_tile:I = 0x7f0802c1

.field public static final marin_clear:I = 0x7f0802c2

.field public static final marin_clear_border_bottom_dark_gray_1px:I = 0x7f0802c3

.field public static final marin_clear_border_bottom_dim_translucent_1px:I = 0x7f0802c4

.field public static final marin_clear_border_bottom_light_gray_1dp:I = 0x7f0802c5

.field public static final marin_clear_border_bottom_light_gray_1px:I = 0x7f0802c6

.field public static final marin_clear_border_light_gray_1px:I = 0x7f0802c7

.field public static final marin_clear_border_top_bottom_light_gray_1px:I = 0x7f0802c8

.field public static final marin_clear_border_top_light_gray:I = 0x7f0802c9

.field public static final marin_clear_border_top_right_light_gray_1px:I = 0x7f0802ca

.field public static final marin_cursor_medium_gray:I = 0x7f0802cb

.field public static final marin_custom_tile:I = 0x7f0802cc

.field public static final marin_dark_gray:I = 0x7f0802cd

.field public static final marin_dark_gray_pressed:I = 0x7f0802ce

.field public static final marin_dark_gray_pressed_rounded:I = 0x7f0802cf

.field public static final marin_dark_gray_rounded:I = 0x7f0802d0

.field public static final marin_dark_translucent:I = 0x7f0802d1

.field public static final marin_dark_translucent_pressed:I = 0x7f0802d2

.field public static final marin_darker_gray:I = 0x7f0802d3

.field public static final marin_darker_gray_pressed:I = 0x7f0802d4

.field public static final marin_dialog_full_holo_light:I = 0x7f0802d5

.field public static final marin_dim_translucent_pressed:I = 0x7f0802d6

.field public static final marin_dim_translucent_pressed_border_bottom_clear_1px:I = 0x7f0802d7

.field public static final marin_dim_translucent_pressed_border_top_clear_1px:I = 0x7f0802d8

.field public static final marin_divider_charge_tickets:I = 0x7f0802d9

.field public static final marin_divider_horizontal_clear_20dp:I = 0x7f0802da

.field public static final marin_divider_horizontal_clear_buyer_gutter_half:I = 0x7f0802db

.field public static final marin_divider_horizontal_clear_small:I = 0x7f0802dc

.field public static final marin_divider_horizontal_dark_gray:I = 0x7f0802dd

.field public static final marin_divider_horizontal_darker_gray:I = 0x7f0802de

.field public static final marin_divider_horizontal_gutter_quarter:I = 0x7f0802df

.field public static final marin_divider_horizontal_light_gray:I = 0x7f0802e0

.field public static final marin_divider_horizontal_light_gray_padding_gutter_half:I = 0x7f0802e1

.field public static final marin_divider_horizontal_light_gray_pressed:I = 0x7f0802e2

.field public static final marin_divider_square_clear_medium:I = 0x7f0802e3

.field public static final marin_divider_vertical_blue_charge:I = 0x7f0802e4

.field public static final marin_divider_vertical_clear_20dp:I = 0x7f0802e5

.field public static final marin_divider_vertical_clear_gutter_half:I = 0x7f0802e6

.field public static final marin_divider_vertical_light_gray:I = 0x7f0802e7

.field public static final marin_divider_vertical_light_gray_28dp:I = 0x7f0802e8

.field public static final marin_divider_vertical_light_grey_charge:I = 0x7f0802e9

.field public static final marin_drawer_background:I = 0x7f0802ea

.field public static final marin_drawer_background_pressed:I = 0x7f0802eb

.field public static final marin_edit_text_disabled:I = 0x7f0802ec

.field public static final marin_edit_text_focused:I = 0x7f0802ed

.field public static final marin_edit_text_normal:I = 0x7f0802ee

.field public static final marin_education_gradient:I = 0x7f0802ef

.field public static final marin_green:I = 0x7f0802f0

.field public static final marin_green_border_bottom_1dp:I = 0x7f0802f1

.field public static final marin_green_pressed:I = 0x7f0802f2

.field public static final marin_green_pressed_border_bottom_clear_1px:I = 0x7f0802f3

.field public static final marin_image_border_light_gray_1px:I = 0x7f0802f4

.field public static final marin_image_border_light_gray_pressed_1px:I = 0x7f0802f5

.field public static final marin_light_gray:I = 0x7f0802f6

.field public static final marin_light_gray_border_top_1dp:I = 0x7f0802f7

.field public static final marin_light_gray_left_border_ultra_light_gray_pressed_1px:I = 0x7f0802f8

.field public static final marin_light_gray_pressed:I = 0x7f0802f9

.field public static final marin_medium_gray:I = 0x7f0802fa

.field public static final marin_medium_gray_border_bottom_medium_gray_pressed_1px:I = 0x7f0802fb

.field public static final marin_medium_gray_pressed:I = 0x7f0802fc

.field public static final marin_photo_placeholder:I = 0x7f0802fd

.field public static final marin_primary_button_disabled:I = 0x7f0802fe

.field public static final marin_progress_horizontal:I = 0x7f0802ff

.field public static final marin_progress_large_dark:I = 0x7f080300

.field public static final marin_progress_large_light:I = 0x7f080301

.field public static final marin_progress_medium_dark:I = 0x7f080302

.field public static final marin_progress_medium_light:I = 0x7f080303

.field public static final marin_progress_primary:I = 0x7f080304

.field public static final marin_progress_secondary:I = 0x7f080305

.field public static final marin_progress_small_dark:I = 0x7f080306

.field public static final marin_progress_small_light:I = 0x7f080307

.field public static final marin_progress_spinner_large_dark:I = 0x7f080308

.field public static final marin_progress_spinner_large_light:I = 0x7f080309

.field public static final marin_progress_spinner_medium_dark:I = 0x7f08030a

.field public static final marin_progress_spinner_medium_light:I = 0x7f08030b

.field public static final marin_progress_spinner_small_dark:I = 0x7f08030c

.field public static final marin_progress_spinner_small_light:I = 0x7f08030d

.field public static final marin_progress_spinner_tiny:I = 0x7f08030e

.field public static final marin_progress_tiny:I = 0x7f08030f

.field public static final marin_red:I = 0x7f080310

.field public static final marin_screen_scrim:I = 0x7f080311

.field public static final marin_scrollbar_handle:I = 0x7f080312

.field public static final marin_section_header_gradient_background:I = 0x7f080313

.field public static final marin_selector_badge_dark_gray:I = 0x7f080314

.field public static final marin_selector_badge_fatal_border_dark_gray:I = 0x7f080315

.field public static final marin_selector_badge_fatal_border_ultra_light_gray:I = 0x7f080316

.field public static final marin_selector_badge_red_border_dark_gray:I = 0x7f080317

.field public static final marin_selector_badge_red_border_ultra_light_gray:I = 0x7f080318

.field public static final marin_selector_badge_ultra_light_gray:I = 0x7f080319

.field public static final marin_selector_barely_dark_translucent:I = 0x7f08031a

.field public static final marin_selector_black_transparent_fifty_border_white_transparent_twenty:I = 0x7f08031b

.field public static final marin_selector_black_transparent_fifty_pressed:I = 0x7f08031c

.field public static final marin_selector_black_transparent_thirty:I = 0x7f08031d

.field public static final marin_selector_blue:I = 0x7f08031e

.field public static final marin_selector_blue_pressed:I = 0x7f08031f

.field public static final marin_selector_blue_pressed_blue_selected:I = 0x7f080320

.field public static final marin_selector_blue_selected_light_gray_pressed:I = 0x7f080321

.field public static final marin_selector_blue_white_when_disabled:I = 0x7f080322

.field public static final marin_selector_border_bottom_1dp_ultra_light_gray:I = 0x7f080323

.field public static final marin_selector_button:I = 0x7f080324

.field public static final marin_selector_button_bluebackground_inline:I = 0x7f080325

.field public static final marin_selector_button_check:I = 0x7f080326

.field public static final marin_selector_button_edit_text:I = 0x7f080327

.field public static final marin_selector_button_inline:I = 0x7f080328

.field public static final marin_selector_button_radio:I = 0x7f080329

.field public static final marin_selector_button_radio_red:I = 0x7f08032a

.field public static final marin_selector_button_white_medium_gray_border:I = 0x7f08032b

.field public static final marin_selector_checked_dark_gray:I = 0x7f08032c

.field public static final marin_selector_clear_border_bottom_light_gray_1px:I = 0x7f08032d

.field public static final marin_selector_clear_border_left_light_gray_1px:I = 0x7f08032e

.field public static final marin_selector_clear_border_top_1px:I = 0x7f08032f

.field public static final marin_selector_clear_pressed_ultra_light_gray_pressed_checked_dark_gray:I = 0x7f080330

.field public static final marin_selector_clear_ultra_light_gray_pressed:I = 0x7f080331

.field public static final marin_selector_dark_gray:I = 0x7f080332

.field public static final marin_selector_dark_gray_pressed_darker_gray_selected:I = 0x7f080333

.field public static final marin_selector_dark_gray_rounded:I = 0x7f080334

.field public static final marin_selector_dark_gray_when_pressed:I = 0x7f080335

.field public static final marin_selector_dark_translucent:I = 0x7f080336

.field public static final marin_selector_darker_blue:I = 0x7f080337

.field public static final marin_selector_dim_translucent_pressed:I = 0x7f080338

.field public static final marin_selector_dim_translucent_pressed_border_bottom_clear_1px:I = 0x7f080339

.field public static final marin_selector_dim_translucent_pressed_border_top_clear_1px:I = 0x7f08033a

.field public static final marin_selector_drawer_pressed:I = 0x7f08033b

.field public static final marin_selector_edit_text:I = 0x7f08033c

.field public static final marin_selector_edit_text_border:I = 0x7f08033d

.field public static final marin_selector_edit_text_border_red:I = 0x7f08033e

.field public static final marin_selector_enable_setting_button:I = 0x7f08033f

.field public static final marin_selector_glyph_button_inline:I = 0x7f080340

.field public static final marin_selector_green_pressed:I = 0x7f080341

.field public static final marin_selector_green_pressed_border:I = 0x7f080342

.field public static final marin_selector_image_border_light_gray:I = 0x7f080343

.field public static final marin_selector_keypad:I = 0x7f080344

.field public static final marin_selector_list:I = 0x7f080345

.field public static final marin_selector_list_half_gutter:I = 0x7f080346

.field public static final marin_selector_list_top_bottom_border_light_gray_1px:I = 0x7f080347

.field public static final marin_selector_medium_gray:I = 0x7f080348

.field public static final marin_selector_red:I = 0x7f080349

.field public static final marin_selector_scrim:I = 0x7f08034a

.field public static final marin_selector_switch_inner:I = 0x7f08034b

.field public static final marin_selector_switch_track:I = 0x7f08034c

.field public static final marin_selector_ultra_light_gray:I = 0x7f08034d

.field public static final marin_selector_ultra_light_gray_border_bottom_light_gray_1px:I = 0x7f08034e

.field public static final marin_selector_ultra_light_gray_pressed:I = 0x7f08034f

.field public static final marin_selector_ultra_light_gray_pressed_border_bottom_clear_1dp:I = 0x7f080350

.field public static final marin_selector_ultra_light_gray_pressed_border_bottom_clear_2dp:I = 0x7f080351

.field public static final marin_selector_ultra_light_gray_when_pressed:I = 0x7f080352

.field public static final marin_selector_ultra_light_gray_when_pressed_border_bottom_clear_1dp:I = 0x7f080353

.field public static final marin_selector_ultra_light_gray_when_pressed_border_bottom_clear_2dp:I = 0x7f080354

.field public static final marin_selector_white:I = 0x7f080355

.field public static final marin_selector_white_border_left_ultra_light_gray_1px:I = 0x7f080356

.field public static final marin_selector_white_border_light_gray_1px_blue_selected:I = 0x7f080357

.field public static final marin_selector_white_disabled_clear_activated_blue:I = 0x7f080358

.field public static final marin_selector_white_ultra_light_gray_pressed:I = 0x7f080359

.field public static final marin_shape_text_box:I = 0x7f08035a

.field public static final marin_shape_text_box_blue:I = 0x7f08035b

.field public static final marin_shape_text_box_dark:I = 0x7f08035c

.field public static final marin_shape_text_box_red:I = 0x7f08035d

.field public static final marin_switch_bg:I = 0x7f08035e

.field public static final marin_switch_bg_disabled:I = 0x7f08035f

.field public static final marin_switch_bg_focused:I = 0x7f080360

.field public static final marin_switch_thumb:I = 0x7f080361

.field public static final marin_switch_thumb_activated:I = 0x7f080362

.field public static final marin_switch_thumb_disabled:I = 0x7f080363

.field public static final marin_switch_thumb_pressed:I = 0x7f080364

.field public static final marin_thumb:I = 0x7f080365

.field public static final marin_tool_tip_left_up_arrow:I = 0x7f080366

.field public static final marin_track:I = 0x7f080367

.field public static final marin_transition_clear_to_ultra_light_gray:I = 0x7f080368

.field public static final marin_ultra_light_gray:I = 0x7f080369

.field public static final marin_ultra_light_gray_border_blue_1dp:I = 0x7f08036a

.field public static final marin_ultra_light_gray_border_blue_1px:I = 0x7f08036b

.field public static final marin_ultra_light_gray_border_blue_1px_no_padding:I = 0x7f08036c

.field public static final marin_ultra_light_gray_border_bottom_clear_1dp:I = 0x7f08036d

.field public static final marin_ultra_light_gray_border_bottom_clear_2dp:I = 0x7f08036e

.field public static final marin_ultra_light_gray_border_bottom_light_gray_1dp:I = 0x7f08036f

.field public static final marin_ultra_light_gray_border_bottom_light_gray_1px:I = 0x7f080370

.field public static final marin_ultra_light_gray_border_left_light_gray_1px:I = 0x7f080371

.field public static final marin_ultra_light_gray_border_light_gray_1px:I = 0x7f080372

.field public static final marin_ultra_light_gray_border_light_gray_1px_no_padding:I = 0x7f080373

.field public static final marin_ultra_light_gray_border_top_blue_1px:I = 0x7f080374

.field public static final marin_ultra_light_gray_border_top_bottom_light_gray_1px:I = 0x7f080375

.field public static final marin_ultra_light_gray_border_top_light_gray_1px:I = 0x7f080376

.field public static final marin_ultra_light_gray_border_top_right_light_gray_1px:I = 0x7f080377

.field public static final marin_ultra_light_gray_pressed:I = 0x7f080378

.field public static final marin_ultra_light_gray_pressed_border_bottom_clear_1dp:I = 0x7f080379

.field public static final marin_ultra_light_gray_pressed_border_bottom_clear_2dp:I = 0x7f08037a

.field public static final marin_ultra_light_gray_pressed_border_bottom_light_gray_1px:I = 0x7f08037b

.field public static final marin_ultra_light_gray_pressed_border_light_gray_1px:I = 0x7f08037c

.field public static final marin_ultra_light_gray_rounded:I = 0x7f08037d

.field public static final marin_white:I = 0x7f08037e

.field public static final marin_white_border_blue_1dp:I = 0x7f08037f

.field public static final marin_white_border_bottom_light_gray_1px:I = 0x7f080380

.field public static final marin_white_border_bottom_medium_gray_1px:I = 0x7f080381

.field public static final marin_white_border_left_light_gray_1px:I = 0x7f080382

.field public static final marin_white_border_light_gray:I = 0x7f080383

.field public static final marin_white_border_light_gray_2dp:I = 0x7f080384

.field public static final marin_white_border_light_gray_pressed:I = 0x7f080385

.field public static final marin_white_border_medium_gray_1px:I = 0x7f080386

.field public static final marin_white_border_top_light_gray_1px:I = 0x7f080387

.field public static final marin_white_to_clear_gradient:I = 0x7f080388

.field public static final marketing_bag_purse_24:I = 0x7f080389

.field public static final marketing_device_landline_handset_24:I = 0x7f08038a

.field public static final marketing_triangle_warning_24:I = 0x7f08038b

.field public static final misc_address_book_40:I = 0x7f08038c

.field public static final misc_bag_purse_80:I = 0x7f08038d

.field public static final misc_bell_service_24:I = 0x7f08038e

.field public static final misc_logo_square_80:I = 0x7f08038f

.field public static final misc_shopping_cart_40:I = 0x7f080390

.field public static final misc_tag_40:I = 0x7f080391

.field public static final misc_tag_percent_28:I = 0x7f080392

.field public static final mtrl_dialog_background:I = 0x7f080393

.field public static final mtrl_dropdown_arrow:I = 0x7f080394

.field public static final mtrl_ic_arrow_drop_down:I = 0x7f080395

.field public static final mtrl_ic_arrow_drop_up:I = 0x7f080396

.field public static final mtrl_ic_cancel:I = 0x7f080397

.field public static final mtrl_ic_error:I = 0x7f080398

.field public static final mtrl_popupmenu_background:I = 0x7f080399

.field public static final mtrl_popupmenu_background_dark:I = 0x7f08039a

.field public static final mtrl_tabs_default_indicator:I = 0x7f08039b

.field public static final navigation_empty_icon:I = 0x7f08039c

.field public static final nfc_card_perspective:I = 0x7f08039d

.field public static final nfc_icon:I = 0x7f08039e

.field public static final no_invoices:I = 0x7f08039f

.field public static final no_source_icon:I = 0x7f0803a0

.field public static final noho_actionbar_icon_back_arrow:I = 0x7f0803a1

.field public static final noho_actionbar_icon_burger:I = 0x7f0803a2

.field public static final noho_actionbar_icon_x:I = 0x7f0803a3

.field public static final noho_background:I = 0x7f0803a4

.field public static final noho_border_light:I = 0x7f0803a5

.field public static final noho_bordered_selector_edit_text:I = 0x7f0803a6

.field public static final noho_button_destructive_background_disabled:I = 0x7f0803a7

.field public static final noho_button_destructive_background_enabled:I = 0x7f0803a8

.field public static final noho_button_destructive_background_pressed:I = 0x7f0803a9

.field public static final noho_button_link_background_disabled:I = 0x7f0803aa

.field public static final noho_button_link_background_enabled:I = 0x7f0803ab

.field public static final noho_button_link_background_pressed:I = 0x7f0803ac

.field public static final noho_button_primary_background_disabled:I = 0x7f0803ad

.field public static final noho_button_primary_background_enabled:I = 0x7f0803ae

.field public static final noho_button_primary_background_pressed:I = 0x7f0803af

.field public static final noho_button_secondary_background_disabled:I = 0x7f0803b0

.field public static final noho_button_secondary_background_enabled:I = 0x7f0803b1

.field public static final noho_button_secondary_background_pressed:I = 0x7f0803b2

.field public static final noho_button_tertiary_background_disabled:I = 0x7f0803b3

.field public static final noho_button_tertiary_background_enabled:I = 0x7f0803b4

.field public static final noho_button_tertiary_background_pressed:I = 0x7f0803b5

.field public static final noho_check_vector_on:I = 0x7f0803b6

.field public static final noho_check_vector_selector:I = 0x7f0803b7

.field public static final noho_clear:I = 0x7f0803b8

.field public static final noho_divider_both_hairline:I = 0x7f0803b9

.field public static final noho_divider_column_left:I = 0x7f0803ba

.field public static final noho_divider_column_right:I = 0x7f0803bb

.field public static final noho_divider_gutter:I = 0x7f0803bc

.field public static final noho_divider_gutter_half:I = 0x7f0803bd

.field public static final noho_divider_horizontal_hairline:I = 0x7f0803be

.field public static final noho_divider_horizontal_hairline_padding:I = 0x7f0803bf

.field public static final noho_divider_vertical_hairline:I = 0x7f0803c0

.field public static final noho_dropdown_button:I = 0x7f0803c1

.field public static final noho_dropdown_button_closed:I = 0x7f0803c2

.field public static final noho_dropdown_button_open:I = 0x7f0803c3

.field public static final noho_dropdown_menu_background:I = 0x7f0803c4

.field public static final noho_dropdown_menu_item_background:I = 0x7f0803c5

.field public static final noho_dropdown_menu_item_background_normal:I = 0x7f0803c6

.field public static final noho_dropdown_menu_item_background_pressed:I = 0x7f0803c7

.field public static final noho_edit_background:I = 0x7f0803c8

.field public static final noho_edit_background_base:I = 0x7f0803c9

.field public static final noho_edit_background_base_first:I = 0x7f0803ca

.field public static final noho_edit_background_base_middle:I = 0x7f0803cb

.field public static final noho_edit_background_disabled:I = 0x7f0803cc

.field public static final noho_edit_background_disabled_first:I = 0x7f0803cd

.field public static final noho_edit_background_disabled_middle:I = 0x7f0803ce

.field public static final noho_edit_background_errored:I = 0x7f0803cf

.field public static final noho_edit_background_selected:I = 0x7f0803d0

.field public static final noho_edit_background_validated:I = 0x7f0803d1

.field public static final noho_edit_cursor_drawable:I = 0x7f0803d2

.field public static final noho_edit_text_background:I = 0x7f0803d3

.field public static final noho_edit_text_background_disabled:I = 0x7f0803d4

.field public static final noho_edit_text_background_enabled:I = 0x7f0803d5

.field public static final noho_edit_text_blank:I = 0x7f0803d6

.field public static final noho_edit_text_focused:I = 0x7f0803d7

.field public static final noho_icon_button_background:I = 0x7f0803d8

.field public static final noho_icon_button_background_normal:I = 0x7f0803d9

.field public static final noho_icon_button_background_pressed:I = 0x7f0803da

.field public static final noho_notification_balloon:I = 0x7f0803db

.field public static final noho_notification_fatal_balloon:I = 0x7f0803dc

.field public static final noho_notification_fatal_filterout:I = 0x7f0803dd

.field public static final noho_notification_filterout:I = 0x7f0803de

.field public static final noho_notification_highpri_balloon:I = 0x7f0803df

.field public static final noho_radio_vector_off:I = 0x7f0803e0

.field public static final noho_radio_vector_on:I = 0x7f0803e1

.field public static final noho_radio_vector_selector:I = 0x7f0803e2

.field public static final noho_right_caret:I = 0x7f0803e3

.field public static final noho_row_background:I = 0x7f0803e4

.field public static final noho_row_background_activated:I = 0x7f0803e5

.field public static final noho_row_background_pressed:I = 0x7f0803e6

.field public static final noho_selectable_background:I = 0x7f0803e7

.field public static final noho_selectable_background_checked:I = 0x7f0803e8

.field public static final noho_selectable_background_disabled:I = 0x7f0803e9

.field public static final noho_selectable_background_normal:I = 0x7f0803ea

.field public static final noho_selector_destructive_button_background:I = 0x7f0803eb

.field public static final noho_selector_dialog_button_background:I = 0x7f0803ec

.field public static final noho_selector_link_button_background:I = 0x7f0803ed

.field public static final noho_selector_primary_button_background:I = 0x7f0803ee

.field public static final noho_selector_primary_tutorial_button_background:I = 0x7f0803ef

.field public static final noho_selector_row_background:I = 0x7f0803f0

.field public static final noho_selector_secondary_button_background:I = 0x7f0803f1

.field public static final noho_selector_tertiary_button_background:I = 0x7f0803f2

.field public static final noho_standard_cyan:I = 0x7f0803f3

.field public static final noho_standard_cyan_dark:I = 0x7f0803f4

.field public static final noho_standard_gray_10:I = 0x7f0803f5

.field public static final noho_standard_gray_20:I = 0x7f0803f6

.field public static final noho_standard_night_almost_black:I = 0x7f0803f7

.field public static final noho_standard_night_dark:I = 0x7f0803f8

.field public static final noho_standard_night_extra_dark:I = 0x7f0803f9

.field public static final noho_standard_night_gray:I = 0x7f0803fa

.field public static final noho_standard_white:I = 0x7f0803fb

.field public static final noho_switch_base:I = 0x7f0803fc

.field public static final noho_switch_base_off:I = 0x7f0803fd

.field public static final noho_switch_base_on:I = 0x7f0803fe

.field public static final noho_switch_thumb:I = 0x7f0803ff

.field public static final noho_tutorial_button_primary_background_disabled:I = 0x7f080400

.field public static final noho_tutorial_button_primary_background_enabled:I = 0x7f080401

.field public static final noho_tutorial_button_primary_background_pressed:I = 0x7f080402

.field public static final noho_white_border_bottom_light_gray_1px:I = 0x7f080403

.field public static final notification_action_background:I = 0x7f080404

.field public static final notification_bg:I = 0x7f080405

.field public static final notification_bg_low:I = 0x7f080406

.field public static final notification_bg_low_normal:I = 0x7f080407

.field public static final notification_bg_low_pressed:I = 0x7f080408

.field public static final notification_bg_normal:I = 0x7f080409

.field public static final notification_bg_normal_pressed:I = 0x7f08040a

.field public static final notification_icon_background:I = 0x7f08040b

.field public static final notification_row_background:I = 0x7f08040c

.field public static final notification_square:I = 0x7f08040d

.field public static final notification_square_error:I = 0x7f08040e

.field public static final notification_square_okay:I = 0x7f08040f

.field public static final notification_template_icon_bg:I = 0x7f080410

.field public static final notification_template_icon_low_bg:I = 0x7f080411

.field public static final notification_tile_bg:I = 0x7f080412

.field public static final notify_panel_notification_icon_bg:I = 0x7f080413

.field public static final onboarding_paragraph_error:I = 0x7f080414

.field public static final onboarding_paragraph_info:I = 0x7f080415

.field public static final order_entry_badge_background:I = 0x7f080416

.field public static final order_entry_badge_background_fatal:I = 0x7f080417

.field public static final order_entry_badge_background_red:I = 0x7f080418

.field public static final order_entry_wide_badge_background:I = 0x7f080419

.field public static final order_entry_wide_badge_background_fatal:I = 0x7f08041a

.field public static final order_entry_wide_badge_background_red:I = 0x7f08041b

.field public static final order_reader_all_hardware:I = 0x7f08041c

.field public static final order_reader_r12:I = 0x7f08041d

.field public static final order_reader_r4:I = 0x7f08041e

.field public static final order_select_address_layout_background:I = 0x7f08041f

.field public static final order_select_address_layout_default:I = 0x7f080420

.field public static final order_select_address_layout_selected:I = 0x7f080421

.field public static final order_square_card:I = 0x7f080422

.field public static final overflow_menu:I = 0x7f080423

.field public static final padlock_pin_check_selector:I = 0x7f080424

.field public static final padlock_pin_digit_selector:I = 0x7f080425

.field public static final pairing_screen_animation:I = 0x7f080426

.field public static final panel_background:I = 0x7f080427

.field public static final panel_background_clear_bottom_border:I = 0x7f080428

.field public static final panel_background_translucent:I = 0x7f080429

.field public static final payment_card_amex_24:I = 0x7f08042a

.field public static final payment_card_chip_24:I = 0x7f08042b

.field public static final payment_card_diners_club_24:I = 0x7f08042c

.field public static final payment_card_discover_24:I = 0x7f08042d

.field public static final payment_card_gift_card_24:I = 0x7f08042e

.field public static final payment_card_gift_ribbon_40:I = 0x7f08042f

.field public static final payment_card_ic_24:I = 0x7f080430

.field public static final payment_card_id_24:I = 0x7f080431

.field public static final payment_card_interac_24:I = 0x7f080432

.field public static final payment_card_jcb_24:I = 0x7f080433

.field public static final payment_card_mastercard_24:I = 0x7f080434

.field public static final payment_card_quicpay_24:I = 0x7f080435

.field public static final payment_card_swipe_24:I = 0x7f080436

.field public static final payment_card_union_pay_24:I = 0x7f080437

.field public static final payment_card_visa_24:I = 0x7f080438

.field public static final payment_divider:I = 0x7f080439

.field public static final payment_pad_landscape_background_edit:I = 0x7f08043a

.field public static final payment_pad_landscape_background_sale:I = 0x7f08043b

.field public static final payment_pad_portrait_background_edit:I = 0x7f08043c

.field public static final payment_pad_portrait_background_sale:I = 0x7f08043d

.field public static final payment_pad_portrait_background_sale_no_border:I = 0x7f08043e

.field public static final pdf_icon:I = 0x7f08043f

.field public static final plan_circle:I = 0x7f080440

.field public static final plug:I = 0x7f080441

.field public static final qr_code_border:I = 0x7f080442

.field public static final quick_amounts_preview_border:I = 0x7f080443

.field public static final r12_education_amber_led_animation:I = 0x7f080444

.field public static final r12_education_cable:I = 0x7f080445

.field public static final r12_education_chip_card:I = 0x7f080446

.field public static final r12_education_dots_amber_1:I = 0x7f080447

.field public static final r12_education_dots_amber_2:I = 0x7f080448

.field public static final r12_education_dots_amber_3:I = 0x7f080449

.field public static final r12_education_dots_amber_4:I = 0x7f08044a

.field public static final r12_education_dots_green_1:I = 0x7f08044b

.field public static final r12_education_dots_green_2:I = 0x7f08044c

.field public static final r12_education_dots_green_3:I = 0x7f08044d

.field public static final r12_education_dots_green_4:I = 0x7f08044e

.field public static final r12_education_dots_orange_1:I = 0x7f08044f

.field public static final r12_education_dots_orange_2:I = 0x7f080450

.field public static final r12_education_dots_orange_3:I = 0x7f080451

.field public static final r12_education_dots_orange_4:I = 0x7f080452

.field public static final r12_education_phone_apple_pay:I = 0x7f080453

.field public static final r12_education_phone_r4:I = 0x7f080454

.field public static final r12_education_r12:I = 0x7f080455

.field public static final r12_education_sticker:I = 0x7f080456

.field public static final r12_education_sticker_au:I = 0x7f080457

.field public static final r12_education_sticker_ca:I = 0x7f080458

.field public static final r12_education_sticker_gb:I = 0x7f080459

.field public static final r12_education_sticker_jp:I = 0x7f08045a

.field public static final r12_education_swipe_card:I = 0x7f08045b

.field public static final r12_education_tap_card_with_hand:I = 0x7f08045c

.field public static final r12_education_video:I = 0x7f08045d

.field public static final r12_lights:I = 0x7f08045e

.field public static final r12_lights_l:I = 0x7f08045f

.field public static final r12_lights_m:I = 0x7f080460

.field public static final r12_lights_xl:I = 0x7f080461

.field public static final r12_lights_xxl:I = 0x7f080462

.field public static final r12_no_lights:I = 0x7f080463

.field public static final r12_no_lights_l:I = 0x7f080464

.field public static final r12_no_lights_m:I = 0x7f080465

.field public static final r12_no_lights_xl:I = 0x7f080466

.field public static final r12_no_lights_xxl:I = 0x7f080467

.field public static final r12_select:I = 0x7f080468

.field public static final r6_first_time_still:I = 0x7f080469

.field public static final rate_background:I = 0x7f08046a

.field public static final rate_cnp_aud:I = 0x7f08046b

.field public static final rate_cnp_cad:I = 0x7f08046c

.field public static final rate_cnp_gbp:I = 0x7f08046d

.field public static final rate_cnp_jpy:I = 0x7f08046e

.field public static final rate_cnp_usd:I = 0x7f08046f

.field public static final rate_dip:I = 0x7f080470

.field public static final rate_dip_cad:I = 0x7f080471

.field public static final rate_swipe_cad:I = 0x7f080472

.field public static final rate_swipe_dollar:I = 0x7f080473

.field public static final rate_swipe_gbp:I = 0x7f080474

.field public static final rate_tap_interac:I = 0x7f080475

.field public static final rate_tap_visa:I = 0x7f080476

.field public static final read_notification_dot:I = 0x7f080477

.field public static final reader_learn_more_r12:I = 0x7f080478

.field public static final reader_learn_more_r4:I = 0x7f080479

.field public static final reader_magstripe_card_swiping_96:I = 0x7f08047a

.field public static final recurring_row_icon:I = 0x7f08047b

.field public static final reminders_row_icon:I = 0x7f08047c

.field public static final reward_row_background:I = 0x7f08047d

.field public static final reward_row_background_selected:I = 0x7f08047e

.field public static final rotated_chip_reader_and_card:I = 0x7f08047f

.field public static final rotated_chip_reader_and_jcb:I = 0x7f080480

.field public static final secure_touch_icon_cancel:I = 0x7f080481

.field public static final secure_touch_icon_check:I = 0x7f080482

.field public static final secure_touch_icon_delete:I = 0x7f080483

.field public static final secure_touch_mode_selection_screen_negative_button_background:I = 0x7f080484

.field public static final secure_touch_mode_selection_screen_positive_button_background:I = 0x7f080485

.field public static final selected_tab_background:I = 0x7f080486

.field public static final selector_badge_pending_actionbar:I = 0x7f080487

.field public static final selector_order_entry_tab_keypad:I = 0x7f080488

.field public static final selector_order_entry_tab_library:I = 0x7f080489

.field public static final selector_payment_badge_expiring:I = 0x7f08048a

.field public static final selector_payment_badge_failure:I = 0x7f08048b

.field public static final selector_payment_badge_pending:I = 0x7f08048c

.field public static final selector_payment_badge_refund:I = 0x7f08048d

.field public static final selector_payment_pad_button:I = 0x7f08048e

.field public static final selector_payment_pad_button_charge:I = 0x7f08048f

.field public static final selector_payment_pad_button_ticket:I = 0x7f080490

.field public static final selector_payment_pad_button_ticket_save:I = 0x7f080491

.field public static final selector_payment_pad_button_ticket_tickets:I = 0x7f080492

.field public static final selector_switcher_button:I = 0x7f080493

.field public static final send_row_icon:I = 0x7f080494

.field public static final setup_dialog_gradient:I = 0x7f080495

.field public static final setup_dialog_gradient_pressed:I = 0x7f080496

.field public static final setup_dialog_primary_background:I = 0x7f080497

.field public static final setup_guide_card_reader:I = 0x7f080498

.field public static final setup_guide_gradient_rounded_rectangle:I = 0x7f080499

.field public static final setup_guide_header_background:I = 0x7f08049a

.field public static final setup_guide_overflow_icon:I = 0x7f08049b

.field public static final setup_guide_wand:I = 0x7f08049c

.field public static final share_row_icon:I = 0x7f08049d

.field public static final sms_marketing_coupon:I = 0x7f08049e

.field public static final speech_bubble:I = 0x7f08049f

.field public static final speech_bubble_gray:I = 0x7f0804a0

.field public static final speech_bubble_selector:I = 0x7f0804a1

.field public static final splash_page_accept_payments:I = 0x7f0804a2

.field public static final splash_page_build_trust:I = 0x7f0804a3

.field public static final splash_page_reports:I = 0x7f0804a4

.field public static final splash_page_sell_in_minutes:I = 0x7f0804a5

.field public static final spos_whats_new_tour_sales_reports:I = 0x7f0804a6

.field public static final sq_product_capital_24:I = 0x7f0804a7

.field public static final sq_product_deposits_24:I = 0x7f0804a8

.field public static final sq_product_invoices_24:I = 0x7f0804a9

.field public static final sq_product_item_services_24:I = 0x7f0804aa

.field public static final sq_product_orders_24:I = 0x7f0804ab

.field public static final sq_product_reports_24:I = 0x7f0804ac

.field public static final sq_product_settings_24:I = 0x7f0804ad

.field public static final sq_product_transactions_24:I = 0x7f0804ae

.field public static final sq_selector_ultra_light_gray_when_pressed:I = 0x7f0804af

.field public static final square_card_button_selected_indicator:I = 0x7f0804b0

.field public static final square_card_disabled_icon:I = 0x7f0804b1

.field public static final square_card_sample_signature:I = 0x7f0804b2

.field public static final square_card_signature_clear:I = 0x7f0804b3

.field public static final square_card_signature_draw_mode:I = 0x7f0804b4

.field public static final square_card_signature_outline:I = 0x7f0804b5

.field public static final square_card_signature_undo:I = 0x7f0804b6

.field public static final square_card_stamps_icon:I = 0x7f0804b7

.field public static final square_card_trash_icon:I = 0x7f0804b8

.field public static final square_card_trash_icon_closing:I = 0x7f0804b9

.field public static final square_card_trash_icon_opening:I = 0x7f0804ba

.field public static final square_logo_40:I = 0x7f0804bb

.field public static final stacked_panel_background:I = 0x7f0804bc

.field public static final switch_employee_tooltip_left_down_arrow:I = 0x7f0804bd

.field public static final switch_employee_tooltip_right_up_arrow:I = 0x7f0804be

.field public static final t2:I = 0x7f0804bf

.field public static final tab_bar_background:I = 0x7f0804c0

.field public static final test_custom_background:I = 0x7f0804c1

.field public static final text_above_image_splash_screen_gradient:I = 0x7f0804c2

.field public static final timecards_clocked_in_green_indicator:I = 0x7f0804c3

.field public static final tooltip_center_down_arrow:I = 0x7f0804c4

.field public static final tooltip_center_down_arrow_dark:I = 0x7f0804c5

.field public static final tooltip_frame_dark:I = 0x7f0804c6

.field public static final tooltip_frame_light:I = 0x7f0804c7

.field public static final top_light_gray_border_white_background_1px:I = 0x7f0804c8

.field public static final tour_background:I = 0x7f0804c9

.field public static final tour_learn_more_backoffice:I = 0x7f0804ca

.field public static final tour_learn_more_pos:I = 0x7f0804cb

.field public static final tour_learn_more_reader:I = 0x7f0804cc

.field public static final tour_learn_more_simple:I = 0x7f0804cd

.field public static final transactions_applet_row_bottom_border:I = 0x7f0804ce

.field public static final transactions_history_list_header_row_background:I = 0x7f0804cf

.field public static final transactions_history_list_row_divider:I = 0x7f0804d0

.field public static final transparent_selector:I = 0x7f0804d1

.field public static final tutorial2_button_background:I = 0x7f0804d2

.field public static final tutorial_done:I = 0x7f0804d3

.field public static final tutorial_item:I = 0x7f0804d4

.field public static final tutorial_lifepreserver:I = 0x7f0804d5

.field public static final tutorial_screen_0:I = 0x7f0804d6

.field public static final tutorial_screen_1:I = 0x7f0804d7

.field public static final tutorial_screen_2:I = 0x7f0804d8

.field public static final tutorial_screen_3:I = 0x7f0804d9

.field public static final tutorial_screen_4:I = 0x7f0804da

.field public static final tutorial_screen_5:I = 0x7f0804db

.field public static final tutorial_screen_6:I = 0x7f0804dc

.field public static final tutorial_screen_7:I = 0x7f0804dd

.field public static final ui_divide_24:I = 0x7f0804de

.field public static final ui_equal_24:I = 0x7f0804df

.field public static final ui_eye_24:I = 0x7f0804e0

.field public static final ui_info_stroked_16:I = 0x7f0804e1

.field public static final ui_items_80:I = 0x7f0804e2

.field public static final ui_link_16:I = 0x7f0804e3

.field public static final ui_lock_24:I = 0x7f0804e4

.field public static final ui_lock_24_with_inset:I = 0x7f0804e5

.field public static final ui_magnifying_glass_24:I = 0x7f0804e6

.field public static final ui_minus_24:I = 0x7f0804e7

.field public static final ui_overflow_24:I = 0x7f0804e8

.field public static final ui_plus_24:I = 0x7f0804e9

.field public static final ui_settings_24:I = 0x7f0804ea

.field public static final ui_settings_80:I = 0x7f0804eb

.field public static final ui_share_24:I = 0x7f0804ec

.field public static final ui_tag_24:I = 0x7f0804ed

.field public static final ui_triangle_warning_80:I = 0x7f0804ee

.field public static final ui_user_40:I = 0x7f0804ef

.field public static final ui_x_24:I = 0x7f0804f0

.field public static final ui_x_filled_16:I = 0x7f0804f1

.field public static final ui_x_stroked_16:I = 0x7f0804f2

.field public static final unread_notification_dot:I = 0x7f0804f3

.field public static final visa_ca:I = 0x7f0804f4

.field public static final warn_business_name_truncated:I = 0x7f0804f5

.field public static final white_rounded_rectangle:I = 0x7f0804f6

.field public static final white_square_card_preview_base:I = 0x7f0804f7

.field public static final white_square_card_preview_base_small_radius:I = 0x7f0804f8

.field public static final white_square_card_preview_chip:I = 0x7f0804f9

.field public static final window_background_logo:I = 0x7f0804fa


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12926
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
