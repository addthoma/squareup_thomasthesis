.class public final Lcom/squareup/posapp/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/posapp/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final abc_background_cache_hint_selector_material_dark:I = 0x7f060000

.field public static final abc_background_cache_hint_selector_material_light:I = 0x7f060001

.field public static final abc_btn_colored_borderless_text_material:I = 0x7f060002

.field public static final abc_btn_colored_text_material:I = 0x7f060003

.field public static final abc_color_highlight_material:I = 0x7f060004

.field public static final abc_hint_foreground_material_dark:I = 0x7f060005

.field public static final abc_hint_foreground_material_light:I = 0x7f060006

.field public static final abc_input_method_navigation_guard:I = 0x7f060007

.field public static final abc_primary_text_disable_only_material_dark:I = 0x7f060008

.field public static final abc_primary_text_disable_only_material_light:I = 0x7f060009

.field public static final abc_primary_text_material_dark:I = 0x7f06000a

.field public static final abc_primary_text_material_light:I = 0x7f06000b

.field public static final abc_search_url_text:I = 0x7f06000c

.field public static final abc_search_url_text_normal:I = 0x7f06000d

.field public static final abc_search_url_text_pressed:I = 0x7f06000e

.field public static final abc_search_url_text_selected:I = 0x7f06000f

.field public static final abc_secondary_text_material_dark:I = 0x7f060010

.field public static final abc_secondary_text_material_light:I = 0x7f060011

.field public static final abc_tint_btn_checkable:I = 0x7f060012

.field public static final abc_tint_default:I = 0x7f060013

.field public static final abc_tint_edittext:I = 0x7f060014

.field public static final abc_tint_seek_thumb:I = 0x7f060015

.field public static final abc_tint_spinner:I = 0x7f060016

.field public static final abc_tint_switch_track:I = 0x7f060017

.field public static final accent_material_dark:I = 0x7f060018

.field public static final accent_material_light:I = 0x7f060019

.field public static final accessible_text_color:I = 0x7f06001a

.field public static final add_color:I = 0x7f06001b

.field public static final add_color_disabled:I = 0x7f06001c

.field public static final amount_text_color_default:I = 0x7f06001d

.field public static final amount_text_color_positive:I = 0x7f06001e

.field public static final appointment_row_cancelled:I = 0x7f06001f

.field public static final appointment_row_past:I = 0x7f060020

.field public static final appointment_row_upcoming:I = 0x7f060021

.field public static final background_floating_material_dark:I = 0x7f060022

.field public static final background_floating_material_light:I = 0x7f060023

.field public static final background_material_dark:I = 0x7f060024

.field public static final background_material_light:I = 0x7f060025

.field public static final backspace_disabled:I = 0x7f060026

.field public static final badge_color_high:I = 0x7f060027

.field public static final badge_color_nc:I = 0x7f060028

.field public static final balance_activity_details_pill_color:I = 0x7f060029

.field public static final bank_fields_icon_failure:I = 0x7f06002a

.field public static final bank_fields_icon_success:I = 0x7f06002b

.field public static final bottom_sheet_indicator:I = 0x7f06002c

.field public static final bright_foreground_disabled_material_dark:I = 0x7f06002d

.field public static final bright_foreground_disabled_material_light:I = 0x7f06002e

.field public static final bright_foreground_inverse_material_dark:I = 0x7f06002f

.field public static final bright_foreground_inverse_material_light:I = 0x7f060030

.field public static final bright_foreground_material_dark:I = 0x7f060031

.field public static final bright_foreground_material_light:I = 0x7f060032

.field public static final button_disabled:I = 0x7f060033

.field public static final button_material_dark:I = 0x7f060034

.field public static final button_material_light:I = 0x7f060035

.field public static final button_normal:I = 0x7f060036

.field public static final button_pressed:I = 0x7f060037

.field public static final buyer_facing_background:I = 0x7f060038

.field public static final buyer_facing_button_text:I = 0x7f060039

.field public static final buyer_facing_glyph_title_text:I = 0x7f06003a

.field public static final buyer_flow_merchant_background_translucent:I = 0x7f06003b

.field public static final calendar_active_month_bg:I = 0x7f06003c

.field public static final calendar_bg:I = 0x7f06003d

.field public static final calendar_divider:I = 0x7f06003e

.field public static final calendar_highlighted_day_bg:I = 0x7f06003f

.field public static final calendar_inactive_month_bg:I = 0x7f060040

.field public static final calendar_selected_day_bg:I = 0x7f060041

.field public static final calendar_selected_range_bg:I = 0x7f060042

.field public static final calendar_text_active:I = 0x7f060043

.field public static final calendar_text_highlighted:I = 0x7f060044

.field public static final calendar_text_inactive:I = 0x7f060045

.field public static final calendar_text_selected:I = 0x7f060046

.field public static final calendar_text_selector:I = 0x7f060047

.field public static final calendar_text_unselectable:I = 0x7f060048

.field public static final capital_hero_background:I = 0x7f060049

.field public static final capital_hero_content_color:I = 0x7f06004a

.field public static final capital_no_offer_body_text_color:I = 0x7f06004b

.field public static final capital_no_offer_icon_color:I = 0x7f06004c

.field public static final capital_plan_minimum_due_bullet:I = 0x7f06004d

.field public static final capital_plan_outstanding_due_bullet:I = 0x7f06004e

.field public static final capital_plan_overdue_due_bullet:I = 0x7f06004f

.field public static final capital_plan_overdue_text:I = 0x7f060050

.field public static final capital_plan_standard_text:I = 0x7f060051

.field public static final capital_plan_total_paid_bullet:I = 0x7f060052

.field public static final card_expired_or_failed:I = 0x7f060053

.field public static final card_pending:I = 0x7f060054

.field public static final cardreader_low_battery:I = 0x7f060055

.field public static final cardreader_medium_gray:I = 0x7f060056

.field public static final cardview_dark_background:I = 0x7f060057

.field public static final cardview_light_background:I = 0x7f060058

.field public static final cardview_shadow_end_color:I = 0x7f060059

.field public static final cardview_shadow_start_color:I = 0x7f06005a

.field public static final cart_diff_highlight_background:I = 0x7f06005b

.field public static final cart_footer_banner_background:I = 0x7f06005c

.field public static final check_color:I = 0x7f06005d

.field public static final check_color_bg:I = 0x7f06005e

.field public static final check_color_bg_actived:I = 0x7f06005f

.field public static final check_color_bg_disabled:I = 0x7f060060

.field public static final check_color_disabled:I = 0x7f060061

.field public static final checkbox_themeable_attribute_color:I = 0x7f060062

.field public static final common_google_signin_btn_text_dark:I = 0x7f060063

.field public static final common_google_signin_btn_text_dark_default:I = 0x7f060064

.field public static final common_google_signin_btn_text_dark_disabled:I = 0x7f060065

.field public static final common_google_signin_btn_text_dark_focused:I = 0x7f060066

.field public static final common_google_signin_btn_text_dark_pressed:I = 0x7f060067

.field public static final common_google_signin_btn_text_light:I = 0x7f060068

.field public static final common_google_signin_btn_text_light_default:I = 0x7f060069

.field public static final common_google_signin_btn_text_light_disabled:I = 0x7f06006a

.field public static final common_google_signin_btn_text_light_focused:I = 0x7f06006b

.field public static final common_google_signin_btn_text_light_pressed:I = 0x7f06006c

.field public static final common_google_signin_btn_tint:I = 0x7f06006d

.field public static final community_reward_link_color:I = 0x7f06006e

.field public static final container_transparent:I = 0x7f06006f

.field public static final counter_view_disabled:I = 0x7f060070

.field public static final counter_view_enabled:I = 0x7f060071

.field public static final create_links_help_text_color:I = 0x7f060072

.field public static final create_passcode_body_text:I = 0x7f060073

.field public static final create_passcode_error:I = 0x7f060074

.field public static final crm_initial_circle_azure:I = 0x7f060075

.field public static final crm_initial_circle_blue:I = 0x7f060076

.field public static final crm_initial_circle_purple:I = 0x7f060077

.field public static final crm_initial_circle_registerblue:I = 0x7f060078

.field public static final crm_initial_circle_registerbluepressed:I = 0x7f060079

.field public static final date_picker_text:I = 0x7f06007a

.field public static final date_picker_text_selector:I = 0x7f06007b

.field public static final deactivated_message_background:I = 0x7f06007c

.field public static final default_background_slice_color:I = 0x7f06007d

.field public static final default_primary_chart_data:I = 0x7f06007e

.field public static final default_secondary_chart_data:I = 0x7f06007f

.field public static final default_selected_primary_chart_data:I = 0x7f060080

.field public static final default_selected_secondary_chart_data:I = 0x7f060081

.field public static final default_title_indicator_footer_color:I = 0x7f060082

.field public static final default_title_indicator_selected_color:I = 0x7f060083

.field public static final default_title_indicator_text_color:I = 0x7f060084

.field public static final default_underline_indicator_selected_color:I = 0x7f060085

.field public static final deposits_report_info_color:I = 0x7f060086

.field public static final design_bottom_navigation_shadow_color:I = 0x7f060087

.field public static final design_box_stroke_color:I = 0x7f060088

.field public static final design_dark_default_color_background:I = 0x7f060089

.field public static final design_dark_default_color_error:I = 0x7f06008a

.field public static final design_dark_default_color_on_background:I = 0x7f06008b

.field public static final design_dark_default_color_on_error:I = 0x7f06008c

.field public static final design_dark_default_color_on_primary:I = 0x7f06008d

.field public static final design_dark_default_color_on_secondary:I = 0x7f06008e

.field public static final design_dark_default_color_on_surface:I = 0x7f06008f

.field public static final design_dark_default_color_primary:I = 0x7f060090

.field public static final design_dark_default_color_primary_dark:I = 0x7f060091

.field public static final design_dark_default_color_primary_variant:I = 0x7f060092

.field public static final design_dark_default_color_secondary:I = 0x7f060093

.field public static final design_dark_default_color_secondary_variant:I = 0x7f060094

.field public static final design_dark_default_color_surface:I = 0x7f060095

.field public static final design_default_color_background:I = 0x7f060096

.field public static final design_default_color_error:I = 0x7f060097

.field public static final design_default_color_on_background:I = 0x7f060098

.field public static final design_default_color_on_error:I = 0x7f060099

.field public static final design_default_color_on_primary:I = 0x7f06009a

.field public static final design_default_color_on_secondary:I = 0x7f06009b

.field public static final design_default_color_on_surface:I = 0x7f06009c

.field public static final design_default_color_primary:I = 0x7f06009d

.field public static final design_default_color_primary_dark:I = 0x7f06009e

.field public static final design_default_color_primary_variant:I = 0x7f06009f

.field public static final design_default_color_secondary:I = 0x7f0600a0

.field public static final design_default_color_secondary_variant:I = 0x7f0600a1

.field public static final design_default_color_surface:I = 0x7f0600a2

.field public static final design_error:I = 0x7f0600a3

.field public static final design_fab_shadow_end_color:I = 0x7f0600a4

.field public static final design_fab_shadow_mid_color:I = 0x7f0600a5

.field public static final design_fab_shadow_start_color:I = 0x7f0600a6

.field public static final design_fab_stroke_end_inner_color:I = 0x7f0600a7

.field public static final design_fab_stroke_end_outer_color:I = 0x7f0600a8

.field public static final design_fab_stroke_top_inner_color:I = 0x7f0600a9

.field public static final design_fab_stroke_top_outer_color:I = 0x7f0600aa

.field public static final design_icon_tint:I = 0x7f0600ab

.field public static final design_snackbar_background_color:I = 0x7f0600ac

.field public static final dim_foreground_disabled_material_dark:I = 0x7f0600ad

.field public static final dim_foreground_disabled_material_light:I = 0x7f0600ae

.field public static final dim_foreground_material_dark:I = 0x7f0600af

.field public static final dim_foreground_material_light:I = 0x7f0600b0

.field public static final dispute_actionable:I = 0x7f0600b1

.field public static final dispute_blank_image_background:I = 0x7f0600b2

.field public static final dispute_form_text_answer:I = 0x7f0600b3

.field public static final dispute_header:I = 0x7f0600b4

.field public static final dispute_lost:I = 0x7f0600b5

.field public static final dispute_pending:I = 0x7f0600b6

.field public static final dispute_row_value:I = 0x7f0600b7

.field public static final dispute_won:I = 0x7f0600b8

.field public static final edit_item_blue:I = 0x7f0600b9

.field public static final edit_item_brown:I = 0x7f0600ba

.field public static final edit_item_cyan:I = 0x7f0600bb

.field public static final edit_item_gray:I = 0x7f0600bc

.field public static final edit_item_green:I = 0x7f0600bd

.field public static final edit_item_light_green:I = 0x7f0600be

.field public static final edit_item_pink:I = 0x7f0600bf

.field public static final edit_item_purple:I = 0x7f0600c0

.field public static final edit_item_red:I = 0x7f0600c1

.field public static final edit_item_yellow:I = 0x7f0600c2

.field public static final edit_option_value_color_picker_black:I = 0x7f0600c3

.field public static final edit_option_value_color_picker_blue:I = 0x7f0600c4

.field public static final edit_option_value_color_picker_brown:I = 0x7f0600c5

.field public static final edit_option_value_color_picker_gold:I = 0x7f0600c6

.field public static final edit_option_value_color_picker_green:I = 0x7f0600c7

.field public static final edit_option_value_color_picker_indigo:I = 0x7f0600c8

.field public static final edit_option_value_color_picker_lime:I = 0x7f0600c9

.field public static final edit_option_value_color_picker_maroon:I = 0x7f0600ca

.field public static final edit_option_value_color_picker_orange:I = 0x7f0600cb

.field public static final edit_option_value_color_picker_pink:I = 0x7f0600cc

.field public static final edit_option_value_color_picker_plum:I = 0x7f0600cd

.field public static final edit_option_value_color_picker_purple:I = 0x7f0600ce

.field public static final edit_option_value_color_picker_teal:I = 0x7f0600cf

.field public static final edit_option_value_color_picker_white:I = 0x7f0600d0

.field public static final egiftcard_default_bg_color:I = 0x7f0600d1

.field public static final egiftcard_design_border_color:I = 0x7f0600d2

.field public static final egiftcard_design_border_color_selected:I = 0x7f0600d3

.field public static final error_banner_background:I = 0x7f0600d4

.field public static final error_color_material_dark:I = 0x7f0600d5

.field public static final error_color_material_light:I = 0x7f0600d6

.field public static final error_text_selector:I = 0x7f0600d7

.field public static final field_error_indicator:I = 0x7f0600d8

.field public static final foreground_material_dark:I = 0x7f0600d9

.field public static final foreground_material_light:I = 0x7f0600da

.field public static final freeze_banner_background:I = 0x7f0600db

.field public static final glyph_background:I = 0x7f0600dc

.field public static final glyph_shadow:I = 0x7f0600dd

.field public static final header_background:I = 0x7f0600de

.field public static final highlighted_text_material_dark:I = 0x7f0600df

.field public static final highlighted_text_material_light:I = 0x7f0600e0

.field public static final home_background:I = 0x7f0600e1

.field public static final home_view_sales_frame_shadow:I = 0x7f0600e2

.field public static final hs__color_1565C0:I = 0x7f0600e3

.field public static final hs__color_19000000:I = 0x7f0600e4

.field public static final hs__color_19FFFFFF:I = 0x7f0600e5

.field public static final hs__color_1A000000:I = 0x7f0600e6

.field public static final hs__color_1F000000:I = 0x7f0600e7

.field public static final hs__color_1FFFFFFF:I = 0x7f0600e8

.field public static final hs__color_2E2E2E:I = 0x7f0600e9

.field public static final hs__color_40000000:I = 0x7f0600ea

.field public static final hs__color_4000B0FF:I = 0x7f0600eb

.field public static final hs__color_4000E5FF:I = 0x7f0600ec

.field public static final hs__color_402196F3:I = 0x7f0600ed

.field public static final hs__color_42000000:I = 0x7f0600ee

.field public static final hs__color_696969:I = 0x7f0600ef

.field public static final hs__color_7A7A7A:I = 0x7f0600f0

.field public static final hs__color_80B1B1B1:I = 0x7f0600f1

.field public static final hs__color_80FFFFFF:I = 0x7f0600f2

.field public static final hs__color_8A000000:I = 0x7f0600f3

.field public static final hs__color_8C000000:I = 0x7f0600f4

.field public static final hs__color_8F8F8F:I = 0x7f0600f5

.field public static final hs__color_B3FFFFFF:I = 0x7f0600f6

.field public static final hs__color_DAE8F0:I = 0x7f0600f7

.field public static final hs__color_DE000000:I = 0x7f0600f8

.field public static final hs__color_FF000000:I = 0x7f0600f9

.field public static final hs__color_FF00B0FF:I = 0x7f0600fa

.field public static final hs__color_FF00E5FF:I = 0x7f0600fb

.field public static final hs__color_FF1976D2:I = 0x7f0600fc

.field public static final hs__color_FF1E88E5:I = 0x7f0600fd

.field public static final hs__color_FF2196F3:I = 0x7f0600fe

.field public static final hs__color_FF222222:I = 0x7f0600ff

.field public static final hs__color_FF37474F:I = 0x7f060100

.field public static final hs__color_FF455A64:I = 0x7f060101

.field public static final hs__color_FF607D8B:I = 0x7f060102

.field public static final hs__color_FF616161:I = 0x7f060103

.field public static final hs__color_FF929495:I = 0x7f060104

.field public static final hs__color_FFADB0B0:I = 0x7f060105

.field public static final hs__color_FFBDBDBD:I = 0x7f060106

.field public static final hs__color_FFCFD8DC:I = 0x7f060107

.field public static final hs__color_FFD50000:I = 0x7f060108

.field public static final hs__color_FFDADFE2:I = 0x7f060109

.field public static final hs__color_FFE0E0E0:I = 0x7f06010a

.field public static final hs__color_FFF44336:I = 0x7f06010b

.field public static final hs__color_FFF80054:I = 0x7f06010c

.field public static final hs__color_FFFAFAFA:I = 0x7f06010d

.field public static final hs__color_FFFF6E6E:I = 0x7f06010e

.field public static final hs__color_FFFFFF:I = 0x7f06010f

.field public static final hs__color_FFFFFFFF:I = 0x7f060110

.field public static final hud_background:I = 0x7f060111

.field public static final installments_row_disabled:I = 0x7f060112

.field public static final instant_transfer_button_text_blue:I = 0x7f060113

.field public static final instant_transfer_button_text_white:I = 0x7f060114

.field public static final invoice_app_color:I = 0x7f060115

.field public static final invoice_app_color_pressed:I = 0x7f060116

.field public static final invoice_calendar_text_selector:I = 0x7f060117

.field public static final invoices_big_text_header_subtitle:I = 0x7f060118

.field public static final invoices_big_text_header_title:I = 0x7f060119

.field public static final invoices_cart_cyan:I = 0x7f06011a

.field public static final invoices_cart_dark_gray:I = 0x7f06011b

.field public static final invoices_divider:I = 0x7f06011c

.field public static final invoices_header_background:I = 0x7f06011d

.field public static final key_colors:I = 0x7f06011e

.field public static final keypad_divider_color:I = 0x7f06011f

.field public static final keypad_key_subtitle_color:I = 0x7f060120

.field public static final keypad_screen_clear_button_color:I = 0x7f060121

.field public static final keypad_screen_clear_button_icon_x_color:I = 0x7f060122

.field public static final keypad_screen_done_button_color:I = 0x7f060123

.field public static final keypad_screen_done_button_icon_check_color:I = 0x7f060124

.field public static final keypad_screen_star_color:I = 0x7f060125

.field public static final keypad_screen_title_color:I = 0x7f060126

.field public static final letters_text_color:I = 0x7f060127

.field public static final letters_text_color_disabled:I = 0x7f060128

.field public static final library_item_background_color:I = 0x7f060129

.field public static final library_item_background_color_action:I = 0x7f06012a

.field public static final library_item_color_action:I = 0x7f06012b

.field public static final library_item_text_color_normal:I = 0x7f06012c

.field public static final line_color:I = 0x7f06012d

.field public static final location_button_fill_default:I = 0x7f06012e

.field public static final location_button_fill_pressed:I = 0x7f06012f

.field public static final location_button_stroke_default:I = 0x7f060130

.field public static final location_button_stroke_pressed:I = 0x7f060131

.field public static final magstripe_center:I = 0x7f060132

.field public static final magstripe_end:I = 0x7f060133

.field public static final magstripe_start:I = 0x7f060134

.field public static final marin_action_bar_background:I = 0x7f060135

.field public static final marin_action_bar_background_inverted:I = 0x7f060136

.field public static final marin_action_bar_divider_inverted:I = 0x7f060137

.field public static final marin_barely_dark_translucent_pressed:I = 0x7f060138

.field public static final marin_black:I = 0x7f060139

.field public static final marin_black_transparent_fifty:I = 0x7f06013a

.field public static final marin_black_transparent_forty:I = 0x7f06013b

.field public static final marin_black_transparent_seventyfive:I = 0x7f06013c

.field public static final marin_black_transparent_thirty:I = 0x7f06013d

.field public static final marin_blue:I = 0x7f06013e

.field public static final marin_blue_disabled:I = 0x7f06013f

.field public static final marin_blue_divider:I = 0x7f060140

.field public static final marin_blue_pressed:I = 0x7f060141

.field public static final marin_blue_selected:I = 0x7f060142

.field public static final marin_blue_selected_pressed:I = 0x7f060143

.field public static final marin_clear:I = 0x7f060144

.field public static final marin_dark_gray:I = 0x7f060145

.field public static final marin_dark_gray_pressed:I = 0x7f060146

.field public static final marin_dark_green:I = 0x7f060147

.field public static final marin_dark_translucent:I = 0x7f060148

.field public static final marin_dark_translucent_pressed:I = 0x7f060149

.field public static final marin_darker_blue:I = 0x7f06014a

.field public static final marin_darker_blue_pressed:I = 0x7f06014b

.field public static final marin_darker_gray:I = 0x7f06014c

.field public static final marin_darker_gray_pressed:I = 0x7f06014d

.field public static final marin_dim_translucent_pressed:I = 0x7f06014e

.field public static final marin_divider_gray:I = 0x7f06014f

.field public static final marin_drawer_background:I = 0x7f060150

.field public static final marin_drawer_background_pressed:I = 0x7f060151

.field public static final marin_drawer_dark_gray:I = 0x7f060152

.field public static final marin_fake_transparent_white_on_gray:I = 0x7f060153

.field public static final marin_focused_fill:I = 0x7f060154

.field public static final marin_green:I = 0x7f060155

.field public static final marin_green_border:I = 0x7f060156

.field public static final marin_green_pressed:I = 0x7f060157

.field public static final marin_light_gray:I = 0x7f060158

.field public static final marin_light_gray_pressed:I = 0x7f060159

.field public static final marin_light_green:I = 0x7f06015a

.field public static final marin_medium_gray:I = 0x7f06015b

.field public static final marin_medium_gray_pressed:I = 0x7f06015c

.field public static final marin_orange:I = 0x7f06015d

.field public static final marin_orange_disabled:I = 0x7f06015e

.field public static final marin_order_entry_tab_color:I = 0x7f06015f

.field public static final marin_red:I = 0x7f060160

.field public static final marin_red_pressed:I = 0x7f060161

.field public static final marin_screen_scrim:I = 0x7f060162

.field public static final marin_text_selector_blue:I = 0x7f060163

.field public static final marin_text_selector_blue_disabled_dark_gray:I = 0x7f060164

.field public static final marin_text_selector_blue_disabled_light_gray:I = 0x7f060165

.field public static final marin_text_selector_blue_disabled_light_gray_activated_white:I = 0x7f060166

.field public static final marin_text_selector_blue_disabled_medium_gray:I = 0x7f060167

.field public static final marin_text_selector_dark_gray:I = 0x7f060168

.field public static final marin_text_selector_dark_gray_checked_blue:I = 0x7f060169

.field public static final marin_text_selector_dark_gray_checked_white:I = 0x7f06016a

.field public static final marin_text_selector_dark_gray_disabled_light_gray_pressed_light_gray:I = 0x7f06016b

.field public static final marin_text_selector_dark_gray_disabled_medium_gray:I = 0x7f06016c

.field public static final marin_text_selector_dark_gray_selected_blue:I = 0x7f06016d

.field public static final marin_text_selector_dark_green_checked_light_green:I = 0x7f06016e

.field public static final marin_text_selector_drawer_dark_selected_white:I = 0x7f06016f

.field public static final marin_text_selector_edit_text:I = 0x7f060170

.field public static final marin_text_selector_light_gray_selected_white:I = 0x7f060171

.field public static final marin_text_selector_list:I = 0x7f060172

.field public static final marin_text_selector_medium_gray:I = 0x7f060173

.field public static final marin_text_selector_medium_gray_checked_white:I = 0x7f060174

.field public static final marin_text_selector_medium_gray_disabled_light_gray:I = 0x7f060175

.field public static final marin_text_selector_white:I = 0x7f060176

.field public static final marin_text_selector_white_disabled_dark_gray:I = 0x7f060177

.field public static final marin_text_selector_white_disabled_fake_transparent:I = 0x7f060178

.field public static final marin_text_selector_white_disabled_light_gray:I = 0x7f060179

.field public static final marin_text_selector_white_disabled_white_translucent:I = 0x7f06017a

.field public static final marin_text_shadow:I = 0x7f06017b

.field public static final marin_translucent_black:I = 0x7f06017c

.field public static final marin_translucent_dark:I = 0x7f06017d

.field public static final marin_transparent:I = 0x7f06017e

.field public static final marin_ultra_light_gray:I = 0x7f06017f

.field public static final marin_ultra_light_gray_disabled:I = 0x7f060180

.field public static final marin_ultra_light_gray_inline_button_disabled:I = 0x7f060181

.field public static final marin_ultra_light_gray_pressed:I = 0x7f060182

.field public static final marin_white:I = 0x7f060183

.field public static final marin_white_translucent:I = 0x7f060184

.field public static final marin_white_transparent_twenty:I = 0x7f060185

.field public static final marin_window_background:I = 0x7f060186

.field public static final marin_window_background_dark:I = 0x7f060187

.field public static final marin_window_background_inverted:I = 0x7f060188

.field public static final master_card_intersection:I = 0x7f060189

.field public static final master_card_left_circle_center:I = 0x7f06018a

.field public static final master_card_left_circle_end:I = 0x7f06018b

.field public static final master_card_left_circle_start:I = 0x7f06018c

.field public static final master_card_right_circle_center:I = 0x7f06018d

.field public static final master_card_right_circle_end:I = 0x7f06018e

.field public static final master_card_right_circle_start:I = 0x7f06018f

.field public static final material_blue_grey_800:I = 0x7f060190

.field public static final material_blue_grey_900:I = 0x7f060191

.field public static final material_blue_grey_950:I = 0x7f060192

.field public static final material_deep_teal_200:I = 0x7f060193

.field public static final material_deep_teal_500:I = 0x7f060194

.field public static final material_grey_100:I = 0x7f060195

.field public static final material_grey_300:I = 0x7f060196

.field public static final material_grey_50:I = 0x7f060197

.field public static final material_grey_600:I = 0x7f060198

.field public static final material_grey_800:I = 0x7f060199

.field public static final material_grey_850:I = 0x7f06019a

.field public static final material_grey_900:I = 0x7f06019b

.field public static final material_on_background_disabled:I = 0x7f06019c

.field public static final material_on_background_emphasis_high_type:I = 0x7f06019d

.field public static final material_on_background_emphasis_medium:I = 0x7f06019e

.field public static final material_on_primary_disabled:I = 0x7f06019f

.field public static final material_on_primary_emphasis_high_type:I = 0x7f0601a0

.field public static final material_on_primary_emphasis_medium:I = 0x7f0601a1

.field public static final material_on_surface_disabled:I = 0x7f0601a2

.field public static final material_on_surface_emphasis_high_type:I = 0x7f0601a3

.field public static final material_on_surface_emphasis_medium:I = 0x7f0601a4

.field public static final message_bar_background_attention:I = 0x7f0601a5

.field public static final message_bar_background_editing:I = 0x7f0601a6

.field public static final message_bar_background_error:I = 0x7f0601a7

.field public static final message_bar_background_offline_mode:I = 0x7f0601a8

.field public static final message_bar_background_ready:I = 0x7f0601a9

.field public static final mtrl_bottom_nav_colored_item_tint:I = 0x7f0601aa

.field public static final mtrl_bottom_nav_colored_ripple_color:I = 0x7f0601ab

.field public static final mtrl_bottom_nav_item_tint:I = 0x7f0601ac

.field public static final mtrl_bottom_nav_ripple_color:I = 0x7f0601ad

.field public static final mtrl_btn_bg_color_selector:I = 0x7f0601ae

.field public static final mtrl_btn_ripple_color:I = 0x7f0601af

.field public static final mtrl_btn_stroke_color_selector:I = 0x7f0601b0

.field public static final mtrl_btn_text_btn_bg_color_selector:I = 0x7f0601b1

.field public static final mtrl_btn_text_btn_ripple_color:I = 0x7f0601b2

.field public static final mtrl_btn_text_color_disabled:I = 0x7f0601b3

.field public static final mtrl_btn_text_color_selector:I = 0x7f0601b4

.field public static final mtrl_btn_transparent_bg_color:I = 0x7f0601b5

.field public static final mtrl_calendar_item_stroke_color:I = 0x7f0601b6

.field public static final mtrl_calendar_selected_range:I = 0x7f0601b7

.field public static final mtrl_card_view_foreground:I = 0x7f0601b8

.field public static final mtrl_card_view_ripple:I = 0x7f0601b9

.field public static final mtrl_chip_background_color:I = 0x7f0601ba

.field public static final mtrl_chip_close_icon_tint:I = 0x7f0601bb

.field public static final mtrl_chip_ripple_color:I = 0x7f0601bc

.field public static final mtrl_chip_surface_color:I = 0x7f0601bd

.field public static final mtrl_chip_text_color:I = 0x7f0601be

.field public static final mtrl_choice_chip_background_color:I = 0x7f0601bf

.field public static final mtrl_choice_chip_ripple_color:I = 0x7f0601c0

.field public static final mtrl_choice_chip_text_color:I = 0x7f0601c1

.field public static final mtrl_error:I = 0x7f0601c2

.field public static final mtrl_extended_fab_bg_color_selector:I = 0x7f0601c3

.field public static final mtrl_extended_fab_ripple_color:I = 0x7f0601c4

.field public static final mtrl_extended_fab_text_color_selector:I = 0x7f0601c5

.field public static final mtrl_fab_ripple_color:I = 0x7f0601c6

.field public static final mtrl_filled_background_color:I = 0x7f0601c7

.field public static final mtrl_filled_icon_tint:I = 0x7f0601c8

.field public static final mtrl_filled_stroke_color:I = 0x7f0601c9

.field public static final mtrl_indicator_text_color:I = 0x7f0601ca

.field public static final mtrl_navigation_item_background_color:I = 0x7f0601cb

.field public static final mtrl_navigation_item_icon_tint:I = 0x7f0601cc

.field public static final mtrl_navigation_item_text_color:I = 0x7f0601cd

.field public static final mtrl_on_primary_text_btn_text_color_selector:I = 0x7f0601ce

.field public static final mtrl_outlined_icon_tint:I = 0x7f0601cf

.field public static final mtrl_outlined_stroke_color:I = 0x7f0601d0

.field public static final mtrl_popupmenu_overlay_color:I = 0x7f0601d1

.field public static final mtrl_scrim_color:I = 0x7f0601d2

.field public static final mtrl_tabs_colored_ripple_color:I = 0x7f0601d3

.field public static final mtrl_tabs_icon_color_selector:I = 0x7f0601d4

.field public static final mtrl_tabs_icon_color_selector_colored:I = 0x7f0601d5

.field public static final mtrl_tabs_legacy_text_color_selector:I = 0x7f0601d6

.field public static final mtrl_tabs_ripple_color:I = 0x7f0601d7

.field public static final mtrl_text_btn_text_color_selector:I = 0x7f0601d8

.field public static final mtrl_textinput_default_box_stroke_color:I = 0x7f0601d9

.field public static final mtrl_textinput_disabled_color:I = 0x7f0601da

.field public static final mtrl_textinput_filled_box_default_background_color:I = 0x7f0601db

.field public static final mtrl_textinput_focused_box_stroke_color:I = 0x7f0601dc

.field public static final mtrl_textinput_hovered_box_stroke_color:I = 0x7f0601dd

.field public static final noho_background:I = 0x7f0601de

.field public static final noho_banner_background:I = 0x7f0601df

.field public static final noho_button_secondary_border_disabled:I = 0x7f0601e0

.field public static final noho_button_secondary_border_enabled:I = 0x7f0601e1

.field public static final noho_button_secondary_border_pressed:I = 0x7f0601e2

.field public static final noho_button_tertiary_border_disabled:I = 0x7f0601e3

.field public static final noho_button_tertiary_border_enabled:I = 0x7f0601e4

.field public static final noho_button_tertiary_border_pressed:I = 0x7f0601e5

.field public static final noho_color_selectable_label:I = 0x7f0601e6

.field public static final noho_color_selectable_value:I = 0x7f0601e7

.field public static final noho_color_selector_destructive_button_subtitle:I = 0x7f0601e8

.field public static final noho_color_selector_destructive_button_text:I = 0x7f0601e9

.field public static final noho_color_selector_dialog_button_text:I = 0x7f0601ea

.field public static final noho_color_selector_link_button_text:I = 0x7f0601eb

.field public static final noho_color_selector_primary_button_subtitle:I = 0x7f0601ec

.field public static final noho_color_selector_primary_button_text:I = 0x7f0601ed

.field public static final noho_color_selector_row_action_icon:I = 0x7f0601ee

.field public static final noho_color_selector_row_action_link:I = 0x7f0601ef

.field public static final noho_color_selector_row_description:I = 0x7f0601f0

.field public static final noho_color_selector_row_icon:I = 0x7f0601f1

.field public static final noho_color_selector_row_label:I = 0x7f0601f2

.field public static final noho_color_selector_row_value:I = 0x7f0601f3

.field public static final noho_color_selector_secondary_button_border:I = 0x7f0601f4

.field public static final noho_color_selector_secondary_button_subtitle:I = 0x7f0601f5

.field public static final noho_color_selector_secondary_button_text:I = 0x7f0601f6

.field public static final noho_color_selector_tertiary_button_border:I = 0x7f0601f7

.field public static final noho_color_selector_tertiary_button_subtitle:I = 0x7f0601f8

.field public static final noho_color_selector_tertiary_button_text:I = 0x7f0601f9

.field public static final noho_color_selector_time_picker:I = 0x7f0601fa

.field public static final noho_color_selector_topbar_icon:I = 0x7f0601fb

.field public static final noho_divider_hairline:I = 0x7f0601fc

.field public static final noho_dropdown_button:I = 0x7f0601fd

.field public static final noho_dropdown_menu_divider:I = 0x7f0601fe

.field public static final noho_dropdown_menu_text_color:I = 0x7f0601ff

.field public static final noho_dropdown_selected_item_background:I = 0x7f060200

.field public static final noho_dropdown_selected_text_color:I = 0x7f060201

.field public static final noho_edit_background_border:I = 0x7f060202

.field public static final noho_edit_background_color:I = 0x7f060203

.field public static final noho_edit_background_disabled_color:I = 0x7f060204

.field public static final noho_edit_background_inner_shadow:I = 0x7f060205

.field public static final noho_edit_background_underline_error:I = 0x7f060206

.field public static final noho_edit_background_underline_selected:I = 0x7f060207

.field public static final noho_edit_background_underline_validated:I = 0x7f060208

.field public static final noho_edit_cursor:I = 0x7f060209

.field public static final noho_edit_hint_color:I = 0x7f06020a

.field public static final noho_edit_note_color:I = 0x7f06020b

.field public static final noho_edit_text_color:I = 0x7f06020c

.field public static final noho_edit_view_password_color:I = 0x7f06020d

.field public static final noho_icon_color:I = 0x7f06020e

.field public static final noho_inner_shadow_color:I = 0x7f06020f

.field public static final noho_input_details:I = 0x7f060210

.field public static final noho_input_details_link:I = 0x7f060211

.field public static final noho_input_error:I = 0x7f060212

.field public static final noho_label_default:I = 0x7f060213

.field public static final noho_notification_balloon:I = 0x7f060214

.field public static final noho_notification_fatal_balloon:I = 0x7f060215

.field public static final noho_notification_highpri_balloon:I = 0x7f060216

.field public static final noho_notification_text:I = 0x7f060217

.field public static final noho_number_picker_selected:I = 0x7f060218

.field public static final noho_number_picker_unselected:I = 0x7f060219

.field public static final noho_recycler_edges:I = 0x7f06021a

.field public static final noho_row_action_icon:I = 0x7f06021b

.field public static final noho_row_activated_action_icon:I = 0x7f06021c

.field public static final noho_row_activated_icon:I = 0x7f06021d

.field public static final noho_row_disabled_action_icon:I = 0x7f06021e

.field public static final noho_row_disabled_icon:I = 0x7f06021f

.field public static final noho_row_icon:I = 0x7f060220

.field public static final noho_row_legacy_icon:I = 0x7f060221

.field public static final noho_row_legacy_icon_destructive:I = 0x7f060222

.field public static final noho_selectable_label_checked:I = 0x7f060223

.field public static final noho_selectable_label_default:I = 0x7f060224

.field public static final noho_selectable_label_disabled:I = 0x7f060225

.field public static final noho_selectable_value_checked:I = 0x7f060226

.field public static final noho_selectable_value_default:I = 0x7f060227

.field public static final noho_selectable_value_disabled:I = 0x7f060228

.field public static final noho_standard_black:I = 0x7f060229

.field public static final noho_standard_blue:I = 0x7f06022a

.field public static final noho_standard_brown:I = 0x7f06022b

.field public static final noho_standard_cyan:I = 0x7f06022c

.field public static final noho_standard_cyan_dark:I = 0x7f06022d

.field public static final noho_standard_gold:I = 0x7f06022e

.field public static final noho_standard_gray_10:I = 0x7f06022f

.field public static final noho_standard_gray_20:I = 0x7f060230

.field public static final noho_standard_gray_30:I = 0x7f060231

.field public static final noho_standard_gray_40:I = 0x7f060232

.field public static final noho_standard_gray_50:I = 0x7f060233

.field public static final noho_standard_gray_60:I = 0x7f060234

.field public static final noho_standard_gray_70:I = 0x7f060235

.field public static final noho_standard_gray_80:I = 0x7f060236

.field public static final noho_standard_green:I = 0x7f060237

.field public static final noho_standard_indigo:I = 0x7f060238

.field public static final noho_standard_lilac:I = 0x7f060239

.field public static final noho_standard_lime:I = 0x7f06023a

.field public static final noho_standard_maroon:I = 0x7f06023b

.field public static final noho_standard_night_almost_black:I = 0x7f06023c

.field public static final noho_standard_night_dark:I = 0x7f06023d

.field public static final noho_standard_night_extra_dark:I = 0x7f06023e

.field public static final noho_standard_night_gray:I = 0x7f06023f

.field public static final noho_standard_night_gray_35_opacity:I = 0x7f060240

.field public static final noho_standard_night_gray_pressed:I = 0x7f060241

.field public static final noho_standard_orange:I = 0x7f060242

.field public static final noho_standard_pink:I = 0x7f060243

.field public static final noho_standard_plum:I = 0x7f060244

.field public static final noho_standard_purple:I = 0x7f060245

.field public static final noho_standard_system_error:I = 0x7f060246

.field public static final noho_standard_system_error_dark:I = 0x7f060247

.field public static final noho_standard_system_success:I = 0x7f060248

.field public static final noho_standard_system_warning:I = 0x7f060249

.field public static final noho_standard_system_yellow:I = 0x7f06024a

.field public static final noho_standard_teal:I = 0x7f06024b

.field public static final noho_standard_white:I = 0x7f06024c

.field public static final noho_switch_base_off_color:I = 0x7f06024d

.field public static final noho_switch_base_on_color:I = 0x7f06024e

.field public static final noho_switch_thumb_color:I = 0x7f06024f

.field public static final noho_text_banner:I = 0x7f060250

.field public static final noho_text_body:I = 0x7f060251

.field public static final noho_text_button_buyer_facing_secondary:I = 0x7f060252

.field public static final noho_text_button_destructive:I = 0x7f060253

.field public static final noho_text_button_link_disabled:I = 0x7f060254

.field public static final noho_text_button_link_enabled:I = 0x7f060255

.field public static final noho_text_button_primary_disabled:I = 0x7f060256

.field public static final noho_text_button_primary_enabled:I = 0x7f060257

.field public static final noho_text_button_primary_subtitle_disabled:I = 0x7f060258

.field public static final noho_text_button_primary_subtitle_enabled:I = 0x7f060259

.field public static final noho_text_button_secondary_disabled:I = 0x7f06025a

.field public static final noho_text_button_secondary_enabled:I = 0x7f06025b

.field public static final noho_text_button_secondary_pressed:I = 0x7f06025c

.field public static final noho_text_button_secondary_subtitle_disabled:I = 0x7f06025d

.field public static final noho_text_button_secondary_subtitle_enabled:I = 0x7f06025e

.field public static final noho_text_button_secondary_subtitle_pressed:I = 0x7f06025f

.field public static final noho_text_button_tertiary_disabled:I = 0x7f060260

.field public static final noho_text_button_tertiary_enabled:I = 0x7f060261

.field public static final noho_text_button_tertiary_pressed:I = 0x7f060262

.field public static final noho_text_button_tertiary_subtitle_disabled:I = 0x7f060263

.field public static final noho_text_button_tertiary_subtitle_enabled:I = 0x7f060264

.field public static final noho_text_button_tertiary_subtitle_pressed:I = 0x7f060265

.field public static final noho_text_button_tutorial_secondary_enabled:I = 0x7f060266

.field public static final noho_text_help:I = 0x7f060267

.field public static final noho_text_help_link:I = 0x7f060268

.field public static final noho_text_hint:I = 0x7f060269

.field public static final noho_text_input:I = 0x7f06026a

.field public static final noho_text_label:I = 0x7f06026b

.field public static final noho_text_row_action_link:I = 0x7f06026c

.field public static final noho_text_row_activated_label:I = 0x7f06026d

.field public static final noho_text_row_activated_value:I = 0x7f06026e

.field public static final noho_text_row_description:I = 0x7f06026f

.field public static final noho_text_row_disabled_action_link:I = 0x7f060270

.field public static final noho_text_row_disabled_description:I = 0x7f060271

.field public static final noho_text_row_disabled_label:I = 0x7f060272

.field public static final noho_text_row_disabled_value:I = 0x7f060273

.field public static final noho_text_row_label:I = 0x7f060274

.field public static final noho_text_row_text_icon:I = 0x7f060275

.field public static final noho_text_row_value:I = 0x7f060276

.field public static final noho_text_subheader:I = 0x7f060277

.field public static final noho_text_topbar_action_disabled:I = 0x7f060278

.field public static final noho_text_topbar_action_enabled:I = 0x7f060279

.field public static final noho_text_topbar_title:I = 0x7f06027a

.field public static final noho_text_warning:I = 0x7f06027b

.field public static final noho_topbar_background:I = 0x7f06027c

.field public static final noho_topbar_divider:I = 0x7f06027d

.field public static final noho_topbar_icon_disabled:I = 0x7f06027e

.field public static final noho_topbar_icon_enabled:I = 0x7f06027f

.field public static final notification_action_color_filter:I = 0x7f060280

.field public static final notification_background_color:I = 0x7f060281

.field public static final notification_icon_bg_color:I = 0x7f060282

.field public static final notification_material_background_media_default_color:I = 0x7f060283

.field public static final notification_preferences_snackbar_color:I = 0x7f060284

.field public static final onboarding_warning_background:I = 0x7f060285

.field public static final online_checkout_checkout_link_qr_code_border:I = 0x7f060286

.field public static final orderhub_link_color:I = 0x7f060287

.field public static final orderhub_text_color_canceled:I = 0x7f060288

.field public static final orderhub_text_color_completed:I = 0x7f060289

.field public static final orderhub_text_color_overdue:I = 0x7f06028a

.field public static final orderhub_text_color_progress:I = 0x7f06028b

.field public static final payment_request_amount_invalid:I = 0x7f06028c

.field public static final payroll_upsell_image_background:I = 0x7f06028d

.field public static final pin_submit_color:I = 0x7f06028e

.field public static final primary_dark_material_dark:I = 0x7f06028f

.field public static final primary_dark_material_light:I = 0x7f060290

.field public static final primary_material_dark:I = 0x7f060291

.field public static final primary_material_light:I = 0x7f060292

.field public static final primary_text_default_material_dark:I = 0x7f060293

.field public static final primary_text_default_material_light:I = 0x7f060294

.field public static final primary_text_disabled_material_dark:I = 0x7f060295

.field public static final primary_text_disabled_material_light:I = 0x7f060296

.field public static final profile_row_text_link:I = 0x7f060297

.field public static final qr_code_border:I = 0x7f060298

.field public static final quick_amounts_add:I = 0x7f060299

.field public static final quick_amounts_preview_background:I = 0x7f06029a

.field public static final quick_amounts_preview_border:I = 0x7f06029b

.field public static final quick_amounts_preview_group:I = 0x7f06029c

.field public static final r6_first_time_video_background:I = 0x7f06029d

.field public static final r6_first_time_video_fallback_blue:I = 0x7f06029e

.field public static final r6_first_time_video_subtitle:I = 0x7f06029f

.field public static final read_dot_border_color:I = 0x7f0602a0

.field public static final read_notification_text_color:I = 0x7f0602a1

.field public static final register_blue:I = 0x7f0602a2

.field public static final register_blue_button_color:I = 0x7f0602a3

.field public static final register_blue_pressed:I = 0x7f0602a4

.field public static final reward_row_initial_cirlce_bg_applied:I = 0x7f0602a5

.field public static final reward_row_initial_cirlce_bg_disabled:I = 0x7f0602a6

.field public static final reward_row_initial_cirlce_bg_enabled:I = 0x7f0602a7

.field public static final ripple_material_dark:I = 0x7f0602a8

.field public static final ripple_material_light:I = 0x7f0602a9

.field public static final sales_report_amount_count_toggle_background_color_selected:I = 0x7f0602aa

.field public static final sales_report_amount_count_toggle_background_color_unselected:I = 0x7f0602ab

.field public static final sales_report_amount_count_toggle_text_color_selected:I = 0x7f0602ac

.field public static final sales_report_amount_count_toggle_text_color_unselected:I = 0x7f0602ad

.field public static final sales_report_comparison_range_background:I = 0x7f0602ae

.field public static final sales_report_comparison_range_text_color:I = 0x7f0602af

.field public static final sales_report_emplyee_search_no_employees_found:I = 0x7f0602b0

.field public static final sales_report_info_color:I = 0x7f0602b1

.field public static final sales_report_payment_method_card_bar_color:I = 0x7f0602b2

.field public static final sales_report_payment_method_cash_bar_color:I = 0x7f0602b3

.field public static final sales_report_payment_method_emoney_bar_color:I = 0x7f0602b4

.field public static final sales_report_payment_method_external_bar_color:I = 0x7f0602b5

.field public static final sales_report_payment_method_fees_bar_color:I = 0x7f0602b6

.field public static final sales_report_payment_method_gift_card_bar_color:I = 0x7f0602b7

.field public static final sales_report_payment_method_installment_bar_color:I = 0x7f0602b8

.field public static final sales_report_payment_method_other_bar_color:I = 0x7f0602b9

.field public static final sales_report_payment_method_split_tender_bar_color:I = 0x7f0602ba

.field public static final sales_report_payment_method_third_party_card_bar_color:I = 0x7f0602bb

.field public static final sales_report_payment_method_zero_amount_bar_color:I = 0x7f0602bc

.field public static final sales_report_prominent_edge_color:I = 0x7f0602bd

.field public static final sales_report_summary_details_row_text_color:I = 0x7f0602be

.field public static final sales_report_summary_details_subrow_text_color:I = 0x7f0602bf

.field public static final sales_report_summary_negative_percentage_color:I = 0x7f0602c0

.field public static final sales_report_summary_positive_percentage_color:I = 0x7f0602c1

.field public static final sales_report_time_selector_background:I = 0x7f0602c2

.field public static final sales_report_top_bar_comparison_icon_color:I = 0x7f0602c3

.field public static final sales_report_top_bar_comparison_icon_disabled:I = 0x7f0602c4

.field public static final sales_report_top_bar_comparison_icon_enabled:I = 0x7f0602c5

.field public static final sales_report_top_bar_comparison_icon_selected:I = 0x7f0602c6

.field public static final sales_report_top_bar_title_text_color:I = 0x7f0602c7

.field public static final secondary_text_default_material_dark:I = 0x7f0602c8

.field public static final secondary_text_default_material_light:I = 0x7f0602c9

.field public static final secondary_text_disabled_material_dark:I = 0x7f0602ca

.field public static final secondary_text_disabled_material_light:I = 0x7f0602cb

.field public static final selected_tab_background_color:I = 0x7f0602cc

.field public static final selected_tab_text_color:I = 0x7f0602cd

.field public static final setup_dialog_primary_end_color:I = 0x7f0602ce

.field public static final setup_dialog_primary_pressed_end_color:I = 0x7f0602cf

.field public static final setup_dialog_primary_pressed_start_color:I = 0x7f0602d0

.field public static final setup_dialog_primary_start_color:I = 0x7f0602d1

.field public static final setup_dialog_secondary_color:I = 0x7f0602d2

.field public static final setup_guide_gradient_end:I = 0x7f0602d3

.field public static final setup_guide_gradient_start:I = 0x7f0602d4

.field public static final setup_guide_header_gradient_end:I = 0x7f0602d5

.field public static final setup_guide_header_gradient_start:I = 0x7f0602d6

.field public static final skip_color_bg:I = 0x7f0602d7

.field public static final skip_color_bg_pressed:I = 0x7f0602d8

.field public static final snackbar_text_color:I = 0x7f0602d9

.field public static final speedtest_failure_glyph_color:I = 0x7f0602da

.field public static final speedtest_link_color:I = 0x7f0602db

.field public static final speedtest_success_glyph_color:I = 0x7f0602dc

.field public static final splash_page_page_indicator_highlight_color:I = 0x7f0602dd

.field public static final sq_dark_gray:I = 0x7f0602de

.field public static final sq_dark_gray_button_color_pressed:I = 0x7f0602df

.field public static final sq_dark_gray_pressed:I = 0x7f0602e0

.field public static final sq_light_gray:I = 0x7f0602e1

.field public static final sq_light_gray_pressed:I = 0x7f0602e2

.field public static final square_card_logo_color:I = 0x7f0602e3

.field public static final square_card_signature_button_background:I = 0x7f0602e4

.field public static final square_card_signature_icon_colors:I = 0x7f0602e5

.field public static final square_card_signature_outline:I = 0x7f0602e6

.field public static final square_card_signature_preview_background:I = 0x7f0602e7

.field public static final square_card_signature_preview_signature:I = 0x7f0602e8

.field public static final square_card_signature_preview_signature_background:I = 0x7f0602e9

.field public static final square_card_signature_selected_indicator:I = 0x7f0602ea

.field public static final switch_thumb_disabled_material_dark:I = 0x7f0602eb

.field public static final switch_thumb_disabled_material_light:I = 0x7f0602ec

.field public static final switch_thumb_material_dark:I = 0x7f0602ed

.field public static final switch_thumb_material_light:I = 0x7f0602ee

.field public static final switch_thumb_normal_material_dark:I = 0x7f0602ef

.field public static final switch_thumb_normal_material_light:I = 0x7f0602f0

.field public static final tab_bar_background_color:I = 0x7f0602f1

.field public static final test_mtrl_calendar_day:I = 0x7f0602f2

.field public static final test_mtrl_calendar_day_selected:I = 0x7f0602f3

.field public static final text_color:I = 0x7f0602f4

.field public static final text_color_disabled:I = 0x7f0602f5

.field public static final text_dispute_description:I = 0x7f0602f6

.field public static final text_selector_switcher_button:I = 0x7f0602f7

.field public static final tile_text_selector_dark_gray:I = 0x7f0602f8

.field public static final title_color:I = 0x7f0602f9

.field public static final title_color_default:I = 0x7f0602fa

.field public static final title_color_disabled:I = 0x7f0602fb

.field public static final title_color_purchase:I = 0x7f0602fc

.field public static final title_color_refund:I = 0x7f0602fd

.field public static final tooltip_background_dark:I = 0x7f0602fe

.field public static final tooltip_background_light:I = 0x7f0602ff

.field public static final transaction_details_tender_customer_initials:I = 0x7f060300

.field public static final transaction_details_tender_customer_name:I = 0x7f060301

.field public static final transactions_history_divider:I = 0x7f060302

.field public static final transactions_history_header_background:I = 0x7f060303

.field public static final transactions_history_row_subtitle:I = 0x7f060304

.field public static final transactions_history_search_bar_icon:I = 0x7f060305

.field public static final transactions_history_search_bar_tap_disabled_text:I = 0x7f060306

.field public static final transactions_history_search_bar_tap_enabled_text:I = 0x7f060307

.field public static final transactions_history_search_bar_text:I = 0x7f060308

.field public static final transactions_history_search_bar_text_hint:I = 0x7f060309

.field public static final transactions_history_top_message_panel_message_text:I = 0x7f06030a

.field public static final tutorial_pager_background_color:I = 0x7f06030b

.field public static final unread_notification_text_color:I = 0x7f06030c

.field public static final unselected_tab_text_color:I = 0x7f06030d

.field public static final vpi__background_holo_dark:I = 0x7f06030e

.field public static final vpi__background_holo_light:I = 0x7f06030f

.field public static final vpi__bright_foreground_disabled_holo_dark:I = 0x7f060310

.field public static final vpi__bright_foreground_disabled_holo_light:I = 0x7f060311

.field public static final vpi__bright_foreground_holo_dark:I = 0x7f060312

.field public static final vpi__bright_foreground_holo_light:I = 0x7f060313

.field public static final vpi__bright_foreground_inverse_holo_dark:I = 0x7f060314

.field public static final vpi__bright_foreground_inverse_holo_light:I = 0x7f060315

.field public static final warning_banner_row_title_color:I = 0x7f060316

.field public static final white_transparent_five_percent:I = 0x7f060317


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10721
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
