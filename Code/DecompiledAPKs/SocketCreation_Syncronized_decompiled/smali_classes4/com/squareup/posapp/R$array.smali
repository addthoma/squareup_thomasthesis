.class public final Lcom/squareup/posapp/R$array;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/posapp/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "array"
.end annotation


# static fields
.field public static final activation_legal_url_names_ca:I = 0x7f030000

.field public static final activation_legal_url_names_jp:I = 0x7f030001

.field public static final activation_legal_url_names_other:I = 0x7f030002

.field public static final activation_legal_url_names_us:I = 0x7f030003

.field public static final activation_legal_urls_ca:I = 0x7f030004

.field public static final activation_legal_urls_other:I = 0x7f030005

.field public static final activation_legal_urls_us:I = 0x7f030006

.field public static final bank_account_types:I = 0x7f030007

.field public static final ca_province_abbrevs:I = 0x7f030008

.field public static final ca_provinces:I = 0x7f030009

.field public static final day_of_week:I = 0x7f03000a

.field public static final day_of_week_abbreviations:I = 0x7f03000b

.field public static final day_of_week_responsive:I = 0x7f03000c

.field public static final defaultChartViewColors:I = 0x7f03000d

.field public static final defaultChartViewSelectedColors:I = 0x7f03000e

.field public static final duration_hour_options:I = 0x7f03000f

.field public static final duration_minute_options:I = 0x7f030010

.field public static final fee_inclusion_types_short:I = 0x7f030011

.field public static final invoice_file_attachment_sources:I = 0x7f030012

.field public static final merchant_profile_colors:I = 0x7f030013

.field public static final photo_sources:I = 0x7f030014

.field public static final refund_reasons:I = 0x7f030015

.field public static final us_state_abbrevs:I = 0x7f030016

.field public static final us_states:I = 0x7f030017


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
