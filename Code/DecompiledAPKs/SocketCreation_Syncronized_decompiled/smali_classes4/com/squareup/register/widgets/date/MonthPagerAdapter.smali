.class final Lcom/squareup/register/widgets/date/MonthPagerAdapter;
.super Landroidx/viewpager/widget/PagerAdapter;
.source "MonthPagerAdapter.java"


# instance fields
.field private final activeViews:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/register/widgets/date/MonthView;",
            ">;"
        }
    .end annotation
.end field

.field private final dateRange:Lcom/squareup/register/widgets/date/DateRange;

.field private final listener:Lcom/squareup/register/widgets/date/MonthView$SelectionListener;

.field private final recycledViews:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue<",
            "Lcom/squareup/register/widgets/date/MonthView;",
            ">;"
        }
    .end annotation
.end field

.field private selectedRange:Lcom/squareup/register/widgets/date/DateRange;


# direct methods
.method constructor <init>(Lcom/squareup/register/widgets/date/MonthView$SelectionListener;Lcom/squareup/register/widgets/date/DateRange;)V
    .locals 1

    .line 30
    invoke-direct {p0}, Landroidx/viewpager/widget/PagerAdapter;-><init>()V

    .line 20
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/register/widgets/date/MonthPagerAdapter;->activeViews:Ljava/util/Set;

    .line 21
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/squareup/register/widgets/date/MonthPagerAdapter;->recycledViews:Ljava/util/Queue;

    .line 31
    iput-object p1, p0, Lcom/squareup/register/widgets/date/MonthPagerAdapter;->listener:Lcom/squareup/register/widgets/date/MonthView$SelectionListener;

    .line 32
    iput-object p2, p0, Lcom/squareup/register/widgets/date/MonthPagerAdapter;->dateRange:Lcom/squareup/register/widgets/date/DateRange;

    return-void
.end method

.method private calculateRelativeMonth(Ljava/util/Calendar;)I
    .locals 2

    const/4 v0, 0x1

    .line 126
    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0xc

    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Ljava/util/Calendar;->get(I)I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method private calendarForPosition(I)Ljava/util/Calendar;
    .locals 3

    .line 115
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 116
    iget-object v1, p0, Lcom/squareup/register/widgets/date/MonthPagerAdapter;->dateRange:Lcom/squareup/register/widgets/date/DateRange;

    invoke-virtual {v1}, Lcom/squareup/register/widgets/date/DateRange;->getStartCalendar()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/4 v1, 0x2

    .line 117
    invoke-virtual {v0, v1, p1}, Ljava/util/Calendar;->add(II)V

    return-object v0
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .line 65
    check-cast p3, Lcom/squareup/register/widgets/date/MonthView;

    .line 66
    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 67
    iget-object p1, p0, Lcom/squareup/register/widgets/date/MonthPagerAdapter;->activeViews:Ljava/util/Set;

    invoke-interface {p1, p3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 68
    iget-object p1, p0, Lcom/squareup/register/widgets/date/MonthPagerAdapter;->recycledViews:Ljava/util/Queue;

    invoke-interface {p1, p3}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getCount()I
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/register/widgets/date/MonthPagerAdapter;->dateRange:Lcom/squareup/register/widgets/date/DateRange;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/date/DateRange;->toMonths()I

    move-result v0

    return v0
.end method

.method getMonthForPosition(I)I
    .locals 1

    .line 111
    invoke-direct {p0, p1}, Lcom/squareup/register/widgets/date/MonthPagerAdapter;->calendarForPosition(I)Ljava/util/Calendar;

    move-result-object p1

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result p1

    return p1
.end method

.method getYearForPosition(I)I
    .locals 1

    .line 102
    invoke-direct {p0, p1}, Lcom/squareup/register/widgets/date/MonthPagerAdapter;->calendarForPosition(I)Ljava/util/Calendar;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result p1

    return p1
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 3

    .line 44
    iget-object v0, p0, Lcom/squareup/register/widgets/date/MonthPagerAdapter;->recycledViews:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/date/MonthView;

    if-nez v0, :cond_0

    .line 46
    sget v0, Lcom/squareup/widgets/pos/R$layout;->month_pager_view:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/date/MonthView;

    .line 47
    iget-object v1, p0, Lcom/squareup/register/widgets/date/MonthPagerAdapter;->listener:Lcom/squareup/register/widgets/date/MonthView$SelectionListener;

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/date/MonthView;->setSelectionListener(Lcom/squareup/register/widgets/date/MonthView$SelectionListener;)V

    .line 50
    :cond_0
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 51
    iget-object p1, p0, Lcom/squareup/register/widgets/date/MonthPagerAdapter;->activeViews:Ljava/util/Set;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 53
    invoke-virtual {p0, p2}, Lcom/squareup/register/widgets/date/MonthPagerAdapter;->getYearForPosition(I)I

    move-result p1

    .line 54
    invoke-virtual {p0, p2}, Lcom/squareup/register/widgets/date/MonthPagerAdapter;->getMonthForPosition(I)I

    move-result p2

    .line 55
    iget-object v1, p0, Lcom/squareup/register/widgets/date/MonthPagerAdapter;->dateRange:Lcom/squareup/register/widgets/date/DateRange;

    invoke-virtual {v1}, Lcom/squareup/register/widgets/date/DateRange;->getStartCalendar()Ljava/util/Calendar;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/register/widgets/date/MonthPagerAdapter;->dateRange:Lcom/squareup/register/widgets/date/DateRange;

    invoke-virtual {v2}, Lcom/squareup/register/widgets/date/DateRange;->getEndCalendar()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/squareup/register/widgets/date/MonthView;->setMonth(IILjava/util/Calendar;Ljava/util/Calendar;)V

    .line 57
    iget-object p1, p0, Lcom/squareup/register/widgets/date/MonthPagerAdapter;->selectedRange:Lcom/squareup/register/widgets/date/DateRange;

    if-eqz p1, :cond_1

    .line 58
    invoke-virtual {p1}, Lcom/squareup/register/widgets/date/DateRange;->getStartDate()Ljava/util/Date;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/register/widgets/date/MonthPagerAdapter;->selectedRange:Lcom/squareup/register/widgets/date/DateRange;

    invoke-virtual {p2}, Lcom/squareup/register/widgets/date/DateRange;->getEndDate()Ljava/util/Date;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lcom/squareup/register/widgets/date/MonthView;->dateRangeSelected(Ljava/util/Date;Ljava/util/Date;)V

    :cond_1
    return-object v0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 0

    if-ne p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method positionForDate(Ljava/util/Calendar;)I
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/register/widgets/date/MonthPagerAdapter;->dateRange:Lcom/squareup/register/widgets/date/DateRange;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/date/DateRange;->getStartCalendar()Ljava/util/Calendar;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/register/widgets/date/MonthPagerAdapter;->calculateRelativeMonth(Ljava/util/Calendar;)I

    move-result v0

    .line 92
    invoke-direct {p0, p1}, Lcom/squareup/register/widgets/date/MonthPagerAdapter;->calculateRelativeMonth(Ljava/util/Calendar;)I

    move-result p1

    sub-int/2addr p1, v0

    return p1
.end method

.method setSelectedRange(Lcom/squareup/register/widgets/date/DateRange;)V
    .locals 3

    .line 76
    iput-object p1, p0, Lcom/squareup/register/widgets/date/MonthPagerAdapter;->selectedRange:Lcom/squareup/register/widgets/date/DateRange;

    .line 79
    iget-object p1, p0, Lcom/squareup/register/widgets/date/MonthPagerAdapter;->activeViews:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/date/MonthView;

    .line 80
    iget-object v1, p0, Lcom/squareup/register/widgets/date/MonthPagerAdapter;->selectedRange:Lcom/squareup/register/widgets/date/DateRange;

    invoke-virtual {v1}, Lcom/squareup/register/widgets/date/DateRange;->getStartDate()Ljava/util/Date;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/register/widgets/date/MonthPagerAdapter;->selectedRange:Lcom/squareup/register/widgets/date/DateRange;

    invoke-virtual {v2}, Lcom/squareup/register/widgets/date/DateRange;->getEndDate()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/register/widgets/date/MonthView;->dateRangeSelected(Ljava/util/Date;Ljava/util/Date;)V

    goto :goto_0

    :cond_0
    return-void
.end method
