.class public Lcom/squareup/register/widgets/card/CardEditor;
.super Lcom/squareup/noho/NohoLinearLayout;
.source "CardEditor.java"

# interfaces
.implements Lcom/squareup/register/widgets/card/OnBrandListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;,
        Lcom/squareup/register/widgets/card/CardEditor$CardTextWatcher;,
        Lcom/squareup/register/widgets/card/CardEditor$ParentComponent;
    }
.end annotation


# static fields
.field private static final CVV_HINT_SHORT_PLACEHOLDER:Ljava/lang/String; = "####"

.field private static final CVV_MAX_LENGTH:I = 0x4

.field private static final ENOUGH_POSTAL_FOR_LAST4:Ljava/lang/String; = "00000"


# instance fields
.field private final animDuration:J

.field private final animator:Lcom/squareup/widgets/PersistentViewAnimator;

.field private cardBrand:Lcom/squareup/Card$Brand;

.field private final cardGlyph:Lcom/squareup/glyph/SquareGlyphView;

.field private final cardNumberScrubber:Lcom/squareup/text/CardNumberScrubber;

.field private compact:Z

.field currency:Lcom/squareup/protos/common/CurrencyCode;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final cvv:Lcom/squareup/register/widgets/card/CvvEditor;

.field private final cvvWatcher:Lcom/squareup/text/AutoAdvanceTextWatcher;

.field private final details:Landroid/view/ViewGroup;

.field private final detailsInAnimOverTime:Landroid/view/animation/Animation;

.field private final detailsOutAnimImmediate:Landroid/view/animation/Animation;

.field private final detailsOutAnimOverTime:Landroid/view/animation/Animation;

.field private final expiration:Lcom/squareup/register/widgets/card/ExpirationEditor;

.field private hideGlyphBasedOnHint:Z

.field private final number:Lcom/squareup/register/widgets/card/PanEditor;

.field private numberHintBeforeFocused:Ljava/lang/CharSequence;

.field private final numberInAnimImmediate:Landroid/view/animation/Animation;

.field private final numberInAnimOverTime:Landroid/view/animation/Animation;

.field private final numberLastDigits:Landroid/widget/TextView;

.field private final numberOutAnimOverTime:Landroid/view/animation/Animation;

.field private onCardListener:Lcom/squareup/register/widgets/card/OnCardListener;

.field private onFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

.field private final postal:Lcom/squareup/register/widgets/card/PostalEditText;

.field private final postalKeyboardSwitch:Lcom/squareup/glyph/SquareGlyphView;

.field private final postalPiiScrubber:Lcom/squareup/accessibility/AccessibilityScrubber;

.field private final shadowPainter:Lcom/squareup/noho/EdgePainter;

.field private showPostalCode:Z

.field private strategy:Lcom/squareup/register/widgets/card/PanValidationStrategy;

.field private syncCardGlyphToBrand:Z

.field private textSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .line 111
    invoke-direct {p0, p1, p2}, Lcom/squareup/noho/NohoLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 89
    new-instance v0, Lcom/squareup/text/CardNumberScrubber;

    invoke-direct {v0}, Lcom/squareup/text/CardNumberScrubber;-><init>()V

    iput-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->cardNumberScrubber:Lcom/squareup/text/CardNumberScrubber;

    .line 98
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    iput-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->cardBrand:Lcom/squareup/Card$Brand;

    const/4 v0, 0x1

    .line 99
    iput-boolean v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->syncCardGlyphToBrand:Z

    .line 105
    iput-boolean v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->hideGlyphBasedOnHint:Z

    .line 112
    sget v0, Lcom/squareup/widgets/pos/R$layout;->card_editor:I

    invoke-static {p1, v0, p0}, Lcom/squareup/register/widgets/card/CardEditor;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 113
    const-class v0, Lcom/squareup/register/widgets/card/CardEditor$ParentComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->componentInParent(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/card/CardEditor$ParentComponent;

    invoke-interface {v0, p0}, Lcom/squareup/register/widgets/card/CardEditor$ParentComponent;->inject(Lcom/squareup/register/widgets/card/CardEditor;)V

    .line 115
    sget-object v0, Lcom/squareup/widgets/pos/R$styleable;->CardEditor:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 116
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->CardEditor_hideGlyph:I

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    .line 117
    sget v1, Lcom/squareup/widgets/pos/R$styleable;->CardEditor_compact:I

    invoke-virtual {p1, v1, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/squareup/register/widgets/card/CardEditor;->compact:Z

    .line 119
    sget v1, Lcom/squareup/widgets/pos/R$styleable;->CardEditor_sqInnerShadowHeight:I

    const/4 v2, -0x1

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 120
    sget v3, Lcom/squareup/widgets/pos/R$styleable;->CardEditor_sqInnerShadowColor:I

    invoke-virtual {p1, v3, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    .line 121
    new-instance v4, Lcom/squareup/noho/EdgePainter;

    invoke-direct {v4, p0, v1, v3}, Lcom/squareup/noho/EdgePainter;-><init>(Landroid/view/View;II)V

    iput-object v4, p0, Lcom/squareup/register/widgets/card/CardEditor;->shadowPainter:Lcom/squareup/noho/EdgePainter;

    .line 122
    iget-object v1, p0, Lcom/squareup/register/widgets/card/CardEditor;->shadowPainter:Lcom/squareup/noho/EdgePainter;

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Lcom/squareup/noho/EdgePainter;->addEdges(I)V

    .line 124
    sget v1, Lcom/squareup/widgets/pos/R$styleable;->CardEditor_android_textSize:I

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/squareup/register/widgets/card/CardEditor;->textSize:I

    .line 126
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 128
    new-instance p1, Lcom/squareup/register/widgets/card/CardEditor$1;

    invoke-direct {p1, p0}, Lcom/squareup/register/widgets/card/CardEditor$1;-><init>(Lcom/squareup/register/widgets/card/CardEditor;)V

    .line 134
    sget v1, Lcom/squareup/widgets/pos/R$id;->animator:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/widgets/PersistentViewAnimator;

    iput-object v1, p0, Lcom/squareup/register/widgets/card/CardEditor;->animator:Lcom/squareup/widgets/PersistentViewAnimator;

    .line 136
    sget v1, Lcom/squareup/widgets/pos/R$id;->card_glyph:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v1, p0, Lcom/squareup/register/widgets/card/CardEditor;->cardGlyph:Lcom/squareup/glyph/SquareGlyphView;

    .line 137
    iget-object v1, p0, Lcom/squareup/register/widgets/card/CardEditor;->cardGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardEditor;->getDefaultGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    const/16 v1, 0x8

    if-eqz p2, :cond_0

    .line 139
    iget-object p2, p0, Lcom/squareup/register/widgets/card/CardEditor;->cardGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p2, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    goto :goto_0

    .line 141
    :cond_0
    iget-object p2, p0, Lcom/squareup/register/widgets/card/CardEditor;->cardGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p2, p1}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    :goto_0
    sget p2, Lcom/squareup/widgets/pos/R$id;->number:I

    invoke-static {p0, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/register/widgets/card/PanEditor;

    iput-object p2, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    .line 146
    sget p2, Lcom/squareup/widgets/pos/R$id;->number_last_digits:I

    invoke-static {p0, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/squareup/register/widgets/card/CardEditor;->numberLastDigits:Landroid/widget/TextView;

    .line 147
    iget-object p2, p0, Lcom/squareup/register/widgets/card/CardEditor;->numberLastDigits:Landroid/widget/TextView;

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    sget p1, Lcom/squareup/widgets/pos/R$id;->details:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->details:Landroid/view/ViewGroup;

    .line 150
    sget p1, Lcom/squareup/widgets/pos/R$id;->expiration_editor:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/register/widgets/card/ExpirationEditor;

    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->expiration:Lcom/squareup/register/widgets/card/ExpirationEditor;

    .line 151
    sget p1, Lcom/squareup/widgets/pos/R$id;->cvv_editor:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/register/widgets/card/CvvEditor;

    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvv:Lcom/squareup/register/widgets/card/CvvEditor;

    .line 154
    new-instance p1, Lcom/squareup/register/widgets/card/PermissiveCardPanValidationStrategy;

    invoke-direct {p1}, Lcom/squareup/register/widgets/card/PermissiveCardPanValidationStrategy;-><init>()V

    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/card/CardEditor;->setStrategy(Lcom/squareup/register/widgets/card/PanValidationStrategy;)V

    .line 156
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvv:Lcom/squareup/register/widgets/card/CvvEditor;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/card/CvvEditor;->getHint()Ljava/lang/CharSequence;

    move-result-object p1

    .line 157
    iget-object p2, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvv:Lcom/squareup/register/widgets/card/CvvEditor;

    new-instance v3, Lcom/squareup/register/widgets/card/CardEditor$2;

    invoke-direct {v3, p0, p1}, Lcom/squareup/register/widgets/card/CardEditor$2;-><init>(Lcom/squareup/register/widgets/card/CardEditor;Ljava/lang/CharSequence;)V

    invoke-virtual {p2, v3}, Lcom/squareup/register/widgets/card/CvvEditor;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 179
    sget p1, Lcom/squareup/widgets/pos/R$id;->postal_pii_scrubber:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/accessibility/AccessibilityScrubber;

    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->postalPiiScrubber:Lcom/squareup/accessibility/AccessibilityScrubber;

    .line 180
    sget p1, Lcom/squareup/widgets/pos/R$id;->postal:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/register/widgets/card/PostalEditText;

    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->postal:Lcom/squareup/register/widgets/card/PostalEditText;

    .line 182
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-virtual {p1, p0}, Lcom/squareup/register/widgets/card/PanEditor;->setOnBrandListener(Lcom/squareup/register/widgets/card/OnBrandListener;)V

    .line 183
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    new-instance p2, Lcom/squareup/register/widgets/card/CardEditor$3;

    invoke-direct {p2, p0}, Lcom/squareup/register/widgets/card/CardEditor$3;-><init>(Lcom/squareup/register/widgets/card/CardEditor;)V

    invoke-virtual {p1, p2}, Lcom/squareup/register/widgets/card/PanEditor;->setOnPanListener(Lcom/squareup/register/widgets/card/OnPanListener;)V

    .line 211
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    new-instance p2, Lcom/squareup/register/widgets/card/-$$Lambda$CardEditor$4b1wsoQP24Ny_eZm6erxwh_it1Q;

    invoke-direct {p2, p0}, Lcom/squareup/register/widgets/card/-$$Lambda$CardEditor$4b1wsoQP24Ny_eZm6erxwh_it1Q;-><init>(Lcom/squareup/register/widgets/card/CardEditor;)V

    invoke-virtual {p1, p2}, Lcom/squareup/register/widgets/card/PanEditor;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 225
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    new-instance p2, Lcom/squareup/register/widgets/card/CardEditor$4;

    invoke-direct {p2, p0}, Lcom/squareup/register/widgets/card/CardEditor$4;-><init>(Lcom/squareup/register/widgets/card/CardEditor;)V

    invoke-virtual {p1, p2}, Lcom/squareup/register/widgets/card/PanEditor;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 234
    iget p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->textSize:I

    if-eq p1, v2, :cond_1

    .line 235
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-virtual {p0}, Lcom/squareup/register/widgets/card/CardEditor;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    iget v2, p0, Lcom/squareup/register/widgets/card/CardEditor;->textSize:I

    invoke-virtual {p2, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    int-to-float p2, p2

    invoke-virtual {p1, v0, p2}, Lcom/squareup/register/widgets/card/PanEditor;->setTextSize(IF)V

    .line 238
    :cond_1
    new-instance p1, Lcom/squareup/register/widgets/card/CardEditor$CardTextWatcher;

    const/4 p2, 0x0

    invoke-direct {p1, p0, p2}, Lcom/squareup/register/widgets/card/CardEditor$CardTextWatcher;-><init>(Lcom/squareup/register/widgets/card/CardEditor;Lcom/squareup/register/widgets/card/CardEditor$1;)V

    .line 240
    iget-boolean p2, p0, Lcom/squareup/register/widgets/card/CardEditor;->compact:Z

    if-eqz p2, :cond_2

    .line 241
    iget-object p2, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvv:Lcom/squareup/register/widgets/card/CvvEditor;

    invoke-virtual {p2, v1}, Lcom/squareup/register/widgets/card/CvvEditor;->setVisibility(I)V

    .line 242
    iget-object p2, p0, Lcom/squareup/register/widgets/card/CardEditor;->postal:Lcom/squareup/register/widgets/card/PostalEditText;

    invoke-virtual {p2, v1}, Lcom/squareup/register/widgets/card/PostalEditText;->setVisibility(I)V

    .line 245
    :cond_2
    iget-object p2, p0, Lcom/squareup/register/widgets/card/CardEditor;->expiration:Lcom/squareup/register/widgets/card/ExpirationEditor;

    new-instance v0, Lcom/squareup/text/AutoAdvanceTextWatcher;

    iget-boolean v1, p0, Lcom/squareup/register/widgets/card/CardEditor;->compact:Z

    iget-object v2, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvv:Lcom/squareup/register/widgets/card/CvvEditor;

    const/4 v3, 0x5

    invoke-direct {v0, v1, p2, v2, v3}, Lcom/squareup/text/AutoAdvanceTextWatcher;-><init>(ZLandroid/view/View;Landroid/view/View;I)V

    invoke-virtual {p2, v0}, Lcom/squareup/register/widgets/card/ExpirationEditor;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 247
    iget-object p2, p0, Lcom/squareup/register/widgets/card/CardEditor;->expiration:Lcom/squareup/register/widgets/card/ExpirationEditor;

    invoke-virtual {p2, p1}, Lcom/squareup/register/widgets/card/ExpirationEditor;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 248
    iget-object p2, p0, Lcom/squareup/register/widgets/card/CardEditor;->expiration:Lcom/squareup/register/widgets/card/ExpirationEditor;

    new-instance v0, Lcom/squareup/register/widgets/card/-$$Lambda$CardEditor$XruimnqN5BhXTg_Uc7Vlr-l9Z3M;

    invoke-direct {v0, p0}, Lcom/squareup/register/widgets/card/-$$Lambda$CardEditor$XruimnqN5BhXTg_Uc7Vlr-l9Z3M;-><init>(Lcom/squareup/register/widgets/card/CardEditor;)V

    invoke-virtual {p2, v0}, Lcom/squareup/register/widgets/card/ExpirationEditor;->setOnDeleteKeyListener(Lcom/squareup/widgets/OnDeleteEditText$OnDeleteKeyListener;)V

    .line 249
    new-instance p2, Lcom/squareup/register/widgets/card/-$$Lambda$CardEditor$qUoa7es_IGbf05sFzJvVyrY62UA;

    invoke-direct {p2, p0}, Lcom/squareup/register/widgets/card/-$$Lambda$CardEditor$qUoa7es_IGbf05sFzJvVyrY62UA;-><init>(Lcom/squareup/register/widgets/card/CardEditor;)V

    invoke-static {p0, p2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 252
    new-instance p2, Lcom/squareup/register/widgets/card/-$$Lambda$CardEditor$368aiBvPT03kF1ovf6F56PToAb4;

    invoke-direct {p2, p0}, Lcom/squareup/register/widgets/card/-$$Lambda$CardEditor$368aiBvPT03kF1ovf6F56PToAb4;-><init>(Lcom/squareup/register/widgets/card/CardEditor;)V

    invoke-static {p0, p2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 255
    new-instance p2, Lcom/squareup/text/AutoAdvanceTextWatcher;

    iget-boolean v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->compact:Z

    iget-object v1, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvv:Lcom/squareup/register/widgets/card/CvvEditor;

    iget-object v2, p0, Lcom/squareup/register/widgets/card/CardEditor;->postal:Lcom/squareup/register/widgets/card/PostalEditText;

    sget-object v3, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    invoke-virtual {v3}, Lcom/squareup/Card$Brand;->cvvLength()I

    move-result v3

    invoke-direct {p2, v0, v1, v2, v3}, Lcom/squareup/text/AutoAdvanceTextWatcher;-><init>(ZLandroid/view/View;Landroid/view/View;I)V

    iput-object p2, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvvWatcher:Lcom/squareup/text/AutoAdvanceTextWatcher;

    .line 256
    iget-object p2, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvv:Lcom/squareup/register/widgets/card/CvvEditor;

    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvvWatcher:Lcom/squareup/text/AutoAdvanceTextWatcher;

    invoke-virtual {p2, v0}, Lcom/squareup/register/widgets/card/CvvEditor;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 257
    iget-object p2, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvv:Lcom/squareup/register/widgets/card/CvvEditor;

    invoke-virtual {p2, p1}, Lcom/squareup/register/widgets/card/CvvEditor;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 258
    iget-object p2, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvv:Lcom/squareup/register/widgets/card/CvvEditor;

    new-instance v0, Lcom/squareup/register/widgets/card/-$$Lambda$CardEditor$tMz8Xv5s32QWwQlpEcuR200AqXI;

    invoke-direct {v0, p0}, Lcom/squareup/register/widgets/card/-$$Lambda$CardEditor$tMz8Xv5s32QWwQlpEcuR200AqXI;-><init>(Lcom/squareup/register/widgets/card/CardEditor;)V

    invoke-virtual {p2, v0}, Lcom/squareup/register/widgets/card/CvvEditor;->setOnDeleteKeyListener(Lcom/squareup/widgets/OnDeleteEditText$OnDeleteKeyListener;)V

    .line 259
    new-instance p2, Lcom/squareup/register/widgets/card/-$$Lambda$CardEditor$hRHTNsSg_fWx3DX3nAoApeoO3Tg;

    invoke-direct {p2, p0}, Lcom/squareup/register/widgets/card/-$$Lambda$CardEditor$hRHTNsSg_fWx3DX3nAoApeoO3Tg;-><init>(Lcom/squareup/register/widgets/card/CardEditor;)V

    invoke-static {p0, p2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 262
    new-instance p2, Lcom/squareup/register/widgets/card/-$$Lambda$CardEditor$k-QSrRRzhFLcUtnqpDlDw4nMxkY;

    invoke-direct {p2, p0}, Lcom/squareup/register/widgets/card/-$$Lambda$CardEditor$k-QSrRRzhFLcUtnqpDlDw4nMxkY;-><init>(Lcom/squareup/register/widgets/card/CardEditor;)V

    invoke-static {p0, p2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 268
    iget-object p2, p0, Lcom/squareup/register/widgets/card/CardEditor;->postal:Lcom/squareup/register/widgets/card/PostalEditText;

    invoke-virtual {p2, p1}, Lcom/squareup/register/widgets/card/PostalEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 269
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->postal:Lcom/squareup/register/widgets/card/PostalEditText;

    new-instance p2, Lcom/squareup/register/widgets/card/-$$Lambda$CardEditor$hewhondEyxAPP4aP2MkA6EjMI_o;

    invoke-direct {p2, p0}, Lcom/squareup/register/widgets/card/-$$Lambda$CardEditor$hewhondEyxAPP4aP2MkA6EjMI_o;-><init>(Lcom/squareup/register/widgets/card/CardEditor;)V

    invoke-virtual {p1, p2}, Lcom/squareup/register/widgets/card/PostalEditText;->setOnDeleteKeyListener(Lcom/squareup/widgets/OnDeleteEditText$OnDeleteKeyListener;)V

    .line 271
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->postal:Lcom/squareup/register/widgets/card/PostalEditText;

    new-instance p2, Lcom/squareup/register/widgets/card/CardEditor$5;

    invoke-direct {p2, p0}, Lcom/squareup/register/widgets/card/CardEditor$5;-><init>(Lcom/squareup/register/widgets/card/CardEditor;)V

    invoke-virtual {p1, p2}, Lcom/squareup/register/widgets/card/PostalEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 283
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->postal:Lcom/squareup/register/widgets/card/PostalEditText;

    new-instance p2, Lcom/squareup/register/widgets/card/CardEditor$6;

    invoke-direct {p2, p0}, Lcom/squareup/register/widgets/card/CardEditor$6;-><init>(Lcom/squareup/register/widgets/card/CardEditor;)V

    invoke-virtual {p1, p2}, Lcom/squareup/register/widgets/card/PostalEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 290
    sget p1, Lcom/squareup/widgets/pos/R$id;->postal_keyboard_switch:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/glyph/SquareGlyphView;

    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->postalKeyboardSwitch:Lcom/squareup/glyph/SquareGlyphView;

    .line 291
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->postalKeyboardSwitch:Lcom/squareup/glyph/SquareGlyphView;

    new-instance p2, Lcom/squareup/register/widgets/card/CardEditor$7;

    invoke-direct {p2, p0}, Lcom/squareup/register/widgets/card/CardEditor$7;-><init>(Lcom/squareup/register/widgets/card/CardEditor;)V

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 297
    invoke-virtual {p0}, Lcom/squareup/register/widgets/card/CardEditor;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/widgets/pos/R$integer;->card_slide_time:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    int-to-long p1, p1

    iput-wide p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->animDuration:J

    .line 299
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardEditor;->detailsInAnimOverTime()Landroid/view/animation/Animation;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->detailsInAnimOverTime:Landroid/view/animation/Animation;

    .line 300
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardEditor;->detailsOutAnimOverTime()Landroid/view/animation/Animation;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->detailsOutAnimOverTime:Landroid/view/animation/Animation;

    .line 301
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardEditor;->detailsOutAnimImmediate()Landroid/view/animation/Animation;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->detailsOutAnimImmediate:Landroid/view/animation/Animation;

    .line 302
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardEditor;->numberInAnimOverTime()Landroid/view/animation/Animation;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->numberInAnimOverTime:Landroid/view/animation/Animation;

    .line 303
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardEditor;->numberOutAnimOverTime()Landroid/view/animation/Animation;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->numberOutAnimOverTime:Landroid/view/animation/Animation;

    .line 304
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardEditor;->numberInAnimImmediate()Landroid/view/animation/Animation;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->numberInAnimImmediate:Landroid/view/animation/Animation;

    .line 307
    invoke-virtual {p0}, Lcom/squareup/register/widgets/card/CardEditor;->isSaveEnabled()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/card/CardEditor;->setSaveEnabled(Z)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/register/widgets/card/CardEditor;)V
    .locals 0

    .line 62
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardEditor;->flipBackToNumberOverTime()V

    return-void
.end method

.method static synthetic access$100()I
    .locals 1

    .line 62
    sget v0, Lcom/squareup/register/widgets/card/CardEditor;->CVV_MAX_LENGTH:I

    return v0
.end method

.method static synthetic access$1000(Lcom/squareup/register/widgets/card/CardEditor;)Lcom/squareup/widgets/PersistentViewAnimator;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/squareup/register/widgets/card/CardEditor;->animator:Lcom/squareup/widgets/PersistentViewAnimator;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/squareup/register/widgets/card/CardEditor;Lcom/squareup/Card$PanWarning;)V
    .locals 0

    .line 62
    invoke-direct {p0, p1}, Lcom/squareup/register/widgets/card/CardEditor;->checkInput(Lcom/squareup/Card$PanWarning;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/squareup/register/widgets/card/CardEditor;)Lcom/squareup/glyph/SquareGlyphView;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/squareup/register/widgets/card/CardEditor;->postalKeyboardSwitch:Lcom/squareup/glyph/SquareGlyphView;

    return-object p0
.end method

.method static synthetic access$1400(Lcom/squareup/register/widgets/card/CardEditor;)V
    .locals 0

    .line 62
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardEditor;->sendOnFocusChanged()V

    return-void
.end method

.method static synthetic access$1500(Lcom/squareup/register/widgets/card/CardEditor;)V
    .locals 0

    .line 62
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardEditor;->onKeyboardSwitchClicked()V

    return-void
.end method

.method static synthetic access$1700(Lcom/squareup/register/widgets/card/CardEditor;)J
    .locals 2

    .line 62
    iget-wide v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->animDuration:J

    return-wide v0
.end method

.method static synthetic access$200(Lcom/squareup/register/widgets/card/CardEditor;)Lcom/squareup/register/widgets/card/CvvEditor;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvv:Lcom/squareup/register/widgets/card/CvvEditor;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/register/widgets/card/CardEditor;)Lcom/squareup/register/widgets/card/OnCardListener;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/squareup/register/widgets/card/CardEditor;->onCardListener:Lcom/squareup/register/widgets/card/OnCardListener;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/register/widgets/card/CardEditor;)Lcom/squareup/register/widgets/card/PanValidationStrategy;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/squareup/register/widgets/card/CardEditor;->strategy:Lcom/squareup/register/widgets/card/PanValidationStrategy;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/register/widgets/card/CardEditor;)Ljava/lang/String;
    .locals 0

    .line 62
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardEditor;->readCardNumberLastDigits()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/register/widgets/card/CardEditor;)Landroid/widget/TextView;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/squareup/register/widgets/card/CardEditor;->numberLastDigits:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/register/widgets/card/CardEditor;)Landroid/view/ViewGroup;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/squareup/register/widgets/card/CardEditor;->details:Landroid/view/ViewGroup;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/register/widgets/card/CardEditor;)Landroid/view/animation/Animation;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/squareup/register/widgets/card/CardEditor;->detailsInAnimOverTime:Landroid/view/animation/Animation;

    return-object p0
.end method

.method static synthetic access$900(Lcom/squareup/register/widgets/card/CardEditor;)Landroid/view/animation/Animation;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/squareup/register/widgets/card/CardEditor;->numberOutAnimOverTime:Landroid/view/animation/Animation;

    return-object p0
.end method

.method private checkInput(Lcom/squareup/Card$PanWarning;)V
    .locals 1

    .line 579
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->onCardListener:Lcom/squareup/register/widgets/card/OnCardListener;

    if-nez v0, :cond_0

    return-void

    .line 580
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/register/widgets/card/CardEditor;->getCard()Lcom/squareup/Card;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 582
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->onCardListener:Lcom/squareup/register/widgets/card/OnCardListener;

    invoke-interface {p1, v0}, Lcom/squareup/register/widgets/card/OnCardListener;->onCardValid(Lcom/squareup/Card;)V

    goto :goto_0

    .line 584
    :cond_1
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->onCardListener:Lcom/squareup/register/widgets/card/OnCardListener;

    invoke-interface {v0, p1}, Lcom/squareup/register/widgets/card/OnCardListener;->onCardInvalid(Lcom/squareup/Card$PanWarning;)V

    :goto_0
    return-void
.end method

.method private detailsInAnimOverTime()Landroid/view/animation/Animation;
    .locals 2

    .line 640
    new-instance v0, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;-><init>(Lcom/squareup/register/widgets/card/CardEditor;Lcom/squareup/register/widgets/card/CardEditor$1;)V

    .line 641
    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->in()Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;

    move-result-object v0

    .line 642
    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->doNotTranslate()Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;

    move-result-object v0

    .line 643
    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->overTime()Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;

    move-result-object v0

    .line 644
    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->build()Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method private detailsOutAnimImmediate()Landroid/view/animation/Animation;
    .locals 2

    .line 656
    new-instance v0, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;-><init>(Lcom/squareup/register/widgets/card/CardEditor;Lcom/squareup/register/widgets/card/CardEditor$1;)V

    .line 657
    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->out()Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;

    move-result-object v0

    .line 658
    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->doNotTranslate()Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;

    move-result-object v0

    .line 659
    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->immediate()Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;

    move-result-object v0

    .line 660
    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->build()Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method private detailsOutAnimOverTime()Landroid/view/animation/Animation;
    .locals 2

    .line 648
    new-instance v0, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;-><init>(Lcom/squareup/register/widgets/card/CardEditor;Lcom/squareup/register/widgets/card/CardEditor$1;)V

    .line 649
    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->out()Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;

    move-result-object v0

    .line 650
    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->doNotTranslate()Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;

    move-result-object v0

    .line 651
    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->overTime()Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;

    move-result-object v0

    .line 652
    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->build()Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method private extractPostal()Ljava/lang/String;
    .locals 1

    .line 468
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->postal:Lcom/squareup/register/widgets/card/PostalEditText;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/PostalEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private flipBackToNumber(Z)V
    .locals 3

    if-eqz p1, :cond_0

    .line 377
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->numberInAnimOverTime:Landroid/view/animation/Animation;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->numberInAnimImmediate:Landroid/view/animation/Animation;

    :goto_0
    if-eqz p1, :cond_1

    .line 378
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->detailsOutAnimOverTime:Landroid/view/animation/Animation;

    goto :goto_1

    :cond_1
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->detailsOutAnimImmediate:Landroid/view/animation/Animation;

    .line 379
    :goto_1
    iget-object v1, p0, Lcom/squareup/register/widgets/card/CardEditor;->animator:Lcom/squareup/widgets/PersistentViewAnimator;

    iget-object v2, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-virtual {v1, v2, v0, p1}, Lcom/squareup/widgets/PersistentViewAnimator;->animateTo(Landroid/view/View;Landroid/view/animation/Animation;Landroid/view/animation/Animation;)V

    return-void
.end method

.method private flipBackToNumberImmediately()V
    .locals 1

    const/4 v0, 0x0

    .line 373
    invoke-direct {p0, v0}, Lcom/squareup/register/widgets/card/CardEditor;->flipBackToNumber(Z)V

    return-void
.end method

.method private flipBackToNumberOverTime()V
    .locals 1

    const/4 v0, 0x1

    .line 369
    invoke-direct {p0, v0}, Lcom/squareup/register/widgets/card/CardEditor;->flipBackToNumber(Z)V

    return-void
.end method

.method private getDefaultGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 2

    .line 316
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->currency:Lcom/squareup/protos/common/CurrencyCode;

    const/4 v1, 0x0

    invoke-static {v1, v0}, Lcom/squareup/util/ProtoGlyphs;->card(Lcom/squareup/Card$Brand;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    return-object v0
.end method

.method private guessBrand()Lcom/squareup/Card$Brand;
    .locals 1

    .line 547
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardEditor;->readCardNumber()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/Card;->guessBrand(Ljava/lang/CharSequence;)Lcom/squareup/Card$Brand;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic lambda$wjqfvfI3sKnGwVuZuj98IDhOL_k(Lcom/squareup/register/widgets/card/CardEditor;)Lcom/squareup/Card$Brand;
    .locals 0

    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardEditor;->guessBrand()Lcom/squareup/Card$Brand;

    move-result-object p0

    return-object p0
.end method

.method private numberInAnimImmediate()Landroid/view/animation/Animation;
    .locals 2

    .line 680
    new-instance v0, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;-><init>(Lcom/squareup/register/widgets/card/CardEditor;Lcom/squareup/register/widgets/card/CardEditor$1;)V

    .line 681
    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->in()Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;

    move-result-object v0

    .line 682
    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->translate()Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;

    move-result-object v0

    .line 683
    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->immediate()Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;

    move-result-object v0

    .line 684
    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->build()Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method private numberInAnimOverTime()Landroid/view/animation/Animation;
    .locals 2

    .line 664
    new-instance v0, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;-><init>(Lcom/squareup/register/widgets/card/CardEditor;Lcom/squareup/register/widgets/card/CardEditor$1;)V

    .line 665
    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->in()Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;

    move-result-object v0

    .line 666
    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->translate()Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;

    move-result-object v0

    .line 667
    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->overTime()Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;

    move-result-object v0

    .line 668
    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->build()Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method private numberOutAnimOverTime()Landroid/view/animation/Animation;
    .locals 2

    .line 672
    new-instance v0, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;-><init>(Lcom/squareup/register/widgets/card/CardEditor;Lcom/squareup/register/widgets/card/CardEditor$1;)V

    .line 673
    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->out()Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;

    move-result-object v0

    .line 674
    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->translate()Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;

    move-result-object v0

    .line 675
    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->overTime()Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;

    move-result-object v0

    .line 676
    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->build()Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method private onKeyboardSwitchClicked()V
    .locals 2

    .line 551
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->postal:Lcom/squareup/register/widgets/card/PostalEditText;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/PostalEditText;->toggleKeyboard()V

    .line 552
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->postalKeyboardSwitch:Lcom/squareup/glyph/SquareGlyphView;

    iget-object v1, p0, Lcom/squareup/register/widgets/card/CardEditor;->postal:Lcom/squareup/register/widgets/card/PostalEditText;

    invoke-virtual {v1}, Lcom/squareup/register/widgets/card/PostalEditText;->getToggleKeyboardGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    return-void
.end method

.method private onPostalMeasured(I)V
    .locals 7

    .line 350
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->postal:Lcom/squareup/register/widgets/card/PostalEditText;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/PostalEditText;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    .line 351
    iget-object v1, p0, Lcom/squareup/register/widgets/card/CardEditor;->postal:Lcom/squareup/register/widgets/card/PostalEditText;

    invoke-virtual {v1}, Lcom/squareup/register/widgets/card/PostalEditText;->getHint()Ljava/lang/CharSequence;

    move-result-object v1

    .line 352
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v1

    const-string v2, "00000"

    .line 353
    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    int-to-float p1, p1

    cmpg-float v2, p1, v1

    if-ltz v2, :cond_0

    cmpg-float v2, p1, v0

    if-gez v2, :cond_4

    .line 356
    :cond_0
    iget-object v2, p0, Lcom/squareup/register/widgets/card/CardEditor;->numberLastDigits:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 359
    iget-object v2, p0, Lcom/squareup/register/widgets/card/CardEditor;->numberLastDigits:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    .line 360
    iget-object v5, p0, Lcom/squareup/register/widgets/card/CardEditor;->numberLastDigits:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v5

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v6

    invoke-virtual {v5, v2, v3, v6}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v2

    add-float/2addr p1, v2

    float-to-int p1, p1

    int-to-float p1, p1

    cmpg-float v1, p1, v1

    if-gez v1, :cond_1

    const/4 v3, 0x1

    .line 362
    :cond_1
    iget-boolean v1, p0, Lcom/squareup/register/widgets/card/CardEditor;->hideGlyphBasedOnHint:Z

    if-eqz v1, :cond_2

    if-nez v3, :cond_3

    :cond_2
    cmpg-float p1, p1, v0

    if-gez p1, :cond_4

    .line 363
    :cond_3
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->cardGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p1, v4}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    :cond_4
    return-void
.end method

.method private readCardNumber()Ljava/lang/String;
    .locals 1

    .line 472
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/PanEditor;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->removeSpaces(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private readCardNumberLastDigits()Ljava/lang/String;
    .locals 2

    .line 476
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardEditor;->readCardNumber()Ljava/lang/String;

    move-result-object v0

    .line 477
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x4

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private sendOnFocusChanged()V
    .locals 2

    .line 320
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->onFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    if-eqz v0, :cond_0

    .line 321
    invoke-virtual {p0}, Lcom/squareup/register/widgets/card/CardEditor;->fieldsAreFocused()Z

    move-result v1

    invoke-interface {v0, p0, v1}, Landroid/view/View$OnFocusChangeListener;->onFocusChange(Landroid/view/View;Z)V

    :cond_0
    return-void
.end method

.method private setTextIfChanged(Lrx/functions/Func0;Lrx/functions/Action1;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/Func0<",
            "Landroid/text/Editable;",
            ">;",
            "Lrx/functions/Action1<",
            "Ljava/lang/CharSequence;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    if-eqz p3, :cond_0

    goto :goto_0

    :cond_0
    const-string p3, ""

    .line 461
    :goto_0
    invoke-interface {p1}, Lrx/functions/Func0;->call()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/Editable;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 462
    invoke-interface {p2, p3}, Lrx/functions/Action1;->call(Ljava/lang/Object;)V

    .line 463
    invoke-interface {p1}, Lrx/functions/Func0;->call()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/text/Spannable;

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result p2

    invoke-static {p1, p2}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    :cond_1
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 3

    .line 448
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->cardGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardEditor;->getDefaultGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 449
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/PanEditor;->setText(Ljava/lang/CharSequence;)V

    .line 450
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->numberLastDigits:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 451
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, Lcom/squareup/register/widgets/card/PanEditor;->setAlpha(F)V

    .line 452
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->expiration:Lcom/squareup/register/widgets/card/ExpirationEditor;

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/ExpirationEditor;->setText(Ljava/lang/CharSequence;)V

    .line 453
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvv:Lcom/squareup/register/widgets/card/CvvEditor;

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/CvvEditor;->setText(Ljava/lang/CharSequence;)V

    .line 454
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->postal:Lcom/squareup/register/widgets/card/PostalEditText;

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/PostalEditText;->setText(Ljava/lang/CharSequence;)V

    .line 455
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardEditor;->flipBackToNumberImmediately()V

    return-void
.end method

.method public fieldsAreFocused()Z
    .locals 1

    .line 326
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/PanEditor;->isFocused()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->expiration:Lcom/squareup/register/widgets/card/ExpirationEditor;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/ExpirationEditor;->isFocused()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvv:Lcom/squareup/register/widgets/card/CvvEditor;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CvvEditor;->isFocused()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->postal:Lcom/squareup/register/widgets/card/PostalEditText;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/PostalEditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public getCard()Lcom/squareup/Card;
    .locals 8

    .line 395
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/PanEditor;->getCard()Lcom/squareup/Card;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 398
    :cond_0
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->postal:Lcom/squareup/register/widgets/card/PostalEditText;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/PostalEditText;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-ne v0, v1, :cond_1

    const/4 v7, 0x1

    goto :goto_0

    :cond_1
    const/4 v7, 0x0

    .line 399
    :goto_0
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->strategy:Lcom/squareup/register/widgets/card/PanValidationStrategy;

    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    :goto_1
    const-string v0, "strategy must be set!"

    invoke-static {v3, v0}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 400
    iget-object v1, p0, Lcom/squareup/register/widgets/card/CardEditor;->strategy:Lcom/squareup/register/widgets/card/PanValidationStrategy;

    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    iget-object v3, v0, Lcom/squareup/register/widgets/card/PanEditor;->giftCards:Lcom/squareup/giftcard/GiftCards;

    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvv:Lcom/squareup/register/widgets/card/CvvEditor;

    .line 401
    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CvvEditor;->extractCvv()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardEditor;->extractPostal()Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->expiration:Lcom/squareup/register/widgets/card/ExpirationEditor;

    .line 402
    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/ExpirationEditor;->extractCardExpiration()Ljava/lang/String;

    move-result-object v6

    .line 400
    invoke-interface/range {v1 .. v7}, Lcom/squareup/register/widgets/card/PanValidationStrategy;->validateAndContinueBuildingCard(Lcom/squareup/Card;Lcom/squareup/giftcard/GiftCards;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/squareup/Card;

    move-result-object v0

    return-object v0
.end method

.method public getPartialCard()Lcom/squareup/register/widgets/card/PartialCard;
    .locals 7

    .line 407
    new-instance v6, Lcom/squareup/register/widgets/card/PartialCard;

    iget-object v1, p0, Lcom/squareup/register/widgets/card/CardEditor;->cardBrand:Lcom/squareup/Card$Brand;

    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/PanEditor;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvv:Lcom/squareup/register/widgets/card/CvvEditor;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CvvEditor;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->expiration:Lcom/squareup/register/widgets/card/ExpirationEditor;

    .line 408
    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/ExpirationEditor;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->postal:Lcom/squareup/register/widgets/card/PostalEditText;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/PostalEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/register/widgets/card/PartialCard;-><init>(Lcom/squareup/Card$Brand;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v6
.end method

.method public hasCard()Z
    .locals 1

    .line 481
    invoke-virtual {p0}, Lcom/squareup/register/widgets/card/CardEditor;->getCard()Lcom/squareup/Card;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hideGlyphBasedOnHint(Z)V
    .locals 0

    .line 605
    iput-boolean p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->hideGlyphBasedOnHint:Z

    return-void
.end method

.method public ignoreFocusChange()V
    .locals 1

    .line 331
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->expiration:Lcom/squareup/register/widgets/card/ExpirationEditor;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/ExpirationEditor;->setIgnoreFocusChange()V

    .line 332
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvv:Lcom/squareup/register/widgets/card/CvvEditor;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CvvEditor;->setIgnoreFocusChange()V

    return-void
.end method

.method public init(Lcom/squareup/CountryCode;ZLcom/squareup/register/widgets/card/PanValidationStrategy;)V
    .locals 2

    .line 509
    iput-boolean p2, p0, Lcom/squareup/register/widgets/card/CardEditor;->showPostalCode:Z

    .line 511
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->postalPiiScrubber:Lcom/squareup/accessibility/AccessibilityScrubber;

    invoke-static {p1}, Lcom/squareup/address/CountryResources;->postalCardHint(Lcom/squareup/CountryCode;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/accessibility/AccessibilityScrubber;->setContentDescription(I)V

    .line 512
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->postal:Lcom/squareup/register/widgets/card/PostalEditText;

    invoke-static {p1}, Lcom/squareup/address/CountryResources;->postalCardHint(Lcom/squareup/CountryCode;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/PostalEditText;->setHint(I)V

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    .line 515
    iget-boolean p2, p0, Lcom/squareup/register/widgets/card/CardEditor;->compact:Z

    if-nez p2, :cond_0

    .line 516
    iget-object p2, p0, Lcom/squareup/register/widgets/card/CardEditor;->postal:Lcom/squareup/register/widgets/card/PostalEditText;

    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Lcom/squareup/register/widgets/card/PostalEditText;->setVisibility(I)V

    .line 518
    :cond_0
    iget-object p2, p0, Lcom/squareup/register/widgets/card/CardEditor;->postal:Lcom/squareup/register/widgets/card/PostalEditText;

    invoke-virtual {p2, p1}, Lcom/squareup/register/widgets/card/PostalEditText;->showHintForCountry(Lcom/squareup/CountryCode;)V

    .line 519
    iget-object p2, p0, Lcom/squareup/register/widgets/card/CardEditor;->postal:Lcom/squareup/register/widgets/card/PostalEditText;

    invoke-virtual {p2, p1}, Lcom/squareup/register/widgets/card/PostalEditText;->showDefaultKeyboardForCountry(Lcom/squareup/CountryCode;)V

    .line 520
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->postalKeyboardSwitch:Lcom/squareup/glyph/SquareGlyphView;

    const/4 p2, 0x4

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 521
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->postalKeyboardSwitch:Lcom/squareup/glyph/SquareGlyphView;

    iget-object p2, p0, Lcom/squareup/register/widgets/card/CardEditor;->postal:Lcom/squareup/register/widgets/card/PostalEditText;

    invoke-virtual {p2}, Lcom/squareup/register/widgets/card/PostalEditText;->getToggleKeyboardGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    goto :goto_0

    .line 529
    :cond_1
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvvWatcher:Lcom/squareup/text/AutoAdvanceTextWatcher;

    invoke-virtual {p1, v0}, Lcom/squareup/text/AutoAdvanceTextWatcher;->setNextView(Landroid/view/View;)V

    .line 530
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->postal:Lcom/squareup/register/widgets/card/PostalEditText;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Lcom/squareup/register/widgets/card/PostalEditText;->setVisibility(I)V

    .line 531
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->postalKeyboardSwitch:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 534
    :goto_0
    invoke-virtual {p0, p3}, Lcom/squareup/register/widgets/card/CardEditor;->setStrategy(Lcom/squareup/register/widgets/card/PanValidationStrategy;)V

    .line 535
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->strategy:Lcom/squareup/register/widgets/card/PanValidationStrategy;

    iget-object p2, p0, Lcom/squareup/register/widgets/card/CardEditor;->currency:Lcom/squareup/protos/common/CurrencyCode;

    invoke-interface {p1, v0, p2}, Lcom/squareup/register/widgets/card/PanValidationStrategy;->getGlyph(Lcom/squareup/Card$Brand;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/card/CardEditor;->setCardGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    return-void
.end method

.method public synthetic lambda$new$0$CardEditor(Landroid/view/View;Z)V
    .locals 0

    .line 212
    iget-boolean p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->compact:Z

    if-eqz p1, :cond_1

    if-eqz p2, :cond_0

    .line 214
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/card/PanEditor;->getHint()Ljava/lang/CharSequence;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->numberHintBeforeFocused:Ljava/lang/CharSequence;

    .line 215
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    sget p2, Lcom/squareup/widgets/pos/R$string;->ce_card_number_hint_focused:I

    invoke-virtual {p1, p2}, Lcom/squareup/register/widgets/card/PanEditor;->setHint(I)V

    goto :goto_0

    .line 219
    :cond_0
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    iget-object p2, p0, Lcom/squareup/register/widgets/card/CardEditor;->numberHintBeforeFocused:Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lcom/squareup/register/widgets/card/PanEditor;->setHint(Ljava/lang/CharSequence;)V

    .line 222
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardEditor;->sendOnFocusChanged()V

    return-void
.end method

.method public synthetic lambda$new$1$CardEditor(Landroid/view/View;)V
    .locals 0

    .line 248
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardEditor;->flipBackToNumberOverTime()V

    return-void
.end method

.method public synthetic lambda$new$10$CardEditor()Lrx/Subscription;
    .locals 2

    .line 262
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvv:Lcom/squareup/register/widgets/card/CvvEditor;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CvvEditor;->onCvvValid()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/register/widgets/card/-$$Lambda$CardEditor$TvVHHNtR_QcOlsdESka4Y54Ldlc;

    invoke-direct {v1, p0}, Lcom/squareup/register/widgets/card/-$$Lambda$CardEditor$TvVHHNtR_QcOlsdESka4Y54Ldlc;-><init>(Lcom/squareup/register/widgets/card/CardEditor;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$new$11$CardEditor(Landroid/view/View;)V
    .locals 2

    .line 269
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->postal:Lcom/squareup/register/widgets/card/PostalEditText;

    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvv:Lcom/squareup/register/widgets/card/CvvEditor;

    iget-boolean v1, p0, Lcom/squareup/register/widgets/card/CardEditor;->compact:Z

    invoke-static {p1, v0, v1}, Lcom/squareup/text/AutoAdvanceTextWatcher;->focusNextView(Landroid/view/View;Landroid/view/View;Z)V

    return-void
.end method

.method public synthetic lambda$new$3$CardEditor()Lrx/Subscription;
    .locals 2

    .line 249
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->expiration:Lcom/squareup/register/widgets/card/ExpirationEditor;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/ExpirationEditor;->onFocusChange()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/register/widgets/card/-$$Lambda$CardEditor$DZh5Lx_FqeDr_kPsLHs4S6626sk;

    invoke-direct {v1, p0}, Lcom/squareup/register/widgets/card/-$$Lambda$CardEditor$DZh5Lx_FqeDr_kPsLHs4S6626sk;-><init>(Lcom/squareup/register/widgets/card/CardEditor;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$new$5$CardEditor()Lrx/Subscription;
    .locals 2

    .line 252
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->expiration:Lcom/squareup/register/widgets/card/ExpirationEditor;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/ExpirationEditor;->onExpirationValid()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/register/widgets/card/-$$Lambda$CardEditor$S9yJLqD_6CfsXyZ51pxWIdoDJEs;

    invoke-direct {v1, p0}, Lcom/squareup/register/widgets/card/-$$Lambda$CardEditor$S9yJLqD_6CfsXyZ51pxWIdoDJEs;-><init>(Lcom/squareup/register/widgets/card/CardEditor;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$new$6$CardEditor(Landroid/view/View;)V
    .locals 2

    .line 258
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvv:Lcom/squareup/register/widgets/card/CvvEditor;

    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->expiration:Lcom/squareup/register/widgets/card/ExpirationEditor;

    iget-boolean v1, p0, Lcom/squareup/register/widgets/card/CardEditor;->compact:Z

    invoke-static {p1, v0, v1}, Lcom/squareup/text/AutoAdvanceTextWatcher;->focusNextView(Landroid/view/View;Landroid/view/View;Z)V

    return-void
.end method

.method public synthetic lambda$new$8$CardEditor()Lrx/Subscription;
    .locals 2

    .line 259
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvv:Lcom/squareup/register/widgets/card/CvvEditor;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CvvEditor;->onFocusChange()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/register/widgets/card/-$$Lambda$CardEditor$3MNJpX5eNlOCJZSAwFPliFDR594;

    invoke-direct {v1, p0}, Lcom/squareup/register/widgets/card/-$$Lambda$CardEditor$3MNJpX5eNlOCJZSAwFPliFDR594;-><init>(Lcom/squareup/register/widgets/card/CardEditor;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$2$CardEditor(Lkotlin/Unit;)V
    .locals 0

    .line 250
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardEditor;->sendOnFocusChanged()V

    return-void
.end method

.method public synthetic lambda$null$4$CardEditor(Lkotlin/Unit;)V
    .locals 2

    .line 253
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->expiration:Lcom/squareup/register/widgets/card/ExpirationEditor;

    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvv:Lcom/squareup/register/widgets/card/CvvEditor;

    iget-boolean v1, p0, Lcom/squareup/register/widgets/card/CardEditor;->compact:Z

    invoke-static {p1, v0, v1}, Lcom/squareup/text/AutoAdvanceTextWatcher;->focusNextView(Landroid/view/View;Landroid/view/View;Z)V

    return-void
.end method

.method public synthetic lambda$null$7$CardEditor(Lkotlin/Unit;)V
    .locals 0

    .line 260
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardEditor;->sendOnFocusChanged()V

    return-void
.end method

.method public synthetic lambda$null$9$CardEditor(Lkotlin/Unit;)V
    .locals 2

    .line 263
    iget-boolean p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->showPostalCode:Z

    if-eqz p1, :cond_0

    .line 264
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvv:Lcom/squareup/register/widgets/card/CvvEditor;

    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->postal:Lcom/squareup/register/widgets/card/PostalEditText;

    iget-boolean v1, p0, Lcom/squareup/register/widgets/card/CardEditor;->compact:Z

    invoke-static {p1, v0, v1}, Lcom/squareup/text/AutoAdvanceTextWatcher;->focusNextView(Landroid/view/View;Landroid/view/View;Z)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$12$CardEditor(Landroid/view/View;II)V
    .locals 0

    .line 344
    invoke-direct {p0, p2}, Lcom/squareup/register/widgets/card/CardEditor;->onPostalMeasured(I)V

    return-void
.end method

.method obfuscatedPan()Ljava/lang/String;
    .locals 5

    .line 620
    invoke-virtual {p0}, Lcom/squareup/register/widgets/card/CardEditor;->getCard()Lcom/squareup/Card;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    return-object v0

    .line 623
    :cond_0
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->cardNumberScrubber:Lcom/squareup/text/CardNumberScrubber;

    invoke-virtual {p0}, Lcom/squareup/register/widgets/card/CardEditor;->getCard()Lcom/squareup/Card;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/text/CardNumberScrubber;->setBrand(Lcom/squareup/Card$Brand;)V

    .line 624
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->cardNumberScrubber:Lcom/squareup/text/CardNumberScrubber;

    invoke-virtual {p0}, Lcom/squareup/register/widgets/card/CardEditor;->getCard()Lcom/squareup/Card;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/Card;->getPan()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/text/CardNumberScrubber;->scrub(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 625
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    .line 626
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 627
    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x20

    if-ne v3, v4, :cond_1

    .line 629
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 630
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x4

    if-lt v2, v4, :cond_2

    .line 631
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_2
    const/16 v3, 0x2022

    .line 633
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 636
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .line 336
    invoke-super {p0}, Lcom/squareup/noho/NohoLinearLayout;->onAttachedToWindow()V

    .line 341
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/PanEditor;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardEditor;->flipBackToNumberOverTime()V

    .line 344
    :cond_0
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->postal:Lcom/squareup/register/widgets/card/PostalEditText;

    new-instance v1, Lcom/squareup/register/widgets/card/-$$Lambda$CardEditor$w0d3iAdZ0E6W2gQAjeEOVUAJivE;

    invoke-direct {v1, p0}, Lcom/squareup/register/widgets/card/-$$Lambda$CardEditor$w0d3iAdZ0E6W2gQAjeEOVUAJivE;-><init>(Lcom/squareup/register/widgets/card/CardEditor;)V

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->waitForHeightMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    return-void
.end method

.method public onBrandChanged(Lcom/squareup/Card$Brand;)V
    .locals 4

    .line 486
    iget-boolean v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->syncCardGlyphToBrand:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->cardBrand:Lcom/squareup/Card$Brand;

    if-eq p1, v0, :cond_0

    .line 487
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->cardGlyph:Lcom/squareup/glyph/SquareGlyphView;

    iget-object v1, p0, Lcom/squareup/register/widgets/card/CardEditor;->currency:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p1, v1}, Lcom/squareup/util/ProtoGlyphs;->card(Lcom/squareup/Card$Brand;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->flipTo(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 489
    :cond_0
    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->cardBrand:Lcom/squareup/Card$Brand;

    .line 490
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvv:Lcom/squareup/register/widgets/card/CvvEditor;

    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->cardBrand:Lcom/squareup/Card$Brand;

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/card/CvvEditor;->setBrand(Lcom/squareup/Card$Brand;)V

    .line 491
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->cardBrand:Lcom/squareup/Card$Brand;

    invoke-virtual {p1}, Lcom/squareup/Card$Brand;->cvvLength()I

    move-result p1

    .line 492
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvvWatcher:Lcom/squareup/text/AutoAdvanceTextWatcher;

    invoke-virtual {v0, p1}, Lcom/squareup/text/AutoAdvanceTextWatcher;->setLength(I)V

    const/4 v0, 0x0

    const/4 v1, 0x4

    if-lt p1, v1, :cond_1

    .line 494
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvv:Lcom/squareup/register/widgets/card/CvvEditor;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/card/CvvEditor;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    const-string v3, "0000"

    invoke-virtual {v2, v3, v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;II)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/card/CvvEditor;->setMinWidth(I)V

    goto :goto_0

    .line 496
    :cond_1
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvv:Lcom/squareup/register/widgets/card/CvvEditor;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/card/CvvEditor;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    const/4 v2, 0x3

    const-string v3, "000"

    invoke-virtual {v1, v3, v0, v2}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;II)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/card/CvvEditor;->setMinWidth(I)V

    :goto_0
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .line 311
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->shadowPainter:Lcom/squareup/noho/EdgePainter;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/EdgePainter;->drawEdges(Landroid/graphics/Canvas;)V

    .line 312
    invoke-super {p0, p1}, Lcom/squareup/noho/NohoLinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public setCardGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V
    .locals 1

    const/4 v0, 0x0

    .line 443
    iput-boolean v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->syncCardGlyphToBrand:Z

    .line 444
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->cardGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    return-void
.end method

.method public setCardInputHint(I)V
    .locals 1

    .line 564
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/card/PanEditor;->setHint(I)V

    return-void
.end method

.method public setCardInputHint(Ljava/lang/CharSequence;)V
    .locals 1

    .line 560
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/card/PanEditor;->setHint(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setCompact(Z)V
    .locals 0

    .line 568
    iput-boolean p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->compact:Z

    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .line 589
    invoke-super {p0, p1}, Lcom/squareup/noho/NohoLinearLayout;->setEnabled(Z)V

    if-eqz p1, :cond_0

    .line 591
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/card/PanEditor;->setEnabled(Z)V

    .line 592
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/card/PanEditor;->setFocusableInTouchMode(Z)V

    goto :goto_0

    .line 594
    :cond_0
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardEditor;->flipBackToNumberOverTime()V

    .line 595
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    const-string v0, ""

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/card/PanEditor;->setText(Ljava/lang/CharSequence;)V

    .line 596
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/card/PanEditor;->clearFocus()V

    .line 597
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/card/PanEditor;->setEnabled(Z)V

    .line 599
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/card/PanEditor;->setFocusable(Z)V

    .line 600
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    :goto_0
    return-void
.end method

.method public setOnCardListener(Lcom/squareup/register/widgets/card/OnCardListener;)V
    .locals 0

    .line 387
    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->onCardListener:Lcom/squareup/register/widgets/card/OnCardListener;

    return-void
.end method

.method public setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V
    .locals 0

    .line 556
    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->onFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    return-void
.end method

.method public setPartialCard(Lcom/squareup/register/widgets/card/PartialCard;)V
    .locals 6

    if-nez p1, :cond_0

    .line 412
    new-instance p1, Lcom/squareup/register/widgets/card/PartialCard;

    sget-object v1, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/register/widgets/card/PartialCard;-><init>(Lcom/squareup/Card$Brand;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    :cond_0
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->onCardListener:Lcom/squareup/register/widgets/card/OnCardListener;

    const/4 v1, 0x0

    .line 416
    iput-object v1, p0, Lcom/squareup/register/widgets/card/CardEditor;->onCardListener:Lcom/squareup/register/widgets/card/OnCardListener;

    .line 419
    iget-object v1, p1, Lcom/squareup/register/widgets/card/PartialCard;->brand:Lcom/squareup/Card$Brand;

    iput-object v1, p0, Lcom/squareup/register/widgets/card/CardEditor;->cardBrand:Lcom/squareup/Card$Brand;

    .line 420
    iget-boolean v1, p0, Lcom/squareup/register/widgets/card/CardEditor;->syncCardGlyphToBrand:Z

    if-eqz v1, :cond_1

    .line 421
    iget-object v1, p0, Lcom/squareup/register/widgets/card/CardEditor;->cardGlyph:Lcom/squareup/glyph/SquareGlyphView;

    iget-object v2, p0, Lcom/squareup/register/widgets/card/CardEditor;->cardBrand:Lcom/squareup/Card$Brand;

    iget-object v3, p0, Lcom/squareup/register/widgets/card/CardEditor;->currency:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v2, v3}, Lcom/squareup/util/ProtoGlyphs;->card(Lcom/squareup/Card$Brand;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 423
    :cond_1
    iget-object v1, p1, Lcom/squareup/register/widgets/card/PartialCard;->brand:Lcom/squareup/Card$Brand;

    invoke-virtual {p0, v1}, Lcom/squareup/register/widgets/card/CardEditor;->onBrandChanged(Lcom/squareup/Card$Brand;)V

    .line 426
    iget-object v1, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/register/widgets/card/-$$Lambda$ok-TYdVPuoRSEcTfCZ9EOXYoihg;

    invoke-direct {v2, v1}, Lcom/squareup/register/widgets/card/-$$Lambda$ok-TYdVPuoRSEcTfCZ9EOXYoihg;-><init>(Lcom/squareup/register/widgets/card/PanEditor;)V

    iget-object v1, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/register/widgets/card/-$$Lambda$Ony_YG0oKMh3C5z7S0_uVCnPL0M;

    invoke-direct {v3, v1}, Lcom/squareup/register/widgets/card/-$$Lambda$Ony_YG0oKMh3C5z7S0_uVCnPL0M;-><init>(Lcom/squareup/register/widgets/card/PanEditor;)V

    iget-object v1, p1, Lcom/squareup/register/widgets/card/PartialCard;->pan:Ljava/lang/String;

    invoke-direct {p0, v2, v3, v1}, Lcom/squareup/register/widgets/card/CardEditor;->setTextIfChanged(Lrx/functions/Func0;Lrx/functions/Action1;Ljava/lang/String;)V

    .line 427
    iget-object v1, p0, Lcom/squareup/register/widgets/card/CardEditor;->expiration:Lcom/squareup/register/widgets/card/ExpirationEditor;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/register/widgets/card/-$$Lambda$FTtNpUXiJxXAa66PkNcDXh8fowg;

    invoke-direct {v2, v1}, Lcom/squareup/register/widgets/card/-$$Lambda$FTtNpUXiJxXAa66PkNcDXh8fowg;-><init>(Lcom/squareup/register/widgets/card/ExpirationEditor;)V

    iget-object v1, p0, Lcom/squareup/register/widgets/card/CardEditor;->expiration:Lcom/squareup/register/widgets/card/ExpirationEditor;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/register/widgets/card/-$$Lambda$C7KaPfMziP-F4OeePisNaDLoxq0;

    invoke-direct {v3, v1}, Lcom/squareup/register/widgets/card/-$$Lambda$C7KaPfMziP-F4OeePisNaDLoxq0;-><init>(Lcom/squareup/register/widgets/card/ExpirationEditor;)V

    iget-object v1, p1, Lcom/squareup/register/widgets/card/PartialCard;->expiration:Ljava/lang/String;

    invoke-direct {p0, v2, v3, v1}, Lcom/squareup/register/widgets/card/CardEditor;->setTextIfChanged(Lrx/functions/Func0;Lrx/functions/Action1;Ljava/lang/String;)V

    .line 428
    iget-object v1, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvv:Lcom/squareup/register/widgets/card/CvvEditor;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/register/widgets/card/-$$Lambda$_hBiuEgBHqGnffQbCZZ5CQK835k;

    invoke-direct {v2, v1}, Lcom/squareup/register/widgets/card/-$$Lambda$_hBiuEgBHqGnffQbCZZ5CQK835k;-><init>(Lcom/squareup/register/widgets/card/CvvEditor;)V

    iget-object v1, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvv:Lcom/squareup/register/widgets/card/CvvEditor;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/register/widgets/card/-$$Lambda$IzfQmDilk7ylCqppCIZXQ8f0y9g;

    invoke-direct {v3, v1}, Lcom/squareup/register/widgets/card/-$$Lambda$IzfQmDilk7ylCqppCIZXQ8f0y9g;-><init>(Lcom/squareup/register/widgets/card/CvvEditor;)V

    iget-object v1, p1, Lcom/squareup/register/widgets/card/PartialCard;->verification:Ljava/lang/String;

    invoke-direct {p0, v2, v3, v1}, Lcom/squareup/register/widgets/card/CardEditor;->setTextIfChanged(Lrx/functions/Func0;Lrx/functions/Action1;Ljava/lang/String;)V

    .line 429
    iget-object v1, p0, Lcom/squareup/register/widgets/card/CardEditor;->postal:Lcom/squareup/register/widgets/card/PostalEditText;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/register/widgets/card/-$$Lambda$Aw1uIpGeEzp0AcCcGc--6E3pDNY;

    invoke-direct {v2, v1}, Lcom/squareup/register/widgets/card/-$$Lambda$Aw1uIpGeEzp0AcCcGc--6E3pDNY;-><init>(Lcom/squareup/register/widgets/card/PostalEditText;)V

    iget-object v1, p0, Lcom/squareup/register/widgets/card/CardEditor;->postal:Lcom/squareup/register/widgets/card/PostalEditText;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/register/widgets/card/-$$Lambda$koWNQ4FlMLSfh4tN0MV7MBeHxR4;

    invoke-direct {v3, v1}, Lcom/squareup/register/widgets/card/-$$Lambda$koWNQ4FlMLSfh4tN0MV7MBeHxR4;-><init>(Lcom/squareup/register/widgets/card/PostalEditText;)V

    iget-object p1, p1, Lcom/squareup/register/widgets/card/PartialCard;->postalCode:Ljava/lang/String;

    invoke-direct {p0, v2, v3, p1}, Lcom/squareup/register/widgets/card/CardEditor;->setTextIfChanged(Lrx/functions/Func0;Lrx/functions/Action1;Ljava/lang/String;)V

    .line 432
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/card/PanEditor;->getCard()Lcom/squareup/Card;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 433
    invoke-virtual {p1}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object p1

    sget-object v1, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    if-ne p1, v1, :cond_3

    .line 434
    :cond_2
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardEditor;->flipBackToNumberOverTime()V

    .line 438
    :cond_3
    iput-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->onCardListener:Lcom/squareup/register/widgets/card/OnCardListener;

    return-void
.end method

.method public setSaveEnabled(Z)V
    .locals 1

    .line 610
    invoke-super {p0, p1}, Lcom/squareup/noho/NohoLinearLayout;->setSaveEnabled(Z)V

    .line 612
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/card/PanEditor;->setSaveEnabled(Z)V

    .line 613
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvv:Lcom/squareup/register/widgets/card/CvvEditor;

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/card/CvvEditor;->setSaveEnabled(Z)V

    .line 614
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->expiration:Lcom/squareup/register/widgets/card/ExpirationEditor;

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/card/ExpirationEditor;->setSaveEnabled(Z)V

    .line 615
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->postal:Lcom/squareup/register/widgets/card/PostalEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/card/PostalEditText;->setSaveEnabled(Z)V

    return-void
.end method

.method public setStrategy(Lcom/squareup/register/widgets/card/PanValidationStrategy;)V
    .locals 2

    .line 539
    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->strategy:Lcom/squareup/register/widgets/card/PanValidationStrategy;

    .line 540
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/card/PanEditor;->setStrategy(Lcom/squareup/register/widgets/card/PanValidationStrategy;)V

    .line 541
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->expiration:Lcom/squareup/register/widgets/card/ExpirationEditor;

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/card/ExpirationEditor;->setStrategy(Lcom/squareup/register/widgets/card/PanValidationStrategy;)V

    .line 542
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->cvv:Lcom/squareup/register/widgets/card/CvvEditor;

    new-instance v1, Lcom/squareup/register/widgets/card/-$$Lambda$CardEditor$wjqfvfI3sKnGwVuZuj98IDhOL_k;

    invoke-direct {v1, p0}, Lcom/squareup/register/widgets/card/-$$Lambda$CardEditor$wjqfvfI3sKnGwVuZuj98IDhOL_k;-><init>(Lcom/squareup/register/widgets/card/CardEditor;)V

    invoke-virtual {v0, p1, v1}, Lcom/squareup/register/widgets/card/CvvEditor;->setStrategyAndBrandGuesser(Lcom/squareup/register/widgets/card/PanValidationStrategy;Lrx/functions/Func0;)V

    .line 543
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->cardNumberScrubber:Lcom/squareup/text/CardNumberScrubber;

    invoke-virtual {v0, p1}, Lcom/squareup/text/CardNumberScrubber;->setStrategy(Lcom/squareup/register/widgets/card/PanValidationStrategy;)V

    return-void
.end method

.method public showSoftKeyboard()V
    .locals 1

    .line 383
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor;->number:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-static {v0}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;)V

    return-void
.end method
