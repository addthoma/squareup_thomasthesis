.class public Lcom/squareup/register/widgets/card/PermissiveCardPanValidationStrategy;
.super Lcom/squareup/register/widgets/card/CardPanValidationStrategy;
.source "PermissiveCardPanValidationStrategy.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;-><init>()V

    return-void
.end method


# virtual methods
.method public panCharacterValid(C)Z
    .locals 1

    .line 15
    invoke-static {p1}, Ljava/lang/Character;->isDigit(C)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, Ljava/lang/Character;->isAlphabetic(I)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method
