.class public final Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs$Companion$CREATOR$1;
.super Ljava/lang/Object;
.source "LegacyNohoDatePickerRunner.kt"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLegacyNohoDatePickerRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LegacyNohoDatePickerRunner.kt\ncom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs$Companion$CREATOR$1\n*L\n1#1,111:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000%\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u001d\u0010\u0006\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH\u0016\u00a2\u0006\u0002\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "com/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs$Companion$CREATOR$1",
        "Landroid/os/Parcelable$Creator;",
        "Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;",
        "createFromParcel",
        "source",
        "Landroid/os/Parcel;",
        "newArray",
        "",
        "size",
        "",
        "(I)[Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;",
        "widgets-pos_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;
    .locals 8

    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    new-instance v0, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;

    .line 102
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-static {p1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunnerKt;->access$readLocalDate(Landroid/os/Parcel;)Lorg/threeten/bp/LocalDate;

    move-result-object v4

    const-string v1, "readLocalDate()"

    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunnerKt;->access$readLocalDate(Landroid/os/Parcel;)Lorg/threeten/bp/LocalDate;

    move-result-object v5

    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunnerKt;->access$readLocalDate(Landroid/os/Parcel;)Lorg/threeten/bp/LocalDate;

    move-result-object v6

    invoke-static {v6, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/util/Parcels;->readBooleanFromInt(Landroid/os/Parcel;)Z

    move-result v7

    move-object v1, v0

    .line 101
    invoke-direct/range {v1 .. v7}, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;-><init>(JLorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Z)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 99
    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs$Companion$CREATOR$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;
    .locals 0

    .line 106
    new-array p1, p1, [Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 99
    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs$Companion$CREATOR$1;->newArray(I)[Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;

    move-result-object p1

    return-object p1
.end method
