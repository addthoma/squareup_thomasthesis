.class public final Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen_DialogBuilder_MembersInjector;
.super Ljava/lang/Object;
.source "LegacyNohoDatePickerDialogScreen_DialogBuilder_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;",
        ">;"
    }
.end annotation


# instance fields
.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;",
            ">;)V"
        }
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen_DialogBuilder_MembersInjector;->runnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;",
            ">;"
        }
    .end annotation

    .line 22
    new-instance v0, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen_DialogBuilder_MembersInjector;

    invoke-direct {v0, p0}, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen_DialogBuilder_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectRunner(Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;->runner:Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;)V
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen_DialogBuilder_MembersInjector;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;

    invoke-static {p1, v0}, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen_DialogBuilder_MembersInjector;->injectRunner(Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 8
    check-cast p1, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;

    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen_DialogBuilder_MembersInjector;->injectMembers(Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;)V

    return-void
.end method
