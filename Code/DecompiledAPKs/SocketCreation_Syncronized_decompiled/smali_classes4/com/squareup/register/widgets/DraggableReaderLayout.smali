.class public Lcom/squareup/register/widgets/DraggableReaderLayout;
.super Landroid/widget/FrameLayout;
.source "DraggableReaderLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/widgets/DraggableReaderLayout$DragCallback;
    }
.end annotation


# instance fields
.field private continueSettling:Z

.field private final dragHelper:Landroidx/customview/widget/ViewDragHelper;

.field private final isPortrait:Z

.field private final jackSize:I

.field private reader:Landroid/view/View;

.field private readerOutCount:I

.field private settlingReaderOut:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    invoke-static {p1}, Lcom/squareup/util/Views;->isPortrait(Landroid/content/Context;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/register/widgets/DraggableReaderLayout;->isPortrait:Z

    .line 36
    new-instance p1, Lcom/squareup/register/widgets/DraggableReaderLayout$DragCallback;

    invoke-direct {p1, p0}, Lcom/squareup/register/widgets/DraggableReaderLayout$DragCallback;-><init>(Lcom/squareup/register/widgets/DraggableReaderLayout;)V

    invoke-static {p0, p1}, Landroidx/customview/widget/ViewDragHelper;->create(Landroid/view/ViewGroup;Landroidx/customview/widget/ViewDragHelper$Callback;)Landroidx/customview/widget/ViewDragHelper;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/register/widgets/DraggableReaderLayout;->dragHelper:Landroidx/customview/widget/ViewDragHelper;

    .line 37
    invoke-virtual {p0}, Lcom/squareup/register/widgets/DraggableReaderLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/loggedout/R$dimen;->reader_jack_size:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/register/widgets/DraggableReaderLayout;->jackSize:I

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/register/widgets/DraggableReaderLayout;)Z
    .locals 0

    .line 22
    iget-boolean p0, p0, Lcom/squareup/register/widgets/DraggableReaderLayout;->isPortrait:Z

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/register/widgets/DraggableReaderLayout;)I
    .locals 0

    .line 22
    iget p0, p0, Lcom/squareup/register/widgets/DraggableReaderLayout;->jackSize:I

    return p0
.end method

.method static synthetic access$200(Lcom/squareup/register/widgets/DraggableReaderLayout;)Z
    .locals 0

    .line 22
    iget-boolean p0, p0, Lcom/squareup/register/widgets/DraggableReaderLayout;->continueSettling:Z

    return p0
.end method

.method static synthetic access$202(Lcom/squareup/register/widgets/DraggableReaderLayout;Z)Z
    .locals 0

    .line 22
    iput-boolean p1, p0, Lcom/squareup/register/widgets/DraggableReaderLayout;->continueSettling:Z

    return p1
.end method

.method static synthetic access$302(Lcom/squareup/register/widgets/DraggableReaderLayout;Z)Z
    .locals 0

    .line 22
    iput-boolean p1, p0, Lcom/squareup/register/widgets/DraggableReaderLayout;->settlingReaderOut:Z

    return p1
.end method

.method static synthetic access$400(Lcom/squareup/register/widgets/DraggableReaderLayout;)Landroid/view/View;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/squareup/register/widgets/DraggableReaderLayout;->reader:Landroid/view/View;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/register/widgets/DraggableReaderLayout;)Landroidx/customview/widget/ViewDragHelper;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/squareup/register/widgets/DraggableReaderLayout;->dragHelper:Landroidx/customview/widget/ViewDragHelper;

    return-object p0
.end method


# virtual methods
.method public computeScroll()V
    .locals 3

    .line 46
    iget-boolean v0, p0, Lcom/squareup/register/widgets/DraggableReaderLayout;->continueSettling:Z

    if-eqz v0, :cond_1

    .line 47
    iget-object v0, p0, Lcom/squareup/register/widgets/DraggableReaderLayout;->dragHelper:Landroidx/customview/widget/ViewDragHelper;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/customview/widget/ViewDragHelper;->continueSettling(Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/register/widgets/DraggableReaderLayout;->continueSettling:Z

    .line 48
    iget-boolean v0, p0, Lcom/squareup/register/widgets/DraggableReaderLayout;->continueSettling:Z

    if-eqz v0, :cond_0

    .line 49
    invoke-static {p0}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    goto :goto_0

    .line 50
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/register/widgets/DraggableReaderLayout;->settlingReaderOut:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 51
    iput-boolean v0, p0, Lcom/squareup/register/widgets/DraggableReaderLayout;->settlingReaderOut:Z

    .line 53
    invoke-virtual {p0}, Lcom/squareup/register/widgets/DraggableReaderLayout;->requestLayout()V

    .line 54
    iget-object v0, p0, Lcom/squareup/register/widgets/DraggableReaderLayout;->reader:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 74
    new-instance v0, Lcom/squareup/register/widgets/-$$Lambda$DraggableReaderLayout$edUbvRGVZoE4-UsIVnOUf9gWrFk;

    invoke-direct {v0, p0}, Lcom/squareup/register/widgets/-$$Lambda$DraggableReaderLayout$edUbvRGVZoE4-UsIVnOUf9gWrFk;-><init>(Lcom/squareup/register/widgets/DraggableReaderLayout;)V

    const-wide/16 v1, 0x1f4

    invoke-virtual {p0, v0, v1, v2}, Lcom/squareup/register/widgets/DraggableReaderLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_1
    :goto_0
    return-void
.end method

.method public synthetic lambda$computeScroll$0$DraggableReaderLayout()V
    .locals 2

    .line 75
    iget-object v0, p0, Lcom/squareup/register/widgets/DraggableReaderLayout;->reader:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 76
    invoke-virtual {p0}, Lcom/squareup/register/widgets/DraggableReaderLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/loggedout/R$anim;->reader_insert:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 77
    iget-object v1, p0, Lcom/squareup/register/widgets/DraggableReaderLayout;->reader:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 41
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 42
    sget v0, Lcom/squareup/loggedout/R$id;->reader:I

    invoke-virtual {p0, v0}, Lcom/squareup/register/widgets/DraggableReaderLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/register/widgets/DraggableReaderLayout;->reader:Landroid/view/View;

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .line 84
    invoke-static {p1}, Landroidx/core/view/MotionEventCompat;->getActionMasked(Landroid/view/MotionEvent;)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/squareup/register/widgets/DraggableReaderLayout;->dragHelper:Landroidx/customview/widget/ViewDragHelper;

    invoke-virtual {v0, p1}, Landroidx/customview/widget/ViewDragHelper;->shouldInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1

    .line 86
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/squareup/register/widgets/DraggableReaderLayout;->dragHelper:Landroidx/customview/widget/ViewDragHelper;

    invoke-virtual {p1}, Landroidx/customview/widget/ViewDragHelper;->cancel()V

    const/4 p1, 0x0

    return p1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/squareup/register/widgets/DraggableReaderLayout;->dragHelper:Landroidx/customview/widget/ViewDragHelper;

    invoke-virtual {v0, p1}, Landroidx/customview/widget/ViewDragHelper;->processTouchEvent(Landroid/view/MotionEvent;)V

    const/4 p1, 0x1

    return p1
.end method
