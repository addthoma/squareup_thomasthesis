.class Lcom/squareup/register/widgets/EditTextDialogFactory$1;
.super Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.source "EditTextDialogFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/register/widgets/EditTextDialogFactory;->createEditTextDialog(Landroid/content/Context;Lcom/squareup/text/Scrubber;Lcom/squareup/register/widgets/EditTextDialogFactory$EditTextDialogListener;)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/register/widgets/EditTextDialogFactory;

.field final synthetic val$dialog:Landroid/app/AlertDialog;

.field final synthetic val$editor:Lcom/squareup/widgets/SelectableEditText;

.field final synthetic val$listener:Lcom/squareup/register/widgets/EditTextDialogFactory$EditTextDialogListener;


# direct methods
.method constructor <init>(Lcom/squareup/register/widgets/EditTextDialogFactory;Lcom/squareup/register/widgets/EditTextDialogFactory$EditTextDialogListener;Lcom/squareup/widgets/SelectableEditText;Landroid/app/AlertDialog;)V
    .locals 0

    .line 81
    iput-object p1, p0, Lcom/squareup/register/widgets/EditTextDialogFactory$1;->this$0:Lcom/squareup/register/widgets/EditTextDialogFactory;

    iput-object p2, p0, Lcom/squareup/register/widgets/EditTextDialogFactory$1;->val$listener:Lcom/squareup/register/widgets/EditTextDialogFactory$EditTextDialogListener;

    iput-object p3, p0, Lcom/squareup/register/widgets/EditTextDialogFactory$1;->val$editor:Lcom/squareup/widgets/SelectableEditText;

    iput-object p4, p0, Lcom/squareup/register/widgets/EditTextDialogFactory$1;->val$dialog:Landroid/app/AlertDialog;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x6

    if-ne p1, p2, :cond_0

    .line 84
    iget-object p1, p0, Lcom/squareup/register/widgets/EditTextDialogFactory$1;->val$listener:Lcom/squareup/register/widgets/EditTextDialogFactory$EditTextDialogListener;

    iget-object p2, p0, Lcom/squareup/register/widgets/EditTextDialogFactory$1;->val$editor:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p2}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/register/widgets/EditTextDialogFactory$EditTextDialogListener;->onDone(Ljava/lang/String;)V

    .line 85
    iget-object p1, p0, Lcom/squareup/register/widgets/EditTextDialogFactory$1;->val$dialog:Landroid/app/AlertDialog;

    invoke-virtual {p1}, Landroid/app/AlertDialog;->dismiss()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
