.class public final Lcom/squareup/register/widgets/GlassSpinnerState$Factory;
.super Ljava/lang/Object;
.source "GlassSpinnerState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/widgets/GlassSpinnerState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0007J\u001a\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\tH\u0007J\u001a\u0010\n\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\tH\u0007\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/register/widgets/GlassSpinnerState$Factory;",
        "",
        "()V",
        "hideSpinner",
        "Lcom/squareup/register/widgets/GlassSpinnerState;",
        "showDebouncedSpinner",
        "show",
        "",
        "textId",
        "",
        "showNonDebouncedSpinner",
        "widgets-pos_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;-><init>()V

    return-void
.end method

.method public static synthetic showDebouncedSpinner$default(Lcom/squareup/register/widgets/GlassSpinnerState$Factory;ZIILjava/lang/Object;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, -0x1

    .line 46
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->showDebouncedSpinner(ZI)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic showNonDebouncedSpinner$default(Lcom/squareup/register/widgets/GlassSpinnerState$Factory;ZIILjava/lang/Object;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, -0x1

    .line 32
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->showNonDebouncedSpinner(ZI)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final hideSpinner()Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 55
    sget-object v0, Lcom/squareup/register/widgets/GlassSpinnerState$Hide;->INSTANCE:Lcom/squareup/register/widgets/GlassSpinnerState$Hide;

    check-cast v0, Lcom/squareup/register/widgets/GlassSpinnerState;

    return-object v0
.end method

.method public final showDebouncedSpinner(Z)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {p0, p1, v0, v1, v2}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->showDebouncedSpinner$default(Lcom/squareup/register/widgets/GlassSpinnerState$Factory;ZIILjava/lang/Object;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    return-object p1
.end method

.method public final showDebouncedSpinner(ZI)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    if-eqz p1, :cond_0

    .line 48
    new-instance p1, Lcom/squareup/register/widgets/GlassSpinnerState$Show;

    const/4 v0, 0x1

    invoke-direct {p1, v0, p2}, Lcom/squareup/register/widgets/GlassSpinnerState$Show;-><init>(ZI)V

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/register/widgets/GlassSpinnerState$Hide;->INSTANCE:Lcom/squareup/register/widgets/GlassSpinnerState$Hide;

    :goto_0
    check-cast p1, Lcom/squareup/register/widgets/GlassSpinnerState;

    return-object p1
.end method

.method public final showNonDebouncedSpinner(Z)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {p0, p1, v0, v1, v2}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->showNonDebouncedSpinner$default(Lcom/squareup/register/widgets/GlassSpinnerState$Factory;ZIILjava/lang/Object;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    return-object p1
.end method

.method public final showNonDebouncedSpinner(ZI)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    if-eqz p1, :cond_0

    .line 34
    new-instance p1, Lcom/squareup/register/widgets/GlassSpinnerState$Show;

    const/4 v0, 0x0

    invoke-direct {p1, v0, p2}, Lcom/squareup/register/widgets/GlassSpinnerState$Show;-><init>(ZI)V

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/register/widgets/GlassSpinnerState$Hide;->INSTANCE:Lcom/squareup/register/widgets/GlassSpinnerState$Hide;

    :goto_0
    check-cast p1, Lcom/squareup/register/widgets/GlassSpinnerState;

    return-object p1
.end method
