.class Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;
.super Ljava/lang/Object;
.source "SmartScrollListView.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/widgets/SmartScrollListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OnScrollListenerWrapper"
.end annotation


# instance fields
.field private currentVisibleItemHeight:I

.field private currentVisibleItemIndex:I

.field private currentVisibleItemOffset:I

.field private final delegate:Landroid/widget/AbsListView$OnScrollListener;

.field private final headerHeight:I

.field private headerOffset:I

.field private final smartScrollHeaderCallback:Lcom/squareup/register/widgets/SmartScrollListView$SmartScrollHeaderCallback;


# direct methods
.method private constructor <init>(Lcom/squareup/register/widgets/SmartScrollListView$SmartScrollHeaderCallback;Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-object p1, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->smartScrollHeaderCallback:Lcom/squareup/register/widgets/SmartScrollListView$SmartScrollHeaderCallback;

    .line 83
    iput-object p2, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->delegate:Landroid/widget/AbsListView$OnScrollListener;

    .line 84
    invoke-interface {p1}, Lcom/squareup/register/widgets/SmartScrollListView$SmartScrollHeaderCallback;->getHeaderHeight()I

    move-result p1

    iput p1, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->headerHeight:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/register/widgets/SmartScrollListView$SmartScrollHeaderCallback;Landroid/widget/AbsListView$OnScrollListener;Lcom/squareup/register/widgets/SmartScrollListView$1;)V
    .locals 0

    .line 65
    invoke-direct {p0, p1, p2}, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;-><init>(Lcom/squareup/register/widgets/SmartScrollListView$SmartScrollHeaderCallback;Landroid/widget/AbsListView$OnScrollListener;)V

    return-void
.end method

.method private calculateScrollDistance(Landroid/widget/AbsListView;I)I
    .locals 3

    .line 135
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 140
    :cond_0
    invoke-virtual {p1, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    .line 141
    invoke-virtual {p1, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    .line 142
    iget v1, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->currentVisibleItemIndex:I

    if-ne p2, v1, :cond_1

    .line 143
    iget v1, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->currentVisibleItemOffset:I

    :goto_0
    sub-int/2addr v1, v0

    goto :goto_1

    :cond_1
    if-le p2, v1, :cond_3

    add-int/lit8 v1, v1, 0x1

    if-ne p2, v1, :cond_2

    .line 146
    iget v1, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->currentVisibleItemHeight:I

    iget v2, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->currentVisibleItemOffset:I

    add-int/2addr v1, v2

    goto :goto_0

    .line 150
    :cond_2
    iget v1, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->headerHeight:I

    goto :goto_1

    :cond_3
    add-int/lit8 v1, v1, -0x1

    if-ne p2, v1, :cond_4

    .line 154
    iget v1, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->currentVisibleItemOffset:I

    sub-int/2addr v1, p1

    goto :goto_0

    .line 158
    :cond_4
    iget v1, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->headerOffset:I

    neg-int v1, v1

    .line 162
    :goto_1
    iput v0, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->currentVisibleItemOffset:I

    .line 163
    iput p2, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->currentVisibleItemIndex:I

    .line 164
    iput p1, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->currentVisibleItemHeight:I

    return v1
.end method

.method private shouldHeaderAlwaysShow(Landroid/widget/AbsListView;)Z
    .locals 2

    .line 127
    iget v0, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->currentVisibleItemIndex:I

    const/4 v1, 0x0

    if-nez v0, :cond_1

    .line 128
    invoke-virtual {p1, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result p1

    if-ltz p1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :cond_1
    return v1
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 3

    .line 106
    invoke-direct {p0, p1, p2}, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->calculateScrollDistance(Landroid/widget/AbsListView;I)I

    move-result v0

    .line 108
    iget v1, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->headerOffset:I

    iget v2, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->headerHeight:I

    if-ge v1, v2, :cond_0

    if-gtz v0, :cond_1

    :cond_0
    iget v1, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->headerOffset:I

    if-lez v1, :cond_2

    if-gez v0, :cond_2

    .line 110
    :cond_1
    iget v1, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->headerOffset:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->headerOffset:I

    .line 111
    iget v0, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->headerOffset:I

    iget v1, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->headerHeight:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->headerOffset:I

    .line 113
    iget-object v0, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->smartScrollHeaderCallback:Lcom/squareup/register/widgets/SmartScrollListView$SmartScrollHeaderCallback;

    iget v1, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->headerOffset:I

    invoke-interface {v0, v1}, Lcom/squareup/register/widgets/SmartScrollListView$SmartScrollHeaderCallback;->onMoved(I)V

    .line 116
    :cond_2
    iget-object v0, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->delegate:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/widget/AbsListView$OnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V

    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2

    if-nez p2, :cond_2

    .line 92
    iget v0, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->headerOffset:I

    iget v1, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->headerHeight:I

    div-int/lit8 v1, v1, 0x2

    if-lt v0, v1, :cond_1

    invoke-direct {p0, p1}, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->shouldHeaderAlwaysShow(Landroid/widget/AbsListView;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->smartScrollHeaderCallback:Lcom/squareup/register/widgets/SmartScrollListView$SmartScrollHeaderCallback;

    invoke-interface {v0}, Lcom/squareup/register/widgets/SmartScrollListView$SmartScrollHeaderCallback;->onHide()V

    .line 97
    iget v0, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->headerHeight:I

    iput v0, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->headerOffset:I

    goto :goto_1

    .line 93
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->smartScrollHeaderCallback:Lcom/squareup/register/widgets/SmartScrollListView$SmartScrollHeaderCallback;

    invoke-interface {v0}, Lcom/squareup/register/widgets/SmartScrollListView$SmartScrollHeaderCallback;->onShown()V

    const/4 v0, 0x0

    .line 94
    iput v0, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->headerOffset:I

    .line 101
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;->delegate:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2}, Landroid/widget/AbsListView$OnScrollListener;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    return-void
.end method
