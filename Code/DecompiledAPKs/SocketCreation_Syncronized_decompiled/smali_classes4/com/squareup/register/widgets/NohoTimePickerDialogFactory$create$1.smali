.class final Lcom/squareup/register/widgets/NohoTimePickerDialogFactory$create$1;
.super Ljava/lang/Object;
.source "NohoTimePickerDialogFactory.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/register/widgets/NohoTimePickerDialogFactory;->create(Landroid/content/Context;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u001c\u0010\u0003\u001a\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Landroid/app/AlertDialog;",
        "kotlin.jvm.PlatformType",
        "wrapper",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;
    .locals 5

    const-string/jumbo v0, "wrapper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-static {p1}, Lcom/squareup/workflow/legacy/ScreenKt;->getUnwrapV2Screen(Lcom/squareup/workflow/legacy/Screen;)Lcom/squareup/workflow/legacy/V2Screen;

    move-result-object p1

    check-cast p1, Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;

    .line 28
    invoke-virtual {p1}, Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;->getState()Lcom/squareup/register/widgets/NohoTimePickerDialogState;

    move-result-object v0

    .line 29
    invoke-virtual {p1}, Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    .line 31
    iget-object v1, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogFactory$create$1;->$context:Landroid/content/Context;

    sget v2, Lcom/squareup/widgets/pos/R$layout;->noho_time_picker_dialog:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 33
    invoke-virtual {v0}, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->getMaxTime()Lorg/threeten/bp/LocalTime;

    move-result-object v2

    check-cast v2, Ljava/lang/Comparable;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->getMinTime()Lorg/threeten/bp/LocalTime;

    move-result-object v3

    check-cast v3, Ljava/lang/Comparable;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->getCurrentTime()Lorg/threeten/bp/LocalTime;

    move-result-object v4

    check-cast v4, Ljava/lang/Comparable;

    invoke-static {v3, v4}, Lkotlin/comparisons/ComparisonsKt;->maxOf(Ljava/lang/Comparable;Ljava/lang/Comparable;)Ljava/lang/Comparable;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/comparisons/ComparisonsKt;->minOf(Ljava/lang/Comparable;Ljava/lang/Comparable;)Ljava/lang/Comparable;

    move-result-object v2

    check-cast v2, Lorg/threeten/bp/LocalTime;

    const-string/jumbo v3, "view"

    .line 35
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v3, Lcom/squareup/widgets/pos/R$id;->time_picker:I

    invoke-static {v1, v3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/squareup/noho/NohoTimePicker;

    .line 36
    invoke-virtual {v3, v2}, Lcom/squareup/noho/NohoTimePicker;->setCurrentTime(Lorg/threeten/bp/LocalTime;)V

    .line 37
    invoke-virtual {v0}, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->getMinTime()Lorg/threeten/bp/LocalTime;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/squareup/noho/NohoTimePicker;->setMinTime(Lorg/threeten/bp/LocalTime;)V

    .line 38
    invoke-virtual {v0}, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->getMaxTime()Lorg/threeten/bp/LocalTime;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/squareup/noho/NohoTimePicker;->setMaxTime(Lorg/threeten/bp/LocalTime;)V

    .line 40
    new-instance v2, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    iget-object v4, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-direct {v2, v4}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 41
    invoke-virtual {v2, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setView(Landroid/view/View;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x1

    .line 42
    invoke-virtual {v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    .line 43
    new-instance v2, Lcom/squareup/register/widgets/NohoTimePickerDialogFactory$create$1$builder$1;

    invoke-direct {v2, p1}, Lcom/squareup/register/widgets/NohoTimePickerDialogFactory$create$1$builder$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    .line 44
    new-instance v2, Lcom/squareup/register/widgets/NohoTimePickerDialogFactory$create$1$builder$2;

    invoke-direct {v2, p1, v3}, Lcom/squareup/register/widgets/NohoTimePickerDialogFactory$create$1$builder$2;-><init>(Lkotlin/jvm/functions/Function1;Lcom/squareup/noho/NohoTimePicker;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    const v3, 0x104000a

    invoke-virtual {v1, v3, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    .line 48
    invoke-virtual {v0}, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->getAllowClear()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    sget v0, Lcom/squareup/widgets/pos/R$string;->date_picker_remove:I

    new-instance v2, Lcom/squareup/register/widgets/NohoTimePickerDialogFactory$create$1$1;

    invoke-direct {v2, p1}, Lcom/squareup/register/widgets/NohoTimePickerDialogFactory$create$1$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v0, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    goto :goto_0

    :cond_0
    const/high16 v0, 0x1040000

    .line 54
    new-instance v2, Lcom/squareup/register/widgets/NohoTimePickerDialogFactory$create$1$2;

    invoke-direct {v2, p1}, Lcom/squareup/register/widgets/NohoTimePickerDialogFactory$create$1$2;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v0, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    .line 59
    :goto_0
    invoke-virtual {v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/NohoTimePickerDialogFactory$create$1;->apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method
