.class Lcom/squareup/register/widgets/card/CardEditor$4;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "CardEditor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/register/widgets/card/CardEditor;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/register/widgets/card/CardEditor;


# direct methods
.method constructor <init>(Lcom/squareup/register/widgets/card/CardEditor;)V
    .locals 0

    .line 225
    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor$4;->this$0:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .line 227
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor$4;->this$0:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-static {p1}, Lcom/squareup/register/widgets/card/CardEditor;->access$300(Lcom/squareup/register/widgets/card/CardEditor;)Lcom/squareup/register/widgets/card/OnCardListener;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 228
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor$4;->this$0:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-static {p1}, Lcom/squareup/register/widgets/card/CardEditor;->access$300(Lcom/squareup/register/widgets/card/CardEditor;)Lcom/squareup/register/widgets/card/OnCardListener;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor$4;->this$0:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor;->getPartialCard()Lcom/squareup/register/widgets/card/PartialCard;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/register/widgets/card/OnCardListener;->onCardChanged(Lcom/squareup/register/widgets/card/PartialCard;)V

    .line 230
    :cond_0
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor$4;->this$0:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-static {p1}, Lcom/squareup/register/widgets/card/CardEditor;->access$400(Lcom/squareup/register/widgets/card/CardEditor;)Lcom/squareup/register/widgets/card/PanValidationStrategy;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/register/widgets/card/CardEditor$4;->this$0:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-virtual {v1}, Lcom/squareup/register/widgets/card/CardEditor;->getPartialCard()Lcom/squareup/register/widgets/card/PartialCard;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/register/widgets/card/PartialCard;->brand:Lcom/squareup/Card$Brand;

    iget-object v2, p0, Lcom/squareup/register/widgets/card/CardEditor$4;->this$0:Lcom/squareup/register/widgets/card/CardEditor;

    iget-object v2, v2, Lcom/squareup/register/widgets/card/CardEditor;->currency:Lcom/squareup/protos/common/CurrencyCode;

    invoke-interface {v0, v1, v2}, Lcom/squareup/register/widgets/card/PanValidationStrategy;->getGlyph(Lcom/squareup/Card$Brand;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/card/CardEditor;->setCardGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    return-void
.end method
