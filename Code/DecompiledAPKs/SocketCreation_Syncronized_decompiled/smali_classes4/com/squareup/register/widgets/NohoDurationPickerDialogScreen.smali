.class public final Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen;
.super Lcom/squareup/container/ContainerTreeKey;
.source "NohoDurationPickerDialogScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;
.implements Landroid/os/Parcelable;


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$Component;,
        Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;,
        Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoDurationPickerDialogScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoDurationPickerDialogScreen.kt\ncom/squareup/register/widgets/NohoDurationPickerDialogScreen\n+ 2 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,109:1\n24#2,4:110\n*E\n*S KotlinDebug\n*F\n+ 1 NohoDurationPickerDialogScreen.kt\ncom/squareup/register/widgets/NohoDurationPickerDialogScreen\n*L\n35#1,4:110\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0007\u0018\u0000 \n2\u00020\u00012\u00020\u00022\u00020\u0003:\u0003\n\u000b\u000cB\u0005\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00070\u00062\u0006\u0010\u0008\u001a\u00020\tH\u0016\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen;",
        "Lcom/squareup/container/ContainerTreeKey;",
        "Lcom/squareup/workflow/DialogFactory;",
        "Landroid/os/Parcelable;",
        "()V",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "Companion",
        "Component",
        "DialogBuilder",
        "widgets-pos_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen;->Companion:Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$Companion;

    .line 110
    new-instance v0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 113
    sput-object v0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/squareup/container/ContainerTreeKey;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    new-instance v0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;

    invoke-direct {v0, p1}, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;->build()Landroid/app/Dialog;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(DialogBuilder(context).build())"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
