.class public Lcom/squareup/register/widgets/validation/ReboundAnimation;
.super Landroid/view/animation/TranslateAnimation;
.source "ReboundAnimation.java"


# direct methods
.method public constructor <init>(F)V
    .locals 2

    const/4 v0, 0x0

    .line 11
    invoke-direct {p0, p1, v0, v0, v0}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    const-wide/16 v0, 0xc8

    .line 12
    invoke-virtual {p0, v0, v1}, Lcom/squareup/register/widgets/validation/ReboundAnimation;->setDuration(J)V

    .line 13
    new-instance p1, Lcom/squareup/register/widgets/validation/ReboundInterpolator;

    invoke-direct {p1}, Lcom/squareup/register/widgets/validation/ReboundInterpolator;-><init>()V

    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/validation/ReboundAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    return-void
.end method
