.class public abstract Lcom/squareup/register/widgets/SmartScrollContainer;
.super Landroid/widget/FrameLayout;
.source "SmartScrollContainer.java"


# instance fields
.field protected header:Landroid/view/View;

.field private headerHeight:I

.field protected listView:Lcom/squareup/register/widgets/SmartScrollListView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/register/widgets/SmartScrollContainer;)I
    .locals 0

    .line 17
    iget p0, p0, Lcom/squareup/register/widgets/SmartScrollContainer;->headerHeight:I

    return p0
.end method


# virtual methods
.method protected abstract isHeaderEnabled()Z
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 28
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    const/4 v0, 0x0

    .line 30
    invoke-virtual {p0, v0}, Lcom/squareup/register/widgets/SmartScrollContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/SmartScrollListView;

    iput-object v0, p0, Lcom/squareup/register/widgets/SmartScrollContainer;->listView:Lcom/squareup/register/widgets/SmartScrollListView;

    const/4 v0, 0x1

    .line 31
    invoke-virtual {p0, v0}, Lcom/squareup/register/widgets/SmartScrollContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/register/widgets/SmartScrollContainer;->header:Landroid/view/View;

    .line 32
    iget-object v0, p0, Lcom/squareup/register/widgets/SmartScrollContainer;->header:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput v0, p0, Lcom/squareup/register/widgets/SmartScrollContainer;->headerHeight:I

    .line 34
    iget-object v0, p0, Lcom/squareup/register/widgets/SmartScrollContainer;->listView:Lcom/squareup/register/widgets/SmartScrollListView;

    new-instance v1, Lcom/squareup/register/widgets/SmartScrollContainer$1;

    invoke-direct {v1, p0}, Lcom/squareup/register/widgets/SmartScrollContainer$1;-><init>(Lcom/squareup/register/widgets/SmartScrollContainer;)V

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/SmartScrollListView;->updateForSmartScrollHeader(Lcom/squareup/register/widgets/SmartScrollListView$SmartScrollHeaderCallback;)V

    .line 72
    invoke-virtual {p0}, Lcom/squareup/register/widgets/SmartScrollContainer;->isHeaderEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/squareup/register/widgets/SmartScrollContainer;->header:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public resetHeaderTranslationY()V
    .locals 2

    .line 88
    iget-object v0, p0, Lcom/squareup/register/widgets/SmartScrollContainer;->header:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    return-void
.end method

.method public updateHeaderVisibility(Z)V
    .locals 4

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x4

    .line 79
    :goto_0
    iget-object v1, p0, Lcom/squareup/register/widgets/SmartScrollContainer;->header:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v0, :cond_2

    .line 80
    iget-object v1, p0, Lcom/squareup/register/widgets/SmartScrollContainer;->header:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 81
    iget-object v0, p0, Lcom/squareup/register/widgets/SmartScrollContainer;->listView:Lcom/squareup/register/widgets/SmartScrollListView;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/SmartScrollListView;->getPaddingTop()I

    move-result v0

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, -0x1

    :goto_1
    iget-object v1, p0, Lcom/squareup/register/widgets/SmartScrollContainer;->header:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    mul-int p1, p1, v1

    add-int/2addr v0, p1

    .line 82
    iget-object p1, p0, Lcom/squareup/register/widgets/SmartScrollContainer;->listView:Lcom/squareup/register/widgets/SmartScrollListView;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/SmartScrollListView;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/register/widgets/SmartScrollContainer;->listView:Lcom/squareup/register/widgets/SmartScrollListView;

    invoke-virtual {v2}, Lcom/squareup/register/widgets/SmartScrollListView;->getPaddingRight()I

    move-result v2

    iget-object v3, p0, Lcom/squareup/register/widgets/SmartScrollContainer;->listView:Lcom/squareup/register/widgets/SmartScrollListView;

    .line 83
    invoke-virtual {v3}, Lcom/squareup/register/widgets/SmartScrollListView;->getPaddingBottom()I

    move-result v3

    .line 82
    invoke-virtual {p1, v1, v0, v2, v3}, Lcom/squareup/register/widgets/SmartScrollListView;->setPadding(IIII)V

    :cond_2
    return-void
.end method
