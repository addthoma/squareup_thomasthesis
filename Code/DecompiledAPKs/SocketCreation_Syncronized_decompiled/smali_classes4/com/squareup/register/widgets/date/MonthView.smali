.class public final Lcom/squareup/register/widgets/date/MonthView;
.super Landroid/view/ViewGroup;
.source "MonthView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/widgets/date/MonthView$CellDay;,
        Lcom/squareup/register/widgets/date/MonthView$SelectionListener;
    }
.end annotation


# static fields
.field private static final CELL_ROWS:I = 0x6

.field private static final COLUMNS:I = 0x7

.field private static final ROWS:I = 0x7


# instance fields
.field private final cellSize:I

.field private dayOffset:I

.field private hasSelection:Z

.field private final innerVerticalPadding:I

.field private measuredCellSize:I

.field private final rowTopBottomPadding:I

.field private final selectedPaint:Landroid/graphics/Paint;

.field private selectionEndCellIndex:I

.field private selectionEndsAfter:Z

.field private selectionListener:Lcom/squareup/register/widgets/date/MonthView$SelectionListener;

.field private selectionStartCellIndex:I

.field private selectionStartsBefore:Z

.field private final weekdayFormat:Ljava/text/DateFormat;

.field private weekdayRowHeight:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .line 108
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 109
    invoke-virtual {p0}, Lcom/squareup/register/widgets/date/MonthView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 110
    sget p2, Lcom/squareup/marin/R$dimen;->marin_calendar_max_tile_size:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/register/widgets/date/MonthView;->cellSize:I

    .line 111
    sget p2, Lcom/squareup/marin/R$dimen;->marin_gap_calendar_vertical_padding:I

    .line 112
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/register/widgets/date/MonthView;->rowTopBottomPadding:I

    .line 114
    new-instance p2, Ljava/text/SimpleDateFormat;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    const-string v1, "EEE"

    invoke-direct {p2, v1, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object p2, p0, Lcom/squareup/register/widgets/date/MonthView;->weekdayFormat:Ljava/text/DateFormat;

    .line 116
    new-instance p2, Landroid/graphics/Paint;

    const/4 v0, 0x1

    invoke-direct {p2, v0}, Landroid/graphics/Paint;-><init>(I)V

    iput-object p2, p0, Lcom/squareup/register/widgets/date/MonthView;->selectedPaint:Landroid/graphics/Paint;

    .line 117
    iget-object p2, p0, Lcom/squareup/register/widgets/date/MonthView;->selectedPaint:Landroid/graphics/Paint;

    sget v0, Lcom/squareup/marin/R$color;->marin_blue:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 119
    new-instance p2, Lcom/squareup/register/widgets/date/MonthView$1;

    invoke-direct {p2, p0}, Lcom/squareup/register/widgets/date/MonthView$1;-><init>(Lcom/squareup/register/widgets/date/MonthView;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x7

    if-ge v1, v2, :cond_0

    .line 130
    sget v2, Lcom/squareup/widgets/pos/R$layout;->date_picker_weekday:I

    invoke-static {v2, p0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 131
    invoke-virtual {p0, v2}, Lcom/squareup/register/widgets/date/MonthView;->addView(Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_1
    const/4 v3, 0x6

    if-ge v1, v3, :cond_2

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v2, :cond_1

    .line 136
    sget v4, Lcom/squareup/widgets/pos/R$layout;->date_picker_weekday_cell:I

    invoke-static {v4, p0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 137
    invoke-virtual {v4, p2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    invoke-virtual {p0, v4}, Lcom/squareup/register/widgets/date/MonthView;->addView(Landroid/view/View;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 142
    :cond_2
    sget p2, Lcom/squareup/marin/R$dimen;->marin_gap_mini:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/register/widgets/date/MonthView;->innerVerticalPadding:I

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/register/widgets/date/MonthView;)Lcom/squareup/register/widgets/date/MonthView$SelectionListener;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/register/widgets/date/MonthView;->selectionListener:Lcom/squareup/register/widgets/date/MonthView$SelectionListener;

    return-object p0
.end method

.method private clearSelection()V
    .locals 3

    .line 321
    iget-boolean v0, p0, Lcom/squareup/register/widgets/date/MonthView;->hasSelection:Z

    if-eqz v0, :cond_0

    .line 322
    iget v0, p0, Lcom/squareup/register/widgets/date/MonthView;->selectionStartCellIndex:I

    :goto_0
    iget v1, p0, Lcom/squareup/register/widgets/date/MonthView;->selectionEndCellIndex:I

    if-gt v0, v1, :cond_0

    .line 323
    invoke-direct {p0, v0}, Lcom/squareup/register/widgets/date/MonthView;->getCellAt(I)Landroid/widget/TextView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setSelected(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private drawSelectedCellCircle(Landroid/graphics/Canvas;I)V
    .locals 3

    .line 388
    invoke-direct {p0, p2}, Lcom/squareup/register/widgets/date/MonthView;->getCellAt(I)Landroid/widget/TextView;

    move-result-object p2

    .line 389
    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v1

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    .line 390
    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v2

    add-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    .line 391
    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result p2

    div-int/lit8 p2, p2, 0x2

    int-to-float p2, p2

    iget-object v2, p0, Lcom/squareup/register/widgets/date/MonthView;->selectedPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, p2, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    return-void
.end method

.method private getCellAt(I)Landroid/widget/TextView;
    .locals 0

    add-int/lit8 p1, p1, 0x7

    .line 220
    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/date/MonthView;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    return-object p1
.end method

.method private getCellCount()I
    .locals 1

    .line 224
    invoke-virtual {p0}, Lcom/squareup/register/widgets/date/MonthView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x7

    return v0
.end method


# virtual methods
.method dateRangeSelected(Ljava/util/Date;Ljava/util/Date;)V
    .locals 11

    .line 228
    invoke-direct {p0}, Lcom/squareup/register/widgets/date/MonthView;->clearSelection()V

    .line 230
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 231
    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 232
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object p1

    .line 233
    invoke-virtual {p1, p2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 235
    invoke-direct {p0}, Lcom/squareup/register/widgets/date/MonthView;->getCellCount()I

    move-result p2

    .line 236
    invoke-static {v0, p1}, Lcom/squareup/util/Times;->sameDate(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v1

    const/4 v2, 0x5

    const/4 v3, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eqz v1, :cond_3

    .line 237
    iput-boolean v4, p0, Lcom/squareup/register/widgets/date/MonthView;->selectionStartsBefore:Z

    .line 238
    iput-boolean v4, p0, Lcom/squareup/register/widgets/date/MonthView;->selectionEndsAfter:Z

    .line 239
    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result p1

    .line 240
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 241
    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    const/4 v2, 0x0

    :goto_0
    const/4 v3, -0x1

    if-ge v2, p2, :cond_1

    .line 245
    invoke-direct {p0, v2}, Lcom/squareup/register/widgets/date/MonthView;->getCellAt(I)Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/register/widgets/date/MonthView$CellDay;

    .line 246
    iget v7, v6, Lcom/squareup/register/widgets/date/MonthView$CellDay;->year:I

    if-ne v7, p1, :cond_0

    iget v7, v6, Lcom/squareup/register/widgets/date/MonthView$CellDay;->month:I

    if-ne v7, v1, :cond_0

    iget v6, v6, Lcom/squareup/register/widgets/date/MonthView$CellDay;->day:I

    if-ne v6, v0, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, -0x1

    :goto_1
    if-eq v2, v3, :cond_2

    const/4 v4, 0x1

    .line 251
    :cond_2
    iput-boolean v4, p0, Lcom/squareup/register/widgets/date/MonthView;->hasSelection:Z

    .line 252
    iget-boolean p1, p0, Lcom/squareup/register/widgets/date/MonthView;->hasSelection:Z

    if-eqz p1, :cond_17

    .line 253
    iput v2, p0, Lcom/squareup/register/widgets/date/MonthView;->selectionEndCellIndex:I

    iput v2, p0, Lcom/squareup/register/widgets/date/MonthView;->selectionStartCellIndex:I

    .line 254
    invoke-direct {p0, v2}, Lcom/squareup/register/widgets/date/MonthView;->getCellAt(I)Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1, v5}, Landroid/widget/TextView;->setSelected(Z)V

    goto/16 :goto_10

    .line 257
    :cond_3
    invoke-direct {p0, v4}, Lcom/squareup/register/widgets/date/MonthView;->getCellAt(I)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/register/widgets/date/MonthView$CellDay;

    add-int/lit8 v6, p2, -0x1

    .line 258
    invoke-direct {p0, v6}, Lcom/squareup/register/widgets/date/MonthView;->getCellAt(I)Landroid/widget/TextView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/register/widgets/date/MonthView$CellDay;

    .line 260
    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v8

    .line 261
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v9

    .line 262
    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 263
    invoke-virtual {p1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v10

    .line 264
    invoke-virtual {p1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 265
    invoke-virtual {p1, v2}, Ljava/util/Calendar;->get(I)I

    move-result p1

    .line 266
    iget v2, v1, Lcom/squareup/register/widgets/date/MonthView$CellDay;->year:I

    if-lt v8, v2, :cond_6

    iget v2, v1, Lcom/squareup/register/widgets/date/MonthView$CellDay;->year:I

    if-ne v8, v2, :cond_4

    iget v2, v1, Lcom/squareup/register/widgets/date/MonthView$CellDay;->month:I

    if-lt v9, v2, :cond_6

    :cond_4
    iget v2, v1, Lcom/squareup/register/widgets/date/MonthView$CellDay;->year:I

    if-ne v8, v2, :cond_5

    iget v2, v1, Lcom/squareup/register/widgets/date/MonthView$CellDay;->month:I

    if-ne v9, v2, :cond_5

    iget v2, v1, Lcom/squareup/register/widgets/date/MonthView$CellDay;->day:I

    if-ge v0, v2, :cond_5

    goto :goto_2

    :cond_5
    const/4 v2, 0x0

    goto :goto_3

    :cond_6
    :goto_2
    const/4 v2, 0x1

    :goto_3
    iput-boolean v2, p0, Lcom/squareup/register/widgets/date/MonthView;->selectionStartsBefore:Z

    .line 271
    iget v2, v7, Lcom/squareup/register/widgets/date/MonthView$CellDay;->year:I

    if-gt v10, v2, :cond_9

    iget v2, v7, Lcom/squareup/register/widgets/date/MonthView$CellDay;->year:I

    if-ne v10, v2, :cond_7

    iget v2, v7, Lcom/squareup/register/widgets/date/MonthView$CellDay;->month:I

    if-gt v3, v2, :cond_9

    :cond_7
    iget v2, v7, Lcom/squareup/register/widgets/date/MonthView$CellDay;->year:I

    if-ne v10, v2, :cond_8

    iget v2, v7, Lcom/squareup/register/widgets/date/MonthView$CellDay;->month:I

    if-ne v3, v2, :cond_8

    iget v2, v7, Lcom/squareup/register/widgets/date/MonthView$CellDay;->day:I

    if-le p1, v2, :cond_8

    goto :goto_4

    :cond_8
    const/4 v2, 0x0

    goto :goto_5

    :cond_9
    :goto_4
    const/4 v2, 0x1

    :goto_5
    iput-boolean v2, p0, Lcom/squareup/register/widgets/date/MonthView;->selectionEndsAfter:Z

    .line 274
    iget v2, v7, Lcom/squareup/register/widgets/date/MonthView$CellDay;->year:I

    if-gt v8, v2, :cond_c

    iget v2, v7, Lcom/squareup/register/widgets/date/MonthView$CellDay;->year:I

    if-ne v8, v2, :cond_a

    iget v2, v7, Lcom/squareup/register/widgets/date/MonthView$CellDay;->month:I

    if-gt v9, v2, :cond_c

    :cond_a
    iget v2, v7, Lcom/squareup/register/widgets/date/MonthView$CellDay;->year:I

    if-ne v8, v2, :cond_b

    iget v2, v7, Lcom/squareup/register/widgets/date/MonthView$CellDay;->month:I

    if-ne v9, v2, :cond_b

    iget v2, v7, Lcom/squareup/register/widgets/date/MonthView$CellDay;->day:I

    if-le v0, v2, :cond_b

    goto :goto_6

    :cond_b
    const/4 v2, 0x0

    goto :goto_7

    :cond_c
    :goto_6
    const/4 v2, 0x1

    .line 279
    :goto_7
    iget v7, v1, Lcom/squareup/register/widgets/date/MonthView$CellDay;->year:I

    if-lt v10, v7, :cond_f

    iget v7, v1, Lcom/squareup/register/widgets/date/MonthView$CellDay;->year:I

    if-ne v10, v7, :cond_d

    iget v7, v1, Lcom/squareup/register/widgets/date/MonthView$CellDay;->month:I

    if-lt v3, v7, :cond_f

    :cond_d
    iget v7, v1, Lcom/squareup/register/widgets/date/MonthView$CellDay;->year:I

    if-ne v10, v7, :cond_e

    iget v7, v1, Lcom/squareup/register/widgets/date/MonthView$CellDay;->month:I

    if-ne v3, v7, :cond_e

    iget v1, v1, Lcom/squareup/register/widgets/date/MonthView$CellDay;->day:I

    if-ge p1, v1, :cond_e

    goto :goto_8

    :cond_e
    const/4 v1, 0x0

    goto :goto_9

    :cond_f
    :goto_8
    const/4 v1, 0x1

    :goto_9
    if-nez v1, :cond_10

    if-nez v2, :cond_10

    const/4 v1, 0x1

    goto :goto_a

    :cond_10
    const/4 v1, 0x0

    .line 283
    :goto_a
    iput-boolean v1, p0, Lcom/squareup/register/widgets/date/MonthView;->hasSelection:Z

    .line 285
    iget-boolean v1, p0, Lcom/squareup/register/widgets/date/MonthView;->hasSelection:Z

    if-eqz v1, :cond_17

    .line 286
    iget-boolean v1, p0, Lcom/squareup/register/widgets/date/MonthView;->selectionStartsBefore:Z

    if-eqz v1, :cond_11

    .line 287
    iput v4, p0, Lcom/squareup/register/widgets/date/MonthView;->selectionStartCellIndex:I

    goto :goto_c

    :cond_11
    const/4 v1, 0x0

    :goto_b
    if-ge v1, p2, :cond_13

    .line 291
    invoke-direct {p0, v1}, Lcom/squareup/register/widgets/date/MonthView;->getCellAt(I)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/register/widgets/date/MonthView$CellDay;

    .line 292
    iget v7, v2, Lcom/squareup/register/widgets/date/MonthView$CellDay;->year:I

    if-ne v7, v8, :cond_12

    iget v7, v2, Lcom/squareup/register/widgets/date/MonthView$CellDay;->month:I

    if-ne v7, v9, :cond_12

    iget v2, v2, Lcom/squareup/register/widgets/date/MonthView$CellDay;->day:I

    if-ne v2, v0, :cond_12

    .line 295
    iput v1, p0, Lcom/squareup/register/widgets/date/MonthView;->selectionStartCellIndex:I

    goto :goto_c

    :cond_12
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    .line 300
    :cond_13
    :goto_c
    iget-boolean v0, p0, Lcom/squareup/register/widgets/date/MonthView;->selectionEndsAfter:Z

    if-eqz v0, :cond_14

    .line 301
    iput v6, p0, Lcom/squareup/register/widgets/date/MonthView;->selectionEndCellIndex:I

    goto :goto_e

    :cond_14
    :goto_d
    if-ge v4, p2, :cond_16

    .line 305
    invoke-direct {p0, v4}, Lcom/squareup/register/widgets/date/MonthView;->getCellAt(I)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/date/MonthView$CellDay;

    .line 306
    iget v1, v0, Lcom/squareup/register/widgets/date/MonthView$CellDay;->year:I

    if-ne v1, v10, :cond_15

    iget v1, v0, Lcom/squareup/register/widgets/date/MonthView$CellDay;->month:I

    if-ne v1, v3, :cond_15

    iget v0, v0, Lcom/squareup/register/widgets/date/MonthView$CellDay;->day:I

    if-ne v0, p1, :cond_15

    .line 307
    iput v4, p0, Lcom/squareup/register/widgets/date/MonthView;->selectionEndCellIndex:I

    goto :goto_e

    :cond_15
    add-int/lit8 v4, v4, 0x1

    goto :goto_d

    .line 312
    :cond_16
    :goto_e
    iget p1, p0, Lcom/squareup/register/widgets/date/MonthView;->selectionStartCellIndex:I

    :goto_f
    iget p2, p0, Lcom/squareup/register/widgets/date/MonthView;->selectionEndCellIndex:I

    if-gt p1, p2, :cond_17

    .line 313
    invoke-direct {p0, p1}, Lcom/squareup/register/widgets/date/MonthView;->getCellAt(I)Landroid/widget/TextView;

    move-result-object p2

    invoke-virtual {p2, v5}, Landroid/widget/TextView;->setSelected(Z)V

    add-int/lit8 p1, p1, 0x1

    goto :goto_f

    .line 317
    :cond_17
    :goto_10
    invoke-virtual {p0}, Lcom/squareup/register/widgets/date/MonthView;->invalidate()V

    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 14

    .line 329
    iget-boolean v0, p0, Lcom/squareup/register/widgets/date/MonthView;->hasSelection:Z

    if-nez v0, :cond_0

    .line 330
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    return-void

    .line 334
    :cond_0
    iget v0, p0, Lcom/squareup/register/widgets/date/MonthView;->selectionStartCellIndex:I

    iget v1, p0, Lcom/squareup/register/widgets/date/MonthView;->selectionEndCellIndex:I

    if-ne v0, v1, :cond_1

    iget-boolean v1, p0, Lcom/squareup/register/widgets/date/MonthView;->selectionStartsBefore:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/squareup/register/widgets/date/MonthView;->selectionEndsAfter:Z

    if-nez v1, :cond_1

    .line 337
    invoke-direct {p0, p1, v0}, Lcom/squareup/register/widgets/date/MonthView;->drawSelectedCellCircle(Landroid/graphics/Canvas;I)V

    .line 338
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    return-void

    .line 344
    :cond_1
    iget-boolean v0, p0, Lcom/squareup/register/widgets/date/MonthView;->selectionStartsBefore:Z

    if-eqz v0, :cond_2

    const/4 v0, -0x1

    goto :goto_0

    .line 347
    :cond_2
    iget v0, p0, Lcom/squareup/register/widgets/date/MonthView;->selectionStartCellIndex:I

    div-int/lit8 v1, v0, 0x7

    .line 348
    invoke-direct {p0, p1, v0}, Lcom/squareup/register/widgets/date/MonthView;->drawSelectedCellCircle(Landroid/graphics/Canvas;I)V

    move v0, v1

    .line 350
    :goto_0
    iget-boolean v1, p0, Lcom/squareup/register/widgets/date/MonthView;->selectionEndsAfter:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x7

    goto :goto_1

    .line 353
    :cond_3
    iget v1, p0, Lcom/squareup/register/widgets/date/MonthView;->selectionEndCellIndex:I

    div-int/lit8 v2, v1, 0x7

    .line 354
    invoke-direct {p0, p1, v1}, Lcom/squareup/register/widgets/date/MonthView;->drawSelectedCellCircle(Landroid/graphics/Canvas;I)V

    move v1, v2

    :goto_1
    const/4 v2, 0x0

    :goto_2
    const/4 v3, 0x6

    if-ge v2, v3, :cond_8

    if-lt v2, v0, :cond_7

    if-le v2, v1, :cond_4

    goto :goto_5

    :cond_4
    mul-int/lit8 v3, v2, 0x7

    .line 362
    invoke-direct {p0, v3}, Lcom/squareup/register/widgets/date/MonthView;->getCellAt(I)Landroid/widget/TextView;

    move-result-object v3

    .line 363
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v4

    .line 364
    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v3

    if-ne v2, v0, :cond_5

    .line 368
    iget v5, p0, Lcom/squareup/register/widgets/date/MonthView;->selectionStartCellIndex:I

    invoke-direct {p0, v5}, Lcom/squareup/register/widgets/date/MonthView;->getCellAt(I)Landroid/widget/TextView;

    move-result-object v5

    .line 369
    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v6

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v5

    add-int/2addr v6, v5

    div-int/lit8 v6, v6, 0x2

    goto :goto_3

    .line 371
    :cond_5
    invoke-virtual {p0}, Lcom/squareup/register/widgets/date/MonthView;->getPaddingLeft()I

    move-result v5

    iget v6, p0, Lcom/squareup/register/widgets/date/MonthView;->innerVerticalPadding:I

    sub-int v6, v5, v6

    :goto_3
    if-ne v2, v1, :cond_6

    .line 376
    iget v5, p0, Lcom/squareup/register/widgets/date/MonthView;->selectionEndCellIndex:I

    invoke-direct {p0, v5}, Lcom/squareup/register/widgets/date/MonthView;->getCellAt(I)Landroid/widget/TextView;

    move-result-object v5

    .line 377
    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v7

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v5

    add-int/2addr v7, v5

    div-int/lit8 v7, v7, 0x2

    goto :goto_4

    .line 379
    :cond_6
    invoke-virtual {p0}, Lcom/squareup/register/widgets/date/MonthView;->getWidth()I

    move-result v5

    invoke-virtual {p0}, Lcom/squareup/register/widgets/date/MonthView;->getPaddingRight()I

    move-result v7

    iget v8, p0, Lcom/squareup/register/widgets/date/MonthView;->innerVerticalPadding:I

    sub-int/2addr v7, v8

    sub-int v7, v5, v7

    :goto_4
    int-to-float v9, v6

    int-to-float v10, v4

    int-to-float v11, v7

    int-to-float v12, v3

    .line 382
    iget-object v13, p0, Lcom/squareup/register/widgets/date/MonthView;->selectedPaint:Landroid/graphics/Paint;

    move-object v8, p1

    invoke-virtual/range {v8 .. v13}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_7
    :goto_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 384
    :cond_8
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 9

    .line 456
    invoke-virtual {p0}, Lcom/squareup/register/widgets/date/MonthView;->getPaddingTop()I

    move-result p1

    .line 457
    invoke-virtual {p0}, Lcom/squareup/register/widgets/date/MonthView;->getPaddingLeft()I

    move-result p2

    .line 458
    invoke-virtual {p0}, Lcom/squareup/register/widgets/date/MonthView;->getPaddingRight()I

    move-result p3

    .line 459
    invoke-virtual {p0}, Lcom/squareup/register/widgets/date/MonthView;->getMeasuredWidth()I

    move-result p4

    add-int/2addr p3, p2

    sub-int/2addr p4, p3

    .line 460
    iget p3, p0, Lcom/squareup/register/widgets/date/MonthView;->measuredCellSize:I

    const/4 p5, 0x7

    mul-int/lit8 p3, p3, 0x7

    sub-int/2addr p4, p3

    const/4 p3, 0x6

    div-int/2addr p4, p3

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p5, :cond_0

    .line 464
    invoke-virtual {p0, v1}, Lcom/squareup/register/widgets/date/MonthView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 465
    iget v3, p0, Lcom/squareup/register/widgets/date/MonthView;->measuredCellSize:I

    add-int v4, v3, p4

    mul-int v4, v4, v1

    add-int/2addr v4, p2

    .line 466
    div-int/lit8 v3, v3, 0x2

    add-int/2addr v4, v3

    .line 467
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    .line 468
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    .line 469
    div-int/lit8 v6, v3, 0x2

    sub-int/2addr v4, v6

    add-int/2addr v3, v4

    .line 470
    invoke-virtual {v2, v4, v0, v3, v5}, Landroid/view/View;->layout(IIII)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_1
    if-ge v1, p3, :cond_2

    move v3, v2

    const/4 v2, 0x0

    :goto_2
    if-ge v2, p5, :cond_1

    .line 474
    invoke-direct {p0, v3}, Lcom/squareup/register/widgets/date/MonthView;->getCellAt(I)Landroid/widget/TextView;

    move-result-object v4

    add-int/lit8 v3, v3, 0x1

    .line 476
    iget v5, p0, Lcom/squareup/register/widgets/date/MonthView;->measuredCellSize:I

    add-int v6, v5, p4

    mul-int v6, v6, v2

    add-int/2addr v6, p2

    .line 477
    iget v7, p0, Lcom/squareup/register/widgets/date/MonthView;->weekdayRowHeight:I

    add-int/2addr v7, p1

    iget v8, p0, Lcom/squareup/register/widgets/date/MonthView;->rowTopBottomPadding:I

    add-int/2addr v7, v8

    add-int/2addr v8, v5

    mul-int v8, v8, v1

    add-int/2addr v7, v8

    add-int v8, v6, v5

    add-int/2addr v5, v7

    .line 479
    invoke-virtual {v4, v6, v7, v8, v5}, Landroid/view/View;->layout(IIII)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_1
    add-int/lit8 v1, v1, 0x1

    move v2, v3

    goto :goto_1

    :cond_2
    return-void
.end method

.method protected onMeasure(II)V
    .locals 11

    .line 395
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-eqz v0, :cond_6

    .line 399
    iget v0, p0, Lcom/squareup/register/widgets/date/MonthView;->cellSize:I

    iput v0, p0, Lcom/squareup/register/widgets/date/MonthView;->measuredCellSize:I

    .line 400
    invoke-virtual {p0}, Lcom/squareup/register/widgets/date/MonthView;->getPaddingTop()I

    move-result v0

    .line 401
    invoke-virtual {p0}, Lcom/squareup/register/widgets/date/MonthView;->getPaddingLeft()I

    move-result v1

    .line 402
    invoke-virtual {p0}, Lcom/squareup/register/widgets/date/MonthView;->getPaddingBottom()I

    move-result v2

    .line 403
    invoke-virtual {p0}, Lcom/squareup/register/widgets/date/MonthView;->getPaddingRight()I

    move-result v3

    const/4 v4, 0x0

    .line 405
    invoke-static {v4, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 406
    iput v4, p0, Lcom/squareup/register/widgets/date/MonthView;->weekdayRowHeight:I

    const/4 v6, 0x0

    :goto_0
    const/4 v7, 0x7

    if-ge v6, v7, :cond_0

    .line 408
    invoke-virtual {p0, v6}, Lcom/squareup/register/widgets/date/MonthView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 409
    invoke-virtual {v7, v5, v5}, Landroid/view/View;->measure(II)V

    .line 410
    iget v8, p0, Lcom/squareup/register/widgets/date/MonthView;->weekdayRowHeight:I

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    invoke-static {v8, v7}, Ljava/lang/Math;->max(II)I

    move-result v7

    iput v7, p0, Lcom/squareup/register/widgets/date/MonthView;->weekdayRowHeight:I

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 413
    :cond_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v6

    const/high16 v8, -0x80000000

    const/high16 v9, 0x40000000    # 2.0f

    if-ne v6, v8, :cond_1

    .line 415
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p2

    .line 416
    iget v6, p0, Lcom/squareup/register/widgets/date/MonthView;->cellSize:I

    iget v8, p0, Lcom/squareup/register/widgets/date/MonthView;->rowTopBottomPadding:I

    add-int/2addr v6, v8

    mul-int/lit8 v6, v6, 0x6

    add-int/2addr v6, v0

    iget v10, p0, Lcom/squareup/register/widgets/date/MonthView;->weekdayRowHeight:I

    add-int/2addr v6, v10

    add-int/2addr v6, v2

    if-ge p2, v6, :cond_2

    add-int/2addr v10, v0

    add-int/2addr v10, v2

    sub-int/2addr p2, v10

    mul-int/lit8 v8, v8, 0x6

    sub-int/2addr p2, v8

    .line 421
    div-int/lit8 p2, p2, 0x6

    iput p2, p0, Lcom/squareup/register/widgets/date/MonthView;->measuredCellSize:I

    goto :goto_1

    :cond_1
    if-ne v6, v9, :cond_2

    .line 425
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p2

    .line 426
    iget v6, p0, Lcom/squareup/register/widgets/date/MonthView;->weekdayRowHeight:I

    add-int/2addr v6, v0

    add-int/2addr v6, v2

    sub-int/2addr p2, v6

    iget v6, p0, Lcom/squareup/register/widgets/date/MonthView;->rowTopBottomPadding:I

    mul-int/lit8 v6, v6, 0x6

    sub-int/2addr p2, v6

    div-int/lit8 p2, p2, 0x6

    iput p2, p0, Lcom/squareup/register/widgets/date/MonthView;->measuredCellSize:I

    .line 430
    :cond_2
    :goto_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    .line 431
    iget p2, p0, Lcom/squareup/register/widgets/date/MonthView;->measuredCellSize:I

    mul-int/lit8 p2, p2, 0x7

    add-int/2addr p2, v1

    add-int/2addr p2, v3

    if-le p2, p1, :cond_3

    sub-int p2, p1, v1

    sub-int/2addr p2, v3

    .line 432
    div-int/2addr p2, v7

    iput p2, p0, Lcom/squareup/register/widgets/date/MonthView;->measuredCellSize:I

    .line 435
    :cond_3
    iget p2, p0, Lcom/squareup/register/widgets/date/MonthView;->measuredCellSize:I

    iget v1, p0, Lcom/squareup/register/widgets/date/MonthView;->rowTopBottomPadding:I

    add-int/2addr v1, p2

    mul-int/lit8 v1, v1, 0x6

    add-int/2addr v1, v0

    iget v0, p0, Lcom/squareup/register/widgets/date/MonthView;->weekdayRowHeight:I

    add-int/2addr v1, v0

    add-int/2addr v1, v2

    .line 440
    invoke-static {p2, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 442
    iget v0, p0, Lcom/squareup/register/widgets/date/MonthView;->weekdayRowHeight:I

    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    :goto_2
    if-ge v4, v7, :cond_4

    .line 444
    invoke-virtual {p0, v4}, Lcom/squareup/register/widgets/date/MonthView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 445
    invoke-virtual {v2, v5, v0}, Landroid/view/View;->measure(II)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 447
    :cond_4
    invoke-virtual {p0}, Lcom/squareup/register/widgets/date/MonthView;->getChildCount()I

    move-result v0

    :goto_3
    if-ge v7, v0, :cond_5

    .line 449
    invoke-virtual {p0, v7}, Lcom/squareup/register/widgets/date/MonthView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 450
    invoke-virtual {v2, p2, p2}, Landroid/view/View;->measure(II)V

    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 452
    :cond_5
    invoke-virtual {p0, p1, v1}, Lcom/squareup/register/widgets/date/MonthView;->setMeasuredDimension(II)V

    return-void

    .line 396
    :cond_6
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string/jumbo p2, "width needs constraints"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method setMonth(IILjava/util/Calendar;Ljava/util/Calendar;)V
    .locals 16

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    const/4 v5, 0x1

    .line 154
    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v6

    const/4 v7, 0x2

    .line 155
    invoke-virtual {v4, v7}, Ljava/util/Calendar;->get(I)I

    move-result v8

    const/4 v9, 0x5

    .line 156
    invoke-virtual {v4, v9}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 158
    invoke-virtual {v3, v5}, Ljava/util/Calendar;->get(I)I

    move-result v10

    .line 159
    invoke-virtual {v3, v7}, Ljava/util/Calendar;->get(I)I

    move-result v11

    .line 160
    invoke-virtual {v3, v9}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 163
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v12

    .line 164
    invoke-virtual {v12}, Ljava/util/Calendar;->getFirstDayOfWeek()I

    move-result v13

    .line 167
    invoke-virtual {v12, v1, v2, v5}, Ljava/util/Calendar;->set(III)V

    const/4 v14, 0x7

    .line 168
    invoke-virtual {v12, v14}, Ljava/util/Calendar;->get(I)I

    move-result v15

    sub-int v15, v13, v15

    .line 170
    iput v15, v0, Lcom/squareup/register/widgets/date/MonthView;->dayOffset:I

    .line 171
    iget v15, v0, Lcom/squareup/register/widgets/date/MonthView;->dayOffset:I

    if-lez v15, :cond_0

    sub-int/2addr v15, v14

    .line 173
    iput v15, v0, Lcom/squareup/register/widgets/date/MonthView;->dayOffset:I

    :cond_0
    const/4 v15, 0x0

    const/4 v9, 0x0

    :goto_0
    if-ge v9, v14, :cond_1

    add-int v5, v13, v9

    .line 181
    invoke-virtual {v12, v14, v5}, Ljava/util/Calendar;->set(II)V

    .line 182
    iget-object v5, v0, Lcom/squareup/register/widgets/date/MonthView;->weekdayFormat:Ljava/text/DateFormat;

    invoke-virtual {v12}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v14

    invoke-virtual {v5, v14}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    .line 183
    invoke-virtual {v0, v9}, Lcom/squareup/register/widgets/date/MonthView;->getChildAt(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    .line 184
    invoke-virtual {v14, v7}, Landroid/widget/TextView;->setImportantForAccessibility(I)V

    .line 185
    invoke-virtual {v5, v15}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v14, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    add-int/lit8 v9, v9, 0x1

    const/4 v5, 0x1

    const/4 v14, 0x7

    goto :goto_0

    .line 188
    :cond_1
    invoke-virtual {v12, v1, v2, v5}, Ljava/util/Calendar;->set(III)V

    .line 189
    iget v1, v0, Lcom/squareup/register/widgets/date/MonthView;->dayOffset:I

    const/4 v9, 0x5

    invoke-virtual {v12, v9, v1}, Ljava/util/Calendar;->add(II)V

    .line 192
    invoke-direct/range {p0 .. p0}, Lcom/squareup/register/widgets/date/MonthView;->getCellCount()I

    move-result v1

    const/4 v13, 0x0

    :goto_1
    if-ge v13, v1, :cond_b

    .line 194
    invoke-direct {v0, v13}, Lcom/squareup/register/widgets/date/MonthView;->getCellAt(I)Landroid/widget/TextView;

    move-result-object v14

    .line 195
    invoke-virtual {v12, v5}, Ljava/util/Calendar;->get(I)I

    move-result v15

    .line 196
    invoke-virtual {v12, v7}, Ljava/util/Calendar;->get(I)I

    move-result v5

    .line 197
    invoke-virtual {v12, v9}, Ljava/util/Calendar;->get(I)I

    move-result v7

    .line 198
    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v14, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-ne v5, v2, :cond_2

    const/4 v9, 0x1

    goto :goto_2

    :cond_2
    const/4 v9, 0x0

    .line 200
    :goto_2
    new-instance v0, Lcom/squareup/register/widgets/date/MonthView$CellDay;

    invoke-direct {v0, v15, v5, v7, v9}, Lcom/squareup/register/widgets/date/MonthView$CellDay;-><init>(IIIZ)V

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    if-gt v15, v10, :cond_5

    if-ne v15, v10, :cond_3

    if-gt v5, v11, :cond_5

    :cond_3
    if-ne v15, v10, :cond_4

    if-ne v5, v11, :cond_4

    if-lt v7, v3, :cond_4

    goto :goto_3

    :cond_4
    const/4 v0, 0x0

    goto :goto_4

    :cond_5
    :goto_3
    const/4 v0, 0x1

    :goto_4
    if-lt v15, v6, :cond_8

    if-ne v15, v6, :cond_6

    if-lt v5, v8, :cond_8

    :cond_6
    if-ne v15, v6, :cond_7

    if-ne v5, v8, :cond_7

    if-gt v7, v4, :cond_7

    goto :goto_5

    :cond_7
    const/4 v5, 0x0

    goto :goto_6

    :cond_8
    :goto_5
    const/4 v5, 0x1

    :goto_6
    if-eqz v0, :cond_9

    if-eqz v5, :cond_9

    const/4 v0, 0x1

    goto :goto_7

    :cond_9
    const/4 v0, 0x0

    :goto_7
    if-eqz v9, :cond_a

    if-eqz v0, :cond_a

    const/4 v5, 0x1

    goto :goto_8

    :cond_a
    const/4 v5, 0x0

    .line 212
    :goto_8
    invoke-virtual {v14, v5}, Landroid/widget/TextView;->setActivated(Z)V

    .line 213
    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    const/4 v0, 0x5

    const/4 v5, 0x1

    .line 214
    invoke-virtual {v12, v0, v5}, Ljava/util/Calendar;->add(II)V

    add-int/lit8 v13, v13, 0x1

    const/4 v7, 0x2

    const/4 v9, 0x5

    const/4 v15, 0x0

    move-object/from16 v0, p0

    goto :goto_1

    :cond_b
    return-void
.end method

.method public setSelectionListener(Lcom/squareup/register/widgets/date/MonthView$SelectionListener;)V
    .locals 0

    .line 485
    iput-object p1, p0, Lcom/squareup/register/widgets/date/MonthView;->selectionListener:Lcom/squareup/register/widgets/date/MonthView$SelectionListener;

    return-void
.end method
