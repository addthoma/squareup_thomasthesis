.class public final Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Creator_Factory;
.super Ljava/lang/Object;
.source "FirstCardPaymentTutorialV2_Creator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;",
        ">;"
    }
.end annotation


# instance fields
.field private final appIdlingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/AppIdling;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryAppletGatewayProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCogsHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialCogsHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/AppIdling;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialCogsHelper;",
            ">;)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Creator_Factory;->tutorialProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p2, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Creator_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p3, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Creator_Factory;->appIdlingProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p4, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Creator_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p5, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Creator_Factory;->orderEntryAppletGatewayProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p6, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Creator_Factory;->tutorialCogsHelperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Creator_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/AppIdling;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialCogsHelper;",
            ">;)",
            "Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Creator_Factory;"
        }
    .end annotation

    .line 57
    new-instance v7, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Creator_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Creator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Ljavax/inject/Provider;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/main/AppIdling;Lcom/squareup/settings/server/Features;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/register/tutorial/TutorialCogsHelper;)Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;",
            ">;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/ui/main/AppIdling;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            "Lcom/squareup/register/tutorial/TutorialCogsHelper;",
            ")",
            "Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;"
        }
    .end annotation

    .line 64
    new-instance v7, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;-><init>(Ljavax/inject/Provider;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/main/AppIdling;Lcom/squareup/settings/server/Features;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/register/tutorial/TutorialCogsHelper;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;
    .locals 6

    .line 48
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Creator_Factory;->tutorialProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Creator_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v2, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Creator_Factory;->appIdlingProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/main/AppIdling;

    iget-object v3, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Creator_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/settings/server/Features;

    iget-object v4, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Creator_Factory;->orderEntryAppletGatewayProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/orderentry/OrderEntryAppletGateway;

    iget-object v5, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Creator_Factory;->tutorialCogsHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/register/tutorial/TutorialCogsHelper;

    invoke-static/range {v0 .. v5}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Creator_Factory;->newInstance(Ljavax/inject/Provider;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/main/AppIdling;Lcom/squareup/settings/server/Features;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/register/tutorial/TutorialCogsHelper;)Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Creator_Factory;->get()Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;

    move-result-object v0

    return-object v0
.end method
