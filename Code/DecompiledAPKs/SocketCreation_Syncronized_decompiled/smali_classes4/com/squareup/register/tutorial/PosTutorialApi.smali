.class public Lcom/squareup/register/tutorial/PosTutorialApi;
.super Lcom/squareup/register/tutorial/TutorialApi;
.source "PosTutorialApi.java"


# instance fields
.field private final cardTutorial:Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;

.field private final cashTutorial:Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

.field private placeholderCreator:Lcom/squareup/register/tutorial/PlaceholderTutorial$Creator;

.field private final posContainer:Lcom/squareup/ui/main/PosContainer;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

.field private final v2Creator:Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;


# direct methods
.method constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;Lcom/squareup/register/tutorial/PlaceholderTutorial$Creator;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/orderentry/OrderEntryAppletGateway;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 37
    invoke-direct {p0}, Lcom/squareup/register/tutorial/TutorialApi;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/register/tutorial/PosTutorialApi;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 39
    iput-object p2, p0, Lcom/squareup/register/tutorial/PosTutorialApi;->features:Lcom/squareup/settings/server/Features;

    .line 40
    iput-object p3, p0, Lcom/squareup/register/tutorial/PosTutorialApi;->cardTutorial:Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;

    .line 41
    iput-object p4, p0, Lcom/squareup/register/tutorial/PosTutorialApi;->cashTutorial:Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;

    .line 42
    iput-object p5, p0, Lcom/squareup/register/tutorial/PosTutorialApi;->v2Creator:Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;

    .line 43
    iput-object p6, p0, Lcom/squareup/register/tutorial/PosTutorialApi;->placeholderCreator:Lcom/squareup/register/tutorial/PlaceholderTutorial$Creator;

    .line 44
    iput-object p7, p0, Lcom/squareup/register/tutorial/PosTutorialApi;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    .line 45
    iput-object p8, p0, Lcom/squareup/register/tutorial/PosTutorialApi;->posContainer:Lcom/squareup/ui/main/PosContainer;

    .line 46
    iput-object p9, p0, Lcom/squareup/register/tutorial/PosTutorialApi;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    return-void
.end method

.method private acceptsCards()Z
    .locals 1

    .line 131
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosTutorialApi;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getOnboardingSettings()Lcom/squareup/settings/server/OnboardingSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/OnboardingSettings;->acceptsCards()Z

    move-result v0

    return v0
.end method

.method private onEndFirstPaymentTutorial(Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;)V
    .locals 1

    .line 114
    invoke-virtual {p1}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->clearOnEndAction()V

    .line 115
    iget-object p1, p0, Lcom/squareup/register/tutorial/PosTutorialApi;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Exit placeholder tutorial"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    return-void
.end method

.method private resetAndStart(Z)V
    .locals 3

    .line 84
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosTutorialApi;->posContainer:Lcom/squareup/ui/main/PosContainer;

    iget-object v1, p0, Lcom/squareup/register/tutorial/PosTutorialApi;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    sget-object v2, Lcom/squareup/orderentry/OrderEntryMode;->LAST_SELECTED:Lcom/squareup/orderentry/OrderEntryMode;

    invoke-interface {v1, v2}, Lcom/squareup/orderentry/OrderEntryAppletGateway;->historyFactoryForMode(Lcom/squareup/orderentry/OrderEntryMode;)Lcom/squareup/ui/main/HistoryFactory;

    move-result-object v1

    sget-object v2, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-interface {v0, v1, v2}, Lcom/squareup/ui/main/PosContainer;->resetHistory(Lcom/squareup/ui/main/HistoryFactory;Lflow/Direction;)V

    .line 86
    invoke-direct {p0, p1}, Lcom/squareup/register/tutorial/PosTutorialApi;->startTutorial(Z)V

    return-void
.end method

.method private shouldShowCardTutorial()Z
    .locals 1

    .line 123
    invoke-direct {p0}, Lcom/squareup/register/tutorial/PosTutorialApi;->acceptsCards()Z

    move-result v0

    return v0
.end method

.method private shouldShowCashTutorial()Z
    .locals 1

    .line 127
    invoke-direct {p0}, Lcom/squareup/register/tutorial/PosTutorialApi;->shouldShowCardTutorial()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private startFirstCardTutorial(Z)V
    .locals 2

    .line 99
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosTutorialApi;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->FIRST_PAYMENT_TUTORIAL_V2:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosTutorialApi;->v2Creator:Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;

    invoke-virtual {v0, p1}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;->ready(Z)V

    goto :goto_0

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosTutorialApi;->cardTutorial:Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;

    invoke-direct {p0, v0, p1}, Lcom/squareup/register/tutorial/PosTutorialApi;->startFirstPaymentTutorial(Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;Z)V

    :goto_0
    return-void
.end method

.method private startFirstPaymentTutorial(Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;Z)V
    .locals 1

    .line 108
    new-instance v0, Lcom/squareup/register/tutorial/-$$Lambda$PosTutorialApi$H6O1FY_YEdxtgCmWgAJEGjaeXmU;

    invoke-direct {v0, p0, p1}, Lcom/squareup/register/tutorial/-$$Lambda$PosTutorialApi$H6O1FY_YEdxtgCmWgAJEGjaeXmU;-><init>(Lcom/squareup/register/tutorial/PosTutorialApi;Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;)V

    invoke-virtual {p1, v0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->setOnEndAction(Ljava/lang/Runnable;)V

    .line 109
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosTutorialApi;->placeholderCreator:Lcom/squareup/register/tutorial/PlaceholderTutorial$Creator;

    invoke-virtual {v0, p2}, Lcom/squareup/register/tutorial/PlaceholderTutorial$Creator;->ready(Z)V

    .line 110
    invoke-virtual {p1}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->forceStart()V

    return-void
.end method

.method private startTutorial(Z)V
    .locals 1

    .line 90
    invoke-direct {p0}, Lcom/squareup/register/tutorial/PosTutorialApi;->acceptsCards()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    invoke-direct {p0, p1}, Lcom/squareup/register/tutorial/PosTutorialApi;->startFirstCardTutorial(Z)V

    goto :goto_0

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosTutorialApi;->cashTutorial:Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;

    invoke-direct {p0, v0, p1}, Lcom/squareup/register/tutorial/PosTutorialApi;->startFirstPaymentTutorial(Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;Z)V

    :goto_0
    return-void
.end method


# virtual methods
.method public forceStartFirstPaymentTutorial()V
    .locals 1

    const/4 v0, 0x0

    .line 50
    invoke-direct {p0, v0}, Lcom/squareup/register/tutorial/PosTutorialApi;->resetAndStart(Z)V

    return-void
.end method

.method public synthetic lambda$startFirstPaymentTutorial$0$PosTutorialApi(Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;)V
    .locals 0

    .line 108
    invoke-direct {p0, p1}, Lcom/squareup/register/tutorial/PosTutorialApi;->onEndFirstPaymentTutorial(Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;)V

    return-void
.end method

.method public maybeAutoStartFirstPaymentTutorial()V
    .locals 1

    .line 65
    invoke-direct {p0}, Lcom/squareup/register/tutorial/PosTutorialApi;->shouldShowCardTutorial()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/register/tutorial/PosTutorialApi;->cardTutorial:Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;

    invoke-virtual {v0}, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->isTriggered()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 70
    :cond_0
    invoke-direct {p0}, Lcom/squareup/register/tutorial/PosTutorialApi;->shouldShowCashTutorial()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/register/tutorial/PosTutorialApi;->cashTutorial:Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;

    invoke-virtual {v0}, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->isTriggered()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    const/4 v0, 0x1

    .line 74
    invoke-direct {p0, v0}, Lcom/squareup/register/tutorial/PosTutorialApi;->resetAndStart(Z)V

    return-void
.end method

.method public maybeStartFirstPaymentTutorialForDeepLink()Lcom/squareup/ui/main/HistoryFactory;
    .locals 2

    const/4 v0, 0x0

    .line 78
    invoke-direct {p0, v0}, Lcom/squareup/register/tutorial/PosTutorialApi;->startTutorial(Z)V

    .line 80
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosTutorialApi;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    sget-object v1, Lcom/squareup/orderentry/OrderEntryMode;->LAST_SELECTED:Lcom/squareup/orderentry/OrderEntryMode;

    invoke-interface {v0, v1}, Lcom/squareup/orderentry/OrderEntryAppletGateway;->historyFactoryForMode(Lcom/squareup/orderentry/OrderEntryMode;)Lcom/squareup/ui/main/HistoryFactory;

    move-result-object v0

    return-object v0
.end method

.method public onFinishedReceipt()V
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosTutorialApi;->cardTutorial:Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;

    invoke-virtual {v0}, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->onFinishedReceipt()V

    return-void
.end method
