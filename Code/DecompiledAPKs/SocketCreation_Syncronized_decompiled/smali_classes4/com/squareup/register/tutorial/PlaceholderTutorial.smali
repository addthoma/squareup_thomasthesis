.class public final Lcom/squareup/register/tutorial/PlaceholderTutorial;
.super Ljava/lang/Object;
.source "PlaceholderTutorial.kt"

# interfaces
.implements Lcom/squareup/tutorialv2/Tutorial;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/tutorial/PlaceholderTutorial$Creator;,
        Lcom/squareup/register/tutorial/PlaceholderTutorial$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u00172\u00020\u0001:\u0002\u0017\u0018B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016J\u0008\u0010\u000b\u001a\u00020\u0008H\u0016J\u0008\u0010\u000c\u001a\u00020\u0008H\u0016J\u001a\u0010\r\u001a\u00020\u00082\u0006\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u0016J\u0018\u0010\u0012\u001a\u00020\u00082\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u000e\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0016H\u0016R\u001c\u0010\u0003\u001a\u0010\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/register/tutorial/PlaceholderTutorial;",
        "Lcom/squareup/tutorialv2/Tutorial;",
        "()V",
        "state",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/tutorialv2/TutorialState;",
        "kotlin.jvm.PlatformType",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitRequested",
        "onExitScope",
        "onTutorialEvent",
        "name",
        "",
        "value",
        "",
        "onTutorialPendingActionEvent",
        "pendingAction",
        "Lcom/squareup/tutorialv2/TutorialCore$PendingAction;",
        "tutorialState",
        "Lio/reactivex/Observable;",
        "Companion",
        "Creator",
        "pos-tutorials_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/register/tutorial/PlaceholderTutorial$Companion;

.field public static final EXIT:Ljava/lang/String; = "Exit placeholder tutorial"


# instance fields
.field private final state:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/tutorialv2/TutorialState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/register/tutorial/PlaceholderTutorial$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/register/tutorial/PlaceholderTutorial$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/register/tutorial/PlaceholderTutorial;->Companion:Lcom/squareup/register/tutorial/PlaceholderTutorial$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    invoke-static {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    const-string v1, "BehaviorRelay.createDefault(TutorialState.CLEAR)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/register/tutorial/PlaceholderTutorial;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onExitRequested()V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onTutorialEvent(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    const-string p2, "name"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "Exit placeholder tutorial"

    .line 29
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 30
    iget-object p1, p0, Lcom/squareup/register/tutorial/PlaceholderTutorial;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object p2, Lcom/squareup/tutorialv2/TutorialState;->FINISHED:Lcom/squareup/tutorialv2/TutorialState;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public onTutorialPendingActionEvent(Ljava/lang/String;Lcom/squareup/tutorialv2/TutorialCore$PendingAction;)V
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "pendingAction"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public tutorialState()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/tutorialv2/TutorialState;",
            ">;"
        }
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/squareup/register/tutorial/PlaceholderTutorial;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method
