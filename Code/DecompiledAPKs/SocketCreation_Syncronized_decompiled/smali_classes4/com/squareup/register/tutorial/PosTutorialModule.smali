.class public abstract Lcom/squareup/register/tutorial/PosTutorialModule;
.super Ljava/lang/Object;
.source "PosTutorialModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideScreenEmitter(Ldagger/Lazy;)Lcom/squareup/register/tutorial/TutorialScreenEmitter;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;)",
            "Lcom/squareup/register/tutorial/TutorialScreenEmitter;"
        }
    .end annotation

    .line 19
    new-instance v0, Lcom/squareup/register/tutorial/PosTutorialScreenEmitter;

    invoke-direct {v0, p0}, Lcom/squareup/register/tutorial/PosTutorialScreenEmitter;-><init>(Ldagger/Lazy;)V

    return-object v0
.end method


# virtual methods
.method abstract bindAppletWatcher(Lcom/squareup/register/tutorial/TutorialAppletWatcher;)Lmortar/Scoped;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method
