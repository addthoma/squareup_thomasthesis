.class public Lcom/squareup/register/tutorial/TutorialPhrases;
.super Ljava/lang/Object;
.source "TutorialPhrases.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addMediumWeight(Landroid/content/Context;Lcom/squareup/util/Res;ILjava/lang/String;I)Lcom/squareup/phrase/Phrase;
    .locals 1

    .line 17
    sget-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {p0, v0}, Lcom/squareup/marketfont/MarketSpanKt;->marketSpanFor(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/fonts/FontSpan;

    move-result-object p0

    .line 18
    invoke-interface {p1, p4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p4

    invoke-static {p4, p0}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object p0

    .line 19
    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    invoke-virtual {p1, p3, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    return-object p0
.end method
