.class public Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;
.super Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;
.source "FirstPaymentCardTutorial.java"


# instance fields
.field private final bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

.field private final bankLinkingStarter:Lcom/squareup/banklinking/BankLinkingStarter;

.field private canSeeRatesTour:Z

.field private final cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

.field private final disposables:Lio/reactivex/disposables/CompositeDisposable;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final feeTutorial:Lcom/squareup/feetutorial/FeeTutorial;

.field private hasSeenRatesTour:Z

.field private latestCapabilities:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
            ">;"
        }
    .end annotation
.end field

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;


# direct methods
.method public constructor <init>(Lcom/squareup/accountfreeze/AccountFreeze;Landroid/app/Application;Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/payment/pending/PendingTransactionsStore;Lcom/squareup/badbus/BadBus;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;Lcom/squareup/register/tutorial/V1TutorialHomeIdentifier;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/LocalSetting;Lio/reactivex/Scheduler;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/ui/main/CheckoutWorkflowRunner;Lcom/squareup/settings/server/Features;Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/banklinking/BankLinkingStarter;Lcom/squareup/feetutorial/FeeTutorial;)V
    .locals 24
    .param p17    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/accountfreeze/AccountFreeze;",
            "Landroid/app/Application;",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/orderentry/pages/OrderEntryPages;",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            "Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;",
            "Lcom/squareup/register/tutorial/V1TutorialHomeIdentifier;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/FirstPaymentTooltipStatus;",
            ">;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            "Lcom/squareup/ui/main/CheckoutWorkflowRunner;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            "Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            "Lcom/squareup/banklinking/BankLinkingStarter;",
            "Lcom/squareup/feetutorial/FeeTutorial;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v15, p0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v19, p6

    move-object/from16 v9, p7

    move-object/from16 v4, p9

    move-object/from16 v8, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v10, p15

    move-object/from16 v18, p16

    move-object/from16 v20, p19

    move-object/from16 v21, p21

    move-object/from16 v22, p23

    .line 107
    invoke-interface/range {p8 .. p8}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v7

    sget-object v16, Lcom/squareup/analytics/RegisterActionName;->PAYMENT_TUTORIAL_DECLINED:Lcom/squareup/analytics/RegisterActionName;

    move-object/from16 v15, v16

    sget-object v16, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_TUTORIAL_DECLINE_DIALOG:Lcom/squareup/analytics/RegisterViewName;

    sget-object v17, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_TUTORIAL_FINISH_DIALOG:Lcom/squareup/analytics/RegisterViewName;

    .line 111
    invoke-interface/range {p24 .. p24}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result v23

    .line 106
    invoke-direct/range {v0 .. v23}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;-><init>(Lcom/squareup/accountfreeze/AccountFreeze;Landroid/app/Application;Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/payment/pending/PendingTransactionsStore;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tickets/OpenTicketsSettings;ZLcom/squareup/badbus/BadBus;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;Lcom/squareup/register/tutorial/V1TutorialHomeIdentifier;Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/settings/LocalSetting;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/CheckoutWorkflowRunner;Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;Z)V

    .line 71
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 73
    const-class v0, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    .line 74
    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, v1, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->latestCapabilities:Ljava/util/EnumSet;

    move-object/from16 v0, p4

    .line 112
    iput-object v0, v1, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object/from16 v0, p17

    .line 113
    iput-object v0, v1, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->mainScheduler:Lio/reactivex/Scheduler;

    move-object/from16 v0, p18

    .line 114
    iput-object v0, v1, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    move-object/from16 v0, p20

    .line 115
    iput-object v0, v1, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->features:Lcom/squareup/settings/server/Features;

    move-object/from16 v0, p21

    .line 116
    iput-object v0, v1, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    move-object/from16 v0, p22

    .line 117
    iput-object v0, v1, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    move-object/from16 v0, p25

    .line 118
    iput-object v0, v1, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->bankLinkingStarter:Lcom/squareup/banklinking/BankLinkingStarter;

    move-object/from16 v0, p26

    .line 119
    iput-object v0, v1, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->feeTutorial:Lcom/squareup/feetutorial/FeeTutorial;

    return-void
.end method

.method private fromBankSettingsState(Lcom/squareup/banklinking/BankAccountSettings$State;)Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$FirstPaymentTutorialPrompt;
    .locals 3

    .line 201
    invoke-virtual {p1}, Lcom/squareup/banklinking/BankAccountSettings$State;->isSuccessful()Z

    move-result v0

    if-nez v0, :cond_0

    .line 202
    new-instance p1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$EndTutorialPrompt;

    sget v0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_end_content_verified:I

    invoke-direct {p1, v0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$EndTutorialPrompt;-><init>(I)V

    return-object p1

    .line 204
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/banklinking/BankAccountSettings$State;->showLinkBankAccount()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 205
    new-instance p1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$EndTutorialActionPrompt;

    sget v0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_end_content_no_bank:I

    sget v1, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_end_link_bank_account:I

    invoke-direct {p1, v0, v1}, Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$EndTutorialActionPrompt;-><init>(II)V

    return-object p1

    .line 208
    :cond_1
    sget-object v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial$1;->$SwitchMap$com$squareup$protos$client$bankaccount$BankAccountVerificationState:[I

    invoke-virtual {p1}, Lcom/squareup/banklinking/BankAccountSettings$State;->verificationState()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 222
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown bank account verification state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    invoke-virtual {p1}, Lcom/squareup/banklinking/BankAccountSettings$State;->verificationState()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 219
    :pswitch_0
    new-instance p1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$EndTutorialPrompt;

    sget v0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_end_content_failed_verify:I

    invoke-direct {p1, v0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$EndTutorialPrompt;-><init>(I)V

    return-object p1

    .line 215
    :pswitch_1
    new-instance p1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$EndTutorialPrompt;

    sget v0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_end_content_pending:I

    invoke-direct {p1, v0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$EndTutorialPrompt;-><init>(I)V

    return-object p1

    .line 210
    :pswitch_2
    new-instance p1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$EndTutorialPrompt;

    sget v0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_end_content_verified:I

    invoke-direct {p1, v0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$EndTutorialPrompt;-><init>(I)V

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private getContent(Lcom/squareup/payment/CardOptionEnabled;)Ljava/lang/CharSequence;
    .locals 1

    .line 252
    sget-object v0, Lcom/squareup/payment/CardOptionEnabled$ShouldBeDisabled$BelowMin;->INSTANCE:Lcom/squareup/payment/CardOptionEnabled$ShouldBeDisabled$BelowMin;

    if-ne p1, v0, :cond_0

    .line 253
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    sget v0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_charge_more:I

    invoke-virtual {p1, v0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->withMinimum(I)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    .line 254
    :cond_0
    sget-object v0, Lcom/squareup/payment/CardOptionEnabled$ShouldBeDisabled$AboveMax;->INSTANCE:Lcom/squareup/payment/CardOptionEnabled$ShouldBeDisabled$AboveMax;

    if-ne p1, v0, :cond_1

    .line 255
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    sget v0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_charge_less:I

    invoke-virtual {p1, v0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->withMaximum(I)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    .line 257
    :cond_1
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->latestCapabilities:Ljava/util/EnumSet;

    invoke-virtual {p1, v0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getPaymentTypeScreenText(Ljava/util/EnumSet;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method public static synthetic lambda$jF-OQPMjIAREq3jlycj6zxRe8a8(Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;Lcom/squareup/banklinking/BankAccountSettings$State;)Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$FirstPaymentTutorialPrompt;
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->fromBankSettingsState(Lcom/squareup/banklinking/BankAccountSettings$State;)Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$FirstPaymentTutorialPrompt;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public bridge synthetic clearOnEndAction()V
    .locals 0

    .line 60
    invoke-super {p0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->clearOnEndAction()V

    return-void
.end method

.method public completeButtonAction()V
    .locals 1

    .line 152
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->bankLinkingStarter:Lcom/squareup/banklinking/BankLinkingStarter;

    invoke-interface {v0}, Lcom/squareup/banklinking/BankLinkingStarter;->maybeStartBankLinking()V

    return-void
.end method

.method public forceStart()V
    .locals 1

    .line 172
    invoke-super {p0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->forceStart()V

    const/4 v0, 0x0

    .line 173
    iput-boolean v0, p0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->hasSeenRatesTour:Z

    return-void
.end method

.method protected getFinishPrompt()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/register/tutorial/TutorialDialog$Prompt;",
            ">;"
        }
    .end annotation

    .line 195
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    invoke-interface {v0}, Lcom/squareup/banklinking/BankAccountSettings;->state()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/register/tutorial/-$$Lambda$FirstPaymentCardTutorial$jF-OQPMjIAREq3jlycj6zxRe8a8;

    invoke-direct {v1, p0}, Lcom/squareup/register/tutorial/-$$Lambda$FirstPaymentCardTutorial$jF-OQPMjIAREq3jlycj6zxRe8a8;-><init>(Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic handlePromptTap(Lcom/squareup/register/tutorial/TutorialDialog$Prompt;Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;)V
    .locals 0

    .line 60
    invoke-super {p0, p1, p2}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->handlePromptTap(Lcom/squareup/register/tutorial/TutorialDialog$Prompt;Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;)V

    return-void
.end method

.method public isTriggered()Z
    .locals 2

    .line 147
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->FIRST_PAYMENT_TUTORIAL_V2:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->isTriggered()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onEnd()V
    .locals 1

    .line 142
    invoke-super {p0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->onEnd()V

    .line 143
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 123
    invoke-super {p0, p1}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->onEnterScope(Lmortar/MortarScope;)V

    .line 125
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->feeTutorial:Lcom/squareup/feetutorial/FeeTutorial;

    invoke-interface {p1}, Lcom/squareup/feetutorial/FeeTutorial;->getCanShow()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->canSeeRatesTour:Z

    return-void
.end method

.method public onExitScope()V
    .locals 0

    .line 129
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->onEnd()V

    return-void
.end method

.method public bridge synthetic onFinishedReceipt()V
    .locals 0

    .line 60
    invoke-super {p0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->onFinishedReceipt()V

    return-void
.end method

.method protected onHandlePaymentScreen(Lcom/squareup/container/ContainerTreeKey;)Z
    .locals 2

    .line 233
    invoke-virtual {p0, p1}, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->isPaymentTypeScreen(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 234
    sget-object p1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_3_plug_in_or_swipe:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->cardOption()Lcom/squareup/payment/CardOptionEnabled;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->getContent(Lcom/squareup/payment/CardOptionEnabled;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->setContent(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;Ljava/lang/CharSequence;)V

    return v1

    .line 237
    :cond_0
    instance-of v0, p1, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreen;

    if-eqz v0, :cond_1

    .line 239
    sget-object p1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_3a_enter_card_info:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    invoke-virtual {v0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getCardEntryScreenText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->setContent(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;Ljava/lang/CharSequence;)V

    return v1

    .line 242
    :cond_1
    instance-of p1, p1, Lcom/squareup/invoices/edit/EditInvoiceScreen;

    if-eqz p1, :cond_2

    .line 243
    sget-object p1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_3a_enter_invoice_info:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    invoke-virtual {v0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getInvoiceScreenText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->setContent(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;Ljava/lang/CharSequence;)V

    return v1

    :cond_2
    const/4 p1, 0x0

    return p1
.end method

.method protected onHomeUpdate()V
    .locals 2

    .line 179
    iget-boolean v0, p0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->canSeeRatesTour:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->hasSeenRatesTour:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 180
    iput-boolean v0, p0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->hasSeenRatesTour:Z

    .line 181
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->feeTutorial:Lcom/squareup/feetutorial/FeeTutorial;

    invoke-interface {v0}, Lcom/squareup/feetutorial/FeeTutorial;->activate()V

    return-void

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->cardOptionShouldBeEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 188
    sget-object v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_2_tap_charge:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    iget-object v1, p0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    invoke-virtual {v1}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getTapChargeText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->setContent(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 190
    :cond_1
    sget-object v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_1_start:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    iget-object v1, p0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    invoke-virtual {v1}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getStartTextWithMinimum()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->setContent(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method onNewReaderCapabilities(Ljava/util/EnumSet;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
            ">;)V"
        }
    .end annotation

    .line 157
    iput-object p1, p0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->latestCapabilities:Ljava/util/EnumSet;

    .line 159
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->getLastNonFlowScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    .line 160
    invoke-virtual {p0, p1}, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->isPaymentTypeScreen(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    invoke-virtual {p0, p1}, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->onShowScreenWhileRunning(Lcom/squareup/container/ContainerTreeKey;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic onRequestExitTutorial()V
    .locals 0

    .line 60
    invoke-super {p0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->onRequestExitTutorial()V

    return-void
.end method

.method public bridge synthetic onShowScreen(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 0

    .line 60
    invoke-super {p0, p1}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->onShowScreen(Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method public bridge synthetic onShowingThanks()V
    .locals 0

    .line 60
    invoke-super {p0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->onShowingThanks()V

    return-void
.end method

.method public onStart()V
    .locals 3

    const/4 v0, 0x0

    .line 133
    iput-boolean v0, p0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->hasSeenRatesTour:Z

    .line 134
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerStates()Lrx/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->features:Lcom/squareup/settings/server/Features;

    .line 135
    invoke-static {v2}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracleFilters;->asCapabilities(Lcom/squareup/settings/server/Features;)Lrx/Observable$Transformer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v1

    .line 134
    invoke-static {v1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->mainScheduler:Lio/reactivex/Scheduler;

    .line 137
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/register/tutorial/-$$Lambda$SKEaJ1Cdfis4Dk1o2TayGDQbvSU;

    invoke-direct {v2, p0}, Lcom/squareup/register/tutorial/-$$Lambda$SKEaJ1Cdfis4Dk1o2TayGDQbvSU;-><init>(Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;)V

    .line 138
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 134
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method public bridge synthetic setOnEndAction(Ljava/lang/Runnable;)V
    .locals 0

    .line 60
    invoke-super {p0, p1}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->setOnEndAction(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected shouldTrigger()Z
    .locals 2

    .line 166
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->eligibleForSquareCardProcessing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->FIRST_PAYMENT_TUTORIAL_V2:Lcom/squareup/settings/server/Features$Feature;

    .line 167
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
