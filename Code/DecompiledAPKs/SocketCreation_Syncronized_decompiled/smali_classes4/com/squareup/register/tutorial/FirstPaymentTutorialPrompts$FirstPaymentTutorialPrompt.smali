.class abstract Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$FirstPaymentTutorialPrompt;
.super Ljava/lang/Object;
.source "FirstPaymentTutorialPrompts.java"

# interfaces
.implements Lcom/squareup/register/tutorial/TutorialDialog$Prompt;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "FirstPaymentTutorialPrompt"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getLayoutResId()I
    .locals 1

    .line 18
    sget v0, Lcom/squareup/common/tutorial/R$layout;->tutorial_dialog:I

    return v0
.end method

.method abstract handleTap(Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;)V
.end method
