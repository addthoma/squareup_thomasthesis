.class public final Lcom/squareup/receipt/PrintedReceiptSettings;
.super Ljava/lang/Object;
.source "PrintedReceiptSettings.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/receipt/PrintedReceiptSettings;",
        "",
        "()V",
        "tenderRequiresPrintedReceipt",
        "",
        "tender",
        "Lcom/squareup/payment/tender/BaseTender;",
        "print_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/receipt/PrintedReceiptSettings;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7
    new-instance v0, Lcom/squareup/receipt/PrintedReceiptSettings;

    invoke-direct {v0}, Lcom/squareup/receipt/PrintedReceiptSettings;-><init>()V

    sput-object v0, Lcom/squareup/receipt/PrintedReceiptSettings;->INSTANCE:Lcom/squareup/receipt/PrintedReceiptSettings;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final tenderRequiresPrintedReceipt(Lcom/squareup/payment/tender/BaseTender;)Z
    .locals 4

    const-string v0, "tender"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    instance-of v0, p1, Lcom/squareup/payment/AcceptsTips;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object v0, v1

    goto :goto_0

    :cond_0
    move-object v0, p1

    :goto_0
    check-cast v0, Lcom/squareup/payment/AcceptsTips;

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/squareup/payment/AcceptsTips;->tipOnPrintedReceipt()Z

    move-result v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    .line 11
    :goto_1
    instance-of v3, p1, Lcom/squareup/payment/AcceptsSignature;

    if-nez v3, :cond_2

    move-object p1, v1

    :cond_2
    check-cast p1, Lcom/squareup/payment/AcceptsSignature;

    if-eqz p1, :cond_3

    invoke-interface {p1}, Lcom/squareup/payment/AcceptsSignature;->signOnPrintedReceipt()Z

    move-result p1

    goto :goto_2

    :cond_3
    const/4 p1, 0x0

    :goto_2
    if-nez v0, :cond_4

    if-eqz p1, :cond_5

    :cond_4
    const/4 v2, 0x1

    :cond_5
    return v2
.end method
