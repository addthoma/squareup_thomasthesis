.class final Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;
.super Ljava/lang/Object;
.source "RealPermissionWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/Worker;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/permissionworkflow/RealPermissionWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "PermissionWorker"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/Worker<",
        "Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorkerResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u001d\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0014\u0010\n\u001a\u00020\u000b2\n\u0010\u000c\u001a\u0006\u0012\u0002\u0008\u00030\u0001H\u0016J\u000e\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u000eH\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;",
        "Lcom/squareup/workflow/Worker;",
        "Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorkerResult;",
        "permissionGatekeeper",
        "Lcom/squareup/permissions/PermissionGatekeeper;",
        "passcodeEmployeeManagement",
        "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
        "permissionRequestInput",
        "Lcom/squareup/permissionworkflow/PermissionRequestInput;",
        "(Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissionworkflow/PermissionRequestInput;)V",
        "doesSameWorkAs",
        "",
        "otherWorker",
        "run",
        "Lkotlinx/coroutines/flow/Flow;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final permissionRequestInput:Lcom/squareup/permissionworkflow/PermissionRequestInput;


# direct methods
.method public constructor <init>(Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissionworkflow/PermissionRequestInput;)V
    .locals 1

    const-string v0, "permissionGatekeeper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "passcodeEmployeeManagement"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "permissionRequestInput"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    iput-object p2, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iput-object p3, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;->permissionRequestInput:Lcom/squareup/permissionworkflow/PermissionRequestInput;

    return-void
.end method

.method public static final synthetic access$getPasscodeEmployeeManagement$p(Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;)Lcom/squareup/permissions/PasscodeEmployeeManagement;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    return-object p0
.end method

.method public static final synthetic access$getPermissionGatekeeper$p(Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;)Lcom/squareup/permissions/PermissionGatekeeper;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    return-object p0
.end method

.method public static final synthetic access$getPermissionRequestInput$p(Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;)Lcom/squareup/permissionworkflow/PermissionRequestInput;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;->permissionRequestInput:Lcom/squareup/permissionworkflow/PermissionRequestInput;

    return-object p0
.end method


# virtual methods
.method public doesSameWorkAs(Lcom/squareup/workflow/Worker;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Worker<",
            "*>;)Z"
        }
    .end annotation

    const-string v0, "otherWorker"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    instance-of v0, p1, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 56
    :cond_0
    check-cast p1, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;

    iget-object p1, p1, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;->permissionRequestInput:Lcom/squareup/permissionworkflow/PermissionRequestInput;

    invoke-virtual {p1}, Lcom/squareup/permissionworkflow/PermissionRequestInput;->getPermission()Lcom/squareup/permissions/Permission;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;->permissionRequestInput:Lcom/squareup/permissionworkflow/PermissionRequestInput;

    invoke-virtual {v0}, Lcom/squareup/permissionworkflow/PermissionRequestInput;->getPermission()Lcom/squareup/permissions/Permission;

    move-result-object v0

    if-ne p1, v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public run()Lkotlinx/coroutines/flow/Flow;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlinx/coroutines/flow/Flow<",
            "Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorkerResult;",
            ">;"
        }
    .end annotation

    .line 59
    new-instance v0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;-><init>(Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->flow(Lkotlin/jvm/functions/Function2;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    return-object v0
.end method
