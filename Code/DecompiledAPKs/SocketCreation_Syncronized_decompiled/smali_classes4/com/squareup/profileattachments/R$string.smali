.class public final Lcom/squareup/profileattachments/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/profileattachments/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final crm_profile_attachments_delete:I = 0x7f120731

.field public static final crm_profile_attachments_delete_cancelled:I = 0x7f120732

.field public static final crm_profile_attachments_delete_file_fallback_name:I = 0x7f120733

.field public static final crm_profile_attachments_delete_file_message:I = 0x7f120734

.field public static final crm_profile_attachments_delete_file_title:I = 0x7f120735

.field public static final crm_profile_attachments_delete_success:I = 0x7f120736

.field public static final crm_profile_attachments_download:I = 0x7f120737

.field public static final crm_profile_attachments_general_failure:I = 0x7f120739

.field public static final crm_profile_attachments_header:I = 0x7f12073a

.field public static final crm_profile_attachments_options:I = 0x7f12073c

.field public static final crm_profile_attachments_rename:I = 0x7f12073d

.field public static final crm_profile_attachments_rename_file_cancelled:I = 0x7f12073e

.field public static final crm_profile_attachments_rename_file_hint:I = 0x7f12073f

.field public static final crm_profile_attachments_rename_file_message:I = 0x7f120740

.field public static final crm_profile_attachments_rename_file_title:I = 0x7f120741

.field public static final crm_profile_attachments_take_a_photo:I = 0x7f120742

.field public static final crm_profile_attachments_upload_failed:I = 0x7f120743

.field public static final crm_profile_attachments_upload_in_progress:I = 0x7f120745

.field public static final crm_profile_attachments_upload_photo:I = 0x7f120746

.field public static final crm_profile_attachments_upload_succeeded:I = 0x7f120747

.field public static final open_file:I = 0x7f121164

.field public static final upload:I = 0x7f121af8


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
