.class public final Lcom/squareup/referrals/ReferralCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ReferralCoordinator.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private balanceAmountView:Lcom/squareup/widgets/MessageView;

.field private balanceView:Landroid/widget/LinearLayout;

.field private final device:Lcom/squareup/util/Device;

.field private dueDateView:Lcom/squareup/widgets/MessageView;

.field private glyphView:Lcom/squareup/glyph/SquareGlyphView;

.field private inviteFriendsButton:Landroid/widget/Button;

.field private laterButton:Landroid/widget/Button;

.field private messageView:Lcom/squareup/widgets/MessageView;

.field private referralData:Lcom/squareup/referrals/ReferralRunner$ReferralData;

.field private final referralRunner:Lcom/squareup/referrals/ReferralRunner;

.field private final res:Lcom/squareup/util/Res;

.field private rewardsCopyExperiment:Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;

.field private final spinner:Lcom/squareup/register/widgets/GlassSpinner;

.field private titleView:Lcom/squareup/widgets/MessageView;

.field private urlView:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/referrals/ReferralRunner;Lcom/squareup/util/Device;Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;Lcom/squareup/register/widgets/GlassSpinner;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 53
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/squareup/referrals/ReferralCoordinator;->res:Lcom/squareup/util/Res;

    .line 55
    iput-object p2, p0, Lcom/squareup/referrals/ReferralCoordinator;->referralRunner:Lcom/squareup/referrals/ReferralRunner;

    .line 56
    iput-object p3, p0, Lcom/squareup/referrals/ReferralCoordinator;->device:Lcom/squareup/util/Device;

    .line 57
    iput-object p4, p0, Lcom/squareup/referrals/ReferralCoordinator;->rewardsCopyExperiment:Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;

    .line 58
    iput-object p5, p0, Lcom/squareup/referrals/ReferralCoordinator;->spinner:Lcom/squareup/register/widgets/GlassSpinner;

    return-void
.end method

.method private actionBarWithUp(Landroid/view/View;)V
    .locals 3

    .line 111
    iget-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v1, p0, Lcom/squareup/referrals/ReferralCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/referrals/R$string;->referral_get_free_processing:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)V

    .line 112
    iget-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v1, p0, Lcom/squareup/referrals/ReferralCoordinator;->referralRunner:Lcom/squareup/referrals/ReferralRunner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/referrals/-$$Lambda$6UjFQs5wDF7kWIQzv7oHie-0idA;

    invoke-direct {v2, v1}, Lcom/squareup/referrals/-$$Lambda$6UjFQs5wDF7kWIQzv7oHie-0idA;-><init>(Lcom/squareup/referrals/ReferralRunner;)V

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 114
    new-instance v0, Lcom/squareup/referrals/-$$Lambda$ReferralCoordinator$LJGblgwHiWcUcc2kk3SbwdCMRmo;

    invoke-direct {v0, p0}, Lcom/squareup/referrals/-$$Lambda$ReferralCoordinator$LJGblgwHiWcUcc2kk3SbwdCMRmo;-><init>(Lcom/squareup/referrals/ReferralCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private actionBarWithoutUp(Landroid/view/View;)V
    .locals 3

    .line 101
    iget-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v1, p0, Lcom/squareup/referrals/ReferralCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/referrals/R$string;->referral_get_free_processing:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setTitleNoUpButton(Ljava/lang/CharSequence;)V

    .line 103
    new-instance v0, Lcom/squareup/referrals/-$$Lambda$ReferralCoordinator$ZeDQ0bjtzwpLXNejNNOQBMX6xq0;

    invoke-direct {v0, p0}, Lcom/squareup/referrals/-$$Lambda$ReferralCoordinator$ZeDQ0bjtzwpLXNejNNOQBMX6xq0;-><init>(Lcom/squareup/referrals/ReferralCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 159
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    .line 160
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 161
    sget v0, Lcom/squareup/referrals/R$id;->referral_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator;->inviteFriendsButton:Landroid/widget/Button;

    .line 162
    sget v0, Lcom/squareup/referrals/R$id;->later_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator;->laterButton:Landroid/widget/Button;

    .line 163
    sget v0, Lcom/squareup/referrals/R$id;->referral_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator;->messageView:Lcom/squareup/widgets/MessageView;

    .line 164
    sget v0, Lcom/squareup/referrals/R$id;->referral_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator;->titleView:Lcom/squareup/widgets/MessageView;

    .line 165
    sget v0, Lcom/squareup/referrals/R$id;->balance_due_date:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator;->dueDateView:Lcom/squareup/widgets/MessageView;

    .line 166
    sget v0, Lcom/squareup/referrals/R$id;->balance_amount:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator;->balanceAmountView:Lcom/squareup/widgets/MessageView;

    .line 167
    sget v0, Lcom/squareup/referrals/R$id;->balance:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator;->balanceView:Landroid/widget/LinearLayout;

    .line 168
    sget v0, Lcom/squareup/referrals/R$id;->referral_glyph:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    .line 169
    sget v0, Lcom/squareup/referrals/R$id;->referral_link:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/referrals/ReferralCoordinator;->urlView:Landroid/widget/TextView;

    return-void
.end method

.method private onReferralTapped()V
    .locals 3

    .line 155
    iget-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator;->referralRunner:Lcom/squareup/referrals/ReferralRunner;

    iget-object v1, p0, Lcom/squareup/referrals/ReferralCoordinator;->urlView:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/squareup/util/Views;->getActivity(Landroid/view/View;)Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/referrals/ReferralCoordinator;->referralData:Lcom/squareup/referrals/ReferralRunner$ReferralData;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/referrals/ReferralRunner;->launchReferralIntent(Landroid/app/Activity;Lcom/squareup/referrals/ReferralRunner$ReferralData;)V

    return-void
.end method

.method private setBalanceView(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 2

    .line 147
    iget-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 148
    iget-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator;->titleView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 149
    iget-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator;->balanceView:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 150
    iget-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator;->dueDateView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 151
    iget-object p1, p0, Lcom/squareup/referrals/ReferralCoordinator;->balanceAmountView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private setData(Lcom/squareup/referrals/ReferralRunner$ReferralData;Z)V
    .locals 1

    .line 122
    iput-object p1, p0, Lcom/squareup/referrals/ReferralCoordinator;->referralData:Lcom/squareup/referrals/ReferralRunner$ReferralData;

    .line 123
    invoke-virtual {p1}, Lcom/squareup/referrals/ReferralRunner$ReferralData;->hasBalance()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    iget-object p2, p1, Lcom/squareup/referrals/ReferralRunner$ReferralData;->expiresAt:Ljava/lang/CharSequence;

    iget-object v0, p1, Lcom/squareup/referrals/ReferralRunner$ReferralData;->balance:Ljava/lang/CharSequence;

    invoke-direct {p0, p2, v0}, Lcom/squareup/referrals/ReferralCoordinator;->setBalanceView(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 126
    :cond_0
    invoke-direct {p0, p2}, Lcom/squareup/referrals/ReferralCoordinator;->setNoBalanceView(Z)V

    .line 129
    :goto_0
    iget-object p2, p0, Lcom/squareup/referrals/ReferralCoordinator;->titleView:Lcom/squareup/widgets/MessageView;

    iget-object v0, p1, Lcom/squareup/referrals/ReferralRunner$ReferralData;->freeProcessingTitle:Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    iget-object p2, p0, Lcom/squareup/referrals/ReferralCoordinator;->messageView:Lcom/squareup/widgets/MessageView;

    iget-object v0, p1, Lcom/squareup/referrals/ReferralRunner$ReferralData;->freeProcessingNote:Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object p2, p0, Lcom/squareup/referrals/ReferralCoordinator;->inviteFriendsButton:Landroid/widget/Button;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 133
    iget-object p2, p0, Lcom/squareup/referrals/ReferralCoordinator;->urlView:Landroid/widget/TextView;

    iget-object p1, p1, Lcom/squareup/referrals/ReferralRunner$ReferralData;->prettyUrl:Ljava/lang/String;

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private setNoBalanceView(Z)V
    .locals 2

    .line 137
    iget-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator;->balanceView:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 139
    iget-object p1, p0, Lcom/squareup/referrals/ReferralCoordinator;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p1, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    goto :goto_0

    .line 141
    :cond_0
    iget-object p1, p0, Lcom/squareup/referrals/ReferralCoordinator;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 143
    :goto_0
    iget-object p1, p0, Lcom/squareup/referrals/ReferralCoordinator;->titleView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    .line 62
    invoke-direct {p0, p1}, Lcom/squareup/referrals/ReferralCoordinator;->bindViews(Landroid/view/View;)V

    .line 63
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lflow/path/Path;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v0

    check-cast v0, Lcom/squareup/referrals/ReferralScreen;

    .line 64
    sget-object v1, Lcom/squareup/referrals/ReferralCoordinator$1;->$SwitchMap$com$squareup$referrals$ReferralScreen$Style:[I

    iget-object v0, v0, Lcom/squareup/referrals/ReferralScreen;->style:Lcom/squareup/referrals/ReferralScreen$Style;

    invoke-virtual {v0}, Lcom/squareup/referrals/ReferralScreen$Style;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 78
    invoke-direct {p0, p1}, Lcom/squareup/referrals/ReferralCoordinator;->actionBarWithUp(Landroid/view/View;)V

    goto :goto_0

    .line 80
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/referrals/ReferralCoordinator;->actionBarWithoutUp(Landroid/view/View;)V

    goto :goto_0

    .line 73
    :cond_2
    invoke-direct {p0, p1}, Lcom/squareup/referrals/ReferralCoordinator;->actionBarWithUp(Landroid/view/View;)V

    goto :goto_0

    .line 66
    :cond_3
    iget-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->hide()V

    .line 67
    iget-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator;->laterButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 68
    iget-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator;->laterButton:Landroid/widget/Button;

    new-instance v1, Lcom/squareup/referrals/-$$Lambda$ReferralCoordinator$fKvTXyWWLmgnzPCnbPaRS6x5Um0;

    invoke-direct {v1, p0}, Lcom/squareup/referrals/-$$Lambda$ReferralCoordinator$fKvTXyWWLmgnzPCnbPaRS6x5Um0;-><init>(Lcom/squareup/referrals/ReferralCoordinator;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    sget v0, Lcom/squareup/marin/R$color;->marin_window_background:I

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 84
    :goto_0
    iget-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator;->inviteFriendsButton:Landroid/widget/Button;

    new-instance v1, Lcom/squareup/referrals/-$$Lambda$ReferralCoordinator$yBAiZ-jyG8WOtCWxhnyAqRxYn6Q;

    invoke-direct {v1, p0}, Lcom/squareup/referrals/-$$Lambda$ReferralCoordinator$yBAiZ-jyG8WOtCWxhnyAqRxYn6Q;-><init>(Lcom/squareup/referrals/ReferralCoordinator;)V

    .line 85
    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    .line 84
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    new-instance v0, Lcom/squareup/referrals/-$$Lambda$ReferralCoordinator$TE7K1LerrYoyTiPcMkZ2OzIkoZM;

    invoke-direct {v0, p0}, Lcom/squareup/referrals/-$$Lambda$ReferralCoordinator$TE7K1LerrYoyTiPcMkZ2OzIkoZM;-><init>(Lcom/squareup/referrals/ReferralCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 97
    new-instance v0, Lcom/squareup/referrals/-$$Lambda$ReferralCoordinator$2SUrJj_tr7u5hq_ady9lA1JVdZQ;

    invoke-direct {v0, p0, p1}, Lcom/squareup/referrals/-$$Lambda$ReferralCoordinator$2SUrJj_tr7u5hq_ady9lA1JVdZQ;-><init>(Lcom/squareup/referrals/ReferralCoordinator;Landroid/view/View;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$actionBarWithUp$8$ReferralCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 115
    iget-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator;->rewardsCopyExperiment:Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;

    invoke-virtual {v0}, Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;->showMultipleRewardsCopyBehavior()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/referrals/-$$Lambda$ReferralCoordinator$mWnvlei7A82kTcsAhZGU_m7aefo;

    invoke-direct {v1, p0}, Lcom/squareup/referrals/-$$Lambda$ReferralCoordinator$mWnvlei7A82kTcsAhZGU_m7aefo;-><init>(Lcom/squareup/referrals/ReferralCoordinator;)V

    .line 116
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$actionBarWithoutUp$6$ReferralCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator;->rewardsCopyExperiment:Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;

    invoke-virtual {v0}, Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;->showMultipleRewardsCopyBehavior()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/referrals/-$$Lambda$ReferralCoordinator$E-2Hsg9iIGqs3VClKHsvQ7vVTy0;

    invoke-direct {v1, p0}, Lcom/squareup/referrals/-$$Lambda$ReferralCoordinator$E-2Hsg9iIGqs3VClKHsvQ7vVTy0;-><init>(Lcom/squareup/referrals/ReferralCoordinator;)V

    .line 105
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$0$ReferralCoordinator(Landroid/view/View;)V
    .locals 0

    .line 68
    iget-object p1, p0, Lcom/squareup/referrals/ReferralCoordinator;->referralRunner:Lcom/squareup/referrals/ReferralRunner;

    invoke-virtual {p1}, Lcom/squareup/referrals/ReferralRunner;->onUserDismissed()V

    return-void
.end method

.method public synthetic lambda$attach$1$ReferralCoordinator(Landroid/view/View;)V
    .locals 0

    .line 85
    invoke-direct {p0}, Lcom/squareup/referrals/ReferralCoordinator;->onReferralTapped()V

    return-void
.end method

.method public synthetic lambda$attach$3$ReferralCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 87
    iget-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator;->referralRunner:Lcom/squareup/referrals/ReferralRunner;

    invoke-virtual {v0}, Lcom/squareup/referrals/ReferralRunner;->referralViewData()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/referrals/ReferralCoordinator;->spinner:Lcom/squareup/register/widgets/GlassSpinner;

    sget v2, Lcom/squareup/referrals/R$string;->loading:I

    .line 88
    invoke-virtual {v1, v2}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransformRx2(I)Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/referrals/-$$Lambda$ReferralCoordinator$3yrWIA161vQAOUioW4DjmDGgby8;

    invoke-direct {v1, p0}, Lcom/squareup/referrals/-$$Lambda$ReferralCoordinator$3yrWIA161vQAOUioW4DjmDGgby8;-><init>(Lcom/squareup/referrals/ReferralCoordinator;)V

    .line 89
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$4$ReferralCoordinator(Landroid/view/View;)Lrx/Subscription;
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator;->spinner:Lcom/squareup/register/widgets/GlassSpinner;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/GlassSpinner;->showOrHideSpinner(Landroid/content/Context;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$2$ReferralCoordinator(Lcom/squareup/referrals/ReferralRunner$ReferralData;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 90
    iget-boolean v0, p1, Lcom/squareup/referrals/ReferralRunner$ReferralData;->hasError:Z

    if-nez v0, :cond_1

    .line 91
    iget-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/squareup/referrals/ReferralCoordinator;->setData(Lcom/squareup/referrals/ReferralRunner$ReferralData;Z)V

    :cond_1
    return-void
.end method

.method public synthetic lambda$null$5$ReferralCoordinator(Ljava/lang/Boolean;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 105
    iget-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v1, p0, Lcom/squareup/referrals/ReferralCoordinator;->res:Lcom/squareup/util/Res;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    sget p1, Lcom/squareup/referrals/R$string;->referral_earn_rewards:I

    goto :goto_0

    :cond_0
    sget p1, Lcom/squareup/referrals/R$string;->referral_get_free_processing:I

    :goto_0
    invoke-interface {v1, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setTitleNoUpButton(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public synthetic lambda$null$7$ReferralCoordinator(Ljava/lang/Boolean;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 116
    iget-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v1, p0, Lcom/squareup/referrals/ReferralCoordinator;->res:Lcom/squareup/util/Res;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    sget p1, Lcom/squareup/referrals/R$string;->referral_earn_rewards:I

    goto :goto_0

    :cond_0
    sget p1, Lcom/squareup/referrals/R$string;->referral_get_free_processing:I

    :goto_0
    invoke-interface {v1, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)V

    return-void
.end method
