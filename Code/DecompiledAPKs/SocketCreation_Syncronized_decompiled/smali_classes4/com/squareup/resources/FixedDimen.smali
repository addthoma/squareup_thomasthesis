.class public final Lcom/squareup/resources/FixedDimen;
.super Ljava/lang/Object;
.source "DimenModels.kt"

# interfaces
.implements Lcom/squareup/resources/DimenModel;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/resources/FixedDimen$Creator;,
        Lcom/squareup/resources/FixedDimen$Unit;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDimenModels.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DimenModels.kt\ncom/squareup/resources/FixedDimen\n*L\n1#1,71:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0087\u0008\u0018\u00002\u00020\u0001:\u0001!B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\t\u0010\u000e\u001a\u00020\u0003H\u00d6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u00d6\u0003J\t\u0010\u0013\u001a\u00020\u0003H\u00d6\u0001J\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\u0010\u0010\u0018\u001a\u00020\u00032\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\u0010\u0010\u0019\u001a\u00020\u00032\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\u0019\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/resources/FixedDimen;",
        "Lcom/squareup/resources/DimenModel;",
        "value",
        "",
        "unit",
        "Lcom/squareup/resources/FixedDimen$Unit;",
        "(ILcom/squareup/resources/FixedDimen$Unit;)V",
        "getUnit",
        "()Lcom/squareup/resources/FixedDimen$Unit;",
        "getValue",
        "()I",
        "component1",
        "component2",
        "copy",
        "describeContents",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toFloat",
        "",
        "context",
        "Landroid/content/Context;",
        "toOffset",
        "toSize",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "Unit",
        "resources_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final unit:Lcom/squareup/resources/FixedDimen$Unit;

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/resources/FixedDimen$Creator;

    invoke-direct {v0}, Lcom/squareup/resources/FixedDimen$Creator;-><init>()V

    sput-object v0, Lcom/squareup/resources/FixedDimen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILcom/squareup/resources/FixedDimen$Unit;)V
    .locals 1

    const-string v0, "unit"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/resources/FixedDimen;->value:I

    iput-object p2, p0, Lcom/squareup/resources/FixedDimen;->unit:Lcom/squareup/resources/FixedDimen$Unit;

    return-void
.end method

.method public synthetic constructor <init>(ILcom/squareup/resources/FixedDimen$Unit;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    .line 43
    sget-object p2, Lcom/squareup/resources/FixedDimen$Unit;->PX:Lcom/squareup/resources/FixedDimen$Unit;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/resources/FixedDimen;-><init>(ILcom/squareup/resources/FixedDimen$Unit;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/resources/FixedDimen;ILcom/squareup/resources/FixedDimen$Unit;ILjava/lang/Object;)Lcom/squareup/resources/FixedDimen;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget p1, p0, Lcom/squareup/resources/FixedDimen;->value:I

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/resources/FixedDimen;->unit:Lcom/squareup/resources/FixedDimen$Unit;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/resources/FixedDimen;->copy(ILcom/squareup/resources/FixedDimen$Unit;)Lcom/squareup/resources/FixedDimen;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/resources/FixedDimen;->value:I

    return v0
.end method

.method public final component2()Lcom/squareup/resources/FixedDimen$Unit;
    .locals 1

    iget-object v0, p0, Lcom/squareup/resources/FixedDimen;->unit:Lcom/squareup/resources/FixedDimen$Unit;

    return-object v0
.end method

.method public final copy(ILcom/squareup/resources/FixedDimen$Unit;)Lcom/squareup/resources/FixedDimen;
    .locals 1

    const-string v0, "unit"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/resources/FixedDimen;

    invoke-direct {v0, p1, p2}, Lcom/squareup/resources/FixedDimen;-><init>(ILcom/squareup/resources/FixedDimen$Unit;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/resources/FixedDimen;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/resources/FixedDimen;

    iget v0, p0, Lcom/squareup/resources/FixedDimen;->value:I

    iget v1, p1, Lcom/squareup/resources/FixedDimen;->value:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/resources/FixedDimen;->unit:Lcom/squareup/resources/FixedDimen$Unit;

    iget-object p1, p1, Lcom/squareup/resources/FixedDimen;->unit:Lcom/squareup/resources/FixedDimen$Unit;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getUnit()Lcom/squareup/resources/FixedDimen$Unit;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/resources/FixedDimen;->unit:Lcom/squareup/resources/FixedDimen$Unit;

    return-object v0
.end method

.method public final getValue()I
    .locals 1

    .line 43
    iget v0, p0, Lcom/squareup/resources/FixedDimen;->value:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/squareup/resources/FixedDimen;->value:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/resources/FixedDimen;->unit:Lcom/squareup/resources/FixedDimen$Unit;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public toFloat(Landroid/content/Context;)F
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/resources/FixedDimen;->unit:Lcom/squareup/resources/FixedDimen$Unit;

    invoke-virtual {v0}, Lcom/squareup/resources/FixedDimen$Unit;->getAndroidUnit$resources_release()I

    move-result v0

    iget v1, p0, Lcom/squareup/resources/FixedDimen;->value:I

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const-string v2, "context.resources"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    invoke-static {v0, v1, p1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result p1

    return p1
.end method

.method public toOffset(Landroid/content/Context;)I
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-virtual {p0, p1}, Lcom/squareup/resources/FixedDimen;->toFloat(Landroid/content/Context;)F

    move-result p1

    float-to-int p1, p1

    return p1
.end method

.method public toSize(Landroid/content/Context;)I
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-virtual {p0, p1}, Lcom/squareup/resources/FixedDimen;->toFloat(Landroid/content/Context;)F

    move-result p1

    invoke-static {p1}, Lkotlin/math/MathKt;->roundToInt(F)I

    move-result p1

    const/4 v0, 0x1

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    return p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "FixedDimen(value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/resources/FixedDimen;->value:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", unit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/resources/FixedDimen;->unit:Lcom/squareup/resources/FixedDimen$Unit;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget p2, p0, Lcom/squareup/resources/FixedDimen;->value:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    iget-object p2, p0, Lcom/squareup/resources/FixedDimen;->unit:Lcom/squareup/resources/FixedDimen$Unit;

    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
