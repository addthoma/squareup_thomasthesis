.class public final Lcom/squareup/receiving/EventSinkSessionExpiredHandler;
.super Ljava/lang/Object;
.source "EventSinkSessionExpiredHandler.kt"

# interfaces
.implements Lcom/squareup/receiving/StandardReceiver$SessionExpiredHandler;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/receiving/EventSinkSessionExpiredHandler;",
        "Lcom/squareup/receiving/StandardReceiver$SessionExpiredHandler;",
        "eventSink",
        "Lcom/squareup/badbus/BadEventSink;",
        "(Lcom/squareup/badbus/BadEventSink;)V",
        "onSessionExpired",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final eventSink:Lcom/squareup/badbus/BadEventSink;


# direct methods
.method public constructor <init>(Lcom/squareup/badbus/BadEventSink;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "eventSink"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/receiving/EventSinkSessionExpiredHandler;->eventSink:Lcom/squareup/badbus/BadEventSink;

    return-void
.end method


# virtual methods
.method public onSessionExpired()V
    .locals 2

    .line 11
    iget-object v0, p0, Lcom/squareup/receiving/EventSinkSessionExpiredHandler;->eventSink:Lcom/squareup/badbus/BadEventSink;

    new-instance v1, Lcom/squareup/account/AccountEvents$SessionExpired;

    invoke-direct {v1}, Lcom/squareup/account/AccountEvents$SessionExpired;-><init>()V

    invoke-interface {v0, v1}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    return-void
.end method
