.class final Lcom/squareup/receiving/StandardReceiver$succeedOrFailSingle$1;
.super Ljava/lang/Object;
.source "StandardReceiver.kt"

# interfaces
.implements Lio/reactivex/SingleTransformer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/receiving/StandardReceiver;->succeedOrFailSingle$public_release(Lretrofit2/Retrofit;Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;Lkotlin/jvm/functions/Function1;)Lio/reactivex/SingleTransformer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Upstream:",
        "Ljava/lang/Object;",
        "Downstream:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/SingleTransformer<",
        "TT;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
        "+TT;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\u0010\u0000\u001a>\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u0002H\u0003 \u0004*\n\u0012\u0004\u0012\u0002H\u0003\u0018\u00010\u00020\u0002 \u0004*\u001e\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u0002H\u0003 \u0004*\n\u0012\u0004\u0012\u0002H\u0003\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u0001\"\u0008\u0008\u0000\u0010\u0003*\u00020\u00052\u0014\u0010\u0006\u001a\u0010\u0012\u000c\u0012\n \u0004*\u0004\u0018\u0001H\u0003H\u00030\u0001H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "T",
        "kotlin.jvm.PlatformType",
        "",
        "single",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $annotations:[Ljava/lang/annotation/Annotation;

.field final synthetic $isSuccessful:Lkotlin/jvm/functions/Function1;

.field final synthetic $retrofit:Lretrofit2/Retrofit;

.field final synthetic $type:Ljava/lang/reflect/Type;

.field final synthetic this$0:Lcom/squareup/receiving/StandardReceiver;


# direct methods
.method constructor <init>(Lcom/squareup/receiving/StandardReceiver;Lretrofit2/Retrofit;Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/receiving/StandardReceiver$succeedOrFailSingle$1;->this$0:Lcom/squareup/receiving/StandardReceiver;

    iput-object p2, p0, Lcom/squareup/receiving/StandardReceiver$succeedOrFailSingle$1;->$retrofit:Lretrofit2/Retrofit;

    iput-object p3, p0, Lcom/squareup/receiving/StandardReceiver$succeedOrFailSingle$1;->$type:Ljava/lang/reflect/Type;

    iput-object p4, p0, Lcom/squareup/receiving/StandardReceiver$succeedOrFailSingle$1;->$annotations:[Ljava/lang/annotation/Annotation;

    iput-object p5, p0, Lcom/squareup/receiving/StandardReceiver$succeedOrFailSingle$1;->$isSuccessful:Lkotlin/jvm/functions/Function1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lio/reactivex/Single;)Lio/reactivex/Single;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Single<",
            "TT;>;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "TT;>;>;"
        }
    .end annotation

    const-string v0, "single"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    sget-object v0, Lcom/squareup/receiving/ReceivedResponse;->Companion:Lcom/squareup/receiving/ReceivedResponse$Companion;

    iget-object v1, p0, Lcom/squareup/receiving/StandardReceiver$succeedOrFailSingle$1;->$retrofit:Lretrofit2/Retrofit;

    iget-object v2, p0, Lcom/squareup/receiving/StandardReceiver$succeedOrFailSingle$1;->$type:Ljava/lang/reflect/Type;

    iget-object v3, p0, Lcom/squareup/receiving/StandardReceiver$succeedOrFailSingle$1;->$annotations:[Ljava/lang/annotation/Annotation;

    iget-object v4, p0, Lcom/squareup/receiving/StandardReceiver$succeedOrFailSingle$1;->$isSuccessful:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/receiving/ReceivedResponse$Companion;->receiveFromRetrofit(Lretrofit2/Retrofit;Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;Lkotlin/jvm/functions/Function1;)Lio/reactivex/SingleTransformer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->compose(Lio/reactivex/SingleTransformer;)Lio/reactivex/Single;

    move-result-object p1

    .line 128
    iget-object v0, p0, Lcom/squareup/receiving/StandardReceiver$succeedOrFailSingle$1;->this$0:Lcom/squareup/receiving/StandardReceiver;

    invoke-virtual {v0}, Lcom/squareup/receiving/StandardReceiver;->receivedResponseToSuccessOrFailure()Lio/reactivex/SingleTransformer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->compose(Lio/reactivex/SingleTransformer;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Lio/reactivex/Single;)Lio/reactivex/SingleSource;
    .locals 0

    .line 39
    invoke-virtual {p0, p1}, Lcom/squareup/receiving/StandardReceiver$succeedOrFailSingle$1;->apply(Lio/reactivex/Single;)Lio/reactivex/Single;

    move-result-object p1

    check-cast p1, Lio/reactivex/SingleSource;

    return-object p1
.end method
