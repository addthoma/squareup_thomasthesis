.class public final Lcom/squareup/receiving/ReceivedResponseRetrofit1;
.super Ljava/lang/Object;
.source "ReceivedResponseRetrofit1.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u001c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u0002H\u00050\u0004\"\u0004\u0008\u0000\u0010\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0007J4\u0010\u0008\u001a\u0014\u0012\u0004\u0012\u0002H\u0005\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00050\n0\t\"\u0004\u0008\u0000\u0010\u00052\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u00020\r0\u000cH\u0007J\u001d\u0010\u000e\u001a\u0004\u0018\u0001H\u0005\"\u0004\u0008\u0000\u0010\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0002\u00a2\u0006\u0002\u0010\u000fJ1\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u0002H\u00050\u0011\"\u0004\u0008\u0000\u0010\u0005*\u000e\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u00020\r0\u000c2\u0006\u0010\u0012\u001a\u0002H\u0005H\u0002\u00a2\u0006\u0002\u0010\u0013\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/receiving/ReceivedResponseRetrofit1;",
        "",
        "()V",
        "fromRetrofitError1",
        "Lcom/squareup/receiving/ReceivedResponse$Error;",
        "T",
        "error",
        "Lretrofit/RetrofitError;",
        "receiveFromRetrofit1",
        "Lio/reactivex/SingleTransformer;",
        "Lcom/squareup/receiving/ReceivedResponse;",
        "isSuccessful",
        "Lkotlin/Function1;",
        "",
        "tryToReadErrorBody",
        "(Lretrofit/RetrofitError;)Ljava/lang/Object;",
        "accept",
        "Lcom/squareup/receiving/ReceivedResponse$Okay;",
        "t",
        "(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)Lcom/squareup/receiving/ReceivedResponse$Okay;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/receiving/ReceivedResponseRetrofit1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/receiving/ReceivedResponseRetrofit1;

    invoke-direct {v0}, Lcom/squareup/receiving/ReceivedResponseRetrofit1;-><init>()V

    sput-object v0, Lcom/squareup/receiving/ReceivedResponseRetrofit1;->INSTANCE:Lcom/squareup/receiving/ReceivedResponseRetrofit1;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final accept(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)Lcom/squareup/receiving/ReceivedResponse$Okay;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Ljava/lang/Boolean;",
            ">;TT;)",
            "Lcom/squareup/receiving/ReceivedResponse$Okay<",
            "TT;>;"
        }
    .end annotation

    .line 138
    invoke-interface {p1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Accepted;

    invoke-direct {p1, p2}, Lcom/squareup/receiving/ReceivedResponse$Okay$Accepted;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    invoke-direct {p1, p2}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;-><init>(Ljava/lang/Object;)V

    :goto_0
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay;

    return-object p1
.end method

.method public static final synthetic access$accept(Lcom/squareup/receiving/ReceivedResponseRetrofit1;Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)Lcom/squareup/receiving/ReceivedResponse$Okay;
    .locals 0

    .line 23
    invoke-direct {p0, p1, p2}, Lcom/squareup/receiving/ReceivedResponseRetrofit1;->accept(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)Lcom/squareup/receiving/ReceivedResponse$Okay;

    move-result-object p0

    return-object p0
.end method

.method public static final fromRetrofitError1(Lretrofit/RetrofitError;)Lcom/squareup/receiving/ReceivedResponse$Error;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lretrofit/RetrofitError;",
            ")",
            "Lcom/squareup/receiving/ReceivedResponse$Error<",
            "TT;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Visible only for use from legacy call sites in SquareEndpoints and CallFailure"
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "error"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-virtual {p0}, Lretrofit/RetrofitError;->getKind()Lretrofit/RetrofitError$Kind;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    sget-object v1, Lcom/squareup/receiving/ReceivedResponseRetrofit1$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lretrofit/RetrofitError$Kind;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_8

    const/4 v1, 0x2

    if-eq v0, v1, :cond_8

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    .line 80
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    check-cast p0, Ljava/lang/Throwable;

    const-string v1, "Unexpected Retrofit error: "

    invoke-direct {v0, v1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 59
    :cond_2
    invoke-virtual {p0}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v0

    const-string v1, "error.response"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lretrofit/client/Response;->getStatus()I

    move-result v0

    const/16 v1, 0x191

    if-ne v0, v1, :cond_3

    .line 62
    new-instance v0, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;

    sget-object v1, Lcom/squareup/receiving/ReceivedResponseRetrofit1;->INSTANCE:Lcom/squareup/receiving/ReceivedResponseRetrofit1;

    invoke-direct {v1, p0}, Lcom/squareup/receiving/ReceivedResponseRetrofit1;->tryToReadErrorBody(Lretrofit/RetrofitError;)Ljava/lang/Object;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;-><init>(Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/receiving/ReceivedResponse$Error;

    goto :goto_0

    :cond_3
    const/16 v1, 0x198

    if-ne v0, v1, :cond_4

    .line 65
    sget-object p0, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;->INSTANCE:Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    move-object v0, p0

    check-cast v0, Lcom/squareup/receiving/ReceivedResponse$Error;

    goto :goto_0

    :cond_4
    const/16 v1, 0x1ad

    if-ne v0, v1, :cond_5

    .line 69
    sget-object p0, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;->INSTANCE:Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    move-object v0, p0

    check-cast v0, Lcom/squareup/receiving/ReceivedResponse$Error;

    goto :goto_0

    :cond_5
    const/16 v1, 0x1f4

    if-lt v0, v1, :cond_6

    .line 72
    new-instance p0, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    invoke-direct {p0, v0}, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;-><init>(I)V

    move-object v0, p0

    check-cast v0, Lcom/squareup/receiving/ReceivedResponse$Error;

    goto :goto_0

    :cond_6
    const/16 v1, 0x190

    if-lt v0, v1, :cond_7

    .line 75
    new-instance v1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    sget-object v2, Lcom/squareup/receiving/ReceivedResponseRetrofit1;->INSTANCE:Lcom/squareup/receiving/ReceivedResponseRetrofit1;

    invoke-direct {v2, p0}, Lcom/squareup/receiving/ReceivedResponseRetrofit1;->tryToReadErrorBody(Lretrofit/RetrofitError;)Ljava/lang/Object;

    move-result-object p0

    invoke-direct {v1, p0, v0}, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;-><init>(Ljava/lang/Object;I)V

    move-object v0, v1

    check-cast v0, Lcom/squareup/receiving/ReceivedResponse$Error;

    goto :goto_0

    .line 77
    :cond_7
    new-instance p0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected HTTP status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    .line 57
    :cond_8
    sget-object p0, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;->INSTANCE:Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    move-object v0, p0

    check-cast v0, Lcom/squareup/receiving/ReceivedResponse$Error;

    :goto_0
    return-object v0
.end method

.method public static final receiveFromRetrofit1(Lkotlin/jvm/functions/Function1;)Lio/reactivex/SingleTransformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lio/reactivex/SingleTransformer<",
            "TT;",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "TT;>;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "isSuccessful"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    new-instance v0, Lcom/squareup/receiving/ReceivedResponseRetrofit1$receiveFromRetrofit1$1;

    invoke-direct {v0, p0}, Lcom/squareup/receiving/ReceivedResponseRetrofit1$receiveFromRetrofit1$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lio/reactivex/SingleTransformer;

    return-object v0
.end method

.method private final tryToReadErrorBody(Lretrofit/RetrofitError;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lretrofit/RetrofitError;",
            ")TT;"
        }
    .end annotation

    .line 118
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    .line 122
    :try_start_0
    invoke-virtual {p1}, Lretrofit/RetrofitError;->getBody()Ljava/lang/Object;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 131
    :goto_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    .line 132
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    :catchall_0
    move-exception p1

    if-eqz v0, :cond_0

    .line 131
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    .line 132
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :cond_0
    throw p1

    :catch_0
    const/4 p1, 0x0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    :goto_1
    return-object p1
.end method
