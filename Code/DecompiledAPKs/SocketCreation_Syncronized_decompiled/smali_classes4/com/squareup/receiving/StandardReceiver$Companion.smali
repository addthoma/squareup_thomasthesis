.class public final Lcom/squareup/receiving/StandardReceiver$Companion;
.super Ljava/lang/Object;
.source "StandardReceiver.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/receiving/StandardReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J \u0010\u0003\u001a\u0014\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00060\u0005\u0012\u0004\u0012\u0002H\u00060\u0004\"\u0004\u0008\u0000\u0010\u0006H\u0007J\"\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u0007\"\u0004\u0008\u0000\u0010\u0006*\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00060\u00050\u0007J2\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u0005\"\u0004\u0008\u0000\u0010\u0006*\u0008\u0012\u0004\u0012\u0002H\u00060\u00052\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u00020\u000b0\nH\u0007\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/receiving/StandardReceiver$Companion;",
        "",
        "()V",
        "filterSuccess",
        "Lio/reactivex/ObservableTransformer;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "T",
        "Lio/reactivex/Observable;",
        "rejectIfNot",
        "isSuccessful",
        "Lkotlin/Function1;",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 156
    invoke-direct {p0}, Lcom/squareup/receiving/StandardReceiver$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final filterSuccess(Lio/reactivex/Observable;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "TT;>;>;)",
            "Lio/reactivex/Observable<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$filterSuccess"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 172
    sget-object v0, Lcom/squareup/receiving/StandardReceiver;->Companion:Lcom/squareup/receiving/StandardReceiver$Companion;

    invoke-virtual {v0}, Lcom/squareup/receiving/StandardReceiver$Companion;->filterSuccess()Lio/reactivex/ObservableTransformer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "compose(StandardReceiver.filterSuccess())"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final filterSuccess()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lio/reactivex/ObservableTransformer<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "TT;>;TT;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 163
    sget-object v0, Lcom/squareup/receiving/StandardReceiver$Companion$filterSuccess$1;->INSTANCE:Lcom/squareup/receiving/StandardReceiver$Companion$filterSuccess$1;

    check-cast v0, Lio/reactivex/ObservableTransformer;

    return-object v0
.end method

.method public final rejectIfNot(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "+TT;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "TT;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "$this$rejectIfNot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "isSuccessful"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {v0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p2, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-nez p2, :cond_0

    .line 183
    new-instance p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    invoke-virtual {v0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;-><init>(Ljava/lang/Object;)V

    check-cast p1, Lcom/squareup/receiving/ReceivedResponse;

    invoke-static {p1}, Lcom/squareup/receiving/StandardReceiverKt;->toSuccessOrFailure(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p1

    :cond_0
    return-object p1
.end method
