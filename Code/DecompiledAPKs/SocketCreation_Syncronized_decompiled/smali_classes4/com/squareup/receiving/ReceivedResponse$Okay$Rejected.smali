.class public final Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;
.super Lcom/squareup/receiving/ReceivedResponse$Okay;
.source "ReceivedResponse.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/receiving/ReceivedResponse$Okay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Rejected"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/receiving/ReceivedResponse$Okay<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u0000*\u0006\u0008\u0002\u0010\u0001 \u00012\u0008\u0012\u0004\u0012\u0002H\u00010\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00028\u0002\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0008\u001a\u00028\u0002H\u00c6\u0003\u00a2\u0006\u0002\u0010\u0006J\u001e\u0010\t\u001a\u0008\u0012\u0004\u0012\u00028\u00020\u00002\u0008\u0008\u0002\u0010\u0003\u001a\u00028\u0002H\u00c6\u0001\u00a2\u0006\u0002\u0010\nJ\u0013\u0010\u000b\u001a\u00020\u000c2\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eH\u00d6\u0003J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001J+\u0010\u0011\u001a\u0002H\u0012\"\u0004\u0008\u0003\u0010\u00122\u0016\u0010\u0013\u001a\u0012\u0012\u0006\u0008\u0000\u0012\u00028\u0002\u0012\u0006\u0008\u0001\u0012\u0002H\u00120\u0014H\u0016\u00a2\u0006\u0002\u0010\u0015J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001R\u0016\u0010\u0003\u001a\u00028\u0002X\u0096\u0004\u00a2\u0006\n\n\u0002\u0010\u0007\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;",
        "T",
        "Lcom/squareup/receiving/ReceivedResponse$Okay;",
        "response",
        "(Ljava/lang/Object;)V",
        "getResponse",
        "()Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "component1",
        "copy",
        "(Ljava/lang/Object;)Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "map",
        "M",
        "javaMapper",
        "Lcom/squareup/receiving/ReceivedMapper;",
        "(Lcom/squareup/receiving/ReceivedMapper;)Ljava/lang/Object;",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final response:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 47
    invoke-direct {p0, v0}, Lcom/squareup/receiving/ReceivedResponse$Okay;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->response:Ljava/lang/Object;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->copy(Ljava/lang/Object;)Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final copy(Ljava/lang/Object;)Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    invoke-direct {v0, p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    invoke-virtual {p0}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getResponse()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 47
    iget-object v0, p0, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->response:Ljava/lang/Object;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public map(Lcom/squareup/receiving/ReceivedMapper;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/receiving/ReceivedMapper<",
            "-TT;+TM;>;)TM;"
        }
    .end annotation

    const-string v0, "javaMapper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-virtual {p0}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/receiving/ReceivedMapper;->isRejected(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Rejected(response="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
