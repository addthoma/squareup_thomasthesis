.class public Lcom/squareup/prices/PricingEngineServiceResult$Builder;
.super Ljava/lang/Object;
.source "PricingEngineServiceResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/prices/PricingEngineServiceResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private blocks:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/ApplicationBlock;",
            ">;>;"
        }
    .end annotation
.end field

.field private cartDiscountsToRemove:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation
.end field

.field private catalogDiscounts:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;"
        }
    .end annotation
.end field

.field private order:Lcom/squareup/payment/Order;

.field private wholeBlocks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/ApplicationWholeBlock;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/prices/PricingEngineServiceResult;
    .locals 8

    .line 120
    new-instance v7, Lcom/squareup/prices/PricingEngineServiceResult;

    iget-object v1, p0, Lcom/squareup/prices/PricingEngineServiceResult$Builder;->order:Lcom/squareup/payment/Order;

    iget-object v2, p0, Lcom/squareup/prices/PricingEngineServiceResult$Builder;->cartDiscountsToRemove:Ljava/util/Set;

    iget-object v3, p0, Lcom/squareup/prices/PricingEngineServiceResult$Builder;->catalogDiscounts:Ljava/util/Map;

    iget-object v4, p0, Lcom/squareup/prices/PricingEngineServiceResult$Builder;->blocks:Ljava/util/Map;

    iget-object v5, p0, Lcom/squareup/prices/PricingEngineServiceResult$Builder;->wholeBlocks:Ljava/util/List;

    const/4 v6, 0x0

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/prices/PricingEngineServiceResult;-><init>(Lcom/squareup/payment/Order;Ljava/util/Set;Ljava/util/Map;Ljava/util/Map;Ljava/util/List;Lcom/squareup/prices/PricingEngineServiceResult$1;)V

    return-object v7
.end method

.method public setBlocks(Ljava/util/Map;)Lcom/squareup/prices/PricingEngineServiceResult$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/ApplicationBlock;",
            ">;>;)",
            "Lcom/squareup/prices/PricingEngineServiceResult$Builder;"
        }
    .end annotation

    .line 110
    iput-object p1, p0, Lcom/squareup/prices/PricingEngineServiceResult$Builder;->blocks:Ljava/util/Map;

    return-object p0
.end method

.method public setCartDiscountsToRemove(Ljava/util/Set;)Lcom/squareup/prices/PricingEngineServiceResult$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/checkout/Discount;",
            ">;)",
            "Lcom/squareup/prices/PricingEngineServiceResult$Builder;"
        }
    .end annotation

    .line 100
    iput-object p1, p0, Lcom/squareup/prices/PricingEngineServiceResult$Builder;->cartDiscountsToRemove:Ljava/util/Set;

    return-object p0
.end method

.method public setCatalogDiscounts(Ljava/util/Map;)Lcom/squareup/prices/PricingEngineServiceResult$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;)",
            "Lcom/squareup/prices/PricingEngineServiceResult$Builder;"
        }
    .end annotation

    .line 105
    iput-object p1, p0, Lcom/squareup/prices/PricingEngineServiceResult$Builder;->catalogDiscounts:Ljava/util/Map;

    return-object p0
.end method

.method public setOrder(Lcom/squareup/payment/Order;)Lcom/squareup/prices/PricingEngineServiceResult$Builder;
    .locals 0

    .line 95
    iput-object p1, p0, Lcom/squareup/prices/PricingEngineServiceResult$Builder;->order:Lcom/squareup/payment/Order;

    return-object p0
.end method

.method public setWholeBlocks(Ljava/util/List;)Lcom/squareup/prices/PricingEngineServiceResult$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/ApplicationWholeBlock;",
            ">;)",
            "Lcom/squareup/prices/PricingEngineServiceResult$Builder;"
        }
    .end annotation

    .line 115
    iput-object p1, p0, Lcom/squareup/prices/PricingEngineServiceResult$Builder;->wholeBlocks:Ljava/util/List;

    return-object p0
.end method
