.class public final Lcom/squareup/pos/loggedout/PosLearnMoreTourPager;
.super Ljava/lang/Object;
.source "PosLearnMoreTourPager.kt"

# interfaces
.implements Lcom/squareup/ui/tour/LearnMoreTourPager;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPosLearnMoreTourPager.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PosLearnMoreTourPager.kt\ncom/squareup/pos/loggedout/PosLearnMoreTourPager\n*L\n1#1,38:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/pos/loggedout/PosLearnMoreTourPager;",
        "Lcom/squareup/ui/tour/LearnMoreTourPager;",
        "countryGuesser",
        "Lcom/squareup/location/CountryCodeGuesser;",
        "(Lcom/squareup/location/CountryCodeGuesser;)V",
        "getLearnMoreItems",
        "",
        "Lcom/squareup/tour/Tour$HasTourPages;",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final countryGuesser:Lcom/squareup/location/CountryCodeGuesser;


# direct methods
.method public constructor <init>(Lcom/squareup/location/CountryCodeGuesser;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "countryGuesser"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/pos/loggedout/PosLearnMoreTourPager;->countryGuesser:Lcom/squareup/location/CountryCodeGuesser;

    return-void
.end method


# virtual methods
.method public getLearnMoreItems()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/tour/Tour$HasTourPages;",
            ">;"
        }
    .end annotation

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 21
    iget-object v1, p0, Lcom/squareup/pos/loggedout/PosLearnMoreTourPager;->countryGuesser:Lcom/squareup/location/CountryCodeGuesser;

    invoke-interface {v1}, Lcom/squareup/location/CountryCodeGuesser;->tryTelephony()Lcom/squareup/CountryCode;

    move-result-object v1

    .line 23
    invoke-static {v1}, Lcom/squareup/account/CountryCapabilitiesKt;->getHasPayments(Lcom/squareup/CountryCode;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 25
    move-object v2, v0

    check-cast v2, Ljava/util/Collection;

    sget-object v3, Lcom/squareup/pos/loggedout/PosLearnMoreTour;->LEARN_MORE_SELLING_SIMPLE:Lcom/squareup/pos/loggedout/PosLearnMoreTour;

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 27
    invoke-static {v1}, Lcom/squareup/account/CountryCapabilitiesKt;->getHasFreeReaderPromo(Lcom/squareup/CountryCode;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 28
    sget-object v1, Lcom/squareup/pos/loggedout/PosLearnMoreTour;->LEARN_MORE_READER:Lcom/squareup/pos/loggedout/PosLearnMoreTour;

    invoke-interface {v2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 32
    :cond_0
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    sget-object v2, Lcom/squareup/pos/loggedout/PosLearnMoreTour;->LEARN_MORE_POS:Lcom/squareup/pos/loggedout/PosLearnMoreTour;

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 33
    sget-object v2, Lcom/squareup/pos/loggedout/PosLearnMoreTour;->LEARN_MORE_BACKOFFICE:Lcom/squareup/pos/loggedout/PosLearnMoreTour;

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    return-object v0
.end method
