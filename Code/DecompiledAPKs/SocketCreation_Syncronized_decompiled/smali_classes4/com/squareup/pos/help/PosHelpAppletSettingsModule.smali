.class public abstract Lcom/squareup/pos/help/PosHelpAppletSettingsModule;
.super Ljava/lang/Object;
.source "PosHelpAppletSettingsModule.kt"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/ui/help/PosCommonHelpAppletSettingsModule;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000|\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a2\u0006\u0002\u0008\u0007J\u0015\u0010\u0008\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\nH!\u00a2\u0006\u0002\u0008\u000bJ\u0015\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH!\u00a2\u0006\u0002\u0008\u0010J\u0015\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H!\u00a2\u0006\u0002\u0008\u0015J\u0015\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019H!\u00a2\u0006\u0002\u0008\u001aJ\u0015\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eH!\u00a2\u0006\u0002\u0008\u001fJ\u0015\u0010 \u001a\u00020!2\u0006\u0010\u0013\u001a\u00020\"H!\u00a2\u0006\u0002\u0008#J\u0015\u0010\t\u001a\u00020\n2\u0006\u0010$\u001a\u00020%H!\u00a2\u0006\u0002\u0008&J\u0015\u0010\'\u001a\u00020(2\u0006\u0010)\u001a\u00020*H!\u00a2\u0006\u0002\u0008+\u00a8\u0006,"
    }
    d2 = {
        "Lcom/squareup/pos/help/PosHelpAppletSettingsModule;",
        "",
        "()V",
        "bindBackgroundMessagingActivityHandlerToMainActivity",
        "Lmortar/Scoped;",
        "backgroundMessagingActivityHandler",
        "Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;",
        "bindBackgroundMessagingActivityHandlerToMainActivity$spos_help_release",
        "bindMessagingControllerToMainActivity",
        "messagingController",
        "Lcom/squareup/ui/help/MessagingController;",
        "bindMessagingControllerToMainActivity$spos_help_release",
        "helpAppletEntryPoint",
        "Lcom/squareup/ui/help/api/HelpAppletEntryPoint;",
        "posHelpAppletEntryPoint",
        "Lcom/squareup/pos/help/PosHelpAppletEntryPoint;",
        "helpAppletEntryPoint$spos_help_release",
        "helpAppletSettings",
        "Lcom/squareup/ui/help/api/HelpAppletSettings;",
        "posHelpAppletSettings",
        "Lcom/squareup/pos/help/PosHelpAppletSettings;",
        "helpAppletSettings$spos_help_release",
        "helpBadge",
        "Lcom/squareup/ui/help/HelpBadge;",
        "realHelpBadge",
        "Lcom/squareup/ui/help/RealHelpBadge;",
        "helpBadge$spos_help_release",
        "helpContentLinks",
        "Lcom/squareup/ui/help/api/HelpContentStaticLinks;",
        "posHelpContent",
        "Lcom/squareup/ui/help/help/PosHelpContentStaticLinks;",
        "helpContentLinks$spos_help_release",
        "jediHelpSettings",
        "Lcom/squareup/jedi/JediHelpSettings;",
        "Lcom/squareup/pos/help/PosJediHelpSettings;",
        "jediHelpSettings$spos_help_release",
        "controller",
        "Lcom/squareup/ui/help/messages/HelpshiftMessagingController;",
        "messagingController$spos_help_release",
        "supportMessagingNotifier",
        "Lcom/squareup/ui/help/SupportMessagingNotifier;",
        "messagingNotifier",
        "Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;",
        "supportMessagingNotifier$spos_help_release",
        "spos-help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindBackgroundMessagingActivityHandlerToMainActivity$spos_help_release(Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;)Lmortar/Scoped;
    .annotation runtime Lcom/squareup/ForMainActivity;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method public abstract bindMessagingControllerToMainActivity$spos_help_release(Lcom/squareup/ui/help/MessagingController;)Lmortar/Scoped;
    .annotation runtime Lcom/squareup/ForMainActivity;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method public abstract helpAppletEntryPoint$spos_help_release(Lcom/squareup/pos/help/PosHelpAppletEntryPoint;)Lcom/squareup/ui/help/api/HelpAppletEntryPoint;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract helpAppletSettings$spos_help_release(Lcom/squareup/pos/help/PosHelpAppletSettings;)Lcom/squareup/ui/help/api/HelpAppletSettings;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract helpBadge$spos_help_release(Lcom/squareup/ui/help/RealHelpBadge;)Lcom/squareup/ui/help/HelpBadge;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract helpContentLinks$spos_help_release(Lcom/squareup/ui/help/help/PosHelpContentStaticLinks;)Lcom/squareup/ui/help/api/HelpContentStaticLinks;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract jediHelpSettings$spos_help_release(Lcom/squareup/pos/help/PosJediHelpSettings;)Lcom/squareup/jedi/JediHelpSettings;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract messagingController$spos_help_release(Lcom/squareup/ui/help/messages/HelpshiftMessagingController;)Lcom/squareup/ui/help/MessagingController;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract supportMessagingNotifier$spos_help_release(Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;)Lcom/squareup/ui/help/SupportMessagingNotifier;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
