.class public final Lcom/squareup/pos/help/PosHelpAppletEntryPoint;
.super Lcom/squareup/ui/help/api/HelpAppletEntryPoint;
.source "PosHelpAppletEntryPoint.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001BW\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u00a2\u0006\u0002\u0010\u0016\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/pos/help/PosHelpAppletEntryPoint;",
        "Lcom/squareup/ui/help/api/HelpAppletEntryPoint;",
        "helpAppletSelectedSectionSetting",
        "Lcom/squareup/ui/help/HelpAppletSelectedSectionSetting;",
        "gatekeeper",
        "Lcom/squareup/permissions/PermissionGatekeeper;",
        "helpSection",
        "Lcom/squareup/ui/help/help/HelpSection;",
        "tutorialsSection",
        "Lcom/squareup/ui/help/tutorials/TutorialsSection;",
        "ordersSection",
        "Lcom/squareup/ui/help/orders/OrdersSection;",
        "announcementsSection",
        "Lcom/squareup/ui/help/announcements/AnnouncementsSection;",
        "troubleshootingSection",
        "Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection;",
        "aboutSection",
        "Lcom/squareup/ui/help/about/AboutSection;",
        "legalSection",
        "Lcom/squareup/ui/help/legal/PosLegalSection;",
        "referralsSection",
        "Lcom/squareup/ui/help/referrals/ReferralsSection;",
        "(Lcom/squareup/ui/help/HelpAppletSelectedSectionSetting;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/help/help/HelpSection;Lcom/squareup/ui/help/tutorials/TutorialsSection;Lcom/squareup/ui/help/orders/OrdersSection;Lcom/squareup/ui/help/announcements/AnnouncementsSection;Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection;Lcom/squareup/ui/help/about/AboutSection;Lcom/squareup/ui/help/legal/PosLegalSection;Lcom/squareup/ui/help/referrals/ReferralsSection;)V",
        "spos-help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/ui/help/HelpAppletSelectedSectionSetting;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/help/help/HelpSection;Lcom/squareup/ui/help/tutorials/TutorialsSection;Lcom/squareup/ui/help/orders/OrdersSection;Lcom/squareup/ui/help/announcements/AnnouncementsSection;Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection;Lcom/squareup/ui/help/about/AboutSection;Lcom/squareup/ui/help/legal/PosLegalSection;Lcom/squareup/ui/help/referrals/ReferralsSection;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "helpAppletSelectedSectionSetting"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "gatekeeper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "helpSection"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tutorialsSection"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ordersSection"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "announcementsSection"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "troubleshootingSection"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "aboutSection"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "legalSection"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "referralsSection"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    check-cast p3, Lcom/squareup/applet/AppletSection;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/applet/AppletSection;

    check-cast p4, Lcom/squareup/applet/AppletSection;

    const/4 v1, 0x0

    aput-object p4, v0, v1

    check-cast p5, Lcom/squareup/applet/AppletSection;

    const/4 p4, 0x1

    aput-object p5, v0, p4

    .line 29
    check-cast p6, Lcom/squareup/applet/AppletSection;

    const/4 p4, 0x2

    aput-object p6, v0, p4

    check-cast p7, Lcom/squareup/applet/AppletSection;

    const/4 p4, 0x3

    aput-object p7, v0, p4

    check-cast p8, Lcom/squareup/applet/AppletSection;

    const/4 p4, 0x4

    aput-object p8, v0, p4

    check-cast p9, Lcom/squareup/applet/AppletSection;

    const/4 p4, 0x5

    aput-object p9, v0, p4

    check-cast p10, Lcom/squareup/applet/AppletSection;

    const/4 p4, 0x6

    aput-object p10, v0, p4

    .line 27
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/ui/help/api/HelpAppletEntryPoint;-><init>(Lcom/squareup/ui/help/HelpAppletSelectedSectionSetting;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/applet/AppletSection;[Lcom/squareup/applet/AppletSection;)V

    return-void
.end method
