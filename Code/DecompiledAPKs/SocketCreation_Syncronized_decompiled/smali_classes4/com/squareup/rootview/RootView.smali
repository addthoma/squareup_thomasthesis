.class public Lcom/squareup/rootview/RootView;
.super Lcom/squareup/container/ContainerRelativeLayout;
.source "RootView.java"

# interfaces
.implements Lcom/squareup/ui/main/MainActivityScope$View;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/rootview/RootView$Component;
    }
.end annotation


# instance fields
.field badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private bodyContainer:Landroid/view/ViewGroup;

.field private cardOverSheetContainer:Lcom/squareup/container/CardScreenContainer;

.field private cardScreenContainer:Lcom/squareup/container/CardScreenContainer;

.field private enteringFullScreen:Z

.field features:Lcom/squareup/settings/server/Features;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private fullSheetContainer:Landroid/view/ViewGroup;

.field mainContainer:Lcom/squareup/container/ContainerPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/container/ContainerPresenter<",
            "Lcom/squareup/ui/main/MainActivityScope$View;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private masterContainer:Landroid/view/ViewGroup;

.field private messageBars:Landroid/view/ViewGroup;

.field private final messageBarsLayoutTransition:Lcom/squareup/rootview/RootLayoutTransition;

.field readerMessageBar:Lcom/squareup/messagebar/api/ReaderMessageBar;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final rootLayoutTransition:Lcom/squareup/rootview/RootLayoutTransition;

.field rootViewBinder:Lcom/squareup/rootview/RootViewBinder;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field touchEventMonitor:Lcom/squareup/ui/TouchEventMonitor;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field tutorialCoordinator:Lcom/squareup/tutorialv2/TutorialCoordinator;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 61
    invoke-direct {p0, p1, p2}, Lcom/squareup/container/ContainerRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    new-instance p2, Lcom/squareup/rootview/RootLayoutTransition;

    invoke-direct {p2}, Lcom/squareup/rootview/RootLayoutTransition;-><init>()V

    iput-object p2, p0, Lcom/squareup/rootview/RootView;->rootLayoutTransition:Lcom/squareup/rootview/RootLayoutTransition;

    .line 56
    new-instance p2, Lcom/squareup/rootview/RootLayoutTransition;

    invoke-direct {p2}, Lcom/squareup/rootview/RootLayoutTransition;-><init>()V

    iput-object p2, p0, Lcom/squareup/rootview/RootView;->messageBarsLayoutTransition:Lcom/squareup/rootview/RootLayoutTransition;

    .line 62
    const-class p2, Lcom/squareup/rootview/RootView$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/rootview/RootView$Component;

    invoke-interface {p1, p0}, Lcom/squareup/rootview/RootView$Component;->inject(Lcom/squareup/rootview/RootView;)V

    return-void
.end method

.method private bindViews()V
    .locals 2

    .line 196
    sget v0, Lcom/squareup/rootview/R$id;->root_body_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/rootview/RootView;->bodyContainer:Landroid/view/ViewGroup;

    .line 197
    sget v0, Lcom/squareup/rootview/R$id;->root_master_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/rootview/RootView;->masterContainer:Landroid/view/ViewGroup;

    .line 198
    sget v0, Lcom/squareup/rootview/R$id;->root_message_bars:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/rootview/RootView;->messageBars:Landroid/view/ViewGroup;

    .line 199
    sget v0, Lcom/squareup/rootview/R$id;->root_card_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/CardScreenContainer;

    iput-object v0, p0, Lcom/squareup/rootview/RootView;->cardScreenContainer:Lcom/squareup/container/CardScreenContainer;

    .line 200
    sget v0, Lcom/squareup/rootview/R$id;->root_fullsheet_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/rootview/RootView;->fullSheetContainer:Landroid/view/ViewGroup;

    .line 201
    sget v0, Lcom/squareup/rootview/R$id;->root_card_over_sheet_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/CardScreenContainer;

    iput-object v0, p0, Lcom/squareup/rootview/RootView;->cardOverSheetContainer:Lcom/squareup/container/CardScreenContainer;

    .line 203
    sget v0, Lcom/squareup/rootview/R$id;->tutorial2_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 205
    new-instance v1, Lcom/squareup/rootview/-$$Lambda$RootView$sm6ev1ndCfgHx13MkI-LbFsOotg;

    invoke-direct {v1, p0}, Lcom/squareup/rootview/-$$Lambda$RootView$sm6ev1ndCfgHx13MkI-LbFsOotg;-><init>(Lcom/squareup/rootview/RootView;)V

    invoke-static {v0, v1}, Lcom/squareup/coordinators/Coordinators;->bind(Landroid/view/View;Lcom/squareup/coordinators/CoordinatorProvider;)V

    :cond_0
    return-void
.end method

.method private getCurrentChild()Landroid/view/View;
    .locals 2

    .line 192
    invoke-virtual {p0}, Lcom/squareup/rootview/RootView;->getContentLayout()Landroid/view/ViewGroup;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public disableLayoutTransitions()V
    .locals 2

    const/4 v0, 0x0

    .line 161
    invoke-virtual {p0, v0}, Lcom/squareup/rootview/RootView;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 162
    iget-object v1, p0, Lcom/squareup/rootview/RootView;->messageBars:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    return-void
.end method

.method public enableLayoutTransitions()V
    .locals 2

    .line 156
    iget-object v0, p0, Lcom/squareup/rootview/RootView;->rootLayoutTransition:Lcom/squareup/rootview/RootLayoutTransition;

    invoke-virtual {p0, v0}, Lcom/squareup/rootview/RootView;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 157
    iget-object v0, p0, Lcom/squareup/rootview/RootView;->messageBars:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/squareup/rootview/RootView;->messageBarsLayoutTransition:Lcom/squareup/rootview/RootLayoutTransition;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    return-void
.end method

.method public finishEnterFullScreen()V
    .locals 2

    .line 172
    iget-object v0, p0, Lcom/squareup/rootview/RootView;->messageBars:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 173
    iput-boolean v1, p0, Lcom/squareup/rootview/RootView;->enteringFullScreen:Z

    .line 174
    invoke-virtual {p0}, Lcom/squareup/rootview/RootView;->enableLayoutTransitions()V

    return-void
.end method

.method public getCardLayout()Landroid/view/ViewGroup;
    .locals 1

    .line 90
    iget-object v0, p0, Lcom/squareup/rootview/RootView;->cardScreenContainer:Lcom/squareup/container/CardScreenContainer;

    invoke-interface {v0}, Lcom/squareup/container/CardScreenContainer;->asViewGroup()Landroid/view/ViewGroup;

    move-result-object v0

    return-object v0
.end method

.method public getCardOverSheetLayout()Landroid/view/ViewGroup;
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/squareup/rootview/RootView;->cardOverSheetContainer:Lcom/squareup/container/CardScreenContainer;

    invoke-interface {v0}, Lcom/squareup/container/CardScreenContainer;->asViewGroup()Landroid/view/ViewGroup;

    move-result-object v0

    return-object v0
.end method

.method public getContentLayout()Landroid/view/ViewGroup;
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/rootview/RootView;->bodyContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public getFullSheetLayout()Landroid/view/ViewGroup;
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/squareup/rootview/RootView;->fullSheetContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public getMasterLayout()Landroid/view/ViewGroup;
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/squareup/rootview/RootView;->masterContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public synthetic lambda$bindViews$0$RootView(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 0

    .line 206
    iget-object p1, p0, Lcom/squareup/rootview/RootView;->tutorialCoordinator:Lcom/squareup/tutorialv2/TutorialCoordinator;

    invoke-virtual {p1, p0}, Lcom/squareup/tutorialv2/TutorialCoordinator;->setRootView(Landroid/view/View;)V

    .line 207
    iget-object p1, p0, Lcom/squareup/rootview/RootView;->tutorialCoordinator:Lcom/squareup/tutorialv2/TutorialCoordinator;

    return-object p1
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/rootview/RootView;->mainContainer:Lcom/squareup/container/ContainerPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/container/ContainerPresenter;->dropView(Lcom/squareup/container/ContainerView;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/rootview/RootView;->rootViewBinder:Lcom/squareup/rootview/RootViewBinder;

    invoke-interface {v0, p0}, Lcom/squareup/rootview/RootViewBinder;->onDetached(Lcom/squareup/rootview/RootView;)V

    .line 82
    invoke-super {p0}, Lcom/squareup/container/ContainerRelativeLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 66
    invoke-super {p0}, Lcom/squareup/container/ContainerRelativeLayout;->onFinishInflate()V

    .line 67
    invoke-direct {p0}, Lcom/squareup/rootview/RootView;->bindViews()V

    .line 69
    iget-object v0, p0, Lcom/squareup/rootview/RootView;->readerMessageBar:Lcom/squareup/messagebar/api/ReaderMessageBar;

    iget-object v1, p0, Lcom/squareup/rootview/RootView;->messageBars:Landroid/view/ViewGroup;

    invoke-interface {v0, v1}, Lcom/squareup/messagebar/api/ReaderMessageBar;->inflateInto(Landroid/view/ViewGroup;)V

    .line 71
    invoke-virtual {p0}, Lcom/squareup/rootview/RootView;->enableLayoutTransitions()V

    .line 75
    iget-object v0, p0, Lcom/squareup/rootview/RootView;->rootViewBinder:Lcom/squareup/rootview/RootViewBinder;

    invoke-interface {v0, p0}, Lcom/squareup/rootview/RootViewBinder;->onAttached(Lcom/squareup/rootview/RootView;)V

    .line 76
    iget-object v0, p0, Lcom/squareup/rootview/RootView;->mainContainer:Lcom/squareup/container/ContainerPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/container/ContainerPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 136
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/squareup/rootview/RootView;->touchEventMonitor:Lcom/squareup/ui/TouchEventMonitor;

    invoke-interface {v0}, Lcom/squareup/ui/TouchEventMonitor;->onTouchEvent()V

    .line 139
    :cond_0
    invoke-super {p0, p1}, Lcom/squareup/container/ContainerRelativeLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .line 185
    invoke-direct {p0}, Lcom/squareup/rootview/RootView;->getCurrentChild()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 186
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lmortar/MortarScope;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object v0

    invoke-virtual {v0}, Lmortar/MortarScope;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 187
    :cond_0
    invoke-super/range {p0 .. p5}, Lcom/squareup/container/ContainerRelativeLayout;->onLayout(ZIIII)V

    :cond_1
    return-void
.end method

.method public onRequestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2

    .line 143
    iget-object v0, p0, Lcom/squareup/rootview/RootView;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    invoke-interface {v0}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/rootview/RootView;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->IGNORE_ACCESSIBILITY_SCRUBBER:Lcom/squareup/settings/server/Features$Feature;

    .line 144
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 145
    invoke-static {p2}, Lcom/squareup/accessibility/AccessibilityScrubber;->getPiiRecord(Landroid/view/accessibility/AccessibilityEvent;)Landroid/view/accessibility/AccessibilityRecord;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 147
    invoke-static {p0, p2, v0}, Lcom/squareup/accessibility/AccessibilityScrubber;->sendPiiScrubbedEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;Landroid/view/accessibility/AccessibilityRecord;)V

    const/4 p1, 0x0

    return p1

    .line 152
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/squareup/container/ContainerRelativeLayout;->onRequestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result p1

    return p1
.end method

.method public requestFocus(ILandroid/graphics/Rect;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public setCardOverSheetVisible(ZZLflow/TraversalCallback;)V
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/squareup/rootview/RootView;->cardOverSheetContainer:Lcom/squareup/container/CardScreenContainer;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/container/CardScreenContainer;->setCardVisible(ZZLflow/TraversalCallback;)V

    .line 119
    invoke-static {p0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method public setCardVisible(ZZLflow/TraversalCallback;)V
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/rootview/RootView;->cardScreenContainer:Lcom/squareup/container/CardScreenContainer;

    if-eqz p2, :cond_0

    iget-boolean p2, p0, Lcom/squareup/rootview/RootView;->enteringFullScreen:Z

    if-nez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/container/CardScreenContainer;->setCardVisible(ZZLflow/TraversalCallback;)V

    .line 97
    invoke-static {p0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method public setFullSheetVisible(ZZLflow/TraversalCallback;)V
    .locals 2

    .line 107
    iget-object v0, p0, Lcom/squareup/rootview/RootView;->fullSheetContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/squareup/rootview/RootView;->bodyContainer:Landroid/view/ViewGroup;

    invoke-static {v0, v1, p1, p2, p3}, Lcom/squareup/container/CardScreenContainers;->setCardVisible(Landroid/view/ViewGroup;Landroid/view/View;ZZLflow/TraversalCallback;)V

    return-void
.end method

.method public startEnterFullScreen()V
    .locals 2

    const/4 v0, 0x1

    .line 166
    iput-boolean v0, p0, Lcom/squareup/rootview/RootView;->enteringFullScreen:Z

    .line 167
    invoke-virtual {p0}, Lcom/squareup/rootview/RootView;->disableLayoutTransitions()V

    .line 168
    iget-object v0, p0, Lcom/squareup/rootview/RootView;->messageBars:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method
