.class public interface abstract Lcom/squareup/permissions/EmployeeModel$Get_all_employeesModel;
.super Ljava/lang/Object;
.source "EmployeeModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/permissions/EmployeeModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Get_all_employeesModel"
.end annotation


# virtual methods
.method public abstract active()Ljava/lang/Boolean;
.end method

.method public abstract can_access_register_with_passcode()Ljava/lang/Boolean;
.end method

.method public abstract can_track_time()Ljava/lang/Boolean;
.end method

.method public abstract clockin_time()Ljava/lang/String;
.end method

.method public abstract employee_number()Ljava/lang/String;
.end method

.method public abstract first_name()Ljava/lang/String;
.end method

.method public abstract group_concat_permission()Ljava/lang/String;
.end method

.method public abstract hashed_passcode()Ljava/lang/String;
.end method

.method public abstract is_account_owner()Ljava/lang/Boolean;
.end method

.method public abstract is_owner()Ljava/lang/Boolean;
.end method

.method public abstract iterations()Ljava/lang/Integer;
.end method

.method public abstract last_name()Ljava/lang/String;
.end method

.method public abstract role_token()Ljava/lang/String;
.end method

.method public abstract salt()Ljava/lang/String;
.end method

.method public abstract timecard_id()Ljava/lang/String;
.end method

.method public abstract token()Ljava/lang/String;
.end method
