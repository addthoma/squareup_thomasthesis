.class public Lcom/squareup/permissions/EmployeeManagementModeDecider;
.super Ljava/lang/Object;
.source "EmployeeManagementModeDecider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;
    }
.end annotation


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final isReaderSdk:Z

.field private mode:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

.field private final modeRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/EmployeeManagementSettings;Z)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 69
    iput-object v0, p0, Lcom/squareup/permissions/EmployeeManagementModeDecider;->mode:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    .line 71
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/permissions/EmployeeManagementModeDecider;->modeRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 77
    iput-object p1, p0, Lcom/squareup/permissions/EmployeeManagementModeDecider;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 78
    iput-object p2, p0, Lcom/squareup/permissions/EmployeeManagementModeDecider;->features:Lcom/squareup/settings/server/Features;

    .line 79
    iput-object p3, p0, Lcom/squareup/permissions/EmployeeManagementModeDecider;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    .line 80
    iput-boolean p4, p0, Lcom/squareup/permissions/EmployeeManagementModeDecider;->isReaderSdk:Z

    return-void
.end method

.method private decideMode()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;
    .locals 1

    .line 163
    iget-boolean v0, p0, Lcom/squareup/permissions/EmployeeManagementModeDecider;->isReaderSdk:Z

    if-eqz v0, :cond_0

    .line 164
    sget-object v0, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->OWNER:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    return-object v0

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/squareup/permissions/EmployeeManagementModeDecider;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getEmployeeSettings()Lcom/squareup/settings/server/EmployeeSettings;

    move-result-object v0

    .line 168
    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeSettings;->getEmployeeToken()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 169
    sget-object v0, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->EMPLOYEE_LOGIN:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    return-object v0

    .line 172
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->isPasscodeEmployeeManagementModeAllowed()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/permissions/EmployeeManagementModeDecider;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    .line 173
    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->isPasscodeEmployeeManagementEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 174
    sget-object v0, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->PASSCODE_EMPLOYEE_MANAGEMENT:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    return-object v0

    .line 177
    :cond_2
    sget-object v0, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->OWNER:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    return-object v0
.end method

.method private setModeInternal(Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;)V
    .locals 1

    .line 148
    iput-object p1, p0, Lcom/squareup/permissions/EmployeeManagementModeDecider;->mode:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    .line 149
    iget-object p1, p0, Lcom/squareup/permissions/EmployeeManagementModeDecider;->modeRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/permissions/EmployeeManagementModeDecider;->mode:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private transitionMode(Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;)Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;
    .locals 5

    .line 153
    iget-object v0, p0, Lcom/squareup/permissions/EmployeeManagementModeDecider;->mode:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    const-string v1, "Mode must be set to be transitioned."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 154
    iget-object v0, p0, Lcom/squareup/permissions/EmployeeManagementModeDecider;->mode:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v3, 0x2

    new-array v4, v3, [Ljava/lang/Object;

    aput-object p1, v4, v2

    aput-object p2, v4, v1

    const-string p1, "Expected mode %s in order to be transitioned to %s."

    invoke-static {v0, p1, v4}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 155
    invoke-direct {p0}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->decideMode()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    move-result-object p1

    if-ne p1, p2, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    new-array v3, v3, [Ljava/lang/Object;

    aput-object p2, v3, v2

    aput-object p1, v3, v1

    const-string p2, "Expected to transition to mode %s, decider decided on %s instead."

    .line 156
    invoke-static {v0, p2, v3}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 158
    invoke-direct {p0, p1}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->setModeInternal(Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;)V

    .line 159
    iget-object p1, p0, Lcom/squareup/permissions/EmployeeManagementModeDecider;->mode:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    return-object p1
.end method


# virtual methods
.method public downgradeModeFromPasscodeEmployeeManagementToOwner()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;
    .locals 2

    .line 129
    sget-object v0, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->PASSCODE_EMPLOYEE_MANAGEMENT:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    sget-object v1, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->OWNER:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    invoke-direct {p0, v0, v1}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->transitionMode(Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;)Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    move-result-object v0

    return-object v0
.end method

.method public getMode()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/permissions/EmployeeManagementModeDecider;->mode:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    return-object v0
.end method

.method public isPasscodeEmployeeManagementModeAllowed()Z
    .locals 6

    .line 105
    iget-object v0, p0, Lcom/squareup/permissions/EmployeeManagementModeDecider;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getEmployeeSettings()Lcom/squareup/settings/server/EmployeeSettings;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeSettings;->isEmployeeManagementEnabledForAccount()Ljava/lang/Boolean;

    move-result-object v1

    .line 107
    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeSettings;->getEmployeesEntity()Lcom/squareup/server/account/protos/EmployeesEntity;

    move-result-object v0

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    .line 108
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/EmployeesEntity;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 109
    :goto_0
    iget-object v4, p0, Lcom/squareup/permissions/EmployeeManagementModeDecider;->features:Lcom/squareup/settings/server/Features;

    sget-object v5, Lcom/squareup/settings/server/Features$Feature;->EMPLOYEE_MANAGEMENT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v4, v5}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v4

    if-eqz v4, :cond_1

    if-nez v0, :cond_1

    if-eqz v1, :cond_1

    .line 112
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    return v2
.end method

.method public isTimecardOnlyOwnerMode()Z
    .locals 2

    .line 143
    iget-object v0, p0, Lcom/squareup/permissions/EmployeeManagementModeDecider;->mode:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    sget-object v1, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->OWNER:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/permissions/EmployeeManagementModeDecider;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->isTimecardEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public mode()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;",
            ">;"
        }
    .end annotation

    .line 101
    iget-object v0, p0, Lcom/squareup/permissions/EmployeeManagementModeDecider;->modeRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public modeSupportsEmployeeCache()Z
    .locals 2

    .line 139
    iget-object v0, p0, Lcom/squareup/permissions/EmployeeManagementModeDecider;->mode:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    sget-object v1, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->PASSCODE_EMPLOYEE_MANAGEMENT:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    if-eq v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->isTimecardOnlyOwnerMode()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public updateMode()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/squareup/permissions/EmployeeManagementModeDecider;->mode:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    if-nez v0, :cond_0

    .line 122
    invoke-direct {p0}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->decideMode()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->setModeInternal(Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;)V

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/squareup/permissions/EmployeeManagementModeDecider;->mode:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    return-object v0
.end method

.method public upgradeModeFromOwnerToPasscodeEmployeeManagement()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;
    .locals 2

    .line 134
    sget-object v0, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->OWNER:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    sget-object v1, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->PASSCODE_EMPLOYEE_MANAGEMENT:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    invoke-direct {p0, v0, v1}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->transitionMode(Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;)Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    move-result-object v0

    return-object v0
.end method
