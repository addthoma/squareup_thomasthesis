.class public Lcom/squareup/permissions/EmployeesServiceHelper;
.super Ljava/lang/Object;
.source "EmployeesServiceHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/permissions/EmployeesServiceHelper$HoursMinutes;
    }
.end annotation


# static fields
.field public static final STATUS_OK:Ljava/lang/String; = "OK"


# instance fields
.field private final employeesService:Lcom/squareup/server/employees/EmployeesService;

.field private final mainScheduler:Lrx/Scheduler;


# direct methods
.method public constructor <init>(Lcom/squareup/server/employees/EmployeesService;Lrx/Scheduler;)V
    .locals 0
    .param p2    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/permissions/EmployeesServiceHelper;->employeesService:Lcom/squareup/server/employees/EmployeesService;

    .line 34
    iput-object p2, p0, Lcom/squareup/permissions/EmployeesServiceHelper;->mainScheduler:Lrx/Scheduler;

    return-void
.end method

.method public static getTotalTime(Lcom/squareup/server/employees/ClockInOutResponse;)Lcom/squareup/permissions/EmployeesServiceHelper$HoursMinutes;
    .locals 5

    .line 51
    iget-object v0, p0, Lcom/squareup/server/employees/ClockInOutResponse;->timecard:Lcom/squareup/server/employees/ClockInOutResponse$TimeCard;

    iget-object v0, v0, Lcom/squareup/server/employees/ClockInOutResponse$TimeCard;->clockin_time:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 52
    iget-object p0, p0, Lcom/squareup/server/employees/ClockInOutResponse;->timecard:Lcom/squareup/server/employees/ClockInOutResponse$TimeCard;

    iget-object p0, p0, Lcom/squareup/server/employees/ClockInOutResponse$TimeCard;->clockout_time:Ljava/lang/String;

    invoke-static {p0}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p0

    if-eqz v0, :cond_0

    if-eqz p0, :cond_0

    .line 54
    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    sub-long/2addr v1, v3

    .line 57
    sget-object p0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v3

    long-to-int p0, v3

    .line 58
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v0

    long-to-int v1, v0

    rem-int/lit8 v1, v1, 0x3c

    .line 60
    new-instance v0, Lcom/squareup/permissions/EmployeesServiceHelper$HoursMinutes;

    invoke-direct {v0, p0, v1}, Lcom/squareup/permissions/EmployeesServiceHelper$HoursMinutes;-><init>(II)V

    return-object v0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method


# virtual methods
.method public clockIn(Ljava/lang/String;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/server/employees/ClockInOutResponse;",
            ">;"
        }
    .end annotation

    .line 38
    invoke-static {p1}, Lcom/squareup/server/employees/ClockInOutBody;->createClockIn(Ljava/lang/String;)Lcom/squareup/server/employees/ClockInOutBody;

    move-result-object p1

    .line 40
    iget-object v0, p0, Lcom/squareup/permissions/EmployeesServiceHelper;->employeesService:Lcom/squareup/server/employees/EmployeesService;

    invoke-interface {v0, p1}, Lcom/squareup/server/employees/EmployeesService;->clockIn(Lcom/squareup/server/employees/ClockInOutBody;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/permissions/EmployeesServiceHelper;->mainScheduler:Lrx/Scheduler;

    invoke-virtual {p1, v0}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public clockOut(Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/server/employees/ClockInOutResponse;",
            ">;"
        }
    .end annotation

    .line 44
    invoke-static {p1, p2}, Lcom/squareup/server/employees/ClockInOutBody;->createClockOut(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/server/employees/ClockInOutBody;

    move-result-object p1

    .line 46
    iget-object p2, p0, Lcom/squareup/permissions/EmployeesServiceHelper;->employeesService:Lcom/squareup/server/employees/EmployeesService;

    invoke-interface {p2, p1}, Lcom/squareup/server/employees/EmployeesService;->clockOut(Lcom/squareup/server/employees/ClockInOutBody;)Lrx/Observable;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/permissions/EmployeesServiceHelper;->mainScheduler:Lrx/Scheduler;

    invoke-virtual {p1, p2}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
