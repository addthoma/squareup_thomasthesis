.class final Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2$2;
.super Ljava/lang/Object;
.source "EmployeeCacheUpdater.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2;->apply(Ljava/lang/Boolean;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/ObservableSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/permissions/Employee;",
        "kotlin.jvm.PlatformType",
        "cursor",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $cursorRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

.field final synthetic this$0:Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2;


# direct methods
.method constructor <init>(Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2;Lcom/jakewharton/rxrelay2/PublishRelay;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2$2;->this$0:Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2;

    iput-object p2, p0, Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2$2;->$cursorRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/permissions/Employee;",
            ">;"
        }
    .end annotation

    const-string v0, "cursor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sentinel_start_value"

    .line 141
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2$2;->this$0:Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2;

    iget-object v0, v0, Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2;->this$0:Lcom/squareup/permissions/EmployeeCacheUpdater;

    invoke-static {v0}, Lcom/squareup/permissions/EmployeeCacheUpdater;->access$getService$p(Lcom/squareup/permissions/EmployeeCacheUpdater;)Lcom/squareup/server/employees/EmployeesService;

    move-result-object v0

    const/16 v1, 0x2710

    invoke-interface {v0, p1, v1}, Lcom/squareup/server/employees/EmployeesService;->getEmployees(Ljava/lang/String;I)Lrx/Observable;

    move-result-object p1

    const-string v0, "service.getEmployees(actualCursor, MAX_RESULTS)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    const/4 v0, 0x3

    const-wide/16 v1, 0x2

    .line 148
    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 149
    iget-object v4, p0, Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2$2;->this$0:Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2;

    iget-object v4, v4, Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2;->this$0:Lcom/squareup/permissions/EmployeeCacheUpdater;

    invoke-static {v4}, Lcom/squareup/permissions/EmployeeCacheUpdater;->access$getMainScheduler$p(Lcom/squareup/permissions/EmployeeCacheUpdater;)Lio/reactivex/Scheduler;

    move-result-object v4

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    .line 145
    invoke-static/range {v0 .. v7}, Lcom/squareup/util/rx2/Rx2RetryStrategies;->exponentialBackoffThenError$default(IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lio/reactivex/functions/Function;

    move-result-object v0

    .line 144
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->retryWhen(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 152
    new-instance v0, Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2$2$1;

    invoke-direct {v0, p0}, Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2$2$1;-><init>(Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2$2;)V

    check-cast v0, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p1

    .line 157
    iget-object v0, p0, Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2$2;->this$0:Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2;

    iget-object v0, v0, Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2;->this$0:Lcom/squareup/permissions/EmployeeCacheUpdater;

    invoke-static {v0}, Lcom/squareup/permissions/EmployeeCacheUpdater;->access$getComputationScheduler$p(Lcom/squareup/permissions/EmployeeCacheUpdater;)Lio/reactivex/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    .line 158
    sget-object v0, Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2$2$2;->INSTANCE:Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2$2$2;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->flatMapIterable(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 159
    sget-object v0, Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2$2$3;->INSTANCE:Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2$2$3;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2$2;->apply(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
