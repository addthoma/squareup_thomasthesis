.class public final synthetic Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$C5sqo-XHu5VS3OXvJH2mqRAnB30;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Predicate;


# instance fields
.field private final synthetic f$0:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private final synthetic f$1:Ljava/util/Set;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/permissions/PasscodeEmployeeManagement;Ljava/util/Set;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$C5sqo-XHu5VS3OXvJH2mqRAnB30;->f$0:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iput-object p2, p0, Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$C5sqo-XHu5VS3OXvJH2mqRAnB30;->f$1:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public final test(Ljava/lang/Object;)Z
    .locals 2

    iget-object v0, p0, Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$C5sqo-XHu5VS3OXvJH2mqRAnB30;->f$0:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iget-object v1, p0, Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$C5sqo-XHu5VS3OXvJH2mqRAnB30;->f$1:Ljava/util/Set;

    check-cast p1, Lcom/squareup/permissions/Employee;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->lambda$getTeamEmployeeForPasscodeAndPermission$6$PasscodeEmployeeManagement(Ljava/util/Set;Lcom/squareup/permissions/Employee;)Z

    move-result p1

    return p1
.end method
