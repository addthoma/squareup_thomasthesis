.class public interface abstract Lcom/squareup/invoicesappletapi/EditInvoiceInTenderRunner;
.super Ljava/lang/Object;
.source "EditInvoiceInTenderRunner.java"


# static fields
.field public static final NOT_SUPPORTED:Lcom/squareup/invoicesappletapi/EditInvoiceInTenderRunner;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 6
    const-class v0, Lcom/squareup/invoicesappletapi/EditInvoiceInTenderRunner;

    .line 7
    invoke-static {v0}, Lcom/squareup/util/Objects;->allMethodsThrowUnsupportedOperation(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoicesappletapi/EditInvoiceInTenderRunner;

    sput-object v0, Lcom/squareup/invoicesappletapi/EditInvoiceInTenderRunner;->NOT_SUPPORTED:Lcom/squareup/invoicesappletapi/EditInvoiceInTenderRunner;

    return-void
.end method


# virtual methods
.method public abstract start()V
.end method
