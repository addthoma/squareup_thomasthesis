.class final Lcom/squareup/googlepay/RealGooglePay$tryToObtainWalletId$1$2;
.super Ljava/lang/Object;
.source "RealGooglePay.kt"

# interfaces
.implements Lcom/google/android/gms/common/api/ResultCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/googlepay/RealGooglePay$tryToObtainWalletId$1;->subscribe(Lio/reactivex/SingleEmitter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R::",
        "Lcom/google/android/gms/common/api/Result;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/common/api/ResultCallback<",
        "Lcom/google/android/gms/tapandpay/TapAndPay$GetActiveWalletIdResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "result",
        "Lcom/google/android/gms/tapandpay/TapAndPay$GetActiveWalletIdResult;",
        "onResult"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $emitter:Lio/reactivex/SingleEmitter;


# direct methods
.method constructor <init>(Lio/reactivex/SingleEmitter;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/googlepay/RealGooglePay$tryToObtainWalletId$1$2;->$emitter:Lio/reactivex/SingleEmitter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onResult(Lcom/google/android/gms/common/api/Result;)V
    .locals 0

    .line 36
    check-cast p1, Lcom/google/android/gms/tapandpay/TapAndPay$GetActiveWalletIdResult;

    invoke-virtual {p0, p1}, Lcom/squareup/googlepay/RealGooglePay$tryToObtainWalletId$1$2;->onResult(Lcom/google/android/gms/tapandpay/TapAndPay$GetActiveWalletIdResult;)V

    return-void
.end method

.method public final onResult(Lcom/google/android/gms/tapandpay/TapAndPay$GetActiveWalletIdResult;)V
    .locals 3

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 186
    iget-object v0, p0, Lcom/squareup/googlepay/RealGooglePay$tryToObtainWalletId$1$2;->$emitter:Lio/reactivex/SingleEmitter;

    const-string v1, "emitter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0}, Lio/reactivex/SingleEmitter;->isDisposed()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 188
    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/tapandpay/TapAndPay$GetActiveWalletIdResult;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    const-string v1, "status"

    .line 189
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->isSuccess()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 190
    invoke-interface {p1}, Lcom/google/android/gms/tapandpay/TapAndPay$GetActiveWalletIdResult;->getActiveWalletId()Ljava/lang/String;

    move-result-object p1

    .line 191
    iget-object v0, p0, Lcom/squareup/googlepay/RealGooglePay$tryToObtainWalletId$1$2;->$emitter:Lio/reactivex/SingleEmitter;

    invoke-interface {v0, p1}, Lio/reactivex/SingleEmitter;->onSuccess(Ljava/lang/Object;)V

    goto :goto_1

    .line 193
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->getStatusCode()I

    move-result p1

    const/16 v1, 0x3a9a

    if-eq p1, v1, :cond_3

    const/16 v1, 0x3aa1

    if-eq p1, v1, :cond_2

    .line 196
    sget-object p1, Lcom/squareup/googlepay/GooglePayException$UnknownException;->INSTANCE:Lcom/squareup/googlepay/GooglePayException$UnknownException;

    check-cast p1, Lcom/squareup/googlepay/GooglePayException;

    goto :goto_0

    .line 195
    :cond_2
    sget-object p1, Lcom/squareup/googlepay/GooglePayException$GooglePayUnavailableException;->INSTANCE:Lcom/squareup/googlepay/GooglePayException$GooglePayUnavailableException;

    check-cast p1, Lcom/squareup/googlepay/GooglePayException;

    goto :goto_0

    .line 194
    :cond_3
    sget-object p1, Lcom/squareup/googlepay/GooglePayException$WalletIdNotFoundException;->INSTANCE:Lcom/squareup/googlepay/GooglePayException$WalletIdNotFoundException;

    check-cast p1, Lcom/squareup/googlepay/GooglePayException;

    .line 198
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failure: tryToObtainWalletId :: status code = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->getStatusCode()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ltimber/log/Timber;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 201
    iget-object v0, p0, Lcom/squareup/googlepay/RealGooglePay$tryToObtainWalletId$1$2;->$emitter:Lio/reactivex/SingleEmitter;

    check-cast p1, Ljava/lang/Throwable;

    invoke-interface {v0, p1}, Lio/reactivex/SingleEmitter;->onError(Ljava/lang/Throwable;)V

    :goto_1
    return-void
.end method
