.class public final Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;
.super Lcom/squareup/wire/Message;
.source "MerchantAuthCode.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$ProtoAdapter_MerchantAuthCode;,
        Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;",
        "Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_KEYGEN_TIMESTAMP:Ljava/lang/Long;

.field public static final DEFAULT_MASTER_KEY_ID:Ljava/lang/Integer;

.field public static final DEFAULT_PAYLOAD_MAC:Lokio/ByteString;

.field private static final serialVersionUID:J


# instance fields
.field public final keygen_timestamp:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0x2
    .end annotation
.end field

.field public final master_key_id:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x1
    .end annotation
.end field

.field public final payload_mac:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 27
    new-instance v0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$ProtoAdapter_MerchantAuthCode;

    invoke-direct {v0}, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$ProtoAdapter_MerchantAuthCode;-><init>()V

    sput-object v0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 31
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->DEFAULT_MASTER_KEY_ID:Ljava/lang/Integer;

    const-wide/16 v0, 0x0

    .line 33
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->DEFAULT_KEYGEN_TIMESTAMP:Ljava/lang/Long;

    .line 35
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->DEFAULT_PAYLOAD_MAC:Lokio/ByteString;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Long;Lokio/ByteString;)V
    .locals 1

    .line 65
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;-><init>(Ljava/lang/Integer;Ljava/lang/Long;Lokio/ByteString;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Long;Lokio/ByteString;Lokio/ByteString;)V
    .locals 1

    .line 70
    sget-object v0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 71
    iput-object p1, p0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->master_key_id:Ljava/lang/Integer;

    .line 72
    iput-object p2, p0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->keygen_timestamp:Ljava/lang/Long;

    .line 73
    iput-object p3, p0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->payload_mac:Lokio/ByteString;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 89
    :cond_0
    instance-of v1, p1, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 90
    :cond_1
    check-cast p1, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;

    .line 91
    invoke-virtual {p0}, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->master_key_id:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->master_key_id:Ljava/lang/Integer;

    .line 92
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->keygen_timestamp:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->keygen_timestamp:Ljava/lang/Long;

    .line 93
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->payload_mac:Lokio/ByteString;

    iget-object p1, p1, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->payload_mac:Lokio/ByteString;

    .line 94
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 99
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 101
    invoke-virtual {p0}, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 102
    iget-object v1, p0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->master_key_id:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 103
    iget-object v1, p0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->keygen_timestamp:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 104
    iget-object v1, p0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->payload_mac:Lokio/ByteString;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 105
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$Builder;
    .locals 2

    .line 78
    new-instance v0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$Builder;

    invoke-direct {v0}, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$Builder;-><init>()V

    .line 79
    iget-object v1, p0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->master_key_id:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$Builder;->master_key_id:Ljava/lang/Integer;

    .line 80
    iget-object v1, p0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->keygen_timestamp:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$Builder;->keygen_timestamp:Ljava/lang/Long;

    .line 81
    iget-object v1, p0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->payload_mac:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$Builder;->payload_mac:Lokio/ByteString;

    .line 82
    invoke-virtual {p0}, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->newBuilder()Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 113
    iget-object v1, p0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->master_key_id:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", master_key_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->master_key_id:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 114
    :cond_0
    iget-object v1, p0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->keygen_timestamp:Ljava/lang/Long;

    if-eqz v1, :cond_1

    const-string v1, ", keygen_timestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->keygen_timestamp:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 115
    :cond_1
    iget-object v1, p0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->payload_mac:Lokio/ByteString;

    if-eqz v1, :cond_2

    const-string v1, ", payload_mac="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->payload_mac:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "MerchantAuthCode{"

    .line 116
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
