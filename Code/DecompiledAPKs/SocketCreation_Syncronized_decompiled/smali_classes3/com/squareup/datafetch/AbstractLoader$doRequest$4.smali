.class final Lcom/squareup/datafetch/AbstractLoader$doRequest$4;
.super Ljava/lang/Object;
.source "AbstractLoader.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/datafetch/AbstractLoader;->doRequest(Lmortar/MortarScope;Ljava/lang/Object;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a(\u0012$\u0012\"\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u0004 \u0005*\u0010\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u0004\u0018\u00010\u00020\u00020\u0001\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0006\"\u0008\u0008\u0001\u0010\u0004*\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/datafetch/AbstractLoader$Response;",
        "TInput",
        "TItem",
        "kotlin.jvm.PlatformType",
        "",
        "pagingParams",
        "Lcom/squareup/datafetch/AbstractLoader$PagingParams;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $input:Ljava/lang/Object;

.field final synthetic this$0:Lcom/squareup/datafetch/AbstractLoader;


# direct methods
.method constructor <init>(Lcom/squareup/datafetch/AbstractLoader;Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/datafetch/AbstractLoader$doRequest$4;->this$0:Lcom/squareup/datafetch/AbstractLoader;

    iput-object p2, p0, Lcom/squareup/datafetch/AbstractLoader$doRequest$4;->$input:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/datafetch/AbstractLoader$PagingParams;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/datafetch/AbstractLoader$PagingParams;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/datafetch/AbstractLoader$Response<",
            "TTInput;TTItem;>;>;"
        }
    .end annotation

    const-string v0, "pagingParams"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 183
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader$doRequest$4;->this$0:Lcom/squareup/datafetch/AbstractLoader;

    iget-object v1, p0, Lcom/squareup/datafetch/AbstractLoader$doRequest$4;->$input:Ljava/lang/Object;

    new-instance v2, Lcom/squareup/datafetch/AbstractLoader$doRequest$4$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/datafetch/AbstractLoader$doRequest$4$1;-><init>(Lcom/squareup/datafetch/AbstractLoader$doRequest$4;Lcom/squareup/datafetch/AbstractLoader$PagingParams;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1, p1, v2}, Lcom/squareup/datafetch/AbstractLoader;->fetch(Ljava/lang/Object;Lcom/squareup/datafetch/AbstractLoader$PagingParams;Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v0

    .line 186
    new-instance v1, Lcom/squareup/datafetch/AbstractLoader$doRequest$4$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/datafetch/AbstractLoader$doRequest$4$2;-><init>(Lcom/squareup/datafetch/AbstractLoader$doRequest$4;Lcom/squareup/datafetch/AbstractLoader$PagingParams;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 69
    check-cast p1, Lcom/squareup/datafetch/AbstractLoader$PagingParams;

    invoke-virtual {p0, p1}, Lcom/squareup/datafetch/AbstractLoader$doRequest$4;->apply(Lcom/squareup/datafetch/AbstractLoader$PagingParams;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
