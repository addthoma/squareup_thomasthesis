.class public final Lcom/squareup/loggedout/LoggedOutOnboardingStarter_Factory;
.super Ljava/lang/Object;
.source "LoggedOutOnboardingStarter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/loggedout/LoggedOutOnboardingStarter;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final loggedOutFinisherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LoggedOutFinisher;",
            ">;"
        }
    .end annotation
.end field

.field private final onboardingActivityStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingActivityStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final onboardingTypeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingType;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingActivityStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LoggedOutFinisher;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/loggedout/LoggedOutOnboardingStarter_Factory;->applicationProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/squareup/loggedout/LoggedOutOnboardingStarter_Factory;->onboardingTypeProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/loggedout/LoggedOutOnboardingStarter_Factory;->onboardingActivityStarterProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/loggedout/LoggedOutOnboardingStarter_Factory;->loggedOutFinisherProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/loggedout/LoggedOutOnboardingStarter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingType;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingActivityStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LoggedOutFinisher;",
            ">;)",
            "Lcom/squareup/loggedout/LoggedOutOnboardingStarter_Factory;"
        }
    .end annotation

    .line 47
    new-instance v0, Lcom/squareup/loggedout/LoggedOutOnboardingStarter_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/loggedout/LoggedOutOnboardingStarter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Landroid/app/Application;Lcom/squareup/onboarding/OnboardingType;Lcom/squareup/onboarding/OnboardingActivityStarter;Lcom/squareup/ui/loggedout/LoggedOutFinisher;)Lcom/squareup/loggedout/LoggedOutOnboardingStarter;
    .locals 1

    .line 53
    new-instance v0, Lcom/squareup/loggedout/LoggedOutOnboardingStarter;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/loggedout/LoggedOutOnboardingStarter;-><init>(Landroid/app/Application;Lcom/squareup/onboarding/OnboardingType;Lcom/squareup/onboarding/OnboardingActivityStarter;Lcom/squareup/ui/loggedout/LoggedOutFinisher;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/loggedout/LoggedOutOnboardingStarter;
    .locals 4

    .line 40
    iget-object v0, p0, Lcom/squareup/loggedout/LoggedOutOnboardingStarter_Factory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/loggedout/LoggedOutOnboardingStarter_Factory;->onboardingTypeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/onboarding/OnboardingType;

    iget-object v2, p0, Lcom/squareup/loggedout/LoggedOutOnboardingStarter_Factory;->onboardingActivityStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/onboarding/OnboardingActivityStarter;

    iget-object v3, p0, Lcom/squareup/loggedout/LoggedOutOnboardingStarter_Factory;->loggedOutFinisherProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/loggedout/LoggedOutFinisher;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/loggedout/LoggedOutOnboardingStarter_Factory;->newInstance(Landroid/app/Application;Lcom/squareup/onboarding/OnboardingType;Lcom/squareup/onboarding/OnboardingActivityStarter;Lcom/squareup/ui/loggedout/LoggedOutFinisher;)Lcom/squareup/loggedout/LoggedOutOnboardingStarter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/loggedout/LoggedOutOnboardingStarter_Factory;->get()Lcom/squareup/loggedout/LoggedOutOnboardingStarter;

    move-result-object v0

    return-object v0
.end method
