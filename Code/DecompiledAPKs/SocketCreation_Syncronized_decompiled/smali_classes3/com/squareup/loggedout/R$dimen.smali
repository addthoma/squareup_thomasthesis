.class public final Lcom/squareup/loggedout/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loggedout/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final landing_button_height:I = 0x7f0701e5

.field public static final landing_gap:I = 0x7f0701e6

.field public static final reader_jack_size:I = 0x7f070450

.field public static final splash_page_button_bar_bottom_margin:I = 0x7f0704cf

.field public static final splash_page_button_bar_button_height:I = 0x7f0704d0

.field public static final splash_page_button_bar_button_text_size:I = 0x7f0704d1

.field public static final splash_page_button_bar_button_top_margin:I = 0x7f0704d2

.field public static final splash_page_button_bar_page_indicator_bottom_margin:I = 0x7f0704d3

.field public static final splash_page_button_bar_side_margin:I = 0x7f0704d4

.field public static final splash_page_button_bar_width:I = 0x7f0704d5

.field public static final splash_page_image_bottom_margin:I = 0x7f0704d6

.field public static final splash_page_logo_size:I = 0x7f0704d7

.field public static final splash_page_logo_top_margin:I = 0x7f0704d8

.field public static final splash_page_text_bottom_margin:I = 0x7f0704d9

.field public static final splash_page_text_line_height_multiplier:I = 0x7f0704da

.field public static final splash_page_text_side_margin:I = 0x7f0704db

.field public static final splash_page_text_size:I = 0x7f0704dc

.field public static final splash_page_text_width:I = 0x7f0704dd

.field public static final text_above_image_splash_page_image_top_margin:I = 0x7f070523

.field public static final text_above_image_splash_page_logo_top_margin:I = 0x7f070524

.field public static final text_above_image_splash_page_text_top_margin:I = 0x7f070525

.field public static final world_landing_horizontal_padding:I = 0x7f070570

.field public static final world_landing_vertical_padding:I = 0x7f070571


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
