.class final Lcom/squareup/loggedout/LoggedOutOnboardingStarter$onEnterScope$1;
.super Ljava/lang/Object;
.source "LoggedOutOnboardingStarter.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loggedout/LoggedOutOnboardingStarter;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/loggedout/LoggedOutOnboardingStarter$OnboardingMethod;",
        "kotlin.jvm.PlatformType",
        "params",
        "Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/loggedout/LoggedOutOnboardingStarter;


# direct methods
.method constructor <init>(Lcom/squareup/loggedout/LoggedOutOnboardingStarter;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/loggedout/LoggedOutOnboardingStarter$onEnterScope$1;->this$0:Lcom/squareup/loggedout/LoggedOutOnboardingStarter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/loggedout/LoggedOutOnboardingStarter$OnboardingMethod;",
            ">;"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lcom/squareup/loggedout/LoggedOutOnboardingStarter$onEnterScope$1;->this$0:Lcom/squareup/loggedout/LoggedOutOnboardingStarter;

    invoke-static {v0}, Lcom/squareup/loggedout/LoggedOutOnboardingStarter;->access$getOnboardingType$p(Lcom/squareup/loggedout/LoggedOutOnboardingStarter;)Lcom/squareup/onboarding/OnboardingType;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/onboarding/OnboardingType;->variant()Lio/reactivex/Observable;

    move-result-object v0

    .line 44
    new-instance v1, Lcom/squareup/loggedout/LoggedOutOnboardingStarter$onEnterScope$1$1;

    invoke-direct {v1, p1}, Lcom/squareup/loggedout/LoggedOutOnboardingStarter$onEnterScope$1$1;-><init>(Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 45
    invoke-virtual {p1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 28
    check-cast p1, Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;

    invoke-virtual {p0, p1}, Lcom/squareup/loggedout/LoggedOutOnboardingStarter$onEnterScope$1;->apply(Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
