.class public interface abstract Lcom/squareup/notificationcenterdata/impl/Database;
.super Ljava/lang/Object;
.source "Database.kt"

# interfaces
.implements Lcom/squareup/sqldelight/Transacter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/notificationcenterdata/impl/Database$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008f\u0018\u0000 \u00062\u00020\u0001:\u0001\u0006R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/notificationcenterdata/impl/Database;",
        "Lcom/squareup/sqldelight/Transacter;",
        "notificationStateQueries",
        "Lcom/squareup/notificationcenterdata/db/NotificationStateQueries;",
        "getNotificationStateQueries",
        "()Lcom/squareup/notificationcenterdata/db/NotificationStateQueries;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/notificationcenterdata/impl/Database$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/notificationcenterdata/impl/Database$Companion;->$$INSTANCE:Lcom/squareup/notificationcenterdata/impl/Database$Companion;

    sput-object v0, Lcom/squareup/notificationcenterdata/impl/Database;->Companion:Lcom/squareup/notificationcenterdata/impl/Database$Companion;

    return-void
.end method


# virtual methods
.method public abstract getNotificationStateQueries()Lcom/squareup/notificationcenterdata/db/NotificationStateQueries;
.end method
