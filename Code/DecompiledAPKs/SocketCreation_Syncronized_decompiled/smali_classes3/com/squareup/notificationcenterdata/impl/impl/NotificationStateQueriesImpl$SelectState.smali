.class final Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectState;
.super Lcom/squareup/sqldelight/Query;
.source "DatabaseImpl.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SelectState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/sqldelight/Query<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0082\u0004\u0018\u0000*\n\u0008\u0000\u0010\u0001 \u0001*\u00020\u00022\u0008\u0012\u0004\u0012\u0002H\u00010\u0003B)\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0012\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00028\u00000\t\u00a2\u0006\u0002\u0010\u000bJ\u0008\u0010\u000c\u001a\u00020\nH\u0016J\u0008\u0010\r\u001a\u00020\u0005H\u0016R\u0010\u0010\u0004\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectState;",
        "T",
        "",
        "Lcom/squareup/sqldelight/Query;",
        "id",
        "",
        "source",
        "Lcom/squareup/notificationcenterdata/Notification$Source;",
        "mapper",
        "Lkotlin/Function1;",
        "Lcom/squareup/sqldelight/db/SqlCursor;",
        "(Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$Source;Lkotlin/jvm/functions/Function1;)V",
        "execute",
        "toString",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public final id:Ljava/lang/String;

.field public final source:Lcom/squareup/notificationcenterdata/Notification$Source;

.field final synthetic this$0:Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;


# direct methods
.method public constructor <init>(Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$Source;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/notificationcenterdata/Notification$Source;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/sqldelight/db/SqlCursor;",
            "+TT;>;)V"
        }
    .end annotation

    const-string v0, "id"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mapper"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    iput-object p1, p0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectState;->this$0:Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;

    .line 117
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;->getSelectState$impl_release()Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1, p4}, Lcom/squareup/sqldelight/Query;-><init>(Ljava/util/List;Lkotlin/jvm/functions/Function1;)V

    iput-object p2, p0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectState;->id:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectState;->source:Lcom/squareup/notificationcenterdata/Notification$Source;

    return-void
.end method


# virtual methods
.method public execute()Lcom/squareup/sqldelight/db/SqlCursor;
    .locals 5

    .line 118
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectState;->this$0:Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;

    invoke-static {v0}, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;->access$getDriver$p(Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;)Lcom/squareup/sqldelight/db/SqlDriver;

    move-result-object v0

    const v1, 0x1444e802

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 122
    new-instance v2, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectState$execute$1;

    invoke-direct {v2, p0}, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectState$execute$1;-><init>(Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$SelectState;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    const-string v3, "SELECT state\nFROM notification_states\nWHERE id = ?1 AND source = ?2"

    const/4 v4, 0x2

    .line 118
    invoke-interface {v0, v1, v3, v4, v2}, Lcom/squareup/sqldelight/db/SqlDriver;->executeQuery(Ljava/lang/Integer;Ljava/lang/String;ILkotlin/jvm/functions/Function1;)Lcom/squareup/sqldelight/db/SqlCursor;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    const-string v0, "NotificationState.sq:selectState"

    return-object v0
.end method
