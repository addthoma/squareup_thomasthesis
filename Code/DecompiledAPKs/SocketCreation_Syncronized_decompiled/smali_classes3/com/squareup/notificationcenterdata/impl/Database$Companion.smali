.class public final Lcom/squareup/notificationcenterdata/impl/Database$Companion;
.super Ljava/lang/Object;
.source "Database.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/notificationcenterdata/impl/Database;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0019\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0086\u0002R\u0011\u0010\u0003\u001a\u00020\u00048F\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/notificationcenterdata/impl/Database$Companion;",
        "",
        "()V",
        "Schema",
        "Lcom/squareup/sqldelight/db/SqlDriver$Schema;",
        "getSchema",
        "()Lcom/squareup/sqldelight/db/SqlDriver$Schema;",
        "invoke",
        "Lcom/squareup/notificationcenterdata/impl/Database;",
        "driver",
        "Lcom/squareup/sqldelight/db/SqlDriver;",
        "notification_statesAdapter",
        "Lcom/squareup/notificationcenterdata/db/Notification_states$Adapter;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$INSTANCE:Lcom/squareup/notificationcenterdata/impl/Database$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 13
    new-instance v0, Lcom/squareup/notificationcenterdata/impl/Database$Companion;

    invoke-direct {v0}, Lcom/squareup/notificationcenterdata/impl/Database$Companion;-><init>()V

    sput-object v0, Lcom/squareup/notificationcenterdata/impl/Database$Companion;->$$INSTANCE:Lcom/squareup/notificationcenterdata/impl/Database$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getSchema()Lcom/squareup/sqldelight/db/SqlDriver$Schema;
    .locals 1

    .line 15
    const-class v0, Lcom/squareup/notificationcenterdata/impl/Database;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImplKt;->getSchema(Lkotlin/reflect/KClass;)Lcom/squareup/sqldelight/db/SqlDriver$Schema;

    move-result-object v0

    return-object v0
.end method

.method public final invoke(Lcom/squareup/sqldelight/db/SqlDriver;Lcom/squareup/notificationcenterdata/db/Notification_states$Adapter;)Lcom/squareup/notificationcenterdata/impl/Database;
    .locals 1

    const-string v0, "driver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notification_statesAdapter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    const-class v0, Lcom/squareup/notificationcenterdata/impl/Database;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImplKt;->newInstance(Lkotlin/reflect/KClass;Lcom/squareup/sqldelight/db/SqlDriver;Lcom/squareup/notificationcenterdata/db/Notification_states$Adapter;)Lcom/squareup/notificationcenterdata/impl/Database;

    move-result-object p1

    return-object p1
.end method
