.class public final Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl$Schema;
.super Ljava/lang/Object;
.source "DatabaseImpl.kt"

# interfaces
.implements Lcom/squareup/sqldelight/db/SqlDriver$Schema;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Schema"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016J \u0010\u000b\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000c\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u0004H\u0016R\u0014\u0010\u0003\u001a\u00020\u00048VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl$Schema;",
        "Lcom/squareup/sqldelight/db/SqlDriver$Schema;",
        "()V",
        "version",
        "",
        "getVersion",
        "()I",
        "create",
        "",
        "driver",
        "Lcom/squareup/sqldelight/db/SqlDriver;",
        "migrate",
        "oldVersion",
        "newVersion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl$Schema;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl$Schema;

    invoke-direct {v0}, Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl$Schema;-><init>()V

    sput-object v0, Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl$Schema;->INSTANCE:Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl$Schema;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Lcom/squareup/sqldelight/db/SqlDriver;)V
    .locals 8

    const-string v0, "driver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const-string v3, "CREATE TABLE notification_states (\n  id TEXT NOT NULL PRIMARY KEY,\n  source TEXT NOT NULL,\n  state TEXT NOT NULL\n)"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v1, p1

    .line 38
    invoke-static/range {v1 .. v7}, Lcom/squareup/sqldelight/db/SqlDriver$DefaultImpls;->execute$default(Lcom/squareup/sqldelight/db/SqlDriver;Ljava/lang/Integer;Ljava/lang/String;ILkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public getVersion()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public migrate(Lcom/squareup/sqldelight/db/SqlDriver;II)V
    .locals 0

    const-string p2, "driver"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
