.class final Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl;
.super Lcom/squareup/sqldelight/TransacterImpl;
.source "DatabaseImpl.kt"

# interfaces
.implements Lcom/squareup/notificationcenterdata/impl/Database;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl$Schema;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002:\u0001\u000eB\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007R\u0014\u0010\u0008\u001a\u00020\tX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0014\u0010\u0005\u001a\u00020\u0006X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\r\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl;",
        "Lcom/squareup/sqldelight/TransacterImpl;",
        "Lcom/squareup/notificationcenterdata/impl/Database;",
        "driver",
        "Lcom/squareup/sqldelight/db/SqlDriver;",
        "notification_statesAdapter",
        "Lcom/squareup/notificationcenterdata/db/Notification_states$Adapter;",
        "(Lcom/squareup/sqldelight/db/SqlDriver;Lcom/squareup/notificationcenterdata/db/Notification_states$Adapter;)V",
        "notificationStateQueries",
        "Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;",
        "getNotificationStateQueries",
        "()Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;",
        "getNotification_statesAdapter$impl_release",
        "()Lcom/squareup/notificationcenterdata/db/Notification_states$Adapter;",
        "Schema",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final notificationStateQueries:Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;

.field private final notification_statesAdapter:Lcom/squareup/notificationcenterdata/db/Notification_states$Adapter;


# direct methods
.method public constructor <init>(Lcom/squareup/sqldelight/db/SqlDriver;Lcom/squareup/notificationcenterdata/db/Notification_states$Adapter;)V
    .locals 1

    const-string v0, "driver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notification_statesAdapter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0, p1}, Lcom/squareup/sqldelight/TransacterImpl;-><init>(Lcom/squareup/sqldelight/db/SqlDriver;)V

    iput-object p2, p0, Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl;->notification_statesAdapter:Lcom/squareup/notificationcenterdata/db/Notification_states$Adapter;

    .line 31
    new-instance p2, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;

    invoke-direct {p2, p0, p1}, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;-><init>(Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl;Lcom/squareup/sqldelight/db/SqlDriver;)V

    iput-object p2, p0, Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl;->notificationStateQueries:Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;

    return-void
.end method


# virtual methods
.method public bridge synthetic getNotificationStateQueries()Lcom/squareup/notificationcenterdata/db/NotificationStateQueries;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl;->getNotificationStateQueries()Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;

    move-result-object v0

    check-cast v0, Lcom/squareup/notificationcenterdata/db/NotificationStateQueries;

    return-object v0
.end method

.method public getNotificationStateQueries()Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl;->notificationStateQueries:Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;

    return-object v0
.end method

.method public final getNotification_statesAdapter$impl_release()Lcom/squareup/notificationcenterdata/db/Notification_states$Adapter;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl;->notification_statesAdapter:Lcom/squareup/notificationcenterdata/db/Notification_states$Adapter;

    return-object v0
.end method
