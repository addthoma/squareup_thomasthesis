.class public final enum Lcom/squareup/notificationcenterdata/Notification$Source;
.super Ljava/lang/Enum;
.source "Notification.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/notificationcenterdata/Notification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Source"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/notificationcenterdata/Notification$Source$Creator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/notificationcenterdata/Notification$Source;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0087\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J\t\u0010\u0004\u001a\u00020\u0005H\u00d6\u0001J\u0019\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0005H\u00d6\u0001j\u0002\u0008\u000bj\u0002\u0008\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/notificationcenterdata/Notification$Source;",
        "",
        "Landroid/os/Parcelable;",
        "(Ljava/lang/String;I)V",
        "describeContents",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "LOCAL",
        "REMOTE",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/notificationcenterdata/Notification$Source;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final enum LOCAL:Lcom/squareup/notificationcenterdata/Notification$Source;

.field public static final enum REMOTE:Lcom/squareup/notificationcenterdata/Notification$Source;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/notificationcenterdata/Notification$Source;

    new-instance v1, Lcom/squareup/notificationcenterdata/Notification$Source;

    const/4 v2, 0x0

    const-string v3, "LOCAL"

    invoke-direct {v1, v3, v2}, Lcom/squareup/notificationcenterdata/Notification$Source;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/notificationcenterdata/Notification$Source;->LOCAL:Lcom/squareup/notificationcenterdata/Notification$Source;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/notificationcenterdata/Notification$Source;

    const/4 v2, 0x1

    const-string v3, "REMOTE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/notificationcenterdata/Notification$Source;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/notificationcenterdata/Notification$Source;->REMOTE:Lcom/squareup/notificationcenterdata/Notification$Source;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/notificationcenterdata/Notification$Source;->$VALUES:[Lcom/squareup/notificationcenterdata/Notification$Source;

    new-instance v0, Lcom/squareup/notificationcenterdata/Notification$Source$Creator;

    invoke-direct {v0}, Lcom/squareup/notificationcenterdata/Notification$Source$Creator;-><init>()V

    sput-object v0, Lcom/squareup/notificationcenterdata/Notification$Source;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 67
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/notificationcenterdata/Notification$Source;
    .locals 1

    const-class v0, Lcom/squareup/notificationcenterdata/Notification$Source;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/notificationcenterdata/Notification$Source;

    return-object p0
.end method

.method public static values()[Lcom/squareup/notificationcenterdata/Notification$Source;
    .locals 1

    sget-object v0, Lcom/squareup/notificationcenterdata/Notification$Source;->$VALUES:[Lcom/squareup/notificationcenterdata/Notification$Source;

    invoke-virtual {v0}, [Lcom/squareup/notificationcenterdata/Notification$Source;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/notificationcenterdata/Notification$Source;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
