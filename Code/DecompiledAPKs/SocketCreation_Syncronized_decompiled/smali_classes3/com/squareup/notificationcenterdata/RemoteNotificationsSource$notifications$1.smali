.class final Lcom/squareup/notificationcenterdata/RemoteNotificationsSource$notifications$1;
.super Ljava/lang/Object;
.source "RemoteNotificationsSource.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;->notifications()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRemoteNotificationsSource.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RemoteNotificationsSource.kt\ncom/squareup/notificationcenterdata/RemoteNotificationsSource$notifications$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,93:1\n1412#2,9:94\n1642#2,2:103\n1421#2:105\n1360#2:106\n1429#2,3:107\n*E\n*S KotlinDebug\n*F\n+ 1 RemoteNotificationsSource.kt\ncom/squareup/notificationcenterdata/RemoteNotificationsSource$notifications$1\n*L\n48#1,9:94\n48#1,2:103\n48#1:105\n51#1:106\n51#1,3:107\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012&\u0010\u0002\u001a\"\u0012\u0004\u0012\u00020\u0004\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0006 \u0007*\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00050\u00050\u0003H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/notificationcenterdata/NotificationsSource$Result;",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/communications/MessagesRepository$Result;",
        "",
        "",
        "kotlin.jvm.PlatformType",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;


# direct methods
.method constructor <init>(Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/notificationcenterdata/RemoteNotificationsSource$notifications$1;->this$0:Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lkotlin/Pair;)Lcom/squareup/notificationcenterdata/NotificationsSource$Result;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "+",
            "Lcom/squareup/communications/MessagesRepository$Result;",
            "+",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;)",
            "Lcom/squareup/notificationcenterdata/NotificationsSource$Result;"
        }
    .end annotation

    move-object/from16 v0, p0

    const-string v1, "<name for destructuring parameter 0>"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/communications/MessagesRepository$Result;

    invoke-virtual/range {p1 .. p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 45
    instance-of v3, v1, Lcom/squareup/communications/MessagesRepository$Result$Success;

    if-eqz v3, :cond_4

    .line 46
    iget-object v3, v0, Lcom/squareup/notificationcenterdata/RemoteNotificationsSource$notifications$1;->this$0:Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;

    check-cast v1, Lcom/squareup/communications/MessagesRepository$Result$Success;

    invoke-virtual {v1}, Lcom/squareup/communications/MessagesRepository$Result$Success;->getMessages()Ljava/util/List;

    move-result-object v1

    .line 47
    invoke-static {v3, v1}, Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;->access$aggregated(Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 94
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    check-cast v3, Ljava/util/Collection;

    .line 103
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 102
    check-cast v4, Lcom/squareup/communications/Message;

    .line 49
    iget-object v5, v0, Lcom/squareup/notificationcenterdata/RemoteNotificationsSource$notifications$1;->this$0:Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;

    invoke-static {v5}, Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;->access$getClientActionTranslationDispatcher$p(Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;)Lcom/squareup/clientactiontranslation/ClientActionTranslationDispatcher;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/squareup/notificationcenterdata/utils/MessagesKt;->toNotification(Lcom/squareup/communications/Message;Lcom/squareup/clientactiontranslation/ClientActionTranslationDispatcher;)Lcom/squareup/notificationcenterdata/Notification;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 102
    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 105
    :cond_1
    check-cast v3, Ljava/util/List;

    check-cast v3, Ljava/lang/Iterable;

    .line 106
    new-instance v1, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v3, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 107
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 108
    move-object v5, v4

    check-cast v5, Lcom/squareup/notificationcenterdata/Notification;

    .line 52
    invoke-virtual {v5}, Lcom/squareup/notificationcenterdata/Notification;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    sget-object v10, Lcom/squareup/notificationcenterdata/Notification$State;->READ:Lcom/squareup/notificationcenterdata/Notification$State;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x7ef

    const/16 v18, 0x0

    invoke-static/range {v5 .. v18}, Lcom/squareup/notificationcenterdata/Notification;->copy$default(Lcom/squareup/notificationcenterdata/Notification;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$State;Lcom/squareup/notificationcenterdata/Notification$Priority;Lcom/squareup/notificationcenterdata/Notification$DisplayType;Lcom/squareup/notificationcenterdata/Notification$Destination;Lcom/squareup/notificationcenterdata/Notification$Source;Lorg/threeten/bp/Instant;Lcom/squareup/communications/Message$Type;ILjava/lang/Object;)Lcom/squareup/notificationcenterdata/Notification;

    move-result-object v5

    .line 53
    :cond_2
    invoke-interface {v1, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 109
    :cond_3
    check-cast v1, Ljava/util/List;

    .line 55
    new-instance v2, Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Success;

    invoke-direct {v2, v1}, Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Success;-><init>(Ljava/util/List;)V

    check-cast v2, Lcom/squareup/notificationcenterdata/NotificationsSource$Result;

    goto :goto_2

    .line 57
    :cond_4
    instance-of v1, v1, Lcom/squareup/communications/MessagesRepository$Result$Failure;

    if-eqz v1, :cond_5

    .line 58
    iget-object v1, v0, Lcom/squareup/notificationcenterdata/RemoteNotificationsSource$notifications$1;->this$0:Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;

    invoke-static {v1}, Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;->access$getAnalytics$p(Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;)Lcom/squareup/analytics/Analytics;

    move-result-object v1

    sget-object v2, Lcom/squareup/analytics/RegisterErrorName;->REMOTE_NOTIFICATIONS_LOADING_FAILED:Lcom/squareup/analytics/RegisterErrorName;

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logError(Lcom/squareup/analytics/RegisterErrorName;)V

    .line 59
    sget-object v1, Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Failure;->INSTANCE:Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Failure;

    move-object v2, v1

    check-cast v2, Lcom/squareup/notificationcenterdata/NotificationsSource$Result;

    :goto_2
    return-object v2

    :cond_5
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/notificationcenterdata/RemoteNotificationsSource$notifications$1;->apply(Lkotlin/Pair;)Lcom/squareup/notificationcenterdata/NotificationsSource$Result;

    move-result-object p1

    return-object p1
.end method
