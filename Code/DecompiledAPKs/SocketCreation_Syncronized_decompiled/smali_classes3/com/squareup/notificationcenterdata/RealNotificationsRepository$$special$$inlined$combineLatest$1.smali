.class public final Lcom/squareup/notificationcenterdata/RealNotificationsRepository$$special$$inlined$combineLatest$1;
.super Ljava/lang/Object;
.source "RxKotlin.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/notificationcenterdata/RealNotificationsRepository;-><init>(Ljava/util/Set;Ljava/util/Comparator;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "[",
        "Ljava/lang/Object;",
        "TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRxKotlin.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RxKotlin.kt\ncom/squareup/util/rx2/Observables$combineLatest$2\n+ 2 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n+ 3 RealNotificationsRepository.kt\ncom/squareup/notificationcenterdata/RealNotificationsRepository\n+ 4 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,1655:1\n9338#2:1656\n9671#2,3:1657\n65#3,4:1660\n67#3:1664\n66#3:1665\n72#3:1681\n73#3,2:1686\n75#3:1690\n76#3,5:1694\n84#3,3:1700\n1265#4,12:1666\n704#4:1678\n777#4,2:1679\n1577#4,4:1682\n215#4,2:1688\n1360#4:1691\n1429#4,2:1692\n1431#4:1699\n1577#4,4:1703\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0011\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\u0010\u0000\u001a\u0002H\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0001*\u00020\u00032*\u0010\u0004\u001a&\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00030\u0003 \u0006*\u0012\u0012\u000e\u0008\u0001\u0012\n \u0006*\u0004\u0018\u00010\u00030\u00030\u00050\u0005H\n\u00a2\u0006\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\t"
    }
    d2 = {
        "<anonymous>",
        "R",
        "T",
        "",
        "elements",
        "",
        "kotlin.jvm.PlatformType",
        "apply",
        "([Ljava/lang/Object;)Ljava/lang/Object;",
        "com/squareup/util/rx2/Observables$combineLatest$2"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $notificationComparator$inlined:Ljava/util/Comparator;


# direct methods
.method public constructor <init>(Ljava/util/Comparator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/notificationcenterdata/RealNotificationsRepository$$special$$inlined$combineLatest$1;->$notificationComparator$inlined:Ljava/util/Comparator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 49
    check-cast p1, [Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcom/squareup/notificationcenterdata/RealNotificationsRepository$$special$$inlined$combineLatest$1;->apply([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final apply([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Object;",
            ")TR;"
        }
    .end annotation

    move-object/from16 v0, p1

    const-string v1, "elements"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1656
    new-instance v1, Ljava/util/ArrayList;

    array-length v2, v0

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 1657
    array-length v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_1

    aget-object v5, v0, v4

    if-eqz v5, :cond_0

    .line 73
    invoke-interface {v1, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type T"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1659
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 1666
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 1673
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 1674
    check-cast v4, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;

    .line 1665
    invoke-virtual {v4}, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;->getNotifications()Ljava/util/List;

    move-result-object v4

    check-cast v4, Ljava/lang/Iterable;

    .line 1675
    invoke-static {v0, v4}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_1

    .line 1677
    :cond_2
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 1678
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 1679
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    const/4 v5, 0x1

    if-eqz v4, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v6, v4

    check-cast v6, Lcom/squareup/notificationcenterdata/Notification;

    .line 1664
    invoke-virtual {v6}, Lcom/squareup/notificationcenterdata/Notification;->getDestination()Lcom/squareup/notificationcenterdata/Notification$Destination;

    move-result-object v6

    sget-object v7, Lcom/squareup/notificationcenterdata/Notification$Destination$Nowhere;->INSTANCE:Lcom/squareup/notificationcenterdata/Notification$Destination$Nowhere;

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    xor-int/2addr v5, v6

    if-eqz v5, :cond_3

    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1680
    :cond_4
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    move-object/from16 v0, p0

    .line 1663
    iget-object v4, v0, Lcom/squareup/notificationcenterdata/RealNotificationsRepository$$special$$inlined$combineLatest$1;->$notificationComparator$inlined:Ljava/util/Comparator;

    invoke-static {v2, v4}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v2

    .line 1681
    move-object v4, v2

    check-cast v4, Ljava/lang/Iterable;

    .line 1682
    instance-of v6, v4, Ljava/util/Collection;

    if-eqz v6, :cond_5

    move-object v6, v4

    check-cast v6, Ljava/util/Collection;

    invoke-interface {v6}, Ljava/util/Collection;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_5

    const/4 v7, 0x0

    goto :goto_4

    .line 1684
    :cond_5
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    const/4 v7, 0x0

    :cond_6
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/notificationcenterdata/Notification;

    .line 1681
    invoke-virtual {v8}, Lcom/squareup/notificationcenterdata/Notification;->getDisplayType()Lcom/squareup/notificationcenterdata/Notification$DisplayType;

    move-result-object v8

    sget-object v9, Lcom/squareup/notificationcenterdata/Notification$DisplayType$WarningBanner;->INSTANCE:Lcom/squareup/notificationcenterdata/Notification$DisplayType$WarningBanner;

    invoke-static {v8, v9}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    add-int/lit8 v7, v7, 0x1

    if-gez v7, :cond_6

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwCountOverflow()V

    goto :goto_3

    :cond_7
    :goto_4
    if-le v7, v5, :cond_8

    const/4 v6, 0x1

    goto :goto_5

    :cond_8
    const/4 v6, 0x0

    :goto_5
    if-eqz v6, :cond_d

    .line 1688
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/notificationcenterdata/Notification;

    .line 1687
    invoke-virtual {v6}, Lcom/squareup/notificationcenterdata/Notification;->getDisplayType()Lcom/squareup/notificationcenterdata/Notification$DisplayType;

    move-result-object v7

    sget-object v8, Lcom/squareup/notificationcenterdata/Notification$DisplayType$WarningBanner;->INSTANCE:Lcom/squareup/notificationcenterdata/Notification$DisplayType$WarningBanner;

    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 1691
    new-instance v2, Ljava/util/ArrayList;

    const/16 v7, 0xa

    invoke-static {v4, v7}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v7

    invoke-direct {v2, v7}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 1692
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    .line 1693
    move-object v8, v7

    check-cast v8, Lcom/squareup/notificationcenterdata/Notification;

    .line 1694
    invoke-virtual {v8}, Lcom/squareup/notificationcenterdata/Notification;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6}, Lcom/squareup/notificationcenterdata/Notification;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    .line 1695
    sget-object v7, Lcom/squareup/notificationcenterdata/Notification$DisplayType$WarningBanner;->INSTANCE:Lcom/squareup/notificationcenterdata/Notification$DisplayType$WarningBanner;

    move-object v15, v7

    check-cast v15, Lcom/squareup/notificationcenterdata/Notification$DisplayType;

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x7bf

    const/16 v21, 0x0

    invoke-static/range {v8 .. v21}, Lcom/squareup/notificationcenterdata/Notification;->copy$default(Lcom/squareup/notificationcenterdata/Notification;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$State;Lcom/squareup/notificationcenterdata/Notification$Priority;Lcom/squareup/notificationcenterdata/Notification$DisplayType;Lcom/squareup/notificationcenterdata/Notification$Destination;Lcom/squareup/notificationcenterdata/Notification$Source;Lorg/threeten/bp/Instant;Lcom/squareup/communications/Message$Type;ILjava/lang/Object;)Lcom/squareup/notificationcenterdata/Notification;

    move-result-object v7

    goto :goto_7

    :cond_a
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    .line 1697
    sget-object v7, Lcom/squareup/notificationcenterdata/Notification$DisplayType$Normal;->INSTANCE:Lcom/squareup/notificationcenterdata/Notification$DisplayType$Normal;

    move-object v15, v7

    check-cast v15, Lcom/squareup/notificationcenterdata/Notification$DisplayType;

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x7bf

    const/16 v21, 0x0

    invoke-static/range {v8 .. v21}, Lcom/squareup/notificationcenterdata/Notification;->copy$default(Lcom/squareup/notificationcenterdata/Notification;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$State;Lcom/squareup/notificationcenterdata/Notification$Priority;Lcom/squareup/notificationcenterdata/Notification$DisplayType;Lcom/squareup/notificationcenterdata/Notification$Destination;Lcom/squareup/notificationcenterdata/Notification$Source;Lorg/threeten/bp/Instant;Lcom/squareup/communications/Message$Type;ILjava/lang/Object;)Lcom/squareup/notificationcenterdata/Notification;

    move-result-object v7

    .line 1698
    :goto_7
    invoke-interface {v2, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 1699
    :cond_b
    check-cast v2, Ljava/util/List;

    goto :goto_8

    .line 1689
    :cond_c
    new-instance v1, Ljava/util/NoSuchElementException;

    const-string v2, "Collection contains no element matching the predicate."

    invoke-direct {v1, v2}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 1703
    :cond_d
    :goto_8
    instance-of v4, v1, Ljava/util/Collection;

    if-eqz v4, :cond_e

    move-object v4, v1

    check-cast v4, Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_e

    const/4 v4, 0x0

    goto :goto_a

    .line 1705
    :cond_e
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v4, 0x0

    :cond_f
    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_10

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;

    .line 1702
    invoke-virtual {v6}, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;->getHasError()Z

    move-result v6

    if-eqz v6, :cond_f

    add-int/lit8 v4, v4, 0x1

    if-gez v4, :cond_f

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwCountOverflow()V

    goto :goto_9

    :cond_10
    :goto_a
    if-lez v4, :cond_11

    const/4 v3, 0x1

    .line 1700
    :cond_11
    new-instance v1, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;

    invoke-direct {v1, v2, v3}, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;-><init>(Ljava/util/List;Z)V

    return-object v1
.end method
