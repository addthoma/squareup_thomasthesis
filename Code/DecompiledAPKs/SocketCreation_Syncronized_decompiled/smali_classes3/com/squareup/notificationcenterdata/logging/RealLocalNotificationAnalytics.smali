.class public final Lcom/squareup/notificationcenterdata/logging/RealLocalNotificationAnalytics;
.super Ljava/lang/Object;
.source "RealLocalNotificationAnalytics.kt"

# interfaces
.implements Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealLocalNotificationAnalytics.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealLocalNotificationAnalytics.kt\ncom/squareup/notificationcenterdata/logging/RealLocalNotificationAnalytics\n*L\n1#1,30:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/notificationcenterdata/logging/RealLocalNotificationAnalytics;",
        "Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/analytics/Analytics;)V",
        "logLocalNotificationCreated",
        "",
        "notification",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/notificationcenterdata/logging/RealLocalNotificationAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method


# virtual methods
.method public logLocalNotificationCreated(Lcom/squareup/notificationcenterdata/Notification;)V
    .locals 10

    const-string v0, "notification"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/logging/RealLocalNotificationAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 17
    new-instance v9, Lcom/squareup/notificationcenterdata/logging/LocalNotificationEvent;

    .line 18
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/Notification;->getId()Ljava/lang/String;

    move-result-object v2

    .line 19
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/Notification;->getTitle()Ljava/lang/String;

    move-result-object v3

    .line 20
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/Notification;->getContent()Ljava/lang/String;

    move-result-object v4

    .line 21
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/Notification;->getDestination()Lcom/squareup/notificationcenterdata/Notification$Destination;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/notificationcenterdata/utils/DestinationsKt;->getClientActionString(Lcom/squareup/notificationcenterdata/Notification$Destination;)Ljava/lang/String;

    move-result-object v6

    .line 22
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/Notification;->getDestination()Lcom/squareup/notificationcenterdata/Notification$Destination;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/notificationcenterdata/utils/DestinationsKt;->getBrowserDialogBody(Lcom/squareup/notificationcenterdata/Notification$Destination;)Ljava/lang/String;

    move-result-object v5

    .line 23
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/Notification;->getDestination()Lcom/squareup/notificationcenterdata/Notification$Destination;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/notificationcenterdata/utils/DestinationsKt;->getUrlString(Lcom/squareup/notificationcenterdata/Notification$Destination;)Ljava/lang/String;

    move-result-object v7

    .line 24
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/Notification;->getCreatedAt()Lorg/threeten/bp/Instant;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/utilities/threeten/InstantsKt;->asIso8601(Lorg/threeten/bp/Instant;)Ljava/lang/String;

    move-result-object v8

    move-object v1, v9

    .line 17
    invoke-direct/range {v1 .. v8}, Lcom/squareup/notificationcenterdata/logging/LocalNotificationEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    check-cast v9, Lcom/squareup/eventstream/v2/AppEvent;

    .line 15
    invoke-interface {v0, v9}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method
