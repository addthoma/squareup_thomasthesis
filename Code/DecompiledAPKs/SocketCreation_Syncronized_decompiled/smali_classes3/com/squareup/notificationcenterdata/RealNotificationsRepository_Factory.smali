.class public final Lcom/squareup/notificationcenterdata/RealNotificationsRepository_Factory;
.super Ljava/lang/Object;
.source "RealNotificationsRepository_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/notificationcenterdata/RealNotificationsRepository;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/notificationcenterdata/NotificationsSource;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Comparator<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/notificationcenterdata/NotificationsSource;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Comparator<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;>;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/notificationcenterdata/RealNotificationsRepository_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 21
    iput-object p2, p0, Lcom/squareup/notificationcenterdata/RealNotificationsRepository_Factory;->arg1Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/notificationcenterdata/RealNotificationsRepository_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/notificationcenterdata/NotificationsSource;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Comparator<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;>;)",
            "Lcom/squareup/notificationcenterdata/RealNotificationsRepository_Factory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/notificationcenterdata/RealNotificationsRepository_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/notificationcenterdata/RealNotificationsRepository_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljava/util/Set;Ljava/util/Comparator;)Lcom/squareup/notificationcenterdata/RealNotificationsRepository;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/notificationcenterdata/NotificationsSource;",
            ">;",
            "Ljava/util/Comparator<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;)",
            "Lcom/squareup/notificationcenterdata/RealNotificationsRepository;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/notificationcenterdata/RealNotificationsRepository;

    invoke-direct {v0, p0, p1}, Lcom/squareup/notificationcenterdata/RealNotificationsRepository;-><init>(Ljava/util/Set;Ljava/util/Comparator;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/notificationcenterdata/RealNotificationsRepository;
    .locals 2

    .line 26
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/RealNotificationsRepository_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iget-object v1, p0, Lcom/squareup/notificationcenterdata/RealNotificationsRepository_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Comparator;

    invoke-static {v0, v1}, Lcom/squareup/notificationcenterdata/RealNotificationsRepository_Factory;->newInstance(Ljava/util/Set;Ljava/util/Comparator;)Lcom/squareup/notificationcenterdata/RealNotificationsRepository;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/RealNotificationsRepository_Factory;->get()Lcom/squareup/notificationcenterdata/RealNotificationsRepository;

    move-result-object v0

    return-object v0
.end method
