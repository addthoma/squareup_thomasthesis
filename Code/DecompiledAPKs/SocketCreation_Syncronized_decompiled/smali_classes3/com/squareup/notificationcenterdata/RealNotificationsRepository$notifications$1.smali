.class final Lcom/squareup/notificationcenterdata/RealNotificationsRepository$notifications$1;
.super Ljava/lang/Object;
.source "RealNotificationsRepository.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/notificationcenterdata/RealNotificationsRepository;->notifications(Lcom/squareup/notificationcenterdata/Notification$Priority;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealNotificationsRepository.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealNotificationsRepository.kt\ncom/squareup/notificationcenterdata/RealNotificationsRepository$notifications$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,105:1\n704#2:106\n777#2,2:107\n*E\n*S KotlinDebug\n*F\n+ 1 RealNotificationsRepository.kt\ncom/squareup/notificationcenterdata/RealNotificationsRepository$notifications$1\n*L\n100#1:106\n100#1,2:107\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;",
        "result",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $priority:Lcom/squareup/notificationcenterdata/Notification$Priority;


# direct methods
.method constructor <init>(Lcom/squareup/notificationcenterdata/Notification$Priority;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/notificationcenterdata/RealNotificationsRepository$notifications$1;->$priority:Lcom/squareup/notificationcenterdata/Notification$Priority;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;)Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;
    .locals 5

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;->getNotifications()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 106
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 107
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/notificationcenterdata/Notification;

    .line 100
    invoke-virtual {v3}, Lcom/squareup/notificationcenterdata/Notification;->getPriority()Lcom/squareup/notificationcenterdata/Notification$Priority;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/notificationcenterdata/RealNotificationsRepository$notifications$1;->$priority:Lcom/squareup/notificationcenterdata/Notification$Priority;

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 108
    :cond_1
    check-cast v1, Ljava/util/List;

    .line 101
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;->getHasError()Z

    move-result p1

    .line 99
    new-instance v0, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;

    invoke-direct {v0, v1, p1}, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;-><init>(Ljava/util/List;Z)V

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;

    invoke-virtual {p0, p1}, Lcom/squareup/notificationcenterdata/RealNotificationsRepository$notifications$1;->apply(Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;)Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;

    move-result-object p1

    return-object p1
.end method
