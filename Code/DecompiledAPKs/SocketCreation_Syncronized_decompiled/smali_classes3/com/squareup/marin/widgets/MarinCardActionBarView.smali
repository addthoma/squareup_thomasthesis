.class public Lcom/squareup/marin/widgets/MarinCardActionBarView;
.super Lcom/squareup/marin/widgets/MarinBaseActionBarView;
.source "MarinCardActionBarView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/marin/widgets/MarinCardActionBarView$Component;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field presenter:Lcom/squareup/marin/widgets/MarinCardActionBar;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1, p2}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    const-class p2, Lcom/squareup/marin/widgets/MarinCardActionBarView$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/marin/widgets/MarinCardActionBarView$Component;

    invoke-interface {p1, p0}, Lcom/squareup/marin/widgets/MarinCardActionBarView$Component;->inject(Lcom/squareup/marin/widgets/MarinCardActionBarView;)V

    return-void
.end method


# virtual methods
.method protected getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinCardActionBarView;->presenter:Lcom/squareup/marin/widgets/MarinCardActionBar;

    return-object v0
.end method
