.class public Lcom/squareup/marin/widgets/MarinCardActionBarModule;
.super Ljava/lang/Object;
.source "MarinCardActionBarModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/marin/widgets/MarinCardActionBarModule$SharedScope;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideMarinActionBarForCard()Lcom/squareup/marin/widgets/MarinCardActionBar;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 18
    new-instance v0, Lcom/squareup/marin/widgets/MarinCardActionBar;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinCardActionBar;-><init>()V

    return-object v0
.end method
