.class public Lcom/squareup/marin/widgets/MarinActionBarModule;
.super Ljava/lang/Object;
.source "MarinActionBarModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/marin/widgets/MarinActionBarModule$SharedScope;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideMarinActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 19
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar;-><init>()V

    return-object v0
.end method
