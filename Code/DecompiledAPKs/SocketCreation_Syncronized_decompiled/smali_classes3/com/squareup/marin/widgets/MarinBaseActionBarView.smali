.class public abstract Lcom/squareup/marin/widgets/MarinBaseActionBarView;
.super Landroid/widget/FrameLayout;
.source "MarinBaseActionBarView.java"

# interfaces
.implements Lcom/squareup/marin/widgets/Badgeable;


# static fields
.field public static final NO_TEXT:Ljava/lang/String; = ""


# instance fields
.field private backgroundView:Landroid/view/View;

.field private badge:Lcom/squareup/widgets/MarinBadgeView;

.field private final buttonPadding:I

.field private canShowBadges:Z

.field private final context:Landroid/content/Context;

.field private customViewContainer:Landroid/widget/FrameLayout;

.field private fourthButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

.field private primaryButton:Landroid/widget/Button;

.field private secondaryButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

.field private thirdButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

.field private upButtonContainer:Landroid/view/View;

.field private upGlyph:Lcom/squareup/glyph/SquareGlyphView;

.field private upText:Landroid/widget/TextView;

.field private upTextRightPadding:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 47
    sget v0, Lcom/squareup/marin/R$attr;->marinActionBarStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 51
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p2, 0x1

    .line 44
    iput-boolean p2, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->canShowBadges:Z

    .line 52
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->context:Landroid/content/Context;

    .line 53
    sget p2, Lcom/squareup/marin/R$layout;->marin_action_bar:I

    invoke-static {p1, p2, p0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 54
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/marin/R$dimen;->marin_action_bar_button_padding:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->buttonPadding:I

    .line 55
    iget p1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->buttonPadding:I

    iput p1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->upTextRightPadding:I

    return-void
.end method

.method private bindViews()V
    .locals 2

    .line 322
    sget v0, Lcom/squareup/marin/R$id;->up_button:I

    invoke-virtual {p0, v0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->upButtonContainer:Landroid/view/View;

    .line 323
    sget v0, Lcom/squareup/marin/R$id;->up_button_glyph:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->upGlyph:Lcom/squareup/glyph/SquareGlyphView;

    .line 324
    sget v0, Lcom/squareup/marin/R$id;->up_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->upText:Landroid/widget/TextView;

    .line 325
    sget v0, Lcom/squareup/marin/R$id;->badge_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MarinBadgeView;

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->badge:Lcom/squareup/widgets/MarinBadgeView;

    .line 326
    sget v0, Lcom/squareup/marin/R$id;->primary_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->primaryButton:Landroid/widget/Button;

    .line 327
    sget v0, Lcom/squareup/marin/R$id;->secondary_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphTextView;

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->secondaryButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    .line 328
    sget v0, Lcom/squareup/marin/R$id;->third_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphTextView;

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->thirdButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    .line 329
    sget v0, Lcom/squareup/marin/R$id;->fourth_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphTextView;

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->fourthButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    .line 330
    sget v0, Lcom/squareup/marin/R$id;->actionbar_custom_view_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->customViewContainer:Landroid/widget/FrameLayout;

    .line 331
    sget v0, Lcom/squareup/marin/R$id;->marin_action_bar_background:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->backgroundView:Landroid/view/View;

    .line 333
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->upButtonContainer:Landroid/view/View;

    new-instance v1, Lcom/squareup/marin/widgets/MarinBaseActionBarView$1;

    invoke-direct {v1, p0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView$1;-><init>(Lcom/squareup/marin/widgets/MarinBaseActionBarView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 338
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->primaryButton:Landroid/widget/Button;

    new-instance v1, Lcom/squareup/marin/widgets/MarinBaseActionBarView$2;

    invoke-direct {v1, p0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView$2;-><init>(Lcom/squareup/marin/widgets/MarinBaseActionBarView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 343
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->secondaryButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    new-instance v1, Lcom/squareup/marin/widgets/MarinBaseActionBarView$3;

    invoke-direct {v1, p0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView$3;-><init>(Lcom/squareup/marin/widgets/MarinBaseActionBarView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 348
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->thirdButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    new-instance v1, Lcom/squareup/marin/widgets/MarinBaseActionBarView$4;

    invoke-direct {v1, p0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView$4;-><init>(Lcom/squareup/marin/widgets/MarinBaseActionBarView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 353
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->fourthButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    new-instance v1, Lcom/squareup/marin/widgets/MarinBaseActionBarView$5;

    invoke-direct {v1, p0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView$5;-><init>(Lcom/squareup/marin/widgets/MarinBaseActionBarView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 358
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->customViewContainer:Landroid/widget/FrameLayout;

    new-instance v1, Lcom/squareup/marin/widgets/MarinBaseActionBarView$6;

    invoke-direct {v1, p0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView$6;-><init>(Lcom/squareup/marin/widgets/MarinBaseActionBarView;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method canShowBadges(Z)V
    .locals 0

    .line 181
    iput-boolean p1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->canShowBadges:Z

    return-void
.end method

.method clearUpGlyph()V
    .locals 2

    .line 131
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->upButtonContainer:Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/widgets/CheatSheet;->remove(Landroid/view/View;)V

    .line 132
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->upButtonContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->upGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method protected abstract getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;
.end method

.method getUpButtonTooltip(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Ljava/lang/CharSequence;
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 141
    :cond_0
    sget-object v1, Lcom/squareup/marin/widgets/MarinBaseActionBarView$7;->$SwitchMap$com$squareup$glyph$GlyphTypeface$Glyph:[I

    invoke-virtual {p1}, Lcom/squareup/glyph/GlyphTypeface$Glyph;->ordinal()I

    move-result p1

    aget p1, v1, p1

    const/4 v1, 0x1

    if-eq p1, v1, :cond_3

    const/4 v1, 0x2

    if-eq p1, v1, :cond_2

    const/4 v1, 0x3

    if-eq p1, v1, :cond_1

    return-object v0

    .line 149
    :cond_1
    sget p1, Lcom/squareup/marin/R$string;->content_description_navigate_burger_button:I

    goto :goto_0

    .line 146
    :cond_2
    sget p1, Lcom/squareup/marin/R$string;->content_description_navigate_up_button:I

    goto :goto_0

    .line 143
    :cond_3
    sget p1, Lcom/squareup/marin/R$string;->content_description_navigate_x_button:I

    .line 154
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public hideBadge()V
    .locals 1

    .line 158
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->badge:Lcom/squareup/widgets/MarinBadgeView;

    invoke-virtual {v0}, Lcom/squareup/widgets/MarinBadgeView;->hideBadge()V

    return-void
.end method

.method hideCustomView()V
    .locals 2

    .line 301
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->customViewContainer:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    return-void
.end method

.method hideFourthButton()V
    .locals 2

    .line 278
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->fourthButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setVisibility(I)V

    return-void
.end method

.method hidePrimaryButton()V
    .locals 2

    .line 190
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->primaryButton:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method

.method hideSecondaryButton()V
    .locals 2

    .line 208
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->secondaryButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setVisibility(I)V

    return-void
.end method

.method hideThirdButton()V
    .locals 2

    .line 247
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->thirdButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setClickable(Z)V

    .line 248
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->thirdButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setVisibility(I)V

    return-void
.end method

.method hideUpGlyph()V
    .locals 4

    .line 83
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->upButtonContainer:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 84
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->upText:Landroid/widget/TextView;

    iget v1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->buttonPadding:I

    iget v2, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->upTextRightPadding:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    return-void
.end method

.method hideUpText()V
    .locals 2

    .line 99
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->upText:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 67
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/squareup/marin/widgets/MarinActionBar;->dropView(Lcom/squareup/marin/widgets/MarinBaseActionBarView;)V

    .line 68
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 61
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 62
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->bindViews()V

    .line 63
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/squareup/marin/widgets/MarinActionBar;->takeView(Lcom/squareup/marin/widgets/MarinBaseActionBarView;)V

    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->backgroundView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundResource(I)V

    return-void
.end method

.method setCustomView(Landroid/view/View;Ljava/lang/CharSequence;)V
    .locals 1

    .line 305
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->customViewContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    if-eqz p1, :cond_0

    .line 307
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->customViewContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 308
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->customViewContainer:Landroid/widget/FrameLayout;

    invoke-virtual {p1, p2}, Landroid/widget/FrameLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 309
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->customViewContainer:Landroid/widget/FrameLayout;

    invoke-static {p1}, Lcom/squareup/widgets/CheatSheet;->setup(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method setCustomViewEnabled(Z)V
    .locals 2

    .line 314
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->customViewContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    const/4 v0, 0x0

    .line 315
    :goto_0
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->customViewContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 316
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->customViewContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/view/View;->setEnabled(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method setFourthButtonEnabled(Z)V
    .locals 1

    .line 282
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->fourthButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setEnabled(Z)V

    return-void
.end method

.method setFourthButtonGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V
    .locals 2

    .line 287
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->fourthButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;I)V

    .line 288
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->fourthButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 289
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->fourthButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-static {p1}, Lcom/squareup/widgets/CheatSheet;->setup(Landroid/view/View;)V

    .line 291
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->fourthButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setCompoundDrawablePadding(I)V

    .line 292
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->fourthButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    const-string p2, ""

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setPrimaryButtonEnabled(Z)V
    .locals 1

    .line 194
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->primaryButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method setPrimaryButtonText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 199
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->primaryButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setSecondaryButtonBackgroundColor(I)V
    .locals 2

    .line 228
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->secondaryButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setBackgroundColor(I)V

    return-void
.end method

.method setSecondaryButtonEnabled(Z)V
    .locals 1

    .line 212
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->secondaryButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setEnabled(Z)V

    return-void
.end method

.method setSecondaryButtonGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V
    .locals 2

    .line 232
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->secondaryButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;I)V

    .line 233
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->secondaryButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 234
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->secondaryButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-static {p1}, Lcom/squareup/widgets/CheatSheet;->setup(Landroid/view/View;)V

    .line 236
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->secondaryButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setCompoundDrawablePadding(I)V

    .line 237
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->secondaryButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    const-string p2, ""

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setSecondaryButtonText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 217
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->secondaryButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setText(Ljava/lang/CharSequence;)V

    .line 218
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 219
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->secondaryButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->removeAllGlyphs()V

    :cond_0
    return-void
.end method

.method setSecondaryButtonTextColor(I)V
    .locals 2

    .line 224
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->secondaryButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->context:Landroid/content/Context;

    invoke-static {v1, p1}, Landroidx/core/content/ContextCompat;->getColorStateList(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method setThirdButtonEnabled(Z)V
    .locals 1

    .line 252
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->thirdButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setEnabled(Z)V

    return-void
.end method

.method setThirdButtonGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V
    .locals 2

    .line 264
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->thirdButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;I)V

    .line 265
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->thirdButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 266
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->thirdButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-static {p1}, Lcom/squareup/widgets/CheatSheet;->setup(Landroid/view/View;)V

    .line 268
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->thirdButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setCompoundDrawablePadding(I)V

    .line 269
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->thirdButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    const-string p2, ""

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setThirdButtonText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 257
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->thirdButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setText(Ljava/lang/CharSequence;)V

    .line 258
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 259
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->thirdButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->removeAllGlyphs()V

    :cond_0
    return-void
.end method

.method setUpButtonEnabled(Z)V
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->upButtonContainer:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method setUpCenteredText()V
    .locals 2

    .line 117
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->upText:Landroid/widget/TextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    return-void
.end method

.method setUpGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->upGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 122
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->upButtonContainer:Landroid/view/View;

    invoke-virtual {p1, p2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 123
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->upButtonContainer:Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/widgets/CheatSheet;->setup(Landroid/view/View;)V

    return-void
.end method

.method setUpGlyphColor(I)V
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->upGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphColorRes(I)V

    return-void
.end method

.method setUpText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->upText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setUpTextColor(I)V
    .locals 1

    .line 107
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->upText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method setUpTextRightPadded(Z)V
    .locals 4

    if-eqz p1, :cond_0

    .line 111
    iget p1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->buttonPadding:I

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput p1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->upTextRightPadding:I

    .line 112
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->upText:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->upText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v1

    iget v2, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->upTextRightPadding:I

    iget-object v3, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->upText:Landroid/widget/TextView;

    .line 113
    invoke-virtual {v3}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v3

    .line 112
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    return-void
.end method

.method public showBadge(Ljava/lang/CharSequence;)V
    .locals 1

    .line 162
    iget-boolean v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->canShowBadges:Z

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->badge:Lcom/squareup/widgets/MarinBadgeView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MarinBadgeView;->showBadge(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method showCustomView()V
    .locals 2

    .line 297
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->customViewContainer:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    return-void
.end method

.method public showFatalPriorityBadge()V
    .locals 1

    .line 174
    iget-boolean v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->canShowBadges:Z

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->badge:Lcom/squareup/widgets/MarinBadgeView;

    invoke-virtual {v0}, Lcom/squareup/widgets/MarinBadgeView;->showFatalPriorityBadge()V

    :cond_0
    return-void
.end method

.method showFourthButton()V
    .locals 2

    .line 274
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->fourthButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setVisibility(I)V

    return-void
.end method

.method public showHighPriorityBadge(Ljava/lang/CharSequence;)V
    .locals 1

    .line 168
    iget-boolean v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->canShowBadges:Z

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->badge:Lcom/squareup/widgets/MarinBadgeView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MarinBadgeView;->showHighPriorityBadge(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method showPrimaryButton()V
    .locals 2

    .line 186
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->primaryButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method

.method showSecondaryButton()V
    .locals 2

    .line 204
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->secondaryButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setVisibility(I)V

    return-void
.end method

.method showThirdButton()V
    .locals 2

    .line 242
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->thirdButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setClickable(Z)V

    .line 243
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->thirdButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setVisibility(I)V

    return-void
.end method

.method showUpGlyph()V
    .locals 3

    .line 77
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->upButtonContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 79
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->upText:Landroid/widget/TextView;

    iget v2, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->upTextRightPadding:I

    invoke-virtual {v0, v1, v1, v2, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    return-void
.end method

.method showUpText()V
    .locals 2

    .line 92
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinBaseActionBarView;->upText:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method
