.class public Lcom/squareup/marin/widgets/MarinGiftCardDrawable;
.super Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;
.source "MarinGiftCardDrawable.java"


# static fields
.field private static final LARGE_CENTER_X_DP:F = 21.5f

.field private static final LARGE_TEXT_BOTTOM_DP:F = 39.0f

.field private static final LARGE_TEXT_TOP_DP:F = 23.5f

.field private static final LARGE_TEXT_WIDTH_DP:F = 29.5f

.field private static final MEDIUM_CENTER_X_DP:F = 18.5f

.field private static final MEDIUM_TEXT_BOTTOM_DP:F = 37.5f

.field private static final MEDIUM_TEXT_TOP_DP:F = 13.5f

.field private static final MEDIUM_TEXT_WIDTH_DP:F = 24.0f


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected calculateTextBoundBottom(ZI)I
    .locals 0

    if-eqz p1, :cond_0

    const/high16 p1, 0x421c0000    # 39.0f

    goto :goto_0

    :cond_0
    const/high16 p1, 0x42160000    # 37.5f

    .line 40
    :goto_0
    invoke-static {p1, p2}, Lcom/squareup/util/Views;->dpToPxRounded(FI)I

    move-result p1

    return p1
.end method

.method protected calculateTextBoundTop(ZI)I
    .locals 0

    if-eqz p1, :cond_0

    const/high16 p1, 0x41bc0000    # 23.5f

    goto :goto_0

    :cond_0
    const/high16 p1, 0x41580000    # 13.5f

    .line 36
    :goto_0
    invoke-static {p1, p2}, Lcom/squareup/util/Views;->dpToPxRounded(FI)I

    move-result p1

    return p1
.end method

.method protected calculateTextBoundWidth(ZI)I
    .locals 0

    if-eqz p1, :cond_0

    const/high16 p1, 0x41ec0000    # 29.5f

    goto :goto_0

    :cond_0
    const/high16 p1, 0x41c00000    # 24.0f

    .line 44
    :goto_0
    invoke-static {p1, p2}, Lcom/squareup/util/Views;->dpToPxRounded(FI)I

    move-result p1

    return p1
.end method

.method protected calculateTextPositionX(ZI)I
    .locals 0

    if-eqz p1, :cond_0

    const/high16 p1, 0x41ac0000    # 21.5f

    goto :goto_0

    :cond_0
    const/high16 p1, 0x41940000    # 18.5f

    .line 48
    :goto_0
    invoke-static {p1, p2}, Lcom/squareup/util/Views;->dpToPxRounded(FI)I

    move-result p1

    return p1
.end method

.method public doDrawText(Landroid/graphics/Canvas;)V
    .locals 4

    .line 32
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinGiftCardDrawable;->text:Ljava/lang/String;

    iget v1, p0, Lcom/squareup/marin/widgets/MarinGiftCardDrawable;->textXPosition:F

    iget v2, p0, Lcom/squareup/marin/widgets/MarinGiftCardDrawable;->textYPosition:F

    iget-object v3, p0, Lcom/squareup/marin/widgets/MarinGiftCardDrawable;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method protected getGlyph(Z)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 0

    if-eqz p1, :cond_0

    .line 52
    sget-object p1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->GIFT_CARD_LARGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->GIFT_CARD_MEDIUM:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    :goto_0
    return-object p1
.end method
