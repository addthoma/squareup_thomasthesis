.class public Lcom/squareup/marin/widgets/MarinCheckBox;
.super Lcom/squareup/marin/widgets/MarinSquareCompoundButton;
.source "MarinCheckBox.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 12
    invoke-direct {p0, p1, p2}, Lcom/squareup/marin/widgets/MarinSquareCompoundButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 13
    sget p1, Lcom/squareup/marin/R$drawable;->marin_selector_button_check:I

    invoke-virtual {p0, p1}, Lcom/squareup/marin/widgets/MarinCheckBox;->setBackgroundResource(I)V

    return-void
.end method
