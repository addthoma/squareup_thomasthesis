.class public Lcom/squareup/marin/widgets/MarinPageIndicator;
.super Landroid/view/View;
.source "MarinPageIndicator.java"

# interfaces
.implements Lcom/squareup/viewpagerindicator/PageIndicator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/marin/widgets/MarinPageIndicator$SavedState;
    }
.end annotation


# instance fields
.field private currentPage:I

.field private diameter:I

.field private final growInterpolator:Landroid/view/animation/Interpolator;

.field private final highlightPaint:Landroid/graphics/Paint;

.field private final mattePaint:Landroid/graphics/Paint;

.field private pageOffset:F

.field private final shrinkInterpolator:Landroid/view/animation/Interpolator;

.field private spacing:I

.field private viewPager:Landroidx/viewpager/widget/ViewPager;

.field private viewPagerListener:Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 43
    invoke-direct {p0, p1, v0}, Lcom/squareup/marin/widgets/MarinPageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    .line 47
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/marin/widgets/MarinPageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .line 51
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->highlightPaint:Landroid/graphics/Paint;

    .line 26
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->mattePaint:Landroid/graphics/Paint;

    .line 28
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->shrinkInterpolator:Landroid/view/animation/Interpolator;

    .line 29
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->growInterpolator:Landroid/view/animation/Interpolator;

    .line 52
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinPageIndicator;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 56
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinPageIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 57
    sget-object v1, Lcom/squareup/marin/R$styleable;->MarinPageIndicator:[I

    const/4 v2, 0x0

    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 59
    iget-object p2, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->mattePaint:Landroid/graphics/Paint;

    sget-object p3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 60
    iget-object p2, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->mattePaint:Landroid/graphics/Paint;

    sget p3, Lcom/squareup/marin/R$styleable;->MarinPageIndicator_indicatorHighlightColor:I

    sget v1, Lcom/squareup/marin/R$color;->marin_light_gray:I

    .line 61
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 60
    invoke-virtual {p1, p3, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p3

    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setColor(I)V

    .line 62
    iget-object p2, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->highlightPaint:Landroid/graphics/Paint;

    sget-object p3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 63
    iget-object p2, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->highlightPaint:Landroid/graphics/Paint;

    sget p3, Lcom/squareup/marin/R$styleable;->MarinPageIndicator_indicatorMatteColor:I

    sget v1, Lcom/squareup/marin/R$color;->marin_blue:I

    .line 64
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 63
    invoke-virtual {p1, p3, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p3

    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setColor(I)V

    .line 65
    sget p2, Lcom/squareup/marin/R$styleable;->MarinPageIndicator_indicatorDiameter:I

    sget p3, Lcom/squareup/marin/R$dimen;->marin_page_indicator_default_diameter:I

    .line 66
    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p3

    .line 65
    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->diameter:I

    .line 67
    sget p2, Lcom/squareup/marin/R$styleable;->MarinPageIndicator_indicatorSpacing:I

    iget p3, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->diameter:I

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->spacing:I

    .line 68
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private measureHeight(I)I
    .locals 3

    .line 247
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 248
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    const/high16 v1, 0x40000000    # 2.0f

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 253
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinPageIndicator;->getPaddingTop()I

    move-result v1

    iget v2, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->diameter:I

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinPageIndicator;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    const/high16 v2, -0x80000000

    if-ne v0, v2, :cond_1

    .line 257
    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    goto :goto_0

    :cond_1
    move p1, v1

    :goto_0
    return p1
.end method

.method private measureWidth(I)I
    .locals 4

    .line 224
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 225
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    const/high16 v1, 0x40000000    # 2.0f

    if-eq v0, v1, :cond_2

    .line 227
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->viewPager:Landroidx/viewpager/widget/ViewPager;

    if-nez v1, :cond_0

    goto :goto_0

    .line 231
    :cond_0
    invoke-virtual {v1}, Landroidx/viewpager/widget/ViewPager;->getAdapter()Landroidx/viewpager/widget/PagerAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/viewpager/widget/PagerAdapter;->getCount()I

    move-result v1

    .line 232
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinPageIndicator;->getPaddingLeft()I

    move-result v2

    iget v3, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->diameter:I

    mul-int v3, v3, v1

    add-int/2addr v2, v3

    add-int/lit8 v1, v1, -0x1

    iget v3, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->spacing:I

    mul-int v1, v1, v3

    add-int/2addr v2, v1

    .line 235
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinPageIndicator;->getPaddingRight()I

    move-result v1

    add-int/2addr v1, v2

    const/high16 v2, -0x80000000

    if-ne v0, v2, :cond_1

    .line 239
    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    goto :goto_0

    :cond_1
    move p1, v1

    :cond_2
    :goto_0
    return p1
.end method


# virtual methods
.method public notifyDataSetChanged()V
    .locals 0

    .line 169
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinPageIndicator;->invalidate()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 9

    .line 76
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 77
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->viewPager:Landroidx/viewpager/widget/ViewPager;

    if-nez v0, :cond_0

    return-void

    .line 81
    :cond_0
    invoke-virtual {v0}, Landroidx/viewpager/widget/ViewPager;->getAdapter()Landroidx/viewpager/widget/PagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/viewpager/widget/PagerAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 86
    :cond_1
    iget v1, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->currentPage:I

    if-lt v1, v0, :cond_2

    add-int/lit8 v0, v0, -0x1

    .line 87
    invoke-virtual {p0, v0}, Lcom/squareup/marin/widgets/MarinPageIndicator;->setCurrentItem(I)V

    return-void

    .line 91
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinPageIndicator;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    .line 92
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinPageIndicator;->getPaddingLeft()I

    move-result v2

    .line 94
    iget v3, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->diameter:I

    int-to-float v4, v3

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    int-to-float v2, v2

    add-float/2addr v2, v4

    .line 96
    iget v5, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->spacing:I

    add-int/2addr v3, v5

    int-to-float v3, v3

    .line 99
    iget-object v5, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->mattePaint:Landroid/graphics/Paint;

    invoke-virtual {v5}, Landroid/graphics/Paint;->getAlpha()I

    move-result v5

    if-lez v5, :cond_3

    const/4 v5, 0x0

    move v6, v2

    :goto_0
    if-ge v5, v0, :cond_3

    int-to-float v7, v1

    .line 101
    iget-object v8, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->mattePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v7, v4, v8}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    add-float/2addr v6, v3

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 108
    :cond_3
    iget v0, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->currentPage:I

    int-to-float v0, v0

    mul-float v0, v0, v3

    add-float/2addr v2, v0

    .line 109
    iget v0, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->pageOffset:F

    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v6, 0x3f000000    # 0.5f

    cmpg-float v7, v0, v6

    if-gtz v7, :cond_4

    div-float/2addr v0, v6

    sub-float/2addr v5, v0

    .line 112
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->shrinkInterpolator:Landroid/view/animation/Interpolator;

    invoke-interface {v0, v5}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    mul-float v4, v4, v0

    goto :goto_1

    :cond_4
    div-float/2addr v0, v6

    sub-float/2addr v0, v5

    .line 116
    iget-object v5, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->growInterpolator:Landroid/view/animation/Interpolator;

    invoke-interface {v5, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    mul-float v4, v4, v0

    add-float/2addr v2, v3

    :goto_1
    int-to-float v0, v1

    .line 122
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->highlightPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v0, v4, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 0

    .line 72
    invoke-direct {p0, p1}, Lcom/squareup/marin/widgets/MarinPageIndicator;->measureWidth(I)I

    move-result p1

    invoke-direct {p0, p2}, Lcom/squareup/marin/widgets/MarinPageIndicator;->measureHeight(I)I

    move-result p2

    invoke-virtual {p0, p1, p2}, Lcom/squareup/marin/widgets/MarinPageIndicator;->setMeasuredDimension(II)V

    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .locals 1

    .line 173
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->viewPagerListener:Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_0

    .line 174
    invoke-interface {v0, p1}, Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;->onPageScrollStateChanged(I)V

    :cond_0
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 1

    .line 180
    iput p1, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->currentPage:I

    .line 181
    iput p2, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->pageOffset:F

    .line 182
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinPageIndicator;->invalidate()V

    .line 184
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->viewPagerListener:Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_0

    .line 185
    invoke-interface {v0, p1, p2, p3}, Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;->onPageScrolled(IFI)V

    :cond_0
    return-void
.end method

.method public onPageSelected(I)V
    .locals 1

    .line 190
    iput p1, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->currentPage:I

    .line 191
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinPageIndicator;->invalidate()V

    .line 193
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->viewPagerListener:Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_0

    .line 194
    invoke-interface {v0, p1}, Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;->onPageSelected(I)V

    :cond_0
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .line 126
    check-cast p1, Lcom/squareup/marin/widgets/MarinPageIndicator$SavedState;

    .line 127
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinPageIndicator$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 128
    iget p1, p1, Lcom/squareup/marin/widgets/MarinPageIndicator$SavedState;->currentPage:I

    iput p1, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->currentPage:I

    .line 129
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinPageIndicator;->requestLayout()V

    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .line 133
    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 134
    new-instance v1, Lcom/squareup/marin/widgets/MarinPageIndicator$SavedState;

    invoke-direct {v1, v0}, Lcom/squareup/marin/widgets/MarinPageIndicator$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 135
    iget v0, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->currentPage:I

    iput v0, v1, Lcom/squareup/marin/widgets/MarinPageIndicator$SavedState;->currentPage:I

    return-object v1
.end method

.method public setCurrentItem(I)V
    .locals 1

    .line 160
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->viewPager:Landroidx/viewpager/widget/ViewPager;

    if-eqz v0, :cond_0

    .line 163
    invoke-virtual {v0, p1}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    .line 164
    iput p1, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->currentPage:I

    .line 165
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinPageIndicator;->invalidate()V

    return-void

    .line 161
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "ViewPager has not been bound."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setHighlightColor(I)V
    .locals 1

    .line 209
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->highlightPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method

.method public setMatteColor(I)V
    .locals 1

    .line 219
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->mattePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method

.method public setOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V
    .locals 0

    .line 199
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->viewPagerListener:Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

    return-void
.end method

.method public setViewPager(Landroidx/viewpager/widget/ViewPager;)V
    .locals 2

    .line 145
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->viewPager:Landroidx/viewpager/widget/ViewPager;

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    if-eqz v0, :cond_1

    const/4 v1, 0x0

    .line 149
    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/ViewPager;->setOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    .line 151
    :cond_1
    invoke-virtual {p1}, Landroidx/viewpager/widget/ViewPager;->getAdapter()Landroidx/viewpager/widget/PagerAdapter;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 154
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->viewPager:Landroidx/viewpager/widget/ViewPager;

    .line 155
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinPageIndicator;->viewPager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {p1, p0}, Landroidx/viewpager/widget/ViewPager;->setOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    .line 156
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinPageIndicator;->invalidate()V

    return-void

    .line 152
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "ViewPager does not have adapter instance."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setViewPager(Landroidx/viewpager/widget/ViewPager;I)V
    .locals 0

    .line 140
    invoke-virtual {p0, p1}, Lcom/squareup/marin/widgets/MarinPageIndicator;->setViewPager(Landroidx/viewpager/widget/ViewPager;)V

    .line 141
    invoke-virtual {p0, p2}, Lcom/squareup/marin/widgets/MarinPageIndicator;->setCurrentItem(I)V

    return-void
.end method
