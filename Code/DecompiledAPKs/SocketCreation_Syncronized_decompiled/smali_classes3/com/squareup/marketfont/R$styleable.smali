.class public final Lcom/squareup/marketfont/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/marketfont/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final MarketAutoCompleteTextView:[I

.field public static final MarketAutoCompleteTextView_weight:I

.field public static final MarketButton:[I

.field public static final MarketButton_weight:I

.field public static final MarketCheckedTextView:[I

.field public static final MarketCheckedTextView_weight:I

.field public static final MarketEditText:[I

.field public static final MarketEditText_weight:I

.field public static final MarketRadioButton:[I

.field public static final MarketRadioButton_weight:I

.field public static final MarketTextView:[I

.field public static final MarketTextView_weight:I

.field public static final TextAppearanceSpanCompat:[I

.field public static final TextAppearanceSpanCompat_android_fontFamily:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x1

    new-array v1, v0, [I

    const v2, 0x7f040497

    const/4 v3, 0x0

    aput v2, v1, v3

    .line 28
    sput-object v1, Lcom/squareup/marketfont/R$styleable;->MarketAutoCompleteTextView:[I

    new-array v1, v0, [I

    aput v2, v1, v3

    .line 30
    sput-object v1, Lcom/squareup/marketfont/R$styleable;->MarketButton:[I

    new-array v1, v0, [I

    aput v2, v1, v3

    .line 32
    sput-object v1, Lcom/squareup/marketfont/R$styleable;->MarketCheckedTextView:[I

    new-array v1, v0, [I

    aput v2, v1, v3

    .line 34
    sput-object v1, Lcom/squareup/marketfont/R$styleable;->MarketEditText:[I

    new-array v1, v0, [I

    aput v2, v1, v3

    .line 36
    sput-object v1, Lcom/squareup/marketfont/R$styleable;->MarketRadioButton:[I

    new-array v1, v0, [I

    aput v2, v1, v3

    .line 38
    sput-object v1, Lcom/squareup/marketfont/R$styleable;->MarketTextView:[I

    new-array v0, v0, [I

    const v1, 0x10103ac

    aput v1, v0, v3

    .line 40
    sput-object v0, Lcom/squareup/marketfont/R$styleable;->TextAppearanceSpanCompat:[I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
