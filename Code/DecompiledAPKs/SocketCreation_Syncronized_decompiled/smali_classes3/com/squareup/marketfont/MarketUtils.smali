.class public final Lcom/squareup/marketfont/MarketUtils;
.super Ljava/lang/Object;
.source "MarketUtils.java"


# static fields
.field private static final ANDROID_ATTR_TEXT_APPEARANCE:[I

.field private static final ATTR_NOT_FOUND:I = -0x1

.field private static final MARKET_WEIGHT_ATTR:[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x1

    new-array v1, v0, [I

    const/4 v2, 0x0

    const v3, 0x1010034

    aput v3, v1, v2

    .line 17
    sput-object v1, Lcom/squareup/marketfont/MarketUtils;->ANDROID_ATTR_TEXT_APPEARANCE:[I

    new-array v0, v0, [I

    .line 19
    sget v1, Lcom/squareup/marketfont/R$attr;->marketWeight:I

    aput v1, v0, v2

    sput-object v0, Lcom/squareup/marketfont/MarketUtils;->MARKET_WEIGHT_ATTR:[I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static configureOptimalPaintFlags(Landroid/graphics/Paint;)V
    .locals 0

    .line 75
    invoke-static {p0}, Lcom/squareup/fonts/FontUtilsKt;->configureOptimalTextFlags(Landroid/graphics/Paint;)V

    return-void
.end method

.method public static ensureSpannableTextType(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)Landroid/widget/TextView$BufferType;
    .locals 1

    .line 85
    sget-object v0, Landroid/widget/TextView$BufferType;->NORMAL:Landroid/widget/TextView$BufferType;

    if-ne p1, v0, :cond_0

    instance-of p0, p0, Landroid/text/SpannedString;

    if-eqz p0, :cond_0

    .line 86
    sget-object p1, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    :cond_0
    return-object p1
.end method

.method private static getMarketWeightFromTextAppearance(Landroid/content/Context;Landroid/util/AttributeSet;I)I
    .locals 2

    .line 108
    sget-object v0, Lcom/squareup/marketfont/MarketUtils;->ANDROID_ATTR_TEXT_APPEARANCE:[I

    const/4 v1, 0x0

    .line 109
    invoke-virtual {p0, p1, v0, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    const/4 p2, -0x1

    if-eqz p1, :cond_1

    .line 114
    :try_start_0
    invoke-virtual {p1, v1, p2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 116
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 122
    sget-object p1, Lcom/squareup/marketfont/MarketUtils;->MARKET_WEIGHT_ATTR:[I

    .line 123
    invoke-virtual {p0, v0, p1}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 126
    :try_start_1
    invoke-virtual {p0, v1, p2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 128
    invoke-virtual {p0}, Landroid/content/res/TypedArray;->recycle()V

    return p1

    :catchall_0
    move-exception p1

    invoke-virtual {p0}, Landroid/content/res/TypedArray;->recycle()V

    .line 129
    throw p1

    :cond_0
    return p2

    :catchall_1
    move-exception p0

    .line 116
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 117
    throw p0

    :cond_1
    return p2
.end method

.method public static getWeight(Landroid/content/Context;Landroid/util/AttributeSet;[III)Lcom/squareup/marketfont/MarketFont$Weight;
    .locals 0

    .line 44
    invoke-static {p0, p1, p2, p3, p4}, Lcom/squareup/marketfont/MarketUtils;->getWeightFromStyles(Landroid/content/Context;Landroid/util/AttributeSet;[III)I

    move-result p2

    .line 45
    invoke-static {p0, p1, p4}, Lcom/squareup/marketfont/MarketUtils;->getMarketWeightFromTextAppearance(Landroid/content/Context;Landroid/util/AttributeSet;I)I

    move-result p0

    const/4 p1, -0x1

    if-eq p2, p1, :cond_1

    if-ne p0, p1, :cond_0

    goto :goto_0

    .line 48
    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "marketWeight and weight cannot both be set for the same view."

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_1
    :goto_0
    if-ne p2, p1, :cond_2

    if-ne p0, p1, :cond_2

    .line 52
    sget-object p0, Lcom/squareup/marketfont/MarketFont$Weight;->DEFAULT:Lcom/squareup/marketfont/MarketFont$Weight;

    return-object p0

    :cond_2
    if-ne p2, p1, :cond_3

    goto :goto_1

    :cond_3
    move p0, p2

    .line 56
    :goto_1
    invoke-static {}, Lcom/squareup/marketfont/MarketFont$Weight;->values()[Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object p1

    aget-object p0, p1, p0

    return-object p0
.end method

.method private static getWeightFromStyles(Landroid/content/Context;Landroid/util/AttributeSet;[III)I
    .locals 1

    const/4 v0, 0x0

    .line 97
    invoke-virtual {p0, p1, p2, p4, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p0

    const/4 p1, -0x1

    .line 100
    :try_start_0
    invoke-virtual {p0, p3, p1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    invoke-virtual {p0}, Landroid/content/res/TypedArray;->recycle()V

    return p1

    :catchall_0
    move-exception p1

    invoke-virtual {p0}, Landroid/content/res/TypedArray;->recycle()V

    .line 103
    throw p1
.end method

.method public static setTextViewTypeface(Landroid/widget/TextView;Lcom/squareup/marketfont/MarketFont$Weight;)V
    .locals 2

    .line 65
    invoke-virtual {p0}, Landroid/widget/TextView;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 67
    :cond_0
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 68
    new-instance v1, Lcom/squareup/marketfont/MarketSpannableFactory;

    invoke-direct {v1, v0, p1}, Lcom/squareup/marketfont/MarketSpannableFactory;-><init>(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)V

    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setSpannableFactory(Landroid/text/Spannable$Factory;)V

    .line 69
    invoke-virtual {p0}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object p1

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 70
    invoke-virtual {p0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/marketfont/MarketUtils;->configureOptimalPaintFlags(Landroid/graphics/Paint;)V

    return-void
.end method
