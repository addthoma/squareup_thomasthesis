.class public final Lcom/squareup/money/PriceLocaleHelper_Factory;
.super Ljava/lang/Object;
.source "PriceLocaleHelper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/money/PriceLocaleHelper;",
        ">;"
    }
.end annotation


# instance fields
.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyLocaleDigitsKeyListenerFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyDigitsKeyListenerFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyLocaleHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyDigitsKeyListenerFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/money/PriceLocaleHelper_Factory;->moneyLocaleHelperProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/money/PriceLocaleHelper_Factory;->moneyLocaleDigitsKeyListenerFactoryProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/money/PriceLocaleHelper_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/squareup/money/PriceLocaleHelper_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/money/PriceLocaleHelper_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyDigitsKeyListenerFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)",
            "Lcom/squareup/money/PriceLocaleHelper_Factory;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/money/PriceLocaleHelper_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/money/PriceLocaleHelper_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/money/MoneyDigitsKeyListenerFactory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/money/PriceLocaleHelper;
    .locals 1

    .line 52
    new-instance v0, Lcom/squareup/money/PriceLocaleHelper;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/money/PriceLocaleHelper;-><init>(Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/money/MoneyDigitsKeyListenerFactory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/common/CurrencyCode;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/money/PriceLocaleHelper;
    .locals 4

    .line 38
    iget-object v0, p0, Lcom/squareup/money/PriceLocaleHelper_Factory;->moneyLocaleHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/money/MoneyLocaleHelper;

    iget-object v1, p0, Lcom/squareup/money/PriceLocaleHelper_Factory;->moneyLocaleDigitsKeyListenerFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/money/MoneyDigitsKeyListenerFactory;

    iget-object v2, p0, Lcom/squareup/money/PriceLocaleHelper_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v3, p0, Lcom/squareup/money/PriceLocaleHelper_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/money/PriceLocaleHelper_Factory;->newInstance(Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/money/MoneyDigitsKeyListenerFactory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/money/PriceLocaleHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/money/PriceLocaleHelper_Factory;->get()Lcom/squareup/money/PriceLocaleHelper;

    move-result-object v0

    return-object v0
.end method
