.class public final Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyDigitsKeyListenerFactoryFactory;
.super Ljava/lang/Object;
.source "CurrencyMoneyModule_ProvideMoneyDigitsKeyListenerFactoryFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/money/MoneyDigitsKeyListenerFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyDigitsKeyListenerFactoryFactory;->currencyCodeProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyDigitsKeyListenerFactoryFactory;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyDigitsKeyListenerFactoryFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyDigitsKeyListenerFactoryFactory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyDigitsKeyListenerFactoryFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyDigitsKeyListenerFactoryFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideMoneyDigitsKeyListenerFactory(Lcom/squareup/protos/common/CurrencyCode;Ljava/util/Locale;)Lcom/squareup/money/MoneyDigitsKeyListenerFactory;
    .locals 0

    .line 41
    invoke-static {p0, p1}, Lcom/squareup/money/CurrencyMoneyModule;->provideMoneyDigitsKeyListenerFactory(Lcom/squareup/protos/common/CurrencyCode;Ljava/util/Locale;)Lcom/squareup/money/MoneyDigitsKeyListenerFactory;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/money/MoneyDigitsKeyListenerFactory;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/money/MoneyDigitsKeyListenerFactory;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyDigitsKeyListenerFactoryFactory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v1, p0, Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyDigitsKeyListenerFactoryFactory;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Locale;

    invoke-static {v0, v1}, Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyDigitsKeyListenerFactoryFactory;->provideMoneyDigitsKeyListenerFactory(Lcom/squareup/protos/common/CurrencyCode;Ljava/util/Locale;)Lcom/squareup/money/MoneyDigitsKeyListenerFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyDigitsKeyListenerFactoryFactory;->get()Lcom/squareup/money/MoneyDigitsKeyListenerFactory;

    move-result-object v0

    return-object v0
.end method
