.class public final Lcom/squareup/money/MoneyModule_ProvideRealCentsMoneyFormatterFactory;
.super Ljava/lang/Object;
.source "MoneyModule_ProvideRealCentsMoneyFormatterFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/text/Formatter<",
        "Lcom/squareup/protos/common/Money;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyLocaleFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyLocaleFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyLocaleFormatter;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/money/MoneyModule_ProvideRealCentsMoneyFormatterFactory;->localeProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/money/MoneyModule_ProvideRealCentsMoneyFormatterFactory;->resProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/money/MoneyModule_ProvideRealCentsMoneyFormatterFactory;->moneyLocaleFormatterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/money/MoneyModule_ProvideRealCentsMoneyFormatterFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyLocaleFormatter;",
            ">;)",
            "Lcom/squareup/money/MoneyModule_ProvideRealCentsMoneyFormatterFactory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/money/MoneyModule_ProvideRealCentsMoneyFormatterFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/money/MoneyModule_ProvideRealCentsMoneyFormatterFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideRealCentsMoneyFormatter(Ljavax/inject/Provider;Lcom/squareup/util/Res;Lcom/squareup/money/MoneyLocaleFormatter;)Lcom/squareup/text/Formatter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/money/MoneyLocaleFormatter;",
            ")",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 47
    invoke-static {p0, p1, p2}, Lcom/squareup/money/MoneyModule;->provideRealCentsMoneyFormatter(Ljavax/inject/Provider;Lcom/squareup/util/Res;Lcom/squareup/money/MoneyLocaleFormatter;)Lcom/squareup/text/Formatter;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/text/Formatter;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/text/Formatter;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 36
    iget-object v0, p0, Lcom/squareup/money/MoneyModule_ProvideRealCentsMoneyFormatterFactory;->localeProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/money/MoneyModule_ProvideRealCentsMoneyFormatterFactory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/money/MoneyModule_ProvideRealCentsMoneyFormatterFactory;->moneyLocaleFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/money/MoneyLocaleFormatter;

    invoke-static {v0, v1, v2}, Lcom/squareup/money/MoneyModule_ProvideRealCentsMoneyFormatterFactory;->provideRealCentsMoneyFormatter(Ljavax/inject/Provider;Lcom/squareup/util/Res;Lcom/squareup/money/MoneyLocaleFormatter;)Lcom/squareup/text/Formatter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/money/MoneyModule_ProvideRealCentsMoneyFormatterFactory;->get()Lcom/squareup/text/Formatter;

    move-result-object v0

    return-object v0
.end method
