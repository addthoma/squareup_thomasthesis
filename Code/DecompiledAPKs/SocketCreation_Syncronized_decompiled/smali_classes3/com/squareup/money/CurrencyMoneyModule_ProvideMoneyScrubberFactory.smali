.class public final Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyScrubberFactory;
.super Ljava/lang/Object;
.source "CurrencyMoneyModule_ProvideMoneyScrubberFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/text/SelectableTextScrubber;",
        ">;"
    }
.end annotation


# instance fields
.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyScrubberFactory;->currencyCodeProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyScrubberFactory;->moneyFormatterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyScrubberFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;)",
            "Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyScrubberFactory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyScrubberFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyScrubberFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideMoneyScrubber(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;)Lcom/squareup/text/SelectableTextScrubber;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)",
            "Lcom/squareup/text/SelectableTextScrubber;"
        }
    .end annotation

    .line 45
    invoke-static {p0, p1}, Lcom/squareup/money/CurrencyMoneyModule;->provideMoneyScrubber(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;)Lcom/squareup/text/SelectableTextScrubber;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/text/SelectableTextScrubber;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/text/SelectableTextScrubber;
    .locals 2

    .line 34
    iget-object v0, p0, Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyScrubberFactory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v1, p0, Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyScrubberFactory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/text/Formatter;

    invoke-static {v0, v1}, Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyScrubberFactory;->provideMoneyScrubber(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;)Lcom/squareup/text/SelectableTextScrubber;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/money/CurrencyMoneyModule_ProvideMoneyScrubberFactory;->get()Lcom/squareup/text/SelectableTextScrubber;

    move-result-object v0

    return-object v0
.end method
