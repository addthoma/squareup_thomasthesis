.class public final Lcom/squareup/money/MoneyLocaleFormatter;
.super Ljava/lang/Object;
.source "MoneyLocaleFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/money/MoneyLocaleFormatter$Mode;,
        Lcom/squareup/money/MoneyLocaleFormatter$DecimalFormatCache;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u0002\u000e\u000fB\u0005\u00a2\u0006\u0002\u0010\u0002J \u0010\u0006\u001a\u00020\u00072\u0008\u0010\u0008\u001a\u0004\u0018\u00010\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rR\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/money/MoneyLocaleFormatter;",
        "",
        "()V",
        "decimalFormatCacheThreadLocal",
        "Ljava/lang/ThreadLocal;",
        "Lcom/squareup/money/MoneyLocaleFormatter$DecimalFormatCache;",
        "format",
        "",
        "money",
        "Lcom/squareup/protos/common/Money;",
        "locale",
        "Ljava/util/Locale;",
        "mode",
        "Lcom/squareup/money/MoneyLocaleFormatter$Mode;",
        "DecimalFormatCache",
        "Mode",
        "proto-utilities_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final decimalFormatCacheThreadLocal:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Lcom/squareup/money/MoneyLocaleFormatter$DecimalFormatCache;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lcom/squareup/money/MoneyLocaleFormatter;->decimalFormatCacheThreadLocal:Ljava/lang/ThreadLocal;

    return-void
.end method


# virtual methods
.method public final format(Lcom/squareup/protos/common/Money;Ljava/util/Locale;Lcom/squareup/money/MoneyLocaleFormatter$Mode;)Ljava/lang/String;
    .locals 5

    const-string v0, "locale"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mode"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    .line 44
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    .line 45
    iget-object v1, p0, Lcom/squareup/money/MoneyLocaleFormatter;->decimalFormatCacheThreadLocal:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/money/MoneyLocaleFormatter$DecimalFormatCache;

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    .line 46
    invoke-virtual {v1}, Lcom/squareup/money/MoneyLocaleFormatter$DecimalFormatCache;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v3

    if-ne v0, v3, :cond_1

    invoke-virtual {v1}, Lcom/squareup/money/MoneyLocaleFormatter$DecimalFormatCache;->getLocale()Ljava/util/Locale;

    move-result-object v3

    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    xor-int/2addr v3, v2

    if-eqz v3, :cond_2

    .line 47
    :cond_1
    invoke-static {p2, v0}, Lcom/squareup/currency_db/NumberFormats;->getCurrencyFormat(Ljava/util/Locale;Lcom/squareup/protos/common/CurrencyCode;)Ljava/text/DecimalFormat;

    move-result-object v1

    .line 48
    new-instance v3, Lcom/squareup/money/MoneyLocaleFormatter$DecimalFormatCache;

    const-string v4, "currencyCode"

    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "decimalFormat"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v3, v0, p2, v1}, Lcom/squareup/money/MoneyLocaleFormatter$DecimalFormatCache;-><init>(Lcom/squareup/protos/common/CurrencyCode;Ljava/util/Locale;Ljava/text/DecimalFormat;)V

    .line 49
    iget-object p2, p0, Lcom/squareup/money/MoneyLocaleFormatter;->decimalFormatCacheThreadLocal:Ljava/lang/ThreadLocal;

    invoke-virtual {p2, v3}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    move-object v1, v3

    .line 52
    :cond_2
    sget-object p2, Lcom/squareup/money/MoneyLocaleFormatter$Mode;->SHORTER:Lcom/squareup/money/MoneyLocaleFormatter$Mode;

    const/4 v3, 0x0

    if-ne p3, p2, :cond_3

    invoke-static {p1}, Lcom/squareup/money/MoneyMath;->isWholeUnits(Lcom/squareup/protos/common/Money;)Z

    move-result p2

    if-eqz p2, :cond_3

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_4

    goto :goto_1

    .line 53
    :cond_4
    invoke-static {v0}, Lcom/squareup/currency_db/Currencies;->getFractionDigits(Lcom/squareup/protos/common/CurrencyCode;)I

    move-result v3

    .line 54
    :goto_1
    invoke-virtual {v1}, Lcom/squareup/money/MoneyLocaleFormatter$DecimalFormatCache;->getDecimalFormat()Ljava/text/DecimalFormat;

    move-result-object p2

    invoke-virtual {p2, v3}, Ljava/text/DecimalFormat;->setMinimumFractionDigits(I)V

    .line 55
    invoke-virtual {v1}, Lcom/squareup/money/MoneyLocaleFormatter$DecimalFormatCache;->getDecimalFormat()Ljava/text/DecimalFormat;

    move-result-object p2

    invoke-static {p1}, Lcom/squareup/money/MoneyMath;->asDecimalWholeUnits(Lcom/squareup/protos/common/Money;)Ljava/math/BigDecimal;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "cache.decimalFormat.form\u2026DecimalWholeUnits(money))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
