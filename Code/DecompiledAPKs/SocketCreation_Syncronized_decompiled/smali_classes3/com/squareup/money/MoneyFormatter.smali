.class public final Lcom/squareup/money/MoneyFormatter;
.super Ljava/lang/Object;
.source "MoneyFormatter.kt"

# interfaces
.implements Lcom/squareup/text/Formatter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/text/Formatter<",
        "Lcom/squareup/protos/common/Money;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\'\u0008\u0016\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nB\'\u0008\u0007\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u000b\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\u000cJ\u0012\u0010\r\u001a\u00020\u000e2\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0002H\u0016R\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/money/MoneyFormatter;",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "localeProvider",
        "Lkotlin/Function0;",
        "Ljava/util/Locale;",
        "moneyLocaleFormatter",
        "Lcom/squareup/money/MoneyLocaleFormatter;",
        "mode",
        "Lcom/squareup/money/MoneyLocaleFormatter$Mode;",
        "(Lkotlin/jvm/functions/Function0;Lcom/squareup/money/MoneyLocaleFormatter;Lcom/squareup/money/MoneyLocaleFormatter$Mode;)V",
        "Ljavax/inject/Provider;",
        "(Ljavax/inject/Provider;Lcom/squareup/money/MoneyLocaleFormatter;Lcom/squareup/money/MoneyLocaleFormatter$Mode;)V",
        "format",
        "",
        "money",
        "proto-utilities_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final mode:Lcom/squareup/money/MoneyLocaleFormatter$Mode;

.field private final moneyLocaleFormatter:Lcom/squareup/money/MoneyLocaleFormatter;


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Lcom/squareup/money/MoneyLocaleFormatter;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/money/MoneyLocaleFormatter;",
            ")V"
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/money/MoneyFormatter;-><init>(Ljavax/inject/Provider;Lcom/squareup/money/MoneyLocaleFormatter;Lcom/squareup/money/MoneyLocaleFormatter$Mode;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljavax/inject/Provider;Lcom/squareup/money/MoneyLocaleFormatter;Lcom/squareup/money/MoneyLocaleFormatter$Mode;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/money/MoneyLocaleFormatter;",
            "Lcom/squareup/money/MoneyLocaleFormatter$Mode;",
            ")V"
        }
    .end annotation

    const-string v0, "localeProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyLocaleFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mode"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/money/MoneyFormatter;->localeProvider:Ljavax/inject/Provider;

    iput-object p2, p0, Lcom/squareup/money/MoneyFormatter;->moneyLocaleFormatter:Lcom/squareup/money/MoneyLocaleFormatter;

    iput-object p3, p0, Lcom/squareup/money/MoneyFormatter;->mode:Lcom/squareup/money/MoneyLocaleFormatter$Mode;

    return-void
.end method

.method public synthetic constructor <init>(Ljavax/inject/Provider;Lcom/squareup/money/MoneyLocaleFormatter;Lcom/squareup/money/MoneyLocaleFormatter$Mode;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    .line 13
    sget-object p3, Lcom/squareup/money/MoneyLocaleFormatter$Mode;->DEFAULT:Lcom/squareup/money/MoneyLocaleFormatter$Mode;

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/money/MoneyFormatter;-><init>(Ljavax/inject/Provider;Lcom/squareup/money/MoneyLocaleFormatter;Lcom/squareup/money/MoneyLocaleFormatter$Mode;)V

    return-void
.end method

.method public constructor <init>(Lkotlin/jvm/functions/Function0;Lcom/squareup/money/MoneyLocaleFormatter;Lcom/squareup/money/MoneyLocaleFormatter$Mode;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/money/MoneyLocaleFormatter;",
            "Lcom/squareup/money/MoneyLocaleFormatter$Mode;",
            ")V"
        }
    .end annotation

    const-string v0, "localeProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyLocaleFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mode"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    new-instance v0, Lcom/squareup/money/MoneyFormatter$1;

    invoke-direct {v0, p1}, Lcom/squareup/money/MoneyFormatter$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Ljavax/inject/Provider;

    invoke-direct {p0, v0, p2, p3}, Lcom/squareup/money/MoneyFormatter;-><init>(Ljavax/inject/Provider;Lcom/squareup/money/MoneyLocaleFormatter;Lcom/squareup/money/MoneyLocaleFormatter$Mode;)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/functions/Function0;Lcom/squareup/money/MoneyLocaleFormatter;Lcom/squareup/money/MoneyLocaleFormatter$Mode;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    .line 19
    sget-object p3, Lcom/squareup/money/MoneyLocaleFormatter$Mode;->DEFAULT:Lcom/squareup/money/MoneyLocaleFormatter$Mode;

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/money/MoneyFormatter;-><init>(Lkotlin/jvm/functions/Function0;Lcom/squareup/money/MoneyLocaleFormatter;Lcom/squareup/money/MoneyLocaleFormatter$Mode;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic format(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/protos/common/Money;

    invoke-virtual {p0, p1}, Lcom/squareup/money/MoneyFormatter;->format(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    return-object p1
.end method

.method public format(Lcom/squareup/protos/common/Money;)Ljava/lang/String;
    .locals 3

    .line 23
    iget-object v0, p0, Lcom/squareup/money/MoneyFormatter;->moneyLocaleFormatter:Lcom/squareup/money/MoneyLocaleFormatter;

    iget-object v1, p0, Lcom/squareup/money/MoneyFormatter;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    const-string v2, "localeProvider.get()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/util/Locale;

    iget-object v2, p0, Lcom/squareup/money/MoneyFormatter;->mode:Lcom/squareup/money/MoneyLocaleFormatter$Mode;

    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/money/MoneyLocaleFormatter;->format(Lcom/squareup/protos/common/Money;Ljava/util/Locale;Lcom/squareup/money/MoneyLocaleFormatter$Mode;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
