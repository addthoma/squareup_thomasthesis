.class public final Lcom/squareup/money/CompactMoneyFormatterKt;
.super Ljava/lang/Object;
.source "CompactMoneyFormatter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0003\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "fractionDigits",
        "",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "getFractionDigits",
        "(Lcom/squareup/protos/common/CurrencyCode;)I",
        "proto-utilities_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$getFractionDigits$p(Lcom/squareup/protos/common/CurrencyCode;)I
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/money/CompactMoneyFormatterKt;->getFractionDigits(Lcom/squareup/protos/common/CurrencyCode;)I

    move-result p0

    return p0
.end method

.method private static final getFractionDigits(Lcom/squareup/protos/common/CurrencyCode;)I
    .locals 0

    .line 150
    invoke-static {p0}, Lcom/squareup/currency_db/Currencies;->getFractionDigits(Lcom/squareup/protos/common/CurrencyCode;)I

    move-result p0

    return p0
.end method
