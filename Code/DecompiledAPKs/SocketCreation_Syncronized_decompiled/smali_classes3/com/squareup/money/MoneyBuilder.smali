.class public final Lcom/squareup/money/MoneyBuilder;
.super Ljava/lang/Object;
.source "MoneyBuilder.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0007\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/money/MoneyBuilder;",
        "",
        "()V",
        "of",
        "Lcom/squareup/protos/common/Money;",
        "amount",
        "",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "public"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/money/MoneyBuilder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/money/MoneyBuilder;

    invoke-direct {v0}, Lcom/squareup/money/MoneyBuilder;-><init>()V

    sput-object v0, Lcom/squareup/money/MoneyBuilder;->INSTANCE:Lcom/squareup/money/MoneyBuilder;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "currencyCode"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-static {p2, p0, p1}, Lcom/squareup/money/MoneyBuilderKt;->of(Lcom/squareup/protos/common/CurrencyCode;J)Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0
.end method
