.class public final Lcom/squareup/disputes/DisputesReactor;
.super Ljava/lang/Object;
.source "DisputesReactor.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/rx2/Reactor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/disputes/DisputesReactor$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/legacy/rx2/Reactor<",
        "Lcom/squareup/disputes/DisputesState;",
        "Lcom/squareup/disputes/DisputesEvent;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDisputesReactor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DisputesReactor.kt\ncom/squareup/disputes/DisputesReactor\n*L\n1#1,232:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000  2\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0001:\u0001 B/\u0008\u0001\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ.\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u00112\u0006\u0010\u0012\u001a\u00020\u00022\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u00142\u0006\u0010\u000b\u001a\u00020\u000cH\u0002J*\u0010\u0015\u001a\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00162\u0006\u0010\u0012\u001a\u00020\u00022\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J:\u0010\u0019\u001a\u0016\u0012\u0012\u0008\u0001\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u00110\u001a2\u0006\u0010\u001b\u001a\u00020\u00022\u000c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u001d2\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u0010\u0010\u001e\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020\u0014H\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/disputes/DisputesReactor;",
        "Lcom/squareup/workflow/legacy/rx2/Reactor;",
        "Lcom/squareup/disputes/DisputesState;",
        "Lcom/squareup/disputes/DisputesEvent;",
        "",
        "browserLauncher",
        "Lcom/squareup/util/BrowserLauncher;",
        "server",
        "Lcom/squareup/http/Server;",
        "disputesLoader",
        "Lcom/squareup/disputes/DisputesLoader;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "handlesDisputes",
        "Lcom/squareup/disputes/api/HandlesDisputes;",
        "(Lcom/squareup/util/BrowserLauncher;Lcom/squareup/http/Server;Lcom/squareup/disputes/DisputesLoader;Lcom/squareup/analytics/Analytics;Lcom/squareup/disputes/api/HandlesDisputes;)V",
        "changeStateForPaymentTokenAfterLoadingDisputes",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "initialState",
        "paymentToken",
        "",
        "launch",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "onReact",
        "Lio/reactivex/Single;",
        "state",
        "events",
        "Lcom/squareup/workflow/legacy/rx2/EventChannel;",
        "openUrl",
        "path",
        "Companion",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final BASE_URL_PROD:Ljava/lang/String; = "https://squareup.com/"

.field private static final BASE_URL_STAGING:Ljava/lang/String; = "https://squareupstaging.com/"

.field public static final Companion:Lcom/squareup/disputes/DisputesReactor$Companion;


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final browserLauncher:Lcom/squareup/util/BrowserLauncher;

.field private final disputesLoader:Lcom/squareup/disputes/DisputesLoader;

.field private final handlesDisputes:Lcom/squareup/disputes/api/HandlesDisputes;

.field private final server:Lcom/squareup/http/Server;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/disputes/DisputesReactor$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/disputes/DisputesReactor$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/disputes/DisputesReactor;->Companion:Lcom/squareup/disputes/DisputesReactor$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/BrowserLauncher;Lcom/squareup/http/Server;Lcom/squareup/disputes/DisputesLoader;Lcom/squareup/analytics/Analytics;Lcom/squareup/disputes/api/HandlesDisputes;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "browserLauncher"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "server"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "disputesLoader"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handlesDisputes"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/disputes/DisputesReactor;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iput-object p2, p0, Lcom/squareup/disputes/DisputesReactor;->server:Lcom/squareup/http/Server;

    iput-object p3, p0, Lcom/squareup/disputes/DisputesReactor;->disputesLoader:Lcom/squareup/disputes/DisputesLoader;

    iput-object p4, p0, Lcom/squareup/disputes/DisputesReactor;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p5, p0, Lcom/squareup/disputes/DisputesReactor;->handlesDisputes:Lcom/squareup/disputes/api/HandlesDisputes;

    return-void
.end method

.method public static final synthetic access$changeStateForPaymentTokenAfterLoadingDisputes(Lcom/squareup/disputes/DisputesReactor;Lcom/squareup/disputes/DisputesState;Ljava/lang/String;Lcom/squareup/analytics/Analytics;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 0

    .line 51
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/disputes/DisputesReactor;->changeStateForPaymentTokenAfterLoadingDisputes(Lcom/squareup/disputes/DisputesState;Ljava/lang/String;Lcom/squareup/analytics/Analytics;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/disputes/DisputesReactor;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/disputes/DisputesReactor;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$getBrowserLauncher$p(Lcom/squareup/disputes/DisputesReactor;)Lcom/squareup/util/BrowserLauncher;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/disputes/DisputesReactor;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    return-object p0
.end method

.method public static final synthetic access$getDisputesLoader$p(Lcom/squareup/disputes/DisputesReactor;)Lcom/squareup/disputes/DisputesLoader;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/disputes/DisputesReactor;->disputesLoader:Lcom/squareup/disputes/DisputesLoader;

    return-object p0
.end method

.method public static final synthetic access$getHandlesDisputes$p(Lcom/squareup/disputes/DisputesReactor;)Lcom/squareup/disputes/api/HandlesDisputes;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/disputes/DisputesReactor;->handlesDisputes:Lcom/squareup/disputes/api/HandlesDisputes;

    return-object p0
.end method

.method public static final synthetic access$openUrl(Lcom/squareup/disputes/DisputesReactor;Ljava/lang/String;)V
    .locals 0

    .line 51
    invoke-direct {p0, p1}, Lcom/squareup/disputes/DisputesReactor;->openUrl(Ljava/lang/String;)V

    return-void
.end method

.method private final changeStateForPaymentTokenAfterLoadingDisputes(Lcom/squareup/disputes/DisputesState;Ljava/lang/String;Lcom/squareup/analytics/Analytics;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/disputes/DisputesState;",
            "Ljava/lang/String;",
            "Lcom/squareup/analytics/Analytics;",
            ")",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/disputes/DisputesState;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 207
    instance-of v0, p1, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;

    if-eqz v0, :cond_6

    move-object v0, p2

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    goto :goto_3

    .line 212
    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;

    invoke-virtual {v0}, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;->getResponse()Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->resolved_dispute:Ljava/util/List;

    const-string v2, "initialState.response.resolved_dispute"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/util/Collection;

    invoke-virtual {v0}, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;->getResponse()Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->unresolved_dispute:Ljava/util/List;

    const-string v2, "initialState.response.unresolved_dispute"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v1, v0}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 213
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/protos/client/cbms/DisputedPayment;

    .line 214
    iget-object v2, v2, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_token:Ljava/lang/String;

    invoke-static {v2, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    .line 211
    :goto_2
    check-cast v1, Lcom/squareup/protos/client/cbms/DisputedPayment;

    if-eqz v1, :cond_5

    .line 218
    invoke-static {p3, p2}, Lcom/squareup/disputes/DisputesAnalyticsKt;->selectedDispute(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    .line 219
    new-instance p1, Lcom/squareup/disputes/DisputesState$DetailState;

    invoke-direct {p1, v1}, Lcom/squareup/disputes/DisputesState$DetailState;-><init>(Lcom/squareup/protos/client/cbms/DisputedPayment;)V

    check-cast p1, Lcom/squareup/disputes/DisputesState;

    goto :goto_4

    .line 221
    :cond_5
    invoke-static {p3}, Lcom/squareup/disputes/DisputesAnalyticsKt;->viewedReport(Lcom/squareup/analytics/Analytics;)V

    goto :goto_4

    .line 208
    :cond_6
    :goto_3
    invoke-static {p3}, Lcom/squareup/disputes/DisputesAnalyticsKt;->viewedReport(Lcom/squareup/analytics/Analytics;)V

    .line 206
    :goto_4
    new-instance p2, Lcom/squareup/workflow/legacy/EnterState;

    invoke-direct {p2, p1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    check-cast p2, Lcom/squareup/workflow/legacy/Reaction;

    return-object p2
.end method

.method private final openUrl(Ljava/lang/String;)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/16 v2, 0x2f

    aput-char v2, v0, v1

    .line 191
    invoke-static {p1, v0}, Lkotlin/text/StringsKt;->trimStart(Ljava/lang/String;[C)Ljava/lang/String;

    move-result-object p1

    .line 192
    iget-object v0, p0, Lcom/squareup/disputes/DisputesReactor;->server:Lcom/squareup/http/Server;

    invoke-virtual {v0}, Lcom/squareup/http/Server;->isProduction()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "https://squareup.com/"

    goto :goto_0

    :cond_0
    const-string v0, "https://squareupstaging.com/"

    .line 194
    :goto_0
    iget-object v1, p0, Lcom/squareup/disputes/DisputesReactor;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public launch(Lcom/squareup/disputes/DisputesState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/disputes/DisputesState;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "Lcom/squareup/disputes/DisputesState;",
            "Lcom/squareup/disputes/DisputesEvent;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "initialState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflows"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    .line 64
    invoke-static/range {v1 .. v6}, Lcom/squareup/workflow/legacy/rx2/ReactorKt;->doLaunch$default(Lcom/squareup/workflow/legacy/rx2/Reactor;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    sget-object p2, Lcom/squareup/disputes/DisputesReactor$launch$1;->INSTANCE:Lcom/squareup/disputes/DisputesReactor$launch$1;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, p2}, Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt;->switchMapState(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic launch(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/disputes/DisputesState;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/disputes/DisputesReactor;->launch(Lcom/squareup/disputes/DisputesState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public onReact(Lcom/squareup/disputes/DisputesState;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/disputes/DisputesState;",
            "Lcom/squareup/workflow/legacy/rx2/EventChannel<",
            "Lcom/squareup/disputes/DisputesEvent;",
            ">;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/disputes/DisputesState;",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "events"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflows"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    instance-of v0, p1, Lcom/squareup/disputes/DisputesState$WaitingToStart;

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/disputes/DisputesReactor$onReact$1;->INSTANCE:Lcom/squareup/disputes/DisputesReactor$onReact$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p1}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    goto/16 :goto_0

    .line 81
    :cond_0
    instance-of v0, p1, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingDisputes;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/disputes/DisputesReactor$onReact$2;

    invoke-direct {v0, p0, p3, p1}, Lcom/squareup/disputes/DisputesReactor$onReact$2;-><init>(Lcom/squareup/disputes/DisputesReactor;Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/disputes/DisputesState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    goto/16 :goto_0

    .line 93
    :cond_1
    instance-of v0, p1, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;

    if-eqz v0, :cond_2

    new-instance p3, Lcom/squareup/disputes/DisputesReactor$onReact$3;

    invoke-direct {p3, p0, p1}, Lcom/squareup/disputes/DisputesReactor$onReact$3;-><init>(Lcom/squareup/disputes/DisputesReactor;Lcom/squareup/disputes/DisputesState;)V

    check-cast p3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p3}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    goto/16 :goto_0

    .line 109
    :cond_2
    instance-of v0, p1, Lcom/squareup/disputes/DisputesState$DetailState;

    if-eqz v0, :cond_3

    new-instance p3, Lcom/squareup/disputes/DisputesReactor$onReact$4;

    invoke-direct {p3, p0, p1}, Lcom/squareup/disputes/DisputesReactor$onReact$4;-><init>(Lcom/squareup/disputes/DisputesReactor;Lcom/squareup/disputes/DisputesState;)V

    check-cast p3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p3}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    goto/16 :goto_0

    .line 131
    :cond_3
    instance-of v0, p1, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingMoreDisputes;

    if-eqz v0, :cond_4

    new-instance v0, Lcom/squareup/disputes/DisputesReactor$onReact$5;

    invoke-direct {v0, p0, p1, p3}, Lcom/squareup/disputes/DisputesReactor$onReact$5;-><init>(Lcom/squareup/disputes/DisputesReactor;Lcom/squareup/disputes/DisputesState;Lcom/squareup/workflow/legacy/WorkflowPool;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 148
    :cond_4
    instance-of v0, p1, Lcom/squareup/disputes/DisputesState$ActionDialogState;

    if-eqz v0, :cond_5

    new-instance p3, Lcom/squareup/disputes/DisputesReactor$onReact$6;

    invoke-direct {p3, p0, p1}, Lcom/squareup/disputes/DisputesReactor$onReact$6;-><init>(Lcom/squareup/disputes/DisputesReactor;Lcom/squareup/disputes/DisputesState;)V

    check-cast p3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p3}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 159
    :cond_5
    instance-of v0, p1, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$LoadingForm;

    if-eqz v0, :cond_6

    new-instance v0, Lcom/squareup/disputes/DisputesReactor$onReact$7;

    invoke-direct {v0, p0, p1, p3}, Lcom/squareup/disputes/DisputesReactor$onReact$7;-><init>(Lcom/squareup/disputes/DisputesReactor;Lcom/squareup/disputes/DisputesState;Lcom/squareup/workflow/legacy/WorkflowPool;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 166
    :cond_6
    instance-of p3, p1, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$FormLoaded;

    if-eqz p3, :cond_7

    new-instance p3, Lcom/squareup/disputes/DisputesReactor$onReact$8;

    invoke-direct {p3, p0, p1}, Lcom/squareup/disputes/DisputesReactor$onReact$8;-><init>(Lcom/squareup/disputes/DisputesReactor;Lcom/squareup/disputes/DisputesState;)V

    check-cast p3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p3}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 171
    :cond_7
    instance-of p3, p1, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$LoadingFormError;

    if-eqz p3, :cond_8

    new-instance p3, Lcom/squareup/disputes/DisputesReactor$onReact$9;

    invoke-direct {p3, p1}, Lcom/squareup/disputes/DisputesReactor$onReact$9;-><init>(Lcom/squareup/disputes/DisputesState;)V

    check-cast p3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p3}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 176
    :cond_8
    instance-of p3, p1, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingDisputesError;

    if-eqz p3, :cond_9

    sget-object p1, Lcom/squareup/disputes/DisputesReactor$onReact$10;->INSTANCE:Lcom/squareup/disputes/DisputesReactor$onReact$10;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p1}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 181
    :cond_9
    instance-of p3, p1, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingMoreError;

    if-eqz p3, :cond_a

    new-instance p3, Lcom/squareup/disputes/DisputesReactor$onReact$11;

    invoke-direct {p3, p1}, Lcom/squareup/disputes/DisputesReactor$onReact$11;-><init>(Lcom/squareup/disputes/DisputesState;)V

    check-cast p3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p3}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_a
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic onReact(Ljava/lang/Object;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/disputes/DisputesState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/disputes/DisputesReactor;->onReact(Lcom/squareup/disputes/DisputesState;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
