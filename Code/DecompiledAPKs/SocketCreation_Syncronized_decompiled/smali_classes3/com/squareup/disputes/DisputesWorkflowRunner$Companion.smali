.class public final Lcom/squareup/disputes/DisputesWorkflowRunner$Companion;
.super Ljava/lang/Object;
.source "DisputesWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/disputes/DisputesWorkflowRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDisputesWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DisputesWorkflowRunner.kt\ncom/squareup/disputes/DisputesWorkflowRunner$Companion\n*L\n1#1,100:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0010\t\u001a\u0004\u0018\u00010\u0004R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/disputes/DisputesWorkflowRunner$Companion;",
        "",
        "()V",
        "NAME",
        "",
        "startNewWorkflow",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "paymentToken",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 59
    invoke-direct {p0}, Lcom/squareup/disputes/DisputesWorkflowRunner$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final startNewWorkflow(Lmortar/MortarScope;Ljava/lang/String;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-static {}, Lcom/squareup/disputes/DisputesWorkflowRunner;->access$getNAME$cp()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/container/PosWorkflowRunnerKt;->getWorkflowRunner(Lmortar/MortarScope;Ljava/lang/String;)Lcom/squareup/container/PosWorkflowRunner;

    move-result-object p1

    .line 67
    check-cast p1, Lcom/squareup/disputes/DisputesWorkflowRunner;

    .line 68
    invoke-static {p1}, Lcom/squareup/disputes/DisputesWorkflowRunner;->access$ensureWorkflow(Lcom/squareup/disputes/DisputesWorkflowRunner;)V

    if-eqz p2, :cond_0

    .line 71
    new-instance v0, Lcom/squareup/disputes/InitializeWorkflowEvent$LoadDispute;

    invoke-direct {v0, p2}, Lcom/squareup/disputes/InitializeWorkflowEvent$LoadDispute;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/disputes/InitializeWorkflowEvent;

    goto :goto_0

    .line 73
    :cond_0
    sget-object p2, Lcom/squareup/disputes/InitializeWorkflowEvent$LoadAllDisputes;->INSTANCE:Lcom/squareup/disputes/InitializeWorkflowEvent$LoadAllDisputes;

    move-object v0, p2

    check-cast v0, Lcom/squareup/disputes/InitializeWorkflowEvent;

    .line 69
    :goto_0
    invoke-static {p1, v0}, Lcom/squareup/disputes/DisputesWorkflowRunner;->access$sendEvent(Lcom/squareup/disputes/DisputesWorkflowRunner;Lcom/squareup/disputes/InitializeWorkflowEvent;)V

    return-void
.end method
