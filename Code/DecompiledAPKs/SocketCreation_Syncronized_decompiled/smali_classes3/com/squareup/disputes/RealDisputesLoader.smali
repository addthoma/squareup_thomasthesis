.class public final Lcom/squareup/disputes/RealDisputesLoader;
.super Ljava/lang/Object;
.source "DisputesLoader.kt"

# interfaces
.implements Lcom/squareup/disputes/DisputesLoader;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDisputesLoader.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DisputesLoader.kt\ncom/squareup/disputes/RealDisputesLoader\n+ 2 ProtosPure.kt\ncom/squareup/util/ProtosPure\n*L\n1#1,131:1\n132#2,3:132\n*E\n*S KotlinDebug\n*F\n+ 1 DisputesLoader.kt\ncom/squareup/disputes/RealDisputesLoader\n*L\n117#1,3:132\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0000\u0018\u00002\u00020\u0001B)\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u0010\u0010\u0013\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00150\u0014H\u0016J\u0008\u0010\u0016\u001a\u00020\u0015H\u0016J\u0018\u0010\u0017\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00150\u00142\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J\u0018\u0010\u001a\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00150\u00142\u0006\u0010\u001b\u001a\u00020\u001cH\u0016J\u0019\u0010\u001d\u001a\u00020\u001e2\n\u0008\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0002\u00a2\u0006\u0002\u0010\u001fJ\u0014\u0010 \u001a\n \"*\u0004\u0018\u00010!0!*\u00020\u0019H\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/disputes/RealDisputesLoader;",
        "Lcom/squareup/disputes/DisputesLoader;",
        "service",
        "Lcom/squareup/server/disputes/DisputesService;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "messages",
        "Lcom/squareup/receiving/FailureMessageFactory;",
        "res",
        "Landroid/content/res/Resources;",
        "(Lcom/squareup/server/disputes/DisputesService;Lio/reactivex/Scheduler;Lcom/squareup/receiving/FailureMessageFactory;Landroid/content/res/Resources;)V",
        "baseResponse",
        "Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;",
        "dateTimeFactory",
        "Lcom/squareup/util/DateTimeFactory;",
        "addResolved",
        "",
        "moreDisputes",
        "Lcom/squareup/protos/client/cbms/ListDisputesResponse;",
        "initialLoadDisputes",
        "Lio/reactivex/Single;",
        "Lcom/squareup/disputes/DisputesState;",
        "latestResponseState",
        "loadForm",
        "disputedPayment",
        "Lcom/squareup/protos/client/cbms/DisputedPayment;",
        "loadMore",
        "cursor",
        "",
        "request",
        "Lcom/squareup/protos/client/cbms/ListDisputesRequest;",
        "(Ljava/lang/Integer;)Lcom/squareup/protos/client/cbms/ListDisputesRequest;",
        "getFormToken",
        "",
        "kotlin.jvm.PlatformType",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private baseResponse:Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

.field private final dateTimeFactory:Lcom/squareup/util/DateTimeFactory;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final messages:Lcom/squareup/receiving/FailureMessageFactory;

.field private final res:Landroid/content/res/Resources;

.field private final service:Lcom/squareup/server/disputes/DisputesService;


# direct methods
.method public constructor <init>(Lcom/squareup/server/disputes/DisputesService;Lio/reactivex/Scheduler;Lcom/squareup/receiving/FailureMessageFactory;Landroid/content/res/Resources;)V
    .locals 1
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "service"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messages"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/disputes/RealDisputesLoader;->service:Lcom/squareup/server/disputes/DisputesService;

    iput-object p2, p0, Lcom/squareup/disputes/RealDisputesLoader;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p3, p0, Lcom/squareup/disputes/RealDisputesLoader;->messages:Lcom/squareup/receiving/FailureMessageFactory;

    iput-object p4, p0, Lcom/squareup/disputes/RealDisputesLoader;->res:Landroid/content/res/Resources;

    .line 42
    new-instance p1, Lcom/squareup/util/DateTimeFactory;

    invoke-direct {p1}, Lcom/squareup/util/DateTimeFactory;-><init>()V

    iput-object p1, p0, Lcom/squareup/disputes/RealDisputesLoader;->dateTimeFactory:Lcom/squareup/util/DateTimeFactory;

    return-void
.end method

.method public static final synthetic access$addResolved(Lcom/squareup/disputes/RealDisputesLoader;Lcom/squareup/protos/client/cbms/ListDisputesResponse;)V
    .locals 0

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/disputes/RealDisputesLoader;->addResolved(Lcom/squareup/protos/client/cbms/ListDisputesResponse;)V

    return-void
.end method

.method public static final synthetic access$getBaseResponse$p(Lcom/squareup/disputes/RealDisputesLoader;)Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;
    .locals 1

    .line 35
    iget-object p0, p0, Lcom/squareup/disputes/RealDisputesLoader;->baseResponse:Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    if-nez p0, :cond_0

    const-string v0, "baseResponse"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getMessages$p(Lcom/squareup/disputes/RealDisputesLoader;)Lcom/squareup/receiving/FailureMessageFactory;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/disputes/RealDisputesLoader;->messages:Lcom/squareup/receiving/FailureMessageFactory;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/disputes/RealDisputesLoader;)Landroid/content/res/Resources;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/disputes/RealDisputesLoader;->res:Landroid/content/res/Resources;

    return-object p0
.end method

.method public static final synthetic access$setBaseResponse$p(Lcom/squareup/disputes/RealDisputesLoader;Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;)V
    .locals 0

    .line 35
    iput-object p1, p0, Lcom/squareup/disputes/RealDisputesLoader;->baseResponse:Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    return-void
.end method

.method private final addResolved(Lcom/squareup/protos/client/cbms/ListDisputesResponse;)V
    .locals 5

    .line 117
    iget-object v0, p0, Lcom/squareup/disputes/RealDisputesLoader;->baseResponse:Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    if-nez v0, :cond_0

    const-string v1, "baseResponse"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Lcom/squareup/wire/Message;

    .line 133
    invoke-virtual {v0}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object v0

    if-eqz v0, :cond_1

    move-object v1, v0

    check-cast v1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;

    .line 118
    iget-object v2, v1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->resolved_dispute:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/ListDisputesResponse;->dispute:Ljava/util/List;

    const-string v4, "moreDisputes.dispute"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Ljava/util/Collection;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 119
    iget-object p1, p1, Lcom/squareup/protos/client/cbms/ListDisputesResponse;->cursor:Ljava/lang/Integer;

    iput-object p1, v1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->resolved_dispute_cursor:Ljava/lang/Integer;

    .line 134
    invoke-virtual {v0}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object p1

    const-string v0, "baseResponse.buildUpon {\u2026moreDisputes.cursor\n    }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    iput-object p1, p0, Lcom/squareup/disputes/RealDisputesLoader;->baseResponse:Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    return-void

    .line 133
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type B"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final getFormToken(Lcom/squareup/protos/client/cbms/DisputedPayment;)Ljava/lang/String;
    .locals 1

    .line 123
    iget-object p1, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->information_request:Ljava/util/List;

    const-string v0, "information_request"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/cbms/InformationRequest;

    iget-object p1, p1, Lcom/squareup/protos/client/cbms/InformationRequest;->form_token:Ljava/lang/String;

    return-object p1
.end method

.method private final request(Ljava/lang/Integer;)Lcom/squareup/protos/client/cbms/ListDisputesRequest;
    .locals 1

    .line 109
    new-instance v0, Lcom/squareup/protos/client/cbms/ListDisputesRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/cbms/ListDisputesRequest$Builder;-><init>()V

    .line 110
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/cbms/ListDisputesRequest$Builder;->cursor(Ljava/lang/Integer;)Lcom/squareup/protos/client/cbms/ListDisputesRequest$Builder;

    move-result-object p1

    .line 111
    iget-object v0, p0, Lcom/squareup/disputes/RealDisputesLoader;->dateTimeFactory:Lcom/squareup/util/DateTimeFactory;

    invoke-virtual {v0}, Lcom/squareup/util/DateTimeFactory;->now()Lcom/squareup/protos/common/time/DateTime;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/cbms/ListDisputesRequest$Builder;->created_at_max(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/cbms/ListDisputesRequest$Builder;

    move-result-object p1

    .line 112
    iget-object v0, p0, Lcom/squareup/disputes/RealDisputesLoader;->dateTimeFactory:Lcom/squareup/util/DateTimeFactory;

    invoke-static {v0}, Lcom/squareup/disputes/DisputesLoaderKt;->access$fiveYearsAgo(Lcom/squareup/util/DateTimeFactory;)Lcom/squareup/protos/common/time/DateTime;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/cbms/ListDisputesRequest$Builder;->created_at_min(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/cbms/ListDisputesRequest$Builder;

    move-result-object p1

    const/16 v0, 0xa

    .line 113
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/cbms/ListDisputesRequest$Builder;->limit(Ljava/lang/Integer;)Lcom/squareup/protos/client/cbms/ListDisputesRequest$Builder;

    move-result-object p1

    .line 114
    invoke-virtual {p1}, Lcom/squareup/protos/client/cbms/ListDisputesRequest$Builder;->build()Lcom/squareup/protos/client/cbms/ListDisputesRequest;

    move-result-object p1

    const-string v0, "ListDisputesRequest.Buil\u2026.limit(10)\n      .build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method static synthetic request$default(Lcom/squareup/disputes/RealDisputesLoader;Ljava/lang/Integer;ILjava/lang/Object;)Lcom/squareup/protos/client/cbms/ListDisputesRequest;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 109
    check-cast p1, Ljava/lang/Integer;

    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/disputes/RealDisputesLoader;->request(Ljava/lang/Integer;)Lcom/squareup/protos/client/cbms/ListDisputesRequest;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public initialLoadDisputes()Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/disputes/DisputesState;",
            ">;"
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/squareup/disputes/RealDisputesLoader;->service:Lcom/squareup/server/disputes/DisputesService;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {p0, v1, v2, v1}, Lcom/squareup/disputes/RealDisputesLoader;->request$default(Lcom/squareup/disputes/RealDisputesLoader;Ljava/lang/Integer;ILjava/lang/Object;)Lcom/squareup/protos/client/cbms/ListDisputesRequest;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/server/disputes/DisputesService;->getDisputeSummaryAndListDisputes(Lcom/squareup/protos/client/cbms/ListDisputesRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 48
    iget-object v1, p0, Lcom/squareup/disputes/RealDisputesLoader;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    .line 49
    new-instance v1, Lcom/squareup/disputes/RealDisputesLoader$initialLoadDisputes$1;

    invoke-direct {v1, p0}, Lcom/squareup/disputes/RealDisputesLoader$initialLoadDisputes$1;-><init>(Lcom/squareup/disputes/RealDisputesLoader;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "service.getDisputeSummar\u2026  }\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public latestResponseState()Lcom/squareup/disputes/DisputesState;
    .locals 5

    .line 87
    new-instance v0, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;

    iget-object v1, p0, Lcom/squareup/disputes/RealDisputesLoader;->baseResponse:Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    if-nez v1, :cond_0

    const-string v2, "baseResponse"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;-><init>(Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Lcom/squareup/disputes/DisputesState;

    return-object v0
.end method

.method public loadForm(Lcom/squareup/protos/client/cbms/DisputedPayment;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/cbms/DisputedPayment;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/disputes/DisputesState;",
            ">;"
        }
    .end annotation

    const-string v0, "disputedPayment"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    iget-object v0, p0, Lcom/squareup/disputes/RealDisputesLoader;->service:Lcom/squareup/server/disputes/DisputesService;

    new-instance v1, Lcom/squareup/protos/client/irf/GetFormRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/irf/GetFormRequest$Builder;-><init>()V

    invoke-direct {p0, p1}, Lcom/squareup/disputes/RealDisputesLoader;->getFormToken(Lcom/squareup/protos/client/cbms/DisputedPayment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/irf/GetFormRequest$Builder;->token(Ljava/lang/String;)Lcom/squareup/protos/client/irf/GetFormRequest$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/client/irf/GetFormRequest$Builder;->build()Lcom/squareup/protos/client/irf/GetFormRequest;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/server/disputes/DisputesService;->getForm(Lcom/squareup/protos/client/irf/GetFormRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 92
    iget-object v1, p0, Lcom/squareup/disputes/RealDisputesLoader;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    .line 93
    new-instance v1, Lcom/squareup/disputes/RealDisputesLoader$loadForm$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/disputes/RealDisputesLoader$loadForm$1;-><init>(Lcom/squareup/disputes/RealDisputesLoader;Lcom/squareup/protos/client/cbms/DisputedPayment;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "service.getForm(GetFormR\u2026  }\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public loadMore(I)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/disputes/DisputesState;",
            ">;"
        }
    .end annotation

    .line 69
    iget-object v0, p0, Lcom/squareup/disputes/RealDisputesLoader;->service:Lcom/squareup/server/disputes/DisputesService;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/disputes/RealDisputesLoader;->request(Ljava/lang/Integer;)Lcom/squareup/protos/client/cbms/ListDisputesRequest;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/server/disputes/DisputesService;->listResolvedDisputes(Lcom/squareup/protos/client/cbms/ListDisputesRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 70
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 71
    iget-object v0, p0, Lcom/squareup/disputes/RealDisputesLoader;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    .line 72
    new-instance v0, Lcom/squareup/disputes/RealDisputesLoader$loadMore$1;

    invoke-direct {v0, p0}, Lcom/squareup/disputes/RealDisputesLoader$loadMore$1;-><init>(Lcom/squareup/disputes/RealDisputesLoader;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "service.listResolvedDisp\u2026d(baseResponse)\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
