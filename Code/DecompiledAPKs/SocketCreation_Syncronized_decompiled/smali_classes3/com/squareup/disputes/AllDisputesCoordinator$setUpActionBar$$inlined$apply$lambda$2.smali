.class final Lcom/squareup/disputes/AllDisputesCoordinator$setUpActionBar$$inlined$apply$lambda$2;
.super Lkotlin/jvm/internal/Lambda;
.source "AllDisputesCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/disputes/AllDisputesCoordinator;->setUpActionBar(Lcom/squareup/disputes/AllDisputes$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke",
        "com/squareup/disputes/AllDisputesCoordinator$setUpActionBar$1$2"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screenData$inlined:Lcom/squareup/disputes/AllDisputes$ScreenData;

.field final synthetic $workflow$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

.field final synthetic this$0:Lcom/squareup/disputes/AllDisputesCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/disputes/AllDisputesCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/disputes/AllDisputes$ScreenData;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/disputes/AllDisputesCoordinator$setUpActionBar$$inlined$apply$lambda$2;->this$0:Lcom/squareup/disputes/AllDisputesCoordinator;

    iput-object p2, p0, Lcom/squareup/disputes/AllDisputesCoordinator$setUpActionBar$$inlined$apply$lambda$2;->$workflow$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

    iput-object p3, p0, Lcom/squareup/disputes/AllDisputesCoordinator$setUpActionBar$$inlined$apply$lambda$2;->$screenData$inlined:Lcom/squareup/disputes/AllDisputes$ScreenData;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 34
    invoke-virtual {p0}, Lcom/squareup/disputes/AllDisputesCoordinator$setUpActionBar$$inlined$apply$lambda$2;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 2

    .line 150
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesCoordinator$setUpActionBar$$inlined$apply$lambda$2;->$workflow$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

    sget-object v1, Lcom/squareup/disputes/AllDisputesEvent$LearnMoreClicked;->INSTANCE:Lcom/squareup/disputes/AllDisputesEvent$LearnMoreClicked;

    invoke-interface {v0, v1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
