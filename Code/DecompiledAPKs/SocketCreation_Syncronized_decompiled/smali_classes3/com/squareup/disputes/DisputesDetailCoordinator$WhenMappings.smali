.class public final synthetic Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I

.field public static final synthetic $EnumSwitchMapping$2:[I

.field public static final synthetic $EnumSwitchMapping$3:[I

.field public static final synthetic $EnumSwitchMapping$4:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 9

    invoke-static {}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->values()[Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->NEW_DISPUTE:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->DISPUTE_REOPENED:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->LOST:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->WON:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->ordinal()I

    move-result v1

    const/4 v5, 0x4

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->ACCEPTED:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->ordinal()I

    move-result v1

    const/4 v6, 0x5

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->UNDER_REVIEW:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->ordinal()I

    move-result v1

    const/4 v7, 0x6

    aput v7, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->PENDING_RESOLUTION:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->ordinal()I

    move-result v1

    const/4 v8, 0x7

    aput v8, v0, v1

    invoke-static {}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->values()[Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->AMOUNT_DIFFERS:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->CANCELLED:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->COMPLIANCE:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->CUSTOMER_REQUESTS_CREDIT:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->ordinal()I

    move-result v1

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->DISSATISFIED:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->ordinal()I

    move-result v1

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->DUPLICATE:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->ordinal()I

    move-result v1

    aput v7, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->EMV_LIABILITY_SHIFT:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->ordinal()I

    move-result v1

    aput v8, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->FRAUD:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->ordinal()I

    move-result v1

    const/16 v6, 0x8

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->NO_KNOWLEDGE:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->ordinal()I

    move-result v1

    const/16 v6, 0x9

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->NOT_AS_DESCRIBED:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->ordinal()I

    move-result v1

    const/16 v6, 0xa

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->NOT_RECEIVED:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->ordinal()I

    move-result v1

    const/16 v6, 0xb

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->PAID_BY_OTHER_MEANS:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->ordinal()I

    move-result v1

    const/16 v6, 0xc

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->UNAUTHORIZED:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->ordinal()I

    move-result v1

    const/16 v6, 0xd

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->UNKNOWN:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->ordinal()I

    move-result v1

    const/16 v6, 0xe

    aput v6, v0, v1

    invoke-static {}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->values()[Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->PENDING_RESOLUTION:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->UNDER_REVIEW:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->ordinal()I

    move-result v1

    aput v3, v0, v1

    invoke-static {}, Lcom/squareup/protos/client/cbms/ActionableStatus;->values()[Lcom/squareup/protos/client/cbms/ActionableStatus;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/ActionableStatus;->NO_ACTION_DISPUTED_AMOUNT_TOO_LOW:Lcom/squareup/protos/client/cbms/ActionableStatus;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/ActionableStatus;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/ActionableStatus;->NO_ACTION_REFUNDED:Lcom/squareup/protos/client/cbms/ActionableStatus;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/ActionableStatus;->ordinal()I

    move-result v1

    aput v3, v0, v1

    invoke-static {}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->values()[Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->ACCEPTED:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->LOST:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->WON:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->DISPUTE_REOPENED:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->ordinal()I

    move-result v1

    aput v5, v0, v1

    return-void
.end method
