.class public final Lcom/squareup/disputes/SummaryCoordinator_Factory_Factory;
.super Ljava/lang/Object;
.source "SummaryCoordinator_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/disputes/SummaryCoordinator$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final summaryAdapterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/SummaryAdapter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/SummaryAdapter;",
            ">;)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/disputes/SummaryCoordinator_Factory_Factory;->summaryAdapterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/disputes/SummaryCoordinator_Factory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/SummaryAdapter;",
            ">;)",
            "Lcom/squareup/disputes/SummaryCoordinator_Factory_Factory;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/disputes/SummaryCoordinator_Factory_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/disputes/SummaryCoordinator_Factory_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljavax/inject/Provider;)Lcom/squareup/disputes/SummaryCoordinator$Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/SummaryAdapter;",
            ">;)",
            "Lcom/squareup/disputes/SummaryCoordinator$Factory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/disputes/SummaryCoordinator$Factory;

    invoke-direct {v0, p0}, Lcom/squareup/disputes/SummaryCoordinator$Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/disputes/SummaryCoordinator$Factory;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/disputes/SummaryCoordinator_Factory_Factory;->summaryAdapterProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/squareup/disputes/SummaryCoordinator_Factory_Factory;->newInstance(Ljavax/inject/Provider;)Lcom/squareup/disputes/SummaryCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/disputes/SummaryCoordinator_Factory_Factory;->get()Lcom/squareup/disputes/SummaryCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method
