.class public final Lcom/squareup/disputes/Summary$ScreenData$WithData;
.super Lcom/squareup/disputes/Summary$ScreenData;
.source "SummaryScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/disputes/Summary$ScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "WithData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/disputes/Summary$ScreenData$WithData;",
        "Lcom/squareup/disputes/Summary$ScreenData;",
        "challengeEngine",
        "Lcom/squareup/disputes/ChallengeEngine;",
        "(Lcom/squareup/disputes/ChallengeEngine;)V",
        "getChallengeEngine",
        "()Lcom/squareup/disputes/ChallengeEngine;",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final challengeEngine:Lcom/squareup/disputes/ChallengeEngine;


# direct methods
.method public constructor <init>(Lcom/squareup/disputes/ChallengeEngine;)V
    .locals 1

    const-string v0, "challengeEngine"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 18
    invoke-direct {p0, v0}, Lcom/squareup/disputes/Summary$ScreenData;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/disputes/Summary$ScreenData$WithData;->challengeEngine:Lcom/squareup/disputes/ChallengeEngine;

    return-void
.end method


# virtual methods
.method public final getChallengeEngine()Lcom/squareup/disputes/ChallengeEngine;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/disputes/Summary$ScreenData$WithData;->challengeEngine:Lcom/squareup/disputes/ChallengeEngine;

    return-object v0
.end method
