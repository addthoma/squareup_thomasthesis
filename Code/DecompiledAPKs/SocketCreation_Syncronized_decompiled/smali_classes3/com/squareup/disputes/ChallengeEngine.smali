.class public final Lcom/squareup/disputes/ChallengeEngine;
.super Ljava/lang/Object;
.source "ChallengeEngine.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nChallengeEngine.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ChallengeEngine.kt\ncom/squareup/disputes/ChallengeEngine\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,177:1\n704#2:178\n777#2,2:179\n704#2:181\n777#2,2:182\n1642#2,2:184\n713#2,10:186\n1651#2,2:196\n723#2,2:198\n1653#2:200\n725#2:201\n1642#2,2:202\n310#2,7:204\n310#2,7:211\n310#2,7:218\n310#2,7:225\n704#2:232\n777#2,2:233\n704#2:235\n777#2,2:236\n1847#2,3:238\n215#2,2:241\n1265#2,12:243\n1265#2,9:255\n1360#2:264\n1429#2,3:265\n1274#2,3:268\n1462#2,8:271\n1099#2,2:279\n1127#2,4:281\n*E\n*S KotlinDebug\n*F\n+ 1 ChallengeEngine.kt\ncom/squareup/disputes/ChallengeEngine\n*L\n63#1:178\n63#1,2:179\n66#1:181\n66#1,2:182\n66#1,2:184\n70#1,10:186\n70#1,2:196\n70#1,2:198\n70#1:200\n70#1:201\n75#1,2:202\n88#1,7:204\n102#1,7:211\n116#1,7:218\n130#1,7:225\n143#1:232\n143#1,2:233\n145#1:235\n145#1,2:236\n154#1,3:238\n162#1,2:241\n32#1,12:243\n37#1,9:255\n37#1:264\n37#1,3:265\n37#1,3:268\n41#1,8:271\n43#1,2:279\n43#1,4:281\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0008\u000c\n\u0002\u0010\u0002\n\u0000\u0008\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u0011\u001a\u00020\tJ\u000e\u0010\u0012\u001a\u00020\t2\u0006\u0010\u0013\u001a\u00020\u000eJ\u000e\u0010\u0014\u001a\u00020\t2\u0006\u0010\u0013\u001a\u00020\u000eJ\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008J\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008J\u000e\u0010\u0017\u001a\u00020\u00062\u0006\u0010\u0013\u001a\u00020\u000eJ\u000e\u0010\u0018\u001a\u00020\u00062\u0006\u0010\u0013\u001a\u00020\u000eJ\u0006\u0010\u0019\u001a\u00020\u0006J\u0006\u0010\u001a\u001a\u00020\u001bR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR \u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\t0\rX\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/disputes/ChallengeEngine;",
        "",
        "irfProto",
        "Lcom/squareup/protos/client/irf/InformationRequestDetail;",
        "(Lcom/squareup/protos/client/irf/InformationRequestDetail;)V",
        "alreadyComplete",
        "",
        "fieldsList",
        "",
        "Lcom/squareup/disputes/Field;",
        "getFieldsList$disputes_release",
        "()Ljava/util/List;",
        "fieldsMap",
        "",
        "",
        "getFieldsMap$disputes_release",
        "()Ljava/util/Map;",
        "getFirstIncompleteField",
        "getNextField",
        "currentFieldName",
        "getPreviousField",
        "getSummary",
        "getVisibleFields",
        "hasNextField",
        "hasPreviousField",
        "isFormComplete",
        "setUpFields",
        "",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final alreadyComplete:Z

.field private final fieldsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/disputes/Field;",
            ">;"
        }
    .end annotation
.end field

.field private final fieldsMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/disputes/Field;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/irf/InformationRequestDetail;)V
    .locals 8

    const-string v0, "irfProto"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iget-object v0, p1, Lcom/squareup/protos/client/irf/InformationRequestDetail;->page:Ljava/util/List;

    const-string v1, "irfProto.page"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 243
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 250
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 251
    check-cast v2, Lcom/squareup/protos/client/irf/Page;

    .line 32
    iget-object v2, v2, Lcom/squareup/protos/client/irf/Page;->section:Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 252
    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0

    .line 254
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 33
    iget-object v0, p1, Lcom/squareup/protos/client/irf/InformationRequestDetail;->status:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    sget-object v2, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;->COMPLETED:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    if-eq v0, v2, :cond_2

    iget-object p1, p1, Lcom/squareup/protos/client/irf/InformationRequestDetail;->status:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    sget-object v0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;->REVIEWED:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    if-ne p1, v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 p1, 0x1

    :goto_2
    iput-boolean p1, p0, Lcom/squareup/disputes/ChallengeEngine;->alreadyComplete:Z

    .line 35
    new-instance p1, Lcom/google/gson/Gson;

    invoke-direct {p1}, Lcom/google/gson/Gson;-><init>()V

    .line 37
    check-cast v1, Ljava/lang/Iterable;

    .line 255
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 262
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/16 v3, 0xa

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 263
    check-cast v2, Lcom/squareup/protos/client/irf/Section;

    .line 38
    iget-object v4, v2, Lcom/squareup/protos/client/irf/Section;->field:Ljava/util/List;

    const-string v5, "section.field"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Ljava/lang/Iterable;

    .line 264
    new-instance v5, Ljava/util/ArrayList;

    invoke-static {v4, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v5, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v5, Ljava/util/Collection;

    .line 265
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 266
    check-cast v4, Lcom/squareup/protos/client/irf/Field;

    .line 39
    new-instance v6, Lcom/squareup/disputes/Field$Factory;

    invoke-direct {v6, p1}, Lcom/squareup/disputes/Field$Factory;-><init>(Lcom/google/gson/Gson;)V

    const-string v7, "it"

    invoke-static {v4, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6, v2, v4}, Lcom/squareup/disputes/Field$Factory;->create(Lcom/squareup/protos/client/irf/Section;Lcom/squareup/protos/client/irf/Field;)Lcom/squareup/disputes/Field;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 267
    :cond_3
    check-cast v5, Ljava/util/List;

    .line 40
    check-cast v5, Ljava/lang/Iterable;

    .line 268
    invoke-static {v0, v5}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_3

    .line 270
    :cond_4
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 271
    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    .line 272
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 273
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_5
    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 274
    move-object v4, v2

    check-cast v4, Lcom/squareup/disputes/Field;

    .line 41
    invoke-virtual {v4}, Lcom/squareup/disputes/Field;->getFieldName()Ljava/lang/String;

    move-result-object v4

    .line 275
    invoke-virtual {p1, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 276
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 278
    :cond_6
    check-cast v1, Ljava/util/List;

    iput-object v1, p0, Lcom/squareup/disputes/ChallengeEngine;->fieldsList:Ljava/util/List;

    .line 43
    iget-object p1, p0, Lcom/squareup/disputes/ChallengeEngine;->fieldsList:Ljava/util/List;

    check-cast p1, Ljava/lang/Iterable;

    .line 279
    invoke-static {p1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-static {v0}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result v0

    const/16 v1, 0x10

    invoke-static {v0, v1}, Lkotlin/ranges/RangesKt;->coerceAtLeast(II)I

    move-result v0

    .line 280
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1, v0}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v1, Ljava/util/Map;

    .line 281
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_6
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 282
    move-object v2, v0

    check-cast v2, Lcom/squareup/disputes/Field;

    .line 43
    invoke-virtual {v2}, Lcom/squareup/disputes/Field;->getFieldName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_6

    .line 284
    :cond_7
    iput-object v1, p0, Lcom/squareup/disputes/ChallengeEngine;->fieldsMap:Ljava/util/Map;

    .line 45
    invoke-virtual {p0}, Lcom/squareup/disputes/ChallengeEngine;->setUpFields()V

    return-void
.end method


# virtual methods
.method public final getFieldsList$disputes_release()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/disputes/Field;",
            ">;"
        }
    .end annotation

    .line 22
    iget-object v0, p0, Lcom/squareup/disputes/ChallengeEngine;->fieldsList:Ljava/util/List;

    return-object v0
.end method

.method public final getFieldsMap$disputes_release()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/disputes/Field;",
            ">;"
        }
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/squareup/disputes/ChallengeEngine;->fieldsMap:Ljava/util/Map;

    return-object v0
.end method

.method public final getFirstIncompleteField()Lcom/squareup/disputes/Field;
    .locals 3

    .line 162
    iget-object v0, p0, Lcom/squareup/disputes/ChallengeEngine;->fieldsList:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 241
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/disputes/Field;

    .line 162
    iget-object v2, p0, Lcom/squareup/disputes/ChallengeEngine;->fieldsMap:Ljava/util/Map;

    invoke-virtual {v1, v2}, Lcom/squareup/disputes/Field;->canShow(Ljava/util/Map;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/squareup/disputes/Field;->hasAnswer()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_0

    return-object v1

    .line 242
    :cond_2
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "Collection contains no element matching the predicate."

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final getNextField(Ljava/lang/String;)Lcom/squareup/disputes/Field;
    .locals 4

    const-string v0, "currentFieldName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    invoke-virtual {p0}, Lcom/squareup/disputes/ChallengeEngine;->getVisibleFields()Ljava/util/List;

    move-result-object v0

    .line 212
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 213
    check-cast v3, Lcom/squareup/disputes/Field;

    .line 102
    invoke-virtual {v3}, Lcom/squareup/disputes/Field;->getFieldName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, -0x1

    :goto_1
    if-ltz v2, :cond_2

    add-int/lit8 v2, v2, 0x1

    .line 106
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/disputes/Field;

    return-object p1

    .line 104
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "current field "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " should not be shown"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final getPreviousField(Ljava/lang/String;)Lcom/squareup/disputes/Field;
    .locals 4

    const-string v0, "currentFieldName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    invoke-virtual {p0}, Lcom/squareup/disputes/ChallengeEngine;->getVisibleFields()Ljava/util/List;

    move-result-object v0

    .line 226
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 227
    check-cast v3, Lcom/squareup/disputes/Field;

    .line 130
    invoke-virtual {v3}, Lcom/squareup/disputes/Field;->getFieldName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, -0x1

    :goto_1
    if-ltz v2, :cond_2

    add-int/lit8 v2, v2, -0x1

    .line 134
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/disputes/Field;

    return-object p1

    .line 132
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "current field "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " should not be shown"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final getSummary()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/disputes/Field;",
            ">;"
        }
    .end annotation

    .line 170
    invoke-virtual {p0}, Lcom/squareup/disputes/ChallengeEngine;->isFormComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    invoke-virtual {p0}, Lcom/squareup/disputes/ChallengeEngine;->getVisibleFields()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 173
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Form is not complete"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final getVisibleFields()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/disputes/Field;",
            ">;"
        }
    .end annotation

    .line 142
    invoke-virtual {p0}, Lcom/squareup/disputes/ChallengeEngine;->isFormComplete()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_3

    .line 143
    iget-object v0, p0, Lcom/squareup/disputes/ChallengeEngine;->fieldsList:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 232
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    check-cast v3, Ljava/util/Collection;

    .line 233
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Lcom/squareup/disputes/Field;

    .line 143
    iget-object v6, p0, Lcom/squareup/disputes/ChallengeEngine;->fieldsMap:Ljava/util/Map;

    invoke-virtual {v5, v6}, Lcom/squareup/disputes/Field;->canShow(Ljava/util/Map;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v5}, Lcom/squareup/disputes/Field;->hasAnswer()Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v5, 0x1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_0

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 234
    :cond_2
    check-cast v3, Ljava/util/List;

    check-cast v3, Ljava/lang/Iterable;

    .line 143
    invoke-static {v3, v2}, Lkotlin/collections/CollectionsKt;->drop(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v0

    goto :goto_4

    .line 145
    :cond_3
    iget-object v0, p0, Lcom/squareup/disputes/ChallengeEngine;->fieldsList:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 235
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    check-cast v3, Ljava/util/Collection;

    .line 236
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_4
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Lcom/squareup/disputes/Field;

    .line 145
    iget-object v6, p0, Lcom/squareup/disputes/ChallengeEngine;->fieldsMap:Ljava/util/Map;

    invoke-virtual {v5, v6}, Lcom/squareup/disputes/Field;->canShow(Ljava/util/Map;)Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-virtual {v5}, Lcom/squareup/disputes/Field;->hasAnswer()Z

    move-result v5

    if-eqz v5, :cond_5

    const/4 v5, 0x1

    goto :goto_3

    :cond_5
    const/4 v5, 0x0

    :goto_3
    if-eqz v5, :cond_4

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 237
    :cond_6
    check-cast v3, Ljava/util/List;

    check-cast v3, Ljava/lang/Iterable;

    .line 145
    invoke-static {v3, v2}, Lkotlin/collections/CollectionsKt;->drop(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 146
    invoke-virtual {p0}, Lcom/squareup/disputes/ChallengeEngine;->getFirstIncompleteField()Lcom/squareup/disputes/Field;

    move-result-object v1

    .line 145
    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    :goto_4
    return-object v0
.end method

.method public final hasNextField(Ljava/lang/String;)Z
    .locals 5

    const-string v0, "currentFieldName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    invoke-virtual {p0}, Lcom/squareup/disputes/ChallengeEngine;->getVisibleFields()Ljava/util/List;

    move-result-object v0

    .line 205
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 206
    check-cast v4, Lcom/squareup/disputes/Field;

    .line 88
    invoke-virtual {v4}, Lcom/squareup/disputes/Field;->getFieldName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, -0x1

    :goto_1
    if-ltz v3, :cond_3

    .line 92
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->getLastIndex(Ljava/util/List;)I

    move-result p1

    if-ge v3, p1, :cond_2

    const/4 v2, 0x1

    :cond_2
    return v2

    .line 90
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "current field "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " should not be shown"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final hasPreviousField(Ljava/lang/String;)Z
    .locals 4

    const-string v0, "currentFieldName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    invoke-virtual {p0}, Lcom/squareup/disputes/ChallengeEngine;->getVisibleFields()Ljava/util/List;

    move-result-object v0

    .line 219
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 220
    check-cast v3, Lcom/squareup/disputes/Field;

    .line 116
    invoke-virtual {v3}, Lcom/squareup/disputes/Field;->getFieldName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, -0x1

    :goto_1
    if-ltz v2, :cond_3

    if-lez v2, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1

    .line 118
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "current field "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " should not be shown"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final isFormComplete()Z
    .locals 5

    .line 154
    iget-object v0, p0, Lcom/squareup/disputes/ChallengeEngine;->fieldsList:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 238
    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    .line 239
    :cond_0
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/disputes/Field;

    .line 154
    iget-object v4, p0, Lcom/squareup/disputes/ChallengeEngine;->fieldsMap:Ljava/util/Map;

    invoke-virtual {v1, v4}, Lcom/squareup/disputes/Field;->canShow(Ljava/util/Map;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v1}, Lcom/squareup/disputes/Field;->hasAnswer()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    const/4 v3, 0x0

    :cond_3
    :goto_1
    return v3
.end method

.method public final setUpFields()V
    .locals 11

    .line 60
    iget-object v0, p0, Lcom/squareup/disputes/ChallengeEngine;->fieldsList:Ljava/util/List;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_a

    check-cast v0, Lcom/squareup/disputes/Field$SelectField;

    const-string/jumbo v1, "yes"

    .line 61
    invoke-virtual {v0, v1}, Lcom/squareup/disputes/Field$SelectField;->setAnswer(Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/disputes/ChallengeEngine;->fieldsList:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 178
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 179
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/disputes/Field;

    .line 63
    iget-object v4, p0, Lcom/squareup/disputes/ChallengeEngine;->fieldsMap:Ljava/util/Map;

    invoke-virtual {v3, v4}, Lcom/squareup/disputes/Field;->canShow(Ljava/util/Map;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 180
    :cond_1
    check-cast v1, Ljava/util/List;

    .line 65
    iget-boolean v0, p0, Lcom/squareup/disputes/ChallengeEngine;->alreadyComplete:Z

    const/4 v2, 0x1

    if-eqz v0, :cond_4

    .line 66
    check-cast v1, Ljava/lang/Iterable;

    .line 181
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 182
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lcom/squareup/disputes/Field;

    .line 66
    invoke-virtual {v4}, Lcom/squareup/disputes/Field;->hasAnswer()Z

    move-result v4

    xor-int/2addr v4, v2

    if-eqz v4, :cond_2

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 183
    :cond_3
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 184
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/disputes/Field;

    .line 66
    invoke-virtual {v1}, Lcom/squareup/disputes/Field;->skip()V

    goto :goto_2

    .line 69
    :cond_4
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->getLastIndex(Ljava/util/List;)I

    move-result v0

    .line 70
    move-object v3, v1

    check-cast v3, Ljava/lang/Iterable;

    .line 186
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    check-cast v4, Ljava/util/Collection;

    .line 197
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    add-int/lit8 v8, v6, 0x1

    if-gez v6, :cond_5

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    .line 198
    :cond_5
    move-object v9, v7

    check-cast v9, Lcom/squareup/disputes/Field;

    .line 71
    instance-of v10, v9, Lcom/squareup/disputes/Field$FileField;

    if-eqz v10, :cond_6

    if-ge v6, v0, :cond_6

    .line 73
    invoke-virtual {v9}, Lcom/squareup/disputes/Field;->hasAnswer()Z

    move-result v6

    if-nez v6, :cond_6

    .line 74
    invoke-interface {v1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/disputes/Field;

    invoke-virtual {v6}, Lcom/squareup/disputes/Field;->hasAnswer()Z

    move-result v6

    if-eqz v6, :cond_6

    const/4 v6, 0x1

    goto :goto_4

    :cond_6
    const/4 v6, 0x0

    :goto_4
    if-eqz v6, :cond_7

    invoke-interface {v4, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_7
    move v6, v8

    goto :goto_3

    .line 201
    :cond_8
    check-cast v4, Ljava/util/List;

    check-cast v4, Ljava/lang/Iterable;

    .line 202
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/disputes/Field;

    .line 75
    invoke-virtual {v1}, Lcom/squareup/disputes/Field;->skip()V

    goto :goto_5

    :cond_9
    return-void

    .line 60
    :cond_a
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type com.squareup.disputes.Field.SelectField"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
