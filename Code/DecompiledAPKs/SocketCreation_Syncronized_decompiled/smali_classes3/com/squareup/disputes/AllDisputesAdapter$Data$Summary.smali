.class public final Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;
.super Lcom/squareup/disputes/AllDisputesAdapter$Data;
.source "AllDisputesAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/disputes/AllDisputesAdapter$Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Summary"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0005H\u00c6\u0003J\'\u0010\u0010\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u00d6\u0003J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u000b\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;",
        "Lcom/squareup/disputes/AllDisputesAdapter$Data;",
        "count",
        "",
        "disputed",
        "",
        "protection",
        "(JLjava/lang/CharSequence;Ljava/lang/CharSequence;)V",
        "getCount",
        "()J",
        "getDisputed",
        "()Ljava/lang/CharSequence;",
        "getProtection",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final count:J

.field private final disputed:Ljava/lang/CharSequence;

.field private final protection:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(JLjava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 2

    const-string v0, "disputed"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "protection"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 234
    sget-object v0, Lcom/squareup/disputes/AllDisputesAdapter$ViewType;->TYPE_SUMMARY:Lcom/squareup/disputes/AllDisputesAdapter$ViewType;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/squareup/disputes/AllDisputesAdapter$Data;-><init>(Lcom/squareup/disputes/AllDisputesAdapter$ViewType;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-wide p1, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;->count:J

    iput-object p3, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;->disputed:Ljava/lang/CharSequence;

    iput-object p4, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;->protection:Ljava/lang/CharSequence;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;JLjava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/Object;)Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-wide p1, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;->count:J

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p3, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;->disputed:Ljava/lang/CharSequence;

    :cond_1
    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_2

    iget-object p4, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;->protection:Ljava/lang/CharSequence;

    :cond_2
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;->copy(JLjava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;->count:J

    return-wide v0
.end method

.method public final component2()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;->disputed:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component3()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;->protection:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final copy(JLjava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;
    .locals 1

    const-string v0, "disputed"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "protection"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;-><init>(JLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;

    iget-wide v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;->count:J

    iget-wide v2, p1, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;->count:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;->disputed:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;->disputed:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;->protection:Ljava/lang/CharSequence;

    iget-object p1, p1, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;->protection:Ljava/lang/CharSequence;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCount()J
    .locals 2

    .line 231
    iget-wide v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;->count:J

    return-wide v0
.end method

.method public final getDisputed()Ljava/lang/CharSequence;
    .locals 1

    .line 232
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;->disputed:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getProtection()Ljava/lang/CharSequence;
    .locals 1

    .line 233
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;->protection:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-wide v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;->count:J

    invoke-static {v0, v1}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;->disputed:Ljava/lang/CharSequence;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;->protection:Ljava/lang/CharSequence;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Summary(count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;->count:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", disputed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;->disputed:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", protection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;->protection:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
