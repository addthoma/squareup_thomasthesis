.class public final Lcom/squareup/disputes/RealHandlesDisputes;
.super Ljava/lang/Object;
.source "RealHandlesDisputes.kt"

# interfaces
.implements Lcom/squareup/disputes/api/HandlesDisputes;
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/LoggedInScope;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealHandlesDisputes.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealHandlesDisputes.kt\ncom/squareup/disputes/RealHandlesDisputes\n+ 2 RxKotlin.kt\ncom/squareup/util/rx2/Observables\n*L\n1#1,91:1\n119#2,4:92\n57#2,4:96\n*E\n*S KotlinDebug\n*F\n+ 1 RealHandlesDisputes.kt\ncom/squareup/disputes/RealHandlesDisputes\n*L\n50#1,4:92\n62#1,4:96\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0001\u0018\u00002\u00020\u00012\u00020\u0002BQ\u0008\u0001\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u000e\u0008\u0001\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c\u0012\u000e\u0008\u0001\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c\u0012\u0008\u0008\u0001\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0002\u0010\u0011J\u000e\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0017H\u0016J\u0008\u0010\u0018\u001a\u00020\u0013H\u0002J\u0008\u0010\u0019\u001a\u00020\u0013H\u0002J\u0008\u0010\u001a\u001a\u00020\u001bH\u0016J\u0008\u0010\u001c\u001a\u00020\u001bH\u0016J\u0010\u0010\u001d\u001a\u00020\u001b2\u0006\u0010\u001e\u001a\u00020\u001fH\u0016J\u0008\u0010 \u001a\u00020\u001bH\u0016J\u000e\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0017H\u0016J\u000c\u0010\"\u001a\u00020\u0013*\u00020\rH\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\u00020\u00138VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\u0014R\u0014\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/disputes/RealHandlesDisputes;",
        "Lcom/squareup/disputes/api/HandlesDisputes;",
        "Lmortar/Scoped;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "employeeManagement",
        "Lcom/squareup/permissions/EmployeeManagement;",
        "passcodeEmployeeManagement",
        "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
        "clock",
        "Lcom/squareup/util/Clock;",
        "lastSeenDisputesReport",
        "Lcom/f2prateek/rx/preferences2/Preference;",
        "",
        "lastSeenDisputesPopup",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/util/Clock;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lio/reactivex/Scheduler;)V",
        "isVisible",
        "",
        "()Z",
        "millisInADay",
        "disputeNotification",
        "Lio/reactivex/Observable;",
        "hasDisputeNotification",
        "hasNotSeenDisputesReportInLastDay",
        "markPopupClosed",
        "",
        "markReportSeen",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "shouldShowPopup",
        "moreThanADayAgo",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final clock:Lcom/squareup/util/Clock;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final lastSeenDisputesPopup:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final lastSeenDisputesReport:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final millisInADay:J

.field private final passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/util/Clock;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lio/reactivex/Scheduler;)V
    .locals 1
    .param p5    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/disputes/LastSeenDisputesReport;
        .end annotation
    .end param
    .param p6    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/disputes/LastSeenDisputesPopup;
        .end annotation
    .end param
    .param p7    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            "Lcom/squareup/util/Clock;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;",
            "Lio/reactivex/Scheduler;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "settings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "employeeManagement"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "passcodeEmployeeManagement"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lastSeenDisputesReport"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lastSeenDisputesPopup"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/disputes/RealHandlesDisputes;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p2, p0, Lcom/squareup/disputes/RealHandlesDisputes;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    iput-object p3, p0, Lcom/squareup/disputes/RealHandlesDisputes;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iput-object p4, p0, Lcom/squareup/disputes/RealHandlesDisputes;->clock:Lcom/squareup/util/Clock;

    iput-object p5, p0, Lcom/squareup/disputes/RealHandlesDisputes;->lastSeenDisputesReport:Lcom/f2prateek/rx/preferences2/Preference;

    iput-object p6, p0, Lcom/squareup/disputes/RealHandlesDisputes;->lastSeenDisputesPopup:Lcom/f2prateek/rx/preferences2/Preference;

    iput-object p7, p0, Lcom/squareup/disputes/RealHandlesDisputes;->mainScheduler:Lio/reactivex/Scheduler;

    .line 37
    sget-object p1, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 p2, 0x1

    invoke-virtual {p1, p2, p3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide p1

    iput-wide p1, p0, Lcom/squareup/disputes/RealHandlesDisputes;->millisInADay:J

    return-void
.end method

.method public static final synthetic access$hasDisputeNotification(Lcom/squareup/disputes/RealHandlesDisputes;)Z
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/squareup/disputes/RealHandlesDisputes;->hasDisputeNotification()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$moreThanADayAgo(Lcom/squareup/disputes/RealHandlesDisputes;J)Z
    .locals 0

    .line 26
    invoke-direct {p0, p1, p2}, Lcom/squareup/disputes/RealHandlesDisputes;->moreThanADayAgo(J)Z

    move-result p0

    return p0
.end method

.method private final hasDisputeNotification()Z
    .locals 2

    .line 46
    invoke-virtual {p0}, Lcom/squareup/disputes/RealHandlesDisputes;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/squareup/disputes/RealHandlesDisputes;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getDisputesSettings()Lcom/squareup/settings/server/DisputesSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/DisputesSettings;->hasActiveDisputes()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/squareup/disputes/RealHandlesDisputes;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_DISPUTES:Lcom/squareup/permissions/Permission;

    invoke-interface {v0, v1}, Lcom/squareup/permissions/EmployeeManagement;->hasPermission(Lcom/squareup/permissions/Permission;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    invoke-direct {p0}, Lcom/squareup/disputes/RealHandlesDisputes;->hasNotSeenDisputesReportInLastDay()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final hasNotSeenDisputesReportInLastDay()Z
    .locals 2

    .line 77
    iget-object v0, p0, Lcom/squareup/disputes/RealHandlesDisputes;->lastSeenDisputesReport:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "lastSeenDisputesReport.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/squareup/disputes/RealHandlesDisputes;->moreThanADayAgo(J)Z

    move-result v0

    return v0
.end method

.method private final moreThanADayAgo(J)Z
    .locals 3

    .line 79
    iget-object v0, p0, Lcom/squareup/disputes/RealHandlesDisputes;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p1

    iget-wide p1, p0, Lcom/squareup/disputes/RealHandlesDisputes;->millisInADay:J

    cmp-long v2, v0, p1

    if-lez v2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public disputeNotification()Lio/reactivex/Observable;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 49
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 51
    iget-object v0, p0, Lcom/squareup/disputes/RealHandlesDisputes;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->settingsAvailableRx2()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "settings.settingsAvailableRx2()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    iget-object v1, p0, Lcom/squareup/disputes/RealHandlesDisputes;->lastSeenDisputesReport:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v1}, Lcom/f2prateek/rx/preferences2/Preference;->asObservable()Lio/reactivex/Observable;

    move-result-object v1

    const-string v2, "lastSeenDisputesReport.asObservable()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    iget-object v2, p0, Lcom/squareup/disputes/RealHandlesDisputes;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v2}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->onCurrentEmployeeChanged()Lio/reactivex/Observable;

    move-result-object v2

    const-string v3, "passcodeEmployeeManageme\u2026nCurrentEmployeeChanged()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v3, 0x0

    .line 55
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v3}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v3

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v5, 0x1

    invoke-static {v5, v6, v4}, Lio/reactivex/Observable;->interval(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/Observable;

    move-result-object v4

    check-cast v4, Lio/reactivex/ObservableSource;

    invoke-virtual {v3, v4}, Lio/reactivex/Observable;->concatWith(Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v3

    const-string v4, "just(0L).concatWith(interval(1, MINUTES))"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    check-cast v0, Lio/reactivex/ObservableSource;

    check-cast v1, Lio/reactivex/ObservableSource;

    check-cast v2, Lio/reactivex/ObservableSource;

    check-cast v3, Lio/reactivex/ObservableSource;

    .line 94
    new-instance v4, Lcom/squareup/disputes/RealHandlesDisputes$disputeNotification$$inlined$combineLatest$1;

    invoke-direct {v4, p0}, Lcom/squareup/disputes/RealHandlesDisputes$disputeNotification$$inlined$combineLatest$1;-><init>(Lcom/squareup/disputes/RealHandlesDisputes;)V

    check-cast v4, Lio/reactivex/functions/Function4;

    .line 92
    invoke-static {v0, v1, v2, v3, v4}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function4;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026ion(t1, t2, t3, t4) }\n  )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    iget-object v1, p0, Lcom/squareup/disputes/RealHandlesDisputes;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observables\n        .com\u2026.observeOn(mainScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public isVisible()Z
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/disputes/RealHandlesDisputes;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getDisputesSettings()Lcom/squareup/settings/server/DisputesSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/DisputesSettings;->canSeeDisputes()Z

    move-result v0

    return v0
.end method

.method public markPopupClosed()V
    .locals 3

    .line 74
    iget-object v0, p0, Lcom/squareup/disputes/RealHandlesDisputes;->lastSeenDisputesPopup:Lcom/f2prateek/rx/preferences2/Preference;

    iget-object v1, p0, Lcom/squareup/disputes/RealHandlesDisputes;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v1}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public markReportSeen()V
    .locals 3

    .line 70
    iget-object v0, p0, Lcom/squareup/disputes/RealHandlesDisputes;->lastSeenDisputesReport:Lcom/f2prateek/rx/preferences2/Preference;

    iget-object v1, p0, Lcom/squareup/disputes/RealHandlesDisputes;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v1}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/disputes/RealHandlesDisputes;->lastSeenDisputesReport:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->delete()V

    .line 84
    iget-object v0, p0, Lcom/squareup/disputes/RealHandlesDisputes;->lastSeenDisputesPopup:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->delete()V

    return-void
.end method

.method public shouldShowPopup()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 61
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 63
    invoke-virtual {p0}, Lcom/squareup/disputes/RealHandlesDisputes;->disputeNotification()Lio/reactivex/Observable;

    move-result-object v0

    .line 64
    iget-object v1, p0, Lcom/squareup/disputes/RealHandlesDisputes;->lastSeenDisputesPopup:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v1}, Lcom/f2prateek/rx/preferences2/Preference;->asObservable()Lio/reactivex/Observable;

    move-result-object v1

    const-string v2, "lastSeenDisputesPopup.asObservable()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    check-cast v0, Lio/reactivex/ObservableSource;

    check-cast v1, Lio/reactivex/ObservableSource;

    .line 98
    new-instance v2, Lcom/squareup/disputes/RealHandlesDisputes$shouldShowPopup$$inlined$combineLatest$1;

    invoke-direct {v2, p0}, Lcom/squareup/disputes/RealHandlesDisputes$shouldShowPopup$$inlined$combineLatest$1;-><init>(Lcom/squareup/disputes/RealHandlesDisputes;)V

    check-cast v2, Lio/reactivex/functions/BiFunction;

    .line 96
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026ineFunction(t1, t2) }\n  )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    iget-object v1, p0, Lcom/squareup/disputes/RealHandlesDisputes;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observables\n        .com\u2026.observeOn(mainScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
