.class Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "PageLabelEditScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/PageLabelEditScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/orderentry/PageLabelEditView;",
        ">;"
    }
.end annotation


# instance fields
.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final flow:Lflow/Flow;

.field private final res:Lcom/squareup/util/Res;

.field private screen:Lcom/squareup/orderentry/PageLabelEditScreen;


# direct methods
.method constructor <init>(Lflow/Flow;Ljavax/inject/Provider;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Flow;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 51
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;->flow:Lflow/Flow;

    .line 53
    iput-object p2, p0, Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;->cogsProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p3, p0, Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method finish()V
    .locals 4

    .line 90
    invoke-virtual {p0}, Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/PageLabelEditView;

    invoke-virtual {v0}, Lcom/squareup/orderentry/PageLabelEditView;->hideKeyboard()V

    .line 91
    iget-object v0, p0, Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/orderentry/PageLabelEditScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public synthetic lambda$save$0$PageLabelEditScreen$Presenter(Ljava/lang/String;Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Void;
    .locals 2

    .line 81
    const-class v0, Lcom/squareup/shared/catalog/models/CatalogPage;

    iget-object v1, p0, Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;->screen:Lcom/squareup/orderentry/PageLabelEditScreen;

    iget-object v1, v1, Lcom/squareup/orderentry/PageLabelEditScreen;->pageId:Ljava/lang/String;

    invoke-interface {p2, v0, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->findById(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogPage;

    .line 82
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogPage;->object()Lcom/squareup/wire/Message;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/items/Page;

    invoke-virtual {v1}, Lcom/squareup/api/items/Page;->newBuilder()Lcom/squareup/api/items/Page$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/squareup/api/items/Page$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/Page$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/api/items/Page$Builder;->build()Lcom/squareup/api/items/Page;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogPage;->copy(Lcom/squareup/api/items/Page;)Lcom/squareup/shared/catalog/models/CatalogPage;

    move-result-object p1

    .line 83
    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-interface {p2, p1, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->write(Ljava/util/Collection;Ljava/util/Collection;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 58
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/PageLabelEditScreen;

    iput-object p1, p0, Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;->screen:Lcom/squareup/orderentry/PageLabelEditScreen;

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 62
    invoke-virtual {p0}, Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/PageLabelEditView;

    invoke-virtual {v0}, Lcom/squareup/orderentry/PageLabelEditView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    .line 63
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/orderentry/R$string;->page_label_edit_title:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    const/4 v1, 0x1

    .line 64
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    const/4 v1, 0x0

    .line 65
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setSecondaryButtonEnabled(Z)V

    .line 66
    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$UoX7iOCcKMZztTbDyiq2wPjIJ5g;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$UoX7iOCcKMZztTbDyiq2wPjIJ5g;-><init>(Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 67
    iget-object v1, p0, Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/strings/R$string;->done:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 68
    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$jvnOBskjeSW5Xye9bTjuob20lMA;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$jvnOBskjeSW5Xye9bTjuob20lMA;-><init>(Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showPrimaryButton(Ljava/lang/Runnable;)V

    .line 70
    invoke-virtual {p0}, Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/PageLabelEditView;

    iget-object v1, p0, Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;->screen:Lcom/squareup/orderentry/PageLabelEditScreen;

    iget-object v1, v1, Lcom/squareup/orderentry/PageLabelEditScreen;->pageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/PageLabelEditView;->setPageName(Ljava/lang/String;)V

    if-nez p1, :cond_0

    .line 73
    invoke-virtual {p0}, Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/PageLabelEditView;

    invoke-virtual {p1}, Lcom/squareup/orderentry/PageLabelEditView;->requestInitialFocus()V

    :cond_0
    return-void
.end method

.method save()V
    .locals 3

    .line 78
    invoke-virtual {p0}, Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/PageLabelEditView;

    invoke-virtual {v0}, Lcom/squareup/orderentry/PageLabelEditView;->getPageName()Ljava/lang/String;

    move-result-object v0

    .line 79
    iget-object v1, p0, Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cogs/Cogs;

    .line 80
    new-instance v2, Lcom/squareup/orderentry/-$$Lambda$PageLabelEditScreen$Presenter$W0aYwp_MFs123JJAGYjlMyrnc6A;

    invoke-direct {v2, p0, v0}, Lcom/squareup/orderentry/-$$Lambda$PageLabelEditScreen$Presenter$W0aYwp_MFs123JJAGYjlMyrnc6A;-><init>(Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;Ljava/lang/String;)V

    .line 85
    invoke-static {v1}, Lcom/squareup/shared/catalog/CatalogTasks;->syncWhenFinished(Lcom/squareup/shared/catalog/Catalog;)Lcom/squareup/shared/catalog/CatalogCallback;

    move-result-object v0

    .line 80
    invoke-interface {v1, v2, v0}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    .line 86
    invoke-virtual {p0}, Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;->finish()V

    return-void
.end method
