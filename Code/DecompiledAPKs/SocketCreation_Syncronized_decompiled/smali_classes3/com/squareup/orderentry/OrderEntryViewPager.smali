.class public Lcom/squareup/orderentry/OrderEntryViewPager;
.super Landroidx/viewpager/widget/ViewPager;
.source "OrderEntryViewPager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;
    }
.end annotation


# static fields
.field private static final MAX_TAB_COUNT:I = 0x9


# instance fields
.field private isBeingDragged:Z

.field private keypadPanel:Landroid/view/View;

.field private libraryPanel:Landroid/view/View;

.field private pagingEnabled:Z

.field presenter:Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 50
    invoke-direct {p0, p1, p2}, Landroidx/viewpager/widget/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p2, 0x1

    .line 45
    iput-boolean p2, p0, Lcom/squareup/orderentry/OrderEntryViewPager;->pagingEnabled:Z

    .line 51
    const-class p2, Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;

    invoke-interface {p1, p0}, Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;->inject(Lcom/squareup/orderentry/OrderEntryViewPager;)V

    .line 53
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/marin/R$dimen;->marin_gap_medium:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 52
    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/OrderEntryViewPager;->setPageMargin(I)V

    const/16 p1, 0x9

    .line 54
    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/OrderEntryViewPager;->setOffscreenPageLimit(I)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/orderentry/OrderEntryViewPager;)Z
    .locals 0

    .line 34
    iget-boolean p0, p0, Lcom/squareup/orderentry/OrderEntryViewPager;->pagingEnabled:Z

    return p0
.end method

.method static synthetic access$002(Lcom/squareup/orderentry/OrderEntryViewPager;Z)Z
    .locals 0

    .line 34
    iput-boolean p1, p0, Lcom/squareup/orderentry/OrderEntryViewPager;->pagingEnabled:Z

    return p1
.end method

.method static synthetic access$100(Lcom/squareup/orderentry/OrderEntryViewPager;)Landroid/view/View;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/orderentry/OrderEntryViewPager;->keypadPanel:Landroid/view/View;

    return-object p0
.end method

.method static synthetic access$102(Lcom/squareup/orderentry/OrderEntryViewPager;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryViewPager;->keypadPanel:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$200(Lcom/squareup/orderentry/OrderEntryViewPager;)Landroid/view/View;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/orderentry/OrderEntryViewPager;->libraryPanel:Landroid/view/View;

    return-object p0
.end method

.method static synthetic access$202(Lcom/squareup/orderentry/OrderEntryViewPager;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryViewPager;->libraryPanel:Landroid/view/View;

    return-object p1
.end method


# virtual methods
.method installAdapter()V
    .locals 1

    .line 58
    new-instance v0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;-><init>(Lcom/squareup/orderentry/OrderEntryViewPager;)V

    .line 59
    invoke-virtual {p0, v0}, Lcom/squareup/orderentry/OrderEntryViewPager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    .line 60
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPager;->presenter:Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->currentPage()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/orderentry/OrderEntryViewPager;->setCurrentItem(I)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPager;->presenter:Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->dropView(Lcom/squareup/orderentry/OrderEntryViewPager;)V

    .line 77
    invoke-super {p0}, Landroidx/viewpager/widget/ViewPager;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 71
    invoke-super {p0}, Landroidx/viewpager/widget/ViewPager;->onFinishInflate()V

    .line 72
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPager;->presenter:Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 88
    iget-boolean v0, p0, Lcom/squareup/orderentry/OrderEntryViewPager;->pagingEnabled:Z

    if-eqz v0, :cond_0

    .line 89
    invoke-super {p0, p1}, Landroidx/viewpager/widget/ViewPager;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/orderentry/OrderEntryViewPager;->isBeingDragged:Z

    .line 90
    iget-boolean p1, p0, Lcom/squareup/orderentry/OrderEntryViewPager;->isBeingDragged:Z

    return p1

    :cond_0
    const/4 p1, 0x0

    .line 92
    iput-boolean p1, p0, Lcom/squareup/orderentry/OrderEntryViewPager;->isBeingDragged:Z

    return p1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 81
    iget-boolean v0, p0, Lcom/squareup/orderentry/OrderEntryViewPager;->pagingEnabled:Z

    if-eqz v0, :cond_0

    .line 82
    invoke-super {p0, p1}, Landroidx/viewpager/widget/ViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public setOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V
    .locals 1

    .line 104
    new-instance v0, Lcom/squareup/ui/main/FixScrollOffsetPageChangeListener;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/main/FixScrollOffsetPageChangeListener;-><init>(Landroidx/viewpager/widget/ViewPager;Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    invoke-super {p0, v0}, Landroidx/viewpager/widget/ViewPager;->setOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    return-void
.end method

.method public setPageMargin(I)V
    .locals 1

    .line 99
    rem-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_0

    add-int/lit8 p1, p1, -0x1

    .line 100
    :cond_0
    invoke-super {p0, p1}, Landroidx/viewpager/widget/ViewPager;->setPageMargin(I)V

    return-void
.end method

.method setPagingEnabled(Z)V
    .locals 1

    .line 113
    iget-boolean v0, p0, Lcom/squareup/orderentry/OrderEntryViewPager;->pagingEnabled:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/orderentry/OrderEntryViewPager;->isBeingDragged:Z

    if-eqz v0, :cond_0

    .line 114
    invoke-static {}, Lcom/squareup/dialog/GlassDialogUtil;->createCancelEvent()Landroid/view/MotionEvent;

    move-result-object v0

    .line 115
    invoke-super {p0, v0}, Landroidx/viewpager/widget/ViewPager;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 116
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 118
    :cond_0
    iput-boolean p1, p0, Lcom/squareup/orderentry/OrderEntryViewPager;->pagingEnabled:Z

    return-void
.end method

.method updateAdapter()V
    .locals 1

    .line 64
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryViewPager;->getAdapter()Landroidx/viewpager/widget/PagerAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 65
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryViewPager;->getAdapter()Landroidx/viewpager/widget/PagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/viewpager/widget/PagerAdapter;->notifyDataSetChanged()V

    .line 66
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPager;->presenter:Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->currentPage()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/orderentry/OrderEntryViewPager;->setCurrentItem(I)V

    :cond_0
    return-void
.end method
