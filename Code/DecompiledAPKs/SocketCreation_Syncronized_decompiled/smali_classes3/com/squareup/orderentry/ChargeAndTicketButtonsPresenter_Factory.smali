.class public final Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;
.super Ljava/lang/Object;
.source "ChargeAndTicketButtonsPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final badBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final cashDrawerTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashdrawer/CashDrawerTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final chargeAndTicketButtonFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final dippedCardTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final glassControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/widgets/glass/GlassConfirmController;",
            ">;"
        }
    .end annotation
.end field

.field private final hasSeparatedPrintoutsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltySellerCartBannerFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryScreenStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;"
        }
    .end annotation
.end field

.field private final orderPrintingDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final readerHudManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
            ">;"
        }
    .end annotation
.end field

.field private final sellerScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/seller/SellerScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final smartPaymentFlowStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final swipeHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/payment/SwipeHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionMetricsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final voidCompSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final x2ScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/payment/SwipeHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/widgets/glass/GlassConfirmController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashdrawer/CashDrawerTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/seller/SellerScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 111
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->chargeAndTicketButtonFormatterProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 112
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 113
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->badBusProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 114
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 115
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 116
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->swipeHandlerProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 117
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 118
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->tenderStarterProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 119
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->smartPaymentFlowStarterProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 120
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->glassControllerProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 121
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 122
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 123
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->cashDrawerTrackerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 124
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->dippedCardTrackerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 125
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->readerHudManagerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 126
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->orderPrintingDispatcherProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 127
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 128
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 129
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->transactionMetricsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 130
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->tenderFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 131
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->voidCompSettingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 132
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->hasSeparatedPrintoutsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 133
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->sellerScopeRunnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p24

    .line 134
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->loyaltySellerCartBannerFormatterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p25

    .line 135
    iput-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;
    .locals 27
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/payment/SwipeHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/widgets/glass/GlassConfirmController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashdrawer/CashDrawerTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/seller/SellerScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    .line 165
    new-instance v26, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;

    move-object/from16 v0, v26

    invoke-direct/range {v0 .. v25}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v26
.end method

.method public static newInstance(Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;Lcom/squareup/analytics/Analytics;Lcom/squareup/badbus/BadBus;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/payment/SwipeHandler;Lflow/Flow;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/widgets/glass/GlassConfirmController;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/cardreader/dipper/ReaderHudManager;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/util/Device;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;Lcom/squareup/ui/seller/SellerScopeRunner;Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;Lcom/squareup/settings/server/Features;)Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;
    .locals 27

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    .line 181
    new-instance v26, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;

    move-object/from16 v0, v26

    invoke-direct/range {v0 .. v25}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;-><init>(Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;Lcom/squareup/analytics/Analytics;Lcom/squareup/badbus/BadBus;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/payment/SwipeHandler;Lflow/Flow;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/widgets/glass/GlassConfirmController;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/cardreader/dipper/ReaderHudManager;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/util/Device;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;Lcom/squareup/ui/seller/SellerScopeRunner;Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;Lcom/squareup/settings/server/Features;)V

    return-object v26
.end method


# virtual methods
.method public get()Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;
    .locals 27

    move-object/from16 v0, p0

    .line 140
    iget-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->chargeAndTicketButtonFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;

    iget-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->badBusProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/badbus/BadBus;

    iget-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/orderentry/OrderEntryScreenState;

    iget-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->swipeHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/ui/payment/SwipeHandler;

    iget-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->tenderStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/ui/tender/TenderStarter;

    iget-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->smartPaymentFlowStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    iget-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->glassControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/widgets/glass/GlassConfirmController;

    iget-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->cashDrawerTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/cashdrawer/CashDrawerTracker;

    iget-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->dippedCardTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/cardreader/DippedCardTracker;

    iget-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->readerHudManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/cardreader/dipper/ReaderHudManager;

    iget-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->orderPrintingDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/print/OrderPrintingDispatcher;

    iget-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/util/Device;

    iget-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->transactionMetricsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/ui/main/TransactionMetrics;

    iget-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->tenderFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/payment/tender/TenderFactory;

    iget-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->voidCompSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    iget-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->hasSeparatedPrintoutsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;

    iget-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->sellerScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v24, v1

    check-cast v24, Lcom/squareup/ui/seller/SellerScopeRunner;

    iget-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->loyaltySellerCartBannerFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v25, v1

    check-cast v25, Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;

    iget-object v1, v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v26, v1

    check-cast v26, Lcom/squareup/settings/server/Features;

    invoke-static/range {v2 .. v26}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->newInstance(Lcom/squareup/orderentry/ChargeAndTicketButtonFormatter;Lcom/squareup/analytics/Analytics;Lcom/squareup/badbus/BadBus;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/payment/SwipeHandler;Lflow/Flow;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/widgets/glass/GlassConfirmController;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/cardreader/dipper/ReaderHudManager;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/util/Device;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;Lcom/squareup/ui/seller/SellerScopeRunner;Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;Lcom/squareup/settings/server/Features;)Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter_Factory;->get()Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;

    move-result-object v0

    return-object v0
.end method
