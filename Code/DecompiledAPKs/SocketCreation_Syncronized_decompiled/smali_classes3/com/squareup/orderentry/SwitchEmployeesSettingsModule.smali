.class public final Lcom/squareup/orderentry/SwitchEmployeesSettingsModule;
.super Ljava/lang/Object;
.source "SwitchEmployeesSettingsModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orderentry/SwitchEmployeesSettingsModule$SwitchEmployeesSeen;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c7\u0002\u0018\u00002\u00020\u0001:\u0001\u000fB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u001a\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0001\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0007J\u0018\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r2\u0008\u0008\u0001\u0010\u0008\u001a\u00020\tH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/orderentry/SwitchEmployeesSettingsModule;",
        "",
        "()V",
        "SWITCH_EMPLOYEES_SEEN",
        "",
        "SWITCH_EMPLOYEES_TOOL_TIP_STATUS",
        "provideSwitchEmployeesSetting",
        "Lcom/squareup/settings/StringSetLocalSetting;",
        "preferences",
        "Landroid/content/SharedPreferences;",
        "gson",
        "Lcom/google/gson/Gson;",
        "provideSwitchEmployeesTooltipStatus",
        "Lcom/squareup/settings/LocalSetting;",
        "Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;",
        "SwitchEmployeesSeen",
        "order-entry_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/orderentry/SwitchEmployeesSettingsModule;

.field private static final SWITCH_EMPLOYEES_SEEN:Ljava/lang/String; = "switch-employees-seen"

.field private static final SWITCH_EMPLOYEES_TOOL_TIP_STATUS:Ljava/lang/String; = "switch-employees-tooltip-status"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    new-instance v0, Lcom/squareup/orderentry/SwitchEmployeesSettingsModule;

    invoke-direct {v0}, Lcom/squareup/orderentry/SwitchEmployeesSettingsModule;-><init>()V

    sput-object v0, Lcom/squareup/orderentry/SwitchEmployeesSettingsModule;->INSTANCE:Lcom/squareup/orderentry/SwitchEmployeesSettingsModule;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final provideSwitchEmployeesSetting(Landroid/content/SharedPreferences;Lcom/google/gson/Gson;)Lcom/squareup/settings/StringSetLocalSetting;
    .locals 2
    .param p0    # Landroid/content/SharedPreferences;
        .annotation runtime Lcom/squareup/settings/LoggedInSettings;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/dagger/LoggedInScope;
    .end annotation

    .annotation runtime Lcom/squareup/orderentry/SwitchEmployeesSettingsModule$SwitchEmployeesSeen;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "preferences"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "gson"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    new-instance v0, Lcom/squareup/settings/StringSetLocalSetting;

    const-string v1, "switch-employees-seen"

    invoke-direct {v0, p0, v1, p1}, Lcom/squareup/settings/StringSetLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method public static final provideSwitchEmployeesTooltipStatus(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 3
    .param p0    # Landroid/content/SharedPreferences;
        .annotation runtime Lcom/squareup/settings/LoggedInSettings;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "preferences"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    new-instance v0, Lcom/squareup/settings/EnumLocalSetting;

    .line 40
    const-class v1, Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;

    const-string v2, "switch-employees-tooltip-status"

    .line 38
    invoke-direct {v0, p0, v2, v1}, Lcom/squareup/settings/EnumLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Class;)V

    check-cast v0, Lcom/squareup/settings/LocalSetting;

    return-object v0
.end method
