.class public Lcom/squareup/orderentry/FlyByCoordinator;
.super Ljava/lang/Object;
.source "FlyByCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orderentry/FlyByCoordinator$PulseViewRunnable;,
        Lcom/squareup/orderentry/FlyByCoordinator$RemoveFlyByViewAndShadow;,
        Lcom/squareup/orderentry/FlyByCoordinator$Offsets;
    }
.end annotation


# static fields
.field private static final GROW_DURATION_MS:J = 0x15eL

.field private static final MAX_FLYBY_TIME_MS:I = 0x190

.field private static final MIN_FLYBY_TIME_MS:I = 0xfa

.field private static final PULSE_DURATION_MS:J = 0xafL

.field private static final SPEED_PER_DP:F = 1.5f


# instance fields
.field private final container:Landroid/widget/FrameLayout;

.field private final displayFrame:Landroid/graphics/Rect;

.field private final metrics:Landroid/util/DisplayMetrics;

.field private final pulseRunnables:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final removeRunnables:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final viewsInFlight:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/widget/FrameLayout;)V
    .locals 2

    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iput-object p1, p0, Lcom/squareup/orderentry/FlyByCoordinator;->container:Landroid/widget/FrameLayout;

    .line 109
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/squareup/orderentry/FlyByCoordinator;->viewsInFlight:Ljava/util/List;

    .line 110
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/squareup/orderentry/FlyByCoordinator;->pulseRunnables:Ljava/util/List;

    .line 111
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/squareup/orderentry/FlyByCoordinator;->removeRunnables:Ljava/util/List;

    .line 112
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/squareup/orderentry/FlyByCoordinator;->displayFrame:Landroid/graphics/Rect;

    .line 113
    iget-object v0, p0, Lcom/squareup/orderentry/FlyByCoordinator;->container:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/squareup/orderentry/FlyByCoordinator;->displayFrame:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 114
    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/orderentry/FlyByCoordinator;->metrics:Landroid/util/DisplayMetrics;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/orderentry/FlyByCoordinator;)Ljava/util/List;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/squareup/orderentry/FlyByCoordinator;->viewsInFlight:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/orderentry/FlyByCoordinator;)Landroid/widget/FrameLayout;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/squareup/orderentry/FlyByCoordinator;->container:Landroid/widget/FrameLayout;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/orderentry/FlyByCoordinator;)Ljava/util/List;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/squareup/orderentry/FlyByCoordinator;->removeRunnables:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$300(ILandroid/widget/TextView;)Landroid/view/animation/AnimationSet;
    .locals 0

    .line 43
    invoke-static {p0, p1}, Lcom/squareup/orderentry/FlyByCoordinator;->buildPulseAnimation(ILandroid/widget/TextView;)Landroid/view/animation/AnimationSet;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/orderentry/FlyByCoordinator;)Ljava/util/List;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/squareup/orderentry/FlyByCoordinator;->pulseRunnables:Ljava/util/List;

    return-object p0
.end method

.method private buildFlyByAnimator(Landroid/widget/TextView;ZZILcom/squareup/orderentry/FlyByCoordinator$Offsets;Landroid/widget/ImageView;Landroid/widget/ImageView;Lcom/squareup/orderentry/FlyByListener;)V
    .locals 18

    move-object/from16 v9, p0

    move-object/from16 v0, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    .line 221
    new-instance v10, Landroid/animation/AnimatorSet;

    invoke-direct {v10}, Landroid/animation/AnimatorSet;-><init>()V

    .line 223
    iget-object v1, v0, Lcom/squareup/orderentry/FlyByCoordinator$Offsets;->displacement:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-double v1, v1

    const-wide/high16 v3, 0x4000000000000000L    # 2.0

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    iget-object v5, v0, Lcom/squareup/orderentry/FlyByCoordinator$Offsets;->displacement:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-double v5, v5

    invoke-static {v5, v6, v3, v4}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v3

    add-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v1

    .line 224
    iget-object v3, v9, Lcom/squareup/orderentry/FlyByCoordinator;->metrics:Landroid/util/DisplayMetrics;

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    float-to-double v3, v3

    div-double/2addr v1, v3

    const-wide/high16 v3, 0x3ff8000000000000L    # 1.5

    mul-double v1, v1, v3

    const-wide v3, 0x406f400000000000L    # 250.0

    .line 226
    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->max(DD)D

    move-result-wide v1

    const-wide/high16 v3, 0x4079000000000000L    # 400.0

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->min(DD)D

    move-result-wide v1

    double-to-int v5, v1

    const/4 v1, 0x5

    new-array v2, v1, [F

    .line 228
    fill-array-data v2, :array_0

    const-string v3, "scaleX"

    invoke-static {v3, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    new-array v1, v1, [F

    .line 229
    fill-array-data v1, :array_1

    const-string v3, "scaleY"

    invoke-static {v3, v1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    const/4 v3, 0x2

    new-array v4, v3, [F

    const/4 v6, 0x0

    const/4 v11, 0x0

    aput v6, v4, v11

    .line 230
    iget-object v12, v0, Lcom/squareup/orderentry/FlyByCoordinator$Offsets;->displacement:Landroid/graphics/Point;

    iget v12, v12, Landroid/graphics/Point;->x:I

    int-to-float v12, v12

    const/4 v13, 0x1

    aput v12, v4, v13

    const-string/jumbo v12, "translationX"

    invoke-static {v12, v4}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v4

    const/4 v14, 0x3

    new-array v15, v14, [Landroid/animation/PropertyValuesHolder;

    aput-object v2, v15, v11

    aput-object v1, v15, v13

    aput-object v4, v15, v3

    .line 231
    invoke-static {v7, v15}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    int-to-long v14, v5

    .line 232
    invoke-virtual {v1, v14, v15}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 233
    sget-object v4, Lcom/squareup/ui/Interpolators;->QuadEaseInEaseOut:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v4}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 234
    invoke-virtual {v10, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    new-array v1, v3, [F

    aput v6, v1, v11

    .line 236
    iget-object v4, v0, Lcom/squareup/orderentry/FlyByCoordinator$Offsets;->displacement:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    aput v4, v1, v13

    const-string/jumbo v4, "translationY"

    invoke-static {v4, v1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    new-array v2, v13, [Landroid/animation/PropertyValuesHolder;

    aput-object v1, v2, v11

    .line 237
    invoke-static {v7, v2}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 238
    invoke-virtual {v1, v14, v15}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 239
    sget-object v2, Lcom/squareup/ui/Interpolators;->QuadEaseOut:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 240
    invoke-virtual {v10, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    new-array v1, v3, [F

    aput v6, v1, v11

    .line 242
    iget-object v2, v0, Lcom/squareup/orderentry/FlyByCoordinator$Offsets;->displacement:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    aput v2, v1, v13

    invoke-static {v12, v1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    new-array v2, v3, [F

    aput v6, v2, v11

    .line 243
    iget-object v0, v0, Lcom/squareup/orderentry/FlyByCoordinator$Offsets;->displacement:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    aput v0, v2, v13

    invoke-static {v4, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    const/4 v4, 0x4

    new-array v2, v4, [F

    .line 244
    fill-array-data v2, :array_2

    const-string v12, "alpha"

    invoke-static {v12, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    const-string v6, "rotation"

    if-eqz p2, :cond_0

    const/4 v3, 0x3

    new-array v13, v3, [F

    .line 247
    fill-array-data v13, :array_3

    invoke-static {v6, v13}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v13

    new-array v3, v4, [Landroid/animation/PropertyValuesHolder;

    aput-object v1, v3, v11

    const/16 v16, 0x1

    aput-object v0, v3, v16

    const/16 v17, 0x2

    aput-object v2, v3, v17

    const/4 v0, 0x3

    aput-object v13, v3, v0

    .line 249
    invoke-static {v8, v3}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    const/4 v3, 0x3

    goto :goto_0

    :cond_0
    const/4 v3, 0x3

    const/16 v16, 0x1

    const/16 v17, 0x2

    new-array v13, v3, [Landroid/animation/PropertyValuesHolder;

    aput-object v1, v13, v11

    aput-object v0, v13, v16

    aput-object v2, v13, v17

    .line 251
    invoke-static {v8, v13}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 253
    :goto_0
    invoke-virtual {v1, v14, v15}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 254
    sget-object v0, Lcom/squareup/ui/Interpolators;->CubicEaseInEaseOut:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v0}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 255
    invoke-virtual {v10, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    if-eqz p2, :cond_1

    new-array v0, v3, [F

    .line 258
    fill-array-data v0, :array_4

    invoke-static {v6, v0}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    new-array v1, v4, [Landroid/animation/Keyframe;

    const v3, 0x3e4ccccd    # 0.2f

    const/4 v4, 0x0

    .line 261
    invoke-static {v4, v3}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v3

    aput-object v3, v1, v11

    const/high16 v3, 0x3f000000    # 0.5f

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-static {v3, v6}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v3

    const/4 v13, 0x1

    aput-object v3, v1, v13

    const v3, 0x3f7851ec    # 0.97f

    .line 262
    invoke-static {v3, v6}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v3

    const/4 v2, 0x2

    aput-object v3, v1, v2

    invoke-static {v6, v4}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v3

    const/4 v4, 0x3

    aput-object v3, v1, v4

    .line 261
    invoke-static {v12, v1}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    new-array v2, v2, [Landroid/animation/PropertyValuesHolder;

    aput-object v0, v2, v11

    aput-object v1, v2, v13

    .line 263
    invoke-static {v7, v2}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 264
    sget-object v1, Lcom/squareup/ui/Interpolators;->CubicEaseInEaseOut:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 265
    invoke-virtual {v0, v14, v15}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 266
    invoke-virtual {v10, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 269
    :cond_1
    iget-object v0, v9, Lcom/squareup/orderentry/FlyByCoordinator;->container:Landroid/widget/FrameLayout;

    invoke-static {v10, v0}, Lcom/squareup/util/Views;->endOnDetach(Landroid/animation/Animator;Landroid/view/View;)V

    .line 270
    new-instance v11, Lcom/squareup/orderentry/FlyByCoordinator$1;

    move-object v0, v11

    move-object/from16 v1, p0

    move/from16 v2, p3

    move-object/from16 v3, p1

    move/from16 v4, p4

    move-object/from16 v6, p8

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/orderentry/FlyByCoordinator$1;-><init>(Lcom/squareup/orderentry/FlyByCoordinator;ZLandroid/widget/TextView;IILcom/squareup/orderentry/FlyByListener;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    invoke-virtual {v10, v11}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 296
    invoke-virtual {v10}, Landroid/animation/AnimatorSet;->start()V

    return-void

    :array_0
    .array-data 4
        0x3f666666    # 0.9f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f666666    # 0.9f
        0x3e4ccccd    # 0.2f
    .end array-data

    :array_1
    .array-data 4
        0x3f666666    # 0.9f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f666666    # 0.9f
        0x3e4ccccd    # 0.2f
    .end array-data

    :array_2
    .array-data 4
        0x3dcccccd    # 0.1f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
    .end array-data

    :array_3
    .array-data 4
        -0x3ee00000    # -10.0f
        0x40800000    # 4.0f
        0x41200000    # 10.0f
    .end array-data

    :array_4
    .array-data 4
        -0x3ee00000    # -10.0f
        0x40800000    # 4.0f
        0x41200000    # 10.0f
    .end array-data
.end method

.method private buildOffsetsFromPositions(Landroid/graphics/Point;Landroid/graphics/Point;Landroid/view/View;[I[I[I)Lcom/squareup/orderentry/FlyByCoordinator$Offsets;
    .locals 5

    const/4 v0, 0x0

    .line 120
    aget v1, p4, v0

    iget-object v2, p0, Lcom/squareup/orderentry/FlyByCoordinator;->displayFrame:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    const/4 v2, 0x1

    .line 121
    aget p4, p4, v2

    iget-object v3, p0, Lcom/squareup/orderentry/FlyByCoordinator;->displayFrame:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr p4, v3

    .line 123
    aget v3, p5, v0

    iget-object v4, p0, Lcom/squareup/orderentry/FlyByCoordinator;->displayFrame:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    .line 124
    aget p5, p5, v2

    iget-object v4, p0, Lcom/squareup/orderentry/FlyByCoordinator;->displayFrame:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr p5, v4

    .line 126
    iget v4, p2, Landroid/graphics/Point;->x:I

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    .line 127
    iget p2, p2, Landroid/graphics/Point;->y:I

    div-int/lit8 p2, p2, 0x2

    add-int/2addr p5, p2

    .line 129
    iget p2, p1, Landroid/graphics/Point;->x:I

    div-int/lit8 p2, p2, 0x2

    sub-int/2addr v3, p2

    .line 130
    iget p2, p1, Landroid/graphics/Point;->y:I

    div-int/lit8 p2, p2, 0x2

    sub-int/2addr p5, p2

    .line 132
    aget p2, p6, v0

    iget-object v0, p0, Lcom/squareup/orderentry/FlyByCoordinator;->displayFrame:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr p2, v0

    .line 133
    aget p6, p6, v2

    iget-object v0, p0, Lcom/squareup/orderentry/FlyByCoordinator;->displayFrame:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr p6, v0

    .line 135
    invoke-virtual {p3}, Landroid/view/View;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr p2, v0

    .line 136
    invoke-virtual {p3}, Landroid/view/View;->getHeight()I

    move-result p3

    div-int/lit8 p3, p3, 0x2

    add-int/2addr p6, p3

    .line 138
    iget p3, p1, Landroid/graphics/Point;->x:I

    div-int/lit8 p3, p3, 0x2

    sub-int/2addr p2, p3

    .line 139
    iget p1, p1, Landroid/graphics/Point;->y:I

    div-int/lit8 p1, p1, 0x2

    sub-int/2addr p6, p1

    .line 141
    new-instance p1, Lcom/squareup/orderentry/FlyByCoordinator$Offsets;

    new-instance p3, Landroid/graphics/Point;

    sub-int v0, v3, v1

    sub-int p4, p5, p4

    invoke-direct {p3, v0, p4}, Landroid/graphics/Point;-><init>(II)V

    new-instance p4, Landroid/graphics/Point;

    sub-int/2addr p2, v3

    sub-int/2addr p6, p5

    invoke-direct {p4, p2, p6}, Landroid/graphics/Point;-><init>(II)V

    const/4 p2, 0x0

    invoke-direct {p1, p3, p4, p2}, Lcom/squareup/orderentry/FlyByCoordinator$Offsets;-><init>(Landroid/graphics/Point;Landroid/graphics/Point;Lcom/squareup/orderentry/FlyByCoordinator$1;)V

    return-object p1
.end method

.method private static buildPulseAnimation(ILandroid/widget/TextView;)Landroid/view/animation/AnimationSet;
    .locals 14

    .line 320
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f400000    # 0.75f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v2, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 321
    new-instance v12, Landroid/view/animation/ScaleAnimation;

    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v5, 0x3f400000    # 0.75f

    const/high16 v6, 0x3f800000    # 1.0f

    const/high16 v7, 0x3f400000    # 0.75f

    const/4 v8, 0x1

    const/high16 v9, 0x3f000000    # 0.5f

    const/4 v10, 0x1

    const/high16 v11, 0x3f000000    # 0.5f

    move-object v3, v12

    invoke-direct/range {v3 .. v11}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 323
    new-instance v3, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v12, v3}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 325
    new-instance v3, Landroid/view/animation/AnimationSet;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 326
    invoke-virtual {v3, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 327
    invoke-virtual {v3, v12}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    const-wide/16 v5, 0xaf

    .line 328
    invoke-virtual {v3, v5, v6}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 330
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 331
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 332
    new-instance v1, Landroid/view/animation/ScaleAnimation;

    const v6, 0x3ee66666    # 0.45f

    const/high16 v7, 0x3f800000    # 1.0f

    const v8, 0x3ee66666    # 0.45f

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v12, 0x1

    const/high16 v13, 0x3f000000    # 0.5f

    move-object v5, v1

    invoke-direct/range {v5 .. v13}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 334
    new-instance v2, Landroid/view/animation/OvershootInterpolator;

    const/high16 v5, 0x42040000    # 33.0f

    invoke-direct {v2, v5}, Landroid/view/animation/OvershootInterpolator;-><init>(F)V

    invoke-virtual {v1, v2}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 336
    new-instance v2, Landroid/view/animation/AnimationSet;

    invoke-direct {v2, v4}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 337
    invoke-virtual {v2, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 338
    invoke-virtual {v2, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    const-wide/16 v0, 0x15e

    .line 339
    invoke-virtual {v2, v0, v1}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 340
    new-instance v0, Landroid/view/animation/OvershootInterpolator;

    invoke-direct {v0, v5}, Landroid/view/animation/OvershootInterpolator;-><init>(F)V

    invoke-virtual {v2, v0}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 342
    new-instance v0, Lcom/squareup/orderentry/FlyByCoordinator$2;

    invoke-direct {v0, v2, p1, p0}, Lcom/squareup/orderentry/FlyByCoordinator$2;-><init>(Landroid/view/animation/AnimationSet;Landroid/widget/TextView;I)V

    invoke-virtual {v3, v0}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    return-object v3
.end method

.method private createFlyByView(ILandroid/widget/TextView;Landroid/graphics/drawable/Drawable;ZZILcom/squareup/orderentry/FlyByCoordinator$Offsets;Lcom/squareup/orderentry/FlyByListener;)V
    .locals 10

    move-object v9, p0

    move-object/from16 v5, p7

    .line 199
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, 0x0

    move v2, p1

    invoke-direct {v0, p1, p1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 200
    iget-object v1, v5, Lcom/squareup/orderentry/FlyByCoordinator$Offsets;->source:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 201
    iget-object v1, v5, Lcom/squareup/orderentry/FlyByCoordinator$Offsets;->source:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 203
    new-instance v6, Landroid/widget/ImageView;

    invoke-virtual {p2}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v6, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    const/4 v2, 0x2

    .line 204
    invoke-virtual {v6, v2, v1}, Landroid/widget/ImageView;->setLayerType(ILandroid/graphics/Paint;)V

    move-object v3, p3

    .line 205
    invoke-virtual {v6, p3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 207
    new-instance v7, Landroid/widget/ImageView;

    invoke-virtual {p2}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v7, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 208
    invoke-virtual {v7, v2, v1}, Landroid/widget/ImageView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 209
    sget v1, Lcom/squareup/orderentry/R$drawable;->flyby_shadow:I

    invoke-virtual {v7, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 210
    iget-object v1, v9, Lcom/squareup/orderentry/FlyByCoordinator;->viewsInFlight:Ljava/util/List;

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    iget-object v1, v9, Lcom/squareup/orderentry/FlyByCoordinator;->viewsInFlight:Ljava/util/List;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    iget-object v1, v9, Lcom/squareup/orderentry/FlyByCoordinator;->container:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v7, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 213
    iget-object v1, v9, Lcom/squareup/orderentry/FlyByCoordinator;->container:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v6, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    move-object v0, p0

    move-object v1, p2

    move v2, p4

    move v3, p5

    move/from16 v4, p6

    move-object/from16 v8, p8

    .line 214
    invoke-direct/range {v0 .. v8}, Lcom/squareup/orderentry/FlyByCoordinator;->buildFlyByAnimator(Landroid/widget/TextView;ZZILcom/squareup/orderentry/FlyByCoordinator$Offsets;Landroid/widget/ImageView;Landroid/widget/ImageView;Lcom/squareup/orderentry/FlyByListener;)V

    return-void
.end method


# virtual methods
.method addFlyByDiscount(ILandroid/view/View;Landroid/widget/TextView;ZZILcom/squareup/orderentry/FlyByListener;)V
    .locals 10

    move-object v9, p0

    .line 168
    iget-object v0, v9, Lcom/squareup/orderentry/FlyByCoordinator;->container:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forDiscount(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    move v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    invoke-virtual/range {v0 .. v8}, Lcom/squareup/orderentry/FlyByCoordinator;->addFlyByView(ILandroid/view/View;Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;ZZILcom/squareup/orderentry/FlyByListener;)V

    return-void
.end method

.method addFlyByView(ILandroid/view/View;Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;ZZILcom/squareup/orderentry/FlyByListener;)V
    .locals 11

    move-object v7, p3

    if-nez v7, :cond_0

    .line 177
    invoke-interface/range {p8 .. p8}, Lcom/squareup/orderentry/FlyByListener;->onStart()V

    return-void

    :cond_0
    const/4 v0, 0x2

    new-array v4, v0, [I

    new-array v6, v0, [I

    move-object v9, p0

    .line 183
    iget-object v1, v9, Lcom/squareup/orderentry/FlyByCoordinator;->container:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->getLocationOnScreen([I)V

    .line 184
    invoke-virtual {p3, v6}, Landroid/widget/TextView;->getLocationOnScreen([I)V

    new-array v5, v0, [I

    move-object v0, p2

    .line 187
    invoke-virtual {p2, v5}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 189
    new-instance v1, Landroid/graphics/Point;

    move v8, p1

    invoke-direct {v1, p1, p1}, Landroid/graphics/Point;-><init>(II)V

    new-instance v2, Landroid/graphics/Point;

    .line 190
    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-direct {v2, v3, v0}, Landroid/graphics/Point;-><init>(II)V

    move-object v0, p0

    move-object v3, p3

    .line 189
    invoke-direct/range {v0 .. v6}, Lcom/squareup/orderentry/FlyByCoordinator;->buildOffsetsFromPositions(Landroid/graphics/Point;Landroid/graphics/Point;Landroid/view/View;[I[I[I)Lcom/squareup/orderentry/FlyByCoordinator$Offsets;

    move-result-object v10

    move v1, p1

    move-object v2, p3

    move-object v3, p4

    move/from16 v4, p5

    move/from16 v5, p6

    move/from16 v6, p7

    move-object v7, v10

    move-object/from16 v8, p8

    .line 192
    invoke-direct/range {v0 .. v8}, Lcom/squareup/orderentry/FlyByCoordinator;->createFlyByView(ILandroid/widget/TextView;Landroid/graphics/drawable/Drawable;ZZILcom/squareup/orderentry/FlyByCoordinator$Offsets;Lcom/squareup/orderentry/FlyByListener;)V

    return-void
.end method

.method addFlyByViewAfterPanel(I[ILandroid/graphics/drawable/Drawable;ILandroid/widget/TextView;Lcom/squareup/orderentry/FlyByListener;)V
    .locals 11

    move v1, p1

    move-object/from16 v9, p5

    if-nez v9, :cond_0

    .line 149
    invoke-interface/range {p6 .. p6}, Lcom/squareup/orderentry/FlyByListener;->onStart()V

    return-void

    :cond_0
    const/4 v0, 0x2

    new-array v6, v0, [I

    new-array v8, v0, [I

    move-object v10, p0

    .line 155
    iget-object v0, v10, Lcom/squareup/orderentry/FlyByCoordinator;->container:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v6}, Landroid/widget/FrameLayout;->getLocationOnScreen([I)V

    .line 156
    invoke-virtual {v9, v8}, Landroid/widget/TextView;->getLocationOnScreen([I)V

    .line 158
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3, p1, p1}, Landroid/graphics/Point;-><init>(II)V

    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4, p1, p1}, Landroid/graphics/Point;-><init>(II)V

    move-object v2, p0

    move-object/from16 v5, p5

    move-object v7, p2

    invoke-direct/range {v2 .. v8}, Lcom/squareup/orderentry/FlyByCoordinator;->buildOffsetsFromPositions(Landroid/graphics/Point;Landroid/graphics/Point;Landroid/view/View;[I[I[I)Lcom/squareup/orderentry/FlyByCoordinator$Offsets;

    move-result-object v7

    const/4 v4, 0x1

    const/4 v5, 0x1

    move-object v0, p0

    move v1, p1

    move-object/from16 v2, p5

    move-object v3, p3

    move v6, p4

    move-object/from16 v8, p6

    .line 161
    invoke-direct/range {v0 .. v8}, Lcom/squareup/orderentry/FlyByCoordinator;->createFlyByView(ILandroid/widget/TextView;Landroid/graphics/drawable/Drawable;ZZILcom/squareup/orderentry/FlyByCoordinator$Offsets;Lcom/squareup/orderentry/FlyByListener;)V

    return-void
.end method

.method destroyFlyByViews()V
    .locals 3

    .line 300
    iget-object v0, p0, Lcom/squareup/orderentry/FlyByCoordinator;->removeRunnables:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 301
    iget-object v2, p0, Lcom/squareup/orderentry/FlyByCoordinator;->container:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v1}, Landroid/widget/FrameLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 304
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/FlyByCoordinator;->pulseRunnables:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 305
    iget-object v2, p0, Lcom/squareup/orderentry/FlyByCoordinator;->container:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v1}, Landroid/widget/FrameLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    goto :goto_1

    .line 308
    :cond_1
    iget-object v0, p0, Lcom/squareup/orderentry/FlyByCoordinator;->viewsInFlight:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 309
    invoke-virtual {v1}, Landroid/view/View;->clearAnimation()V

    .line 310
    iget-object v2, p0, Lcom/squareup/orderentry/FlyByCoordinator;->container:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    goto :goto_2

    .line 313
    :cond_2
    iget-object v0, p0, Lcom/squareup/orderentry/FlyByCoordinator;->removeRunnables:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 314
    iget-object v0, p0, Lcom/squareup/orderentry/FlyByCoordinator;->pulseRunnables:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 315
    iget-object v0, p0, Lcom/squareup/orderentry/FlyByCoordinator;->viewsInFlight:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method
