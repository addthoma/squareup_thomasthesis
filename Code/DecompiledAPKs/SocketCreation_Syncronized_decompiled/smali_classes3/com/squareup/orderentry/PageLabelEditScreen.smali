.class public final Lcom/squareup/orderentry/PageLabelEditScreen;
.super Lcom/squareup/ui/seller/InSellerScope;
.source "PageLabelEditScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/orderentry/PageLabelEditScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orderentry/PageLabelEditScreen$Component;,
        Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/orderentry/PageLabelEditScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final pageId:Ljava/lang/String;

.field final pageName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 106
    sget-object v0, Lcom/squareup/orderentry/-$$Lambda$PageLabelEditScreen$hD-oEhSGiEJ5cs4_cCnGS74kC7A;->INSTANCE:Lcom/squareup/orderentry/-$$Lambda$PageLabelEditScreen$hD-oEhSGiEJ5cs4_cCnGS74kC7A;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/orderentry/PageLabelEditScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/squareup/ui/seller/InSellerScope;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/orderentry/PageLabelEditScreen;->pageId:Ljava/lang/String;

    .line 39
    iput-object p2, p0, Lcom/squareup/orderentry/PageLabelEditScreen;->pageName:Ljava/lang/String;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/orderentry/PageLabelEditScreen;
    .locals 2

    .line 107
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 108
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    .line 109
    new-instance v1, Lcom/squareup/orderentry/PageLabelEditScreen;

    invoke-direct {v1, v0, p0}, Lcom/squareup/orderentry/PageLabelEditScreen;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 102
    iget-object p2, p0, Lcom/squareup/orderentry/PageLabelEditScreen;->pageId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 103
    iget-object p2, p0, Lcom/squareup/orderentry/PageLabelEditScreen;->pageName:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 113
    sget v0, Lcom/squareup/orderentry/R$layout;->page_label_edit_view:I

    return v0
.end method
