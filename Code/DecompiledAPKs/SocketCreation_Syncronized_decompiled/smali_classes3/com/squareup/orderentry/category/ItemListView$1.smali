.class Lcom/squareup/orderentry/category/ItemListView$1;
.super Lcom/squareup/debounce/DebouncedOnItemClickListener;
.source "ItemListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/category/ItemListView;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/category/ItemListView;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/category/ItemListView;)V
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/squareup/orderentry/category/ItemListView$1;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnItemClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .line 116
    iget-object p1, p0, Lcom/squareup/orderentry/category/ItemListView$1;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    iget-object p1, p1, Lcom/squareup/orderentry/category/ItemListView;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    sget-object p4, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_ITEM_LIST:Lcom/squareup/analytics/RegisterViewName;

    invoke-virtual {p1, p4}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->start(Lcom/squareup/analytics/RegisterViewName;)V

    .line 119
    iget-object p1, p0, Lcom/squareup/orderentry/category/ItemListView$1;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    invoke-static {p1}, Lcom/squareup/orderentry/category/ItemListView;->access$000(Lcom/squareup/orderentry/category/ItemListView;)Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;

    move-result-object p1

    invoke-virtual {p1, p3}, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->isEnabled(I)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 120
    :cond_0
    iget-object p1, p0, Lcom/squareup/orderentry/category/ItemListView$1;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    invoke-static {p1}, Lcom/squareup/orderentry/category/ItemListView;->access$000(Lcom/squareup/orderentry/category/ItemListView;)Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;

    move-result-object p1

    invoke-virtual {p1, p3}, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->getItemType(I)I

    move-result p1

    if-eqz p1, :cond_5

    const/4 p2, 0x1

    if-eq p1, p2, :cond_4

    const/4 p2, 0x2

    if-eq p1, p2, :cond_3

    const/4 p2, 0x3

    if-eq p1, p2, :cond_2

    const/4 p2, 0x4

    if-ne p1, p2, :cond_1

    .line 134
    iget-object p1, p0, Lcom/squareup/orderentry/category/ItemListView$1;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    iget-object p1, p1, Lcom/squareup/orderentry/category/ItemListView;->presenter:Lcom/squareup/orderentry/category/ItemListScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->onCreateRewardsClicked()V

    goto :goto_0

    .line 140
    :cond_1
    new-instance p2, Ljava/lang/IllegalStateException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Unknown row type:"

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 137
    :cond_2
    iget-object p1, p0, Lcom/squareup/orderentry/category/ItemListView$1;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    iget-object p1, p1, Lcom/squareup/orderentry/category/ItemListView;->presenter:Lcom/squareup/orderentry/category/ItemListScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->onCreateAllGiftCardsClicked()V

    goto :goto_0

    .line 131
    :cond_3
    iget-object p1, p0, Lcom/squareup/orderentry/category/ItemListView$1;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    iget-object p1, p1, Lcom/squareup/orderentry/category/ItemListView;->presenter:Lcom/squareup/orderentry/category/ItemListScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->onCreateAllDiscountsClicked()V

    goto :goto_0

    .line 128
    :cond_4
    iget-object p1, p0, Lcom/squareup/orderentry/category/ItemListView$1;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    iget-object p1, p1, Lcom/squareup/orderentry/category/ItemListView;->presenter:Lcom/squareup/orderentry/category/ItemListScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->onCreateAllItemsClicked()V

    goto :goto_0

    .line 123
    :cond_5
    check-cast p2, Lcom/squareup/librarylist/LibraryItemListNohoRow;

    .line 124
    iget-object p1, p0, Lcom/squareup/orderentry/category/ItemListView$1;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    invoke-static {p1}, Lcom/squareup/orderentry/category/ItemListView;->access$000(Lcom/squareup/orderentry/category/ItemListView;)Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;

    move-result-object p1

    invoke-virtual {p1, p3}, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    .line 125
    iget-object p3, p0, Lcom/squareup/orderentry/category/ItemListView$1;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    iget-object p3, p3, Lcom/squareup/orderentry/category/ItemListView;->presenter:Lcom/squareup/orderentry/category/ItemListScreen$Presenter;

    invoke-virtual {p2}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getItemThumbnail()Landroid/widget/ImageView;

    move-result-object p2

    invoke-virtual {p3, p2, p1}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->onEntryClicked(Landroid/view/View;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)V

    :goto_0
    return-void
.end method
