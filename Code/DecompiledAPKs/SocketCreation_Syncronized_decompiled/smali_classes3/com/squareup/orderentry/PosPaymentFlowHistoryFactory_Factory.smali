.class public final Lcom/squareup/orderentry/PosPaymentFlowHistoryFactory_Factory;
.super Ljava/lang/Object;
.source "PosPaymentFlowHistoryFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/orderentry/PosPaymentFlowHistoryFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final apiRequestControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiRequestController;",
            ">;"
        }
    .end annotation
.end field

.field private final homeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/Home;",
            ">;"
        }
    .end annotation
.end field

.field private final invoicesAppletRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiRequestController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/Home;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/orderentry/PosPaymentFlowHistoryFactory_Factory;->apiRequestControllerProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/orderentry/PosPaymentFlowHistoryFactory_Factory;->homeProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/orderentry/PosPaymentFlowHistoryFactory_Factory;->invoicesAppletRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/PosPaymentFlowHistoryFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiRequestController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/Home;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ">;)",
            "Lcom/squareup/orderentry/PosPaymentFlowHistoryFactory_Factory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/orderentry/PosPaymentFlowHistoryFactory_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/orderentry/PosPaymentFlowHistoryFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/api/ApiRequestController;Lcom/squareup/ui/main/Home;Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;)Lcom/squareup/orderentry/PosPaymentFlowHistoryFactory;
    .locals 1

    .line 46
    new-instance v0, Lcom/squareup/orderentry/PosPaymentFlowHistoryFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/orderentry/PosPaymentFlowHistoryFactory;-><init>(Lcom/squareup/api/ApiRequestController;Lcom/squareup/ui/main/Home;Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/orderentry/PosPaymentFlowHistoryFactory;
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/orderentry/PosPaymentFlowHistoryFactory_Factory;->apiRequestControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/ApiRequestController;

    iget-object v1, p0, Lcom/squareup/orderentry/PosPaymentFlowHistoryFactory_Factory;->homeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/main/Home;

    iget-object v2, p0, Lcom/squareup/orderentry/PosPaymentFlowHistoryFactory_Factory;->invoicesAppletRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    invoke-static {v0, v1, v2}, Lcom/squareup/orderentry/PosPaymentFlowHistoryFactory_Factory;->newInstance(Lcom/squareup/api/ApiRequestController;Lcom/squareup/ui/main/Home;Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;)Lcom/squareup/orderentry/PosPaymentFlowHistoryFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/orderentry/PosPaymentFlowHistoryFactory_Factory;->get()Lcom/squareup/orderentry/PosPaymentFlowHistoryFactory;

    move-result-object v0

    return-object v0
.end method
