.class final Lcom/squareup/orderentry/FlyByCoordinator$2;
.super Lcom/squareup/ui/EmptyAnimationListener;
.source "FlyByCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/FlyByCoordinator;->buildPulseAnimation(ILandroid/widget/TextView;)Landroid/view/animation/AnimationSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$destinationView:Landroid/widget/TextView;

.field final synthetic val$growingSet:Landroid/view/animation/AnimationSet;

.field final synthetic val$newQuantity:I


# direct methods
.method constructor <init>(Landroid/view/animation/AnimationSet;Landroid/widget/TextView;I)V
    .locals 0

    .line 342
    iput-object p1, p0, Lcom/squareup/orderentry/FlyByCoordinator$2;->val$growingSet:Landroid/view/animation/AnimationSet;

    iput-object p2, p0, Lcom/squareup/orderentry/FlyByCoordinator$2;->val$destinationView:Landroid/widget/TextView;

    iput p3, p0, Lcom/squareup/orderentry/FlyByCoordinator$2;->val$newQuantity:I

    invoke-direct {p0}, Lcom/squareup/ui/EmptyAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 1

    .line 344
    iget-object p1, p0, Lcom/squareup/orderentry/FlyByCoordinator$2;->val$growingSet:Landroid/view/animation/AnimationSet;

    invoke-virtual {p1}, Landroid/view/animation/AnimationSet;->startNow()V

    .line 345
    iget-object p1, p0, Lcom/squareup/orderentry/FlyByCoordinator$2;->val$destinationView:Landroid/widget/TextView;

    iget v0, p0, Lcom/squareup/orderentry/FlyByCoordinator$2;->val$newQuantity:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
