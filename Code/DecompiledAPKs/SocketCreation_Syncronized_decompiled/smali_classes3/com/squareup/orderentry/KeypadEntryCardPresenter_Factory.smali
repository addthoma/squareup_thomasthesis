.class public final Lcom/squareup/orderentry/KeypadEntryCardPresenter_Factory;
.super Ljava/lang/Object;
.source "KeypadEntryCardPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/orderentry/KeypadEntryCardPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final currencyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final perUnitFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/KeypadEntryScreen$Runner;",
            ">;"
        }
    .end annotation
.end field

.field private final vibratorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/os/Vibrator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/os/Vibrator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/KeypadEntryScreen$Runner;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter_Factory;->currencyProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter_Factory;->perUnitFormatterProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter_Factory;->vibratorProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p5, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/KeypadEntryCardPresenter_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/os/Vibrator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/KeypadEntryScreen$Runner;",
            ">;)",
            "Lcom/squareup/orderentry/KeypadEntryCardPresenter_Factory;"
        }
    .end annotation

    .line 48
    new-instance v6, Lcom/squareup/orderentry/KeypadEntryCardPresenter_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/orderentry/KeypadEntryCardPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/quantity/PerUnitFormatter;Landroid/os/Vibrator;Lcom/squareup/orderentry/KeypadEntryScreen$Runner;)Lcom/squareup/orderentry/KeypadEntryCardPresenter;
    .locals 7

    .line 53
    new-instance v6, Lcom/squareup/orderentry/KeypadEntryCardPresenter;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/orderentry/KeypadEntryCardPresenter;-><init>(Lcom/squareup/util/Res;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/quantity/PerUnitFormatter;Landroid/os/Vibrator;Lcom/squareup/orderentry/KeypadEntryScreen$Runner;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/orderentry/KeypadEntryCardPresenter;
    .locals 5

    .line 42
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter_Factory;->currencyProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v2, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter_Factory;->perUnitFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v3, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter_Factory;->vibratorProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Vibrator;

    iget-object v4, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/orderentry/KeypadEntryScreen$Runner;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/orderentry/KeypadEntryCardPresenter_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/quantity/PerUnitFormatter;Landroid/os/Vibrator;Lcom/squareup/orderentry/KeypadEntryScreen$Runner;)Lcom/squareup/orderentry/KeypadEntryCardPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/orderentry/KeypadEntryCardPresenter_Factory;->get()Lcom/squareup/orderentry/KeypadEntryCardPresenter;

    move-result-object v0

    return-object v0
.end method
