.class public final enum Lcom/squareup/orderentry/FavoritePageView$ViewType;
.super Ljava/lang/Enum;
.source "FavoritePageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/FavoritePageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ViewType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/orderentry/FavoritePageView$ViewType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/orderentry/FavoritePageView$ViewType;

.field public static final enum DELETE:Lcom/squareup/orderentry/FavoritePageView$ViewType;

.field public static final enum TILE_EMPTY:Lcom/squareup/orderentry/FavoritePageView$ViewType;

.field public static final enum TILE_REGULAR:Lcom/squareup/orderentry/FavoritePageView$ViewType;

.field public static final enum TILE_STACKED:Lcom/squareup/orderentry/FavoritePageView$ViewType;

.field public static final enum TOOLTIP:Lcom/squareup/orderentry/FavoritePageView$ViewType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 66
    new-instance v0, Lcom/squareup/orderentry/FavoritePageView$ViewType;

    const/4 v1, 0x0

    const-string v2, "DELETE"

    invoke-direct {v0, v2, v1}, Lcom/squareup/orderentry/FavoritePageView$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/orderentry/FavoritePageView$ViewType;->DELETE:Lcom/squareup/orderentry/FavoritePageView$ViewType;

    new-instance v0, Lcom/squareup/orderentry/FavoritePageView$ViewType;

    const/4 v2, 0x1

    const-string v3, "TILE_EMPTY"

    invoke-direct {v0, v3, v2}, Lcom/squareup/orderentry/FavoritePageView$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/orderentry/FavoritePageView$ViewType;->TILE_EMPTY:Lcom/squareup/orderentry/FavoritePageView$ViewType;

    new-instance v0, Lcom/squareup/orderentry/FavoritePageView$ViewType;

    const/4 v3, 0x2

    const-string v4, "TILE_REGULAR"

    invoke-direct {v0, v4, v3}, Lcom/squareup/orderentry/FavoritePageView$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/orderentry/FavoritePageView$ViewType;->TILE_REGULAR:Lcom/squareup/orderentry/FavoritePageView$ViewType;

    new-instance v0, Lcom/squareup/orderentry/FavoritePageView$ViewType;

    const/4 v4, 0x3

    const-string v5, "TILE_STACKED"

    invoke-direct {v0, v5, v4}, Lcom/squareup/orderentry/FavoritePageView$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/orderentry/FavoritePageView$ViewType;->TILE_STACKED:Lcom/squareup/orderentry/FavoritePageView$ViewType;

    new-instance v0, Lcom/squareup/orderentry/FavoritePageView$ViewType;

    const/4 v5, 0x4

    const-string v6, "TOOLTIP"

    invoke-direct {v0, v6, v5}, Lcom/squareup/orderentry/FavoritePageView$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/orderentry/FavoritePageView$ViewType;->TOOLTIP:Lcom/squareup/orderentry/FavoritePageView$ViewType;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/orderentry/FavoritePageView$ViewType;

    .line 65
    sget-object v6, Lcom/squareup/orderentry/FavoritePageView$ViewType;->DELETE:Lcom/squareup/orderentry/FavoritePageView$ViewType;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/orderentry/FavoritePageView$ViewType;->TILE_EMPTY:Lcom/squareup/orderentry/FavoritePageView$ViewType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/orderentry/FavoritePageView$ViewType;->TILE_REGULAR:Lcom/squareup/orderentry/FavoritePageView$ViewType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/orderentry/FavoritePageView$ViewType;->TILE_STACKED:Lcom/squareup/orderentry/FavoritePageView$ViewType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/orderentry/FavoritePageView$ViewType;->TOOLTIP:Lcom/squareup/orderentry/FavoritePageView$ViewType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/orderentry/FavoritePageView$ViewType;->$VALUES:[Lcom/squareup/orderentry/FavoritePageView$ViewType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 65
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/orderentry/FavoritePageView$ViewType;
    .locals 1

    .line 65
    const-class v0, Lcom/squareup/orderentry/FavoritePageView$ViewType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/orderentry/FavoritePageView$ViewType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/orderentry/FavoritePageView$ViewType;
    .locals 1

    .line 65
    sget-object v0, Lcom/squareup/orderentry/FavoritePageView$ViewType;->$VALUES:[Lcom/squareup/orderentry/FavoritePageView$ViewType;

    invoke-virtual {v0}, [Lcom/squareup/orderentry/FavoritePageView$ViewType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/orderentry/FavoritePageView$ViewType;

    return-object v0
.end method
