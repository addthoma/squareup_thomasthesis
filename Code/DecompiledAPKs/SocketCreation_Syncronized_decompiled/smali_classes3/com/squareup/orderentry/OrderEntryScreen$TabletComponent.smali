.class public interface abstract Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;
.super Ljava/lang/Object;
.source "OrderEntryScreen.java"

# interfaces
.implements Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;
.implements Lcom/squareup/ui/cart/CartComponent;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/orderentry/OrderEntryScreen$TabletModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/OrderEntryScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TabletComponent"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/orderentry/CartDropDownView;)V
.end method

.method public abstract inject(Lcom/squareup/orderentry/LibraryPanelTablet;)V
.end method

.method public abstract inject(Lcom/squareup/orderentry/NavigationBarEditView;)V
.end method

.method public abstract inject(Lcom/squareup/orderentry/NavigationBarSaleView;)V
.end method

.method public abstract inject(Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;)V
.end method

.method public abstract inject(Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;)V
.end method

.method public abstract inject(Lcom/squareup/orderentry/PaymentPadTabletPortraitView;)V
.end method

.method public abstract inject(Lcom/squareup/ui/cart/header/CartHeaderTabletView;)V
.end method

.method public abstract inject(Lcom/squareup/ui/cart/menu/CartMenuArrowButton;)V
.end method

.method public abstract inject(Lcom/squareup/ui/permissions/EmployeeLockButtonWide;)V
.end method
