.class public final Lcom/squareup/orderentry/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final add_note_view:I = 0x7f0d004a

.field public static final barcode_not_found:I = 0x7f0d0088

.field public static final cart_container_view:I = 0x7f0d00c4

.field public static final cart_discounts_view:I = 0x7f0d00c5

.field public static final cart_menu_view:I = 0x7f0d00ca

.field public static final cart_recycler_view:I = 0x7f0d00cb

.field public static final cart_sale_list_row:I = 0x7f0d00cc

.field public static final cart_taxes_view:I = 0x7f0d00cd

.field public static final cart_ticket_line_row:I = 0x7f0d00ce

.field public static final cart_ticket_note_list_row:I = 0x7f0d00cf

.field public static final category_drop_down_row:I = 0x7f0d00d9

.field public static final charge_and_tickets_button_content:I = 0x7f0d00dd

.field public static final clock_skew_view:I = 0x7f0d00ea

.field public static final discount_entry_money_view:I = 0x7f0d01dd

.field public static final discount_entry_percent_view:I = 0x7f0d01de

.field public static final edit_split_ticket_view:I = 0x7f0d0225

.field public static final edit_ticket_view:I = 0x7f0d022d

.field public static final employee_lock_button:I = 0x7f0d024c

.field public static final favorite_item_list_view:I = 0x7f0d026b

.field public static final favorite_page:I = 0x7f0d026c

.field public static final favorite_page_tooltip:I = 0x7f0d026d

.field public static final favorite_tile_delete:I = 0x7f0d026f

.field public static final gift_card_activiation_view:I = 0x7f0d0276

.field public static final home_drawer_button:I = 0x7f0d028d

.field public static final home_drawer_button_wide:I = 0x7f0d028e

.field public static final item_fee_list_row:I = 0x7f0d0304

.field public static final keypad_entry_percent:I = 0x7f0d031a

.field public static final keypad_panel:I = 0x7f0d031c

.field public static final library_create_new_item_dialog_view:I = 0x7f0d0328

.field public static final library_panel:I = 0x7f0d032a

.field public static final master_detail_ticket_view:I = 0x7f0d034e

.field public static final master_group_list_view:I = 0x7f0d034f

.field public static final master_group_row:I = 0x7f0d0350

.field public static final merge_ticket_row:I = 0x7f0d0358

.field public static final merge_ticket_view:I = 0x7f0d0359

.field public static final move_ticket_view:I = 0x7f0d0360

.field public static final new_ticket_custom_ticket_button:I = 0x7f0d0380

.field public static final new_ticket_no_predefined_tickets:I = 0x7f0d0381

.field public static final new_ticket_select_group_header:I = 0x7f0d0382

.field public static final new_ticket_view:I = 0x7f0d0383

.field public static final order_entry_actionbar_edit_tab_glyph:I = 0x7f0d03da

.field public static final order_entry_actionbar_edit_tab_text:I = 0x7f0d03db

.field public static final order_entry_actionbar_sale_tab_glyph:I = 0x7f0d03dc

.field public static final order_entry_actionbar_sale_tab_text:I = 0x7f0d03dd

.field public static final order_entry_actionbar_tab_text_wide:I = 0x7f0d03de

.field public static final order_entry_view:I = 0x7f0d03df

.field public static final order_entry_view_content:I = 0x7f0d03e0

.field public static final order_entry_view_pager_v1:I = 0x7f0d03e1

.field public static final order_entry_view_pager_v2:I = 0x7f0d03e2

.field public static final order_entry_view_wide:I = 0x7f0d03e3

.field public static final page_label_edit_view:I = 0x7f0d041a

.field public static final payment_pad_landscape_cart_header_layout:I = 0x7f0d0436

.field public static final payment_pad_landscape_seller_cart_footer_banner:I = 0x7f0d0437

.field public static final payment_pad_landscape_view:I = 0x7f0d0438

.field public static final payment_pad_portrait_view:I = 0x7f0d0439

.field public static final predefined_tickets_group_or_template_row:I = 0x7f0d044d

.field public static final price_entry_view:I = 0x7f0d0451

.field public static final split_ticket_item_row:I = 0x7f0d04e9

.field public static final split_ticket_note_row:I = 0x7f0d04ea

.field public static final split_ticket_tax_total_row:I = 0x7f0d04eb

.field public static final split_ticket_view:I = 0x7f0d04ec

.field public static final start_drawer_modal:I = 0x7f0d0504

.field public static final ticket_detail_buttons:I = 0x7f0d0528

.field public static final ticket_detail_checkable_ticket_group:I = 0x7f0d0529

.field public static final ticket_detail_convert_to_custom_ticket_button:I = 0x7f0d052a

.field public static final ticket_detail_header:I = 0x7f0d052b

.field public static final ticket_detail_ticket_group_header:I = 0x7f0d052c

.field public static final ticket_detail_view:I = 0x7f0d052d

.field public static final ticket_employee_row:I = 0x7f0d052e

.field public static final ticket_list_button_row:I = 0x7f0d052f

.field public static final ticket_list_error_row:I = 0x7f0d0530

.field public static final ticket_list_no_tickets:I = 0x7f0d0531

.field public static final ticket_list_section_header_row:I = 0x7f0d0532

.field public static final ticket_list_simple_no_tickets:I = 0x7f0d0533

.field public static final ticket_list_sort_row:I = 0x7f0d0534

.field public static final ticket_list_text_row:I = 0x7f0d0535

.field public static final ticket_list_ticket_row:I = 0x7f0d0536

.field public static final ticket_list_view:I = 0x7f0d0537

.field public static final ticket_list_view_contents:I = 0x7f0d0538

.field public static final ticket_sort_group:I = 0x7f0d0539

.field public static final ticket_transfer_employees_view:I = 0x7f0d053b

.field public static final ticket_view:I = 0x7f0d053c


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 301
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
