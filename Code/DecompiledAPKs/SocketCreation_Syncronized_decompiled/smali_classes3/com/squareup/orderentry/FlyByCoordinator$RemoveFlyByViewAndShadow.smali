.class Lcom/squareup/orderentry/FlyByCoordinator$RemoveFlyByViewAndShadow;
.super Ljava/lang/Object;
.source "FlyByCoordinator.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/FlyByCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RemoveFlyByViewAndShadow"
.end annotation


# instance fields
.field private final imageView:Landroid/widget/ImageView;

.field private final shadowView:Landroid/widget/ImageView;

.field final synthetic this$0:Lcom/squareup/orderentry/FlyByCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/FlyByCoordinator;Landroid/widget/ImageView;Landroid/widget/ImageView;)V
    .locals 0

    .line 63
    iput-object p1, p0, Lcom/squareup/orderentry/FlyByCoordinator$RemoveFlyByViewAndShadow;->this$0:Lcom/squareup/orderentry/FlyByCoordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p2, p0, Lcom/squareup/orderentry/FlyByCoordinator$RemoveFlyByViewAndShadow;->imageView:Landroid/widget/ImageView;

    .line 65
    iput-object p3, p0, Lcom/squareup/orderentry/FlyByCoordinator$RemoveFlyByViewAndShadow;->shadowView:Landroid/widget/ImageView;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .line 69
    iget-object v0, p0, Lcom/squareup/orderentry/FlyByCoordinator$RemoveFlyByViewAndShadow;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 70
    iget-object v0, p0, Lcom/squareup/orderentry/FlyByCoordinator$RemoveFlyByViewAndShadow;->this$0:Lcom/squareup/orderentry/FlyByCoordinator;

    invoke-static {v0}, Lcom/squareup/orderentry/FlyByCoordinator;->access$000(Lcom/squareup/orderentry/FlyByCoordinator;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/orderentry/FlyByCoordinator$RemoveFlyByViewAndShadow;->imageView:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 71
    iget-object v0, p0, Lcom/squareup/orderentry/FlyByCoordinator$RemoveFlyByViewAndShadow;->this$0:Lcom/squareup/orderentry/FlyByCoordinator;

    invoke-static {v0}, Lcom/squareup/orderentry/FlyByCoordinator;->access$000(Lcom/squareup/orderentry/FlyByCoordinator;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/orderentry/FlyByCoordinator$RemoveFlyByViewAndShadow;->shadowView:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 72
    iget-object v0, p0, Lcom/squareup/orderentry/FlyByCoordinator$RemoveFlyByViewAndShadow;->this$0:Lcom/squareup/orderentry/FlyByCoordinator;

    invoke-static {v0}, Lcom/squareup/orderentry/FlyByCoordinator;->access$100(Lcom/squareup/orderentry/FlyByCoordinator;)Landroid/widget/FrameLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/orderentry/FlyByCoordinator$RemoveFlyByViewAndShadow;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/orderentry/FlyByCoordinator$RemoveFlyByViewAndShadow;->this$0:Lcom/squareup/orderentry/FlyByCoordinator;

    invoke-static {v0}, Lcom/squareup/orderentry/FlyByCoordinator;->access$100(Lcom/squareup/orderentry/FlyByCoordinator;)Landroid/widget/FrameLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/orderentry/FlyByCoordinator$RemoveFlyByViewAndShadow;->shadowView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 74
    iget-object v0, p0, Lcom/squareup/orderentry/FlyByCoordinator$RemoveFlyByViewAndShadow;->this$0:Lcom/squareup/orderentry/FlyByCoordinator;

    invoke-static {v0}, Lcom/squareup/orderentry/FlyByCoordinator;->access$200(Lcom/squareup/orderentry/FlyByCoordinator;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method
