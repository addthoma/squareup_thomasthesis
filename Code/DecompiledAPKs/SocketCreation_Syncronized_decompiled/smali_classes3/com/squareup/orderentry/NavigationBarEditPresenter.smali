.class public Lcom/squareup/orderentry/NavigationBarEditPresenter;
.super Lcom/squareup/orderentry/NavigationBarAbstractPresenter;
.source "NavigationBarEditPresenter.java"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final flow:Lflow/Flow;


# direct methods
.method public constructor <init>(Lcom/squareup/orderentry/pages/OrderEntryPages;Lflow/Flow;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Device;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/librarylist/LibraryListStateManager;)V
    .locals 7
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    move-object v1, p5

    move-object v2, p1

    move-object v3, p3

    move-object v4, p7

    move-object v5, p4

    move-object v6, p6

    .line 30
    invoke-direct/range {v0 .. v6}, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;-><init>(Lcom/squareup/util/Device;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/librarylist/LibraryListStateManager;Lcom/squareup/analytics/Analytics;Lcom/squareup/tutorialv2/TutorialCore;)V

    .line 31
    iput-object p4, p0, Lcom/squareup/orderentry/NavigationBarEditPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 32
    iput-object p2, p0, Lcom/squareup/orderentry/NavigationBarEditPresenter;->flow:Lflow/Flow;

    return-void
.end method


# virtual methods
.method public tabLongClicked(Lcom/squareup/orderentry/pages/OrderEntryPage;)V
    .locals 4

    .line 36
    invoke-virtual {p1}, Lcom/squareup/orderentry/pages/OrderEntryPage;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    .line 37
    iget-object v2, p0, Lcom/squareup/orderentry/NavigationBarEditPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v3, Lcom/squareup/orderentry/PageTabLongPress;

    invoke-direct {v3, v0, v1}, Lcom/squareup/orderentry/PageTabLongPress;-><init>(ZZ)V

    invoke-interface {v2, v3}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 38
    iget-object v0, p0, Lcom/squareup/orderentry/NavigationBarEditPresenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/orderentry/PageLabelEditScreen;

    invoke-virtual {p1}, Lcom/squareup/orderentry/pages/OrderEntryPage;->getFavoritesPageId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/orderentry/pages/OrderEntryPage;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v2, p1}, Lcom/squareup/orderentry/PageLabelEditScreen;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
