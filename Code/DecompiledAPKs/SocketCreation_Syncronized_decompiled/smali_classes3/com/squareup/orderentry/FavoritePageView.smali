.class public Lcom/squareup/orderentry/FavoritePageView;
.super Landroid/view/ViewGroup;
.source "FavoritePageView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orderentry/FavoritePageView$DragLocalState;,
        Lcom/squareup/orderentry/FavoritePageView$EnabledClickListener;,
        Lcom/squareup/orderentry/FavoritePageView$ViewType;
    }
.end annotation


# static fields
.field private static final EDIT_MODE_ANIM_DURATION_MS:I = 0x96

.field private static final MAX_EDIT_ANIM_DELAY_MS:I = 0x12c

.field private static final START_EDITING_DURATION_MS:I = 0x3e8

.field private static final TILE_ANIM_DURATION_MS:I = 0xc8

.field public static final TOOLTIP_ANIMATION_DURATION_MS:I = 0xc8


# instance fields
.field catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private columnCount:I

.field private final deleteButtonSize:I

.field private final deleteButtonsByPosition:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Landroid/graphics/Point;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private innerPadding:I

.field private lastDownX:F

.field private lastDownY:F

.field private outerPadding:I

.field presenter:Lcom/squareup/orderentry/FavoritePageScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final regularTileExtraHeight:I

.field private requestLayoutDisposable:Lio/reactivex/disposables/CompositeDisposable;

.field private final res:Landroid/content/res/Resources;

.field private rowCount:I

.field private final stackedTileExtraHeight:I

.field private final startEditing:Ljava/lang/Runnable;

.field private tempEmptyTileWhileDragging:Lcom/squareup/orderentry/EmptyTile;

.field private textTile:Z

.field private tileHeight:I

.field private tileWidth:I

.field private final tilesByPosition:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Landroid/graphics/Point;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field toastFactory:Lcom/squareup/util/ToastFactory;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final tooltipArrowHeight:I

.field private tooltipView:Landroid/view/View;

.field private final touchSlop:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 99
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 79
    new-instance p2, Lcom/squareup/orderentry/-$$Lambda$FavoritePageView$UjkZPuWX0UIaYybvIWbQPdn4XrA;

    invoke-direct {p2, p0}, Lcom/squareup/orderentry/-$$Lambda$FavoritePageView$UjkZPuWX0UIaYybvIWbQPdn4XrA;-><init>(Lcom/squareup/orderentry/FavoritePageView;)V

    iput-object p2, p0, Lcom/squareup/orderentry/FavoritePageView;->startEditing:Ljava/lang/Runnable;

    const/high16 p2, -0x40800000    # -1.0f

    .line 84
    iput p2, p0, Lcom/squareup/orderentry/FavoritePageView;->lastDownX:F

    .line 85
    iput p2, p0, Lcom/squareup/orderentry/FavoritePageView;->lastDownY:F

    .line 90
    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    iput-object p2, p0, Lcom/squareup/orderentry/FavoritePageView;->tilesByPosition:Ljava/util/HashMap;

    .line 91
    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    iput-object p2, p0, Lcom/squareup/orderentry/FavoritePageView;->deleteButtonsByPosition:Ljava/util/HashMap;

    .line 94
    new-instance p2, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {p2}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object p2, p0, Lcom/squareup/orderentry/FavoritePageView;->requestLayoutDisposable:Lio/reactivex/disposables/CompositeDisposable;

    .line 100
    const-class p2, Lcom/squareup/orderentry/FavoritePageScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/orderentry/FavoritePageScreen$Component;

    invoke-interface {p2, p0}, Lcom/squareup/orderentry/FavoritePageScreen$Component;->inject(Lcom/squareup/orderentry/FavoritePageView;)V

    .line 101
    invoke-virtual {p0}, Lcom/squareup/orderentry/FavoritePageView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/orderentry/FavoritePageView;->res:Landroid/content/res/Resources;

    .line 103
    invoke-direct {p0}, Lcom/squareup/orderentry/FavoritePageView;->updatePaddings()V

    .line 105
    iget-object p2, p0, Lcom/squareup/orderentry/FavoritePageView;->res:Landroid/content/res/Resources;

    sget v0, Lcom/squareup/marin/R$dimen;->marin_grid_regular_tile_extra_height:I

    .line 106
    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/orderentry/FavoritePageView;->regularTileExtraHeight:I

    .line 107
    iget-object p2, p0, Lcom/squareup/orderentry/FavoritePageView;->res:Landroid/content/res/Resources;

    sget v0, Lcom/squareup/marin/R$dimen;->marin_grid_stacked_tile_extra_height:I

    .line 108
    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/orderentry/FavoritePageView;->stackedTileExtraHeight:I

    .line 109
    iget-object p2, p0, Lcom/squareup/orderentry/FavoritePageView;->res:Landroid/content/res/Resources;

    sget v0, Lcom/squareup/registerlib/R$dimen;->grid_delete_button_size_with_padding:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/orderentry/FavoritePageView;->deleteButtonSize:I

    .line 111
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result p1

    mul-int/lit8 p1, p1, 0x4

    iput p1, p0, Lcom/squareup/orderentry/FavoritePageView;->touchSlop:I

    .line 112
    iget-object p1, p0, Lcom/squareup/orderentry/FavoritePageView;->res:Landroid/content/res/Resources;

    sget p2, Lcom/squareup/orderentry/R$dimen;->favorite_tooltip_bottom_arrow:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/orderentry/FavoritePageView;->tooltipArrowHeight:I

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/orderentry/FavoritePageView;Landroid/view/View;)Landroid/graphics/Point;
    .locals 0

    .line 57
    invoke-direct {p0, p1}, Lcom/squareup/orderentry/FavoritePageView;->getViewPosition(Landroid/view/View;)Landroid/graphics/Point;

    move-result-object p0

    return-object p0
.end method

.method private clearCallback()V
    .locals 1

    .line 997
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageView;->startEditing:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/squareup/orderentry/FavoritePageView;->removeCallbacks(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private createStackedDiscountTile(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;
    .locals 1

    .line 481
    iget-boolean p2, p0, Lcom/squareup/orderentry/FavoritePageView;->textTile:Z

    const-string v0, ""

    if-eqz p2, :cond_0

    .line 482
    sget p2, Lcom/squareup/widgets/pos/R$layout;->favorite_text_tile:I

    .line 483
    invoke-static {p2, p0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/orderentry/TextTile;

    .line 484
    invoke-virtual {p2, p1, v0}, Lcom/squareup/orderentry/TextTile;->setContent(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    sget-object p1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->TAG_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p2, p1}, Lcom/squareup/orderentry/TextTile;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 486
    invoke-virtual {p2}, Lcom/squareup/orderentry/TextTile;->setStackedBackground()V

    goto :goto_0

    .line 489
    :cond_0
    sget p2, Lcom/squareup/widgets/pos/R$layout;->favorite_item_icon_tile:I

    .line 490
    invoke-static {p2, p0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/orderentry/GridTileView;

    .line 491
    invoke-virtual {p2, p1, v0}, Lcom/squareup/orderentry/GridTileView;->setDiscount(Ljava/lang/CharSequence;Ljava/lang/String;)V

    .line 492
    invoke-virtual {p2}, Lcom/squareup/orderentry/GridTileView;->setStackedBackground()V

    .line 495
    :goto_0
    sget p1, Lcom/squareup/orderentry/R$id;->favorite_view_type_tag:I

    sget-object v0, Lcom/squareup/orderentry/FavoritePageView$ViewType;->TILE_STACKED:Lcom/squareup/orderentry/FavoritePageView$ViewType;

    invoke-virtual {p2, p1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 496
    invoke-virtual {p2, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 497
    invoke-direct {p0, p2}, Lcom/squareup/orderentry/FavoritePageView;->setDraggable(Landroid/view/View;)V

    return-object p2
.end method

.method private createStackedGiftCardTile(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;
    .locals 1

    .line 516
    iget-boolean p2, p0, Lcom/squareup/orderentry/FavoritePageView;->textTile:Z

    const-string v0, ""

    if-eqz p2, :cond_0

    .line 517
    sget p2, Lcom/squareup/widgets/pos/R$layout;->favorite_text_tile:I

    .line 518
    invoke-static {p2, p0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/orderentry/TextTile;

    .line 519
    invoke-virtual {p2, p1, v0}, Lcom/squareup/orderentry/TextTile;->setContent(Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    sget-object p1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->GIFT_CARD_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p2, p1}, Lcom/squareup/orderentry/TextTile;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 521
    invoke-virtual {p2}, Lcom/squareup/orderentry/TextTile;->setStackedBackground()V

    goto :goto_0

    .line 524
    :cond_0
    sget p2, Lcom/squareup/widgets/pos/R$layout;->favorite_item_icon_tile:I

    .line 525
    invoke-static {p2, p0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/orderentry/GridTileView;

    .line 526
    invoke-virtual {p2, p1, v0, v0}, Lcom/squareup/orderentry/GridTileView;->setGiftCard(Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)V

    .line 527
    invoke-virtual {p2}, Lcom/squareup/orderentry/GridTileView;->setStackedBackground()V

    .line 530
    :goto_0
    sget p1, Lcom/squareup/orderentry/R$id;->favorite_view_type_tag:I

    sget-object v0, Lcom/squareup/orderentry/FavoritePageView$ViewType;->TILE_STACKED:Lcom/squareup/orderentry/FavoritePageView$ViewType;

    invoke-virtual {p2, p1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 531
    invoke-virtual {p2, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 532
    invoke-direct {p0, p2}, Lcom/squareup/orderentry/FavoritePageView;->setDraggable(Landroid/view/View;)V

    return-object p2
.end method

.method private createStackedGlyphTile(Ljava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;Landroid/view/View$OnClickListener;)Landroid/view/View;
    .locals 2

    .line 538
    iget-boolean v0, p0, Lcom/squareup/orderentry/FavoritePageView;->textTile:Z

    if-eqz v0, :cond_0

    .line 539
    sget v0, Lcom/squareup/widgets/pos/R$layout;->favorite_text_tile:I

    .line 540
    invoke-static {v0, p0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/TextTile;

    const-string v1, ""

    .line 541
    invoke-virtual {v0, p1, v1}, Lcom/squareup/orderentry/TextTile;->setContent(Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    invoke-virtual {v0, p2}, Lcom/squareup/orderentry/TextTile;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 543
    invoke-virtual {v0}, Lcom/squareup/orderentry/TextTile;->setStackedBackground()V

    goto :goto_0

    .line 546
    :cond_0
    sget v0, Lcom/squareup/widgets/pos/R$layout;->favorite_item_icon_tile:I

    .line 547
    invoke-static {v0, p0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/GridTileView;

    .line 548
    invoke-virtual {v0, p1, p2}, Lcom/squareup/orderentry/GridTileView;->setGlyph(Ljava/lang/CharSequence;Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 549
    invoke-virtual {v0}, Lcom/squareup/orderentry/GridTileView;->setStackedBackground()V

    .line 552
    :goto_0
    sget p1, Lcom/squareup/orderentry/R$id;->favorite_view_type_tag:I

    sget-object p2, Lcom/squareup/orderentry/FavoritePageView$ViewType;->TILE_STACKED:Lcom/squareup/orderentry/FavoritePageView$ViewType;

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 553
    invoke-virtual {v0, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 554
    invoke-direct {p0, v0}, Lcom/squareup/orderentry/FavoritePageView;->setDraggable(Landroid/view/View;)V

    return-object v0
.end method

.method private createStackedGlyphTile(Ljava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;
    .locals 0

    .line 560
    invoke-direct {p0, p1, p2, p4}, Lcom/squareup/orderentry/FavoritePageView;->createStackedGlyphTile(Ljava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method private createStackedRewardsCardTile(Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;
    .locals 1

    .line 503
    iget-boolean v0, p0, Lcom/squareup/orderentry/FavoritePageView;->textTile:Z

    if-eqz v0, :cond_0

    .line 504
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->REWARDS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    goto :goto_0

    .line 505
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageView;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isTabletAtLeast10Inches()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 506
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->REWARDS_LARGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    goto :goto_0

    .line 508
    :cond_1
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->REWARDS_MEDIUM:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 510
    :goto_0
    invoke-direct {p0, p1, v0, p2}, Lcom/squareup/orderentry/FavoritePageView;->createStackedGlyphTile(Ljava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method private dragEnded(Landroid/view/View;)V
    .locals 2

    .line 888
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageView;->tempEmptyTileWhileDragging:Lcom/squareup/orderentry/EmptyTile;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 890
    iput-object v1, p0, Lcom/squareup/orderentry/FavoritePageView;->tempEmptyTileWhileDragging:Lcom/squareup/orderentry/EmptyTile;

    .line 894
    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$FavoritePageView$ZDd4Vc-v5hKaOwws0KBjCBLmq7o;

    invoke-direct {v1, p0, v0, p1}, Lcom/squareup/orderentry/-$$Lambda$FavoritePageView$ZDd4Vc-v5hKaOwws0KBjCBLmq7o;-><init>(Lcom/squareup/orderentry/FavoritePageView;Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {p0, v1}, Lcom/squareup/orderentry/FavoritePageView;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method private dragStarted(Landroid/view/View;)V
    .locals 4

    .line 921
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageView;->tempEmptyTileWhileDragging:Lcom/squareup/orderentry/EmptyTile;

    if-nez v0, :cond_1

    .line 922
    invoke-direct {p0, p1}, Lcom/squareup/orderentry/FavoritePageView;->getViewPosition(Landroid/view/View;)Landroid/graphics/Point;

    move-result-object v0

    .line 923
    iget-object v1, p0, Lcom/squareup/orderentry/FavoritePageView;->deleteButtonsByPosition:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v1, :cond_0

    const/4 v2, 0x4

    .line 935
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 936
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 937
    invoke-virtual {p0}, Lcom/squareup/orderentry/FavoritePageView;->createEmptyView()Lcom/squareup/orderentry/EmptyTile;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/orderentry/FavoritePageView;->tempEmptyTileWhileDragging:Lcom/squareup/orderentry/EmptyTile;

    .line 938
    iget-object p1, p0, Lcom/squareup/orderentry/FavoritePageView;->tempEmptyTileWhileDragging:Lcom/squareup/orderentry/EmptyTile;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p1, v1}, Lcom/squareup/orderentry/EmptyTile;->setPlusButtonSize(F)V

    .line 939
    iget-object p1, p0, Lcom/squareup/orderentry/FavoritePageView;->tempEmptyTileWhileDragging:Lcom/squareup/orderentry/EmptyTile;

    sget v1, Lcom/squareup/orderentry/R$id;->favorite_view_position_tag:I

    invoke-virtual {p1, v1, v0}, Lcom/squareup/orderentry/EmptyTile;->setTag(ILjava/lang/Object;)V

    .line 940
    iget-object p1, p0, Lcom/squareup/orderentry/FavoritePageView;->tempEmptyTileWhileDragging:Lcom/squareup/orderentry/EmptyTile;

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/FavoritePageView;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 926
    :cond_0
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not find delete button for view "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " when starting drag, on position "

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " deleteButtonsByPosition:"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/squareup/orderentry/FavoritePageView;->deleteButtonsByPosition:Ljava/util/HashMap;

    .line 931
    invoke-virtual {p1}, Ljava/util/HashMap;->size()I

    move-result p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " tilesByPosition:"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/squareup/orderentry/FavoritePageView;->tilesByPosition:Ljava/util/HashMap;

    .line 933
    invoke-virtual {p1}, Ljava/util/HashMap;->size()I

    move-result p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    :goto_0
    return-void
.end method

.method private getViewPosition(Landroid/view/View;)Landroid/graphics/Point;
    .locals 1

    .line 1005
    sget v0, Lcom/squareup/orderentry/R$id;->favorite_view_position_tag:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/graphics/Point;

    return-object p1
.end method

.method private getViewType(Landroid/view/View;)Lcom/squareup/orderentry/FavoritePageView$ViewType;
    .locals 1

    .line 1009
    sget v0, Lcom/squareup/orderentry/R$id;->favorite_view_type_tag:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/FavoritePageView$ViewType;

    return-object p1
.end method

.method private isEmptyTileView(Landroid/view/View;)Z
    .locals 1

    .line 1001
    invoke-direct {p0, p1}, Lcom/squareup/orderentry/FavoritePageView;->getViewType(Landroid/view/View;)Lcom/squareup/orderentry/FavoritePageView$ViewType;

    move-result-object p1

    sget-object v0, Lcom/squareup/orderentry/FavoritePageView$ViewType;->TILE_EMPTY:Lcom/squareup/orderentry/FavoritePageView$ViewType;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public static synthetic lambda$UjkZPuWX0UIaYybvIWbQPdn4XrA(Lcom/squareup/orderentry/FavoritePageView;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/orderentry/FavoritePageView;->startEditing()V

    return-void
.end method

.method private pointInView(Landroid/view/View;FF)Z
    .locals 1

    .line 1013
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, p2, v0

    if-ltz v0, :cond_0

    .line 1014
    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v0

    int-to-float v0, v0

    cmpg-float p2, p2, v0

    if-gez p2, :cond_0

    .line 1015
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result p2

    int-to-float p2, p2

    cmpl-float p2, p3, p2

    if-ltz p2, :cond_0

    .line 1016
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result p1

    int-to-float p1, p1

    cmpg-float p1, p3, p1

    if-gez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private setAsDropTarget(Lcom/squareup/orderentry/EmptyTile;)V
    .locals 1

    .line 835
    new-instance v0, Lcom/squareup/orderentry/-$$Lambda$FavoritePageView$KWukKVwTHTsEdDZOg1E-Uu1pXUU;

    invoke-direct {v0, p0, p1}, Lcom/squareup/orderentry/-$$Lambda$FavoritePageView$KWukKVwTHTsEdDZOg1E-Uu1pXUU;-><init>(Lcom/squareup/orderentry/FavoritePageView;Lcom/squareup/orderentry/EmptyTile;)V

    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/EmptyTile;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    return-void
.end method

.method private setDraggable(Landroid/view/View;)V
    .locals 1

    .line 906
    new-instance v0, Lcom/squareup/orderentry/-$$Lambda$FavoritePageView$Z5y3D4RglN0_tQbpICdDyzuEG3E;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/-$$Lambda$FavoritePageView$Z5y3D4RglN0_tQbpICdDyzuEG3E;-><init>(Lcom/squareup/orderentry/FavoritePageView;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    return-void
.end method

.method private startDraggingIfTile(FF)V
    .locals 5

    .line 981
    invoke-virtual {p0}, Lcom/squareup/orderentry/FavoritePageView;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    .line 983
    invoke-virtual {p0, v1}, Lcom/squareup/orderentry/FavoritePageView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 984
    invoke-direct {p0, v2, p1, p2}, Lcom/squareup/orderentry/FavoritePageView;->pointInView(Landroid/view/View;FF)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 985
    invoke-direct {p0, v2}, Lcom/squareup/orderentry/FavoritePageView;->getViewType(Landroid/view/View;)Lcom/squareup/orderentry/FavoritePageView$ViewType;

    move-result-object v3

    .line 986
    sget-object v4, Lcom/squareup/orderentry/FavoritePageView$11;->$SwitchMap$com$squareup$orderentry$FavoritePageView$ViewType:[I

    invoke-virtual {v3}, Lcom/squareup/orderentry/FavoritePageView$ViewType;->ordinal()I

    move-result v3

    aget v3, v4, v3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    goto :goto_1

    .line 989
    :cond_0
    invoke-direct {p0, v2}, Lcom/squareup/orderentry/FavoritePageView;->startDraggingTile(Landroid/view/View;)V

    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private startDraggingTile(Landroid/view/View;)V
    .locals 4

    const-string v0, ""

    .line 916
    invoke-static {v0, v0}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v0

    new-instance v1, Landroid/view/View$DragShadowBuilder;

    invoke-direct {v1, p1}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    new-instance v2, Lcom/squareup/orderentry/FavoritePageView$DragLocalState;

    const/4 v3, 0x0

    invoke-direct {v2, p0, p1, p0, v3}, Lcom/squareup/orderentry/FavoritePageView$DragLocalState;-><init>(Lcom/squareup/orderentry/FavoritePageView;Landroid/view/View;Lcom/squareup/orderentry/FavoritePageView;Lcom/squareup/orderentry/FavoritePageView$1;)V

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    return-void
.end method

.method private startEditing()V
    .locals 6

    .line 950
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageView;->presenter:Lcom/squareup/orderentry/FavoritePageScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->canGoToEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 951
    invoke-virtual {p0}, Lcom/squareup/orderentry/FavoritePageView;->cancelPressToEditMode()V

    return-void

    .line 955
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/orderentry/FavoritePageView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    const-wide/16 v1, 0x32

    .line 956
    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate(J)V

    .line 958
    iget v0, p0, Lcom/squareup/orderentry/FavoritePageView;->lastDownX:F

    .line 959
    iget v1, p0, Lcom/squareup/orderentry/FavoritePageView;->lastDownY:F

    .line 961
    iget-object v2, p0, Lcom/squareup/orderentry/FavoritePageView;->presenter:Lcom/squareup/orderentry/FavoritePageScreen$Presenter;

    invoke-virtual {v2}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->startEditing()V

    .line 964
    invoke-virtual {p0}, Lcom/squareup/orderentry/FavoritePageView;->getChildCount()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_1

    .line 967
    invoke-virtual {p0, v4}, Lcom/squareup/orderentry/FavoritePageView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 968
    invoke-virtual {v5, v3}, Landroid/view/View;->setPressed(Z)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    const/high16 v2, -0x40800000    # -1.0f

    cmpl-float v2, v0, v2

    if-eqz v2, :cond_2

    .line 975
    iget-object v2, p0, Lcom/squareup/orderentry/FavoritePageView;->presenter:Lcom/squareup/orderentry/FavoritePageScreen$Presenter;

    invoke-virtual {v2}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->canDragTiles()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 976
    invoke-direct {p0, v0, v1}, Lcom/squareup/orderentry/FavoritePageView;->startDraggingIfTile(FF)V

    :cond_2
    return-void
.end method

.method private startPressToEditMode()V
    .locals 3

    .line 945
    invoke-direct {p0}, Lcom/squareup/orderentry/FavoritePageView;->clearCallback()V

    .line 946
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageView;->startEditing:Ljava/lang/Runnable;

    const-wide/16 v1, 0x3e8

    invoke-virtual {p0, v0, v1, v2}, Lcom/squareup/orderentry/FavoritePageView;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private touchStill(FF)Z
    .locals 4

    .line 828
    iget v0, p0, Lcom/squareup/orderentry/FavoritePageView;->lastDownX:F

    iget v1, p0, Lcom/squareup/orderentry/FavoritePageView;->touchSlop:I

    int-to-float v2, v1

    sub-float v2, v0, v2

    cmpl-float v2, p1, v2

    if-ltz v2, :cond_0

    iget v2, p0, Lcom/squareup/orderentry/FavoritePageView;->lastDownY:F

    int-to-float v3, v1

    sub-float v3, v2, v3

    cmpl-float v3, p2, v3

    if-ltz v3, :cond_0

    int-to-float v3, v1

    add-float/2addr v0, v3

    cmpg-float p1, p1, v0

    if-gez p1, :cond_0

    int-to-float p1, v1

    add-float/2addr v2, p1

    cmpg-float p1, p2, v2

    if-gez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private updatePaddings()V
    .locals 2

    .line 813
    iget-boolean v0, p0, Lcom/squareup/orderentry/FavoritePageView;->textTile:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageView;->res:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/marin/R$dimen;->marin_text_tile_inner_padding:I

    .line 814
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageView;->res:Landroid/content/res/Resources;

    iget-object v1, p0, Lcom/squareup/orderentry/FavoritePageView;->device:Lcom/squareup/util/Device;

    .line 815
    invoke-interface {v1}, Lcom/squareup/util/Device;->isLandscapeLongTablet()Z

    move-result v1

    if-eqz v1, :cond_1

    sget v1, Lcom/squareup/marin/R$dimen;->marin_gap_grid_padding_wide:I

    goto :goto_0

    :cond_1
    sget v1, Lcom/squareup/marin/R$dimen;->marin_gap_grid_inner_padding:I

    :goto_0
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    :goto_1
    iput v0, p0, Lcom/squareup/orderentry/FavoritePageView;->innerPadding:I

    .line 818
    iget-boolean v0, p0, Lcom/squareup/orderentry/FavoritePageView;->textTile:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageView;->res:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/marin/R$dimen;->marin_text_tile_outer_padding:I

    .line 819
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_3

    :cond_2
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageView;->res:Landroid/content/res/Resources;

    iget-object v1, p0, Lcom/squareup/orderentry/FavoritePageView;->device:Lcom/squareup/util/Device;

    .line 820
    invoke-interface {v1}, Lcom/squareup/util/Device;->isLandscapeLongTablet()Z

    move-result v1

    if-eqz v1, :cond_3

    sget v1, Lcom/squareup/marin/R$dimen;->marin_gap_grid_padding_wide:I

    goto :goto_2

    :cond_3
    sget v1, Lcom/squareup/marin/R$dimen;->marin_gap_grid_outer_padding:I

    :goto_2
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    :goto_3
    iput v0, p0, Lcom/squareup/orderentry/FavoritePageView;->outerPadding:I

    .line 824
    iget v0, p0, Lcom/squareup/orderentry/FavoritePageView;->outerPadding:I

    invoke-virtual {p0, v0, v0, v0, v0}, Lcom/squareup/orderentry/FavoritePageView;->setPadding(IIII)V

    return-void
.end method


# virtual methods
.method protected cancelPressToEditMode()V
    .locals 1

    .line 807
    invoke-direct {p0}, Lcom/squareup/orderentry/FavoritePageView;->clearCallback()V

    const/high16 v0, -0x40800000    # -1.0f

    .line 808
    iput v0, p0, Lcom/squareup/orderentry/FavoritePageView;->lastDownX:F

    .line 809
    iput v0, p0, Lcom/squareup/orderentry/FavoritePageView;->lastDownY:F

    return-void
.end method

.method createAllDiscountsView(Ljava/lang/String;)Landroid/view/View;
    .locals 2

    .line 450
    invoke-virtual {p0}, Lcom/squareup/orderentry/FavoritePageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/orderentry/R$string;->favorite_tile_all_discounts:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 451
    new-instance v1, Lcom/squareup/orderentry/FavoritePageView$7;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/FavoritePageView$7;-><init>(Lcom/squareup/orderentry/FavoritePageView;)V

    invoke-direct {p0, v0, p1, v1}, Lcom/squareup/orderentry/FavoritePageView;->createStackedDiscountTile(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method createAllGiftCardsView(Ljava/lang/String;)Landroid/view/View;
    .locals 2

    .line 460
    invoke-virtual {p0}, Lcom/squareup/orderentry/FavoritePageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/librarylist/R$string;->item_library_all_gift_cards:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 461
    new-instance v1, Lcom/squareup/orderentry/FavoritePageView$8;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/FavoritePageView$8;-><init>(Lcom/squareup/orderentry/FavoritePageView;)V

    invoke-direct {p0, v0, p1, v1}, Lcom/squareup/orderentry/FavoritePageView;->createStackedGiftCardTile(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method createAllItemsView(Ljava/lang/String;)Landroid/view/View;
    .locals 3

    .line 428
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageView;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-interface {v0}, Lcom/squareup/catalogapi/CatalogIntegrationController;->shouldShowPartiallyRolledOutFeature()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 429
    invoke-virtual {p0}, Lcom/squareup/orderentry/FavoritePageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/orderentry/R$string;->favorite_tile_all_items_and_services:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 431
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/orderentry/FavoritePageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/orderentry/R$string;->favorite_tile_all_items:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 435
    :goto_0
    iget-boolean v1, p0, Lcom/squareup/orderentry/FavoritePageView;->textTile:Z

    if-eqz v1, :cond_1

    .line 436
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->STACK_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    goto :goto_1

    .line 437
    :cond_1
    iget-object v1, p0, Lcom/squareup/orderentry/FavoritePageView;->device:Lcom/squareup/util/Device;

    invoke-interface {v1}, Lcom/squareup/util/Device;->isTabletAtLeast10Inches()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 438
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->STACK_LARGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    goto :goto_1

    .line 440
    :cond_2
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->STACK_MEDIUM:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 442
    :goto_1
    new-instance v2, Lcom/squareup/orderentry/FavoritePageView$6;

    invoke-direct {v2, p0}, Lcom/squareup/orderentry/FavoritePageView$6;-><init>(Lcom/squareup/orderentry/FavoritePageView;)V

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/squareup/orderentry/FavoritePageView;->createStackedGlyphTile(Ljava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method createDiscountView(Lcom/squareup/shared/catalog/models/CatalogDiscount;Ljava/lang/CharSequence;)Landroid/view/View;
    .locals 2

    .line 373
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getName()Ljava/lang/String;

    move-result-object v0

    .line 375
    iget-boolean v1, p0, Lcom/squareup/orderentry/FavoritePageView;->textTile:Z

    if-eqz v1, :cond_0

    .line 376
    sget p2, Lcom/squareup/widgets/pos/R$layout;->favorite_text_tile:I

    .line 377
    invoke-static {p2, p0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/orderentry/TextTile;

    const-string v1, ""

    .line 378
    invoke-virtual {p2, v0, v1}, Lcom/squareup/orderentry/TextTile;->setContent(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->TAG_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p2, v0}, Lcom/squareup/orderentry/TextTile;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    goto :goto_0

    .line 382
    :cond_0
    sget v1, Lcom/squareup/widgets/pos/R$layout;->favorite_item_icon_tile:I

    .line 383
    invoke-static {v1, p0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/orderentry/GridTileView;

    .line 384
    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, v0, p2}, Lcom/squareup/orderentry/GridTileView;->setDiscount(Ljava/lang/CharSequence;Ljava/lang/String;)V

    move-object p2, v1

    .line 387
    :goto_0
    sget v0, Lcom/squareup/orderentry/R$id;->favorite_view_type_tag:I

    sget-object v1, Lcom/squareup/orderentry/FavoritePageView$ViewType;->TILE_REGULAR:Lcom/squareup/orderentry/FavoritePageView$ViewType;

    invoke-virtual {p2, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 388
    new-instance v0, Lcom/squareup/orderentry/FavoritePageView$4;

    invoke-direct {v0, p0, p2, p1}, Lcom/squareup/orderentry/FavoritePageView$4;-><init>(Lcom/squareup/orderentry/FavoritePageView;Landroid/view/View;Lcom/squareup/shared/catalog/models/CatalogDiscount;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 393
    invoke-direct {p0, p2}, Lcom/squareup/orderentry/FavoritePageView;->setDraggable(Landroid/view/View;)V

    return-object p2
.end method

.method createEmptyView()Lcom/squareup/orderentry/EmptyTile;
    .locals 3

    .line 326
    sget v0, Lcom/squareup/widgets/pos/R$layout;->favorite_empty_tile:I

    .line 327
    invoke-static {v0, p0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/EmptyTile;

    .line 328
    sget v1, Lcom/squareup/orderentry/R$id;->favorite_view_type_tag:I

    sget-object v2, Lcom/squareup/orderentry/FavoritePageView$ViewType;->TILE_EMPTY:Lcom/squareup/orderentry/FavoritePageView$ViewType;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/orderentry/EmptyTile;->setTag(ILjava/lang/Object;)V

    .line 329
    new-instance v1, Lcom/squareup/orderentry/FavoritePageView$2;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/FavoritePageView$2;-><init>(Lcom/squareup/orderentry/FavoritePageView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/EmptyTile;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 335
    invoke-direct {p0, v0}, Lcom/squareup/orderentry/FavoritePageView;->setAsDropTarget(Lcom/squareup/orderentry/EmptyTile;)V

    return-object v0
.end method

.method createItemView(Lcom/squareup/shared/catalog/models/CatalogItem;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;
    .locals 5

    .line 340
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItem;->getName()Ljava/lang/String;

    move-result-object v0

    .line 341
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItem;->getAbbreviationOrAbbreviatedName()Ljava/lang/String;

    move-result-object v1

    .line 342
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItem;->getColor()Ljava/lang/String;

    move-result-object v2

    .line 344
    iget-boolean v3, p0, Lcom/squareup/orderentry/FavoritePageView;->textTile:Z

    if-eqz v3, :cond_0

    .line 345
    sget p2, Lcom/squareup/widgets/pos/R$layout;->favorite_text_tile:I

    .line 346
    invoke-static {p2, p0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/orderentry/TextTile;

    .line 347
    invoke-virtual {p2, v0, v2}, Lcom/squareup/orderentry/TextTile;->setContent(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItem;->isGiftCard()Z

    move-result p3

    if-eqz p3, :cond_2

    .line 349
    sget-object p3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->GIFT_CARD_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p2, p3}, Lcom/squareup/orderentry/TextTile;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    goto :goto_1

    .line 353
    :cond_0
    sget v3, Lcom/squareup/widgets/pos/R$layout;->favorite_item_icon_tile:I

    .line 354
    invoke-static {v3, p0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/squareup/orderentry/GridTileView;

    .line 355
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItem;->isGiftCard()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 356
    invoke-virtual {v3, v0, v2, p3}, Lcom/squareup/orderentry/GridTileView;->setGiftCard(Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 358
    :cond_1
    invoke-virtual {v3, v0, v1, v2, p2}, Lcom/squareup/orderentry/GridTileView;->setItem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    move-object p2, v3

    .line 362
    :cond_2
    :goto_1
    sget p3, Lcom/squareup/orderentry/R$id;->favorite_view_type_tag:I

    sget-object v0, Lcom/squareup/orderentry/FavoritePageView$ViewType;->TILE_REGULAR:Lcom/squareup/orderentry/FavoritePageView$ViewType;

    invoke-virtual {p2, p3, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 363
    new-instance p3, Lcom/squareup/orderentry/FavoritePageView$3;

    invoke-direct {p3, p0, p2, p1}, Lcom/squareup/orderentry/FavoritePageView$3;-><init>(Lcom/squareup/orderentry/FavoritePageView;Landroid/view/View;Lcom/squareup/shared/catalog/models/CatalogItem;)V

    invoke-virtual {p2, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 368
    invoke-direct {p0, p2}, Lcom/squareup/orderentry/FavoritePageView;->setDraggable(Landroid/view/View;)V

    return-object p2
.end method

.method createMenuCategoryView(Lcom/squareup/shared/catalog/models/CatalogItemCategory;)Landroid/view/View;
    .locals 4

    .line 398
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getName()Ljava/lang/String;

    move-result-object v0

    .line 400
    iget-boolean v1, p0, Lcom/squareup/orderentry/FavoritePageView;->textTile:Z

    if-eqz v1, :cond_0

    .line 401
    sget v1, Lcom/squareup/widgets/pos/R$layout;->favorite_text_tile:I

    .line 402
    invoke-static {v1, p0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/orderentry/TextTile;

    .line 403
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getColor()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/squareup/orderentry/TextTile;->setContent(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->STACK_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v1, v0}, Lcom/squareup/orderentry/TextTile;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 405
    invoke-virtual {v1}, Lcom/squareup/orderentry/TextTile;->setStackedBackground()V

    goto :goto_0

    .line 408
    :cond_0
    sget v1, Lcom/squareup/widgets/pos/R$layout;->favorite_item_icon_tile:I

    .line 409
    invoke-static {v1, p0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/orderentry/GridTileView;

    .line 411
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getAbbreviation()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/squareup/util/Strings;->abbreviate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 412
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getColor()Ljava/lang/String;

    move-result-object v3

    .line 410
    invoke-virtual {v1, v0, v2, v3}, Lcom/squareup/orderentry/GridTileView;->setCategory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    invoke-virtual {v1}, Lcom/squareup/orderentry/GridTileView;->setStackedBackground()V

    .line 416
    :goto_0
    sget v0, Lcom/squareup/orderentry/R$id;->favorite_view_type_tag:I

    sget-object v2, Lcom/squareup/orderentry/FavoritePageView$ViewType;->TILE_STACKED:Lcom/squareup/orderentry/FavoritePageView$ViewType;

    invoke-virtual {v1, v0, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 417
    new-instance v0, Lcom/squareup/orderentry/FavoritePageView$5;

    invoke-direct {v0, p0, p1}, Lcom/squareup/orderentry/FavoritePageView$5;-><init>(Lcom/squareup/orderentry/FavoritePageView;Lcom/squareup/shared/catalog/models/CatalogItemCategory;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 422
    invoke-direct {p0, v1}, Lcom/squareup/orderentry/FavoritePageView;->setDraggable(Landroid/view/View;)V

    return-object v1
.end method

.method createRewardsSearchView()Landroid/view/View;
    .locals 2

    .line 470
    invoke-virtual {p0}, Lcom/squareup/orderentry/FavoritePageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/librarylist/R$string;->item_library_redeem_rewards:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 471
    new-instance v1, Lcom/squareup/orderentry/FavoritePageView$9;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/FavoritePageView$9;-><init>(Lcom/squareup/orderentry/FavoritePageView;)V

    invoke-direct {p0, v0, v1}, Lcom/squareup/orderentry/FavoritePageView;->createStackedRewardsCardTile(Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method createUnknownView()Landroid/view/View;
    .locals 4

    .line 566
    iget-boolean v0, p0, Lcom/squareup/orderentry/FavoritePageView;->textTile:Z

    if-eqz v0, :cond_0

    .line 567
    sget v0, Lcom/squareup/widgets/pos/R$layout;->favorite_text_tile:I

    .line 568
    invoke-static {v0, p0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/TextTile;

    .line 569
    invoke-virtual {p0}, Lcom/squareup/orderentry/FavoritePageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/orderentry/R$string;->favorite_tile_unknown_name:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/squareup/orderentry/TextTile;->setContent(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 572
    :cond_0
    sget v0, Lcom/squareup/widgets/pos/R$layout;->favorite_item_icon_tile:I

    .line 573
    invoke-static {v0, p0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/GridTileView;

    .line 574
    invoke-virtual {p0}, Lcom/squareup/orderentry/FavoritePageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 575
    sget v2, Lcom/squareup/orderentry/R$string;->favorite_tile_unknown_name:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/squareup/orderentry/R$string;->favorite_tile_unknown_abbrev:I

    .line 576
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 575
    invoke-virtual {v0, v2, v1}, Lcom/squareup/orderentry/GridTileView;->setContent(Ljava/lang/String;Ljava/lang/String;)V

    .line 579
    :goto_0
    sget v1, Lcom/squareup/orderentry/R$id;->favorite_view_type_tag:I

    sget-object v2, Lcom/squareup/orderentry/FavoritePageView$ViewType;->TILE_REGULAR:Lcom/squareup/orderentry/FavoritePageView$ViewType;

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 580
    new-instance v1, Lcom/squareup/orderentry/FavoritePageView$10;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/FavoritePageView$10;-><init>(Lcom/squareup/orderentry/FavoritePageView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 585
    invoke-direct {p0, v0}, Lcom/squareup/orderentry/FavoritePageView;->setDraggable(Landroid/view/View;)V

    return-object v0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .line 771
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 773
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_4

    const/high16 v3, -0x40800000    # -1.0f

    if-eq v1, v2, :cond_2

    const/4 v4, 0x2

    if-eq v1, v4, :cond_0

    const/4 p1, 0x3

    if-eq v1, p1, :cond_2

    goto :goto_0

    .line 790
    :cond_0
    iget v1, p0, Lcom/squareup/orderentry/FavoritePageView;->lastDownX:F

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_3

    .line 791
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 792
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    float-to-int p1, p1

    int-to-float v0, v0

    int-to-float p1, p1

    .line 793
    invoke-direct {p0, v0, p1}, Lcom/squareup/orderentry/FavoritePageView;->touchStill(FF)Z

    move-result p1

    if-nez p1, :cond_5

    .line 794
    iget-object p1, p0, Lcom/squareup/orderentry/FavoritePageView;->presenter:Lcom/squareup/orderentry/FavoritePageScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->canDragTiles()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 795
    iget p1, p0, Lcom/squareup/orderentry/FavoritePageView;->lastDownX:F

    iget v0, p0, Lcom/squareup/orderentry/FavoritePageView;->lastDownY:F

    invoke-direct {p0, p1, v0}, Lcom/squareup/orderentry/FavoritePageView;->startDraggingIfTile(FF)V

    .line 797
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/orderentry/FavoritePageView;->cancelPressToEditMode()V

    goto :goto_1

    .line 776
    :cond_2
    iget p1, p0, Lcom/squareup/orderentry/FavoritePageView;->lastDownX:F

    cmpl-float p1, p1, v3

    if-eqz p1, :cond_3

    .line 777
    invoke-virtual {p0}, Lcom/squareup/orderentry/FavoritePageView;->cancelPressToEditMode()V

    goto :goto_1

    :cond_3
    :goto_0
    move v2, v0

    goto :goto_1

    .line 782
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/squareup/orderentry/FavoritePageView;->lastDownX:F

    .line 783
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    iput p1, p0, Lcom/squareup/orderentry/FavoritePageView;->lastDownY:F

    .line 785
    iget-object p1, p0, Lcom/squareup/orderentry/FavoritePageView;->presenter:Lcom/squareup/orderentry/FavoritePageScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->inSaleMode()Z

    move-result p1

    if-eqz p1, :cond_5

    .line 786
    invoke-direct {p0}, Lcom/squareup/orderentry/FavoritePageView;->startPressToEditMode()V

    :cond_5
    :goto_1
    return v2
.end method

.method public synthetic lambda$dragEnded$1$FavoritePageView(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .line 895
    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/FavoritePageView;->removeView(Landroid/view/View;)V

    const/4 p1, 0x0

    .line 896
    invoke-virtual {p2, p1}, Landroid/view/View;->setVisibility(I)V

    .line 897
    invoke-direct {p0, p2}, Lcom/squareup/orderentry/FavoritePageView;->getViewPosition(Landroid/view/View;)Landroid/graphics/Point;

    move-result-object p2

    .line 898
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageView;->deleteButtonsByPosition:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/View;

    .line 899
    invoke-virtual {p2, p1}, Landroid/view/View;->setVisibility(I)V

    .line 900
    iget-object p1, p0, Lcom/squareup/orderentry/FavoritePageView;->presenter:Lcom/squareup/orderentry/FavoritePageScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->endItemDrag()V

    return-void
.end method

.method public synthetic lambda$setAsDropTarget$0$FavoritePageView(Lcom/squareup/orderentry/EmptyTile;Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 6

    .line 836
    invoke-virtual {p3}, Landroid/view/DragEvent;->getAction()I

    move-result p2

    .line 837
    invoke-virtual {p3}, Landroid/view/DragEvent;->getLocalState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/FavoritePageView$DragLocalState;

    invoke-static {v0}, Lcom/squareup/orderentry/FavoritePageView$DragLocalState;->access$300(Lcom/squareup/orderentry/FavoritePageView$DragLocalState;)Landroid/view/View;

    move-result-object v0

    .line 838
    invoke-virtual {p3}, Landroid/view/DragEvent;->getLocalState()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/orderentry/FavoritePageView$DragLocalState;

    invoke-static {p3}, Lcom/squareup/orderentry/FavoritePageView$DragLocalState;->access$400(Lcom/squareup/orderentry/FavoritePageView$DragLocalState;)Lcom/squareup/orderentry/FavoritePageView;

    move-result-object p3

    const/4 v1, 0x0

    if-eq p3, p0, :cond_0

    return v1

    :cond_0
    const/4 p3, 0x1

    packed-switch p2, :pswitch_data_0

    return v1

    .line 854
    :pswitch_0
    invoke-virtual {p1, v1}, Lcom/squareup/orderentry/EmptyTile;->setDropTarget(Z)V

    return p3

    .line 849
    :pswitch_1
    invoke-virtual {p1, p3}, Lcom/squareup/orderentry/EmptyTile;->setDropTarget(Z)V

    return p3

    .line 878
    :pswitch_2
    invoke-virtual {p1, v1}, Lcom/squareup/orderentry/EmptyTile;->setDropTarget(Z)V

    .line 879
    invoke-direct {p0, v0}, Lcom/squareup/orderentry/FavoritePageView;->dragEnded(Landroid/view/View;)V

    return p3

    .line 857
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/squareup/orderentry/FavoritePageView;->getViewPosition(Landroid/view/View;)Landroid/graphics/Point;

    move-result-object p2

    .line 858
    invoke-direct {p0, v0}, Lcom/squareup/orderentry/FavoritePageView;->getViewPosition(Landroid/view/View;)Landroid/graphics/Point;

    move-result-object v2

    .line 859
    invoke-virtual {v2, p2}, Landroid/graphics/Point;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    return p3

    .line 862
    :cond_1
    iget-object v3, p0, Lcom/squareup/orderentry/FavoritePageView;->deleteButtonsByPosition:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 863
    sget v4, Lcom/squareup/orderentry/R$id;->favorite_view_position_tag:I

    new-instance v5, Landroid/graphics/Point;

    invoke-direct {v5, v2}, Landroid/graphics/Point;-><init>(Landroid/graphics/Point;)V

    invoke-virtual {p1, v4, v5}, Lcom/squareup/orderentry/EmptyTile;->setTag(ILjava/lang/Object;)V

    .line 864
    invoke-virtual {p1, v1}, Lcom/squareup/orderentry/EmptyTile;->setDropTarget(Z)V

    .line 866
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, p2}, Landroid/graphics/Point;-><init>(Landroid/graphics/Point;)V

    .line 867
    sget v4, Lcom/squareup/orderentry/R$id;->favorite_view_position_tag:I

    invoke-virtual {v0, v4, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 868
    sget v4, Lcom/squareup/orderentry/R$id;->favorite_view_position_tag:I

    invoke-virtual {v3, v4, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 869
    iget-object v4, p0, Lcom/squareup/orderentry/FavoritePageView;->deleteButtonsByPosition:Ljava/util/HashMap;

    invoke-virtual {v4, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 870
    iget-object v1, p0, Lcom/squareup/orderentry/FavoritePageView;->tilesByPosition:Ljava/util/HashMap;

    invoke-virtual {v1, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 871
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageView;->tilesByPosition:Ljava/util/HashMap;

    invoke-virtual {v0, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 872
    iget-object p1, p0, Lcom/squareup/orderentry/FavoritePageView;->presenter:Lcom/squareup/orderentry/FavoritePageScreen$Presenter;

    iget v0, v2, Landroid/graphics/Point;->x:I

    iget v1, v2, Landroid/graphics/Point;->y:I

    iget v2, p2, Landroid/graphics/Point;->x:I

    iget p2, p2, Landroid/graphics/Point;->y:I

    invoke-virtual {p1, v0, v1, v2, p2}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->moveTile(IIII)V

    .line 875
    invoke-virtual {p0}, Lcom/squareup/orderentry/FavoritePageView;->requestLayout()V

    :pswitch_4
    return p3

    .line 846
    :pswitch_5
    invoke-direct {p0, v0}, Lcom/squareup/orderentry/FavoritePageView;->dragStarted(Landroid/view/View;)V

    return p3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public synthetic lambda$setDraggable$2$FavoritePageView(Landroid/view/View;)Z
    .locals 1

    .line 907
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageView;->presenter:Lcom/squareup/orderentry/FavoritePageScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->canDragTiles()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 908
    invoke-direct {p0, p1}, Lcom/squareup/orderentry/FavoritePageView;->startDraggingTile(Landroid/view/View;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 595
    invoke-direct {p0}, Lcom/squareup/orderentry/FavoritePageView;->clearCallback()V

    .line 596
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageView;->presenter:Lcom/squareup/orderentry/FavoritePageScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 597
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 598
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageView;->requestLayoutDisposable:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->dispose()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 116
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 117
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageView;->presenter:Lcom/squareup/orderentry/FavoritePageScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 18

    move-object/from16 v0, p0

    .line 687
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/orderentry/FavoritePageView;->getPaddingLeft()I

    move-result v1

    .line 688
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/orderentry/FavoritePageView;->getPaddingTop()I

    move-result v2

    .line 689
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/orderentry/FavoritePageView;->getMeasuredWidth()I

    move-result v3

    const/4 v4, 0x2

    div-int/2addr v3, v4

    .line 691
    iget v5, v0, Lcom/squareup/orderentry/FavoritePageView;->tileWidth:I

    iget v6, v0, Lcom/squareup/orderentry/FavoritePageView;->innerPadding:I

    add-int/2addr v5, v6

    .line 692
    iget v7, v0, Lcom/squareup/orderentry/FavoritePageView;->tileHeight:I

    add-int/2addr v7, v6

    .line 694
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/orderentry/FavoritePageView;->getChildCount()I

    move-result v6

    const/4 v8, 0x0

    :goto_0
    if-ge v8, v6, :cond_4

    .line 696
    invoke-virtual {v0, v8}, Lcom/squareup/orderentry/FavoritePageView;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    .line 697
    invoke-direct {v0, v9}, Lcom/squareup/orderentry/FavoritePageView;->getViewType(Landroid/view/View;)Lcom/squareup/orderentry/FavoritePageView$ViewType;

    move-result-object v10

    .line 698
    sget-object v11, Lcom/squareup/orderentry/FavoritePageView$11;->$SwitchMap$com$squareup$orderentry$FavoritePageView$ViewType:[I

    invoke-virtual {v10}, Lcom/squareup/orderentry/FavoritePageView$ViewType;->ordinal()I

    move-result v12

    aget v11, v11, v12

    const/4 v12, 0x1

    if-eq v11, v12, :cond_3

    if-eq v11, v4, :cond_3

    const/4 v12, 0x3

    if-eq v11, v12, :cond_3

    const/4 v13, 0x4

    if-eq v11, v13, :cond_1

    const/4 v12, 0x5

    if-ne v11, v12, :cond_0

    .line 739
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredWidth()I

    move-result v10

    .line 740
    div-int/lit8 v11, v10, 0x2

    sub-int v11, v3, v11

    .line 741
    iget v12, v0, Lcom/squareup/orderentry/FavoritePageView;->rowCount:I

    div-int/2addr v12, v4

    mul-int v12, v12, v7

    add-int/2addr v12, v2

    .line 743
    iget v13, v0, Lcom/squareup/orderentry/FavoritePageView;->tooltipArrowHeight:I

    add-int/2addr v12, v13

    iget v13, v0, Lcom/squareup/orderentry/FavoritePageView;->innerPadding:I

    div-int/2addr v13, v4

    sub-int/2addr v12, v13

    add-int/2addr v10, v11

    .line 746
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v13

    sub-int v13, v12, v13

    .line 747
    invoke-virtual {v9, v11, v13, v10, v12}, Landroid/view/View;->layout(IIII)V

    goto/16 :goto_1

    .line 751
    :cond_0
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 714
    :cond_1
    invoke-direct {v0, v9}, Lcom/squareup/orderentry/FavoritePageView;->getViewPosition(Landroid/view/View;)Landroid/graphics/Point;

    move-result-object v10

    .line 715
    iget v11, v10, Landroid/graphics/Point;->x:I

    .line 716
    iget v10, v10, Landroid/graphics/Point;->y:I

    mul-int v11, v11, v5

    add-int/2addr v11, v1

    mul-int v10, v10, v7

    add-int/2addr v10, v2

    .line 719
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredWidth()I

    move-result v13

    .line 721
    invoke-virtual {v9}, Landroid/view/View;->getPaddingLeft()I

    move-result v14

    invoke-virtual {v9}, Landroid/view/View;->getPaddingRight()I

    move-result v15

    add-int/2addr v14, v15

    sub-int v14, v13, v14

    .line 722
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v15

    .line 724
    invoke-virtual {v9}, Landroid/view/View;->getPaddingTop()I

    move-result v16

    invoke-virtual {v9}, Landroid/view/View;->getPaddingBottom()I

    move-result v17

    add-int v16, v16, v17

    sub-int v16, v15, v16

    .line 729
    iget-object v4, v0, Lcom/squareup/orderentry/FavoritePageView;->device:Lcom/squareup/util/Device;

    invoke-interface {v4}, Lcom/squareup/util/Device;->isTabletAtLeast10Inches()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v12, 0x2

    .line 731
    :cond_2
    invoke-virtual {v9}, Landroid/view/View;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v11, v4

    div-int/2addr v14, v12

    sub-int/2addr v11, v14

    add-int/2addr v13, v11

    .line 733
    invoke-virtual {v9}, Landroid/view/View;->getPaddingTop()I

    move-result v4

    sub-int/2addr v10, v4

    div-int v16, v16, v12

    sub-int v10, v10, v16

    add-int/2addr v15, v10

    .line 735
    invoke-virtual {v9, v11, v10, v13, v15}, Landroid/view/View;->layout(IIII)V

    goto :goto_1

    .line 702
    :cond_3
    invoke-direct {v0, v9}, Lcom/squareup/orderentry/FavoritePageView;->getViewPosition(Landroid/view/View;)Landroid/graphics/Point;

    move-result-object v4

    .line 703
    iget v10, v4, Landroid/graphics/Point;->x:I

    .line 704
    iget v4, v4, Landroid/graphics/Point;->y:I

    mul-int v10, v10, v5

    add-int/2addr v10, v1

    mul-int v4, v4, v7

    add-int/2addr v4, v2

    .line 707
    iget v11, v0, Lcom/squareup/orderentry/FavoritePageView;->tileWidth:I

    add-int/2addr v11, v10

    .line 709
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    add-int/2addr v12, v4

    .line 710
    invoke-virtual {v9, v10, v4, v11, v12}, Landroid/view/View;->layout(IIII)V

    :goto_1
    add-int/lit8 v8, v8, 0x1

    const/4 v4, 0x2

    goto/16 :goto_0

    :cond_4
    return-void
.end method

.method protected onMeasure(II)V
    .locals 17

    move-object/from16 v0, p0

    .line 602
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 603
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    const/4 v3, 0x0

    if-eqz v1, :cond_8

    if-eqz v2, :cond_8

    .line 607
    iget v4, v0, Lcom/squareup/orderentry/FavoritePageView;->columnCount:I

    if-eqz v4, :cond_8

    iget v4, v0, Lcom/squareup/orderentry/FavoritePageView;->rowCount:I

    if-nez v4, :cond_0

    goto/16 :goto_3

    .line 615
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/orderentry/FavoritePageView;->getPaddingLeft()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Lcom/squareup/orderentry/FavoritePageView;->getPaddingRight()I

    move-result v5

    add-int/2addr v4, v5

    iget v5, v0, Lcom/squareup/orderentry/FavoritePageView;->columnCount:I

    add-int/lit8 v6, v5, -0x1

    iget v7, v0, Lcom/squareup/orderentry/FavoritePageView;->innerPadding:I

    mul-int v6, v6, v7

    add-int/2addr v4, v6

    .line 616
    iget v6, v0, Lcom/squareup/orderentry/FavoritePageView;->outerPadding:I

    add-int/2addr v6, v6

    iget v8, v0, Lcom/squareup/orderentry/FavoritePageView;->rowCount:I

    add-int/lit8 v9, v8, -0x1

    mul-int v9, v9, v7

    add-int/2addr v6, v9

    sub-int v4, v1, v4

    .line 618
    div-int/2addr v4, v5

    iput v4, v0, Lcom/squareup/orderentry/FavoritePageView;->tileWidth:I

    sub-int v4, v2, v6

    .line 619
    div-int/2addr v4, v8

    iput v4, v0, Lcom/squareup/orderentry/FavoritePageView;->tileHeight:I

    .line 628
    iget v4, v0, Lcom/squareup/orderentry/FavoritePageView;->tileHeight:I

    iget v5, v0, Lcom/squareup/orderentry/FavoritePageView;->tileWidth:I

    const/4 v6, 0x2

    if-le v4, v5, :cond_1

    iget-object v4, v0, Lcom/squareup/orderentry/FavoritePageView;->device:Lcom/squareup/util/Device;

    invoke-interface {v4}, Lcom/squareup/util/Device;->isLandscapeLongTablet()Z

    move-result v4

    if-nez v4, :cond_1

    .line 629
    iget v4, v0, Lcom/squareup/orderentry/FavoritePageView;->tileHeight:I

    iget v5, v0, Lcom/squareup/orderentry/FavoritePageView;->tileWidth:I

    sub-int/2addr v4, v5

    .line 630
    iget v7, v0, Lcom/squareup/orderentry/FavoritePageView;->rowCount:I

    mul-int v4, v4, v7

    div-int/2addr v4, v6

    .line 631
    iget v7, v0, Lcom/squareup/orderentry/FavoritePageView;->outerPadding:I

    add-int v8, v7, v4

    add-int/2addr v7, v4

    .line 634
    iput v5, v0, Lcom/squareup/orderentry/FavoritePageView;->tileHeight:I

    goto :goto_0

    .line 638
    :cond_1
    iget v8, v0, Lcom/squareup/orderentry/FavoritePageView;->outerPadding:I

    move v7, v8

    .line 642
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/orderentry/FavoritePageView;->getPaddingLeft()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Lcom/squareup/orderentry/FavoritePageView;->getPaddingRight()I

    move-result v5

    invoke-virtual {v0, v4, v8, v5, v7}, Lcom/squareup/orderentry/FavoritePageView;->setPadding(IIII)V

    .line 644
    iget v4, v0, Lcom/squareup/orderentry/FavoritePageView;->tileWidth:I

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 645
    iget v7, v0, Lcom/squareup/orderentry/FavoritePageView;->tileHeight:I

    invoke-static {v7, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    .line 646
    iget v8, v0, Lcom/squareup/orderentry/FavoritePageView;->tileHeight:I

    iget v9, v0, Lcom/squareup/orderentry/FavoritePageView;->regularTileExtraHeight:I

    add-int/2addr v8, v9

    invoke-static {v8, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .line 647
    iget v9, v0, Lcom/squareup/orderentry/FavoritePageView;->tileHeight:I

    iget v10, v0, Lcom/squareup/orderentry/FavoritePageView;->stackedTileExtraHeight:I

    add-int/2addr v9, v10

    invoke-static {v9, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    .line 648
    iget v10, v0, Lcom/squareup/orderentry/FavoritePageView;->deleteButtonSize:I

    invoke-static {v10, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    .line 649
    iget v11, v0, Lcom/squareup/orderentry/FavoritePageView;->deleteButtonSize:I

    invoke-static {v11, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    .line 651
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/orderentry/FavoritePageView;->getChildCount()I

    move-result v12

    :goto_1
    if-ge v3, v12, :cond_7

    .line 653
    invoke-virtual {v0, v3}, Lcom/squareup/orderentry/FavoritePageView;->getChildAt(I)Landroid/view/View;

    move-result-object v13

    .line 654
    invoke-direct {v0, v13}, Lcom/squareup/orderentry/FavoritePageView;->getViewType(Landroid/view/View;)Lcom/squareup/orderentry/FavoritePageView$ViewType;

    move-result-object v14

    .line 655
    sget-object v15, Lcom/squareup/orderentry/FavoritePageView$11;->$SwitchMap$com$squareup$orderentry$FavoritePageView$ViewType:[I

    invoke-virtual {v14}, Lcom/squareup/orderentry/FavoritePageView$ViewType;->ordinal()I

    move-result v16

    aget v15, v15, v16

    const/4 v5, 0x1

    if-eq v15, v5, :cond_6

    if-eq v15, v6, :cond_5

    const/4 v5, 0x3

    if-eq v15, v5, :cond_4

    const/4 v5, 0x4

    if-eq v15, v5, :cond_3

    const/4 v5, 0x5

    if-ne v15, v5, :cond_2

    .line 670
    iget v5, v0, Lcom/squareup/orderentry/FavoritePageView;->columnCount:I

    sub-int/2addr v5, v6

    .line 672
    iget v14, v0, Lcom/squareup/orderentry/FavoritePageView;->tileWidth:I

    mul-int v14, v14, v5

    const/4 v15, 0x1

    sub-int/2addr v5, v15

    iget v15, v0, Lcom/squareup/orderentry/FavoritePageView;->innerPadding:I

    mul-int v5, v5, v15

    add-int/2addr v14, v5

    .line 674
    invoke-virtual {v13}, Landroid/view/View;->getPaddingLeft()I

    move-result v5

    add-int/2addr v14, v5

    .line 675
    invoke-virtual {v13}, Landroid/view/View;->getPaddingRight()I

    move-result v5

    add-int/2addr v14, v5

    const/high16 v5, 0x40000000    # 2.0f

    .line 672
    invoke-static {v14, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v14

    .line 676
    iget v15, v0, Lcom/squareup/orderentry/FavoritePageView;->tileHeight:I

    const/16 v16, 0x3

    mul-int/lit8 v15, v15, 0x3

    const/high16 v5, -0x80000000

    invoke-static {v15, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 677
    invoke-virtual {v13, v14, v5}, Landroid/view/View;->measure(II)V

    goto :goto_2

    .line 680
    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 666
    :cond_3
    invoke-virtual {v13, v10, v11}, Landroid/view/View;->measure(II)V

    goto :goto_2

    .line 663
    :cond_4
    invoke-virtual {v13, v4, v9}, Landroid/view/View;->measure(II)V

    goto :goto_2

    .line 660
    :cond_5
    invoke-virtual {v13, v4, v8}, Landroid/view/View;->measure(II)V

    goto :goto_2

    .line 657
    :cond_6
    invoke-virtual {v13, v4, v7}, Landroid/view/View;->measure(II)V

    :goto_2
    add-int/lit8 v3, v3, 0x1

    const/high16 v5, 0x40000000    # 2.0f

    goto :goto_1

    .line 683
    :cond_7
    invoke-virtual {v0, v1, v2}, Lcom/squareup/orderentry/FavoritePageView;->setMeasuredDimension(II)V

    return-void

    .line 608
    :cond_8
    :goto_3
    iput v3, v0, Lcom/squareup/orderentry/FavoritePageView;->tileWidth:I

    .line 609
    iput v3, v0, Lcom/squareup/orderentry/FavoritePageView;->tileHeight:I

    .line 610
    invoke-virtual {v0, v1, v2}, Lcom/squareup/orderentry/FavoritePageView;->setMeasuredDimension(II)V

    return-void
.end method

.method public requestLayoutsForTutorial()V
    .locals 2

    .line 204
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageView;->requestLayoutDisposable:Lio/reactivex/disposables/CompositeDisposable;

    sget-object v1, Lcom/squareup/tutorialv2/RequestLayoutTimeout;->FAST:Lcom/squareup/tutorialv2/RequestLayoutTimeout;

    invoke-static {p0, v1}, Lcom/squareup/tutorialv2/TutorialUtilities;->requestLayouts(Landroid/view/View;Lcom/squareup/tutorialv2/RequestLayoutTimeout;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method setTextTile(Z)V
    .locals 0

    .line 590
    iput-boolean p1, p0, Lcom/squareup/orderentry/FavoritePageView;->textTile:Z

    .line 591
    invoke-direct {p0}, Lcom/squareup/orderentry/FavoritePageView;->updatePaddings()V

    return-void
.end method

.method public setViews(II[Landroid/view/View;)V
    .locals 22

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    .line 121
    iput v1, v0, Lcom/squareup/orderentry/FavoritePageView;->rowCount:I

    .line 122
    iput v2, v0, Lcom/squareup/orderentry/FavoritePageView;->columnCount:I

    mul-int v1, v1, v2

    .line 123
    array-length v4, v3

    if-ne v1, v4, :cond_6

    .line 128
    array-length v1, v3

    new-array v1, v1, [Z

    .line 130
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/orderentry/FavoritePageView;->getChildCount()I

    move-result v4

    const-wide/16 v5, 0xc8

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v8, 0x1

    if-lez v4, :cond_2

    .line 131
    iget-object v4, v0, Lcom/squareup/orderentry/FavoritePageView;->tilesByPosition:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Map$Entry;

    .line 132
    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/graphics/Point;

    .line 133
    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/View;

    .line 134
    iget v11, v10, Landroid/graphics/Point;->y:I

    mul-int v11, v11, v2

    iget v10, v10, Landroid/graphics/Point;->x:I

    add-int/2addr v11, v10

    .line 135
    aget-object v10, v3, v11

    .line 136
    invoke-direct {v0, v9}, Lcom/squareup/orderentry/FavoritePageView;->isEmptyTileView(Landroid/view/View;)Z

    move-result v12

    if-nez v12, :cond_0

    .line 137
    invoke-direct {v0, v10}, Lcom/squareup/orderentry/FavoritePageView;->isEmptyTileView(Landroid/view/View;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 139
    new-instance v10, Landroid/view/animation/ScaleAnimation;

    const/high16 v14, 0x3f800000    # 1.0f

    const/4 v15, 0x0

    const/high16 v16, 0x3f800000    # 1.0f

    const/16 v17, 0x0

    const/16 v18, 0x1

    const/high16 v19, 0x3f000000    # 0.5f

    const/16 v20, 0x1

    const/high16 v21, 0x3f000000    # 0.5f

    move-object v13, v10

    invoke-direct/range {v13 .. v21}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 141
    new-instance v13, Landroid/view/animation/AlphaAnimation;

    const/4 v14, 0x0

    invoke-direct {v13, v7, v14}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 142
    new-instance v14, Landroid/view/animation/AnimationSet;

    invoke-direct {v14, v8}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 143
    invoke-virtual {v14, v10}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 144
    invoke-virtual {v14, v13}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 145
    invoke-virtual {v14, v5, v6}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 146
    invoke-virtual {v9, v14}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 148
    :cond_0
    aput-boolean v12, v1, v11

    goto :goto_0

    .line 150
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/orderentry/FavoritePageView;->removeAllViews()V

    .line 151
    iget-object v4, v0, Lcom/squareup/orderentry/FavoritePageView;->tilesByPosition:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    .line 152
    iget-object v4, v0, Lcom/squareup/orderentry/FavoritePageView;->deleteButtonsByPosition:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    const/4 v4, 0x0

    .line 153
    iput-object v4, v0, Lcom/squareup/orderentry/FavoritePageView;->tooltipView:Landroid/view/View;

    :cond_2
    const/4 v4, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 157
    :goto_1
    array-length v10, v3

    if-ge v8, v10, :cond_4

    .line 158
    div-int v10, v8, v2

    .line 159
    rem-int v11, v8, v2

    .line 160
    aget-object v12, v3, v8

    .line 161
    new-instance v13, Landroid/graphics/Point;

    invoke-direct {v13, v11, v10}, Landroid/graphics/Point;-><init>(II)V

    .line 162
    sget v10, Lcom/squareup/orderentry/R$id;->favorite_view_position_tag:I

    invoke-virtual {v12, v10, v13}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 167
    invoke-virtual {v12, v4}, Landroid/view/View;->setSaveFromParentEnabled(Z)V

    .line 168
    iget-object v10, v0, Lcom/squareup/orderentry/FavoritePageView;->tilesByPosition:Ljava/util/HashMap;

    invoke-virtual {v10, v13, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    invoke-virtual {v0, v12}, Lcom/squareup/orderentry/FavoritePageView;->addView(Landroid/view/View;)V

    .line 170
    invoke-direct {v0, v12}, Lcom/squareup/orderentry/FavoritePageView;->isEmptyTileView(Landroid/view/View;)Z

    move-result v10

    and-int/2addr v9, v10

    .line 172
    aget-boolean v11, v1, v8

    if-eqz v11, :cond_3

    if-nez v10, :cond_3

    const v10, 0x3c23d70a    # 0.01f

    .line 176
    invoke-virtual {v12, v10}, Landroid/view/View;->setScaleX(F)V

    .line 177
    invoke-virtual {v12, v10}, Landroid/view/View;->setScaleY(F)V

    .line 178
    invoke-virtual {v12}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v10

    .line 179
    invoke-virtual {v10, v7}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v10

    .line 180
    invoke-virtual {v10, v7}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v10

    new-instance v11, Landroid/view/animation/OvershootInterpolator;

    invoke-direct {v11}, Landroid/view/animation/OvershootInterpolator;-><init>()V

    .line 181
    invoke-virtual {v10, v11}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v10

    .line 182
    invoke-virtual {v10, v5, v6}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 184
    :cond_3
    sget v10, Lcom/squareup/orderentry/R$layout;->favorite_tile_delete:I

    invoke-static {v10, v0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    .line 185
    new-instance v11, Lcom/squareup/orderentry/FavoritePageView$1;

    invoke-direct {v11, v0}, Lcom/squareup/orderentry/FavoritePageView$1;-><init>(Lcom/squareup/orderentry/FavoritePageView;)V

    invoke-virtual {v10, v11}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 191
    sget v11, Lcom/squareup/orderentry/R$id;->favorite_view_position_tag:I

    invoke-virtual {v10, v11, v13}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 192
    sget v11, Lcom/squareup/orderentry/R$id;->favorite_view_type_tag:I

    sget-object v12, Lcom/squareup/orderentry/FavoritePageView$ViewType;->DELETE:Lcom/squareup/orderentry/FavoritePageView$ViewType;

    invoke-virtual {v10, v11, v12}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 193
    iget-object v11, v0, Lcom/squareup/orderentry/FavoritePageView;->deleteButtonsByPosition:Ljava/util/HashMap;

    invoke-virtual {v11, v13, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    invoke-virtual {v0, v10}, Lcom/squareup/orderentry/FavoritePageView;->addView(Landroid/view/View;)V

    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_4
    if-eqz v9, :cond_5

    .line 197
    sget v1, Lcom/squareup/orderentry/R$layout;->favorite_page_tooltip:I

    invoke-static {v1, v0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageView;->tooltipView:Landroid/view/View;

    .line 198
    iget-object v1, v0, Lcom/squareup/orderentry/FavoritePageView;->tooltipView:Landroid/view/View;

    sget v2, Lcom/squareup/orderentry/R$id;->favorite_view_type_tag:I

    sget-object v3, Lcom/squareup/orderentry/FavoritePageView$ViewType;->TOOLTIP:Lcom/squareup/orderentry/FavoritePageView$ViewType;

    invoke-virtual {v1, v2, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 199
    iget-object v1, v0, Lcom/squareup/orderentry/FavoritePageView;->tooltipView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/FavoritePageView;->addView(Landroid/view/View;)V

    :cond_5
    return-void

    .line 124
    :cond_6
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Received "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length v3, v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " views, expected "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public showEditMode()V
    .locals 16

    move-object/from16 v0, p0

    .line 208
    iget v1, v0, Lcom/squareup/orderentry/FavoritePageView;->lastDownX:F

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v1, v1, v4

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 209
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/orderentry/FavoritePageView;->getWidth()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Lcom/squareup/orderentry/FavoritePageView;->getWidth()I

    move-result v5

    mul-int v4, v4, v5

    invoke-virtual/range {p0 .. p0}, Lcom/squareup/orderentry/FavoritePageView;->getHeight()I

    move-result v5

    invoke-virtual/range {p0 .. p0}, Lcom/squareup/orderentry/FavoritePageView;->getHeight()I

    move-result v6

    mul-int v5, v5, v6

    add-int/2addr v4, v5

    int-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-int v4, v4

    .line 211
    iget-object v5, v0, Lcom/squareup/orderentry/FavoritePageView;->tilesByPosition:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    const-wide/16 v7, 0x96

    const-wide v9, 0x4072c00000000000L    # 300.0

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v12, 0x2

    if-eqz v6, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    .line 212
    invoke-direct {v0, v6}, Lcom/squareup/orderentry/FavoritePageView;->isEmptyTileView(Landroid/view/View;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 213
    invoke-virtual {v0, v6, v2}, Lcom/squareup/orderentry/FavoritePageView;->updateChildEnabled(Landroid/view/View;Z)V

    .line 214
    check-cast v6, Lcom/squareup/orderentry/EmptyTile;

    if-eqz v1, :cond_2

    .line 216
    invoke-virtual {v6}, Lcom/squareup/orderentry/EmptyTile;->getLeft()I

    move-result v13

    invoke-virtual {v6}, Lcom/squareup/orderentry/EmptyTile;->getWidth()I

    move-result v14

    div-int/2addr v14, v12

    add-int/2addr v13, v14

    .line 217
    invoke-virtual {v6}, Lcom/squareup/orderentry/EmptyTile;->getTop()I

    move-result v14

    invoke-virtual {v6}, Lcom/squareup/orderentry/EmptyTile;->getHeight()I

    move-result v15

    div-int/2addr v15, v12

    add-int/2addr v14, v15

    int-to-float v13, v13

    .line 219
    iget v15, v0, Lcom/squareup/orderentry/FavoritePageView;->lastDownX:F

    sub-float/2addr v13, v15

    int-to-float v14, v14

    .line 220
    iget v15, v0, Lcom/squareup/orderentry/FavoritePageView;->lastDownY:F

    sub-float/2addr v14, v15

    mul-float v13, v13, v13

    mul-float v14, v14, v14

    add-float/2addr v13, v14

    float-to-double v13, v13

    .line 222
    invoke-static {v13, v14}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v13

    mul-double v13, v13, v9

    int-to-double v9, v4

    div-double/2addr v13, v9

    double-to-long v9, v13

    new-array v12, v12, [F

    .line 227
    invoke-virtual {v6}, Lcom/squareup/orderentry/EmptyTile;->getPlusButtonSize()F

    move-result v13

    aput v13, v12, v3

    aput v11, v12, v2

    const-string v11, "plusButtonSize"

    invoke-static {v6, v11, v12}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    .line 228
    invoke-virtual {v6, v7, v8}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 229
    invoke-virtual {v6, v9, v10}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 230
    invoke-virtual {v6}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_1

    .line 232
    :cond_2
    invoke-virtual {v6, v11}, Lcom/squareup/orderentry/EmptyTile;->setPlusButtonSize(F)V

    goto :goto_1

    .line 237
    :cond_3
    iget-object v2, v0, Lcom/squareup/orderentry/FavoritePageView;->deleteButtonsByPosition:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    const/16 v6, 0x8

    if-eqz v5, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 238
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/graphics/Point;

    .line 239
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    .line 240
    iget-object v14, v0, Lcom/squareup/orderentry/FavoritePageView;->tilesByPosition:Ljava/util/HashMap;

    invoke-virtual {v14, v13}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/view/View;

    .line 242
    sget v14, Lcom/squareup/orderentry/R$id;->favorite_view_type_tag:I

    invoke-virtual {v13, v14}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/squareup/orderentry/FavoritePageView$ViewType;

    .line 244
    sget-object v14, Lcom/squareup/orderentry/FavoritePageView$ViewType;->TILE_EMPTY:Lcom/squareup/orderentry/FavoritePageView$ViewType;

    if-ne v13, v14, :cond_4

    .line 245
    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 247
    :cond_4
    invoke-virtual {v5}, Landroid/view/View;->clearAnimation()V

    .line 248
    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    if-eqz v1, :cond_5

    .line 250
    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v6

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v13

    div-int/2addr v13, v12

    add-int/2addr v6, v13

    .line 251
    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v13

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v14

    div-int/2addr v14, v12

    add-int/2addr v13, v14

    int-to-float v6, v6

    .line 253
    iget v14, v0, Lcom/squareup/orderentry/FavoritePageView;->lastDownX:F

    sub-float/2addr v6, v14

    int-to-float v13, v13

    .line 254
    iget v14, v0, Lcom/squareup/orderentry/FavoritePageView;->lastDownY:F

    sub-float/2addr v13, v14

    mul-float v6, v6, v6

    mul-float v13, v13, v13

    add-float/2addr v6, v13

    float-to-double v13, v6

    .line 256
    invoke-static {v13, v14}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v13

    mul-double v13, v13, v9

    int-to-double v9, v4

    div-double/2addr v13, v9

    double-to-long v9, v13

    const/4 v6, 0x0

    .line 259
    invoke-virtual {v5, v6}, Landroid/view/View;->setScaleX(F)V

    .line 260
    invoke-virtual {v5, v6}, Landroid/view/View;->setScaleY(F)V

    .line 261
    invoke-virtual {v5}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    .line 262
    invoke-virtual {v5, v7, v8}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    .line 263
    invoke-virtual {v5, v11}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    .line 264
    invoke-virtual {v5, v11}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    .line 265
    invoke-virtual {v5, v9, v10}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    :cond_5
    :goto_3
    const-wide v9, 0x4072c00000000000L    # 300.0

    goto/16 :goto_2

    .line 269
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/orderentry/FavoritePageView;->cancelPressToEditMode()V

    .line 270
    iget-object v2, v0, Lcom/squareup/orderentry/FavoritePageView;->tooltipView:Landroid/view/View;

    if-eqz v2, :cond_8

    if-eqz v1, :cond_7

    .line 272
    new-instance v1, Landroid/view/animation/ScaleAnimation;

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v11, 0x0

    const/4 v12, 0x1

    const/high16 v13, 0x3f000000    # 0.5f

    const/4 v14, 0x1

    const/high16 v15, 0x3f000000    # 0.5f

    move-object v7, v1

    invoke-direct/range {v7 .. v15}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    const-wide/16 v2, 0xc8

    .line 274
    invoke-virtual {v1, v2, v3}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 275
    new-instance v2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 276
    iget-object v2, v0, Lcom/squareup/orderentry/FavoritePageView;->tooltipView:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 278
    :cond_7
    iget-object v1, v0, Lcom/squareup/orderentry/FavoritePageView;->tooltipView:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_8
    return-void
.end method

.method public showSaleMode(Z)V
    .locals 16

    move-object/from16 v0, p0

    .line 283
    iget-object v1, v0, Lcom/squareup/orderentry/FavoritePageView;->tilesByPosition:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const-wide/16 v3, 0x96

    const/4 v5, 0x0

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 284
    invoke-direct {v0, v2}, Lcom/squareup/orderentry/FavoritePageView;->isEmptyTileView(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 285
    invoke-virtual {v0, v2, v5}, Lcom/squareup/orderentry/FavoritePageView;->updateChildEnabled(Landroid/view/View;Z)V

    .line 286
    check-cast v2, Lcom/squareup/orderentry/EmptyTile;

    const/4 v6, 0x0

    if-eqz p1, :cond_1

    const/4 v7, 0x2

    new-array v7, v7, [F

    .line 289
    invoke-virtual {v2}, Lcom/squareup/orderentry/EmptyTile;->getPlusButtonSize()F

    move-result v8

    aput v8, v7, v5

    const/4 v5, 0x1

    aput v6, v7, v5

    const-string v5, "plusButtonSize"

    invoke-static {v2, v5, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 290
    invoke-virtual {v2, v3, v4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 291
    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0

    .line 293
    :cond_1
    invoke-virtual {v2, v6}, Lcom/squareup/orderentry/EmptyTile;->setPlusButtonSize(F)V

    goto :goto_0

    .line 298
    :cond_2
    iget-object v1, v0, Lcom/squareup/orderentry/FavoritePageView;->deleteButtonsByPosition:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 299
    invoke-virtual {v2}, Landroid/view/View;->clearAnimation()V

    .line 300
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v6

    if-nez v6, :cond_3

    if-eqz p1, :cond_4

    .line 303
    new-instance v6, Landroid/view/animation/ScaleAnimation;

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v11, 0x0

    const/4 v12, 0x1

    const/high16 v13, 0x3f000000    # 0.5f

    const/4 v14, 0x1

    const/high16 v15, 0x3f000000    # 0.5f

    move-object v7, v6

    invoke-direct/range {v7 .. v15}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 305
    invoke-virtual {v6, v3, v4}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 306
    invoke-virtual {v2, v6}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_4
    const/16 v6, 0x8

    .line 308
    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 311
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/orderentry/FavoritePageView;->cancelPressToEditMode()V

    .line 313
    iget-object v1, v0, Lcom/squareup/orderentry/FavoritePageView;->tooltipView:Landroid/view/View;

    if-eqz v1, :cond_6

    .line 314
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    if-eqz p1, :cond_6

    .line 316
    new-instance v1, Landroid/view/animation/ScaleAnimation;

    const/4 v7, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v11, 0x1

    const/high16 v12, 0x3f000000    # 0.5f

    const/4 v13, 0x1

    const/high16 v14, 0x3f000000    # 0.5f

    move-object v6, v1

    invoke-direct/range {v6 .. v14}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    const-wide/16 v2, 0xc8

    .line 318
    invoke-virtual {v1, v2, v3}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 319
    new-instance v2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 320
    iget-object v2, v0, Lcom/squareup/orderentry/FavoritePageView;->tooltipView:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_6
    return-void
.end method

.method public updateChildEnabled(IIZ)V
    .locals 2

    .line 757
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageView;->tilesByPosition:Ljava/util/HashMap;

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, p1, p2}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    .line 758
    invoke-virtual {p0, p1, p3}, Lcom/squareup/orderentry/FavoritePageView;->updateChildEnabled(Landroid/view/View;Z)V

    return-void
.end method

.method protected updateChildEnabled(Landroid/view/View;Z)V
    .locals 1

    .line 762
    instance-of v0, p1, Lcom/squareup/orderentry/HasEnabledLook;

    if-eqz v0, :cond_0

    .line 763
    check-cast p1, Lcom/squareup/orderentry/HasEnabledLook;

    .line 764
    invoke-interface {p1, p2}, Lcom/squareup/orderentry/HasEnabledLook;->setLooksEnabled(Z)V

    goto :goto_0

    .line 766
    :cond_0
    invoke-virtual {p1, p2}, Landroid/view/View;->setEnabled(Z)V

    :goto_0
    return-void
.end method
