.class public abstract Lcom/squareup/orderentry/OrderEntryAppletModule;
.super Ljava/lang/Object;
.source "OrderEntryAppletModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/orderentry/OrderEntryCommonModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideHomeScreenSelector(Lcom/squareup/ui/main/OrderEntryHomeScreenSelector;)Lcom/squareup/ui/main/HomeScreenSelector;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideOrderEntryAppletGateway(Lcom/squareup/orderentry/RealOrderEntryAppletGateway$AsNonHomeApplet;)Lcom/squareup/orderentry/OrderEntryAppletGateway;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
