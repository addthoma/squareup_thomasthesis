.class public Lcom/squareup/orderentry/CartDropDownView;
.super Lcom/squareup/ui/DropDownContainer;
.source "CartDropDownView.java"


# instance fields
.field presenter:Lcom/squareup/orderentry/CartDropDownPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 16
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/DropDownContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected inject()V
    .locals 2

    .line 20
    invoke-virtual {p0}, Lcom/squareup/orderentry/CartDropDownView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;

    invoke-static {v0, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;

    invoke-interface {v0, p0}, Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;->inject(Lcom/squareup/orderentry/CartDropDownView;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/orderentry/CartDropDownView;->presenter:Lcom/squareup/orderentry/CartDropDownPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/CartDropDownPresenter;->dropView(Ljava/lang/Object;)V

    .line 30
    invoke-super {p0}, Lcom/squareup/ui/DropDownContainer;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 24
    invoke-super {p0}, Lcom/squareup/ui/DropDownContainer;->onFinishInflate()V

    .line 25
    iget-object v0, p0, Lcom/squareup/orderentry/CartDropDownView;->presenter:Lcom/squareup/orderentry/CartDropDownPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/CartDropDownPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method
