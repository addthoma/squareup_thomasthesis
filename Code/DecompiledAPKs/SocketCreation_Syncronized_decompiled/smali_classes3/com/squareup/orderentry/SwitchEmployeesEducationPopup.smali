.class public Lcom/squareup/orderentry/SwitchEmployeesEducationPopup;
.super Lcom/squareup/flowlegacy/DialogPopup;
.source "SwitchEmployeesEducationPopup.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/flowlegacy/DialogPopup<",
        "Lcom/squareup/ui/Showing;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final device:Lcom/squareup/util/Device;

.field private final message:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/squareup/util/Device;Ljava/lang/String;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1}, Lcom/squareup/flowlegacy/DialogPopup;-><init>(Landroid/content/Context;)V

    .line 37
    iput-object p2, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPopup;->device:Lcom/squareup/util/Device;

    .line 38
    iput-object p3, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPopup;->message:Ljava/lang/String;

    return-void
.end method

.method private getBackgroundDrawable()Landroid/graphics/drawable/Drawable;
    .locals 2

    .line 102
    invoke-virtual {p0}, Lcom/squareup/orderentry/SwitchEmployeesEducationPopup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 103
    iget-object v1, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPopup;->device:Lcom/squareup/util/Device;

    .line 104
    invoke-interface {v1}, Lcom/squareup/util/Device;->isLandscapeLongTablet()Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Lcom/squareup/orderentry/common/R$drawable;->switch_employee_tooltip_left_down_arrow:I

    goto :goto_0

    :cond_0
    sget v1, Lcom/squareup/orderentry/common/R$drawable;->switch_employee_tooltip_right_up_arrow:I

    .line 106
    :goto_0
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$createDialog$0(Lcom/squareup/mortar/PopupPresenter;Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x0

    .line 96
    invoke-virtual {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->onDismissed(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic createDialog(Landroid/os/Parcelable;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/ui/Showing;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/orderentry/SwitchEmployeesEducationPopup;->createDialog(Lcom/squareup/ui/Showing;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;

    move-result-object p1

    return-object p1
.end method

.method protected createDialog(Lcom/squareup/ui/Showing;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/Showing;",
            "Z",
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/ui/Showing;",
            "Ljava/lang/Void;",
            ">;)",
            "Landroid/app/Dialog;"
        }
    .end annotation

    .line 43
    invoke-virtual {p0}, Lcom/squareup/orderentry/SwitchEmployeesEducationPopup;->getContext()Landroid/content/Context;

    move-result-object p1

    sget p2, Lcom/squareup/orderentry/common/R$layout;->education_popup:I

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 44
    invoke-direct {p0}, Lcom/squareup/orderentry/SwitchEmployeesEducationPopup;->getBackgroundDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 46
    sget p2, Lcom/squareup/orderentry/common/R$id;->title:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/widgets/ScalingTextView;

    .line 47
    sget v0, Lcom/squareup/orderentry/common/R$string;->switch_employee_tooltip_title:I

    invoke-virtual {p2, v0}, Lcom/squareup/widgets/ScalingTextView;->setText(I)V

    .line 49
    sget p2, Lcom/squareup/orderentry/common/R$id;->message:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/widgets/MessageView;

    .line 50
    iget-object v0, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPopup;->message:Ljava/lang/String;

    invoke-virtual {p2, v0}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    sget p2, Lcom/squareup/orderentry/common/R$id;->hide_button:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/glyph/SquareGlyphView;

    .line 53
    new-instance v0, Lcom/squareup/orderentry/SwitchEmployeesEducationPopup$1;

    invoke-direct {v0, p0, p3}, Lcom/squareup/orderentry/SwitchEmployeesEducationPopup$1;-><init>(Lcom/squareup/orderentry/SwitchEmployeesEducationPopup;Lcom/squareup/mortar/PopupPresenter;)V

    invoke-virtual {p2, v0}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/SwitchEmployeesEducationPopup$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/orderentry/SwitchEmployeesEducationPopup$2;-><init>(Lcom/squareup/orderentry/SwitchEmployeesEducationPopup;Landroid/view/View;Lcom/squareup/glyph/SquareGlyphView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 73
    new-instance p2, Lcom/squareup/dialog/GlassDialog;

    invoke-virtual {p0}, Lcom/squareup/orderentry/SwitchEmployeesEducationPopup;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/orderentry/common/R$style;->Theme_Dialog_Tooltip:I

    invoke-direct {p2, v0, v1}, Lcom/squareup/dialog/GlassDialog;-><init>(Landroid/content/Context;I)V

    .line 74
    invoke-static {}, Lcom/squareup/util/Dialogs;->ignoresSearchKeyListener()Landroid/content/DialogInterface$OnKeyListener;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 75
    invoke-virtual {p2, p1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    const/4 p1, 0x1

    .line 76
    invoke-virtual {p2, p1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 78
    invoke-virtual {p2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 79
    invoke-virtual {p0}, Lcom/squareup/orderentry/SwitchEmployeesEducationPopup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 80
    sget v2, Lcom/squareup/orderentry/common/R$dimen;->switch_employee_tool_tip_dialog_top_nav_offset:I

    .line 81
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 83
    iget-object v3, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPopup;->device:Lcom/squareup/util/Device;

    invoke-interface {v3}, Lcom/squareup/util/Device;->isLandscapeLongTablet()Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v3, 0x53

    .line 84
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 85
    sget v3, Lcom/squareup/orderentry/common/R$dimen;->nav_column_landscape_width:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sub-int/2addr v1, v2

    .line 86
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 87
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    goto :goto_0

    :cond_0
    const/16 v3, 0x35

    .line 89
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 90
    sget v3, Lcom/squareup/marin/R$dimen;->marin_min_height:I

    .line 91
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 94
    :goto_0
    invoke-virtual {p2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 95
    invoke-virtual {p2, p1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 96
    new-instance p1, Lcom/squareup/orderentry/-$$Lambda$SwitchEmployeesEducationPopup$6Z4JD8trIagZiail4N825_Tx4XE;

    invoke-direct {p1, p3}, Lcom/squareup/orderentry/-$$Lambda$SwitchEmployeesEducationPopup$6Z4JD8trIagZiail4N825_Tx4XE;-><init>(Lcom/squareup/mortar/PopupPresenter;)V

    invoke-virtual {p2, p1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    return-object p2
.end method
