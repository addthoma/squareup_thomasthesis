.class public abstract Lcom/squareup/orderentry/NavigationBarView;
.super Landroid/widget/LinearLayout;
.source "NavigationBarView.java"


# static fields
.field private static final SCROLL_FUDGE_FACTOR:I = 0x5


# instance fields
.field private borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

.field private content:Landroid/view/ViewGroup;

.field private contentScrollview:Landroid/widget/ScrollView;

.field private final navigationTabs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private needsRightBorder:Z


# direct methods
.method protected constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 47
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/orderentry/NavigationBarView;->navigationTabs:Ljava/util/List;

    return-void
.end method

.method private makeNarrowTabView(Lcom/squareup/orderentry/pages/OrderEntryPage;)Landroid/view/View;
    .locals 4

    .line 170
    invoke-virtual {p1}, Lcom/squareup/orderentry/pages/OrderEntryPage;->hasGlyph()Z

    move-result v0

    if-nez v0, :cond_1

    .line 171
    invoke-virtual {p1}, Lcom/squareup/orderentry/pages/OrderEntryPage;->getName()Ljava/lang/String;

    move-result-object p1

    .line 172
    invoke-virtual {p0}, Lcom/squareup/orderentry/NavigationBarView;->getNarrowTabTextLayout()I

    move-result v0

    .line 173
    invoke-static {v0, p0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/ShorteningTextView;

    const/4 v1, 0x0

    .line 174
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/ShorteningTextView;->setSingleLine(Z)V

    const/4 v2, 0x1

    .line 175
    invoke-virtual {v0, v2}, Lcom/squareup/widgets/ShorteningTextView;->setMaxLines(I)V

    .line 176
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ShorteningTextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    invoke-virtual {v0, v2}, Lcom/squareup/widgets/ShorteningTextView;->setShowShortTextWhenEllipsized(Z)V

    if-eqz p1, :cond_0

    .line 178
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x2

    if-lt v2, v3, :cond_0

    .line 179
    invoke-virtual {p1, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ShorteningTextView;->setShortText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 181
    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ShorteningTextView;->setShortText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 185
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/orderentry/NavigationBarView;->getNarrowTabGlyphLayout()I

    move-result v0

    .line 186
    invoke-static {v0, p0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    .line 187
    invoke-virtual {p1}, Lcom/squareup/orderentry/pages/OrderEntryPage;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    :goto_0
    return-object v0
.end method

.method private makeWideTabView(Lcom/squareup/orderentry/pages/OrderEntryPage;)Landroid/view/View;
    .locals 1

    .line 162
    invoke-virtual {p0}, Lcom/squareup/orderentry/NavigationBarView;->getWideTabLayout()I

    move-result v0

    .line 163
    invoke-static {v0, p0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 164
    invoke-virtual {p1}, Lcom/squareup/orderentry/pages/OrderEntryPage;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method private shouldScroll()Z
    .locals 2

    .line 111
    iget-object v0, p0, Lcom/squareup/orderentry/NavigationBarView;->contentScrollview:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/orderentry/NavigationBarView;->content:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v1

    add-int/lit8 v1, v1, -0x5

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .line 79
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/orderentry/NavigationBarView;->borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/squareup/orderentry/NavigationBarView;->shouldScroll()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 82
    iget-object v0, p0, Lcom/squareup/orderentry/NavigationBarView;->borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/BorderPainter;->clearBorders()V

    .line 83
    iget-object v0, p0, Lcom/squareup/orderentry/NavigationBarView;->borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/BorderPainter;->addBorder(I)V

    .line 84
    iget-boolean v0, p0, Lcom/squareup/orderentry/NavigationBarView;->needsRightBorder:Z

    const/4 v1, 0x4

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/squareup/orderentry/NavigationBarView;->borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/BorderPainter;->addBorder(I)V

    .line 88
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/orderentry/NavigationBarView;->getChildCount()I

    move-result v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_2

    add-int/lit8 v3, v0, -0x1

    if-ne v2, v3, :cond_1

    .line 91
    iget-object v3, p0, Lcom/squareup/orderentry/NavigationBarView;->borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

    invoke-virtual {v3}, Lcom/squareup/marin/widgets/BorderPainter;->clearBorders()V

    .line 92
    iget-boolean v3, p0, Lcom/squareup/orderentry/NavigationBarView;->needsRightBorder:Z

    if-eqz v3, :cond_2

    .line 93
    iget-object v3, p0, Lcom/squareup/orderentry/NavigationBarView;->borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

    invoke-virtual {v3, v1}, Lcom/squareup/marin/widgets/BorderPainter;->addBorder(I)V

    .line 98
    :cond_1
    iget-object v3, p0, Lcom/squareup/orderentry/NavigationBarView;->borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

    invoke-virtual {p0, v2}, Lcom/squareup/orderentry/NavigationBarView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, p1, v4}, Lcom/squareup/marin/widgets/BorderPainter;->drawChildBorders(Landroid/graphics/Canvas;Landroid/view/View;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method protected abstract getNarrowTabGlyphLayout()I
.end method

.method protected abstract getNarrowTabTextLayout()I
.end method

.method protected abstract getPresenter()Lcom/squareup/orderentry/NavigationBarAbstractPresenter;
.end method

.method protected abstract getWideTabLayout()I
.end method

.method protected abstract isEditor()Z
.end method

.method public synthetic lambda$onFinishInflate$0$NavigationBarView(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 0

    .line 67
    invoke-direct {p0}, Lcom/squareup/orderentry/NavigationBarView;->shouldScroll()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method public synthetic lambda$updateTabs$1$NavigationBarView(Lcom/squareup/orderentry/pages/OrderEntryPage;Landroid/view/View;)Z
    .locals 0

    .line 138
    invoke-virtual {p0}, Lcom/squareup/orderentry/NavigationBarView;->getPresenter()Lcom/squareup/orderentry/NavigationBarAbstractPresenter;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->tabLongClicked(Lcom/squareup/orderentry/pages/OrderEntryPage;)V

    const/4 p1, 0x1

    return p1
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 74
    invoke-virtual {p0}, Lcom/squareup/orderentry/NavigationBarView;->getPresenter()Lcom/squareup/orderentry/NavigationBarAbstractPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->dropView(Ljava/lang/Object;)V

    .line 75
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 61
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 62
    sget v0, Lcom/squareup/orderentry/R$id;->nav_bar_content:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/orderentry/NavigationBarView;->content:Landroid/view/ViewGroup;

    .line 63
    iget-object v0, p0, Lcom/squareup/orderentry/NavigationBarView;->content:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    .line 64
    iput-object p0, p0, Lcom/squareup/orderentry/NavigationBarView;->content:Landroid/view/ViewGroup;

    goto :goto_0

    .line 66
    :cond_0
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/squareup/orderentry/NavigationBarView;->contentScrollview:Landroid/widget/ScrollView;

    .line 67
    iget-object v0, p0, Lcom/squareup/orderentry/NavigationBarView;->contentScrollview:Landroid/widget/ScrollView;

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$NavigationBarView$qFrp_FOPHZew3Ay6FcIwiyQOaD4;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$NavigationBarView$qFrp_FOPHZew3Ay6FcIwiyQOaD4;-><init>(Lcom/squareup/orderentry/NavigationBarView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 68
    new-instance v0, Lcom/squareup/marin/widgets/BorderPainter;

    sget v1, Lcom/squareup/marin/R$dimen;->marin_divider_width_1px:I

    invoke-direct {v0, p0, v1}, Lcom/squareup/marin/widgets/BorderPainter;-><init>(Landroid/view/View;I)V

    iput-object v0, p0, Lcom/squareup/orderentry/NavigationBarView;->borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

    .line 70
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/orderentry/NavigationBarView;->getPresenter()Lcom/squareup/orderentry/NavigationBarAbstractPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    .line 104
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 105
    iget-object p1, p0, Lcom/squareup/orderentry/NavigationBarView;->contentScrollview:Landroid/widget/ScrollView;

    if-eqz p1, :cond_0

    .line 106
    invoke-direct {p0}, Lcom/squareup/orderentry/NavigationBarView;->shouldScroll()Z

    move-result p2

    invoke-virtual {p1, p2}, Landroid/widget/ScrollView;->setVerticalScrollBarEnabled(Z)V

    :cond_0
    return-void
.end method

.method public updateSelectedTab(Lcom/squareup/orderentry/pages/OrderEntryPageKey;)V
    .locals 3

    .line 194
    iget-boolean v0, p1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->isFavoritesGrid:Z

    iput-boolean v0, p0, Lcom/squareup/orderentry/NavigationBarView;->needsRightBorder:Z

    .line 195
    iget-object v0, p0, Lcom/squareup/orderentry/NavigationBarView;->navigationTabs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 196
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0

    .line 200
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/orderentry/NavigationBarView;->invalidate()V

    return-void
.end method

.method updateTabs(Lcom/squareup/orderentry/pages/OrderEntryPageList;)V
    .locals 5

    .line 115
    iget-object v0, p0, Lcom/squareup/orderentry/NavigationBarView;->navigationTabs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 116
    iget-object v2, p0, Lcom/squareup/orderentry/NavigationBarView;->content:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 120
    iget-object v1, p0, Lcom/squareup/orderentry/NavigationBarView;->content:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 121
    iget-object v2, p0, Lcom/squareup/orderentry/NavigationBarView;->content:Landroid/view/ViewGroup;

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v2, v2, Lcom/squareup/ui/permissions/EmployeeLockButton;

    if-eqz v2, :cond_1

    .line 122
    iget-object v0, p0, Lcom/squareup/orderentry/NavigationBarView;->content:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 123
    iget-object v1, p0, Lcom/squareup/orderentry/NavigationBarView;->content:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 126
    :cond_1
    iget-object v1, p0, Lcom/squareup/orderentry/NavigationBarView;->navigationTabs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    const/4 v1, 0x0

    .line 127
    :goto_1
    invoke-virtual {p1}, Lcom/squareup/orderentry/pages/OrderEntryPageList;->tabCount()I

    move-result v2

    if-ge v1, v2, :cond_6

    .line 128
    invoke-virtual {p1, v1}, Lcom/squareup/orderentry/pages/OrderEntryPageList;->getTab(I)Lcom/squareup/orderentry/pages/OrderEntryPage;

    move-result-object v2

    .line 129
    invoke-virtual {p0}, Lcom/squareup/orderentry/NavigationBarView;->isEditor()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Lcom/squareup/orderentry/pages/OrderEntryPage;->isVisibleInEditMode()Z

    move-result v3

    if-nez v3, :cond_2

    goto :goto_4

    .line 133
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/orderentry/NavigationBarView;->getOrientation()I

    move-result v3

    if-nez v3, :cond_3

    .line 134
    invoke-direct {p0, v2}, Lcom/squareup/orderentry/NavigationBarView;->makeNarrowTabView(Lcom/squareup/orderentry/pages/OrderEntryPage;)Landroid/view/View;

    move-result-object v3

    goto :goto_2

    .line 135
    :cond_3
    invoke-direct {p0, v2}, Lcom/squareup/orderentry/NavigationBarView;->makeWideTabView(Lcom/squareup/orderentry/pages/OrderEntryPage;)Landroid/view/View;

    move-result-object v3

    .line 136
    :goto_2
    invoke-virtual {v2}, Lcom/squareup/orderentry/pages/OrderEntryPage;->hasFavoritesPageId()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 137
    new-instance v4, Lcom/squareup/orderentry/-$$Lambda$NavigationBarView$aS2_-RoCV1CyjtklA2UWsMX4P64;

    invoke-direct {v4, p0, v2}, Lcom/squareup/orderentry/-$$Lambda$NavigationBarView$aS2_-RoCV1CyjtklA2UWsMX4P64;-><init>(Lcom/squareup/orderentry/NavigationBarView;Lcom/squareup/orderentry/pages/OrderEntryPage;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    goto :goto_3

    .line 141
    :cond_4
    invoke-virtual {p0}, Lcom/squareup/orderentry/NavigationBarView;->getPresenter()Lcom/squareup/orderentry/NavigationBarAbstractPresenter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->isThreeColumn()Z

    move-result v4

    if-nez v4, :cond_5

    .line 143
    invoke-static {v3}, Lcom/squareup/widgets/CheatSheet;->setup(Landroid/view/View;)V

    .line 145
    :cond_5
    :goto_3
    invoke-virtual {v2}, Lcom/squareup/orderentry/pages/OrderEntryPage;->getKey()Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 146
    iget-object v4, p0, Lcom/squareup/orderentry/NavigationBarView;->content:Landroid/view/ViewGroup;

    invoke-virtual {v4, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 147
    iget-object v4, p0, Lcom/squareup/orderentry/NavigationBarView;->navigationTabs:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 148
    invoke-virtual {v2}, Lcom/squareup/orderentry/pages/OrderEntryPage;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 149
    new-instance v2, Lcom/squareup/orderentry/NavigationBarView$1;

    invoke-direct {v2, p0}, Lcom/squareup/orderentry/NavigationBarView$1;-><init>(Lcom/squareup/orderentry/NavigationBarView;)V

    invoke-virtual {v3, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_6
    if-eqz v0, :cond_7

    .line 157
    iget-object p1, p0, Lcom/squareup/orderentry/NavigationBarView;->content:Landroid/view/ViewGroup;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_7
    return-void
.end method
