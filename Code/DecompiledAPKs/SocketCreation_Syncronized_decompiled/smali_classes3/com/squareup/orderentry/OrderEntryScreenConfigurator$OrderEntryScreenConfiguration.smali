.class public Lcom/squareup/orderentry/OrderEntryScreenConfigurator$OrderEntryScreenConfiguration;
.super Ljava/lang/Object;
.source "OrderEntryScreenConfigurator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/OrderEntryScreenConfigurator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OrderEntryScreenConfiguration"
.end annotation


# instance fields
.field final configureOrderEntryTabsV2:Z

.field final diminishedTabHeight:I

.field final fullTabHeight:I

.field final orderEntryViewPagerId:I

.field final upperSectionBackgroundColor:I


# direct methods
.method constructor <init>(IIIIZ)V
    .locals 0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput p1, p0, Lcom/squareup/orderentry/OrderEntryScreenConfigurator$OrderEntryScreenConfiguration;->orderEntryViewPagerId:I

    .line 55
    iput p2, p0, Lcom/squareup/orderentry/OrderEntryScreenConfigurator$OrderEntryScreenConfiguration;->fullTabHeight:I

    .line 56
    iput p3, p0, Lcom/squareup/orderentry/OrderEntryScreenConfigurator$OrderEntryScreenConfiguration;->diminishedTabHeight:I

    .line 57
    iput p4, p0, Lcom/squareup/orderentry/OrderEntryScreenConfigurator$OrderEntryScreenConfiguration;->upperSectionBackgroundColor:I

    .line 58
    iput-boolean p5, p0, Lcom/squareup/orderentry/OrderEntryScreenConfigurator$OrderEntryScreenConfiguration;->configureOrderEntryTabsV2:Z

    return-void
.end method
