.class public final Lcom/squareup/orderentry/PaymentPadPhonePresenter;
.super Lmortar/ViewPresenter;
.source "PaymentPadPhonePresenter.kt"


# annotations
.annotation runtime Lcom/squareup/orderentry/OrderEntryScreen$PhoneScope;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabFormat;,
        Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/orderentry/PaymentPadPhoneView;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u001f B?\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0002\u0010\u0011J\u0006\u0010\u0016\u001a\u00020\u0017J\u0006\u0010\u0018\u001a\u00020\u0017J\u0012\u0010\u0019\u001a\u00020\u00172\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u0016J\u0010\u0010\u001c\u001a\u00020\u00172\u0006\u0010\u001d\u001a\u00020\u001eH\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0012\u001a\u00020\u00138F\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\u0015R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/orderentry/PaymentPadPhonePresenter;",
        "Lmortar/ViewPresenter;",
        "Lcom/squareup/orderentry/PaymentPadPhoneView;",
        "pagerPresenter",
        "Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;",
        "orderEntryPages",
        "Lcom/squareup/orderentry/pages/OrderEntryPages;",
        "orderEntryScreenState",
        "Lcom/squareup/orderentry/OrderEntryScreenState;",
        "device",
        "Lcom/squareup/util/Device;",
        "tutorialCore",
        "Lcom/squareup/tutorialv2/TutorialCore;",
        "orderEntryScreenConfigurator",
        "Lcom/squareup/orderentry/OrderEntryScreenConfigurator;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/util/Device;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/orderentry/OrderEntryScreenConfigurator;Lcom/squareup/settings/server/Features;)V",
        "screenConfiguration",
        "Lcom/squareup/orderentry/OrderEntryScreenConfigurator$OrderEntryScreenConfiguration;",
        "getScreenConfiguration",
        "()Lcom/squareup/orderentry/OrderEntryScreenConfigurator$OrderEntryScreenConfiguration;",
        "onKeypadTabClicked",
        "",
        "onLibraryTabClick",
        "onLoad",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "updateTabsSize",
        "tabConfiguration",
        "Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;",
        "TabConfiguration",
        "TabFormat",
        "order-entry_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final device:Lcom/squareup/util/Device;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

.field private final orderEntryScreenConfigurator:Lcom/squareup/orderentry/OrderEntryScreenConfigurator;

.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

.field private final pagerPresenter:Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method public constructor <init>(Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/util/Device;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/orderentry/OrderEntryScreenConfigurator;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "pagerPresenter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderEntryPages"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderEntryScreenState"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tutorialCore"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderEntryScreenConfigurator"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    iput-object p1, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->pagerPresenter:Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;

    iput-object p2, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    iput-object p3, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    iput-object p4, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->device:Lcom/squareup/util/Device;

    iput-object p5, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    iput-object p6, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->orderEntryScreenConfigurator:Lcom/squareup/orderentry/OrderEntryScreenConfigurator;

    iput-object p7, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static final synthetic access$getDevice$p(Lcom/squareup/orderentry/PaymentPadPhonePresenter;)Lcom/squareup/util/Device;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->device:Lcom/squareup/util/Device;

    return-object p0
.end method

.method public static final synthetic access$getFeatures$p(Lcom/squareup/orderentry/PaymentPadPhonePresenter;)Lcom/squareup/settings/server/Features;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->features:Lcom/squareup/settings/server/Features;

    return-object p0
.end method

.method public static final synthetic access$getOrderEntryPages$p(Lcom/squareup/orderentry/PaymentPadPhonePresenter;)Lcom/squareup/orderentry/pages/OrderEntryPages;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    return-object p0
.end method

.method public static final synthetic access$getOrderEntryScreenState$p(Lcom/squareup/orderentry/PaymentPadPhonePresenter;)Lcom/squareup/orderentry/OrderEntryScreenState;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    return-object p0
.end method

.method public static final synthetic access$getView(Lcom/squareup/orderentry/PaymentPadPhonePresenter;)Lcom/squareup/orderentry/PaymentPadPhoneView;
    .locals 0

    .line 21
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->getView()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/orderentry/PaymentPadPhoneView;

    return-object p0
.end method

.method public static final synthetic access$hasView(Lcom/squareup/orderentry/PaymentPadPhonePresenter;)Z
    .locals 0

    .line 21
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->hasView()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$updateTabsSize(Lcom/squareup/orderentry/PaymentPadPhonePresenter;Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1}, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->updateTabsSize(Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;)V

    return-void
.end method

.method private final updateTabsSize(Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;)V
    .locals 1

    .line 99
    invoke-virtual {p1}, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabConfiguration;->getTabFormat()Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabFormat;

    move-result-object p1

    sget-object v0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/orderentry/PaymentPadPhonePresenter$TabFormat;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 101
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/PaymentPadPhoneView;

    invoke-virtual {p1}, Lcom/squareup/orderentry/PaymentPadPhoneView;->showFullTabs()V

    goto :goto_0

    .line 100
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/PaymentPadPhoneView;

    invoke-virtual {p1}, Lcom/squareup/orderentry/PaymentPadPhoneView;->showSlimTabs()V

    :goto_0
    return-void
.end method


# virtual methods
.method public final getScreenConfiguration()Lcom/squareup/orderentry/OrderEntryScreenConfigurator$OrderEntryScreenConfiguration;
    .locals 2

    .line 33
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->orderEntryScreenConfigurator:Lcom/squareup/orderentry/OrderEntryScreenConfigurator;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenConfigurator;->getConfiguration()Lcom/squareup/orderentry/OrderEntryScreenConfigurator$OrderEntryScreenConfiguration;

    move-result-object v0

    const-string v1, "orderEntryScreenConfigurator.configuration"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final onKeypadTabClicked()V
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->pagerPresenter:Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->animateToKeypad()V

    return-void
.end method

.method public final onLibraryTabClick()V
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->pagerPresenter:Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->animateToLibrary()V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 36
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 51
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/PaymentPadPhoneView;

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/view/View;

    new-instance v1, Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$1;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$1;-><init>(Lcom/squareup/orderentry/PaymentPadPhonePresenter;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 68
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/PaymentPadPhoneView;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/view/View;

    new-instance v0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$2;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$2;-><init>(Lcom/squareup/orderentry/PaymentPadPhonePresenter;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 81
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->pagerPresenter:Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;

    new-instance v0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$3;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$3;-><init>(Lcom/squareup/orderentry/PaymentPadPhonePresenter;)V

    check-cast v0, Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->setPageScrollListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    return-void
.end method
