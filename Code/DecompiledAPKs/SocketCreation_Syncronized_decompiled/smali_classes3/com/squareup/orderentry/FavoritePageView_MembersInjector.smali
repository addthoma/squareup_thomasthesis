.class public final Lcom/squareup/orderentry/FavoritePageView_MembersInjector;
.super Ljava/lang/Object;
.source "FavoritePageView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/orderentry/FavoritePageView;",
        ">;"
    }
.end annotation


# instance fields
.field private final catalogIntegrationControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/FavoritePageScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field

.field private final toastFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/FavoritePageScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/orderentry/FavoritePageView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/squareup/orderentry/FavoritePageView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/orderentry/FavoritePageView_MembersInjector;->toastFactoryProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/orderentry/FavoritePageView_MembersInjector;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/FavoritePageScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/orderentry/FavoritePageView;",
            ">;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/orderentry/FavoritePageView_MembersInjector;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/orderentry/FavoritePageView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectCatalogIntegrationController(Lcom/squareup/orderentry/FavoritePageView;Lcom/squareup/catalogapi/CatalogIntegrationController;)V
    .locals 0

    .line 70
    iput-object p1, p0, Lcom/squareup/orderentry/FavoritePageView;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    return-void
.end method

.method public static injectDevice(Lcom/squareup/orderentry/FavoritePageView;Lcom/squareup/util/Device;)V
    .locals 0

    .line 54
    iput-object p1, p0, Lcom/squareup/orderentry/FavoritePageView;->device:Lcom/squareup/util/Device;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/orderentry/FavoritePageView;Ljava/lang/Object;)V
    .locals 0

    .line 59
    check-cast p1, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;

    iput-object p1, p0, Lcom/squareup/orderentry/FavoritePageView;->presenter:Lcom/squareup/orderentry/FavoritePageScreen$Presenter;

    return-void
.end method

.method public static injectToastFactory(Lcom/squareup/orderentry/FavoritePageView;Lcom/squareup/util/ToastFactory;)V
    .locals 0

    .line 64
    iput-object p1, p0, Lcom/squareup/orderentry/FavoritePageView;->toastFactory:Lcom/squareup/util/ToastFactory;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/orderentry/FavoritePageView;)V
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Device;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/FavoritePageView_MembersInjector;->injectDevice(Lcom/squareup/orderentry/FavoritePageView;Lcom/squareup/util/Device;)V

    .line 47
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/orderentry/FavoritePageView_MembersInjector;->injectPresenter(Lcom/squareup/orderentry/FavoritePageView;Ljava/lang/Object;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageView_MembersInjector;->toastFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/ToastFactory;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/FavoritePageView_MembersInjector;->injectToastFactory(Lcom/squareup/orderentry/FavoritePageView;Lcom/squareup/util/ToastFactory;)V

    .line 49
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageView_MembersInjector;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/FavoritePageView_MembersInjector;->injectCatalogIntegrationController(Lcom/squareup/orderentry/FavoritePageView;Lcom/squareup/catalogapi/CatalogIntegrationController;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 11
    check-cast p1, Lcom/squareup/orderentry/FavoritePageView;

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/FavoritePageView_MembersInjector;->injectMembers(Lcom/squareup/orderentry/FavoritePageView;)V

    return-void
.end method
