.class public final synthetic Lcom/squareup/orderentry/RealOrderEntryAppletGateway$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/squareup/orderentry/OrderEntryMode;->values()[Lcom/squareup/orderentry/OrderEntryMode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/orderentry/RealOrderEntryAppletGateway$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/orderentry/RealOrderEntryAppletGateway$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/orderentry/OrderEntryMode;->LAST_SELECTED:Lcom/squareup/orderentry/OrderEntryMode;

    invoke-virtual {v1}, Lcom/squareup/orderentry/OrderEntryMode;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/orderentry/RealOrderEntryAppletGateway$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/orderentry/OrderEntryMode;->FAVORITES:Lcom/squareup/orderentry/OrderEntryMode;

    invoke-virtual {v1}, Lcom/squareup/orderentry/OrderEntryMode;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/orderentry/RealOrderEntryAppletGateway$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/orderentry/OrderEntryMode;->KEYPAD:Lcom/squareup/orderentry/OrderEntryMode;

    invoke-virtual {v1}, Lcom/squareup/orderentry/OrderEntryMode;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/orderentry/RealOrderEntryAppletGateway$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/orderentry/OrderEntryMode;->EDIT_FAVORITES_FROM_ITEMS_APPLET:Lcom/squareup/orderentry/OrderEntryMode;

    invoke-virtual {v1}, Lcom/squareup/orderentry/OrderEntryMode;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1

    return-void
.end method
