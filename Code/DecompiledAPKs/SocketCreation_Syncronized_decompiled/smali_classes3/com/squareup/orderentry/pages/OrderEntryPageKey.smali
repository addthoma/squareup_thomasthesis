.class public final enum Lcom/squareup/orderentry/pages/OrderEntryPageKey;
.super Ljava/lang/Enum;
.source "OrderEntryPageKey.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/orderentry/pages/OrderEntryPageKey;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/orderentry/pages/OrderEntryPageKey;

.field public static final ALL:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orderentry/pages/OrderEntryPageKey;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ALL_ITEMS:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

.field public static final enum CATEGORIES:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

.field public static final enum DISCOUNTS:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

.field public static final enum FAV1:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

.field public static final enum FAV2:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

.field public static final enum FAV3:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

.field public static final enum FAV4:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

.field public static final enum FAV5:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

.field public static final enum FAV6:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

.field public static final enum FAV7:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

.field public static final enum GIFT_CARDS:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

.field private static final IN_LIBRARY:Z = true

.field public static final enum KEYPAD:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

.field public static final enum LIBRARY:Lcom/squareup/orderentry/pages/OrderEntryPageKey;


# instance fields
.field public final glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public final isFavoritesGrid:Z

.field public final isInLibrary:Z

.field private final nameId:I


# direct methods
.method static constructor <clinit>()V
    .locals 22

    .line 34
    new-instance v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NAVIGATION_1:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v2, 0x0

    const-string v3, "FAV1"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;)V

    sput-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->FAV1:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    .line 35
    new-instance v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NAVIGATION_2:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v3, 0x1

    const-string v4, "FAV2"

    invoke-direct {v0, v4, v3, v1}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;)V

    sput-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->FAV2:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    .line 36
    new-instance v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NAVIGATION_3:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v4, 0x2

    const-string v5, "FAV3"

    invoke-direct {v0, v5, v4, v1}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;)V

    sput-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->FAV3:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    .line 37
    new-instance v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NAVIGATION_4:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v5, 0x3

    const-string v6, "FAV4"

    invoke-direct {v0, v6, v5, v1}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;)V

    sput-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->FAV4:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    .line 38
    new-instance v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NAVIGATION_5:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v6, 0x4

    const-string v7, "FAV5"

    invoke-direct {v0, v7, v6, v1}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;)V

    sput-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->FAV5:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    .line 39
    new-instance v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NAVIGATION_6:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v7, 0x5

    const-string v8, "FAV6"

    invoke-direct {v0, v8, v7, v1}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;)V

    sput-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->FAV6:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    .line 40
    new-instance v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NAVIGATION_7:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v8, 0x6

    const-string v9, "FAV7"

    invoke-direct {v0, v9, v8, v1}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;-><init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;)V

    sput-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->FAV7:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    .line 43
    new-instance v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    sget v13, Lcom/squareup/orderentry/common/R$string;->navigation_open_tab_library:I

    sget-object v15, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NAVIGATION_LIBRARY:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v11, "LIBRARY"

    const/4 v12, 0x7

    const/4 v14, 0x1

    move-object v10, v0

    invoke-direct/range {v10 .. v15}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;-><init>(Ljava/lang/String;IIZLcom/squareup/glyph/GlyphTypeface$Glyph;)V

    sput-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->LIBRARY:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    .line 44
    new-instance v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    sget v19, Lcom/squareup/orderentry/common/R$string;->navigation_open_tab_keypad:I

    sget-object v21, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NAVIGATION_KEYPAD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v17, "KEYPAD"

    const/16 v18, 0x8

    const/16 v20, 0x0

    move-object/from16 v16, v0

    invoke-direct/range {v16 .. v21}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;-><init>(Ljava/lang/String;IIZLcom/squareup/glyph/GlyphTypeface$Glyph;)V

    sput-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->KEYPAD:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    .line 47
    new-instance v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    sget v12, Lcom/squareup/orderentry/common/R$string;->navigation_open_tab_all_items:I

    const-string v10, "ALL_ITEMS"

    const/16 v11, 0x9

    const/4 v13, 0x1

    const/4 v14, 0x0

    move-object v9, v0

    invoke-direct/range {v9 .. v14}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;-><init>(Ljava/lang/String;IIZLcom/squareup/glyph/GlyphTypeface$Glyph;)V

    sput-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->ALL_ITEMS:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    .line 48
    new-instance v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    sget v18, Lcom/squareup/orderentry/common/R$string;->navigation_open_tab_categories:I

    const-string v16, "CATEGORIES"

    const/16 v17, 0xa

    const/16 v19, 0x1

    const/16 v20, 0x0

    move-object v15, v0

    invoke-direct/range {v15 .. v20}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;-><init>(Ljava/lang/String;IIZLcom/squareup/glyph/GlyphTypeface$Glyph;)V

    sput-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->CATEGORIES:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    .line 49
    new-instance v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    sget v12, Lcom/squareup/orderentry/common/R$string;->navigation_open_tab_discounts:I

    const-string v10, "DISCOUNTS"

    const/16 v11, 0xb

    move-object v9, v0

    invoke-direct/range {v9 .. v14}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;-><init>(Ljava/lang/String;IIZLcom/squareup/glyph/GlyphTypeface$Glyph;)V

    sput-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->DISCOUNTS:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    .line 50
    new-instance v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    sget v18, Lcom/squareup/orderentry/common/R$string;->navigation_open_tab_gift_cards:I

    const-string v16, "GIFT_CARDS"

    const/16 v17, 0xc

    move-object v15, v0

    invoke-direct/range {v15 .. v20}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;-><init>(Ljava/lang/String;IIZLcom/squareup/glyph/GlyphTypeface$Glyph;)V

    sput-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->GIFT_CARDS:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    const/16 v0, 0xd

    new-array v0, v0, [Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    .line 32
    sget-object v1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->FAV1:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->FAV2:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->FAV3:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->FAV4:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->FAV5:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->FAV6:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->FAV7:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->LIBRARY:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->KEYPAD:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->ALL_ITEMS:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->CATEGORIES:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->DISCOUNTS:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->GIFT_CARDS:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->$VALUES:[Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    .line 53
    invoke-static {}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->values()[Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->ALL:Ljava/util/List;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIZLcom/squareup/glyph/GlyphTypeface$Glyph;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
            ")V"
        }
    .end annotation

    .line 109
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 110
    iput p3, p0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->nameId:I

    .line 111
    iput-boolean p4, p0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->isInLibrary:Z

    const/4 p1, 0x0

    .line 112
    iput-boolean p1, p0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->isFavoritesGrid:Z

    .line 113
    iput-object p5, p0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/glyph/GlyphTypeface$Glyph;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
            ")V"
        }
    .end annotation

    .line 101
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    const/4 p1, 0x0

    .line 102
    iput-boolean p1, p0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->isInLibrary:Z

    const/4 p1, 0x1

    .line 103
    iput-boolean p1, p0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->isFavoritesGrid:Z

    const/4 p1, -0x1

    .line 104
    iput p1, p0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->nameId:I

    .line 105
    iput-object p3, p0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-void
.end method

.method static defaultIndex(Lcom/squareup/settings/server/AccountStatusSettings;Z)Lcom/squareup/orderentry/pages/OrderEntryPageKey;
    .locals 0

    if-nez p1, :cond_1

    .line 90
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->shouldShowLibraryFirstPhone()Z

    move-result p1

    if-nez p1, :cond_1

    .line 91
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->hasPosBestAvailableProductIntent()Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    if-eqz p0, :cond_2

    .line 92
    sget-object p0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->LIBRARY:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    goto :goto_2

    :cond_2
    sget-object p0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->KEYPAD:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    :goto_2
    return-object p0
.end method

.method static libraryFilterToTab(Lcom/squareup/librarylist/LibraryListState$Filter;)Lcom/squareup/orderentry/pages/OrderEntryPageKey;
    .locals 3

    .line 58
    sget-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey$1;->$SwitchMap$com$squareup$librarylist$LibraryListState$Filter:[I

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryListState$Filter;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 67
    sget-object p0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->GIFT_CARDS:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    return-object p0

    .line 69
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected library filter "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 65
    :cond_1
    sget-object p0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->DISCOUNTS:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    return-object p0

    .line 63
    :cond_2
    sget-object p0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->ALL_ITEMS:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    return-object p0

    .line 61
    :cond_3
    sget-object p0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->CATEGORIES:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    return-object p0
.end method

.method static tabToLibraryFilter(Lcom/squareup/orderentry/pages/OrderEntryPageKey;)Lcom/squareup/librarylist/LibraryListState$Filter;
    .locals 3

    .line 74
    sget-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey$1;->$SwitchMap$com$squareup$orderentry$pages$OrderEntryPageKey:[I

    invoke-virtual {p0}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 82
    sget-object p0, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_GIFT_CARDS:Lcom/squareup/librarylist/LibraryListState$Filter;

    return-object p0

    .line 84
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected tab "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 80
    :cond_1
    sget-object p0, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_DISCOUNTS:Lcom/squareup/librarylist/LibraryListState$Filter;

    return-object p0

    .line 78
    :cond_2
    sget-object p0, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_CATEGORIES:Lcom/squareup/librarylist/LibraryListState$Filter;

    return-object p0

    .line 76
    :cond_3
    sget-object p0, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_ITEMS:Lcom/squareup/librarylist/LibraryListState$Filter;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/orderentry/pages/OrderEntryPageKey;
    .locals 1

    .line 32
    const-class v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    return-object p0
.end method

.method public static values()[Lcom/squareup/orderentry/pages/OrderEntryPageKey;
    .locals 1

    .line 32
    sget-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->$VALUES:[Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    invoke-virtual {v0}, [Lcom/squareup/orderentry/pages/OrderEntryPageKey;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    return-object v0
.end method


# virtual methods
.method public getNameId(Lcom/squareup/catalogapi/CatalogIntegrationController;)I
    .locals 2

    .line 117
    iget v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->nameId:I

    sget v1, Lcom/squareup/orderentry/common/R$string;->navigation_open_tab_all_items:I

    if-ne v0, v1, :cond_0

    .line 118
    invoke-interface {p1}, Lcom/squareup/catalogapi/CatalogIntegrationController;->shouldShowPartiallyRolledOutFeature()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 119
    sget p1, Lcom/squareup/orderentry/common/R$string;->navigation_open_tab_all_items_and_services:I

    return p1

    .line 121
    :cond_0
    iget p1, p0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->nameId:I

    return p1
.end method
