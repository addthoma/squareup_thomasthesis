.class public final enum Lcom/squareup/dipper/events/BleConnectionState;
.super Ljava/lang/Enum;
.source "BleConnectionState.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/dipper/events/BleConnectionState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\"\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012j\u0002\u0008\u0013j\u0002\u0008\u0014j\u0002\u0008\u0015j\u0002\u0008\u0016j\u0002\u0008\u0017j\u0002\u0008\u0018j\u0002\u0008\u0019j\u0002\u0008\u001aj\u0002\u0008\u001bj\u0002\u0008\u001cj\u0002\u0008\u001dj\u0002\u0008\u001ej\u0002\u0008\u001fj\u0002\u0008 j\u0002\u0008!j\u0002\u0008\"\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/dipper/events/BleConnectionState;",
        "",
        "(Ljava/lang/String;I)V",
        "UNKNOWN_BLE_CONNECTION_STATE",
        "NONE",
        "CREATED",
        "WAITING_FOR_CONNECTION_TO_READER",
        "WAITING_FOR_SERVICE_DISCOVERY",
        "WAITING_FOR_SERVICE_CHARACTERISTIC_VERSION",
        "WAITING_FOR_SERIAL_NUMBER",
        "WAITING_FOR_BOND_STATUS_FROM_READER",
        "WAITING_FOR_REMOVE_BOND",
        "WAITING_FOR_BOND",
        "WAITING_FOR_CONNECTION_INTERVAL",
        "WAITING_FOR_COMMS_VERSION",
        "WAITING_FOR_MTU_NOTIFICATIONS",
        "WAITING_FOR_DATA_NOTIFICATIONS",
        "READY",
        "POSSIBLY_DISCONNECTING",
        "WAITING_FOR_DISCONNECT_TO_SILENTLY_RECONNECT",
        "DESTROYING_FROM_DISCONNECT_WHILE_CONNECTED",
        "DESTROYING_FROM_SELLER_ACTION",
        "DESTROYING_FOR_ALREADY_BONDED",
        "DESTROYING_FOR_BAD_READER_STATE",
        "DESTROYING_FOR_INCOMPATIBLE_SERVICE_VERSION",
        "DESTROYING_FOR_OLD_SERVICES_CACHED",
        "DESTROYING_FOR_PAIRING_TIMEOUT",
        "DESTROYING_FOR_TOO_MANY_RECONNECT_ATTEMPTS",
        "DESTROYING_FOR_UNABLE_TO_CREATE_BOND",
        "DESTROYING_AFTER_GATT_ERROR",
        "WAITING_FOR_RECONNECT_AFTER_GATT_ERROR",
        "DESTROYING_FOR_UNEXPECTED_STATE",
        "WAITING_FOR_DISCONNECTION",
        "WAITING_FOR_CONNECTION_CONTROL",
        "DISCONNECTED",
        "dipper_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum CREATED:Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum DESTROYING_AFTER_GATT_ERROR:Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum DESTROYING_FOR_ALREADY_BONDED:Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum DESTROYING_FOR_BAD_READER_STATE:Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum DESTROYING_FOR_INCOMPATIBLE_SERVICE_VERSION:Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum DESTROYING_FOR_OLD_SERVICES_CACHED:Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum DESTROYING_FOR_PAIRING_TIMEOUT:Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum DESTROYING_FOR_TOO_MANY_RECONNECT_ATTEMPTS:Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum DESTROYING_FOR_UNABLE_TO_CREATE_BOND:Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum DESTROYING_FOR_UNEXPECTED_STATE:Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum DESTROYING_FROM_DISCONNECT_WHILE_CONNECTED:Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum DESTROYING_FROM_SELLER_ACTION:Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum DISCONNECTED:Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum NONE:Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum POSSIBLY_DISCONNECTING:Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum READY:Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum UNKNOWN_BLE_CONNECTION_STATE:Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum WAITING_FOR_BOND:Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum WAITING_FOR_BOND_STATUS_FROM_READER:Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum WAITING_FOR_COMMS_VERSION:Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum WAITING_FOR_CONNECTION_CONTROL:Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum WAITING_FOR_CONNECTION_INTERVAL:Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum WAITING_FOR_CONNECTION_TO_READER:Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum WAITING_FOR_DATA_NOTIFICATIONS:Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum WAITING_FOR_DISCONNECTION:Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum WAITING_FOR_DISCONNECT_TO_SILENTLY_RECONNECT:Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum WAITING_FOR_MTU_NOTIFICATIONS:Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum WAITING_FOR_RECONNECT_AFTER_GATT_ERROR:Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum WAITING_FOR_REMOVE_BOND:Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum WAITING_FOR_SERIAL_NUMBER:Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum WAITING_FOR_SERVICE_CHARACTERISTIC_VERSION:Lcom/squareup/dipper/events/BleConnectionState;

.field public static final enum WAITING_FOR_SERVICE_DISCOVERY:Lcom/squareup/dipper/events/BleConnectionState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0x20

    new-array v0, v0, [Lcom/squareup/dipper/events/BleConnectionState;

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const/4 v2, 0x0

    const-string v3, "UNKNOWN_BLE_CONNECTION_STATE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->UNKNOWN_BLE_CONNECTION_STATE:Lcom/squareup/dipper/events/BleConnectionState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const/4 v2, 0x1

    const-string v3, "NONE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->NONE:Lcom/squareup/dipper/events/BleConnectionState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const/4 v2, 0x2

    const-string v3, "CREATED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->CREATED:Lcom/squareup/dipper/events/BleConnectionState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const/4 v2, 0x3

    const-string v3, "WAITING_FOR_CONNECTION_TO_READER"

    invoke-direct {v1, v3, v2}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_CONNECTION_TO_READER:Lcom/squareup/dipper/events/BleConnectionState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const/4 v2, 0x4

    const-string v3, "WAITING_FOR_SERVICE_DISCOVERY"

    invoke-direct {v1, v3, v2}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_SERVICE_DISCOVERY:Lcom/squareup/dipper/events/BleConnectionState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const/4 v2, 0x5

    const-string v3, "WAITING_FOR_SERVICE_CHARACTERISTIC_VERSION"

    invoke-direct {v1, v3, v2}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_SERVICE_CHARACTERISTIC_VERSION:Lcom/squareup/dipper/events/BleConnectionState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const/4 v2, 0x6

    const-string v3, "WAITING_FOR_SERIAL_NUMBER"

    invoke-direct {v1, v3, v2}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_SERIAL_NUMBER:Lcom/squareup/dipper/events/BleConnectionState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const/4 v2, 0x7

    const-string v3, "WAITING_FOR_BOND_STATUS_FROM_READER"

    invoke-direct {v1, v3, v2}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_BOND_STATUS_FROM_READER:Lcom/squareup/dipper/events/BleConnectionState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const/16 v2, 0x8

    const-string v3, "WAITING_FOR_REMOVE_BOND"

    invoke-direct {v1, v3, v2}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_REMOVE_BOND:Lcom/squareup/dipper/events/BleConnectionState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const/16 v2, 0x9

    const-string v3, "WAITING_FOR_BOND"

    invoke-direct {v1, v3, v2}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_BOND:Lcom/squareup/dipper/events/BleConnectionState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const/16 v2, 0xa

    const-string v3, "WAITING_FOR_CONNECTION_INTERVAL"

    invoke-direct {v1, v3, v2}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_CONNECTION_INTERVAL:Lcom/squareup/dipper/events/BleConnectionState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const/16 v2, 0xb

    const-string v3, "WAITING_FOR_COMMS_VERSION"

    invoke-direct {v1, v3, v2}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_COMMS_VERSION:Lcom/squareup/dipper/events/BleConnectionState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const/16 v2, 0xc

    const-string v3, "WAITING_FOR_MTU_NOTIFICATIONS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_MTU_NOTIFICATIONS:Lcom/squareup/dipper/events/BleConnectionState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const/16 v2, 0xd

    const-string v3, "WAITING_FOR_DATA_NOTIFICATIONS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_DATA_NOTIFICATIONS:Lcom/squareup/dipper/events/BleConnectionState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const/16 v2, 0xe

    const-string v3, "READY"

    invoke-direct {v1, v3, v2}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->READY:Lcom/squareup/dipper/events/BleConnectionState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const-string v2, "POSSIBLY_DISCONNECTING"

    const/16 v3, 0xf

    invoke-direct {v1, v2, v3}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->POSSIBLY_DISCONNECTING:Lcom/squareup/dipper/events/BleConnectionState;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const-string v2, "WAITING_FOR_DISCONNECT_TO_SILENTLY_RECONNECT"

    const/16 v3, 0x10

    invoke-direct {v1, v2, v3}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_DISCONNECT_TO_SILENTLY_RECONNECT:Lcom/squareup/dipper/events/BleConnectionState;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const-string v2, "DESTROYING_FROM_DISCONNECT_WHILE_CONNECTED"

    const/16 v3, 0x11

    invoke-direct {v1, v2, v3}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->DESTROYING_FROM_DISCONNECT_WHILE_CONNECTED:Lcom/squareup/dipper/events/BleConnectionState;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const-string v2, "DESTROYING_FROM_SELLER_ACTION"

    const/16 v3, 0x12

    invoke-direct {v1, v2, v3}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->DESTROYING_FROM_SELLER_ACTION:Lcom/squareup/dipper/events/BleConnectionState;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const-string v2, "DESTROYING_FOR_ALREADY_BONDED"

    const/16 v3, 0x13

    invoke-direct {v1, v2, v3}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->DESTROYING_FOR_ALREADY_BONDED:Lcom/squareup/dipper/events/BleConnectionState;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const-string v2, "DESTROYING_FOR_BAD_READER_STATE"

    const/16 v3, 0x14

    invoke-direct {v1, v2, v3}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->DESTROYING_FOR_BAD_READER_STATE:Lcom/squareup/dipper/events/BleConnectionState;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const-string v2, "DESTROYING_FOR_INCOMPATIBLE_SERVICE_VERSION"

    const/16 v3, 0x15

    invoke-direct {v1, v2, v3}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->DESTROYING_FOR_INCOMPATIBLE_SERVICE_VERSION:Lcom/squareup/dipper/events/BleConnectionState;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const-string v2, "DESTROYING_FOR_OLD_SERVICES_CACHED"

    const/16 v3, 0x16

    invoke-direct {v1, v2, v3}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->DESTROYING_FOR_OLD_SERVICES_CACHED:Lcom/squareup/dipper/events/BleConnectionState;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const-string v2, "DESTROYING_FOR_PAIRING_TIMEOUT"

    const/16 v3, 0x17

    invoke-direct {v1, v2, v3}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->DESTROYING_FOR_PAIRING_TIMEOUT:Lcom/squareup/dipper/events/BleConnectionState;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const-string v2, "DESTROYING_FOR_TOO_MANY_RECONNECT_ATTEMPTS"

    const/16 v3, 0x18

    invoke-direct {v1, v2, v3}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->DESTROYING_FOR_TOO_MANY_RECONNECT_ATTEMPTS:Lcom/squareup/dipper/events/BleConnectionState;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const-string v2, "DESTROYING_FOR_UNABLE_TO_CREATE_BOND"

    const/16 v3, 0x19

    invoke-direct {v1, v2, v3}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->DESTROYING_FOR_UNABLE_TO_CREATE_BOND:Lcom/squareup/dipper/events/BleConnectionState;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const-string v2, "DESTROYING_AFTER_GATT_ERROR"

    const/16 v3, 0x1a

    invoke-direct {v1, v2, v3}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->DESTROYING_AFTER_GATT_ERROR:Lcom/squareup/dipper/events/BleConnectionState;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const-string v2, "WAITING_FOR_RECONNECT_AFTER_GATT_ERROR"

    const/16 v3, 0x1b

    invoke-direct {v1, v2, v3}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_RECONNECT_AFTER_GATT_ERROR:Lcom/squareup/dipper/events/BleConnectionState;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const-string v2, "DESTROYING_FOR_UNEXPECTED_STATE"

    const/16 v3, 0x1c

    invoke-direct {v1, v2, v3}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->DESTROYING_FOR_UNEXPECTED_STATE:Lcom/squareup/dipper/events/BleConnectionState;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const-string v2, "WAITING_FOR_DISCONNECTION"

    const/16 v3, 0x1d

    invoke-direct {v1, v2, v3}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_DISCONNECTION:Lcom/squareup/dipper/events/BleConnectionState;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const-string v2, "WAITING_FOR_CONNECTION_CONTROL"

    const/16 v3, 0x1e

    invoke-direct {v1, v2, v3}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_CONNECTION_CONTROL:Lcom/squareup/dipper/events/BleConnectionState;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleConnectionState;

    const-string v2, "DISCONNECTED"

    const/16 v3, 0x1f

    invoke-direct {v1, v2, v3}, Lcom/squareup/dipper/events/BleConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleConnectionState;->DISCONNECTED:Lcom/squareup/dipper/events/BleConnectionState;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/dipper/events/BleConnectionState;->$VALUES:[Lcom/squareup/dipper/events/BleConnectionState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/dipper/events/BleConnectionState;
    .locals 1

    const-class v0, Lcom/squareup/dipper/events/BleConnectionState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/dipper/events/BleConnectionState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/dipper/events/BleConnectionState;
    .locals 1

    sget-object v0, Lcom/squareup/dipper/events/BleConnectionState;->$VALUES:[Lcom/squareup/dipper/events/BleConnectionState;

    invoke-virtual {v0}, [Lcom/squareup/dipper/events/BleConnectionState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/dipper/events/BleConnectionState;

    return-object v0
.end method
