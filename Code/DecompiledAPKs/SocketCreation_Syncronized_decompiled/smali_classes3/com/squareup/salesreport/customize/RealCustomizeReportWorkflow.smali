.class public Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "CustomizeReportWorkflow.kt"

# interfaces
.implements Lcom/squareup/salesreport/customize/CustomizeReportWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/salesreport/customize/CustomizeReportInput;",
        "Lcom/squareup/salesreport/customize/CustomizeReportState;",
        "Lcom/squareup/salesreport/customize/CustomizeReportResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/salesreport/customize/CustomizeReportWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCustomizeReportWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CustomizeReportWorkflow.kt\ncom/squareup/salesreport/customize/RealCustomizeReportWorkflow\n+ 2 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 3 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 4 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 5 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,387:1\n85#2:388\n240#3:389\n276#4:390\n149#5,5:391\n149#5,5:396\n149#5,5:401\n*E\n*S KotlinDebug\n*F\n+ 1 CustomizeReportWorkflow.kt\ncom/squareup/salesreport/customize/RealCustomizeReportWorkflow\n*L\n118#1:388\n118#1:389\n118#1:390\n132#1,5:391\n140#1,5:396\n303#1,5:401\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00c4\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0016\u0018\u00002\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002BM\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u000e\u0008\u0001\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0010\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u0015\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u0012\u0006\u0010\u0019\u001a\u00020\u001a\u00a2\u0006\u0002\u0010\u001bJD\u0010\u001c\u001a\u001a\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u00062\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 2\u0012\u0010!\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\"H\u0002J\u0018\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020&2\u0006\u0010\'\u001a\u00020\u0011H\u0002J$\u0010(\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050)2\u0006\u0010*\u001a\u00020+2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J$\u0010,\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050)2\u0006\u0010-\u001a\u00020.2\u0006\u0010\u001d\u001a\u00020/H\u0002J\u001a\u00100\u001a\u00020\u00042\u0006\u00101\u001a\u00020\u00032\u0008\u00102\u001a\u0004\u0018\u000103H\u0016JN\u00104\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u00101\u001a\u00020\u00032\u0006\u0010\u001d\u001a\u00020\u00042\u0012\u0010!\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\"H\u0016J\u0010\u00105\u001a\u0002032\u0006\u0010\u001d\u001a\u00020\u0004H\u0016Jo\u00106\u001a\u00020\u001e\"\u0008\u0008\u0000\u00107*\u00020\u0004*\u0002H72\n\u0008\u0002\u00108\u001a\u0004\u0018\u0001092\n\u0008\u0002\u0010:\u001a\u0004\u0018\u0001092\n\u0008\u0002\u0010;\u001a\u0004\u0018\u00010<2\n\u0008\u0002\u0010=\u001a\u0004\u0018\u00010<2\n\u0008\u0002\u0010>\u001a\u0004\u0018\u00010\u00112\n\u0008\u0002\u0010?\u001a\u0004\u0018\u00010\u00112\n\u0008\u0002\u0010@\u001a\u0004\u0018\u00010AH\u0002\u00a2\u0006\u0002\u0010BJ\u0014\u0010C\u001a\u00020<*\u00020D2\u0006\u0010E\u001a\u00020<H\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006F"
    }
    d2 = {
        "Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;",
        "Lcom/squareup/salesreport/customize/CustomizeReportWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/salesreport/customize/CustomizeReportInput;",
        "Lcom/squareup/salesreport/customize/CustomizeReportState;",
        "Lcom/squareup/salesreport/customize/CustomizeReportResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "currentTime",
        "Lcom/squareup/time/CurrentTime;",
        "employees",
        "Lcom/squareup/permissions/Employees;",
        "thisDeviceOnlyFilteredLocalSetting",
        "Lcom/squareup/settings/LocalSetting;",
        "",
        "employeeManagement",
        "Lcom/squareup/permissions/EmployeeManagement;",
        "localeProvider",
        "Ljavax/inject/Provider;",
        "Ljava/util/Locale;",
        "realNohoTimePickerDialogWorkflow",
        "Lcom/squareup/register/widgets/NohoTimePickerDialogWorkflow;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/time/CurrentTime;Lcom/squareup/permissions/Employees;Lcom/squareup/settings/LocalSetting;Lcom/squareup/permissions/EmployeeManagement;Ljavax/inject/Provider;Lcom/squareup/register/widgets/NohoTimePickerDialogWorkflow;Lcom/squareup/util/Res;)V",
        "getCustomizeAllFieldsCardScreen",
        "state",
        "Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;",
        "uiDisplayState",
        "Lcom/squareup/salesreport/UiDisplayState;",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "getInitialNohoTimePickerDialogState",
        "Lcom/squareup/register/widgets/NohoTimePickerDialogState;",
        "reportConfig",
        "Lcom/squareup/customreport/data/ReportConfig;",
        "isEditingStartDate",
        "handleCustomizeAllFieldsEvent",
        "Lcom/squareup/workflow/WorkflowAction;",
        "customizeAllFieldsEvent",
        "Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent;",
        "handleEmployeeSelectionEvent",
        "event",
        "Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$EmployeeSelectionEvent;",
        "Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "snapshotState",
        "copyWithValues",
        "T",
        "startDate",
        "Lorg/threeten/bp/LocalDate;",
        "endDate",
        "startTime",
        "Lorg/threeten/bp/LocalTime;",
        "endTime",
        "allDay",
        "thisDeviceOnly",
        "rangeSelection",
        "Lcom/squareup/customreport/data/RangeSelection;",
        "(Lcom/squareup/salesreport/customize/CustomizeReportState;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/customreport/data/RangeSelection;)Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;",
        "newTime",
        "Lcom/squareup/register/widgets/NohoTimePickerDialogResult;",
        "previousTime",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currentTime:Lcom/squareup/time/CurrentTime;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final employees:Lcom/squareup/permissions/Employees;

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final realNohoTimePickerDialogWorkflow:Lcom/squareup/register/widgets/NohoTimePickerDialogWorkflow;

.field private final res:Lcom/squareup/util/Res;

.field private final thisDeviceOnlyFilteredLocalSetting:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/time/CurrentTime;Lcom/squareup/permissions/Employees;Lcom/squareup/settings/LocalSetting;Lcom/squareup/permissions/EmployeeManagement;Ljavax/inject/Provider;Lcom/squareup/register/widgets/NohoTimePickerDialogWorkflow;Lcom/squareup/util/Res;)V
    .locals 1
    .param p3    # Lcom/squareup/settings/LocalSetting;
        .annotation runtime Lcom/squareup/salesreport/settings/SalesReportThisDeviceOnlyFilteredSetting;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/time/CurrentTime;",
            "Lcom/squareup/permissions/Employees;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/register/widgets/NohoTimePickerDialogWorkflow;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "currentTime"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "employees"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "thisDeviceOnlyFilteredLocalSetting"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "employeeManagement"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localeProvider"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "realNohoTimePickerDialogWorkflow"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->currentTime:Lcom/squareup/time/CurrentTime;

    iput-object p2, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->employees:Lcom/squareup/permissions/Employees;

    iput-object p3, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->thisDeviceOnlyFilteredLocalSetting:Lcom/squareup/settings/LocalSetting;

    iput-object p4, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    iput-object p5, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->localeProvider:Ljavax/inject/Provider;

    iput-object p6, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->realNohoTimePickerDialogWorkflow:Lcom/squareup/register/widgets/NohoTimePickerDialogWorkflow;

    iput-object p7, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public static final synthetic access$getEmployeeManagement$p(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;)Lcom/squareup/permissions/EmployeeManagement;
    .locals 0

    .line 76
    iget-object p0, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    return-object p0
.end method

.method public static final synthetic access$getLocaleProvider$p(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;)Ljavax/inject/Provider;
    .locals 0

    .line 76
    iget-object p0, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->localeProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;)Lcom/squareup/util/Res;
    .locals 0

    .line 76
    iget-object p0, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$handleCustomizeAllFieldsEvent(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent;Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 76
    invoke-direct {p0, p1, p2}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->handleCustomizeAllFieldsEvent(Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent;Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$handleEmployeeSelectionEvent(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$EmployeeSelectionEvent;Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 76
    invoke-direct {p0, p1, p2}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->handleEmployeeSelectionEvent(Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$EmployeeSelectionEvent;Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$newTime(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;Lcom/squareup/register/widgets/NohoTimePickerDialogResult;Lorg/threeten/bp/LocalTime;)Lorg/threeten/bp/LocalTime;
    .locals 0

    .line 76
    invoke-direct {p0, p1, p2}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->newTime(Lcom/squareup/register/widgets/NohoTimePickerDialogResult;Lorg/threeten/bp/LocalTime;)Lorg/threeten/bp/LocalTime;

    move-result-object p0

    return-object p0
.end method

.method private final copyWithValues(Lcom/squareup/salesreport/customize/CustomizeReportState;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/customreport/data/RangeSelection;)Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/salesreport/customize/CustomizeReportState;",
            ">(TT;",
            "Lorg/threeten/bp/LocalDate;",
            "Lorg/threeten/bp/LocalDate;",
            "Lorg/threeten/bp/LocalTime;",
            "Lorg/threeten/bp/LocalTime;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/customreport/data/RangeSelection;",
            ")",
            "Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;"
        }
    .end annotation

    .line 354
    new-instance v0, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    .line 355
    invoke-virtual {p1}, Lcom/squareup/salesreport/customize/CustomizeReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v1

    if-eqz p2, :cond_0

    move-object v2, p2

    goto :goto_0

    .line 356
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/salesreport/customize/CustomizeReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/customreport/data/ReportConfig;->getStartDate()Lorg/threeten/bp/LocalDate;

    move-result-object v2

    :goto_0
    if-eqz p3, :cond_1

    move-object/from16 v3, p3

    goto :goto_1

    .line 357
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/salesreport/customize/CustomizeReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/customreport/data/ReportConfig;->getEndDate()Lorg/threeten/bp/LocalDate;

    move-result-object v3

    :goto_1
    if-eqz p4, :cond_2

    move-object/from16 v4, p4

    goto :goto_2

    .line 358
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/salesreport/customize/CustomizeReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/customreport/data/ReportConfig;->getStartTime()Lorg/threeten/bp/LocalTime;

    move-result-object v4

    :goto_2
    if-eqz p5, :cond_3

    move-object/from16 v5, p5

    goto :goto_3

    .line 359
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/salesreport/customize/CustomizeReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/customreport/data/ReportConfig;->getEndTime()Lorg/threeten/bp/LocalTime;

    move-result-object v5

    :goto_3
    if-eqz p6, :cond_4

    .line 360
    invoke-virtual/range {p6 .. p6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    goto :goto_4

    :cond_4
    invoke-virtual {p1}, Lcom/squareup/salesreport/customize/CustomizeReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v6

    invoke-virtual {v6}, Lcom/squareup/customreport/data/ReportConfig;->getAllDay()Z

    move-result v6

    :goto_4
    if-eqz p7, :cond_5

    .line 361
    invoke-virtual/range {p7 .. p7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    goto :goto_5

    :cond_5
    invoke-virtual {p1}, Lcom/squareup/salesreport/customize/CustomizeReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v7

    invoke-virtual {v7}, Lcom/squareup/customreport/data/ReportConfig;->getThisDeviceOnly()Z

    move-result v7

    :goto_5
    const/4 v8, 0x0

    if-eqz p8, :cond_6

    move-object/from16 v9, p8

    goto :goto_6

    .line 362
    :cond_6
    invoke-virtual {p1}, Lcom/squareup/salesreport/customize/CustomizeReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v9

    invoke-virtual {v9}, Lcom/squareup/customreport/data/ReportConfig;->getRangeSelection()Lcom/squareup/customreport/data/RangeSelection;

    move-result-object v9

    :goto_6
    const/4 v10, 0x0

    const/16 v11, 0x140

    const/4 v12, 0x0

    .line 355
    invoke-static/range {v1 .. v12}, Lcom/squareup/customreport/data/ReportConfig;->copy$default(Lcom/squareup/customreport/data/ReportConfig;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;ZZLcom/squareup/customreport/data/EmployeeFiltersSelection;Lcom/squareup/customreport/data/RangeSelection;Lcom/squareup/customreport/data/ComparisonRange;ILjava/lang/Object;)Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v1

    move-object v2, p0

    .line 364
    iget-object v3, v2, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-static {v3}, Lcom/squareup/salesreport/util/EmployeeManagementsKt;->isEnabled(Lcom/squareup/permissions/EmployeeManagement;)Z

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object p1, v0

    move-object p2, v1

    move/from16 p3, v3

    move/from16 p4, v4

    move/from16 p5, v5

    move-object/from16 p6, v6

    .line 354
    invoke-direct/range {p1 .. p6}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;-><init>(Lcom/squareup/customreport/data/ReportConfig;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method static synthetic copyWithValues$default(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;Lcom/squareup/salesreport/customize/CustomizeReportState;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/customreport/data/RangeSelection;ILjava/lang/Object;)Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;
    .locals 8

    if-nez p10, :cond_7

    and-int/lit8 v0, p9, 0x1

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 346
    move-object v0, v1

    check-cast v0, Lorg/threeten/bp/LocalDate;

    goto :goto_0

    :cond_0
    move-object v0, p2

    :goto_0
    and-int/lit8 v2, p9, 0x2

    if-eqz v2, :cond_1

    .line 347
    move-object v2, v1

    check-cast v2, Lorg/threeten/bp/LocalDate;

    goto :goto_1

    :cond_1
    move-object v2, p3

    :goto_1
    and-int/lit8 v3, p9, 0x4

    if-eqz v3, :cond_2

    .line 348
    move-object v3, v1

    check-cast v3, Lorg/threeten/bp/LocalTime;

    goto :goto_2

    :cond_2
    move-object v3, p4

    :goto_2
    and-int/lit8 v4, p9, 0x8

    if-eqz v4, :cond_3

    .line 349
    move-object v4, v1

    check-cast v4, Lorg/threeten/bp/LocalTime;

    goto :goto_3

    :cond_3
    move-object v4, p5

    :goto_3
    and-int/lit8 v5, p9, 0x10

    if-eqz v5, :cond_4

    .line 350
    move-object v5, v1

    check-cast v5, Ljava/lang/Boolean;

    goto :goto_4

    :cond_4
    move-object v5, p6

    :goto_4
    and-int/lit8 v6, p9, 0x20

    if-eqz v6, :cond_5

    .line 351
    move-object v6, v1

    check-cast v6, Ljava/lang/Boolean;

    goto :goto_5

    :cond_5
    move-object v6, p7

    :goto_5
    and-int/lit8 v7, p9, 0x40

    if-eqz v7, :cond_6

    .line 352
    check-cast v1, Lcom/squareup/customreport/data/RangeSelection;

    goto :goto_6

    :cond_6
    move-object/from16 v1, p8

    :goto_6
    move-object p2, p0

    move-object p3, p1

    move-object p4, v0

    move-object p5, v2

    move-object p6, v3

    move-object p7, v4

    move-object/from16 p8, v5

    move-object/from16 p9, v6

    move-object/from16 p10, v1

    invoke-direct/range {p2 .. p10}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->copyWithValues(Lcom/squareup/salesreport/customize/CustomizeReportState;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/customreport/data/RangeSelection;)Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    move-result-object v0

    return-object v0

    .line 0
    :cond_7
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Super calls with default arguments not supported in this target, function: copyWithValues"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private final getCustomizeAllFieldsCardScreen(Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;Lcom/squareup/salesreport/UiDisplayState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;",
            "Lcom/squareup/salesreport/UiDisplayState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/salesreport/customize/CustomizeReportState;",
            "-",
            "Lcom/squareup/salesreport/customize/CustomizeReportResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 298
    sget-object v0, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    const/4 v1, 0x1

    new-array v1, v1, [Lkotlin/Pair;

    sget-object v2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    new-instance v3, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;

    .line 301
    iget-object v4, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-interface {v4}, Lcom/squareup/time/CurrentTime;->localDate()Lorg/threeten/bp/LocalDate;

    move-result-object v4

    .line 302
    new-instance v5, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$getCustomizeAllFieldsCardScreen$1;

    invoke-direct {v5, p0, p3, p1}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$getCustomizeAllFieldsCardScreen$1;-><init>(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;)V

    check-cast v5, Lkotlin/jvm/functions/Function1;

    .line 298
    invoke-direct {v3, p1, p2, v4, v5}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;-><init>(Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;Lcom/squareup/salesreport/UiDisplayState;Lorg/threeten/bp/LocalDate;Lkotlin/jvm/functions/Function1;)V

    check-cast v3, Lcom/squareup/workflow/legacy/V2Screen;

    .line 402
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 403
    const-class p2, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string p3, ""

    invoke-static {p2, p3}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 404
    sget-object p3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p3

    .line 402
    invoke-direct {p1, p2, v3, p3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 298
    invoke-static {v2, p1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p1

    const/4 p2, 0x0

    aput-object p1, v1, p2

    invoke-virtual {v0, v1}, Lcom/squareup/container/PosLayering$Companion;->partial([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method private final getInitialNohoTimePickerDialogState(Lcom/squareup/customreport/data/ReportConfig;Z)Lcom/squareup/register/widgets/NohoTimePickerDialogState;
    .locals 8

    .line 315
    invoke-virtual {p1}, Lcom/squareup/customreport/data/ReportConfig;->getStartDate()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/customreport/data/ReportConfig;->getEndDate()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz p2, :cond_0

    if-eqz v0, :cond_0

    .line 317
    sget-object v1, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->Companion:Lcom/squareup/register/widgets/NohoTimePickerDialogState$Companion;

    .line 318
    invoke-virtual {p1}, Lcom/squareup/customreport/data/ReportConfig;->getStartTime()Lorg/threeten/bp/LocalTime;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/squareup/customreport/data/ReportConfig;->getEndTime()Lorg/threeten/bp/LocalTime;

    move-result-object v4

    const/4 v5, 0x0

    const/16 v6, 0xa

    const/4 v7, 0x0

    .line 317
    invoke-static/range {v1 .. v7}, Lcom/squareup/register/widgets/NohoTimePickerDialogState$Companion;->create$default(Lcom/squareup/register/widgets/NohoTimePickerDialogState$Companion;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;ZILjava/lang/Object;)Lcom/squareup/register/widgets/NohoTimePickerDialogState;

    move-result-object p1

    goto :goto_0

    :cond_0
    if-eqz p2, :cond_1

    if-nez v0, :cond_1

    .line 320
    sget-object v0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->Companion:Lcom/squareup/register/widgets/NohoTimePickerDialogState$Companion;

    .line 321
    invoke-virtual {p1}, Lcom/squareup/customreport/data/ReportConfig;->getStartTime()Lorg/threeten/bp/LocalTime;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xe

    const/4 v6, 0x0

    .line 320
    invoke-static/range {v0 .. v6}, Lcom/squareup/register/widgets/NohoTimePickerDialogState$Companion;->create$default(Lcom/squareup/register/widgets/NohoTimePickerDialogState$Companion;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;ZILjava/lang/Object;)Lcom/squareup/register/widgets/NohoTimePickerDialogState;

    move-result-object p1

    goto :goto_0

    :cond_1
    if-nez p2, :cond_2

    if-eqz v0, :cond_2

    .line 323
    sget-object v0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->Companion:Lcom/squareup/register/widgets/NohoTimePickerDialogState$Companion;

    .line 324
    invoke-virtual {p1}, Lcom/squareup/customreport/data/ReportConfig;->getEndTime()Lorg/threeten/bp/LocalTime;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/customreport/data/ReportConfig;->getStartTime()Lorg/threeten/bp/LocalTime;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    .line 323
    invoke-static/range {v0 .. v6}, Lcom/squareup/register/widgets/NohoTimePickerDialogState$Companion;->create$default(Lcom/squareup/register/widgets/NohoTimePickerDialogState$Companion;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;ZILjava/lang/Object;)Lcom/squareup/register/widgets/NohoTimePickerDialogState;

    move-result-object p1

    goto :goto_0

    .line 326
    :cond_2
    sget-object v0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->Companion:Lcom/squareup/register/widgets/NohoTimePickerDialogState$Companion;

    .line 327
    invoke-virtual {p1}, Lcom/squareup/customreport/data/ReportConfig;->getEndTime()Lorg/threeten/bp/LocalTime;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xe

    const/4 v6, 0x0

    .line 326
    invoke-static/range {v0 .. v6}, Lcom/squareup/register/widgets/NohoTimePickerDialogState$Companion;->create$default(Lcom/squareup/register/widgets/NohoTimePickerDialogState$Companion;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;ZILjava/lang/Object;)Lcom/squareup/register/widgets/NohoTimePickerDialogState;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final handleCustomizeAllFieldsEvent(Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent;Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;)Lcom/squareup/workflow/WorkflowAction;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent;",
            "Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/salesreport/customize/CustomizeReportState;",
            "Lcom/squareup/salesreport/customize/CustomizeReportResult;",
            ">;"
        }
    .end annotation

    .line 174
    instance-of v0, p1, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$SaveReportCustomization;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    new-instance p1, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$1;

    invoke-direct {p1, p0, p2}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$1;-><init>(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v2, p1, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_2

    .line 183
    :cond_0
    instance-of v0, p1, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$GoBackFromCustomizeReport;

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$2;->INSTANCE:Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$2;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v2, p1, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_2

    .line 184
    :cond_1
    instance-of v0, p1, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$StartEditingStartTime;

    if-eqz v0, :cond_2

    new-instance p1, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$3;

    invoke-direct {p1, p2}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$3;-><init>(Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v2, p1, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_2

    .line 190
    :cond_2
    instance-of v0, p1, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$StartEditingEndTime;

    if-eqz v0, :cond_3

    new-instance p1, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$4;

    invoke-direct {p1, p2}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$4;-><init>(Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v2, p1, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_2

    .line 196
    :cond_3
    instance-of v0, p1, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$ChooseDateRange;

    if-eqz v0, :cond_6

    .line 197
    invoke-virtual {p2}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v0

    .line 198
    move-object v3, p1

    check-cast v3, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$ChooseDateRange;

    invoke-virtual {v3}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$ChooseDateRange;->getStartDate()Lorg/threeten/bp/LocalDate;

    move-result-object v4

    invoke-virtual {v3}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$ChooseDateRange;->getEndDate()Lorg/threeten/bp/LocalDate;

    move-result-object v3

    .line 197
    invoke-static {v0, v4, v3}, Lcom/squareup/salesreport/customize/CustomizeReportWorkflowKt;->access$shouldResetTimes(Lcom/squareup/customreport/data/ReportConfig;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 201
    sget-object v3, Lcom/squareup/customreport/data/ReportConfig;->Companion:Lcom/squareup/customreport/data/ReportConfig$Companion;

    invoke-virtual {v3}, Lcom/squareup/customreport/data/ReportConfig$Companion;->getDEFAULT_START_TIME()Lorg/threeten/bp/LocalTime;

    move-result-object v3

    goto :goto_0

    .line 203
    :cond_4
    invoke-virtual {p2}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/customreport/data/ReportConfig;->getStartTime()Lorg/threeten/bp/LocalTime;

    move-result-object v3

    :goto_0
    move-object v8, v3

    if-eqz v0, :cond_5

    .line 206
    sget-object v0, Lcom/squareup/customreport/data/ReportConfig;->Companion:Lcom/squareup/customreport/data/ReportConfig$Companion;

    invoke-virtual {v0}, Lcom/squareup/customreport/data/ReportConfig$Companion;->getDEFAULT_END_TIME()Lorg/threeten/bp/LocalTime;

    move-result-object v0

    goto :goto_1

    .line 208
    :cond_5
    invoke-virtual {p2}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/customreport/data/ReportConfig;->getEndTime()Lorg/threeten/bp/LocalTime;

    move-result-object v0

    :goto_1
    move-object v9, v0

    .line 211
    new-instance v0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$5;

    move-object v4, v0

    move-object v5, p0

    move-object v6, p2

    move-object v7, p1

    invoke-direct/range {v4 .. v9}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$5;-><init>(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v2, v0, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_2

    .line 221
    :cond_6
    instance-of v0, p1, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$ToggleAllDay;

    if-eqz v0, :cond_7

    .line 222
    new-instance p1, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$6;

    invoke-direct {p1, p0, p2}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$6;-><init>(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v2, p1, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_2

    .line 229
    :cond_7
    instance-of v0, p1, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$ChooseDeviceSetting;

    if-eqz v0, :cond_8

    .line 230
    iget-object v0, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->thisDeviceOnlyFilteredLocalSetting:Lcom/squareup/settings/LocalSetting;

    move-object v3, p1

    check-cast v3, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$ChooseDeviceSetting;

    invoke-virtual {v3}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$ChooseDeviceSetting;->getThisDeviceOnly()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v0, v3}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 231
    new-instance v0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$7;

    invoke-direct {v0, p0, p2, p1}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$7;-><init>(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v2, v0, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_2

    .line 235
    :cond_8
    instance-of v0, p1, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$FilterByEmployee;

    if-eqz v0, :cond_9

    new-instance p1, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$8;

    invoke-direct {p1, p2}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$8;-><init>(Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v2, p1, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_2

    .line 239
    :cond_9
    instance-of p1, p1, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$DatePickerInitialized;

    if-eqz p1, :cond_a

    new-instance p1, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$9;

    invoke-direct {p1, p2}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$9;-><init>(Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v2, p1, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_2
    return-object p1

    :cond_a
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final handleEmployeeSelectionEvent(Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$EmployeeSelectionEvent;Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;)Lcom/squareup/workflow/WorkflowAction;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$EmployeeSelectionEvent;",
            "Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/salesreport/customize/CustomizeReportState;",
            "Lcom/squareup/salesreport/customize/CustomizeReportResult;",
            ">;"
        }
    .end annotation

    .line 246
    instance-of v0, p1, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$EmployeeSelectionEvent$FinishEmployeeSelection;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    new-instance p1, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleEmployeeSelectionEvent$1;

    invoke-direct {p1, p0, p2}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleEmployeeSelectionEvent$1;-><init>(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v2, p1, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_2

    .line 247
    :cond_0
    instance-of v0, p1, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$EmployeeSelectionEvent$ToggleEmployeeFilter;

    if-eqz v0, :cond_7

    .line 248
    check-cast p1, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$EmployeeSelectionEvent$ToggleEmployeeFilter;

    invoke-virtual {p1}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$EmployeeSelectionEvent$ToggleEmployeeFilter;->getEmployeeFilter()Lcom/squareup/customreport/data/EmployeeFilter;

    move-result-object v0

    .line 250
    invoke-virtual {p2}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/customreport/data/ReportConfig;->getEmployeeFiltersSelection()Lcom/squareup/customreport/data/EmployeeFiltersSelection;

    move-result-object v3

    .line 252
    instance-of v4, v3, Lcom/squareup/customreport/data/EmployeeFiltersSelection$NoEmployeesSelection;

    if-eqz v4, :cond_1

    invoke-virtual {p1}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$EmployeeSelectionEvent$ToggleEmployeeFilter;->getEmployeeFilter()Lcom/squareup/customreport/data/EmployeeFilter;

    move-result-object p1

    invoke-static {p1}, Lkotlin/collections/SetsKt;->setOf(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p1

    goto :goto_0

    .line 253
    :cond_1
    instance-of v4, v3, Lcom/squareup/customreport/data/EmployeeFiltersSelection$AllEmployeesSelection;

    if-eqz v4, :cond_2

    invoke-virtual {p2}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;->getAllEmployeeFilters()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-virtual {p1}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$EmployeeSelectionEvent$ToggleEmployeeFilter;->getEmployeeFilter()Lcom/squareup/customreport/data/EmployeeFilter;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/collections/CollectionsKt;->minus(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object p1

    goto :goto_0

    .line 254
    :cond_2
    instance-of p1, v3, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;

    if-eqz p1, :cond_6

    check-cast v3, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;

    invoke-virtual {v3}, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;->getEmployeeFilters()Ljava/util/Set;

    move-result-object p1

    .line 255
    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 256
    invoke-static {p1, v0}, Lkotlin/collections/SetsKt;->minus(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p1

    goto :goto_0

    .line 258
    :cond_3
    invoke-static {p1, v0}, Lkotlin/collections/SetsKt;->plus(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p1

    .line 265
    :goto_0
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v0

    if-nez v0, :cond_4

    .line 266
    sget-object p1, Lcom/squareup/customreport/data/EmployeeFiltersSelection$NoEmployeesSelection;->INSTANCE:Lcom/squareup/customreport/data/EmployeeFiltersSelection$NoEmployeesSelection;

    check-cast p1, Lcom/squareup/customreport/data/EmployeeFiltersSelection;

    goto :goto_1

    .line 267
    :cond_4
    invoke-virtual {p2}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;->getAllEmployeeFilters()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v0, v3, :cond_5

    sget-object p1, Lcom/squareup/customreport/data/EmployeeFiltersSelection$AllEmployeesSelection;->INSTANCE:Lcom/squareup/customreport/data/EmployeeFiltersSelection$AllEmployeesSelection;

    check-cast p1, Lcom/squareup/customreport/data/EmployeeFiltersSelection;

    goto :goto_1

    .line 268
    :cond_5
    new-instance v0, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;

    invoke-direct {v0, p1}, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;-><init>(Ljava/util/Set;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/customreport/data/EmployeeFiltersSelection;

    .line 271
    :goto_1
    new-instance v0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleEmployeeSelectionEvent$2;

    invoke-direct {v0, p2, p1}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleEmployeeSelectionEvent$2;-><init>(Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;Lcom/squareup/customreport/data/EmployeeFiltersSelection;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v2, v0, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_2

    .line 254
    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 279
    :cond_7
    instance-of v0, p1, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$EmployeeSelectionEvent$SelectAllEmployees;

    if-eqz v0, :cond_8

    new-instance p1, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleEmployeeSelectionEvent$3;

    invoke-direct {p1, p2}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleEmployeeSelectionEvent$3;-><init>(Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v2, p1, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_2

    .line 284
    :cond_8
    instance-of v0, p1, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$EmployeeSelectionEvent$DeselectAllEmployees;

    if-eqz v0, :cond_9

    new-instance p1, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleEmployeeSelectionEvent$4;

    invoke-direct {p1, p2}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleEmployeeSelectionEvent$4;-><init>(Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v2, p1, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_2

    .line 289
    :cond_9
    instance-of v0, p1, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$EmployeeSelectionEvent$SearchTermChanged;

    if-eqz v0, :cond_a

    new-instance v0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleEmployeeSelectionEvent$5;

    invoke-direct {v0, p0, p2, p1}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleEmployeeSelectionEvent$5;-><init>(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$EmployeeSelectionEvent;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v2, v0, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_2
    return-object p1

    :cond_a
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final newTime(Lcom/squareup/register/widgets/NohoTimePickerDialogResult;Lorg/threeten/bp/LocalTime;)Lorg/threeten/bp/LocalTime;
    .locals 1

    .line 307
    instance-of v0, p1, Lcom/squareup/register/widgets/NohoTimePickerDialogResult$TimeChanged;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/register/widgets/NohoTimePickerDialogResult$TimeChanged;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/NohoTimePickerDialogResult$TimeChanged;->getNewTime()Lorg/threeten/bp/LocalTime;

    move-result-object p2

    :cond_0
    return-object p2
.end method


# virtual methods
.method public initialState(Lcom/squareup/salesreport/customize/CustomizeReportInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/salesreport/customize/CustomizeReportState;
    .locals 12

    const-string p2, "props"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 335
    new-instance p2, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    .line 336
    invoke-virtual {p1}, Lcom/squareup/salesreport/customize/CustomizeReportInput;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v0

    .line 337
    iget-object p1, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->thisDeviceOnlyFilteredLocalSetting:Lcom/squareup/settings/LocalSetting;

    invoke-interface {p1}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object p1

    const-string/jumbo v1, "thisDeviceOnlyFilteredLocalSetting.get()"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x1df

    const/4 v11, 0x0

    .line 336
    invoke-static/range {v0 .. v11}, Lcom/squareup/customreport/data/ReportConfig;->copy$default(Lcom/squareup/customreport/data/ReportConfig;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;ZZLcom/squareup/customreport/data/EmployeeFiltersSelection;Lcom/squareup/customreport/data/RangeSelection;Lcom/squareup/customreport/data/ComparisonRange;ILjava/lang/Object;)Lcom/squareup/customreport/data/ReportConfig;

    move-result-object p1

    .line 339
    iget-object v0, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-static {v0}, Lcom/squareup/salesreport/util/EmployeeManagementsKt;->isEnabled(Lcom/squareup/permissions/EmployeeManagement;)Z

    move-result v0

    const/4 v1, 0x0

    .line 335
    invoke-direct {p2, p1, v0, v1}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;-><init>(Lcom/squareup/customreport/data/ReportConfig;ZZ)V

    check-cast p2, Lcom/squareup/salesreport/customize/CustomizeReportState;

    return-object p2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 76
    check-cast p1, Lcom/squareup/salesreport/customize/CustomizeReportInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->initialState(Lcom/squareup/salesreport/customize/CustomizeReportInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/salesreport/customize/CustomizeReportState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 76
    check-cast p1, Lcom/squareup/salesreport/customize/CustomizeReportInput;

    check-cast p2, Lcom/squareup/salesreport/customize/CustomizeReportState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->render(Lcom/squareup/salesreport/customize/CustomizeReportInput;Lcom/squareup/salesreport/customize/CustomizeReportState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/salesreport/customize/CustomizeReportInput;Lcom/squareup/salesreport/customize/CustomizeReportState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/customize/CustomizeReportInput;",
            "Lcom/squareup/salesreport/customize/CustomizeReportState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/salesreport/customize/CustomizeReportState;",
            "-",
            "Lcom/squareup/salesreport/customize/CustomizeReportResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    instance-of v0, p2, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    if-eqz v0, :cond_0

    .line 94
    check-cast p2, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    invoke-virtual {p1}, Lcom/squareup/salesreport/customize/CustomizeReportInput;->getUiDisplayState()Lcom/squareup/salesreport/UiDisplayState;

    move-result-object p1

    invoke-direct {p0, p2, p1, p3}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->getCustomizeAllFieldsCardScreen(Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;Lcom/squareup/salesreport/UiDisplayState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 96
    :cond_0
    instance-of v0, p2, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadingEmployeeSelection;

    const-string v1, ""

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v0, :cond_1

    .line 98
    iget-object p1, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->employees:Lcom/squareup/permissions/Employees;

    .line 99
    invoke-virtual {p1}, Lcom/squareup/permissions/Employees;->activeEmployees()Lio/reactivex/Observable;

    move-result-object p1

    .line 100
    invoke-virtual {p1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object p1

    .line 101
    new-instance v0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$1;

    invoke-direct {v0, p0}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$1;-><init>(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "employees\n              \u2026        )\n              }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 388
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$$inlined$asWorker$1;

    const/4 v4, 0x0

    invoke-direct {v0, p1, v4}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 389
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 390
    const-class v0, Ljava/util/List;

    sget-object v4, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v5, Lcom/squareup/customreport/data/EmployeeFilter;

    invoke-static {v5}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v5

    invoke-virtual {v4, v5}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v4

    invoke-static {v0, v4}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v4, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v4, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v6, v4

    check-cast v6, Lcom/squareup/workflow/Worker;

    const/4 v7, 0x0

    .line 119
    new-instance p1, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$2;

    invoke-direct {p1, p0, p2}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$2;-><init>(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;Lcom/squareup/salesreport/customize/CustomizeReportState;)V

    move-object v8, p1

    check-cast v8, Lkotlin/jvm/functions/Function1;

    const/4 v9, 0x2

    const/4 v10, 0x0

    move-object v5, p3

    .line 97
    invoke-static/range {v5 .. v10}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 128
    sget-object p1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    new-array v0, v3, [Lkotlin/Pair;

    sget-object v3, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    .line 129
    new-instance v4, Lcom/squareup/salesreport/customize/CustomizeReportScreen$EmployeeSelectionScreen;

    .line 130
    move-object v5, p2

    check-cast v5, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection;

    .line 131
    new-instance v6, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$3;

    invoke-direct {v6, p0, p3, p2}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$3;-><init>(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/salesreport/customize/CustomizeReportState;)V

    check-cast v6, Lkotlin/jvm/functions/Function1;

    .line 129
    invoke-direct {v4, v5, v6}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$EmployeeSelectionScreen;-><init>(Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection;Lkotlin/jvm/functions/Function1;)V

    check-cast v4, Lcom/squareup/workflow/legacy/V2Screen;

    .line 392
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 393
    const-class p3, Lcom/squareup/salesreport/customize/CustomizeReportScreen$EmployeeSelectionScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 394
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 392
    invoke-direct {p2, p3, v4, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 128
    invoke-static {v3, p2}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p2

    aput-object p2, v0, v2

    invoke-virtual {p1, v0}, Lcom/squareup/container/PosLayering$Companion;->partial([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 135
    :cond_1
    instance-of v0, p2, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;

    if-eqz v0, :cond_2

    .line 136
    sget-object p1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    new-array v0, v3, [Lkotlin/Pair;

    sget-object v3, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    .line 137
    new-instance v4, Lcom/squareup/salesreport/customize/CustomizeReportScreen$EmployeeSelectionScreen;

    .line 138
    move-object v5, p2

    check-cast v5, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection;

    .line 139
    new-instance v6, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$4;

    invoke-direct {v6, p0, p3, p2}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$4;-><init>(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/salesreport/customize/CustomizeReportState;)V

    check-cast v6, Lkotlin/jvm/functions/Function1;

    .line 137
    invoke-direct {v4, v5, v6}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$EmployeeSelectionScreen;-><init>(Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection;Lkotlin/jvm/functions/Function1;)V

    check-cast v4, Lcom/squareup/workflow/legacy/V2Screen;

    .line 397
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 398
    const-class p3, Lcom/squareup/salesreport/customize/CustomizeReportScreen$EmployeeSelectionScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 399
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 397
    invoke-direct {p2, p3, v4, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 136
    invoke-static {v3, p2}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p2

    aput-object p2, v0, v2

    invoke-virtual {p1, v0}, Lcom/squareup/container/PosLayering$Companion;->partial([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 142
    :cond_2
    instance-of v0, p2, Lcom/squareup/salesreport/customize/CustomizeReportState$EditingStartTime;

    if-eqz v0, :cond_3

    .line 144
    iget-object v0, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->realNohoTimePickerDialogWorkflow:Lcom/squareup/register/widgets/NohoTimePickerDialogWorkflow;

    move-object v5, v0

    check-cast v5, Lcom/squareup/workflow/Workflow;

    .line 145
    invoke-virtual {p2}, Lcom/squareup/salesreport/customize/CustomizeReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v0

    invoke-direct {p0, v0, v3}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->getInitialNohoTimePickerDialogState(Lcom/squareup/customreport/data/ReportConfig;Z)Lcom/squareup/register/widgets/NohoTimePickerDialogState;

    move-result-object v6

    const/4 v7, 0x0

    .line 146
    new-instance v0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$5;

    invoke-direct {v0, p0, p2}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$5;-><init>(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;Lcom/squareup/salesreport/customize/CustomizeReportState;)V

    move-object v8, v0

    check-cast v8, Lkotlin/jvm/functions/Function1;

    const/4 v9, 0x4

    const/4 v10, 0x0

    move-object v4, p3

    .line 143
    invoke-static/range {v4 .. v10}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 152
    check-cast p2, Lcom/squareup/salesreport/customize/CustomizeReportState$EditingStartTime;

    invoke-virtual {p2}, Lcom/squareup/salesreport/customize/CustomizeReportState$EditingStartTime;->getCustomizeAllFieldsState()Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    move-result-object p2

    invoke-virtual {p1}, Lcom/squareup/salesreport/customize/CustomizeReportInput;->getUiDisplayState()Lcom/squareup/salesreport/UiDisplayState;

    move-result-object p1

    .line 151
    invoke-direct {p0, p2, p1, p3}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->getCustomizeAllFieldsCardScreen(Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;Lcom/squareup/salesreport/UiDisplayState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    .line 150
    invoke-static {v0, p1}, Lkotlin/collections/MapsKt;->plus(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 155
    :cond_3
    instance-of v0, p2, Lcom/squareup/salesreport/customize/CustomizeReportState$EditingEndTime;

    if-eqz v0, :cond_4

    .line 157
    iget-object v0, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->realNohoTimePickerDialogWorkflow:Lcom/squareup/register/widgets/NohoTimePickerDialogWorkflow;

    move-object v4, v0

    check-cast v4, Lcom/squareup/workflow/Workflow;

    .line 158
    invoke-virtual {p2}, Lcom/squareup/salesreport/customize/CustomizeReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v0

    invoke-direct {p0, v0, v2}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->getInitialNohoTimePickerDialogState(Lcom/squareup/customreport/data/ReportConfig;Z)Lcom/squareup/register/widgets/NohoTimePickerDialogState;

    move-result-object v5

    const/4 v6, 0x0

    .line 159
    new-instance v0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$6;

    invoke-direct {v0, p0, p2}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$6;-><init>(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;Lcom/squareup/salesreport/customize/CustomizeReportState;)V

    move-object v7, v0

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v3, p3

    .line 156
    invoke-static/range {v3 .. v9}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 165
    check-cast p2, Lcom/squareup/salesreport/customize/CustomizeReportState$EditingEndTime;

    invoke-virtual {p2}, Lcom/squareup/salesreport/customize/CustomizeReportState$EditingEndTime;->getCustomizeAllFieldsState()Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    move-result-object p2

    invoke-virtual {p1}, Lcom/squareup/salesreport/customize/CustomizeReportInput;->getUiDisplayState()Lcom/squareup/salesreport/UiDisplayState;

    move-result-object p1

    .line 164
    invoke-direct {p0, p2, p1, p3}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->getCustomizeAllFieldsCardScreen(Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;Lcom/squareup/salesreport/UiDisplayState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    .line 163
    invoke-static {v0, p1}, Lkotlin/collections/MapsKt;->plus(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/salesreport/customize/CustomizeReportState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 343
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 76
    check-cast p1, Lcom/squareup/salesreport/customize/CustomizeReportState;

    invoke-virtual {p0, p1}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->snapshotState(Lcom/squareup/salesreport/customize/CustomizeReportState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
