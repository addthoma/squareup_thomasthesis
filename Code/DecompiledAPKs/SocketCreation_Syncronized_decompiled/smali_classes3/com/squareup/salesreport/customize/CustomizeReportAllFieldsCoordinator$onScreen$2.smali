.class final Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$2;
.super Lkotlin/jvm/internal/Lambda;
.source "CustomizeReportAllFieldsCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator;->onScreen(Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;


# direct methods
.method constructor <init>(Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$2;->$screen:Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 47
    invoke-virtual {p0}, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$2;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$onScreen$2;->$screen:Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;

    invoke-virtual {v0}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    sget-object v1, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$GoBackFromCustomizeReport;->INSTANCE:Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$GoBackFromCustomizeReport;

    invoke-interface {v0, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
