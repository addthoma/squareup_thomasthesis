.class public final Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealCustomizeReportWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/Employees;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/NohoTimePickerDialogWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/Employees;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/NohoTimePickerDialogWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 42
    iput-object p6, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 43
    iput-object p7, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow_Factory;->arg6Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/Employees;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/NohoTimePickerDialogWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow_Factory;"
        }
    .end annotation

    .line 55
    new-instance v8, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/time/CurrentTime;Lcom/squareup/permissions/Employees;Lcom/squareup/settings/LocalSetting;Lcom/squareup/permissions/EmployeeManagement;Ljavax/inject/Provider;Lcom/squareup/register/widgets/NohoTimePickerDialogWorkflow;Lcom/squareup/util/Res;)Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/time/CurrentTime;",
            "Lcom/squareup/permissions/Employees;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/register/widgets/NohoTimePickerDialogWorkflow;",
            "Lcom/squareup/util/Res;",
            ")",
            "Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;"
        }
    .end annotation

    .line 61
    new-instance v8, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;-><init>(Lcom/squareup/time/CurrentTime;Lcom/squareup/permissions/Employees;Lcom/squareup/settings/LocalSetting;Lcom/squareup/permissions/EmployeeManagement;Ljavax/inject/Provider;Lcom/squareup/register/widgets/NohoTimePickerDialogWorkflow;Lcom/squareup/util/Res;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;
    .locals 8

    .line 48
    iget-object v0, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/time/CurrentTime;

    iget-object v0, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/permissions/Employees;

    iget-object v0, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/settings/LocalSetting;

    iget-object v0, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v5, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/register/widgets/NohoTimePickerDialogWorkflow;

    iget-object v0, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/util/Res;

    invoke-static/range {v1 .. v7}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow_Factory;->newInstance(Lcom/squareup/time/CurrentTime;Lcom/squareup/permissions/Employees;Lcom/squareup/settings/LocalSetting;Lcom/squareup/permissions/EmployeeManagement;Ljavax/inject/Provider;Lcom/squareup/register/widgets/NohoTimePickerDialogWorkflow;Lcom/squareup/util/Res;)Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow_Factory;->get()Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;

    move-result-object v0

    return-object v0
.end method
