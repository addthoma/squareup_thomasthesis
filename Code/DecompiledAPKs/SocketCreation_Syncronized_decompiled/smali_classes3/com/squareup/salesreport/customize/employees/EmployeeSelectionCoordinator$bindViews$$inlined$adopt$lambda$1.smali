.class public final Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$bindViews$$inlined$adopt$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RecyclerNoho.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->bindViews(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function3<",
        "Ljava/lang/Integer;",
        "Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$EmployeeFilterRow;",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRecyclerNoho.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RecyclerNoho.kt\ncom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$5\n+ 2 EmployeeSelectionCoordinator.kt\ncom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator\n+ 3 Views.kt\ncom/squareup/util/Views\n*L\n1#1,173:1\n170#2,3:174\n175#2:184\n1103#3,7:177\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\n\u0008\u0001\u0010\u0004\u0018\u0001*\u0002H\u00022\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u0002H\u00042\u0006\u0010\u0008\u001a\u00020\tH\n\u00a2\u0006\u0004\u0008\n\u0010\u000b\u00a8\u0006\r"
    }
    d2 = {
        "<anonymous>",
        "",
        "I",
        "",
        "S",
        "<anonymous parameter 0>",
        "",
        "item",
        "row",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "invoke",
        "(ILjava/lang/Object;Lcom/squareup/noho/NohoCheckableRow;)V",
        "com/squareup/noho/dsl/RecyclerNohoKt$nohoRow$5",
        "com/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$$special$$inlined$nohoRow$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;


# direct methods
.method public constructor <init>(Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$bindViews$$inlined$adopt$lambda$1;->this$0:Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;

    const/4 p1, 0x3

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p3, Lcom/squareup/noho/NohoCheckableRow;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$bindViews$$inlined$adopt$lambda$1;->invoke(ILjava/lang/Object;Lcom/squareup/noho/NohoCheckableRow;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ILjava/lang/Object;Lcom/squareup/noho/NohoCheckableRow;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$EmployeeFilterRow;",
            "Lcom/squareup/noho/NohoCheckableRow;",
            ")V"
        }
    .end annotation

    const-string p1, "item"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "row"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    check-cast p2, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$EmployeeFilterRow;

    .line 174
    invoke-virtual {p2}, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$EmployeeFilterRow;->getEmployeeFilter()Lcom/squareup/customreport/data/EmployeeFilter;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$bindViews$$inlined$adopt$lambda$1;->this$0:Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;

    invoke-static {v0}, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->access$getRes$p(Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;)Lcom/squareup/util/Res;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/salesreport/util/EmployeeFiltersKt;->displayValue(Lcom/squareup/customreport/data/EmployeeFilter;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p3, p1}, Lcom/squareup/noho/NohoCheckableRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 175
    invoke-virtual {p2}, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$EmployeeFilterRow;->isSelected()Z

    move-result p1

    invoke-virtual {p3, p1}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 176
    check-cast p3, Landroid/view/View;

    .line 177
    new-instance p1, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$bindViews$$inlined$adopt$lambda$1$1;

    invoke-direct {p1, p2, p0}, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$bindViews$$inlined$adopt$lambda$1$1;-><init>(Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$EmployeeFilterRow;Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$bindViews$$inlined$adopt$lambda$1;)V

    check-cast p1, Landroid/view/View$OnClickListener;

    invoke-virtual {p3, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
