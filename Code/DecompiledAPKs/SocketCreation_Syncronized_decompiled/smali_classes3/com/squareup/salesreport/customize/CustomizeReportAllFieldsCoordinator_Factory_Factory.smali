.class public final Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator_Factory_Factory;
.super Ljava/lang/Object;
.source "CustomizeReportAllFieldsCoordinator_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/util/LocalTimeFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/util/LocalTimeFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator_Factory_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator_Factory_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 26
    iput-object p3, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator_Factory_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator_Factory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/util/LocalTimeFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator_Factory_Factory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator_Factory_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/salesreport/util/LocalTimeFormatter;Lio/reactivex/Scheduler;Lcom/squareup/util/Res;)Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$Factory;
    .locals 1

    .line 42
    new-instance v0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$Factory;-><init>(Lcom/squareup/salesreport/util/LocalTimeFormatter;Lio/reactivex/Scheduler;Lcom/squareup/util/Res;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$Factory;
    .locals 3

    .line 31
    iget-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator_Factory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/salesreport/util/LocalTimeFormatter;

    iget-object v1, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator_Factory_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/reactivex/Scheduler;

    iget-object v2, p0, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator_Factory_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Res;

    invoke-static {v0, v1, v2}, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator_Factory_Factory;->newInstance(Lcom/squareup/salesreport/util/LocalTimeFormatter;Lio/reactivex/Scheduler;Lcom/squareup/util/Res;)Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator_Factory_Factory;->get()Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method
