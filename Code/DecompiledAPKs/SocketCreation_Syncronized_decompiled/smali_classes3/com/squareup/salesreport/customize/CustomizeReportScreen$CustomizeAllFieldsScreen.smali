.class public final Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;
.super Lcom/squareup/salesreport/customize/CustomizeReportScreen;
.source "CustomizeReportScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/salesreport/customize/CustomizeReportScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CustomizeAllFieldsScreen"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u000f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B1\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0012\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\t\u00a2\u0006\u0002\u0010\u000cJ\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0007H\u00c6\u0003J\u0015\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\tH\u00c6\u0003J=\u0010\u0019\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0014\u0008\u0002\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\tH\u00c6\u0001J\u0013\u0010\u001a\u001a\u00020\u001b2\u0008\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u00d6\u0003J\t\u0010\u001e\u001a\u00020\u001fH\u00d6\u0001J\t\u0010 \u001a\u00020!H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u001d\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;",
        "Lcom/squareup/salesreport/customize/CustomizeReportScreen;",
        "state",
        "Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;",
        "uiDisplayState",
        "Lcom/squareup/salesreport/UiDisplayState;",
        "latestSelectableDate",
        "Lorg/threeten/bp/LocalDate;",
        "onEvent",
        "Lkotlin/Function1;",
        "Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent;",
        "",
        "(Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;Lcom/squareup/salesreport/UiDisplayState;Lorg/threeten/bp/LocalDate;Lkotlin/jvm/functions/Function1;)V",
        "getLatestSelectableDate",
        "()Lorg/threeten/bp/LocalDate;",
        "getOnEvent",
        "()Lkotlin/jvm/functions/Function1;",
        "getState",
        "()Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;",
        "getUiDisplayState",
        "()Lcom/squareup/salesreport/UiDisplayState;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final latestSelectableDate:Lorg/threeten/bp/LocalDate;

.field private final onEvent:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final state:Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

.field private final uiDisplayState:Lcom/squareup/salesreport/UiDisplayState;


# direct methods
.method public constructor <init>(Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;Lcom/squareup/salesreport/UiDisplayState;Lorg/threeten/bp/LocalDate;Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;",
            "Lcom/squareup/salesreport/UiDisplayState;",
            "Lorg/threeten/bp/LocalDate;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "uiDisplayState"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "latestSelectableDate"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    move-object v0, p1

    check-cast v0, Lcom/squareup/salesreport/customize/CustomizeReportState;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/squareup/salesreport/customize/CustomizeReportScreen;-><init>(Lcom/squareup/salesreport/customize/CustomizeReportState;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->state:Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    iput-object p2, p0, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->uiDisplayState:Lcom/squareup/salesreport/UiDisplayState;

    iput-object p3, p0, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->latestSelectableDate:Lorg/threeten/bp/LocalDate;

    iput-object p4, p0, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;Lcom/squareup/salesreport/UiDisplayState;Lorg/threeten/bp/LocalDate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    invoke-virtual {p0}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->getState()Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    move-result-object p1

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->uiDisplayState:Lcom/squareup/salesreport/UiDisplayState;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->latestSelectableDate:Lorg/threeten/bp/LocalDate;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->copy(Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;Lcom/squareup/salesreport/UiDisplayState;Lorg/threeten/bp/LocalDate;Lkotlin/jvm/functions/Function1;)Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->getState()Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/squareup/salesreport/UiDisplayState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->uiDisplayState:Lcom/squareup/salesreport/UiDisplayState;

    return-object v0
.end method

.method public final component3()Lorg/threeten/bp/LocalDate;
    .locals 1

    iget-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->latestSelectableDate:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public final component4()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final copy(Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;Lcom/squareup/salesreport/UiDisplayState;Lorg/threeten/bp/LocalDate;Lkotlin/jvm/functions/Function1;)Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;",
            "Lcom/squareup/salesreport/UiDisplayState;",
            "Lorg/threeten/bp/LocalDate;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "uiDisplayState"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "latestSelectableDate"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;-><init>(Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;Lcom/squareup/salesreport/UiDisplayState;Lorg/threeten/bp/LocalDate;Lkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;

    invoke-virtual {p0}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->getState()Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->getState()Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->uiDisplayState:Lcom/squareup/salesreport/UiDisplayState;

    iget-object v1, p1, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->uiDisplayState:Lcom/squareup/salesreport/UiDisplayState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->latestSelectableDate:Lorg/threeten/bp/LocalDate;

    iget-object v1, p1, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->latestSelectableDate:Lorg/threeten/bp/LocalDate;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    iget-object p1, p1, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getLatestSelectableDate()Lorg/threeten/bp/LocalDate;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->latestSelectableDate:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public final getOnEvent()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 20
    iget-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public getState()Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->state:Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    return-object v0
.end method

.method public bridge synthetic getState()Lcom/squareup/salesreport/customize/CustomizeReportState;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->getState()Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    move-result-object v0

    check-cast v0, Lcom/squareup/salesreport/customize/CustomizeReportState;

    return-object v0
.end method

.method public final getUiDisplayState()Lcom/squareup/salesreport/UiDisplayState;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->uiDisplayState:Lcom/squareup/salesreport/UiDisplayState;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->getState()Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->uiDisplayState:Lcom/squareup/salesreport/UiDisplayState;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->latestSelectableDate:Lorg/threeten/bp/LocalDate;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CustomizeAllFieldsScreen(state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->getState()Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", uiDisplayState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->uiDisplayState:Lcom/squareup/salesreport/UiDisplayState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", latestSelectableDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->latestSelectableDate:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onEvent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeAllFieldsScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
