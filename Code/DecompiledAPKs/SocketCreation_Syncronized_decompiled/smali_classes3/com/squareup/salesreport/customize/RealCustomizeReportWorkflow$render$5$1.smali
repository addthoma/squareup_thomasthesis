.class final Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$5$1;
.super Lkotlin/jvm/internal/Lambda;
.source "CustomizeReportWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$5;->invoke(Lcom/squareup/register/widgets/NohoTimePickerDialogResult;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Updater<",
        "Lcom/squareup/salesreport/customize/CustomizeReportState;",
        "-",
        "Lcom/squareup/salesreport/customize/CustomizeReportResult;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/salesreport/customize/CustomizeReportState;",
        "Lcom/squareup/salesreport/customize/CustomizeReportResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $it:Lcom/squareup/register/widgets/NohoTimePickerDialogResult;

.field final synthetic this$0:Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$5;


# direct methods
.method constructor <init>(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$5;Lcom/squareup/register/widgets/NohoTimePickerDialogResult;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$5$1;->this$0:Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$5;

    iput-object p2, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$5$1;->$it:Lcom/squareup/register/widgets/NohoTimePickerDialogResult;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 76
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Updater;

    invoke-virtual {p0, p1}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$5$1;->invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/salesreport/customize/CustomizeReportState;",
            "-",
            "Lcom/squareup/salesreport/customize/CustomizeReportResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    iget-object v0, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$5$1;->this$0:Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$5;

    iget-object v1, v0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$5;->this$0:Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;

    iget-object v0, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$5$1;->this$0:Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$5;

    iget-object v2, v0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$5;->$state:Lcom/squareup/salesreport/customize/CustomizeReportState;

    iget-object v0, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$5$1;->this$0:Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$5;

    iget-object v0, v0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$5;->this$0:Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;

    iget-object v3, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$5$1;->$it:Lcom/squareup/register/widgets/NohoTimePickerDialogResult;

    iget-object v4, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$5$1;->this$0:Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$5;

    iget-object v4, v4, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$render$5;->$state:Lcom/squareup/salesreport/customize/CustomizeReportState;

    invoke-virtual {v4}, Lcom/squareup/salesreport/customize/CustomizeReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/customreport/data/ReportConfig;->getStartTime()Lorg/threeten/bp/LocalTime;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->access$newTime(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;Lcom/squareup/register/widgets/NohoTimePickerDialogResult;Lorg/threeten/bp/LocalTime;)Lorg/threeten/bp/LocalTime;

    move-result-object v5

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x7b

    const/4 v11, 0x0

    invoke-static/range {v1 .. v11}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->copyWithValues$default(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;Lcom/squareup/salesreport/customize/CustomizeReportState;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/customreport/data/RangeSelection;ILjava/lang/Object;)Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void
.end method
