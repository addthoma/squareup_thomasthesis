.class final Lcom/squareup/cycler/Recycler$Adapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "Recycler.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cycler/Recycler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Adapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<I:",
        "Ljava/lang/Object;",
        ">",
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/cycler/Recycler$ViewHolder<",
        "+",
        "Landroid/view/View;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRecycler.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Recycler.kt\ncom/squareup/cycler/Recycler$Adapter\n+ 2 RecyclerData.kt\ncom/squareup/cycler/RecyclerData\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,606:1\n57#2,3:607\n60#2,3:617\n63#2:627\n57#2,7:628\n57#2,7:635\n300#3,7:610\n300#3,7:620\n*E\n*S KotlinDebug\n*F\n+ 1 Recycler.kt\ncom/squareup/cycler/Recycler$Adapter\n*L\n439#1,3:607\n439#1,3:617\n439#1:627\n483#1,7:628\n497#1,7:635\n439#1,7:610\n439#1,7:620\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\\\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0007\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0002\u0018\u0000*\u0008\u0008\u0001\u0010\u0001*\u00020\u00022\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u00040\u0003B\u001b\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00028\u00010\t\u00a2\u0006\u0002\u0010\nJ\u0008\u0010\u001d\u001a\u00020\u001eH\u0016J\u0010\u0010\u001f\u001a\u00020\u001b2\u0006\u0010 \u001a\u00020\u001eH\u0016J\u0010\u0010!\u001a\u00020\u001e2\u0006\u0010 \u001a\u00020\u001eH\u0016J\u0018\u0010\"\u001a\u00020\u001e2\u0006\u0010#\u001a\u00020\u001e2\u0006\u0010$\u001a\u00020\u001eH\u0002J\u001e\u0010%\u001a\u00020&2\u000c\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010 \u001a\u00020\u001eH\u0016J\u001e\u0010(\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020\u001eH\u0016J\u000c\u0010#\u001a\u00020\u001e*\u00020\u001eH\u0002J\u000c\u0010$\u001a\u00020\u001e*\u00020\u001eH\u0002R\u0017\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00028\u00010\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR \u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00028\u00010\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\"\u0004\u0008\u0013\u0010\u0014R\u001c\u0010\u0015\u001a\n\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u0016X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018R$\u0010\u0019\u001a\u0018\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00020\u001b0\u001aj\u0008\u0012\u0004\u0012\u00028\u0001`\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006,"
    }
    d2 = {
        "Lcom/squareup/cycler/Recycler$Adapter;",
        "I",
        "",
        "Landroidx/recyclerview/widget/RecyclerView$Adapter;",
        "Lcom/squareup/cycler/Recycler$ViewHolder;",
        "Landroid/view/View;",
        "creatorContext",
        "Lcom/squareup/cycler/Recycler$CreatorContext;",
        "config",
        "Lcom/squareup/cycler/Recycler$Config;",
        "(Lcom/squareup/cycler/Recycler$CreatorContext;Lcom/squareup/cycler/Recycler$Config;)V",
        "getConfig",
        "()Lcom/squareup/cycler/Recycler$Config;",
        "getCreatorContext",
        "()Lcom/squareup/cycler/Recycler$CreatorContext;",
        "currentRecyclerData",
        "Lcom/squareup/cycler/RecyclerData;",
        "getCurrentRecyclerData",
        "()Lcom/squareup/cycler/RecyclerData;",
        "setCurrentRecyclerData",
        "(Lcom/squareup/cycler/RecyclerData;)V",
        "itemComparator",
        "Lcom/squareup/cycler/ItemComparator;",
        "getItemComparator$lib_release",
        "()Lcom/squareup/cycler/ItemComparator;",
        "stableIdProvider",
        "Lkotlin/Function1;",
        "",
        "Lcom/squareup/cycler/StableIdProvider;",
        "getItemCount",
        "",
        "getItemId",
        "position",
        "getItemViewType",
        "makeViewType",
        "rowType",
        "subType",
        "onBindViewHolder",
        "",
        "holder",
        "onCreateViewHolder",
        "parent",
        "Landroid/view/ViewGroup;",
        "viewType",
        "lib_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field private final config:Lcom/squareup/cycler/Recycler$Config;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler$Config<",
            "TI;>;"
        }
    .end annotation
.end field

.field private final creatorContext:Lcom/squareup/cycler/Recycler$CreatorContext;

.field private currentRecyclerData:Lcom/squareup/cycler/RecyclerData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/RecyclerData<",
            "TI;>;"
        }
    .end annotation
.end field

.field private final itemComparator:Lcom/squareup/cycler/ItemComparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/ItemComparator<",
            "TI;>;"
        }
    .end annotation
.end field

.field private final stableIdProvider:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "TI;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cycler/Recycler$CreatorContext;Lcom/squareup/cycler/Recycler$Config;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/Recycler$CreatorContext;",
            "Lcom/squareup/cycler/Recycler$Config<",
            "TI;>;)V"
        }
    .end annotation

    const-string v0, "creatorContext"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "config"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 408
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    iput-object p1, p0, Lcom/squareup/cycler/Recycler$Adapter;->creatorContext:Lcom/squareup/cycler/Recycler$CreatorContext;

    iput-object p2, p0, Lcom/squareup/cycler/Recycler$Adapter;->config:Lcom/squareup/cycler/Recycler$Config;

    .line 414
    iget-object p1, p0, Lcom/squareup/cycler/Recycler$Adapter;->config:Lcom/squareup/cycler/Recycler$Config;

    invoke-virtual {p1}, Lcom/squareup/cycler/Recycler$Config;->getHasStableIdProvider$lib_release()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/cycler/Recycler$Adapter;->setHasStableIds(Z)V

    .line 415
    iget-object p1, p0, Lcom/squareup/cycler/Recycler$Adapter;->config:Lcom/squareup/cycler/Recycler$Config;

    invoke-virtual {p1}, Lcom/squareup/cycler/Recycler$Config;->getStableIdProvider$lib_release()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cycler/Recycler$Adapter;->stableIdProvider:Lkotlin/jvm/functions/Function1;

    .line 417
    iget-object p1, p0, Lcom/squareup/cycler/Recycler$Adapter;->config:Lcom/squareup/cycler/Recycler$Config;

    invoke-virtual {p1}, Lcom/squareup/cycler/Recycler$Config;->createItemComparator$lib_release()Lcom/squareup/cycler/ItemComparator;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cycler/Recycler$Adapter;->itemComparator:Lcom/squareup/cycler/ItemComparator;

    .line 420
    sget-object p1, Lcom/squareup/cycler/RecyclerData;->Companion:Lcom/squareup/cycler/RecyclerData$Companion;

    invoke-virtual {p1}, Lcom/squareup/cycler/RecyclerData$Companion;->empty()Lcom/squareup/cycler/RecyclerData;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cycler/Recycler$Adapter;->currentRecyclerData:Lcom/squareup/cycler/RecyclerData;

    return-void
.end method

.method private final makeViewType(II)I
    .locals 0

    shl-int/lit8 p1, p1, 0x8

    or-int/2addr p1, p2

    return p1
.end method

.method private final rowType(I)I
    .locals 0

    shr-int/lit8 p1, p1, 0x8

    return p1
.end method

.method private final subType(I)I
    .locals 0

    and-int/lit16 p1, p1, 0xff

    return p1
.end method


# virtual methods
.method public final getConfig()Lcom/squareup/cycler/Recycler$Config;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/cycler/Recycler$Config<",
            "TI;>;"
        }
    .end annotation

    .line 407
    iget-object v0, p0, Lcom/squareup/cycler/Recycler$Adapter;->config:Lcom/squareup/cycler/Recycler$Config;

    return-object v0
.end method

.method public final getCreatorContext()Lcom/squareup/cycler/Recycler$CreatorContext;
    .locals 1

    .line 406
    iget-object v0, p0, Lcom/squareup/cycler/Recycler$Adapter;->creatorContext:Lcom/squareup/cycler/Recycler$CreatorContext;

    return-object v0
.end method

.method public final getCurrentRecyclerData()Lcom/squareup/cycler/RecyclerData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/cycler/RecyclerData<",
            "TI;>;"
        }
    .end annotation

    .line 420
    iget-object v0, p0, Lcom/squareup/cycler/Recycler$Adapter;->currentRecyclerData:Lcom/squareup/cycler/RecyclerData;

    return-object v0
.end method

.method public final getItemComparator$lib_release()Lcom/squareup/cycler/ItemComparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/cycler/ItemComparator<",
            "TI;>;"
        }
    .end annotation

    .line 411
    iget-object v0, p0, Lcom/squareup/cycler/Recycler$Adapter;->itemComparator:Lcom/squareup/cycler/ItemComparator;

    return-object v0
.end method

.method public getItemCount()I
    .locals 1

    .line 477
    iget-object v0, p0, Lcom/squareup/cycler/Recycler$Adapter;->currentRecyclerData:Lcom/squareup/cycler/RecyclerData;

    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getTotalCount()I

    move-result v0

    return v0
.end method

.method public getItemId(I)J
    .locals 4

    .line 497
    iget-object v0, p0, Lcom/squareup/cycler/Recycler$Adapter;->currentRecyclerData:Lcom/squareup/cycler/RecyclerData;

    .line 636
    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getExtraItemIndex()I

    move-result v1

    const/16 v2, 0x29

    const-string v3, "getItemId for invalid position ("

    if-ne p1, v1, :cond_2

    .line 637
    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getHasExtraItem()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getExtraItem()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    goto :goto_0

    .line 501
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 640
    :cond_2
    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/cycler/DataSource;->getSize()I

    move-result v1

    if-ltz p1, :cond_3

    if-le v1, p1, :cond_3

    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cycler/DataSource;->get(I)Ljava/lang/Object;

    move-result-object p1

    .line 499
    iget-object v0, p0, Lcom/squareup/cycler/Recycler$Adapter;->stableIdProvider:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    :goto_0
    return-wide v0

    .line 501
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public getItemViewType(I)I
    .locals 6

    .line 439
    iget-object v0, p0, Lcom/squareup/cycler/Recycler$Adapter;->currentRecyclerData:Lcom/squareup/cycler/RecyclerData;

    .line 608
    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getExtraItemIndex()I

    move-result v1

    const/4 v2, -0x1

    const/4 v3, 0x0

    const/16 v4, 0x29

    const-string v5, "getItemViewType for invalid position ("

    if-ne p1, v1, :cond_4

    .line 609
    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getHasExtraItem()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getExtraItem()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 447
    :cond_0
    iget-object v0, p0, Lcom/squareup/cycler/Recycler$Adapter;->config:Lcom/squareup/cycler/Recycler$Config;

    invoke-virtual {v0}, Lcom/squareup/cycler/Recycler$Config;->getExtraItemSpecs()Ljava/util/List;

    move-result-object v0

    .line 611
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 612
    check-cast v1, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 447
    invoke-virtual {v1, p1}, Lcom/squareup/cycler/Recycler$RowSpec;->matches(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v2, v3

    goto :goto_1

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    const/4 p1, 0x1

    add-int/2addr p1, v2

    neg-int p1, p1

    return p1

    .line 450
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 451
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 450
    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 619
    :cond_4
    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/cycler/DataSource;->getSize()I

    move-result v1

    if-ltz p1, :cond_7

    if-le v1, p1, :cond_7

    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cycler/DataSource;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 442
    iget-object v1, p0, Lcom/squareup/cycler/Recycler$Adapter;->config:Lcom/squareup/cycler/Recycler$Config;

    invoke-virtual {v1}, Lcom/squareup/cycler/Recycler$Config;->getRowSpecs()Ljava/util/List;

    move-result-object v1

    .line 621
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 622
    check-cast v4, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 442
    invoke-virtual {v4, v0}, Lcom/squareup/cycler/Recycler$RowSpec;->matches(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    move v2, v3

    goto :goto_3

    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 443
    :cond_6
    :goto_3
    iget-object v1, p0, Lcom/squareup/cycler/Recycler$Adapter;->config:Lcom/squareup/cycler/Recycler$Config;

    invoke-virtual {v1}, Lcom/squareup/cycler/Recycler$Config;->getRowSpecs()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cycler/Recycler$RowSpec;

    invoke-virtual {v1, p1, v0}, Lcom/squareup/cycler/Recycler$RowSpec;->subType(ILjava/lang/Object;)I

    move-result p1

    .line 444
    invoke-direct {p0, v2, p1}, Lcom/squareup/cycler/Recycler$Adapter;->makeViewType(II)I

    move-result p1

    return p1

    .line 450
    :cond_7
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 451
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 450
    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 405
    check-cast p1, Lcom/squareup/cycler/Recycler$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cycler/Recycler$Adapter;->onBindViewHolder(Lcom/squareup/cycler/Recycler$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/cycler/Recycler$ViewHolder;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/Recycler$ViewHolder<",
            "+",
            "Landroid/view/View;",
            ">;I)V"
        }
    .end annotation

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 483
    iget-object v0, p0, Lcom/squareup/cycler/Recycler$Adapter;->currentRecyclerData:Lcom/squareup/cycler/RecyclerData;

    .line 629
    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getExtraItemIndex()I

    move-result v1

    const/16 v2, 0x29

    const-string v3, "onBindViewHolder for invalid position ("

    if-ne p2, v1, :cond_1

    .line 630
    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getHasExtraItem()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getExtraItem()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    goto :goto_0

    .line 488
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 489
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 488
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 633
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/cycler/DataSource;->getSize()I

    move-result v1

    if-ltz p2, :cond_3

    if-le v1, p2, :cond_3

    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/squareup/cycler/DataSource;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 493
    :cond_2
    :goto_0
    invoke-virtual {p1, p2, v0}, Lcom/squareup/cycler/Recycler$ViewHolder;->bind(ILjava/lang/Object;)V

    return-void

    .line 488
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 489
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 488
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 405
    invoke-virtual {p0, p1, p2}, Lcom/squareup/cycler/Recycler$Adapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/cycler/Recycler$ViewHolder;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/cycler/Recycler$ViewHolder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Lcom/squareup/cycler/Recycler$ViewHolder<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-gez p2, :cond_0

    .line 428
    iget-object p1, p0, Lcom/squareup/cycler/Recycler$Adapter;->config:Lcom/squareup/cycler/Recycler$Config;

    invoke-virtual {p1}, Lcom/squareup/cycler/Recycler$Config;->getExtraItemSpecs()Ljava/util/List;

    move-result-object p1

    add-int/lit8 p2, p2, 0x1

    neg-int p2, p2

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 429
    iget-object p2, p0, Lcom/squareup/cycler/Recycler$Adapter;->creatorContext:Lcom/squareup/cycler/Recycler$CreatorContext;

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Lcom/squareup/cycler/Recycler$RowSpec;->createViewHolder(Lcom/squareup/cycler/Recycler$CreatorContext;I)Lcom/squareup/cycler/Recycler$ViewHolder;

    move-result-object p1

    goto :goto_0

    .line 431
    :cond_0
    invoke-direct {p0, p2}, Lcom/squareup/cycler/Recycler$Adapter;->rowType(I)I

    move-result p1

    .line 432
    invoke-direct {p0, p2}, Lcom/squareup/cycler/Recycler$Adapter;->subType(I)I

    move-result p2

    .line 433
    iget-object v0, p0, Lcom/squareup/cycler/Recycler$Adapter;->config:Lcom/squareup/cycler/Recycler$Config;

    invoke-virtual {v0}, Lcom/squareup/cycler/Recycler$Config;->getRowSpecs()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cycler/Recycler$RowSpec;

    iget-object v0, p0, Lcom/squareup/cycler/Recycler$Adapter;->creatorContext:Lcom/squareup/cycler/Recycler$CreatorContext;

    invoke-virtual {p1, v0, p2}, Lcom/squareup/cycler/Recycler$RowSpec;->createViewHolder(Lcom/squareup/cycler/Recycler$CreatorContext;I)Lcom/squareup/cycler/Recycler$ViewHolder;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public final setCurrentRecyclerData(Lcom/squareup/cycler/RecyclerData;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/RecyclerData<",
            "TI;>;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 420
    iput-object p1, p0, Lcom/squareup/cycler/Recycler$Adapter;->currentRecyclerData:Lcom/squareup/cycler/RecyclerData;

    return-void
.end method
