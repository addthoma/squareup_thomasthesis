.class public final Lcom/squareup/cycler/mosaic/MosaicRowSpecKt;
.super Ljava/lang/Object;
.source "MosaicRowSpec.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMosaicRowSpec.kt\nKotlin\n*S Kotlin\n*F\n+ 1 MosaicRowSpec.kt\ncom/squareup/cycler/mosaic/MosaicRowSpecKt\n+ 2 MosaicRowSpec.kt\ncom/squareup/cycler/mosaic/MosaicRowSpec\n*L\n1#1,198:1\n88#1:199\n89#1:202\n88#1,2:203\n135#2,2:200\n*E\n*S KotlinDebug\n*F\n+ 1 MosaicRowSpec.kt\ncom/squareup/cycler/mosaic/MosaicRowSpecKt\n*L\n33#1:199\n33#1:202\n55#1,2:203\n33#1,2:200\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0000\u001aN\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\n\u0008\u0001\u0010\u0004\u0018\u0001*\u0002H\u0002*\u0008\u0012\u0004\u0012\u0002H\u00020\u00052#\u0010\u0006\u001a\u001f\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00040\u0008\u0012\u0004\u0012\u00020\u00010\u0007\u00a2\u0006\u0002\u0008\tH\u0086\u0008\u001aP\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\n\u0008\u0001\u0010\u0004\u0018\u0001*\u0002H\u0002*\u0008\u0012\u0004\u0012\u0002H\u00020\u00052%\u0008\u0008\u0010\u0006\u001a\u001f\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00010\u000b\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u00020\u00010\n\u00a2\u0006\u0002\u0008\tH\u0086\u0008\u001aV\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\n\u0008\u0001\u0010\u0004\u0018\u0001*\u0002H\u0002*\u0008\u0012\u0004\u0012\u0002H\u00020\u00052+\u0008\u0008\u0010\u0006\u001a%\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00010\u000b\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u00020\u00010\u000c\u00a2\u0006\u0002\u0008\tH\u0086\u0008\u00a8\u0006\u000e"
    }
    d2 = {
        "mosaicRow",
        "",
        "I",
        "",
        "S",
        "Lcom/squareup/cycler/Recycler$Config;",
        "block",
        "Lkotlin/Function1;",
        "Lcom/squareup/cycler/mosaic/MosaicRowSpec;",
        "Lkotlin/ExtensionFunctionType;",
        "Lkotlin/Function2;",
        "Lcom/squareup/mosaic/core/UiModelContext;",
        "Lkotlin/Function3;",
        "",
        "recycler-mosaic_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic mosaicRow(Lcom/squareup/cycler/Recycler$Config;Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "S::TI;>(",
            "Lcom/squareup/cycler/Recycler$Config<",
            "TI;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/cycler/mosaic/MosaicRowSpec<",
            "TI;TS;>;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$mosaicRow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    new-instance v0, Lcom/squareup/cycler/mosaic/MosaicRowSpec;

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->needClassReification()V

    sget-object v1, Lcom/squareup/cycler/mosaic/MosaicRowSpecKt$mosaicRow$3;->INSTANCE:Lcom/squareup/cycler/mosaic/MosaicRowSpecKt$mosaicRow$3;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v1}, Lcom/squareup/cycler/mosaic/MosaicRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    invoke-virtual {p0, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    return-void
.end method

.method public static final synthetic mosaicRow(Lcom/squareup/cycler/Recycler$Config;Lkotlin/jvm/functions/Function2;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "S::TI;>(",
            "Lcom/squareup/cycler/Recycler$Config<",
            "TI;>;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/squareup/mosaic/core/UiModelContext<",
            "Lkotlin/Unit;",
            ">;-TS;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$mosaicRow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 199
    new-instance v0, Lcom/squareup/cycler/mosaic/MosaicRowSpec;

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->needClassReification()V

    sget-object v1, Lcom/squareup/cycler/mosaic/MosaicRowSpecKt$mosaicRow$$inlined$mosaicRow$1;->INSTANCE:Lcom/squareup/cycler/mosaic/MosaicRowSpecKt$mosaicRow$$inlined$mosaicRow$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v1}, Lcom/squareup/cycler/mosaic/MosaicRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 200
    new-instance v1, Lcom/squareup/cycler/mosaic/MosaicRowSpec$model$1;

    invoke-direct {v1, p1}, Lcom/squareup/cycler/mosaic/MosaicRowSpec$model$1;-><init>(Lkotlin/jvm/functions/Function2;)V

    check-cast v1, Lkotlin/jvm/functions/Function3;

    invoke-virtual {v0, v1}, Lcom/squareup/cycler/mosaic/MosaicRowSpec;->model(Lkotlin/jvm/functions/Function3;)V

    .line 35
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 199
    invoke-virtual {p0, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    return-void
.end method

.method public static final synthetic mosaicRow(Lcom/squareup/cycler/Recycler$Config;Lkotlin/jvm/functions/Function3;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "S::TI;>(",
            "Lcom/squareup/cycler/Recycler$Config<",
            "TI;>;",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Lcom/squareup/mosaic/core/UiModelContext<",
            "Lkotlin/Unit;",
            ">;-",
            "Ljava/lang/Integer;",
            "-TS;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$mosaicRow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 203
    new-instance v0, Lcom/squareup/cycler/mosaic/MosaicRowSpec;

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->needClassReification()V

    sget-object v1, Lcom/squareup/cycler/mosaic/MosaicRowSpecKt$mosaicRow$$inlined$mosaicRow$2;->INSTANCE:Lcom/squareup/cycler/mosaic/MosaicRowSpecKt$mosaicRow$$inlined$mosaicRow$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v1}, Lcom/squareup/cycler/mosaic/MosaicRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 56
    invoke-virtual {v0, p1}, Lcom/squareup/cycler/mosaic/MosaicRowSpec;->model(Lkotlin/jvm/functions/Function3;)V

    .line 57
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 203
    invoke-virtual {p0, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    return-void
.end method
