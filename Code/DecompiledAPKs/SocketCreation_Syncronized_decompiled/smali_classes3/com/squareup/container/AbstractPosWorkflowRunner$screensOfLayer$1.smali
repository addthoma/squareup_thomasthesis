.class final Lcom/squareup/container/AbstractPosWorkflowRunner$screensOfLayer$1;
.super Ljava/lang/Object;
.source "AbstractPosWorkflowRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/AbstractPosWorkflowRunner;->screensOfLayer(Lcom/squareup/container/ContainerLayering;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Predicate<",
        "Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot<",
        "+TP;+",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/ContainerLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAbstractPosWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AbstractPosWorkflowRunner.kt\ncom/squareup/container/AbstractPosWorkflowRunner$screensOfLayer$1\n*L\n1#1,525:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0003\"\u0004\u0008\u0002\u0010\u000528\u0010\u0006\u001a4\u0012\u0004\u0012\u0002H\u0002\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\t\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000b0\u0008j\n\u0012\u0006\u0008\u0001\u0012\u00020\t`\u000c0\u0007H\n\u00a2\u0006\u0002\u0008\r"
    }
    d2 = {
        "<anonymous>",
        "",
        "P",
        "",
        "O",
        "R",
        "it",
        "Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "test"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $layer:Lcom/squareup/container/ContainerLayering;


# direct methods
.method constructor <init>(Lcom/squareup/container/ContainerLayering;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$screensOfLayer$1;->$layer:Lcom/squareup/container/ContainerLayering;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final test(Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot<",
            "+TP;+",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;)Z"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 492
    invoke-virtual {p1}, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->getRendering()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$screensOfLayer$1;->$layer:Lcom/squareup/container/ContainerLayering;

    if-eqz p1, :cond_0

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type kotlin.collections.Map<K, *>"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public bridge synthetic test(Ljava/lang/Object;)Z
    .locals 0

    .line 140
    check-cast p1, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;

    invoke-virtual {p0, p1}, Lcom/squareup/container/AbstractPosWorkflowRunner$screensOfLayer$1;->test(Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;)Z

    move-result p1

    return p1
.end method
