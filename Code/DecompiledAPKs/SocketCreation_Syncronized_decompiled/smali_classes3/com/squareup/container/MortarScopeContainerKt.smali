.class public final Lcom/squareup/container/MortarScopeContainerKt;
.super Ljava/lang/Object;
.source "MortarScopeContainer.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMortarScopeContainer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 MortarScopeContainer.kt\ncom/squareup/container/MortarScopeContainerKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,95:1\n732#2,9:96\n*E\n*S KotlinDebug\n*F\n+ 1 MortarScopeContainer.kt\ncom/squareup/container/MortarScopeContainerKt\n*L\n72#1,9:96\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0010 \n\u0000\u001a\u000e\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0001\u001a\u0014\u0010\u0007\u001a\u00020\u0008*\u00020\u00012\u0006\u0010\t\u001a\u00020\nH\u0000\u001a\r\u0010\u000b\u001a\u00020\u0008*\u00020\u0001H\u0086\u0010\u001a\u0014\u0010\u000c\u001a\u00020\r*\u00020\u00012\u0006\u0010\u000e\u001a\u00020\u000fH\u0002\u001a\u000c\u0010\u0010\u001a\u00020\r*\u00020\u0001H\u0002\u001a\u0012\u0010\u0011\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\t\u001a\u00020\n\u001a\u001e\u0010\u0012\u001a\u0004\u0018\u00010\u0001*\u00020\u00012\u0006\u0010\t\u001a\u00020\n2\u0008\u0008\u0002\u0010\u0013\u001a\u00020\r\u001a%\u0010\u0014\u001a\u0004\u0018\u00010\u0001*\u00020\u00012\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\n0\u00162\u0006\u0010\u0013\u001a\u00020\rH\u0082\u0010\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00018BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0002\u0010\u0003\u00a8\u0006\u0017"
    }
    d2 = {
        "parent",
        "Lmortar/MortarScope;",
        "getParent",
        "(Lmortar/MortarScope;)Lmortar/MortarScope;",
        "executeAndPruneBootstrapTreeKey",
        "Lcom/squareup/container/RedirectStep;",
        "workflowScope",
        "bootstrap",
        "",
        "key",
        "Lcom/squareup/container/ContainerTreeKey;",
        "destroyAndReapEmptyAncestors",
        "hasChild",
        "",
        "name",
        "",
        "hasChildren",
        "scopeForKey",
        "scopeForKeyOrNull",
        "startScopes",
        "scopeForPathOrNull",
        "path",
        "",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final bootstrap(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;)V
    .locals 3

    const-string v0, "$this$bootstrap"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 56
    invoke-static {p0, p1, v0, v1, v2}, Lcom/squareup/container/MortarScopeContainerKt;->scopeForKeyOrNull$default(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;ZILjava/lang/Object;)Lmortar/MortarScope;

    move-result-object p0

    if-nez p0, :cond_0

    const/4 v0, 0x1

    :cond_0
    if-eqz v0, :cond_1

    return-void

    .line 57
    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Bootstrap scope "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p0, " was expected to self destruct."

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public static final destroyAndReapEmptyAncestors(Lmortar/MortarScope;)V
    .locals 3

    :goto_0
    const-string v0, "$this$destroyAndReapEmptyAncestors"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-static {p0}, Lcom/squareup/container/MortarScopeContainerKt;->hasChildren(Lmortar/MortarScope;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_1

    .line 33
    invoke-virtual {p0}, Lmortar/MortarScope;->destroy()V

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 34
    invoke-virtual {p0}, Lmortar/MortarScope;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "Destroyed scope: %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 35
    invoke-static {p0}, Lcom/squareup/container/MortarScopeContainerKt;->getParent(Lmortar/MortarScope;)Lmortar/MortarScope;

    move-result-object v0

    invoke-virtual {v0}, Lmortar/MortarScope;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/squareup/container/MortarScopeContainerKt;->getParent(Lmortar/MortarScope;)Lmortar/MortarScope;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/container/MortarScopeContainerKt;->hasChildren(Lmortar/MortarScope;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/squareup/container/MortarScopeContainerKt;->getParent(Lmortar/MortarScope;)Lmortar/MortarScope;

    move-result-object p0

    goto :goto_0

    :cond_0
    return-void

    .line 32
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Don\'t know how to deal with a screen with child scope(s) ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p0, ")."

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public static final executeAndPruneBootstrapTreeKey(Lmortar/MortarScope;)Lcom/squareup/container/RedirectStep;
    .locals 1

    const-string/jumbo v0, "workflowScope"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    new-instance v0, Lcom/squareup/container/MortarScopeContainerKt$executeAndPruneBootstrapTreeKey$1;

    invoke-direct {v0, p0}, Lcom/squareup/container/MortarScopeContainerKt$executeAndPruneBootstrapTreeKey$1;-><init>(Lmortar/MortarScope;)V

    check-cast v0, Lcom/squareup/container/RedirectStep;

    return-object v0
.end method

.method private static final getParent(Lmortar/MortarScope;)Lmortar/MortarScope;
    .locals 1

    .line 38
    invoke-static {p0}, Lmortar/ScopeSpy;->parentScope(Lmortar/MortarScope;)Lmortar/MortarScope;

    move-result-object p0

    const-string v0, "ScopeSpy.parentScope(this)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method private static final hasChild(Lmortar/MortarScope;Ljava/lang/String;)Z
    .locals 0

    .line 42
    invoke-virtual {p0, p1}, Lmortar/MortarScope;->findChild(Ljava/lang/String;)Lmortar/MortarScope;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static final hasChildren(Lmortar/MortarScope;)Z
    .locals 0

    .line 40
    invoke-static {p0}, Lmortar/ScopeSpy;->hasChildren(Lmortar/MortarScope;)Z

    move-result p0

    return p0
.end method

.method public static final scopeForKey(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;)Lmortar/MortarScope;
    .locals 3

    const-string v0, "$this$scopeForKey"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 49
    invoke-static {p0, p1, v0, v1, v2}, Lcom/squareup/container/MortarScopeContainerKt;->scopeForKeyOrNull$default(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;ZILjava/lang/Object;)Lmortar/MortarScope;

    move-result-object p0

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Scope for "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " should not self destruct."

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public static final scopeForKeyOrNull(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;Z)Lmortar/MortarScope;
    .locals 3

    const-string v0, "$this$scopeForKeyOrNull"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    invoke-virtual {p1}, Lcom/squareup/container/ContainerTreeKey;->getElements()Ljava/util/List;

    move-result-object p1

    const-string v0, "key.elements"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 103
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Lcom/squareup/container/ContainerTreeKey;

    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 104
    :cond_1
    check-cast v0, Ljava/util/List;

    .line 72
    invoke-static {p0, v0, p2}, Lcom/squareup/container/MortarScopeContainerKt;->scopeForPathOrNull(Lmortar/MortarScope;Ljava/util/List;Z)Lmortar/MortarScope;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic scopeForKeyOrNull$default(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;ZILjava/lang/Object;)Lmortar/MortarScope;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x1

    .line 70
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/squareup/container/MortarScopeContainerKt;->scopeForKeyOrNull(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;Z)Lmortar/MortarScope;

    move-result-object p0

    return-object p0
.end method

.method private static final scopeForPathOrNull(Lmortar/MortarScope;Ljava/util/List;Z)Lmortar/MortarScope;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmortar/MortarScope;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;Z)",
            "Lmortar/MortarScope;"
        }
    .end annotation

    .line 79
    :goto_0
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eqz p2, :cond_0

    .line 81
    invoke-virtual {v0}, Lcom/squareup/container/ContainerTreeKey;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "key.name"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, v4}, Lcom/squareup/container/MortarScopeContainerKt;->hasChild(Lmortar/MortarScope;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    new-array v4, v2, [Ljava/lang/Object;

    .line 82
    invoke-virtual {v0}, Lcom/squareup/container/ContainerTreeKey;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {p0}, Lmortar/MortarScope;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v3

    const-string v5, "Building scope %s from %s"

    invoke-static {v5, v4}, Ltimber/log/Timber;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 83
    invoke-virtual {v0, p0}, Lcom/squareup/container/ContainerTreeKey;->buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;

    move-result-object v4

    .line 84
    invoke-virtual {v0}, Lcom/squareup/container/ContainerTreeKey;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lmortar/MortarScope$Builder;->build(Ljava/lang/String;)Lmortar/MortarScope;

    :cond_0
    new-array v2, v2, [Ljava/lang/Object;

    .line 86
    invoke-virtual {v0}, Lcom/squareup/container/ContainerTreeKey;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v1

    invoke-virtual {p0}, Lmortar/MortarScope;->getName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    const-string v1, "Finding scope %s in %s"

    invoke-static {v1, v2}, Ltimber/log/Timber;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 87
    invoke-virtual {v0}, Lcom/squareup/container/ContainerTreeKey;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmortar/MortarScope;->findChild(Ljava/lang/String;)Lmortar/MortarScope;

    move-result-object p0

    if-nez p0, :cond_1

    const/4 p0, 0x0

    goto :goto_1

    .line 91
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v3, :cond_2

    :goto_1
    return-object p0

    .line 92
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1, v3, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    goto :goto_0
.end method
