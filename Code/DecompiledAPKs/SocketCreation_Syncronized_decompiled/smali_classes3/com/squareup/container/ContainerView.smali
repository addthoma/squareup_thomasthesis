.class public interface abstract Lcom/squareup/container/ContainerView;
.super Ljava/lang/Object;
.source "ContainerView.java"

# interfaces
.implements Lcom/squareup/container/CanInterceptInputEvents;


# virtual methods
.method public abstract getCardLayout()Landroid/view/ViewGroup;
.end method

.method public abstract getCardOverSheetLayout()Landroid/view/ViewGroup;
.end method

.method public abstract getContentLayout()Landroid/view/ViewGroup;
.end method

.method public abstract getContext()Landroid/content/Context;
.end method

.method public abstract getFullSheetLayout()Landroid/view/ViewGroup;
.end method

.method public abstract getMasterLayout()Landroid/view/ViewGroup;
.end method

.method public abstract setCardOverSheetVisible(ZZLflow/TraversalCallback;)V
.end method

.method public abstract setCardVisible(ZZLflow/TraversalCallback;)V
.end method

.method public abstract setFullSheetVisible(ZZLflow/TraversalCallback;)V
.end method
