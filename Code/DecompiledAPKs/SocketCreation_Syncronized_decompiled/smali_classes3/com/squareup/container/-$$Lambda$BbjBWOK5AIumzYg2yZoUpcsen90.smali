.class public final synthetic Lcom/squareup/container/-$$Lambda$BbjBWOK5AIumzYg2yZoUpcsen90;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lcom/squareup/container/RedirectStep;


# static fields
.field public static final synthetic INSTANCE:Lcom/squareup/container/-$$Lambda$BbjBWOK5AIumzYg2yZoUpcsen90;


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/container/-$$Lambda$BbjBWOK5AIumzYg2yZoUpcsen90;

    invoke-direct {v0}, Lcom/squareup/container/-$$Lambda$BbjBWOK5AIumzYg2yZoUpcsen90;-><init>()V

    sput-object v0, Lcom/squareup/container/-$$Lambda$BbjBWOK5AIumzYg2yZoUpcsen90;->INSTANCE:Lcom/squareup/container/-$$Lambda$BbjBWOK5AIumzYg2yZoUpcsen90;

    return-void
.end method

.method private synthetic constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final maybeRedirect(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 0

    invoke-static {p1}, Lcom/squareup/container/CalculatedKey;->redirectForCalculatedKey(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;

    move-result-object p1

    return-object p1
.end method
