.class final Lcom/squareup/container/MortarScopeContainerKt$executeAndPruneBootstrapTreeKey$1;
.super Ljava/lang/Object;
.source "MortarScopeContainer.kt"

# interfaces
.implements Lcom/squareup/container/RedirectStep;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/MortarScopeContainerKt;->executeAndPruneBootstrapTreeKey(Lmortar/MortarScope;)Lcom/squareup/container/RedirectStep;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMortarScopeContainer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 MortarScopeContainer.kt\ncom/squareup/container/MortarScopeContainerKt$executeAndPruneBootstrapTreeKey$1\n*L\n1#1,95:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0004\u0018\u00010\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/container/RedirectStep$Result;",
        "traversal",
        "Lflow/Traversal;",
        "kotlin.jvm.PlatformType",
        "maybeRedirect"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $workflowScope:Lmortar/MortarScope;


# direct methods
.method constructor <init>(Lmortar/MortarScope;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/container/MortarScopeContainerKt$executeAndPruneBootstrapTreeKey$1;->$workflowScope:Lmortar/MortarScope;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final maybeRedirect(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 5

    .line 14
    iget-object v0, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v0}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/squareup/container/BootstrapTreeKey;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/squareup/container/BootstrapTreeKey;

    if-eqz v0, :cond_2

    .line 16
    iget-object v1, p0, Lcom/squareup/container/MortarScopeContainerKt$executeAndPruneBootstrapTreeKey$1;->$workflowScope:Lmortar/MortarScope;

    move-object v2, v0

    check-cast v2, Lcom/squareup/container/ContainerTreeKey;

    invoke-static {v1, v2}, Lcom/squareup/container/MortarScopeContainerKt;->bootstrap(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;)V

    .line 17
    iget-object v1, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v1}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object v1

    .line 19
    invoke-virtual {v1}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    .line 20
    invoke-virtual {v1}, Lflow/History$Builder;->isEmpty()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    .line 21
    invoke-virtual {v1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object v1

    .line 23
    new-instance v2, Lcom/squareup/container/RedirectStep$Result;

    .line 24
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invoked and pruned "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 25
    iget-object p1, p1, Lflow/Traversal;->direction:Lflow/Direction;

    invoke-static {v1, p1}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p1

    .line 23
    invoke-direct {v2, v0, p1}, Lcom/squareup/container/RedirectStep$Result;-><init>(Ljava/lang/String;Lcom/squareup/container/Command;)V

    goto :goto_0

    .line 20
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Uh oh. I didn\'t think this ever happened."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_2
    :goto_0
    return-object v2
.end method
