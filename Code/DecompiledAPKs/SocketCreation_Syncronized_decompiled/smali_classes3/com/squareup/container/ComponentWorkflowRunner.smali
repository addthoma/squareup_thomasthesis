.class public Lcom/squareup/container/ComponentWorkflowRunner;
.super Lcom/squareup/container/AbstractV1PosWorkflowRunner;
.source "ComponentWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/container/ComponentWorkflowRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<C:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        "O:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/container/AbstractV1PosWorkflowRunner<",
        "TE;TO;>;"
    }
.end annotation

.annotation runtime Lkotlin/Deprecated;
    message = "Use this only if you need to run a legacy workflow. Use ComponentWorkflowV2Runner for your v2 workflows."
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0017\u0018\u0000 $*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u0002*\u0008\u0008\u0002\u0010\u0004*\u00020\u00022\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0005:\u0001$B\u0087\u0001\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u000c\u0012\u001c\u0010\r\u001a\u0018\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u000f0\u000ej\u0008\u0012\u0004\u0012\u00028\u0000`\u0010\u00124\u0010\u0011\u001a0\u0012\u0004\u0012\u00028\u0000\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u00120\u000ej\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002`\u0013\u0012\u0008\u0008\u0002\u0010\u0014\u001a\u00020\u0015\u00a2\u0006\u0002\u0010\u0016J\u0010\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020#H\u0015R<\u0010\u0011\u001a0\u0012\u0004\u0012\u00028\u0000\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u00120\u000ej\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002`\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0017\u001a\u00028\u0000X\u0082.\u00a2\u0006\u0004\n\u0002\u0010\u0018R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u00128VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001a\u0010\u001bR\u001e\u0010\u001d\u001a\u00020\u000f2\u0006\u0010\u001c\u001a\u00020\u000f@BX\u0084.\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u001fR$\u0010\r\u001a\u0018\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u000f0\u000ej\u0008\u0012\u0004\u0012\u00028\u0000`\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/container/ComponentWorkflowRunner;",
        "C",
        "",
        "E",
        "O",
        "Lcom/squareup/container/AbstractV1PosWorkflowRunner;",
        "serviceName",
        "",
        "nextHistory",
        "Lio/reactivex/Observable;",
        "Lflow/History;",
        "componentClass",
        "Ljava/lang/Class;",
        "viewFactoryFactory",
        "Lkotlin/Function1;",
        "Lcom/squareup/workflow/WorkflowViewFactory;",
        "Lcom/squareup/container/WorkflowComponentViewFactory;",
        "_starter",
        "Lcom/squareup/container/PosWorkflowStarter;",
        "Lcom/squareup/container/PosWorkflowComponentStarter;",
        "mainDispatcher",
        "Lkotlinx/coroutines/CoroutineDispatcher;",
        "(Ljava/lang/String;Lio/reactivex/Observable;Ljava/lang/Class;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlinx/coroutines/CoroutineDispatcher;)V",
        "component",
        "Ljava/lang/Object;",
        "starter",
        "getStarter",
        "()Lcom/squareup/container/PosWorkflowStarter;",
        "<set-?>",
        "viewFactory",
        "getViewFactory",
        "()Lcom/squareup/workflow/WorkflowViewFactory;",
        "onEnterScope",
        "",
        "newScope",
        "Lmortar/MortarScope;",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/container/ComponentWorkflowRunner$Companion;


# instance fields
.field private final _starter:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "TC;",
            "Lcom/squareup/container/PosWorkflowStarter<",
            "TE;TO;>;>;"
        }
    .end annotation
.end field

.field private component:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TC;"
        }
    .end annotation
.end field

.field private final componentClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "TC;>;"
        }
    .end annotation
.end field

.field private viewFactory:Lcom/squareup/workflow/WorkflowViewFactory;

.field private final viewFactoryFactory:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "TC;",
            "Lcom/squareup/workflow/WorkflowViewFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/container/ComponentWorkflowRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/container/ComponentWorkflowRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/container/ComponentWorkflowRunner;->Companion:Lcom/squareup/container/ComponentWorkflowRunner$Companion;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lio/reactivex/Observable;Ljava/lang/Class;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlinx/coroutines/CoroutineDispatcher;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lio/reactivex/Observable<",
            "Lflow/History;",
            ">;",
            "Ljava/lang/Class<",
            "TC;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TC;+",
            "Lcom/squareup/workflow/WorkflowViewFactory;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-TC;+",
            "Lcom/squareup/container/PosWorkflowStarter<",
            "TE;+TO;>;>;",
            "Lkotlinx/coroutines/CoroutineDispatcher;",
            ")V"
        }
    .end annotation

    const-string v0, "serviceName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nextHistory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "componentClass"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "viewFactoryFactory"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "_starter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainDispatcher"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    invoke-direct {p0, p1, p2, p6}, Lcom/squareup/container/AbstractV1PosWorkflowRunner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lkotlinx/coroutines/CoroutineDispatcher;)V

    iput-object p3, p0, Lcom/squareup/container/ComponentWorkflowRunner;->componentClass:Ljava/lang/Class;

    iput-object p4, p0, Lcom/squareup/container/ComponentWorkflowRunner;->viewFactoryFactory:Lkotlin/jvm/functions/Function1;

    iput-object p5, p0, Lcom/squareup/container/ComponentWorkflowRunner;->_starter:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lio/reactivex/Observable;Ljava/lang/Class;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p7, p7, 0x20

    if-eqz p7, :cond_0

    .line 130
    invoke-static {}, Lcom/squareup/container/CoroutineDispatcherKt;->getMainImmediateDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object p6

    :cond_0
    move-object v6, p6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/container/ComponentWorkflowRunner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Ljava/lang/Class;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlinx/coroutines/CoroutineDispatcher;)V

    return-void
.end method

.method public static final get(Lmortar/MortarScope;Ljava/lang/String;)Lcom/squareup/container/ComponentWorkflowRunner;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C:",
            "Ljava/lang/Object;",
            "I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lmortar/MortarScope;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/container/ComponentWorkflowRunner<",
            "TC;TI;TO;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/container/ComponentWorkflowRunner;->Companion:Lcom/squareup/container/ComponentWorkflowRunner$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/container/ComponentWorkflowRunner$Companion;->get(Lmortar/MortarScope;Ljava/lang/String;)Lcom/squareup/container/ComponentWorkflowRunner;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public getStarter()Lcom/squareup/container/PosWorkflowStarter;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/container/PosWorkflowStarter<",
            "TE;TO;>;"
        }
    .end annotation

    .line 166
    iget-object v0, p0, Lcom/squareup/container/ComponentWorkflowRunner;->_starter:Lkotlin/jvm/functions/Function1;

    iget-object v1, p0, Lcom/squareup/container/ComponentWorkflowRunner;->component:Ljava/lang/Object;

    if-nez v1, :cond_0

    const-string v2, "component"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/PosWorkflowStarter;

    return-object v0
.end method

.method protected final getViewFactory()Lcom/squareup/workflow/WorkflowViewFactory;
    .locals 2

    .line 157
    iget-object v0, p0, Lcom/squareup/container/ComponentWorkflowRunner;->viewFactory:Lcom/squareup/workflow/WorkflowViewFactory;

    if-nez v0, :cond_0

    const-string/jumbo v1, "viewFactory"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "newScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 161
    iget-object v0, p0, Lcom/squareup/container/ComponentWorkflowRunner;->componentClass:Ljava/lang/Class;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/container/ComponentWorkflowRunner;->component:Ljava/lang/Object;

    .line 162
    iget-object v0, p0, Lcom/squareup/container/ComponentWorkflowRunner;->viewFactoryFactory:Lkotlin/jvm/functions/Function1;

    iget-object v1, p0, Lcom/squareup/container/ComponentWorkflowRunner;->component:Ljava/lang/Object;

    if-nez v1, :cond_0

    const-string v2, "component"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/WorkflowViewFactory;

    iput-object v0, p0, Lcom/squareup/container/ComponentWorkflowRunner;->viewFactory:Lcom/squareup/workflow/WorkflowViewFactory;

    .line 163
    invoke-super {p0, p1}, Lcom/squareup/container/AbstractV1PosWorkflowRunner;->onEnterScope(Lmortar/MortarScope;)V

    return-void
.end method
