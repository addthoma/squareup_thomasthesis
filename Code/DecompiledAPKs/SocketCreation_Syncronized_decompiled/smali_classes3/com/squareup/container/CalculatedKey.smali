.class public final Lcom/squareup/container/CalculatedKey;
.super Lcom/squareup/container/ContainerTreeKey;
.source "CalculatedKey.java"

# interfaces
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;
    }
.end annotation


# instance fields
.field private final debugName:Ljava/lang/String;

.field private final historyToCommand:Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;

.field precalcOrigin:Lflow/History;


# direct methods
.method public constructor <init>(Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V
    .locals 1

    .line 111
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/squareup/container/CalculatedKey;-><init>(Ljava/lang/String;Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V
    .locals 1

    .line 114
    invoke-direct {p0}, Lcom/squareup/container/ContainerTreeKey;-><init>()V

    const-string v0, "historyToCommand"

    .line 115
    invoke-static {p2, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;

    iput-object p2, p0, Lcom/squareup/container/CalculatedKey;->historyToCommand:Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;

    const-string p2, "debugName"

    .line 116
    invoke-static {p1, p2}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/container/CalculatedKey;->debugName:Ljava/lang/String;

    return-void
.end method

.method static synthetic lambda$processorForTests$0(Lflow/Traversal;)Lflow/Traversal;
    .locals 1

    .line 41
    sget-object v0, Lcom/squareup/container/-$$Lambda$BbjBWOK5AIumzYg2yZoUpcsen90;->INSTANCE:Lcom/squareup/container/-$$Lambda$BbjBWOK5AIumzYg2yZoUpcsen90;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/squareup/container/RedirectPipelineFactoryKt;->process(Ljava/util/List;Lflow/Traversal;)Lflow/Traversal;

    move-result-object p0

    return-object p0
.end method

.method public static processorForTests()Lflow/Processor;
    .locals 1

    .line 41
    sget-object v0, Lcom/squareup/container/-$$Lambda$CalculatedKey$CPVPfVmxLtebidboKA0JAL2YeK8;->INSTANCE:Lcom/squareup/container/-$$Lambda$CalculatedKey$CPVPfVmxLtebidboKA0JAL2YeK8;

    return-object v0
.end method

.method static redirectForCalculatedKey(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 4

    .line 54
    iget-object v0, p0, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v0}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/container/CalculatedKey;

    if-nez v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 58
    :cond_0
    iget-object v0, p0, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v0}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object v0

    .line 59
    invoke-virtual {v0}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/container/CalculatedKey;

    .line 60
    invoke-virtual {v0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object v0

    .line 62
    iget-object v2, v1, Lcom/squareup/container/CalculatedKey;->historyToCommand:Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;

    invoke-interface {v2, v0}, Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;->call(Lflow/History;)Lcom/squareup/container/Command;

    move-result-object v2

    .line 65
    iget-object v3, p0, Lflow/Traversal;->origin:Lflow/History;

    invoke-virtual {v3}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v3

    instance-of v3, v3, Lcom/squareup/container/CalculatedKey;

    if-eqz v3, :cond_1

    .line 66
    iget-object v3, p0, Lflow/Traversal;->origin:Lflow/History;

    invoke-virtual {v3}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/container/CalculatedKey;

    .line 67
    iget-object v3, v3, Lcom/squareup/container/CalculatedKey;->precalcOrigin:Lflow/History;

    iput-object v3, v1, Lcom/squareup/container/CalculatedKey;->precalcOrigin:Lflow/History;

    goto :goto_0

    .line 69
    :cond_1
    iget-object v3, p0, Lflow/Traversal;->origin:Lflow/History;

    iput-object v3, v1, Lcom/squareup/container/CalculatedKey;->precalcOrigin:Lflow/History;

    :goto_0
    if-nez v2, :cond_2

    .line 75
    iget-object p0, p0, Lflow/Traversal;->direction:Lflow/Direction;

    invoke-static {v0, p0}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object v2

    .line 78
    :cond_2
    new-instance p0, Lcom/squareup/container/RedirectStep$Result;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CalculatedKey: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, v1, Lcom/squareup/container/CalculatedKey;->debugName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v2}, Lcom/squareup/container/RedirectStep$Result;-><init>(Ljava/lang/String;Lcom/squareup/container/Command;)V

    return-object p0
.end method

.method public static redirectForCalculatedKeyInTest(Lflow/Traversal;)Z
    .locals 0

    .line 49
    invoke-static {p0}, Lcom/squareup/container/CalculatedKey;->redirectForCalculatedKey(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 124
    const-class v1, Lcom/squareup/container/CalculatedKey;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/container/CalculatedKey;->debugName:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 125
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "%s[%s@%s]"

    .line 124
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
