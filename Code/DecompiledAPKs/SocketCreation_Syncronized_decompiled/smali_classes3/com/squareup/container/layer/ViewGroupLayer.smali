.class public final Lcom/squareup/container/layer/ViewGroupLayer;
.super Ljava/lang/Object;
.source "ViewGroupLayer.java"

# interfaces
.implements Lcom/squareup/container/layer/ContainerLayer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/container/layer/ViewGroupLayer$Exposer;
    }
.end annotation


# static fields
.field public static final NULL_EXPOSER:Lcom/squareup/container/layer/ViewGroupLayer$Exposer;


# instance fields
.field private final annotationTypes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Class<",
            "+",
            "Ljava/lang/annotation/Annotation;",
            ">;>;"
        }
    .end annotation
.end field

.field private final contextFactory:Lflow/path/PathContextFactory;

.field private final exposer:Lcom/squareup/container/layer/ViewGroupLayer$Exposer;

.field private final isBackground:Z

.field private final isDefault:Z

.field private final layout:Landroid/view/ViewGroup;

.field private final pathContainer:Lcom/squareup/container/SquarePathContainer;

.field private final visibilityPartner:Lcom/squareup/container/layer/ContainerLayer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 50
    sget-object v0, Lcom/squareup/container/layer/-$$Lambda$ViewGroupLayer$f0nJdPYL-xJxQbDio7DNz2VVm-c;->INSTANCE:Lcom/squareup/container/layer/-$$Lambda$ViewGroupLayer$f0nJdPYL-xJxQbDio7DNz2VVm-c;

    sput-object v0, Lcom/squareup/container/layer/ViewGroupLayer;->NULL_EXPOSER:Lcom/squareup/container/layer/ViewGroupLayer$Exposer;

    return-void
.end method

.method private constructor <init>(Lflow/path/PathContextFactory;Landroid/view/ViewGroup;Lcom/squareup/container/layer/ViewGroupLayer$Exposer;ZZLjava/util/Collection;Lcom/squareup/container/layer/ContainerLayer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/path/PathContextFactory;",
            "Landroid/view/ViewGroup;",
            "Lcom/squareup/container/layer/ViewGroupLayer$Exposer;",
            "ZZ",
            "Ljava/util/Collection<",
            "Ljava/lang/Class<",
            "+",
            "Ljava/lang/annotation/Annotation;",
            ">;>;",
            "Lcom/squareup/container/layer/ContainerLayer;",
            ")V"
        }
    .end annotation

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    iput-object p7, p0, Lcom/squareup/container/layer/ViewGroupLayer;->visibilityPartner:Lcom/squareup/container/layer/ContainerLayer;

    .line 104
    invoke-static {p5, p6}, Lcom/squareup/container/layer/ViewGroupLayer;->validateAnnotationTypes(ZLjava/util/Collection;)V

    .line 105
    new-instance p7, Ljava/util/LinkedHashSet;

    invoke-direct {p7, p6}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {p7}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object p6

    iput-object p6, p0, Lcom/squareup/container/layer/ViewGroupLayer;->annotationTypes:Ljava/util/Set;

    .line 106
    iput-object p1, p0, Lcom/squareup/container/layer/ViewGroupLayer;->contextFactory:Lflow/path/PathContextFactory;

    .line 107
    iput-object p3, p0, Lcom/squareup/container/layer/ViewGroupLayer;->exposer:Lcom/squareup/container/layer/ViewGroupLayer$Exposer;

    .line 108
    iput-boolean p4, p0, Lcom/squareup/container/layer/ViewGroupLayer;->isBackground:Z

    .line 109
    iput-boolean p5, p0, Lcom/squareup/container/layer/ViewGroupLayer;->isDefault:Z

    .line 110
    iput-object p2, p0, Lcom/squareup/container/layer/ViewGroupLayer;->layout:Landroid/view/ViewGroup;

    .line 111
    new-instance p2, Lcom/squareup/container/SquarePathContainer;

    invoke-direct {p2, p1}, Lcom/squareup/container/SquarePathContainer;-><init>(Lflow/path/PathContextFactory;)V

    iput-object p2, p0, Lcom/squareup/container/layer/ViewGroupLayer;->pathContainer:Lcom/squareup/container/SquarePathContainer;

    return-void
.end method

.method private static annotationTypeList(Ljava/lang/Class;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/Class<",
            "+",
            "Ljava/lang/annotation/Annotation;",
            ">;>;"
        }
    .end annotation

    .line 93
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 94
    invoke-static {p0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    return-object v0
.end method

.method public static defaultLayer(Lflow/path/PathContextFactory;Landroid/view/ViewGroup;Lcom/squareup/container/layer/ViewGroupLayer$Exposer;)Lcom/squareup/container/layer/ViewGroupLayer;
    .locals 9

    .line 60
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v6

    .line 61
    new-instance v8, Lcom/squareup/container/layer/ViewGroupLayer;

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v7, 0x0

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v7}, Lcom/squareup/container/layer/ViewGroupLayer;-><init>(Lflow/path/PathContextFactory;Landroid/view/ViewGroup;Lcom/squareup/container/layer/ViewGroupLayer$Exposer;ZZLjava/util/Collection;Lcom/squareup/container/layer/ContainerLayer;)V

    return-object v8
.end method

.method private hasMyAnnotation(Lcom/squareup/container/ContainerTreeKey;)Z
    .locals 3

    .line 254
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    .line 256
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/annotation/Annotation;

    .line 257
    invoke-interface {v1}, Ljava/lang/annotation/Annotation;->annotationType()Ljava/lang/Class;

    move-result-object v1

    .line 258
    iget-object v2, p0, Lcom/squareup/container/layer/ViewGroupLayer;->annotationTypes:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    or-int/2addr v0, v1

    goto :goto_0

    :cond_0
    return v0
.end method

.method private isUnannotated(Lcom/squareup/container/ContainerTreeKey;)Z
    .locals 0

    .line 250
    invoke-static {p1}, Lcom/squareup/container/layer/Layers;->hasLayerAnnotation(Lflow/path/Path;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method static synthetic lambda$makeInaccessible$2(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 0

    const/4 p0, 0x1

    return p0
.end method

.method static synthetic lambda$static$0(ZZLflow/TraversalCallback;)V
    .locals 0

    .line 51
    invoke-interface {p2}, Lflow/TraversalCallback;->onTraversalCompleted()V

    return-void
.end method

.method private makeAccessible()V
    .locals 2

    .line 265
    iget-object v0, p0, Lcom/squareup/container/layer/ViewGroupLayer;->layout:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setImportantForAccessibility(Landroid/view/View;I)V

    .line 266
    iget-object v0, p0, Lcom/squareup/container/layer/ViewGroupLayer;->layout:Landroid/view/ViewGroup;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setClickable(Z)V

    .line 267
    iget-object v0, p0, Lcom/squareup/container/layer/ViewGroupLayer;->layout:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method private makeInaccessible()V
    .locals 2

    .line 272
    iget-object v0, p0, Lcom/squareup/container/layer/ViewGroupLayer;->layout:Landroid/view/ViewGroup;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setImportantForAccessibility(Landroid/view/View;I)V

    .line 273
    iget-object v0, p0, Lcom/squareup/container/layer/ViewGroupLayer;->layout:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setClickable(Z)V

    .line 274
    iget-object v0, p0, Lcom/squareup/container/layer/ViewGroupLayer;->layout:Landroid/view/ViewGroup;

    sget-object v1, Lcom/squareup/container/layer/-$$Lambda$ViewGroupLayer$Q1eafm8yiKD0GXR14mv8GRDHdb8;->INSTANCE:Lcom/squareup/container/layer/-$$Lambda$ViewGroupLayer$Q1eafm8yiKD0GXR14mv8GRDHdb8;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method private static validateAnnotationType(Ljava/lang/Class;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+",
            "Ljava/lang/annotation/Annotation;",
            ">;)V"
        }
    .end annotation

    .line 238
    const-class v0, Lcom/squareup/container/layer/Layer;

    invoke-virtual {p0, v0}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object p0

    if-eqz p0, :cond_0

    return-void

    .line 239
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Layer annotations must be annotated with @"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-class v1, Lcom/squareup/container/layer/Layer;

    .line 240
    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static validateAnnotationTypes(ZLjava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/Collection<",
            "Ljava/lang/Class<",
            "+",
            "Ljava/lang/annotation/Annotation;",
            ">;>;)V"
        }
    .end annotation

    if-nez p0, :cond_0

    const-string p0, "annotationTypes"

    .line 230
    invoke-static {p1, p0}, Lcom/squareup/util/Preconditions;->nonEmpty(Ljava/util/Collection;Ljava/lang/String;)Ljava/util/Collection;

    .line 232
    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Class;

    .line 233
    invoke-static {p1}, Lcom/squareup/container/layer/ViewGroupLayer;->validateAnnotationType(Ljava/lang/Class;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static withAnnotation(Lflow/path/PathContextFactory;Landroid/view/ViewGroup;ZLcom/squareup/container/layer/ViewGroupLayer$Exposer;Ljava/lang/Class;)Lcom/squareup/container/layer/ViewGroupLayer;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/path/PathContextFactory;",
            "Landroid/view/ViewGroup;",
            "Z",
            "Lcom/squareup/container/layer/ViewGroupLayer$Exposer;",
            "Ljava/lang/Class<",
            "+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "Lcom/squareup/container/layer/ViewGroupLayer;"
        }
    .end annotation

    .line 72
    invoke-static {p4}, Lcom/squareup/container/layer/ViewGroupLayer;->annotationTypeList(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v6

    .line 73
    new-instance p4, Lcom/squareup/container/layer/ViewGroupLayer;

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v0, p4

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move v4, p2

    invoke-direct/range {v0 .. v7}, Lcom/squareup/container/layer/ViewGroupLayer;-><init>(Lflow/path/PathContextFactory;Landroid/view/ViewGroup;Lcom/squareup/container/layer/ViewGroupLayer$Exposer;ZZLjava/util/Collection;Lcom/squareup/container/layer/ContainerLayer;)V

    return-object p4
.end method


# virtual methods
.method public as(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    .line 220
    iget-object v0, p0, Lcom/squareup/container/layer/ViewGroupLayer;->layout:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public cleanUp()V
    .locals 1

    .line 152
    iget-object v0, p0, Lcom/squareup/container/layer/ViewGroupLayer;->pathContainer:Lcom/squareup/container/SquarePathContainer;

    invoke-virtual {v0}, Lcom/squareup/container/SquarePathContainer;->forceFinishTraversal()V

    return-void
.end method

.method public varargs dispatchLayer(Lflow/Traversal;Lflow/TraversalCallback;[Lflow/path/PathContext;)V
    .locals 11

    .line 165
    new-instance v0, Lcom/squareup/container/TraversalCallbackSet;

    const-string v1, "callback"

    .line 166
    invoke-static {p2, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lflow/TraversalCallback;

    invoke-direct {v0, p2}, Lcom/squareup/container/TraversalCallbackSet;-><init>(Lflow/TraversalCallback;)V

    const-string/jumbo p2, "traversal"

    .line 168
    invoke-static {p1, p2}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lflow/Traversal;

    iget-object p2, p2, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {p2}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/container/ContainerTreeKey;

    .line 169
    iget-object v1, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {p0, v1}, Lcom/squareup/container/layer/ViewGroupLayer;->findTopmostScreenToShow(Lflow/History;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v1

    if-ne v1, p2, :cond_0

    const-string v2, "showMe"

    .line 172
    invoke-static {v1, v2}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/container/ContainerTreeKey;

    invoke-static {v2}, Lcom/squareup/container/ParcelableTester;->assertScreenCanBeParceled(Lcom/squareup/container/ContainerTreeKey;)V

    .line 173
    iget-object v2, p0, Lcom/squareup/container/layer/ViewGroupLayer;->pathContainer:Lcom/squareup/container/SquarePathContainer;

    iget-object v3, p0, Lcom/squareup/container/layer/ViewGroupLayer;->layout:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Lcom/squareup/container/TraversalCallbackSet;->add()Lflow/TraversalCallback;

    move-result-object v4

    invoke-virtual {v2, v3, p1, v4, p3}, Lcom/squareup/container/SquarePathContainer;->executeFlowTraversal(Landroid/view/ViewGroup;Lflow/Traversal;Lflow/TraversalCallback;[Lflow/path/PathContext;)V

    goto :goto_0

    :cond_0
    if-eqz v1, :cond_1

    .line 175
    invoke-static {v1}, Lcom/squareup/container/ParcelableTester;->assertScreenCanBeParceled(Lcom/squareup/container/ContainerTreeKey;)V

    .line 176
    iget-object v5, p0, Lcom/squareup/container/layer/ViewGroupLayer;->pathContainer:Lcom/squareup/container/SquarePathContainer;

    iget-object v6, p0, Lcom/squareup/container/layer/ViewGroupLayer;->layout:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Lcom/squareup/container/ContainerTreeKey;->asPath()Lflow/path/Path;

    move-result-object v7

    sget-object v8, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-virtual {v0}, Lcom/squareup/container/TraversalCallbackSet;->add()Lflow/TraversalCallback;

    move-result-object v9

    move-object v10, p3

    invoke-virtual/range {v5 .. v10}, Lcom/squareup/container/SquarePathContainer;->setPath(Landroid/view/ViewGroup;Lflow/path/Path;Lflow/Direction;Lflow/TraversalCallback;[Lflow/path/PathContext;)V

    :cond_1
    :goto_0
    if-eq v1, p2, :cond_3

    .line 186
    iget-object v2, p0, Lcom/squareup/container/layer/ViewGroupLayer;->visibilityPartner:Lcom/squareup/container/layer/ContainerLayer;

    if-eqz v2, :cond_2

    invoke-interface {v2, p2}, Lcom/squareup/container/layer/ContainerLayer;->owns(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_1

    .line 189
    :cond_2
    invoke-direct {p0}, Lcom/squareup/container/layer/ViewGroupLayer;->makeInaccessible()V

    goto :goto_2

    .line 187
    :cond_3
    :goto_1
    invoke-direct {p0}, Lcom/squareup/container/layer/ViewGroupLayer;->makeAccessible()V

    :goto_2
    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v1, :cond_4

    const/4 v4, 0x1

    goto :goto_3

    :cond_4
    const/4 v4, 0x0

    .line 193
    :goto_3
    new-instance v5, Lcom/squareup/container/layer/-$$Lambda$ViewGroupLayer$8z_mrcFMOUvAMHKsMfFGsQx5plc;

    invoke-direct {v5, p0, v4, p3}, Lcom/squareup/container/layer/-$$Lambda$ViewGroupLayer$8z_mrcFMOUvAMHKsMfFGsQx5plc;-><init>(Lcom/squareup/container/layer/ViewGroupLayer;Z[Lflow/path/PathContext;)V

    .line 208
    iget-object p3, p1, Lflow/Traversal;->direction:Lflow/Direction;

    sget-object v4, Lflow/Direction;->REPLACE:Lflow/Direction;

    if-eq p3, v4, :cond_5

    const/4 p3, 0x1

    goto :goto_4

    :cond_5
    const/4 p3, 0x0

    :goto_4
    if-eqz p3, :cond_7

    .line 209
    invoke-virtual {p0, p2}, Lcom/squareup/container/layer/ViewGroupLayer;->owns(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result p2

    if-nez p2, :cond_6

    iget-object p1, p1, Lflow/Traversal;->origin:Lflow/History;

    invoke-virtual {p1}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/container/ContainerTreeKey;

    invoke-virtual {p0, p1}, Lcom/squareup/container/layer/ViewGroupLayer;->owns(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result p1

    if-eqz p1, :cond_7

    :cond_6
    const/4 p1, 0x1

    goto :goto_5

    :cond_7
    const/4 p1, 0x0

    .line 210
    :goto_5
    iget-object p2, p0, Lcom/squareup/container/layer/ViewGroupLayer;->exposer:Lcom/squareup/container/layer/ViewGroupLayer$Exposer;

    if-eqz v1, :cond_8

    goto :goto_6

    :cond_8
    const/4 v2, 0x0

    :goto_6
    invoke-virtual {v0, v5}, Lcom/squareup/container/TraversalCallbackSet;->add(Lflow/TraversalCallback;)Lflow/TraversalCallback;

    move-result-object p3

    invoke-interface {p2, v2, p1, p3}, Lcom/squareup/container/layer/ViewGroupLayer$Exposer;->setVisible(ZZLflow/TraversalCallback;)V

    .line 212
    invoke-virtual {v0}, Lcom/squareup/container/TraversalCallbackSet;->release()V

    return-void
.end method

.method public findTopmostScreenToShow(Lflow/History;)Lcom/squareup/container/ContainerTreeKey;
    .locals 3

    .line 137
    invoke-virtual {p1}, Lflow/History;->framesFromTop()Ljava/lang/Iterable;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 138
    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    .line 139
    invoke-virtual {p0, v0}, Lcom/squareup/container/layer/ViewGroupLayer;->owns(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v2

    if-eqz v2, :cond_1

    return-object v0

    .line 142
    :cond_1
    iget-boolean v2, p0, Lcom/squareup/container/layer/ViewGroupLayer;->isBackground:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/squareup/container/layer/ViewGroupLayer;->isDefault:Z

    if-nez v2, :cond_0

    invoke-direct {p0, v0}, Lcom/squareup/container/layer/ViewGroupLayer;->isUnannotated(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_2
    return-object v1
.end method

.method public getVisibleContext()Landroid/content/Context;
    .locals 2

    .line 132
    invoke-virtual {p0}, Lcom/squareup/container/layer/ViewGroupLayer;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/squareup/container/layer/ViewGroupLayer;->pathContainer:Lcom/squareup/container/SquarePathContainer;

    iget-object v1, p0, Lcom/squareup/container/layer/ViewGroupLayer;->layout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Lcom/squareup/container/SquarePathContainer;->getActiveChild(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0

    .line 132
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Cannot be called while not showing."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public is(Ljava/lang/Class;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;)Z"
        }
    .end annotation

    .line 216
    invoke-virtual {p0}, Lcom/squareup/container/layer/ViewGroupLayer;->isShowing()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/container/layer/ViewGroupLayer;->layout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public isDefault()Z
    .locals 1

    .line 156
    iget-boolean v0, p0, Lcom/squareup/container/layer/ViewGroupLayer;->isDefault:Z

    return v0
.end method

.method public isShowing()Z
    .locals 1

    .line 160
    iget-object v0, p0, Lcom/squareup/container/layer/ViewGroupLayer;->layout:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isVisibleWith(Lcom/squareup/container/layer/ContainerLayer;)Lcom/squareup/container/layer/ViewGroupLayer;
    .locals 9

    .line 115
    new-instance v8, Lcom/squareup/container/layer/ViewGroupLayer;

    iget-object v1, p0, Lcom/squareup/container/layer/ViewGroupLayer;->contextFactory:Lflow/path/PathContextFactory;

    iget-object v2, p0, Lcom/squareup/container/layer/ViewGroupLayer;->layout:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/squareup/container/layer/ViewGroupLayer;->exposer:Lcom/squareup/container/layer/ViewGroupLayer$Exposer;

    iget-boolean v4, p0, Lcom/squareup/container/layer/ViewGroupLayer;->isBackground:Z

    iget-boolean v5, p0, Lcom/squareup/container/layer/ViewGroupLayer;->isDefault:Z

    iget-object v6, p0, Lcom/squareup/container/layer/ViewGroupLayer;->annotationTypes:Ljava/util/Set;

    move-object v0, v8

    move-object v7, p1

    invoke-direct/range {v0 .. v7}, Lcom/squareup/container/layer/ViewGroupLayer;-><init>(Lflow/path/PathContextFactory;Landroid/view/ViewGroup;Lcom/squareup/container/layer/ViewGroupLayer$Exposer;ZZLjava/util/Collection;Lcom/squareup/container/layer/ContainerLayer;)V

    return-object v8
.end method

.method public synthetic lambda$dispatchLayer$1$ViewGroupLayer(Z[Lflow/path/PathContext;)V
    .locals 2

    if-eqz p1, :cond_1

    .line 194
    iget-object p1, p0, Lcom/squareup/container/layer/ViewGroupLayer;->layout:Landroid/view/ViewGroup;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p1

    if-lez p1, :cond_1

    .line 198
    array-length p1, p2

    if-lez p1, :cond_0

    .line 199
    iget-object p1, p0, Lcom/squareup/container/layer/ViewGroupLayer;->layout:Landroid/view/ViewGroup;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lflow/path/PathContext;->get(Landroid/content/Context;)Lflow/path/PathContext;

    move-result-object p1

    .line 200
    iget-object v0, p0, Lcom/squareup/container/layer/ViewGroupLayer;->contextFactory:Lflow/path/PathContextFactory;

    array-length v1, p2

    add-int/lit8 v1, v1, -0x1

    aget-object v1, p2, v1

    invoke-virtual {p1, v0, v1, p2}, Lflow/path/PathContext;->destroyNotIn(Lflow/path/PathContextFactory;Lflow/path/PathContext;[Lflow/path/PathContext;)V

    .line 202
    :cond_0
    iget-object p1, p0, Lcom/squareup/container/layer/ViewGroupLayer;->layout:Landroid/view/ViewGroup;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->removeAllViews()V

    :cond_1
    return-void
.end method

.method public owns(Lcom/squareup/container/ContainerTreeKey;)Z
    .locals 1

    .line 245
    iget-boolean v0, p0, Lcom/squareup/container/layer/ViewGroupLayer;->isDefault:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/squareup/container/layer/ViewGroupLayer;->isUnannotated(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 246
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/container/layer/ViewGroupLayer;->hasMyAnnotation(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result p1

    if-eqz p1, :cond_2

    :cond_1
    const/4 p1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public plus(Ljava/lang/Class;)Lcom/squareup/container/layer/ViewGroupLayer;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "Lcom/squareup/container/layer/ViewGroupLayer;"
        }
    .end annotation

    .line 124
    invoke-static {p1}, Lcom/squareup/container/layer/ViewGroupLayer;->validateAnnotationType(Ljava/lang/Class;)V

    .line 125
    new-instance v6, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/squareup/container/layer/ViewGroupLayer;->annotationTypes:Ljava/util/Set;

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 126
    invoke-interface {v6, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    new-instance p1, Lcom/squareup/container/layer/ViewGroupLayer;

    iget-object v1, p0, Lcom/squareup/container/layer/ViewGroupLayer;->contextFactory:Lflow/path/PathContextFactory;

    iget-object v2, p0, Lcom/squareup/container/layer/ViewGroupLayer;->layout:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/squareup/container/layer/ViewGroupLayer;->exposer:Lcom/squareup/container/layer/ViewGroupLayer$Exposer;

    iget-boolean v4, p0, Lcom/squareup/container/layer/ViewGroupLayer;->isBackground:Z

    iget-boolean v5, p0, Lcom/squareup/container/layer/ViewGroupLayer;->isDefault:Z

    iget-object v7, p0, Lcom/squareup/container/layer/ViewGroupLayer;->visibilityPartner:Lcom/squareup/container/layer/ContainerLayer;

    move-object v0, p1

    invoke-direct/range {v0 .. v7}, Lcom/squareup/container/layer/ViewGroupLayer;-><init>(Lflow/path/PathContextFactory;Landroid/view/ViewGroup;Lcom/squareup/container/layer/ViewGroupLayer$Exposer;ZZLjava/util/Collection;Lcom/squareup/container/layer/ContainerLayer;)V

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 224
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/squareup/container/layer/ViewGroupLayer;->annotationTypes:Ljava/util/Set;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/container/layer/ViewGroupLayer;->isDefault:Z

    if-eqz v1, :cond_0

    const-string v1, " (default)"

    goto :goto_0

    :cond_0
    const-string v1, ""

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
