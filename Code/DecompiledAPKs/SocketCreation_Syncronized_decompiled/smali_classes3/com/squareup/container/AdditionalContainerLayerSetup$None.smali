.class public final Lcom/squareup/container/AdditionalContainerLayerSetup$None;
.super Ljava/lang/Object;
.source "AdditionalContainerLayerSetup.kt"

# interfaces
.implements Lcom/squareup/container/AdditionalContainerLayerSetup;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/container/AdditionalContainerLayerSetup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "None"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J$\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00042\u0006\u0010\u0008\u001a\u00020\tH\u0016J\u0012\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0016\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/container/AdditionalContainerLayerSetup$None;",
        "Lcom/squareup/container/AdditionalContainerLayerSetup;",
        "()V",
        "additionalLayersToSetup",
        "",
        "Lcom/squareup/container/AdditionalContainerLayer;",
        "currentLayers",
        "Lcom/squareup/container/layer/ContainerLayer;",
        "contextFactory",
        "Lflow/path/PathContextFactory;",
        "redirectForAdditionalLayers",
        "Lcom/squareup/container/RedirectStep$Result;",
        "traversal",
        "Lflow/Traversal;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/container/AdditionalContainerLayerSetup$None;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 31
    new-instance v0, Lcom/squareup/container/AdditionalContainerLayerSetup$None;

    invoke-direct {v0}, Lcom/squareup/container/AdditionalContainerLayerSetup$None;-><init>()V

    sput-object v0, Lcom/squareup/container/AdditionalContainerLayerSetup$None;->INSTANCE:Lcom/squareup/container/AdditionalContainerLayerSetup$None;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public additionalLayersToSetup(Ljava/util/List;Lflow/path/PathContextFactory;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/container/layer/ContainerLayer;",
            ">;",
            "Lflow/path/PathContextFactory;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/container/AdditionalContainerLayer;",
            ">;"
        }
    .end annotation

    const-string v0, "currentLayers"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "contextFactory"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public redirectForAdditionalLayers(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 1

    const-string/jumbo v0, "traversal"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    return-object p1
.end method
