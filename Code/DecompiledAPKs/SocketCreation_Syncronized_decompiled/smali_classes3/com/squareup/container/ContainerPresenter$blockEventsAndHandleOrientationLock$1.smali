.class final Lcom/squareup/container/ContainerPresenter$blockEventsAndHandleOrientationLock$1;
.super Ljava/lang/Object;
.source "ContainerPresenter.kt"

# interfaces
.implements Lflow/TraversalCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/ContainerPresenter;->blockEventsAndHandleOrientationLock(Lflow/Traversal;Lflow/TraversalCallback;)Lcom/squareup/container/DispatchStep$Result;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nContainerPresenter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ContainerPresenter.kt\ncom/squareup/container/ContainerPresenter$blockEventsAndHandleOrientationLock$1\n*L\n1#1,594:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "V",
        "Lcom/squareup/container/ContainerView;",
        "onTraversalCompleted"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $incomingLayersRequireLock:Z

.field final synthetic $originalCallback:Lflow/TraversalCallback;

.field final synthetic $viewInTraversal:Lcom/squareup/container/ContainerView;

.field final synthetic this$0:Lcom/squareup/container/ContainerPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/container/ContainerPresenter;Lcom/squareup/container/ContainerView;Lflow/TraversalCallback;Z)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/container/ContainerPresenter$blockEventsAndHandleOrientationLock$1;->this$0:Lcom/squareup/container/ContainerPresenter;

    iput-object p2, p0, Lcom/squareup/container/ContainerPresenter$blockEventsAndHandleOrientationLock$1;->$viewInTraversal:Lcom/squareup/container/ContainerView;

    iput-object p3, p0, Lcom/squareup/container/ContainerPresenter$blockEventsAndHandleOrientationLock$1;->$originalCallback:Lflow/TraversalCallback;

    iput-boolean p4, p0, Lcom/squareup/container/ContainerPresenter$blockEventsAndHandleOrientationLock$1;->$incomingLayersRequireLock:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTraversalCompleted()V
    .locals 5

    .line 548
    iget-object v0, p0, Lcom/squareup/container/ContainerPresenter$blockEventsAndHandleOrientationLock$1;->this$0:Lcom/squareup/container/ContainerPresenter;

    invoke-static {v0}, Lcom/squareup/container/ContainerPresenter;->access$getOrphanContext$p(Lcom/squareup/container/ContainerPresenter;)Lflow/path/PathContext;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 550
    iget-object v1, p0, Lcom/squareup/container/ContainerPresenter$blockEventsAndHandleOrientationLock$1;->this$0:Lcom/squareup/container/ContainerPresenter;

    invoke-static {v1}, Lcom/squareup/container/ContainerPresenter;->access$getVisiblePathContexts$p(Lcom/squareup/container/ContainerPresenter;)[Lflow/path/PathContext;

    move-result-object v1

    .line 551
    iget-object v2, p0, Lcom/squareup/container/ContainerPresenter$blockEventsAndHandleOrientationLock$1;->this$0:Lcom/squareup/container/ContainerPresenter;

    invoke-static {v2}, Lcom/squareup/container/ContainerPresenter;->access$getContextFactory$p(Lcom/squareup/container/ContainerPresenter;)Lflow/path/PathContextFactory;

    move-result-object v2

    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    aget-object v3, v1, v3

    array-length v4, v1

    invoke-static {v1, v4}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lflow/path/PathContext;

    invoke-virtual {v0, v2, v3, v1}, Lflow/path/PathContext;->destroyNotIn(Lflow/path/PathContextFactory;Lflow/path/PathContext;[Lflow/path/PathContext;)V

    .line 552
    iget-object v0, p0, Lcom/squareup/container/ContainerPresenter$blockEventsAndHandleOrientationLock$1;->this$0:Lcom/squareup/container/ContainerPresenter;

    const/4 v1, 0x0

    check-cast v1, Lflow/path/PathContext;

    invoke-static {v0, v1}, Lcom/squareup/container/ContainerPresenter;->access$setOrphanContext$p(Lcom/squareup/container/ContainerPresenter;Lflow/path/PathContext;)V

    .line 554
    :cond_0
    iget-object v0, p0, Lcom/squareup/container/ContainerPresenter$blockEventsAndHandleOrientationLock$1;->$viewInTraversal:Lcom/squareup/container/ContainerView;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/squareup/container/ContainerView;->interceptInputEvents(Z)V

    .line 555
    iget-object v0, p0, Lcom/squareup/container/ContainerPresenter$blockEventsAndHandleOrientationLock$1;->$originalCallback:Lflow/TraversalCallback;

    invoke-interface {v0}, Lflow/TraversalCallback;->onTraversalCompleted()V

    .line 556
    iget-boolean v0, p0, Lcom/squareup/container/ContainerPresenter$blockEventsAndHandleOrientationLock$1;->$incomingLayersRequireLock:Z

    if-nez v0, :cond_1

    .line 557
    iget-object v0, p0, Lcom/squareup/container/ContainerPresenter$blockEventsAndHandleOrientationLock$1;->this$0:Lcom/squareup/container/ContainerPresenter;

    invoke-static {v0}, Lcom/squareup/container/ContainerPresenter;->access$getOrientationLock$p(Lcom/squareup/container/ContainerPresenter;)Lcom/squareup/container/OrientationLock;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/container/OrientationLock;->requestUnlockOrientation()V

    :cond_1
    return-void
.end method
