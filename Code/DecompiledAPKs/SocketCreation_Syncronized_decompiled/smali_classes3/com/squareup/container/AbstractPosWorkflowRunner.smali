.class public abstract Lcom/squareup/container/AbstractPosWorkflowRunner;
.super Ljava/lang/Object;
.source "AbstractPosWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/container/PosWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;,
        Lcom/squareup/container/AbstractPosWorkflowRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        "O:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/container/PosWorkflowRunner<",
        "TO;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAbstractPosWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AbstractPosWorkflowRunner.kt\ncom/squareup/container/AbstractPosWorkflowRunner\n*L\n1#1,525:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00f8\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008&\u0018\u0000 x*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u0002*\u0004\u0008\u0002\u0010\u00042\u0008\u0012\u0004\u0012\u0002H\u00030\u0005:\u0002xyB-\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0008\u0008\u0002\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ%\u0010;\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020=0<2\u0006\u0010>\u001a\u00020.2\u0006\u0010?\u001a\u00020@H\u0000\u00a2\u0006\u0002\u0008AJ \u0010B\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020=0<2\u0006\u0010>\u001a\u00020.2\u0006\u0010?\u001a\u00020@H\u0007J%\u0010C\u001a\u00020D2\u0006\u0010>\u001a\u00020.2\u0006\u0010E\u001a\u00020@2\u0006\u0010F\u001a\u00020GH\u0000\u00a2\u0006\u0002\u0008HJ \u0010I\u001a\u00020D2\u0006\u0010>\u001a\u00020.2\u0006\u0010E\u001a\u00020@2\u0006\u0010F\u001a\u00020GH\u0007JV\u0010J\u001a\u00020.2\u0006\u0010\u0006\u001a\u00020\u00072\u0008\u0010K\u001a\u0004\u0018\u00010+2\u0012\u0010L\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030Mj\u0002`N2\u0006\u0010O\u001a\u00020P2\u0006\u0010Q\u001a\u00020R2\u0006\u0010\u001f\u001a\u00020\u00022\u0006\u0010S\u001a\u00020T2\u0006\u0010U\u001a\u00020\u000cH\u0002J\u0010\u0010V\u001a\u00020W2\u0006\u0010X\u001a\u00020\u0007H\u0002J\u0008\u0010Y\u001a\u00020WH\u0004J\u0010\u0010Z\u001a\u00020W2\u0006\u0010[\u001a\u00020.H\u0002J\u0010\u0010\\\u001a\u00020W2\u0006\u0010]\u001a\u00020^H\u0015J\u000e\u0010_\u001a\n\u0012\u0006\u0008\u0001\u0012\u00028\u00010\tJ\u0015\u0010_\u001a\u00020W2\u0006\u0010`\u001a\u00028\u0001H\u0002\u00a2\u0006\u0002\u0010#J\u0014\u0010a\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020.0-0\tH\u0016J\u000e\u0010b\u001a\u00020W2\u0006\u0010c\u001a\u00020dJ;\u0010e\u001a(\u0012\u0006\u0008\u0001\u0012\u00020P\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030Mj\u0002`N0fj\n\u0012\u0006\u0008\u0001\u0012\u00020P`g2\u0006\u0010h\u001a\u00028\u0002H$\u00a2\u0006\u0002\u0010iJh\u0010j\u001aZ\u0012&\u0012$\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003 k*\u0012\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0018\u00010Mj\u0004\u0018\u0001`N0Mj\u0002`N k*,\u0012&\u0012$\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003 k*\u0012\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0018\u00010Mj\u0004\u0018\u0001`N0Mj\u0002`N\u0018\u00010\t0\t2\u0006\u0010O\u001a\u00020PH\u0002J\u0008\u0010l\u001a\u00020WH\u0004J\"\u0010m\u001a\u00020W2\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00028\u00000\'2\n\u0008\u0002\u0010Q\u001a\u0004\u0018\u00010RH\u0002J\u0008\u0010n\u001a\u00020WH\u0004J\u00f5\u0001\u0010o\u001a\u00ee\u0001\u0012p\u0012n\u0012\u0004\u0012\u00028\u0000\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020P\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030Mj\u0002`N0fj\n\u0012\u0006\u0008\u0001\u0012\u00020P`g k*6\u0012\u0004\u0012\u00028\u0000\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020P\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030Mj\u0002`N0fj\n\u0012\u0006\u0008\u0001\u0012\u00020P`g\u0018\u00010p0p k*v\u0012p\u0012n\u0012\u0004\u0012\u00028\u0000\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020P\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030Mj\u0002`N0fj\n\u0012\u0006\u0008\u0001\u0012\u00020P`g k*6\u0012\u0004\u0012\u00028\u0000\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020P\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030Mj\u0002`N0fj\n\u0012\u0006\u0008\u0001\u0012\u00020P`g\u0018\u00010p0p\u0018\u00010\t0\tH\u0002J.\u0010q\u001a(\u0012$\u0012\"\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0002 k*\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0002\u0018\u00010p0p0\tH\u0004J4\u0010r\u001a\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u000409\"\u0008\u0008\u0003\u0010\u0003*\u00020\u0002\"\u0004\u0008\u0004\u0010\u0004*\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u000409H\u0014JJ\u0010s\u001a4\u0012\u0004\u0012\u00028\u0000\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020P\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030Mj\u0002`N0fj\n\u0012\u0006\u0008\u0001\u0012\u00020P`g0p*\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00020pH\u0002J\u0084\u0001\u0010t\u001a\u0008\u0012\u0004\u0012\u0002Hu0\t\"\u0004\u0008\u0003\u0010\u0001\"\u0008\u0008\u0004\u0010\u0003*\u00020\u0002\"\u0004\u0008\u0005\u0010\u0004\"\u0004\u0008\u0006\u0010u*\"\u0012\u001e\u0012\u001c\u0012\u0004\u0012\u0002H\u0003\u0012\u0012\u0008\u0001\u0012\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00040:090\t24\u0010v\u001a0\u0012\u001e\u0012\u001c\u0012\u0004\u0012\u0002H\u0003\u0012\u0012\u0008\u0001\u0012\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00040:09\u0012\u000c\u0012\n\u0012\u0006\u0008\u0001\u0012\u0002Hu0\t0wH\u0002R\u0014\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00028\u00010\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0012\u001a\u00020\u00138BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0016\u0010\u0017\u001a\u0004\u0008\u0014\u0010\u0015R\u0014\u0010\u0018\u001a\u00020\u000c8DX\u0084\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0018\u0010\u0019R\u0014\u0010\u001a\u001a\u00020\u001bX\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u001dR\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u001f\u001a\u00028\u00002\u0006\u0010\u001e\u001a\u00028\u00008E@DX\u0084\u000e\u00a2\u0006\u000c\u001a\u0004\u0008 \u0010!\"\u0004\u0008\"\u0010#R\u0014\u0010$\u001a\u0008\u0012\u0004\u0012\u00028\u00000%X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010&\u001a\u0008\u0012\u0004\u0012\u00028\u00000\'X\u0084\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008(\u0010)R\u0010\u0010*\u001a\u0004\u0018\u00010+X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010,\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020.0-0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010/\u001a\u000200X\u00a4\u0004\u00a2\u0006\u0006\u001a\u0004\u00081\u00102R$\u00103\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u000204X\u00a4\u0004\u00a2\u0006\u0006\u001a\u0004\u00085\u00106R.\u00107\u001a\"\u0012\u001e\u0012\u001c\u0012\u0004\u0012\u00028\u0001\u0012\u0012\u0008\u0001\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00020:0908X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006z"
    }
    d2 = {
        "Lcom/squareup/container/AbstractPosWorkflowRunner;",
        "P",
        "",
        "O",
        "R",
        "Lcom/squareup/container/PosWorkflowRunner;",
        "serviceName",
        "",
        "nextHistory",
        "Lio/reactivex/Observable;",
        "Lflow/History;",
        "cancelAfterOneResult",
        "",
        "mainDispatcher",
        "Lkotlinx/coroutines/CoroutineDispatcher;",
        "(Ljava/lang/String;Lio/reactivex/Observable;ZLkotlinx/coroutines/CoroutineDispatcher;)V",
        "_results",
        "Lio/reactivex/observables/ConnectableObservable;",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "getContainerHints",
        "()Lcom/squareup/workflow/ui/ContainerHints;",
        "containerHints$delegate",
        "Lkotlin/Lazy;",
        "isRunning",
        "()Z",
        "layeringUtils",
        "Lcom/squareup/container/PosLayeringUtils;",
        "getLayeringUtils",
        "()Lcom/squareup/container/PosLayeringUtils;",
        "value",
        "props",
        "getProps",
        "()Ljava/lang/Object;",
        "setProps",
        "(Ljava/lang/Object;)V",
        "propsChannel",
        "Lkotlinx/coroutines/channels/ConflatedBroadcastChannel;",
        "propsFlow",
        "Lkotlinx/coroutines/flow/Flow;",
        "getPropsFlow",
        "()Lkotlinx/coroutines/flow/Flow;",
        "scopeKey",
        "Lcom/squareup/container/ContainerTreeKey;",
        "showFlowScreens",
        "",
        "Lcom/squareup/container/WorkflowTreeKey;",
        "viewFactory",
        "Lcom/squareup/workflow/WorkflowViewFactory;",
        "getViewFactory",
        "()Lcom/squareup/workflow/WorkflowViewFactory;",
        "workflow",
        "Lcom/squareup/workflow/Workflow;",
        "getWorkflow",
        "()Lcom/squareup/workflow/Workflow;",
        "workflowHosts",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/workflow/rx2/RxWorkflowHost;",
        "Lcom/squareup/container/RenderedPropsWorkflow$PropsAndRendering;",
        "buildDialog",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "treeKey",
        "contextForNewDialog",
        "Landroid/content/Context;",
        "buildDialog$public_release",
        "buildDialogForTest",
        "buildView",
        "Landroid/view/View;",
        "contextForNewView",
        "container",
        "Landroid/view/ViewGroup;",
        "buildView$public_release",
        "buildViewForTest",
        "createTreeKey",
        "parent",
        "screen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "layer",
        "Lcom/squareup/container/ContainerLayering;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "hint",
        "Lcom/squareup/workflow/ScreenHint;",
        "isPersistent",
        "dropSpentWorkflow",
        "",
        "reason",
        "ensureWorkflow",
        "maybeRestore",
        "key",
        "onEnterScope",
        "newScope",
        "Lmortar/MortarScope;",
        "onResult",
        "result",
        "onUpdateScreens",
        "registerServices",
        "scopeBuilder",
        "Lmortar/MortarScope$Builder;",
        "renderingToLayering",
        "",
        "Lcom/squareup/workflow/LayeredScreen;",
        "rendering",
        "(Ljava/lang/Object;)Ljava/util/Map;",
        "screensOfLayer",
        "kotlin.jvm.PlatformType",
        "startOrRestart",
        "startWorkflow",
        "stop",
        "workflowLayerings",
        "Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;",
        "workflowPropsRenderingsSnapshots",
        "dedupForLegacyWorkflows",
        "renderingsToLayeredRenderings",
        "switchMapRunning",
        "T",
        "transform",
        "Lkotlin/Function1;",
        "Companion",
        "PropsRenderingSnapshot",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/container/AbstractPosWorkflowRunner$Companion;

.field private static final NoWorkflowSentinel:Lcom/squareup/workflow/rx2/RxWorkflowHost;


# instance fields
.field private final _results:Lio/reactivex/observables/ConnectableObservable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/observables/ConnectableObservable<",
            "TO;>;"
        }
    .end annotation
.end field

.field private final cancelAfterOneResult:Z

.field private final containerHints$delegate:Lkotlin/Lazy;

.field private final layeringUtils:Lcom/squareup/container/PosLayeringUtils;

.field private final mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

.field private final nextHistory:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lflow/History;",
            ">;"
        }
    .end annotation
.end field

.field private final propsChannel:Lkotlinx/coroutines/channels/ConflatedBroadcastChannel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/coroutines/channels/ConflatedBroadcastChannel<",
            "TP;>;"
        }
    .end annotation
.end field

.field private final propsFlow:Lkotlinx/coroutines/flow/Flow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/coroutines/flow/Flow<",
            "TP;>;"
        }
    .end annotation
.end field

.field private scopeKey:Lcom/squareup/container/ContainerTreeKey;

.field private final serviceName:Ljava/lang/String;

.field private final showFlowScreens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/container/WorkflowTreeKey;",
            ">;>;"
        }
    .end annotation
.end field

.field private final workflowHosts:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/workflow/rx2/RxWorkflowHost<",
            "TO;+",
            "Lcom/squareup/container/RenderedPropsWorkflow$PropsAndRendering<",
            "TP;TR;>;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/container/AbstractPosWorkflowRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/container/AbstractPosWorkflowRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/container/AbstractPosWorkflowRunner;->Companion:Lcom/squareup/container/AbstractPosWorkflowRunner$Companion;

    .line 522
    new-instance v0, Lcom/squareup/container/AbstractPosWorkflowRunner$Companion$NoWorkflowSentinel$1;

    invoke-direct {v0}, Lcom/squareup/container/AbstractPosWorkflowRunner$Companion$NoWorkflowSentinel$1;-><init>()V

    check-cast v0, Lcom/squareup/workflow/rx2/RxWorkflowHost;

    sput-object v0, Lcom/squareup/container/AbstractPosWorkflowRunner;->NoWorkflowSentinel:Lcom/squareup/workflow/rx2/RxWorkflowHost;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lio/reactivex/Observable;ZLkotlinx/coroutines/CoroutineDispatcher;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lio/reactivex/Observable<",
            "Lflow/History;",
            ">;Z",
            "Lkotlinx/coroutines/CoroutineDispatcher;",
            ")V"
        }
    .end annotation

    const-string v0, "serviceName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nextHistory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainDispatcher"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->serviceName:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->nextHistory:Lio/reactivex/Observable;

    iput-boolean p3, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->cancelAfterOneResult:Z

    iput-object p4, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    .line 157
    sget-object p1, Lcom/squareup/container/AbstractPosWorkflowRunner;->NoWorkflowSentinel:Lcom/squareup/workflow/rx2/RxWorkflowHost;

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefault(NoWorkflowSentinel)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->workflowHosts:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 172
    iget-object p1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->workflowHosts:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast p1, Lio/reactivex/Observable;

    .line 173
    sget-object p2, Lcom/squareup/container/AbstractPosWorkflowRunner$_results$1;->INSTANCE:Lcom/squareup/container/AbstractPosWorkflowRunner$_results$1;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, p1, p2}, Lcom/squareup/container/AbstractPosWorkflowRunner;->switchMapRunning(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;

    move-result-object p1

    .line 174
    new-instance p2, Lcom/squareup/container/AbstractPosWorkflowRunner$_results$2;

    move-object p3, p0

    check-cast p3, Lcom/squareup/container/AbstractPosWorkflowRunner;

    invoke-direct {p2, p3}, Lcom/squareup/container/AbstractPosWorkflowRunner$_results$2;-><init>(Lcom/squareup/container/AbstractPosWorkflowRunner;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    new-instance p3, Lcom/squareup/container/AbstractPosWorkflowRunner$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {p3, p2}, Lcom/squareup/container/AbstractPosWorkflowRunner$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast p3, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, p3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p1

    .line 175
    invoke-virtual {p1}, Lio/reactivex/Observable;->publish()Lio/reactivex/observables/ConnectableObservable;

    move-result-object p1

    const-string/jumbo p2, "workflowHosts\n      .swi\u2026nResult)\n      .publish()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->_results:Lio/reactivex/observables/ConnectableObservable;

    .line 178
    iget-object p1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->workflowHosts:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast p1, Lio/reactivex/Observable;

    new-instance p2, Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1;

    invoke-direct {p2, p0}, Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1;-><init>(Lcom/squareup/container/AbstractPosWorkflowRunner;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, p1, p2}, Lcom/squareup/container/AbstractPosWorkflowRunner;->switchMapRunning(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->showFlowScreens:Lio/reactivex/Observable;

    .line 223
    sget-object p1, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    new-instance p2, Lcom/squareup/container/AbstractPosWorkflowRunner$containerHints$2;

    invoke-direct {p2, p0}, Lcom/squareup/container/AbstractPosWorkflowRunner$containerHints$2;-><init>(Lcom/squareup/container/AbstractPosWorkflowRunner;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, p2}, Lkotlin/LazyKt;->lazy(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->containerHints$delegate:Lkotlin/Lazy;

    .line 227
    new-instance p1, Lkotlinx/coroutines/channels/ConflatedBroadcastChannel;

    invoke-direct {p1}, Lkotlinx/coroutines/channels/ConflatedBroadcastChannel;-><init>()V

    iput-object p1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->propsChannel:Lkotlinx/coroutines/channels/ConflatedBroadcastChannel;

    .line 236
    iget-object p1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->propsChannel:Lkotlinx/coroutines/channels/ConflatedBroadcastChannel;

    check-cast p1, Lkotlinx/coroutines/channels/BroadcastChannel;

    invoke-static {p1}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlinx/coroutines/channels/BroadcastChannel;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->propsFlow:Lkotlinx/coroutines/flow/Flow;

    .line 260
    new-instance p1, Lcom/squareup/container/PosLayeringUtils;

    invoke-direct {p1}, Lcom/squareup/container/PosLayeringUtils;-><init>()V

    iput-object p1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->layeringUtils:Lcom/squareup/container/PosLayeringUtils;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lio/reactivex/Observable;ZLkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_0

    .line 144
    invoke-static {}, Lcom/squareup/container/CoroutineDispatcherKt;->getMainImmediateDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object p4

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/container/AbstractPosWorkflowRunner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;ZLkotlinx/coroutines/CoroutineDispatcher;)V

    return-void
.end method

.method public static final synthetic access$createTreeKey(Lcom/squareup/container/AbstractPosWorkflowRunner;Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/ContainerLayering;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)Lcom/squareup/container/WorkflowTreeKey;
    .locals 0

    .line 140
    invoke-direct/range {p0 .. p8}, Lcom/squareup/container/AbstractPosWorkflowRunner;->createTreeKey(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/ContainerLayering;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)Lcom/squareup/container/WorkflowTreeKey;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$dropSpentWorkflow(Lcom/squareup/container/AbstractPosWorkflowRunner;Ljava/lang/String;)V
    .locals 0

    .line 140
    invoke-direct {p0, p1}, Lcom/squareup/container/AbstractPosWorkflowRunner;->dropSpentWorkflow(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$getNoWorkflowSentinel$cp()Lcom/squareup/workflow/rx2/RxWorkflowHost;
    .locals 1

    .line 140
    sget-object v0, Lcom/squareup/container/AbstractPosWorkflowRunner;->NoWorkflowSentinel:Lcom/squareup/workflow/rx2/RxWorkflowHost;

    return-object v0
.end method

.method public static final synthetic access$getScopeKey$p(Lcom/squareup/container/AbstractPosWorkflowRunner;)Lcom/squareup/container/ContainerTreeKey;
    .locals 0

    .line 140
    iget-object p0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->scopeKey:Lcom/squareup/container/ContainerTreeKey;

    return-object p0
.end method

.method public static final synthetic access$getServiceName$p(Lcom/squareup/container/AbstractPosWorkflowRunner;)Ljava/lang/String;
    .locals 0

    .line 140
    iget-object p0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->serviceName:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic access$maybeRestore(Lcom/squareup/container/AbstractPosWorkflowRunner;Lcom/squareup/container/WorkflowTreeKey;)V
    .locals 0

    .line 140
    invoke-direct {p0, p1}, Lcom/squareup/container/AbstractPosWorkflowRunner;->maybeRestore(Lcom/squareup/container/WorkflowTreeKey;)V

    return-void
.end method

.method public static final synthetic access$onResult(Lcom/squareup/container/AbstractPosWorkflowRunner;Ljava/lang/Object;)V
    .locals 0

    .line 140
    invoke-direct {p0, p1}, Lcom/squareup/container/AbstractPosWorkflowRunner;->onResult(Ljava/lang/Object;)V

    return-void
.end method

.method public static final synthetic access$renderingsToLayeredRenderings(Lcom/squareup/container/AbstractPosWorkflowRunner;Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;)Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;
    .locals 0

    .line 140
    invoke-direct {p0, p1}, Lcom/squareup/container/AbstractPosWorkflowRunner;->renderingsToLayeredRenderings(Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;)Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$setScopeKey$p(Lcom/squareup/container/AbstractPosWorkflowRunner;Lcom/squareup/container/ContainerTreeKey;)V
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->scopeKey:Lcom/squareup/container/ContainerTreeKey;

    return-void
.end method

.method private final createTreeKey(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/ContainerLayering;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)Lcom/squareup/container/WorkflowTreeKey;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/container/ContainerTreeKey;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/Snapshot;",
            "Ljava/lang/Object;",
            "Lcom/squareup/workflow/ScreenHint;",
            "Z)",
            "Lcom/squareup/container/WorkflowTreeKey;"
        }
    .end annotation

    move-object v0, p3

    .line 455
    invoke-virtual/range {p7 .. p7}, Lcom/squareup/workflow/ScreenHint;->getSection()Ljava/lang/Class;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 456
    new-instance v1, Lcom/squareup/container/WorkflowSectionKey;

    iget-object v5, v0, Lcom/squareup/workflow/legacy/Screen;->key:Lcom/squareup/workflow/legacy/Screen$Key;

    move-object v2, v1

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v2 .. v9}, Lcom/squareup/container/WorkflowSectionKey;-><init>(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)V

    check-cast v1, Lcom/squareup/container/WorkflowTreeKey;

    goto :goto_0

    .line 458
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/container/AbstractPosWorkflowRunner;->getLayeringUtils()Lcom/squareup/container/PosLayeringUtils;

    move-result-object v2

    .line 459
    iget-object v6, v0, Lcom/squareup/workflow/legacy/Screen;->key:Lcom/squareup/workflow/legacy/Screen$Key;

    move-object v3, p4

    move-object v4, p1

    move-object v5, p2

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move/from16 v10, p8

    .line 458
    invoke-virtual/range {v2 .. v10}, Lcom/squareup/container/PosLayeringUtils;->layeringToTreeKey(Lcom/squareup/container/ContainerLayering;Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)Lcom/squareup/container/WorkflowTreeKey;

    move-result-object v1

    :goto_0
    return-object v1
.end method

.method private final dropSpentWorkflow(Ljava/lang/String;)V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 472
    iget-object v1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->serviceName:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const-string p1, "dropSpentWorkflow (%s): %s"

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 473
    iget-object p1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->workflowHosts:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast p1, Lcom/squareup/workflow/rx2/RxWorkflowHost;

    invoke-interface {p1}, Lcom/squareup/workflow/rx2/RxWorkflowHost;->cancel()V

    .line 474
    iget-object p1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->workflowHosts:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v0, Lcom/squareup/container/AbstractPosWorkflowRunner;->NoWorkflowSentinel:Lcom/squareup/workflow/rx2/RxWorkflowHost;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private final getContainerHints()Lcom/squareup/workflow/ui/ContainerHints;
    .locals 1

    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->containerHints$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/ui/ContainerHints;

    return-object v0
.end method

.method private final maybeRestore(Lcom/squareup/container/WorkflowTreeKey;)V
    .locals 6

    .line 374
    invoke-virtual {p0}, Lcom/squareup/container/AbstractPosWorkflowRunner;->isRunning()Z

    move-result v0

    if-nez v0, :cond_2

    .line 375
    invoke-virtual {p1}, Lcom/squareup/container/WorkflowTreeKey;->isPersistent()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/container/WorkflowTreeKey;->getSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 378
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/container/WorkflowTreeKey;->isPersistent()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lcom/squareup/container/WorkflowTreeKey;->getProps()Ljava/lang/Object;

    move-result-object v2

    sget-object v4, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/2addr v2, v3

    if-eqz v2, :cond_1

    .line 379
    iget-object v2, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->propsFlow:Lkotlinx/coroutines/flow/Flow;

    new-instance v4, Lcom/squareup/container/AbstractPosWorkflowRunner$maybeRestore$restoredProps$1;

    invoke-direct {v4, p1, v1}, Lcom/squareup/container/AbstractPosWorkflowRunner$maybeRestore$restoredProps$1;-><init>(Lcom/squareup/container/WorkflowTreeKey;Lkotlin/coroutines/Continuation;)V

    check-cast v4, Lkotlin/jvm/functions/Function2;

    invoke-static {v2, v4}, Lkotlinx/coroutines/flow/FlowKt;->onStart(Lkotlinx/coroutines/flow/Flow;Lkotlin/jvm/functions/Function2;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v1

    goto :goto_1

    .line 381
    :cond_1
    iget-object v1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->propsFlow:Lkotlinx/coroutines/flow/Flow;

    :goto_1
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 383
    iget-object v5, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->serviceName:Ljava/lang/String;

    aput-object v5, v2, v4

    aput-object p1, v2, v3

    const-string p1, "restoring (%s) from %s"

    invoke-static {p1, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 384
    invoke-direct {p0, v1, v0}, Lcom/squareup/container/AbstractPosWorkflowRunner;->startWorkflow(Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;)V

    :cond_2
    return-void
.end method

.method private final onResult(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TO;)V"
        }
    .end annotation

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 465
    iget-object v1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->serviceName:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const-string p1, "result (%s): %s"

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 466
    iget-boolean p1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->cancelAfterOneResult:Z

    if-eqz p1, :cond_0

    const-string p1, "cancelAfterOneResult"

    .line 467
    invoke-direct {p0, p1}, Lcom/squareup/container/AbstractPosWorkflowRunner;->dropSpentWorkflow(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private final renderingsToLayeredRenderings(Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;)Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot<",
            "+TP;+TR;>;)",
            "Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot<",
            "TP;",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation

    .line 513
    new-instance v0, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;

    .line 514
    invoke-virtual {p1}, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->getProps()Ljava/lang/Object;

    move-result-object v1

    .line 515
    invoke-virtual {p1}, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->getRendering()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/squareup/container/AbstractPosWorkflowRunner;->renderingToLayering(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    .line 516
    invoke-virtual {p1}, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->getSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    .line 513
    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)V

    return-object v0
.end method

.method private final screensOfLayer(Lcom/squareup/container/ContainerLayering;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/container/ContainerLayering;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 491
    invoke-direct {p0}, Lcom/squareup/container/AbstractPosWorkflowRunner;->workflowLayerings()Lio/reactivex/Observable;

    move-result-object v0

    .line 492
    new-instance v1, Lcom/squareup/container/AbstractPosWorkflowRunner$screensOfLayer$1;

    invoke-direct {v1, p1}, Lcom/squareup/container/AbstractPosWorkflowRunner$screensOfLayer$1;-><init>(Lcom/squareup/container/ContainerLayering;)V

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 493
    new-instance v1, Lcom/squareup/container/AbstractPosWorkflowRunner$screensOfLayer$2;

    invoke-direct {v1, p1}, Lcom/squareup/container/AbstractPosWorkflowRunner$screensOfLayer$2;-><init>(Lcom/squareup/container/ContainerLayering;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method private final startWorkflow(Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlinx/coroutines/flow/Flow<",
            "+TP;>;",
            "Lcom/squareup/workflow/Snapshot;",
            ")V"
        }
    .end annotation

    .line 499
    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->workflowHosts:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 500
    new-instance v1, Lcom/squareup/container/RenderedPropsWorkflow;

    invoke-virtual {p0}, Lcom/squareup/container/AbstractPosWorkflowRunner;->getWorkflow()Lcom/squareup/workflow/Workflow;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/container/RenderedPropsWorkflow;-><init>(Lcom/squareup/workflow/Workflow;)V

    check-cast v1, Lcom/squareup/workflow/Workflow;

    iget-object v2, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    check-cast v2, Lkotlin/coroutines/CoroutineContext;

    invoke-static {p1, v1, p2, v2}, Lcom/squareup/workflow/rx2/RxWorkflowHostKt;->runAsRx(Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Workflow;Lcom/squareup/workflow/Snapshot;Lkotlin/coroutines/CoroutineContext;)Lcom/squareup/workflow/rx2/RxWorkflowHost;

    move-result-object p1

    .line 501
    invoke-virtual {p0, p1}, Lcom/squareup/container/AbstractPosWorkflowRunner;->dedupForLegacyWorkflows(Lcom/squareup/workflow/rx2/RxWorkflowHost;)Lcom/squareup/workflow/rx2/RxWorkflowHost;

    move-result-object p1

    .line 499
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic startWorkflow$default(Lcom/squareup/container/AbstractPosWorkflowRunner;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;ILjava/lang/Object;)V
    .locals 0

    if-nez p4, :cond_1

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 497
    check-cast p2, Lcom/squareup/workflow/Snapshot;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/container/AbstractPosWorkflowRunner;->startWorkflow(Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;)V

    return-void

    .line 0
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: startWorkflow"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private final switchMapRunning(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/rx2/RxWorkflowHost<",
            "TO;+",
            "Lcom/squareup/container/RenderedPropsWorkflow$PropsAndRendering<",
            "TP;TR;>;>;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/workflow/rx2/RxWorkflowHost<",
            "+TO;+",
            "Lcom/squareup/container/RenderedPropsWorkflow$PropsAndRendering<",
            "TP;TR;>;>;+",
            "Lio/reactivex/Observable<",
            "+TT;>;>;)",
            "Lio/reactivex/Observable<",
            "TT;>;"
        }
    .end annotation

    .line 163
    new-instance v0, Lcom/squareup/container/AbstractPosWorkflowRunner$switchMapRunning$1;

    invoke-direct {v0, p2}, Lcom/squareup/container/AbstractPosWorkflowRunner$switchMapRunning$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "switchMap { workflowHost\u2026rkflowHost)\n      }\n    }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final workflowLayerings()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot<",
            "TP;",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;>;"
        }
    .end annotation

    .line 488
    invoke-virtual {p0}, Lcom/squareup/container/AbstractPosWorkflowRunner;->workflowPropsRenderingsSnapshots()Lio/reactivex/Observable;

    move-result-object v0

    .line 489
    new-instance v1, Lcom/squareup/container/AbstractPosWorkflowRunner$workflowLayerings$1;

    invoke-direct {v1, p0}, Lcom/squareup/container/AbstractPosWorkflowRunner$workflowLayerings$1;-><init>(Lcom/squareup/container/AbstractPosWorkflowRunner;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final buildDialog$public_release(Lcom/squareup/container/WorkflowTreeKey;Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/container/WorkflowTreeKey;",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string/jumbo v0, "treeKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contextForNewDialog"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 359
    invoke-direct {p0, p1}, Lcom/squareup/container/AbstractPosWorkflowRunner;->maybeRestore(Lcom/squareup/container/WorkflowTreeKey;)V

    .line 360
    invoke-virtual {p0}, Lcom/squareup/container/AbstractPosWorkflowRunner;->getViewFactory()Lcom/squareup/workflow/WorkflowViewFactory;

    move-result-object v0

    .line 361
    iget-object v1, p1, Lcom/squareup/container/WorkflowTreeKey;->screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    .line 362
    invoke-virtual {p0}, Lcom/squareup/container/AbstractPosWorkflowRunner;->getLayeringUtils()Lcom/squareup/container/PosLayeringUtils;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/squareup/container/PosLayeringUtils;->treeKeyToLayering(Lcom/squareup/container/WorkflowTreeKey;)Lcom/squareup/container/ContainerLayering;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/container/AbstractPosWorkflowRunner;->screensOfLayer(Lcom/squareup/container/ContainerLayering;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v2, "screensOfLayer(layeringU\u2026eeKeyToLayering(treeKey))"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 360
    invoke-interface {v0, v1, p1, p2}, Lcom/squareup/workflow/WorkflowViewFactory;->buildDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lio/reactivex/Observable;Landroid/content/Context;)Lio/reactivex/Single;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    return-object p1
.end method

.method public final buildDialogForTest(Lcom/squareup/container/WorkflowTreeKey;Landroid/content/Context;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/container/WorkflowTreeKey;",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string/jumbo v0, "treeKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contextForNewDialog"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 352
    invoke-virtual {p0, p1, p2}, Lcom/squareup/container/AbstractPosWorkflowRunner;->buildDialog$public_release(Lcom/squareup/container/WorkflowTreeKey;Landroid/content/Context;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public final buildView$public_release(Lcom/squareup/container/WorkflowTreeKey;Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    const-string/jumbo v0, "treeKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contextForNewView"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 338
    invoke-direct {p0, p1}, Lcom/squareup/container/AbstractPosWorkflowRunner;->maybeRestore(Lcom/squareup/container/WorkflowTreeKey;)V

    .line 339
    invoke-virtual {p0}, Lcom/squareup/container/AbstractPosWorkflowRunner;->getViewFactory()Lcom/squareup/workflow/WorkflowViewFactory;

    move-result-object v1

    .line 340
    iget-object v2, p1, Lcom/squareup/container/WorkflowTreeKey;->screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    .line 341
    invoke-virtual {p0}, Lcom/squareup/container/AbstractPosWorkflowRunner;->getLayeringUtils()Lcom/squareup/container/PosLayeringUtils;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/container/PosLayeringUtils;->treeKeyToLayering(Lcom/squareup/container/WorkflowTreeKey;)Lcom/squareup/container/ContainerLayering;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/container/AbstractPosWorkflowRunner;->screensOfLayer(Lcom/squareup/container/ContainerLayering;)Lio/reactivex/Observable;

    move-result-object v3

    const-string p1, "screensOfLayer(layeringU\u2026eeKeyToLayering(treeKey))"

    invoke-static {v3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 344
    invoke-direct {p0}, Lcom/squareup/container/AbstractPosWorkflowRunner;->getContainerHints()Lcom/squareup/workflow/ui/ContainerHints;

    move-result-object v6

    move-object v4, p3

    move-object v5, p2

    .line 339
    invoke-interface/range {v1 .. v6}, Lcom/squareup/workflow/WorkflowViewFactory;->buildView(Lcom/squareup/workflow/legacy/Screen$Key;Lio/reactivex/Observable;Landroid/view/ViewGroup;Landroid/content/Context;Lcom/squareup/workflow/ui/ContainerHints;)Landroid/view/View;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    return-object p1
.end method

.method public final buildViewForTest(Lcom/squareup/container/WorkflowTreeKey;Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    const-string/jumbo v0, "treeKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contextForNewView"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 330
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/container/AbstractPosWorkflowRunner;->buildView$public_release(Lcom/squareup/container/WorkflowTreeKey;Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method protected dedupForLegacyWorkflows(Lcom/squareup/workflow/rx2/RxWorkflowHost;)Lcom/squareup/workflow/rx2/RxWorkflowHost;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<O:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/rx2/RxWorkflowHost<",
            "+TO;TR;>;)",
            "Lcom/squareup/workflow/rx2/RxWorkflowHost<",
            "TO;TR;>;"
        }
    .end annotation

    const-string v0, "$this$dedupForLegacyWorkflows"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method protected final ensureWorkflow()V
    .locals 1

    .line 297
    invoke-virtual {p0}, Lcom/squareup/container/AbstractPosWorkflowRunner;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/container/AbstractPosWorkflowRunner;->startOrRestart()V

    :cond_0
    return-void
.end method

.method protected getLayeringUtils()Lcom/squareup/container/PosLayeringUtils;
    .locals 1

    .line 260
    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->layeringUtils:Lcom/squareup/container/PosLayeringUtils;

    return-object v0
.end method

.method protected final getProps()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        level = .enum Lkotlin/DeprecationLevel;->ERROR:Lkotlin/DeprecationLevel;
        message = "Use propsChannel"
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = "propsChannel.value"
            imports = {}
        .end subannotation
    .end annotation

    .line 245
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method protected final getPropsFlow()Lkotlinx/coroutines/flow/Flow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlinx/coroutines/flow/Flow<",
            "TP;>;"
        }
    .end annotation

    .line 236
    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->propsFlow:Lkotlinx/coroutines/flow/Flow;

    return-object v0
.end method

.method protected abstract getViewFactory()Lcom/squareup/workflow/WorkflowViewFactory;
.end method

.method protected abstract getWorkflow()Lcom/squareup/workflow/Workflow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/Workflow<",
            "TP;TO;TR;>;"
        }
    .end annotation
.end method

.method protected final isRunning()Z
    .locals 2

    .line 265
    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->workflowHosts:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/rx2/RxWorkflowHost;

    sget-object v1, Lcom/squareup/container/AbstractPosWorkflowRunner;->NoWorkflowSentinel:Lcom/squareup/workflow/rx2/RxWorkflowHost;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 4

    const-string v0, "newScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 392
    invoke-static {p1}, Lcom/squareup/container/ContainerTreeKey;->hasKey(Lmortar/MortarScope;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-eqz v0, :cond_0

    .line 393
    invoke-static {p1}, Lcom/squareup/container/ContainerTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->scopeKey:Lcom/squareup/container/ContainerTreeKey;

    new-array v0, v3, [Ljava/lang/Object;

    .line 394
    iget-object v3, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->scopeKey:Lcom/squareup/container/ContainerTreeKey;

    aput-object v3, v0, v2

    iget-object v2, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->serviceName:Ljava/lang/String;

    aput-object v2, v0, v1

    const-string v1, "entering scope %s (%s)"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-array v0, v3, [Ljava/lang/Object;

    .line 396
    invoke-virtual {p1}, Lmortar/MortarScope;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    iget-object v2, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->serviceName:Ljava/lang/String;

    aput-object v2, v0, v1

    const-string v1, "entering scope %s (%s), no ContainerTreeKey found"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 404
    :goto_0
    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->nextHistory:Lio/reactivex/Observable;

    .line 405
    sget-object v1, Lcom/squareup/container/AbstractPosWorkflowRunner$onEnterScope$1;->INSTANCE:Lcom/squareup/container/AbstractPosWorkflowRunner$onEnterScope$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 406
    const-class v1, Lcom/squareup/container/WorkflowTreeKey;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    .line 407
    new-instance v1, Lcom/squareup/container/AbstractPosWorkflowRunner$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/container/AbstractPosWorkflowRunner$onEnterScope$2;-><init>(Lcom/squareup/container/AbstractPosWorkflowRunner;)V

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "nextHistory\n        .map\u2026viceName == serviceName }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 408
    new-instance v1, Lcom/squareup/container/AbstractPosWorkflowRunner$onEnterScope$3;

    invoke-direct {v1, p0}, Lcom/squareup/container/AbstractPosWorkflowRunner$onEnterScope$3;-><init>(Lcom/squareup/container/AbstractPosWorkflowRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 414
    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->nextHistory:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/container/AbstractPosWorkflowRunner$onEnterScope$myTreeKeysInFlow$1;

    invoke-direct {v1, p0}, Lcom/squareup/container/AbstractPosWorkflowRunner$onEnterScope$myTreeKeysInFlow$1;-><init>(Lcom/squareup/container/AbstractPosWorkflowRunner;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 421
    invoke-direct {p0}, Lcom/squareup/container/AbstractPosWorkflowRunner;->workflowLayerings()Lio/reactivex/Observable;

    move-result-object v1

    const-string/jumbo v2, "workflowLayerings()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "myTreeKeysInFlow"

    .line 422
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/ObservableSource;

    invoke-static {v1, v0}, Lcom/squareup/util/rx2/RxKotlinKt;->withLatestFrom(Lio/reactivex/Observable;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    .line 423
    new-instance v1, Lcom/squareup/container/AbstractPosWorkflowRunner$onEnterScope$4;

    invoke-direct {v1, p0}, Lcom/squareup/container/AbstractPosWorkflowRunner$onEnterScope$4;-><init>(Lcom/squareup/container/AbstractPosWorkflowRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 441
    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->_results:Lio/reactivex/observables/ConnectableObservable;

    invoke-virtual {v0}, Lio/reactivex/observables/ConnectableObservable;->connect()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "_results.connect()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 442
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public final onResult()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "+TO;>;"
        }
    .end annotation

    .line 300
    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->_results:Lio/reactivex/observables/ConnectableObservable;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method public onUpdateScreens()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/container/WorkflowTreeKey;",
            ">;>;"
        }
    .end annotation

    .line 302
    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->showFlowScreens:Lio/reactivex/Observable;

    return-object v0
.end method

.method public final registerServices(Lmortar/MortarScope$Builder;)V
    .locals 3

    const-string v0, "scopeBuilder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 305
    iget-object v1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->serviceName:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "registerServices (%s)"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 307
    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->serviceName:Ljava/lang/String;

    invoke-virtual {p1, v0, p0}, Lmortar/MortarScope$Builder;->withService(Ljava/lang/String;Ljava/lang/Object;)Lmortar/MortarScope$Builder;

    .line 315
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->serviceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "-AWR-STARTER"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/squareup/container/AbstractPosWorkflowRunner$registerServices$1;

    invoke-direct {v1, p0}, Lcom/squareup/container/AbstractPosWorkflowRunner$registerServices$1;-><init>(Lcom/squareup/container/AbstractPosWorkflowRunner;)V

    check-cast v1, Lmortar/Scoped;

    invoke-virtual {p1, v0, v1}, Lmortar/MortarScope$Builder;->withService(Ljava/lang/String;Lmortar/Scoped;)Lmortar/MortarScope$Builder;

    return-void
.end method

.method protected abstract renderingToLayering(Ljava/lang/Object;)Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation
.end method

.method protected final setProps(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;)V"
        }
    .end annotation

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 247
    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->propsChannel:Lkotlinx/coroutines/channels/ConflatedBroadcastChannel;

    invoke-virtual {v0, p1}, Lkotlinx/coroutines/channels/ConflatedBroadcastChannel;->offer(Ljava/lang/Object;)Z

    return-void
.end method

.method protected final startOrRestart()V
    .locals 3

    .line 273
    invoke-virtual {p0}, Lcom/squareup/container/AbstractPosWorkflowRunner;->isRunning()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_0

    new-array v0, v2, [Ljava/lang/Object;

    .line 274
    iget-object v2, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->serviceName:Ljava/lang/String;

    aput-object v2, v0, v1

    const-string v1, "starting (%s)"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    const-string v0, "startOrRestart"

    .line 276
    invoke-direct {p0, v0}, Lcom/squareup/container/AbstractPosWorkflowRunner;->dropSpentWorkflow(Ljava/lang/String;)V

    new-array v0, v2, [Ljava/lang/Object;

    .line 277
    iget-object v2, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->serviceName:Ljava/lang/String;

    aput-object v2, v0, v1

    const-string v1, "restarting (%s)"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 279
    :goto_0
    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->propsFlow:Lkotlinx/coroutines/flow/Flow;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {p0, v0, v2, v1, v2}, Lcom/squareup/container/AbstractPosWorkflowRunner;->startWorkflow$default(Lcom/squareup/container/AbstractPosWorkflowRunner;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;ILjava/lang/Object;)V

    return-void
.end method

.method protected final stop()V
    .locals 1

    .line 286
    invoke-virtual {p0}, Lcom/squareup/container/AbstractPosWorkflowRunner;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "stop"

    .line 287
    invoke-direct {p0, v0}, Lcom/squareup/container/AbstractPosWorkflowRunner;->dropSpentWorkflow(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected final workflowPropsRenderingsSnapshots()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot<",
            "TP;TR;>;>;"
        }
    .end annotation

    .line 478
    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner;->workflowHosts:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    sget-object v1, Lcom/squareup/container/AbstractPosWorkflowRunner$workflowPropsRenderingsSnapshots$1;->INSTANCE:Lcom/squareup/container/AbstractPosWorkflowRunner$workflowPropsRenderingsSnapshots$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v0, v1}, Lcom/squareup/container/AbstractPosWorkflowRunner;->switchMapRunning(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
