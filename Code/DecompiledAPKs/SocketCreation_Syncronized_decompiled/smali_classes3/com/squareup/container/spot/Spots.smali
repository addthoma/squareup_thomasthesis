.class public Lcom/squareup/container/spot/Spots;
.super Ljava/lang/Object;
.source "Spots.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/container/spot/Spots$GrowOverSpot;,
        Lcom/squareup/container/spot/Spots$HereCrossFadeSpot;,
        Lcom/squareup/container/spot/Spots$HereSpot;,
        Lcom/squareup/container/spot/Spots$BelowSpot;,
        Lcom/squareup/container/spot/Spots$RightSpot;,
        Lcom/squareup/container/spot/Spots$RightStableActionBarSpot;
    }
.end annotation


# static fields
.field public static final BELOW:Lcom/squareup/container/spot/Spot;

.field public static final GROW_OVER:Lcom/squareup/container/spot/Spot;

.field public static final HERE:Lcom/squareup/container/spot/Spot;

.field public static final HERE_CROSS_FADE:Lcom/squareup/container/spot/Spot;

.field public static final RIGHT:Lcom/squareup/container/spot/Spot;

.field public static final RIGHT_STABLE_ACTION_BAR:Lcom/squareup/container/spot/Spot;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 25
    new-instance v0, Lcom/squareup/container/spot/Spots$RightStableActionBarSpot;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/container/spot/Spots$RightStableActionBarSpot;-><init>(Lcom/squareup/container/spot/Spots$1;)V

    sput-object v0, Lcom/squareup/container/spot/Spots;->RIGHT_STABLE_ACTION_BAR:Lcom/squareup/container/spot/Spot;

    .line 27
    new-instance v0, Lcom/squareup/container/spot/Spots$RightSpot;

    invoke-direct {v0, v1}, Lcom/squareup/container/spot/Spots$RightSpot;-><init>(Lcom/squareup/container/spot/Spots$1;)V

    sput-object v0, Lcom/squareup/container/spot/Spots;->RIGHT:Lcom/squareup/container/spot/Spot;

    .line 30
    new-instance v0, Lcom/squareup/container/spot/Spots$BelowSpot;

    invoke-direct {v0, v1}, Lcom/squareup/container/spot/Spots$BelowSpot;-><init>(Lcom/squareup/container/spot/Spots$1;)V

    sput-object v0, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    .line 33
    new-instance v0, Lcom/squareup/container/spot/Spots$HereSpot;

    invoke-direct {v0, v1}, Lcom/squareup/container/spot/Spots$HereSpot;-><init>(Lcom/squareup/container/spot/Spots$1;)V

    sput-object v0, Lcom/squareup/container/spot/Spots;->HERE:Lcom/squareup/container/spot/Spot;

    .line 35
    new-instance v0, Lcom/squareup/container/spot/Spots$HereCrossFadeSpot;

    invoke-direct {v0, v1}, Lcom/squareup/container/spot/Spots$HereCrossFadeSpot;-><init>(Lcom/squareup/container/spot/Spots$1;)V

    sput-object v0, Lcom/squareup/container/spot/Spots;->HERE_CROSS_FADE:Lcom/squareup/container/spot/Spot;

    .line 38
    new-instance v0, Lcom/squareup/container/spot/Spots$GrowOverSpot;

    invoke-direct {v0, v1}, Lcom/squareup/container/spot/Spots$GrowOverSpot;-><init>(Lcom/squareup/container/spot/Spots$1;)V

    sput-object v0, Lcom/squareup/container/spot/Spots;->GROW_OVER:Lcom/squareup/container/spot/Spot;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
