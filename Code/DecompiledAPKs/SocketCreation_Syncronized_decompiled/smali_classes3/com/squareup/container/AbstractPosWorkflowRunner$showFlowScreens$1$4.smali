.class final Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1$4;
.super Ljava/lang/Object;
.source "AbstractPosWorkflowRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1;->invoke(Lcom/squareup/workflow/rx2/RxWorkflowHost;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAbstractPosWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AbstractPosWorkflowRunner.kt\ncom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1$4\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,525:1\n1360#2:526\n1429#2,3:527\n950#2:530\n*E\n*S KotlinDebug\n*F\n+ 1 AbstractPosWorkflowRunner.kt\ncom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1$4\n*L\n194#1:526\n194#1,3:527\n210#1:530\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004\"\u0008\u0008\u0001\u0010\u0005*\u00020\u0004\"\u0004\u0008\u0002\u0010\u000628\u0010\u0007\u001a4\u0012\u0004\u0012\u0002H\u0003\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\n\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000bj\u0002`\u000c0\tj\n\u0012\u0006\u0008\u0001\u0012\u00020\n`\r0\u0008H\n\u00a2\u0006\u0002\u0008\u000e"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/container/WorkflowTreeKey;",
        "P",
        "",
        "O",
        "R",
        "wfState",
        "Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1;


# direct methods
.method constructor <init>(Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1$4;->this$0:Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 140
    check-cast p1, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;

    invoke-virtual {p0, p1}, Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1$4;->apply(Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final apply(Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot<",
            "+TP;+",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;)",
            "Ljava/util/List<",
            "Lcom/squareup/container/WorkflowTreeKey;",
            ">;"
        }
    .end annotation

    const-string/jumbo v0, "wfState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 193
    invoke-virtual {p1}, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->getRendering()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 526
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 527
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 528
    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    move-object v8, v3

    check-cast v8, Lcom/squareup/container/ContainerLayering;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    move-object v7, v2

    check-cast v7, Lcom/squareup/workflow/legacy/Screen;

    .line 195
    iget-object v2, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1$4;->this$0:Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1;

    iget-object v2, v2, Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1;->this$0:Lcom/squareup/container/AbstractPosWorkflowRunner;

    invoke-virtual {v2}, Lcom/squareup/container/AbstractPosWorkflowRunner;->getViewFactory()Lcom/squareup/workflow/WorkflowViewFactory;

    move-result-object v2

    iget-object v3, v7, Lcom/squareup/workflow/legacy/Screen;->key:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-interface {v2, v3}, Lcom/squareup/workflow/WorkflowViewFactory;->getHintForKey(Lcom/squareup/workflow/legacy/Screen$Key;)Lcom/squareup/workflow/ScreenHint;

    move-result-object v11

    if-eqz v11, :cond_0

    .line 199
    iget-object v2, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1$4;->this$0:Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1;

    iget-object v4, v2, Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1;->this$0:Lcom/squareup/container/AbstractPosWorkflowRunner;

    .line 200
    iget-object v2, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1$4;->this$0:Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1;

    iget-object v2, v2, Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1;->this$0:Lcom/squareup/container/AbstractPosWorkflowRunner;

    invoke-static {v2}, Lcom/squareup/container/AbstractPosWorkflowRunner;->access$getServiceName$p(Lcom/squareup/container/AbstractPosWorkflowRunner;)Ljava/lang/String;

    move-result-object v5

    .line 201
    iget-object v2, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1$4;->this$0:Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1;

    iget-object v2, v2, Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1;->this$0:Lcom/squareup/container/AbstractPosWorkflowRunner;

    invoke-static {v2}, Lcom/squareup/container/AbstractPosWorkflowRunner;->access$getScopeKey$p(Lcom/squareup/container/AbstractPosWorkflowRunner;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v6

    .line 204
    invoke-virtual {p1}, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->getSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object v9

    .line 205
    invoke-virtual {p1}, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->getProps()Ljava/lang/Object;

    move-result-object v10

    .line 207
    invoke-virtual {p1}, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->getRendering()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    invoke-static {v2}, Lcom/squareup/workflow/LayeredScreenKt;->isPersistent(Ljava/util/Map;)Z

    move-result v12

    .line 199
    invoke-static/range {v4 .. v12}, Lcom/squareup/container/AbstractPosWorkflowRunner;->access$createTreeKey(Lcom/squareup/container/AbstractPosWorkflowRunner;Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/ContainerLayering;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)Lcom/squareup/container/WorkflowTreeKey;

    move-result-object v2

    .line 208
    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 196
    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Missing ScreenHint for "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v7, Lcom/squareup/workflow/legacy/Screen;->key:Lcom/squareup/workflow/legacy/Screen$Key;

    iget-object v0, v0, Lcom/squareup/workflow/legacy/Screen$Key;->typeName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ". Did you forget to include "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "the screen in your view factory?"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 195
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 529
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 530
    new-instance p1, Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1$4$$special$$inlined$sortedBy$1;

    invoke-direct {p1, p0}, Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1$4$$special$$inlined$sortedBy$1;-><init>(Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1$4;)V

    check-cast p1, Ljava/util/Comparator;

    invoke-static {v1, p1}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p1

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 211
    iget-object v2, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1$4;->this$0:Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1;

    iget-object v2, v2, Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1;->this$0:Lcom/squareup/container/AbstractPosWorkflowRunner;

    invoke-static {v2}, Lcom/squareup/container/AbstractPosWorkflowRunner;->access$getServiceName$p(Lcom/squareup/container/AbstractPosWorkflowRunner;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const-string v1, "showFlowScreens(%s): %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object p1
.end method
