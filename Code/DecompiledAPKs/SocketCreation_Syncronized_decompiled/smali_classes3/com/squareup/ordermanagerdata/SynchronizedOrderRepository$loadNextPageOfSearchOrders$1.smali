.class final Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$loadNextPageOfSearchOrders$1;
.super Ljava/lang/Object;
.source "SynchronizedOrderRepository.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->loadNextPageOfSearchOrders(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/orders/SearchOrdersResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $searchInFlight:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;


# direct methods
.method constructor <init>(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$loadNextPageOfSearchOrders$1;->$searchInFlight:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/orders/SearchOrdersResponse;",
            ">;)",
            "Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 309
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 310
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$loadNextPageOfSearchOrders$1;->$searchInFlight:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;

    .line 311
    new-instance v0, Lcom/squareup/ordermanagerdata/ResultState$Success;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/orders/SearchOrdersResponse;

    iget-object v2, v2, Lcom/squareup/protos/client/orders/SearchOrdersResponse;->orders:Ljava/util/List;

    invoke-direct {v0, v2}, Lcom/squareup/ordermanagerdata/ResultState$Success;-><init>(Ljava/lang/Object;)V

    move-object v2, v0

    check-cast v2, Lcom/squareup/ordermanagerdata/ResultState;

    const/4 v3, 0x0

    .line 312
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/orders/SearchOrdersResponse;

    iget-object v4, p1, Lcom/squareup/protos/client/orders/SearchOrdersResponse;->cursor:Ljava/lang/String;

    .line 313
    iget-object p1, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$loadNextPageOfSearchOrders$1;->$searchInFlight:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;

    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->getPagesFetched()I

    move-result p1

    add-int/lit8 v5, p1, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x0

    .line 310
    invoke-static/range {v1 .. v7}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->copy$default(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;Lcom/squareup/ordermanagerdata/ResultState;Lcom/squareup/ordermanagerdata/SearchQuery;Ljava/lang/String;IILjava/lang/Object;)Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;

    move-result-object p1

    goto :goto_0

    .line 317
    :cond_0
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$loadNextPageOfSearchOrders$1;->$searchInFlight:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;

    sget-object p1, Lcom/squareup/ordermanagerdata/ResultState$Failure$GenericError;->INSTANCE:Lcom/squareup/ordermanagerdata/ResultState$Failure$GenericError;

    move-object v1, p1

    check-cast v1, Lcom/squareup/ordermanagerdata/ResultState;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xe

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->copy$default(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;Lcom/squareup/ordermanagerdata/ResultState;Lcom/squareup/ordermanagerdata/SearchQuery;Ljava/lang/String;IILjava/lang/Object;)Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$loadNextPageOfSearchOrders$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;

    move-result-object p1

    return-object p1
.end method
