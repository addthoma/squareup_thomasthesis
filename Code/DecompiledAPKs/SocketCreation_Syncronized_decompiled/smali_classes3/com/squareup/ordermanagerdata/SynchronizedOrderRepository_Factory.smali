.class public final Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository_Factory;
.super Ljava/lang/Object;
.source "SynchronizedOrderRepository_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/local/LocalOrderStore;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/sync/OrderSyncNotifier;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/local/LocalOrderStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/sync/OrderSyncNotifier;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 30
    iput-object p4, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository_Factory;->arg3Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/local/LocalOrderStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/sync/OrderSyncNotifier;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository_Factory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;Lcom/squareup/ordermanagerdata/local/LocalOrderStore;Lcom/squareup/ordermanagerdata/sync/OrderSyncNotifier;Lcom/squareup/settings/server/Features;)Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;
    .locals 1

    .line 46
    new-instance v0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;-><init>(Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;Lcom/squareup/ordermanagerdata/local/LocalOrderStore;Lcom/squareup/ordermanagerdata/sync/OrderSyncNotifier;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;
    .locals 4

    .line 35
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;

    iget-object v1, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ordermanagerdata/local/LocalOrderStore;

    iget-object v2, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ordermanagerdata/sync/OrderSyncNotifier;

    iget-object v3, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository_Factory;->newInstance(Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;Lcom/squareup/ordermanagerdata/local/LocalOrderStore;Lcom/squareup/ordermanagerdata/sync/OrderSyncNotifier;Lcom/squareup/settings/server/Features;)Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository_Factory;->get()Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;

    move-result-object v0

    return-object v0
.end method
