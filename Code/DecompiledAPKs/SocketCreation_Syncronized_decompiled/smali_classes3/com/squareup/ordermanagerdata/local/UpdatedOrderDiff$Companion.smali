.class public final Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff$Companion;
.super Ljava/lang/Object;
.source "InMemoryLocalOrderStore.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J \u0010\u0007\u001a\u00020\u00082\u0008\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010\u000b\u001a\u00020\u00052\u0006\u0010\u000c\u001a\u00020\u0004J\u001a\u0010\r\u001a\u00020\u00042\u0008\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010\u000b\u001a\u00020\u0005H\u0002J\u001a\u0010\u000e\u001a\u00020\u00042\u0008\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010\u000b\u001a\u00020\u0005H\u0002J\u000c\u0010\u000f\u001a\u00020\u0004*\u00020\u0005H\u0002R\u0018\u0010\u0003\u001a\u00020\u0004*\u00020\u00058BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0006\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff$Companion;",
        "",
        "()V",
        "isUpcomingOrder",
        "",
        "Lcom/squareup/orders/model/Order;",
        "(Lcom/squareup/orders/model/Order;)Z",
        "create",
        "Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;",
        "localOrder",
        "Lcom/squareup/ordermanagerdata/local/WrappedOrder;",
        "updatedOrder",
        "isFirstSync",
        "didTransitionFromUpcomingToNew",
        "didTransitionOrderGroupOrDisplayState",
        "shouldBeNotified",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 447
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 447
    invoke-direct {p0}, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff$Companion;-><init>()V

    return-void
.end method

.method private final didTransitionFromUpcomingToNew(Lcom/squareup/ordermanagerdata/local/WrappedOrder;Lcom/squareup/orders/model/Order;)Z
    .locals 1

    if-eqz p1, :cond_0

    .line 509
    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/local/WrappedOrder;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getDisplayState(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->state:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    sget-object v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->UPCOMING:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    if-ne p1, v0, :cond_1

    invoke-static {p2}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getDisplayState(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->state:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    sget-object p2, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->NEW:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    if-ne p1, p2, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    return p1
.end method

.method private final didTransitionOrderGroupOrDisplayState(Lcom/squareup/ordermanagerdata/local/WrappedOrder;Lcom/squareup/orders/model/Order;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 501
    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/local/WrappedOrder;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getDisplayState(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->state:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    invoke-static {p2}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getDisplayState(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->state:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    if-ne v1, v2, :cond_3

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/local/WrappedOrder;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-static {p1}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getGroup(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderGroup;

    move-result-object v0

    :cond_1
    invoke-static {p2}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getGroup(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderGroup;

    move-result-object p1

    if-eq v0, p1, :cond_2

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    goto :goto_2

    :cond_3
    :goto_1
    const/4 p1, 0x1

    :goto_2
    return p1
.end method

.method private final isUpcomingOrder(Lcom/squareup/orders/model/Order;)Z
    .locals 2

    .line 514
    invoke-static {p1}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getGroup(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderGroup;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/client/orders/OrderGroup;->UPCOMING:Lcom/squareup/protos/client/orders/OrderGroup;

    if-ne v0, v1, :cond_0

    invoke-static {p1}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getDisplayState(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->state:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    sget-object v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->UPCOMING:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final shouldBeNotified(Lcom/squareup/orders/model/Order;)Z
    .locals 2

    .line 517
    invoke-static {p1}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getDisplayState(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->state:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    sget-object v1, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->NEW:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    if-eq v0, v1, :cond_1

    invoke-static {p1}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getDisplayState(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->state:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    sget-object v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->UPCOMING:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method


# virtual methods
.method public final create(Lcom/squareup/ordermanagerdata/local/WrappedOrder;Lcom/squareup/orders/model/Order;Z)Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;
    .locals 11

    const-string/jumbo v0, "updatedOrder"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    .line 471
    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/local/WrappedOrder;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/orders/model/Order;->version:Ljava/lang/Integer;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p2, Lcom/squareup/orders/model/Order;->version:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    const/4 v2, 0x0

    if-nez v0, :cond_2

    move-object v0, p0

    check-cast v0, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff$Companion;

    invoke-direct {v0, p1, p2}, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff$Companion;->didTransitionOrderGroupOrDisplayState(Lcom/squareup/ordermanagerdata/local/WrappedOrder;Lcom/squareup/orders/model/Order;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v5, 0x1

    :goto_2
    if-eqz p3, :cond_3

    const/4 v0, 0x1

    goto :goto_3

    :cond_3
    if-eqz p1, :cond_4

    .line 473
    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/local/WrappedOrder;->isKnown()Z

    move-result v0

    goto :goto_3

    :cond_4
    const/4 v0, 0x0

    :goto_3
    if-eqz p3, :cond_5

    const/4 p3, 0x1

    goto :goto_4

    :cond_5
    if-eqz p1, :cond_6

    .line 474
    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/local/WrappedOrder;->isPrinted()Z

    move-result p3

    goto :goto_4

    :cond_6
    const/4 p3, 0x0

    .line 475
    :goto_4
    new-instance v4, Lcom/squareup/ordermanagerdata/local/WrappedOrder;

    invoke-direct {v4, p2, v0, p3}, Lcom/squareup/ordermanagerdata/local/WrappedOrder;-><init>(Lcom/squareup/orders/model/Order;ZZ)V

    .line 483
    move-object v3, p0

    check-cast v3, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff$Companion;

    .line 482
    invoke-direct {v3, p1, p2}, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff$Companion;->didTransitionFromUpcomingToNew(Lcom/squareup/ordermanagerdata/local/WrappedOrder;Lcom/squareup/orders/model/Order;)Z

    move-result p1

    if-nez p1, :cond_8

    .line 483
    invoke-direct {v3, p2}, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff$Companion;->shouldBeNotified(Lcom/squareup/orders/model/Order;)Z

    move-result p1

    if-eqz p1, :cond_7

    if-nez v0, :cond_7

    goto :goto_5

    :cond_7
    const/4 v9, 0x0

    goto :goto_6

    :cond_8
    :goto_5
    const/4 v9, 0x1

    .line 485
    :goto_6
    new-instance p1, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;

    .line 488
    invoke-direct {v3, p2}, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff$Companion;->isUpcomingOrder(Lcom/squareup/orders/model/Order;)Z

    move-result v0

    if-eqz v0, :cond_9

    if-eqz v5, :cond_9

    const/4 v7, 0x1

    goto :goto_7

    :cond_9
    const/4 v7, 0x0

    .line 489
    :goto_7
    invoke-static {p2}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getGroup(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderGroup;

    move-result-object v0

    sget-object v3, Lcom/squareup/protos/client/orders/OrderGroup;->ACTIVE:Lcom/squareup/protos/client/orders/OrderGroup;

    if-ne v0, v3, :cond_a

    if-eqz v5, :cond_a

    const/4 v6, 0x1

    goto :goto_8

    :cond_a
    const/4 v6, 0x0

    .line 490
    :goto_8
    invoke-static {p2}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getGroup(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderGroup;

    move-result-object v0

    sget-object v3, Lcom/squareup/protos/client/orders/OrderGroup;->COMPLETED:Lcom/squareup/protos/client/orders/OrderGroup;

    if-ne v0, v3, :cond_b

    if-eqz v5, :cond_b

    const/4 v8, 0x1

    goto :goto_9

    :cond_b
    const/4 v8, 0x0

    .line 492
    :goto_9
    invoke-static {p2}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getDisplayState(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    move-result-object p2

    iget-object p2, p2, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->state:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    sget-object v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->NEW:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    if-ne p2, v0, :cond_c

    if-nez p3, :cond_c

    const/4 v10, 0x1

    goto :goto_a

    :cond_c
    const/4 v10, 0x0

    :goto_a
    move-object v3, p1

    .line 485
    invoke-direct/range {v3 .. v10}, Lcom/squareup/ordermanagerdata/local/UpdatedOrderDiff;-><init>(Lcom/squareup/ordermanagerdata/local/WrappedOrder;ZZZZZZ)V

    return-object p1
.end method
