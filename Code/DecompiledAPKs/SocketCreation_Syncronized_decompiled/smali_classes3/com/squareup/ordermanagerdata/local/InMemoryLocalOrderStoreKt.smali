.class public final Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;
.super Ljava/lang/Object;
.source "InMemoryLocalOrderStore.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInMemoryLocalOrderStore.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InMemoryLocalOrderStore.kt\ncom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,528:1\n310#2,7:529\n310#2,7:536\n310#2,7:543\n*E\n*S KotlinDebug\n*F\n+ 1 InMemoryLocalOrderStore.kt\ncom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt\n*L\n371#1,7:529\n384#1,7:536\n397#1,7:543\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u001a \u0010\u0005\u001a\u00020\u0006*\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00020\u00080\u00072\u0006\u0010\t\u001a\u00020\u0002H\u0002\u001a>\u0010\n\u001a\u00020\u0006*\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00020\u00080\u00072\u0006\u0010\u000b\u001a\u00020\u00022\u001c\u0008\u0002\u0010\u000c\u001a\u0016\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u0001j\n\u0012\u0004\u0012\u00020\u0002\u0018\u0001`\u0003H\u0002\u001a\u0014\u0010\r\u001a\u00020\u000e*\u00020\u00022\u0006\u0010\u000f\u001a\u00020\u0010H\u0002\u001a \u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0008*\u0008\u0012\u0004\u0012\u00020\u00020\u00082\u0006\u0010\t\u001a\u00020\u0002H\u0002\u001a\u001a\u0010\u0011\u001a\u00020\u0006*\u0008\u0012\u0004\u0012\u00020\u00020\u00122\u0006\u0010\t\u001a\u00020\u0002H\u0002\u001a \u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0008*\u0008\u0012\u0004\u0012\u00020\u00020\u00082\u0006\u0010\u000b\u001a\u00020\u0002H\u0002\"\u001e\u0010\u0000\u001a\u0012\u0012\u0004\u0012\u00020\u00020\u0001j\u0008\u0012\u0004\u0012\u00020\u0002`\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\"\u001e\u0010\u0004\u001a\u0012\u0012\u0004\u0012\u00020\u00020\u0001j\u0008\u0012\u0004\u0012\u00020\u0002`\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000*\u000c\u0008\u0002\u0010\u0014\"\u00020\u00152\u00020\u0015\u00a8\u0006\u0016"
    }
    d2 = {
        "ACTIVE_ORDER_COMPARATOR",
        "Ljava/util/Comparator;",
        "Lcom/squareup/orders/model/Order;",
        "Lkotlin/Comparator;",
        "COMPLETED_ORDER_COMPARATOR",
        "acceptWithRemovedOrder",
        "",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "",
        "removedOrder",
        "acceptWithUpdatedOrder",
        "updatedOrder",
        "comparator",
        "inSameGroupAsQuery",
        "",
        "query",
        "Lcom/squareup/ordermanagerdata/SearchQuery;",
        "removeOrder",
        "",
        "updateOrder",
        "OrderId",
        "",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final ACTIVE_ORDER_COMPARATOR:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lcom/squareup/orders/model/Order;",
            ">;"
        }
    .end annotation
.end field

.field private static final COMPLETED_ORDER_COMPARATOR:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lcom/squareup/orders/model/Order;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 522
    new-instance v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt$$special$$inlined$compareByDescending$1;

    invoke-direct {v0}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt$$special$$inlined$compareByDescending$1;-><init>()V

    check-cast v0, Ljava/util/Comparator;

    .line 523
    new-instance v1, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt$$special$$inlined$thenBy$1;

    invoke-direct {v1, v0}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt$$special$$inlined$thenBy$1;-><init>(Ljava/util/Comparator;)V

    check-cast v1, Ljava/util/Comparator;

    sput-object v1, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->ACTIVE_ORDER_COMPARATOR:Ljava/util/Comparator;

    .line 526
    new-instance v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt$$special$$inlined$compareByDescending$2;

    invoke-direct {v0}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt$$special$$inlined$compareByDescending$2;-><init>()V

    check-cast v0, Ljava/util/Comparator;

    .line 527
    new-instance v1, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt$$special$$inlined$thenBy$2;

    invoke-direct {v1, v0}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt$$special$$inlined$thenBy$2;-><init>(Ljava/util/Comparator;)V

    check-cast v1, Ljava/util/Comparator;

    sput-object v1, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->COMPLETED_ORDER_COMPARATOR:Ljava/util/Comparator;

    return-void
.end method

.method private static final acceptWithRemovedOrder(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lcom/squareup/orders/model/Order;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;",
            "Lcom/squareup/orders/model/Order;",
            ")V"
        }
    .end annotation

    .line 361
    invoke-virtual {p0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    const-string/jumbo v1, "value ?: emptyList()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 362
    invoke-static {v0, p1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->removeOrder(Ljava/util/List;Lcom/squareup/orders/model/Order;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private static final acceptWithUpdatedOrder(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lcom/squareup/orders/model/Order;Ljava/util/Comparator;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;",
            "Lcom/squareup/orders/model/Order;",
            "Ljava/util/Comparator<",
            "Lcom/squareup/orders/model/Order;",
            ">;)V"
        }
    .end annotation

    .line 347
    invoke-virtual {p0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    const-string/jumbo v1, "value ?: emptyList()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 348
    invoke-static {v0, p1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->updateOrder(Ljava/util/List;Lcom/squareup/orders/model/Order;)Ljava/util/List;

    move-result-object p1

    if-nez p2, :cond_1

    .line 351
    invoke-virtual {p0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_1

    .line 353
    :cond_1
    check-cast p1, Ljava/lang/Iterable;

    invoke-static {p1, p2}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :goto_1
    return-void
.end method

.method static synthetic acceptWithUpdatedOrder$default(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lcom/squareup/orders/model/Order;Ljava/util/Comparator;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 345
    check-cast p2, Ljava/util/Comparator;

    :cond_0
    invoke-static {p0, p1, p2}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->acceptWithUpdatedOrder(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lcom/squareup/orders/model/Order;Ljava/util/Comparator;)V

    return-void
.end method

.method public static final synthetic access$acceptWithRemovedOrder(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lcom/squareup/orders/model/Order;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->acceptWithRemovedOrder(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lcom/squareup/orders/model/Order;)V

    return-void
.end method

.method public static final synthetic access$acceptWithUpdatedOrder(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lcom/squareup/orders/model/Order;Ljava/util/Comparator;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->acceptWithUpdatedOrder(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lcom/squareup/orders/model/Order;Ljava/util/Comparator;)V

    return-void
.end method

.method public static final synthetic access$getACTIVE_ORDER_COMPARATOR$p()Ljava/util/Comparator;
    .locals 1

    .line 1
    sget-object v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->ACTIVE_ORDER_COMPARATOR:Ljava/util/Comparator;

    return-object v0
.end method

.method public static final synthetic access$getCOMPLETED_ORDER_COMPARATOR$p()Ljava/util/Comparator;
    .locals 1

    .line 1
    sget-object v0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->COMPLETED_ORDER_COMPARATOR:Ljava/util/Comparator;

    return-object v0
.end method

.method public static final synthetic access$inSameGroupAsQuery(Lcom/squareup/orders/model/Order;Lcom/squareup/ordermanagerdata/SearchQuery;)Z
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->inSameGroupAsQuery(Lcom/squareup/orders/model/Order;Lcom/squareup/ordermanagerdata/SearchQuery;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$removeOrder(Ljava/util/List;Lcom/squareup/orders/model/Order;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->removeOrder(Ljava/util/List;Lcom/squareup/orders/model/Order;)V

    return-void
.end method

.method private static final inSameGroupAsQuery(Lcom/squareup/orders/model/Order;Lcom/squareup/ordermanagerdata/SearchQuery;)Z
    .locals 0

    .line 404
    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/SearchQuery;->getOrderGroup()Lcom/squareup/protos/client/orders/OrderGroup;

    move-result-object p1

    invoke-static {p0}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getGroup(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderGroup;

    move-result-object p0

    if-ne p1, p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static final removeOrder(Ljava/util/List;Lcom/squareup/orders/model/Order;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;",
            "Lcom/squareup/orders/model/Order;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;"
        }
    .end annotation

    .line 537
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 538
    check-cast v2, Lcom/squareup/orders/model/Order;

    .line 384
    iget-object v2, v2, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    :goto_1
    if-gez v1, :cond_2

    goto :goto_2

    .line 390
    :cond_2
    check-cast p0, Ljava/util/Collection;

    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p0

    .line 391
    invoke-interface {p0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :goto_2
    return-object p0
.end method

.method private static final removeOrder(Ljava/util/List;Lcom/squareup/orders/model/Order;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;",
            "Lcom/squareup/orders/model/Order;",
            ")V"
        }
    .end annotation

    .line 544
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 545
    check-cast v2, Lcom/squareup/orders/model/Order;

    .line 397
    iget-object v2, v2, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    :goto_1
    if-ltz v1, :cond_2

    .line 399
    invoke-interface {p0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_2
    return-void
.end method

.method private static final updateOrder(Ljava/util/List;Lcom/squareup/orders/model/Order;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;",
            "Lcom/squareup/orders/model/Order;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;"
        }
    .end annotation

    .line 530
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 531
    check-cast v2, Lcom/squareup/orders/model/Order;

    .line 371
    iget-object v2, v2, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    :goto_1
    if-gez v1, :cond_2

    .line 374
    check-cast p0, Ljava/util/Collection;

    invoke-static {p0, p1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    goto :goto_2

    .line 377
    :cond_2
    check-cast p0, Ljava/util/Collection;

    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p0

    .line 378
    invoke-interface {p0, v1, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :goto_2
    return-object p0
.end method
