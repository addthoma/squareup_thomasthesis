.class public final Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;
.super Ljava/lang/Object;
.source "RealRemoteOrderStore.kt"

# interfaces
.implements Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealRemoteOrderStore.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealRemoteOrderStore.kt\ncom/squareup/ordermanagerdata/remote/RealRemoteOrderStore\n*L\n1#1,444:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00ae\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u0000 @2\u00020\u0001:\u0001@B/\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0014\u0010\u0012\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00150\u00140\u0013H\u0016J2\u0010\u0016\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00170\u00140\u00132\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001b2\u000c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u001d0\u000eH\u0016J\u0008\u0010\u0008\u001a\u00020\u001eH\u0002J\u001e\u0010\u001f\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00150\u00140\u00132\u0008\u0010 \u001a\u0004\u0018\u00010\u000fH\u0016J\u0008\u0010!\u001a\u00020\u000fH\u0002J\u0010\u0010\"\u001a\u00020\u000f2\u0006\u0010#\u001a\u00020$H\u0007J.\u0010%\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020&0\u00140\u00132\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\'\u001a\u00020(2\u0008\u0010)\u001a\u0004\u0018\u00010*H\u0016J\u001c\u0010+\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020,0\u00140\u00132\u0006\u0010-\u001a\u00020\u000fH\u0016J&\u0010.\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00170\u00140\u00132\u0006\u0010\u0018\u001a\u00020\u00192\u0008\u0010/\u001a\u0004\u0018\u00010\u000fH\u0016J\u0018\u00100\u001a\u0002012\u0006\u0010#\u001a\u00020$2\u0006\u00102\u001a\u000203H\u0002J,\u00104\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00170\u00140\u00132\u0006\u00105\u001a\u0002062\u0006\u0010-\u001a\u00020\u000f2\u0006\u00107\u001a\u000208H\u0002J&\u00109\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00150\u00140\u00132\u0006\u0010:\u001a\u00020;2\u0008\u0010 \u001a\u0004\u0018\u00010\u000fH\u0016J4\u0010<\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00170\u00140\u00132\u0006\u0010\u0018\u001a\u00020\u00192\u0008\u0010)\u001a\u0004\u0018\u00010*2\u000c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u001d0\u000eH\u0016J$\u0010=\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00170\u00140\u00132\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010>\u001a\u00020?H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e*\u00020\u000b8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0011\u00a8\u0006A"
    }
    d2 = {
        "Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;",
        "Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;",
        "res",
        "Lcom/squareup/util/Res;",
        "currentTime",
        "Lcom/squareup/time/CurrentTime;",
        "orderHubService",
        "Lcom/squareup/server/orderhub/OrderHubService;",
        "clientSupport",
        "Lcom/squareup/ordermanagerdata/OrderManagerClientSupport;",
        "accountStatusSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "(Lcom/squareup/util/Res;Lcom/squareup/time/CurrentTime;Lcom/squareup/server/orderhub/OrderHubService;Lcom/squareup/ordermanagerdata/OrderManagerClientSupport;Lcom/squareup/settings/server/AccountStatusSettings;)V",
        "bazaarLocationIds",
        "",
        "",
        "getBazaarLocationIds",
        "(Lcom/squareup/settings/server/AccountStatusSettings;)Ljava/util/List;",
        "activeOrders",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/orders/SearchOrdersResponse;",
        "cancelOrder",
        "Lcom/squareup/protos/client/orders/PerformFulfillmentActionResponse;",
        "order",
        "Lcom/squareup/orders/model/Order;",
        "cancelReason",
        "Lcom/squareup/ordermanagerdata/CancellationReason;",
        "lineItemQuantities",
        "Lcom/squareup/protos/client/orders/LineItemQuantity;",
        "Lcom/squareup/protos/client/orders/ClientSupport;",
        "completedOrders",
        "paginationToken",
        "currentDateTime",
        "dateTimeFromMillisInPast",
        "millisInPast",
        "",
        "editTracking",
        "Lcom/squareup/protos/client/orders/UpdateOrderResponse;",
        "fulfillment",
        "Lcom/squareup/orders/model/Order$Fulfillment;",
        "tracking",
        "Lcom/squareup/ordermanagerdata/TrackingInfo;",
        "getOrder",
        "Lcom/squareup/protos/client/orders/GetOrderResponse;",
        "orderId",
        "markOrderInProgress",
        "pickupTimeOverride",
        "orderSearchTimeRange",
        "Lcom/squareup/orders/SearchOrdersDateTimeFilter;",
        "group",
        "Lcom/squareup/protos/client/orders/OrderGroup;",
        "performFulfillmentAction",
        "fulfillmentAction",
        "Lcom/squareup/protos/client/orders/FulfillmentAction;",
        "orderVersion",
        "",
        "searchOrders",
        "query",
        "Lcom/squareup/ordermanagerdata/SearchQuery;",
        "shipOrder",
        "transitionOrder",
        "action",
        "Lcom/squareup/protos/client/orders/Action$Type;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ACTIVE_ORDER_SEARCH_LIMIT:I = 0x3e8

.field private static final CLIENT_SUPPORT_BUILDER:Lcom/squareup/protos/client/orders/ClientSupport$Builder;

.field public static final Companion:Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore$Companion;

.field private static final SUPPORTED_ACTIONS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/Action$Type;",
            ">;"
        }
    .end annotation
.end field

.field private static final SUPPORTED_ORDER_FEATURES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/ClientSupport$Feature;",
            ">;"
        }
    .end annotation
.end field

.field private static final SUPPORTED_ORDER_FEATURES_WITH_ECOM_DELIVERY:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/ClientSupport$Feature;",
            ">;"
        }
    .end annotation
.end field

.field private static final SUPPORTED_ORDER_GROUPS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/OrderGroup;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final clientSupport:Lcom/squareup/ordermanagerdata/OrderManagerClientSupport;

.field private final currentTime:Lcom/squareup/time/CurrentTime;

.field private final orderHubService:Lcom/squareup/server/orderhub/OrderHubService;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    new-instance v0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->Companion:Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore$Companion;

    const/4 v0, 0x7

    new-array v1, v0, [Lcom/squareup/protos/client/orders/Action$Type;

    .line 413
    sget-object v2, Lcom/squareup/protos/client/orders/Action$Type;->ACCEPT:Lcom/squareup/protos/client/orders/Action$Type;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/protos/client/orders/Action$Type;->MARK_IN_PROGRESS:Lcom/squareup/protos/client/orders/Action$Type;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    sget-object v2, Lcom/squareup/protos/client/orders/Action$Type;->MARK_PICKED_UP:Lcom/squareup/protos/client/orders/Action$Type;

    const/4 v5, 0x2

    aput-object v2, v1, v5

    sget-object v2, Lcom/squareup/protos/client/orders/Action$Type;->MARK_READY:Lcom/squareup/protos/client/orders/Action$Type;

    const/4 v6, 0x3

    aput-object v2, v1, v6

    sget-object v2, Lcom/squareup/protos/client/orders/Action$Type;->MARK_SHIPPED:Lcom/squareup/protos/client/orders/Action$Type;

    const/4 v7, 0x4

    aput-object v2, v1, v7

    sget-object v2, Lcom/squareup/protos/client/orders/Action$Type;->COMPLETE:Lcom/squareup/protos/client/orders/Action$Type;

    const/4 v8, 0x5

    aput-object v2, v1, v8

    sget-object v2, Lcom/squareup/protos/client/orders/Action$Type;->CANCEL:Lcom/squareup/protos/client/orders/Action$Type;

    const/4 v9, 0x6

    aput-object v2, v1, v9

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    sput-object v1, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->SUPPORTED_ACTIONS:Ljava/util/List;

    new-array v1, v0, [Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    .line 417
    sget-object v2, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->CAVIAR_PICKUP:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    aput-object v2, v1, v3

    .line 418
    sget-object v2, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->CAVIAR_MANAGED_DELIVERY:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    aput-object v2, v1, v4

    .line 419
    sget-object v2, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->SQUARE_PICKUP:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    aput-object v2, v1, v5

    .line 420
    sget-object v2, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->WEEBLY_DIGITAL:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    aput-object v2, v1, v6

    .line 421
    sget-object v2, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->WEEBLY_PICKUP:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    aput-object v2, v1, v7

    .line 422
    sget-object v2, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->WEEBLY_SHIPMENT:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    aput-object v2, v1, v8

    .line 423
    sget-object v2, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->FRACTIONAL_QUANTITIES:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    aput-object v2, v1, v9

    .line 416
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    sput-object v1, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->SUPPORTED_ORDER_FEATURES:Ljava/util/List;

    const/16 v1, 0x8

    new-array v1, v1, [Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    .line 428
    sget-object v2, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->CAVIAR_PICKUP:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    aput-object v2, v1, v3

    .line 429
    sget-object v2, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->CAVIAR_MANAGED_DELIVERY:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    aput-object v2, v1, v4

    .line 430
    sget-object v2, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->SQUARE_PICKUP:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    aput-object v2, v1, v5

    .line 431
    sget-object v2, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->WEEBLY_DELIVERY:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    aput-object v2, v1, v6

    .line 432
    sget-object v2, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->WEEBLY_DIGITAL:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    aput-object v2, v1, v7

    .line 433
    sget-object v2, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->WEEBLY_PICKUP:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    aput-object v2, v1, v8

    .line 434
    sget-object v2, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->WEEBLY_SHIPMENT:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    aput-object v2, v1, v9

    .line 435
    sget-object v2, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->FRACTIONAL_QUANTITIES:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    aput-object v2, v1, v0

    .line 427
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->SUPPORTED_ORDER_FEATURES_WITH_ECOM_DELIVERY:Ljava/util/List;

    new-array v0, v6, [Lcom/squareup/protos/client/orders/OrderGroup;

    .line 438
    sget-object v1, Lcom/squareup/protos/client/orders/OrderGroup;->ACTIVE:Lcom/squareup/protos/client/orders/OrderGroup;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/orders/OrderGroup;->COMPLETED:Lcom/squareup/protos/client/orders/OrderGroup;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/orders/OrderGroup;->UPCOMING:Lcom/squareup/protos/client/orders/OrderGroup;

    aput-object v1, v0, v5

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->SUPPORTED_ORDER_GROUPS:Ljava/util/List;

    .line 440
    new-instance v0, Lcom/squareup/protos/client/orders/ClientSupport$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/ClientSupport$Builder;-><init>()V

    .line 441
    sget-object v1, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->SUPPORTED_ORDER_GROUPS:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/ClientSupport$Builder;->supported_groups(Ljava/util/List;)Lcom/squareup/protos/client/orders/ClientSupport$Builder;

    move-result-object v0

    sput-object v0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->CLIENT_SUPPORT_BUILDER:Lcom/squareup/protos/client/orders/ClientSupport$Builder;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/time/CurrentTime;Lcom/squareup/server/orderhub/OrderHubService;Lcom/squareup/ordermanagerdata/OrderManagerClientSupport;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentTime"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderHubService"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clientSupport"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountStatusSettings"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->currentTime:Lcom/squareup/time/CurrentTime;

    iput-object p3, p0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->orderHubService:Lcom/squareup/server/orderhub/OrderHubService;

    iput-object p4, p0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->clientSupport:Lcom/squareup/ordermanagerdata/OrderManagerClientSupport;

    iput-object p5, p0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method

.method public static final synthetic access$getSUPPORTED_ACTIONS$cp()Ljava/util/List;
    .locals 1

    .line 74
    sget-object v0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->SUPPORTED_ACTIONS:Ljava/util/List;

    return-object v0
.end method

.method public static final synthetic access$getSUPPORTED_ORDER_FEATURES$cp()Ljava/util/List;
    .locals 1

    .line 74
    sget-object v0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->SUPPORTED_ORDER_FEATURES:Ljava/util/List;

    return-object v0
.end method

.method public static final synthetic access$getSUPPORTED_ORDER_FEATURES_WITH_ECOM_DELIVERY$cp()Ljava/util/List;
    .locals 1

    .line 74
    sget-object v0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->SUPPORTED_ORDER_FEATURES_WITH_ECOM_DELIVERY:Ljava/util/List;

    return-object v0
.end method

.method private final clientSupport()Lcom/squareup/protos/client/orders/ClientSupport;
    .locals 2

    .line 368
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getOrderHubSettings()Lcom/squareup/settings/server/OrderHubSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/OrderHubSettings;->getCanSupportEcomDeliveryOrders()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 369
    sget-object v0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->SUPPORTED_ORDER_FEATURES_WITH_ECOM_DELIVERY:Ljava/util/List;

    goto :goto_0

    .line 371
    :cond_0
    sget-object v0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->SUPPORTED_ORDER_FEATURES:Ljava/util/List;

    .line 374
    :goto_0
    sget-object v1, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->CLIENT_SUPPORT_BUILDER:Lcom/squareup/protos/client/orders/ClientSupport$Builder;

    .line 375
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/orders/ClientSupport$Builder;->supported_features(Ljava/util/List;)Lcom/squareup/protos/client/orders/ClientSupport$Builder;

    move-result-object v0

    .line 376
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->clientSupport:Lcom/squareup/ordermanagerdata/OrderManagerClientSupport;

    invoke-interface {v1}, Lcom/squareup/ordermanagerdata/OrderManagerClientSupport;->getSupportedActions()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/ClientSupport$Builder;->supported_actions(Ljava/util/List;)Lcom/squareup/protos/client/orders/ClientSupport$Builder;

    move-result-object v0

    .line 377
    invoke-virtual {v0}, Lcom/squareup/protos/client/orders/ClientSupport$Builder;->build()Lcom/squareup/protos/client/orders/ClientSupport;

    move-result-object v0

    const-string v1, "CLIENT_SUPPORT_BUILDER\n \u2026Actions)\n        .build()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final currentDateTime()Ljava/lang/String;
    .locals 2

    .line 308
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-interface {v0}, Lcom/squareup/time/CurrentTime;->zonedDateTime()Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    .line 309
    invoke-virtual {v0}, Lorg/threeten/bp/ZonedDateTime;->toOffsetDateTime()Lorg/threeten/bp/OffsetDateTime;

    move-result-object v0

    .line 310
    invoke-virtual {v0}, Lorg/threeten/bp/OffsetDateTime;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "currentTime.zonedDateTim\u2026ime()\n        .toString()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final getBazaarLocationIds(Lcom/squareup/settings/server/AccountStatusSettings;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 358
    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getOrderHubSettings()Lcom/squareup/settings/server/OrderHubSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/OrderHubSettings;->getAllowBazaarOrders()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getOrderHubSettings()Lcom/squareup/settings/server/OrderHubSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/settings/server/OrderHubSettings;->getHasBazaarOnlineStore()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 361
    iget-object p1, p0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object p1

    const-string v0, "accountStatusSettings.userSettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOfNotNull(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    goto :goto_0

    .line 363
    :cond_0
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final orderSearchTimeRange(JLcom/squareup/protos/client/orders/OrderGroup;)Lcom/squareup/orders/SearchOrdersDateTimeFilter;
    .locals 1

    .line 390
    new-instance v0, Lcom/squareup/protos/connect/v2/common/TimeRange$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/common/TimeRange$Builder;-><init>()V

    .line 391
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->dateTimeFromMillisInPast(J)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/common/TimeRange$Builder;->start_at(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/common/TimeRange$Builder;

    move-result-object p1

    .line 392
    invoke-direct {p0}, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->currentDateTime()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/connect/v2/common/TimeRange$Builder;->end_at(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/common/TimeRange$Builder;

    move-result-object p1

    .line 393
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/common/TimeRange$Builder;->build()Lcom/squareup/protos/connect/v2/common/TimeRange;

    move-result-object p1

    .line 394
    sget-object p2, Lcom/squareup/protos/client/orders/OrderGroup;->COMPLETED:Lcom/squareup/protos/client/orders/OrderGroup;

    const-string v0, "SearchOrdersDateTimeFilt\u2026Range)\n          .build()"

    if-ne p3, p2, :cond_0

    .line 395
    new-instance p2, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;

    invoke-direct {p2}, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;-><init>()V

    .line 396
    invoke-virtual {p2, p1}, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;->closed_at(Lcom/squareup/protos/connect/v2/common/TimeRange;)Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;

    move-result-object p1

    .line 397
    invoke-virtual {p1}, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;->build()Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    move-result-object p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 399
    :cond_0
    new-instance p2, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;

    invoke-direct {p2}, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;-><init>()V

    .line 400
    invoke-virtual {p2, p1}, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;->created_at(Lcom/squareup/protos/connect/v2/common/TimeRange;)Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;

    move-result-object p1

    .line 401
    invoke-virtual {p1}, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;->build()Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    move-result-object p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method private final performFulfillmentAction(Lcom/squareup/protos/client/orders/FulfillmentAction;Ljava/lang/String;I)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/orders/FulfillmentAction;",
            "Ljava/lang/String;",
            "I)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/orders/PerformFulfillmentActionResponse;",
            ">;>;"
        }
    .end annotation

    .line 329
    new-instance v0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;-><init>()V

    .line 330
    invoke-direct {p0}, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->clientSupport()Lcom/squareup/protos/client/orders/ClientSupport;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;->client_support(Lcom/squareup/protos/client/orders/ClientSupport;)Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;

    move-result-object v0

    .line 331
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;->action(Lcom/squareup/protos/client/orders/FulfillmentAction;)Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;

    move-result-object p1

    .line 332
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;->order_id(Ljava/lang/String;)Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;

    move-result-object p1

    .line 333
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;->order_version(Ljava/lang/Integer;)Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;

    move-result-object p1

    .line 334
    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;->build()Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;

    move-result-object p1

    .line 336
    iget-object p2, p0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->orderHubService:Lcom/squareup/server/orderhub/OrderHubService;

    const-string p3, "request"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/server/orderhub/OrderHubService;->performFulfillmentAction(Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 337
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public activeOrders()Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/orders/SearchOrdersResponse;",
            ">;>;"
        }
    .end annotation

    .line 83
    new-instance v0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;-><init>()V

    .line 84
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    const-string v2, "accountStatusSettings.userSettings"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->location_ids(Ljava/util/List;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    move-result-object v0

    .line 85
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-direct {p0, v1}, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->getBazaarLocationIds(Lcom/squareup/settings/server/AccountStatusSettings;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->bazaar_location_ids(Ljava/util/List;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    move-result-object v0

    .line 86
    invoke-direct {p0}, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->clientSupport()Lcom/squareup/protos/client/orders/ClientSupport;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->client_support(Lcom/squareup/protos/client/orders/ClientSupport;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    move-result-object v0

    .line 87
    sget-object v1, Lcom/squareup/protos/client/orders/OrderGroup;->ACTIVE:Lcom/squareup/protos/client/orders/OrderGroup;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->order_groups(Ljava/util/List;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    move-result-object v0

    .line 89
    new-instance v1, Lcom/squareup/orders/SearchOrdersSort$Builder;

    invoke-direct {v1}, Lcom/squareup/orders/SearchOrdersSort$Builder;-><init>()V

    .line 90
    sget-object v2, Lcom/squareup/orders/SearchOrdersSort$Field;->CREATED_AT:Lcom/squareup/orders/SearchOrdersSort$Field;

    invoke-virtual {v1, v2}, Lcom/squareup/orders/SearchOrdersSort$Builder;->sort_field(Lcom/squareup/orders/SearchOrdersSort$Field;)Lcom/squareup/orders/SearchOrdersSort$Builder;

    move-result-object v1

    .line 91
    invoke-virtual {v1}, Lcom/squareup/orders/SearchOrdersSort$Builder;->build()Lcom/squareup/orders/SearchOrdersSort;

    move-result-object v1

    .line 88
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->sort(Lcom/squareup/orders/SearchOrdersSort;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    move-result-object v0

    const/16 v1, 0x3e8

    .line 93
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->limit(Ljava/lang/Integer;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    move-result-object v0

    .line 94
    invoke-virtual {v0}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->build()Lcom/squareup/protos/client/orders/SearchOrdersRequest;

    move-result-object v0

    .line 96
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->orderHubService:Lcom/squareup/server/orderhub/OrderHubService;

    const-string v2, "request"

    .line 97
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Lcom/squareup/server/orderhub/OrderHubService;->getOrders(Lcom/squareup/protos/client/orders/SearchOrdersRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object v0

    .line 98
    invoke-virtual {v0}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public cancelOrder(Lcom/squareup/orders/model/Order;Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/List;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            "Lcom/squareup/ordermanagerdata/CancellationReason;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/LineItemQuantity;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/orders/PerformFulfillmentActionResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cancelReason"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lineItemQuantities"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 252
    new-instance v0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;-><init>()V

    .line 254
    new-instance v1, Lcom/squareup/protos/client/orders/CancelAction$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/orders/CancelAction$Builder;-><init>()V

    .line 255
    iget-object v2, p0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->res:Lcom/squareup/util/Res;

    invoke-static {p2, v2}, Lcom/squareup/ordermanagerdata/proto/CancellationReasonsKt;->getReasonName(Lcom/squareup/ordermanagerdata/CancellationReason;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, p2}, Lcom/squareup/protos/client/orders/CancelAction$Builder;->cancel_reason(Ljava/lang/String;)Lcom/squareup/protos/client/orders/CancelAction$Builder;

    move-result-object p2

    .line 256
    invoke-direct {p0}, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->currentDateTime()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/squareup/protos/client/orders/CancelAction$Builder;->canceled_at(Ljava/lang/String;)Lcom/squareup/protos/client/orders/CancelAction$Builder;

    move-result-object p2

    .line 257
    invoke-virtual {p2}, Lcom/squareup/protos/client/orders/CancelAction$Builder;->build()Lcom/squareup/protos/client/orders/CancelAction;

    move-result-object p2

    .line 253
    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->cancel(Lcom/squareup/protos/client/orders/CancelAction;)Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    move-result-object p2

    .line 259
    invoke-virtual {p2, p3}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->line_items(Ljava/util/List;)Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    move-result-object p2

    .line 260
    invoke-static {p1}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object p3

    iget-object p3, p3, Lcom/squareup/orders/model/Order$Fulfillment;->uid:Ljava/lang/String;

    if-eqz p3, :cond_0

    goto :goto_0

    :cond_0
    const-string p3, ""

    :goto_0
    invoke-virtual {p2, p3}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->fulfillment_id(Ljava/lang/String;)Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    move-result-object p2

    .line 261
    invoke-virtual {p2}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->build()Lcom/squareup/protos/client/orders/FulfillmentAction;

    move-result-object p2

    const-string p3, "fulfillmentAction"

    .line 263
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p3, p1, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    const-string v0, "order.id"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p1, Lcom/squareup/orders/model/Order;->version:Ljava/lang/Integer;

    const-string v0, "order.version"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-direct {p0, p2, p3, p1}, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->performFulfillmentAction(Lcom/squareup/protos/client/orders/FulfillmentAction;Ljava/lang/String;I)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public completedOrders(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/orders/SearchOrdersResponse;",
            ">;>;"
        }
    .end annotation

    .line 104
    new-instance v0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;-><init>()V

    .line 105
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    const-string v2, "accountStatusSettings.userSettings"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->location_ids(Ljava/util/List;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    move-result-object v0

    .line 106
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-direct {p0, v1}, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->getBazaarLocationIds(Lcom/squareup/settings/server/AccountStatusSettings;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->bazaar_location_ids(Ljava/util/List;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    move-result-object v0

    .line 107
    invoke-direct {p0}, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->clientSupport()Lcom/squareup/protos/client/orders/ClientSupport;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->client_support(Lcom/squareup/protos/client/orders/ClientSupport;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    move-result-object v0

    .line 108
    sget-object v1, Lcom/squareup/protos/client/orders/OrderGroup;->COMPLETED:Lcom/squareup/protos/client/orders/OrderGroup;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->order_groups(Ljava/util/List;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    move-result-object v0

    .line 109
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->cursor(Ljava/lang/String;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    move-result-object p1

    .line 111
    new-instance v0, Lcom/squareup/orders/SearchOrdersSort$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/SearchOrdersSort$Builder;-><init>()V

    .line 112
    sget-object v1, Lcom/squareup/orders/SearchOrdersSort$Field;->CLOSED_AT:Lcom/squareup/orders/SearchOrdersSort$Field;

    invoke-virtual {v0, v1}, Lcom/squareup/orders/SearchOrdersSort$Builder;->sort_field(Lcom/squareup/orders/SearchOrdersSort$Field;)Lcom/squareup/orders/SearchOrdersSort$Builder;

    move-result-object v0

    .line 113
    invoke-virtual {v0}, Lcom/squareup/orders/SearchOrdersSort$Builder;->build()Lcom/squareup/orders/SearchOrdersSort;

    move-result-object v0

    .line 110
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->sort(Lcom/squareup/orders/SearchOrdersSort;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    move-result-object p1

    .line 115
    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->build()Lcom/squareup/protos/client/orders/SearchOrdersRequest;

    move-result-object p1

    .line 117
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->orderHubService:Lcom/squareup/server/orderhub/OrderHubService;

    const-string v1, "request"

    .line 118
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/server/orderhub/OrderHubService;->getOrders(Lcom/squareup/protos/client/orders/SearchOrdersRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 119
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public final dateTimeFromMillisInPast(J)Ljava/lang/String;
    .locals 2

    .line 318
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-interface {v0}, Lcom/squareup/time/CurrentTime;->zonedDateTime()Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    .line 319
    sget-object v1, Lorg/threeten/bp/temporal/ChronoUnit;->MILLIS:Lorg/threeten/bp/temporal/ChronoUnit;

    check-cast v1, Lorg/threeten/bp/temporal/TemporalUnit;

    invoke-virtual {v0, p1, p2, v1}, Lorg/threeten/bp/ZonedDateTime;->minus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object p1

    .line 320
    invoke-virtual {p1}, Lorg/threeten/bp/ZonedDateTime;->toOffsetDateTime()Lorg/threeten/bp/OffsetDateTime;

    move-result-object p1

    .line 321
    invoke-virtual {p1}, Lorg/threeten/bp/OffsetDateTime;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "currentTime.zonedDateTim\u2026ime()\n        .toString()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public editTracking(Lcom/squareup/orders/model/Order;Lcom/squareup/orders/model/Order$Fulfillment;Lcom/squareup/ordermanagerdata/TrackingInfo;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            "Lcom/squareup/orders/model/Order$Fulfillment;",
            "Lcom/squareup/ordermanagerdata/TrackingInfo;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/orders/UpdateOrderResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fulfillment"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p3, :cond_0

    .line 162
    new-instance v0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;-><init>()V

    .line 163
    iget-object v1, p2, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->type(Lcom/squareup/orders/model/Order$FulfillmentType;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;

    move-result-object v0

    const-string v1, "Fulfillment.Builder()\n  \u2026  .type(fulfillment.type)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    invoke-static {v0, p3}, Lcom/squareup/ordermanagerdata/proto/FulfillmentBuildersKt;->trackingInfo(Lcom/squareup/orders/model/Order$Fulfillment$Builder;Lcom/squareup/ordermanagerdata/TrackingInfo;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;

    move-result-object p3

    const/4 v0, 0x0

    .line 165
    invoke-virtual {p3, v0}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->type(Lcom/squareup/orders/model/Order$FulfillmentType;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;

    move-result-object p3

    .line 166
    iget-object p2, p2, Lcom/squareup/orders/model/Order$Fulfillment;->uid:Ljava/lang/String;

    invoke-virtual {p3, p2}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;

    move-result-object p2

    .line 167
    invoke-virtual {p2}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->build()Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object p2

    .line 169
    new-instance p3, Lcom/squareup/orders/model/Order$Builder;

    invoke-direct {p3}, Lcom/squareup/orders/model/Order$Builder;-><init>()V

    .line 170
    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    invoke-virtual {p3, p2}, Lcom/squareup/orders/model/Order$Builder;->fulfillments(Ljava/util/List;)Lcom/squareup/orders/model/Order$Builder;

    move-result-object p2

    .line 171
    iget-object p3, p1, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    invoke-virtual {p2, p3}, Lcom/squareup/orders/model/Order$Builder;->id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Builder;

    move-result-object p2

    .line 172
    iget-object p1, p1, Lcom/squareup/orders/model/Order;->version:Ljava/lang/Integer;

    invoke-virtual {p2, p1}, Lcom/squareup/orders/model/Order$Builder;->version(Ljava/lang/Integer;)Lcom/squareup/orders/model/Order$Builder;

    move-result-object p1

    .line 173
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$Builder;->build()Lcom/squareup/orders/model/Order;

    move-result-object p1

    .line 175
    new-instance p2, Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;-><init>()V

    .line 176
    invoke-direct {p0}, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->clientSupport()Lcom/squareup/protos/client/orders/ClientSupport;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;->client_support(Lcom/squareup/protos/client/orders/ClientSupport;)Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;

    move-result-object p2

    .line 177
    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;->order(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;

    move-result-object p1

    .line 178
    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;->build()Lcom/squareup/protos/client/orders/UpdateOrderRequest;

    move-result-object p1

    goto :goto_0

    .line 180
    :cond_0
    new-instance p3, Lcom/squareup/orders/model/Order$Builder;

    invoke-direct {p3}, Lcom/squareup/orders/model/Order$Builder;-><init>()V

    .line 181
    iget-object v0, p1, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    invoke-virtual {p3, v0}, Lcom/squareup/orders/model/Order$Builder;->id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Builder;

    move-result-object p3

    .line 182
    iget-object p1, p1, Lcom/squareup/orders/model/Order;->version:Ljava/lang/Integer;

    invoke-virtual {p3, p1}, Lcom/squareup/orders/model/Order$Builder;->version(Ljava/lang/Integer;)Lcom/squareup/orders/model/Order$Builder;

    move-result-object p1

    .line 183
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$Builder;->build()Lcom/squareup/orders/model/Order;

    move-result-object p1

    .line 185
    new-instance p3, Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;

    invoke-direct {p3}, Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;-><init>()V

    .line 186
    invoke-direct {p0}, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->clientSupport()Lcom/squareup/protos/client/orders/ClientSupport;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;->client_support(Lcom/squareup/protos/client/orders/ClientSupport;)Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;

    move-result-object p3

    .line 187
    invoke-virtual {p3, p1}, Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;->order(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;

    move-result-object p1

    const/4 p3, 0x2

    new-array p3, p3, [Ljava/lang/String;

    const/4 v0, 0x0

    .line 190
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "fulfillments["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p2, Lcom/squareup/orders/model/Order$Fulfillment;->uid:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "].shipment_details.carrier"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p3, v0

    const/4 v0, 0x1

    .line 191
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p2, Lcom/squareup/orders/model/Order$Fulfillment;->uid:Ljava/lang/String;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "].shipment_details.tracking_number"

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    aput-object p2, p3, v0

    .line 189
    invoke-static {p3}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    .line 188
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;->fields_to_clear(Ljava/util/List;)Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;

    move-result-object p1

    .line 194
    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/UpdateOrderRequest$Builder;->build()Lcom/squareup/protos/client/orders/UpdateOrderRequest;

    move-result-object p1

    .line 196
    :goto_0
    iget-object p2, p0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->orderHubService:Lcom/squareup/server/orderhub/OrderHubService;

    const-string p3, "request"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/server/orderhub/OrderHubService;->updateOrder(Lcom/squareup/protos/client/orders/UpdateOrderRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 197
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public getOrder(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/orders/GetOrderResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "orderId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 343
    new-instance v0, Lcom/squareup/protos/client/orders/GetOrderRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/GetOrderRequest$Builder;-><init>()V

    .line 344
    invoke-direct {p0}, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->clientSupport()Lcom/squareup/protos/client/orders/ClientSupport;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/GetOrderRequest$Builder;->client_support(Lcom/squareup/protos/client/orders/ClientSupport;)Lcom/squareup/protos/client/orders/GetOrderRequest$Builder;

    move-result-object v0

    .line 345
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/orders/GetOrderRequest$Builder;->order_id(Ljava/lang/String;)Lcom/squareup/protos/client/orders/GetOrderRequest$Builder;

    move-result-object p1

    .line 346
    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/GetOrderRequest$Builder;->build()Lcom/squareup/protos/client/orders/GetOrderRequest;

    move-result-object p1

    .line 348
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->orderHubService:Lcom/squareup/server/orderhub/OrderHubService;

    const-string v1, "request"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/server/orderhub/OrderHubService;->getOrder(Lcom/squareup/protos/client/orders/GetOrderRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 349
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public markOrderInProgress(Lcom/squareup/orders/model/Order;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/orders/PerformFulfillmentActionResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 290
    new-instance v0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;-><init>()V

    .line 291
    invoke-static {p1}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/orders/model/Order$Fulfillment;->uid:Ljava/lang/String;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, ""

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->fulfillment_id(Ljava/lang/String;)Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    move-result-object v0

    .line 293
    new-instance v1, Lcom/squareup/protos/client/orders/MarkInProgressAction$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/orders/MarkInProgressAction$Builder;-><init>()V

    .line 294
    invoke-direct {p0}, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->currentDateTime()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/orders/MarkInProgressAction$Builder;->in_progress_at(Ljava/lang/String;)Lcom/squareup/protos/client/orders/MarkInProgressAction$Builder;

    move-result-object v1

    .line 295
    invoke-virtual {v1, p2}, Lcom/squareup/protos/client/orders/MarkInProgressAction$Builder;->pickup_at(Ljava/lang/String;)Lcom/squareup/protos/client/orders/MarkInProgressAction$Builder;

    move-result-object p2

    .line 296
    invoke-virtual {p2}, Lcom/squareup/protos/client/orders/MarkInProgressAction$Builder;->build()Lcom/squareup/protos/client/orders/MarkInProgressAction;

    move-result-object p2

    .line 292
    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_in_progress(Lcom/squareup/protos/client/orders/MarkInProgressAction;)Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    move-result-object p2

    .line 298
    invoke-virtual {p2}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->build()Lcom/squareup/protos/client/orders/FulfillmentAction;

    move-result-object p2

    const-string v0, "fulfillmentAction"

    .line 300
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p1, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    const-string v1, "order.id"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p1, Lcom/squareup/orders/model/Order;->version:Ljava/lang/Integer;

    const-string v1, "order.version"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-direct {p0, p2, v0, p1}, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->performFulfillmentAction(Lcom/squareup/protos/client/orders/FulfillmentAction;Ljava/lang/String;I)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public searchOrders(Lcom/squareup/ordermanagerdata/SearchQuery;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ordermanagerdata/SearchQuery;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/orders/SearchOrdersResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "query"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    new-instance v0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;-><init>()V

    .line 127
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    const-string v2, "accountStatusSettings.userSettings"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->location_ids(Ljava/util/List;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    move-result-object v0

    .line 128
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-direct {p0, v1}, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->getBazaarLocationIds(Lcom/squareup/settings/server/AccountStatusSettings;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->bazaar_location_ids(Ljava/util/List;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    move-result-object v0

    .line 129
    invoke-direct {p0}, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->clientSupport()Lcom/squareup/protos/client/orders/ClientSupport;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->client_support(Lcom/squareup/protos/client/orders/ClientSupport;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    move-result-object v0

    .line 130
    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/SearchQuery;->getOrderGroup()Lcom/squareup/protos/client/orders/OrderGroup;

    move-result-object v1

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->order_groups(Ljava/util/List;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    move-result-object v0

    .line 131
    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/SearchQuery;->getSearchTerm()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->search_term(Ljava/lang/String;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    move-result-object v0

    .line 132
    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/SearchQuery;->getSearchRangeMillis()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/SearchQuery;->getOrderGroup()Lcom/squareup/protos/client/orders/OrderGroup;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->orderSearchTimeRange(JLcom/squareup/protos/client/orders/OrderGroup;)Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->date_time_filter(Lcom/squareup/orders/SearchOrdersDateTimeFilter;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    move-result-object v0

    .line 133
    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->cursor(Ljava/lang/String;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    move-result-object p2

    .line 135
    new-instance v0, Lcom/squareup/orders/SearchOrdersSort$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/SearchOrdersSort$Builder;-><init>()V

    .line 136
    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/SearchQuery;->getOrderGroup()Lcom/squareup/protos/client/orders/OrderGroup;

    move-result-object v1

    sget-object v2, Lcom/squareup/protos/client/orders/OrderGroup;->COMPLETED:Lcom/squareup/protos/client/orders/OrderGroup;

    if-ne v1, v2, :cond_0

    sget-object v1, Lcom/squareup/orders/SearchOrdersSort$Field;->CLOSED_AT:Lcom/squareup/orders/SearchOrdersSort$Field;

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/orders/SearchOrdersSort$Field;->CREATED_AT:Lcom/squareup/orders/SearchOrdersSort$Field;

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/orders/SearchOrdersSort$Builder;->sort_field(Lcom/squareup/orders/SearchOrdersSort$Field;)Lcom/squareup/orders/SearchOrdersSort$Builder;

    move-result-object v0

    .line 137
    invoke-virtual {v0}, Lcom/squareup/orders/SearchOrdersSort$Builder;->build()Lcom/squareup/orders/SearchOrdersSort;

    move-result-object v0

    .line 134
    invoke-virtual {p2, v0}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->sort(Lcom/squareup/orders/SearchOrdersSort;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    move-result-object p2

    .line 140
    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/SearchQuery;->getFulfillment()Lcom/squareup/orders/model/Order$FulfillmentType;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 142
    new-instance v1, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;

    invoke-direct {v1}, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;-><init>()V

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;->fulfillment_types(Ljava/util/List;)Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;->build()Lcom/squareup/orders/SearchOrdersFulfillmentFilter;

    move-result-object v0

    .line 141
    invoke-virtual {p2, v0}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->fulfillments(Lcom/squareup/orders/SearchOrdersFulfillmentFilter;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    .line 145
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/SearchQuery;->getSource()Lcom/squareup/protos/client/orders/Channel;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 146
    iget-object p1, p1, Lcom/squareup/protos/client/orders/Channel;->display_name:Ljava/lang/String;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->order_source_name(Ljava/util/List;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    .line 149
    :cond_2
    iget-object p1, p0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->orderHubService:Lcom/squareup/server/orderhub/OrderHubService;

    .line 150
    invoke-virtual {p2}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->build()Lcom/squareup/protos/client/orders/SearchOrdersRequest;

    move-result-object p2

    const-string v0, "requestBuilder.build()"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, p2}, Lcom/squareup/server/orderhub/OrderHubService;->getOrders(Lcom/squareup/protos/client/orders/SearchOrdersRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 151
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public shipOrder(Lcom/squareup/orders/model/Order;Lcom/squareup/ordermanagerdata/TrackingInfo;Ljava/util/List;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            "Lcom/squareup/ordermanagerdata/TrackingInfo;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/LineItemQuantity;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/orders/PerformFulfillmentActionResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lineItemQuantities"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 271
    new-instance v0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;-><init>()V

    .line 272
    invoke-static {p1}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/orders/model/Order$Fulfillment;->uid:Ljava/lang/String;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, ""

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->fulfillment_id(Ljava/lang/String;)Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    move-result-object v0

    .line 273
    invoke-virtual {v0, p3}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->line_items(Ljava/util/List;)Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    move-result-object p3

    .line 275
    new-instance v0, Lcom/squareup/protos/client/orders/MarkShippedAction$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/MarkShippedAction$Builder;-><init>()V

    .line 276
    invoke-direct {p0}, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->currentDateTime()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/MarkShippedAction$Builder;->shipped_at(Ljava/lang/String;)Lcom/squareup/protos/client/orders/MarkShippedAction$Builder;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz p2, :cond_1

    .line 277
    invoke-virtual {p2}, Lcom/squareup/ordermanagerdata/TrackingInfo;->getCarrierName()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    move-object v2, v1

    :goto_1
    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/orders/MarkShippedAction$Builder;->carrier(Ljava/lang/String;)Lcom/squareup/protos/client/orders/MarkShippedAction$Builder;

    move-result-object v0

    if-eqz p2, :cond_2

    .line 278
    invoke-virtual {p2}, Lcom/squareup/ordermanagerdata/TrackingInfo;->getTrackingNumber()Ljava/lang/String;

    move-result-object v1

    :cond_2
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/MarkShippedAction$Builder;->tracking_number(Ljava/lang/String;)Lcom/squareup/protos/client/orders/MarkShippedAction$Builder;

    move-result-object p2

    .line 279
    invoke-virtual {p2}, Lcom/squareup/protos/client/orders/MarkShippedAction$Builder;->build()Lcom/squareup/protos/client/orders/MarkShippedAction;

    move-result-object p2

    .line 274
    invoke-virtual {p3, p2}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_shipped(Lcom/squareup/protos/client/orders/MarkShippedAction;)Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    move-result-object p2

    .line 281
    invoke-virtual {p2}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->build()Lcom/squareup/protos/client/orders/FulfillmentAction;

    move-result-object p2

    const-string p3, "fulfillmentAction"

    .line 283
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p3, p1, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    const-string v0, "order.id"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p1, Lcom/squareup/orders/model/Order;->version:Ljava/lang/Integer;

    const-string v0, "order.version"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-direct {p0, p2, p3, p1}, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->performFulfillmentAction(Lcom/squareup/protos/client/orders/FulfillmentAction;Ljava/lang/String;I)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public transitionOrder(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action$Type;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            "Lcom/squareup/protos/client/orders/Action$Type;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/orders/PerformFulfillmentActionResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "action"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 204
    invoke-direct {p0}, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->currentDateTime()Ljava/lang/String;

    move-result-object v0

    .line 206
    invoke-static {p1}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/orders/model/Order$Fulfillment;->uid:Ljava/lang/String;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, ""

    .line 207
    :goto_0
    sget-object v2, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p2}, Lcom/squareup/protos/client/orders/Action$Type;->ordinal()I

    move-result p2

    aget p2, v2, p2

    packed-switch p2, :pswitch_data_0

    .line 237
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Action did not match a known action type"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 236
    :pswitch_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Order must be canceled with the cancelOrder function"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 235
    :pswitch_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Order must be shipped with the shipOrder function"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 232
    :pswitch_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Order must be marked in progress with the markOrderInProgress function."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 226
    :pswitch_4
    new-instance p2, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;-><init>()V

    .line 228
    new-instance v2, Lcom/squareup/protos/client/orders/MarkPickedUpAction$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/orders/MarkPickedUpAction$Builder;-><init>()V

    .line 229
    invoke-virtual {v2, v0}, Lcom/squareup/protos/client/orders/MarkPickedUpAction$Builder;->picked_up_at(Ljava/lang/String;)Lcom/squareup/protos/client/orders/MarkPickedUpAction$Builder;

    move-result-object v0

    .line 230
    invoke-virtual {v0}, Lcom/squareup/protos/client/orders/MarkPickedUpAction$Builder;->build()Lcom/squareup/protos/client/orders/MarkPickedUpAction;

    move-result-object v0

    .line 227
    invoke-virtual {p2, v0}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_picked_up(Lcom/squareup/protos/client/orders/MarkPickedUpAction;)Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    move-result-object p2

    goto :goto_1

    .line 220
    :pswitch_5
    new-instance p2, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;-><init>()V

    .line 222
    new-instance v2, Lcom/squareup/protos/client/orders/MarkReadyAction$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/orders/MarkReadyAction$Builder;-><init>()V

    .line 223
    invoke-virtual {v2, v0}, Lcom/squareup/protos/client/orders/MarkReadyAction$Builder;->ready_at(Ljava/lang/String;)Lcom/squareup/protos/client/orders/MarkReadyAction$Builder;

    move-result-object v0

    .line 224
    invoke-virtual {v0}, Lcom/squareup/protos/client/orders/MarkReadyAction$Builder;->build()Lcom/squareup/protos/client/orders/MarkReadyAction;

    move-result-object v0

    .line 221
    invoke-virtual {p2, v0}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_ready(Lcom/squareup/protos/client/orders/MarkReadyAction;)Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    move-result-object p2

    goto :goto_1

    .line 214
    :pswitch_6
    new-instance p2, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;-><init>()V

    .line 216
    new-instance v2, Lcom/squareup/protos/client/orders/CompleteAction$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/orders/CompleteAction$Builder;-><init>()V

    .line 217
    invoke-virtual {v2, v0}, Lcom/squareup/protos/client/orders/CompleteAction$Builder;->completed_at(Ljava/lang/String;)Lcom/squareup/protos/client/orders/CompleteAction$Builder;

    move-result-object v0

    .line 218
    invoke-virtual {v0}, Lcom/squareup/protos/client/orders/CompleteAction$Builder;->build()Lcom/squareup/protos/client/orders/CompleteAction;

    move-result-object v0

    .line 215
    invoke-virtual {p2, v0}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->complete(Lcom/squareup/protos/client/orders/CompleteAction;)Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    move-result-object p2

    goto :goto_1

    .line 208
    :pswitch_7
    new-instance p2, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;-><init>()V

    .line 210
    new-instance v2, Lcom/squareup/protos/client/orders/AcceptAction$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/orders/AcceptAction$Builder;-><init>()V

    .line 211
    invoke-virtual {v2, v0}, Lcom/squareup/protos/client/orders/AcceptAction$Builder;->accepted_at(Ljava/lang/String;)Lcom/squareup/protos/client/orders/AcceptAction$Builder;

    move-result-object v0

    .line 212
    invoke-virtual {v0}, Lcom/squareup/protos/client/orders/AcceptAction$Builder;->build()Lcom/squareup/protos/client/orders/AcceptAction;

    move-result-object v0

    .line 209
    invoke-virtual {p2, v0}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->accept(Lcom/squareup/protos/client/orders/AcceptAction;)Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    move-result-object p2

    .line 241
    :goto_1
    invoke-virtual {p2, v1}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->fulfillment_id(Ljava/lang/String;)Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    move-result-object p2

    .line 242
    invoke-virtual {p2}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->build()Lcom/squareup/protos/client/orders/FulfillmentAction;

    move-result-object p2

    const-string v0, "fulfillmentAction"

    .line 244
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p1, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    const-string v1, "order.id"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p1, Lcom/squareup/orders/model/Order;->version:Ljava/lang/Integer;

    const-string v1, "order.version"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-direct {p0, p2, v0, p1}, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->performFulfillmentAction(Lcom/squareup/protos/client/orders/FulfillmentAction;Ljava/lang/String;I)Lio/reactivex/Single;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
