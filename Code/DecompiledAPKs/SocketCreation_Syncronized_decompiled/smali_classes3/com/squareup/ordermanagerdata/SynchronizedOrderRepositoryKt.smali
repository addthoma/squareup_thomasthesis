.class public final Lcom/squareup/ordermanagerdata/SynchronizedOrderRepositoryKt;
.super Ljava/lang/Object;
.source "SynchronizedOrderRepository.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u001a1\u0010\u0004\u001a\u00020\u0005\"\u0004\u0008\u0000\u0010\u00062\u0008\u0010\u0007\u001a\u0004\u0018\u0001H\u00062\u0012\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u00020\u00020\tH\u0002\u00a2\u0006\u0002\u0010\n\u001a8\u0010\u000b\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00060\r0\u000c\"\u0004\u0008\u0000\u0010\u0006*\u0008\u0012\u0004\u0012\u0002H\u00060\u000c2\u0012\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00050\tH\u0002\u001a8\u0010\u000b\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00060\r0\u0010\"\u0004\u0008\u0000\u0010\u0006*\u0008\u0012\u0004\u0012\u0002H\u00060\u00102\u0012\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00050\tH\u0002\u001aL\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u0002H\u00120\r\"\u0004\u0008\u0000\u0010\u0006\"\u0004\u0008\u0001\u0010\u0012*\u0008\u0012\u0004\u0012\u0002H\u00060\u00132\u0012\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u00120\t2\u0012\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u00020\u00020\tH\u0002\u001a\u000c\u0010\u0015\u001a\u00020\u0005*\u00020\u0002H\u0002\u001a,\u0010\u0015\u001a\u00020\u0005\"\u0004\u0008\u0000\u0010\u0006*\u0008\u0012\u0004\u0012\u0002H\u00060\u00162\u0012\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u00020\u00020\tH\u0002\u001a,\u0010\u0017\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00180\r0\u0010*\u00020\u00192\u0012\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00050\tH\u0002\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0000\u0010\u0003\u00a8\u0006\u001a"
    }
    d2 = {
        "isVersionMismatch",
        "",
        "Lcom/squareup/protos/client/Status;",
        "(Lcom/squareup/protos/client/Status;)Z",
        "getFailureFromResponse",
        "Lcom/squareup/ordermanagerdata/ResultState$Failure;",
        "T",
        "response",
        "statusSource",
        "Lkotlin/Function1;",
        "(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ordermanagerdata/ResultState$Failure;",
        "mapToResultState",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/ordermanagerdata/ResultState;",
        "errorMapper",
        "",
        "Lio/reactivex/Single;",
        "toResultState",
        "R",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "successParser",
        "toResultStateFailure",
        "Lcom/squareup/receiving/ReceivedResponse;",
        "toResultStateSingle",
        "",
        "Lio/reactivex/Completable;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$mapToResultState(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepositoryKt;->mapToResultState(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toResultState(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ordermanagerdata/ResultState;
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepositoryKt;->toResultState(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ordermanagerdata/ResultState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toResultStateSingle(Lio/reactivex/Completable;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepositoryKt;->toResultStateSingle(Lio/reactivex/Completable;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method private static final getFailureFromResponse(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ordermanagerdata/ResultState$Failure;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Lcom/squareup/protos/client/Status;",
            ">;)",
            "Lcom/squareup/ordermanagerdata/ResultState$Failure;"
        }
    .end annotation

    if-nez p0, :cond_0

    .line 552
    sget-object p0, Lcom/squareup/ordermanagerdata/ResultState$Failure$GenericError;->INSTANCE:Lcom/squareup/ordermanagerdata/ResultState$Failure$GenericError;

    check-cast p0, Lcom/squareup/ordermanagerdata/ResultState$Failure;

    goto :goto_0

    .line 554
    :cond_0
    invoke-interface {p1, p0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/Status;

    invoke-static {p0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepositoryKt;->toResultStateFailure(Lcom/squareup/protos/client/Status;)Lcom/squareup/ordermanagerdata/ResultState$Failure;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method private static final isVersionMismatch(Lcom/squareup/protos/client/Status;)Z
    .locals 1

    .line 572
    iget-object p0, p0, Lcom/squareup/protos/client/Status;->ext_orders_error:Lcom/squareup/protos/connect/v2/resources/Error;

    if-eqz p0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/connect/v2/resources/Error;->code:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->VERSION_MISMATCH:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    if-ne p0, v0, :cond_1

    const/4 p0, 0x1

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    :goto_1
    return p0
.end method

.method private static final mapToResultState(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Throwable;",
            "+",
            "Lcom/squareup/ordermanagerdata/ResultState$Failure;",
            ">;)",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "TT;>;>;"
        }
    .end annotation

    .line 581
    sget-object v0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepositoryKt$mapToResultState$1;->INSTANCE:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepositoryKt$mapToResultState$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p0, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p0

    if-eqz p1, :cond_0

    new-instance v0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepositoryKt$sam$io_reactivex_functions_Function$0;

    invoke-direct {v0, p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepositoryKt$sam$io_reactivex_functions_Function$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object p1, v0

    :cond_0
    check-cast p1, Lio/reactivex/functions/Function;

    invoke-virtual {p0, p1}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p0

    const-string p1, "map { Success(it) as Res\u2026nErrorReturn(errorMapper)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method private static final mapToResultState(Lio/reactivex/Single;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Single<",
            "TT;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Throwable;",
            "+",
            "Lcom/squareup/ordermanagerdata/ResultState$Failure;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "TT;>;>;"
        }
    .end annotation

    .line 591
    sget-object v0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepositoryKt$mapToResultState$2;->INSTANCE:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepositoryKt$mapToResultState$2;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p0, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p0

    if-eqz p1, :cond_0

    new-instance v0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepositoryKt$sam$io_reactivex_functions_Function$0;

    invoke-direct {v0, p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepositoryKt$sam$io_reactivex_functions_Function$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object p1, v0

    :cond_0
    check-cast p1, Lio/reactivex/functions/Function;

    invoke-virtual {p0, p1}, Lio/reactivex/Single;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p0

    const-string p1, "map { Success(it) as Res\u2026nErrorReturn(errorMapper)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method private static final toResultState(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ordermanagerdata/ResultState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "+TT;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;+TR;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Lcom/squareup/protos/client/Status;",
            ">;)",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "TR;>;"
        }
    .end annotation

    .line 516
    instance-of v0, p0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    new-instance p2, Lcom/squareup/ordermanagerdata/ResultState$Success;

    check-cast p0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p0

    invoke-interface {p1, p0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    invoke-direct {p2, p0}, Lcom/squareup/ordermanagerdata/ResultState$Success;-><init>(Ljava/lang/Object;)V

    check-cast p2, Lcom/squareup/ordermanagerdata/ResultState;

    goto :goto_0

    .line 517
    :cond_0
    instance-of p1, p0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p1, :cond_1

    check-cast p0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-virtual {p0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p0

    invoke-static {p0, p2}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepositoryKt;->toResultStateFailure(Lcom/squareup/receiving/ReceivedResponse;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ordermanagerdata/ResultState$Failure;

    move-result-object p0

    move-object p2, p0

    check-cast p2, Lcom/squareup/ordermanagerdata/ResultState;

    :goto_0
    return-object p2

    :cond_1
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method private static final toResultStateFailure(Lcom/squareup/protos/client/Status;)Lcom/squareup/ordermanagerdata/ResultState$Failure;
    .locals 0

    .line 562
    invoke-static {p0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepositoryKt;->isVersionMismatch(Lcom/squareup/protos/client/Status;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 563
    sget-object p0, Lcom/squareup/ordermanagerdata/ResultState$Failure$VersionMismatchError;->INSTANCE:Lcom/squareup/ordermanagerdata/ResultState$Failure$VersionMismatchError;

    check-cast p0, Lcom/squareup/ordermanagerdata/ResultState$Failure;

    goto :goto_0

    .line 565
    :cond_0
    sget-object p0, Lcom/squareup/ordermanagerdata/ResultState$Failure$GenericError;->INSTANCE:Lcom/squareup/ordermanagerdata/ResultState$Failure$GenericError;

    check-cast p0, Lcom/squareup/ordermanagerdata/ResultState$Failure;

    :goto_0
    return-object p0
.end method

.method private static final toResultStateFailure(Lcom/squareup/receiving/ReceivedResponse;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ordermanagerdata/ResultState$Failure;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "+TT;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Lcom/squareup/protos/client/Status;",
            ">;)",
            "Lcom/squareup/ordermanagerdata/ResultState$Failure;"
        }
    .end annotation

    .line 531
    instance-of v0, p0, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    invoke-virtual {p0}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p0

    invoke-interface {p1, p0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/Status;

    invoke-static {p0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepositoryKt;->toResultStateFailure(Lcom/squareup/protos/client/Status;)Lcom/squareup/ordermanagerdata/ResultState$Failure;

    move-result-object p0

    goto :goto_0

    .line 532
    :cond_0
    instance-of v0, p0, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    if-eqz v0, :cond_1

    sget-object p0, Lcom/squareup/ordermanagerdata/ResultState$Failure$ConnectionError;->INSTANCE:Lcom/squareup/ordermanagerdata/ResultState$Failure$ConnectionError;

    check-cast p0, Lcom/squareup/ordermanagerdata/ResultState$Failure;

    goto :goto_0

    .line 533
    :cond_1
    instance-of v0, p0, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    if-eqz v0, :cond_2

    sget-object p0, Lcom/squareup/ordermanagerdata/ResultState$Failure$GenericError;->INSTANCE:Lcom/squareup/ordermanagerdata/ResultState$Failure$GenericError;

    check-cast p0, Lcom/squareup/ordermanagerdata/ResultState$Failure;

    goto :goto_0

    .line 534
    :cond_2
    instance-of v0, p0, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-eqz v0, :cond_3

    check-cast p0, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    invoke-virtual {p0}, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;->getResponse()Ljava/lang/Object;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepositoryKt;->getFailureFromResponse(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ordermanagerdata/ResultState$Failure;

    move-result-object p0

    goto :goto_0

    .line 535
    :cond_3
    instance-of v0, p0, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;

    if-eqz v0, :cond_4

    check-cast p0, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;

    invoke-virtual {p0}, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;->getResponse()Ljava/lang/Object;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepositoryKt;->getFailureFromResponse(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ordermanagerdata/ResultState$Failure;

    move-result-object p0

    :goto_0
    return-object p0

    .line 536
    :cond_4
    instance-of p0, p0, Lcom/squareup/receiving/ReceivedResponse$Okay$Accepted;

    if-eqz p0, :cond_5

    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Accepted is not a failure"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    :cond_5
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method private static final toResultStateSingle(Lio/reactivex/Completable;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Completable;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Throwable;",
            "+",
            "Lcom/squareup/ordermanagerdata/ResultState$Failure;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation

    .line 601
    new-instance v0, Lcom/squareup/ordermanagerdata/ResultState$Success;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-direct {v0, v1}, Lcom/squareup/ordermanagerdata/ResultState$Success;-><init>(Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/ordermanagerdata/ResultState;

    invoke-virtual {p0, v0}, Lio/reactivex/Completable;->toSingleDefault(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p0

    if-eqz p1, :cond_0

    new-instance v0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepositoryKt$sam$io_reactivex_functions_Function$0;

    invoke-direct {v0, p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepositoryKt$sam$io_reactivex_functions_Function$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object p1, v0

    :cond_0
    check-cast p1, Lio/reactivex/functions/Function;

    invoke-virtual {p0, p1}, Lio/reactivex/Single;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p0

    const-string/jumbo p1, "toSingleDefault(Success(\u2026nErrorReturn(errorMapper)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
