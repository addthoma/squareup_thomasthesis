.class public Lcom/squareup/hardware/UsbDetachedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "UsbDetachedReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/hardware/UsbDetachedReceiver$Component;
    }
.end annotation


# instance fields
.field eventSink:Lcom/squareup/badbus/BadEventSink;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field usbDiscoverer:Lcom/squareup/usb/UsbDiscoverer;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    .line 27
    invoke-static {}, Lcom/squareup/mortar/AppContextWrapper;->appContext()Landroid/content/Context;

    move-result-object p1

    const-class p2, Lcom/squareup/hardware/UsbDetachedReceiver$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/hardware/UsbDetachedReceiver$Component;

    invoke-interface {p1, p0}, Lcom/squareup/hardware/UsbDetachedReceiver$Component;->inject(Lcom/squareup/hardware/UsbDetachedReceiver;)V

    .line 29
    iget-object p1, p0, Lcom/squareup/hardware/UsbDetachedReceiver;->usbDiscoverer:Lcom/squareup/usb/UsbDiscoverer;

    invoke-virtual {p1}, Lcom/squareup/usb/UsbDiscoverer;->scheduleSearchForUsbDevices()V

    .line 30
    iget-object p1, p0, Lcom/squareup/hardware/UsbDetachedReceiver;->eventSink:Lcom/squareup/badbus/BadEventSink;

    new-instance p2, Lcom/squareup/hardware/UsbDevicesChangedEvent;

    const/4 v0, 0x0

    invoke-direct {p2, v0}, Lcom/squareup/hardware/UsbDevicesChangedEvent;-><init>(Z)V

    invoke-interface {p1, p2}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    return-void
.end method
