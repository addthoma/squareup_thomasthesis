.class public final Lcom/squareup/hudtoaster/HudToaster$Companion;
.super Ljava/lang/Object;
.source "HudToaster.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/hudtoaster/HudToaster;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/hudtoaster/HudToaster$Companion;",
        "",
        "()V",
        "NO_STRING_ID",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$INSTANCE:Lcom/squareup/hudtoaster/HudToaster$Companion;

.field public static final NO_STRING_ID:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/hudtoaster/HudToaster$Companion;

    invoke-direct {v0}, Lcom/squareup/hudtoaster/HudToaster$Companion;-><init>()V

    sput-object v0, Lcom/squareup/hudtoaster/HudToaster$Companion;->$$INSTANCE:Lcom/squareup/hudtoaster/HudToaster$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
