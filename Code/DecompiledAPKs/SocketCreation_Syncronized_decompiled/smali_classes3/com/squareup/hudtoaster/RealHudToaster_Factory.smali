.class public final Lcom/squareup/hudtoaster/RealHudToaster_Factory;
.super Ljava/lang/Object;
.source "RealHudToaster_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/hudtoaster/RealHudToaster;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final hudToasterLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/log/HudToasterLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final toastFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/log/HudToasterLogger;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/hudtoaster/RealHudToaster_Factory;->applicationProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/hudtoaster/RealHudToaster_Factory;->toastFactoryProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p3, p0, Lcom/squareup/hudtoaster/RealHudToaster_Factory;->hudToasterLoggerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/hudtoaster/RealHudToaster_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/log/HudToasterLogger;",
            ">;)",
            "Lcom/squareup/hudtoaster/RealHudToaster_Factory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/hudtoaster/RealHudToaster_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/hudtoaster/RealHudToaster_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Landroid/app/Application;Lcom/squareup/util/ToastFactory;Lcom/squareup/hudtoaster/log/HudToasterLogger;)Lcom/squareup/hudtoaster/RealHudToaster;
    .locals 1

    .line 42
    new-instance v0, Lcom/squareup/hudtoaster/RealHudToaster;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/hudtoaster/RealHudToaster;-><init>(Landroid/app/Application;Lcom/squareup/util/ToastFactory;Lcom/squareup/hudtoaster/log/HudToasterLogger;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/hudtoaster/RealHudToaster;
    .locals 3

    .line 31
    iget-object v0, p0, Lcom/squareup/hudtoaster/RealHudToaster_Factory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/hudtoaster/RealHudToaster_Factory;->toastFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/ToastFactory;

    iget-object v2, p0, Lcom/squareup/hudtoaster/RealHudToaster_Factory;->hudToasterLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/hudtoaster/log/HudToasterLogger;

    invoke-static {v0, v1, v2}, Lcom/squareup/hudtoaster/RealHudToaster_Factory;->newInstance(Landroid/app/Application;Lcom/squareup/util/ToastFactory;Lcom/squareup/hudtoaster/log/HudToasterLogger;)Lcom/squareup/hudtoaster/RealHudToaster;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/hudtoaster/RealHudToaster_Factory;->get()Lcom/squareup/hudtoaster/RealHudToaster;

    move-result-object v0

    return-object v0
.end method
