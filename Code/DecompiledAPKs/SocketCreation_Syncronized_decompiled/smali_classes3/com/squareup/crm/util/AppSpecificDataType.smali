.class public final enum Lcom/squareup/crm/util/AppSpecificDataType;
.super Ljava/lang/Enum;
.source "RolodexContactHelper.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/crm/util/AppSpecificDataType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000j\u0002\u0008\u0005j\u0002\u0008\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/crm/util/AppSpecificDataType;",
        "",
        "asdName",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "LOYALTY_ACCOUNT_TOKEN",
        "LOYALTY_ACCOUNT_PHONE_TOKEN",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/crm/util/AppSpecificDataType;

.field public static final enum LOYALTY_ACCOUNT_PHONE_TOKEN:Lcom/squareup/crm/util/AppSpecificDataType;

.field public static final enum LOYALTY_ACCOUNT_TOKEN:Lcom/squareup/crm/util/AppSpecificDataType;


# instance fields
.field public final asdName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/crm/util/AppSpecificDataType;

    new-instance v1, Lcom/squareup/crm/util/AppSpecificDataType;

    const/4 v2, 0x0

    const-string v3, "LOYALTY_ACCOUNT_TOKEN"

    const-string v4, "LoyaltyAccountToken"

    .line 67
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/crm/util/AppSpecificDataType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/crm/util/AppSpecificDataType;->LOYALTY_ACCOUNT_TOKEN:Lcom/squareup/crm/util/AppSpecificDataType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/crm/util/AppSpecificDataType;

    const/4 v2, 0x1

    const-string v3, "LOYALTY_ACCOUNT_PHONE_TOKEN"

    const-string v4, "LoyaltyPhoneToken"

    .line 73
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/crm/util/AppSpecificDataType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/crm/util/AppSpecificDataType;->LOYALTY_ACCOUNT_PHONE_TOKEN:Lcom/squareup/crm/util/AppSpecificDataType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/crm/util/AppSpecificDataType;->$VALUES:[Lcom/squareup/crm/util/AppSpecificDataType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 58
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/crm/util/AppSpecificDataType;->asdName:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/crm/util/AppSpecificDataType;
    .locals 1

    const-class v0, Lcom/squareup/crm/util/AppSpecificDataType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/crm/util/AppSpecificDataType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/crm/util/AppSpecificDataType;
    .locals 1

    sget-object v0, Lcom/squareup/crm/util/AppSpecificDataType;->$VALUES:[Lcom/squareup/crm/util/AppSpecificDataType;

    invoke-virtual {v0}, [Lcom/squareup/crm/util/AppSpecificDataType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/crm/util/AppSpecificDataType;

    return-object v0
.end method
