.class Lcom/squareup/crm/RealCustomerManagementSettings;
.super Ljava/lang/Object;
.source "RealCustomerManagementSettings.java"

# interfaces
.implements Lcom/squareup/crm/CustomerManagementSettings;


# instance fields
.field private final afterCheckoutEnabled:Lcom/squareup/crm/CustomerManagementAfterCheckoutSetting;

.field private final apiTransactionState:Lcom/squareup/api/ApiTransactionState;

.field private final beforeCheckoutEnabled:Lcom/squareup/crm/CustomerManagementBeforeCheckoutSetting;

.field private final device:Lcom/squareup/util/Device;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final isReaderSdk:Z

.field private final saveCardEnabled:Lcom/squareup/crm/CustomerManagementUseCardOnFileSetting;

.field private final saveCardPostTransactionEnabled:Lcom/squareup/crm/CustomerManagementSaveCardPostTransactionSetting;


# direct methods
.method constructor <init>(Lcom/squareup/api/ApiTransactionState;Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;Lcom/squareup/crm/CustomerManagementBeforeCheckoutSetting;Lcom/squareup/crm/CustomerManagementAfterCheckoutSetting;Lcom/squareup/crm/CustomerManagementUseCardOnFileSetting;Lcom/squareup/crm/CustomerManagementSaveCardPostTransactionSetting;Z)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/crm/RealCustomerManagementSettings;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    .line 34
    iput-object p2, p0, Lcom/squareup/crm/RealCustomerManagementSettings;->device:Lcom/squareup/util/Device;

    .line 35
    iput-object p3, p0, Lcom/squareup/crm/RealCustomerManagementSettings;->features:Lcom/squareup/settings/server/Features;

    .line 36
    iput-object p4, p0, Lcom/squareup/crm/RealCustomerManagementSettings;->beforeCheckoutEnabled:Lcom/squareup/crm/CustomerManagementBeforeCheckoutSetting;

    .line 37
    iput-object p5, p0, Lcom/squareup/crm/RealCustomerManagementSettings;->afterCheckoutEnabled:Lcom/squareup/crm/CustomerManagementAfterCheckoutSetting;

    .line 38
    iput-object p6, p0, Lcom/squareup/crm/RealCustomerManagementSettings;->saveCardEnabled:Lcom/squareup/crm/CustomerManagementUseCardOnFileSetting;

    .line 39
    iput-object p7, p0, Lcom/squareup/crm/RealCustomerManagementSettings;->saveCardPostTransactionEnabled:Lcom/squareup/crm/CustomerManagementSaveCardPostTransactionSetting;

    .line 40
    iput-boolean p8, p0, Lcom/squareup/crm/RealCustomerManagementSettings;->isReaderSdk:Z

    return-void
.end method


# virtual methods
.method public isAddCustomerToSaleEnabled()Z
    .locals 1

    .line 59
    invoke-virtual {p0}, Lcom/squareup/crm/RealCustomerManagementSettings;->isBeforeCheckoutEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/crm/RealCustomerManagementSettings;->isAfterCheckoutEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isAfterCheckoutEnabled()Z
    .locals 1

    .line 71
    invoke-virtual {p0}, Lcom/squareup/crm/RealCustomerManagementSettings;->isAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/crm/RealCustomerManagementSettings;->isReaderSdk:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/crm/RealCustomerManagementSettings;->afterCheckoutEnabled:Lcom/squareup/crm/CustomerManagementAfterCheckoutSetting;

    .line 74
    invoke-virtual {v0}, Lcom/squareup/crm/CustomerManagementAfterCheckoutSetting;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isAllowed()Z
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/crm/RealCustomerManagementSettings;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    invoke-virtual {v0}, Lcom/squareup/api/ApiTransactionState;->customersSupported()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/crm/RealCustomerManagementSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CUSTOMER_MANAGEMENT_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    .line 45
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/crm/RealCustomerManagementSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CUSTOMER_MANAGEMENT_TABLET:Lcom/squareup/settings/server/Features$Feature;

    .line 46
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isBeforeCheckoutEnabled()Z
    .locals 1

    .line 63
    invoke-virtual {p0}, Lcom/squareup/crm/RealCustomerManagementSettings;->isAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/crm/RealCustomerManagementSettings;->beforeCheckoutEnabled:Lcom/squareup/crm/CustomerManagementBeforeCheckoutSetting;

    invoke-virtual {v0}, Lcom/squareup/crm/CustomerManagementBeforeCheckoutSetting;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isCardOnFileEnabled()Z
    .locals 1

    .line 105
    invoke-virtual {p0}, Lcom/squareup/crm/RealCustomerManagementSettings;->isCardOnFileFeatureEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/crm/RealCustomerManagementSettings;->isSaveCardEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isCardOnFileFeatureEnabled()Z
    .locals 2

    .line 109
    invoke-virtual {p0}, Lcom/squareup/crm/RealCustomerManagementSettings;->isAllowed()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/crm/RealCustomerManagementSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_CARD_ON_FILE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/crm/RealCustomerManagementSettings;->device:Lcom/squareup/util/Device;

    .line 110
    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/crm/RealCustomerManagementSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_CARD_ON_FILE_ON_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isEmvCardOnFileFeatureEnabled()Z
    .locals 2

    .line 114
    invoke-virtual {p0}, Lcom/squareup/crm/RealCustomerManagementSettings;->isCardOnFileEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/crm/RealCustomerManagementSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CARD_ON_FILE_DIP_POST_PAYMENT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSaveCardEnabled()Z
    .locals 1

    .line 82
    invoke-virtual {p0}, Lcom/squareup/crm/RealCustomerManagementSettings;->isCardOnFileFeatureEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/crm/RealCustomerManagementSettings;->saveCardEnabled:Lcom/squareup/crm/CustomerManagementUseCardOnFileSetting;

    invoke-virtual {v0}, Lcom/squareup/crm/CustomerManagementUseCardOnFileSetting;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSaveCardPostTransactionEnabled()Z
    .locals 1

    .line 90
    invoke-virtual {p0}, Lcom/squareup/crm/RealCustomerManagementSettings;->isCardOnFileEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/crm/RealCustomerManagementSettings;->isReaderSdk:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/crm/RealCustomerManagementSettings;->saveCardPostTransactionEnabled:Lcom/squareup/crm/CustomerManagementSaveCardPostTransactionSetting;

    .line 93
    invoke-virtual {v0}, Lcom/squareup/crm/CustomerManagementSaveCardPostTransactionSetting;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public setAfterCheckoutEnabled(Z)V
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/crm/RealCustomerManagementSettings;->afterCheckoutEnabled:Lcom/squareup/crm/CustomerManagementAfterCheckoutSetting;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/crm/CustomerManagementAfterCheckoutSetting;->setValueLocally(Ljava/lang/Object;)V

    return-void
.end method

.method public setBeforeCheckoutEnabled(Z)V
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/crm/RealCustomerManagementSettings;->beforeCheckoutEnabled:Lcom/squareup/crm/CustomerManagementBeforeCheckoutSetting;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/crm/CustomerManagementBeforeCheckoutSetting;->setValueLocally(Ljava/lang/Object;)V

    return-void
.end method

.method public setSaveCardEnabled(Z)V
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/crm/RealCustomerManagementSettings;->saveCardEnabled:Lcom/squareup/crm/CustomerManagementUseCardOnFileSetting;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/crm/CustomerManagementUseCardOnFileSetting;->setValueLocally(Ljava/lang/Object;)V

    return-void
.end method

.method public setSaveCardPostTransactionEnabled(Z)V
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/crm/RealCustomerManagementSettings;->saveCardPostTransactionEnabled:Lcom/squareup/crm/CustomerManagementSaveCardPostTransactionSetting;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/crm/CustomerManagementSaveCardPostTransactionSetting;->setValueLocally(Ljava/lang/Object;)V

    return-void
.end method

.method public shouldDisplaySettingSectionOn()Z
    .locals 1

    .line 51
    invoke-virtual {p0}, Lcom/squareup/crm/RealCustomerManagementSettings;->isBeforeCheckoutEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 52
    invoke-virtual {p0}, Lcom/squareup/crm/RealCustomerManagementSettings;->isAfterCheckoutEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 53
    invoke-virtual {p0}, Lcom/squareup/crm/RealCustomerManagementSettings;->isSaveCardEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 54
    invoke-virtual {p0}, Lcom/squareup/crm/RealCustomerManagementSettings;->isSaveCardPostTransactionEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public showCardButtonEnabledForTenderType(ZZ)Z
    .locals 0

    if-nez p1, :cond_1

    if-eqz p2, :cond_0

    .line 119
    invoke-virtual {p0}, Lcom/squareup/crm/RealCustomerManagementSettings;->isEmvCardOnFileFeatureEnabled()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method
