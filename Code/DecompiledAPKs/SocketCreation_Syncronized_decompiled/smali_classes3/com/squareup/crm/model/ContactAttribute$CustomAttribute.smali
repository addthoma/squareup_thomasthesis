.class public abstract Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;
.super Lcom/squareup/crm/model/ContactAttribute;
.source "ContactAttribute.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/crm/model/ContactAttribute;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "CustomAttribute"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;,
        Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomNumberAttribute;,
        Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomBooleanAttribute;,
        Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;,
        Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;,
        Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomPhoneAttribute;,
        Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEmailAttribute;,
        Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomAddressAttribute;,
        Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomDateAttribute;,
        Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/crm/model/ContactAttribute;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \u0012*\u0004\u0008\u0000\u0010\u00012\u00020\u0002:\n\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019\u001a\u001bB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u0011\u001a\u00020\u000bH&R\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0007R\u0012\u0010\u0008\u001a\u00020\u0005X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\u0007R\u0012\u0010\n\u001a\u00020\u000bX\u00a4\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\rR\u0014\u0010\u000e\u001a\u0004\u0018\u00018\u0000X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000f\u0010\u0010\u0082\u0001\t\u001c\u001d\u001e\u001f !\"#$\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;",
        "T",
        "Lcom/squareup/crm/model/ContactAttribute;",
        "()V",
        "fallbackValue",
        "",
        "getFallbackValue",
        "()Ljava/lang/String;",
        "name",
        "getName",
        "original",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;",
        "getOriginal",
        "()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;",
        "value",
        "getValue",
        "()Ljava/lang/Object;",
        "toAttribute",
        "Companion",
        "CustomAddressAttribute",
        "CustomBooleanAttribute",
        "CustomDateAttribute",
        "CustomEmailAttribute",
        "CustomEnumAttribute",
        "CustomNumberAttribute",
        "CustomPhoneAttribute",
        "CustomTextAttribute",
        "CustomUnknownAttribute",
        "Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;",
        "Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomNumberAttribute;",
        "Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomBooleanAttribute;",
        "Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;",
        "Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;",
        "Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomPhoneAttribute;",
        "Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEmailAttribute;",
        "Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomAddressAttribute;",
        "Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomDateAttribute;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;->Companion:Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 87
    invoke-direct {p0, v0}, Lcom/squareup/crm/model/ContactAttribute;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 87
    invoke-direct {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getFallbackValue()Ljava/lang/String;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method protected abstract getOriginal()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;
.end method

.method public abstract getValue()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public abstract toAttribute()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;
.end method
