.class public interface abstract Lcom/squareup/crm/RolodexServiceHelper;
.super Ljava/lang/Object;
.source "RolodexServiceHelper.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00ee\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001J.\u0010\u0002\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u00040\u0003j\u0008\u0012\u0004\u0012\u00020\u0005`\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH&JJ\u0010\u000b\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000c0\u00040\u0003j\u0008\u0012\u0004\u0012\u00020\u000c`\u00062\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e2\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\n0\u000e2\u0006\u0010\u0011\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\nH&J@\u0010\u0013\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00140\u00040\u0003j\u0008\u0012\u0004\u0012\u00020\u0014`\u00062\u0006\u0010\u0015\u001a\u00020\n2\u0006\u0010\u0016\u001a\u00020\n2\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\'J&\u0010\u001b\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u001c0\u00040\u0003j\u0008\u0012\u0004\u0012\u00020\u001c`\u00062\u0006\u0010\u001d\u001a\u00020\nH&J&\u0010\u001e\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u001f0\u00040\u0003j\u0008\u0012\u0004\u0012\u00020\u001f`\u00062\u0006\u0010 \u001a\u00020!H&J&\u0010\"\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020#0\u00040\u0003j\u0008\u0012\u0004\u0012\u00020#`\u00062\u0006\u0010\u0007\u001a\u00020\u0008H&J&\u0010$\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020%0\u00040\u0003j\u0008\u0012\u0004\u0012\u00020%`\u00062\u0006\u0010&\u001a\u00020\'H&J&\u0010(\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020)0\u00040\u0003j\u0008\u0012\u0004\u0012\u00020)`\u00062\u0006\u0010*\u001a\u00020+H&J.\u0010,\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020-0\u00040\u0003j\u0008\u0012\u0004\u0012\u00020-`\u00062\u0006\u0010.\u001a\u00020/2\u0006\u00100\u001a\u000201H&J.\u00102\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002030\u00040\u0003j\u0008\u0012\u0004\u0012\u000203`\u00062\u0006\u0010\u001d\u001a\u00020\n2\u0006\u00104\u001a\u000201H&J&\u00105\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002060\u00040\u0003j\u0008\u0012\u0004\u0012\u000206`\u00062\u0006\u0010\u0015\u001a\u00020\nH&J.\u00105\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002060\u00040\u0003j\u0008\u0012\u0004\u0012\u000206`\u00062\u0006\u0010\u0015\u001a\u00020\n2\u0006\u00107\u001a\u000201H&J\n\u00108\u001a\u0004\u0018\u000109H&J&\u0010:\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020;0\u00040\u0003j\u0008\u0012\u0004\u0012\u00020;`\u00062\u0006\u0010\u0007\u001a\u00020\u0008H&J&\u0010<\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020=0\u00040\u0003j\u0008\u0012\u0004\u0012\u00020=`\u00062\u0006\u0010>\u001a\u000201H&Je\u0010?\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020@0\u00040\u0003j\u0008\u0012\u0004\u0012\u00020@`\u00062\u0008\u0010\t\u001a\u0004\u0018\u00010\n2\u0008\u0010A\u001a\u0004\u0018\u00010\n2\u000e\u0010B\u001a\n\u0012\u0004\u0012\u00020C\u0018\u00010\u000e2\u0008\u0010D\u001a\u0004\u0018\u00010E2\u0008\u0010F\u001a\u0004\u0018\u00010\n2\u0008\u0010G\u001a\u0004\u0018\u00010HH&\u00a2\u0006\u0002\u0010IJ?\u0010J\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020K0\u00040\u0003j\u0008\u0012\u0004\u0012\u00020K`\u00062\u0006\u0010\u0015\u001a\u00020\n2\u0008\u0010L\u001a\u0004\u0018\u00010H2\u0008\u0010F\u001a\u0004\u0018\u00010\nH&\u00a2\u0006\u0002\u0010MJ\u001e\u0010N\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020O0\u00040\u0003j\u0008\u0012\u0004\u0012\u00020O`\u0006H&J&\u0010P\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020Q0\u00040\u0003j\u0008\u0012\u0004\u0012\u00020Q`\u00062\u0006\u0010>\u001a\u000201H&J7\u0010R\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020S0\u00040\u0003j\u0008\u0012\u0004\u0012\u00020S`\u00062\u0008\u0010L\u001a\u0004\u0018\u00010H2\u0008\u0010F\u001a\u0004\u0018\u00010\nH&\u00a2\u0006\u0002\u0010TJ.\u0010U\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020V0\u00040\u0003j\u0008\u0012\u0004\u0012\u00020V`\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\u0019\u001a\u00020\u001aH&J8\u0010U\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020V0\u00040\u0003j\u0008\u0012\u0004\u0012\u00020V`\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\u0019\u001a\u00020\u001a2\u0008\u0010W\u001a\u0004\u0018\u00010XH&J\u000e\u0010Y\u001a\u0008\u0012\u0004\u0012\u00020[0ZH&J\u000e\u0010\\\u001a\u0008\u0012\u0004\u0012\u00020[0ZH&J\u001e\u0010]\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020^0\u00040\u0003j\u0008\u0012\u0004\u0012\u00020^`\u0006H&J\u0008\u0010_\u001a\u00020`H&J(\u0010a\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020b0\u00040\u0003j\u0008\u0012\u0004\u0012\u00020b`\u00062\u0008\u0010c\u001a\u0004\u0018\u00010\nH&J.\u0010d\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020e0\u00040\u0003j\u0008\u0012\u0004\u0012\u00020e`\u00062\u0006\u0010\u0015\u001a\u00020\n2\u0006\u0010f\u001a\u00020\nH&J6\u0010g\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020h0\u00040\u0003j\u0008\u0012\u0004\u0012\u00020h`\u00062\u0006\u0010\u001d\u001a\u00020\n2\u0006\u0010i\u001a\u00020\n2\u0006\u0010\u0015\u001a\u00020\nH&J.\u0010j\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00140\u00040\u0003j\u0008\u0012\u0004\u0012\u00020\u0014`\u00062\u0006\u0010*\u001a\u00020+2\u0006\u0010\u0016\u001a\u00020\nH\'J6\u0010k\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002030\u00040\u0003j\u0008\u0012\u0004\u0012\u000203`\u00062\u0006\u0010\u0015\u001a\u00020\n2\u0006\u0010i\u001a\u00020\n2\u0006\u0010l\u001a\u00020mH&J:\u0010n\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000c0\u00040\u0003j\u0008\u0012\u0004\u0012\u00020\u000c`\u00062\u000c\u0010o\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e2\u000c\u0010p\u001a\u0008\u0012\u0004\u0012\u00020\n0\u000eH&J.\u0010q\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020r0\u00040\u0003j\u0008\u0012\u0004\u0012\u00020r`\u00062\u0006\u0010 \u001a\u00020!2\u0006\u0010\u0019\u001a\u00020\u001aH&J:\u0010s\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020t0\u00040\u0003j\u0008\u0012\u0004\u0012\u00020t`\u00062\u0006\u0010u\u001a\u00020\u001a2\u0008\u0010c\u001a\u0004\u0018\u00010\n2\u0008\u0010 \u001a\u0004\u0018\u00010!H&J.\u0010v\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020w0\u00040\u0003j\u0008\u0012\u0004\u0012\u00020w`\u00062\u0006\u0010&\u001a\u00020\'2\u0006\u0010\u0019\u001a\u00020\u001aH&J&\u0010x\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020y0\u00040\u0003j\u0008\u0012\u0004\u0012\u00020y`\u00062\u0006\u0010&\u001a\u00020zH&Jf\u0010{\u001a\u0018\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020|0\u00040\u0003j\u0008\u0012\u0004\u0012\u00020|`\u00062\u0006\u0010\u0015\u001a\u00020\n2\u0008\u0010}\u001a\u0004\u0018\u00010\n2\u0006\u0010~\u001a\u00020\u007f2\n\u0010\u0080\u0001\u001a\u0005\u0018\u00010\u0081\u00012\n\u0010\u0082\u0001\u001a\u0005\u0018\u00010\u0083\u00012\t\u0010\u0084\u0001\u001a\u0004\u0018\u00010\n2\t\u0010\u0085\u0001\u001a\u0004\u0018\u00010\nH&\u00a8\u0006\u0086\u0001"
    }
    d2 = {
        "Lcom/squareup/crm/RolodexServiceHelper;",
        "",
        "addContactsToGroup",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/rolodex/AddContactsToGroupsResponse;",
        "Lcom/squareup/crm/Result;",
        "contactSet",
        "Lcom/squareup/protos/client/rolodex/ContactSet;",
        "groupToken",
        "",
        "createNewProfileAttribute",
        "Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaResponse;",
        "existingAttributeDefinitions",
        "",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;",
        "existingAttributeKeysInOrder",
        "newAttributeDefinition",
        "newAttributeKey",
        "createNote",
        "Lcom/squareup/protos/client/rolodex/UpsertNoteResponse;",
        "contactToken",
        "body",
        "reminder",
        "Lcom/squareup/protos/client/rolodex/Reminder;",
        "requestUuid",
        "Ljava/util/UUID;",
        "deleteAttachment",
        "Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;",
        "attachmentToken",
        "deleteContact",
        "Lcom/squareup/protos/client/rolodex/DeleteContactResponse;",
        "contact",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "deleteContacts",
        "Lcom/squareup/protos/client/rolodex/DeleteContactsResponse;",
        "deleteGroup",
        "Lcom/squareup/protos/client/rolodex/DeleteGroupResponse;",
        "group",
        "Lcom/squareup/protos/client/rolodex/Group;",
        "deleteNote",
        "Lcom/squareup/protos/client/rolodex/DeleteNoteResponse;",
        "note",
        "Lcom/squareup/protos/client/rolodex/Note;",
        "dismissMergeProposal",
        "Lcom/squareup/protos/client/rolodex/DismissMergeProposalResponse;",
        "mergeProposal",
        "Lcom/squareup/protos/client/rolodex/MergeProposal;",
        "dismissed",
        "",
        "downloadAttachment",
        "Lokhttp3/ResponseBody;",
        "isPreview",
        "getContact",
        "Lcom/squareup/protos/client/rolodex/GetContactResponse;",
        "includeAttachments",
        "getCurrentEmployee",
        "Lcom/squareup/protos/client/Employee;",
        "getManualMergeProposal",
        "Lcom/squareup/protos/client/rolodex/ListManualMergeProposalResponse;",
        "getMerchant",
        "Lcom/squareup/protos/client/rolodex/GetMerchantResponse;",
        "includeCounts",
        "listContacts",
        "Lcom/squareup/protos/client/rolodex/ListContactsResponse;",
        "searchTerm",
        "filterList",
        "Lcom/squareup/protos/client/rolodex/Filter;",
        "sortType",
        "Lcom/squareup/protos/client/rolodex/ListContactsSortType;",
        "pagingKey",
        "pageSize",
        "",
        "(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/rolodex/ListContactsSortType;Ljava/lang/String;Ljava/lang/Integer;)Lio/reactivex/Single;",
        "listEventsForContact",
        "Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;",
        "limit",
        "(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Lio/reactivex/Single;",
        "listFilterTemplates",
        "Lcom/squareup/protos/client/rolodex/ListFilterTemplatesResponse;",
        "listGroups",
        "Lcom/squareup/protos/client/rolodex/ListGroupsResponse;",
        "listMergeProposals",
        "Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;",
        "(Ljava/lang/Integer;Ljava/lang/String;)Lio/reactivex/Single;",
        "manualMergeContacts",
        "Lcom/squareup/protos/client/rolodex/ManualMergeContactsResponse;",
        "loyaltyAccountMapping",
        "Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;",
        "onContactsAddedOrRemoved",
        "Lio/reactivex/Observable;",
        "",
        "onGroupsAddedOrRemoved",
        "runMergeAllJob",
        "Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;",
        "runMergeProposalJob",
        "Lio/reactivex/Completable;",
        "shouldShowEmailCollection",
        "Lcom/squareup/protos/client/rolodex/ShouldShowEmailCollectionResponse;",
        "paymentToken",
        "unlinkInstrumentOnFile",
        "Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileResponse;",
        "instrumentToken",
        "updateAttachment",
        "Lcom/squareup/protos/client/rolodex/UpdateAttachmentResponse;",
        "fileName",
        "updateNote",
        "uploadAttachment",
        "requestBody",
        "Lokhttp3/RequestBody;",
        "upsertAttributeSchema",
        "attributeDefinitions",
        "attributeKeysInOrder",
        "upsertContact",
        "Lcom/squareup/protos/client/rolodex/UpsertContactResponse;",
        "upsertContactForEmailCollection",
        "Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionResponse;",
        "requestToken",
        "upsertManualGroup",
        "Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;",
        "upsertSmartGroup",
        "Lcom/squareup/protos/client/rolodex/UpsertGroupV2Response;",
        "Lcom/squareup/protos/client/rolodex/GroupV2;",
        "verifyAndLinkCard",
        "Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;",
        "cardholderName",
        "card",
        "Lcom/squareup/Card;",
        "cardData",
        "Lcom/squareup/protos/client/bills/CardData;",
        "entryMethod",
        "Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;",
        "postalCode",
        "uniqueKey",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract addContactsToGroup(Lcom/squareup/protos/client/rolodex/ContactSet;Ljava/lang/String;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/ContactSet;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/AddContactsToGroupsResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract createNewProfileAttribute(Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;Ljava/lang/String;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract createNote(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/Reminder;Ljava/util/UUID;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/rolodex/Reminder;",
            "Ljava/util/UUID;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/UpsertNoteResponse;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use custom attributes instead"
    .end annotation
.end method

.method public abstract deleteAttachment(Ljava/lang/String;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract deleteContact(Lcom/squareup/protos/client/rolodex/Contact;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/DeleteContactResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract deleteContacts(Lcom/squareup/protos/client/rolodex/ContactSet;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/ContactSet;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/DeleteContactsResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract deleteGroup(Lcom/squareup/protos/client/rolodex/Group;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/DeleteGroupResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract deleteNote(Lcom/squareup/protos/client/rolodex/Note;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Note;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/DeleteNoteResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract dismissMergeProposal(Lcom/squareup/protos/client/rolodex/MergeProposal;Z)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/MergeProposal;",
            "Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/DismissMergeProposalResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract downloadAttachment(Ljava/lang/String;Z)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lokhttp3/ResponseBody;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract getContact(Ljava/lang/String;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/GetContactResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract getContact(Ljava/lang/String;Z)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/GetContactResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract getCurrentEmployee()Lcom/squareup/protos/client/Employee;
.end method

.method public abstract getManualMergeProposal(Lcom/squareup/protos/client/rolodex/ContactSet;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/ContactSet;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/ListManualMergeProposalResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract getMerchant(Z)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/GetMerchantResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract listContacts(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/rolodex/ListContactsSortType;Ljava/lang/String;Ljava/lang/Integer;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;",
            "Lcom/squareup/protos/client/rolodex/ListContactsSortType;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/ListContactsResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract listEventsForContact(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract listFilterTemplates()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/ListFilterTemplatesResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract listGroups(Z)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/ListGroupsResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract listMergeProposals(Ljava/lang/Integer;Ljava/lang/String;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract manualMergeContacts(Lcom/squareup/protos/client/rolodex/ContactSet;Ljava/util/UUID;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/ContactSet;",
            "Ljava/util/UUID;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/ManualMergeContactsResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract manualMergeContacts(Lcom/squareup/protos/client/rolodex/ContactSet;Ljava/util/UUID;Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/ContactSet;",
            "Ljava/util/UUID;",
            "Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/ManualMergeContactsResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract onContactsAddedOrRemoved()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onGroupsAddedOrRemoved()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end method

.method public abstract runMergeAllJob()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract runMergeProposalJob()Lio/reactivex/Completable;
.end method

.method public abstract shouldShowEmailCollection(Ljava/lang/String;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/ShouldShowEmailCollectionResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract unlinkInstrumentOnFile(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract updateAttachment(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/UpdateAttachmentResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract updateNote(Lcom/squareup/protos/client/rolodex/Note;Ljava/lang/String;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Note;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/UpsertNoteResponse;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use custom attributes instead"
    .end annotation
.end method

.method public abstract uploadAttachment(Ljava/lang/String;Ljava/lang/String;Lokhttp3/RequestBody;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lokhttp3/RequestBody;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lokhttp3/ResponseBody;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract upsertAttributeSchema(Ljava/util/List;Ljava/util/List;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract upsertContact(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/UUID;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            "Ljava/util/UUID;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/UpsertContactResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract upsertContactForEmailCollection(Ljava/util/UUID;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/Contact;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/UUID;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract upsertManualGroup(Lcom/squareup/protos/client/rolodex/Group;Ljava/util/UUID;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Group;",
            "Ljava/util/UUID;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract upsertSmartGroup(Lcom/squareup/protos/client/rolodex/GroupV2;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/GroupV2;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/UpsertGroupV2Response;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract verifyAndLinkCard(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/Card;Lcom/squareup/protos/client/bills/CardData;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/Card;",
            "Lcom/squareup/protos/client/bills/CardData;",
            "Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;",
            ">;>;"
        }
    .end annotation
.end method
