.class public final Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealEditGroupWorkflow.kt"

# interfaces
.implements Lcom/squareup/crm/groups/edit/EditGroupWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/crm/groups/edit/EditGroupState;",
        "Lcom/squareup/crm/groups/edit/EditGroupOutput;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        ">;",
        "Lcom/squareup/crm/groups/edit/EditGroupWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealEditGroupWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealEditGroupWorkflow.kt\ncom/squareup/crm/groups/edit/RealEditGroupWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 5 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,113:1\n32#2,12:114\n85#3:126\n240#4:127\n276#5:128\n*E\n*S KotlinDebug\n*F\n+ 1 RealEditGroupWorkflow.kt\ncom/squareup/crm/groups/edit/RealEditGroupWorkflow\n*L\n41#1,12:114\n89#1:126\n89#1:127\n89#1:128\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0005\u0018\u00002\u00020\u00012\u001a\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0002B\u0017\u0008\u0007\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u001c\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\r2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u001f\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u00032\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0016\u00a2\u0006\u0002\u0010\u0015J1\u0010\u0016\u001a\u00020\u00062\u0006\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u0017\u001a\u00020\u00042\u0012\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0019H\u0016\u00a2\u0006\u0002\u0010\u001aJ&\u0010\u001b\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\rj\u0002`\u001d0\u001c2\u0006\u0010\u0017\u001a\u00020\u0004H\u0002J$\u0010\u001e\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\r2\u0006\u0010\u0017\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020 H\u0002J\u0010\u0010!\u001a\u00020\u00142\u0006\u0010\u0017\u001a\u00020\u0004H\u0016J\u001c\u0010\"\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\r2\u0006\u0010\u0017\u001a\u00020\u0004H\u0002J$\u0010#\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\r2\u0006\u0010\u0017\u001a\u00020\u00042\u0006\u0010$\u001a\u00020 H\u0002R\u001a\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;",
        "Lcom/squareup/crm/groups/edit/EditGroupWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "",
        "Lcom/squareup/crm/groups/edit/EditGroupState;",
        "Lcom/squareup/crm/groups/edit/EditGroupOutput;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "rolodex",
        "Lcom/squareup/crm/RolodexServiceHelper;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/util/Res;)V",
        "close",
        "Lcom/squareup/workflow/WorkflowAction;",
        "completeSave",
        "group",
        "Lcom/squareup/protos/client/rolodex/Group;",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/crm/groups/edit/EditGroupState;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "(Lkotlin/Unit;Lcom/squareup/crm/groups/edit/EditGroupState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/V2Screen;",
        "saveGroup",
        "Lcom/squareup/workflow/Worker;",
        "Lcom/squareup/crm/groups/edit/EditGroupAction;",
        "showError",
        "message",
        "",
        "snapshotState",
        "startSave",
        "updateName",
        "name",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final close:Lcom/squareup/workflow/WorkflowAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/crm/groups/edit/EditGroupState;",
            "Lcom/squareup/crm/groups/edit/EditGroupOutput;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final rolodex:Lcom/squareup/crm/RolodexServiceHelper;


# direct methods
.method public constructor <init>(Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "rolodex"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    iput-object p2, p0, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;->res:Lcom/squareup/util/Res;

    .line 109
    sget-object p1, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$close$1;->INSTANCE:Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$close$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    const/4 p2, 0x0

    const/4 v0, 0x1

    invoke-static {p0, p2, p1, v0, p2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;->close:Lcom/squareup/workflow/WorkflowAction;

    return-void
.end method

.method public static final synthetic access$completeSave(Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;Lcom/squareup/protos/client/rolodex/Group;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 27
    invoke-direct {p0, p1}, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;->completeSave(Lcom/squareup/protos/client/rolodex/Group;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getClose$p(Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;->close:Lcom/squareup/workflow/WorkflowAction;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;)Lcom/squareup/util/Res;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$showError(Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;Lcom/squareup/crm/groups/edit/EditGroupState;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 27
    invoke-direct {p0, p1, p2}, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;->showError(Lcom/squareup/crm/groups/edit/EditGroupState;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$startSave(Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;Lcom/squareup/crm/groups/edit/EditGroupState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 27
    invoke-direct {p0, p1}, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;->startSave(Lcom/squareup/crm/groups/edit/EditGroupState;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$updateName(Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;Lcom/squareup/crm/groups/edit/EditGroupState;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 27
    invoke-direct {p0, p1, p2}, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;->updateName(Lcom/squareup/crm/groups/edit/EditGroupState;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private final completeSave(Lcom/squareup/protos/client/rolodex/Group;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/crm/groups/edit/EditGroupState;",
            "Lcom/squareup/crm/groups/edit/EditGroupOutput;",
            ">;"
        }
    .end annotation

    .line 100
    new-instance v0, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$completeSave$1;

    invoke-direct {v0, p1}, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$completeSave$1;-><init>(Lcom/squareup/protos/client/rolodex/Group;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final saveGroup(Lcom/squareup/crm/groups/edit/EditGroupState;)Lcom/squareup/workflow/Worker;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/crm/groups/edit/EditGroupState;",
            ")",
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/crm/groups/edit/EditGroupState;",
            "Lcom/squareup/crm/groups/edit/EditGroupOutput;",
            ">;>;"
        }
    .end annotation

    .line 69
    new-instance v0, Lcom/squareup/protos/client/rolodex/Group$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/Group$Builder;-><init>()V

    .line 70
    sget-object v1, Lcom/squareup/protos/client/rolodex/GroupType;->MANUAL_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/Group$Builder;->group_type(Lcom/squareup/protos/client/rolodex/GroupType;)Lcom/squareup/protos/client/rolodex/Group$Builder;

    move-result-object v0

    .line 71
    invoke-virtual {p1}, Lcom/squareup/crm/groups/edit/EditGroupState;->getGroupName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/Group$Builder;->display_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Group$Builder;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/Group$Builder;->build()Lcom/squareup/protos/client/rolodex/Group;

    move-result-object v0

    .line 74
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    .line 76
    iget-object v2, p0, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    const-string v3, "group"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v3, "uuid"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2, v0, v1}, Lcom/squareup/crm/RolodexServiceHelper;->upsertManualGroup(Lcom/squareup/protos/client/rolodex/Group;Ljava/util/UUID;)Lio/reactivex/Single;

    move-result-object v0

    .line 77
    new-instance v1, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$saveGroup$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$saveGroup$1;-><init>(Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;Lcom/squareup/crm/groups/edit/EditGroupState;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "rolodex.upsertManualGrou\u2026  )\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$saveGroup$$inlined$asWorker$1;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$saveGroup$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 127
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 128
    const-class v0, Lcom/squareup/workflow/WorkflowAction;

    sget-object v1, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v2, Lcom/squareup/crm/groups/edit/EditGroupState;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v3, Lcom/squareup/crm/groups/edit/EditGroupOutput;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v3

    invoke-virtual {v2, v3}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v1, Lcom/squareup/workflow/Worker;

    return-object v1
.end method

.method private final showError(Lcom/squareup/crm/groups/edit/EditGroupState;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/crm/groups/edit/EditGroupState;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/crm/groups/edit/EditGroupState;",
            "Lcom/squareup/crm/groups/edit/EditGroupOutput;",
            ">;"
        }
    .end annotation

    .line 104
    new-instance p2, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$showError$1;

    invoke-direct {p2, p1}, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$showError$1;-><init>(Lcom/squareup/crm/groups/edit/EditGroupState;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 v0, 0x1

    invoke-static {p0, p1, p2, v0, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final startSave(Lcom/squareup/crm/groups/edit/EditGroupState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/crm/groups/edit/EditGroupState;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/crm/groups/edit/EditGroupState;",
            "Lcom/squareup/crm/groups/edit/EditGroupOutput;",
            ">;"
        }
    .end annotation

    .line 96
    new-instance v0, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$startSave$1;

    invoke-direct {v0, p1}, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$startSave$1;-><init>(Lcom/squareup/crm/groups/edit/EditGroupState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final updateName(Lcom/squareup/crm/groups/edit/EditGroupState;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/crm/groups/edit/EditGroupState;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/crm/groups/edit/EditGroupState;",
            "Lcom/squareup/crm/groups/edit/EditGroupOutput;",
            ">;"
        }
    .end annotation

    .line 92
    new-instance v0, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$updateName$1;

    invoke-direct {v0, p1, p2}, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$updateName$1;-><init>(Lcom/squareup/crm/groups/edit/EditGroupState;Ljava/lang/String;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 p2, 0x1

    invoke-static {p0, p1, v0, p2, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/crm/groups/edit/EditGroupState;
    .locals 3

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    const/4 v0, 0x0

    if-eqz p2, :cond_4

    .line 114
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p2}, Lokio/ByteString;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, v0

    :goto_1
    if-eqz p2, :cond_3

    .line 119
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    const-string v2, "Parcel.obtain()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 121
    array-length v2, p2

    invoke-virtual {v1, p2, p1, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 122
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 123
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v1, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p2

    if-nez p2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string v2, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    goto :goto_2

    :cond_3
    move-object p2, v0

    .line 125
    :goto_2
    check-cast p2, Lcom/squareup/crm/groups/edit/EditGroupState;

    if-eqz p2, :cond_4

    goto :goto_3

    .line 41
    :cond_4
    new-instance p2, Lcom/squareup/crm/groups/edit/EditGroupState;

    const/4 v1, 0x3

    invoke-direct {p2, v0, p1, v1, v0}, Lcom/squareup/crm/groups/edit/EditGroupState;-><init>(Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :goto_3
    return-object p2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;->initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/crm/groups/edit/EditGroupState;

    move-result-object p1

    return-object p1
.end method

.method public render(Lkotlin/Unit;Lcom/squareup/crm/groups/edit/EditGroupState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/V2Screen;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            "Lcom/squareup/crm/groups/edit/EditGroupState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/crm/groups/edit/EditGroupState;",
            "-",
            "Lcom/squareup/crm/groups/edit/EditGroupOutput;",
            ">;)",
            "Lcom/squareup/workflow/legacy/V2Screen;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-virtual {p2}, Lcom/squareup/crm/groups/edit/EditGroupState;->getSaving()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 52
    invoke-direct {p0, p2}, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;->saveGroup(Lcom/squareup/crm/groups/edit/EditGroupState;)Lcom/squareup/workflow/Worker;

    move-result-object v1

    const/4 v2, 0x0

    sget-object p1, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$render$1;->INSTANCE:Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$render$1;

    move-object v3, p1

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 57
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/crm/groups/edit/EditGroupState;->getGroupName()Ljava/lang/String;

    move-result-object p1

    .line 59
    new-instance v0, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$render$2;

    invoke-direct {v0, p0, p2}, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$render$2;-><init>(Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;Lcom/squareup/crm/groups/edit/EditGroupState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const-string v1, "groupName"

    .line 56
    invoke-static {p3, p1, v1, v0}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v3

    .line 61
    invoke-virtual {p2}, Lcom/squareup/crm/groups/edit/EditGroupState;->getSaving()Z

    move-result v4

    .line 62
    invoke-virtual {p2}, Lcom/squareup/crm/groups/edit/EditGroupState;->getSaving()Z

    move-result p1

    const/4 v0, 0x1

    if-nez p1, :cond_1

    invoke-virtual {p2}, Lcom/squareup/crm/groups/edit/EditGroupState;->getGroupName()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/2addr p1, v0

    if-eqz p1, :cond_1

    const/4 v5, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    const/4 v5, 0x0

    .line 63
    :goto_0
    new-instance p1, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$render$3;

    invoke-direct {p1, p0, p3}, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$render$3;-><init>(Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;Lcom/squareup/workflow/RenderContext;)V

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function0;

    .line 64
    new-instance p1, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$render$4;

    invoke-direct {p1, p0, p3, p2}, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow$render$4;-><init>(Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/crm/groups/edit/EditGroupState;)V

    move-object v7, p1

    check-cast v7, Lkotlin/jvm/functions/Function0;

    .line 55
    new-instance p1, Lcom/squareup/crm/groups/edit/EditGroupRendering;

    move-object v2, p1

    invoke-direct/range {v2 .. v7}, Lcom/squareup/crm/groups/edit/EditGroupRendering;-><init>(Lcom/squareup/workflow/text/WorkflowEditableText;ZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Lkotlin/Unit;

    check-cast p2, Lcom/squareup/crm/groups/edit/EditGroupState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;->render(Lkotlin/Unit;Lcom/squareup/crm/groups/edit/EditGroupState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/V2Screen;

    move-result-object p1

    return-object p1
.end method

.method public snapshotState(Lcom/squareup/crm/groups/edit/EditGroupState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 27
    check-cast p1, Lcom/squareup/crm/groups/edit/EditGroupState;

    invoke-virtual {p0, p1}, Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;->snapshotState(Lcom/squareup/crm/groups/edit/EditGroupState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
