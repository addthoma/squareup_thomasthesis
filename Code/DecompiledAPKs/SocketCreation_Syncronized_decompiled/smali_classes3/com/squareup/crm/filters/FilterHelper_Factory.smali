.class public final Lcom/squareup/crm/filters/FilterHelper_Factory;
.super Ljava/lang/Object;
.source "FilterHelper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/crm/filters/FilterHelper;",
        ">;"
    }
.end annotation


# instance fields
.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final groupLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexGroupLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexGroupLoader;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/crm/filters/FilterHelper_Factory;->resProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/crm/filters/FilterHelper_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/crm/filters/FilterHelper_Factory;->groupLoaderProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/crm/filters/FilterHelper_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexGroupLoader;",
            ">;)",
            "Lcom/squareup/crm/filters/FilterHelper_Factory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/crm/filters/FilterHelper_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/crm/filters/FilterHelper_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/crm/RolodexGroupLoader;)Lcom/squareup/crm/filters/FilterHelper;
    .locals 1

    .line 44
    new-instance v0, Lcom/squareup/crm/filters/FilterHelper;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/crm/filters/FilterHelper;-><init>(Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/crm/RolodexGroupLoader;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/crm/filters/FilterHelper;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/crm/filters/FilterHelper_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/crm/filters/FilterHelper_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/Features;

    iget-object v2, p0, Lcom/squareup/crm/filters/FilterHelper_Factory;->groupLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/crm/RolodexGroupLoader;

    invoke-static {v0, v1, v2}, Lcom/squareup/crm/filters/FilterHelper_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/crm/RolodexGroupLoader;)Lcom/squareup/crm/filters/FilterHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/crm/filters/FilterHelper_Factory;->get()Lcom/squareup/crm/filters/FilterHelper;

    move-result-object v0

    return-object v0
.end method
