.class public final Lcom/squareup/crm/RealEmailCollectionSettings_Factory;
.super Ljava/lang/Object;
.source "RealEmailCollectionSettings_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/crm/RealEmailCollectionSettings;",
        ">;"
    }
.end annotation


# instance fields
.field private final emailCollectionEnabledProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/EmailCollectionEnabledSetting;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/EmailCollectionEnabledSetting;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/crm/RealEmailCollectionSettings_Factory;->emailCollectionEnabledProvider:Ljavax/inject/Provider;

    .line 21
    iput-object p2, p0, Lcom/squareup/crm/RealEmailCollectionSettings_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/crm/RealEmailCollectionSettings_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/EmailCollectionEnabledSetting;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/crm/RealEmailCollectionSettings_Factory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/crm/RealEmailCollectionSettings_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/crm/RealEmailCollectionSettings_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljava/lang/Object;Lcom/squareup/settings/server/Features;)Lcom/squareup/crm/RealEmailCollectionSettings;
    .locals 1

    .line 37
    new-instance v0, Lcom/squareup/crm/RealEmailCollectionSettings;

    check-cast p0, Lcom/squareup/crm/EmailCollectionEnabledSetting;

    invoke-direct {v0, p0, p1}, Lcom/squareup/crm/RealEmailCollectionSettings;-><init>(Lcom/squareup/crm/EmailCollectionEnabledSetting;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/crm/RealEmailCollectionSettings;
    .locals 2

    .line 26
    iget-object v0, p0, Lcom/squareup/crm/RealEmailCollectionSettings_Factory;->emailCollectionEnabledProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/crm/RealEmailCollectionSettings_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1}, Lcom/squareup/crm/RealEmailCollectionSettings_Factory;->newInstance(Ljava/lang/Object;Lcom/squareup/settings/server/Features;)Lcom/squareup/crm/RealEmailCollectionSettings;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/crm/RealEmailCollectionSettings_Factory;->get()Lcom/squareup/crm/RealEmailCollectionSettings;

    move-result-object v0

    return-object v0
.end method
