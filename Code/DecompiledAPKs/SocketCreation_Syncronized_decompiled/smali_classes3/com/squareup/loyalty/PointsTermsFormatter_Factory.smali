.class public final Lcom/squareup/loyalty/PointsTermsFormatter_Factory;
.super Ljava/lang/Object;
.source "PointsTermsFormatter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/loyalty/PointsTermsFormatter;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/loyalty/PointsTermsFormatter_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 23
    iput-object p2, p0, Lcom/squareup/loyalty/PointsTermsFormatter_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 24
    iput-object p3, p0, Lcom/squareup/loyalty/PointsTermsFormatter_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/loyalty/PointsTermsFormatter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/loyalty/PointsTermsFormatter_Factory;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/loyalty/PointsTermsFormatter_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/loyalty/PointsTermsFormatter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/loyalty/LoyaltySettings;Ljavax/inject/Provider;Lcom/squareup/util/Res;)Lcom/squareup/loyalty/PointsTermsFormatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")",
            "Lcom/squareup/loyalty/PointsTermsFormatter;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/loyalty/PointsTermsFormatter;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/loyalty/PointsTermsFormatter;-><init>(Lcom/squareup/loyalty/LoyaltySettings;Ljavax/inject/Provider;Lcom/squareup/util/Res;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/loyalty/PointsTermsFormatter;
    .locals 3

    .line 29
    iget-object v0, p0, Lcom/squareup/loyalty/PointsTermsFormatter_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/loyalty/LoyaltySettings;

    iget-object v1, p0, Lcom/squareup/loyalty/PointsTermsFormatter_Factory;->arg1Provider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/squareup/loyalty/PointsTermsFormatter_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Res;

    invoke-static {v0, v1, v2}, Lcom/squareup/loyalty/PointsTermsFormatter_Factory;->newInstance(Lcom/squareup/loyalty/LoyaltySettings;Ljavax/inject/Provider;Lcom/squareup/util/Res;)Lcom/squareup/loyalty/PointsTermsFormatter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/loyalty/PointsTermsFormatter_Factory;->get()Lcom/squareup/loyalty/PointsTermsFormatter;

    move-result-object v0

    return-object v0
.end method
