.class final Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withCustomer$4;
.super Ljava/lang/Object;
.source "LoyaltyPointsRowSubtitleRenderer.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;->getSubtitle(Lcom/squareup/payment/Transaction;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLoyaltyPointsRowSubtitleRenderer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LoyaltyPointsRowSubtitleRenderer.kt\ncom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withCustomer$4\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,91:1\n1550#2,3:92\n*E\n*S KotlinDebug\n*F\n+ 1 LoyaltyPointsRowSubtitleRenderer.kt\ncom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withCustomer$4\n*L\n50#1,3:92\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "pointsResponse",
        "Lcom/squareup/loyalty/LoyaltyStatusResponse;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;


# direct methods
.method constructor <init>(Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withCustomer$4;->this$0:Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/loyalty/LoyaltyStatusResponse;)I
    .locals 8

    .line 44
    sget-object v0, Lcom/squareup/loyalty/LoyaltyStatusResponse$ErrorRetrievingLoyalty;->INSTANCE:Lcom/squareup/loyalty/LoyaltyStatusResponse$ErrorRetrievingLoyalty;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 46
    :cond_0
    instance-of v0, p1, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;

    const/4 v2, 0x1

    if-eqz v0, :cond_7

    .line 47
    check-cast p1, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;

    invoke-virtual {p1}, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->getCurrentPoints()I

    move-result v0

    .line 48
    invoke-virtual {p1}, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->getCoupons()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/2addr p1, v2

    if-nez p1, :cond_6

    .line 49
    iget-object p1, p0, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withCustomer$4;->this$0:Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;

    invoke-static {p1}, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;->access$getLoyaltySettings$p(Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;)Lcom/squareup/loyalty/LoyaltySettings;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/loyalty/LoyaltySettings;->rewardTiers()Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_5

    check-cast p1, Ljava/lang/Iterable;

    .line 92
    instance-of v3, p1, Ljava/util/Collection;

    if-eqz v3, :cond_2

    move-object v3, p1

    check-cast v3, Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    const/4 p1, 0x0

    goto :goto_1

    .line 93
    :cond_2
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/RewardTier;

    .line 50
    iget-object v3, v3, Lcom/squareup/server/account/protos/RewardTier;->points:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    int-to-long v5, v0

    cmp-long v7, v3, v5

    if-gtz v7, :cond_4

    const/4 v3, 0x1

    goto :goto_0

    :cond_4
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_3

    const/4 p1, 0x1

    :goto_1
    if-ne p1, v2, :cond_5

    goto :goto_2

    :cond_5
    return v1

    .line 53
    :cond_6
    :goto_2
    sget p1, Lcom/squareup/loyaltycalculator/R$string;->loyalty_row_rewards_available:I

    return p1

    .line 60
    :cond_7
    instance-of v0, p1, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$NoLoyalty;

    if-eqz v0, :cond_9

    .line 61
    check-cast p1, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$NoLoyalty;

    invoke-virtual {p1}, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$NoLoyalty;->getCoupons()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/2addr p1, v2

    if-eqz p1, :cond_8

    .line 63
    sget p1, Lcom/squareup/loyaltycalculator/R$string;->loyalty_row_coupons_available:I

    return p1

    .line 66
    :cond_8
    sget p1, Lcom/squareup/loyaltycalculator/R$string;->loyalty_row_enroll_to_claim:I

    return p1

    .line 61
    :cond_9
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 12
    check-cast p1, Lcom/squareup/loyalty/LoyaltyStatusResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withCustomer$4;->call(Lcom/squareup/loyalty/LoyaltyStatusResponse;)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method
