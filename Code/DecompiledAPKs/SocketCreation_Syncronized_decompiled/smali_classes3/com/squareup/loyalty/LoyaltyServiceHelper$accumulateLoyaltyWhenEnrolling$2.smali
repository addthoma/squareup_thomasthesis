.class final Lcom/squareup/loyalty/LoyaltyServiceHelper$accumulateLoyaltyWhenEnrolling$2;
.super Ljava/lang/Object;
.source "LoyaltyServiceHelper.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyalty/LoyaltyServiceHelper;->accumulateLoyaltyWhenEnrolling(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/loyalty/AccumulateLoyaltyStatusResult;",
        "received",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $loyaltyEvent:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;


# direct methods
.method constructor <init>(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper$accumulateLoyaltyWhenEnrolling$2;->$loyaltyEvent:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/loyalty/AccumulateLoyaltyStatusResult;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse;",
            ">;)",
            "Lcom/squareup/loyalty/AccumulateLoyaltyStatusResult;"
        }
    .end annotation

    const-string v0, "received"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 196
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse;

    iget-object v0, v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse;->error:Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;

    sget-object v1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;->ACCUMULATION_ERROR_INVALID_PHONE:Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;

    if-eq v0, v1, :cond_0

    .line 200
    new-instance v0, Lcom/squareup/loyalty/AccumulateLoyaltyStatusResult$ShowRewardProgress;

    .line 201
    sget-object v1, Lcom/squareup/loyalty/LoyaltyServiceHelper;->Companion:Lcom/squareup/loyalty/LoyaltyServiceHelper$Companion;

    iget-object v2, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper$accumulateLoyaltyWhenEnrolling$2;->$loyaltyEvent:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/loyalty/LoyaltyServiceHelper$Companion;->convertEnrollableToEnrolled(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse;)Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    move-result-object v1

    .line 202
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse;

    .line 200
    invoke-direct {v0, v1, p1}, Lcom/squareup/loyalty/AccumulateLoyaltyStatusResult$ShowRewardProgress;-><init>(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse;)V

    check-cast v0, Lcom/squareup/loyalty/AccumulateLoyaltyStatusResult;

    return-object v0

    .line 205
    :cond_0
    new-instance p1, Lcom/squareup/loyalty/AccumulateLoyaltyStatusResult$ShowAllDone;

    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper$accumulateLoyaltyWhenEnrolling$2;->$loyaltyEvent:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    invoke-direct {p1, v0}, Lcom/squareup/loyalty/AccumulateLoyaltyStatusResult$ShowAllDone;-><init>(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V

    check-cast p1, Lcom/squareup/loyalty/AccumulateLoyaltyStatusResult;

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 81
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/loyalty/LoyaltyServiceHelper$accumulateLoyaltyWhenEnrolling$2;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/loyalty/AccumulateLoyaltyStatusResult;

    move-result-object p1

    return-object p1
.end method
