.class public final Lcom/squareup/loyalty/CouponsServiceHelper;
.super Ljava/lang/Object;
.source "CouponsServiceHelper.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loyalty/CouponsServiceHelper$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u000c2\u00020\u0001:\u0001\u000cB\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u0008\u0010\u0008\u001a\u0004\u0018\u00010\tH\u0007J\u001a\u0010\n\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00070\u000b0\u00062\u0006\u0010\u0008\u001a\u00020\tR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/loyalty/CouponsServiceHelper;",
        "",
        "coupons",
        "Lcom/squareup/server/coupons/CouponsService;",
        "(Lcom/squareup/server/coupons/CouponsService;)V",
        "lookup",
        "Lio/reactivex/Single;",
        "Lcom/squareup/protos/client/coupons/LookupCouponsResponse;",
        "query",
        "",
        "lookupV2",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/loyalty/CouponsServiceHelper$Companion;

.field private static final EMPTY_LOOKUP_COUPONS_RESPONSE:Lcom/squareup/protos/client/coupons/LookupCouponsResponse;


# instance fields
.field private final coupons:Lcom/squareup/server/coupons/CouponsService;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/loyalty/CouponsServiceHelper$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/loyalty/CouponsServiceHelper$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/loyalty/CouponsServiceHelper;->Companion:Lcom/squareup/loyalty/CouponsServiceHelper$Companion;

    .line 60
    new-instance v0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$Builder;-><init>()V

    .line 61
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$Builder;->coupon(Ljava/util/List;)Lcom/squareup/protos/client/coupons/LookupCouponsResponse$Builder;

    move-result-object v0

    .line 62
    invoke-virtual {v0}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$Builder;->build()Lcom/squareup/protos/client/coupons/LookupCouponsResponse;

    move-result-object v0

    sput-object v0, Lcom/squareup/loyalty/CouponsServiceHelper;->EMPTY_LOOKUP_COUPONS_RESPONSE:Lcom/squareup/protos/client/coupons/LookupCouponsResponse;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/coupons/CouponsService;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "coupons"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/loyalty/CouponsServiceHelper;->coupons:Lcom/squareup/server/coupons/CouponsService;

    return-void
.end method

.method public static final synthetic access$getEMPTY_LOOKUP_COUPONS_RESPONSE$cp()Lcom/squareup/protos/client/coupons/LookupCouponsResponse;
    .locals 1

    .line 20
    sget-object v0, Lcom/squareup/loyalty/CouponsServiceHelper;->EMPTY_LOOKUP_COUPONS_RESPONSE:Lcom/squareup/protos/client/coupons/LookupCouponsResponse;

    return-object v0
.end method


# virtual methods
.method public final lookup(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/protos/client/coupons/LookupCouponsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "LOYALTY-3530 Use version that is more workflow compatible"
    .end annotation

    .line 27
    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28
    sget-object p1, Lcom/squareup/loyalty/CouponsServiceHelper;->EMPTY_LOOKUP_COUPONS_RESPONSE:Lcom/squareup/protos/client/coupons/LookupCouponsResponse;

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "just(EMPTY_LOOKUP_COUPONS_RESPONSE)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 31
    :cond_0
    new-instance v0, Lcom/squareup/protos/client/coupons/LookupCouponsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/coupons/LookupCouponsRequest$Builder;-><init>()V

    .line 32
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/coupons/LookupCouponsRequest$Builder;->query(Ljava/lang/String;)Lcom/squareup/protos/client/coupons/LookupCouponsRequest$Builder;

    move-result-object p1

    .line 33
    invoke-virtual {p1}, Lcom/squareup/protos/client/coupons/LookupCouponsRequest$Builder;->build()Lcom/squareup/protos/client/coupons/LookupCouponsRequest;

    move-result-object p1

    .line 35
    iget-object v0, p0, Lcom/squareup/loyalty/CouponsServiceHelper;->coupons:Lcom/squareup/server/coupons/CouponsService;

    invoke-interface {v0, p1}, Lcom/squareup/server/coupons/CouponsService;->lookup(Lcom/squareup/protos/client/coupons/LookupCouponsRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    .line 36
    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 37
    sget-object v0, Lcom/squareup/loyalty/CouponsServiceHelper$lookup$1;->INSTANCE:Lcom/squareup/loyalty/CouponsServiceHelper$lookup$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 38
    sget-object v0, Lcom/squareup/loyalty/CouponsServiceHelper$lookup$2;->INSTANCE:Lcom/squareup/loyalty/CouponsServiceHelper$lookup$2;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "coupons.lookup(request)\n\u2026NSE\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final lookupV2(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/coupons/LookupCouponsResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "query"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    new-instance v0, Lcom/squareup/protos/client/coupons/LookupCouponsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/coupons/LookupCouponsRequest$Builder;-><init>()V

    .line 51
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/coupons/LookupCouponsRequest$Builder;->query(Ljava/lang/String;)Lcom/squareup/protos/client/coupons/LookupCouponsRequest$Builder;

    move-result-object p1

    .line 52
    invoke-virtual {p1}, Lcom/squareup/protos/client/coupons/LookupCouponsRequest$Builder;->build()Lcom/squareup/protos/client/coupons/LookupCouponsRequest;

    move-result-object p1

    .line 54
    iget-object v0, p0, Lcom/squareup/loyalty/CouponsServiceHelper;->coupons:Lcom/squareup/server/coupons/CouponsService;

    invoke-interface {v0, p1}, Lcom/squareup/server/coupons/CouponsService;->lookup(Lcom/squareup/protos/client/coupons/LookupCouponsRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    .line 55
    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
