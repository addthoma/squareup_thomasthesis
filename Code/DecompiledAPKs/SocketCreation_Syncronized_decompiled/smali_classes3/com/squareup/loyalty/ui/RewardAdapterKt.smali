.class public final Lcom/squareup/loyalty/ui/RewardAdapterKt;
.super Ljava/lang/Object;
.source "RewardAdapter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0012\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001*\u00020\u0003\u001a\u0012\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00030\u0003*\u00020\u0001\u00a8\u0006\u0004"
    }
    d2 = {
        "toRewardTier",
        "Lcom/squareup/server/account/protos/RewardTier;",
        "kotlin.jvm.PlatformType",
        "Lcom/squareup/protos/client/loyalty/RewardTier;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final toRewardTier(Lcom/squareup/server/account/protos/RewardTier;)Lcom/squareup/protos/client/loyalty/RewardTier;
    .locals 2

    const-string v0, "$this$toRewardTier"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    new-instance v0, Lcom/squareup/protos/client/loyalty/RewardTier$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/RewardTier$Builder;-><init>()V

    .line 41
    iget-object v1, p0, Lcom/squareup/server/account/protos/RewardTier;->coupon_definition_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/loyalty/RewardTier$Builder;->coupon_definition_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/RewardTier$Builder;

    move-result-object v0

    .line 42
    iget-object v1, p0, Lcom/squareup/server/account/protos/RewardTier;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/loyalty/RewardTier$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/RewardTier$Builder;

    move-result-object v0

    .line 43
    iget-object p0, p0, Lcom/squareup/server/account/protos/RewardTier;->points:Ljava/lang/Long;

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/loyalty/RewardTier$Builder;->points(Ljava/lang/Long;)Lcom/squareup/protos/client/loyalty/RewardTier$Builder;

    move-result-object p0

    .line 44
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/RewardTier$Builder;->build()Lcom/squareup/protos/client/loyalty/RewardTier;

    move-result-object p0

    return-object p0
.end method

.method public static final toRewardTier(Lcom/squareup/protos/client/loyalty/RewardTier;)Lcom/squareup/server/account/protos/RewardTier;
    .locals 2

    const-string v0, "$this$toRewardTier"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    new-instance v0, Lcom/squareup/server/account/protos/RewardTier$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/RewardTier$Builder;-><init>()V

    .line 51
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RewardTier;->coupon_definition_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/RewardTier$Builder;->coupon_definition_token(Ljava/lang/String;)Lcom/squareup/server/account/protos/RewardTier$Builder;

    move-result-object v0

    .line 52
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RewardTier;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/RewardTier$Builder;->name(Ljava/lang/String;)Lcom/squareup/server/account/protos/RewardTier$Builder;

    move-result-object v0

    .line 53
    iget-object p0, p0, Lcom/squareup/protos/client/loyalty/RewardTier;->points:Ljava/lang/Long;

    invoke-virtual {v0, p0}, Lcom/squareup/server/account/protos/RewardTier$Builder;->points(Ljava/lang/Long;)Lcom/squareup/server/account/protos/RewardTier$Builder;

    move-result-object p0

    .line 54
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/RewardTier$Builder;->build()Lcom/squareup/server/account/protos/RewardTier;

    move-result-object p0

    return-object p0
.end method
