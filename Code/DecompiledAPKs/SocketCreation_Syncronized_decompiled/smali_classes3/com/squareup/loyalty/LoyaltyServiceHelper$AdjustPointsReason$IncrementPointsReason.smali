.class public abstract Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$IncrementPointsReason;
.super Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;
.source "LoyaltyServiceHelper.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "IncrementPointsReason"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$IncrementPointsReason$ComplimentaryPoints;,
        Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$IncrementPointsReason$MissedPointsInTransaction;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\t\nB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0002\u000b\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$IncrementPointsReason;",
        "Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;",
        "proto",
        "Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;",
        "(Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;)V",
        "getProto",
        "()Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;",
        "toProtoId",
        "",
        "ComplimentaryPoints",
        "MissedPointsInTransaction",
        "Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$IncrementPointsReason$ComplimentaryPoints;",
        "Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$IncrementPointsReason$MissedPointsInTransaction;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final proto:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;


# direct methods
.method private constructor <init>(Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;)V
    .locals 1

    const/4 v0, 0x0

    .line 115
    invoke-direct {p0, v0}, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$IncrementPointsReason;->proto:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 113
    invoke-direct {p0, p1}, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$IncrementPointsReason;-><init>(Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;)V

    return-void
.end method


# virtual methods
.method public final getProto()Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$IncrementPointsReason;->proto:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    return-object v0
.end method

.method public toProtoId()I
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$IncrementPointsReason;->proto:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    invoke-virtual {v0}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;->getValue()I

    move-result v0

    return v0
.end method
