.class public final Lcom/squareup/loyalty/LoyaltyServiceHelper$Companion;
.super Ljava/lang/Object;
.source "LoyaltyServiceHelper.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loyalty/LoyaltyServiceHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLoyaltyServiceHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LoyaltyServiceHelper.kt\ncom/squareup/loyalty/LoyaltyServiceHelper$Companion\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,587:1\n1577#2,4:588\n*E\n*S KotlinDebug\n*F\n+ 1 LoyaltyServiceHelper.kt\ncom/squareup/loyalty/LoyaltyServiceHelper$Companion\n*L\n562#1,4:588\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u001e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00042\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007H\u0007J\u0019\u0010\t\u001a\u0004\u0018\u00010\u00042\u0008\u0010\n\u001a\u0004\u0018\u00010\u000bH\u0007\u00a2\u0006\u0002\u0010\u000cJ\u0012\u0010\r\u001a\u00020\u000e2\u0008\u0010\n\u001a\u0004\u0018\u00010\u000bH\u0007J\u0014\u0010\u000f\u001a\u00020\u0010*\u00020\u00102\u0006\u0010\n\u001a\u00020\u0011H\u0007\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/loyalty/LoyaltyServiceHelper$Companion;",
        "",
        "()V",
        "getAvailableRewards",
        "",
        "points",
        "tiers",
        "",
        "Lcom/squareup/server/account/protos/RewardTier;",
        "getPointsFromGetLoyaltyStatus",
        "response",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;",
        "(Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;)Ljava/lang/Integer;",
        "hasLoyaltyAccount",
        "",
        "convertEnrollableToEnrolled",
        "Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;",
        "Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 537
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 537
    invoke-direct {p0}, Lcom/squareup/loyalty/LoyaltyServiceHelper$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final convertEnrollableToEnrolled(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse;)Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;
    .locals 12
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "$this$convertEnrollableToEnrolled"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "response"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 572
    sget-object v1, Lcom/squareup/loyalty/MaybeLoyaltyEvent;->Companion:Lcom/squareup/loyalty/MaybeLoyaltyEvent$Companion;

    .line 573
    invoke-virtual {p1}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v2

    .line 574
    invoke-virtual {p1}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->getBillIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    .line 575
    iget-object v4, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->tenderType:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 576
    iget-object v5, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 577
    iget-object v7, p2, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse;->loyalty_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    const-string v0, "response.loyalty_status"

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 579
    sget-object v8, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$EventContext;->ACCUMULATE_LOYALTY_STATUS_RESPONSE:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$EventContext;

    .line 580
    iget-object v9, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->unitToken:Ljava/lang/String;

    .line 581
    iget-object v0, p2, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse;->is_first_star:Ljava/lang/Boolean;

    const-string v6, "response.is_first_star"

    invoke-static {v0, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    .line 582
    iget-object p1, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->tenderType:Lcom/squareup/protos/client/bills/Tender$Type;

    sget-object v0, Lcom/squareup/protos/client/bills/Tender$Type;->CARD:Lcom/squareup/protos/client/bills/Tender$Type;

    if-ne p1, v0, :cond_0

    iget-object p1, p2, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse;->is_first_star:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    const/4 v11, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    const/4 v11, 0x0

    :goto_0
    const/4 v6, 0x1

    .line 572
    invoke-virtual/range {v1 .. v11}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$Companion;->createLoyaltyEventFromLoyaltyStatus(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/protos/client/bills/Cart;ZLcom/squareup/protos/client/loyalty/LoyaltyStatus;Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$EventContext;Ljava/lang/String;ZZ)Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    move-result-object p1

    return-object p1
.end method

.method public final getAvailableRewards(ILjava/util/List;)I
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/RewardTier;",
            ">;)I"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string/jumbo v0, "tiers"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 561
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-le v0, v1, :cond_4

    .line 562
    check-cast p2, Ljava/lang/Iterable;

    .line 588
    instance-of v0, p2, Ljava/util/Collection;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_2

    .line 590
    :cond_0
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    const/4 v0, 0x0

    :cond_1
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/RewardTier;

    int-to-long v4, p1

    .line 562
    iget-object v3, v3, Lcom/squareup/server/account/protos/RewardTier;->points:Ljava/lang/Long;

    const-string v6, "it.points"

    invoke-static {v3, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-ltz v3, :cond_2

    const/4 v3, 0x1

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_1

    add-int/lit8 v0, v0, 0x1

    if-gez v0, :cond_1

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwCountOverflow()V

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_2

    :cond_4
    int-to-long v0, p1

    .line 565
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/server/account/protos/RewardTier;

    iget-object p1, p1, Lcom/squareup/server/account/protos/RewardTier;->points:Ljava/lang/Long;

    const-string/jumbo p2, "tiers[0].points"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    div-long/2addr v0, p1

    long-to-int v2, v0

    :goto_2
    return v2
.end method

.method public final getPointsFromGetLoyaltyStatus(Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;)Ljava/lang/Integer;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    if-eqz p1, :cond_0

    .line 545
    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;->loyalty_status:Ljava/util/List;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;->status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->balance:Ljava/lang/Integer;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public final hasLoyaltyAccount(Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;)Z
    .locals 2
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    .line 549
    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;->loyalty_account:Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;->mappings:Ljava/util/List;

    if-eqz v1, :cond_0

    check-cast v1, Ljava/util/Collection;

    .line 548
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/2addr v1, v0

    if-ne v1, v0, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;->loyalty_status:Ljava/util/List;

    if-eqz p1, :cond_0

    check-cast p1, Ljava/util/Collection;

    .line 549
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/2addr p1, v0

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
