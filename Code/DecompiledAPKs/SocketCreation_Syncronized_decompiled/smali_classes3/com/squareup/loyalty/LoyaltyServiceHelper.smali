.class public Lcom/squareup/loyalty/LoyaltyServiceHelper;
.super Ljava/lang/Object;
.source "LoyaltyServiceHelper.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;,
        Lcom/squareup/loyalty/LoyaltyServiceHelper$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLoyaltyServiceHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LoyaltyServiceHelper.kt\ncom/squareup/loyalty/LoyaltyServiceHelper\n*L\n1#1,587:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0086\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0016\u0018\u0000 b2\u00020\u0001:\u0002abB)\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJR\u0010\u000b\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000e0\r0\u000c2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\n\u0008\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u00142\n\u0008\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u00142\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u00172\n\u0008\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0002J\u0016\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u000c2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J<\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u000c2\u0006\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u00142\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u00142\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u00172\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J4\u0010\u001f\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020 0\r0\u000c2\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020$2\u0006\u0010&\u001a\u00020\'H\u0016JJ\u0010(\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020)0\r0\u000c2\u000c\u0010*\u001a\u0008\u0012\u0004\u0012\u00020\u00140+2\u0008\u0008\u0002\u0010,\u001a\u00020\u00122\u0008\u0008\u0002\u0010-\u001a\u00020\u00122\u0008\u0008\u0002\u0010.\u001a\u00020\u00122\u0008\u0008\u0002\u0010/\u001a\u00020\u0012H\u0017J$\u00100\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002010\r0\u000c2\u0006\u00102\u001a\u0002032\u0006\u00104\u001a\u00020\u0014H\u0016J\u001c\u00105\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002060\r0\u000c2\u0006\u00107\u001a\u00020\u0014H\u0016J\n\u00108\u001a\u0004\u0018\u000109H\u0002J\u001c\u0010:\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020;0\r0\u000c2\u0006\u00107\u001a\u00020\u0014H\u0016J\u001c\u0010<\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020=0\r0\u000c2\u0006\u0010>\u001a\u00020\u0014H\u0016J\u001c\u0010?\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020@0\r0\u000c2\u0006\u0010A\u001a\u00020BH\u0016J\u001e\u0010C\u001a\u0010\u0012\u000c\u0012\n E*\u0004\u0018\u00010D0D0\u000c2\u0006\u0010F\u001a\u00020GH\u0016J\u001c\u0010H\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020I0\r0\u000c2\u0006\u0010J\u001a\u00020\u0014H\u0016J\u0010\u0010K\u001a\u00020L2\u0006\u0010M\u001a\u00020\u001bH\u0002J0\u0010N\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020O0\r0\u000c2\u0006\u0010P\u001a\u00020\u00142\u0006\u0010Q\u001a\u00020\u00142\n\u0008\u0002\u0010R\u001a\u0004\u0018\u00010\u0014H\u0016J\u001c\u0010S\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020T0\r0\u000c2\u0006\u0010U\u001a\u00020\u0014H\u0016J$\u0010V\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020W0\r0\u000c2\u0006\u00107\u001a\u00020\u00142\u0006\u0010X\u001a\u00020\u0014H\u0016J$\u0010Y\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020Z0\r0\u000c2\u0006\u0010[\u001a\u00020\u00142\u0006\u00102\u001a\u000203H\u0016J\"\u0010\\\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020]0\r0\u000c2\u000c\u0010^\u001a\u0008\u0012\u0004\u0012\u00020\u00140+H\u0016J\u0014\u0010_\u001a\u00020`*\u00020`2\u0006\u0010&\u001a\u00020\'H\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006c"
    }
    d2 = {
        "Lcom/squareup/loyalty/LoyaltyServiceHelper;",
        "",
        "loyalty",
        "Lcom/squareup/server/loyalty/LoyaltyService;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "employeeManagement",
        "Lcom/squareup/permissions/EmployeeManagement;",
        "loyaltyAnalytics",
        "Lcom/squareup/loyalty/LoyaltyAnalytics;",
        "(Lcom/squareup/server/loyalty/LoyaltyService;Lio/reactivex/Scheduler;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/loyalty/LoyaltyAnalytics;)V",
        "accumulateLoyalty",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse;",
        "loyaltyEvent",
        "Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;",
        "isEnrolled",
        "",
        "phoneNumber",
        "",
        "phoneId",
        "reasonForPointAccrual",
        "Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;",
        "newlyAcceptedTos",
        "Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;",
        "accumulateLoyaltyWhenAlreadyEnrolled",
        "Lcom/squareup/loyalty/AccumulateLoyaltyStatusResult;",
        "accumulateLoyaltyWhenEnrolling",
        "enteredLoyaltyPhoneNumber",
        "loyaltyPhoneId",
        "adjustPunches",
        "Lcom/squareup/protos/client/loyalty/AdjustPunchesResponse;",
        "customerId",
        "Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;",
        "balanceBeforeAdjustment",
        "",
        "newBalance",
        "reason",
        "Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;",
        "batchGetLoyaltyAccounts",
        "Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;",
        "loyaltyAccountTokens",
        "",
        "includeActiveMappings",
        "includeContact",
        "forceContactCreation",
        "includeRawPhoneNumber",
        "createLoyaltyAccount",
        "Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse;",
        "loyaltyAccountMapping",
        "Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;",
        "contactToken",
        "deleteLoyaltyAccount",
        "Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;",
        "loyaltyAccountToken",
        "generateCreatorDetails",
        "Lcom/squareup/protos/client/CreatorDetails;",
        "getExpiringPoints",
        "Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse;",
        "getLoyaltyAccountFromMapping",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingResponse;",
        "rawPhoneNumber",
        "getLoyaltyReport",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;",
        "range",
        "Lcom/squareup/protos/common/time/DateTimeInterval;",
        "getLoyaltyStatusForContact",
        "Lcom/squareup/loyalty/LoyaltyStatusResponse;",
        "kotlin.jvm.PlatformType",
        "contact",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "getLoyaltyStatusForContactToken",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;",
        "contact_token",
        "logLoyaltyResult",
        "",
        "result",
        "redeemPoints",
        "Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;",
        "couponDefinitionToken",
        "phoneToken",
        "returnCouponToken",
        "returnReward",
        "Lcom/squareup/protos/client/loyalty/ReturnRewardResponse;",
        "couponToken",
        "transferLoyaltyAccount",
        "Lcom/squareup/protos/client/loyalty/TransferLoyaltyAccountResponse;",
        "toContactToken",
        "updateLoyaltyAccountMappings",
        "Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;",
        "loyaltyAccountMappingToken",
        "voidCoupons",
        "Lcom/squareup/protos/client/loyalty/VoidCouponsResponse;",
        "couponTokens",
        "maybeAddReasonCode",
        "Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;",
        "AdjustPointsReason",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/loyalty/LoyaltyServiceHelper$Companion;


# instance fields
.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final loyalty:Lcom/squareup/server/loyalty/LoyaltyService;

.field private final loyaltyAnalytics:Lcom/squareup/loyalty/LoyaltyAnalytics;

.field private final mainScheduler:Lio/reactivex/Scheduler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/loyalty/LoyaltyServiceHelper$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/loyalty/LoyaltyServiceHelper$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/loyalty/LoyaltyServiceHelper;->Companion:Lcom/squareup/loyalty/LoyaltyServiceHelper$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/loyalty/LoyaltyService;Lio/reactivex/Scheduler;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/loyalty/LoyaltyAnalytics;)V
    .locals 1
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "loyalty"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "employeeManagement"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltyAnalytics"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper;->loyalty:Lcom/squareup/server/loyalty/LoyaltyService;

    iput-object p2, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p3, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    iput-object p4, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper;->loyaltyAnalytics:Lcom/squareup/loyalty/LoyaltyAnalytics;

    return-void
.end method

.method public static final synthetic access$logLoyaltyResult(Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/loyalty/AccumulateLoyaltyStatusResult;)V
    .locals 0

    .line 81
    invoke-direct {p0, p1}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->logLoyaltyResult(Lcom/squareup/loyalty/AccumulateLoyaltyStatusResult;)V

    return-void
.end method

.method private final accumulateLoyalty(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;ZLjava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;)Lio/reactivex/Single;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;",
            "Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse;",
            ">;>;"
        }
    .end annotation

    .line 151
    new-instance v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;-><init>()V

    .line 152
    invoke-virtual {p1}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->tender_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    move-result-object v0

    .line 153
    invoke-virtual {p1}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->getBillIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    move-result-object v0

    .line 154
    iget-object v1, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->tenderType:Lcom/squareup/protos/client/bills/Tender$Type;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->tender_type(Lcom/squareup/protos/client/bills/Tender$Type;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    move-result-object v0

    .line 155
    iget-object p1, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    move-result-object p1

    .line 156
    invoke-virtual {p1, p5}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->reason_for_point_accrual(Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    move-result-object p1

    if-eqz p6, :cond_0

    .line 158
    invoke-virtual {p1, p6}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->newly_accepted_tos(Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    :cond_0
    xor-int/lit8 p2, p2, 0x1

    .line 160
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->enroll_intent(Ljava/lang/Boolean;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    if-eqz p3, :cond_1

    .line 162
    invoke-virtual {p1, p3}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->phone_number(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    goto :goto_0

    :cond_1
    if-eqz p4, :cond_2

    .line 163
    invoke-virtual {p1, p4}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->phone_number_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    .line 167
    :goto_0
    iget-object p2, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper;->loyalty:Lcom/squareup/server/loyalty/LoyaltyService;

    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;

    move-result-object p1

    const-string p3, "request.build()"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/server/loyalty/LoyaltyService;->accumulateLoyaltyStatus(Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    .line 168
    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    const-wide/16 v1, 0x2

    .line 170
    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v4, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper;->mainScheduler:Lio/reactivex/Scheduler;

    new-instance p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    sget-object p2, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;->INSTANCE:Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    check-cast p2, Lcom/squareup/receiving/ReceivedResponse;

    invoke-direct {p1, p2}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;-><init>(Lcom/squareup/receiving/ReceivedResponse;)V

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    move-object v5, p1

    check-cast v5, Lio/reactivex/SingleSource;

    .line 169
    invoke-virtual/range {v0 .. v5}, Lio/reactivex/Single;->timeout(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;Lio/reactivex/SingleSource;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "loyalty.accumulateLoyalt\u2026(NetworkError))\n        )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 164
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "A phoneNumber or phoneId is needed for enrollment."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method static synthetic accumulateLoyalty$default(Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;ZLjava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;ILjava/lang/Object;)Lio/reactivex/Single;
    .locals 9

    if-nez p8, :cond_3

    and-int/lit8 v0, p7, 0x4

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 144
    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    move-object v5, v0

    goto :goto_0

    :cond_0
    move-object v5, p3

    :goto_0
    and-int/lit8 v0, p7, 0x8

    if-eqz v0, :cond_1

    .line 145
    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    move-object v6, v0

    goto :goto_1

    :cond_1
    move-object v6, p4

    :goto_1
    and-int/lit8 v0, p7, 0x20

    if-eqz v0, :cond_2

    .line 149
    move-object v0, v1

    check-cast v0, Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    move-object v8, v0

    goto :goto_2

    :cond_2
    move-object v8, p6

    :goto_2
    move-object v2, p0

    move-object v3, p1

    move v4, p2

    move-object v7, p5

    invoke-direct/range {v2 .. v8}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->accumulateLoyalty(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;ZLjava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0

    .line 0
    :cond_3
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Super calls with default arguments not supported in this target, function: accumulateLoyalty"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static synthetic batchGetLoyaltyAccounts$default(Lcom/squareup/loyalty/LoyaltyServiceHelper;Ljava/util/List;ZZZZILjava/lang/Object;)Lio/reactivex/Single;
    .locals 7

    if-nez p7, :cond_4

    and-int/lit8 p7, p6, 0x2

    const/4 v0, 0x1

    if-eqz p7, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    move v3, p2

    :goto_0
    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_1

    const/4 v4, 0x1

    goto :goto_1

    :cond_1
    move v4, p3

    :goto_1
    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_2

    const/4 p4, 0x0

    const/4 v5, 0x0

    goto :goto_2

    :cond_2
    move v5, p4

    :goto_2
    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_3

    const/4 v6, 0x1

    goto :goto_3

    :cond_3
    move v6, p5

    :goto_3
    move-object v1, p0

    move-object v2, p1

    .line 292
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->batchGetLoyaltyAccounts(Ljava/util/List;ZZZZ)Lio/reactivex/Single;

    move-result-object p0

    return-object p0

    .line 0
    :cond_4
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: batchGetLoyaltyAccounts"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final convertEnrollableToEnrolled(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse;)Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/loyalty/LoyaltyServiceHelper;->Companion:Lcom/squareup/loyalty/LoyaltyServiceHelper$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/loyalty/LoyaltyServiceHelper$Companion;->convertEnrollableToEnrolled(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse;)Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    move-result-object p0

    return-object p0
.end method

.method private final generateCreatorDetails()Lcom/squareup/protos/client/CreatorDetails;
    .locals 3

    .line 524
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeToken()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 527
    :cond_0
    new-instance v0, Lcom/squareup/protos/client/CreatorDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/CreatorDetails$Builder;-><init>()V

    .line 529
    new-instance v1, Lcom/squareup/protos/client/Employee$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/Employee$Builder;-><init>()V

    .line 530
    iget-object v2, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v2}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/Employee$Builder;->employee_token(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;

    move-result-object v1

    .line 531
    invoke-virtual {v1}, Lcom/squareup/protos/client/Employee$Builder;->build()Lcom/squareup/protos/client/Employee;

    move-result-object v1

    .line 528
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/CreatorDetails$Builder;->employee(Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/CreatorDetails$Builder;

    move-result-object v0

    .line 533
    invoke-virtual {v0}, Lcom/squareup/protos/client/CreatorDetails$Builder;->build()Lcom/squareup/protos/client/CreatorDetails;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public static final getAvailableRewards(ILjava/util/List;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/RewardTier;",
            ">;)I"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/loyalty/LoyaltyServiceHelper;->Companion:Lcom/squareup/loyalty/LoyaltyServiceHelper$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/loyalty/LoyaltyServiceHelper$Companion;->getAvailableRewards(ILjava/util/List;)I

    move-result p0

    return p0
.end method

.method public static final getPointsFromGetLoyaltyStatus(Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;)Ljava/lang/Integer;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/loyalty/LoyaltyServiceHelper;->Companion:Lcom/squareup/loyalty/LoyaltyServiceHelper$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/loyalty/LoyaltyServiceHelper$Companion;->getPointsFromGetLoyaltyStatus(Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;)Ljava/lang/Integer;

    move-result-object p0

    return-object p0
.end method

.method public static final hasLoyaltyAccount(Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;)Z
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/loyalty/LoyaltyServiceHelper;->Companion:Lcom/squareup/loyalty/LoyaltyServiceHelper$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/loyalty/LoyaltyServiceHelper$Companion;->hasLoyaltyAccount(Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;)Z

    move-result p0

    return p0
.end method

.method private final logLoyaltyResult(Lcom/squareup/loyalty/AccumulateLoyaltyStatusResult;)V
    .locals 1

    .line 509
    instance-of v0, p1, Lcom/squareup/loyalty/AccumulateLoyaltyStatusResult$ShowRewardProgress;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper;->loyaltyAnalytics:Lcom/squareup/loyalty/LoyaltyAnalytics;

    invoke-virtual {p1}, Lcom/squareup/loyalty/AccumulateLoyaltyStatusResult;->getLoyaltyEvent()Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/loyalty/LoyaltyAnalytics;->logActionEnrollSubmitSuccess(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V

    goto :goto_0

    .line 510
    :cond_0
    instance-of v0, p1, Lcom/squareup/loyalty/AccumulateLoyaltyStatusResult$ShowAllDone;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper;->loyaltyAnalytics:Lcom/squareup/loyalty/LoyaltyAnalytics;

    invoke-virtual {p1}, Lcom/squareup/loyalty/AccumulateLoyaltyStatusResult;->getLoyaltyEvent()Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/loyalty/LoyaltyAnalytics;->logActionEnrollSubmitTimeout(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private final maybeAddReasonCode(Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;)Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;
    .locals 1

    .line 518
    instance-of v0, p2, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$IncrementPointsReason;

    if-eqz v0, :cond_0

    check-cast p2, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$IncrementPointsReason;

    invoke-virtual {p2}, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$IncrementPointsReason;->getProto()Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->increment_reason(Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;)Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;

    move-result-object p1

    const-string p2, "increment_reason(reason.proto)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 519
    :cond_0
    instance-of v0, p2, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason;

    if-eqz v0, :cond_1

    check-cast p2, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason;

    invoke-virtual {p2}, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason;->getProto()Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->decrement_reason(Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;)Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;

    move-result-object p1

    const-string p2, "decrement_reason(reason.proto)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public static synthetic redeemPoints$default(Lcom/squareup/loyalty/LoyaltyServiceHelper;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lio/reactivex/Single;
    .locals 0

    if-nez p5, :cond_1

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 247
    check-cast p3, Ljava/lang/String;

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->redeemPoints(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0

    .line 0
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: redeemPoints"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public accumulateLoyaltyWhenAlreadyEnrolled(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)Lio/reactivex/Single;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/loyalty/AccumulateLoyaltyStatusResult;",
            ">;"
        }
    .end annotation

    const-string v0, "loyaltyEvent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 220
    iget-object v5, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->phoneToken:Ljava/lang/String;

    .line 221
    iget-object v6, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->earlyComputedReason:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v1, p0

    move-object v2, p1

    .line 217
    invoke-static/range {v1 .. v9}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->accumulateLoyalty$default(Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;ZLjava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;ILjava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    .line 225
    new-instance v1, Lcom/squareup/loyalty/LoyaltyServiceHelper$accumulateLoyaltyWhenAlreadyEnrolled$1;

    invoke-direct {v1, p1}, Lcom/squareup/loyalty/LoyaltyServiceHelper$accumulateLoyaltyWhenAlreadyEnrolled$1;-><init>(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 231
    new-instance v0, Lcom/squareup/loyalty/LoyaltyServiceHelper$accumulateLoyaltyWhenAlreadyEnrolled$2;

    move-object v1, p0

    check-cast v1, Lcom/squareup/loyalty/LoyaltyServiceHelper;

    invoke-direct {v0, v1}, Lcom/squareup/loyalty/LoyaltyServiceHelper$accumulateLoyaltyWhenAlreadyEnrolled$2;-><init>(Lcom/squareup/loyalty/LoyaltyServiceHelper;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    new-instance v1, Lcom/squareup/loyalty/LoyaltyServiceHelperKt$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v1, v0}, Lcom/squareup/loyalty/LoyaltyServiceHelperKt$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v1}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "accumulateLoyalty(\n     \u2026ccess(::logLoyaltyResult)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public accumulateLoyaltyWhenEnrolling(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;)Lio/reactivex/Single;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;",
            "Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/loyalty/AccumulateLoyaltyStatusResult;",
            ">;"
        }
    .end annotation

    const-string v0, "loyaltyEvent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newlyAcceptedTos"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    .line 185
    invoke-direct/range {v1 .. v7}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->accumulateLoyalty(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;ZLjava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;)Lio/reactivex/Single;

    move-result-object p2

    .line 193
    sget-object p3, Lcom/squareup/loyalty/LoyaltyServiceHelper$accumulateLoyaltyWhenEnrolling$1;->INSTANCE:Lcom/squareup/loyalty/LoyaltyServiceHelper$accumulateLoyaltyWhenEnrolling$1;

    check-cast p3, Lio/reactivex/functions/Function;

    invoke-virtual {p2, p3}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p2

    .line 194
    new-instance p3, Lcom/squareup/loyalty/LoyaltyServiceHelper$accumulateLoyaltyWhenEnrolling$2;

    invoke-direct {p3, p1}, Lcom/squareup/loyalty/LoyaltyServiceHelper$accumulateLoyaltyWhenEnrolling$2;-><init>(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V

    check-cast p3, Lio/reactivex/functions/Function;

    invoke-virtual {p2, p3}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 207
    new-instance p2, Lcom/squareup/loyalty/LoyaltyServiceHelper$accumulateLoyaltyWhenEnrolling$3;

    move-object p3, p0

    check-cast p3, Lcom/squareup/loyalty/LoyaltyServiceHelper;

    invoke-direct {p2, p3}, Lcom/squareup/loyalty/LoyaltyServiceHelper$accumulateLoyaltyWhenEnrolling$3;-><init>(Lcom/squareup/loyalty/LoyaltyServiceHelper;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    new-instance p3, Lcom/squareup/loyalty/LoyaltyServiceHelperKt$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {p3, p2}, Lcom/squareup/loyalty/LoyaltyServiceHelperKt$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast p3, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, p3}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "accumulateLoyalty(\n     \u2026ccess(::logLoyaltyResult)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public adjustPunches(Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;IILcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;",
            "II",
            "Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/loyalty/AdjustPunchesResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "customerId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reason"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sub-int/2addr p3, p2

    .line 96
    new-instance v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;-><init>()V

    .line 97
    invoke-direct {p0}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->generateCreatorDetails()Lcom/squareup/protos/client/CreatorDetails;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;

    move-result-object v0

    .line 98
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->customer_identifier(Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;)Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;

    move-result-object p1

    .line 99
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->adjustment(Ljava/lang/Integer;)Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;

    move-result-object p1

    int-to-long v0, p2

    const-wide/16 v2, 0x0

    .line 102
    invoke-static {v0, v1, v2, v3}, Lkotlin/ranges/RangesKt;->coerceAtLeast(JJ)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->current_stars(Ljava/lang/Long;)Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;

    move-result-object p1

    .line 103
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->balance_before_adjustment(Ljava/lang/Integer;)Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;

    move-result-object p1

    const-string p2, "AdjustPunchesRequest.Bui\u2026(balanceBeforeAdjustment)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    invoke-direct {p0, p1, p4}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->maybeAddReasonCode(Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;)Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;

    move-result-object p1

    .line 105
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;

    move-result-object p1

    .line 107
    iget-object p2, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper;->loyalty:Lcom/squareup/server/loyalty/LoyaltyService;

    const-string p3, "request"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/server/loyalty/LoyaltyService;->adjustPunches(Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 108
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public batchGetLoyaltyAccounts(Ljava/util/List;)Lio/reactivex/Single;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;",
            ">;>;"
        }
    .end annotation

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1e

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v7}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->batchGetLoyaltyAccounts$default(Lcom/squareup/loyalty/LoyaltyServiceHelper;Ljava/util/List;ZZZZILjava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public batchGetLoyaltyAccounts(Ljava/util/List;Z)Lio/reactivex/Single;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;",
            ">;>;"
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1c

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-static/range {v0 .. v7}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->batchGetLoyaltyAccounts$default(Lcom/squareup/loyalty/LoyaltyServiceHelper;Ljava/util/List;ZZZZILjava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public batchGetLoyaltyAccounts(Ljava/util/List;ZZ)Lio/reactivex/Single;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;ZZ)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;",
            ">;>;"
        }
    .end annotation

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x18

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-static/range {v0 .. v7}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->batchGetLoyaltyAccounts$default(Lcom/squareup/loyalty/LoyaltyServiceHelper;Ljava/util/List;ZZZZILjava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public batchGetLoyaltyAccounts(Ljava/util/List;ZZZ)Lio/reactivex/Single;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;ZZZ)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;",
            ">;>;"
        }
    .end annotation

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-static/range {v0 .. v7}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->batchGetLoyaltyAccounts$default(Lcom/squareup/loyalty/LoyaltyServiceHelper;Ljava/util/List;ZZZZILjava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public batchGetLoyaltyAccounts(Ljava/util/List;ZZZZ)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;ZZZZ)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "loyaltyAccountTokens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 294
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;-><init>()V

    .line 295
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;->include_active_mappings(Ljava/lang/Boolean;)Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;

    move-result-object p2

    .line 296
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;->include_contact(Ljava/lang/Boolean;)Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;

    move-result-object p2

    .line 297
    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;->force_contact_creation(Ljava/lang/Boolean;)Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;

    move-result-object p2

    .line 298
    invoke-virtual {p2}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;->build()Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;

    move-result-object p2

    .line 299
    new-instance p3, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions$Builder;

    invoke-direct {p3}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions$Builder;-><init>()V

    .line 300
    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p4

    invoke-virtual {p3, p4}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions$Builder;->include_raw_phone(Ljava/lang/Boolean;)Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions$Builder;

    move-result-object p3

    .line 301
    invoke-virtual {p3}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions$Builder;->build()Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;

    move-result-object p3

    .line 302
    new-instance p4, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$Builder;

    invoke-direct {p4}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$Builder;-><init>()V

    .line 303
    invoke-virtual {p4, p2}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$Builder;->loyalty_account_options(Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;)Lcom/squareup/protos/client/loyalty/LoyaltyOptions$Builder;

    move-result-object p2

    .line 304
    invoke-virtual {p2, p3}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$Builder;->loyalty_account_mapping_options(Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;)Lcom/squareup/protos/client/loyalty/LoyaltyOptions$Builder;

    move-result-object p2

    .line 305
    invoke-virtual {p2}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$Builder;->build()Lcom/squareup/protos/client/loyalty/LoyaltyOptions;

    move-result-object p2

    .line 307
    iget-object p3, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper;->loyalty:Lcom/squareup/server/loyalty/LoyaltyService;

    .line 308
    new-instance p4, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest$Builder;

    invoke-direct {p4}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest$Builder;-><init>()V

    .line 309
    invoke-virtual {p4, p1}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest$Builder;->loyalty_account_tokens(Ljava/util/List;)Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest$Builder;

    move-result-object p1

    .line 310
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest$Builder;->options(Lcom/squareup/protos/client/loyalty/LoyaltyOptions;)Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest$Builder;

    move-result-object p1

    .line 311
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;

    move-result-object p1

    const-string p2, "BatchGetLoyaltyAccountsR\u2026ons)\n            .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 307
    invoke-interface {p3, p1}, Lcom/squareup/server/loyalty/LoyaltyService;->batchGetLoyaltyAccounts(Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;)Lcom/squareup/server/loyalty/LoyaltyService$LoyaltyStandardResponse;

    move-result-object p1

    .line 313
    invoke-virtual {p1}, Lcom/squareup/server/loyalty/LoyaltyService$LoyaltyStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public createLoyaltyAccount(Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "loyaltyAccountMapping"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contactToken"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 400
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper;->loyalty:Lcom/squareup/server/loyalty/LoyaltyService;

    .line 401
    new-instance v1, Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountRequest$Builder;-><init>()V

    .line 402
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountRequest$Builder;->loyalty_account_mapping(Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;)Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountRequest$Builder;

    move-result-object p1

    .line 403
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountRequest$Builder;->contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountRequest$Builder;

    move-result-object p1

    .line 404
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountRequest;

    move-result-object p1

    const-string p2, "CreateLoyaltyAccountRequ\u2026ken)\n            .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 400
    invoke-interface {v0, p1}, Lcom/squareup/server/loyalty/LoyaltyService;->createLoyaltyAccount(Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 406
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 407
    sget-object p2, Lcom/squareup/loyalty/LoyaltyServiceHelper$createLoyaltyAccount$1;->INSTANCE:Lcom/squareup/loyalty/LoyaltyServiceHelper$createLoyaltyAccount$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "loyalty.createLoyaltyAcc\u2026k()\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public deleteLoyaltyAccount(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "loyaltyAccountToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 353
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper;->loyalty:Lcom/squareup/server/loyalty/LoyaltyService;

    .line 354
    new-instance v1, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountRequest$Builder;-><init>()V

    .line 355
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountRequest$Builder;->loyalty_account_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountRequest$Builder;

    move-result-object p1

    .line 356
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountRequest;

    move-result-object p1

    const-string v1, "DeleteLoyaltyAccountRequ\u2026ken)\n            .build()"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 353
    invoke-interface {v0, p1}, Lcom/squareup/server/loyalty/LoyaltyService;->deleteLoyaltyAccount(Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountRequest;)Lcom/squareup/server/loyalty/LoyaltyService$LoyaltyStandardResponse;

    move-result-object p1

    .line 358
    invoke-virtual {p1}, Lcom/squareup/server/loyalty/LoyaltyService$LoyaltyStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public getExpiringPoints(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "loyaltyAccountToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 469
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper;->loyalty:Lcom/squareup/server/loyalty/LoyaltyService;

    .line 470
    new-instance v1, Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusRequest$Builder;-><init>()V

    .line 471
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusRequest$Builder;->loyalty_account_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusRequest$Builder;

    move-result-object p1

    .line 472
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusRequest;

    move-result-object p1

    const-string v1, "GetExpiringPointsStatusR\u2026ken)\n            .build()"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 469
    invoke-interface {v0, p1}, Lcom/squareup/server/loyalty/LoyaltyService;->getExpiringPoints(Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 474
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public getLoyaltyAccountFromMapping(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "rawPhoneNumber"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 324
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper;->loyalty:Lcom/squareup/server/loyalty/LoyaltyService;

    .line 325
    new-instance v1, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;-><init>()V

    .line 327
    new-instance v2, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;-><init>()V

    .line 328
    invoke-virtual {v2, p1}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;->raw_id(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;

    move-result-object p1

    .line 329
    sget-object v2, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;->TYPE_PHONE:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;

    invoke-virtual {p1, v2}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;->type(Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;)Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;

    move-result-object p1

    .line 330
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;->build()Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    move-result-object p1

    .line 326
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;->loyalty_account_mapping(Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;)Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;

    move-result-object p1

    .line 333
    new-instance v1, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$Builder;-><init>()V

    .line 335
    new-instance v2, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;-><init>()V

    const/4 v3, 0x1

    .line 336
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;->include_contact(Ljava/lang/Boolean;)Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;

    move-result-object v2

    .line 337
    invoke-virtual {v2}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;->build()Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;

    move-result-object v2

    .line 334
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$Builder;->loyalty_account_options(Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;)Lcom/squareup/protos/client/loyalty/LoyaltyOptions$Builder;

    move-result-object v1

    .line 338
    invoke-virtual {v1}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$Builder;->build()Lcom/squareup/protos/client/loyalty/LoyaltyOptions;

    move-result-object v1

    .line 332
    invoke-virtual {p1, v1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;->options(Lcom/squareup/protos/client/loyalty/LoyaltyOptions;)Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;

    move-result-object p1

    .line 339
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;

    move-result-object p1

    const-string v1, "GetLoyaltyAccountFromMap\u2026d()\n            ).build()"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 324
    invoke-interface {v0, p1}, Lcom/squareup/server/loyalty/LoyaltyService;->getLoyaltyAccountFromMapping(Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 341
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public getLoyaltyReport(Lcom/squareup/protos/common/time/DateTimeInterval;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/time/DateTimeInterval;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "range"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 499
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper;->loyalty:Lcom/squareup/server/loyalty/LoyaltyService;

    .line 500
    new-instance v1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportRequest$Builder;-><init>()V

    .line 501
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportRequest$Builder;->dateRange(Lcom/squareup/protos/common/time/DateTimeInterval;)Lcom/squareup/protos/client/loyalty/GetLoyaltyReportRequest$Builder;

    move-result-object p1

    .line 502
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/GetLoyaltyReportRequest;

    move-result-object p1

    const-string v1, "GetLoyaltyReportRequest.\u2026nge)\n            .build()"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 499
    invoke-interface {v0, p1}, Lcom/squareup/server/loyalty/LoyaltyService;->getLoyaltyReport(Lcom/squareup/protos/client/loyalty/GetLoyaltyReportRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 504
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public getLoyaltyStatusForContact(Lcom/squareup/protos/client/rolodex/Contact;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/loyalty/LoyaltyStatusResponse;",
            ">;"
        }
    .end annotation

    const-string v0, "contact"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 423
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    const-string v0, "contact.contact_token"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->getLoyaltyStatusForContactToken(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    .line 424
    sget-object v0, Lcom/squareup/loyalty/LoyaltyServiceHelper$getLoyaltyStatusForContact$1;->INSTANCE:Lcom/squareup/loyalty/LoyaltyServiceHelper$getLoyaltyStatusForContact$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "getLoyaltyStatusForConta\u2026{ it.toPointsResponse() }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public getLoyaltyStatusForContactToken(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "contact_token"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 435
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper;->loyalty:Lcom/squareup/server/loyalty/LoyaltyService;

    .line 436
    new-instance v1, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactRequest$Builder;-><init>()V

    .line 437
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactRequest$Builder;->contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactRequest$Builder;

    move-result-object p1

    .line 438
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactRequest;

    move-result-object p1

    const-string v1, "GetLoyaltyStatusForConta\u2026ken)\n            .build()"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 435
    invoke-interface {v0, p1}, Lcom/squareup/server/loyalty/LoyaltyService;->getLoyaltyStatusForContact(Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 440
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public redeemPoints(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "couponDefinitionToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "phoneToken"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 249
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper;->loyalty:Lcom/squareup/server/loyalty/LoyaltyService;

    .line 250
    new-instance v1, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;-><init>()V

    .line 251
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->idempotence_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;

    move-result-object v1

    .line 252
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->coupon_definition_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;

    move-result-object p1

    .line 253
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->phone_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;

    move-result-object p1

    .line 254
    invoke-virtual {p1, p3}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->return_coupon_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;

    move-result-object p1

    .line 255
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;

    move-result-object p1

    const-string p2, "RedeemPointsRequest.Buil\u2026ken)\n            .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 249
    invoke-interface {v0, p1}, Lcom/squareup/server/loyalty/LoyaltyService;->redeemPoints(Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 257
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public returnReward(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/loyalty/ReturnRewardResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "couponToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 268
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper;->loyalty:Lcom/squareup/server/loyalty/LoyaltyService;

    .line 269
    new-instance v1, Lcom/squareup/protos/client/loyalty/ReturnRewardRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/loyalty/ReturnRewardRequest$Builder;-><init>()V

    .line 270
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/loyalty/ReturnRewardRequest$Builder;->coupon_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/ReturnRewardRequest$Builder;

    move-result-object p1

    .line 271
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/ReturnRewardRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/ReturnRewardRequest;

    move-result-object p1

    const-string v1, "ReturnRewardRequest.Buil\u2026ken)\n            .build()"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 268
    invoke-interface {v0, p1}, Lcom/squareup/server/loyalty/LoyaltyService;->returnReward(Lcom/squareup/protos/client/loyalty/ReturnRewardRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 273
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public transferLoyaltyAccount(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/loyalty/TransferLoyaltyAccountResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "loyaltyAccountToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "toContactToken"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 487
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper;->loyalty:Lcom/squareup/server/loyalty/LoyaltyService;

    .line 488
    new-instance v1, Lcom/squareup/protos/client/loyalty/TransferLoyaltyAccountRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/loyalty/TransferLoyaltyAccountRequest$Builder;-><init>()V

    .line 489
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/loyalty/TransferLoyaltyAccountRequest$Builder;->loyalty_account_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/TransferLoyaltyAccountRequest$Builder;

    move-result-object p1

    .line 490
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/loyalty/TransferLoyaltyAccountRequest$Builder;->to_contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/TransferLoyaltyAccountRequest$Builder;

    move-result-object p1

    .line 491
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/TransferLoyaltyAccountRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/TransferLoyaltyAccountRequest;

    move-result-object p1

    const-string p2, "TransferLoyaltyAccountRe\u2026ken)\n            .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 487
    invoke-interface {v0, p1}, Lcom/squareup/server/loyalty/LoyaltyService;->transferLoyaltyAccount(Lcom/squareup/protos/client/loyalty/TransferLoyaltyAccountRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 493
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public updateLoyaltyAccountMappings(Ljava/lang/String;Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "loyaltyAccountMappingToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltyAccountMapping"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 373
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper;->loyalty:Lcom/squareup/server/loyalty/LoyaltyService;

    .line 374
    new-instance v1, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingRequest$Builder;-><init>()V

    .line 375
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingRequest$Builder;->loyalty_account_mapping_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingRequest$Builder;

    move-result-object p1

    .line 376
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingRequest$Builder;->data(Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;)Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingRequest$Builder;

    move-result-object p1

    .line 377
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingRequest;

    move-result-object p1

    const-string p2, "UpdateLoyaltyAccountMapp\u2026ing)\n            .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 373
    invoke-interface {v0, p1}, Lcom/squareup/server/loyalty/LoyaltyService;->updateLoyaltyAccountMapping(Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingRequest;)Lcom/squareup/server/loyalty/LoyaltyService$LoyaltyStandardResponse;

    move-result-object p1

    .line 379
    invoke-virtual {p1}, Lcom/squareup/server/loyalty/LoyaltyService$LoyaltyStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 380
    sget-object p2, Lcom/squareup/loyalty/LoyaltyServiceHelper$updateLoyaltyAccountMappings$1;->INSTANCE:Lcom/squareup/loyalty/LoyaltyServiceHelper$updateLoyaltyAccountMappings$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "loyalty.updateLoyaltyAcc\u2026k()\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public voidCoupons(Ljava/util/List;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/loyalty/VoidCouponsResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "couponTokens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 450
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper;->loyalty:Lcom/squareup/server/loyalty/LoyaltyService;

    .line 451
    new-instance v1, Lcom/squareup/protos/client/loyalty/VoidCouponsRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/loyalty/VoidCouponsRequest$Builder;-><init>()V

    .line 452
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/loyalty/VoidCouponsRequest$Builder;->coupon_tokens(Ljava/util/List;)Lcom/squareup/protos/client/loyalty/VoidCouponsRequest$Builder;

    move-result-object p1

    .line 453
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/VoidCouponsRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/VoidCouponsRequest;

    move-result-object p1

    const-string v1, "VoidCouponsRequest.Build\u2026ens)\n            .build()"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 450
    invoke-interface {v0, p1}, Lcom/squareup/server/loyalty/LoyaltyService;->voidCoupons(Lcom/squareup/protos/client/loyalty/VoidCouponsRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 455
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
