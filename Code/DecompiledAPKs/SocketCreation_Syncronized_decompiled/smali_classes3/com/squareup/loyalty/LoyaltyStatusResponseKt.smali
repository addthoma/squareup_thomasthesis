.class public final Lcom/squareup/loyalty/LoyaltyStatusResponseKt;
.super Ljava/lang/Object;
.source "LoyaltyStatusResponse.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLoyaltyStatusResponse.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LoyaltyStatusResponse.kt\ncom/squareup/loyalty/LoyaltyStatusResponseKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,78:1\n250#2,2:79\n*E\n*S KotlinDebug\n*F\n+ 1 LoyaltyStatusResponse.kt\ncom/squareup/loyalty/LoyaltyStatusResponseKt\n*L\n53#1,2:79\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u0006\u0010\u0000\u001a\u00020\u0001\u001a\n\u0010\u0002\u001a\u00020\u0003*\u00020\u0004\u001a\u0010\u0010\u0002\u001a\u00020\u0003*\u0008\u0012\u0004\u0012\u00020\u00040\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "emptyLoyaltyStatusResponse",
        "Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$NoLoyalty;",
        "toPointsResponse",
        "Lcom/squareup/loyalty/LoyaltyStatusResponse;",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final emptyLoyaltyStatusResponse()Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$NoLoyalty;
    .locals 2

    .line 75
    new-instance v0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$NoLoyalty;

    .line 76
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 75
    invoke-direct {v0, v1}, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$NoLoyalty;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public static final toPointsResponse(Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;)Lcom/squareup/loyalty/LoyaltyStatusResponse;
    .locals 7

    const-string v0, "$this$toPointsResponse"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    const-string v1, "status.success"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 53
    iget-object v0, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;->loyalty_status:Ljava/util/List;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v0, :cond_5

    check-cast v0, Ljava/lang/Iterable;

    .line 79
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;

    if-eqz v5, :cond_1

    .line 55
    iget-object v6, v5, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;->customer_identifier:Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    if-eqz v6, :cond_1

    iget-object v6, v6, Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;->phone_token:Ljava/lang/String;

    goto :goto_0

    :cond_1
    move-object v6, v3

    :goto_0
    if-eqz v6, :cond_3

    iget-object v5, v5, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;->status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    if-eqz v5, :cond_2

    iget-object v5, v5, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->balance:Ljava/lang/Integer;

    goto :goto_1

    :cond_2
    move-object v5, v3

    :goto_1
    if-eqz v5, :cond_3

    const/4 v5, 0x1

    goto :goto_2

    :cond_3
    const/4 v5, 0x0

    :goto_2
    if-eqz v5, :cond_0

    move-object v3, v4

    .line 80
    :cond_4
    check-cast v3, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;

    .line 57
    :cond_5
    iget-object v0, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;->loyalty_account:Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    if-eqz v0, :cond_6

    iget-object v0, v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;->mappings:Ljava/util/List;

    if-eqz v0, :cond_6

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v2

    if-ne v0, v2, :cond_6

    const/4 v1, 0x1

    :cond_6
    const-string v0, "coupon"

    if-eqz v3, :cond_7

    if-eqz v1, :cond_7

    .line 60
    new-instance v1, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;

    .line 61
    iget-object v2, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;->coupon:Ljava/util/List;

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    iget-object v0, v3, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;->status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    iget-object v0, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->balance:Ljava/lang/Integer;

    const-string v4, "pointsStatus.status.balance"

    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 63
    iget-object v3, v3, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;->customer_identifier:Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    iget-object v3, v3, Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;->phone_token:Ljava/lang/String;

    const-string v4, "pointsStatus.customer_identifier.phone_token"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-direct {v1, v2, v0, v3, p0}, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;-><init>(Ljava/util/List;ILjava/lang/String;Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;)V

    check-cast v1, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus;

    goto :goto_3

    .line 67
    :cond_7
    new-instance v1, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$NoLoyalty;

    iget-object p0, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;->coupon:Ljava/util/List;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, p0}, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$NoLoyalty;-><init>(Ljava/util/List;)V

    check-cast v1, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus;

    .line 59
    :goto_3
    check-cast v1, Lcom/squareup/loyalty/LoyaltyStatusResponse;

    goto :goto_4

    .line 70
    :cond_8
    sget-object p0, Lcom/squareup/loyalty/LoyaltyStatusResponse$ErrorRetrievingLoyalty;->INSTANCE:Lcom/squareup/loyalty/LoyaltyStatusResponse$ErrorRetrievingLoyalty;

    move-object v1, p0

    check-cast v1, Lcom/squareup/loyalty/LoyaltyStatusResponse;

    :goto_4
    return-object v1
.end method

.method public static final toPointsResponse(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/loyalty/LoyaltyStatusResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;",
            ">;)",
            "Lcom/squareup/loyalty/LoyaltyStatusResponse;"
        }
    .end annotation

    const-string v0, "$this$toPointsResponse"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    instance-of v0, p0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;

    invoke-static {p0}, Lcom/squareup/loyalty/LoyaltyStatusResponseKt;->toPointsResponse(Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;)Lcom/squareup/loyalty/LoyaltyStatusResponse;

    move-result-object p0

    goto :goto_0

    .line 46
    :cond_0
    sget-object p0, Lcom/squareup/loyalty/LoyaltyStatusResponse$ErrorRetrievingLoyalty;->INSTANCE:Lcom/squareup/loyalty/LoyaltyStatusResponse$ErrorRetrievingLoyalty;

    check-cast p0, Lcom/squareup/loyalty/LoyaltyStatusResponse;

    :goto_0
    return-object p0
.end method
