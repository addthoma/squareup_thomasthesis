.class final Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter$start$1;
.super Ljava/lang/Object;
.source "X2ActivateEGiftCardWorkflowStarter.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter;->start(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacy/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "state",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter;


# direct methods
.method constructor <init>(Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter$start$1;->this$0:Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;)V
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter$start$1;->this$0:Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter;

    invoke-static {v0}, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter;->access$getMaybeX2SellerScreenRunner$p(Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter;)Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->displayEGiftCardActivation(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;)Z

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;

    invoke-virtual {p0, p1}, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter$start$1;->accept(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;)V

    return-void
.end method
