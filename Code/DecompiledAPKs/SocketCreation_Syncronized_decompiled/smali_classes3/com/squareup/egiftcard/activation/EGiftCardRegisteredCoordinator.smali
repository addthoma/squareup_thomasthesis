.class public final Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "EGiftCardRegisteredCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEGiftCardRegisteredCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EGiftCardRegisteredCoordinator.kt\ncom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,97:1\n1103#2,7:98\n1103#2,7:105\n1103#2,7:112\n1103#2,7:119\n1103#2,7:126\n1103#2,7:133\n*E\n*S KotlinDebug\n*F\n+ 1 EGiftCardRegisteredCoordinator.kt\ncom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator\n*L\n54#1,7:98\n55#1,7:105\n69#1,7:112\n70#1,7:119\n82#1,7:126\n83#1,7:133\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B#\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0010H\u0016J\u0010\u0010\u0015\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0010H\u0002J&\u0010\u0016\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00102\u0006\u0010\u0017\u001a\u00020\u00052\u000c\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0019H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
        "Lcom/squareup/egiftcard/activation/EGiftCardRegisteredScreen;",
        "(Lio/reactivex/Observable;)V",
        "backButton",
        "Lcom/squareup/glyph/SquareGlyphView;",
        "bodyText",
        "Lcom/squareup/marketfont/MarketTextView;",
        "progressBar",
        "Lcom/squareup/marin/widgets/MarinSpinnerGlyph;",
        "successReturnToSale",
        "Landroid/view/View;",
        "titleText",
        "attach",
        "",
        "view",
        "bindViews",
        "update",
        "state",
        "workflowInput",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "egiftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private backButton:Lcom/squareup/glyph/SquareGlyphView;

.field private bodyText:Lcom/squareup/marketfont/MarketTextView;

.field private progressBar:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private successReturnToSale:Landroid/view/View;

.field private titleText:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;Landroid/view/View;Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->update(Landroid/view/View;Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 90
    sget v0, Lcom/squareup/egiftcard/activation/R$id;->egiftcard_progress_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    iput-object v0, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->progressBar:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    .line 91
    sget v0, Lcom/squareup/egiftcard/activation/R$id;->egiftcard_registered_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->titleText:Lcom/squareup/marketfont/MarketTextView;

    .line 92
    sget v0, Lcom/squareup/egiftcard/activation/R$id;->egiftcard_registered_body:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->bodyText:Lcom/squareup/marketfont/MarketTextView;

    .line 93
    sget v0, Lcom/squareup/egiftcard/activation/R$id;->egiftcard_return_to_cart_actionbar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->successReturnToSale:Landroid/view/View;

    .line 94
    sget v0, Lcom/squareup/egiftcard/activation/R$id;->egiftcard_actionbarbutton:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/glyph/SquareGlyphView;

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->backButton:Lcom/squareup/glyph/SquareGlyphView;

    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
            ">;)V"
        }
    .end annotation

    .line 44
    instance-of v0, p2, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;

    const-string v1, ""

    const-string v2, "progressBar"

    const-string v3, "backButton"

    const-string v4, "bodyText"

    const-string v5, "successReturnToSale"

    const-string/jumbo v6, "titleText"

    const/4 v7, 0x0

    if-eqz v0, :cond_9

    .line 45
    sget-object p2, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator$update$1;->INSTANCE:Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator$update$1;

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, p2}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 46
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->progressBar:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    if-nez p1, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p1, Landroid/view/View;

    const/4 p2, 0x1

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 47
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->titleText:Lcom/squareup/marketfont/MarketTextView;

    if-nez p1, :cond_1

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast p1, Landroid/view/View;

    invoke-static {p1, v7}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 48
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->bodyText:Lcom/squareup/marketfont/MarketTextView;

    if-nez p1, :cond_2

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast p1, Landroid/view/View;

    invoke-static {p1, v7}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 49
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->successReturnToSale:Landroid/view/View;

    if-nez p1, :cond_3

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-static {p1, v7}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 50
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->backButton:Lcom/squareup/glyph/SquareGlyphView;

    if-nez p1, :cond_4

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast p1, Landroid/view/View;

    invoke-static {p1, v7}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 52
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->titleText:Lcom/squareup/marketfont/MarketTextView;

    if-nez p1, :cond_5

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    move-object p2, v1

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->bodyText:Lcom/squareup/marketfont/MarketTextView;

    if-nez p1, :cond_6

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->successReturnToSale:Landroid/view/View;

    if-nez p1, :cond_7

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 98
    :cond_7
    new-instance p2, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator$update$$inlined$onClickDebounced$1;

    invoke-direct {p2}, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator$update$$inlined$onClickDebounced$1;-><init>()V

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->backButton:Lcom/squareup/glyph/SquareGlyphView;

    if-nez p1, :cond_8

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    check-cast p1, Landroid/view/View;

    .line 105
    new-instance p2, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator$update$$inlined$onClickDebounced$2;

    invoke-direct {p2}, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator$update$$inlined$onClickDebounced$2;-><init>()V

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 57
    :cond_9
    instance-of v0, p2, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;

    const/high16 v8, 0x10e0000

    if-eqz v0, :cond_13

    .line 58
    new-instance p2, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator$update$4;

    invoke-direct {p2, p3}, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator$update$4;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, p2}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 59
    iget-object p2, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->progressBar:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    if-nez p2, :cond_a

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {p2}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->transitionToSuccess()V

    .line 60
    iget-object p2, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->successReturnToSale:Landroid/view/View;

    if-nez p2, :cond_b

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 61
    :cond_b
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 60
    invoke-static {p2, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    .line 63
    iget-object p2, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->titleText:Lcom/squareup/marketfont/MarketTextView;

    if-nez p2, :cond_c

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    check-cast p2, Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    .line 64
    iget-object p2, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->bodyText:Lcom/squareup/marketfont/MarketTextView;

    if-nez p2, :cond_d

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    check-cast p2, Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    .line 65
    iget-object p2, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->backButton:Lcom/squareup/glyph/SquareGlyphView;

    if-nez p2, :cond_e

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_e
    check-cast p2, Landroid/view/View;

    invoke-static {p2, v7}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 67
    iget-object p2, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->titleText:Lcom/squareup/marketfont/MarketTextView;

    if-nez p2, :cond_f

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_f
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/egiftcard/activation/R$string;->egiftcard_registered_title:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    iget-object p2, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->bodyText:Lcom/squareup/marketfont/MarketTextView;

    if-nez p2, :cond_10

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_10
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/egiftcard/activation/R$string;->egiftcard_registered_body:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p2, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->successReturnToSale:Landroid/view/View;

    if-nez p1, :cond_11

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 112
    :cond_11
    new-instance p2, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator$update$$inlined$onClickDebounced$3;

    invoke-direct {p2, p3}, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator$update$$inlined$onClickDebounced$3;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->backButton:Lcom/squareup/glyph/SquareGlyphView;

    if-nez p1, :cond_12

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_12
    check-cast p1, Landroid/view/View;

    .line 119
    new-instance p2, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator$update$$inlined$onClickDebounced$4;

    invoke-direct {p2}, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator$update$$inlined$onClickDebounced$4;-><init>()V

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 72
    :cond_13
    instance-of v0, p2, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ErrorRegistering;

    if-eqz v0, :cond_1d

    .line 73
    new-instance p2, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator$update$7;

    invoke-direct {p2, p3}, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator$update$7;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, p2}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 74
    iget-object p2, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->progressBar:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    if-nez p2, :cond_14

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_14
    invoke-virtual {p2}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->transitionToFailure()V

    .line 75
    iget-object p2, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->successReturnToSale:Landroid/view/View;

    if-nez p2, :cond_15

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_15
    invoke-static {p2, v7}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 76
    iget-object p2, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->titleText:Lcom/squareup/marketfont/MarketTextView;

    if-nez p2, :cond_16

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_16
    check-cast p2, Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    .line 77
    iget-object p2, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->bodyText:Lcom/squareup/marketfont/MarketTextView;

    if-nez p2, :cond_17

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_17
    check-cast p2, Landroid/view/View;

    invoke-static {p2, v7}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 78
    iget-object p2, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->backButton:Lcom/squareup/glyph/SquareGlyphView;

    if-nez p2, :cond_18

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_18
    check-cast p2, Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    .line 80
    iget-object p2, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->titleText:Lcom/squareup/marketfont/MarketTextView;

    if-nez p2, :cond_19

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_19
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/egiftcard/activation/R$string;->egiftcard_registered_error:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p2, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->bodyText:Lcom/squareup/marketfont/MarketTextView;

    if-nez p1, :cond_1a

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1a
    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->successReturnToSale:Landroid/view/View;

    if-nez p1, :cond_1b

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 126
    :cond_1b
    new-instance p2, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator$update$$inlined$onClickDebounced$5;

    invoke-direct {p2}, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator$update$$inlined$onClickDebounced$5;-><init>()V

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->backButton:Lcom/squareup/glyph/SquareGlyphView;

    if-nez p1, :cond_1c

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1c
    check-cast p1, Landroid/view/View;

    .line 133
    new-instance p2, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator$update$$inlined$onClickDebounced$6;

    invoke-direct {p2, p3}, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator$update$$inlined$onClickDebounced$6;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void

    .line 85
    :cond_1d
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unexpected state "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0, p1}, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->bindViews(Landroid/view/View;)V

    .line 35
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator$attach$1;-><init>(Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
