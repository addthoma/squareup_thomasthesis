.class public final Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ChooseCustomAmountCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nChooseCustomAmountCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ChooseCustomAmountCoordinator.kt\ncom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,140:1\n1103#2,7:141\n1103#2,7:148\n*E\n*S KotlinDebug\n*F\n+ 1 ChooseCustomAmountCoordinator.kt\ncom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator\n*L\n73#1,7:141\n108#1,7:148\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001:\u0001)B9\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u0010\u0010\u001b\u001a\u00020\u00182\u0006\u0010\u001c\u001a\u00020\u001dH\u0016J\u0010\u0010\u001e\u001a\u00020\u00182\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J\u0018\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\u00052\u0006\u0010\"\u001a\u00020#H\u0002J&\u0010$\u001a\u00020\u00182\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010!\u001a\u00020\u00052\u000c\u0010%\u001a\u0008\u0012\u0004\u0012\u00020\u00060&H\u0002J\u0018\u0010\'\u001a\u00020\u00182\u0006\u0010!\u001a\u00020\u00052\u0006\u0010(\u001a\u00020#H\u0002R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0010\u001a\u0010\u0012\u000c\u0012\n \u0012*\u0004\u0018\u00010\n0\n0\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0017\u001a\u0010\u0012\u000c\u0012\n \u0012*\u0004\u0018\u00010\u00180\u00180\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006*"
    }
    d2 = {
        "Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
        "Lcom/squareup/egiftcard/activation/ChooseCustomAmountScreen;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "priceLocaleHelper",
        "Lcom/squareup/money/PriceLocaleHelper;",
        "(Lio/reactivex/Observable;Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;)V",
        "amountEditText",
        "Lcom/squareup/noho/NohoEditText;",
        "amountEntered",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "kotlin.jvm.PlatformType",
        "backButton",
        "Lcom/squareup/glyph/SquareGlyphView;",
        "doneButton",
        "Lcom/squareup/noho/NohoButton;",
        "doneClicked",
        "",
        "hintText",
        "Lcom/squareup/marketfont/MarketTextView;",
        "attach",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "isInputValidMoney",
        "",
        "state",
        "input",
        "",
        "update",
        "workflowInput",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "updateDoneButton",
        "str",
        "Factory",
        "egiftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private amountEditText:Lcom/squareup/noho/NohoEditText;

.field private final amountEntered:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private backButton:Lcom/squareup/glyph/SquareGlyphView;

.field private doneButton:Lcom/squareup/noho/NohoButton;

.field private final doneClicked:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private hintText:Lcom/squareup/marketfont/MarketTextView;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
            ">;>;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ")V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priceLocaleHelper"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p3, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    .line 46
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create<Money>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->amountEntered:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 47
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create<Unit>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->doneClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method

.method public static final synthetic access$getDoneClicked$p(Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;)Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->doneClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object p0
.end method

.method public static final synthetic access$isInputValidMoney(Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;Ljava/lang/CharSequence;)Z
    .locals 0

    .line 34
    invoke-direct {p0, p1, p2}, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->isInputValidMoney(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;Ljava/lang/CharSequence;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$update(Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;Landroid/view/View;Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->update(Landroid/view/View;Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method public static final synthetic access$updateDoneButton(Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;Ljava/lang/CharSequence;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1, p2}, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->updateDoneButton(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 134
    sget v0, Lcom/squareup/egiftcard/activation/R$id;->egiftcard_actionbarbutton:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->backButton:Lcom/squareup/glyph/SquareGlyphView;

    .line 135
    sget v0, Lcom/squareup/egiftcard/activation/R$id;->egiftcard_custom_amount_hint:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->hintText:Lcom/squareup/marketfont/MarketTextView;

    .line 136
    sget v0, Lcom/squareup/egiftcard/activation/R$id;->egiftcard_custom_amount_edit_text:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditText;

    iput-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->amountEditText:Lcom/squareup/noho/NohoEditText;

    .line 137
    sget v0, Lcom/squareup/egiftcard/activation/R$id;->egiftcard_done:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoButton;

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->doneButton:Lcom/squareup/noho/NohoButton;

    return-void
.end method

.method private final isInputValidMoney(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;Ljava/lang/CharSequence;)Z
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    invoke-virtual {v0, p2}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object p2

    invoke-virtual {p1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;->getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/egiftcard/activation/EGiftCardConfig;->getMinLoadAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;->getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/egiftcard/activation/EGiftCardConfig;->getMaxLoadAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 129
    invoke-static {p2, v0, p1}, Lcom/squareup/money/MoneyMath;->inRange(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p1

    return p1
.end method

.method private final update(Landroid/view/View;Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
            ">;)V"
        }
    .end annotation

    .line 72
    new-instance v0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$update$1;

    invoke-direct {v0, p3}, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$update$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->backButton:Lcom/squareup/glyph/SquareGlyphView;

    if-nez v0, :cond_0

    const-string v1, "backButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    .line 141
    new-instance v1, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$update$$inlined$onClickDebounced$1;

    invoke-direct {v1, p3}, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$update$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->hintText:Lcom/squareup/marketfont/MarketTextView;

    if-nez v0, :cond_1

    const-string v1, "hintText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 75
    :cond_1
    sget v1, Lcom/squareup/egiftcard/activation/R$string;->egiftcard_custom_amount_hint:I

    invoke-static {p1, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/view/View;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 76
    iget-object v2, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p2}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;->getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/egiftcard/activation/EGiftCardConfig;->getMinLoadAmount()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    const-string v3, "min_amount"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 77
    iget-object v2, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p2}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;->getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/egiftcard/activation/EGiftCardConfig;->getMaxLoadAmount()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    const-string v3, "max_amount"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 78
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 79
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    new-instance v0, Lcom/squareup/money/MaxMoneyScrubber;

    iget-object v1, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    check-cast v1, Lcom/squareup/money/MoneyExtractor;

    iget-object v2, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p2}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;->getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/egiftcard/activation/EGiftCardConfig;->getMaxLoadAmount()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/money/MaxMoneyScrubber;-><init>(Lcom/squareup/money/MoneyExtractor;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)V

    .line 86
    iget-object v1, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v2, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->amountEditText:Lcom/squareup/noho/NohoEditText;

    const-string v3, "amountEditText"

    if-nez v2, :cond_2

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v2, Lcom/squareup/text/HasSelectableText;

    invoke-virtual {v1, v2}, Lcom/squareup/money/PriceLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;)Lcom/squareup/text/ScrubbingTextWatcher;

    move-result-object v1

    .line 87
    check-cast v0, Lcom/squareup/text/Scrubber;

    invoke-virtual {v1, v0}, Lcom/squareup/text/ScrubbingTextWatcher;->addScrubber(Lcom/squareup/text/Scrubber;)V

    .line 89
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v1, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->amountEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v1, :cond_3

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/squareup/money/PriceLocaleHelper;->setHintToZeroMoney(Landroid/widget/TextView;)V

    .line 90
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->amountEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v0, :cond_4

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    new-instance v1, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$update$3;

    invoke-direct {v1, p0, p2}, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$update$3;-><init>(Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;)V

    check-cast v1, Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 93
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->amountEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v0, :cond_5

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    new-instance v1, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$update$4;

    invoke-direct {v1, p0, p2}, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$update$4;-><init>(Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;)V

    check-cast v1, Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 108
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->doneButton:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_6

    const-string v1, "doneButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast v0, Landroid/view/View;

    .line 148
    new-instance v1, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$update$$inlined$onClickDebounced$2;

    invoke-direct {v1, p0}, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$update$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->doneClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 110
    iget-object v1, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->amountEntered:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v1, Lio/reactivex/ObservableSource;

    sget-object v2, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$update$6;->INSTANCE:Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$update$6;

    check-cast v2, Lio/reactivex/functions/BiFunction;

    invoke-virtual {v0, v1, v2}, Lcom/jakewharton/rxrelay2/PublishRelay;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "doneClicked\n        .wit\u2026mount: Money -> amount })"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    new-instance v1, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$update$7;

    invoke-direct {v1, p3}, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$update$7;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 113
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->amountEditText:Lcom/squareup/noho/NohoEditText;

    if-nez p1, :cond_7

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {p1}, Lcom/squareup/noho/NohoEditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-direct {p0, p2, p1}, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->updateDoneButton(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final updateDoneButton(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;Ljava/lang/CharSequence;)V
    .locals 1

    .line 117
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    invoke-virtual {v0, p2}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 118
    invoke-direct {p0, p1, p2}, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->isInputValidMoney(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;Ljava/lang/CharSequence;)Z

    move-result p1

    const-string p2, "doneButton"

    if-eqz p1, :cond_1

    .line 119
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->amountEntered:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 120
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->doneButton:Lcom/squareup/noho/NohoButton;

    if-nez p1, :cond_0

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoButton;->setEnabled(Z)V

    goto :goto_0

    .line 122
    :cond_1
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->doneButton:Lcom/squareup/noho/NohoButton;

    if-nez p1, :cond_2

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoButton;->setEnabled(Z)V

    :goto_0
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-direct {p0, p1}, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->bindViews(Landroid/view/View;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$attach$1;-><init>(Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
