.class final Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$registerEGiftCardToState$1;
.super Ljava/lang/Object;
.source "ActivateEGiftCardReactor.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor;->registerEGiftCardToState(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0014\u0010\u0003\u001a\u0010\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u0004H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/giftcards/RegisterEGiftCardResponse;",
        "kotlin.jvm.PlatformType",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;


# direct methods
.method constructor <init>(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$registerEGiftCardToState$1;->$state:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/giftcards/RegisterEGiftCardResponse;",
            ">;)",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    .line 124
    instance-of v1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v1, :cond_0

    .line 125
    new-instance v1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;

    iget-object v2, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$registerEGiftCardToState$1;->$state:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;

    invoke-virtual {v2}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    move-result-object v2

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardResponse;->gift_card:Lcom/squareup/protos/client/giftcards/GiftCard;

    const-string v3, "it.response.gift_card"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$registerEGiftCardToState$1;->$state:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;

    invoke-virtual {v3}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$registerEGiftCardToState$1;->$state:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;

    invoke-virtual {v4}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->getAutoClose()Z

    move-result v4

    invoke-direct {v1, v2, p1, v3, v4}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;-><init>(Lcom/squareup/egiftcard/activation/EGiftCardConfig;Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/protos/common/Money;Z)V

    check-cast v1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;

    goto :goto_0

    .line 127
    :cond_0
    new-instance p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ErrorRegistering;

    iget-object v1, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$registerEGiftCardToState$1;->$state:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;

    invoke-virtual {v1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$registerEGiftCardToState$1;->$state:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;

    invoke-virtual {v2}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->getDesignId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$registerEGiftCardToState$1;->$state:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;

    invoke-virtual {v3}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$registerEGiftCardToState$1;->$state:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;

    invoke-virtual {v4}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->getEmail()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p1, v1, v2, v3, v4}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ErrorRegistering;-><init>(Lcom/squareup/egiftcard/activation/EGiftCardConfig;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;)V

    move-object v1, p1

    check-cast v1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;

    .line 123
    :goto_0
    invoke-direct {v0, v1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$registerEGiftCardToState$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
