.class public final Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$Factory;
.super Ljava/lang/Object;
.source "ChooseDesignCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J+\u0010\u0007\u001a\u00020\u00082\u001c\u0010\t\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00020\r0\u000bj\u0002`\u000e0\nH\u0000\u00a2\u0006\u0002\u0008\u000fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$Factory;",
        "",
        "device",
        "Lcom/squareup/util/Device;",
        "picasso",
        "Lcom/squareup/picasso/Picasso;",
        "(Lcom/squareup/util/Device;Lcom/squareup/picasso/Picasso;)V",
        "build",
        "Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingDesign;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
        "Lcom/squareup/egiftcard/activation/ChooseEGiftCardDesignScreen;",
        "build$egiftcard_activation_release",
        "egiftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final device:Lcom/squareup/util/Device;

.field private final picasso:Lcom/squareup/picasso/Picasso;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Device;Lcom/squareup/picasso/Picasso;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "device"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "picasso"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$Factory;->device:Lcom/squareup/util/Device;

    iput-object p2, p0, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$Factory;->picasso:Lcom/squareup/picasso/Picasso;

    return-void
.end method


# virtual methods
.method public final build$egiftcard_activation_release(Lio/reactivex/Observable;)Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingDesign;",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
            ">;>;)",
            "Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    new-instance v0, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator;

    iget-object v1, p0, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$Factory;->device:Lcom/squareup/util/Device;

    iget-object v2, p0, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$Factory;->picasso:Lcom/squareup/picasso/Picasso;

    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/util/Device;Lcom/squareup/picasso/Picasso;)V

    return-object v0
.end method
