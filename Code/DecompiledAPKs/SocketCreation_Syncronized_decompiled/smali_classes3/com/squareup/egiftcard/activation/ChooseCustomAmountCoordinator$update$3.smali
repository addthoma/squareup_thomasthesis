.class public final Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$update$3;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "ChooseCustomAmountCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->update(Landroid/view/View;Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;Lcom/squareup/workflow/legacy/WorkflowInput;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$update$3",
        "Lcom/squareup/text/EmptyTextWatcher;",
        "afterTextChanged",
        "",
        "s",
        "Landroid/text/Editable;",
        "egiftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;

.field final synthetic this$0:Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;",
            ")V"
        }
    .end annotation

    .line 90
    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$update$3;->this$0:Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;

    iput-object p2, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$update$3;->$state:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    const-string v0, "s"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$update$3;->this$0:Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;

    iget-object v1, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$update$3;->$state:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {v0, v1, p1}, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->access$updateDoneButton(Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;Ljava/lang/CharSequence;)V

    return-void
.end method
