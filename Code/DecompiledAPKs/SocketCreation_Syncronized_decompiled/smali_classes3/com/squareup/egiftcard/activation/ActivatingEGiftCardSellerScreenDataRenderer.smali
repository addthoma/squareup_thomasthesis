.class public final Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerScreenDataRenderer;
.super Ljava/lang/Object;
.source "ActivatingEGiftCardSellerScreen.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerScreenDataRenderer;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/util/Res;)V",
        "render",
        "Lcom/squareup/egiftcard/activation/ScreenData;",
        "state",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
        "egiftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerScreenDataRenderer;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public final render(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;)Lcom/squareup/egiftcard/activation/ScreenData;
    .locals 7

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    sget-object v0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$InitState;->INSTANCE:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$InitState;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 51
    instance-of v0, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingDesign;

    if-eqz v0, :cond_0

    .line 52
    new-instance p1, Lcom/squareup/egiftcard/activation/ScreenData;

    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerScreenDataRenderer;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/egiftcard/activation/R$string;->egiftcard_x2_seller_design:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/egiftcard/activation/ScreenData;-><init>(Ljava/lang/String;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_1

    .line 54
    :cond_0
    instance-of v0, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingAmount;

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    instance-of v0, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;

    if-eqz v0, :cond_2

    .line 55
    :goto_0
    new-instance p1, Lcom/squareup/egiftcard/activation/ScreenData;

    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerScreenDataRenderer;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/egiftcard/activation/R$string;->egiftcard_x2_seller_amount:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/egiftcard/activation/ScreenData;-><init>(Ljava/lang/String;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_1

    .line 57
    :cond_2
    instance-of v0, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingEmail;

    if-eqz v0, :cond_3

    new-instance p1, Lcom/squareup/egiftcard/activation/ScreenData;

    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerScreenDataRenderer;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/egiftcard/activation/R$string;->egiftcard_x2_seller_email:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/egiftcard/activation/ScreenData;-><init>(Ljava/lang/String;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_1

    .line 59
    :cond_3
    instance-of v0, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;

    if-eqz v0, :cond_4

    new-instance p1, Lcom/squareup/egiftcard/activation/ScreenData;

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/egiftcard/activation/ScreenData;-><init>(Ljava/lang/String;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_1

    .line 61
    :cond_4
    instance-of v0, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;

    if-eqz v0, :cond_5

    new-instance p1, Lcom/squareup/egiftcard/activation/ScreenData;

    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerScreenDataRenderer;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/egiftcard/activation/R$string;->egiftcard_registered_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p1, v0, v1, v1}, Lcom/squareup/egiftcard/activation/ScreenData;-><init>(Ljava/lang/String;ZZ)V

    :goto_1
    return-object p1

    .line 64
    :cond_5
    instance-of p1, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ErrorRegistering;

    if-eqz p1, :cond_6

    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "ErrorRegistering uses dialog instead"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 49
    :cond_7
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
