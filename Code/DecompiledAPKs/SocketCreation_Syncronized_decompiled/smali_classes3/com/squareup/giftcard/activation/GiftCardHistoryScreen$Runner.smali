.class public interface abstract Lcom/squareup/giftcard/activation/GiftCardHistoryScreen$Runner;
.super Ljava/lang/Object;
.source "GiftCardHistoryScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/giftcard/activation/GiftCardHistoryScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u000e\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H&J\u0008\u0010\u0005\u001a\u00020\u0006H&J\u000e\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0003H&J\u000e\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\n0\u0003H&J\u0008\u0010\u000b\u001a\u00020\u0006H&J\u0008\u0010\u000c\u001a\u00020\u0006H&J\u000e\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H&J\u000e\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u0003H&\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/giftcard/activation/GiftCardHistoryScreen$Runner;",
        "",
        "busy",
        "Lrx/Observable;",
        "",
        "cancelHistoryScreen",
        "",
        "giftCardHistory",
        "Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;",
        "giftCardName",
        "",
        "goToAddValueScreen",
        "goToClearBalanceScreen",
        "isAddValueEnabled",
        "registerGiftCard",
        "Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;",
        "giftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract busy()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract cancelHistoryScreen()V
.end method

.method public abstract giftCardHistory()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;",
            ">;"
        }
    .end annotation
.end method

.method public abstract giftCardName()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract goToAddValueScreen()V
.end method

.method public abstract goToClearBalanceScreen()V
.end method

.method public abstract isAddValueEnabled()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract registerGiftCard()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;",
            ">;"
        }
    .end annotation
.end method
