.class final Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$responseScreenData$2;
.super Ljava/lang/Object;
.source "GiftCardLoadingScopeRunner.kt"

# interfaces
.implements Lrx/functions/Func2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func2<",
        "TA;TB;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;",
        "giftCardResponse",
        "Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;",
        "card",
        "Lcom/squareup/Card;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$responseScreenData$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;Lcom/squareup/Card;)Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;
    .locals 2

    const-string v0, "card"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_1

    .line 299
    iget-object v0, p1, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    const-string v1, "giftCardResponse.status.success"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 301
    iget-object p1, p1, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;->gift_card:Lcom/squareup/protos/client/giftcards/GiftCard;

    .line 302
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$responseScreenData$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->access$getRes$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/squareup/util/Res;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/squareup/text/Cards;->formattedBrandAndUnmaskedDigits(Lcom/squareup/util/Res;Lcom/squareup/Card;)Ljava/lang/String;

    move-result-object p2

    .line 303
    iget-object v0, p1, Lcom/squareup/protos/client/giftcards/GiftCard;->state:Lcom/squareup/protos/client/giftcards/GiftCard$State;

    sget-object v1, Lcom/squareup/protos/client/giftcards/GiftCard$State;->ACTIVE:Lcom/squareup/protos/client/giftcards/GiftCard$State;

    if-ne v0, v1, :cond_0

    .line 304
    iget-object p1, p1, Lcom/squareup/protos/client/giftcards/GiftCard;->balance_money:Lcom/squareup/protos/common/Money;

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    .line 306
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$responseScreenData$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-static {p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->access$getCurrencyCode$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 309
    :goto_0
    sget-object v0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;->Companion:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$Companion;

    const-string v1, "cardBalance"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "giftCardName"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$Companion;->success(Lcom/squareup/protos/common/Money;Ljava/lang/String;)Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;

    move-result-object p1

    return-object p1

    .line 311
    :cond_1
    sget-object p1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;->Companion:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$Companion;

    invoke-virtual {p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$Companion;->cardBalanceError()Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 96
    check-cast p1, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;

    check-cast p2, Lcom/squareup/Card;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$responseScreenData$2;->call(Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;Lcom/squareup/Card;)Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;

    move-result-object p1

    return-object p1
.end method
