.class final Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$3;
.super Ljava/lang/Object;
.source "GiftCardClearBalanceCoordinator.kt"

# interfaces
.implements Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "onConfirm"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$3;->this$0:Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onConfirm()V
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$3;->this$0:Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;->access$getRunner$p(Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;)Lcom/squareup/giftcard/activation/GiftCardClearBalanceScreen$Runner;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/giftcard/activation/GiftCardClearBalanceScreen$Runner;->triggerClearBalance()V

    return-void
.end method
