.class final Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$1;
.super Lkotlin/jvm/internal/Lambda;
.source "GiftCardLoadingScopeRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
        "+",
        "Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012*\u0010\u0002\u001a&\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004 \u0005*\u0012\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 96
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$1;->invoke(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;",
            ">;)V"
        }
    .end annotation

    .line 168
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v0

    const-string v2, "it.response"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;

    invoke-static {v0}, Lcom/squareup/egiftcard/activation/EGiftCardConfigKt;->isEGiftCardVisible(Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->access$getChooseTypeBusy$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 170
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->access$getEGiftCardConfig$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    goto :goto_0

    .line 173
    :cond_0
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-static {p1, v1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->access$setChooseTypeDisplayed$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;Z)V

    .line 174
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-static {p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->access$getFlow$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lflow/Flow;

    move-result-object p1

    .line 175
    sget-object v0, Lflow/Direction;->REPLACE:Lflow/Direction;

    .line 176
    new-instance v2, Lcom/squareup/giftcard/activation/GiftCardLookupScreen;

    iget-object v3, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-static {v3}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->access$getGiftCardLoadingScope$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/squareup/giftcard/activation/GiftCardLoadingScope;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v2, v3}, Lcom/squareup/giftcard/activation/GiftCardLookupScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    check-cast v2, Lcom/squareup/container/ContainerTreeKey;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    .line 177
    const-class v4, Lcom/squareup/giftcard/activation/GiftCardChooseTypeScreen;

    aput-object v4, v3, v1

    .line 174
    invoke-static {p1, v0, v2, v3}, Lcom/squareup/container/Flows;->goBackPastAndAdd(Lflow/Flow;Lflow/Direction;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    :goto_0
    return-void
.end method
