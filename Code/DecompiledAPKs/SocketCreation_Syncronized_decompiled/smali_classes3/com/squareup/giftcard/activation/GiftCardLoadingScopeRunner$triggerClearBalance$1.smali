.class final Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$triggerClearBalance$1;
.super Ljava/lang/Object;
.source "GiftCardLoadingScopeRunner.kt"

# interfaces
.implements Lrx/functions/Func2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->triggerClearBalance()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func2<",
        "TT1;TT2;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u001e\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00040\u00040\u00012\u000e\u0010\u0005\u001a\n \u0003*\u0004\u0018\u00010\u00060\u00062\u000e\u0010\u0007\u001a\n \u0003*\u0004\u0018\u00010\u00040\u0004H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lkotlin/Pair;",
        "Lcom/squareup/protos/client/giftcards/GiftCard;",
        "kotlin.jvm.PlatformType",
        "Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;",
        "gc",
        "Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;",
        "reason",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$triggerClearBalance$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$triggerClearBalance$1;

    invoke-direct {v0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$triggerClearBalance$1;-><init>()V

    sput-object v0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$triggerClearBalance$1;->INSTANCE:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$triggerClearBalance$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 96
    check-cast p1, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;

    check-cast p2, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$triggerClearBalance$1;->call(Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;)Lkotlin/Pair;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;)Lkotlin/Pair;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;",
            "Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;",
            ")",
            "Lkotlin/Pair<",
            "Lcom/squareup/protos/client/giftcards/GiftCard;",
            "Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;",
            ">;"
        }
    .end annotation

    .line 549
    new-instance v0, Lkotlin/Pair;

    iget-object p1, p1, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;->gift_card:Lcom/squareup/protos/client/giftcards/GiftCard;

    invoke-direct {v0, p1, p2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method
