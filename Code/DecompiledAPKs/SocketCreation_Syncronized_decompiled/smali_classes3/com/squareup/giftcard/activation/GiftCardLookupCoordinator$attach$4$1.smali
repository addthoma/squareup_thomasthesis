.class final Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4$1;
.super Ljava/lang/Object;
.source "GiftCardLookupCoordinator.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;->invoke()Lrx/Subscription;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "Lcom/squareup/Card;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/Card;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;


# direct methods
.method constructor <init>(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/Card;

    invoke-virtual {p0, p1}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4$1;->call(Lcom/squareup/Card;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/squareup/Card;)Z
    .locals 2

    .line 121
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->access$getController$p(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;)Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;

    move-result-object v0

    const-string v1, "it"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;->isValidCard(Lcom/squareup/Card;)Z

    move-result p1

    return p1
.end method
