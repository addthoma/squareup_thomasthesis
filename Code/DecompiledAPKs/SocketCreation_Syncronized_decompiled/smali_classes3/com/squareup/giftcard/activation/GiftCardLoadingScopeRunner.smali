.class public final Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;
.super Ljava/lang/Object;
.source "GiftCardLoadingScopeRunner.kt"

# interfaces
.implements Lmortar/Scoped;
.implements Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;
.implements Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;
.implements Lcom/squareup/giftcard/activation/GiftCardHistoryScreen$Runner;
.implements Lcom/squareup/giftcard/activation/GiftCardClearBalanceScreen$Runner;
.implements Lcom/squareup/giftcard/activation/GiftCardChooseTypeScreen$Runner;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/giftcard/activation/GiftCardLoadingScope;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008c\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0017\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0008\t\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u00052\u00020\u0006:\u0001\u007fB{\u0008\u0007\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0008\u0008\u0001\u0010\u000b\u001a\u00020\u000c\u0012\u0008\u0008\u0001\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u0012\u0006\u0010\u0019\u001a\u00020\u001a\u0012\u0006\u0010\u001b\u001a\u00020\u001c\u0012\u0006\u0010\u001d\u001a\u00020\u001e\u0012\u0006\u0010\u001f\u001a\u00020 \u0012\u0006\u0010!\u001a\u00020\"\u00a2\u0006\u0002\u0010#J\u0008\u0010O\u001a\u00020+H\u0016J \u0010P\u001a\u00020(2\u0006\u0010Q\u001a\u00020R2\u0006\u0010S\u001a\u00020I2\u0006\u0010T\u001a\u00020UH\u0016J\u000e\u0010)\u001a\u0008\u0012\u0004\u0012\u00020+0FH\u0016J\u000e\u0010V\u001a\u0008\u0012\u0004\u0012\u00020+0FH\u0016J\u0008\u0010W\u001a\u00020(H\u0016J\u0008\u0010X\u001a\u00020(H\u0016J\u0008\u0010Y\u001a\u00020(H\u0016J\u0008\u0010Z\u001a\u00020(H\u0016J\u0008\u0010[\u001a\u00020(H\u0016J\u000e\u0010-\u001a\u0008\u0012\u0004\u0012\u00020.0FH\u0016J,\u0010/\u001a&\u0012\u000c\u0012\n ,*\u0004\u0018\u00010+0+ ,*\u0012\u0012\u000c\u0012\n ,*\u0004\u0018\u00010+0+\u0018\u00010*0*H\u0016J\u0008\u0010\\\u001a\u00020+H\u0002J\u000e\u0010]\u001a\u0008\u0012\u0004\u0012\u00020:0FH\u0016J\u0008\u0010^\u001a\u00020(H\u0002J\u0008\u0010_\u001a\u00020(H\u0016J\u0010\u0010`\u001a\u00020(2\u0006\u0010-\u001a\u00020.H\u0016J\u0008\u0010a\u001a\u00020(H\u0016J\u0008\u0010b\u001a\u00020(H\u0016J\u0008\u0010c\u001a\u00020RH\u0016J\u000e\u0010>\u001a\u0008\u0012\u0004\u0012\u00020?0FH\u0016J,\u0010d\u001a&\u0012\u000c\u0012\n ,*\u0004\u0018\u00010808 ,*\u0012\u0012\u000c\u0012\n ,*\u0004\u0018\u00010808\u0018\u00010F0FH\u0016J\u0008\u0010e\u001a\u00020(H\u0016J\u0008\u0010f\u001a\u00020(H\u0016J\u000e\u0010g\u001a\u0008\u0012\u0004\u0012\u00020+0FH\u0016J\u0008\u0010h\u001a\u00020+H\u0016J\u0010\u0010i\u001a\u00020+2\u0006\u0010-\u001a\u00020.H\u0016J\u000e\u0010E\u001a\u0008\u0012\u0004\u0012\u00020.0FH\u0016J\u000e\u0010j\u001a\u0008\u0012\u0004\u0012\u00020(0FH\u0016J\u0010\u0010k\u001a\u00020(2\u0006\u0010l\u001a\u00020mH\u0016J\u0008\u0010n\u001a\u00020(H\u0016J\u000e\u0010M\u001a\u0008\u0012\u0004\u0012\u00020N0FH\u0016J#\u0010o\u001a\u0004\u0018\u0001Hp\"\u0004\u0008\u0000\u0010p2\u000c\u0010q\u001a\u0008\u0012\u0004\u0012\u0002Hp0rH\u0002\u00a2\u0006\u0002\u0010sJ\u0010\u0010t\u001a\u00020(2\u0006\u0010-\u001a\u00020.H\u0016J\u0012\u0010u\u001a\u00020(2\u0008\u0008\u0001\u0010v\u001a\u00020wH\u0016J\u0010\u0010x\u001a\u00020(2\u0006\u0010y\u001a\u000208H\u0016J\u0008\u0010z\u001a\u00020(H\u0016J\u0008\u0010{\u001a\u00020(H\u0016J\u0008\u0010|\u001a\u00020(H\u0016J\u000c\u0010}\u001a\u00020(*\u00020\u0008H\u0002J\u000c\u0010~\u001a\u00020(*\u00020\u0008H\u0002R\u000e\u0010$\u001a\u00020%X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010&\u001a\u0008\u0012\u0004\u0012\u00020(0\'X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010)\u001a&\u0012\u000c\u0012\n ,*\u0004\u0018\u00010+0+ ,*\u0012\u0012\u000c\u0012\n ,*\u0004\u0018\u00010+0+\u0018\u00010*0*X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010-\u001a&\u0012\u000c\u0012\n ,*\u0004\u0018\u00010.0. ,*\u0012\u0012\u000c\u0012\n ,*\u0004\u0018\u00010.0.\u0018\u00010*0*X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010/\u001a&\u0012\u000c\u0012\n ,*\u0004\u0018\u00010+0+ ,*\u0012\u0012\u000c\u0012\n ,*\u0004\u0018\u00010+0+\u0018\u00010*0*X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00100\u001a\u00020+X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\"\u00101\u001a\u0008\u0012\u0004\u0012\u0002020*8\u0006X\u0087\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u00083\u00104\u001a\u0004\u00085\u00106R2\u00107\u001a&\u0012\u000c\u0012\n ,*\u0004\u0018\u00010808 ,*\u0012\u0012\u000c\u0012\n ,*\u0004\u0018\u00010808\u0018\u00010*0*X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u00109\u001a&\u0012\u000c\u0012\n ,*\u0004\u0018\u00010:0: ,*\u0012\u0012\u000c\u0012\n ,*\u0004\u0018\u00010:0:\u0018\u00010*0*X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010;\u001a&\u0012\u000c\u0012\n ,*\u0004\u0018\u00010=0= ,*\u0012\u0012\u000c\u0012\n ,*\u0004\u0018\u00010=0=\u0018\u00010<0<X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010>\u001a&\u0012\u000c\u0012\n ,*\u0004\u0018\u00010?0? ,*\u0012\u0012\u000c\u0012\n ,*\u0004\u0018\u00010?0?\u0018\u00010*0*X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020 X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010@\u001a\u00020AX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010B\u001a\n ,*\u0004\u0018\u00010C0CX\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010D\u001a&\u0012\u000c\u0012\n ,*\u0004\u0018\u00010+0+ ,*\u0012\u0012\u000c\u0012\n ,*\u0004\u0018\u00010+0+\u0018\u00010*0*X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010E\u001a&\u0012\u000c\u0012\n ,*\u0004\u0018\u00010.0. ,*\u0012\u0012\u000c\u0012\n ,*\u0004\u0018\u00010.0.\u0018\u00010F0FX\u0082\u000e\u00a2\u0006\u0002\n\u0000Rb\u0010G\u001aV\u0012$\u0012\"\u0012\u0004\u0012\u00020I\u0012\u0004\u0012\u000202 ,*\u0010\u0012\u0004\u0012\u00020I\u0012\u0004\u0012\u000202\u0018\u00010H0H ,**\u0012$\u0012\"\u0012\u0004\u0012\u00020I\u0012\u0004\u0012\u000202 ,*\u0010\u0012\u0004\u0012\u00020I\u0012\u0004\u0012\u000202\u0018\u00010H0H\u0018\u00010<0<X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010J\u001a&\u0012\u000c\u0012\n ,*\u0004\u0018\u00010808 ,*\u0012\u0012\u000c\u0012\n ,*\u0004\u0018\u00010808\u0018\u00010<0<X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010K\u001a&\u0012\u000c\u0012\n ,*\u0004\u0018\u00010(0( ,*\u0012\u0012\u000c\u0012\n ,*\u0004\u0018\u00010(0(\u0018\u00010<0<X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010L\u001a&\u0012\u000c\u0012\n ,*\u0004\u0018\u00010(0( ,*\u0012\u0012\u000c\u0012\n ,*\u0004\u0018\u00010(0(\u0018\u00010<0<X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010M\u001a&\u0012\u000c\u0012\n ,*\u0004\u0018\u00010N0N ,*\u0012\u0012\u000c\u0012\n ,*\u0004\u0018\u00010N0N\u0018\u00010*0*X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\"X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0080\u0001"
    }
    d2 = {
        "Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;",
        "Lmortar/Scoped;",
        "Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;",
        "Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;",
        "Lcom/squareup/giftcard/activation/GiftCardHistoryScreen$Runner;",
        "Lcom/squareup/giftcard/activation/GiftCardClearBalanceScreen$Runner;",
        "Lcom/squareup/giftcard/activation/GiftCardChooseTypeScreen$Runner;",
        "flow",
        "Lflow/Flow;",
        "swipeBusWhenVisible",
        "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "threadEnforcer",
        "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
        "giftCardService",
        "Lcom/squareup/giftcard/GiftCardServiceHelper;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "maybeX2SellerScreenRunner",
        "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
        "bus",
        "Lcom/squareup/badbus/BadBus;",
        "res",
        "Lcom/squareup/util/Res;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "giftCards",
        "Lcom/squareup/giftcard/GiftCards;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "giftCardItemizer",
        "Lcom/squareup/giftcard/activation/GiftCardItemizer;",
        "topScreenChecker",
        "Lcom/squareup/ui/main/TopScreenChecker;",
        "(Lflow/Flow;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/giftcard/GiftCardServiceHelper;Lcom/squareup/settings/server/Features;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/badbus/BadBus;Lcom/squareup/util/Res;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/payment/Transaction;Lcom/squareup/giftcard/activation/GiftCardItemizer;Lcom/squareup/ui/main/TopScreenChecker;)V",
        "activateEGiftCardWorkflowRunner",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowRunner;",
        "autoAdvanceOnSwipe",
        "Lcom/squareup/util/RxWatchdog;",
        "",
        "busy",
        "Lcom/jakewharton/rxrelay/BehaviorRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "card",
        "Lcom/squareup/Card;",
        "chooseTypeBusy",
        "chooseTypeDisplayed",
        "clearReason",
        "Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;",
        "clearReason$annotations",
        "()V",
        "getClearReason",
        "()Lcom/jakewharton/rxrelay/BehaviorRelay;",
        "clearReasonText",
        "",
        "clearedBalance",
        "Lcom/squareup/protos/client/giftcards/ClearBalanceResponse;",
        "eGiftCardConfig",
        "Lcom/jakewharton/rxrelay/PublishRelay;",
        "Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;",
        "giftCardHistory",
        "Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;",
        "giftCardLoadingScope",
        "Lcom/squareup/giftcard/activation/GiftCardLoadingScope;",
        "idPair",
        "Lcom/squareup/protos/client/IdPair;",
        "isAddValueEnabledForHistory",
        "onCardSwiped",
        "Lrx/Observable;",
        "onClearBalance",
        "Lkotlin/Pair;",
        "Lcom/squareup/protos/client/giftcards/GiftCard;",
        "onFetchGiftCardHistory",
        "onRegisterGiftCard",
        "onStartActivateEGiftCard",
        "registerGiftCard",
        "Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;",
        "actionBarHasX",
        "addGiftCardToTransaction",
        "amount",
        "Lcom/squareup/protos/common/Money;",
        "giftCard",
        "activityType",
        "Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;",
        "canUseCashOut",
        "cancelAddValueScreen",
        "cancelChooseType",
        "cancelClearBalance",
        "cancelGiftCardLookup",
        "cancelHistoryScreen",
        "chooseTypeEnabled",
        "clearBalance",
        "clearLoadingState",
        "continueGiftCardLookup",
        "continueGiftCardLookupIfValid",
        "finishAddValueAndCloseActivationFlow",
        "finishClearBalanceAndCloseActivationFlow",
        "getBalance",
        "giftCardName",
        "goToAddValueScreen",
        "goToClearBalanceScreen",
        "isAddValueEnabled",
        "isPlasticCardFeatureEnabled",
        "isValidCard",
        "onCardSwipedInChooseTypeScreen",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "responseOrNull",
        "T",
        "successOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Ljava/lang/Object;",
        "setCard",
        "setClearReason",
        "newId",
        "",
        "setClearReasonText",
        "reasonText",
        "startActivateEGiftCard",
        "startActivatePhysicalGiftCard",
        "triggerClearBalance",
        "goBackPastChooseTypeScreen",
        "goBackUntilChooseTypeScreen",
        "ScreenData",
        "giftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private activateEGiftCardWorkflowRunner:Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowRunner;

.field private final autoAdvanceOnSwipe:Lcom/squareup/util/RxWatchdog;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/util/RxWatchdog<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final bus:Lcom/squareup/badbus/BadBus;

.field private final busy:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final card:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/Card;",
            ">;"
        }
    .end annotation
.end field

.field private final chooseTypeBusy:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private chooseTypeDisplayed:Z

.field private final clearReason:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;",
            ">;"
        }
    .end annotation
.end field

.field private final clearReasonText:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final clearedBalance:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/protos/client/giftcards/ClearBalanceResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final eGiftCardConfig:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private final giftCardHistory:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final giftCardItemizer:Lcom/squareup/giftcard/activation/GiftCardItemizer;

.field private giftCardLoadingScope:Lcom/squareup/giftcard/activation/GiftCardLoadingScope;

.field private final giftCardService:Lcom/squareup/giftcard/GiftCardServiceHelper;

.field private final giftCards:Lcom/squareup/giftcard/GiftCards;

.field private final idPair:Lcom/squareup/protos/client/IdPair;

.field private final isAddValueEnabledForHistory:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final maybeX2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

.field private onCardSwiped:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/squareup/Card;",
            ">;"
        }
    .end annotation
.end field

.field private final onClearBalance:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Pair<",
            "Lcom/squareup/protos/client/giftcards/GiftCard;",
            "Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;",
            ">;>;"
        }
    .end annotation
.end field

.field private final onFetchGiftCardHistory:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final onRegisterGiftCard:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onStartActivateEGiftCard:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final registerGiftCard:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final swipeBusWhenVisible:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

.field private final topScreenChecker:Lcom/squareup/ui/main/TopScreenChecker;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lflow/Flow;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/giftcard/GiftCardServiceHelper;Lcom/squareup/settings/server/Features;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/badbus/BadBus;Lcom/squareup/util/Res;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/payment/Transaction;Lcom/squareup/giftcard/activation/GiftCardItemizer;Lcom/squareup/ui/main/TopScreenChecker;)V
    .locals 1
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p4    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "swipeBusWhenVisible"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "threadEnforcer"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "giftCardService"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "maybeX2SellerScreenRunner"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bus"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "giftCards"

    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transaction"

    invoke-static {p12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "giftCardItemizer"

    invoke-static {p13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "topScreenChecker"

    invoke-static {p14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->flow:Lflow/Flow;

    iput-object p2, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->swipeBusWhenVisible:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    iput-object p5, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->giftCardService:Lcom/squareup/giftcard/GiftCardServiceHelper;

    iput-object p6, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->features:Lcom/squareup/settings/server/Features;

    iput-object p7, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->maybeX2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iput-object p8, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->bus:Lcom/squareup/badbus/BadBus;

    iput-object p9, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->res:Lcom/squareup/util/Res;

    iput-object p10, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p11, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->giftCards:Lcom/squareup/giftcard/GiftCards;

    iput-object p12, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    iput-object p13, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->giftCardItemizer:Lcom/squareup/giftcard/activation/GiftCardItemizer;

    iput-object p14, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->topScreenChecker:Lcom/squareup/ui/main/TopScreenChecker;

    .line 118
    new-instance p1, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    .line 119
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p2

    invoke-virtual {p2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/IdPair$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object p1

    .line 120
    invoke-virtual {p1}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->idPair:Lcom/squareup/protos/client/IdPair;

    .line 125
    invoke-static {}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinatorKt;->getEMPTY_CARD()Lcom/squareup/Card;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->card:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 126
    new-instance p1, Lcom/squareup/util/RxWatchdog;

    invoke-direct {p1, p3, p4}, Lcom/squareup/util/RxWatchdog;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->autoAdvanceOnSwipe:Lcom/squareup/util/RxWatchdog;

    const/4 p1, 0x0

    .line 127
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 p1, 0x1

    .line 128
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->chooseTypeBusy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 131
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->onRegisterGiftCard:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 132
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->onFetchGiftCardHistory:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 133
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->registerGiftCard:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 134
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->giftCardHistory:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 135
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->onClearBalance:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 136
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->clearedBalance:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 139
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p2

    const-string p3, "BehaviorRelay.create()"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->clearReason:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 140
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->clearReasonText:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 141
    invoke-static {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->isAddValueEnabledForHistory:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 144
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->eGiftCardConfig:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 145
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->onStartActivateEGiftCard:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 147
    invoke-static {}, Lrx/Observable;->empty()Lrx/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->onCardSwiped:Lrx/Observable;

    return-void
.end method

.method public static final synthetic access$getActivateEGiftCardWorkflowRunner$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowRunner;
    .locals 1

    .line 96
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->activateEGiftCardWorkflowRunner:Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowRunner;

    if-nez p0, :cond_0

    const-string v0, "activateEGiftCardWorkflowRunner"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getAutoAdvanceOnSwipe$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/squareup/util/RxWatchdog;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->autoAdvanceOnSwipe:Lcom/squareup/util/RxWatchdog;

    return-object p0
.end method

.method public static final synthetic access$getBusy$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getCard$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->card:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getChooseTypeBusy$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->chooseTypeBusy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getChooseTypeDisplayed$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Z
    .locals 0

    .line 96
    iget-boolean p0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->chooseTypeDisplayed:Z

    return p0
.end method

.method public static final synthetic access$getClearReasonText$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->clearReasonText:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getCurrencyCode$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/squareup/protos/common/CurrencyCode;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-object p0
.end method

.method public static final synthetic access$getEGiftCardConfig$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/jakewharton/rxrelay/PublishRelay;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->eGiftCardConfig:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object p0
.end method

.method public static final synthetic access$getFlow$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lflow/Flow;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->flow:Lflow/Flow;

    return-object p0
.end method

.method public static final synthetic access$getGiftCardItemizer$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/squareup/giftcard/activation/GiftCardItemizer;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->giftCardItemizer:Lcom/squareup/giftcard/activation/GiftCardItemizer;

    return-object p0
.end method

.method public static final synthetic access$getGiftCardLoadingScope$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/squareup/giftcard/activation/GiftCardLoadingScope;
    .locals 1

    .line 96
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->giftCardLoadingScope:Lcom/squareup/giftcard/activation/GiftCardLoadingScope;

    if-nez p0, :cond_0

    const-string v0, "giftCardLoadingScope"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getGiftCardService$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/squareup/giftcard/GiftCardServiceHelper;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->giftCardService:Lcom/squareup/giftcard/GiftCardServiceHelper;

    return-object p0
.end method

.method public static final synthetic access$getGiftCards$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/squareup/giftcard/GiftCards;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->giftCards:Lcom/squareup/giftcard/GiftCards;

    return-object p0
.end method

.method public static final synthetic access$getIdPair$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/squareup/protos/client/IdPair;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->idPair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public static final synthetic access$getMaybeX2SellerScreenRunner$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/squareup/x2/MaybeX2SellerScreenRunner;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->maybeX2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    return-object p0
.end method

.method public static final synthetic access$getOnClearBalance$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/jakewharton/rxrelay/PublishRelay;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->onClearBalance:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object p0
.end method

.method public static final synthetic access$getOnFetchGiftCardHistory$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/jakewharton/rxrelay/PublishRelay;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->onFetchGiftCardHistory:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/squareup/util/Res;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$getTransaction$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/squareup/payment/Transaction;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    return-object p0
.end method

.method public static final synthetic access$goBackPastChooseTypeScreen(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;Lflow/Flow;)V
    .locals 0

    .line 96
    invoke-direct {p0, p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->goBackPastChooseTypeScreen(Lflow/Flow;)V

    return-void
.end method

.method public static final synthetic access$goBackUntilChooseTypeScreen(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;Lflow/Flow;)V
    .locals 0

    .line 96
    invoke-direct {p0, p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->goBackUntilChooseTypeScreen(Lflow/Flow;)V

    return-void
.end method

.method public static final synthetic access$isAddValueEnabledForHistory$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->isAddValueEnabledForHistory:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$responseOrNull(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Ljava/lang/Object;
    .locals 0

    .line 96
    invoke-direct {p0, p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->responseOrNull(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$setActivateEGiftCardWorkflowRunner$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowRunner;)V
    .locals 0

    .line 96
    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->activateEGiftCardWorkflowRunner:Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowRunner;

    return-void
.end method

.method public static final synthetic access$setChooseTypeDisplayed$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;Z)V
    .locals 0

    .line 96
    iput-boolean p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->chooseTypeDisplayed:Z

    return-void
.end method

.method public static final synthetic access$setGiftCardLoadingScope$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;Lcom/squareup/giftcard/activation/GiftCardLoadingScope;)V
    .locals 0

    .line 96
    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->giftCardLoadingScope:Lcom/squareup/giftcard/activation/GiftCardLoadingScope;

    return-void
.end method

.method private final chooseTypeEnabled()Z
    .locals 2

    .line 653
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->EGIFT_CARD_IN_POS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method private final clearLoadingState()V
    .locals 2

    .line 602
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->card:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinatorKt;->getEMPTY_CARD()Lcom/squareup/Card;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 603
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->registerGiftCard:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunnerKt;->getEMPTY_REGISTER_CARD()Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 604
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->giftCardHistory:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {}, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinatorKt;->getEMPTY_GIFT_CARD_HISTORY()Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic clearReason$annotations()V
    .locals 0

    return-void
.end method

.method private final goBackPastChooseTypeScreen(Lflow/Flow;)V
    .locals 3

    .line 350
    new-instance v0, Lcom/squareup/container/CalculatedKey;

    sget-object v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$goBackPastChooseTypeScreen$1;->INSTANCE:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$goBackPastChooseTypeScreen$1;

    check-cast v1, Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;

    const-string v2, "goBackPastGiftCardChooseTypeScreen"

    invoke-direct {v0, v2, v1}, Lcom/squareup/container/CalculatedKey;-><init>(Ljava/lang/String;Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private final goBackUntilChooseTypeScreen(Lflow/Flow;)V
    .locals 3

    .line 360
    new-instance v0, Lcom/squareup/container/CalculatedKey;

    sget-object v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$goBackUntilChooseTypeScreen$1;->INSTANCE:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$goBackUntilChooseTypeScreen$1;

    check-cast v1, Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;

    const-string v2, "goBackUntilGiftCardChooseTypeScreen"

    invoke-direct {v0, v2, v1}, Lcom/squareup/container/CalculatedKey;-><init>(Ljava/lang/String;Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private final responseOrNull(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "+TT;>;)TT;"
        }
    .end annotation

    .line 375
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 376
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method


# virtual methods
.method public actionBarHasX()Z
    .locals 1

    .line 404
    iget-boolean v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->chooseTypeDisplayed:Z

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public addGiftCardToTransaction(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;)V
    .locals 7

    const-string v0, "amount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "giftCard"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activityType"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 648
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    .line 649
    iget-object v1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->giftCardItemizer:Lcom/squareup/giftcard/activation/GiftCardItemizer;

    iget-object v5, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->idPair:Lcom/squareup/protos/client/IdPair;

    const-string v2, "idPair"

    invoke-static {v5, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v1 .. v6}, Lcom/squareup/giftcard/activation/GiftCardItemizer;->createGiftCardItemization(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;Lcom/squareup/protos/client/IdPair;Z)Lcom/squareup/checkout/CartItem;

    move-result-object p1

    .line 648
    invoke-virtual {v0, p1}, Lcom/squareup/payment/Transaction;->addOrderItem(Lcom/squareup/checkout/CartItem;)V

    return-void
.end method

.method public busy()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 505
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string v1, "busy"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lrx/Observable;

    return-object v0
.end method

.method public canUseCashOut()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 594
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_CASH_OUT_REASON:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.just(features\u2026led(USE_CASH_OUT_REASON))"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public cancelAddValueScreen()V
    .locals 1

    .line 575
    invoke-direct {p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->clearLoadingState()V

    .line 576
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public cancelChooseType()V
    .locals 2

    .line 391
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/giftcard/activation/GiftCardChooseTypeScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public cancelClearBalance()V
    .locals 1

    .line 566
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public cancelGiftCardLookup()V
    .locals 2

    .line 580
    invoke-direct {p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->clearLoadingState()V

    .line 581
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/giftcard/activation/GiftCardLookupScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public cancelHistoryScreen()V
    .locals 2

    .line 570
    invoke-direct {p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->clearLoadingState()V

    .line 571
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/giftcard/activation/GiftCardHistoryScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public card()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/Card;",
            ">;"
        }
    .end annotation

    .line 406
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->card:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string v1, "card"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lrx/Observable;

    return-object v0
.end method

.method public chooseTypeBusy()Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 393
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->chooseTypeBusy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method public bridge synthetic chooseTypeBusy()Lrx/Observable;
    .locals 1

    .line 96
    invoke-virtual {p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->chooseTypeBusy()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    check-cast v0, Lrx/Observable;

    return-object v0
.end method

.method public clearBalance()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/giftcards/ClearBalanceResponse;",
            ">;"
        }
    .end annotation

    .line 509
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->clearedBalance:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string v1, "clearedBalance"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lrx/Observable;

    return-object v0
.end method

.method public continueGiftCardLookup()V
    .locals 4

    .line 396
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardHistoryScreen;

    iget-object v2, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->giftCardLoadingScope:Lcom/squareup/giftcard/activation/GiftCardLoadingScope;

    if-nez v2, :cond_0

    const-string v3, "giftCardLoadingScope"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v2, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v1, v2}, Lcom/squareup/giftcard/activation/GiftCardHistoryScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public continueGiftCardLookupIfValid(Lcom/squareup/Card;)V
    .locals 1

    const-string v0, "card"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 399
    invoke-virtual {p0, p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->isValidCard(Lcom/squareup/Card;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 400
    invoke-virtual {p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->continueGiftCardLookup()V

    :cond_0
    return-void
.end method

.method public finishAddValueAndCloseActivationFlow()V
    .locals 2

    .line 585
    invoke-direct {p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->clearLoadingState()V

    .line 586
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/giftcard/activation/GiftCardAddValueScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->closeCard(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public finishClearBalanceAndCloseActivationFlow()V
    .locals 2

    .line 590
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/giftcard/activation/GiftCardClearBalanceScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->closeCard(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public getBalance()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 554
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->registerGiftCard:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string v1, "registerGiftCard"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;

    iget-object v0, v0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;->gift_card:Lcom/squareup/protos/client/giftcards/GiftCard;

    iget-object v0, v0, Lcom/squareup/protos/client/giftcards/GiftCard;->balance_money:Lcom/squareup/protos/common/Money;

    const-string v1, "registerGiftCard.value.gift_card.balance_money"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getClearReason()Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;",
            ">;"
        }
    .end annotation

    .line 138
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->clearReason:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method public giftCardHistory()Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;",
            ">;"
        }
    .end annotation

    .line 515
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->registerGiftCard:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->registerGiftCard:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string v1, "registerGiftCard"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;

    invoke-static {}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunnerKt;->getEMPTY_REGISTER_CARD()Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;

    move-result-object v2

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 539
    :cond_0
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->onFetchGiftCardHistory:Lcom/jakewharton/rxrelay/PublishRelay;

    iget-object v2, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->registerGiftCard:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;

    iget-object v1, v1, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;->gift_card:Lcom/squareup/protos/client/giftcards/GiftCard;

    iget-object v1, v1, Lcom/squareup/protos/client/giftcards/GiftCard;->server_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    goto :goto_1

    .line 516
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->onRegisterGiftCard:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 517
    invoke-virtual {p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->registerGiftCard()Lrx/Observable;

    move-result-object v0

    .line 518
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$giftCardHistory$1;

    invoke-direct {v1, p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$giftCardHistory$1;-><init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V

    check-cast v1, Lrx/functions/Action0;

    invoke-virtual {v0, v1}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    .line 519
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$giftCardHistory$2;

    invoke-direct {v1, p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$giftCardHistory$2;-><init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V

    check-cast v1, Lrx/functions/Action0;

    invoke-virtual {v0, v1}, Lrx/Observable;->doOnTerminate(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    .line 520
    sget-object v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$giftCardHistory$3;->INSTANCE:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$giftCardHistory$3;

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 521
    sget-object v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$giftCardHistory$4;->INSTANCE:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$giftCardHistory$4;

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 522
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$giftCardHistory$5;

    invoke-direct {v1, p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$giftCardHistory$5;-><init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    .line 541
    :goto_1
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->giftCardHistory:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    const-string v1, "giftCardHistory.distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public giftCardName()Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 419
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->giftCardLoadingScope:Lcom/squareup/giftcard/activation/GiftCardLoadingScope;

    const-string v1, "giftCardLoadingScope"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScope;->getMaybeGiftCard()Lcom/squareup/protos/client/giftcards/GiftCard;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 422
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->res:Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->giftCardLoadingScope:Lcom/squareup/giftcard/activation/GiftCardLoadingScope;

    if-nez v2, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v2}, Lcom/squareup/giftcard/activation/GiftCardLoadingScope;->getMaybeGiftCard()Lcom/squareup/protos/client/giftcards/GiftCard;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    iget-object v2, v2, Lcom/squareup/protos/client/giftcards/GiftCard;->pan_suffix:Ljava/lang/String;

    .line 423
    iget-object v3, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->giftCardLoadingScope:Lcom/squareup/giftcard/activation/GiftCardLoadingScope;

    if-nez v3, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v3}, Lcom/squareup/giftcard/activation/GiftCardLoadingScope;->getMaybeGiftCard()Lcom/squareup/protos/client/giftcards/GiftCard;

    move-result-object v1

    if-nez v1, :cond_4

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_4
    iget-object v1, v1, Lcom/squareup/protos/client/giftcards/GiftCard;->card_type:Lcom/squareup/protos/client/giftcards/GiftCard$CardType;

    sget-object v3, Lcom/squareup/protos/client/giftcards/GiftCard$CardType;->ELECTRONIC:Lcom/squareup/protos/client/giftcards/GiftCard$CardType;

    if-ne v1, v3, :cond_5

    const/4 v1, 0x1

    goto :goto_0

    :cond_5
    const/4 v1, 0x0

    .line 421
    :goto_0
    invoke-static {v0, v2, v1}, Lcom/squareup/text/Cards;->formattedGiftCard(Lcom/squareup/util/Res;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 420
    invoke-static {v0}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    goto :goto_1

    .line 427
    :cond_6
    invoke-virtual {p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->card()Lrx/Observable;

    move-result-object v0

    .line 428
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$giftCardName$1;

    invoke-direct {v1, p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$giftCardName$1;-><init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    :goto_1
    return-object v0
.end method

.method public goToAddValueScreen()V
    .locals 4

    .line 558
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardAddValueScreen;

    iget-object v2, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->giftCardLoadingScope:Lcom/squareup/giftcard/activation/GiftCardLoadingScope;

    if-nez v2, :cond_0

    const-string v3, "giftCardLoadingScope"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v2, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v1, v2}, Lcom/squareup/giftcard/activation/GiftCardAddValueScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    check-cast v1, Lcom/squareup/container/ContainerTreeKey;

    sget-object v2, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->replaceTop(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    return-void
.end method

.method public goToClearBalanceScreen()V
    .locals 4

    .line 562
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardClearBalanceScreen;

    iget-object v2, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->giftCardLoadingScope:Lcom/squareup/giftcard/activation/GiftCardLoadingScope;

    if-nez v2, :cond_0

    const-string v3, "giftCardLoadingScope"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v2, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v1, v2}, Lcom/squareup/giftcard/activation/GiftCardClearBalanceScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public isAddValueEnabled()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 545
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->isAddValueEnabledForHistory:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    const-string v1, "isAddValueEnabledForHistory.distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public isPlasticCardFeatureEnabled()Z
    .locals 2

    .line 491
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->GIFT_CARDS_SHOW_PLASTIC_GIFT_CARDS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public isValidCard(Lcom/squareup/Card;)Z
    .locals 4

    const-string v0, "card"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 480
    invoke-virtual {p1}, Lcom/squareup/Card;->getPan()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinatorKt;->getEMPTY_CARD()Lcom/squareup/Card;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 483
    :cond_0
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->giftCards:Lcom/squareup/giftcard/GiftCards;

    invoke-virtual {v0}, Lcom/squareup/giftcard/GiftCards;->isThirdPartyGiftCardFeatureEnabled()Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    .line 484
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->giftCards:Lcom/squareup/giftcard/GiftCards;

    invoke-virtual {p1}, Lcom/squareup/Card;->getPan()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/squareup/giftcard/GiftCards;->isValidThirdPartyGiftCardPan(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->giftCards:Lcom/squareup/giftcard/GiftCards;

    invoke-virtual {v0, p1}, Lcom/squareup/giftcard/GiftCards;->isPossiblyGiftCard(Lcom/squareup/Card;)Z

    move-result p1

    if-eqz p1, :cond_3

    :cond_1
    :goto_0
    const/4 v1, 0x1

    goto :goto_1

    .line 486
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/Card;->getUnmaskedDigits()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->giftCards:Lcom/squareup/giftcard/GiftCards;

    invoke-virtual {v0, p1}, Lcom/squareup/giftcard/GiftCards;->isPossiblyGiftCard(Lcom/squareup/Card;)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_0

    :cond_3
    :goto_1
    return v1
.end method

.method public onCardSwiped()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/Card;",
            ">;"
        }
    .end annotation

    .line 454
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->onCardSwiped:Lrx/Observable;

    const-string v1, "onCardSwiped"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onCardSwipedInChooseTypeScreen()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 461
    invoke-virtual {p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->onCardSwiped()Lrx/Observable;

    move-result-object v0

    .line 462
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onCardSwipedInChooseTypeScreen$1;

    invoke-direct {v1, p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onCardSwipedInChooseTypeScreen$1;-><init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    const-string v1, "onCardSwiped()\n        .\u2026\n          Unit\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 8

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    const-string v1, "RegisterTreeKey.get(scope)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/giftcard/activation/GiftCardLoadingScope;

    iput-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->giftCardLoadingScope:Lcom/squareup/giftcard/activation/GiftCardLoadingScope;

    .line 152
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->giftCardLoadingScope:Lcom/squareup/giftcard/activation/GiftCardLoadingScope;

    const-string v1, "giftCardLoadingScope"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScope;->getMaybeGiftCard()Lcom/squareup/protos/client/giftcards/GiftCard;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v0, :cond_2

    .line 155
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->registerGiftCard:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 156
    new-instance v4, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse$Builder;

    invoke-direct {v4}, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse$Builder;-><init>()V

    .line 157
    iget-object v5, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->giftCardLoadingScope:Lcom/squareup/giftcard/activation/GiftCardLoadingScope;

    if-nez v5, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v5}, Lcom/squareup/giftcard/activation/GiftCardLoadingScope;->getMaybeGiftCard()Lcom/squareup/protos/client/giftcards/GiftCard;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse$Builder;->gift_card(Lcom/squareup/protos/client/giftcards/GiftCard;)Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse$Builder;

    move-result-object v1

    .line 158
    new-instance v4, Lcom/squareup/protos/client/Status$Builder;

    invoke-direct {v4}, Lcom/squareup/protos/client/Status$Builder;-><init>()V

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/protos/client/Status$Builder;->success(Ljava/lang/Boolean;)Lcom/squareup/protos/client/Status$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/protos/client/Status$Builder;->build()Lcom/squareup/protos/client/Status;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse$Builder;->status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse$Builder;

    move-result-object v1

    .line 159
    invoke-virtual {v1}, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse$Builder;->build()Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;

    move-result-object v1

    .line 155
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 161
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->isAddValueEnabledForHistory:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    goto :goto_0

    .line 162
    :cond_2
    invoke-direct {p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->chooseTypeEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 163
    iput-boolean v3, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->chooseTypeDisplayed:Z

    .line 165
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->giftCardService:Lcom/squareup/giftcard/GiftCardServiceHelper;

    invoke-virtual {v0}, Lcom/squareup/giftcard/GiftCardServiceHelper;->getEGiftCardOrderConfiguration()Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "giftCardService.getEGiftCardOrderConfiguration()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 166
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$1;-><init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Single;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 183
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->eGiftCardConfig:Lcom/jakewharton/rxrelay/PublishRelay;

    check-cast v0, Lrx/Observable;

    iget-object v1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->onStartActivateEGiftCard:Lcom/jakewharton/rxrelay/PublishRelay;

    check-cast v1, Lrx/Observable;

    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v4

    invoke-static {v0, v1, v4}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    const-string v1, "combineLatest(eGiftCardC\u2026ivateEGiftCard, toPair())"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 184
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$2;-><init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/ObservablesKt;->subscribeWith(Lrx/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lrx/Subscription;

    .line 189
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->autoAdvanceOnSwipe:Lcom/squareup/util/RxWatchdog;

    invoke-virtual {v0}, Lcom/squareup/util/RxWatchdog;->timeout()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "autoAdvanceOnSwipe.timeout()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 190
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$3;

    invoke-direct {v1, p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$3;-><init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 195
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->swipeBusWhenVisible:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    invoke-virtual {v0}, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;->successfulSwipes()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "swipeBusWhenVisible.successfulSwipes()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/ObservableSource;

    .line 196
    sget-object v1, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    .line 197
    sget-object v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$swipedCard$1;->INSTANCE:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$swipedCard$1;

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 198
    iget-object v1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->card:Lcom/jakewharton/rxrelay/BehaviorRelay;

    check-cast v1, Lrx/Observable;

    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    .line 200
    sget-object v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$swipedCard$2;->INSTANCE:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$swipedCard$2;

    check-cast v1, Lrx/functions/Func2;

    invoke-static {v1}, Lcom/squareup/util/RxTuples;->expandPairForFunc(Lrx/functions/Func2;)Lrx/functions/Func1;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 201
    sget-object v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$swipedCard$3;->INSTANCE:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$swipedCard$3;

    check-cast v1, Lrx/functions/Func2;

    invoke-static {v1}, Lcom/squareup/util/RxTuples;->expandPairForFunc(Lrx/functions/Func2;)Lrx/functions/Func1;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 204
    new-instance v1, Lcom/squareup/Card$Builder;

    invoke-direct {v1}, Lcom/squareup/Card$Builder;-><init>()V

    .line 205
    sget-object v4, Lcom/squareup/Card$Brand;->VISA:Lcom/squareup/Card$Brand;

    invoke-virtual {v1, v4}, Lcom/squareup/Card$Builder;->brand(Lcom/squareup/Card$Brand;)Lcom/squareup/Card$Builder;

    move-result-object v1

    .line 206
    invoke-virtual {v1}, Lcom/squareup/Card$Builder;->build()Lcom/squareup/Card;

    move-result-object v1

    .line 208
    iget-object v4, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->giftCards:Lcom/squareup/giftcard/GiftCards;

    invoke-virtual {v4, v1}, Lcom/squareup/giftcard/GiftCards;->isPossiblyGiftCard(Lcom/squareup/Card;)Z

    move-result v4

    xor-int/2addr v4, v3

    const-string v5, "Fake chip card must fail gift card check."

    .line 207
    invoke-static {v4, v5}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 212
    iget-object v4, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->bus:Lcom/squareup/badbus/BadBus;

    const-class v5, Lcom/squareup/comms/protos/buyer/OnChipCardSwipedInPlaceOfGiftCard;

    invoke-virtual {v4, v5}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v4

    .line 213
    new-instance v5, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$x2SwipedChipCard$1;

    invoke-direct {v5, v1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$x2SwipedChipCard$1;-><init>(Lcom/squareup/Card;)V

    check-cast v5, Lio/reactivex/functions/Function;

    invoke-virtual {v4, v5}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    .line 215
    iget-object v4, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->bus:Lcom/squareup/badbus/BadBus;

    const-class v5, Lcom/squareup/x2/X2SwipedGiftCard;

    invoke-virtual {v4, v5}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v4

    .line 216
    sget-object v5, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$x2SwipedGiftCard$1;->INSTANCE:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$x2SwipedGiftCard$1;

    check-cast v5, Lio/reactivex/functions/Function;

    invoke-virtual {v4, v5}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v4

    .line 218
    iget-object v5, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->topScreenChecker:Lcom/squareup/ui/main/TopScreenChecker;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Class;

    .line 219
    const-class v7, Lcom/squareup/giftcard/activation/GiftCardLookupScreen;

    aput-object v7, v6, v2

    const-class v2, Lcom/squareup/giftcard/activation/GiftCardChooseTypeScreen;

    aput-object v2, v6, v3

    invoke-static {v6}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/squareup/ui/main/TopScreenChecker;->unobscured(Ljava/util/List;)Lio/reactivex/Observable;

    move-result-object v2

    const-string/jumbo v3, "topScreenChecker\n       \u2026eTypeScreen::class.java))"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lio/reactivex/ObservableSource;

    .line 220
    sget-object v3, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v2, v3}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v2

    const-string/jumbo v3, "x2SwipedGiftCard"

    .line 224
    invoke-static {v4, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lio/reactivex/ObservableSource;

    sget-object v3, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v4, v3}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v3

    const-string/jumbo v4, "x2SwipedChipCard"

    .line 225
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/ObservableSource;

    sget-object v4, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v1, v4}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v1

    .line 222
    invoke-static {v0, v3, v1}, Lrx/Observable;->merge(Lrx/Observable;Lrx/Observable;Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    .line 227
    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    .line 228
    sget-object v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$4;->INSTANCE:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$4;

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 229
    sget-object v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$5;->INSTANCE:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$5;

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->onCardSwiped:Lrx/Observable;

    .line 233
    invoke-virtual {p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->card()Lrx/Observable;

    move-result-object v0

    .line 235
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$6;

    invoke-direct {v1, p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$6;-><init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 236
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$7;

    invoke-direct {v1, p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$7;-><init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "card()\n            // On\u2026S, SECONDS)\n            }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 232
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 242
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->onRegisterGiftCard:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 243
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$8;

    invoke-direct {v1, p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$8;-><init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 250
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$9;

    invoke-direct {v1, p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$9;-><init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 251
    iget-object v1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->registerGiftCard:Lcom/jakewharton/rxrelay/BehaviorRelay;

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "onRegisterGiftCard\n     \u2026bscribe(registerGiftCard)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 241
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 255
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->onClearBalance:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 256
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$10;

    invoke-direct {v1, p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$10;-><init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 266
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$11;

    invoke-direct {v1, p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$11;-><init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 267
    iget-object v1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->clearedBalance:Lcom/jakewharton/rxrelay/BehaviorRelay;

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "onClearBalance\n         \u2026subscribe(clearedBalance)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 254
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 271
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->onFetchGiftCardHistory:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 272
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$12;

    invoke-direct {v1, p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$12;-><init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 279
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$13;

    invoke-direct {v1, p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$13;-><init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 280
    iget-object v1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->giftCardHistory:Lcom/jakewharton/rxrelay/BehaviorRelay;

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "onFetchGiftCardHistory\n \u2026ubscribe(giftCardHistory)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 270
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 284
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->maybeX2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->badIsHodorCheck()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 285
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->onCardSwiped:Lrx/Observable;

    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$swipeScreenData$1;

    invoke-direct {v1, p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$swipeScreenData$1;-><init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 294
    iget-object v1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->registerGiftCard:Lcom/jakewharton/rxrelay/BehaviorRelay;

    check-cast v1, Lrx/Observable;

    iget-object v2, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->card:Lcom/jakewharton/rxrelay/BehaviorRelay;

    check-cast v2, Lrx/Observable;

    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v1

    .line 295
    new-instance v2, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$responseScreenData$1;

    invoke-direct {v2, p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$responseScreenData$1;-><init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V

    check-cast v2, Lrx/functions/Func2;

    invoke-static {v2}, Lcom/squareup/util/RxTuples;->expandPairForFunc(Lrx/functions/Func2;)Lrx/functions/Func1;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v1

    .line 298
    new-instance v2, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$responseScreenData$2;

    invoke-direct {v2, p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$responseScreenData$2;-><init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V

    check-cast v2, Lrx/functions/Func2;

    invoke-static {v2}, Lcom/squareup/util/RxTuples;->expandPairForFunc(Lrx/functions/Func2;)Lrx/functions/Func1;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v1

    .line 314
    invoke-static {v0, v1}, Lrx/Observable;->merge(Lrx/Observable;Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    .line 315
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$14;

    invoke-direct {v1, p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$14;-><init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "Observable.merge(swipeSc\u2026            }\n          }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 314
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 328
    :cond_4
    invoke-static {p1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowRunnerKt;->getActivateEGiftCardWorkflowRunner(Lmortar/MortarScope;)Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowRunner;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->activateEGiftCardWorkflowRunner:Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowRunner;

    .line 331
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->activateEGiftCardWorkflowRunner:Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowRunner;

    const-string v1, "activateEGiftCardWorkflowRunner"

    if-nez v0, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-interface {v0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    .line 332
    new-instance v2, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$15;

    invoke-direct {v2, p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$15;-><init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v2}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 334
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->activateEGiftCardWorkflowRunner:Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowRunner;

    if-nez v0, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-interface {v0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    .line 335
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$16;

    invoke-direct {v1, p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$16;-><init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 381
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->maybeX2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->dismissGiftCardBalanceCheck()Z

    return-void
.end method

.method public registerGiftCard()Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;",
            ">;"
        }
    .end annotation

    .line 497
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->card:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string v1, "card"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/Card;

    invoke-static {}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinatorKt;->getEMPTY_CARD()Lcom/squareup/Card;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const-string v1, "registerGiftCard"

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->registerGiftCard:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->registerGiftCard:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;

    invoke-static {}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunnerKt;->getEMPTY_REGISTER_CARD()Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;

    move-result-object v2

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 499
    :cond_0
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->onRegisterGiftCard:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v2}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 501
    :cond_1
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->registerGiftCard:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lrx/Observable;

    return-object v0
.end method

.method public setCard(Lcom/squareup/Card;)V
    .locals 1

    const-string v0, "card"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 409
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->card:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 412
    invoke-static {}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinatorKt;->getEMPTY_CARD()Lcom/squareup/Card;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 413
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->registerGiftCard:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunnerKt;->getEMPTY_REGISTER_CARD()Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public setClearReason(I)V
    .locals 1

    .line 441
    sget v0, Lcom/squareup/giftcard/activation/R$id;->cash_out_reason:I

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->clearReason:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;->CASH_OUT:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    goto :goto_0

    .line 442
    :cond_0
    sget v0, Lcom/squareup/giftcard/activation/R$id;->lost_card_reason:I

    if-ne p1, v0, :cond_1

    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->clearReason:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;->LOST:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    goto :goto_0

    .line 443
    :cond_1
    sget v0, Lcom/squareup/giftcard/activation/R$id;->reset_card_reason:I

    if-ne p1, v0, :cond_2

    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->clearReason:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;->RESET:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    goto :goto_0

    .line 444
    :cond_2
    sget v0, Lcom/squareup/giftcard/activation/R$id;->other_reason:I

    if-ne p1, v0, :cond_3

    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->clearReason:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;->OTHER:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :goto_0
    return-void

    .line 445
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Unknown Reason for Clear Balance!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public setClearReasonText(Ljava/lang/String;)V
    .locals 1

    const-string v0, "reasonText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 450
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->clearReasonText:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public startActivateEGiftCard()V
    .locals 2

    .line 388
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->onStartActivateEGiftCard:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public startActivatePhysicalGiftCard()V
    .locals 4

    .line 385
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardLookupScreen;

    iget-object v2, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->giftCardLoadingScope:Lcom/squareup/giftcard/activation/GiftCardLoadingScope;

    if-nez v2, :cond_0

    const-string v3, "giftCardLoadingScope"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v2, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v1, v2}, Lcom/squareup/giftcard/activation/GiftCardLookupScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public triggerClearBalance()V
    .locals 3

    .line 549
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->registerGiftCard:Lcom/jakewharton/rxrelay/BehaviorRelay;

    check-cast v0, Lrx/Observable;

    iget-object v1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->clearReason:Lcom/jakewharton/rxrelay/BehaviorRelay;

    check-cast v1, Lrx/Observable;

    sget-object v2, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$triggerClearBalance$1;->INSTANCE:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$triggerClearBalance$1;

    check-cast v2, Lrx/functions/Func2;

    invoke-static {v0, v1, v2}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    .line 550
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$triggerClearBalance$2;

    invoke-direct {v1, p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$triggerClearBalance$2;-><init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    return-void
.end method
