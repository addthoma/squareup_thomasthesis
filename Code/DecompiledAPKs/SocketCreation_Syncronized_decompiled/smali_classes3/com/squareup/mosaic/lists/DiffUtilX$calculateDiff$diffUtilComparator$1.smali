.class public final Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$diffUtilComparator$1;
.super Landroidx/recyclerview/widget/DiffUtil$Callback;
.source "DiffUtilX.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/mosaic/lists/DiffUtilX;->calculateDiff(Lcom/squareup/mosaic/lists/DiffUtilX$Comparator;Lcom/squareup/mosaic/lists/DiffUtilX$Results;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0005*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0005H\u0016J\u0018\u0010\u0007\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0005H\u0016J\u0008\u0010\u0008\u001a\u00020\u0005H\u0016J\u0008\u0010\t\u001a\u00020\u0005H\u0016\u00a8\u0006\n"
    }
    d2 = {
        "com/squareup/mosaic/lists/DiffUtilX$calculateDiff$diffUtilComparator$1",
        "Landroidx/recyclerview/widget/DiffUtil$Callback;",
        "areContentsTheSame",
        "",
        "oldPos",
        "",
        "newPos",
        "areItemsTheSame",
        "getNewListSize",
        "getOldListSize",
        "mosaic-core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $comparator:Lcom/squareup/mosaic/lists/DiffUtilX$Comparator;


# direct methods
.method constructor <init>(Lcom/squareup/mosaic/lists/DiffUtilX$Comparator;)V
    .locals 0

    .line 40
    iput-object p1, p0, Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$diffUtilComparator$1;->$comparator:Lcom/squareup/mosaic/lists/DiffUtilX$Comparator;

    invoke-direct {p0}, Landroidx/recyclerview/widget/DiffUtil$Callback;-><init>()V

    return-void
.end method


# virtual methods
.method public areContentsTheSame(II)Z
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$diffUtilComparator$1;->$comparator:Lcom/squareup/mosaic/lists/DiffUtilX$Comparator;

    invoke-interface {v0, p1, p2}, Lcom/squareup/mosaic/lists/DiffUtilX$Comparator;->areContentsTheSame(II)Z

    move-result p1

    return p1
.end method

.method public areItemsTheSame(II)Z
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$diffUtilComparator$1;->$comparator:Lcom/squareup/mosaic/lists/DiffUtilX$Comparator;

    invoke-interface {v0, p1, p2}, Lcom/squareup/mosaic/lists/DiffUtilX$Comparator;->areItemsTheSame(II)Z

    move-result p1

    return p1
.end method

.method public getNewListSize()I
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$diffUtilComparator$1;->$comparator:Lcom/squareup/mosaic/lists/DiffUtilX$Comparator;

    invoke-interface {v0}, Lcom/squareup/mosaic/lists/DiffUtilX$Comparator;->getNewListSize()I

    move-result v0

    return v0
.end method

.method public getOldListSize()I
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$diffUtilComparator$1;->$comparator:Lcom/squareup/mosaic/lists/DiffUtilX$Comparator;

    invoke-interface {v0}, Lcom/squareup/mosaic/lists/DiffUtilX$Comparator;->getOldListSize()I

    move-result v0

    return v0
.end method
