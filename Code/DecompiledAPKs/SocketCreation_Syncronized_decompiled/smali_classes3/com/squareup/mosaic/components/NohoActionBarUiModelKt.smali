.class public final Lcom/squareup/mosaic/components/NohoActionBarUiModelKt;
.super Ljava/lang/Object;
.source "NohoActionBarUiModel.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoActionBarUiModel.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoActionBarUiModel.kt\ncom/squareup/mosaic/components/NohoActionBarUiModelKt\n*L\n1#1,27:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a3\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003*\u0008\u0012\u0004\u0012\u0002H\u00020\u00042\u0017\u0010\u0005\u001a\u0013\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00010\u0006\u00a2\u0006\u0002\u0008\u0008\u00a8\u0006\t"
    }
    d2 = {
        "nohoActionBar",
        "",
        "P",
        "",
        "Lcom/squareup/mosaic/core/UiModelContext;",
        "block",
        "Lkotlin/Function1;",
        "Lcom/squareup/noho/NohoActionBar$Config$Builder;",
        "Lkotlin/ExtensionFunctionType;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final nohoActionBar(Lcom/squareup/mosaic/core/UiModelContext;Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/mosaic/core/UiModelContext<",
            "TP;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/noho/NohoActionBar$Config$Builder;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$nohoActionBar"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-interface {p0}, Lcom/squareup/mosaic/core/UiModelContext;->createParams()Ljava/lang/Object;

    move-result-object v0

    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    invoke-interface {p1, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance p1, Lcom/squareup/mosaic/components/NohoActionBarUiModel;

    invoke-direct {p1, v0, v1}, Lcom/squareup/mosaic/components/NohoActionBarUiModel;-><init>(Ljava/lang/Object;Lcom/squareup/noho/NohoActionBar$Config$Builder;)V

    check-cast p1, Lcom/squareup/mosaic/core/UiModel;

    invoke-interface {p0, p1}, Lcom/squareup/mosaic/core/UiModelContext;->add(Lcom/squareup/mosaic/core/UiModel;)V

    return-void
.end method
