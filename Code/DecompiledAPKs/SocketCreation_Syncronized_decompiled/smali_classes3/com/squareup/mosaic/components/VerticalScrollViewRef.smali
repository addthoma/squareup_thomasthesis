.class public final Lcom/squareup/mosaic/components/VerticalScrollViewRef;
.super Lcom/squareup/mosaic/core/StandardViewRef;
.source "VerticalScrollViewRef.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mosaic/core/StandardViewRef<",
        "Lcom/squareup/mosaic/components/VerticalScrollUiModel<",
        "*>;",
        "Lcom/squareup/noho/NohoScrollView;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nVerticalScrollViewRef.kt\nKotlin\n*S Kotlin\n*F\n+ 1 VerticalScrollViewRef.kt\ncom/squareup/mosaic/components/VerticalScrollViewRef\n+ 2 ViewRef.kt\ncom/squareup/mosaic/core/ViewRefKt\n*L\n1#1,41:1\n133#2,8:42\n*E\n*S KotlinDebug\n*F\n+ 1 VerticalScrollViewRef.kt\ncom/squareup/mosaic/components/VerticalScrollViewRef\n*L\n30#1,8:42\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0018\u00002\u0012\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u001c\u0010\r\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\n\u0010\u000e\u001a\u0006\u0012\u0002\u0008\u00030\u0002H\u0016J\"\u0010\u000f\u001a\u00020\u00102\u000c\u0010\u0011\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u00022\n\u0010\u0012\u001a\u0006\u0012\u0002\u0008\u00030\u0002H\u0016R\"\u0010\u0007\u001a\u0010\u0012\u000c\u0012\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\t0\u00088VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u000bR\u0018\u0010\u000c\u001a\u000c\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/mosaic/components/VerticalScrollViewRef;",
        "Lcom/squareup/mosaic/core/StandardViewRef;",
        "Lcom/squareup/mosaic/components/VerticalScrollUiModel;",
        "Lcom/squareup/noho/NohoScrollView;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "children",
        "Lkotlin/sequences/Sequence;",
        "Lcom/squareup/mosaic/core/ViewRef;",
        "getChildren",
        "()Lkotlin/sequences/Sequence;",
        "subView",
        "createView",
        "model",
        "doBind",
        "",
        "oldModel",
        "newModel",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private subView:Lcom/squareup/mosaic/core/ViewRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mosaic/core/ViewRef<",
            "**>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0, p1}, Lcom/squareup/mosaic/core/StandardViewRef;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic createView(Landroid/content/Context;Lcom/squareup/mosaic/core/StandardUiModel;)Landroid/view/View;
    .locals 0

    .line 13
    check-cast p2, Lcom/squareup/mosaic/components/VerticalScrollUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/VerticalScrollViewRef;->createView(Landroid/content/Context;Lcom/squareup/mosaic/components/VerticalScrollUiModel;)Lcom/squareup/noho/NohoScrollView;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    return-object p1
.end method

.method public createView(Landroid/content/Context;Lcom/squareup/mosaic/components/VerticalScrollUiModel;)Lcom/squareup/noho/NohoScrollView;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/squareup/mosaic/components/VerticalScrollUiModel<",
            "*>;)",
            "Lcom/squareup/noho/NohoScrollView;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "model"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    new-instance p2, Lcom/squareup/noho/NohoScrollView;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/noho/NohoScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p2
.end method

.method public doBind(Lcom/squareup/mosaic/components/VerticalScrollUiModel;Lcom/squareup/mosaic/components/VerticalScrollUiModel;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/components/VerticalScrollUiModel<",
            "*>;",
            "Lcom/squareup/mosaic/components/VerticalScrollUiModel<",
            "*>;)V"
        }
    .end annotation

    const-string v0, "newModel"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    check-cast p1, Lcom/squareup/mosaic/core/StandardUiModel;

    move-object v0, p2

    check-cast v0, Lcom/squareup/mosaic/core/StandardUiModel;

    invoke-super {p0, p1, v0}, Lcom/squareup/mosaic/core/StandardViewRef;->doBind(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)V

    .line 29
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/VerticalScrollViewRef;->getAndroidView()Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoScrollView;

    invoke-virtual {p2}, Lcom/squareup/mosaic/components/VerticalScrollUiModel;->getFillViewport()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoScrollView;->setFillViewport(Z)V

    .line 30
    iget-object p1, p0, Lcom/squareup/mosaic/components/VerticalScrollViewRef;->subView:Lcom/squareup/mosaic/core/ViewRef;

    .line 31
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/VerticalScrollViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoScrollView;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoScrollView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "androidView.context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/VerticalScrollUiModel;->getSubModel$public_release()Lcom/squareup/mosaic/core/UiModel;

    move-result-object p2

    if-nez p2, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    if-eqz p1, :cond_1

    .line 42
    invoke-virtual {p1, p2}, Lcom/squareup/mosaic/core/ViewRef;->canAccept(Lcom/squareup/mosaic/core/UiModel;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 43
    invoke-static {p1, p2}, Lcom/squareup/mosaic/core/ViewRefKt;->castAndBind(Lcom/squareup/mosaic/core/ViewRef;Lcom/squareup/mosaic/core/UiModel;)V

    goto :goto_1

    .line 47
    :cond_1
    invoke-static {p2, v0}, Lcom/squareup/mosaic/core/UiModelKt;->toView(Lcom/squareup/mosaic/core/UiModel;Landroid/content/Context;)Lcom/squareup/mosaic/core/ViewRef;

    move-result-object p2

    if-eqz p1, :cond_2

    .line 48
    invoke-virtual {p1}, Lcom/squareup/mosaic/core/ViewRef;->getAndroidView()Landroid/view/View;

    move-result-object p1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {p2}, Lcom/squareup/mosaic/core/ViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v0

    if-eqz p1, :cond_3

    .line 34
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/VerticalScrollViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/noho/NohoScrollView;

    invoke-virtual {v1, p1}, Lcom/squareup/noho/NohoScrollView;->removeView(Landroid/view/View;)V

    .line 35
    :cond_3
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/VerticalScrollViewRef;->getAndroidView()Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoScrollView;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoScrollView;->addView(Landroid/view/View;)V

    move-object p1, p2

    .line 49
    :goto_1
    iput-object p1, p0, Lcom/squareup/mosaic/components/VerticalScrollViewRef;->subView:Lcom/squareup/mosaic/core/ViewRef;

    return-void
.end method

.method public bridge synthetic doBind(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)V
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/mosaic/components/VerticalScrollUiModel;

    check-cast p2, Lcom/squareup/mosaic/components/VerticalScrollUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/VerticalScrollViewRef;->doBind(Lcom/squareup/mosaic/components/VerticalScrollUiModel;Lcom/squareup/mosaic/components/VerticalScrollUiModel;)V

    return-void
.end method

.method public bridge synthetic doBind(Lcom/squareup/mosaic/core/UiModel;Lcom/squareup/mosaic/core/UiModel;)V
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/mosaic/components/VerticalScrollUiModel;

    check-cast p2, Lcom/squareup/mosaic/components/VerticalScrollUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/VerticalScrollViewRef;->doBind(Lcom/squareup/mosaic/components/VerticalScrollUiModel;Lcom/squareup/mosaic/components/VerticalScrollUiModel;)V

    return-void
.end method

.method public getChildren()Lkotlin/sequences/Sequence;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/sequences/Sequence<",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "**>;>;"
        }
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalScrollViewRef;->subView:Lcom/squareup/mosaic/core/ViewRef;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/squareup/mosaic/core/ViewRef;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-static {v1}, Lkotlin/sequences/SequencesKt;->sequenceOf([Ljava/lang/Object;)Lkotlin/sequences/Sequence;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lkotlin/sequences/SequencesKt;->emptySequence()Lkotlin/sequences/Sequence;

    move-result-object v0

    :goto_0
    return-object v0
.end method
