.class public final Lcom/squareup/mosaic/components/LabelUiModelKt;
.super Ljava/lang/Object;
.source "LabelUiModel.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLabelUiModel.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LabelUiModel.kt\ncom/squareup/mosaic/components/LabelUiModelKt\n*L\n1#1,27:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a9\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003*\u0008\u0012\u0004\u0012\u0002H\u00020\u00042\u001d\u0010\u0005\u001a\u0019\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00020\u0007\u0012\u0004\u0012\u00020\u00010\u0006\u00a2\u0006\u0002\u0008\u0008\u00a8\u0006\t"
    }
    d2 = {
        "label",
        "",
        "P",
        "",
        "Lcom/squareup/mosaic/core/UiModelContext;",
        "block",
        "Lkotlin/Function1;",
        "Lcom/squareup/mosaic/components/LabelUiModel;",
        "Lkotlin/ExtensionFunctionType;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final label(Lcom/squareup/mosaic/core/UiModelContext;Lkotlin/jvm/functions/Function1;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/mosaic/core/UiModelContext<",
            "TP;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/mosaic/components/LabelUiModel<",
            "TP;>;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$label"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    new-instance v0, Lcom/squareup/mosaic/components/LabelUiModel;

    invoke-interface {p0}, Lcom/squareup/mosaic/core/UiModelContext;->createParams()Ljava/lang/Object;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/mosaic/components/LabelUiModel;-><init>(Ljava/lang/Object;ILcom/squareup/resources/TextModel;Lcom/squareup/noho/NohoLabel$Type;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/squareup/mosaic/core/UiModel;

    invoke-interface {p0, v0}, Lcom/squareup/mosaic/core/UiModelContext;->add(Lcom/squareup/mosaic/core/UiModel;)V

    return-void
.end method
