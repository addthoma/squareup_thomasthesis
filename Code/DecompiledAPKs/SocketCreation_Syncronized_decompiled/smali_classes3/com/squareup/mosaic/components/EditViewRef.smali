.class public final Lcom/squareup/mosaic/components/EditViewRef;
.super Lcom/squareup/mosaic/core/StandardViewRef;
.source "EditViewRef.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mosaic/core/StandardViewRef<",
        "Lcom/squareup/mosaic/components/EditUiModel<",
        "*>;",
        "Lcom/squareup/noho/NohoEditRow;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditViewRef.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditViewRef.kt\ncom/squareup/mosaic/components/EditViewRef\n+ 2 ViewRefUtils.kt\ncom/squareup/mosaic/components/ViewRefUtilsKt\n*L\n1#1,84:1\n15#2,2:85\n*E\n*S KotlinDebug\n*F\n+ 1 EditViewRef.kt\ncom/squareup/mosaic/components/EditViewRef\n*L\n50#1,2:85\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0018\u00002\u0012\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u001c\u0010\t\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\n\u0010\n\u001a\u0006\u0012\u0002\u0008\u00030\u0002H\u0016J\"\u0010\u000b\u001a\u00020\u000c2\u000c\u0010\r\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u00022\n\u0010\u000e\u001a\u0006\u0012\u0002\u0008\u00030\u0002H\u0016R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\u0008X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/mosaic/components/EditViewRef;",
        "Lcom/squareup/mosaic/core/StandardViewRef;",
        "Lcom/squareup/mosaic/components/EditUiModel;",
        "Lcom/squareup/noho/NohoEditRow;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "leftIconPlugin",
        "Lcom/squareup/noho/IconPlugin;",
        "createView",
        "model",
        "doBind",
        "",
        "oldModel",
        "newModel",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private leftIconPlugin:Lcom/squareup/noho/IconPlugin;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0, p1}, Lcom/squareup/mosaic/core/StandardViewRef;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public static final synthetic access$isBinding$p(Lcom/squareup/mosaic/components/EditViewRef;)Z
    .locals 0

    .line 13
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/EditViewRef;->isBinding()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$setBinding$p(Lcom/squareup/mosaic/components/EditViewRef;Z)V
    .locals 0

    .line 13
    invoke-direct {p0, p1}, Lcom/squareup/mosaic/components/EditViewRef;->setBinding(Z)V

    return-void
.end method


# virtual methods
.method public bridge synthetic createView(Landroid/content/Context;Lcom/squareup/mosaic/core/StandardUiModel;)Landroid/view/View;
    .locals 0

    .line 13
    check-cast p2, Lcom/squareup/mosaic/components/EditUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/EditViewRef;->createView(Landroid/content/Context;Lcom/squareup/mosaic/components/EditUiModel;)Lcom/squareup/noho/NohoEditRow;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    return-object p1
.end method

.method public createView(Landroid/content/Context;Lcom/squareup/mosaic/components/EditUiModel;)Lcom/squareup/noho/NohoEditRow;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/squareup/mosaic/components/EditUiModel<",
            "*>;)",
            "Lcom/squareup/noho/NohoEditRow;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "model"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    new-instance p2, Lcom/squareup/noho/NohoEditRow;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/noho/NohoEditRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 21
    new-instance p1, Lcom/squareup/mosaic/components/EditViewRef$createView$$inlined$apply$lambda$1;

    invoke-direct {p1, p0}, Lcom/squareup/mosaic/components/EditViewRef$createView$$inlined$apply$lambda$1;-><init>(Lcom/squareup/mosaic/components/EditViewRef;)V

    check-cast p1, Landroid/text/TextWatcher;

    invoke-virtual {p2, p1}, Lcom/squareup/noho/NohoEditRow;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-object p2
.end method

.method public doBind(Lcom/squareup/mosaic/components/EditUiModel;Lcom/squareup/mosaic/components/EditUiModel;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/components/EditUiModel<",
            "*>;",
            "Lcom/squareup/mosaic/components/EditUiModel<",
            "*>;)V"
        }
    .end annotation

    const-string v0, "newModel"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    move-object v0, p1

    check-cast v0, Lcom/squareup/mosaic/core/StandardUiModel;

    move-object v1, p2

    check-cast v1, Lcom/squareup/mosaic/core/StandardUiModel;

    invoke-super {p0, v0, v1}, Lcom/squareup/mosaic/core/StandardViewRef;->doBind(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)V

    .line 49
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/EditViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {p2}, Lcom/squareup/mosaic/components/EditUiModel;->getText()Lcom/squareup/resources/TextModel;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/EditViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v2}, Lcom/squareup/noho/NohoEditRow;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "androidView.context"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditRow;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 50
    invoke-virtual {p1}, Lcom/squareup/mosaic/components/EditUiModel;->getHint()Lcom/squareup/resources/TextModel;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, v0

    :goto_0
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/EditUiModel;->getHint()Lcom/squareup/resources/TextModel;

    move-result-object v1

    .line 85
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    const/4 v2, 0x1

    xor-int/2addr p1, v2

    if-eqz p1, :cond_1

    .line 51
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/EditViewRef;->getAndroidView()Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/EditViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v4}, Lcom/squareup/noho/NohoEditRow;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v4}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoEditRow;->setHint(Ljava/lang/CharSequence;)V

    .line 52
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/EditViewRef;->getAndroidView()Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/EditViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v1}, Lcom/squareup/noho/NohoEditRow;->getHint()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoEditRow;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 54
    :cond_1
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/EditUiModel;->getPosition()Lcom/squareup/noho/NohoEditRow$PositionInList;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 56
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/EditViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/EditViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v4}, Lcom/squareup/noho/NohoEditRow;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v5, Lcom/squareup/noho/R$drawable;->noho_edit_background:I

    const/4 v6, 0x2

    invoke-static {v4, v5, v0, v6, v0}, Lcom/squareup/noho/ContextExtensions;->getCustomDrawable$default(Landroid/content/Context;ILandroid/content/res/Resources$Theme;ILjava/lang/Object;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/squareup/noho/NohoEditRow;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 57
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/EditViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v1, p1}, Lcom/squareup/noho/NohoEditRow;->setPositionInList(Lcom/squareup/noho/NohoEditRow$PositionInList;)V

    goto :goto_1

    .line 59
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/EditViewRef;->getAndroidView()Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoEditRow;

    move-object v1, v0

    check-cast v1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoEditRow;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 63
    :goto_1
    iget-object p1, p0, Lcom/squareup/mosaic/components/EditViewRef;->leftIconPlugin:Lcom/squareup/noho/IconPlugin;

    const/4 v1, 0x0

    if-eqz p1, :cond_3

    const/4 p1, 0x1

    goto :goto_2

    :cond_3
    const/4 p1, 0x0

    .line 64
    :goto_2
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/EditUiModel;->getLeftIcon()I

    move-result v4

    if-eqz v4, :cond_4

    const/4 v1, 0x1

    :cond_4
    if-nez p1, :cond_6

    if-eqz v1, :cond_6

    .line 71
    new-instance p1, Lcom/squareup/noho/IconPlugin;

    .line 72
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/EditViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v6, Lcom/squareup/noho/NohoEditRow$Side;->START:Lcom/squareup/noho/NohoEditRow$Side;

    invoke-virtual {p2}, Lcom/squareup/mosaic/components/EditUiModel;->getLeftIcon()I

    move-result v7

    .line 73
    sget v8, Lcom/squareup/noho/R$dimen;->noho_edit_search_margin:I

    const/4 v9, 0x0

    const/16 v10, 0x10

    const/4 v11, 0x0

    move-object v4, p1

    .line 71
    invoke-direct/range {v4 .. v11}, Lcom/squareup/noho/IconPlugin;-><init>(Landroid/content/Context;Lcom/squareup/noho/NohoEditRow$Side;IIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/mosaic/components/EditViewRef;->leftIconPlugin:Lcom/squareup/noho/IconPlugin;

    .line 75
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/EditViewRef;->getAndroidView()Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoEditRow;

    iget-object p2, p0, Lcom/squareup/mosaic/components/EditViewRef;->leftIconPlugin:Lcom/squareup/noho/IconPlugin;

    if-nez p2, :cond_5

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_5
    check-cast p2, Lcom/squareup/noho/NohoEditRow$Plugin;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoEditRow;->addPlugin(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    goto :goto_3

    :cond_6
    if-eqz p1, :cond_8

    if-nez v1, :cond_8

    .line 78
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/EditViewRef;->getAndroidView()Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoEditRow;

    iget-object p2, p0, Lcom/squareup/mosaic/components/EditViewRef;->leftIconPlugin:Lcom/squareup/noho/IconPlugin;

    if-nez p2, :cond_7

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_7
    check-cast p2, Lcom/squareup/noho/NohoEditRow$Plugin;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoEditRow;->removePlugin(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    .line 79
    check-cast v0, Lcom/squareup/noho/IconPlugin;

    iput-object v0, p0, Lcom/squareup/mosaic/components/EditViewRef;->leftIconPlugin:Lcom/squareup/noho/IconPlugin;

    :cond_8
    :goto_3
    return-void
.end method

.method public bridge synthetic doBind(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)V
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/mosaic/components/EditUiModel;

    check-cast p2, Lcom/squareup/mosaic/components/EditUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/EditViewRef;->doBind(Lcom/squareup/mosaic/components/EditUiModel;Lcom/squareup/mosaic/components/EditUiModel;)V

    return-void
.end method

.method public bridge synthetic doBind(Lcom/squareup/mosaic/core/UiModel;Lcom/squareup/mosaic/core/UiModel;)V
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/mosaic/components/EditUiModel;

    check-cast p2, Lcom/squareup/mosaic/components/EditUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/EditViewRef;->doBind(Lcom/squareup/mosaic/components/EditUiModel;Lcom/squareup/mosaic/components/EditUiModel;)V

    return-void
.end method
