.class public final Lcom/squareup/mosaic/components/HairlineUiModelKt;
.super Ljava/lang/Object;
.source "HairlineUiModel.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u001a\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003*\u0008\u0012\u0004\u0012\u0002H\u00020\u0004\u001a\u001a\u0010\u0005\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003*\u0008\u0012\u0004\u0012\u0002H\u00020\u0004\u00a8\u0006\u0006"
    }
    d2 = {
        "horizontalHairline",
        "",
        "P",
        "",
        "Lcom/squareup/mosaic/core/UiModelContext;",
        "verticalHairline",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final horizontalHairline(Lcom/squareup/mosaic/core/UiModelContext;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/mosaic/core/UiModelContext<",
            "TP;>;)V"
        }
    .end annotation

    const-string v0, "$this$horizontalHairline"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    new-instance v0, Lcom/squareup/mosaic/components/HairlineUiModel;

    invoke-interface {p0}, Lcom/squareup/mosaic/core/UiModelContext;->createParams()Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/squareup/mosaic/components/HairlineUiModel;-><init>(Ljava/lang/Object;Z)V

    check-cast v0, Lcom/squareup/mosaic/core/UiModel;

    invoke-interface {p0, v0}, Lcom/squareup/mosaic/core/UiModelContext;->add(Lcom/squareup/mosaic/core/UiModel;)V

    return-void
.end method

.method public static final verticalHairline(Lcom/squareup/mosaic/core/UiModelContext;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/mosaic/core/UiModelContext<",
            "TP;>;)V"
        }
    .end annotation

    const-string v0, "$this$verticalHairline"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    new-instance v0, Lcom/squareup/mosaic/components/HairlineUiModel;

    invoke-interface {p0}, Lcom/squareup/mosaic/core/UiModelContext;->createParams()Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/squareup/mosaic/components/HairlineUiModel;-><init>(Ljava/lang/Object;Z)V

    check-cast v0, Lcom/squareup/mosaic/core/UiModel;

    invoke-interface {p0, v0}, Lcom/squareup/mosaic/core/UiModelContext;->add(Lcom/squareup/mosaic/core/UiModel;)V

    return-void
.end method
