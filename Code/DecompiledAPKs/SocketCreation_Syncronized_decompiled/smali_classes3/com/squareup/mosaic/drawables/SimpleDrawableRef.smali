.class public final Lcom/squareup/mosaic/drawables/SimpleDrawableRef;
.super Lcom/squareup/mosaic/drawables/DelayedDrawableViewRef;
.source "SimpleDrawableRef.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mosaic/drawables/DelayedDrawableViewRef<",
        "Lcom/squareup/mosaic/drawables/SimpleDrawableModel;",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0008\u0000\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\u0002H\u0014J\u0018\u0010\u000b\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u000c\u001a\u00020\u0002H\u0016J\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u000c\u001a\u00020\u0002H\u0016\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/mosaic/drawables/SimpleDrawableRef;",
        "Lcom/squareup/mosaic/drawables/DelayedDrawableViewRef;",
        "Lcom/squareup/mosaic/drawables/SimpleDrawableModel;",
        "Landroid/graphics/drawable/Drawable;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "canUpdateTo",
        "",
        "currentModel",
        "newModel",
        "createDrawable",
        "model",
        "updateDrawable",
        "",
        "drawable",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0, p1}, Lcom/squareup/mosaic/drawables/DelayedDrawableViewRef;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic canUpdateTo(Lcom/squareup/mosaic/core/DrawableModel;Lcom/squareup/mosaic/core/DrawableModel;)Z
    .locals 0

    .line 7
    check-cast p1, Lcom/squareup/mosaic/drawables/SimpleDrawableModel;

    check-cast p2, Lcom/squareup/mosaic/drawables/SimpleDrawableModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/drawables/SimpleDrawableRef;->canUpdateTo(Lcom/squareup/mosaic/drawables/SimpleDrawableModel;Lcom/squareup/mosaic/drawables/SimpleDrawableModel;)Z

    move-result p1

    return p1
.end method

.method protected canUpdateTo(Lcom/squareup/mosaic/drawables/SimpleDrawableModel;Lcom/squareup/mosaic/drawables/SimpleDrawableModel;)Z
    .locals 1

    const-string v0, "currentModel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newModel"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-virtual {p1}, Lcom/squareup/mosaic/drawables/SimpleDrawableModel;->getDrawableId()I

    move-result p1

    invoke-virtual {p2}, Lcom/squareup/mosaic/drawables/SimpleDrawableModel;->getDrawableId()I

    move-result p2

    if-ne p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public bridge synthetic createDrawable(Landroid/content/Context;Lcom/squareup/mosaic/core/DrawableModel;)Landroid/graphics/drawable/Drawable;
    .locals 0

    .line 7
    check-cast p2, Lcom/squareup/mosaic/drawables/SimpleDrawableModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/drawables/SimpleDrawableRef;->createDrawable(Landroid/content/Context;Lcom/squareup/mosaic/drawables/SimpleDrawableModel;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    return-object p1
.end method

.method public createDrawable(Landroid/content/Context;Lcom/squareup/mosaic/drawables/SimpleDrawableModel;)Landroid/graphics/drawable/Drawable;
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "model"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-virtual {p2}, Lcom/squareup/mosaic/drawables/SimpleDrawableModel;->getDrawableId()I

    move-result p2

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-static {p1, p2, v0, v1, v0}, Lcom/squareup/noho/ContextExtensions;->getCustomDrawable$default(Landroid/content/Context;ILandroid/content/res/Resources$Theme;ILjava/lang/Object;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic updateDrawable(Landroid/graphics/drawable/Drawable;Lcom/squareup/mosaic/core/DrawableModel;)V
    .locals 0

    .line 7
    check-cast p2, Lcom/squareup/mosaic/drawables/SimpleDrawableModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/drawables/SimpleDrawableRef;->updateDrawable(Landroid/graphics/drawable/Drawable;Lcom/squareup/mosaic/drawables/SimpleDrawableModel;)V

    return-void
.end method

.method public updateDrawable(Landroid/graphics/drawable/Drawable;Lcom/squareup/mosaic/drawables/SimpleDrawableModel;)V
    .locals 1

    const-string v0, "drawable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "model"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
