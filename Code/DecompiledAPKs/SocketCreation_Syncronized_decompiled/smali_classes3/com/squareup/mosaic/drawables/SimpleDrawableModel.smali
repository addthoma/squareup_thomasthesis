.class public final Lcom/squareup/mosaic/drawables/SimpleDrawableModel;
.super Lcom/squareup/mosaic/core/DrawableModel;
.source "SimpleDrawableModel.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0011\u0008\u0001\u0012\u0008\u0008\u0003\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0008\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\t\u001a\u00020\u00002\u0008\u0008\u0003\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0018\u0010\n\u001a\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0016J\u0013\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\"\u0004\u0008\u0007\u0010\u0004\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/mosaic/drawables/SimpleDrawableModel;",
        "Lcom/squareup/mosaic/core/DrawableModel;",
        "drawableId",
        "",
        "(I)V",
        "getDrawableId",
        "()I",
        "setDrawableId",
        "component1",
        "copy",
        "createDrawableRef",
        "Lcom/squareup/mosaic/core/DrawableRef;",
        "context",
        "Landroid/content/Context;",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private drawableId:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/mosaic/drawables/SimpleDrawableModel;-><init>(IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/squareup/mosaic/core/DrawableModel;-><init>()V

    iput p1, p0, Lcom/squareup/mosaic/drawables/SimpleDrawableModel;->drawableId:I

    return-void
.end method

.method public synthetic constructor <init>(IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 21
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/mosaic/drawables/SimpleDrawableModel;-><init>(I)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/mosaic/drawables/SimpleDrawableModel;IILjava/lang/Object;)Lcom/squareup/mosaic/drawables/SimpleDrawableModel;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget p1, p0, Lcom/squareup/mosaic/drawables/SimpleDrawableModel;->drawableId:I

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/mosaic/drawables/SimpleDrawableModel;->copy(I)Lcom/squareup/mosaic/drawables/SimpleDrawableModel;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/mosaic/drawables/SimpleDrawableModel;->drawableId:I

    return v0
.end method

.method public final copy(I)Lcom/squareup/mosaic/drawables/SimpleDrawableModel;
    .locals 1

    new-instance v0, Lcom/squareup/mosaic/drawables/SimpleDrawableModel;

    invoke-direct {v0, p1}, Lcom/squareup/mosaic/drawables/SimpleDrawableModel;-><init>(I)V

    return-object v0
.end method

.method public createDrawableRef(Landroid/content/Context;)Lcom/squareup/mosaic/core/DrawableRef;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lcom/squareup/mosaic/core/DrawableRef<",
            "**>;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    new-instance v0, Lcom/squareup/mosaic/drawables/SimpleDrawableRef;

    invoke-direct {v0, p1}, Lcom/squareup/mosaic/drawables/SimpleDrawableRef;-><init>(Landroid/content/Context;)V

    check-cast v0, Lcom/squareup/mosaic/core/DrawableRef;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/mosaic/drawables/SimpleDrawableModel;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/mosaic/drawables/SimpleDrawableModel;

    iget v0, p0, Lcom/squareup/mosaic/drawables/SimpleDrawableModel;->drawableId:I

    iget p1, p1, Lcom/squareup/mosaic/drawables/SimpleDrawableModel;->drawableId:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDrawableId()I
    .locals 1

    .line 21
    iget v0, p0, Lcom/squareup/mosaic/drawables/SimpleDrawableModel;->drawableId:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    iget v0, p0, Lcom/squareup/mosaic/drawables/SimpleDrawableModel;->drawableId:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    return v0
.end method

.method public final setDrawableId(I)V
    .locals 0

    .line 21
    iput p1, p0, Lcom/squareup/mosaic/drawables/SimpleDrawableModel;->drawableId:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SimpleDrawableModel(drawableId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/mosaic/drawables/SimpleDrawableModel;->drawableId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
