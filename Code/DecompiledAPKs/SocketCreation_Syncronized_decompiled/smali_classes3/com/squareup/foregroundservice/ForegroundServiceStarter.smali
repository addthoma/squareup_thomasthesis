.class public interface abstract Lcom/squareup/foregroundservice/ForegroundServiceStarter;
.super Ljava/lang/Object;
.source "ForegroundServiceStarter.java"

# interfaces
.implements Lcom/squareup/ActivityListener$ResumedPausedListener;


# virtual methods
.method public abstract maybePromoteToForeground(Landroid/app/Service;Landroid/content/Intent;)V
.end method

.method public abstract setStartInForegroundEnabled(Z)V
.end method

.method public abstract startInForeground(Landroid/content/Intent;)V
.end method

.method public abstract startWaitingForForeground(Landroid/content/Intent;)V
.end method
