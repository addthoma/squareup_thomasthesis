.class public interface abstract annotation Lcom/squareup/foregroundservice/ForegroundServicesEnabled;
.super Ljava/lang/Object;
.source "ForegroundServicesEnabled.java"

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation runtime Ljavax/inject/Qualifier;
.end annotation


# static fields
.field public static final FOREGROUND_SERVICES_ENABLED_KEY:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/squareup/foregroundservice/ForegroundServicesEnabled;

    .line 8
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":foreground_services_enabled_key"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/foregroundservice/ForegroundServicesEnabled;->FOREGROUND_SERVICES_ENABLED_KEY:Ljava/lang/String;

    return-void
.end method
