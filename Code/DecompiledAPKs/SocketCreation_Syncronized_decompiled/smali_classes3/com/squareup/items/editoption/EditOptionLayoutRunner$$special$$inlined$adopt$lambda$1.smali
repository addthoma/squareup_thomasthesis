.class final Lcom/squareup/items/editoption/EditOptionLayoutRunner$$special$$inlined$adopt$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EditOptionLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/editoption/EditOptionLayoutRunner;-><init>(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/cycler/MutationExtensionSpec<",
        "Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\n\u00a2\u0006\u0002\u0008\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/cycler/MutationExtensionSpec;",
        "Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow;",
        "invoke",
        "com/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$7"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/items/editoption/EditOptionLayoutRunner;


# direct methods
.method constructor <init>(Lcom/squareup/items/editoption/EditOptionLayoutRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$$special$$inlined$adopt$lambda$1;->this$0:Lcom/squareup/items/editoption/EditOptionLayoutRunner;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 43
    check-cast p1, Lcom/squareup/cycler/MutationExtensionSpec;

    invoke-virtual {p0, p1}, Lcom/squareup/items/editoption/EditOptionLayoutRunner$$special$$inlined$adopt$lambda$1;->invoke(Lcom/squareup/cycler/MutationExtensionSpec;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/cycler/MutationExtensionSpec;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/MutationExtensionSpec<",
            "Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 174
    invoke-virtual {p1, v0}, Lcom/squareup/cycler/MutationExtensionSpec;->setDragAndDropEnabled(Z)V

    .line 176
    sget-object v0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$7$1;->INSTANCE:Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$7$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/MutationExtensionSpec;->canDropOverItem(Lkotlin/jvm/functions/Function1;)V

    .line 183
    new-instance v0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$$special$$inlined$adopt$lambda$1$1;

    invoke-direct {v0, p0}, Lcom/squareup/items/editoption/EditOptionLayoutRunner$$special$$inlined$adopt$lambda$1$1;-><init>(Lcom/squareup/items/editoption/EditOptionLayoutRunner$$special$$inlined$adopt$lambda$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/MutationExtensionSpec;->onMove(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method
