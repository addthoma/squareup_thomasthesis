.class public final Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "SelectOptionValuesForVariationWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action;,
        Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationProps;",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectOptionValuesForVariationWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectOptionValuesForVariationWorkflow.kt\ncom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 4 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,322:1\n32#2,12:323\n149#3,5:335\n149#3,5:340\n149#3,5:345\n1360#4:350\n1429#4,3:351\n*E\n*S KotlinDebug\n*F\n+ 1 SelectOptionValuesForVariationWorkflow.kt\ncom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow\n*L\n43#1,12:323\n57#1,5:335\n100#1,5:340\n101#1,5:345\n115#1:350\n115#1,3:351\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 \u001d2<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0001:\u0002\u001c\u001dB\u000f\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ4\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u00032\u001a\u0010\u0011\u001a\u0016\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0013\u0018\u00010\u0012H\u0002J\u001a\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u000f\u001a\u00020\u00022\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0016JN\u0010\u0018\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u00032\u0012\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u001aH\u0016J\u0010\u0010\u001b\u001a\u00020\u00172\u0006\u0010\u0010\u001a\u00020\u0003H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationProps;",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "selectSingleOptionValueForVariationWorkflow",
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow;",
        "(Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow;)V",
        "buildSelectOptionValuesForVariationScreen",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;",
        "props",
        "state",
        "sink",
        "Lcom/squareup/workflow/Sink;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "initialState",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState$SelectOptionValues;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "Action",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion;


# instance fields
.field private final selectSingleOptionValueForVariationWorkflow:Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;->Companion:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "selectSingleOptionValueForVariationWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;->selectSingleOptionValueForVariationWorkflow:Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow;

    return-void
.end method

.method private final buildSelectOptionValuesForVariationScreen(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationProps;Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;Lcom/squareup/workflow/Sink;)Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationProps;",
            "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;",
            "+",
            "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput;",
            ">;>;)",
            "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;"
        }
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    .line 115
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationProps;->getAssignedOptions()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 350
    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v2, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 351
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 352
    check-cast v4, Lcom/squareup/cogs/itemoptions/ItemOption;

    .line 116
    new-instance v5, Lcom/squareup/items/addsinglevariation/ItemOptionWithSelectedValue;

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;->getSelectedValuesByOptionIds()Ljava/util/Map;

    move-result-object v6

    invoke-virtual {v4}, Lcom/squareup/cogs/itemoptions/ItemOption;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-direct {v5, v4, v6}, Lcom/squareup/items/addsinglevariation/ItemOptionWithSelectedValue;-><init>(Lcom/squareup/cogs/itemoptions/ItemOption;Lcom/squareup/cogs/itemoptions/ItemOptionValue;)V

    invoke-interface {v3, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 353
    :cond_0
    move-object v8, v3

    check-cast v8, Ljava/util/List;

    .line 118
    sget-object v2, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;->Companion:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion;

    .line 119
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationProps;->getAssignedOptions()Ljava/util/List;

    move-result-object v3

    .line 120
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationProps;->getUsedOptionValueCombinations()Ljava/util/List;

    move-result-object v4

    check-cast v4, Ljava/lang/Iterable;

    invoke-static {v4}, Lkotlin/collections/CollectionsKt;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v4

    .line 118
    invoke-virtual {v2, v3, v4}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion;->findAllAvailableItemOptionValueCombinationsWithUsedOptionValues(Ljava/util/List;Ljava/util/Set;)Ljava/util/List;

    move-result-object v9

    .line 122
    sget-object v2, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;->Companion:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion;

    .line 123
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationProps;->getAssignedOptions()Ljava/util/List;

    move-result-object v3

    .line 124
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;->getSelectedValuesByOptionIds()Ljava/util/Map;

    move-result-object v4

    .line 122
    invoke-static {v2, v3, v4}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion;->access$shouldEnableCreateCustomVariationButton(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion;Ljava/util/List;Ljava/util/Map;)Z

    move-result v10

    .line 126
    new-instance v2, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;

    .line 130
    new-instance v3, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$buildSelectOptionValuesForVariationScreen$1;

    invoke-direct {v3, v1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$buildSelectOptionValuesForVariationScreen$1;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v11, v3

    check-cast v11, Lkotlin/jvm/functions/Function1;

    .line 131
    new-instance v3, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$buildSelectOptionValuesForVariationScreen$2;

    invoke-direct {v3, v1, v0}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$buildSelectOptionValuesForVariationScreen$2;-><init>(Lcom/squareup/workflow/Sink;Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationProps;)V

    move-object v12, v3

    check-cast v12, Lkotlin/jvm/functions/Function0;

    .line 140
    new-instance v3, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$buildSelectOptionValuesForVariationScreen$3;

    invoke-direct {v3, v1, v0}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$buildSelectOptionValuesForVariationScreen$3;-><init>(Lcom/squareup/workflow/Sink;Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationProps;)V

    move-object v13, v3

    check-cast v13, Lkotlin/jvm/functions/Function1;

    .line 143
    new-instance v0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$buildSelectOptionValuesForVariationScreen$4;

    invoke-direct {v0, v1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$buildSelectOptionValuesForVariationScreen$4;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v14, v0

    check-cast v14, Lkotlin/jvm/functions/Function0;

    move-object v7, v2

    .line 126
    invoke-direct/range {v7 .. v14}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;-><init>(Ljava/util/List;Ljava/util/List;ZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    return-object v2
.end method


# virtual methods
.method public initialState(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState$SelectOptionValues;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 323
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 328
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 329
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 330
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 331
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 332
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 333
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 334
    :cond_3
    check-cast v1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState$SelectOptionValues;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 43
    :cond_4
    invoke-static {}, Lkotlin/collections/MapsKt;->emptyMap()Ljava/util/Map;

    move-result-object p1

    new-instance v1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState$SelectOptionValues;

    invoke-direct {v1, p1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState$SelectOptionValues;-><init>(Ljava/util/Map;)V

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;->initialState(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState$SelectOptionValues;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationProps;

    check-cast p2, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;->render(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationProps;Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationProps;Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationProps;",
            "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;",
            "-",
            "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    instance-of v0, p2, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState$SelectOptionValues;

    const-string v1, ""

    if-eqz v0, :cond_0

    .line 55
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p3

    .line 52
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;->buildSelectOptionValuesForVariationScreen(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationProps;Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;Lcom/squareup/workflow/Sink;)Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;

    move-result-object p1

    .line 57
    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 336
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 337
    const-class p3, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 338
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 336
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 58
    sget-object p1, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 60
    :cond_0
    instance-of v0, p2, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState$SelectOptionValueForCustomVariation;

    if-eqz v0, :cond_2

    .line 61
    move-object v0, p2

    check-cast v0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState$SelectOptionValueForCustomVariation;

    invoke-virtual {v0}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState$SelectOptionValueForCustomVariation;->getSelectedOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/cogs/itemoptions/ItemOption;->getAllValues()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v1

    .line 62
    invoke-virtual {p2}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;->getSelectedValuesByOptionIds()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState$SelectOptionValueForCustomVariation;->getSelectedOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/cogs/itemoptions/ItemOption;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    if-eqz v2, :cond_1

    .line 64
    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 65
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    :cond_1
    iget-object v3, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;->selectSingleOptionValueForVariationWorkflow:Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow;

    move-object v5, v3

    check-cast v5, Lcom/squareup/workflow/Workflow;

    .line 69
    new-instance v6, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationProps;

    .line 70
    invoke-virtual {v0}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState$SelectOptionValueForCustomVariation;->getSelectedOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v3

    .line 74
    sget-object v4, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;->Companion:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion;

    .line 75
    invoke-virtual {v0}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState$SelectOptionValueForCustomVariation;->getSelectedOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v0

    .line 76
    invoke-virtual {p2}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;->getSelectedValuesByOptionIds()Ljava/util/Map;

    move-result-object p2

    .line 77
    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationProps;->getUsedOptionValueCombinations()Ljava/util/List;

    move-result-object p1

    .line 74
    invoke-static {v4, v0, p2, p1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion;->access$findValueSelectionsMatchingValueCombinations(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Companion;Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/util/Map;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    .line 69
    invoke-direct {v6, v3, v1, v2, p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationProps;-><init>(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/util/List;Lcom/squareup/cogs/itemoptions/ItemOptionValue;Ljava/util/List;)V

    const/4 v7, 0x0

    .line 80
    sget-object p1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$render$1;->INSTANCE:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$render$1;

    move-object v8, p1

    check-cast v8, Lkotlin/jvm/functions/Function1;

    const/4 v9, 0x4

    const/4 v10, 0x0

    move-object v4, p3

    .line 67
    invoke-static/range {v4 .. v10}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto :goto_0

    .line 88
    :cond_2
    instance-of v0, p2, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState$DuplicateVariation;

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    .line 89
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;->buildSelectOptionValuesForVariationScreen(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationProps;Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;Lcom/squareup/workflow/Sink;)Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;

    move-result-object p1

    .line 94
    new-instance p2, Lcom/squareup/items/addsinglevariation/WarnDuplicateVariationDialogScreen;

    .line 95
    new-instance v0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$render$duplicateVariationDialogScreen$1;

    invoke-direct {v0, p3}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$render$duplicateVariationDialogScreen$1;-><init>(Lcom/squareup/workflow/RenderContext;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 94
    invoke-direct {p2, v0}, Lcom/squareup/items/addsinglevariation/WarnDuplicateVariationDialogScreen;-><init>(Lkotlin/jvm/functions/Function0;)V

    .line 99
    sget-object p3, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    const/4 v0, 0x2

    new-array v0, v0, [Lkotlin/Pair;

    const/4 v2, 0x0

    .line 100
    sget-object v3, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 341
    new-instance v4, Lcom/squareup/workflow/legacy/Screen;

    .line 342
    const-class v5, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;

    invoke-static {v5}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v5

    invoke-static {v5, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v5

    .line 343
    sget-object v6, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v6}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v6

    .line 341
    invoke-direct {v4, v5, p1, v6}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 100
    new-instance p1, Lkotlin/Pair;

    invoke-direct {p1, v3, v4}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object p1, v0, v2

    const/4 p1, 0x1

    .line 101
    sget-object v2, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    check-cast p2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 346
    new-instance v3, Lcom/squareup/workflow/legacy/Screen;

    .line 347
    const-class v4, Lcom/squareup/items/addsinglevariation/WarnDuplicateVariationDialogScreen;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    invoke-static {v4, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 348
    sget-object v4, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v4}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v4

    .line 346
    invoke-direct {v3, v1, p2, v4}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 101
    new-instance p2, Lkotlin/Pair;

    invoke-direct {p2, v2, v3}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object p2, v0, p1

    .line 99
    invoke-virtual {p3, v0}, Lcom/squareup/container/PosLayering$Companion;->partial([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;

    invoke-virtual {p0, p1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;->snapshotState(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
