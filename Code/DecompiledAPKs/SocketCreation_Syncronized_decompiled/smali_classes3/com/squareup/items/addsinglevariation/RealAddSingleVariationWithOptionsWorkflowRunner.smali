.class public final Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner;
.super Lcom/squareup/container/DynamicPropsWorkflowV2Runner;
.source "RealAddSingleVariationWithOptionsWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner<",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutput;",
        ">;",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsWorkflowRunner;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u00012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002B\u001f\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0014J\u0018\u0010\u0014\u001a\u00020\u00112\u0006\u0010\u0015\u001a\u00020\u00032\u0006\u0010\u000c\u001a\u00020\rH\u0016R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000c\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u00020\nX\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner;",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsWorkflowRunner;",
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner;",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutput;",
        "viewFactory",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsViewFactory;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "workflow",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsWorkflow;",
        "(Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsViewFactory;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsWorkflow;)V",
        "outputHandler",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutputHandler;",
        "getWorkflow",
        "()Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsWorkflow;",
        "onEnterScope",
        "",
        "newScope",
        "Lmortar/MortarScope;",
        "start",
        "props",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final container:Lcom/squareup/ui/main/PosContainer;

.field private outputHandler:Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutputHandler;

.field private final workflow:Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsViewFactory;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsWorkflow;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "viewFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    sget-object v0, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsWorkflowRunner;->Companion:Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsWorkflowRunner$Companion;

    invoke-virtual {v0}, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsWorkflowRunner$Companion;->getNAME()Ljava/lang/String;

    move-result-object v2

    .line 17
    invoke-interface {p2}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v3

    .line 18
    move-object v4, p1

    check-cast v4, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x18

    const/4 v8, 0x0

    move-object v1, p0

    .line 14
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;ZLkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p2, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    iput-object p3, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner;->workflow:Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsWorkflow;

    return-void
.end method

.method public static final synthetic access$getOutputHandler$p(Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner;)Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutputHandler;
    .locals 0

    .line 9
    iget-object p0, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner;->outputHandler:Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutputHandler;

    return-object p0
.end method

.method public static final synthetic access$setOutputHandler$p(Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner;Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutputHandler;)V
    .locals 0

    .line 9
    iput-object p1, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner;->outputHandler:Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutputHandler;

    return-void
.end method


# virtual methods
.method protected getWorkflow()Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsWorkflow;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner;->workflow:Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsWorkflow;

    return-object v0
.end method

.method public bridge synthetic getWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner;->getWorkflow()Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsWorkflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "newScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-super {p0, p1}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;->onEnterScope(Lmortar/MortarScope;)V

    .line 25
    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    .line 26
    new-instance v1, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner$onEnterScope$1;

    iget-object v2, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    invoke-direct {v1, v2}, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/main/PosContainer;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "onUpdateScreens()\n      \u2026ibe(container::pushStack)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    .line 29
    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    .line 30
    new-instance v1, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner$onEnterScope$2;-><init>(Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "onResult()\n        .subs\u2026putHandler!!.handle(it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public start(Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutputHandler;)V
    .locals 1

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "outputHandler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-virtual {p0, p1}, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner;->setProps(Ljava/lang/Object;)V

    .line 39
    iput-object p2, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner;->outputHandler:Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutputHandler;

    .line 40
    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner;->ensureWorkflow()V

    return-void
.end method
