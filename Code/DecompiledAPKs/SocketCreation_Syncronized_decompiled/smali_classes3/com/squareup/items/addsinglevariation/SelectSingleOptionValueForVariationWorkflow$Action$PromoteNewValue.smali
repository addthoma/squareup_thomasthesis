.class public final Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;
.super Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action;
.source "SelectSingleOptionValueForVariationWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PromoteNewValue"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectSingleOptionValueForVariationWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectSingleOptionValueForVariationWorkflow.kt\ncom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,211:1\n1550#2,3:212\n*E\n*S KotlinDebug\n*F\n+ 1 SelectSingleOptionValueForVariationWorkflow.kt\ncom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue\n*L\n168#1,3:212\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000e\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006H\u00c6\u0003J-\u0010\u0011\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006H\u00c6\u0001J\u0013\u0010\u0012\u001a\u00020\u00132\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0003H\u00d6\u0001J\u0018\u0010\u0019\u001a\u00020\u001a*\u000e\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\u001d0\u001bH\u0016R\u0017\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000c\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;",
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action;",
        "optionId",
        "",
        "optionValueName",
        "existingValues",
        "",
        "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V",
        "getExistingValues",
        "()Ljava/util/List;",
        "getOptionId",
        "()Ljava/lang/String;",
        "getOptionValueName",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;",
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationOutput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final existingValues:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;"
        }
    .end annotation
.end field

.field private final optionId:Ljava/lang/String;

.field private final optionValueName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;)V"
        }
    .end annotation

    const-string v0, "optionId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "optionValueName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "existingValues"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 161
    invoke-direct {p0, v0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;->optionId:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;->optionValueName:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;->existingValues:Ljava/util/List;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;->optionId:Ljava/lang/String;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;->optionValueName:Ljava/lang/String;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;->existingValues:Ljava/util/List;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;->copy(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;",
            "-",
            "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;

    if-eqz v0, :cond_9

    .line 165
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_8

    check-cast v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;

    .line 167
    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;->optionValueName:Ljava/lang/String;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    .line 168
    :cond_0
    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;->existingValues:Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 212
    instance-of v2, v1, Ljava/util/Collection;

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    move-object v2, v1

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 213
    :cond_1
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    .line 168
    invoke-virtual {v2}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v4, "null cannot be cast to non-null type java.lang.String"

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v5, "(this as java.lang.String).toLowerCase()"

    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v2, :cond_4

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, Lkotlin/text/StringsKt;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;->optionValueName:Ljava/lang/String;

    if-eqz v6, :cond_3

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v3, 0x1

    goto :goto_0

    :cond_3
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v4}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v4}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_6
    :goto_0
    if-eqz v3, :cond_7

    .line 169
    invoke-virtual {v0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->gotoDuplicateOptionValueNameState()Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_1

    .line 171
    :cond_7
    new-instance v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationOutput$SelectionUpdated;

    sget-object v1, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->Companion:Lcom/squareup/cogs/itemoptions/ItemOptionValue$Companion;

    iget-object v2, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;->optionValueName:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;->optionId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/cogs/itemoptions/ItemOptionValue$Companion;->from(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationOutput$SelectionUpdated;-><init>(Lcom/squareup/cogs/itemoptions/ItemOptionValue;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    :goto_1
    return-void

    .line 165
    :cond_8
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.items.addsinglevariation.SelectSingleOptionValueForVariationState.SelectOptionValueState"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 164
    :cond_9
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Failed requirement."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;->optionId:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;->optionValueName:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;->existingValues:Ljava/util/List;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;)",
            "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;"
        }
    .end annotation

    const-string v0, "optionId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "optionValueName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "existingValues"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;->optionId:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;->optionId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;->optionValueName:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;->optionValueName:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;->existingValues:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;->existingValues:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getExistingValues()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;"
        }
    .end annotation

    .line 160
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;->existingValues:Ljava/util/List;

    return-object v0
.end method

.method public final getOptionId()Ljava/lang/String;
    .locals 1

    .line 158
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;->optionId:Ljava/lang/String;

    return-object v0
.end method

.method public final getOptionValueName()Ljava/lang/String;
    .locals 1

    .line 159
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;->optionValueName:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;->optionId:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;->optionValueName:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;->existingValues:Ljava/util/List;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PromoteNewValue(optionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;->optionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", optionValueName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;->optionValueName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", existingValues="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;->existingValues:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
