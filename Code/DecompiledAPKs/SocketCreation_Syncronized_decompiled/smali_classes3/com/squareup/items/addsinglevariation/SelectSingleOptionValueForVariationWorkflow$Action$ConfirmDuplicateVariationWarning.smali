.class public final Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$ConfirmDuplicateVariationWarning;
.super Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action;
.source "SelectSingleOptionValueForVariationWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ConfirmDuplicateVariationWarning"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectSingleOptionValueForVariationWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectSingleOptionValueForVariationWorkflow.kt\ncom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$ConfirmDuplicateVariationWarning\n*L\n1#1,211:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u0004*\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005H\u0016\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$ConfirmDuplicateVariationWarning;",
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action;",
        "()V",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;",
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationOutput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$ConfirmDuplicateVariationWarning;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 201
    new-instance v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$ConfirmDuplicateVariationWarning;

    invoke-direct {v0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$ConfirmDuplicateVariationWarning;-><init>()V

    sput-object v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$ConfirmDuplicateVariationWarning;->INSTANCE:Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$ConfirmDuplicateVariationWarning;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 201
    invoke-direct {p0, v0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;",
            "-",
            "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 204
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateVariationState;

    if-eqz v0, :cond_1

    .line 205
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateVariationState;

    .line 206
    invoke-virtual {v0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateVariationState;->gotoSelectOptionValueState()Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void

    .line 205
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.items.addsinglevariation.SelectSingleOptionValueForVariationState.DuplicateVariationState"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 204
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Failed requirement."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
