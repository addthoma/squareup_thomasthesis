.class public final Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;
.super Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;
.source "SelectSingleOptionValueForVariationState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DuplicateOptionValueNameState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0010\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B+\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u0015\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0008H\u00c6\u0003J3\u0010\u0017\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008H\u00c6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001J\u0013\u0010\u001a\u001a\u00020\u00082\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u00d6\u0003J\u0006\u0010\u001d\u001a\u00020\u001eJ\t\u0010\u001f\u001a\u00020\u0019H\u00d6\u0001J\t\u0010 \u001a\u00020\u0003H\u00d6\u0001J\u0019\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020\u0019H\u00d6\u0001R\u0014\u0010\u0004\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u000bR\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0014\u0010\u000f\u001a\u00020\u00088VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0011R\u0014\u0010\u0007\u001a\u00020\u0008X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0011\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;",
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;",
        "searchText",
        "",
        "newValueInEdit",
        "selectedValue",
        "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "shouldHighlightDuplicateName",
        "",
        "(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cogs/itemoptions/ItemOptionValue;Z)V",
        "getNewValueInEdit",
        "()Ljava/lang/String;",
        "getSearchText",
        "getSelectedValue",
        "()Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "shouldFocusOnNewValueRowAndShowKeyboard",
        "getShouldFocusOnNewValueRowAndShowKeyboard",
        "()Z",
        "getShouldHighlightDuplicateName",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "gotoSelectOptionValueState",
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;",
        "hashCode",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final newValueInEdit:Ljava/lang/String;

.field private final searchText:Ljava/lang/String;

.field private final selectedValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

.field private final shouldHighlightDuplicateName:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState$Creator;

    invoke-direct {v0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState$Creator;-><init>()V

    sput-object v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cogs/itemoptions/ItemOptionValue;Z)V
    .locals 1

    const-string v0, "searchText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newValueInEdit"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 55
    invoke-direct {p0, v0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->searchText:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->newValueInEdit:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->selectedValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    iput-boolean p4, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->shouldHighlightDuplicateName:Z

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    const-string p1, ""

    :cond_0
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_1

    const/4 p4, 0x0

    .line 54
    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cogs/itemoptions/ItemOptionValue;Z)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZILjava/lang/Object;)Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->getSearchText()Ljava/lang/String;

    move-result-object p1

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->getNewValueInEdit()Ljava/lang/String;

    move-result-object p2

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->getSelectedValue()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object p3

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->getShouldHighlightDuplicateName()Z

    move-result p4

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->copy(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cogs/itemoptions/ItemOptionValue;Z)Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->getSearchText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->getNewValueInEdit()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component3()Lcom/squareup/cogs/itemoptions/ItemOptionValue;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->getSelectedValue()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v0

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->getShouldHighlightDuplicateName()Z

    move-result v0

    return v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cogs/itemoptions/ItemOptionValue;Z)Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;
    .locals 1

    const-string v0, "searchText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newValueInEdit"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cogs/itemoptions/ItemOptionValue;Z)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->getSearchText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->getSearchText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->getNewValueInEdit()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->getNewValueInEdit()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->getSelectedValue()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->getSelectedValue()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->getShouldHighlightDuplicateName()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->getShouldHighlightDuplicateName()Z

    move-result p1

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getNewValueInEdit()Ljava/lang/String;
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->newValueInEdit:Ljava/lang/String;

    return-object v0
.end method

.method public getSearchText()Ljava/lang/String;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->searchText:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectedValue()Lcom/squareup/cogs/itemoptions/ItemOptionValue;
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->selectedValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    return-object v0
.end method

.method public getShouldFocusOnNewValueRowAndShowKeyboard()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getShouldHighlightDuplicateName()Z
    .locals 1

    .line 54
    iget-boolean v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->shouldHighlightDuplicateName:Z

    return v0
.end method

.method public final gotoSelectOptionValueState()Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;
    .locals 7

    .line 59
    new-instance v6, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;

    .line 60
    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->getSearchText()Ljava/lang/String;

    move-result-object v1

    .line 61
    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->getNewValueInEdit()Ljava/lang/String;

    move-result-object v2

    .line 62
    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->getSelectedValue()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x1

    move-object v0, v6

    .line 59
    invoke-direct/range {v0 .. v5}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZZ)V

    return-object v6
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->getSearchText()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->getNewValueInEdit()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->getSelectedValue()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->getShouldHighlightDuplicateName()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DuplicateOptionValueNameState(searchText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->getSearchText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", newValueInEdit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->getNewValueInEdit()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", selectedValue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->getSelectedValue()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", shouldHighlightDuplicateName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->getShouldHighlightDuplicateName()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->searchText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->newValueInEdit:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->selectedValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-boolean p2, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;->shouldHighlightDuplicateName:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
