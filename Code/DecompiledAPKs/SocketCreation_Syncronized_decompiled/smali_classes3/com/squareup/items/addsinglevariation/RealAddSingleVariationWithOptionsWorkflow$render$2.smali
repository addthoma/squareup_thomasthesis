.class final Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$render$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealAddSingleVariationWithOptionsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow;->render(Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsState;",
        "+",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsState;",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutput;",
        "it",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;


# direct methods
.method constructor <init>(Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$render$2;->$props:Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsState;",
            "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    instance-of v0, p1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$Canceled;

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CancelAddingVariation;->INSTANCE:Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CancelAddingVariation;

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 85
    :cond_0
    instance-of v0, p1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;

    .line 86
    check-cast p1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;

    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;->getNewVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object v1

    .line 87
    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;->getNewOptionValues()Ljava/util/List;

    move-result-object p1

    .line 88
    iget-object v2, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$render$2;->$props:Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;

    invoke-virtual {v2}, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;->getExistingVariations()Ljava/util/List;

    move-result-object v2

    .line 89
    iget-object v3, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$render$2;->$props:Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;

    invoke-virtual {v3}, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;->getAssignedOptions()Ljava/util/List;

    move-result-object v3

    .line 85
    invoke-direct {v0, v1, p1, v2, v3}, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;-><init>(Lcom/squareup/shared/catalog/models/CatalogItemVariation;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$render$2;->invoke(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
