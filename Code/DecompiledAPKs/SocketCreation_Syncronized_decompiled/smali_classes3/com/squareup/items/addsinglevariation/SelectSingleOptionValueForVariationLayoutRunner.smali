.class public final Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner;
.super Ljava/lang/Object;
.source "SelectSingleOptionValueForVariationLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$Factory;,
        Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$SelectSingleOptionValueRow;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectSingleOptionValueForVariationLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectSingleOptionValueForVariationLayoutRunner.kt\ncom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 4 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 5 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 6 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec\n+ 7 RecyclerEdges.kt\ncom/squareup/noho/dsl/RecyclerEdgesKt\n*L\n1#1,274:1\n1360#2:275\n1429#2,3:276\n49#3:279\n50#3,3:285\n53#3:320\n599#4,4:280\n601#4:284\n310#5,6:288\n310#5,3:294\n313#5,3:303\n310#5,6:306\n310#5,6:312\n35#6,6:297\n43#7,2:318\n*E\n*S KotlinDebug\n*F\n+ 1 SelectSingleOptionValueForVariationLayoutRunner.kt\ncom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner\n*L\n200#1:275\n200#1,3:276\n57#1:279\n57#1,3:285\n57#1:320\n57#1,4:280\n57#1:284\n57#1,6:288\n57#1,3:294\n57#1,3:303\n57#1,6:306\n57#1,6:312\n57#1,6:297\n57#1,2:318\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u001a\u001bB\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0018\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00022\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u0010\u0010\u0017\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0002H\u0002J\u0010\u0010\u0018\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0002H\u0002J\u0010\u0010\u0019\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0002H\u0002R\u0016\u0010\u0008\u001a\n \n*\u0004\u0018\u00010\t0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000e\u001a\n \n*\u0004\u0018\u00010\u000f0\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0010\u001a\n \n*\u0004\u0018\u00010\u00110\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;",
        "view",
        "Landroid/view/View;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "kotlin.jvm.PlatformType",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$SelectSingleOptionValueRow;",
        "recyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "searchBar",
        "Lcom/squareup/noho/NohoEditRow;",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "updateActionBar",
        "updateOptionValues",
        "updateSearchBar",
        "Factory",
        "SelectSingleOptionValueRow",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final recycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$SelectSingleOptionValueRow;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private final searchBar:Lcom/squareup/noho/NohoEditRow;

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner;->view:Landroid/view/View;

    .line 53
    iget-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoActionBar;

    iput-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 54
    iget-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/items/assignitemoptions/impl/R$id;->option_value_search_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoEditRow;

    iput-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner;->searchBar:Lcom/squareup/noho/NohoEditRow;

    .line 56
    iget-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/items/assignitemoptions/impl/R$id;->select_single_value_for_variation_recycler:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 57
    iget-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const-string v0, "recyclerView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 279
    sget-object v0, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 280
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 281
    new-instance v0, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v0}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 285
    invoke-virtual {p2}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 286
    invoke-virtual {p2}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 58
    sget-object p2, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$recycler$1$1;->INSTANCE:Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$recycler$1$1;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->stableId(Lkotlin/jvm/functions/Function1;)V

    .line 289
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$$special$$inlined$row$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 64
    sget-object v1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$recycler$1$2$1;->INSTANCE:Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$recycler$1$2$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 78
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    sget-object v1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$recycler$1$2$2;->INSTANCE:Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$recycler$1$2$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p2, v1}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->edges(Lcom/squareup/cycler/Recycler$RowSpec;Lkotlin/jvm/functions/Function1;)V

    .line 288
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 295
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$$special$$inlined$row$2;->INSTANCE:Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$$special$$inlined$row$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 82
    sget v1, Lcom/squareup/items/assignitemoptions/impl/R$layout;->select_option_values_add_option_row:I

    .line 297
    new-instance v2, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$$special$$inlined$create$1;

    invoke-direct {v2, v1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$$special$$inlined$create$1;-><init>(I)V

    check-cast v2, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v2}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 295
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 294
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 307
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$$special$$inlined$row$3;->INSTANCE:Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$$special$$inlined$row$3;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 118
    sget-object v1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$recycler$1$4$1;->INSTANCE:Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$recycler$1$4$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 307
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 306
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 313
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$$special$$inlined$row$4;->INSTANCE:Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$$special$$inlined$row$4;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 136
    sget-object v1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$recycler$1$5$1;->INSTANCE:Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$recycler$1$5$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 313
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 312
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 318
    new-instance p2, Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    invoke-direct {p2}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;-><init>()V

    const/4 v1, 0x0

    .line 156
    invoke-virtual {p2, v1}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->setDefault(I)V

    check-cast p2, Lcom/squareup/cycler/ExtensionSpec;

    .line 318
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->extension(Lcom/squareup/cycler/ExtensionSpec;)V

    .line 283
    invoke-virtual {v0, p1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner;->recycler:Lcom/squareup/cycler/Recycler;

    return-void

    .line 280
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final updateActionBar(Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;)V
    .locals 5

    .line 182
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 172
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 174
    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/cogs/itemoptions/ItemOption;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 175
    new-instance v2, Lcom/squareup/resources/PhraseModel;

    sget v3, Lcom/squareup/items/assignitemoptions/impl/R$string;->combined_name_and_display_name:I

    invoke-direct {v2, v3}, Lcom/squareup/resources/PhraseModel;-><init>(I)V

    .line 176
    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/cogs/itemoptions/ItemOption;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "option_name"

    invoke-virtual {v2, v4, v3}, Lcom/squareup/resources/PhraseModel;->with(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/resources/PhraseModel;

    move-result-object v2

    .line 177
    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/cogs/itemoptions/ItemOption;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "option_display_name"

    invoke-virtual {v2, v4, v3}, Lcom/squareup/resources/PhraseModel;->with(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/resources/PhraseModel;

    move-result-object v2

    check-cast v2, Lcom/squareup/resources/TextModel;

    goto :goto_0

    .line 179
    :cond_0
    new-instance v2, Lcom/squareup/resources/FixedText;

    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/cogs/itemoptions/ItemOption;->getName()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-direct {v2, v3}, Lcom/squareup/resources/FixedText;-><init>(Ljava/lang/CharSequence;)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    .line 173
    :goto_0
    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 181
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$updateActionBar$1;

    invoke-direct {v3, p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$updateActionBar$1;-><init>(Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 182
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final updateOptionValues(Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;)V
    .locals 10

    .line 200
    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->getAvailableValues()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 275
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 276
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 277
    check-cast v2, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    .line 202
    invoke-virtual {v2}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getName()Ljava/lang/String;

    move-result-object v3

    .line 203
    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->getSelectedValue()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v4

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    .line 205
    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->getShouldHighlightDuplicateNewValueName()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-virtual {v2}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "null cannot be cast to non-null type java.lang.String"

    if-eqz v5, :cond_4

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    const-string v7, "(this as java.lang.String).toLowerCase()"

    invoke-static {v5, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "null cannot be cast to non-null type kotlin.CharSequence"

    if-eqz v5, :cond_3

    check-cast v5, Ljava/lang/CharSequence;

    invoke-static {v5}, Lkotlin/text/StringsKt;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->getNewValueInEdit()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v9

    invoke-virtual {v9}, Lcom/squareup/workflow/text/WorkflowEditableText;->getText()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_2

    if-eqz v9, :cond_1

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v6, :cond_2

    if-eqz v6, :cond_0

    check-cast v6, Ljava/lang/CharSequence;

    invoke-static {v6}, Lkotlin/text/StringsKt;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v8}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v6}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    const/4 v6, 0x0

    :goto_1
    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    const/4 v5, 0x1

    goto :goto_2

    :cond_3
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v8}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v6}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    const/4 v5, 0x0

    .line 206
    :goto_2
    new-instance v6, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$updateOptionValues$$inlined$map$lambda$1;

    invoke-direct {v6, v2, p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$updateOptionValues$$inlined$map$lambda$1;-><init>(Lcom/squareup/cogs/itemoptions/ItemOptionValue;Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;)V

    check-cast v6, Lkotlin/jvm/functions/Function0;

    .line 201
    new-instance v2, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$SelectSingleOptionValueRow$OptionValueSelectionRow;

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$SelectSingleOptionValueRow$OptionValueSelectionRow;-><init>(Ljava/lang/String;ZZLkotlin/jvm/functions/Function0;)V

    .line 208
    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 278
    :cond_6
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    .line 210
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    .line 212
    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->shouldShowSearchResults()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 213
    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->getAvailableValues()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->getHasReachedOptionValueNumberLimit()Z

    move-result v1

    if-nez v1, :cond_a

    .line 215
    new-instance v1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$SelectSingleOptionValueRow$CreateFromSearchButtonRow;

    .line 216
    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->getSearchText()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/workflow/text/WorkflowEditableText;->getText()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_7

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 217
    :cond_7
    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->getOnNewValueFromCreateButton()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    .line 215
    invoke-direct {v1, v2, p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$SelectSingleOptionValueRow$CreateFromSearchButtonRow;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    .line 214
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 224
    :cond_8
    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->getHasReachedOptionValueNumberLimit()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 225
    sget-object p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$SelectSingleOptionValueRow$OptionValueNumberLimitHelpTextRow;->INSTANCE:Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$SelectSingleOptionValueRow$OptionValueNumberLimitHelpTextRow;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 228
    :cond_9
    new-instance v1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$SelectSingleOptionValueRow$AddNewOptionValueRow;

    .line 229
    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->getNewValueInEdit()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v2

    .line 230
    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->getShouldHighlightDuplicateNewValueName()Z

    move-result v3

    .line 231
    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->getShouldFocusOnNewValueRowAndShowKeyboard()Z

    move-result v4

    .line 232
    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->getOnNewOptionValuePromoted()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    .line 228
    invoke-direct {v1, v2, v3, v4, p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$SelectSingleOptionValueRow$AddNewOptionValueRow;-><init>(Lcom/squareup/workflow/text/WorkflowEditableText;ZZLkotlin/jvm/functions/Function0;)V

    .line 227
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 237
    :cond_a
    :goto_3
    iget-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner;->recycler:Lcom/squareup/cycler/Recycler;

    new-instance v1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$updateOptionValues$1;

    invoke-direct {v1, v0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$updateOptionValues$1;-><init>(Ljava/util/List;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1}, Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final updateSearchBar(Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;)V
    .locals 4

    .line 188
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner;->searchBar:Lcom/squareup/noho/NohoEditRow;

    .line 189
    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner;->view:Landroid/view/View;

    sget v2, Lcom/squareup/items/assignitemoptions/impl/R$string;->select_option_values_search_bar_hint_format:I

    invoke-static {v1, v2}, Lcom/squareup/phrase/Phrase;->from(Landroid/view/View;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 190
    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/cogs/itemoptions/ItemOption;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    const-string v3, "option"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 191
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 192
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 188
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditRow;->setHint(Ljava/lang/CharSequence;)V

    .line 194
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner;->searchBar:Lcom/squareup/noho/NohoEditRow;

    const-string v1, "searchBar"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->getSearchText()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/workflow/text/EditTextsKt;->setWorkflowText(Landroid/widget/EditText;Lcom/squareup/workflow/text/WorkflowEditableText;)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 163
    invoke-direct {p0, p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner;->updateActionBar(Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;)V

    .line 164
    invoke-direct {p0, p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner;->updateSearchBar(Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;)V

    .line 165
    invoke-direct {p0, p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner;->updateOptionValues(Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;)V

    .line 166
    iget-object p2, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$showRendering$1;-><init>(Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner;->showRendering(Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
