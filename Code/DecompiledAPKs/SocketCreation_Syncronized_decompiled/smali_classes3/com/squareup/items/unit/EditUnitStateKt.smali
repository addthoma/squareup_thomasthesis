.class public final Lcom/squareup/items/unit/EditUnitStateKt;
.super Ljava/lang/Object;
.source "EditUnitState.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditUnitState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditUnitState.kt\ncom/squareup/items/unit/EditUnitStateKt\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 4 BuffersProtos.kt\ncom/squareup/workflow/BuffersProtos\n*L\n1#1,235:1\n158#2,3:236\n161#2:241\n165#2:242\n1591#3,2:239\n56#4:243\n*E\n*S KotlinDebug\n*F\n+ 1 EditUnitState.kt\ncom/squareup/items/unit/EditUnitStateKt\n*L\n189#1,3:236\n189#1:241\n210#1:242\n189#1,2:239\n210#1:243\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u000c\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0002\u001a\u0014\u0010\u0003\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0001H\u0002\u00a8\u0006\u0007"
    }
    d2 = {
        "readRequiresInternet",
        "Lcom/squareup/items/unit/EditUnitState$RequiresInternet;",
        "Lokio/BufferedSource;",
        "writeRequiresInternet",
        "",
        "Lokio/BufferedSink;",
        "requiresInternet",
        "edit-unit_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$readRequiresInternet(Lokio/BufferedSource;)Lcom/squareup/items/unit/EditUnitState$RequiresInternet;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/items/unit/EditUnitStateKt;->readRequiresInternet(Lokio/BufferedSource;)Lcom/squareup/items/unit/EditUnitState$RequiresInternet;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$writeRequiresInternet(Lokio/BufferedSink;Lcom/squareup/items/unit/EditUnitState$RequiresInternet;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/items/unit/EditUnitStateKt;->writeRequiresInternet(Lokio/BufferedSink;Lcom/squareup/items/unit/EditUnitState$RequiresInternet;)V

    return-void
.end method

.method private static final readRequiresInternet(Lokio/BufferedSource;)Lcom/squareup/items/unit/EditUnitState$RequiresInternet;
    .locals 7

    .line 209
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 210
    const-class v1, Lcom/squareup/items/unit/EditUnitState$StandardUnitsList;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 242
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    .line 243
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v4, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    invoke-virtual {v3, v4}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object v3

    if-eqz v3, :cond_0

    check-cast v3, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    .line 213
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v4

    .line 214
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v5

    .line 211
    new-instance v6, Lcom/squareup/items/unit/LocalizedStandardUnit;

    invoke-direct {v6, v3, v4, v5}, Lcom/squareup/items/unit/LocalizedStandardUnit;-><init>(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 243
    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.protos.connect.v2.common.MeasurementUnit"

    invoke-direct {p0, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 242
    :cond_1
    check-cast v1, Ljava/util/List;

    .line 210
    new-instance p0, Lcom/squareup/items/unit/EditUnitState$StandardUnitsList;

    invoke-direct {p0, v1}, Lcom/squareup/items/unit/EditUnitState$StandardUnitsList;-><init>(Ljava/util/List;)V

    check-cast p0, Lcom/squareup/items/unit/EditUnitState$RequiresInternet;

    goto :goto_1

    .line 218
    :cond_2
    const-class v1, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v0, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    .line 219
    sget-object v1, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper;->Companion:Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;

    invoke-virtual {v1, p0}, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;->readCatalogMeasurementUnit(Lokio/BufferedSource;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v1

    .line 220
    new-instance v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    sget-object v3, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper;->Companion:Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;

    invoke-virtual {v3, p0}, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;->readCatalogMeasurementUnit(Lokio/BufferedSource;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    .line 221
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result p0

    .line 218
    invoke-direct {v0, v1, v2, p0}, Lcom/squareup/items/unit/EditUnitState$EditUnit;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;Z)V

    move-object p0, v0

    check-cast p0, Lcom/squareup/items/unit/EditUnitState$RequiresInternet;

    :goto_1
    return-object p0

    .line 224
    :cond_3
    new-instance p0, Ljava/lang/IllegalArgumentException;

    .line 225
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Please add a branch for type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " in method BufferedSource.readRequiresInternet"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 224
    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method private static final writeRequiresInternet(Lokio/BufferedSink;Lcom/squareup/items/unit/EditUnitState$RequiresInternet;)V
    .locals 3

    .line 185
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "requiresInternet::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 186
    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 188
    instance-of v1, p1, Lcom/squareup/items/unit/EditUnitState$StandardUnitsList;

    if-eqz v1, :cond_0

    .line 189
    check-cast p1, Lcom/squareup/items/unit/EditUnitState$StandardUnitsList;

    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitState$StandardUnitsList;->getStandardUnits()Ljava/util/List;

    move-result-object p1

    .line 237
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p0, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 238
    check-cast p1, Ljava/lang/Iterable;

    .line 239
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 238
    check-cast v0, Lcom/squareup/items/unit/LocalizedStandardUnit;

    .line 190
    invoke-virtual {v0}, Lcom/squareup/items/unit/LocalizedStandardUnit;->getMeasurementUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    check-cast v1, Lcom/squareup/wire/Message;

    invoke-static {p0, v1}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    .line 191
    invoke-virtual {v0}, Lcom/squareup/items/unit/LocalizedStandardUnit;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 192
    invoke-virtual {v0}, Lcom/squareup/items/unit/LocalizedStandardUnit;->getAbbreviation()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    goto :goto_0

    .line 196
    :cond_0
    instance-of v1, p1, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    if-eqz v1, :cond_2

    .line 197
    sget-object v0, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper;->Companion:Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;

    check-cast p1, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->getOriginalUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;->writeCatalogMeasurementUnit(Lokio/BufferedSink;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    .line 198
    sget-object v0, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper;->Companion:Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;

    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->getCurrentUnitInEditing()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v1

    const-string v2, "requiresInternet.currentUnitInEditing.build()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p0, v1}, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;->writeCatalogMeasurementUnit(Lokio/BufferedSink;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    .line 199
    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->isDefaultUnit()Z

    move-result p1

    invoke-static {p0, p1}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    :cond_1
    return-void

    .line 202
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    .line 203
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Please add a branch for type "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " in method BufferedSink.writeRequiresInternet"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 202
    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method
