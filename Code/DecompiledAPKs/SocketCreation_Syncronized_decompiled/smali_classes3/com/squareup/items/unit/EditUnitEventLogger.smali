.class public Lcom/squareup/items/unit/EditUnitEventLogger;
.super Ljava/lang/Object;
.source "EditUnitEventLogger.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0008\u0016\u0018\u00002\u00020\u0001:\u0001\u0012B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016J\u0010\u0010\r\u001a\u00020\u00082\u0006\u0010\u000e\u001a\u00020\nH\u0016J\u0018\u0010\u000f\u001a\u00020\u00082\u0006\u0010\u0010\u001a\u00020\n2\u0006\u0010\u0011\u001a\u00020\nH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/items/unit/EditUnitEventLogger;",
        "",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;)V",
        "logMeasurementUnitCreate",
        "",
        "createdUnit",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
        "source",
        "Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;",
        "logMeasurementUnitDelete",
        "deletedUnit",
        "logMeasurementUnitEdit",
        "editedUnit",
        "originalUnit",
        "UnitCreationSource",
        "edit-unit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/unit/EditUnitEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p2, p0, Lcom/squareup/items/unit/EditUnitEventLogger;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public logMeasurementUnitCreate(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;)V
    .locals 7

    const-string v0, "createdUnit"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    sget-object v1, Lcom/squareup/catalog/event/CatalogFeature;->MEASUREMENT_UNIT_CREATED:Lcom/squareup/catalog/event/CatalogFeature;

    .line 25
    iget-object v2, p0, Lcom/squareup/items/unit/EditUnitEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    .line 26
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getId()Ljava/lang/String;

    move-result-object v3

    const-string v0, "createdUnit.id"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getPrecision()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 28
    invoke-static {p1}, Lcom/squareup/items/unit/EditUnitEventLoggerKt;->unitEventDetail(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Ljava/lang/String;

    move-result-object v5

    .line 29
    invoke-virtual {p2}, Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;->getSourceString()Ljava/lang/String;

    move-result-object v6

    .line 24
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/catalog/event/CatalogFeature;->log(Lcom/squareup/analytics/Analytics;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public logMeasurementUnitDelete(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V
    .locals 9

    const-string v0, "deletedUnit"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    sget-object v1, Lcom/squareup/catalog/event/CatalogFeature;->MEASUREMENT_UNIT_DELETED:Lcom/squareup/catalog/event/CatalogFeature;

    .line 59
    iget-object v2, p0, Lcom/squareup/items/unit/EditUnitEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getId()Ljava/lang/String;

    move-result-object v3

    const-string v0, "deletedUnit.id"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getPrecision()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {p1}, Lcom/squareup/items/unit/EditUnitEventLoggerKt;->unitEventDetail(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/16 v7, 0x10

    const/4 v8, 0x0

    .line 58
    invoke-static/range {v1 .. v8}, Lcom/squareup/catalog/event/CatalogFeature;->log$default(Lcom/squareup/catalog/event/CatalogFeature;Lcom/squareup/analytics/Analytics;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method public logMeasurementUnitEdit(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V
    .locals 11

    const-string v0, "editedUnit"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "originalUnit"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    sget-object v1, Lcom/squareup/catalog/event/CatalogFeature;->MEASUREMENT_UNIT_EDITED:Lcom/squareup/catalog/event/CatalogFeature;

    .line 38
    iget-object v2, p0, Lcom/squareup/items/unit/EditUnitEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getId()Ljava/lang/String;

    move-result-object v3

    const-string v0, "editedUnit.id"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getPrecision()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {p1}, Lcom/squareup/items/unit/EditUnitEventLoggerKt;->unitEventDetail(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/16 v7, 0x10

    const/4 v8, 0x0

    .line 37
    invoke-static/range {v1 .. v8}, Lcom/squareup/catalog/event/CatalogFeature;->log$default(Lcom/squareup/catalog/event/CatalogFeature;Lcom/squareup/analytics/Analytics;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    .line 40
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getPrecision()I

    move-result v1

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getPrecision()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 41
    sget-object v3, Lcom/squareup/catalog/event/CatalogFeature;->MEASUREMENT_UNIT_PRECISION_CHANGED:Lcom/squareup/catalog/event/CatalogFeature;

    .line 42
    iget-object v4, p0, Lcom/squareup/items/unit/EditUnitEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getPrecision()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 43
    invoke-static {p1}, Lcom/squareup/items/unit/EditUnitEventLoggerKt;->unitEventDetail(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/16 v9, 0x10

    const/4 v10, 0x0

    .line 41
    invoke-static/range {v3 .. v10}, Lcom/squareup/catalog/event/CatalogFeature;->log$default(Lcom/squareup/catalog/event/CatalogFeature;Lcom/squareup/analytics/Analytics;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    .line 46
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getMeasurementUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const-string v2, "editedUnit.measurementUnit"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/squareup/items/unit/ui/EditUnitCoordinatorKt;->isCustomUnit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 47
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getMeasurementUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/squareup/items/unit/EditUnitEventLogger;->res:Lcom/squareup/util/Res;

    invoke-static {v1, v2}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedAbbreviation(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v1

    .line 48
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getMeasurementUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object p2

    const-string v2, "originalUnit.measurementUnit"

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/squareup/items/unit/EditUnitEventLogger;->res:Lcom/squareup/util/Res;

    invoke-static {p2, v2}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedAbbreviation(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p2

    invoke-static {v1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    xor-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_1

    .line 50
    sget-object v1, Lcom/squareup/catalog/event/CatalogFeature;->MEASUREMENT_UNIT_ABBREVIATION_CHANGED:Lcom/squareup/catalog/event/CatalogFeature;

    .line 51
    iget-object v2, p0, Lcom/squareup/items/unit/EditUnitEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getPrecision()I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 52
    invoke-static {p1}, Lcom/squareup/items/unit/EditUnitEventLoggerKt;->unitEventDetail(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/16 v7, 0x10

    const/4 v8, 0x0

    .line 50
    invoke-static/range {v1 .. v8}, Lcom/squareup/catalog/event/CatalogFeature;->log$default(Lcom/squareup/catalog/event/CatalogFeature;Lcom/squareup/analytics/Analytics;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    :cond_1
    return-void
.end method
