.class final Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EditUnitViewBindings.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/unit/ui/EditUnitViewBindings;->getBindings()Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lio/reactivex/Observable<",
        "Lcom/squareup/workflow/legacy/Screen;",
        ">;",
        "Lcom/squareup/items/unit/ui/DuplicateCustomUnitNameDialogFactory;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\u0010\u0000\u001a\u00020\u00012\u0018\u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/items/unit/ui/DuplicateCustomUnitNameDialogFactory;",
        "it",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/items/unit/DuplicateCustomUnitNameScreen;",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/items/unit/ui/EditUnitViewBindings;


# direct methods
.method constructor <init>(Lcom/squareup/items/unit/ui/EditUnitViewBindings;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$1;->this$0:Lcom/squareup/items/unit/ui/EditUnitViewBindings;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lio/reactivex/Observable;)Lcom/squareup/items/unit/ui/DuplicateCustomUnitNameDialogFactory;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)",
            "Lcom/squareup/items/unit/ui/DuplicateCustomUnitNameDialogFactory;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    new-instance v0, Lcom/squareup/items/unit/ui/DuplicateCustomUnitNameDialogFactory;

    iget-object v1, p0, Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$1;->this$0:Lcom/squareup/items/unit/ui/EditUnitViewBindings;

    invoke-static {v1}, Lcom/squareup/items/unit/ui/EditUnitViewBindings;->access$getRes$p(Lcom/squareup/items/unit/ui/EditUnitViewBindings;)Lcom/squareup/util/Res;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/squareup/items/unit/ui/DuplicateCustomUnitNameDialogFactory;-><init>(Lio/reactivex/Observable;Lcom/squareup/util/Res;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 25
    check-cast p1, Lio/reactivex/Observable;

    invoke-virtual {p0, p1}, Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$1;->invoke(Lio/reactivex/Observable;)Lcom/squareup/items/unit/ui/DuplicateCustomUnitNameDialogFactory;

    move-result-object p1

    return-object p1
.end method
