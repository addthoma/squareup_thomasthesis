.class public final Lcom/squareup/items/unit/ui/CountOfAffectedVariationsAlertDialogFactory;
.super Ljava/lang/Object;
.source "CountOfAffectedVariationsAlertDialogFactory.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B1\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0016\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0018\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u0005H\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/items/unit/ui/CountOfAffectedVariationsAlertDialogFactory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lio/reactivex/Observable;Lcom/squareup/util/Res;)V",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "createDialog",
        "Landroid/app/AlertDialog;",
        "screen",
        "edit-unit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final res:Lcom/squareup/util/Res;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/unit/ui/CountOfAffectedVariationsAlertDialogFactory;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/items/unit/ui/CountOfAffectedVariationsAlertDialogFactory;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public static final synthetic access$createDialog(Lcom/squareup/items/unit/ui/CountOfAffectedVariationsAlertDialogFactory;Landroid/content/Context;Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen;)Landroid/app/AlertDialog;
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2}, Lcom/squareup/items/unit/ui/CountOfAffectedVariationsAlertDialogFactory;->createDialog(Landroid/content/Context;Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen;)Landroid/app/AlertDialog;

    move-result-object p0

    return-object p0
.end method

.method private final createDialog(Landroid/content/Context;Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen;)Landroid/app/AlertDialog;
    .locals 12

    .line 37
    invoke-virtual {p2}, Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen;->getState()Lcom/squareup/items/unit/SaveUnitState$DisplayCountOfAffectedVariations;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/items/unit/SaveUnitState$DisplayCountOfAffectedVariations;->getIncomingSaving()Lcom/squareup/items/unit/SaveUnitState$Saving;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/items/unit/SaveUnitState$Saving;->getSaveUnitAction()Lcom/squareup/items/unit/SaveUnitAction;

    move-result-object v0

    sget-object v1, Lcom/squareup/items/unit/SaveUnitAction;->DELETE:Lcom/squareup/items/unit/SaveUnitAction;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 38
    :goto_0
    invoke-virtual {p2}, Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen;->getState()Lcom/squareup/items/unit/SaveUnitState$DisplayCountOfAffectedVariations;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/items/unit/SaveUnitState$DisplayCountOfAffectedVariations;->getCount()I

    move-result v1

    .line 39
    invoke-virtual {p2}, Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen;->getState()Lcom/squareup/items/unit/SaveUnitState$DisplayCountOfAffectedVariations;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/items/unit/SaveUnitState$DisplayCountOfAffectedVariations;->getIncomingSaving()Lcom/squareup/items/unit/SaveUnitState$Saving;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/items/unit/SaveUnitState$Saving;->getUnitToSave()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/items/unit/ui/CountOfAffectedVariationsAlertDialogFactory;->res:Lcom/squareup/util/Res;

    invoke-static {v4, v5}, Lcom/squareup/quantity/UnitDisplayDataKt;->getUnitDisplayData(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/util/Res;)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/quantity/UnitDisplayData;->getUnitName()Ljava/lang/String;

    move-result-object v4

    if-eqz v0, :cond_1

    .line 43
    sget v5, Lcom/squareup/editunit/R$string;->count_of_affected_variations_alert_confirm_button_text_for_deletion:I

    goto :goto_1

    .line 44
    :cond_1
    sget v5, Lcom/squareup/editunit/R$string;->count_of_affected_variations_alert_confirm_button_text_for_editing:I

    :goto_1
    if-eqz v0, :cond_2

    .line 47
    sget v6, Lcom/squareup/editunit/R$string;->count_of_affected_variations_alert_title_for_deletion:I

    goto :goto_2

    .line 48
    :cond_2
    sget v6, Lcom/squareup/editunit/R$string;->count_of_affected_variations_alert_title_for_editing:I

    :goto_2
    const/16 v7, 0x2b

    const/16 v8, 0x64

    const-string v9, "count"

    if-eqz v0, :cond_5

    const-string/jumbo v10, "unit_name"

    if-ne v1, v3, :cond_3

    .line 53
    iget-object v1, p0, Lcom/squareup/items/unit/ui/CountOfAffectedVariationsAlertDialogFactory;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/editunit/R$string;->count_of_affected_variations_alert_message_for_deletion_singular:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 54
    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v1, v10, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 55
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_5

    .line 57
    :cond_3
    iget-object v3, p0, Lcom/squareup/items/unit/ui/CountOfAffectedVariationsAlertDialogFactory;->res:Lcom/squareup/util/Res;

    sget v11, Lcom/squareup/editunit/R$string;->count_of_affected_variations_alert_message_for_deletion_plural:I

    invoke-interface {v3, v11}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    .line 58
    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v3, v10, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    if-le v1, v8, :cond_4

    .line 61
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    :cond_4
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    :goto_3
    check-cast v1, Ljava/lang/CharSequence;

    .line 59
    invoke-virtual {v3, v9, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 63
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_5

    :cond_5
    if-ne v1, v3, :cond_6

    .line 67
    iget-object v1, p0, Lcom/squareup/items/unit/ui/CountOfAffectedVariationsAlertDialogFactory;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/editunit/R$string;->count_of_affected_variations_alert_message_for_editing_singular:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    goto :goto_5

    .line 69
    :cond_6
    iget-object v3, p0, Lcom/squareup/items/unit/ui/CountOfAffectedVariationsAlertDialogFactory;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/editunit/R$string;->count_of_affected_variations_alert_message_for_editing_plural:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    if-le v1, v8, :cond_7

    .line 72
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_4

    :cond_7
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    :goto_4
    check-cast v1, Ljava/lang/CharSequence;

    .line 70
    invoke-virtual {v3, v9, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 74
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    :goto_5
    if-eqz v0, :cond_8

    .line 79
    sget v0, Lcom/squareup/marin/R$color;->marin_red:I

    goto :goto_6

    .line 80
    :cond_8
    sget v0, Lcom/squareup/marin/R$color;->marin_blue:I

    .line 82
    :goto_6
    new-instance v3, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v3, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 83
    invoke-virtual {v3, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonBackground(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 84
    sget v0, Lcom/squareup/marin/R$color;->marin_white:I

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonTextColor(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 85
    new-instance v0, Lcom/squareup/items/unit/ui/CountOfAffectedVariationsAlertDialogFactory$createDialog$1;

    invoke-direct {v0, p2}, Lcom/squareup/items/unit/ui/CountOfAffectedVariationsAlertDialogFactory$createDialog$1;-><init>(Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen;)V

    check-cast v0, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p1, v5, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 88
    sget v0, Lcom/squareup/common/strings/R$string;->cancel:I

    new-instance v3, Lcom/squareup/items/unit/ui/CountOfAffectedVariationsAlertDialogFactory$createDialog$2;

    invoke-direct {v3, p2}, Lcom/squareup/items/unit/ui/CountOfAffectedVariationsAlertDialogFactory$createDialog$2;-><init>(Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen;)V

    check-cast v3, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p1, v0, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 91
    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 92
    invoke-virtual {p1, v6}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 93
    invoke-virtual {p1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 94
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const-string p2, "ThemedAlertDialog.Builde\u2026(false)\n        .create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    iget-object v0, p0, Lcom/squareup/items/unit/ui/CountOfAffectedVariationsAlertDialogFactory;->screens:Lio/reactivex/Observable;

    const-wide/16 v1, 0x1

    .line 28
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    .line 29
    invoke-virtual {v0}, Lio/reactivex/Observable;->singleOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 30
    new-instance v1, Lcom/squareup/items/unit/ui/CountOfAffectedVariationsAlertDialogFactory$create$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/items/unit/ui/CountOfAffectedVariationsAlertDialogFactory$create$1;-><init>(Lcom/squareup/items/unit/ui/CountOfAffectedVariationsAlertDialogFactory;Landroid/content/Context;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "screens\n        .take(1)\u2026 screen.unwrapV2Screen) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
