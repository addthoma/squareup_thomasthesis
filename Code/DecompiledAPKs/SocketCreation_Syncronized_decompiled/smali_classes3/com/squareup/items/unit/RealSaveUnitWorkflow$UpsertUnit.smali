.class final Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit;
.super Lcom/squareup/workflow/rx2/PublisherWorker;
.source "SaveUnitWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/unit/RealSaveUnitWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "UpsertUnit"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/rx2/PublisherWorker<",
        "Lcom/squareup/shared/catalog/sync/SyncResult<",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0082\u0004\u0018\u00002\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u00020\u0001B\u0015\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0014\u0010\t\u001a\u00020\n2\n\u0010\u000b\u001a\u0006\u0012\u0002\u0008\u00030\u000cH\u0016JD\u0010\r\u001a>\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0003 \u000f*\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00020\u0002 \u000f*\u001e\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0003 \u000f*\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00020\u0002\u0018\u00010\u000e0\u000eH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit;",
        "Lcom/squareup/workflow/rx2/PublisherWorker;",
        "Lcom/squareup/shared/catalog/sync/SyncResult;",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
        "idempotencyKey",
        "",
        "unit",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
        "(Lcom/squareup/items/unit/RealSaveUnitWorkflow;Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V",
        "doesSameWorkAs",
        "",
        "otherWorker",
        "Lcom/squareup/workflow/Worker;",
        "runPublisher",
        "Lio/reactivex/Flowable;",
        "kotlin.jvm.PlatformType",
        "edit-unit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final idempotencyKey:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/items/unit/RealSaveUnitWorkflow;

.field private final unit:Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;


# direct methods
.method public constructor <init>(Lcom/squareup/items/unit/RealSaveUnitWorkflow;Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
            ")V"
        }
    .end annotation

    const-string v0, "idempotencyKey"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unit"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 193
    iput-object p1, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit;->this$0:Lcom/squareup/items/unit/RealSaveUnitWorkflow;

    .line 196
    invoke-direct {p0}, Lcom/squareup/workflow/rx2/PublisherWorker;-><init>()V

    iput-object p2, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit;->idempotencyKey:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit;->unit:Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    return-void
.end method

.method public static final synthetic access$getIdempotencyKey$p(Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit;)Ljava/lang/String;
    .locals 0

    .line 193
    iget-object p0, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit;->idempotencyKey:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic access$getUnit$p(Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;
    .locals 0

    .line 193
    iget-object p0, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit;->unit:Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    return-object p0
.end method


# virtual methods
.method public doesSameWorkAs(Lcom/squareup/workflow/Worker;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Worker<",
            "*>;)Z"
        }
    .end annotation

    const-string v0, "otherWorker"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 197
    instance-of p1, p1, Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit;

    return p1
.end method

.method public runPublisher()Lio/reactivex/Flowable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Flowable<",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;>;"
        }
    .end annotation

    .line 200
    new-instance v0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit$runPublisher$1;

    invoke-direct {v0, p0}, Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit$runPublisher$1;-><init>(Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit;)V

    check-cast v0, Lio/reactivex/SingleOnSubscribe;

    invoke-static {v0}, Lio/reactivex/Single;->create(Lio/reactivex/SingleOnSubscribe;)Lio/reactivex/Single;

    move-result-object v0

    .line 203
    iget-object v1, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit;->this$0:Lcom/squareup/items/unit/RealSaveUnitWorkflow;

    invoke-static {v1}, Lcom/squareup/items/unit/RealSaveUnitWorkflow;->access$getMainScheduler$p(Lcom/squareup/items/unit/RealSaveUnitWorkflow;)Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    .line 204
    invoke-virtual {v0}, Lio/reactivex/Single;->toFlowable()Lio/reactivex/Flowable;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic runPublisher()Lorg/reactivestreams/Publisher;
    .locals 1

    .line 193
    invoke-virtual {p0}, Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit;->runPublisher()Lio/reactivex/Flowable;

    move-result-object v0

    check-cast v0, Lorg/reactivestreams/Publisher;

    return-object v0
.end method
