.class final Lcom/squareup/items/unit/EditUnitState$toSnapshot$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EditUnitState.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/unit/EditUnitState;->toSnapshot()Lcom/squareup/workflow/Snapshot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lokio/BufferedSink;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditUnitState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditUnitState.kt\ncom/squareup/items/unit/EditUnitState$toSnapshot$1\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,235:1\n158#2,3:236\n161#2:241\n1591#3,2:239\n*E\n*S KotlinDebug\n*F\n+ 1 EditUnitState.kt\ncom/squareup/items/unit/EditUnitState$toSnapshot$1\n*L\n99#1,3:236\n99#1:241\n99#1,2:239\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "sink",
        "Lokio/BufferedSink;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/items/unit/EditUnitState;


# direct methods
.method constructor <init>(Lcom/squareup/items/unit/EditUnitState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/unit/EditUnitState$toSnapshot$1;->this$0:Lcom/squareup/items/unit/EditUnitState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 31
    check-cast p1, Lokio/BufferedSink;

    invoke-virtual {p0, p1}, Lcom/squareup/items/unit/EditUnitState$toSnapshot$1;->invoke(Lokio/BufferedSink;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lokio/BufferedSink;)V
    .locals 3

    const-string v0, "sink"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/squareup/items/unit/EditUnitState$toSnapshot$1;->this$0:Lcom/squareup/items/unit/EditUnitState;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "this::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 89
    iget-object v0, p0, Lcom/squareup/items/unit/EditUnitState$toSnapshot$1;->this$0:Lcom/squareup/items/unit/EditUnitState;

    .line 90
    instance-of v1, v0, Lcom/squareup/items/unit/EditUnitState$Saving;

    if-eqz v1, :cond_0

    .line 91
    check-cast v0, Lcom/squareup/items/unit/EditUnitState$Saving;

    invoke-virtual {v0}, Lcom/squareup/items/unit/EditUnitState$Saving;->getIdempotencyKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 92
    sget-object v0, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper;->Companion:Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;

    iget-object v1, p0, Lcom/squareup/items/unit/EditUnitState$toSnapshot$1;->this$0:Lcom/squareup/items/unit/EditUnitState;

    check-cast v1, Lcom/squareup/items/unit/EditUnitState$Saving;

    invoke-virtual {v1}, Lcom/squareup/items/unit/EditUnitState$Saving;->getUnitToSave()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;->writeCatalogMeasurementUnit(Lokio/BufferedSink;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    .line 93
    sget-object v0, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper;->Companion:Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;

    iget-object v1, p0, Lcom/squareup/items/unit/EditUnitState$toSnapshot$1;->this$0:Lcom/squareup/items/unit/EditUnitState;

    check-cast v1, Lcom/squareup/items/unit/EditUnitState$Saving;

    invoke-virtual {v1}, Lcom/squareup/items/unit/EditUnitState$Saving;->getCurrentUnitInEditing()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;->writeCatalogMeasurementUnit(Lokio/BufferedSink;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    .line 94
    sget-object v0, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper;->Companion:Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;

    iget-object v1, p0, Lcom/squareup/items/unit/EditUnitState$toSnapshot$1;->this$0:Lcom/squareup/items/unit/EditUnitState;

    check-cast v1, Lcom/squareup/items/unit/EditUnitState$Saving;

    invoke-virtual {v1}, Lcom/squareup/items/unit/EditUnitState$Saving;->getOriginalUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;->writeCatalogMeasurementUnit(Lokio/BufferedSink;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    .line 95
    iget-object v0, p0, Lcom/squareup/items/unit/EditUnitState$toSnapshot$1;->this$0:Lcom/squareup/items/unit/EditUnitState;

    check-cast v0, Lcom/squareup/items/unit/EditUnitState$Saving;

    invoke-virtual {v0}, Lcom/squareup/items/unit/EditUnitState$Saving;->getSaveUnitAction()Lcom/squareup/items/unit/SaveUnitAction;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeEnumByOrdinal(Lokio/BufferedSink;Ljava/lang/Enum;)Lokio/BufferedSink;

    .line 96
    iget-object v0, p0, Lcom/squareup/items/unit/EditUnitState$toSnapshot$1;->this$0:Lcom/squareup/items/unit/EditUnitState;

    check-cast v0, Lcom/squareup/items/unit/EditUnitState$Saving;

    invoke-virtual {v0}, Lcom/squareup/items/unit/EditUnitState$Saving;->isDefaultUnit()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    goto/16 :goto_1

    .line 98
    :cond_0
    instance-of v1, v0, Lcom/squareup/items/unit/EditUnitState$StandardUnitsList;

    if-eqz v1, :cond_1

    .line 99
    check-cast v0, Lcom/squareup/items/unit/EditUnitState$StandardUnitsList;

    invoke-virtual {v0}, Lcom/squareup/items/unit/EditUnitState$StandardUnitsList;->getStandardUnits()Ljava/util/List;

    move-result-object v0

    .line 237
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {p1, v1}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 238
    check-cast v0, Ljava/lang/Iterable;

    .line 239
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 238
    check-cast v1, Lcom/squareup/items/unit/LocalizedStandardUnit;

    .line 100
    invoke-virtual {v1}, Lcom/squareup/items/unit/LocalizedStandardUnit;->getMeasurementUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v2

    check-cast v2, Lcom/squareup/wire/Message;

    invoke-static {p1, v2}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    .line 101
    invoke-virtual {v1}, Lcom/squareup/items/unit/LocalizedStandardUnit;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 102
    invoke-virtual {v1}, Lcom/squareup/items/unit/LocalizedStandardUnit;->getAbbreviation()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    goto :goto_0

    .line 105
    :cond_1
    instance-of v1, v0, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    if-eqz v1, :cond_2

    .line 106
    sget-object v0, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper;->Companion:Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;

    iget-object v1, p0, Lcom/squareup/items/unit/EditUnitState$toSnapshot$1;->this$0:Lcom/squareup/items/unit/EditUnitState;

    check-cast v1, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    invoke-virtual {v1}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->getOriginalUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;->writeCatalogMeasurementUnit(Lokio/BufferedSink;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    .line 107
    sget-object v0, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper;->Companion:Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;

    iget-object v1, p0, Lcom/squareup/items/unit/EditUnitState$toSnapshot$1;->this$0:Lcom/squareup/items/unit/EditUnitState;

    check-cast v1, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    invoke-virtual {v1}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->getCurrentUnitInEditing()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v1

    const-string v2, "currentUnitInEditing.build()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1, v1}, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;->writeCatalogMeasurementUnit(Lokio/BufferedSink;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    .line 108
    iget-object v0, p0, Lcom/squareup/items/unit/EditUnitState$toSnapshot$1;->this$0:Lcom/squareup/items/unit/EditUnitState;

    check-cast v0, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    invoke-virtual {v0}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->isDefaultUnit()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    goto :goto_1

    .line 110
    :cond_2
    instance-of v1, v0, Lcom/squareup/items/unit/EditUnitState$EditsDiscardAttempted;

    if-eqz v1, :cond_3

    .line 111
    sget-object v0, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper;->Companion:Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;

    iget-object v1, p0, Lcom/squareup/items/unit/EditUnitState$toSnapshot$1;->this$0:Lcom/squareup/items/unit/EditUnitState;

    check-cast v1, Lcom/squareup/items/unit/EditUnitState$EditsDiscardAttempted;

    invoke-virtual {v1}, Lcom/squareup/items/unit/EditUnitState$EditsDiscardAttempted;->getOriginalUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;->writeCatalogMeasurementUnit(Lokio/BufferedSink;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    .line 112
    sget-object v0, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper;->Companion:Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;

    iget-object v1, p0, Lcom/squareup/items/unit/EditUnitState$toSnapshot$1;->this$0:Lcom/squareup/items/unit/EditUnitState;

    check-cast v1, Lcom/squareup/items/unit/EditUnitState$EditsDiscardAttempted;

    invoke-virtual {v1}, Lcom/squareup/items/unit/EditUnitState$EditsDiscardAttempted;->getUpdatedUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;->writeCatalogMeasurementUnit(Lokio/BufferedSink;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    .line 113
    iget-object v0, p0, Lcom/squareup/items/unit/EditUnitState$toSnapshot$1;->this$0:Lcom/squareup/items/unit/EditUnitState;

    check-cast v0, Lcom/squareup/items/unit/EditUnitState$EditsDiscardAttempted;

    invoke-virtual {v0}, Lcom/squareup/items/unit/EditUnitState$EditsDiscardAttempted;->isDefaultUnit()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    goto :goto_1

    .line 115
    :cond_3
    instance-of v1, v0, Lcom/squareup/items/unit/EditUnitState$DuplicateCustomUnitName;

    if-eqz v1, :cond_4

    .line 116
    sget-object v0, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper;->Companion:Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;

    iget-object v1, p0, Lcom/squareup/items/unit/EditUnitState$toSnapshot$1;->this$0:Lcom/squareup/items/unit/EditUnitState;

    check-cast v1, Lcom/squareup/items/unit/EditUnitState$DuplicateCustomUnitName;

    invoke-virtual {v1}, Lcom/squareup/items/unit/EditUnitState$DuplicateCustomUnitName;->getOriginalUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;->writeCatalogMeasurementUnit(Lokio/BufferedSink;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    .line 117
    sget-object v0, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper;->Companion:Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;

    iget-object v1, p0, Lcom/squareup/items/unit/EditUnitState$toSnapshot$1;->this$0:Lcom/squareup/items/unit/EditUnitState;

    check-cast v1, Lcom/squareup/items/unit/EditUnitState$DuplicateCustomUnitName;

    invoke-virtual {v1}, Lcom/squareup/items/unit/EditUnitState$DuplicateCustomUnitName;->getUpdatedUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;->writeCatalogMeasurementUnit(Lokio/BufferedSink;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    .line 118
    iget-object v0, p0, Lcom/squareup/items/unit/EditUnitState$toSnapshot$1;->this$0:Lcom/squareup/items/unit/EditUnitState;

    check-cast v0, Lcom/squareup/items/unit/EditUnitState$DuplicateCustomUnitName;

    invoke-virtual {v0}, Lcom/squareup/items/unit/EditUnitState$DuplicateCustomUnitName;->isDefaultUnit()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    goto :goto_1

    .line 120
    :cond_4
    instance-of v1, v0, Lcom/squareup/items/unit/EditUnitState$WarnInternetUnavailable;

    if-eqz v1, :cond_5

    check-cast v0, Lcom/squareup/items/unit/EditUnitState$WarnInternetUnavailable;

    invoke-virtual {v0}, Lcom/squareup/items/unit/EditUnitState$WarnInternetUnavailable;->getBlockedState()Lcom/squareup/items/unit/EditUnitState$RequiresInternet;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/items/unit/EditUnitStateKt;->access$writeRequiresInternet(Lokio/BufferedSink;Lcom/squareup/items/unit/EditUnitState$RequiresInternet;)V

    goto :goto_1

    .line 121
    :cond_5
    instance-of v1, v0, Lcom/squareup/items/unit/EditUnitState$CheckInternetAvailability;

    if-eqz v1, :cond_6

    check-cast v0, Lcom/squareup/items/unit/EditUnitState$CheckInternetAvailability;

    invoke-virtual {v0}, Lcom/squareup/items/unit/EditUnitState$CheckInternetAvailability;->getPendingState()Lcom/squareup/items/unit/EditUnitState$RequiresInternet;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/items/unit/EditUnitStateKt;->access$writeRequiresInternet(Lokio/BufferedSink;Lcom/squareup/items/unit/EditUnitState$RequiresInternet;)V

    :cond_6
    :goto_1
    return-void
.end method
