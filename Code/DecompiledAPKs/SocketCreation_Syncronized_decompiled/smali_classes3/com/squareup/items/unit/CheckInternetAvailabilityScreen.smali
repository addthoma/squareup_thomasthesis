.class public final Lcom/squareup/items/unit/CheckInternetAvailabilityScreen;
.super Ljava/lang/Object;
.source "CheckInternetAvailabilityScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/unit/CheckInternetAvailabilityScreen$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"
    }
    d2 = {
        "Lcom/squareup/items/unit/CheckInternetAvailabilityScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "()V",
        "Companion",
        "edit-unit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/items/unit/CheckInternetAvailabilityScreen$Companion;

.field private static final KEY:Lcom/squareup/workflow/legacy/Screen$Key;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/items/unit/CheckInternetAvailabilityScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/items/unit/CheckInternetAvailabilityScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/items/unit/CheckInternetAvailabilityScreen;->Companion:Lcom/squareup/items/unit/CheckInternetAvailabilityScreen$Companion;

    .line 12
    const-class v0, Lcom/squareup/items/unit/CheckInternetAvailabilityScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    sput-object v0, Lcom/squareup/items/unit/CheckInternetAvailabilityScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic access$getKEY$cp()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/items/unit/CheckInternetAvailabilityScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method
