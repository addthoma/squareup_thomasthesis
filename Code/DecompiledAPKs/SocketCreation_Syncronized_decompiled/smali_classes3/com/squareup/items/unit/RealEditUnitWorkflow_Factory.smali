.class public final Lcom/squareup/items/unit/RealEditUnitWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealEditUnitWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/items/unit/RealEditUnitWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final allStandardUnitsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/unit/AllPredefinedStandardUnits;",
            ">;"
        }
    .end annotation
.end field

.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final eventLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/unit/EditUnitEventLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final saveUnitWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/unit/SaveUnitWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/unit/SaveUnitWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/unit/AllPredefinedStandardUnits;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/unit/EditUnitEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow_Factory;->resProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow_Factory;->saveUnitWorkflowProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow_Factory;->allStandardUnitsProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow_Factory;->eventLoggerProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p6, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/items/unit/RealEditUnitWorkflow_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/unit/SaveUnitWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/unit/AllPredefinedStandardUnits;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/unit/EditUnitEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/items/unit/RealEditUnitWorkflow_Factory;"
        }
    .end annotation

    .line 54
    new-instance v7, Lcom/squareup/items/unit/RealEditUnitWorkflow_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/items/unit/RealEditUnitWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/items/unit/SaveUnitWorkflow;Lcom/squareup/items/unit/AllPredefinedStandardUnits;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/items/unit/EditUnitEventLogger;Lcom/squareup/settings/server/Features;)Lcom/squareup/items/unit/RealEditUnitWorkflow;
    .locals 8

    .line 60
    new-instance v7, Lcom/squareup/items/unit/RealEditUnitWorkflow;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/items/unit/RealEditUnitWorkflow;-><init>(Lcom/squareup/util/Res;Lcom/squareup/items/unit/SaveUnitWorkflow;Lcom/squareup/items/unit/AllPredefinedStandardUnits;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/items/unit/EditUnitEventLogger;Lcom/squareup/settings/server/Features;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/items/unit/RealEditUnitWorkflow;
    .locals 7

    .line 46
    iget-object v0, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow_Factory;->saveUnitWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/items/unit/SaveUnitWorkflow;

    iget-object v0, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow_Factory;->allStandardUnitsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/items/unit/AllPredefinedStandardUnits;

    iget-object v0, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v0, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow_Factory;->eventLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/items/unit/EditUnitEventLogger;

    iget-object v0, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/settings/server/Features;

    invoke-static/range {v1 .. v6}, Lcom/squareup/items/unit/RealEditUnitWorkflow_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/items/unit/SaveUnitWorkflow;Lcom/squareup/items/unit/AllPredefinedStandardUnits;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/items/unit/EditUnitEventLogger;Lcom/squareup/settings/server/Features;)Lcom/squareup/items/unit/RealEditUnitWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/items/unit/RealEditUnitWorkflow_Factory;->get()Lcom/squareup/items/unit/RealEditUnitWorkflow;

    move-result-object v0

    return-object v0
.end method
