.class public final Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen;
.super Ljava/lang/Object;
.source "CountOfAffectedVariationsAlertScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen$Event;,
        Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0008\u0018\u0000 \r2\u00020\u0001:\u0002\r\u000eB!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005\u00a2\u0006\u0002\u0010\u0008R\u001d\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "state",
        "Lcom/squareup/items/unit/SaveUnitState$DisplayCountOfAffectedVariations;",
        "onEvent",
        "Lkotlin/Function1;",
        "Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen$Event;",
        "",
        "(Lcom/squareup/items/unit/SaveUnitState$DisplayCountOfAffectedVariations;Lkotlin/jvm/functions/Function1;)V",
        "getOnEvent",
        "()Lkotlin/jvm/functions/Function1;",
        "getState",
        "()Lcom/squareup/items/unit/SaveUnitState$DisplayCountOfAffectedVariations;",
        "Companion",
        "Event",
        "edit-unit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen$Companion;

.field private static final KEY:Lcom/squareup/workflow/legacy/Screen$Key;


# instance fields
.field private final onEvent:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen$Event;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final state:Lcom/squareup/items/unit/SaveUnitState$DisplayCountOfAffectedVariations;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen;->Companion:Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen$Companion;

    .line 17
    const-class v0, Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    sput-object v0, Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/items/unit/SaveUnitState$DisplayCountOfAffectedVariations;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/unit/SaveUnitState$DisplayCountOfAffectedVariations;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen$Event;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen;->state:Lcom/squareup/items/unit/SaveUnitState$DisplayCountOfAffectedVariations;

    iput-object p2, p0, Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static final synthetic access$getKEY$cp()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    .line 7
    sget-object v0, Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method


# virtual methods
.method public final getOnEvent()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen$Event;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 9
    iget-object v0, p0, Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getState()Lcom/squareup/items/unit/SaveUnitState$DisplayCountOfAffectedVariations;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen;->state:Lcom/squareup/items/unit/SaveUnitState$DisplayCountOfAffectedVariations;

    return-object v0
.end method
