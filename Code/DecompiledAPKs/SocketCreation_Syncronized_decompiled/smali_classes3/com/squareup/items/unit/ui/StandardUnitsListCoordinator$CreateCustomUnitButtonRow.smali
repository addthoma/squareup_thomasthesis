.class public final Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;
.super Ljava/lang/Object;
.source "StandardUnitsListCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CreateCustomUnitButtonRow"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0011\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0007H\u00c6\u0003J1\u0010\u0015\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0016\u001a\u00020\u00072\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001J\t\u0010\u001a\u001a\u00020\u0005H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0008\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000f\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;",
        "",
        "screen",
        "Lcom/squareup/items/unit/StandardUnitsListScreen;",
        "prepopulatedCustomUnitName",
        "",
        "shouldShowNoResultsHelpText",
        "",
        "shouldShowCreateCustomUnitButton",
        "(Lcom/squareup/items/unit/StandardUnitsListScreen;Ljava/lang/String;ZZ)V",
        "getPrepopulatedCustomUnitName",
        "()Ljava/lang/String;",
        "getScreen",
        "()Lcom/squareup/items/unit/StandardUnitsListScreen;",
        "getShouldShowCreateCustomUnitButton",
        "()Z",
        "getShouldShowNoResultsHelpText",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "edit-unit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final prepopulatedCustomUnitName:Ljava/lang/String;

.field private final screen:Lcom/squareup/items/unit/StandardUnitsListScreen;

.field private final shouldShowCreateCustomUnitButton:Z

.field private final shouldShowNoResultsHelpText:Z


# direct methods
.method public constructor <init>(Lcom/squareup/items/unit/StandardUnitsListScreen;Ljava/lang/String;ZZ)V
    .locals 1

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "prepopulatedCustomUnitName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 249
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->screen:Lcom/squareup/items/unit/StandardUnitsListScreen;

    iput-object p2, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->prepopulatedCustomUnitName:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->shouldShowNoResultsHelpText:Z

    iput-boolean p4, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->shouldShowCreateCustomUnitButton:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;Lcom/squareup/items/unit/StandardUnitsListScreen;Ljava/lang/String;ZZILjava/lang/Object;)Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->screen:Lcom/squareup/items/unit/StandardUnitsListScreen;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->prepopulatedCustomUnitName:Ljava/lang/String;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-boolean p3, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->shouldShowNoResultsHelpText:Z

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-boolean p4, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->shouldShowCreateCustomUnitButton:Z

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->copy(Lcom/squareup/items/unit/StandardUnitsListScreen;Ljava/lang/String;ZZ)Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/items/unit/StandardUnitsListScreen;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->screen:Lcom/squareup/items/unit/StandardUnitsListScreen;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->prepopulatedCustomUnitName:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->shouldShowNoResultsHelpText:Z

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->shouldShowCreateCustomUnitButton:Z

    return v0
.end method

.method public final copy(Lcom/squareup/items/unit/StandardUnitsListScreen;Ljava/lang/String;ZZ)Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;
    .locals 1

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "prepopulatedCustomUnitName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;-><init>(Lcom/squareup/items/unit/StandardUnitsListScreen;Ljava/lang/String;ZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;

    iget-object v0, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->screen:Lcom/squareup/items/unit/StandardUnitsListScreen;

    iget-object v1, p1, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->screen:Lcom/squareup/items/unit/StandardUnitsListScreen;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->prepopulatedCustomUnitName:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->prepopulatedCustomUnitName:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->shouldShowNoResultsHelpText:Z

    iget-boolean v1, p1, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->shouldShowNoResultsHelpText:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->shouldShowCreateCustomUnitButton:Z

    iget-boolean p1, p1, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->shouldShowCreateCustomUnitButton:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getPrepopulatedCustomUnitName()Ljava/lang/String;
    .locals 1

    .line 251
    iget-object v0, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->prepopulatedCustomUnitName:Ljava/lang/String;

    return-object v0
.end method

.method public final getScreen()Lcom/squareup/items/unit/StandardUnitsListScreen;
    .locals 1

    .line 250
    iget-object v0, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->screen:Lcom/squareup/items/unit/StandardUnitsListScreen;

    return-object v0
.end method

.method public final getShouldShowCreateCustomUnitButton()Z
    .locals 1

    .line 253
    iget-boolean v0, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->shouldShowCreateCustomUnitButton:Z

    return v0
.end method

.method public final getShouldShowNoResultsHelpText()Z
    .locals 1

    .line 252
    iget-boolean v0, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->shouldShowNoResultsHelpText:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->screen:Lcom/squareup/items/unit/StandardUnitsListScreen;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->prepopulatedCustomUnitName:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->shouldShowNoResultsHelpText:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->shouldShowCreateCustomUnitButton:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CreateCustomUnitButtonRow(screen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->screen:Lcom/squareup/items/unit/StandardUnitsListScreen;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", prepopulatedCustomUnitName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->prepopulatedCustomUnitName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", shouldShowNoResultsHelpText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->shouldShowNoResultsHelpText:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", shouldShowCreateCustomUnitButton="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;->shouldShowCreateCustomUnitButton:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
