.class final Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SaveUnitWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/unit/RealSaveUnitWorkflow;->render(Lcom/squareup/items/unit/SaveUnitInput;Lcom/squareup/items/unit/SaveUnitState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/shared/catalog/sync/SyncResult<",
        "+",
        "Ljava/lang/Object;",
        ">;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/items/unit/SaveUnitState;",
        "+",
        "Lcom/squareup/items/unit/SaveUnitResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u000e\u0010\u0004\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00060\u0005H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/items/unit/SaveUnitState;",
        "Lcom/squareup/items/unit/SaveUnitResult;",
        "result",
        "Lcom/squareup/shared/catalog/sync/SyncResult;",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/items/unit/SaveUnitState;

.field final synthetic this$0:Lcom/squareup/items/unit/RealSaveUnitWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/items/unit/RealSaveUnitWorkflow;Lcom/squareup/items/unit/SaveUnitState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$1;->this$0:Lcom/squareup/items/unit/RealSaveUnitWorkflow;

    iput-object p2, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$1;->$state:Lcom/squareup/items/unit/SaveUnitState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/shared/catalog/sync/SyncResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "+",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/items/unit/SaveUnitState;",
            "Lcom/squareup/items/unit/SaveUnitResult;",
            ">;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    iget-object v0, p1, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    const/4 v1, 0x2

    if-nez v0, :cond_3

    .line 119
    iget-object v0, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$1;->$state:Lcom/squareup/items/unit/SaveUnitState;

    check-cast v0, Lcom/squareup/items/unit/SaveUnitState$Saving;

    invoke-virtual {v0}, Lcom/squareup/items/unit/SaveUnitState$Saving;->getSaveUnitAction()Lcom/squareup/items/unit/SaveUnitAction;

    move-result-object v0

    sget-object v2, Lcom/squareup/items/unit/RealSaveUnitWorkflow$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {v0}, Lcom/squareup/items/unit/SaveUnitAction;->ordinal()I

    move-result v0

    aget v0, v2, v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_0

    .line 121
    iget-object p1, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$1;->$state:Lcom/squareup/items/unit/SaveUnitState;

    check-cast p1, Lcom/squareup/items/unit/SaveUnitState$Saving;

    invoke-virtual {p1}, Lcom/squareup/items/unit/SaveUnitState$Saving;->getUnitToSave()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object p1

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 120
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/SyncResult;->get()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_2

    check-cast p1, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    .line 124
    :goto_0
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v1, Lcom/squareup/items/unit/SaveUnitResult$UnitSaved;

    invoke-direct {v1, p1}, Lcom/squareup/items/unit/SaveUnitResult$UnitSaved;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_1

    .line 120
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.shared.catalog.connectv2.models.CatalogMeasurementUnit"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 126
    :cond_3
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 127
    new-instance v2, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$SavingFailed;

    .line 128
    iget-object v3, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$1;->$state:Lcom/squareup/items/unit/SaveUnitState;

    check-cast v3, Lcom/squareup/items/unit/SaveUnitState$Saving;

    .line 129
    iget-object v4, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$1;->this$0:Lcom/squareup/items/unit/RealSaveUnitWorkflow;

    .line 130
    iget-object p1, p1, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    const-string v5, "result.error"

    invoke-static {p1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$1;->$state:Lcom/squareup/items/unit/SaveUnitState;

    check-cast v5, Lcom/squareup/items/unit/SaveUnitState$Saving;

    .line 129
    invoke-static {v4, p1, v5}, Lcom/squareup/items/unit/RealSaveUnitWorkflow;->access$getUnitSaveFailedAlertScreenConfiguration(Lcom/squareup/items/unit/RealSaveUnitWorkflow;Lcom/squareup/shared/catalog/sync/SyncError;Lcom/squareup/items/unit/SaveUnitState$Saving;)Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;

    move-result-object p1

    .line 127
    invoke-direct {v2, v3, p1}, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$SavingFailed;-><init>(Lcom/squareup/items/unit/SaveUnitState$Saving;Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;)V

    const/4 p1, 0x0

    .line 126
    invoke-static {v0, v2, p1, v1, p1}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_1
    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 74
    check-cast p1, Lcom/squareup/shared/catalog/sync/SyncResult;

    invoke-virtual {p0, p1}, Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$1;->invoke(Lcom/squareup/shared/catalog/sync/SyncResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
