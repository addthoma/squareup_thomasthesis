.class public abstract Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;
.super Lcom/squareup/items/unit/SaveUnitState;
.source "SaveUnitState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/unit/SaveUnitState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "UnitSaveFailed"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$SavingFailed;,
        Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\u000f\u0010B#\u0008\u0002\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008R\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u0082\u0001\u0002\u0011\u0012\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;",
        "Lcom/squareup/items/unit/SaveUnitState;",
        "previousSaving",
        "Lcom/squareup/items/unit/SaveUnitState$Saving;",
        "previousFetchingCount",
        "Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;",
        "configuration",
        "Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;",
        "(Lcom/squareup/items/unit/SaveUnitState$Saving;Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;)V",
        "getConfiguration",
        "()Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;",
        "getPreviousFetchingCount",
        "()Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;",
        "getPreviousSaving",
        "()Lcom/squareup/items/unit/SaveUnitState$Saving;",
        "FetchingCountFailed",
        "SavingFailed",
        "Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$SavingFailed;",
        "Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;",
        "edit-unit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final configuration:Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;

.field private final previousFetchingCount:Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;

.field private final previousSaving:Lcom/squareup/items/unit/SaveUnitState$Saving;


# direct methods
.method private constructor <init>(Lcom/squareup/items/unit/SaveUnitState$Saving;Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;)V
    .locals 1

    const/4 v0, 0x0

    .line 44
    invoke-direct {p0, v0}, Lcom/squareup/items/unit/SaveUnitState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;->previousSaving:Lcom/squareup/items/unit/SaveUnitState$Saving;

    iput-object p2, p0, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;->previousFetchingCount:Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;

    iput-object p3, p0, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;->configuration:Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/items/unit/SaveUnitState$Saving;Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 40
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;-><init>(Lcom/squareup/items/unit/SaveUnitState$Saving;Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;)V

    return-void
.end method


# virtual methods
.method public getConfiguration()Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;->configuration:Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;

    return-object v0
.end method

.method public getPreviousFetchingCount()Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;->previousFetchingCount:Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;

    return-object v0
.end method

.method public getPreviousSaving()Lcom/squareup/items/unit/SaveUnitState$Saving;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;->previousSaving:Lcom/squareup/items/unit/SaveUnitState$Saving;

    return-object v0
.end method
