.class public final Lcom/squareup/items/unit/SelectableUnitKt;
.super Ljava/lang/Object;
.source "SelectableUnit.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectableUnit.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectableUnit.kt\ncom/squareup/items/unit/SelectableUnitKt\n*L\n1#1,89:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "toSelectableUnit",
        "Lcom/squareup/items/unit/SelectableUnit;",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
        "res",
        "Lcom/squareup/util/Res;",
        "edit-unit_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final toSelectableUnit(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/util/Res;)Lcom/squareup/items/unit/SelectableUnit;
    .locals 7

    const-string v0, "$this$toSelectableUnit"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    new-instance v0, Lcom/squareup/items/unit/SelectableUnit;

    .line 82
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getId()Ljava/lang/String;

    move-result-object v2

    const-string v1, "it.id"

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getMeasurementUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const-string v3, "it.measurementUnit"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, p1}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedName(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v3

    .line 84
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getPrecision()I

    move-result v4

    .line 85
    invoke-static {p0, p1}, Lcom/squareup/quantity/UnitDisplayDataKt;->getUnitDisplayData(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/util/Res;)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/squareup/quantity/UnitDisplayData;->getQuantityPrecisionHint(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v5

    move-object v1, v0

    move-object v6, p0

    .line 81
    invoke-direct/range {v1 .. v6}, Lcom/squareup/items/unit/SelectableUnit;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    return-object v0
.end method
