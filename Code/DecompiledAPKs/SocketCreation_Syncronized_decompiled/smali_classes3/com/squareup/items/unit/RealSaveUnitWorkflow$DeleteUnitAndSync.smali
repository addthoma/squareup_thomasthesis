.class final Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync;
.super Lcom/squareup/workflow/rx2/PublisherWorker;
.source "SaveUnitWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/unit/RealSaveUnitWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "DeleteUnitAndSync"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/rx2/PublisherWorker<",
        "Lcom/squareup/shared/catalog/sync/SyncResult<",
        "Ljava/lang/Void;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0082\u0004\u0018\u00002\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u00020\u0001B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0014\u0010\u0007\u001a\u00020\u00082\n\u0010\t\u001a\u0006\u0012\u0002\u0008\u00030\nH\u0016J\u0016\u0010\u000b\u001a\u0010\u0012\u000c\u0008\u0001\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u00020\u000cH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync;",
        "Lcom/squareup/workflow/rx2/PublisherWorker;",
        "Lcom/squareup/shared/catalog/sync/SyncResult;",
        "Ljava/lang/Void;",
        "unitId",
        "",
        "(Lcom/squareup/items/unit/RealSaveUnitWorkflow;Ljava/lang/String;)V",
        "doesSameWorkAs",
        "",
        "otherWorker",
        "Lcom/squareup/workflow/Worker;",
        "runPublisher",
        "Lorg/reactivestreams/Publisher;",
        "edit-unit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/items/unit/RealSaveUnitWorkflow;

.field private final unitId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/items/unit/RealSaveUnitWorkflow;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "unitId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 207
    iput-object p1, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync;->this$0:Lcom/squareup/items/unit/RealSaveUnitWorkflow;

    .line 209
    invoke-direct {p0}, Lcom/squareup/workflow/rx2/PublisherWorker;-><init>()V

    iput-object p2, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync;->unitId:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic access$getUnitId$p(Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync;)Ljava/lang/String;
    .locals 0

    .line 207
    iget-object p0, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync;->unitId:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public doesSameWorkAs(Lcom/squareup/workflow/Worker;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Worker<",
            "*>;)Z"
        }
    .end annotation

    const-string v0, "otherWorker"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 210
    instance-of p1, p1, Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync;

    return p1
.end method

.method public runPublisher()Lorg/reactivestreams/Publisher;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/reactivestreams/Publisher<",
            "+",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Void;",
            ">;>;"
        }
    .end annotation

    .line 213
    iget-object v0, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync;->this$0:Lcom/squareup/items/unit/RealSaveUnitWorkflow;

    invoke-static {v0}, Lcom/squareup/items/unit/RealSaveUnitWorkflow;->access$getCogs$p(Lcom/squareup/items/unit/RealSaveUnitWorkflow;)Lcom/squareup/cogs/Cogs;

    move-result-object v0

    new-instance v1, Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync$runPublisher$1;

    invoke-direct {v1, p0}, Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync$runPublisher$1;-><init>(Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync;)V

    check-cast v1, Lcom/squareup/shared/catalog/CatalogOnlineTask;

    invoke-interface {v0, v1}, Lcom/squareup/cogs/Cogs;->asSingleOnline(Lcom/squareup/shared/catalog/CatalogOnlineTask;)Lio/reactivex/Single;

    move-result-object v0

    .line 214
    new-instance v1, Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync$runPublisher$2;

    invoke-direct {v1, p0}, Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync$runPublisher$2;-><init>(Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 223
    iget-object v1, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync;->this$0:Lcom/squareup/items/unit/RealSaveUnitWorkflow;

    invoke-static {v1}, Lcom/squareup/items/unit/RealSaveUnitWorkflow;->access$getMainScheduler$p(Lcom/squareup/items/unit/RealSaveUnitWorkflow;)Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    .line 224
    invoke-virtual {v0}, Lio/reactivex/Single;->toFlowable()Lio/reactivex/Flowable;

    move-result-object v0

    const-string v1, "cogs.asSingleOnline { on\u2026)\n          .toFlowable()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lorg/reactivestreams/Publisher;

    return-object v0
.end method
