.class public final Lcom/squareup/items/tutorial/CreateItemTutorial_Factory;
.super Ljava/lang/Object;
.source "CreateItemTutorial_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/items/tutorial/CreateItemTutorial;",
        ">;"
    }
.end annotation


# instance fields
.field private final appletsDrawerRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletsDrawerRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final eventLockerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialEventLocker;",
            ">;"
        }
    .end annotation
.end field

.field private final lockScreenMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/ui/LockScreenMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final phoneTutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final squareDeviceTourSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/SquareDeviceTourSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final tabletTutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletsDrawerRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/ui/LockScreenMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/SquareDeviceTourSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialEventLocker;",
            ">;)V"
        }
    .end annotation

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p2, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Factory;->appletsDrawerRunnerProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p3, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Factory;->tabletTutorialProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p4, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Factory;->phoneTutorialProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p5, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Factory;->lockScreenMonitorProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p6, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Factory;->squareDeviceTourSettingsProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p7, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Factory;->eventLockerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/items/tutorial/CreateItemTutorial_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletsDrawerRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/ui/LockScreenMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/SquareDeviceTourSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialEventLocker;",
            ">;)",
            "Lcom/squareup/items/tutorial/CreateItemTutorial_Factory;"
        }
    .end annotation

    .line 63
    new-instance v8, Lcom/squareup/items/tutorial/CreateItemTutorial_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/items/tutorial/CreateItemTutorial_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/util/Device;Lcom/squareup/applet/AppletsDrawerRunner;Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;Lcom/squareup/permissions/ui/LockScreenMonitor;Lcom/squareup/SquareDeviceTourSettings;Lcom/squareup/register/tutorial/TutorialEventLocker;)Lcom/squareup/items/tutorial/CreateItemTutorial;
    .locals 9

    .line 70
    new-instance v8, Lcom/squareup/items/tutorial/CreateItemTutorial;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/items/tutorial/CreateItemTutorial;-><init>(Lcom/squareup/util/Device;Lcom/squareup/applet/AppletsDrawerRunner;Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;Lcom/squareup/permissions/ui/LockScreenMonitor;Lcom/squareup/SquareDeviceTourSettings;Lcom/squareup/register/tutorial/TutorialEventLocker;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/items/tutorial/CreateItemTutorial;
    .locals 8

    .line 53
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Factory;->appletsDrawerRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/applet/AppletsDrawerRunner;

    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Factory;->tabletTutorialProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;

    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Factory;->phoneTutorialProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;

    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Factory;->lockScreenMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/permissions/ui/LockScreenMonitor;

    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Factory;->squareDeviceTourSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/SquareDeviceTourSettings;

    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorial_Factory;->eventLockerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/register/tutorial/TutorialEventLocker;

    invoke-static/range {v1 .. v7}, Lcom/squareup/items/tutorial/CreateItemTutorial_Factory;->newInstance(Lcom/squareup/util/Device;Lcom/squareup/applet/AppletsDrawerRunner;Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;Lcom/squareup/permissions/ui/LockScreenMonitor;Lcom/squareup/SquareDeviceTourSettings;Lcom/squareup/register/tutorial/TutorialEventLocker;)Lcom/squareup/items/tutorial/CreateItemTutorial;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorial_Factory;->get()Lcom/squareup/items/tutorial/CreateItemTutorial;

    move-result-object v0

    return-object v0
.end method
