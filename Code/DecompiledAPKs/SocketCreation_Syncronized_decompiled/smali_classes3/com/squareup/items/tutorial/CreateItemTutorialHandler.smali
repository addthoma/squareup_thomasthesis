.class public abstract Lcom/squareup/items/tutorial/CreateItemTutorialHandler;
.super Ljava/lang/Object;
.source "CreateItemTutorialHandler.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;,
        Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;,
        Lcom/squareup/items/tutorial/CreateItemTutorialHandler$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u000c\u0008&\u0018\u0000 J2\u00020\u0001:\u0003JKLB5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u001e\u0010.\u001a\u0004\u0018\u00010/2\u0008\u00100\u001a\u0004\u0018\u0001012\u0008\u00102\u001a\u0004\u0018\u00010\u0001H\u0004J\u001e\u00103\u001a\u0004\u0018\u00010/2\u0008\u00100\u001a\u0004\u0018\u0001012\u0008\u00102\u001a\u0004\u0018\u00010\u0001H\u0004J\u001e\u00104\u001a\u0004\u0018\u00010/2\u0008\u00105\u001a\u0004\u0018\u0001012\u0008\u00106\u001a\u0004\u0018\u00010\u0001H&J\u001e\u00107\u001a\u0004\u0018\u00010/2\u0008\u00100\u001a\u0004\u0018\u0001012\u0008\u00102\u001a\u0004\u0018\u00010\u0001H\u0004J0\u00108\u001a\u0004\u0018\u00010/2\u0008\u00100\u001a\u0004\u0018\u0001012\u0008\u00102\u001a\u0004\u0018\u00010\u00012\u0006\u00109\u001a\u00020%2\u0008\u0010:\u001a\u0004\u0018\u000101H\u0004J0\u0010;\u001a\u0004\u0018\u00010/2\u0008\u00100\u001a\u0004\u0018\u0001012\u0008\u00102\u001a\u0004\u0018\u00010\u00012\u0006\u00109\u001a\u00020%2\u0008\u0010:\u001a\u0004\u0018\u000101H\u0004J@\u0010<\u001a\u0004\u0018\u00010/2\u0008\u00100\u001a\u0004\u0018\u0001012\u0008\u00102\u001a\u0004\u0018\u00010\u00012\u0006\u00109\u001a\u00020%2\u0008\u0010:\u001a\u0004\u0018\u0001012\u000e\u0010=\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010/0>H\u0004J\u0014\u0010?\u001a\u0004\u0018\u00010/2\u0008\u00100\u001a\u0004\u0018\u000101H\u0004J\u000e\u0010@\u001a\u00020A2\u0006\u0010\u001e\u001a\u00020\u001fJ\u0008\u0010B\u001a\u00020AH\u0004J\u0008\u0010C\u001a\u00020AH\u0004J\u0008\u0010D\u001a\u00020AH\u0004J\"\u0010E\u001a\u00020A2\u0006\u0010F\u001a\u00020%2\u0006\u00100\u001a\u0002012\u0008\u00102\u001a\u0004\u0018\u00010\u0001H\u0004J\"\u0010G\u001a\u00020A2\u0006\u0010F\u001a\u00020%2\u0006\u00100\u001a\u0002012\u0008\u00102\u001a\u0004\u0018\u00010\u0001H\u0002J\u0006\u0010H\u001a\u00020AJ\n\u0010I\u001a\u0004\u0018\u00010/H\u0004R\u0014\u0010\n\u001a\u00020\u000bX\u0084\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0014\u0010\u0002\u001a\u00020\u0003X\u0084\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0014\u0010\u0006\u001a\u00020\u0007X\u0084\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0015\u001a\u00020\u0016X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018\"\u0004\u0008\u0019\u0010\u001aR\u001a\u0010\u001b\u001a\u00020\u0016X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001c\u0010\u0018\"\u0004\u0008\u001d\u0010\u001aR\u001a\u0010\u001e\u001a\u00020\u001fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008 \u0010!\"\u0004\u0008\"\u0010#R\u001a\u0010$\u001a\u00020%X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008&\u0010\'\"\u0004\u0008(\u0010)R\u0014\u0010\u0004\u001a\u00020\u0005X\u0084\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008*\u0010+R\u0014\u0010\u0008\u001a\u00020\tX\u0084\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008,\u0010-\u00a8\u0006M"
    }
    d2 = {
        "Lcom/squareup/items/tutorial/CreateItemTutorialHandler;",
        "",
        "dialogFactory",
        "Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;",
        "tooltipFactory",
        "Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;",
        "flow",
        "Lflow/Flow;",
        "tutorialApi",
        "Lcom/squareup/register/tutorial/TutorialApi;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "preferences",
        "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
        "(Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;Lflow/Flow;Lcom/squareup/register/tutorial/TutorialApi;Lcom/squareup/analytics/Analytics;Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)V",
        "getAnalytics",
        "()Lcom/squareup/analytics/Analytics;",
        "getDialogFactory",
        "()Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;",
        "getFlow",
        "()Lflow/Flow;",
        "previousItemDialogState",
        "Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;",
        "getPreviousItemDialogState",
        "()Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;",
        "setPreviousItemDialogState",
        "(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;)V",
        "previousState",
        "getPreviousState",
        "setPreviousState",
        "startedFromSupportApplet",
        "",
        "getStartedFromSupportApplet",
        "()Z",
        "setStartedFromSupportApplet",
        "(Z)V",
        "state",
        "Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;",
        "getState",
        "()Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;",
        "setState",
        "(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V",
        "getTooltipFactory",
        "()Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;",
        "getTutorialApi",
        "()Lcom/squareup/register/tutorial/TutorialApi;",
        "handleAdjustInventory",
        "Lcom/squareup/tutorialv2/TutorialState;",
        "eventName",
        "",
        "eventValue",
        "handleEditCategory",
        "handleEvent",
        "name",
        "value",
        "handleItemDialogSadPath",
        "handleItemNameEvent",
        "dismissState",
        "dismissEvent",
        "handleItemPriceEvent",
        "handleItemSaveEvent",
        "savePressed",
        "Lkotlin/Function0;",
        "handleSkip",
        "init",
        "",
        "logModalShowEvent",
        "logStartTutorialEvent",
        "markAsCompleted",
        "saveAndAdvance",
        "nextState",
        "saveAndAdvanceItemDialog",
        "showSkipModal",
        "skipTutorial",
        "Companion",
        "State",
        "TutorialData",
        "items-tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$Companion;

.field public static final SHOW_ENTER_PRICE:Ljava/lang/String; = "Enter Price"

.field public static final SHOW_SAVE_ITEM:Ljava/lang/String; = "Save Item"

.field public static final START_TUTORIAL:Ljava/lang/String; = "Start Tutorial"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final dialogFactory:Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;

.field private final flow:Lflow/Flow;

.field private final preferences:Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

.field private previousItemDialogState:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;

.field private previousState:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;

.field private startedFromSupportApplet:Z

.field private state:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

.field private final tooltipFactory:Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;

.field private final tutorialApi:Lcom/squareup/register/tutorial/TutorialApi;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->Companion:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;Lflow/Flow;Lcom/squareup/register/tutorial/TutorialApi;Lcom/squareup/analytics/Analytics;Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)V
    .locals 7

    const-string v6, "dialogFactory"

    invoke-static {p1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v6, "tooltipFactory"

    invoke-static {p2, v6}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "flow"

    invoke-static {p3, v6}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v6, "tutorialApi"

    invoke-static {p4, v6}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "analytics"

    invoke-static {p5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "preferences"

    invoke-static {p6, v6}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->dialogFactory:Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;

    iput-object p2, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->tooltipFactory:Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;

    iput-object p3, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->flow:Lflow/Flow;

    iput-object p4, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->tutorialApi:Lcom/squareup/register/tutorial/TutorialApi;

    iput-object p5, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p6, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->preferences:Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    .line 109
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->WELCOME:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    iput-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->state:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    .line 110
    new-instance v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object p1, v0

    move-object p2, v1

    move-object p3, v2

    move-object p4, v3

    move p5, v4

    move-object p6, v5

    invoke-direct/range {p1 .. p6}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;-><init>(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->previousState:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;

    .line 111
    new-instance v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;

    move-object p1, v0

    invoke-direct/range {p1 .. p6}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;-><init>(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->previousItemDialogState:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;

    return-void
.end method

.method private final saveAndAdvanceItemDialog(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    .line 533
    new-instance v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;

    iget-object v1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->state:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-direct {v0, v1, p2, p3}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;-><init>(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->previousItemDialogState:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;

    .line 534
    iput-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->state:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    return-void
.end method


# virtual methods
.method protected final getAnalytics()Lcom/squareup/analytics/Analytics;
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->analytics:Lcom/squareup/analytics/Analytics;

    return-object v0
.end method

.method protected final getDialogFactory()Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->dialogFactory:Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;

    return-object v0
.end method

.method protected final getFlow()Lflow/Flow;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->flow:Lflow/Flow;

    return-object v0
.end method

.method protected final getPreviousItemDialogState()Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->previousItemDialogState:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;

    return-object v0
.end method

.method protected final getPreviousState()Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->previousState:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;

    return-object v0
.end method

.method public final getStartedFromSupportApplet()Z
    .locals 1

    .line 112
    iget-boolean v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->startedFromSupportApplet:Z

    return v0
.end method

.method protected final getState()Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->state:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    return-object v0
.end method

.method protected final getTooltipFactory()Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->tooltipFactory:Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;

    return-object v0
.end method

.method protected final getTutorialApi()Lcom/squareup/register/tutorial/TutorialApi;
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->tutorialApi:Lcom/squareup/register/tutorial/TutorialApi;

    return-object v0
.end method

.method protected final handleAdjustInventory(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;
    .locals 3

    const/4 v0, 0x0

    .line 439
    check-cast v0, Lcom/squareup/tutorialv2/TutorialState;

    if-nez p1, :cond_0

    goto :goto_0

    .line 441
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const v2, -0x243b4496

    if-eq v1, v2, :cond_2

    const v2, 0x5ecc8452

    if-eq v1, v2, :cond_1

    goto :goto_0

    :cond_1
    const-string v1, "Adjust Inventory Screen Shown"

    .line 443
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 444
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->state:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {p0, v0, p1, p2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->saveAndAdvance(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V

    .line 445
    iget-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->tooltipFactory:Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;

    invoke-virtual {p1}, Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;->adjustInventory()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string p2, "Adjust Inventory Screen Dismissed"

    .line 449
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 450
    iget-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->previousItemDialogState:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;

    invoke-virtual {p1}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;->getState()Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->state:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    .line 452
    iget-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->previousItemDialogState:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;

    invoke-virtual {p1}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;->getEventName()Ljava/lang/String;

    move-result-object p1

    .line 453
    iget-object p2, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->previousItemDialogState:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;

    invoke-virtual {p2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;->getEventValue()Ljava/lang/Object;

    move-result-object p2

    .line 451
    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v0

    :cond_3
    :goto_0
    return-object v0
.end method

.method protected final handleEditCategory(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;
    .locals 3

    const/4 v0, 0x0

    .line 404
    check-cast v0, Lcom/squareup/tutorialv2/TutorialState;

    if-nez p1, :cond_0

    goto :goto_0

    .line 406
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const v2, -0x3b4a45bf

    if-eq v1, v2, :cond_2

    const v2, -0x1d3b7957

    if-eq v1, v2, :cond_1

    goto :goto_0

    :cond_1
    const-string v1, "Edit Category Screen Shown"

    .line 408
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 409
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->state:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {p0, v0, p1, p2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->saveAndAdvance(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V

    .line 410
    iget-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->tooltipFactory:Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;

    invoke-virtual {p1}, Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;->editCategory()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string p2, "Edit Category Screen Dismissed"

    .line 414
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 415
    iget-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->previousItemDialogState:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;

    invoke-virtual {p1}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;->getState()Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->state:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    .line 417
    iget-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->previousItemDialogState:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;

    invoke-virtual {p1}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;->getEventName()Ljava/lang/String;

    move-result-object p1

    .line 418
    iget-object p2, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->previousItemDialogState:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;

    invoke-virtual {p2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;->getEventValue()Ljava/lang/Object;

    move-result-object p2

    .line 416
    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v0

    :cond_3
    :goto_0
    return-object v0
.end method

.method public abstract handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;
.end method

.method protected final handleItemDialogSadPath(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;
    .locals 2

    const/4 p2, 0x0

    .line 474
    check-cast p2, Lcom/squareup/tutorialv2/TutorialState;

    if-nez p1, :cond_0

    goto :goto_1

    .line 476
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, -0x59230671

    if-eq v0, v1, :cond_2

    const v1, 0x84c4458

    if-eq v0, v1, :cond_1

    goto :goto_1

    :cond_1
    const-string v0, "Edit Item Label Screen Dismissed"

    .line 478
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_0

    :cond_2
    const-string v0, "Item Variation Screen Dismissed"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 480
    :goto_0
    iget-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->previousItemDialogState:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;

    invoke-virtual {p1}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;->getState()Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->state:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    .line 482
    iget-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->previousItemDialogState:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;

    invoke-virtual {p1}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;->getEventName()Ljava/lang/String;

    move-result-object p1

    .line 483
    iget-object p2, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->previousItemDialogState:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;

    invoke-virtual {p2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;->getEventValue()Ljava/lang/Object;

    move-result-object p2

    .line 481
    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object p2

    :cond_3
    :goto_1
    return-object p2
.end method

.method protected final handleItemNameEvent(Ljava/lang/String;Ljava/lang/Object;Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;)Lcom/squareup/tutorialv2/TutorialState;
    .locals 4

    const-string v0, "dismissState"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 204
    move-object v1, v0

    check-cast v1, Lcom/squareup/tutorialv2/TutorialState;

    if-nez p1, :cond_0

    goto/16 :goto_1

    .line 206
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    const-string v3, "Edit Item Shown"

    sparse-switch v2, :sswitch_data_0

    goto/16 :goto_1

    :sswitch_0
    const-string p3, "Adjust Inventory Screen Shown"

    .line 233
    invoke-virtual {p1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_1

    .line 234
    sget-object p3, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ADJUST_INVENTORY:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-direct {p0, p3, v3, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->saveAndAdvanceItemDialog(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V

    .line 235
    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    goto :goto_1

    :sswitch_1
    const-string p2, "Item Changes Discarded"

    .line 221
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 222
    iput-object p3, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->state:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    .line 223
    invoke-virtual {p0, p4, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    goto :goto_1

    :sswitch_2
    const-string p2, "Item Variation Screen Shown"

    .line 239
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    :sswitch_3
    const-string p2, "Edit Item Label Screen Shown"

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 241
    :goto_0
    sget-object p1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEM_DIALOG_SAD_PATH:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-direct {p0, p1, v3, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->saveAndAdvanceItemDialog(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V

    .line 242
    sget-object v1, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    goto :goto_1

    .line 208
    :sswitch_4
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_1

    .line 209
    iget-object p3, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->state:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {p0, p3, p1, p2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->saveAndAdvance(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V

    .line 210
    iget-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->tooltipFactory:Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;

    invoke-virtual {p1}, Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;->enterItemName()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    goto :goto_1

    :sswitch_5
    const-string p3, "Edit Category Screen Shown"

    .line 227
    invoke-virtual {p1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_1

    .line 228
    sget-object p3, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->EDIT_CATEGORY:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-direct {p0, p3, v3, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->saveAndAdvanceItemDialog(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V

    .line 229
    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    goto :goto_1

    :sswitch_6
    const-string p3, "Edit Item Name Entered"

    .line 214
    invoke-virtual {p1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_1

    .line 215
    iget-object p3, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object p4, Lcom/squareup/analytics/RegisterActionName;->CREATE_ITEM_TUTORIAL_ITEM_NAME_ENTERED:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p3, p4}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 216
    sget-object p3, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEM_DIALOG_ENTER_PRICE:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {p0, p3, p1, p2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->saveAndAdvance(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V

    const-string p1, "Enter Price"

    .line 217
    invoke-virtual {p0, p1, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    :cond_1
    :goto_1
    return-object v1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x21f1dfc7 -> :sswitch_6
        -0x1d3b7957 -> :sswitch_5
        -0xb378286 -> :sswitch_4
        -0x1964bc0 -> :sswitch_3
        0xe1b9cf7 -> :sswitch_2
        0x4c63e0b3 -> :sswitch_1
        0x5ecc8452 -> :sswitch_0
    .end sparse-switch
.end method

.method protected final handleItemPriceEvent(Ljava/lang/String;Ljava/lang/Object;Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;)Lcom/squareup/tutorialv2/TutorialState;
    .locals 5

    const-string v0, "dismissState"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 267
    move-object v1, v0

    check-cast v1, Lcom/squareup/tutorialv2/TutorialState;

    if-nez p1, :cond_0

    goto/16 :goto_1

    .line 269
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    const-string v3, "Save Item"

    const-string v4, "Enter Price"

    sparse-switch v2, :sswitch_data_0

    goto/16 :goto_1

    :sswitch_0
    const-string p3, "Edit Item Price Entered"

    .line 283
    invoke-virtual {p1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_1

    .line 284
    iget-object p3, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object p4, Lcom/squareup/analytics/RegisterActionName;->CREATE_ITEM_TUTORIAL_PRICE_ENTERED:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p3, p4}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 285
    sget-object p3, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEM_DIALOG_SAVE_ITEM:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {p0, p3, p1, p2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->saveAndAdvance(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V

    .line 286
    invoke-virtual {p0, v3, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    goto/16 :goto_1

    :sswitch_1
    const-string p3, "Adjust Inventory Screen Shown"

    .line 309
    invoke-virtual {p1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_1

    .line 310
    sget-object p3, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ADJUST_INVENTORY:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-direct {p0, p3, v4, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->saveAndAdvanceItemDialog(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V

    .line 311
    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    goto/16 :goto_1

    :sswitch_2
    const-string p2, "Item Changes Discarded"

    .line 297
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 298
    iput-object p3, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->state:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    .line 299
    invoke-virtual {p0, p4, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    goto/16 :goto_1

    :sswitch_3
    const-string p2, "Item Variation Screen Shown"

    .line 315
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    .line 271
    :sswitch_4
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 272
    iget-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->tooltipFactory:Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;

    invoke-virtual {p1}, Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;->enterItemPrice()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    goto :goto_1

    :sswitch_5
    const-string p2, "Edit Item Label Screen Shown"

    .line 315
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 317
    :goto_0
    sget-object p1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEM_DIALOG_SAD_PATH:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-direct {p0, p1, v4, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->saveAndAdvanceItemDialog(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V

    .line 318
    sget-object v1, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    goto :goto_1

    :sswitch_6
    const-string p2, "Edit Item Shown"

    .line 322
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {p0, v4, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    goto :goto_1

    :sswitch_7
    const-string p3, "Edit Item Save Pressed"

    .line 291
    invoke-virtual {p1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_1

    .line 292
    sget-object p3, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEM_DIALOG_SAVE_ITEM:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    iput-object p3, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->state:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    .line 293
    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    goto :goto_1

    :sswitch_8
    const-string p3, "Edit Category Screen Shown"

    .line 303
    invoke-virtual {p1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_1

    .line 304
    sget-object p3, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->EDIT_CATEGORY:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-direct {p0, p3, v4, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->saveAndAdvanceItemDialog(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V

    .line 305
    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    goto :goto_1

    :sswitch_9
    const-string p3, "Skip Enter Price"

    .line 276
    invoke-virtual {p1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_1

    .line 277
    iget-object p3, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object p4, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_SKIP_ITEM_PRICE:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p3, p4}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 278
    sget-object p3, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEM_DIALOG_SAVE_ITEM:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {p0, p3, p1, p2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->saveAndAdvance(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V

    .line 279
    invoke-virtual {p0, v3, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    :cond_1
    :goto_1
    return-object v1

    :sswitch_data_0
    .sparse-switch
        -0x4db66f40 -> :sswitch_9
        -0x1d3b7957 -> :sswitch_8
        -0x18f9bc2a -> :sswitch_7
        -0xb378286 -> :sswitch_6
        -0x1964bc0 -> :sswitch_5
        0xdca01c1 -> :sswitch_4
        0xe1b9cf7 -> :sswitch_3
        0x4c63e0b3 -> :sswitch_2
        0x5ecc8452 -> :sswitch_1
        0x77f9c8e9 -> :sswitch_0
    .end sparse-switch
.end method

.method protected final handleItemSaveEvent(Ljava/lang/String;Ljava/lang/Object;Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Lkotlin/jvm/functions/Function0;)Lcom/squareup/tutorialv2/TutorialState;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function0<",
            "Lcom/squareup/tutorialv2/TutorialState;",
            ">;)",
            "Lcom/squareup/tutorialv2/TutorialState;"
        }
    .end annotation

    const-string v0, "dismissState"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "savePressed"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 348
    move-object v1, v0

    check-cast v1, Lcom/squareup/tutorialv2/TutorialState;

    if-nez p1, :cond_0

    goto/16 :goto_1

    .line 350
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    const-string v3, "Save Item"

    sparse-switch v2, :sswitch_data_0

    goto/16 :goto_1

    .line 352
    :sswitch_0
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 353
    iget-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->tooltipFactory:Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;

    invoke-virtual {p1}, Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;->saveItem()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    goto :goto_1

    :sswitch_1
    const-string p3, "Adjust Inventory Screen Shown"

    .line 375
    invoke-virtual {p1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_1

    .line 376
    sget-object p3, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ADJUST_INVENTORY:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-direct {p0, p3, v3, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->saveAndAdvanceItemDialog(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V

    .line 377
    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    goto :goto_1

    :sswitch_2
    const-string p2, "Item Changes Discarded"

    .line 363
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 364
    iput-object p3, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->state:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    .line 365
    invoke-virtual {p0, p4, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    goto :goto_1

    :sswitch_3
    const-string p2, "Item Variation Screen Shown"

    .line 381
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    :sswitch_4
    const-string p2, "Edit Item Label Screen Shown"

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 383
    :goto_0
    sget-object p1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEM_DIALOG_SAD_PATH:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-direct {p0, p1, v3, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->saveAndAdvanceItemDialog(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V

    .line 384
    sget-object v1, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    goto :goto_1

    :sswitch_5
    const-string p2, "Edit Item Save Pressed"

    .line 357
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 358
    iget-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object p2, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_SAVE_ITEM:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, p2}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 359
    invoke-interface {p5}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p1

    move-object v1, p1

    check-cast v1, Lcom/squareup/tutorialv2/TutorialState;

    goto :goto_1

    :sswitch_6
    const-string p3, "Edit Category Screen Shown"

    .line 369
    invoke-virtual {p1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_1

    .line 370
    sget-object p3, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->EDIT_CATEGORY:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-direct {p0, p3, v3, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->saveAndAdvanceItemDialog(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V

    .line 371
    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    :cond_1
    :goto_1
    return-object v1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x1d3b7957 -> :sswitch_6
        -0x18f9bc2a -> :sswitch_5
        -0x1964bc0 -> :sswitch_4
        0xe1b9cf7 -> :sswitch_3
        0x4c63e0b3 -> :sswitch_2
        0x5ecc8452 -> :sswitch_1
        0x7ee0a3d6 -> :sswitch_0
    .end sparse-switch
.end method

.method protected final handleSkip(Ljava/lang/String;)Lcom/squareup/tutorialv2/TutorialState;
    .locals 3

    const/4 v0, 0x0

    .line 500
    check-cast v0, Lcom/squareup/tutorialv2/TutorialState;

    if-nez p1, :cond_0

    goto :goto_0

    .line 501
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const v2, -0x45ba1ef6

    if-eq v1, v2, :cond_2

    const v2, -0x2a30eac4

    if-eq v1, v2, :cond_1

    goto :goto_0

    :cond_1
    const-string v1, "TutorialV2DialogScreen primary tapped"

    .line 503
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 504
    iget-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->previousState:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;

    invoke-virtual {p1}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;->getState()Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->state:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    .line 505
    iget-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->previousState:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;

    invoke-virtual {p1}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;->getEventName()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->previousState:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;

    invoke-virtual {v0}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;->getEventValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v1, "TutorialV2DialogScreen secondary tapped"

    .line 509
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 510
    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->skipTutorial()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v0

    :cond_3
    :goto_0
    return-object v0
.end method

.method public final init(Z)V
    .locals 0

    .line 134
    iput-boolean p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->startedFromSupportApplet:Z

    .line 135
    sget-object p1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->WELCOME:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    iput-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->state:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    return-void
.end method

.method protected final logModalShowEvent()V
    .locals 2

    .line 178
    iget-boolean v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->startedFromSupportApplet:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 179
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CREATE_ITEM_TUTORIAL_SHOW_MODAL_VIA_SUPPORT_APPLET:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    goto :goto_0

    :cond_0
    if-nez v0, :cond_1

    .line 180
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CREATE_ITEM_TUTORIAL_SHOW_MODAL_VIA_ONBOARDING:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method protected final logStartTutorialEvent()V
    .locals 2

    .line 173
    iget-boolean v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->startedFromSupportApplet:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 174
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_START_VIA_APPLET:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    goto :goto_0

    :cond_0
    if-nez v0, :cond_1

    .line 175
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_START_VIA_ONBOARDING:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method protected final markAsCompleted()V
    .locals 3

    .line 184
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->preferences:Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "createItemTutorialCompleted"

    invoke-virtual {v0, v2, v1}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    return-void
.end method

.method protected final saveAndAdvance(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    const-string v0, "nextState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 163
    new-instance v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;

    iget-object v1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->state:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-direct {v0, v1, p2, p3}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;-><init>(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->previousState:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;

    .line 164
    iput-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->state:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    return-void
.end method

.method protected final setPreviousItemDialogState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    iput-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->previousItemDialogState:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;

    return-void
.end method

.method protected final setPreviousState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    iput-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->previousState:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;

    return-void
.end method

.method public final setStartedFromSupportApplet(Z)V
    .locals 0

    .line 112
    iput-boolean p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->startedFromSupportApplet:Z

    return-void
.end method

.method protected final setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    iput-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->state:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    return-void
.end method

.method public final showSkipModal()V
    .locals 2

    .line 143
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CREATE_ITEM_TUTORIAL_SHOW_SKIP_MODAL:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 144
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->SKIP:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    iput-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->state:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    .line 145
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->flow:Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->dialogFactory:Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;

    invoke-virtual {v1}, Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;->skipModal()Lcom/squareup/tutorialv2/TutorialV2DialogScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method protected final skipTutorial()Lcom/squareup/tutorialv2/TutorialState;
    .locals 2

    .line 168
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_SKIP_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 169
    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->markAsCompleted()V

    .line 170
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState;->FINISHED:Lcom/squareup/tutorialv2/TutorialState;

    return-object v0
.end method
