.class public final Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler;
.super Ljava/lang/Object;
.source "CreateItemTutorialDeepLinkHandler.kt"

# interfaces
.implements Lcom/squareup/deeplinks/DeepLinkHandler;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0008\u0010\t\u001a\u00020\nH\u0002J\u0010\u0010\u000b\u001a\u00020\n2\u0006\u0010\u000c\u001a\u00020\rH\u0016J\n\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler;",
        "Lcom/squareup/deeplinks/DeepLinkHandler;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "tutorialCreator",
        "Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;",
        "orderEntryAppletGateway",
        "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
        "(Lcom/squareup/settings/server/Features;Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;Lcom/squareup/orderentry/OrderEntryAppletGateway;)V",
        "handleCreateItemTutorialDeepLink",
        "Lcom/squareup/deeplinks/DeepLinkResult;",
        "handleExternal",
        "uri",
        "Landroid/net/Uri;",
        "maybeStartTutorial",
        "Lcom/squareup/ui/main/HistoryFactory;",
        "items-tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

.field private final tutorialCreator:Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;Lcom/squareup/orderentry/OrderEntryAppletGateway;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tutorialCreator"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderEntryAppletGateway"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler;->features:Lcom/squareup/settings/server/Features;

    iput-object p2, p0, Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler;->tutorialCreator:Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;

    iput-object p3, p0, Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    return-void
.end method

.method private final handleCreateItemTutorialDeepLink()Lcom/squareup/deeplinks/DeepLinkResult;
    .locals 2

    .line 33
    invoke-direct {p0}, Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler;->maybeStartTutorial()Lcom/squareup/ui/main/HistoryFactory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 36
    new-instance v1, Lcom/squareup/deeplinks/DeepLinkResult;

    invoke-direct {v1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/ui/main/HistoryFactory;)V

    goto :goto_0

    .line 38
    :cond_0
    new-instance v1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->UNRECOGNIZED:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {v1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    :goto_0
    return-object v1
.end method

.method private final maybeStartTutorial()Lcom/squareup/ui/main/HistoryFactory;
    .locals 2

    .line 43
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_ITEMS_TUTORIAL:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    invoke-interface {v0}, Lcom/squareup/orderentry/OrderEntryAppletGateway;->hasOrderEntryApplet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler;->tutorialCreator:Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;->ready(Z)V

    .line 45
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    sget-object v1, Lcom/squareup/orderentry/OrderEntryMode;->FAVORITES:Lcom/squareup/orderentry/OrderEntryMode;

    invoke-interface {v0, v1}, Lcom/squareup/orderentry/OrderEntryAppletGateway;->historyFactoryForMode(Lcom/squareup/orderentry/OrderEntryMode;)Lcom/squareup/ui/main/HistoryFactory;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method public handleExternal(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;
    .locals 2

    const-string/jumbo v0, "uri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "root"

    const-string v1, "help"

    .line 21
    filled-new-array {v0, v1}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->contains(Ljava/lang/Iterable;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/startTutorial"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "name"

    .line 23
    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "createItem"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 24
    invoke-direct {p0}, Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler;->handleCreateItemTutorialDeepLink()Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object p1

    return-object p1

    .line 26
    :cond_0
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->UNRECOGNIZED:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    return-object p1
.end method
