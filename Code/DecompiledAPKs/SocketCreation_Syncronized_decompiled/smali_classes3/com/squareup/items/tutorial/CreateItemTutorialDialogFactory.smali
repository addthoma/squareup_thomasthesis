.class public final Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;
.super Ljava/lang/Object;
.source "CreateItemTutorialDialogFactory.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 \t2\u00020\u0001:\u0001\tB\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u0005\u001a\u00020\u0006J\u0006\u0010\u0007\u001a\u00020\u0006J\u0006\u0010\u0008\u001a\u00020\u0006R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;",
        "",
        "badMaybeSquareDeviceCheck",
        "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
        "(Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)V",
        "completeModal",
        "Lcom/squareup/tutorialv2/TutorialV2DialogScreen;",
        "skipModal",
        "welcomeModal",
        "Companion",
        "items-tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory$Companion;

.field public static final SKIP_DIALOG_KEY:Ljava/lang/String; = "CREATE_ITEM_TUTORIAL_SKIP"

.field public static final TUTORIAL_FINISHED_DIALOG_KEY:Ljava/lang/String; = "CREATE_ITEM_TUTORIAL_COMPLETE"

.field public static final WELCOME_DIALOG_KEY:Ljava/lang/String; = "CREATE_ITEM_TUTORIAL_WELCOME"


# instance fields
.field private final badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;->Companion:Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "badMaybeSquareDeviceCheck"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    return-void
.end method


# virtual methods
.method public final completeModal()Lcom/squareup/tutorialv2/TutorialV2DialogScreen;
    .locals 9

    .line 29
    new-instance v0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen;

    .line 30
    new-instance v8, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;

    .line 32
    sget v3, Lcom/squareup/items/tutorial/R$string;->create_item_tutorial_complete_dialog_title:I

    .line 33
    sget v4, Lcom/squareup/items/tutorial/R$string;->create_item_tutorial_complete_dialog_content:I

    .line 34
    sget v5, Lcom/squareup/items/tutorial/R$string;->create_item_tutorial_complete_dialog_button_primary:I

    .line 35
    sget v6, Lcom/squareup/items/tutorial/R$string;->create_item_tutorial_complete_dialog_button_secondary:I

    .line 36
    sget v7, Lcom/squareup/common/tutorial/R$drawable;->tutorial_done:I

    const-string v2, "CREATE_ITEM_TUTORIAL_COMPLETE"

    move-object v1, v8

    .line 30
    invoke-direct/range {v1 .. v7}, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;-><init>(Ljava/lang/Object;IIIII)V

    .line 38
    iget-object v1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    invoke-interface {v1}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result v1

    .line 29
    invoke-direct {v0, v8, v1}, Lcom/squareup/tutorialv2/TutorialV2DialogScreen;-><init>(Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;Z)V

    return-object v0
.end method

.method public final skipModal()Lcom/squareup/tutorialv2/TutorialV2DialogScreen;
    .locals 9

    .line 41
    new-instance v0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen;

    .line 42
    new-instance v8, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;

    .line 44
    sget v3, Lcom/squareup/items/tutorial/R$string;->create_item_tutorial_skip_tutorial_dialog_title:I

    .line 45
    sget v4, Lcom/squareup/items/tutorial/R$string;->create_item_tutorial_skip_tutorial_dialog_content:I

    .line 46
    sget v5, Lcom/squareup/items/tutorial/R$string;->create_item_tutorial_skip_tutorial_dialog_button_primary:I

    .line 47
    sget v6, Lcom/squareup/items/tutorial/R$string;->create_item_tutorial_skip_tutorial_dialog_button_secondary:I

    .line 48
    sget v7, Lcom/squareup/common/tutorial/R$drawable;->tutorial_lifepreserver:I

    const-string v2, "CREATE_ITEM_TUTORIAL_SKIP"

    move-object v1, v8

    .line 42
    invoke-direct/range {v1 .. v7}, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;-><init>(Ljava/lang/Object;IIIII)V

    .line 50
    iget-object v1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    invoke-interface {v1}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result v1

    .line 41
    invoke-direct {v0, v8, v1}, Lcom/squareup/tutorialv2/TutorialV2DialogScreen;-><init>(Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;Z)V

    return-object v0
.end method

.method public final welcomeModal()Lcom/squareup/tutorialv2/TutorialV2DialogScreen;
    .locals 9

    .line 17
    new-instance v0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen;

    .line 18
    new-instance v8, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;

    .line 20
    sget v3, Lcom/squareup/items/tutorial/R$string;->create_item_tutorial_start_tutorial_dialog_title:I

    .line 21
    sget v4, Lcom/squareup/items/tutorial/R$string;->create_item_tutorial_start_tutorial_dialog_content:I

    .line 22
    sget v5, Lcom/squareup/items/tutorial/R$string;->create_item_tutorial_start_tutorial_dialog_button_primary:I

    .line 23
    sget v6, Lcom/squareup/items/tutorial/R$string;->create_item_tutorial_start_tutorial_dialog_button_secondary:I

    .line 24
    sget v7, Lcom/squareup/common/tutorial/R$drawable;->tutorial_item:I

    const-string v2, "CREATE_ITEM_TUTORIAL_WELCOME"

    move-object v1, v8

    .line 18
    invoke-direct/range {v1 .. v7}, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;-><init>(Ljava/lang/Object;IIIII)V

    .line 26
    iget-object v1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    invoke-interface {v1}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result v1

    .line 17
    invoke-direct {v0, v8, v1}, Lcom/squareup/tutorialv2/TutorialV2DialogScreen;-><init>(Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;Z)V

    return-object v0
.end method
