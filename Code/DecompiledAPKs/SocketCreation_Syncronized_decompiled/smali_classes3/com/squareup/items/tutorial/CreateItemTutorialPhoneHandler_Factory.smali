.class public final Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler_Factory;
.super Ljava/lang/Object;
.source "CreateItemTutorialPhoneHandler_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final appletsDrawerRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletsDrawerRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final dialogFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final preferencesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private final tooltipFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialApiProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialApi;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletsDrawerRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialApi;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;)V"
        }
    .end annotation

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler_Factory;->appletsDrawerRunnerProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p2, p0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler_Factory;->dialogFactoryProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p3, p0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler_Factory;->tooltipFactoryProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p4, p0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p5, p0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler_Factory;->tutorialApiProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p6, p0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p7, p0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler_Factory;->preferencesProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletsDrawerRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialApi;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;)",
            "Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler_Factory;"
        }
    .end annotation

    .line 61
    new-instance v8, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/applet/AppletsDrawerRunner;Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;Lflow/Flow;Lcom/squareup/register/tutorial/TutorialApi;Lcom/squareup/analytics/Analytics;Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;
    .locals 9

    .line 68
    new-instance v8, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;-><init>(Lcom/squareup/applet/AppletsDrawerRunner;Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;Lflow/Flow;Lcom/squareup/register/tutorial/TutorialApi;Lcom/squareup/analytics/Analytics;Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;
    .locals 8

    .line 52
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler_Factory;->appletsDrawerRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/applet/AppletsDrawerRunner;

    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler_Factory;->dialogFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;

    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler_Factory;->tooltipFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;

    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler_Factory;->tutorialApiProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/register/tutorial/TutorialApi;

    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler_Factory;->preferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    invoke-static/range {v1 .. v7}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler_Factory;->newInstance(Lcom/squareup/applet/AppletsDrawerRunner;Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;Lflow/Flow;Lcom/squareup/register/tutorial/TutorialApi;Lcom/squareup/analytics/Analytics;Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler_Factory;->get()Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;

    move-result-object v0

    return-object v0
.end method
