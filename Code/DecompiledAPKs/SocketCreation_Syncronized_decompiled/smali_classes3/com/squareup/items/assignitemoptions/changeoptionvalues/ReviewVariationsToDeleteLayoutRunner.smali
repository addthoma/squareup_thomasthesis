.class public final Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner;
.super Ljava/lang/Object;
.source "ReviewVariationsToDeleteLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$Factory;,
        Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$ReviewVariationsToDeleteRow;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReviewVariationsToDeleteLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ReviewVariationsToDeleteLayoutRunner.kt\ncom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 4 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 5 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 6 RecyclerEdges.kt\ncom/squareup/noho/dsl/RecyclerEdgesKt\n*L\n1#1,121:1\n1360#2:122\n1429#2,3:123\n49#3:126\n50#3,3:132\n53#3:149\n599#4,4:127\n601#4:131\n310#5,6:135\n310#5,6:141\n43#6,2:147\n*E\n*S KotlinDebug\n*F\n+ 1 ReviewVariationsToDeleteLayoutRunner.kt\ncom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner\n*L\n96#1:122\n96#1,3:123\n34#1:126\n34#1,3:132\n34#1:149\n34#1,4:127\n34#1:131\n34#1,6:135\n34#1,6:141\n34#1,2:147\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0017\u0018B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0018\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0010\u0010\u0015\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0002H\u0002J\u0010\u0010\u0016\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0002H\u0002R\u0016\u0010\u0008\u001a\n \n*\u0004\u0018\u00010\t0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000e\u001a\n \n*\u0004\u0018\u00010\u000f0\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteScreen;",
        "view",
        "Landroid/view/View;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "kotlin.jvm.PlatformType",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$ReviewVariationsToDeleteRow;",
        "recyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "updateActionBar",
        "updateRecycler",
        "Factory",
        "ReviewVariationsToDeleteRow",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final recycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$ReviewVariationsToDeleteRow;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner;->view:Landroid/view/View;

    .line 32
    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoActionBar;

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 33
    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/items/assignitemoptions/impl/R$id;->review_variations_to_delete_recycler:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 34
    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const-string v0, "recyclerView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    sget-object v0, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 127
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 128
    new-instance v0, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v0}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 132
    invoke-virtual {p2}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 133
    invoke-virtual {p2}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 35
    sget-object p2, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$recycler$1$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$recycler$1$1;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->stableId(Lkotlin/jvm/functions/Function1;)V

    .line 136
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$$special$$inlined$row$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 41
    sget-object v1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$recycler$1$2$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$recycler$1$2$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 136
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 135
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 142
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$$special$$inlined$row$2;->INSTANCE:Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$$special$$inlined$row$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 54
    sget-object v1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$recycler$1$3$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$recycler$1$3$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 60
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$recycler$1$3$2;->INSTANCE:Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$recycler$1$3$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p2, v1}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->edges(Lcom/squareup/cycler/Recycler$RowSpec;Lkotlin/jvm/functions/Function1;)V

    .line 141
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 147
    new-instance p2, Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    invoke-direct {p2}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;-><init>()V

    const/4 v1, 0x0

    .line 63
    invoke-virtual {p2, v1}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->setDefault(I)V

    check-cast p2, Lcom/squareup/cycler/ExtensionSpec;

    .line 147
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->extension(Lcom/squareup/cycler/ExtensionSpec;)V

    .line 130
    invoke-virtual {v0, p1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner;->recycler:Lcom/squareup/cycler/Recycler;

    return-void

    .line 127
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final updateActionBar(Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteScreen;)V
    .locals 11

    .line 87
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 78
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 79
    new-instance v2, Lcom/squareup/resources/ResourceString;

    sget v3, Lcom/squareup/items/assignitemoptions/impl/R$string;->review_variations_to_delete_title:I

    invoke-direct {v2, v3}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 80
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$updateActionBar$1;

    invoke-direct {v3, p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$updateActionBar$1;-><init>(Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v4

    .line 82
    sget-object v5, Lcom/squareup/noho/NohoActionButtonStyle;->DESTRUCTIVE:Lcom/squareup/noho/NohoActionButtonStyle;

    .line 83
    new-instance v1, Lcom/squareup/resources/ResourceString;

    sget v2, Lcom/squareup/common/strings/R$string;->delete:I

    invoke-direct {v1, v2}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    move-object v6, v1

    check-cast v6, Lcom/squareup/resources/TextModel;

    .line 84
    new-instance v1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$updateActionBar$2;

    invoke-direct {v1, p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$updateActionBar$2;-><init>(Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteScreen;)V

    move-object v8, v1

    check-cast v8, Lkotlin/jvm/functions/Function0;

    const/4 v7, 0x0

    const/4 v9, 0x4

    const/4 v10, 0x0

    .line 81
    invoke-static/range {v4 .. v10}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionButton$default(Lcom/squareup/noho/NohoActionBar$Config$Builder;Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 87
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final updateRecycler(Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteScreen;)V
    .locals 4

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$ReviewVariationsToDeleteRow;

    .line 94
    sget-object v1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$ReviewVariationsToDeleteRow$Header;->INSTANCE:Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$ReviewVariationsToDeleteRow$Header;

    check-cast v1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$ReviewVariationsToDeleteRow;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->mutableListOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 96
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteScreen;->getVariationNames()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 122
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {p1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 123
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 124
    check-cast v2, Ljava/lang/String;

    .line 96
    new-instance v3, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$ReviewVariationsToDeleteRow$VariationRow;

    invoke-direct {v3, v2}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$ReviewVariationsToDeleteRow$VariationRow;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 125
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    .line 96
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 98
    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner;->recycler:Lcom/squareup/cycler/Recycler;

    new-instance v1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$updateRecycler$2;

    invoke-direct {v1, v0}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$updateRecycler$2;-><init>(Ljava/util/List;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1}, Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    invoke-direct {p0, p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner;->updateActionBar(Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteScreen;)V

    .line 71
    invoke-direct {p0, p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner;->updateRecycler(Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteScreen;)V

    .line 72
    iget-object p2, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$showRendering$1;-><init>(Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 27
    check-cast p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner;->showRendering(Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
