.class public final Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;
.super Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState;
.source "ChangeOrEnableOptionState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UpdateVariations"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\u0017\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000b\u0010\u000b\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001f\u0010\r\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001J\u0013\u0010\u0010\u001a\u00020\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u00d6\u0003J\t\u0010\u0014\u001a\u00020\u000fH\u00d6\u0001J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001J\u0019\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u000fH\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;",
        "Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState;",
        "valueToExtendExistingVariations",
        "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "selectOptionValues",
        "Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;",
        "(Lcom/squareup/cogs/itemoptions/ItemOptionValue;Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;)V",
        "getSelectOptionValues",
        "()Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;",
        "getValueToExtendExistingVariations",
        "()Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "component1",
        "component2",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final selectOptionValues:Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;

.field private final valueToExtendExistingVariations:Lcom/squareup/cogs/itemoptions/ItemOptionValue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations$Creator;

    invoke-direct {v0}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations$Creator;-><init>()V

    sput-object v0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cogs/itemoptions/ItemOptionValue;Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;)V
    .locals 1

    const-string v0, "selectOptionValues"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 19
    invoke-direct {p0, v0}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;->valueToExtendExistingVariations:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    iput-object p2, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;->selectOptionValues:Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;Lcom/squareup/cogs/itemoptions/ItemOptionValue;Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;ILjava/lang/Object;)Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;->valueToExtendExistingVariations:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;->selectOptionValues:Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;->copy(Lcom/squareup/cogs/itemoptions/ItemOptionValue;Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;)Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/cogs/itemoptions/ItemOptionValue;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;->valueToExtendExistingVariations:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    return-object v0
.end method

.method public final component2()Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;->selectOptionValues:Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;

    return-object v0
.end method

.method public final copy(Lcom/squareup/cogs/itemoptions/ItemOptionValue;Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;)Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;
    .locals 1

    const-string v0, "selectOptionValues"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;

    invoke-direct {v0, p1, p2}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;-><init>(Lcom/squareup/cogs/itemoptions/ItemOptionValue;Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;->valueToExtendExistingVariations:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;->valueToExtendExistingVariations:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;->selectOptionValues:Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;

    iget-object p1, p1, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;->selectOptionValues:Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getSelectOptionValues()Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;->selectOptionValues:Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;

    return-object v0
.end method

.method public final getValueToExtendExistingVariations()Lcom/squareup/cogs/itemoptions/ItemOptionValue;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;->valueToExtendExistingVariations:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;->valueToExtendExistingVariations:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;->selectOptionValues:Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UpdateVariations(valueToExtendExistingVariations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;->valueToExtendExistingVariations:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", selectOptionValues="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;->selectOptionValues:Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;->valueToExtendExistingVariations:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object p2, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;->selectOptionValues:Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;

    const/4 v0, 0x0

    invoke-interface {p2, p1, v0}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    return-void
.end method
