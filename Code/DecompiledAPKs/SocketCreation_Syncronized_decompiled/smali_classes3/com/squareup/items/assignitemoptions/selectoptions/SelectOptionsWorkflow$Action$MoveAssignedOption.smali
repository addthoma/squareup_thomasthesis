.class public final Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;
.super Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action;
.source "SelectOptionsWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MoveAssignedOption"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0011\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0008H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\nH\u00c6\u0003J;\u0010\u001a\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\nH\u00c6\u0001J\u0013\u0010\u001b\u001a\u00020\u001c2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u00d6\u0003J\t\u0010\u001f\u001a\u00020\u0005H\u00d6\u0001J\t\u0010 \u001a\u00020!H\u00d6\u0001J\u0018\u0010\"\u001a\u00020#*\u000e\u0012\u0004\u0012\u00020%\u0012\u0004\u0012\u00020&0$H\u0016R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0011\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action;",
        "option",
        "Lcom/squareup/cogs/itemoptions/ItemOption;",
        "from",
        "",
        "to",
        "assignmentEngine",
        "Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;",
        "assignItemOptionEventLogger",
        "Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;",
        "(Lcom/squareup/cogs/itemoptions/ItemOption;IILcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;)V",
        "getAssignItemOptionEventLogger",
        "()Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;",
        "getAssignmentEngine",
        "()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;",
        "getFrom",
        "()I",
        "getOption",
        "()Lcom/squareup/cogs/itemoptions/ItemOption;",
        "getTo",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsOutput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final assignItemOptionEventLogger:Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;

.field private final assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

.field private final from:I

.field private final option:Lcom/squareup/cogs/itemoptions/ItemOption;

.field private final to:I


# direct methods
.method public constructor <init>(Lcom/squareup/cogs/itemoptions/ItemOption;IILcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;)V
    .locals 1

    const-string v0, "option"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "assignmentEngine"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "assignItemOptionEventLogger"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 387
    invoke-direct {p0, v0}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->option:Lcom/squareup/cogs/itemoptions/ItemOption;

    iput p2, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->from:I

    iput p3, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->to:I

    iput-object p4, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    iput-object p5, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->assignItemOptionEventLogger:Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;Lcom/squareup/cogs/itemoptions/ItemOption;IILcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;ILjava/lang/Object;)Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->option:Lcom/squareup/cogs/itemoptions/ItemOption;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget p2, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->from:I

    :cond_1
    move p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget p3, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->to:I

    :cond_2
    move v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->assignItemOptionEventLogger:Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move p4, p7

    move p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->copy(Lcom/squareup/cogs/itemoptions/ItemOption;IILcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;)Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;",
            "-",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 389
    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->option:Lcom/squareup/cogs/itemoptions/ItemOption;

    invoke-virtual {v0}, Lcom/squareup/cogs/itemoptions/ItemOption;->getId()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->from:I

    iget v2, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->to:I

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->reorderOption(Ljava/lang/String;II)V

    .line 391
    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->getAllVariations()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    const-string v0, "The item should have at least one variation throughout the option assignment flow."

    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonEmpty(Ljava/util/Collection;Ljava/lang/String;)Ljava/util/Collection;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    const/4 v0, 0x0

    .line 393
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    const-string v0, "allVariations[0]"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getItemId()Ljava/lang/String;

    move-result-object p1

    .line 394
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->assignItemOptionEventLogger:Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;

    const-string v1, "itemClientId"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;->logReorderOfItemOptionsInItem(Ljava/lang/String;)V

    return-void
.end method

.method public final component1()Lcom/squareup/cogs/itemoptions/ItemOption;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->option:Lcom/squareup/cogs/itemoptions/ItemOption;

    return-object v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->from:I

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->to:I

    return v0
.end method

.method public final component4()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    return-object v0
.end method

.method public final component5()Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->assignItemOptionEventLogger:Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;

    return-object v0
.end method

.method public final copy(Lcom/squareup/cogs/itemoptions/ItemOption;IILcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;)Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;
    .locals 7

    const-string v0, "option"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "assignmentEngine"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "assignItemOptionEventLogger"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;

    move-object v1, v0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;-><init>(Lcom/squareup/cogs/itemoptions/ItemOption;IILcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->option:Lcom/squareup/cogs/itemoptions/ItemOption;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->option:Lcom/squareup/cogs/itemoptions/ItemOption;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->from:I

    iget v1, p1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->from:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->to:I

    iget v1, p1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->to:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->assignItemOptionEventLogger:Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;

    iget-object p1, p1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->assignItemOptionEventLogger:Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAssignItemOptionEventLogger()Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;
    .locals 1

    .line 386
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->assignItemOptionEventLogger:Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;

    return-object v0
.end method

.method public final getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;
    .locals 1

    .line 385
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    return-object v0
.end method

.method public final getFrom()I
    .locals 1

    .line 383
    iget v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->from:I

    return v0
.end method

.method public final getOption()Lcom/squareup/cogs/itemoptions/ItemOption;
    .locals 1

    .line 382
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->option:Lcom/squareup/cogs/itemoptions/ItemOption;

    return-object v0
.end method

.method public final getTo()I
    .locals 1

    .line 384
    iget v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->to:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->option:Lcom/squareup/cogs/itemoptions/ItemOption;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->from:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->to:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->assignItemOptionEventLogger:Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MoveAssignedOption(option="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->option:Lcom/squareup/cogs/itemoptions/ItemOption;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", from="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->from:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", to="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->to:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", assignmentEngine="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", assignItemOptionEventLogger="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;->assignItemOptionEventLogger:Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
