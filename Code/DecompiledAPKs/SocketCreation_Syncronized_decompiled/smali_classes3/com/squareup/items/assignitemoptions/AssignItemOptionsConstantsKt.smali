.class public final Lcom/squareup/items/assignitemoptions/AssignItemOptionsConstantsKt;
.super Ljava/lang/Object;
.source "AssignItemOptionsConstants.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0003"
    }
    d2 = {
        "MAX_ITEM_OPTIONS_COUNT",
        "",
        "MAX_OPTIONS_PER_ITEM",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final MAX_ITEM_OPTIONS_COUNT:I = 0x3e8

.field public static final MAX_OPTIONS_PER_ITEM:I = 0x6
