.class final Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$7$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SelectOptionsLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner;-><init>(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lcom/squareup/cycler/StandardRowSpec$Creator<",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow$NoOptionsMessage;",
        "Lcom/squareup/noho/NohoMessageView;",
        ">;",
        "Landroid/content/Context;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectOptionsLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectOptionsLayoutRunner.kt\ncom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$7$1\n*L\n1#1,438:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u0014\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u00022\u0006\u0010\u0006\u001a\u00020\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/cycler/StandardRowSpec$Creator;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow$NoOptionsMessage;",
        "Lcom/squareup/noho/NohoMessageView;",
        "context",
        "Landroid/content/Context;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$7$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$7$1;

    invoke-direct {v0}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$7$1;-><init>()V

    sput-object v0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$7$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$7$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 62
    check-cast p1, Lcom/squareup/cycler/StandardRowSpec$Creator;

    check-cast p2, Landroid/content/Context;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$7$1;->invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/StandardRowSpec$Creator<",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow;",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow$NoOptionsMessage;",
            "Lcom/squareup/noho/NohoMessageView;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    new-instance v0, Lcom/squareup/noho/NohoMessageView;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {v0, p2, v1, v2, v1}, Lcom/squareup/noho/NohoMessageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/StandardRowSpec$Creator;->setView(Landroid/view/View;)V

    .line 183
    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoMessageView;

    sget v1, Lcom/squareup/vectoricons/R$drawable;->ui_items_80:I

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setDrawable(I)V

    .line 184
    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoMessageView;

    sget v1, Lcom/squareup/items/assignitemoptions/impl/R$string;->select_options_null_state_content:I

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setTitle(I)V

    .line 185
    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoMessageView;

    .line 186
    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/noho/NohoMessageView;

    invoke-virtual {v1}, Lcom/squareup/noho/NohoMessageView;->getPaddingLeft()I

    move-result v1

    .line 187
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 188
    sget v3, Lcom/squareup/noho/R$dimen;->noho_spacing_large:I

    .line 187
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 189
    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/squareup/noho/NohoMessageView;

    invoke-virtual {v3}, Lcom/squareup/noho/NohoMessageView;->getPaddingRight()I

    move-result v3

    .line 190
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 191
    sget v5, Lcom/squareup/noho/R$dimen;->noho_spacing_large:I

    .line 190
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 185
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/noho/NohoMessageView;->setPadding(IIII)V

    .line 193
    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoMessageView;

    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 195
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    .line 196
    sget v1, Lcom/squareup/noho/R$dimen;->noho_spacing_large:I

    .line 195
    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    const/4 v1, 0x0

    .line 193
    invoke-virtual {v0, v1, p2, v1, v1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    check-cast v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoMessageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method
