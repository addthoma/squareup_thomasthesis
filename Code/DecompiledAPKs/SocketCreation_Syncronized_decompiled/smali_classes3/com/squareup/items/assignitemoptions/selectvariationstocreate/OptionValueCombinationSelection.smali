.class public final Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;
.super Ljava/lang/Object;
.source "SelectVariationsToCreateScreen.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection$CREATOR;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u0000 \u001f2\u00020\u0001:\u0001\u001fB\u000f\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u001d\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0011\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0008H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\nH\u00c6\u0003J\'\u0010\u0014\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\nH\u00c6\u0001J\u0008\u0010\u0015\u001a\u00020\u0016H\u0016J\u0013\u0010\u0017\u001a\u00020\u00082\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u0016H\u00d6\u0001J\t\u0010\u001b\u001a\u00020\u0006H\u00d6\u0001J\u0018\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u001e\u001a\u00020\u0016H\u0016R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u000eR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;",
        "Landroid/os/Parcelable;",
        "parcel",
        "Landroid/os/Parcel;",
        "(Landroid/os/Parcel;)V",
        "name",
        "",
        "isSelected",
        "",
        "combination",
        "Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;",
        "(Ljava/lang/String;ZLcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;)V",
        "getCombination",
        "()Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;",
        "()Z",
        "getName",
        "()Ljava/lang/String;",
        "component1",
        "component2",
        "component3",
        "copy",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "writeToParcel",
        "",
        "flags",
        "CREATOR",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection$CREATOR;


# instance fields
.field private final combination:Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;

.field private final isSelected:Z

.field private final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection$CREATOR;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection$CREATOR;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->CREATOR:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection$CREATOR;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string v1, "parcel.readString()!!"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    const/4 v2, 0x0

    int-to-byte v3, v2

    if-eq v1, v3, :cond_1

    const/4 v2, 0x1

    .line 45
    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;->fromByteArray([B)Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;

    move-result-object p1

    const-string v1, "PrimitiveOptionValueComb\u2026parcel.createByteArray())"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-direct {p0, v0, v2, p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;-><init>(Ljava/lang/String;ZLcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZLcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;)V
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "combination"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->name:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->isSelected:Z

    iput-object p3, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->combination:Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;Ljava/lang/String;ZLcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;ILjava/lang/Object;)Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->name:Ljava/lang/String;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-boolean p2, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->isSelected:Z

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->combination:Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->copy(Ljava/lang/String;ZLcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;)Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->isSelected:Z

    return v0
.end method

.method public final component3()Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->combination:Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;ZLcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;)Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "combination"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;-><init>(Ljava/lang/String;ZLcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->isSelected:Z

    iget-boolean v1, p1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->isSelected:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->combination:Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;

    iget-object p1, p1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->combination:Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCombination()Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->combination:Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->name:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->name:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->isSelected:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->combination:Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public final isSelected()Z
    .locals 1

    .line 39
    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->isSelected:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OptionValueCombinationSelection(name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", isSelected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->isSelected:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", combination="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->combination:Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    iget-object p2, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->name:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 49
    iget-boolean p2, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->isSelected:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByte(B)V

    .line 50
    iget-object p2, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;->combination:Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;->toByteArray()[B

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByteArray([B)V

    return-void
.end method
