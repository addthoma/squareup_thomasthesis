.class public final Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealAssignOptionToItemWorkflow.kt"

# interfaces
.implements Lcom/squareup/items/assignitemoptions/AssignOptionToItemWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/items/assignitemoptions/AssignOptionToItemProps;",
        "Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState;",
        "Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/items/assignitemoptions/AssignOptionToItemWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealAssignOptionToItemWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealAssignOptionToItemWorkflow.kt\ncom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n*L\n1#1,143:1\n32#2,12:144\n*E\n*S KotlinDebug\n*F\n+ 1 RealAssignOptionToItemWorkflow.kt\ncom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow\n*L\n47#1,12:144\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u00012\u00020\n:\u0001\u0019B\u0017\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\u0002H\u0002J\u001a\u0010\u0010\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\u00022\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016JN\u0010\u0014\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0015\u001a\u00020\u00032\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0017H\u0016J\u0010\u0010\u0018\u001a\u00020\u00132\u0006\u0010\u0015\u001a\u00020\u0003H\u0016R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/items/assignitemoptions/AssignOptionToItemProps;",
        "Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState;",
        "Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "Lcom/squareup/items/assignitemoptions/AssignOptionToItemWorkflow;",
        "fetchAllVariationsWorkflow",
        "Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow;",
        "selectOptionsWorkflow",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;",
        "(Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow;Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;)V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final fetchAllVariationsWorkflow:Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow;

.field private final selectOptionsWorkflow:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow;Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "fetchAllVariationsWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectOptionsWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow;->fetchAllVariationsWorkflow:Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow;

    iput-object p2, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow;->selectOptionsWorkflow:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;

    return-void
.end method

.method private final initialState(Lcom/squareup/items/assignitemoptions/AssignOptionToItemProps;)Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState;
    .locals 1

    .line 50
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/AssignOptionToItemProps;->getCanSkipFetchingVariations()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/AssignOptionToItemProps;->getItemToken()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 53
    :cond_0
    sget-object p1, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$FetchAllVariations;->INSTANCE:Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$FetchAllVariations;

    check-cast p1, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState;

    goto :goto_1

    .line 51
    :cond_1
    :goto_0
    sget-object v0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;->Companion:Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions$Companion;

    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/AssignOptionToItemProps;->getItemOptionAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions$Companion;->from(Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;

    move-result-object p1

    check-cast p1, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState;

    :goto_1
    return-object p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/items/assignitemoptions/AssignOptionToItemProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState;
    .locals 3

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 144
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p2}, Lokio/ByteString;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, v2

    :goto_1
    if-eqz p2, :cond_3

    .line 149
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    const-string v2, "Parcel.obtain()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 151
    array-length v2, p2

    invoke-virtual {v0, p2, v1, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 152
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 153
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p2, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v2, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 155
    :cond_3
    check-cast v2, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState;

    if-eqz v2, :cond_4

    goto :goto_2

    .line 47
    :cond_4
    invoke-direct {p0, p1}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow;->initialState(Lcom/squareup/items/assignitemoptions/AssignOptionToItemProps;)Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState;

    move-result-object v2

    :goto_2
    return-object v2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/items/assignitemoptions/AssignOptionToItemProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow;->initialState(Lcom/squareup/items/assignitemoptions/AssignOptionToItemProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/items/assignitemoptions/AssignOptionToItemProps;

    check-cast p2, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow;->render(Lcom/squareup/items/assignitemoptions/AssignOptionToItemProps;Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/items/assignitemoptions/AssignOptionToItemProps;Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/assignitemoptions/AssignOptionToItemProps;",
            "Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState;",
            "-",
            "Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    instance-of v0, p2, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$FetchAllVariations;

    if-eqz v0, :cond_1

    .line 66
    iget-object p2, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow;->fetchAllVariationsWorkflow:Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow;

    move-object v1, p2

    check-cast v1, Lcom/squareup/workflow/Workflow;

    .line 67
    new-instance v2, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsProps;

    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/AssignOptionToItemProps;->getItemToken()Ljava/lang/String;

    move-result-object p2

    if-nez p2, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const/4 v0, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-direct {v2, p2, v0, v3, v4}, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsProps;-><init>(Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v3, 0x0

    .line 68
    new-instance p2, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$render$1;

    invoke-direct {p2, p1}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$render$1;-><init>(Lcom/squareup/items/assignitemoptions/AssignOptionToItemProps;)V

    move-object v4, p2

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p3

    .line 65
    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto :goto_0

    .line 78
    :cond_1
    instance-of v0, p2, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;

    if-eqz v0, :cond_2

    .line 80
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow;->selectOptionsWorkflow:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 81
    new-instance v0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;

    .line 82
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/AssignOptionToItemProps;->getItemName()Ljava/lang/String;

    move-result-object v4

    .line 83
    check-cast p2, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;

    invoke-virtual {p2}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;->getHasAssignedOptions()Z

    move-result v5

    .line 84
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/AssignOptionToItemProps;->getItemOptionAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v6

    const/4 v7, 0x0

    const/16 v8, 0x8

    const/4 v9, 0x0

    move-object v3, v0

    .line 81
    invoke-direct/range {v3 .. v9}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;-><init>(Ljava/lang/String;ZLcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v4, 0x0

    .line 86
    new-instance p2, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$render$2;

    invoke-direct {p2, p1}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$render$2;-><init>(Lcom/squareup/items/assignitemoptions/AssignOptionToItemProps;)V

    move-object v5, p2

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p3

    .line 79
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState;

    invoke-virtual {p0, p1}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow;->snapshotState(Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
