.class public final Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "SelectOptionsWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action;,
        Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$DialogAction;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectOptionsWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectOptionsWorkflow.kt\ncom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 4 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,442:1\n32#2,12:443\n1360#3:455\n1429#3,3:456\n1360#3:459\n1429#3,2:460\n1360#3:462\n1429#3,3:463\n1431#3:466\n1360#3:467\n1429#3,3:468\n704#3:471\n777#3,2:472\n1847#3,3:504\n149#4,5:474\n149#4,5:479\n149#4,5:484\n149#4,5:489\n149#4,5:494\n149#4,5:499\n*E\n*S KotlinDebug\n*F\n+ 1 SelectOptionsWorkflow.kt\ncom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow\n*L\n72#1,12:443\n80#1:455\n80#1,3:456\n82#1:459\n82#1,2:460\n82#1:462\n82#1,3:463\n82#1:466\n88#1:467\n88#1,3:468\n89#1:471\n89#1,2:472\n315#1,3:504\n146#1,5:474\n180#1,5:479\n209#1,5:484\n239#1,5:489\n246#1,5:494\n258#1,5:499\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0096\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010%\n\u0002\u0008\u0005\u0018\u00002<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0001:\u000256B9\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0008\u0008\u0001\u0010\u0014\u001a\u00020\u0015\u00a2\u0006\u0002\u0010\u0016J\u0016\u0010\u0017\u001a\u00020\u00182\u000c\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u001aH\u0002J \u0010\u001c\u001a\u00020\u00182\u000c\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u001a2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u0015H\u0002J\u001a\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u00022\u0008\u0010!\u001a\u0004\u0018\u00010\"H\u0016J^\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020\u00032\u0012\u0010&\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\'2\u0006\u0010(\u001a\u00020\u00182\u000c\u0010)\u001a\u0008\u0012\u0004\u0012\u00020*0\u001a2\u000c\u0010+\u001a\u0008\u0012\u0004\u0012\u00020,0\u001a2\u000c\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u001a2\u0006\u0010-\u001a\u00020\u0015H\u0002JN\u0010.\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010 \u001a\u00020\u00022\u0006\u0010%\u001a\u00020\u00032\u0012\u0010&\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\'H\u0016J\u0010\u0010/\u001a\u00020\"2\u0006\u0010%\u001a\u00020\u0003H\u0016J\u0014\u00100\u001a\u00020\u0018*\u00020,2\u0006\u0010\u001d\u001a\u00020\u0015H\u0002JX\u00101\u001a\u001a\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u000802*\u001a\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u0008022\u0006\u00103\u001a\u00020\u00062\u0012\u00104\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u0008H\u0002R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00067"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "changeOrEnableOptionWorkflow",
        "Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow;",
        "selectVariationsToCreateWorkflow",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;",
        "createOptionForItemWorkflow",
        "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;",
        "assignItemOptionEventLogger",
        "Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;",
        "editOptionEventLogger",
        "Lcom/squareup/items/editoption/EditOptionEventLogger;",
        "merchantToken",
        "",
        "(Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;Lcom/squareup/items/editoption/EditOptionEventLogger;Ljava/lang/String;)V",
        "canCreateNewOption",
        "",
        "allItemOptions",
        "",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
        "canCreateOptionFromSearch",
        "searchText",
        "initialState",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$SelectOption;",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "makeBackgroundSelectOptionsScreen",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "isNextEnabled",
        "assignedItemOption",
        "Lcom/squareup/items/assignitemoptions/models/AssignedItemOption;",
        "availableOptions",
        "Lcom/squareup/cogs/itemoptions/ItemOption;",
        "selectedAvailableOptionId",
        "render",
        "snapshotState",
        "match",
        "renderScreenInBackground",
        "",
        "backgroundLayer",
        "backgroundScreen",
        "Action",
        "DialogAction",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final assignItemOptionEventLogger:Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;

.field private final changeOrEnableOptionWorkflow:Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow;

.field private final createOptionForItemWorkflow:Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;

.field private final editOptionEventLogger:Lcom/squareup/items/editoption/EditOptionEventLogger;

.field private final merchantToken:Ljava/lang/String;

.field private final selectVariationsToCreateWorkflow:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;Lcom/squareup/items/editoption/EditOptionEventLogger;Ljava/lang/String;)V
    .locals 1
    .param p6    # Ljava/lang/String;
        .annotation runtime Lcom/squareup/user/MerchantToken;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "changeOrEnableOptionWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectVariationsToCreateWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "createOptionForItemWorkflow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "assignItemOptionEventLogger"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editOptionEventLogger"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "merchantToken"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->changeOrEnableOptionWorkflow:Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow;

    iput-object p2, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->selectVariationsToCreateWorkflow:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;

    iput-object p3, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->createOptionForItemWorkflow:Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;

    iput-object p4, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->assignItemOptionEventLogger:Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;

    iput-object p5, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->editOptionEventLogger:Lcom/squareup/items/editoption/EditOptionEventLogger;

    iput-object p6, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->merchantToken:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic access$getAssignItemOptionEventLogger$p(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;)Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;
    .locals 0

    .line 58
    iget-object p0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->assignItemOptionEventLogger:Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;

    return-object p0
.end method

.method public static final synthetic access$getEditOptionEventLogger$p(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;)Lcom/squareup/items/editoption/EditOptionEventLogger;
    .locals 0

    .line 58
    iget-object p0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->editOptionEventLogger:Lcom/squareup/items/editoption/EditOptionEventLogger;

    return-object p0
.end method

.method public static final synthetic access$getMerchantToken$p(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;)Ljava/lang/String;
    .locals 0

    .line 58
    iget-object p0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->merchantToken:Ljava/lang/String;

    return-object p0
.end method

.method private final canCreateNewOption(Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;)Z"
        }
    .end annotation

    .line 308
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    const/16 v0, 0x3e8

    if-ge p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final canCreateOptionFromSearch(Ljava/util/List;Ljava/lang/String;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .line 313
    invoke-direct {p0, p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->canCreateNewOption(Ljava/util/List;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_9

    .line 314
    move-object v0, p2

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v3, 0x1

    :goto_1
    if-nez v3, :cond_9

    .line 315
    check-cast p1, Ljava/lang/Iterable;

    .line 504
    instance-of v3, p1, Ljava/util/Collection;

    if-eqz v3, :cond_3

    move-object v3, p1

    check-cast v3, Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    const/4 p1, 0x1

    goto :goto_2

    .line 505
    :cond_3
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    .line 315
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "it.name"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "null cannot be cast to non-null type kotlin.CharSequence"

    if-eqz v3, :cond_8

    check-cast v3, Ljava/lang/CharSequence;

    invoke-static {v3}, Lkotlin/text/StringsKt;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, "null cannot be cast to non-null type java.lang.String"

    if-eqz v3, :cond_7

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    const-string v6, "(this as java.lang.String).toLowerCase()"

    invoke-static {v3, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_6

    invoke-static {v0}, Lkotlin/text/StringsKt;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 p1, 0x0

    goto :goto_2

    :cond_5
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v5}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_6
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v4}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_7
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v5}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_8
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v4}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :goto_2
    if-eqz p1, :cond_9

    const/4 v1, 0x1

    :cond_9
    return v1
.end method

.method private final makeBackgroundSelectOptionsScreen(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;Lcom/squareup/workflow/RenderContext;ZLjava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;",
            "-",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsOutput;",
            ">;Z",
            "Ljava/util/List<",
            "Lcom/squareup/items/assignitemoptions/models/AssignedItemOption;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOption;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    .line 282
    new-instance v15, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;

    .line 284
    invoke-direct {v0, v1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->canCreateNewOption(Ljava/util/List;)Z

    move-result v3

    .line 287
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;->getSearchText()Ljava/lang/String;

    move-result-object v2

    .line 285
    invoke-direct {v0, v1, v2}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->canCreateOptionFromSearch(Ljava/util/List;Ljava/lang/String;)Z

    move-result v4

    .line 290
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;->getSearchText()Ljava/lang/String;

    move-result-object v1

    .line 292
    sget-object v2, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$makeBackgroundSelectOptionsScreen$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$makeBackgroundSelectOptionsScreen$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    const-string v5, "SelectOptions-Search"

    move-object/from16 v6, p2

    .line 289
    invoke-static {v6, v1, v5, v2}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v5

    .line 298
    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$makeBackgroundSelectOptionsScreen$2;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$makeBackgroundSelectOptionsScreen$2;

    move-object v9, v1

    check-cast v9, Lkotlin/jvm/functions/Function1;

    .line 299
    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$makeBackgroundSelectOptionsScreen$3;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$makeBackgroundSelectOptionsScreen$3;

    move-object v10, v1

    check-cast v10, Lkotlin/jvm/functions/Function1;

    .line 300
    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$makeBackgroundSelectOptionsScreen$4;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$makeBackgroundSelectOptionsScreen$4;

    move-object v11, v1

    check-cast v11, Lkotlin/jvm/functions/Function2;

    .line 301
    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$makeBackgroundSelectOptionsScreen$5;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$makeBackgroundSelectOptionsScreen$5;

    move-object v12, v1

    check-cast v12, Lkotlin/jvm/functions/Function0;

    .line 302
    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$makeBackgroundSelectOptionsScreen$6;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$makeBackgroundSelectOptionsScreen$6;

    move-object v13, v1

    check-cast v13, Lkotlin/jvm/functions/Function0;

    .line 303
    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$makeBackgroundSelectOptionsScreen$7;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$makeBackgroundSelectOptionsScreen$7;

    move-object v14, v1

    check-cast v14, Lkotlin/jvm/functions/Function0;

    move-object v1, v15

    move/from16 v2, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p7

    .line 282
    invoke-direct/range {v1 .. v14}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;-><init>(ZZZLcom/squareup/workflow/text/WorkflowEditableText;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    return-object v15
.end method

.method private final match(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;)Z
    .locals 6

    if-eqz p2, :cond_5

    .line 434
    check-cast p2, Ljava/lang/CharSequence;

    invoke-static {p2}, Lkotlin/text/StringsKt;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    const-string v0, "null cannot be cast to non-null type java.lang.String"

    if-eqz p2, :cond_4

    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p2

    const-string v1, "(this as java.lang.String).toLowerCase()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 435
    invoke-virtual {p1}, Lcom/squareup/cogs/itemoptions/ItemOption;->getName()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/CharSequence;

    .line 436
    check-cast p2, Ljava/lang/CharSequence;

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static {v2, p2, v5, v4, v3}, Lkotlin/text/StringsKt;->contains$default(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILjava/lang/Object;)Z

    move-result v2

    .line 437
    invoke-virtual {p1}, Lcom/squareup/cogs/itemoptions/ItemOption;->getDisplayName()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    .line 438
    invoke-static {p1, p2, v5, v4, v3}, Lkotlin/text/StringsKt;->contains$default(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILjava/lang/Object;)Z

    move-result p1

    if-nez v2, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    const/4 v5, 0x1

    :cond_1
    return v5

    .line 437
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 435
    :cond_3
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 434
    :cond_4
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final renderScreenInBackground(Ljava/util/Map;Lcom/squareup/container/PosLayering;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 268
    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 269
    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$SelectOption;
    .locals 4

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x1

    const/4 v0, 0x0

    if-eqz p2, :cond_4

    .line 443
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p2}, Lokio/ByteString;->size()I

    move-result v1

    const/4 v2, 0x0

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, v0

    :goto_1
    if-eqz p2, :cond_3

    .line 448
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    const-string v3, "Parcel.obtain()"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 449
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 450
    array-length v3, p2

    invoke-virtual {v1, p2, v2, v3}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 451
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 452
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v1, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p2

    if-nez p2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string v2, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 453
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    goto :goto_2

    :cond_3
    move-object p2, v0

    .line 454
    :goto_2
    check-cast p2, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$SelectOption;

    if-eqz p2, :cond_4

    goto :goto_3

    .line 72
    :cond_4
    new-instance p2, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$SelectOption;

    invoke-direct {p2, v0, p1, v0}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$SelectOption;-><init>(Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :goto_3
    return-object p2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 58
    check-cast p1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->initialState(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$SelectOption;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 58
    check-cast p1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;

    check-cast p2, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->render(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 28
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;",
            "-",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    move-object/from16 v8, p0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    const-string v3, "props"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "state"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "context"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->getItemOptionValuesInUseByOptionIds()Ljava/util/Map;

    move-result-object v3

    .line 80
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->getItemOptionsUsedByItem()Ljava/util/List;

    move-result-object v4

    const-string v5, "props.assignmentEngine.itemOptionsUsedByItem"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Ljava/lang/Iterable;

    .line 455
    new-instance v5, Ljava/util/ArrayList;

    const/16 v6, 0xa

    invoke-static {v4, v6}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v7

    invoke-direct {v5, v7}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v5, Ljava/util/Collection;

    .line 456
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    const-string v9, "it"

    if-eqz v7, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    .line 457
    check-cast v7, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    .line 80
    invoke-static {v7, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v7}, Lcom/squareup/cogs/itemoptions/ItemOptionKt;->toItemOption(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;)Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 458
    :cond_0
    check-cast v5, Ljava/util/List;

    .line 81
    move-object v4, v5

    check-cast v4, Ljava/lang/Iterable;

    .line 459
    new-instance v7, Ljava/util/ArrayList;

    invoke-static {v4, v6}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v10

    invoke-direct {v7, v10}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v7, Ljava/util/Collection;

    .line 460
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    .line 461
    check-cast v10, Lcom/squareup/cogs/itemoptions/ItemOption;

    .line 83
    invoke-virtual {v10}, Lcom/squareup/cogs/itemoptions/ItemOption;->getId()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v3, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    if-nez v11, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    check-cast v11, Ljava/lang/Iterable;

    .line 462
    new-instance v12, Ljava/util/ArrayList;

    invoke-static {v11, v6}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v13

    invoke-direct {v12, v13}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v12, Ljava/util/Collection;

    .line 463
    invoke-interface {v11}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    .line 464
    check-cast v13, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    .line 84
    invoke-static {v13, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v13}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 465
    :cond_2
    check-cast v12, Ljava/util/List;

    .line 85
    new-instance v11, Lcom/squareup/items/assignitemoptions/models/AssignedItemOption;

    invoke-direct {v11, v10, v12}, Lcom/squareup/items/assignitemoptions/models/AssignedItemOption;-><init>(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/util/List;)V

    invoke-interface {v7, v11}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 466
    :cond_3
    move-object v4, v7

    check-cast v4, Ljava/util/List;

    .line 87
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->getItemOptionsNotUsedByItem()Ljava/util/List;

    move-result-object v3

    const-string v7, "props.assignmentEngine.itemOptionsNotUsedByItem"

    invoke-static {v3, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Ljava/lang/Iterable;

    .line 467
    new-instance v7, Ljava/util/ArrayList;

    invoke-static {v3, v6}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v6

    invoke-direct {v7, v6}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v7, Ljava/util/Collection;

    .line 468
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 469
    check-cast v6, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    .line 88
    invoke-static {v6, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v6}, Lcom/squareup/cogs/itemoptions/ItemOptionKt;->toItemOption(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;)Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v6

    invoke-interface {v7, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 470
    :cond_4
    check-cast v7, Ljava/util/List;

    check-cast v7, Ljava/lang/Iterable;

    .line 471
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    check-cast v3, Ljava/util/Collection;

    .line 472
    invoke-interface {v7}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_5
    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    move-object v9, v7

    check-cast v9, Lcom/squareup/cogs/itemoptions/ItemOption;

    .line 89
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;->getSearchText()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->match(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v3, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 473
    :cond_6
    move-object/from16 v19, v3

    check-cast v19, Ljava/util/List;

    .line 90
    move-object v3, v5

    check-cast v3, Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    .line 92
    instance-of v6, v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$SelectOption;

    const-string v7, ""

    const-string v15, "props.assignmentEngine.allItemOptions"

    if-eqz v6, :cond_7

    .line 93
    invoke-interface/range {p3 .. p3}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v6

    .line 94
    new-instance v9, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;

    .line 96
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v10

    invoke-virtual {v10}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->getAllItemOptions()Ljava/util/List;

    move-result-object v10

    invoke-static {v10, v15}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v8, v10}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->canCreateNewOption(Ljava/util/List;)Z

    move-result v10

    .line 98
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v11

    invoke-virtual {v11}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->getAllItemOptions()Ljava/util/List;

    move-result-object v11

    invoke-static {v11, v15}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;->getSearchText()Ljava/lang/String;

    move-result-object v12

    .line 97
    invoke-direct {v8, v11, v12}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->canCreateOptionFromSearch(Ljava/util/List;Ljava/lang/String;)Z

    move-result v16

    .line 102
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;->getSearchText()Ljava/lang/String;

    move-result-object v1

    .line 104
    sget-object v11, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$1;

    check-cast v11, Lkotlin/jvm/functions/Function1;

    const-string v12, "SelectOptions-Search"

    .line 101
    invoke-static {v2, v1, v12, v11}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v17

    .line 109
    new-instance v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$2;

    invoke-direct {v1, v6, v5, v0}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$2;-><init>(Lcom/squareup/workflow/Sink;Ljava/util/List;Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;)V

    move-object/from16 v21, v1

    check-cast v21, Lkotlin/jvm/functions/Function1;

    .line 117
    new-instance v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$3;

    invoke-direct {v1, v6, v5, v0}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$3;-><init>(Lcom/squareup/workflow/Sink;Ljava/util/List;Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;)V

    move-object/from16 v22, v1

    check-cast v22, Lkotlin/jvm/functions/Function1;

    .line 124
    new-instance v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$4;

    invoke-direct {v1, v8, v6, v4, v0}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$4;-><init>(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;Lcom/squareup/workflow/Sink;Ljava/util/List;Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;)V

    move-object/from16 v23, v1

    check-cast v23, Lkotlin/jvm/functions/Function2;

    .line 133
    new-instance v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$5;

    invoke-direct {v1, v6}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$5;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object/from16 v24, v1

    check-cast v24, Lkotlin/jvm/functions/Function0;

    .line 134
    new-instance v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$6;

    invoke-direct {v1, v6, v0}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$6;-><init>(Lcom/squareup/workflow/Sink;Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;)V

    move-object/from16 v25, v1

    check-cast v25, Lkotlin/jvm/functions/Function0;

    .line 142
    new-instance v0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$7;

    invoke-direct {v0, v8}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$7;-><init>(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;)V

    move-object/from16 v26, v0

    check-cast v26, Lkotlin/jvm/functions/Function0;

    const-string v20, ""

    move-object v13, v9

    move v14, v3

    move v15, v10

    move-object/from16 v18, v4

    .line 94
    invoke-direct/range {v13 .. v26}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;-><init>(ZZZLcom/squareup/workflow/text/WorkflowEditableText;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast v9, Lcom/squareup/workflow/legacy/V2Screen;

    .line 475
    new-instance v0, Lcom/squareup/workflow/legacy/Screen;

    .line 476
    const-class v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v1, v7}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 477
    sget-object v2, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v2}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v2

    .line 475
    invoke-direct {v0, v1, v9, v2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 147
    sget-object v1, Lcom/squareup/container/PosLayering;->SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {v0, v1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v0

    goto/16 :goto_5

    .line 149
    :cond_7
    instance-of v5, v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$ChangeOrEnableOption;

    if-eqz v5, :cond_8

    .line 150
    new-instance v5, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;

    .line 151
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;->getItemName()Ljava/lang/String;

    move-result-object v21

    .line 152
    move-object v6, v1

    check-cast v6, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$ChangeOrEnableOption;

    invoke-virtual {v6}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$ChangeOrEnableOption;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v9

    invoke-virtual {v9}, Lcom/squareup/cogs/itemoptions/ItemOption;->getId()Ljava/lang/String;

    move-result-object v22

    .line 153
    invoke-virtual {v6}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$ChangeOrEnableOption;->getInitiallySelectedOptionValues()Ljava/util/List;

    move-result-object v23

    .line 154
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;->isIncrementalAssignment()Z

    move-result v24

    .line 155
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v25

    .line 156
    invoke-virtual {v6}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$ChangeOrEnableOption;->isChange()Z

    move-result v26

    move-object/from16 v20, v5

    .line 150
    invoke-direct/range {v20 .. v26}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;Z)V

    .line 159
    iget-object v6, v8, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->changeOrEnableOptionWorkflow:Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow;

    move-object v10, v6

    check-cast v10, Lcom/squareup/workflow/Workflow;

    const/4 v12, 0x0

    .line 160
    new-instance v6, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$8;

    invoke-direct {v6, v0}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$8;-><init>(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;)V

    move-object v13, v6

    check-cast v13, Lkotlin/jvm/functions/Function1;

    const/4 v14, 0x4

    const/4 v6, 0x0

    move-object/from16 v9, p3

    move-object v11, v5

    move-object v5, v15

    move-object v15, v6

    .line 158
    invoke-static/range {v9 .. v15}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map;

    .line 169
    invoke-static {v6}, Lkotlin/collections/MapsKt;->toMutableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v9

    .line 171
    sget-object v10, Lcom/squareup/container/PosLayering;->SHEET:Lcom/squareup/container/PosLayering;

    .line 178
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->getAllItemOptions()Ljava/util/List;

    move-result-object v6

    invoke-static {v6, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v11, ""

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v5, v19

    move-object v15, v7

    move-object v7, v11

    .line 172
    invoke-direct/range {v0 .. v7}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->makeBackgroundSelectOptionsScreen(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;Lcom/squareup/workflow/RenderContext;ZLjava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 480
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 481
    const-class v2, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v15}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 482
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 480
    invoke-direct {v1, v2, v0, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 170
    invoke-direct {v8, v9, v10, v1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->renderScreenInBackground(Ljava/util/Map;Lcom/squareup/container/PosLayering;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object v0

    .line 181
    invoke-static {v0}, Lkotlin/collections/MapsKt;->toMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    goto/16 :goto_5

    :cond_8
    move-object v5, v15

    move-object v15, v7

    .line 183
    instance-of v6, v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$CreateOption;

    if-eqz v6, :cond_9

    .line 184
    iget-object v3, v8, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->createOptionForItemWorkflow:Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;

    move-object v10, v3

    check-cast v10, Lcom/squareup/workflow/Workflow;

    .line 185
    new-instance v11, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemProps;

    .line 186
    move-object v3, v1

    check-cast v3, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$CreateOption;

    invoke-virtual {v3}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$CreateOption;->getNewOptionName()Ljava/lang/String;

    move-result-object v3

    .line 187
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;->getItemName()Ljava/lang/String;

    move-result-object v6

    .line 188
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;->isIncrementalAssignment()Z

    move-result v7

    .line 189
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v9

    .line 185
    invoke-direct {v11, v3, v6, v7, v9}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemProps;-><init>(Ljava/lang/String;Ljava/lang/String;ZLcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)V

    const/4 v12, 0x0

    .line 191
    new-instance v3, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$9;

    invoke-direct {v3, v0}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$9;-><init>(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;)V

    move-object v13, v3

    check-cast v13, Lkotlin/jvm/functions/Function1;

    const/4 v14, 0x4

    const/4 v3, 0x0

    move-object/from16 v9, p3

    move-object v7, v15

    move-object v15, v3

    .line 183
    invoke-static/range {v9 .. v15}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    .line 198
    invoke-static {v3}, Lkotlin/collections/MapsKt;->toMutableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v9

    .line 200
    sget-object v10, Lcom/squareup/container/PosLayering;->SHEET:Lcom/squareup/container/PosLayering;

    const/4 v3, 0x1

    .line 207
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->getAllItemOptions()Ljava/util/List;

    move-result-object v6

    invoke-static {v6, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v11, ""

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v5, v19

    move-object v15, v7

    move-object v7, v11

    .line 201
    invoke-direct/range {v0 .. v7}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->makeBackgroundSelectOptionsScreen(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;Lcom/squareup/workflow/RenderContext;ZLjava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 485
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 486
    const-class v2, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v15}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 487
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 485
    invoke-direct {v1, v2, v0, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 199
    invoke-direct {v8, v9, v10, v1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->renderScreenInBackground(Ljava/util/Map;Lcom/squareup/container/PosLayering;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object v0

    .line 211
    invoke-static {v0}, Lkotlin/collections/MapsKt;->toMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    goto/16 :goto_5

    .line 212
    :cond_9
    instance-of v6, v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$CreateVariations;

    if-eqz v6, :cond_a

    .line 213
    iget-object v6, v8, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->selectVariationsToCreateWorkflow:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;

    move-object v10, v6

    check-cast v10, Lcom/squareup/workflow/Workflow;

    .line 214
    new-instance v6, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;

    .line 215
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;->getItemName()Ljava/lang/String;

    move-result-object v21

    .line 216
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v22

    .line 217
    move-object v7, v1

    check-cast v7, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$CreateVariations;

    invoke-virtual {v7}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$CreateVariations;->getNumberOfExistingVariations()I

    move-result v23

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x18

    const/16 v27, 0x0

    move-object/from16 v20, v6

    .line 214
    invoke-direct/range {v20 .. v27}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;-><init>(Ljava/lang/String;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;ILcom/squareup/cogs/itemoptions/ItemOptionValue;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v12, 0x0

    .line 218
    new-instance v7, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$10;

    invoke-direct {v7, v0}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$10;-><init>(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;)V

    move-object v13, v7

    check-cast v13, Lkotlin/jvm/functions/Function1;

    const/4 v14, 0x4

    const/4 v7, 0x0

    move-object/from16 v9, p3

    move-object v11, v6

    move-object v6, v15

    move-object v15, v7

    .line 212
    invoke-static/range {v9 .. v15}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map;

    .line 228
    invoke-static {v7}, Lkotlin/collections/MapsKt;->toMutableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v9

    .line 230
    sget-object v10, Lcom/squareup/container/PosLayering;->SHEET:Lcom/squareup/container/PosLayering;

    .line 237
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->getAllItemOptions()Ljava/util/List;

    move-result-object v7

    invoke-static {v7, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v11, ""

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v5, v19

    move-object v12, v6

    move-object v6, v7

    move-object v7, v11

    .line 231
    invoke-direct/range {v0 .. v7}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->makeBackgroundSelectOptionsScreen(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;Lcom/squareup/workflow/RenderContext;ZLjava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 490
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 491
    const-class v2, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v12}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 492
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 490
    invoke-direct {v1, v2, v0, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 229
    invoke-direct {v8, v9, v10, v1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->renderScreenInBackground(Ljava/util/Map;Lcom/squareup/container/PosLayering;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object v0

    .line 240
    invoke-static {v0}, Lkotlin/collections/MapsKt;->toMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    goto/16 :goto_5

    :cond_a
    move-object v12, v15

    .line 241
    instance-of v6, v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$TooManyAssignedOptionsError;

    if-eqz v6, :cond_b

    .line 242
    invoke-interface/range {p3 .. p3}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v6

    .line 243
    new-instance v7, Lcom/squareup/items/assignitemoptions/selectoptions/TooManyAssignedOptionsScreen;

    .line 244
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;->getMaxOptionsPerItem()I

    move-result v9

    .line 245
    new-instance v10, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$11;

    invoke-direct {v10, v6}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$11;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v10, Lkotlin/jvm/functions/Function0;

    .line 243
    invoke-direct {v7, v9, v10}, Lcom/squareup/items/assignitemoptions/selectoptions/TooManyAssignedOptionsScreen;-><init>(ILkotlin/jvm/functions/Function0;)V

    check-cast v7, Lcom/squareup/workflow/legacy/V2Screen;

    .line 495
    new-instance v6, Lcom/squareup/workflow/legacy/Screen;

    .line 496
    const-class v9, Lcom/squareup/items/assignitemoptions/selectoptions/TooManyAssignedOptionsScreen;

    invoke-static {v9}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v9

    invoke-static {v9, v12}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v9

    .line 497
    sget-object v10, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v10}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v10

    .line 495
    invoke-direct {v6, v9, v7, v10}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 246
    sget-object v7, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {v6, v7}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v6

    .line 247
    invoke-static {v6}, Lkotlin/collections/MapsKt;->toMutableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v9

    .line 249
    sget-object v10, Lcom/squareup/container/PosLayering;->SHEET:Lcom/squareup/container/PosLayering;

    .line 256
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->getAllItemOptions()Ljava/util/List;

    move-result-object v6

    invoke-static {v6, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 257
    move-object v0, v1

    check-cast v0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$TooManyAssignedOptionsError;

    invoke-virtual {v0}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$TooManyAssignedOptionsError;->getSelectedAvailableOptionId()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v5, v19

    .line 250
    invoke-direct/range {v0 .. v7}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->makeBackgroundSelectOptionsScreen(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;Lcom/squareup/workflow/RenderContext;ZLjava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 500
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 501
    const-class v2, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v12}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 502
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 500
    invoke-direct {v1, v2, v0, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 248
    invoke-direct {v8, v9, v10, v1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->renderScreenInBackground(Ljava/util/Map;Lcom/squareup/container/PosLayering;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object v0

    .line 259
    invoke-static {v0}, Lkotlin/collections/MapsKt;->toMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    :goto_5
    return-object v0

    :cond_b
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public snapshotState(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 317
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 58
    check-cast p1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;

    invoke-virtual {p0, p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->snapshotState(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
