.class public final Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflowRunner;
.super Lcom/squareup/container/DynamicPropsWorkflowV2Runner;
.source "RealAssignOptionToItemWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/items/assignitemoptions/AssignOptionToItemWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner<",
        "Lcom/squareup/items/assignitemoptions/AssignOptionToItemProps;",
        "Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutput;",
        ">;",
        "Lcom/squareup/items/assignitemoptions/AssignOptionToItemWorkflowRunner;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u00020\u0004B\'\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0014J\u0010\u0010\u0014\u001a\u00020\u00112\u0006\u0010\u0015\u001a\u00020\u0003H\u0002JL\u0010\u0016\u001a\u00020\u00112\u0006\u0010\u0017\u001a\u00020\u00182\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u00182\u0006\u0010\u001a\u001a\u00020\u001b2\u000c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u001e0\u001d2\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u001d2\u000c\u0010 \u001a\u0008\u0012\u0004\u0012\u00020!0\u001dH\u0016R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u00020\nX\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflowRunner;",
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner;",
        "Lcom/squareup/items/assignitemoptions/AssignOptionToItemProps;",
        "Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutput;",
        "Lcom/squareup/items/assignitemoptions/AssignOptionToItemWorkflowRunner;",
        "viewFactory",
        "Lcom/squareup/items/assignitemoptions/AssignOptionToItemViewFactory;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "workflow",
        "Lcom/squareup/items/assignitemoptions/AssignOptionToItemWorkflow;",
        "resultHandler",
        "Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutputHandler;",
        "(Lcom/squareup/items/assignitemoptions/AssignOptionToItemViewFactory;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/items/assignitemoptions/AssignOptionToItemWorkflow;Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutputHandler;)V",
        "getWorkflow",
        "()Lcom/squareup/items/assignitemoptions/AssignOptionToItemWorkflow;",
        "onEnterScope",
        "",
        "newScope",
        "Lmortar/MortarScope;",
        "onOptionAssigned",
        "result",
        "start",
        "itemName",
        "",
        "itemToken",
        "canSkipFetchingVariations",
        "",
        "existingVariations",
        "",
        "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
        "locallyDeletedVariationTokens",
        "existingItemOptions",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final container:Lcom/squareup/ui/main/PosContainer;

.field private final resultHandler:Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutputHandler;

.field private final workflow:Lcom/squareup/items/assignitemoptions/AssignOptionToItemWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/items/assignitemoptions/AssignOptionToItemViewFactory;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/items/assignitemoptions/AssignOptionToItemWorkflow;Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutputHandler;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "viewFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resultHandler"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    sget-object v0, Lcom/squareup/items/assignitemoptions/AssignOptionToItemWorkflowRunner;->Companion:Lcom/squareup/items/assignitemoptions/AssignOptionToItemWorkflowRunner$Companion;

    invoke-virtual {v0}, Lcom/squareup/items/assignitemoptions/AssignOptionToItemWorkflowRunner$Companion;->getNAME()Ljava/lang/String;

    move-result-object v2

    .line 20
    invoke-interface {p2}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v3

    .line 21
    move-object v4, p1

    check-cast v4, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x18

    const/4 v8, 0x0

    move-object v1, p0

    .line 18
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;ZLkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p2, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    iput-object p3, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflowRunner;->workflow:Lcom/squareup/items/assignitemoptions/AssignOptionToItemWorkflow;

    iput-object p4, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflowRunner;->resultHandler:Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutputHandler;

    return-void
.end method

.method public static final synthetic access$onOptionAssigned(Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflowRunner;Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutput;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflowRunner;->onOptionAssigned(Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutput;)V

    return-void
.end method

.method private final onOptionAssigned(Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutput;)V
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflowRunner;->resultHandler:Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutputHandler;

    invoke-interface {v0, p1}, Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutputHandler;->handleOutput(Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutput;)V

    return-void
.end method


# virtual methods
.method protected getWorkflow()Lcom/squareup/items/assignitemoptions/AssignOptionToItemWorkflow;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflowRunner;->workflow:Lcom/squareup/items/assignitemoptions/AssignOptionToItemWorkflow;

    return-object v0
.end method

.method public bridge synthetic getWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflowRunner;->getWorkflow()Lcom/squareup/items/assignitemoptions/AssignOptionToItemWorkflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "newScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-super {p0, p1}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;->onEnterScope(Lmortar/MortarScope;)V

    .line 28
    invoke-virtual {p0}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflowRunner$onEnterScope$1;

    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    invoke-direct {v1, v2}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflowRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/main/PosContainer;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflowRunner$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflowRunner$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "onUpdateScreens().subscribe(container::pushStack)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    .line 31
    invoke-virtual {p0}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflowRunner$onEnterScope$2;

    move-object v2, p0

    check-cast v2, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflowRunner;

    invoke-direct {v1, v2}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflowRunner$onEnterScope$2;-><init>(Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflowRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflowRunner$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflowRunner$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "onResult().subscribe(::onOptionAssigned)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public start(Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;)V"
        }
    .end annotation

    const-string v0, "itemName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "existingVariations"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locallyDeletedVariationTokens"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "existingItemOptions"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-static {p4, p6}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->create(Ljava/util/List;Ljava/util/List;)Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v6

    .line 51
    new-instance p4, Lcom/squareup/items/assignitemoptions/AssignOptionToItemProps;

    const-string p6, "engine"

    .line 56
    invoke-static {v6, p6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, p4

    move-object v2, p1

    move-object v3, p2

    move-object v4, p5

    move v5, p3

    .line 51
    invoke-direct/range {v1 .. v6}, Lcom/squareup/items/assignitemoptions/AssignOptionToItemProps;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)V

    invoke-virtual {p0, p4}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflowRunner;->setProps(Ljava/lang/Object;)V

    .line 58
    invoke-virtual {p0}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflowRunner;->ensureWorkflow()V

    return-void
.end method
