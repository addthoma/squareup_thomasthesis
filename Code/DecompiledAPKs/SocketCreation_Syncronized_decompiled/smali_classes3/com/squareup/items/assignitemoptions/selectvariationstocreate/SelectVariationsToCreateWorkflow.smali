.class public final Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "SelectVariationsToCreateWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState;",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectVariationsToCreateWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectVariationsToCreateWorkflow.kt\ncom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 4 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,225:1\n32#2,12:226\n1370#3:238\n1401#3,4:239\n1360#3:248\n1429#3,3:249\n149#4,5:243\n149#4,5:252\n149#4,5:257\n*E\n*S KotlinDebug\n*F\n+ 1 SelectVariationsToCreateWorkflow.kt\ncom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow\n*L\n49#1,12:226\n59#1:238\n59#1,4:239\n92#1:248\n92#1,3:249\n83#1,5:243\n124#1,5:252\n127#1,5:257\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0001:\u0001\u001cB\u000f\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u001a\u0010\r\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u00022\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0016JN\u0010\u0011\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u000e\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u00032\u0012\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0014H\u0016J\u0010\u0010\u0015\u001a\u00020\u00102\u0006\u0010\u0012\u001a\u00020\u0003H\u0016J0\u0010\u0016\u001a\u00020\u0017*\u00020\u00182\u0006\u0010\u000e\u001a\u00020\u00022\u001a\u0010\u0019\u001a\u0016\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u001b\u0018\u00010\u001aH\u0002R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState;",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "updateExistingWorkflow",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionWorkflow;",
        "(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionWorkflow;)V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "toSelectVariationsToCreateScreen",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$SelectVariations;",
        "actionSink",
        "Lcom/squareup/workflow/Sink;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final updateExistingWorkflow:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionWorkflow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "updateExistingWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;->updateExistingWorkflow:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionWorkflow;

    return-void
.end method

.method private final toSelectVariationsToCreateScreen(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$SelectVariations;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;Lcom/squareup/workflow/Sink;)Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$SelectVariations;",
            "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState;",
            "+",
            "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput;",
            ">;>;)",
            "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;"
        }
    .end annotation

    .line 207
    new-instance v11, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;

    .line 208
    invoke-virtual {p2}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->getItemName()Ljava/lang/String;

    move-result-object v1

    .line 209
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$SelectVariations;->getOptionValueToExtend()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move-object v2, v0

    .line 210
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$SelectVariations;->getOptionValueCombinationSelections()Ljava/util/List;

    move-result-object v3

    .line 211
    invoke-virtual {p2}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->isIncremental()Z

    move-result v4

    .line 212
    new-instance v0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$toSelectVariationsToCreateScreen$1;

    invoke-direct {v0, p3}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$toSelectVariationsToCreateScreen$1;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v5, v0

    check-cast v5, Lkotlin/jvm/functions/Function0;

    .line 213
    new-instance v0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$toSelectVariationsToCreateScreen$2;

    invoke-direct {v0, p1, p3}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$toSelectVariationsToCreateScreen$2;-><init>(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$SelectVariations;Lcom/squareup/workflow/Sink;)V

    move-object v6, v0

    check-cast v6, Lkotlin/jvm/functions/Function0;

    .line 214
    new-instance p1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$toSelectVariationsToCreateScreen$3;

    invoke-direct {p1, p3}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$toSelectVariationsToCreateScreen$3;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v7, p1

    check-cast v7, Lkotlin/jvm/functions/Function2;

    .line 218
    new-instance p1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$toSelectVariationsToCreateScreen$4;

    invoke-direct {p1, p3}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$toSelectVariationsToCreateScreen$4;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v8, p1

    check-cast v8, Lkotlin/jvm/functions/Function1;

    .line 221
    new-instance p1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$toSelectVariationsToCreateScreen$5;

    invoke-direct {p1, p3}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$toSelectVariationsToCreateScreen$5;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v9, p1

    check-cast v9, Lkotlin/jvm/functions/Function0;

    .line 222
    invoke-virtual {p2}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->getMaxNumberOfCombinationsToSelect()I

    move-result v10

    move-object v0, v11

    .line 207
    invoke-direct/range {v0 .. v10}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;I)V

    return-object v11
.end method


# virtual methods
.method public initialState(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState;
    .locals 11

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz p2, :cond_4

    .line 226
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p2}, Lokio/ByteString;->size()I

    move-result v3

    if-lez v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, v1

    :goto_1
    if-eqz p2, :cond_3

    .line 231
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    const-string v4, "Parcel.obtain()"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 232
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 233
    array-length v4, p2

    invoke-virtual {v3, p2, v2, v4}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 234
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 235
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v3, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p2

    if-nez p2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string v4, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {p2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 236
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    goto :goto_2

    :cond_3
    move-object p2, v1

    .line 237
    :goto_2
    check-cast p2, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState;

    goto :goto_3

    :cond_4
    move-object p2, v1

    :goto_3
    if-eqz p2, :cond_5

    return-object p2

    .line 54
    :cond_5
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object p2

    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->getOptionValueToExtend()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {v3}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getIdPair()Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    move-result-object v1

    :cond_6
    invoke-virtual {p2, v1}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->generateCombinations(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;)Ljava/util/List;

    move-result-object p2

    .line 57
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->getOptionValueToExtend()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v1

    const-string v3, "combinations"

    .line 58
    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, p2

    check-cast v3, Ljava/lang/Iterable;

    .line 238
    new-instance v4, Ljava/util/ArrayList;

    const/16 v5, 0xa

    invoke-static {v3, v5}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v4, Ljava/util/Collection;

    .line 240
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    const/4 v5, 0x0

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    add-int/lit8 v7, v5, 0x1

    if-gez v5, :cond_7

    .line 241
    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_7
    check-cast v6, Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;

    .line 60
    new-instance v8, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;

    .line 61
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v9

    .line 62
    move-object v10, v6

    check-cast v10, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;

    .line 61
    invoke-virtual {v9, v10}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->generateVariationNameWithOptionValueCombination(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "props.assignmentEngine.g\u2026             combination)"

    invoke-static {v9, v10}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->getMaxNumberOfCombinationsToSelect()I

    move-result v10

    if-ge v5, v10, :cond_8

    const/4 v5, 0x1

    goto :goto_5

    :cond_8
    const/4 v5, 0x0

    :goto_5
    const-string v10, "combination"

    .line 64
    invoke-static {v6, v10}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-direct {v8, v9, v5, v6}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;-><init>(Ljava/lang/String;ZLcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;)V

    .line 65
    invoke-interface {v4, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v5, v7

    goto :goto_4

    .line 242
    :cond_9
    check-cast v4, Ljava/util/List;

    .line 56
    new-instance v0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$SelectVariations;

    invoke-direct {v0, v1, v4}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$SelectVariations;-><init>(Lcom/squareup/cogs/itemoptions/ItemOptionValue;Ljava/util/List;)V

    .line 67
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->getMaxNumberOfCombinationsToSelect()I

    move-result p1

    if-le p2, p1, :cond_a

    .line 68
    new-instance p1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$WarnVariationNumberLimit;

    invoke-direct {p1, v0}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$WarnVariationNumberLimit;-><init>(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$SelectVariations;)V

    check-cast p1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState;

    goto :goto_6

    .line 70
    :cond_a
    move-object p1, v0

    check-cast p1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState;

    :goto_6
    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 37
    check-cast p1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;->initialState(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 37
    check-cast p1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;

    check-cast p2, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;->render(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;",
            "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState;",
            "-",
            "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v0

    .line 81
    instance-of v1, p2, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$SelectVariations;

    const-string v2, ""

    if-eqz v1, :cond_0

    .line 82
    check-cast p2, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$SelectVariations;

    invoke-direct {p0, p2, p1, v0}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;->toSelectVariationsToCreateScreen(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$SelectVariations;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;Lcom/squareup/workflow/Sink;)Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 244
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 245
    const-class p3, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 246
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 244
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 84
    sget-object p1, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_1

    .line 86
    :cond_0
    instance-of v1, p2, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$ExtendValueSelection;

    if-eqz v1, :cond_4

    .line 87
    move-object v0, p2

    check-cast v0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$ExtendValueSelection;

    invoke-virtual {v0}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$ExtendValueSelection;->getOptionValueToExtend()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 89
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->getItemOptionValuesInUseByOptionIds()Ljava/util/Map;

    move-result-object v1

    .line 90
    invoke-virtual {v0}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$ExtendValueSelection;->getOptionValueToExtend()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getOptionId()Ljava/lang/String;

    move-result-object v2

    .line 89
    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    if-eqz v1, :cond_2

    .line 92
    check-cast v1, Ljava/lang/Iterable;

    .line 248
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 249
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 250
    check-cast v3, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    const-string v4, "it"

    .line 92
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3}, Lcom/squareup/cogs/itemoptions/ItemOptionValueKt;->toItemOptionValue(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;)Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 251
    :cond_1
    check-cast v2, Ljava/util/List;

    .line 94
    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;->updateExistingWorkflow:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionWorkflow;

    move-object v4, v1

    check-cast v4, Lcom/squareup/workflow/Workflow;

    .line 95
    new-instance v5, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionProps;

    .line 96
    invoke-virtual {v0}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$ExtendValueSelection;->getOptionValueToExtend()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v0

    .line 95
    invoke-direct {v5, v0, v2}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionProps;-><init>(Lcom/squareup/cogs/itemoptions/ItemOptionValue;Ljava/util/List;)V

    const/4 v6, 0x0

    .line 99
    new-instance v0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$render$3;

    invoke-direct {v0, p2, p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$render$3;-><init>(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;)V

    move-object v7, v0

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v3, p3

    .line 93
    invoke-static/range {v3 .. v9}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto/16 :goto_1

    .line 91
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "optionValueToExtend must be in engine"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 87
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "We must already have an optionValueToExtend to choose a new value"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 119
    :cond_4
    instance-of p3, p2, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$WarnVariationNumberLimit;

    if-eqz p3, :cond_5

    .line 120
    new-instance p3, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/WarnVariationNumberLimitScreen;

    .line 121
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->getNumberOfExistingVariations()I

    move-result v1

    .line 122
    new-instance v3, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$render$warnVariationNumberLimitScreen$1;

    invoke-direct {v3, v0}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$render$warnVariationNumberLimitScreen$1;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    .line 120
    invoke-direct {p3, v1, v3}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/WarnVariationNumberLimitScreen;-><init>(ILkotlin/jvm/functions/Function0;)V

    check-cast p3, Lcom/squareup/workflow/legacy/V2Screen;

    .line 253
    new-instance v0, Lcom/squareup/workflow/legacy/Screen;

    .line 254
    const-class v1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/WarnVariationNumberLimitScreen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v1, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 255
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 253
    invoke-direct {v0, v1, p3, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 126
    check-cast p2, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$WarnVariationNumberLimit;

    invoke-virtual {p2}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$WarnVariationNumberLimit;->getSelectVariations()Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$SelectVariations;

    move-result-object p2

    const/4 p3, 0x0

    invoke-direct {p0, p2, p1, p3}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;->toSelectVariationsToCreateScreen(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$SelectVariations;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;Lcom/squareup/workflow/Sink;)Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 258
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 259
    const-class p3, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 260
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 258
    invoke-direct {p2, p3, p1, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 128
    sget-object p1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    const/4 p3, 0x2

    new-array p3, p3, [Lkotlin/Pair;

    const/4 v1, 0x0

    .line 129
    new-instance v2, Lkotlin/Pair;

    sget-object v3, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    invoke-direct {v2, v3, p2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v2, p3, v1

    const/4 p2, 0x1

    .line 130
    new-instance v1, Lkotlin/Pair;

    sget-object v2, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-direct {v1, v2, v0}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v1, p3, p2

    .line 128
    invoke-virtual {p1, p3}, Lcom/squareup/container/PosLayering$Companion;->partial([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    :goto_1
    return-object p1

    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 37
    check-cast p1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState;

    invoke-virtual {p0, p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;->snapshotState(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
