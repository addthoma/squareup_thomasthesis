.class public final Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$Action$ChooseNewExtendValue;
.super Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$Action;
.source "SelectVariationsToCreateWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ChooseNewExtendValue"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectVariationsToCreateWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectVariationsToCreateWorkflow.kt\ncom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$Action$ChooseNewExtendValue\n*L\n1#1,225:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u0004*\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005H\u0016\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$Action$ChooseNewExtendValue;",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$Action;",
        "()V",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState;",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$Action$ChooseNewExtendValue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 178
    new-instance v0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$Action$ChooseNewExtendValue;

    invoke-direct {v0}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$Action$ChooseNewExtendValue;-><init>()V

    sput-object v0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$Action$ChooseNewExtendValue;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$Action$ChooseNewExtendValue;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 178
    invoke-direct {p0, v0}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState;",
            "-",
            "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 180
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$SelectVariations;

    if-eqz v0, :cond_1

    .line 182
    sget-object v0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState;->Companion:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$Companion;

    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    check-cast v1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$SelectVariations;

    invoke-virtual {v0, v1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$Companion;->chooseNewExtendValue(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$SelectVariations;)Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$ExtendValueSelection;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.items.assignitemoptions.selectvariationstocreate.SelectVariationsToCreateState.SelectVariations"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 180
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Can only move to ChooseNewExtendValue from SelectVariations"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
