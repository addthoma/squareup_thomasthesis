.class public final Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow_Factory;
.super Ljava/lang/Object;
.source "CreateOptionForItemWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/editoption/EditOptionWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/editoption/EditOptionWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 21
    iput-object p2, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/editoption/EditOptionWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;",
            ">;)",
            "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow_Factory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/items/editoption/EditOptionWorkflow;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;)Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;
    .locals 1

    .line 37
    new-instance v0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;

    invoke-direct {v0, p0, p1}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;-><init>(Lcom/squareup/items/editoption/EditOptionWorkflow;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;
    .locals 2

    .line 26
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/items/editoption/EditOptionWorkflow;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;

    invoke-static {v0, v1}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow_Factory;->newInstance(Lcom/squareup/items/editoption/EditOptionWorkflow;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;)Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow_Factory;->get()Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;

    move-result-object v0

    return-object v0
.end method
