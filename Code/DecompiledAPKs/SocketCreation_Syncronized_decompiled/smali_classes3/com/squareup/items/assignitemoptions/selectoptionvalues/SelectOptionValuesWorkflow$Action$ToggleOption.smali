.class public final Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$ToggleOption;
.super Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action;
.source "SelectOptionValuesWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ToggleOption"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\t\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\n\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000b\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\u000c\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\r\u001a\u00020\u00052\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u00d6\u0003J\t\u0010\u0010\u001a\u00020\u0011H\u00d6\u0001J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\u0018\u0010\u0014\u001a\u00020\u0015*\u000e\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\u00180\u0016H\u0016R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0004\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$ToggleOption;",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action;",
        "optionValue",
        "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "isSelected",
        "",
        "(Lcom/squareup/cogs/itemoptions/ItemOptionValue;Z)V",
        "()Z",
        "getOptionValue",
        "()Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "component1",
        "component2",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesOutput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final isSelected:Z

.field private final optionValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;


# direct methods
.method public constructor <init>(Lcom/squareup/cogs/itemoptions/ItemOptionValue;Z)V
    .locals 1

    const-string v0, "optionValue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 333
    invoke-direct {p0, v0}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$ToggleOption;->optionValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    iput-boolean p2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$ToggleOption;->isSelected:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$ToggleOption;Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZILjava/lang/Object;)Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$ToggleOption;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$ToggleOption;->optionValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-boolean p2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$ToggleOption;->isSelected:Z

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$ToggleOption;->copy(Lcom/squareup/cogs/itemoptions/ItemOptionValue;Z)Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$ToggleOption;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;",
            "-",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 335
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$ToggleOption;->optionValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    iget-boolean v2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$ToggleOption;->isSelected:Z

    invoke-virtual {v0, v1, v2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;->updateValueSelection(Lcom/squareup/cogs/itemoptions/ItemOptionValue;Z)Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.items.assignitemoptions.selectoptionvalues.SelectOptionValuesState.SelectValues"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final component1()Lcom/squareup/cogs/itemoptions/ItemOptionValue;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$ToggleOption;->optionValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$ToggleOption;->isSelected:Z

    return v0
.end method

.method public final copy(Lcom/squareup/cogs/itemoptions/ItemOptionValue;Z)Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$ToggleOption;
    .locals 1

    const-string v0, "optionValue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$ToggleOption;

    invoke-direct {v0, p1, p2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$ToggleOption;-><init>(Lcom/squareup/cogs/itemoptions/ItemOptionValue;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$ToggleOption;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$ToggleOption;

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$ToggleOption;->optionValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$ToggleOption;->optionValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$ToggleOption;->isSelected:Z

    iget-boolean p1, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$ToggleOption;->isSelected:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getOptionValue()Lcom/squareup/cogs/itemoptions/ItemOptionValue;
    .locals 1

    .line 331
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$ToggleOption;->optionValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$ToggleOption;->optionValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$ToggleOption;->isSelected:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public final isSelected()Z
    .locals 1

    .line 332
    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$ToggleOption;->isSelected:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ToggleOption(optionValue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$ToggleOption;->optionValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isSelected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$ToggleOption;->isSelected:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
