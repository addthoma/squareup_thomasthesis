.class final Lcom/squareup/mortar/MortarScopes$completeOnExit$2;
.super Ljava/lang/Object;
.source "MortarScopes.kt"

# interfaces
.implements Lio/reactivex/CompletableOnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/mortar/MortarScopes;->completeOnExit(Lmortar/MortarScope;)Lio/reactivex/Completable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "emitter",
        "Lio/reactivex/CompletableEmitter;",
        "subscribe"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_completeOnExit:Lmortar/MortarScope;


# direct methods
.method constructor <init>(Lmortar/MortarScope;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/mortar/MortarScopes$completeOnExit$2;->$this_completeOnExit:Lmortar/MortarScope;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final subscribe(Lio/reactivex/CompletableEmitter;)V
    .locals 2

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iget-object v0, p0, Lcom/squareup/mortar/MortarScopes$completeOnExit$2;->$this_completeOnExit:Lmortar/MortarScope;

    invoke-virtual {v0}, Lmortar/MortarScope;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 23
    invoke-interface {p1}, Lio/reactivex/CompletableEmitter;->onComplete()V

    return-void

    .line 27
    :cond_0
    iget-object v0, p0, Lcom/squareup/mortar/MortarScopes$completeOnExit$2;->$this_completeOnExit:Lmortar/MortarScope;

    new-instance v1, Lcom/squareup/mortar/MortarScopes$completeOnExit$2$1;

    invoke-direct {v1, p1}, Lcom/squareup/mortar/MortarScopes$completeOnExit$2$1;-><init>(Lio/reactivex/CompletableEmitter;)V

    check-cast v1, Lmortar/Scoped;

    invoke-virtual {v0, v1}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
