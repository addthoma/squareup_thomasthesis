.class public final Lcom/squareup/mortar/AppContextWrapper;
.super Landroid/content/ContextWrapper;
.source "AppContextWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/mortar/AppContextWrapper$ExtraAppContextWrapper;
    }
.end annotation


# static fields
.field private static volatile appContextWrapper:Lcom/squareup/mortar/AppContextWrapper;


# instance fields
.field private final appServiceProvider:Lcom/squareup/mortar/AppServiceProvider;

.field private final extraAppContext:Landroid/content/ContextWrapper;


# direct methods
.method private constructor <init>(Landroid/app/Application;Lcom/squareup/mortar/AppServiceProvider;)V
    .locals 0

    .line 79
    invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    .line 80
    iput-object p2, p0, Lcom/squareup/mortar/AppContextWrapper;->appServiceProvider:Lcom/squareup/mortar/AppServiceProvider;

    .line 84
    new-instance p2, Lcom/squareup/mortar/AppContextWrapper$ExtraAppContextWrapper;

    invoke-direct {p2, p0, p1}, Lcom/squareup/mortar/AppContextWrapper$ExtraAppContextWrapper;-><init>(Lcom/squareup/mortar/AppContextWrapper;Landroid/app/Application;)V

    iput-object p2, p0, Lcom/squareup/mortar/AppContextWrapper;->extraAppContext:Landroid/content/ContextWrapper;

    return-void
.end method

.method public static appContext()Landroid/content/Context;
    .locals 2

    .line 48
    sget-object v0, Lcom/squareup/mortar/AppContextWrapper;->appContextWrapper:Lcom/squareup/mortar/AppContextWrapper;

    const-string v1, "appContextWrapper"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 49
    sget-object v0, Lcom/squareup/mortar/AppContextWrapper;->appContextWrapper:Lcom/squareup/mortar/AppContextWrapper;

    return-object v0
.end method

.method public static application()Landroid/app/Application;
    .locals 2

    .line 56
    sget-object v0, Lcom/squareup/mortar/AppContextWrapper;->appContextWrapper:Lcom/squareup/mortar/AppContextWrapper;

    const-string v1, "appContextWrapper"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 57
    sget-object v0, Lcom/squareup/mortar/AppContextWrapper;->appContextWrapper:Lcom/squareup/mortar/AppContextWrapper;

    invoke-virtual {v0}, Lcom/squareup/mortar/AppContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    return-object v0
.end method

.method public static install(Landroid/app/Application;Lcom/squareup/mortar/AppServiceProvider;)V
    .locals 1

    .line 72
    new-instance v0, Lcom/squareup/mortar/AppContextWrapper;

    invoke-direct {v0, p0, p1}, Lcom/squareup/mortar/AppContextWrapper;-><init>(Landroid/app/Application;Lcom/squareup/mortar/AppServiceProvider;)V

    sput-object v0, Lcom/squareup/mortar/AppContextWrapper;->appContextWrapper:Lcom/squareup/mortar/AppContextWrapper;

    return-void
.end method


# virtual methods
.method public getApplicationContext()Landroid/content/Context;
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/mortar/AppContextWrapper;->extraAppContext:Landroid/content/ContextWrapper;

    return-object v0
.end method

.method public getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/mortar/AppContextWrapper;->appServiceProvider:Lcom/squareup/mortar/AppServiceProvider;

    invoke-interface {v0, p1}, Lcom/squareup/mortar/AppServiceProvider;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 93
    :cond_0
    invoke-super {p0, p1}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
