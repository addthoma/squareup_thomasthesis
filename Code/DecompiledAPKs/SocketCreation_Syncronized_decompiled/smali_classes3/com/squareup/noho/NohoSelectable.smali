.class public final Lcom/squareup/noho/NohoSelectable;
.super Landroidx/constraintlayout/widget/ConstraintLayout;
.source "NohoSelectable.kt"

# interfaces
.implements Landroid/widget/Checkable;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoSelectable.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoSelectable.kt\ncom/squareup/noho/NohoSelectable\n+ 2 Delegates.kt\nkotlin/properties/Delegates\n+ 3 StyledAttributes.kt\ncom/squareup/util/StyledAttributesKt\n*L\n1#1,208:1\n33#2,3:209\n33#2,3:212\n37#3,6:215\n*E\n*S KotlinDebug\n*F\n+ 1 NohoSelectable.kt\ncom/squareup/noho/NohoSelectable\n*L\n66#1,3:209\n70#1,3:212\n173#1,6:215\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0010\u0015\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0006\u0018\u00002\u00020\u00012\u00020\u0002B%\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0008\u0008\u0003\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0008\u0010+\u001a\u00020\u000bH\u0016J\u0010\u0010,\u001a\u00020-2\u0006\u0010.\u001a\u00020\u0008H\u0014J\u0018\u0010/\u001a\u0002002\u0006\u00101\u001a\u00020\u00082\u0006\u00102\u001a\u00020\u0008H\u0014J\u0010\u00103\u001a\u0002002\u0006\u0010\u000c\u001a\u00020\u000bH\u0016J\u0008\u00104\u001a\u000200H\u0016J\u0008\u0010#\u001a\u000200H\u0002J\u0008\u00105\u001a\u000200H\u0002R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R&\u0010\r\u001a\u00020\u00082\u0006\u0010\u000c\u001a\u00020\u00088\u0006@FX\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\"\u0004\u0008\u0010\u0010\u0011R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R$\u0010\u0015\u001a\u00020\u00142\u0006\u0010\u000c\u001a\u00020\u00148F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0016\u0010\u0017\"\u0004\u0008\u0018\u0010\u0019R+\u0010\u001b\u001a\u00020\u00082\u0006\u0010\u001a\u001a\u00020\u00088F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u001e\u0010\u001f\u001a\u0004\u0008\u001c\u0010\u000f\"\u0004\u0008\u001d\u0010\u0011R\u000e\u0010 \u001a\u00020!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R(\u0010\u000c\u001a\u0004\u0018\u00010\u00142\u0008\u0010\u000c\u001a\u0004\u0018\u00010\u0014@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008$\u0010\u0017\"\u0004\u0008%\u0010\u0019R+\u0010&\u001a\u00020\u00082\u0006\u0010\u001a\u001a\u00020\u00088F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008)\u0010\u001f\u001a\u0004\u0008\'\u0010\u000f\"\u0004\u0008(\u0010\u0011R\u0010\u0010*\u001a\u0004\u0018\u00010!X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u00066"
    }
    d2 = {
        "Lcom/squareup/noho/NohoSelectable;",
        "Landroidx/constraintlayout/widget/ConstraintLayout;",
        "Landroid/widget/Checkable;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "checkedState",
        "",
        "value",
        "color",
        "getColor",
        "()I",
        "setColor",
        "(I)V",
        "colorView",
        "Landroid/widget/ImageView;",
        "",
        "label",
        "getLabel",
        "()Ljava/lang/String;",
        "setLabel",
        "(Ljava/lang/String;)V",
        "<set-?>",
        "labelAppearanceId",
        "getLabelAppearanceId",
        "setLabelAppearanceId",
        "labelAppearanceId$delegate",
        "Lkotlin/properties/ReadWriteProperty;",
        "labelView",
        "Lcom/squareup/noho/NohoLabel;",
        "textPadding",
        "updateConstraints",
        "getValue",
        "setValue",
        "valueAppearanceId",
        "getValueAppearanceId",
        "setValueAppearanceId",
        "valueAppearanceId$delegate",
        "valueView",
        "isChecked",
        "onCreateDrawableState",
        "",
        "extraSpace",
        "onMeasure",
        "",
        "widthMeasureSpec",
        "heightMeasureSpec",
        "setChecked",
        "toggle",
        "updateViews",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private checkedState:Z

.field private color:I

.field private colorView:Landroid/widget/ImageView;

.field private final labelAppearanceId$delegate:Lkotlin/properties/ReadWriteProperty;

.field private final labelView:Lcom/squareup/noho/NohoLabel;

.field private final textPadding:I

.field private updateConstraints:Z

.field private value:Ljava/lang/String;

.field private final valueAppearanceId$delegate:Lkotlin/properties/ReadWriteProperty;

.field private valueView:Lcom/squareup/noho/NohoLabel;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-class v0, Lcom/squareup/noho/NohoSelectable;

    const/4 v1, 0x2

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, "labelAppearanceId"

    const-string v5, "getLabelAppearanceId()I"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const-string/jumbo v3, "valueAppearanceId"

    const-string v4, "getValueAppearanceId()I"

    invoke-direct {v2, v0, v3, v4}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aput-object v0, v1, v2

    sput-object v1, Lcom/squareup/noho/NohoSelectable;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoSelectable;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoSelectable;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0, p1, p2, p3}, Landroidx/constraintlayout/widget/ConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    new-instance v0, Lcom/squareup/noho/NohoLabel;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    move-object v1, v0

    move-object v2, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/noho/NohoLabel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v0, p0, Lcom/squareup/noho/NohoSelectable;->labelView:Lcom/squareup/noho/NohoLabel;

    .line 39
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/noho/R$dimen;->noho_gap_16:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/squareup/noho/NohoSelectable;->textPadding:I

    .line 66
    sget-object v0, Lkotlin/properties/Delegates;->INSTANCE:Lkotlin/properties/Delegates;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 209
    new-instance v2, Lcom/squareup/noho/NohoSelectable$$special$$inlined$observable$1;

    invoke-direct {v2, v1, v1, p0}, Lcom/squareup/noho/NohoSelectable$$special$$inlined$observable$1;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/noho/NohoSelectable;)V

    check-cast v2, Lkotlin/properties/ReadWriteProperty;

    .line 211
    iput-object v2, p0, Lcom/squareup/noho/NohoSelectable;->labelAppearanceId$delegate:Lkotlin/properties/ReadWriteProperty;

    .line 70
    sget-object v2, Lkotlin/properties/Delegates;->INSTANCE:Lkotlin/properties/Delegates;

    .line 212
    new-instance v2, Lcom/squareup/noho/NohoSelectable$$special$$inlined$observable$2;

    invoke-direct {v2, v1, v1, p0}, Lcom/squareup/noho/NohoSelectable$$special$$inlined$observable$2;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/noho/NohoSelectable;)V

    check-cast v2, Lkotlin/properties/ReadWriteProperty;

    .line 214
    iput-object v2, p0, Lcom/squareup/noho/NohoSelectable;->valueAppearanceId$delegate:Lkotlin/properties/ReadWriteProperty;

    const/4 v1, 0x1

    .line 135
    iput-boolean v1, p0, Lcom/squareup/noho/NohoSelectable;->updateConstraints:Z

    .line 175
    sget-object v2, Lcom/squareup/noho/R$styleable;->NohoSelectable:[I

    const-string v3, "R.styleable.NohoSelectable"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 177
    sget v3, Lcom/squareup/noho/R$style;->Widget_Noho_Selectable:I

    .line 215
    invoke-virtual {p1, p2, v2, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    :try_start_0
    const-string p2, "a"

    .line 217
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 180
    sget p2, Lcom/squareup/noho/R$styleable;->NohoSelectable_android_minHeight:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoSelectable;->setMinHeight(I)V

    .line 181
    sget p2, Lcom/squareup/noho/R$styleable;->NohoSelectable_sqLabel:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    const-string p2, ""

    :goto_0
    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoSelectable;->setLabel(Ljava/lang/String;)V

    .line 182
    sget p2, Lcom/squareup/noho/R$styleable;->NohoSelectable_sqValue:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoSelectable;->setValue(Ljava/lang/String;)V

    .line 183
    sget p2, Lcom/squareup/noho/R$styleable;->NohoSelectable_sqColor:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p2

    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoSelectable;->setColor(I)V

    .line 184
    sget p2, Lcom/squareup/noho/R$styleable;->NohoSelectable_sqLabelAppearance:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p2

    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoSelectable;->setLabelAppearanceId(I)V

    .line 185
    sget p2, Lcom/squareup/noho/R$styleable;->NohoSelectable_sqValueAppearance:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p2

    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoSelectable;->setValueAppearanceId(I)V

    .line 186
    sget p2, Lcom/squareup/noho/R$styleable;->NohoSelectable_android_checked:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoSelectable;->setChecked(Z)V

    .line 187
    sget p2, Lcom/squareup/noho/R$styleable;->NohoSelectable_android_enabled:I

    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoSelectable;->setEnabled(Z)V

    .line 188
    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 219
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 189
    iget-object p1, p0, Lcom/squareup/noho/NohoSelectable;->labelView:Lcom/squareup/noho/NohoLabel;

    .line 190
    invoke-virtual {p0}, Lcom/squareup/noho/NohoSelectable;->getLabelAppearanceId()I

    move-result p2

    invoke-static {p1, p2}, Lcom/squareup/noho/NohoSelectableKt;->access$setAppearanceId(Lcom/squareup/noho/NohoLabel;I)V

    .line 191
    sget p2, Lcom/squareup/noho/R$id;->selectable_label:I

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoLabel;->setId(I)V

    const p2, 0x800013

    .line 192
    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoLabel;->setGravity(I)V

    .line 193
    sget-object p2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoLabel;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 194
    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoLabel;->setDuplicateParentStateEnabled(Z)V

    .line 196
    iget-object p1, p0, Lcom/squareup/noho/NohoSelectable;->labelView:Lcom/squareup/noho/NohoLabel;

    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoSelectable;->addView(Landroid/view/View;)V

    return-void

    :catchall_0
    move-exception p2

    .line 219
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw p2
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 31
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    .line 32
    sget p3, Lcom/squareup/noho/R$attr;->sqSelectableStyle:I

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/noho/NohoSelectable;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic access$getLabelView$p(Lcom/squareup/noho/NohoSelectable;)Lcom/squareup/noho/NohoLabel;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/noho/NohoSelectable;->labelView:Lcom/squareup/noho/NohoLabel;

    return-object p0
.end method

.method public static final synthetic access$getValueView$p(Lcom/squareup/noho/NohoSelectable;)Lcom/squareup/noho/NohoLabel;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/noho/NohoSelectable;->valueView:Lcom/squareup/noho/NohoLabel;

    return-object p0
.end method

.method public static final synthetic access$mergeDrawableStates$s2666181([I[I)[I
    .locals 0

    .line 27
    invoke-static {p0, p1}, Landroid/view/View;->mergeDrawableStates([I[I)[I

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$setValueView$p(Lcom/squareup/noho/NohoSelectable;Lcom/squareup/noho/NohoLabel;)V
    .locals 0

    .line 27
    iput-object p1, p0, Lcom/squareup/noho/NohoSelectable;->valueView:Lcom/squareup/noho/NohoLabel;

    return-void
.end method

.method private final updateConstraints()V
    .locals 12

    .line 139
    new-instance v6, Landroidx/constraintlayout/widget/ConstraintSet;

    invoke-direct {v6}, Landroidx/constraintlayout/widget/ConstraintSet;-><init>()V

    .line 143
    iget-object v0, p0, Lcom/squareup/noho/NohoSelectable;->valueView:Lcom/squareup/noho/NohoLabel;

    const/4 v7, 0x6

    const/4 v1, 0x7

    const/4 v8, 0x4

    const/4 v9, 0x3

    const/4 v10, 0x0

    if-eqz v0, :cond_0

    .line 144
    sget v0, Lcom/squareup/noho/R$id;->selectable_value:I

    invoke-virtual {v6, v0, v10}, Landroidx/constraintlayout/widget/ConstraintSet;->constrainHeight(II)V

    .line 145
    sget v0, Lcom/squareup/noho/R$id;->selectable_value:I

    const/4 v1, -0x2

    invoke-virtual {v6, v0, v1}, Landroidx/constraintlayout/widget/ConstraintSet;->constrainWidth(II)V

    .line 146
    sget v0, Lcom/squareup/noho/R$id;->selectable_value:I

    invoke-virtual {v6, v0, v9, v10, v9}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIII)V

    .line 147
    sget v0, Lcom/squareup/noho/R$id;->selectable_value:I

    invoke-virtual {v6, v0, v8, v10, v8}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIII)V

    .line 148
    sget v1, Lcom/squareup/noho/R$id;->selectable_value:I

    const/4 v2, 0x7

    const/4 v3, 0x0

    const/4 v4, 0x7

    iget v5, p0, Lcom/squareup/noho/NohoSelectable;->textPadding:I

    move-object v0, v6

    invoke-virtual/range {v0 .. v5}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 149
    sget v0, Lcom/squareup/noho/R$id;->selectable_value:I

    :goto_0
    move v7, v0

    const/4 v11, 0x6

    goto :goto_1

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/squareup/noho/NohoSelectable;->colorView:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 152
    sget v0, Lcom/squareup/noho/R$id;->selectable_color:I

    invoke-virtual {v6, v0, v10}, Landroidx/constraintlayout/widget/ConstraintSet;->constrainHeight(II)V

    .line 153
    sget v0, Lcom/squareup/noho/R$id;->selectable_color:I

    invoke-virtual {v6, v0, v10}, Landroidx/constraintlayout/widget/ConstraintSet;->constrainWidth(II)V

    .line 154
    sget v0, Lcom/squareup/noho/R$id;->selectable_color:I

    const-string v2, "1:1"

    invoke-virtual {v6, v0, v2}, Landroidx/constraintlayout/widget/ConstraintSet;->setDimensionRatio(ILjava/lang/String;)V

    .line 155
    sget v0, Lcom/squareup/noho/R$id;->selectable_color:I

    invoke-virtual {v6, v0, v9, v10, v9}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIII)V

    .line 156
    sget v0, Lcom/squareup/noho/R$id;->selectable_color:I

    invoke-virtual {v6, v0, v8, v10, v8}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIII)V

    .line 157
    sget v0, Lcom/squareup/noho/R$id;->selectable_color:I

    invoke-virtual {v6, v0, v1, v10, v1}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIII)V

    .line 158
    sget v0, Lcom/squareup/noho/R$id;->selectable_color:I

    goto :goto_0

    :cond_1
    const/4 v7, 0x0

    const/4 v11, 0x7

    .line 162
    :goto_1
    sget v0, Lcom/squareup/noho/R$id;->selectable_label:I

    invoke-virtual {v6, v0, v10}, Landroidx/constraintlayout/widget/ConstraintSet;->constrainHeight(II)V

    .line 163
    sget v0, Lcom/squareup/noho/R$id;->selectable_label:I

    invoke-virtual {v6, v0, v10}, Landroidx/constraintlayout/widget/ConstraintSet;->constrainWidth(II)V

    .line 164
    sget v0, Lcom/squareup/noho/R$id;->selectable_label:I

    invoke-virtual {v6, v0, v9, v10, v9}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIII)V

    .line 165
    sget v1, Lcom/squareup/noho/R$id;->selectable_label:I

    const/4 v2, 0x6

    const/4 v3, 0x0

    const/4 v4, 0x6

    iget v5, p0, Lcom/squareup/noho/NohoSelectable;->textPadding:I

    move-object v0, v6

    invoke-virtual/range {v0 .. v5}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 166
    sget v0, Lcom/squareup/noho/R$id;->selectable_label:I

    invoke-virtual {v6, v0, v8, v10, v8}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIII)V

    .line 167
    sget v1, Lcom/squareup/noho/R$id;->selectable_label:I

    const/4 v2, 0x7

    iget v5, p0, Lcom/squareup/noho/NohoSelectable;->textPadding:I

    move-object v0, v6

    move v3, v7

    move v4, v11

    invoke-virtual/range {v0 .. v5}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 169
    move-object v0, p0

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout;

    invoke-virtual {v6, v0}, Landroidx/constraintlayout/widget/ConstraintSet;->applyTo(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    return-void
.end method

.method private final updateViews()V
    .locals 13

    .line 95
    iget-object v0, p0, Lcom/squareup/noho/NohoSelectable;->valueView:Lcom/squareup/noho/NohoLabel;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 96
    :goto_0
    iget-object v3, p0, Lcom/squareup/noho/NohoSelectable;->value:Ljava/lang/String;

    check-cast v3, Ljava/lang/CharSequence;

    if-eqz v3, :cond_2

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-nez v3, :cond_1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v3, 0x1

    :goto_2
    xor-int/2addr v3, v2

    const/4 v4, 0x0

    if-nez v0, :cond_3

    if-eqz v3, :cond_3

    .line 98
    new-instance v12, Lcom/squareup/noho/NohoLabel;

    invoke-virtual {p0}, Lcom/squareup/noho/NohoSelectable;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string v5, "context"

    invoke-static {v6, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xe

    const/4 v11, 0x0

    move-object v5, v12

    invoke-direct/range {v5 .. v11}, Lcom/squareup/noho/NohoLabel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 99
    sget v5, Lcom/squareup/noho/R$id;->selectable_value:I

    invoke-virtual {v12, v5}, Lcom/squareup/noho/NohoLabel;->setId(I)V

    const v5, 0x800015

    .line 100
    invoke-virtual {v12, v5}, Lcom/squareup/noho/NohoLabel;->setGravity(I)V

    .line 101
    invoke-virtual {v12, v2}, Lcom/squareup/noho/NohoLabel;->setDuplicateParentStateEnabled(Z)V

    .line 102
    invoke-virtual {p0}, Lcom/squareup/noho/NohoSelectable;->getValueAppearanceId()I

    move-result v5

    invoke-static {v12, v5}, Lcom/squareup/noho/NohoSelectableKt;->access$setAppearanceId(Lcom/squareup/noho/NohoLabel;I)V

    .line 98
    iput-object v12, p0, Lcom/squareup/noho/NohoSelectable;->valueView:Lcom/squareup/noho/NohoLabel;

    .line 104
    iget-object v5, p0, Lcom/squareup/noho/NohoSelectable;->valueView:Lcom/squareup/noho/NohoLabel;

    check-cast v5, Landroid/view/View;

    invoke-virtual {p0, v5}, Lcom/squareup/noho/NohoSelectable;->addView(Landroid/view/View;)V

    goto :goto_3

    :cond_3
    if-eqz v0, :cond_4

    if-nez v3, :cond_4

    .line 106
    iget-object v5, p0, Lcom/squareup/noho/NohoSelectable;->valueView:Lcom/squareup/noho/NohoLabel;

    check-cast v5, Landroid/view/View;

    invoke-virtual {p0, v5}, Lcom/squareup/noho/NohoSelectable;->removeView(Landroid/view/View;)V

    .line 107
    move-object v5, v4

    check-cast v5, Lcom/squareup/noho/NohoLabel;

    iput-object v5, p0, Lcom/squareup/noho/NohoSelectable;->valueView:Lcom/squareup/noho/NohoLabel;

    .line 110
    :cond_4
    :goto_3
    iget-object v5, p0, Lcom/squareup/noho/NohoSelectable;->colorView:Landroid/widget/ImageView;

    if-eqz v5, :cond_5

    const/4 v5, 0x1

    goto :goto_4

    :cond_5
    const/4 v5, 0x0

    .line 111
    :goto_4
    iget v6, p0, Lcom/squareup/noho/NohoSelectable;->color:I

    if-eqz v6, :cond_6

    const/4 v1, 0x1

    :cond_6
    if-nez v5, :cond_7

    if-eqz v1, :cond_7

    .line 113
    new-instance v4, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/squareup/noho/NohoSelectable;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v4, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 114
    sget v6, Lcom/squareup/noho/R$id;->selectable_color:I

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setId(I)V

    .line 113
    iput-object v4, p0, Lcom/squareup/noho/NohoSelectable;->colorView:Landroid/widget/ImageView;

    .line 116
    iget-object v4, p0, Lcom/squareup/noho/NohoSelectable;->colorView:Landroid/widget/ImageView;

    check-cast v4, Landroid/view/View;

    invoke-virtual {p0, v4}, Lcom/squareup/noho/NohoSelectable;->addView(Landroid/view/View;)V

    goto :goto_5

    :cond_7
    if-eqz v5, :cond_8

    if-nez v1, :cond_8

    .line 118
    iget-object v6, p0, Lcom/squareup/noho/NohoSelectable;->colorView:Landroid/widget/ImageView;

    check-cast v6, Landroid/view/View;

    invoke-virtual {p0, v6}, Lcom/squareup/noho/NohoSelectable;->removeView(Landroid/view/View;)V

    .line 119
    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/squareup/noho/NohoSelectable;->colorView:Landroid/widget/ImageView;

    :cond_8
    :goto_5
    if-ne v0, v3, :cond_9

    if-eq v5, v1, :cond_a

    .line 123
    :cond_9
    iput-boolean v2, p0, Lcom/squareup/noho/NohoSelectable;->updateConstraints:Z

    :cond_a
    return-void
.end method


# virtual methods
.method public final getColor()I
    .locals 1

    .line 56
    iget v0, p0, Lcom/squareup/noho/NohoSelectable;->color:I

    return v0
.end method

.method public final getLabel()Ljava/lang/String;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/noho/NohoSelectable;->labelView:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoLabel;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getLabelAppearanceId()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/noho/NohoSelectable;->labelAppearanceId$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/noho/NohoSelectable;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadWriteProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    return v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/noho/NohoSelectable;->value:Ljava/lang/String;

    return-object v0
.end method

.method public final getValueAppearanceId()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/noho/NohoSelectable;->valueAppearanceId$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/noho/NohoSelectable;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadWriteProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    return v0
.end method

.method public isChecked()Z
    .locals 1

    .line 75
    iget-boolean v0, p0, Lcom/squareup/noho/NohoSelectable;->checkedState:Z

    return v0
.end method

.method protected onCreateDrawableState(I)[I
    .locals 1

    add-int/lit8 p1, p1, 0x1

    .line 87
    invoke-super {p0, p1}, Landroidx/constraintlayout/widget/ConstraintLayout;->onCreateDrawableState(I)[I

    move-result-object p1

    .line 88
    iget-boolean v0, p0, Lcom/squareup/noho/NohoSelectable;->checkedState:Z

    if-eqz v0, :cond_0

    .line 89
    invoke-static {}, Lcom/squareup/noho/NohoSelectableKt;->access$getSELECTABLE_CHECKED_STATES$p()[I

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/noho/NohoSelectable;->access$mergeDrawableStates$s2666181([I[I)[I

    :cond_0
    const-string v0, "result"

    .line 91
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method protected onMeasure(II)V
    .locals 1

    .line 128
    iget-boolean v0, p0, Lcom/squareup/noho/NohoSelectable;->updateConstraints:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 129
    iput-boolean v0, p0, Lcom/squareup/noho/NohoSelectable;->updateConstraints:Z

    .line 130
    invoke-direct {p0}, Lcom/squareup/noho/NohoSelectable;->updateConstraints()V

    .line 132
    :cond_0
    invoke-super {p0, p1, p2}, Landroidx/constraintlayout/widget/ConstraintLayout;->onMeasure(II)V

    return-void
.end method

.method public setChecked(Z)V
    .locals 0

    .line 82
    iput-boolean p1, p0, Lcom/squareup/noho/NohoSelectable;->checkedState:Z

    .line 83
    invoke-virtual {p0}, Lcom/squareup/noho/NohoSelectable;->refreshDrawableState()V

    return-void
.end method

.method public final setColor(I)V
    .locals 1

    .line 58
    iget v0, p0, Lcom/squareup/noho/NohoSelectable;->color:I

    if-eq p1, v0, :cond_0

    const/4 v0, 0x0

    .line 59
    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoSelectable;->setValue(Ljava/lang/String;)V

    .line 60
    iput p1, p0, Lcom/squareup/noho/NohoSelectable;->color:I

    .line 61
    invoke-direct {p0}, Lcom/squareup/noho/NohoSelectable;->updateViews()V

    .line 62
    iget-object v0, p0, Lcom/squareup/noho/NohoSelectable;->colorView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    :cond_0
    return-void
.end method

.method public final setLabel(Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lcom/squareup/noho/NohoSelectable;->labelView:Lcom/squareup/noho/NohoLabel;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setLabelAppearanceId(I)V
    .locals 3

    iget-object v0, p0, Lcom/squareup/noho/NohoSelectable;->labelAppearanceId$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/noho/NohoSelectable;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p0, v1, p1}, Lkotlin/properties/ReadWriteProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method public final setValue(Ljava/lang/String;)V
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/noho/NohoSelectable;->value:Ljava/lang/String;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 48
    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoSelectable;->setColor(I)V

    .line 49
    iput-object p1, p0, Lcom/squareup/noho/NohoSelectable;->value:Ljava/lang/String;

    .line 50
    invoke-direct {p0}, Lcom/squareup/noho/NohoSelectable;->updateViews()V

    .line 51
    iget-object v0, p0, Lcom/squareup/noho/NohoSelectable;->valueView:Lcom/squareup/noho/NohoLabel;

    if-eqz v0, :cond_0

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public final setValueAppearanceId(I)V
    .locals 3

    iget-object v0, p0, Lcom/squareup/noho/NohoSelectable;->valueAppearanceId$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/noho/NohoSelectable;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p0, v1, p1}, Lkotlin/properties/ReadWriteProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method public toggle()V
    .locals 1

    .line 78
    iget-boolean v0, p0, Lcom/squareup/noho/NohoSelectable;->checkedState:Z

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/noho/NohoSelectable;->checkedState:Z

    return-void
.end method
