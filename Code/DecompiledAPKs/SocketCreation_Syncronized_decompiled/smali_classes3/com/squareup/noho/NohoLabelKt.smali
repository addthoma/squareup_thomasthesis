.class public final Lcom/squareup/noho/NohoLabelKt;
.super Ljava/lang/Object;
.source "NohoLabel.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoLabel.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoLabel.kt\ncom/squareup/noho/NohoLabelKt\n+ 2 StyledAttributes.kt\ncom/squareup/util/StyledAttributesKt\n*L\n1#1,187:1\n37#2,6:188\n*E\n*S KotlinDebug\n*F\n+ 1 NohoLabel.kt\ncom/squareup/noho/NohoLabelKt\n*L\n178#1,6:188\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u001a.\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u00012\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0001H\u0002\u00a8\u0006\u0008"
    }
    d2 = {
        "defStyleFromLabelType",
        "",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "defStyleRes",
        "noho_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$defStyleFromLabelType(Landroid/content/Context;Landroid/util/AttributeSet;II)I
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/noho/NohoLabelKt;->defStyleFromLabelType(Landroid/content/Context;Landroid/util/AttributeSet;II)I

    move-result p0

    return p0
.end method

.method private static final defStyleFromLabelType(Landroid/content/Context;Landroid/util/AttributeSet;II)I
    .locals 2

    .line 180
    sget-object v0, Lcom/squareup/noho/R$styleable;->NohoLabel:[I

    const-string v1, "R.styleable.NohoLabel"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 181
    sget v1, Lcom/squareup/noho/R$attr;->sqLabelStyle:I

    .line 188
    invoke-virtual {p0, p1, v0, v1, p3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p0

    :try_start_0
    const-string p1, "a"

    .line 190
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 183
    sget p1, Lcom/squareup/noho/R$styleable;->NohoLabel_sqLabelType:I

    invoke-static {}, Lcom/squareup/noho/NohoLabel$Type;->values()[Lcom/squareup/noho/NohoLabel$Type;

    move-result-object p3

    check-cast p3, [Ljava/lang/Enum;

    const/4 v0, 0x0

    invoke-static {p0, p1, p3, v0}, Lcom/squareup/util/Views;->getEnum(Landroid/content/res/TypedArray;I[Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoLabel$Type;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 192
    invoke-virtual {p0}, Landroid/content/res/TypedArray;->recycle()V

    if-eqz p1, :cond_0

    .line 185
    invoke-virtual {p1}, Lcom/squareup/noho/NohoLabel$Type;->getStyleAttr()I

    move-result p2

    :cond_0
    return p2

    :catchall_0
    move-exception p1

    .line 192
    invoke-virtual {p0}, Landroid/content/res/TypedArray;->recycle()V

    throw p1
.end method
