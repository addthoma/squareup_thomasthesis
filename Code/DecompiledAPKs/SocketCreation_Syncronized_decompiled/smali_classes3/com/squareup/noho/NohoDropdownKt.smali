.class public final Lcom/squareup/noho/NohoDropdownKt;
.super Ljava/lang/Object;
.source "NohoDropdown.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0000\n\u0002\u0010\u0015\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000*L\u0010\u0002\u001a\u0004\u0008\u0000\u0010\u0003\" \u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u00042 \u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0004*@\u0010\u0008\u001a\u0004\u0008\u0000\u0010\u0003\"\u001a\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u00060\t2\u001a\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u00060\t\u00a8\u0006\u000b"
    }
    d2 = {
        "STATES_OPEN",
        "",
        "ConfigureViewBlock",
        "T",
        "Lkotlin/Function4;",
        "",
        "Landroid/view/View;",
        "",
        "CreateViewBlock",
        "Lkotlin/Function3;",
        "Landroid/content/Context;",
        "noho_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final STATES_OPEN:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [I

    .line 225
    sget v1, Lcom/squareup/noho/R$attr;->noho_state_open:I

    const/4 v2, 0x0

    aput v1, v0, v2

    sput-object v0, Lcom/squareup/noho/NohoDropdownKt;->STATES_OPEN:[I

    return-void
.end method

.method public static final synthetic access$getSTATES_OPEN$p()[I
    .locals 1

    .line 1
    sget-object v0, Lcom/squareup/noho/NohoDropdownKt;->STATES_OPEN:[I

    return-object v0
.end method
