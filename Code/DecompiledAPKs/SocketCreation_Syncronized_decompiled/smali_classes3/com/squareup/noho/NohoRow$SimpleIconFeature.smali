.class final Lcom/squareup/noho/NohoRow$SimpleIconFeature;
.super Lcom/squareup/noho/NohoRow$FeatureProperty;
.source "NohoRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "SimpleIconFeature"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/noho/NohoRow$FeatureProperty<",
        "Lcom/squareup/noho/NohoRow$Icon$SimpleIcon;",
        "Landroidx/appcompat/widget/AppCompatImageView;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoRow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoRow.kt\ncom/squareup/noho/NohoRow$SimpleIconFeature\n+ 2 StyledAttributes.kt\ncom/squareup/util/StyledAttributesKt\n*L\n1#1,1059:1\n60#2,6:1060\n60#2,6:1066\n*E\n*S KotlinDebug\n*F\n+ 1 NohoRow.kt\ncom/squareup/noho/NohoRow$SimpleIconFeature\n*L\n922#1,6:1060\n931#1,6:1066\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0008\u00c2\u0002\u0018\u00002\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u001a\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0001\u0010\u0008\u001a\u00020\tH\u0014J\u001a\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u00032\u0008\u0010\r\u001a\u0004\u0018\u00010\u0002H\u0014J\u0012\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\r\u001a\u0004\u0018\u00010\u0002H\u0014J\u001a\u0010\u0010\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u00032\u0008\u0008\u0001\u0010\u0008\u001a\u00020\tH\u0014\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/noho/NohoRow$SimpleIconFeature;",
        "Lcom/squareup/noho/NohoRow$FeatureProperty;",
        "Lcom/squareup/noho/NohoRow$Icon$SimpleIcon;",
        "Landroidx/appcompat/widget/AppCompatImageView;",
        "()V",
        "create",
        "row",
        "Lcom/squareup/noho/NohoRow;",
        "styleId",
        "",
        "doSetValue",
        "",
        "view",
        "value",
        "shouldShowFor",
        "",
        "updateStyleId",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/noho/NohoRow$SimpleIconFeature;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 912
    new-instance v0, Lcom/squareup/noho/NohoRow$SimpleIconFeature;

    invoke-direct {v0}, Lcom/squareup/noho/NohoRow$SimpleIconFeature;-><init>()V

    sput-object v0, Lcom/squareup/noho/NohoRow$SimpleIconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$SimpleIconFeature;

    return-void
.end method

.method private constructor <init>()V
    .locals 7

    .line 914
    sget v1, Lcom/squareup/noho/R$id;->simpleIcon:I

    .line 915
    sget v2, Lcom/squareup/noho/R$dimen;->noho_row_gap_size:I

    .line 916
    sget v3, Lcom/squareup/noho/R$attr;->sqIconStyle:I

    .line 917
    sget v4, Lcom/squareup/noho/R$styleable;->NohoRow_sqIconStyle:I

    .line 918
    sget v5, Lcom/squareup/noho/R$style;->Widget_Noho_Row_Icon:I

    const/4 v6, 0x0

    move-object v0, p0

    .line 913
    invoke-direct/range {v0 .. v6}, Lcom/squareup/noho/NohoRow$FeatureProperty;-><init>(IIIIILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic create(Lcom/squareup/noho/NohoRow;I)Landroid/view/View;
    .locals 0

    .line 912
    invoke-virtual {p0, p1, p2}, Lcom/squareup/noho/NohoRow$SimpleIconFeature;->create(Lcom/squareup/noho/NohoRow;I)Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    return-object p1
.end method

.method protected create(Lcom/squareup/noho/NohoRow;I)Landroidx/appcompat/widget/AppCompatImageView;
    .locals 4

    const-string v0, "row"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 931
    new-instance v0, Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {p1}, Lcom/squareup/noho/NohoRow;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroidx/appcompat/widget/AppCompatImageView;-><init>(Landroid/content/Context;)V

    .line 932
    invoke-virtual {p1}, Lcom/squareup/noho/NohoRow;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v1, "row.context"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, p2}, Lcom/squareup/util/StyledAttributesKt;->styleAsTheme(Landroid/content/Context;I)Landroid/content/res/Resources$Theme;

    move-result-object p1

    sget-object v1, Lcom/squareup/noho/R$styleable;->NohoRow_Icon:[I

    const-string v2, "R.styleable.NohoRow_Icon"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1066
    invoke-virtual {p1, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object p1

    :try_start_0
    const-string v1, "a"

    .line 1068
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 933
    check-cast v1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/AppCompatImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 934
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/AppCompatImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 935
    sget v1, Lcom/squareup/noho/R$styleable;->NohoRow_Icon_android_width:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 936
    sget v3, Lcom/squareup/noho/R$styleable;->NohoRow_Icon_android_height:I

    invoke-virtual {p1, v3, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    .line 937
    new-instance v3, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    invoke-direct {v3, v1, v2}, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;-><init>(II)V

    check-cast v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, v3}, Landroidx/appcompat/widget/AppCompatImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 938
    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1070
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 939
    sget-object p1, Lcom/squareup/noho/NohoRow$SimpleIconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$SimpleIconFeature;

    invoke-virtual {p1, v0, p2}, Lcom/squareup/noho/NohoRow$SimpleIconFeature;->updateStyleId(Landroidx/appcompat/widget/AppCompatImageView;I)V

    return-object v0

    :catchall_0
    move-exception p2

    .line 1070
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw p2
.end method

.method public bridge synthetic doSetValue(Landroid/view/View;Ljava/lang/Object;)V
    .locals 0

    .line 912
    check-cast p1, Landroidx/appcompat/widget/AppCompatImageView;

    check-cast p2, Lcom/squareup/noho/NohoRow$Icon$SimpleIcon;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/noho/NohoRow$SimpleIconFeature;->doSetValue(Landroidx/appcompat/widget/AppCompatImageView;Lcom/squareup/noho/NohoRow$Icon$SimpleIcon;)V

    return-void
.end method

.method protected doSetValue(Landroidx/appcompat/widget/AppCompatImageView;Lcom/squareup/noho/NohoRow$Icon$SimpleIcon;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 944
    invoke-virtual {p2}, Lcom/squareup/noho/NohoRow$Icon$SimpleIcon;->getId()I

    move-result p2

    invoke-virtual {p1, p2}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    return-void

    .line 943
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "doSetValue called for null simpleIcon!"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method protected shouldShowFor(Lcom/squareup/noho/NohoRow$Icon$SimpleIcon;)Z
    .locals 0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public bridge synthetic shouldShowFor(Ljava/lang/Object;)Z
    .locals 0

    .line 912
    check-cast p1, Lcom/squareup/noho/NohoRow$Icon$SimpleIcon;

    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoRow$SimpleIconFeature;->shouldShowFor(Lcom/squareup/noho/NohoRow$Icon$SimpleIcon;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic updateStyleId(Landroid/view/View;I)V
    .locals 0

    .line 912
    check-cast p1, Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/noho/NohoRow$SimpleIconFeature;->updateStyleId(Landroidx/appcompat/widget/AppCompatImageView;I)V

    return-void
.end method

.method protected updateStyleId(Landroidx/appcompat/widget/AppCompatImageView;I)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 922
    invoke-virtual {p1}, Landroidx/appcompat/widget/AppCompatImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "view.context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/squareup/util/StyledAttributesKt;->styleAsTheme(Landroid/content/Context;I)Landroid/content/res/Resources$Theme;

    move-result-object p2

    sget-object v0, Lcom/squareup/noho/R$styleable;->NohoRow_Icon:[I

    const-string v1, "R.styleable.NohoRow_Icon"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1060
    invoke-virtual {p2, v0}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object p2

    :try_start_0
    const-string v0, "a"

    .line 1062
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 923
    sget v0, Lcom/squareup/noho/R$styleable;->NohoRow_Icon_android_colorForeground:I

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 924
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    goto :goto_0

    :cond_0
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->DST:Landroid/graphics/PorterDuff$Mode;

    :goto_0
    invoke-virtual {p1, v1}, Landroidx/appcompat/widget/AppCompatImageView;->setSupportImageTintMode(Landroid/graphics/PorterDuff$Mode;)V

    .line 925
    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setSupportImageTintList(Landroid/content/res/ColorStateList;)V

    .line 926
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1064
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :catchall_0
    move-exception p1

    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    throw p1
.end method
