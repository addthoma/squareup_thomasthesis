.class public final Lcom/squareup/noho/NohoCheckableRow$checkableArrageable$1;
.super Ljava/lang/Object;
.source "NohoCheckableRow.kt"

# interfaces
.implements Lcom/squareup/noho/NohoRow$Arrangeable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/noho/NohoCheckableRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0006*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0004\u0010\u0005R\u0014\u0010\u0006\u001a\u00020\u0007X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0008R\u001a\u0010\t\u001a\u00020\u0003X\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\n\u0010\u0005\"\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\r"
    }
    d2 = {
        "com/squareup/noho/NohoCheckableRow$checkableArrageable$1",
        "Lcom/squareup/noho/NohoRow$Arrangeable;",
        "id",
        "",
        "getId",
        "()I",
        "isEnabled",
        "",
        "()Z",
        "margin",
        "getMargin",
        "setMargin",
        "(I)V",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Landroid/content/Context;

.field private final id:I

.field private final isEnabled:Z

.field private margin:I


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 53
    iput-object p1, p0, Lcom/squareup/noho/NohoCheckableRow$checkableArrageable$1;->$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    sget v0, Lcom/squareup/noho/R$id;->check:I

    iput v0, p0, Lcom/squareup/noho/NohoCheckableRow$checkableArrageable$1;->id:I

    const/4 v0, 0x1

    .line 55
    iput-boolean v0, p0, Lcom/squareup/noho/NohoCheckableRow$checkableArrageable$1;->isEnabled:Z

    .line 59
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/noho/R$dimen;->noho_row_gap_size:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoCheckableRow$checkableArrageable$1;->setMargin(I)V

    return-void
.end method


# virtual methods
.method public get(Lcom/squareup/noho/NohoRow;)Lcom/squareup/noho/NohoRow$Arrangeable;
    .locals 1

    const-string v0, "row"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-static {p0, p1}, Lcom/squareup/noho/NohoRow$Arrangeable$DefaultImpls;->get(Lcom/squareup/noho/NohoRow$Arrangeable;Lcom/squareup/noho/NohoRow;)Lcom/squareup/noho/NohoRow$Arrangeable;

    move-result-object p1

    return-object p1
.end method

.method public getId()I
    .locals 1

    .line 54
    iget v0, p0, Lcom/squareup/noho/NohoCheckableRow$checkableArrageable$1;->id:I

    return v0
.end method

.method public getMargin()I
    .locals 1

    .line 56
    iget v0, p0, Lcom/squareup/noho/NohoCheckableRow$checkableArrageable$1;->margin:I

    return v0
.end method

.method public isEnabled()Z
    .locals 1

    .line 55
    iget-boolean v0, p0, Lcom/squareup/noho/NohoCheckableRow$checkableArrageable$1;->isEnabled:Z

    return v0
.end method

.method public setMargin(I)V
    .locals 0

    .line 56
    iput p1, p0, Lcom/squareup/noho/NohoCheckableRow$checkableArrageable$1;->margin:I

    return-void
.end method
