.class public Lcom/squareup/noho/IconPlugin;
.super Ljava/lang/Object;
.source "NohoEditRowPlugins.kt"

# interfaces
.implements Lcom/squareup/noho/NohoEditRow$Plugin;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0016\u0018\u00002\u00020\u0001B5\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0001\u0010\u0008\u001a\u00020\u0007\u0012\u0008\u0008\u0003\u0010\t\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\nB+\u0008\u0007\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0008\u0008\u0001\u0010\r\u001a\u00020\u0007\u0012\u0008\u0008\u0003\u0010\t\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u000eJ\u0010\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0010\u0010\u001c\u001a\u00020\u00152\u0006\u0010\u001d\u001a\u00020\u001eH\u0016J\u0018\u0010\u001f\u001a\u00020\u00192\u0006\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020#H\u0016R\u0011\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0014\u0010\u0011\u001a\u00020\u00128VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0011\u0010\u0013R\u0011\u0010\u0014\u001a\u00020\u0015\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/noho/IconPlugin;",
        "Lcom/squareup/noho/NohoEditRow$Plugin;",
        "context",
        "Landroid/content/Context;",
        "side",
        "Lcom/squareup/noho/NohoEditRow$Side;",
        "drawableId",
        "",
        "marginBeforeId",
        "tintColor",
        "(Landroid/content/Context;Lcom/squareup/noho/NohoEditRow$Side;III)V",
        "drawable",
        "Landroid/graphics/drawable/Drawable;",
        "marginBefore",
        "(Lcom/squareup/noho/NohoEditRow$Side;Landroid/graphics/drawable/Drawable;II)V",
        "getDrawable",
        "()Landroid/graphics/drawable/Drawable;",
        "isClickable",
        "",
        "()Z",
        "size",
        "Lcom/squareup/noho/NohoEditRow$PluginSize;",
        "getSize",
        "()Lcom/squareup/noho/NohoEditRow$PluginSize;",
        "attach",
        "",
        "editText",
        "Lcom/squareup/noho/NohoEditRow;",
        "measure",
        "editRect",
        "Landroid/graphics/Rect;",
        "onDraw",
        "canvas",
        "Landroid/graphics/Canvas;",
        "drawingInfo",
        "Lcom/squareup/noho/NohoEditRow$DrawingInfo;",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final drawable:Landroid/graphics/drawable/Drawable;

.field private final size:Lcom/squareup/noho/NohoEditRow$PluginSize;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/squareup/noho/NohoEditRow$Side;III)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "side"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-static {p1, p3}, Lcom/squareup/util/Views;->getDrawableCompat(Landroid/content/Context;I)Landroidx/vectordrawable/graphics/drawable/VectorDrawableCompat;

    move-result-object p3

    check-cast p3, Landroid/graphics/drawable/Drawable;

    .line 56
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1, p4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p1

    .line 53
    invoke-direct {p0, p2, p3, p1, p5}, Lcom/squareup/noho/IconPlugin;-><init>(Lcom/squareup/noho/NohoEditRow$Side;Landroid/graphics/drawable/Drawable;II)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Lcom/squareup/noho/NohoEditRow$Side;IIIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 6

    and-int/lit8 p6, p6, 0x10

    if-eqz p6, :cond_0

    const/4 p5, 0x0

    const/4 v5, 0x0

    goto :goto_0

    :cond_0
    move v5, p5

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    .line 52
    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/IconPlugin;-><init>(Landroid/content/Context;Lcom/squareup/noho/NohoEditRow$Side;III)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/noho/NohoEditRow$Side;Landroid/graphics/drawable/Drawable;I)V
    .locals 7

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/squareup/noho/IconPlugin;-><init>(Lcom/squareup/noho/NohoEditRow$Side;Landroid/graphics/drawable/Drawable;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/noho/NohoEditRow$Side;Landroid/graphics/drawable/Drawable;II)V
    .locals 1

    const-string v0, "side"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "drawable"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/squareup/noho/IconPlugin;->drawable:Landroid/graphics/drawable/Drawable;

    if-eqz p4, :cond_0

    .line 62
    iget-object p2, p0, Lcom/squareup/noho/IconPlugin;->drawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p2, p4}, Landroid/graphics/drawable/Drawable;->setTint(I)V

    .line 66
    :cond_0
    new-instance p2, Lcom/squareup/noho/NohoEditRow$PluginSize;

    iget-object p4, p0, Lcom/squareup/noho/IconPlugin;->drawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result p4

    invoke-direct {p2, p1, p4, p3}, Lcom/squareup/noho/NohoEditRow$PluginSize;-><init>(Lcom/squareup/noho/NohoEditRow$Side;II)V

    iput-object p2, p0, Lcom/squareup/noho/IconPlugin;->size:Lcom/squareup/noho/NohoEditRow$PluginSize;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/noho/NohoEditRow$Side;Landroid/graphics/drawable/Drawable;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_0

    const/4 p4, 0x0

    .line 44
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/noho/IconPlugin;-><init>(Lcom/squareup/noho/NohoEditRow$Side;Landroid/graphics/drawable/Drawable;II)V

    return-void
.end method


# virtual methods
.method public attach(Lcom/squareup/noho/NohoEditRow;)V
    .locals 1

    const-string v0, "editText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public description(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 1

    const-string v0, "editDescription"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-static {p0, p1}, Lcom/squareup/noho/NohoEditRow$Plugin$DefaultImpls;->description(Lcom/squareup/noho/NohoEditRow$Plugin;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public detach(Lcom/squareup/noho/NohoEditRow;)V
    .locals 1

    const-string v0, "editText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-static {p0, p1}, Lcom/squareup/noho/NohoEditRow$Plugin$DefaultImpls;->detach(Lcom/squareup/noho/NohoEditRow$Plugin;Lcom/squareup/noho/NohoEditRow;)V

    return-void
.end method

.method public focusChanged()V
    .locals 0

    .line 40
    invoke-static {p0}, Lcom/squareup/noho/NohoEditRow$Plugin$DefaultImpls;->focusChanged(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    return-void
.end method

.method public final getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/noho/IconPlugin;->drawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getSize()Lcom/squareup/noho/NohoEditRow$PluginSize;
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/noho/IconPlugin;->size:Lcom/squareup/noho/NohoEditRow$PluginSize;

    return-object v0
.end method

.method public isClickable()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public measure(Landroid/graphics/Rect;)Lcom/squareup/noho/NohoEditRow$PluginSize;
    .locals 1

    const-string v0, "editRect"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    iget-object p1, p0, Lcom/squareup/noho/IconPlugin;->size:Lcom/squareup/noho/NohoEditRow$PluginSize;

    return-object p1
.end method

.method public onClick()Z
    .locals 1

    .line 40
    invoke-static {p0}, Lcom/squareup/noho/NohoEditRow$Plugin$DefaultImpls;->onClick(Lcom/squareup/noho/NohoEditRow$Plugin;)Z

    move-result v0

    return v0
.end method

.method public onDraw(Landroid/graphics/Canvas;Lcom/squareup/noho/NohoEditRow$DrawingInfo;)V
    .locals 1

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "drawingInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lcom/squareup/noho/IconPlugin;->drawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p2}, Lcom/squareup/noho/NohoEditRow$DrawingInfo;->getPluginRect()Landroid/graphics/Rect;

    move-result-object p2

    invoke-static {v0, p2}, Lcom/squareup/noho/CanvasExtensionsKt;->setBoundsIn(Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)V

    .line 75
    iget-object p2, p0, Lcom/squareup/noho/IconPlugin;->drawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method
