.class public final Lcom/squareup/noho/ContextExtensions;
.super Ljava/lang/Object;
.source "ContextExtensions.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nContextExtensions.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ContextExtensions.kt\ncom/squareup/noho/ContextExtensions\n+ 2 XmlResourceParsing.kt\ncom/squareup/android/xml/XmlResourceParsingKt\n+ 3 XmlResourceParsing.kt\ncom/squareup/android/xml/TagVisitor\n+ 4 Maps.kt\nkotlin/collections/MapsKt__MapsKt\n*L\n1#1,152:1\n22#2,3:153\n38#2:156\n26#2,2:163\n165#3,6:157\n347#4,7:165\n*E\n*S KotlinDebug\n*F\n+ 1 ContextExtensions.kt\ncom/squareup/noho/ContextExtensions\n*L\n69#1,3:153\n69#1:156\n69#1,2:163\n69#1,6:157\n138#1,7:165\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0000\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a>\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0001\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\n\u0010\u0012\u001a\u00060\u0013R\u00020\rH\u0002\u001a6\u0010\u0014\u001a\u00020\u00042\u0008\u0008\u0001\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\n\u0010\u0012\u001a\u00060\u0013R\u00020\rH\u0002\u001a,\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\n\u0010\u0012\u001a\u00060\u0013R\u00020\rH\u0002\u001a6\u0010\u0016\u001a\u00020\u00042\u0008\u0008\u0001\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\n\u0010\u0012\u001a\u00060\u0013R\u00020\rH\u0002\u001a$\u0010\u0017\u001a\u00020\u0004*\u00020\u00182\u0006\u0010\n\u001a\u00020\u000b2\u000e\u0008\u0002\u0010\u0019\u001a\u0008\u0018\u00010\u0013R\u00020\rH\u0007\u001a&\u0010\u0017\u001a\u00020\u0004*\u00020\r2\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\n\u0010\u0012\u001a\u00060\u0013R\u00020\r\"\"\u0010\u0000\u001a\u0016\u0012\u0004\u0012\u00020\u0002\u0012\u000c\u0012\n\u0012\u0006\u0008\u0001\u0012\u00020\u00040\u00030\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0002X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0002X\u0080T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "CONSTRUCTORS_MAP",
        "",
        "",
        "Ljava/lang/reflect/Constructor;",
        "Landroid/graphics/drawable/Drawable;",
        "DRAWABLE",
        "SHAPE",
        "loadDrawableTag",
        "classLoader",
        "Ljava/lang/ClassLoader;",
        "id",
        "",
        "resources",
        "Landroid/content/res/Resources;",
        "parser",
        "Lorg/xmlpull/v1/XmlPullParser;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "theme",
        "Landroid/content/res/Resources$Theme;",
        "loadSelectorTag",
        "loadShapeTag",
        "loadVectorTag",
        "getCustomDrawable",
        "Landroid/content/Context;",
        "themeOverride",
        "noho_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final CONSTRUCTORS_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Constructor<",
            "+",
            "Landroid/graphics/drawable/Drawable;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final DRAWABLE:Ljava/lang/String; = "drawable"

.field public static final SHAPE:Ljava/lang/String; = "shape"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 151
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    sput-object v0, Lcom/squareup/noho/ContextExtensions;->CONSTRUCTORS_MAP:Ljava/util/Map;

    return-void
.end method

.method public static final synthetic access$loadDrawableTag(Ljava/lang/ClassLoader;ILandroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;
    .locals 0

    .line 1
    invoke-static/range {p0 .. p5}, Lcom/squareup/noho/ContextExtensions;->loadDrawableTag(Ljava/lang/ClassLoader;ILandroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$loadSelectorTag(ILandroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3, p4}, Lcom/squareup/noho/ContextExtensions;->loadSelectorTag(ILandroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$loadShapeTag(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/noho/ContextExtensions;->loadShapeTag(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$loadVectorTag(ILandroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3, p4}, Lcom/squareup/noho/ContextExtensions;->loadVectorTag(ILandroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    return-object p0
.end method

.method public static final getCustomDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-static {p0, p1, v0, v1, v0}, Lcom/squareup/noho/ContextExtensions;->getCustomDrawable$default(Landroid/content/Context;ILandroid/content/res/Resources$Theme;ILjava/lang/Object;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    return-object p0
.end method

.method public static final getCustomDrawable(Landroid/content/Context;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;
    .locals 3

    const-string v0, "$this$getCustomDrawable"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "resources"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    const-string v2, "classLoader"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p2

    const-string/jumbo v2, "theme"

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    invoke-static {v0, v1, p1, p2}, Lcom/squareup/noho/ContextExtensions;->getCustomDrawable(Landroid/content/res/Resources;Ljava/lang/ClassLoader;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 45
    instance-of p2, p1, Lcom/squareup/noho/NeedsApplicationContext;

    if-eqz p2, :cond_1

    .line 46
    move-object p2, p1

    check-cast p2, Lcom/squareup/noho/NeedsApplicationContext;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    const-string/jumbo v0, "this@getCustomDrawable.applicationContext"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p0}, Lcom/squareup/noho/NeedsApplicationContext;->setApplicationContext(Landroid/content/Context;)V

    :cond_1
    return-object p1
.end method

.method public static final getCustomDrawable(Landroid/content/res/Resources;Ljava/lang/ClassLoader;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;
    .locals 9

    const-string v0, "$this$getCustomDrawable"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "classLoader"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "theme"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v0

    const-string v1, "getXml(resourceId)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    :try_start_0
    new-instance v1, Lcom/squareup/android/xml/TagVisitor;

    move-object v2, v0

    check-cast v2, Lorg/xmlpull/v1/XmlPullParser;

    move-object v3, v0

    check-cast v3, Landroid/util/AttributeSet;

    invoke-direct {v1, v2, v3}, Lcom/squareup/android/xml/TagVisitor;-><init>(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V

    .line 157
    invoke-virtual {v1}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v2

    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    if-nez v2, :cond_2

    .line 161
    invoke-virtual {v1}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/android/xml/XmlResourceParsingKt;->nextChildTag(Lorg/xmlpull/v1/XmlPullParser;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 71
    invoke-virtual {v1}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v2

    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    goto :goto_1

    :sswitch_0
    const-string p1, "selector"

    .line 72
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object p1

    invoke-virtual {v1}, Lcom/squareup/android/xml/TagVisitor;->getAttrs()Landroid/util/AttributeSet;

    move-result-object v1

    invoke-static {p2, p0, p1, v1, p3}, Lcom/squareup/noho/ContextExtensions;->access$loadSelectorTag(ILandroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    goto :goto_0

    :sswitch_1
    const-string p1, "shape"

    .line 73
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object p1

    invoke-virtual {v1}, Lcom/squareup/android/xml/TagVisitor;->getAttrs()Landroid/util/AttributeSet;

    move-result-object p2

    invoke-static {p0, p1, p2, p3}, Lcom/squareup/noho/ContextExtensions;->access$loadShapeTag(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    goto :goto_0

    :sswitch_2
    const-string/jumbo p1, "vector"

    .line 74
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object p1

    invoke-virtual {v1}, Lcom/squareup/android/xml/TagVisitor;->getAttrs()Landroid/util/AttributeSet;

    move-result-object v1

    invoke-static {p2, p0, p1, v1, p3}, Lcom/squareup/noho/ContextExtensions;->access$loadVectorTag(ILandroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    goto :goto_0

    :sswitch_3
    const-string v3, "drawable"

    .line 75
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v6

    invoke-virtual {v1}, Lcom/squareup/android/xml/TagVisitor;->getAttrs()Landroid/util/AttributeSet;

    move-result-object v7

    move-object v3, p1

    move v4, p2

    move-object v5, p0

    move-object v8, p3

    invoke-static/range {v3 .. v8}, Lcom/squareup/noho/ContextExtensions;->access$loadDrawableTag(Ljava/lang/ClassLoader;ILandroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 163
    :goto_0
    invoke-interface {v0}, Landroid/content/res/XmlResourceParser;->close()V

    return-object p0

    .line 76
    :cond_0
    :goto_1
    :try_start_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    .line 77
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "Cannot inflate drawable. Tag: "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object p2

    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getNamespace()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p2, 0x3a

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object p2

    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 76
    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    .line 161
    :cond_1
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "No root element found"

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    .line 159
    :cond_2
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "Not at beginning of visitRoot"

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p0

    .line 163
    invoke-interface {v0}, Landroid/content/res/XmlResourceParser;->close()V

    throw p0

    :sswitch_data_0
    .sparse-switch
        -0x31437f62 -> :sswitch_3
        -0x30e61ebd -> :sswitch_2
        0x6854fa1 -> :sswitch_1
        0x4705f3df -> :sswitch_0
    .end sparse-switch
.end method

.method public static synthetic getCustomDrawable$default(Landroid/content/Context;ILandroid/content/res/Resources$Theme;ILjava/lang/Object;)Landroid/graphics/drawable/Drawable;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 42
    check-cast p2, Landroid/content/res/Resources$Theme;

    :cond_0
    invoke-static {p0, p1, p2}, Lcom/squareup/noho/ContextExtensions;->getCustomDrawable(Landroid/content/Context;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    return-object p0
.end method

.method private static final loadDrawableTag(Ljava/lang/ClassLoader;ILandroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;
    .locals 3

    .line 136
    invoke-interface {p4}, Landroid/util/AttributeSet;->getClassAttribute()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 138
    sget-object p1, Lcom/squareup/noho/ContextExtensions;->CONSTRUCTORS_MAP:Ljava/util/Map;

    .line 165
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x0

    if-nez v1, :cond_1

    .line 139
    invoke-virtual {p0, v0}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object p0

    if-nez p0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 140
    :cond_0
    const-class v1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v1}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object p0

    new-array v1, v2, [Ljava/lang/Class;

    .line 141
    invoke-virtual {p0, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    const-string p0, "castedClazz.getConstructor()"

    invoke-static {v1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 168
    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    :cond_1
    check-cast v1, Ljava/lang/reflect/Constructor;

    new-array p0, v2, [Ljava/lang/Object;

    .line 143
    invoke-virtual {v1, p0}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    .line 144
    check-cast p0, Landroid/graphics/drawable/Drawable;

    .line 145
    invoke-virtual {p0, p2, p3, p4, p5}, Landroid/graphics/drawable/Drawable;->inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V

    return-object p0

    .line 137
    :cond_2
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "Missing class attribute. Drawable "

    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private static final loadSelectorTag(ILandroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;
    .locals 7

    .line 100
    invoke-interface {p3}, Landroid/util/AttributeSet;->getClassAttribute()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/squareup/noho/StateListVectorDrawable;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    new-instance p0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {p0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 103
    sget-object v1, Lcom/squareup/noho/StateListVectorDrawable;->Companion:Lcom/squareup/noho/StateListVectorDrawable$Companion;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-virtual/range {v1 .. v6}, Lcom/squareup/noho/StateListVectorDrawable$Companion;->inflate$noho_release(Landroid/graphics/drawable/StateListDrawable;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V

    .line 102
    check-cast p0, Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 113
    :cond_0
    invoke-static {p1, p0, p4}, Landroidx/core/content/res/ResourcesCompat;->getDrawable(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    if-nez p0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    const-string p1, "ResourcesCompat.getDrawa\u2026e(resources, id, theme)!!"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p0
.end method

.method private static final loadShapeTag(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;
    .locals 0

    .line 90
    invoke-static {p0, p1, p2, p3}, Landroid/graphics/drawable/ShapeDrawable;->createFromXmlInner(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    const-string p1, "ShapeDrawable.createFrom\u2026es, parser, attrs, theme)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method private static final loadVectorTag(ILandroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;
    .locals 0

    .line 124
    invoke-static {p1, p2, p3, p4}, Landroidx/vectordrawable/graphics/drawable/VectorDrawableCompat;->createFromXmlInner(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)Landroidx/vectordrawable/graphics/drawable/VectorDrawableCompat;

    move-result-object p0

    const-string p1, "VectorDrawableCompat.cre\u2026es, parser, attrs, theme)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Landroid/graphics/drawable/Drawable;

    return-object p0
.end method
