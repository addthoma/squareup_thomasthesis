.class public final Lcom/squareup/noho/NohoEditRow$PluginSize;
.super Ljava/lang/Object;
.source "NohoEditRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoEditRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PluginSize"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\t\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/noho/NohoEditRow$PluginSize;",
        "",
        "side",
        "Lcom/squareup/noho/NohoEditRow$Side;",
        "width",
        "",
        "marginOnSide",
        "(Lcom/squareup/noho/NohoEditRow$Side;II)V",
        "getMarginOnSide",
        "()I",
        "getSide",
        "()Lcom/squareup/noho/NohoEditRow$Side;",
        "getWidth",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final marginOnSide:I

.field private final side:Lcom/squareup/noho/NohoEditRow$Side;

.field private final width:I


# direct methods
.method public constructor <init>(Lcom/squareup/noho/NohoEditRow$Side;II)V
    .locals 1

    const-string v0, "side"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 501
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/noho/NohoEditRow$PluginSize;->side:Lcom/squareup/noho/NohoEditRow$Side;

    iput p2, p0, Lcom/squareup/noho/NohoEditRow$PluginSize;->width:I

    iput p3, p0, Lcom/squareup/noho/NohoEditRow$PluginSize;->marginOnSide:I

    return-void
.end method


# virtual methods
.method public final getMarginOnSide()I
    .locals 1

    .line 501
    iget v0, p0, Lcom/squareup/noho/NohoEditRow$PluginSize;->marginOnSide:I

    return v0
.end method

.method public final getSide()Lcom/squareup/noho/NohoEditRow$Side;
    .locals 1

    .line 501
    iget-object v0, p0, Lcom/squareup/noho/NohoEditRow$PluginSize;->side:Lcom/squareup/noho/NohoEditRow$Side;

    return-object v0
.end method

.method public final getWidth()I
    .locals 1

    .line 501
    iget v0, p0, Lcom/squareup/noho/NohoEditRow$PluginSize;->width:I

    return v0
.end method
