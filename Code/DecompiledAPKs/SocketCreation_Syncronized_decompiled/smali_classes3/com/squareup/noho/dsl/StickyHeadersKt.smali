.class public final Lcom/squareup/noho/dsl/StickyHeadersKt;
.super Ljava/lang/Object;
.source "StickyHeaders.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStickyHeaders.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StickyHeaders.kt\ncom/squareup/noho/dsl/StickyHeadersKt\n*L\n1#1,193:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u001a^\u0010\u0002\u001a\u00020\u0003\"\u0008\u0008\u0000\u0010\u0004*\u00020\u0005\"\u0008\u0008\u0001\u0010\u0006*\u00020\u0005\"\u0008\u0008\u0002\u0010\u0007*\u00020\u0008*\u0008\u0012\u0004\u0012\u0002H\u00040\t2+\u0008\u0004\u0010\n\u001a%\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u00070\u000c\u0012\u0004\u0012\u00020\u00030\u000b\u00a2\u0006\u0002\u0008\rH\u0087\u0008\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000*.\u0010\u000e\u001a\u0004\u0008\u0000\u0010\u000f\u001a\u0004\u0008\u0001\u0010\u0010\"\u000e\u0012\u0004\u0012\u0002H\u000f\u0012\u0004\u0012\u0002H\u00100\u000b2\u000e\u0012\u0004\u0012\u0002H\u000f\u0012\u0004\u0012\u0002H\u00100\u000b\u00a8\u0006\u0011"
    }
    d2 = {
        "RECYCLER_ITEM_LAYOUT",
        "Landroid/widget/LinearLayout$LayoutParams;",
        "stickyHeaders",
        "",
        "I",
        "",
        "H",
        "V",
        "Landroid/view/View;",
        "Lcom/squareup/cycler/Recycler$Config;",
        "block",
        "Lkotlin/Function1;",
        "Lcom/squareup/noho/dsl/StickyHeadersSpec;",
        "Lkotlin/ExtensionFunctionType;",
        "GroupByLambda",
        "ItemType",
        "HeaderDataType",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final RECYCLER_ITEM_LAYOUT:Landroid/widget/LinearLayout$LayoutParams;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 189
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    sput-object v0, Lcom/squareup/noho/dsl/StickyHeadersKt;->RECYCLER_ITEM_LAYOUT:Landroid/widget/LinearLayout$LayoutParams;

    return-void
.end method

.method public static final synthetic access$getRECYCLER_ITEM_LAYOUT$p()Landroid/widget/LinearLayout$LayoutParams;
    .locals 1

    .line 1
    sget-object v0, Lcom/squareup/noho/dsl/StickyHeadersKt;->RECYCLER_ITEM_LAYOUT:Landroid/widget/LinearLayout$LayoutParams;

    return-object v0
.end method

.method public static final stickyHeaders(Lcom/squareup/cycler/Recycler$Config;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation runtime Lcom/squareup/cycler/RecyclerApiMarker;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "H:",
            "Ljava/lang/Object;",
            "V:",
            "Landroid/view/View;",
            ">(",
            "Lcom/squareup/cycler/Recycler$Config<",
            "TI;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/noho/dsl/StickyHeadersSpec<",
            "TI;TH;TV;>;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$stickyHeaders"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    new-instance v0, Lcom/squareup/noho/dsl/StickyHeadersSpec;

    invoke-direct {v0}, Lcom/squareup/noho/dsl/StickyHeadersSpec;-><init>()V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/squareup/cycler/ExtensionSpec;

    invoke-virtual {p0, v0}, Lcom/squareup/cycler/Recycler$Config;->extension(Lcom/squareup/cycler/ExtensionSpec;)V

    return-void
.end method
