.class public final Lcom/squareup/noho/dsl/EdgesExtension$Provider;
.super Ljava/lang/Object;
.source "RecyclerEdges.kt"

# interfaces
.implements Lcom/squareup/noho/NohoEdgeDecoration$EdgeProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/dsl/EdgesExtension;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "Provider"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRecyclerEdges.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RecyclerEdges.kt\ncom/squareup/noho/dsl/EdgesExtension$Provider\n+ 2 RecyclerData.kt\ncom/squareup/cycler/RecyclerData\n+ 3 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 4 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 5 Recycler.kt\ncom/squareup/cycler/Recycler$RowSpec\n*L\n1#1,186:1\n67#2,2:187\n57#2,3:189\n70#2:192\n60#2:198\n71#2:199\n62#2,8:200\n63#2,10:213\n57#2,7:223\n67#2,2:230\n57#2,3:232\n70#2:235\n60#2:241\n71#2:242\n62#2,8:243\n63#2,10:256\n261#3:193\n262#3:196\n251#3:208\n252#3:211\n261#3:236\n262#3:239\n251#3:251\n252#3:254\n205#4,2:194\n205#4,2:209\n205#4,2:237\n205#4,2:252\n527#5:197\n527#5:212\n527#5:240\n527#5:255\n*E\n*S KotlinDebug\n*F\n+ 1 RecyclerEdges.kt\ncom/squareup/noho/dsl/EdgesExtension$Provider\n*L\n168#1,2:187\n168#1,3:189\n168#1:192\n168#1:198\n168#1:199\n168#1,8:200\n168#1,10:213\n172#1,7:223\n181#1,2:230\n181#1,3:232\n181#1:235\n181#1:241\n181#1:242\n181#1,8:243\n181#1,10:256\n168#1:193\n168#1:196\n168#1:208\n168#1:211\n181#1:236\n181#1:239\n181#1:251\n181#1:254\n168#1,2:194\n168#1,2:209\n181#1,2:237\n181#1,2:252\n168#1:197\n168#1:212\n181#1:240\n181#1:255\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0086\u0004\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0005H\u0016J\u001a\u0010\u000c\u001a\u00020\u00032\u0008\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010\r\u001a\u00020\u0003H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/noho/dsl/EdgesExtension$Provider;",
        "Lcom/squareup/noho/NohoEdgeDecoration$EdgeProvider;",
        "defaultEdges",
        "",
        "defaultPadding",
        "Landroid/graphics/Rect;",
        "(Lcom/squareup/noho/dsl/EdgesExtension;ILandroid/graphics/Rect;)V",
        "dividerPadding",
        "",
        "vh",
        "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
        "padding",
        "edges",
        "position",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final defaultEdges:I

.field private final defaultPadding:Landroid/graphics/Rect;

.field final synthetic this$0:Lcom/squareup/noho/dsl/EdgesExtension;


# direct methods
.method public constructor <init>(Lcom/squareup/noho/dsl/EdgesExtension;ILandroid/graphics/Rect;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/graphics/Rect;",
            ")V"
        }
    .end annotation

    const-string v0, "defaultPadding"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 163
    iput-object p1, p0, Lcom/squareup/noho/dsl/EdgesExtension$Provider;->this$0:Lcom/squareup/noho/dsl/EdgesExtension;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/squareup/noho/dsl/EdgesExtension$Provider;->defaultEdges:I

    iput-object p3, p0, Lcom/squareup/noho/dsl/EdgesExtension$Provider;->defaultPadding:Landroid/graphics/Rect;

    return-void
.end method


# virtual methods
.method public dividerPadding(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Landroid/graphics/Rect;)V
    .locals 5

    const-string/jumbo v0, "vh"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "padding"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 181
    iget-object v0, p0, Lcom/squareup/noho/dsl/EdgesExtension$Provider;->this$0:Lcom/squareup/noho/dsl/EdgesExtension;

    invoke-virtual {v0}, Lcom/squareup/noho/dsl/EdgesExtension;->getData()Lcom/squareup/cycler/RecyclerData;

    move-result-object v0

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result v1

    .line 233
    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getExtraItemIndex()I

    move-result v2

    const-string v3, "Collection contains no element matching the predicate."

    const/4 v4, 0x0

    if-ne v1, v2, :cond_3

    .line 234
    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getHasExtraItem()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getExtraItem()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 235
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getConfig()Lcom/squareup/cycler/Recycler$Config;

    move-result-object v0

    .line 236
    invoke-virtual {v0}, Lcom/squareup/cycler/Recycler$Config;->getExtraItemSpecs()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 237
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 236
    invoke-virtual {v2, v1}, Lcom/squareup/cycler/Recycler$RowSpec;->matches(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 240
    invoke-virtual {v2}, Lcom/squareup/cycler/Recycler$RowSpec;->getExtensions()Ljava/util/Map;

    move-result-object v0

    const-class v1, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;

    goto :goto_0

    .line 238
    :cond_2
    new-instance p1, Ljava/util/NoSuchElementException;

    invoke-direct {p1, v3}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 243
    :cond_3
    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v2

    invoke-interface {v2}, Lcom/squareup/cycler/DataSource;->getSize()I

    move-result v2

    if-gez v1, :cond_4

    goto :goto_0

    :cond_4
    if-le v2, v1, :cond_7

    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/squareup/cycler/DataSource;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 250
    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getConfig()Lcom/squareup/cycler/Recycler$Config;

    move-result-object v0

    .line 251
    invoke-virtual {v0}, Lcom/squareup/cycler/Recycler$Config;->getRowSpecs()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 252
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 251
    invoke-virtual {v2, v1}, Lcom/squareup/cycler/Recycler$RowSpec;->matches(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 255
    invoke-virtual {v2}, Lcom/squareup/cycler/Recycler$RowSpec;->getExtensions()Ljava/util/Map;

    move-result-object v0

    const-class v1, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;

    goto :goto_0

    .line 253
    :cond_6
    new-instance p1, Ljava/util/NoSuchElementException;

    invoke-direct {p1, v3}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_7
    :goto_0
    if-eqz v4, :cond_8

    .line 182
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const-string/jumbo v0, "vh.itemView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string/jumbo v0, "vh.itemView.context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/noho/dsl/EdgesExtension$Provider;->defaultPadding:Landroid/graphics/Rect;

    invoke-virtual {v4, p1, v0}, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;->padding$public_release(Landroid/content/Context;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object p1

    if-eqz p1, :cond_8

    goto :goto_1

    :cond_8
    iget-object p1, p0, Lcom/squareup/noho/dsl/EdgesExtension$Provider;->defaultPadding:Landroid/graphics/Rect;

    :goto_1
    invoke-virtual {p2, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    return-void
.end method

.method public edges(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)I
    .locals 4

    .line 168
    iget-object p1, p0, Lcom/squareup/noho/dsl/EdgesExtension$Provider;->this$0:Lcom/squareup/noho/dsl/EdgesExtension;

    invoke-virtual {p1}, Lcom/squareup/noho/dsl/EdgesExtension;->getData()Lcom/squareup/cycler/RecyclerData;

    move-result-object p1

    .line 190
    invoke-virtual {p1}, Lcom/squareup/cycler/RecyclerData;->getExtraItemIndex()I

    move-result v0

    const-string v1, "Collection contains no element matching the predicate."

    const/4 v2, 0x0

    if-ne p2, v0, :cond_3

    .line 191
    invoke-virtual {p1}, Lcom/squareup/cycler/RecyclerData;->getHasExtraItem()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/squareup/cycler/RecyclerData;->getExtraItem()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 192
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/cycler/RecyclerData;->getConfig()Lcom/squareup/cycler/Recycler$Config;

    move-result-object p1

    .line 193
    invoke-virtual {p1}, Lcom/squareup/cycler/Recycler$Config;->getExtraItemSpecs()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 194
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 193
    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$RowSpec;->matches(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 197
    invoke-virtual {v2}, Lcom/squareup/cycler/Recycler$RowSpec;->getExtensions()Ljava/util/Map;

    move-result-object p1

    const-class v0, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object v2, p1

    check-cast v2, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;

    goto :goto_0

    .line 195
    :cond_2
    new-instance p1, Ljava/util/NoSuchElementException;

    invoke-direct {p1, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 200
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cycler/DataSource;->getSize()I

    move-result v0

    if-gez p2, :cond_4

    goto :goto_0

    :cond_4
    if-le v0, p2, :cond_7

    invoke-virtual {p1}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/squareup/cycler/DataSource;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 207
    invoke-virtual {p1}, Lcom/squareup/cycler/RecyclerData;->getConfig()Lcom/squareup/cycler/Recycler$Config;

    move-result-object p1

    .line 208
    invoke-virtual {p1}, Lcom/squareup/cycler/Recycler$Config;->getRowSpecs()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 209
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_5
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 208
    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$RowSpec;->matches(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 212
    invoke-virtual {v2}, Lcom/squareup/cycler/Recycler$RowSpec;->getExtensions()Ljava/util/Map;

    move-result-object p1

    const-class v0, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object v2, p1

    check-cast v2, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;

    goto :goto_0

    .line 210
    :cond_6
    new-instance p1, Ljava/util/NoSuchElementException;

    invoke-direct {p1, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_7
    :goto_0
    if-nez v2, :cond_8

    .line 170
    iget p1, p0, Lcom/squareup/noho/dsl/EdgesExtension$Provider;->defaultEdges:I

    return p1

    .line 172
    :cond_8
    iget-object p1, p0, Lcom/squareup/noho/dsl/EdgesExtension$Provider;->this$0:Lcom/squareup/noho/dsl/EdgesExtension;

    invoke-virtual {p1}, Lcom/squareup/noho/dsl/EdgesExtension;->getData()Lcom/squareup/cycler/RecyclerData;

    move-result-object p1

    .line 224
    invoke-virtual {p1}, Lcom/squareup/cycler/RecyclerData;->getExtraItemIndex()I

    move-result v0

    const/4 v1, 0x0

    if-ne p2, v0, :cond_a

    .line 225
    invoke-virtual {p1}, Lcom/squareup/cycler/RecyclerData;->getHasExtraItem()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p1}, Lcom/squareup/cycler/RecyclerData;->getExtraItem()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_9

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 175
    :cond_9
    invoke-virtual {v2, p2, p1}, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;->edgesFor$public_release(ILjava/lang/Object;)I

    move-result v1

    goto :goto_1

    .line 228
    :cond_a
    invoke-virtual {p1}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cycler/DataSource;->getSize()I

    move-result v0

    if-gez p2, :cond_b

    goto :goto_1

    :cond_b
    if-le v0, p2, :cond_c

    invoke-virtual {p1}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object p1

    invoke-interface {p1, p2}, Lcom/squareup/cycler/DataSource;->get(I)Ljava/lang/Object;

    move-result-object p1

    .line 174
    invoke-virtual {v2, p2, p1}, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;->edgesFor$public_release(ILjava/lang/Object;)I

    move-result v1

    :cond_c
    :goto_1
    return v1
.end method
