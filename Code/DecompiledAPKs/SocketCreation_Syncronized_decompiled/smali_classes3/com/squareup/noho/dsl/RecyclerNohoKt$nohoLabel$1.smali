.class public final Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoLabel$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RecyclerNoho.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/noho/dsl/RecyclerNohoKt;->nohoLabel(Lcom/squareup/cycler/Recycler$Config;Lcom/squareup/noho/NohoLabel$Type;Lkotlin/jvm/functions/Function3;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/cycler/Recycler$CreatorContext;",
        "Lcom/squareup/noho/NohoLabel;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRecyclerNoho.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RecyclerNoho.kt\ncom/squareup/noho/dsl/RecyclerNohoKt$nohoLabel$1\n*L\n1#1,173:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\n\u0008\u0001\u0010\u0004\u0018\u0001*\u0002H\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/noho/NohoLabel;",
        "I",
        "",
        "S",
        "creatorContext",
        "Lcom/squareup/cycler/Recycler$CreatorContext;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $type:Lcom/squareup/noho/NohoLabel$Type;


# direct methods
.method public constructor <init>(Lcom/squareup/noho/NohoLabel$Type;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoLabel$1;->$type:Lcom/squareup/noho/NohoLabel$Type;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/cycler/Recycler$CreatorContext;)Lcom/squareup/noho/NohoLabel;
    .locals 8

    const-string v0, "creatorContext"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 168
    new-instance v0, Lcom/squareup/noho/NohoLabel;

    invoke-virtual {p1}, Lcom/squareup/cycler/Recycler$CreatorContext;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/noho/NohoLabel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iget-object p1, p0, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoLabel$1;->$type:Lcom/squareup/noho/NohoLabel$Type;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoLabel;->apply(Lcom/squareup/noho/NohoLabel$Type;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/squareup/cycler/Recycler$CreatorContext;

    invoke-virtual {p0, p1}, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoLabel$1;->invoke(Lcom/squareup/cycler/Recycler$CreatorContext;)Lcom/squareup/noho/NohoLabel;

    move-result-object p1

    return-object p1
.end method
