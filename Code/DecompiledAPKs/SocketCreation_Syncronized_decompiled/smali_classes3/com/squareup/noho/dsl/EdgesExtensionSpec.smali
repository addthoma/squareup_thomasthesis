.class public final Lcom/squareup/noho/dsl/EdgesExtensionSpec;
.super Ljava/lang/Object;
.source "RecyclerEdges.kt"

# interfaces
.implements Lcom/squareup/cycler/ExtensionSpec;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/cycler/ExtensionSpec<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0000\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u0008\u0012\u0004\u0012\u0002H\u00010\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0013H\u0016R\u001e\u0010\u0005\u001a\u00020\u00068\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\"\u0004\u0008\t\u0010\nR\u001e\u0010\u000b\u001a\u00020\u00068\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000c\u0010\u0008\"\u0004\u0008\r\u0010\nR \u0010\u000e\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u0014\n\u0000\u0012\u0004\u0008\u000f\u0010\u0004\u001a\u0004\u0008\u0010\u0010\u0008\"\u0004\u0008\u0011\u0010\n\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/noho/dsl/EdgesExtensionSpec;",
        "T",
        "",
        "Lcom/squareup/cycler/ExtensionSpec;",
        "()V",
        "defStyleAttr",
        "",
        "getDefStyleAttr",
        "()I",
        "setDefStyleAttr",
        "(I)V",
        "defStyleRes",
        "getDefStyleRes",
        "setDefStyleRes",
        "default",
        "default$annotations",
        "getDefault",
        "setDefault",
        "create",
        "Lcom/squareup/cycler/Extension;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private defStyleAttr:I

.field private defStyleRes:I

.field private default:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    sget v0, Lcom/squareup/noho/R$attr;->sqRecyclerEdgesStyle:I

    iput v0, p0, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->defStyleAttr:I

    .line 124
    sget v0, Lcom/squareup/noho/R$style;->Widget_Noho_RecyclerEdges:I

    iput v0, p0, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->defStyleRes:I

    return-void
.end method

.method public static synthetic default$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public create()Lcom/squareup/cycler/Extension;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/cycler/Extension<",
            "TT;>;"
        }
    .end annotation

    .line 127
    new-instance v0, Lcom/squareup/noho/dsl/EdgesExtension;

    invoke-direct {v0, p0}, Lcom/squareup/noho/dsl/EdgesExtension;-><init>(Lcom/squareup/noho/dsl/EdgesExtensionSpec;)V

    check-cast v0, Lcom/squareup/cycler/Extension;

    return-object v0
.end method

.method public final getDefStyleAttr()I
    .locals 1

    .line 123
    iget v0, p0, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->defStyleAttr:I

    return v0
.end method

.method public final getDefStyleRes()I
    .locals 1

    .line 124
    iget v0, p0, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->defStyleRes:I

    return v0
.end method

.method public final getDefault()I
    .locals 1

    .line 125
    iget v0, p0, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->default:I

    return v0
.end method

.method public final setDefStyleAttr(I)V
    .locals 0

    .line 123
    iput p1, p0, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->defStyleAttr:I

    return-void
.end method

.method public final setDefStyleRes(I)V
    .locals 0

    .line 124
    iput p1, p0, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->defStyleRes:I

    return-void
.end method

.method public final setDefault(I)V
    .locals 0

    .line 125
    iput p1, p0, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->default:I

    return-void
.end method
