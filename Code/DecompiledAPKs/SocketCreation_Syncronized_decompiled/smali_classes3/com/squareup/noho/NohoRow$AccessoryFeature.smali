.class final Lcom/squareup/noho/NohoRow$AccessoryFeature;
.super Lcom/squareup/noho/NohoRow$FeatureProperty;
.source "NohoRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "AccessoryFeature"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/noho/NohoRow$FeatureProperty<",
        "Lcom/squareup/noho/AccessoryType;",
        "Landroidx/appcompat/widget/AppCompatImageView;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoRow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoRow.kt\ncom/squareup/noho/NohoRow$AccessoryFeature\n+ 2 StyledAttributes.kt\ncom/squareup/util/StyledAttributesKt\n*L\n1#1,1059:1\n60#2,6:1060\n*E\n*S KotlinDebug\n*F\n+ 1 NohoRow.kt\ncom/squareup/noho/NohoRow$AccessoryFeature\n*L\n814#1,6:1060\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\u0008\u00c2\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u001a\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0001\u0010\u0008\u001a\u00020\tH\u0014J\u0018\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u00032\u0006\u0010\r\u001a\u00020\u0002H\u0014J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\r\u001a\u00020\u0002H\u0014\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/noho/NohoRow$AccessoryFeature;",
        "Lcom/squareup/noho/NohoRow$FeatureProperty;",
        "Lcom/squareup/noho/AccessoryType;",
        "Landroidx/appcompat/widget/AppCompatImageView;",
        "()V",
        "create",
        "row",
        "Lcom/squareup/noho/NohoRow;",
        "styleId",
        "",
        "doSetValue",
        "",
        "view",
        "value",
        "shouldShowFor",
        "",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/noho/NohoRow$AccessoryFeature;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 801
    new-instance v0, Lcom/squareup/noho/NohoRow$AccessoryFeature;

    invoke-direct {v0}, Lcom/squareup/noho/NohoRow$AccessoryFeature;-><init>()V

    sput-object v0, Lcom/squareup/noho/NohoRow$AccessoryFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$AccessoryFeature;

    return-void
.end method

.method private constructor <init>()V
    .locals 7

    .line 803
    sget v1, Lcom/squareup/noho/R$id;->accessory:I

    .line 804
    sget v2, Lcom/squareup/noho/R$dimen;->noho_row_accessory_gap_size:I

    .line 805
    sget v3, Lcom/squareup/noho/R$attr;->sqAccessoryStyle:I

    .line 806
    sget v4, Lcom/squareup/noho/R$styleable;->NohoRow_sqAccessoryStyle:I

    .line 807
    sget v5, Lcom/squareup/noho/R$style;->Widget_Noho_Row_Accessory:I

    .line 808
    sget-object v6, Lcom/squareup/noho/AccessoryType;->NONE:Lcom/squareup/noho/AccessoryType;

    move-object v0, p0

    .line 802
    invoke-direct/range {v0 .. v6}, Lcom/squareup/noho/NohoRow$FeatureProperty;-><init>(IIIIILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic create(Lcom/squareup/noho/NohoRow;I)Landroid/view/View;
    .locals 0

    .line 801
    invoke-virtual {p0, p1, p2}, Lcom/squareup/noho/NohoRow$AccessoryFeature;->create(Lcom/squareup/noho/NohoRow;I)Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    return-object p1
.end method

.method protected create(Lcom/squareup/noho/NohoRow;I)Landroidx/appcompat/widget/AppCompatImageView;
    .locals 3

    const-string v0, "row"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 814
    new-instance v0, Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {p1}, Lcom/squareup/noho/NohoRow;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroidx/appcompat/widget/AppCompatImageView;-><init>(Landroid/content/Context;)V

    .line 815
    invoke-virtual {p1}, Lcom/squareup/noho/NohoRow;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v1, "row.context"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, p2}, Lcom/squareup/util/StyledAttributesKt;->styleAsTheme(Landroid/content/Context;I)Landroid/content/res/Resources$Theme;

    move-result-object p1

    sget-object p2, Lcom/squareup/noho/R$styleable;->NohoRow_Accessory:[I

    const-string v1, "R.styleable.NohoRow_Accessory"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1060
    invoke-virtual {p1, p2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object p1

    :try_start_0
    const-string p2, "a"

    .line 1062
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 816
    sget-object p2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, p2}, Landroidx/appcompat/widget/AppCompatImageView;->setSupportImageTintMode(Landroid/graphics/PorterDuff$Mode;)V

    .line 817
    sget p2, Lcom/squareup/noho/R$styleable;->NohoRow_Accessory_android_tint:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroidx/appcompat/widget/AppCompatImageView;->setSupportImageTintList(Landroid/content/res/ColorStateList;)V

    const/4 p2, 0x1

    .line 818
    invoke-virtual {v0, p2}, Landroidx/appcompat/widget/AppCompatImageView;->setDuplicateParentStateEnabled(Z)V

    .line 819
    sget p2, Lcom/squareup/noho/R$styleable;->NohoRow_Accessory_android_width:I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    .line 820
    sget v2, Lcom/squareup/noho/R$styleable;->NohoRow_Accessory_android_height:I

    invoke-virtual {p1, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 821
    new-instance v2, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    invoke-direct {v2, p2, v1}, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;-><init>(II)V

    check-cast v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, v2}, Landroidx/appcompat/widget/AppCompatImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 822
    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1064
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-object v0

    :catchall_0
    move-exception p2

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw p2
.end method

.method public bridge synthetic doSetValue(Landroid/view/View;Ljava/lang/Object;)V
    .locals 0

    .line 801
    check-cast p1, Landroidx/appcompat/widget/AppCompatImageView;

    check-cast p2, Lcom/squareup/noho/AccessoryType;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/noho/NohoRow$AccessoryFeature;->doSetValue(Landroidx/appcompat/widget/AppCompatImageView;Lcom/squareup/noho/AccessoryType;)V

    return-void
.end method

.method protected doSetValue(Landroidx/appcompat/widget/AppCompatImageView;Lcom/squareup/noho/AccessoryType;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "value"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 827
    invoke-virtual {p2}, Lcom/squareup/noho/AccessoryType;->getDrawableId()I

    move-result p2

    invoke-virtual {p1, p2}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    return-void
.end method

.method protected shouldShowFor(Lcom/squareup/noho/AccessoryType;)Z
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 811
    sget-object v0, Lcom/squareup/noho/AccessoryType;->NONE:Lcom/squareup/noho/AccessoryType;

    if-eq p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public bridge synthetic shouldShowFor(Ljava/lang/Object;)Z
    .locals 0

    .line 801
    check-cast p1, Lcom/squareup/noho/AccessoryType;

    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoRow$AccessoryFeature;->shouldShowFor(Lcom/squareup/noho/AccessoryType;)Z

    move-result p1

    return p1
.end method
