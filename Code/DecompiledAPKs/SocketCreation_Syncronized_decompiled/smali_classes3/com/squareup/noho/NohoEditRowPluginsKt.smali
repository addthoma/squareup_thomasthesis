.class public final Lcom/squareup/noho/NohoEditRowPluginsKt;
.super Ljava/lang/Object;
.source "NohoEditRowPlugins.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoEditRowPlugins.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoEditRowPlugins.kt\ncom/squareup/noho/NohoEditRowPluginsKt\n*L\n1#1,261:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0002\u001a\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\u0004\u001a\u00020\u0005H\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "INVISIBLE_SIZE",
        "Lcom/squareup/noho/NohoEditRow$PluginSize;",
        "iconPluginForClear",
        "Lcom/squareup/noho/IconPlugin;",
        "context",
        "Landroid/content/Context;",
        "tintColor",
        "",
        "passwordIcon",
        "Landroid/graphics/drawable/Drawable;",
        "noho_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final INVISIBLE_SIZE:Lcom/squareup/noho/NohoEditRow$PluginSize;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 225
    new-instance v0, Lcom/squareup/noho/NohoEditRow$PluginSize;

    sget-object v1, Lcom/squareup/noho/NohoEditRow$Side;->END:Lcom/squareup/noho/NohoEditRow$Side;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/squareup/noho/NohoEditRow$PluginSize;-><init>(Lcom/squareup/noho/NohoEditRow$Side;II)V

    sput-object v0, Lcom/squareup/noho/NohoEditRowPluginsKt;->INVISIBLE_SIZE:Lcom/squareup/noho/NohoEditRow$PluginSize;

    return-void
.end method

.method public static final synthetic access$getINVISIBLE_SIZE$p()Lcom/squareup/noho/NohoEditRow$PluginSize;
    .locals 1

    .line 1
    sget-object v0, Lcom/squareup/noho/NohoEditRowPluginsKt;->INVISIBLE_SIZE:Lcom/squareup/noho/NohoEditRow$PluginSize;

    return-object v0
.end method

.method public static final synthetic access$iconPluginForClear(Landroid/content/Context;I)Lcom/squareup/noho/IconPlugin;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/noho/NohoEditRowPluginsKt;->iconPluginForClear(Landroid/content/Context;I)Lcom/squareup/noho/IconPlugin;

    move-result-object p0

    return-object p0
.end method

.method private static final iconPluginForClear(Landroid/content/Context;I)Lcom/squareup/noho/IconPlugin;
    .locals 9

    .line 231
    new-instance v8, Lcom/squareup/noho/IconPlugin;

    .line 232
    sget-object v2, Lcom/squareup/noho/NohoEditRow$Side;->END:Lcom/squareup/noho/NohoEditRow$Side;

    sget v3, Lcom/squareup/vectoricons/R$drawable;->icon_x:I

    .line 233
    sget v4, Lcom/squareup/noho/R$dimen;->noho_edit_default_margin:I

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object v0, v8

    move-object v1, p0

    .line 231
    invoke-direct/range {v0 .. v7}, Lcom/squareup/noho/IconPlugin;-><init>(Landroid/content/Context;Lcom/squareup/noho/NohoEditRow$Side;IIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    if-eqz p1, :cond_0

    .line 236
    invoke-virtual {v8}, Lcom/squareup/noho/IconPlugin;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {p0, p1}, Landroid/graphics/drawable/Drawable;->setTint(I)V

    .line 237
    invoke-virtual {v8}, Lcom/squareup/noho/IconPlugin;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p0

    sget-object p1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p0, p1}, Landroid/graphics/drawable/Drawable;->setTintMode(Landroid/graphics/PorterDuff$Mode;)V

    :cond_0
    return-object v8
.end method

.method public static final passwordIcon(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 2

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 217
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/noho/R$color;->noho_edit_view_password_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    const-string v1, "context.resources.getCol\u2026edit_view_password_color)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 218
    sget v1, Lcom/squareup/vectoricons/R$drawable;->ui_eye_24:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->getDrawableCompat(Landroid/content/Context;I)Landroidx/vectordrawable/graphics/drawable/VectorDrawableCompat;

    move-result-object p0

    .line 220
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p0, v1}, Landroidx/vectordrawable/graphics/drawable/VectorDrawableCompat;->setTintMode(Landroid/graphics/PorterDuff$Mode;)V

    .line 221
    invoke-virtual {p0, v0}, Landroidx/vectordrawable/graphics/drawable/VectorDrawableCompat;->setTintList(Landroid/content/res/ColorStateList;)V

    .line 219
    check-cast p0, Landroid/graphics/drawable/Drawable;

    return-object p0
.end method
