.class public final Lcom/squareup/noho/NohoEditRowKt;
.super Ljava/lang/Object;
.source "NohoEditRow.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\u0015\n\u0002\u0008\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"
    }
    d2 = {
        "DEFAULT_LABEL_WEIGHT",
        "",
        "ERROR_DRAWABLE_STATE",
        "",
        "VALIDATED_DRAWABLE_STATE",
        "noho_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final DEFAULT_LABEL_WEIGHT:F = 0.75f

.field private static final ERROR_DRAWABLE_STATE:[I

.field private static final VALIDATED_DRAWABLE_STATE:[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x1

    new-array v1, v0, [I

    .line 581
    sget v2, Lcom/squareup/noho/R$attr;->noho_state_error:I

    const/4 v3, 0x0

    aput v2, v1, v3

    sput-object v1, Lcom/squareup/noho/NohoEditRowKt;->ERROR_DRAWABLE_STATE:[I

    new-array v0, v0, [I

    .line 582
    sget v1, Lcom/squareup/noho/R$attr;->noho_state_validated:I

    aput v1, v0, v3

    sput-object v0, Lcom/squareup/noho/NohoEditRowKt;->VALIDATED_DRAWABLE_STATE:[I

    return-void
.end method

.method public static final synthetic access$getERROR_DRAWABLE_STATE$p()[I
    .locals 1

    .line 1
    sget-object v0, Lcom/squareup/noho/NohoEditRowKt;->ERROR_DRAWABLE_STATE:[I

    return-object v0
.end method

.method public static final synthetic access$getVALIDATED_DRAWABLE_STATE$p()[I
    .locals 1

    .line 1
    sget-object v0, Lcom/squareup/noho/NohoEditRowKt;->VALIDATED_DRAWABLE_STATE:[I

    return-object v0
.end method
