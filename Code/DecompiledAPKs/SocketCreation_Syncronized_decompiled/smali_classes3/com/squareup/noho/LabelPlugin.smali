.class public final Lcom/squareup/noho/LabelPlugin;
.super Ljava/lang/Object;
.source "NohoEditRowLabelPlugin.kt"

# interfaces
.implements Lcom/squareup/noho/NohoEditRow$Plugin;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0007\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u000c\u001a\u00020\rH\u0016J\u0008\u0010\u0018\u001a\u00020\u000fH\u0002J\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001cH\u0016J\u0018\u0010\u001d\u001a\u00020\u00172\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!H\u0016R\u000e\u0010\u000b\u001a\u00020\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u00020\u000f8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u0010R\u0010\u0010\u0011\u001a\u00020\u00078\u0002X\u0083\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/noho/LabelPlugin;",
        "Lcom/squareup/noho/NohoEditRow$Plugin;",
        "context",
        "Landroid/content/Context;",
        "text",
        "",
        "appearanceId",
        "",
        "maxWidthPercentage",
        "",
        "(Landroid/content/Context;Ljava/lang/String;IF)V",
        "displayText",
        "editText",
        "Lcom/squareup/noho/NohoEditRow;",
        "isClickable",
        "",
        "()Z",
        "marginWithText",
        "paint",
        "Landroid/text/TextPaint;",
        "getText",
        "()Ljava/lang/String;",
        "attach",
        "",
        "fillAvailableArea",
        "measure",
        "Lcom/squareup/noho/NohoEditRow$PluginSize;",
        "editRect",
        "Landroid/graphics/Rect;",
        "onDraw",
        "canvas",
        "Landroid/graphics/Canvas;",
        "drawingInfo",
        "Lcom/squareup/noho/NohoEditRow$DrawingInfo;",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private displayText:Ljava/lang/String;

.field private editText:Lcom/squareup/noho/NohoEditRow;

.field private final marginWithText:I

.field private final maxWidthPercentage:F

.field private final paint:Landroid/text/TextPaint;

.field private final text:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;IF)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "text"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/squareup/noho/LabelPlugin;->text:Ljava/lang/String;

    iput p4, p0, Lcom/squareup/noho/LabelPlugin;->maxWidthPercentage:F

    .line 41
    invoke-static {p1, p3}, Lcom/squareup/noho/CanvasExtensionsKt;->createTextPaintFromTextAppearance(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/noho/LabelPlugin;->paint:Landroid/text/TextPaint;

    .line 43
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/noho/R$dimen;->noho_edit_label_margin_with_text:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/noho/LabelPlugin;->marginWithText:I

    const-string p1, ""

    .line 44
    iput-object p1, p0, Lcom/squareup/noho/LabelPlugin;->displayText:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Ljava/lang/String;IFILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_0

    const/4 p4, 0x0

    .line 38
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/noho/LabelPlugin;-><init>(Landroid/content/Context;Ljava/lang/String;IF)V

    return-void
.end method

.method private final fillAvailableArea()Z
    .locals 2

    .line 46
    iget v0, p0, Lcom/squareup/noho/LabelPlugin;->maxWidthPercentage:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public attach(Lcom/squareup/noho/NohoEditRow;)V
    .locals 1

    const-string v0, "editText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    iput-object p1, p0, Lcom/squareup/noho/LabelPlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    return-void
.end method

.method public description(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 1

    const-string v0, "editDescription"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-static {p0, p1}, Lcom/squareup/noho/NohoEditRow$Plugin$DefaultImpls;->description(Lcom/squareup/noho/NohoEditRow$Plugin;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public detach(Lcom/squareup/noho/NohoEditRow;)V
    .locals 1

    const-string v0, "editText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-static {p0, p1}, Lcom/squareup/noho/NohoEditRow$Plugin$DefaultImpls;->detach(Lcom/squareup/noho/NohoEditRow$Plugin;Lcom/squareup/noho/NohoEditRow;)V

    return-void
.end method

.method public focusChanged()V
    .locals 0

    .line 34
    invoke-static {p0}, Lcom/squareup/noho/NohoEditRow$Plugin$DefaultImpls;->focusChanged(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    return-void
.end method

.method public final getText()Ljava/lang/String;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/noho/LabelPlugin;->text:Ljava/lang/String;

    return-object v0
.end method

.method public isClickable()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public measure(Landroid/graphics/Rect;)Lcom/squareup/noho/NohoEditRow$PluginSize;
    .locals 5

    const-string v0, "editRect"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result p1

    .line 57
    iget-object v0, p0, Lcom/squareup/noho/LabelPlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    const-string v1, "editText"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->getOriginalPaddingLeft()I

    move-result v0

    sub-int/2addr p1, v0

    .line 58
    iget-object v0, p0, Lcom/squareup/noho/LabelPlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->getOriginalPaddingRight()I

    move-result v0

    sub-int/2addr p1, v0

    .line 59
    iget v0, p0, Lcom/squareup/noho/LabelPlugin;->marginWithText:I

    sub-int/2addr p1, v0

    .line 61
    invoke-direct {p0}, Lcom/squareup/noho/LabelPlugin;->fillAvailableArea()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 62
    iget-object p1, p0, Lcom/squareup/noho/LabelPlugin;->text:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/noho/LabelPlugin;->displayText:Ljava/lang/String;

    .line 63
    iget-object v0, p0, Lcom/squareup/noho/LabelPlugin;->paint:Landroid/text/TextPaint;

    invoke-static {v0, p1}, Lcom/squareup/noho/CanvasExtensionsKt;->textWidth(Landroid/text/TextPaint;Ljava/lang/String;)I

    move-result p1

    goto :goto_1

    :cond_2
    int-to-float p1, p1

    .line 65
    iget v0, p0, Lcom/squareup/noho/LabelPlugin;->maxWidthPercentage:F

    mul-float p1, p1, v0

    float-to-int p1, p1

    .line 68
    iget-object v0, p0, Lcom/squareup/noho/LabelPlugin;->paint:Landroid/text/TextPaint;

    iget-object v2, p0, Lcom/squareup/noho/LabelPlugin;->text:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/squareup/noho/CanvasExtensionsKt;->textWidth(Landroid/text/TextPaint;Ljava/lang/String;)I

    move-result v0

    if-gt v0, p1, :cond_3

    iget-object v0, p0, Lcom/squareup/noho/LabelPlugin;->text:Ljava/lang/String;

    goto :goto_0

    .line 69
    :cond_3
    iget-object v0, p0, Lcom/squareup/noho/LabelPlugin;->text:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/squareup/noho/LabelPlugin;->paint:Landroid/text/TextPaint;

    int-to-float v3, p1

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v0, v2, v3, v4}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 68
    :goto_0
    iput-object v0, p0, Lcom/squareup/noho/LabelPlugin;->displayText:Ljava/lang/String;

    .line 72
    :goto_1
    new-instance v0, Lcom/squareup/noho/NohoEditRow$PluginSize;

    sget-object v2, Lcom/squareup/noho/NohoEditRow$Side;->START:Lcom/squareup/noho/NohoEditRow$Side;

    iget-object v3, p0, Lcom/squareup/noho/LabelPlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    if-nez v3, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v3}, Lcom/squareup/noho/NohoEditRow;->getOriginalPaddingLeft()I

    move-result v1

    invoke-direct {v0, v2, p1, v1}, Lcom/squareup/noho/NohoEditRow$PluginSize;-><init>(Lcom/squareup/noho/NohoEditRow$Side;II)V

    return-object v0
.end method

.method public onClick()Z
    .locals 1

    .line 34
    invoke-static {p0}, Lcom/squareup/noho/NohoEditRow$Plugin$DefaultImpls;->onClick(Lcom/squareup/noho/NohoEditRow$Plugin;)Z

    move-result v0

    return v0
.end method

.method public onDraw(Landroid/graphics/Canvas;Lcom/squareup/noho/NohoEditRow$DrawingInfo;)V
    .locals 3

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "drawingInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    invoke-virtual {p2}, Lcom/squareup/noho/NohoEditRow$DrawingInfo;->getPluginRect()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    .line 84
    invoke-virtual {p2}, Lcom/squareup/noho/NohoEditRow$DrawingInfo;->getPluginRect()Landroid/graphics/Rect;

    move-result-object p2

    invoke-virtual {p2}, Landroid/graphics/Rect;->exactCenterY()F

    move-result p2

    .line 85
    iget-object v1, p0, Lcom/squareup/noho/LabelPlugin;->displayText:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/noho/LabelPlugin;->paint:Landroid/text/TextPaint;

    invoke-static {p1, v1, v0, p2, v2}, Lcom/squareup/noho/CanvasExtensionsKt;->drawTextCenteredAt(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/text/TextPaint;)V

    return-void
.end method
