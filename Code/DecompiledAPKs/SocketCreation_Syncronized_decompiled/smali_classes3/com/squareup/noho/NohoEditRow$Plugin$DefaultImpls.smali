.class public final Lcom/squareup/noho/NohoEditRow$Plugin$DefaultImpls;
.super Ljava/lang/Object;
.source "NohoEditRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoEditRow$Plugin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static description(Lcom/squareup/noho/NohoEditRow$Plugin;Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 0

    const-string p0, "editDescription"

    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 492
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "If Plugin.isAccessible you need to implement description."

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method public static detach(Lcom/squareup/noho/NohoEditRow$Plugin;Lcom/squareup/noho/NohoEditRow;)V
    .locals 0

    const-string p0, "editText"

    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public static focusChanged(Lcom/squareup/noho/NohoEditRow$Plugin;)V
    .locals 0

    return-void
.end method

.method public static onClick(Lcom/squareup/noho/NohoEditRow$Plugin;)Z
    .locals 0

    const/4 p0, 0x1

    return p0
.end method
