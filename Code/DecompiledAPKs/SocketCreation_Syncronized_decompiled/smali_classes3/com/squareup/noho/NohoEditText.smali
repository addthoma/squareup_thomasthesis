.class public Lcom/squareup/noho/NohoEditText;
.super Lcom/squareup/widgets/SelectableEditText;
.source "NohoEditText.java"


# static fields
.field private static final DEFAULT_LABEL_WEIGHT:F = 75.0f

.field private static final SCROLL_MARGIN_BOTTOM_PX:I = 0x3

.field private static final TEXT_ALIGNMENT_LEFT:I = 0x0

.field private static final TEXT_ALIGNMENT_RIGHT:I = 0x1


# instance fields
.field private final canvasBounds:Landroid/graphics/Rect;

.field private final edgeController:Lcom/squareup/noho/NohoEdgeController;

.field private final fillAvailableArea:Z

.field private final labelColor:I

.field private labelLayoutWeight:F

.field private final labelPaddingRight:I

.field private final labelPaint:Landroid/text/TextPaint;

.field private labelText:Ljava/lang/String;

.field private final noteColor:I

.field private final notePaint:Landroid/text/TextPaint;

.field private noteText:Ljava/lang/String;

.field private showKeyboardDelayed:Z

.field private final textAlignment:I

.field private final textBounds:Landroid/graphics/Rect;

.field private final textLeftInset:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 94
    sget v0, Lcom/squareup/noho/R$attr;->nohoEditTextStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/noho/NohoEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .line 98
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/widgets/SelectableEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 71
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/squareup/noho/NohoEditText;->canvasBounds:Landroid/graphics/Rect;

    .line 72
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/squareup/noho/NohoEditText;->textBounds:Landroid/graphics/Rect;

    const/4 v0, 0x0

    .line 91
    iput-boolean v0, p0, Lcom/squareup/noho/NohoEditText;->showKeyboardDelayed:Z

    .line 100
    sget-object v1, Lcom/squareup/noho/R$styleable;->NohoEditText:[I

    invoke-virtual {p1, p2, v1, p3, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 102
    sget v1, Lcom/squareup/noho/R$styleable;->NohoEditText_sqLabelText:I

    invoke-virtual {p1, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/noho/NohoEditText;->labelText:Ljava/lang/String;

    .line 103
    sget v1, Lcom/squareup/noho/R$styleable;->NohoEditText_sqLabelLayoutWeight:I

    const/high16 v2, 0x42960000    # 75.0f

    .line 104
    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/squareup/noho/NohoEditText;->labelLayoutWeight:F

    .line 105
    sget v1, Lcom/squareup/noho/R$styleable;->NohoEditText_sqTextAlignment:I

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/squareup/noho/NohoEditText;->textAlignment:I

    .line 107
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/squareup/noho/R$color;->noho_text_body:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 108
    sget v3, Lcom/squareup/noho/R$styleable;->NohoEditText_sqLabelColor:I

    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/squareup/noho/NohoEditText;->labelColor:I

    .line 109
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/squareup/noho/R$color;->noho_text_hint:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 110
    sget v3, Lcom/squareup/noho/R$styleable;->NohoEditText_sqNoteColor:I

    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/squareup/noho/NohoEditText;->noteColor:I

    .line 112
    new-instance v1, Landroid/text/TextPaint;

    const/16 v3, 0x81

    invoke-direct {v1, v3}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v1, p0, Lcom/squareup/noho/NohoEditText;->labelPaint:Landroid/text/TextPaint;

    .line 113
    iget-object v1, p0, Lcom/squareup/noho/NohoEditText;->labelPaint:Landroid/text/TextPaint;

    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditText;->getTextSize()F

    move-result v4

    invoke-virtual {v1, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 114
    iget-object v1, p0, Lcom/squareup/noho/NohoEditText;->labelPaint:Landroid/text/TextPaint;

    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditText;->getContext()Landroid/content/Context;

    move-result-object v4

    sget-object v5, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {v4, v5}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 115
    iget-object v1, p0, Lcom/squareup/noho/NohoEditText;->labelPaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v4}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 116
    iget-object v1, p0, Lcom/squareup/noho/NohoEditText;->labelPaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v4}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 117
    iget-object v1, p0, Lcom/squareup/noho/NohoEditText;->labelPaint:Landroid/text/TextPaint;

    iget v4, p0, Lcom/squareup/noho/NohoEditText;->labelColor:I

    invoke-virtual {v1, v4}, Landroid/text/TextPaint;->setColor(I)V

    .line 118
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditText;->getPaddingLeft()I

    move-result v1

    iput v1, p0, Lcom/squareup/noho/NohoEditText;->textLeftInset:I

    .line 120
    sget v1, Lcom/squareup/noho/R$styleable;->NohoEditText_sqFillAvailableArea:I

    invoke-virtual {p1, v1, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/squareup/noho/NohoEditText;->fillAvailableArea:Z

    .line 121
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v4, Lcom/squareup/noho/R$dimen;->responsive_16_20_24:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/squareup/noho/NohoEditText;->labelPaddingRight:I

    .line 124
    iget v1, p0, Lcom/squareup/noho/NohoEditText;->textAlignment:I

    if-nez v1, :cond_0

    .line 125
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1, v3}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v1, p0, Lcom/squareup/noho/NohoEditText;->notePaint:Landroid/text/TextPaint;

    .line 126
    iget-object v1, p0, Lcom/squareup/noho/NohoEditText;->notePaint:Landroid/text/TextPaint;

    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditText;->getTextSize()F

    move-result v3

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 127
    iget-object v1, p0, Lcom/squareup/noho/NohoEditText;->notePaint:Landroid/text/TextPaint;

    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditText;->getContext()Landroid/content/Context;

    move-result-object v3

    sget-object v4, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {v3, v4}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 128
    iget-object v1, p0, Lcom/squareup/noho/NohoEditText;->notePaint:Landroid/text/TextPaint;

    sget-object v3, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 129
    iget-object v1, p0, Lcom/squareup/noho/NohoEditText;->notePaint:Landroid/text/TextPaint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 130
    iget-object v1, p0, Lcom/squareup/noho/NohoEditText;->notePaint:Landroid/text/TextPaint;

    iget v3, p0, Lcom/squareup/noho/NohoEditText;->noteColor:I

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setColor(I)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 132
    iput-object v1, p0, Lcom/squareup/noho/NohoEditText;->notePaint:Landroid/text/TextPaint;

    .line 135
    :goto_0
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 137
    new-instance p1, Lcom/squareup/noho/NohoEdgeController;

    invoke-direct {p1, p0, p2, p3, v0}, Lcom/squareup/noho/NohoEdgeController;-><init>(Landroid/view/View;Landroid/util/AttributeSet;II)V

    iput-object p1, p0, Lcom/squareup/noho/NohoEditText;->edgeController:Lcom/squareup/noho/NohoEdgeController;

    .line 139
    invoke-virtual {p0, v2}, Lcom/squareup/noho/NohoEditText;->setFocusableInTouchMode(Z)V

    return-void
.end method

.method private adjustRect(Landroid/graphics/Rect;)V
    .locals 1

    .line 219
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditText;->getHeight()I

    move-result v0

    add-int/lit8 v0, v0, 0x3

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    return-void
.end method

.method private calculateLabelWidth()I
    .locals 5

    .line 249
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 250
    iget-object v1, p0, Lcom/squareup/noho/NohoEditText;->labelPaint:Landroid/text/TextPaint;

    iget-object v2, p0, Lcom/squareup/noho/NohoEditText;->labelText:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4, v3, v0}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 251
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    return v0
.end method

.method private static drawTextCenteredVertically(Landroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/text/TextPaint;Ljava/lang/String;FFI)V
    .locals 1

    if-gtz p6, :cond_0

    return-void

    :cond_0
    int-to-float p6, p6

    .line 227
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {p3, p2, p6, v0}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object p3

    check-cast p3, Ljava/lang/String;

    const/4 p6, 0x0

    .line 228
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p2, p3, p6, v0, p1}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 229
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    move-result p1

    sub-float/2addr p5, p1

    invoke-virtual {p0, p3, p4, p5, p2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method private maybeShowKeyboard()V
    .locals 2

    .line 271
    iget-boolean v0, p0, Lcom/squareup/noho/NohoEditText;->showKeyboardDelayed:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditText;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 272
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/squareup/noho/NohoEditText$1;

    invoke-direct {v1, p0}, Lcom/squareup/noho/NohoEditText$1;-><init>(Lcom/squareup/noho/NohoEditText;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    const/4 v0, 0x0

    .line 281
    iput-boolean v0, p0, Lcom/squareup/noho/NohoEditText;->showKeyboardDelayed:Z

    :cond_1
    return-void
.end method

.method private setUpGravityAndPaddingAccordingToLabelText()V
    .locals 4

    .line 233
    iget-object v0, p0, Lcom/squareup/noho/NohoEditText;->labelText:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/noho/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 235
    iget v0, p0, Lcom/squareup/noho/NohoEditText;->textAlignment:I

    if-nez v0, :cond_0

    const v0, 0x800003

    goto :goto_0

    :cond_0
    const v0, 0x800005

    :goto_0
    or-int/lit8 v0, v0, 0x10

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoEditText;->setGravity(I)V

    .line 236
    iget-boolean v0, p0, Lcom/squareup/noho/NohoEditText;->fillAvailableArea:Z

    if-eqz v0, :cond_1

    .line 237
    invoke-direct {p0}, Lcom/squareup/noho/NohoEditText;->calculateLabelWidth()I

    move-result v0

    iget v1, p0, Lcom/squareup/noho/NohoEditText;->textLeftInset:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    goto :goto_1

    .line 238
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditText;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/squareup/noho/NohoEditText;->labelLayoutWeight:F

    mul-float v0, v0, v1

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    float-to-int v0, v0

    .line 239
    :goto_1
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditText;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditText;->getPaddingRight()I

    move-result v2

    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditText;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/squareup/noho/NohoEditText;->setPadding(IIII)V

    goto :goto_2

    :cond_2
    const v0, 0x800013

    .line 242
    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoEditText;->setGravity(I)V

    .line 244
    iget v0, p0, Lcom/squareup/noho/NohoEditText;->textLeftInset:I

    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditText;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditText;->getPaddingRight()I

    move-result v2

    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditText;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/squareup/noho/NohoEditText;->setPadding(IIII)V

    :goto_2
    return-void
.end method


# virtual methods
.method public focusAndShowKeyboard()V
    .locals 1

    .line 260
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditText;->requestFocus()Z

    const/4 v0, 0x1

    .line 261
    iput-boolean v0, p0, Lcom/squareup/noho/NohoEditText;->showKeyboardDelayed:Z

    .line 262
    invoke-direct {p0}, Lcom/squareup/noho/NohoEditText;->maybeShowKeyboard()V

    return-void
.end method

.method public getFocusedRect(Landroid/graphics/Rect;)V
    .locals 0

    .line 213
    invoke-super {p0, p1}, Lcom/squareup/widgets/SelectableEditText;->getFocusedRect(Landroid/graphics/Rect;)V

    .line 214
    invoke-direct {p0, p1}, Lcom/squareup/noho/NohoEditText;->adjustRect(Landroid/graphics/Rect;)V

    return-void
.end method

.method public getLabel()Ljava/lang/CharSequence;
    .locals 1

    .line 153
    iget-object v0, p0, Lcom/squareup/noho/NohoEditText;->labelText:Ljava/lang/String;

    return-object v0
.end method

.method public getNote()Ljava/lang/CharSequence;
    .locals 1

    .line 164
    iget-object v0, p0, Lcom/squareup/noho/NohoEditText;->noteText:Ljava/lang/String;

    return-object v0
.end method

.method public getOffsetForPosition(FF)I
    .locals 0

    .line 203
    invoke-super {p0, p1, p2}, Lcom/squareup/widgets/SelectableEditText;->getOffsetForPosition(FF)I

    move-result p1

    const/4 p2, -0x1

    if-eq p1, p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 10

    .line 172
    iget-object v0, p0, Lcom/squareup/noho/NohoEditText;->edgeController:Lcom/squareup/noho/NohoEdgeController;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEdgeController;->onDraw(Landroid/graphics/Canvas;)V

    .line 174
    iget-object v0, p0, Lcom/squareup/noho/NohoEditText;->labelText:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/noho/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 175
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/noho/R$dimen;->responsive_16_20_24:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 176
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditText;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/squareup/noho/NohoEditText;->labelLayoutWeight:F

    mul-float v1, v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    float-to-int v1, v1

    .line 177
    iget v2, p0, Lcom/squareup/noho/NohoEditText;->textLeftInset:I

    sub-int/2addr v1, v2

    sub-int v8, v1, v0

    .line 178
    iget-object v0, p0, Lcom/squareup/noho/NohoEditText;->canvasBounds:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->getClipBounds(Landroid/graphics/Rect;)Z

    .line 179
    iget-object v3, p0, Lcom/squareup/noho/NohoEditText;->textBounds:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/squareup/noho/NohoEditText;->labelPaint:Landroid/text/TextPaint;

    iget-object v5, p0, Lcom/squareup/noho/NohoEditText;->labelText:Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/noho/NohoEditText;->canvasBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget v1, p0, Lcom/squareup/noho/NohoEditText;->textLeftInset:I

    add-int/2addr v0, v1

    int-to-float v6, v0

    .line 180
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditText;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v7, v0

    move-object v2, p1

    .line 179
    invoke-static/range {v2 .. v8}, Lcom/squareup/noho/NohoEditText;->drawTextCenteredVertically(Landroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/text/TextPaint;Ljava/lang/String;FFI)V

    .line 183
    :cond_0
    iget-object v0, p0, Lcom/squareup/noho/NohoEditText;->notePaint:Landroid/text/TextPaint;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/noho/NohoEditText;->noteText:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/noho/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 184
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/noho/R$dimen;->noho_gap_12:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 185
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 186
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditText;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    iget-object v5, p0, Lcom/squareup/noho/NohoEditText;->textBounds:Landroid/graphics/Rect;

    invoke-virtual {v2, v1, v3, v4, v5}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 187
    iget-object v1, p0, Lcom/squareup/noho/NohoEditText;->textBounds:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    .line 188
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditText;->getPaddingLeft()I

    move-result v2

    add-int/2addr v2, v1

    add-int/2addr v2, v0

    .line 189
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditText;->getWidth()I

    move-result v0

    sub-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditText;->getPaddingRight()I

    move-result v1

    sub-int v9, v0, v1

    .line 190
    iget-object v4, p0, Lcom/squareup/noho/NohoEditText;->textBounds:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/squareup/noho/NohoEditText;->notePaint:Landroid/text/TextPaint;

    iget-object v6, p0, Lcom/squareup/noho/NohoEditText;->noteText:Ljava/lang/String;

    int-to-float v7, v2

    .line 191
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditText;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v8, v0

    move-object v3, p1

    .line 190
    invoke-static/range {v3 .. v9}, Lcom/squareup/noho/NohoEditText;->drawTextCenteredVertically(Landroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/text/TextPaint;Ljava/lang/String;FFI)V

    .line 194
    :cond_1
    invoke-super {p0, p1}, Lcom/squareup/widgets/SelectableEditText;->onDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    .line 143
    invoke-super/range {p0 .. p5}, Lcom/squareup/widgets/SelectableEditText;->onLayout(ZIIII)V

    .line 144
    invoke-direct {p0}, Lcom/squareup/noho/NohoEditText;->setUpGravityAndPaddingAccordingToLabelText()V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0

    .line 267
    invoke-super {p0, p1}, Lcom/squareup/widgets/SelectableEditText;->onWindowFocusChanged(Z)V

    .line 268
    invoke-direct {p0}, Lcom/squareup/noho/NohoEditText;->maybeShowKeyboard()V

    return-void
.end method

.method public requestRectangleOnScreen(Landroid/graphics/Rect;Z)Z
    .locals 0

    .line 208
    invoke-direct {p0, p1}, Lcom/squareup/noho/NohoEditText;->adjustRect(Landroid/graphics/Rect;)V

    .line 209
    invoke-super {p0, p1, p2}, Lcom/squareup/widgets/SelectableEditText;->requestRectangleOnScreen(Landroid/graphics/Rect;Z)Z

    move-result p1

    return p1
.end method

.method public setBorderEdges(I)V
    .locals 1

    .line 168
    iget-object v0, p0, Lcom/squareup/noho/NohoEditText;->edgeController:Lcom/squareup/noho/NohoEdgeController;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEdgeController;->setBorderEdges(I)V

    return-void
.end method

.method public setLabel(Ljava/lang/CharSequence;)V
    .locals 0

    if-eqz p1, :cond_0

    .line 148
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-object p1, p0, Lcom/squareup/noho/NohoEditText;->labelText:Ljava/lang/String;

    .line 149
    invoke-direct {p0}, Lcom/squareup/noho/NohoEditText;->setUpGravityAndPaddingAccordingToLabelText()V

    return-void
.end method

.method public setNote(Ljava/lang/CharSequence;)V
    .locals 2

    .line 157
    iget v0, p0, Lcom/squareup/noho/NohoEditText;->textAlignment:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "Note can only be set when textAlignment==left"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    if-eqz p1, :cond_1

    .line 159
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    iput-object p1, p0, Lcom/squareup/noho/NohoEditText;->noteText:Ljava/lang/String;

    .line 160
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditText;->invalidate()V

    return-void
.end method
