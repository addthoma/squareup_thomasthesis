.class public final enum Lcom/squareup/noho/AccessoryType;
.super Ljava/lang/Enum;
.source "AccessoryType.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/noho/AccessoryType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\t\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0011\u0008\u0002\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/noho/AccessoryType;",
        "",
        "drawableId",
        "",
        "(Ljava/lang/String;II)V",
        "getDrawableId",
        "()I",
        "NONE",
        "DISCLOSURE",
        "EXPAND",
        "COLLAPSE",
        "OVERFLOW",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/noho/AccessoryType;

.field public static final enum COLLAPSE:Lcom/squareup/noho/AccessoryType;

.field public static final enum DISCLOSURE:Lcom/squareup/noho/AccessoryType;

.field public static final enum EXPAND:Lcom/squareup/noho/AccessoryType;

.field public static final enum NONE:Lcom/squareup/noho/AccessoryType;

.field public static final enum OVERFLOW:Lcom/squareup/noho/AccessoryType;


# instance fields
.field private final drawableId:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/noho/AccessoryType;

    new-instance v1, Lcom/squareup/noho/AccessoryType;

    const/4 v2, 0x0

    const-string v3, "NONE"

    .line 13
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/noho/AccessoryType;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/noho/AccessoryType;->NONE:Lcom/squareup/noho/AccessoryType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/noho/AccessoryType;

    .line 15
    sget v2, Lcom/squareup/vectoricons/R$drawable;->icon_chevron_right:I

    const/4 v3, 0x1

    const-string v4, "DISCLOSURE"

    invoke-direct {v1, v4, v3, v2}, Lcom/squareup/noho/AccessoryType;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/noho/AccessoryType;->DISCLOSURE:Lcom/squareup/noho/AccessoryType;

    aput-object v1, v0, v3

    new-instance v1, Lcom/squareup/noho/AccessoryType;

    .line 17
    sget v2, Lcom/squareup/vectoricons/R$drawable;->icon_chevron_down:I

    const/4 v3, 0x2

    const-string v4, "EXPAND"

    invoke-direct {v1, v4, v3, v2}, Lcom/squareup/noho/AccessoryType;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/noho/AccessoryType;->EXPAND:Lcom/squareup/noho/AccessoryType;

    aput-object v1, v0, v3

    new-instance v1, Lcom/squareup/noho/AccessoryType;

    .line 19
    sget v2, Lcom/squareup/vectoricons/R$drawable;->icon_chevron_up:I

    const/4 v3, 0x3

    const-string v4, "COLLAPSE"

    invoke-direct {v1, v4, v3, v2}, Lcom/squareup/noho/AccessoryType;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/noho/AccessoryType;->COLLAPSE:Lcom/squareup/noho/AccessoryType;

    aput-object v1, v0, v3

    new-instance v1, Lcom/squareup/noho/AccessoryType;

    .line 21
    sget v2, Lcom/squareup/vectoricons/R$drawable;->ui_overflow_24:I

    const/4 v3, 0x4

    const-string v4, "OVERFLOW"

    invoke-direct {v1, v4, v3, v2}, Lcom/squareup/noho/AccessoryType;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/noho/AccessoryType;->OVERFLOW:Lcom/squareup/noho/AccessoryType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/noho/AccessoryType;->$VALUES:[Lcom/squareup/noho/AccessoryType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/squareup/noho/AccessoryType;->drawableId:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/noho/AccessoryType;
    .locals 1

    const-class v0, Lcom/squareup/noho/AccessoryType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/noho/AccessoryType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/noho/AccessoryType;
    .locals 1

    sget-object v0, Lcom/squareup/noho/AccessoryType;->$VALUES:[Lcom/squareup/noho/AccessoryType;

    invoke-virtual {v0}, [Lcom/squareup/noho/AccessoryType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/noho/AccessoryType;

    return-object v0
.end method


# virtual methods
.method public final getDrawableId()I
    .locals 1

    .line 11
    iget v0, p0, Lcom/squareup/noho/AccessoryType;->drawableId:I

    return v0
.end method
