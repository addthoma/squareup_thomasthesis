.class public final enum Lcom/squareup/noho/NohoButtonType;
.super Ljava/lang/Enum;
.source "NohoButtonType.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/noho/NohoButtonType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u001b\u0008\u0002\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0007j\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/noho/NohoButtonType;",
        "",
        "styleAttr",
        "",
        "styleRes",
        "(Ljava/lang/String;III)V",
        "getStyleAttr",
        "()I",
        "getStyleRes",
        "applyTo",
        "",
        "view",
        "Lcom/squareup/noho/NohoButton;",
        "PRIMARY",
        "SECONDARY",
        "TERTIARY",
        "DESTRUCTIVE",
        "LINK",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/noho/NohoButtonType;

.field public static final enum DESTRUCTIVE:Lcom/squareup/noho/NohoButtonType;

.field public static final enum LINK:Lcom/squareup/noho/NohoButtonType;

.field public static final enum PRIMARY:Lcom/squareup/noho/NohoButtonType;

.field public static final enum SECONDARY:Lcom/squareup/noho/NohoButtonType;

.field public static final enum TERTIARY:Lcom/squareup/noho/NohoButtonType;


# instance fields
.field private final styleAttr:I

.field private final styleRes:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/noho/NohoButtonType;

    new-instance v1, Lcom/squareup/noho/NohoButtonType;

    .line 16
    sget v2, Lcom/squareup/noho/R$attr;->nohoPrimaryButtonStyle:I

    sget v3, Lcom/squareup/noho/R$style;->Widget_Noho_PrimaryButton:I

    const/4 v4, 0x0

    const-string v5, "PRIMARY"

    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/noho/NohoButtonType;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/squareup/noho/NohoButtonType;->PRIMARY:Lcom/squareup/noho/NohoButtonType;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/noho/NohoButtonType;

    .line 17
    sget v2, Lcom/squareup/noho/R$attr;->nohoSecondaryButtonStyle:I

    sget v3, Lcom/squareup/noho/R$style;->Widget_Noho_SecondaryButton:I

    const/4 v4, 0x1

    const-string v5, "SECONDARY"

    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/noho/NohoButtonType;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/squareup/noho/NohoButtonType;->SECONDARY:Lcom/squareup/noho/NohoButtonType;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/noho/NohoButtonType;

    .line 18
    sget v2, Lcom/squareup/noho/R$attr;->nohoTertiaryButtonStyle:I

    sget v3, Lcom/squareup/noho/R$style;->Widget_Noho_TertiaryButton:I

    const/4 v4, 0x2

    const-string v5, "TERTIARY"

    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/noho/NohoButtonType;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/squareup/noho/NohoButtonType;->TERTIARY:Lcom/squareup/noho/NohoButtonType;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/noho/NohoButtonType;

    .line 19
    sget v2, Lcom/squareup/noho/R$attr;->nohoDestructiveButtonStyle:I

    sget v3, Lcom/squareup/noho/R$style;->Widget_Noho_DestructiveButton:I

    const/4 v4, 0x3

    const-string v5, "DESTRUCTIVE"

    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/noho/NohoButtonType;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/squareup/noho/NohoButtonType;->DESTRUCTIVE:Lcom/squareup/noho/NohoButtonType;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/noho/NohoButtonType;

    .line 20
    sget v2, Lcom/squareup/noho/R$attr;->nohoLinkButtonStyle:I

    sget v3, Lcom/squareup/noho/R$style;->Widget_Noho_LinkButton:I

    const/4 v4, 0x4

    const-string v5, "LINK"

    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/noho/NohoButtonType;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/squareup/noho/NohoButtonType;->LINK:Lcom/squareup/noho/NohoButtonType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/noho/NohoButtonType;->$VALUES:[Lcom/squareup/noho/NohoButtonType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/squareup/noho/NohoButtonType;->styleAttr:I

    iput p4, p0, Lcom/squareup/noho/NohoButtonType;->styleRes:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/noho/NohoButtonType;
    .locals 1

    const-class v0, Lcom/squareup/noho/NohoButtonType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/noho/NohoButtonType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/noho/NohoButtonType;
    .locals 1

    sget-object v0, Lcom/squareup/noho/NohoButtonType;->$VALUES:[Lcom/squareup/noho/NohoButtonType;

    invoke-virtual {v0}, [Lcom/squareup/noho/NohoButtonType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/noho/NohoButtonType;

    return-object v0
.end method


# virtual methods
.method public final applyTo(Lcom/squareup/noho/NohoButton;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-virtual {p1, p0}, Lcom/squareup/noho/NohoButton;->apply(Lcom/squareup/noho/NohoButtonType;)V

    return-void
.end method

.method public final getStyleAttr()I
    .locals 1

    .line 15
    iget v0, p0, Lcom/squareup/noho/NohoButtonType;->styleAttr:I

    return v0
.end method

.method public final getStyleRes()I
    .locals 1

    .line 15
    iget v0, p0, Lcom/squareup/noho/NohoButtonType;->styleRes:I

    return v0
.end method
