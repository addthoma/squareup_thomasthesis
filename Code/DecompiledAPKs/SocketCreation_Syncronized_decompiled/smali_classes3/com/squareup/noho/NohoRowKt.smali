.class public final Lcom/squareup/noho/NohoRowKt;
.super Ljava/lang/Object;
.source "NohoRow.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoRow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoRow.kt\ncom/squareup/noho/NohoRowKt\n*L\n1#1,1059:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u0010\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u0004\u0018\u00010\u0001H\u0002\u001a\u0012\u0010\u0002\u001a\u00020\u0003*\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u001a\u0016\u0010\u0007\u001a\u00020\u0003*\u00020\u00082\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\n\u001a\u0016\u0010\u000b\u001a\u00020\u0003*\u00020\u00082\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\n\u00a8\u0006\u000c"
    }
    d2 = {
        "nullIfBlank",
        "",
        "setVerticalGravity",
        "",
        "Landroid/widget/TextView;",
        "verticalGravity",
        "",
        "singleLineDescription",
        "Lcom/squareup/noho/NohoRow;",
        "truncateAt",
        "Landroid/text/TextUtils$TruncateAt;",
        "singleLineLabel",
        "noho_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$nullIfBlank(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/noho/NohoRowKt;->nullIfBlank(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0
.end method

.method private static final nullIfBlank(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 1

    if-eqz p0, :cond_0

    .line 59
    invoke-static {p0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 p0, 0x0

    :cond_1
    return-object p0
.end method

.method public static final setVerticalGravity(Landroid/widget/TextView;I)V
    .locals 1

    const-string v0, "$this$setVerticalGravity"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1055
    invoke-virtual {p0}, Landroid/widget/TextView;->getGravity()I

    move-result v0

    and-int/lit8 v0, v0, -0x71

    and-int/lit8 p1, p1, 0x70

    or-int/2addr p1, v0

    .line 1057
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setGravity(I)V

    return-void
.end method

.method public static final singleLineDescription(Lcom/squareup/noho/NohoRow;Landroid/text/TextUtils$TruncateAt;)V
    .locals 1

    const-string v0, "$this$singleLineDescription"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1044
    check-cast p0, Landroid/view/View;

    sget v0, Lcom/squareup/noho/R$id;->description:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroid/widget/TextView;

    const/4 v0, 0x1

    .line 1045
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setMaxLines(I)V

    if-eqz p1, :cond_0

    .line 1047
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    :cond_0
    return-void
.end method

.method public static synthetic singleLineDescription$default(Lcom/squareup/noho/NohoRow;Landroid/text/TextUtils$TruncateAt;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 1043
    check-cast p1, Landroid/text/TextUtils$TruncateAt;

    :cond_0
    invoke-static {p0, p1}, Lcom/squareup/noho/NohoRowKt;->singleLineDescription(Lcom/squareup/noho/NohoRow;Landroid/text/TextUtils$TruncateAt;)V

    return-void
.end method

.method public static final singleLineLabel(Lcom/squareup/noho/NohoRow;Landroid/text/TextUtils$TruncateAt;)V
    .locals 1

    const-string v0, "$this$singleLineLabel"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1030
    check-cast p0, Landroid/view/View;

    sget v0, Lcom/squareup/noho/R$id;->label:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroid/widget/TextView;

    const/4 v0, 0x1

    .line 1031
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setMaxLines(I)V

    if-eqz p1, :cond_0

    .line 1033
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    :cond_0
    return-void
.end method

.method public static synthetic singleLineLabel$default(Lcom/squareup/noho/NohoRow;Landroid/text/TextUtils$TruncateAt;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 1029
    check-cast p1, Landroid/text/TextUtils$TruncateAt;

    :cond_0
    invoke-static {p0, p1}, Lcom/squareup/noho/NohoRowKt;->singleLineLabel(Lcom/squareup/noho/NohoRow;Landroid/text/TextUtils$TruncateAt;)V

    return-void
.end method
