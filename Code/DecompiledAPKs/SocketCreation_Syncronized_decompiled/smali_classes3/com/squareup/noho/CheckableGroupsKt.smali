.class public final Lcom/squareup/noho/CheckableGroupsKt;
.super Ljava/lang/Object;
.source "CheckableGroups.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u001e\u0010\u0000\u001a\u00020\u0001\"\u0004\u0008\u0000\u0010\u0002*\u0008\u0012\u0004\u0012\u0002H\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005*\u0016\u0010\u0006\"\u0008\u0012\u0004\u0012\u00020\u00010\u00072\u0008\u0012\u0004\u0012\u00020\u00010\u0007*4\u0010\u0008\u001a\u0004\u0008\u0000\u0010\u0002\"\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\t2\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\t\u00a8\u0006\n"
    }
    d2 = {
        "setCheckedSilently",
        "",
        "T",
        "Lcom/squareup/noho/ListenableCheckable;",
        "value",
        "",
        "CheckableGroupListener",
        "Lkotlin/Function0;",
        "OnCheckedChange",
        "Lkotlin/Function2;",
        "noho_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final setCheckedSilently(Lcom/squareup/noho/ListenableCheckable;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/noho/ListenableCheckable<",
            "TT;>;Z)V"
        }
    .end annotation

    const-string v0, "$this$setCheckedSilently"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-interface {p0}, Lcom/squareup/noho/ListenableCheckable;->isChecked()Z

    move-result v0

    if-eq p1, v0, :cond_0

    .line 57
    invoke-interface {p0}, Lcom/squareup/noho/ListenableCheckable;->getOnCheckedChange()Lkotlin/jvm/functions/Function2;

    move-result-object v0

    const/4 v1, 0x0

    .line 58
    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-interface {p0, v1}, Lcom/squareup/noho/ListenableCheckable;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 59
    invoke-interface {p0, p1}, Lcom/squareup/noho/ListenableCheckable;->setChecked(Z)V

    .line 60
    invoke-interface {p0, v0}, Lcom/squareup/noho/ListenableCheckable;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    :cond_0
    return-void
.end method
