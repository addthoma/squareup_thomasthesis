.class public final Lcom/squareup/noho/NohoRowUtilsKt;
.super Ljava/lang/Object;
.source "NohoRowUtils.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoRowUtils.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoRowUtils.kt\ncom/squareup/noho/NohoRowUtilsKt\n*L\n1#1,151:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\u001a\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0002\u001a\u0010\u0010\u000b\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0002\u001a\u0012\u0010\u000c\u001a\u00020\r*\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010\u001a\u000c\u0010\u0011\u001a\u0004\u0018\u00010\u0008*\u00020\u000e\u001a\u0012\u0010\u0012\u001a\u00020\r*\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u0002\u001a\u001a\u0010\u0014\u001a\u00020\r*\u00020\u000e2\u0006\u0010\u0015\u001a\u00020\u00022\u0006\u0010\u0016\u001a\u00020\u0017\"\u0016\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u0003\"\u000e\u0010\u0004\u001a\u00020\u0002X\u0082\u0004\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0002X\u0082\u0004\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0002X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "ICON_IDS",
        "",
        "",
        "[Ljava/lang/Integer;",
        "ICON_ID_BOX_OR_PICTURE",
        "ICON_ID_SIMPLE",
        "ICON_ID_TEXT",
        "viewToDeferredDrawable",
        "Landroid/graphics/drawable/Drawable;",
        "view",
        "Landroid/view/View;",
        "viewToDrawable",
        "constrainValueSize",
        "",
        "Lcom/squareup/noho/NohoRow;",
        "percent",
        "",
        "iconToDrawable",
        "setHorizontalGravityForValue",
        "gravity",
        "setMaxLinesForValue",
        "maxLines",
        "truncateAt",
        "Landroid/text/TextUtils$TruncateAt;",
        "noho_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final ICON_IDS:[Ljava/lang/Integer;

.field private static final ICON_ID_BOX_OR_PICTURE:I

.field private static final ICON_ID_SIMPLE:I

.field private static final ICON_ID_TEXT:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 95
    sget v0, Lcom/squareup/noho/R$id;->icon:I

    sput v0, Lcom/squareup/noho/NohoRowUtilsKt;->ICON_ID_BOX_OR_PICTURE:I

    .line 96
    sget v0, Lcom/squareup/noho/R$id;->simpleIcon:I

    sput v0, Lcom/squareup/noho/NohoRowUtilsKt;->ICON_ID_SIMPLE:I

    .line 97
    sget v0, Lcom/squareup/noho/R$id;->textIcon:I

    sput v0, Lcom/squareup/noho/NohoRowUtilsKt;->ICON_ID_TEXT:I

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Integer;

    .line 98
    sget v1, Lcom/squareup/noho/NohoRowUtilsKt;->ICON_ID_BOX_OR_PICTURE:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget v1, Lcom/squareup/noho/NohoRowUtilsKt;->ICON_ID_SIMPLE:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget v1, Lcom/squareup/noho/NohoRowUtilsKt;->ICON_ID_TEXT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/noho/NohoRowUtilsKt;->ICON_IDS:[Ljava/lang/Integer;

    return-void
.end method

.method public static final synthetic access$viewToDrawable(Landroid/view/View;)Landroid/graphics/drawable/Drawable;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/noho/NohoRowUtilsKt;->viewToDrawable(Landroid/view/View;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    return-object p0
.end method

.method public static final constrainValueSize(Lcom/squareup/noho/NohoRow;F)V
    .locals 3

    const-string v0, "$this$constrainValueSize"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    new-instance v0, Landroidx/constraintlayout/widget/ConstraintSet;

    invoke-direct {v0}, Landroidx/constraintlayout/widget/ConstraintSet;-><init>()V

    .line 117
    check-cast p0, Landroidx/constraintlayout/widget/ConstraintLayout;

    invoke-virtual {v0, p0}, Landroidx/constraintlayout/widget/ConstraintSet;->clone(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    .line 119
    sget v1, Lcom/squareup/noho/R$id;->value:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroidx/constraintlayout/widget/ConstraintSet;->constrainWidth(II)V

    .line 121
    sget v1, Lcom/squareup/noho/R$id;->value:I

    invoke-virtual {v0, v1, p1}, Landroidx/constraintlayout/widget/ConstraintSet;->constrainPercentWidth(IF)V

    .line 122
    invoke-virtual {v0, p0}, Landroidx/constraintlayout/widget/ConstraintSet;->applyTo(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    return-void
.end method

.method public static final iconToDrawable(Lcom/squareup/noho/NohoRow;)Landroid/graphics/drawable/Drawable;
    .locals 2

    const-string v0, "$this$iconToDrawable"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    sget-object v0, Lcom/squareup/noho/NohoRowUtilsKt;->ICON_IDS:[Ljava/lang/Integer;

    .line 30
    invoke-static {v0}, Lkotlin/collections/ArraysKt;->asSequence([Ljava/lang/Object;)Lkotlin/sequences/Sequence;

    move-result-object v0

    .line 31
    new-instance v1, Lcom/squareup/noho/NohoRowUtilsKt$iconToDrawable$iconView$1;

    invoke-direct {v1, p0}, Lcom/squareup/noho/NohoRowUtilsKt$iconToDrawable$iconView$1;-><init>(Lcom/squareup/noho/NohoRow;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/sequences/SequencesKt;->map(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object p0

    .line 32
    invoke-static {p0}, Lkotlin/sequences/SequencesKt;->filterNotNull(Lkotlin/sequences/Sequence;)Lkotlin/sequences/Sequence;

    move-result-object p0

    .line 33
    invoke-static {p0}, Lkotlin/sequences/SequencesKt;->firstOrNull(Lkotlin/sequences/Sequence;)Ljava/lang/Object;

    move-result-object p0

    .line 29
    check-cast p0, Landroid/view/View;

    if-eqz p0, :cond_2

    .line 35
    invoke-virtual {p0}, Landroid/view/View;->isDirty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 39
    :cond_0
    invoke-static {p0}, Lcom/squareup/noho/NohoRowUtilsKt;->viewToDrawable(Landroid/view/View;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    goto :goto_1

    .line 37
    :cond_1
    :goto_0
    invoke-static {p0}, Lcom/squareup/noho/NohoRowUtilsKt;->viewToDeferredDrawable(Landroid/view/View;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    goto :goto_1

    :cond_2
    const/4 p0, 0x0

    :goto_1
    return-object p0
.end method

.method public static final setHorizontalGravityForValue(Lcom/squareup/noho/NohoRow;I)V
    .locals 1

    const-string v0, "$this$setHorizontalGravityForValue"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    check-cast p0, Landroid/view/View;

    sget v0, Lcom/squareup/noho/R$id;->value:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroid/widget/TextView;

    .line 148
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setGravity(I)V

    return-void
.end method

.method public static final setMaxLinesForValue(Lcom/squareup/noho/NohoRow;ILandroid/text/TextUtils$TruncateAt;)V
    .locals 1

    const-string v0, "$this$setMaxLinesForValue"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "truncateAt"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    check-cast p0, Landroid/view/View;

    sget v0, Lcom/squareup/noho/R$id;->value:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroid/widget/TextView;

    .line 136
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 137
    invoke-virtual {p0, p2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    return-void
.end method

.method private static final viewToDeferredDrawable(Landroid/view/View;)Landroid/graphics/drawable/Drawable;
    .locals 4

    .line 68
    new-instance v0, Lcom/squareup/noho/DrawableWrapper;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    check-cast v1, Landroid/graphics/drawable/Drawable;

    invoke-direct {v0, v1}, Lcom/squareup/noho/DrawableWrapper;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 70
    new-instance v1, Lcom/squareup/noho/NohoRowUtilsKt$viewToDeferredDrawable$listener$1;

    invoke-direct {v1, p0, v0}, Lcom/squareup/noho/NohoRowUtilsKt$viewToDeferredDrawable$listener$1;-><init>(Landroid/view/View;Lcom/squareup/noho/DrawableWrapper;)V

    .line 90
    invoke-virtual {p0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    move-object v3, v1

    check-cast v3, Landroid/view/ViewTreeObserver$OnPreDrawListener;

    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 91
    check-cast v1, Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {p0, v1}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 92
    check-cast v0, Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private static final viewToDrawable(Landroid/view/View;)Landroid/graphics/drawable/Drawable;
    .locals 4

    .line 53
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 54
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 55
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 52
    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 57
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 58
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "view.context"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 59
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result p0

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v2, v0, p0}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    .line 58
    check-cast v1, Landroid/graphics/drawable/Drawable;

    return-object v1
.end method
