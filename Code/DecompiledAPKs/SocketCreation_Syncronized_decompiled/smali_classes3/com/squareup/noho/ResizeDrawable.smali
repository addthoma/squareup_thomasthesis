.class public final Lcom/squareup/noho/ResizeDrawable;
.super Lcom/squareup/noho/DrawableWrapper;
.source "ResizeDrawable.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/noho/ResizeDrawable$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nResizeDrawable.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ResizeDrawable.kt\ncom/squareup/noho/ResizeDrawable\n+ 2 StyledAttributes.kt\ncom/squareup/util/StyledAttributesKt\n*L\n1#1,77:1\n94#2,6:78\n*E\n*S KotlinDebug\n*F\n+ 1 ResizeDrawable.kt\ncom/squareup/noho/ResizeDrawable\n*L\n61#1,6:78\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u00152\u00020\u0001:\u0001\u0015B\u001f\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007B\u0005\u00a2\u0006\u0002\u0010\u0008J\u0008\u0010\t\u001a\u00020\u0005H\u0016J\u0008\u0010\n\u001a\u00020\u0005H\u0016J.\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u000c\u0010\u0013\u001a\u0008\u0018\u00010\u0014R\u00020\u000eH\u0016R\u000e\u0010\u0006\u001a\u00020\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/noho/ResizeDrawable;",
        "Lcom/squareup/noho/DrawableWrapper;",
        "delegate",
        "Landroid/graphics/drawable/Drawable;",
        "width",
        "",
        "height",
        "(Landroid/graphics/drawable/Drawable;II)V",
        "()V",
        "getIntrinsicHeight",
        "getIntrinsicWidth",
        "inflate",
        "",
        "resources",
        "Landroid/content/res/Resources;",
        "parser",
        "Lorg/xmlpull/v1/XmlPullParser;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "maybeTheme",
        "Landroid/content/res/Resources$Theme;",
        "Companion",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/noho/ResizeDrawable$Companion;


# instance fields
.field private height:I

.field private width:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/noho/ResizeDrawable$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/noho/ResizeDrawable$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/noho/ResizeDrawable;->Companion:Lcom/squareup/noho/ResizeDrawable$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 24
    invoke-direct {p0, v0, v1, v0}, Lcom/squareup/noho/DrawableWrapper;-><init>(Landroid/graphics/drawable/Drawable;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/graphics/drawable/Drawable;II)V
    .locals 1

    const-string v0, "delegate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Lcom/squareup/noho/ResizeDrawable;-><init>()V

    .line 35
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    const-string v0, "delegate.mutate()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/squareup/noho/ResizeDrawable;->setDelegate(Landroid/graphics/drawable/Drawable;)V

    .line 36
    iput p2, p0, Lcom/squareup/noho/ResizeDrawable;->width:I

    .line 37
    iput p3, p0, Lcom/squareup/noho/ResizeDrawable;->height:I

    return-void
.end method

.method public static final synthetic access$getHeight$p(Lcom/squareup/noho/ResizeDrawable;)I
    .locals 0

    .line 24
    iget p0, p0, Lcom/squareup/noho/ResizeDrawable;->height:I

    return p0
.end method

.method public static final synthetic access$getWidth$p(Lcom/squareup/noho/ResizeDrawable;)I
    .locals 0

    .line 24
    iget p0, p0, Lcom/squareup/noho/ResizeDrawable;->width:I

    return p0
.end method

.method public static final synthetic access$setHeight$p(Lcom/squareup/noho/ResizeDrawable;I)V
    .locals 0

    .line 24
    iput p1, p0, Lcom/squareup/noho/ResizeDrawable;->height:I

    return-void
.end method

.method public static final synthetic access$setWidth$p(Lcom/squareup/noho/ResizeDrawable;I)V
    .locals 0

    .line 24
    iput p1, p0, Lcom/squareup/noho/ResizeDrawable;->width:I

    return-void
.end method

.method public static final createFromResources(Landroid/content/Context;II)Lcom/squareup/noho/ResizeDrawable;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/noho/ResizeDrawable;->Companion:Lcom/squareup/noho/ResizeDrawable$Companion;

    invoke-virtual {v0, p0, p1, p2}, Lcom/squareup/noho/ResizeDrawable$Companion;->createFromResources(Landroid/content/Context;II)Lcom/squareup/noho/ResizeDrawable;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public getIntrinsicHeight()I
    .locals 1

    .line 75
    iget v0, p0, Lcom/squareup/noho/ResizeDrawable;->height:I

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    .line 74
    iget v0, p0, Lcom/squareup/noho/ResizeDrawable;->width:I

    return v0
.end method

.method public inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V
    .locals 1

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parser"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "attrs"

    invoke-static {p3, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p4, :cond_0

    goto :goto_0

    .line 60
    :cond_0
    invoke-virtual {p1}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object p4

    :goto_0
    const-string/jumbo p2, "theme"

    .line 61
    invoke-static {p4, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    sget-object p2, Lcom/squareup/noho/R$styleable;->NohoResizeDrawable:[I

    const-string v0, "R.styleable.NohoResizeDrawable"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 78
    invoke-virtual {p4, p3, p2, v0, v0}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    :try_start_0
    const-string p3, "a"

    .line 80
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    sget p3, Lcom/squareup/noho/R$styleable;->NohoResizeDrawable_android_drawable:I

    invoke-virtual {p2, p3, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p3

    .line 68
    invoke-static {p1, p3, p4}, Landroidx/vectordrawable/graphics/drawable/VectorDrawableCompat;->create(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroidx/vectordrawable/graphics/drawable/VectorDrawableCompat;

    move-result-object p1

    if-nez p1, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    check-cast p1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, p1}, Lcom/squareup/noho/ResizeDrawable;->setDelegate(Landroid/graphics/drawable/Drawable;)V

    .line 69
    sget p1, Lcom/squareup/noho/R$styleable;->NohoResizeDrawable_android_width:I

    invoke-virtual {p2, p1, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p1

    invoke-static {p0, p1}, Lcom/squareup/noho/ResizeDrawable;->access$setWidth$p(Lcom/squareup/noho/ResizeDrawable;I)V

    .line 70
    sget p1, Lcom/squareup/noho/R$styleable;->NohoResizeDrawable_android_height:I

    invoke-virtual {p2, p1, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p1

    invoke-static {p0, p1}, Lcom/squareup/noho/ResizeDrawable;->access$setHeight$p(Lcom/squareup/noho/ResizeDrawable;I)V

    .line 71
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :catchall_0
    move-exception p1

    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    throw p1
.end method
