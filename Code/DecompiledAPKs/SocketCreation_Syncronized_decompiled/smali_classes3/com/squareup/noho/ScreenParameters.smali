.class final Lcom/squareup/noho/ScreenParameters;
.super Ljava/lang/Object;
.source "ScreenParameters.java"


# static fields
.field private static generatedInPortrait:Z = false

.field private static screenHeight:I = -0x1

.field private static screenWidth:I = -0x1


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "no instances"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method static getScreenDimens(Landroid/content/Context;)Landroid/graphics/Point;
    .locals 2

    .line 22
    sget v0, Lcom/squareup/noho/ScreenParameters;->screenWidth:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    sget v0, Lcom/squareup/noho/ScreenParameters;->screenHeight:I

    if-ne v0, v1, :cond_1

    .line 23
    :cond_0
    invoke-static {p0}, Lcom/squareup/noho/ScreenParameters;->initDimens(Landroid/content/Context;)V

    .line 26
    :cond_1
    sget-boolean v0, Lcom/squareup/noho/ScreenParameters;->generatedInPortrait:Z

    invoke-static {p0}, Lcom/squareup/noho/ScreenParameters;->isPortrait(Landroid/content/Context;)Z

    move-result p0

    if-ne v0, p0, :cond_2

    .line 27
    new-instance p0, Landroid/graphics/Point;

    sget v0, Lcom/squareup/noho/ScreenParameters;->screenWidth:I

    sget v1, Lcom/squareup/noho/ScreenParameters;->screenHeight:I

    invoke-direct {p0, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    return-object p0

    .line 30
    :cond_2
    new-instance p0, Landroid/graphics/Point;

    sget v0, Lcom/squareup/noho/ScreenParameters;->screenHeight:I

    sget v1, Lcom/squareup/noho/ScreenParameters;->screenWidth:I

    invoke-direct {p0, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    return-object p0
.end method

.method private static initDimens(Landroid/content/Context;)V
    .locals 2

    const-string/jumbo v0, "window"

    .line 35
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 36
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 37
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 38
    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 39
    iget v0, v1, Landroid/graphics/Point;->x:I

    sput v0, Lcom/squareup/noho/ScreenParameters;->screenWidth:I

    .line 40
    iget v0, v1, Landroid/graphics/Point;->y:I

    sput v0, Lcom/squareup/noho/ScreenParameters;->screenHeight:I

    .line 41
    invoke-static {p0}, Lcom/squareup/noho/ScreenParameters;->isPortrait(Landroid/content/Context;)Z

    move-result p0

    sput-boolean p0, Lcom/squareup/noho/ScreenParameters;->generatedInPortrait:Z

    return-void
.end method

.method private static isPortrait(Landroid/content/Context;)Z
    .locals 1

    .line 46
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p0

    .line 47
    iget p0, p0, Landroid/content/res/Configuration;->orientation:I

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
