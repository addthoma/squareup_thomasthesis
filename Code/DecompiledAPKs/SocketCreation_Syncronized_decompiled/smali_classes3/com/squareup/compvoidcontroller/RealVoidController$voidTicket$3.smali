.class final Lcom/squareup/compvoidcontroller/RealVoidController$voidTicket$3;
.super Ljava/lang/Object;
.source "RealVoidController.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/compvoidcontroller/RealVoidController;->voidTicket(Lcom/squareup/tickets/OpenTicket;Ljava/lang/String;Lcom/squareup/payment/OrderSnapshot;Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/IdPair;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $openTicket:Lcom/squareup/tickets/OpenTicket;

.field final synthetic $order:Lcom/squareup/payment/Order;

.field final synthetic $voidedItems:Ljava/util/List;

.field final synthetic this$0:Lcom/squareup/compvoidcontroller/RealVoidController;


# direct methods
.method constructor <init>(Lcom/squareup/compvoidcontroller/RealVoidController;Lcom/squareup/payment/Order;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/compvoidcontroller/RealVoidController$voidTicket$3;->this$0:Lcom/squareup/compvoidcontroller/RealVoidController;

    iput-object p2, p0, Lcom/squareup/compvoidcontroller/RealVoidController$voidTicket$3;->$order:Lcom/squareup/payment/Order;

    iput-object p3, p0, Lcom/squareup/compvoidcontroller/RealVoidController$voidTicket$3;->$openTicket:Lcom/squareup/tickets/OpenTicket;

    iput-object p4, p0, Lcom/squareup/compvoidcontroller/RealVoidController$voidTicket$3;->$voidedItems:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .line 104
    iget-object v0, p0, Lcom/squareup/compvoidcontroller/RealVoidController$voidTicket$3;->this$0:Lcom/squareup/compvoidcontroller/RealVoidController;

    invoke-static {v0}, Lcom/squareup/compvoidcontroller/RealVoidController;->access$getOrderPrintingDispatcher$p(Lcom/squareup/compvoidcontroller/RealVoidController;)Lcom/squareup/print/OrderPrintingDispatcher;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/compvoidcontroller/RealVoidController$voidTicket$3;->$order:Lcom/squareup/payment/Order;

    iget-object v2, p0, Lcom/squareup/compvoidcontroller/RealVoidController$voidTicket$3;->$openTicket:Lcom/squareup/tickets/OpenTicket;

    iget-object v3, p0, Lcom/squareup/compvoidcontroller/RealVoidController$voidTicket$3;->$voidedItems:Ljava/util/List;

    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/print/OrderPrintingDispatcher;->printVoidTicket(Lcom/squareup/payment/Order;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;)V

    return-void
.end method
