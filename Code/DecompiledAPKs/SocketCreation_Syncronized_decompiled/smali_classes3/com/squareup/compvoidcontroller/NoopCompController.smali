.class public final Lcom/squareup/compvoidcontroller/NoopCompController;
.super Ljava/lang/Object;
.source "NoopCompController.kt"

# interfaces
.implements Lcom/squareup/compvoidcontroller/CompController;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0001\u00a2\u0006\u0002\u0010\u0002J,\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u0016J\u001a\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u0016\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/compvoidcontroller/NoopCompController;",
        "Lcom/squareup/compvoidcontroller/CompController;",
        "()V",
        "compTicket",
        "Lcom/squareup/protos/client/IdPair;",
        "openTicket",
        "Lcom/squareup/tickets/OpenTicket;",
        "compDiscount",
        "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
        "baseOrder",
        "Lcom/squareup/payment/OrderSnapshot;",
        "employee",
        "Lcom/squareup/protos/client/Employee;",
        "compTransactionTicket",
        "",
        "impl-noop_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compTicket(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/payment/OrderSnapshot;Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/IdPair;
    .locals 0

    const-string p4, "openTicket"

    invoke-static {p1, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "compDiscount"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "baseOrder"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public compTransactionTicket(Lcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/protos/client/Employee;)V
    .locals 0

    const-string p2, "compDiscount"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
