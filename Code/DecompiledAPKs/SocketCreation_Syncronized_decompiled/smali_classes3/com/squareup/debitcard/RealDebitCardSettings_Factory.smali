.class public final Lcom/squareup/debitcard/RealDebitCardSettings_Factory;
.super Ljava/lang/Object;
.source "RealDebitCardSettings_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/debitcard/RealDebitCardSettings;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/debitcard/LinkDebitCardService;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositAnalytics;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/debitcard/LinkDebitCardService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositAnalytics;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/debitcard/RealDebitCardSettings_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/debitcard/RealDebitCardSettings_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/squareup/debitcard/RealDebitCardSettings_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 29
    iput-object p4, p0, Lcom/squareup/debitcard/RealDebitCardSettings_Factory;->arg3Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/debitcard/RealDebitCardSettings_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/debitcard/LinkDebitCardService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositAnalytics;",
            ">;)",
            "Lcom/squareup/debitcard/RealDebitCardSettings_Factory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/debitcard/RealDebitCardSettings_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/debitcard/RealDebitCardSettings_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/debitcard/LinkDebitCardService;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/instantdeposit/InstantDepositAnalytics;)Lcom/squareup/debitcard/RealDebitCardSettings;
    .locals 1

    .line 45
    new-instance v0, Lcom/squareup/debitcard/RealDebitCardSettings;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/debitcard/RealDebitCardSettings;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/debitcard/LinkDebitCardService;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/instantdeposit/InstantDepositAnalytics;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/debitcard/RealDebitCardSettings;
    .locals 4

    .line 34
    iget-object v0, p0, Lcom/squareup/debitcard/RealDebitCardSettings_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, p0, Lcom/squareup/debitcard/RealDebitCardSettings_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/debitcard/LinkDebitCardService;

    iget-object v2, p0, Lcom/squareup/debitcard/RealDebitCardSettings_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/receiving/FailureMessageFactory;

    iget-object v3, p0, Lcom/squareup/debitcard/RealDebitCardSettings_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/debitcard/RealDebitCardSettings_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/debitcard/LinkDebitCardService;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/instantdeposit/InstantDepositAnalytics;)Lcom/squareup/debitcard/RealDebitCardSettings;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/debitcard/RealDebitCardSettings_Factory;->get()Lcom/squareup/debitcard/RealDebitCardSettings;

    move-result-object v0

    return-object v0
.end method
