.class public final synthetic Lcom/squareup/debitcard/CardDrawables$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 5

    invoke-static {}, Lcom/squareup/Card$Brand;->values()[Lcom/squareup/Card$Brand;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/debitcard/CardDrawables$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/debitcard/CardDrawables$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/Card$Brand;->VISA:Lcom/squareup/Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/Card$Brand;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/debitcard/CardDrawables$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/Card$Brand;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/debitcard/CardDrawables$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/Card$Brand;->AMERICAN_EXPRESS:Lcom/squareup/Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/Card$Brand;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/debitcard/CardDrawables$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/Card$Brand;->DISCOVER:Lcom/squareup/Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/Card$Brand;->ordinal()I

    move-result v1

    const/4 v4, 0x4

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/debitcard/CardDrawables$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/Card$Brand;->DISCOVER_DINERS:Lcom/squareup/Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/Card$Brand;->ordinal()I

    move-result v1

    const/4 v4, 0x5

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/debitcard/CardDrawables$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/Card$Brand;->ordinal()I

    move-result v1

    const/4 v4, 0x6

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/debitcard/CardDrawables$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/Card$Brand;->JCB:Lcom/squareup/Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/Card$Brand;->ordinal()I

    move-result v1

    const/4 v4, 0x7

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/debitcard/CardDrawables$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/Card$Brand;->ordinal()I

    move-result v1

    const/16 v4, 0x8

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/debitcard/CardDrawables$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/Card$Brand;->ordinal()I

    move-result v1

    const/16 v4, 0x9

    aput v4, v0, v1

    invoke-static {}, Lcom/squareup/protos/common/CurrencyCode;->values()[Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/debitcard/CardDrawables$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/debitcard/CardDrawables$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/common/CurrencyCode;->AUD:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v1}, Lcom/squareup/protos/common/CurrencyCode;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/debitcard/CardDrawables$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/common/CurrencyCode;->GBP:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v1}, Lcom/squareup/protos/common/CurrencyCode;->ordinal()I

    move-result v1

    aput v3, v0, v1

    return-void
.end method
