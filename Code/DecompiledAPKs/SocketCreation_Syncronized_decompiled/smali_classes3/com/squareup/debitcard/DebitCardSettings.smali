.class public interface abstract Lcom/squareup/debitcard/DebitCardSettings;
.super Ljava/lang/Object;
.source "DebitCardSettings.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/debitcard/DebitCardSettings$State;,
        Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;,
        Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008f\u0018\u00002\u00020\u0001:\u0003\u0008\t\nJ\u0016\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0005\u001a\u00020\u0006H&J\u000e\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H&\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/debitcard/DebitCardSettings;",
        "",
        "linkCard",
        "Lio/reactivex/Single;",
        "Lcom/squareup/debitcard/DebitCardSettings$State;",
        "linkCardRequest",
        "Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;",
        "resendEmail",
        "LinkCardState",
        "ResendEmailState",
        "State",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract linkCard(Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/debitcard/DebitCardSettings$State;",
            ">;"
        }
    .end annotation
.end method

.method public abstract resendEmail()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/debitcard/DebitCardSettings$State;",
            ">;"
        }
    .end annotation
.end method
