.class public final Lcom/squareup/debitcard/LinkDebitCardResultScreen;
.super Ljava/lang/Object;
.source "LinkDebitCardResultScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u001e\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B{\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\t\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\n\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u0003\u0012\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u0012\u0012\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000e0\u0010\u0012\u000c\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u00a2\u0006\u0002\u0010\u0012J\t\u0010\"\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010#\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\rH\u00c6\u0003J\t\u0010$\u001a\u00020\u0005H\u00c6\u0003J\t\u0010%\u001a\u00020\u0007H\u00c6\u0003J\t\u0010&\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\'\u001a\u00020\u0005H\u00c6\u0003J\t\u0010(\u001a\u00020\u0003H\u00c6\u0003J\t\u0010)\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010*\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\rH\u00c6\u0003J\u0015\u0010+\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000e0\u0010H\u00c6\u0003J\u0085\u0001\u0010,\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00052\u0008\u0008\u0002\u0010\t\u001a\u00020\u00052\u0008\u0008\u0002\u0010\n\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r2\u0014\u0008\u0002\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000e0\u00102\u000e\u0008\u0002\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\rH\u00c6\u0001J\u0013\u0010-\u001a\u00020\u00032\u0008\u0010.\u001a\u0004\u0018\u00010/H\u00d6\u0003J\t\u00100\u001a\u000201H\u00d6\u0001J\t\u00102\u001a\u000203H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\u000b\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u0016R\u0011\u0010\t\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0014R\u0017\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001cR\u001d\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000e0\u0010\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001eR\u0017\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010\u001cR\u0011\u0010\n\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010\u0016R\u0011\u0010\u0008\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008!\u0010\u0014\u00a8\u00064"
    }
    d2 = {
        "Lcom/squareup/debitcard/LinkDebitCardResultScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "linkDebitCardFailed",
        "",
        "actionBarTitle",
        "",
        "glyph",
        "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
        "title",
        "message",
        "showLearnMoreButton",
        "clientUpgradeRequired",
        "onCancelLinkResultClicked",
        "Lkotlin/Function0;",
        "",
        "onPrimaryLinkButtonClicked",
        "Lkotlin/Function1;",
        "onSecondaryLinkButtonClicked",
        "(ZLjava/lang/CharSequence;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V",
        "getActionBarTitle",
        "()Ljava/lang/CharSequence;",
        "getClientUpgradeRequired",
        "()Z",
        "getGlyph",
        "()Lcom/squareup/glyph/GlyphTypeface$Glyph;",
        "getLinkDebitCardFailed",
        "getMessage",
        "getOnCancelLinkResultClicked",
        "()Lkotlin/jvm/functions/Function0;",
        "getOnPrimaryLinkButtonClicked",
        "()Lkotlin/jvm/functions/Function1;",
        "getOnSecondaryLinkButtonClicked",
        "getShowLearnMoreButton",
        "getTitle",
        "component1",
        "component10",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBarTitle:Ljava/lang/CharSequence;

.field private final clientUpgradeRequired:Z

.field private final glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field private final linkDebitCardFailed:Z

.field private final message:Ljava/lang/CharSequence;

.field private final onCancelLinkResultClicked:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onPrimaryLinkButtonClicked:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onSecondaryLinkButtonClicked:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final showLearnMoreButton:Z

.field private final title:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(ZLjava/lang/CharSequence;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/CharSequence;",
            "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            "ZZ",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "actionBarTitle"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "glyph"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "title"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onCancelLinkResultClicked"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onPrimaryLinkButtonClicked"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onSecondaryLinkButtonClicked"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->linkDebitCardFailed:Z

    iput-object p2, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->actionBarTitle:Ljava/lang/CharSequence;

    iput-object p3, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iput-object p4, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->title:Ljava/lang/CharSequence;

    iput-object p5, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->message:Ljava/lang/CharSequence;

    iput-boolean p6, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->showLearnMoreButton:Z

    iput-boolean p7, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->clientUpgradeRequired:Z

    iput-object p8, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->onCancelLinkResultClicked:Lkotlin/jvm/functions/Function0;

    iput-object p9, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->onPrimaryLinkButtonClicked:Lkotlin/jvm/functions/Function1;

    iput-object p10, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->onSecondaryLinkButtonClicked:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public synthetic constructor <init>(ZLjava/lang/CharSequence;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 13

    and-int/lit8 v0, p11, 0x1

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 v3, 0x0

    goto :goto_0

    :cond_0
    move v3, p1

    :goto_0
    and-int/lit8 v0, p11, 0x2

    const-string v2, ""

    if-eqz v0, :cond_1

    .line 10
    move-object v0, v2

    check-cast v0, Ljava/lang/CharSequence;

    move-object v4, v0

    goto :goto_1

    :cond_1
    move-object v4, p2

    :goto_1
    and-int/lit8 v0, p11, 0x4

    if-eqz v0, :cond_2

    .line 11
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-object v5, v0

    goto :goto_2

    :cond_2
    move-object/from16 v5, p3

    :goto_2
    and-int/lit8 v0, p11, 0x8

    if-eqz v0, :cond_3

    .line 12
    move-object v0, v2

    check-cast v0, Ljava/lang/CharSequence;

    move-object v6, v0

    goto :goto_3

    :cond_3
    move-object/from16 v6, p4

    :goto_3
    and-int/lit8 v0, p11, 0x10

    if-eqz v0, :cond_4

    .line 13
    move-object v0, v2

    check-cast v0, Ljava/lang/CharSequence;

    move-object v7, v0

    goto :goto_4

    :cond_4
    move-object/from16 v7, p5

    :goto_4
    and-int/lit8 v0, p11, 0x20

    if-eqz v0, :cond_5

    const/4 v8, 0x0

    goto :goto_5

    :cond_5
    move/from16 v8, p6

    :goto_5
    and-int/lit8 v0, p11, 0x40

    if-eqz v0, :cond_6

    const/4 v9, 0x0

    goto :goto_6

    :cond_6
    move/from16 v9, p7

    :goto_6
    move-object v2, p0

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move-object/from16 v12, p10

    .line 15
    invoke-direct/range {v2 .. v12}, Lcom/squareup/debitcard/LinkDebitCardResultScreen;-><init>(ZLjava/lang/CharSequence;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/debitcard/LinkDebitCardResultScreen;ZLjava/lang/CharSequence;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/debitcard/LinkDebitCardResultScreen;
    .locals 11

    move-object v0, p0

    move/from16 v1, p11

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-boolean v2, v0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->linkDebitCardFailed:Z

    goto :goto_0

    :cond_0
    move v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->actionBarTitle:Ljava/lang/CharSequence;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->title:Ljava/lang/CharSequence;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->message:Ljava/lang/CharSequence;

    goto :goto_4

    :cond_4
    move-object/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-boolean v7, v0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->showLearnMoreButton:Z

    goto :goto_5

    :cond_5
    move/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-boolean v8, v0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->clientUpgradeRequired:Z

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-object v9, v0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->onCancelLinkResultClicked:Lkotlin/jvm/functions/Function0;

    goto :goto_7

    :cond_7
    move-object/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget-object v10, v0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->onPrimaryLinkButtonClicked:Lkotlin/jvm/functions/Function1;

    goto :goto_8

    :cond_8
    move-object/from16 v10, p9

    :goto_8
    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_9

    iget-object v1, v0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->onSecondaryLinkButtonClicked:Lkotlin/jvm/functions/Function0;

    goto :goto_9

    :cond_9
    move-object/from16 v1, p10

    :goto_9
    move p1, v2

    move-object p2, v3

    move-object p3, v4

    move-object p4, v5

    move-object/from16 p5, v6

    move/from16 p6, v7

    move/from16 p7, v8

    move-object/from16 p8, v9

    move-object/from16 p9, v10

    move-object/from16 p10, v1

    invoke-virtual/range {p0 .. p10}, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->copy(ZLjava/lang/CharSequence;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)Lcom/squareup/debitcard/LinkDebitCardResultScreen;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->linkDebitCardFailed:Z

    return v0
.end method

.method public final component10()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->onSecondaryLinkButtonClicked:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component2()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->actionBarTitle:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component3()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object v0
.end method

.method public final component4()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->title:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component5()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->message:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->showLearnMoreButton:Z

    return v0
.end method

.method public final component7()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->clientUpgradeRequired:Z

    return v0
.end method

.method public final component8()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->onCancelLinkResultClicked:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component9()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->onPrimaryLinkButtonClicked:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final copy(ZLjava/lang/CharSequence;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)Lcom/squareup/debitcard/LinkDebitCardResultScreen;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/CharSequence;",
            "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            "ZZ",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/debitcard/LinkDebitCardResultScreen;"
        }
    .end annotation

    const-string v0, "actionBarTitle"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "glyph"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "title"

    move-object/from16 v5, p4

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    move-object/from16 v6, p5

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onCancelLinkResultClicked"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onPrimaryLinkButtonClicked"

    move-object/from16 v10, p9

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onSecondaryLinkButtonClicked"

    move-object/from16 v11, p10

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;

    move-object v1, v0

    move v2, p1

    move/from16 v7, p6

    move/from16 v8, p7

    invoke-direct/range {v1 .. v11}, Lcom/squareup/debitcard/LinkDebitCardResultScreen;-><init>(ZLjava/lang/CharSequence;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/debitcard/LinkDebitCardResultScreen;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/debitcard/LinkDebitCardResultScreen;

    iget-boolean v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->linkDebitCardFailed:Z

    iget-boolean v1, p1, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->linkDebitCardFailed:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->actionBarTitle:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->actionBarTitle:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v1, p1, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->title:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->title:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->message:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->message:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->showLearnMoreButton:Z

    iget-boolean v1, p1, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->showLearnMoreButton:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->clientUpgradeRequired:Z

    iget-boolean v1, p1, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->clientUpgradeRequired:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->onCancelLinkResultClicked:Lkotlin/jvm/functions/Function0;

    iget-object v1, p1, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->onCancelLinkResultClicked:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->onPrimaryLinkButtonClicked:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->onPrimaryLinkButtonClicked:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->onSecondaryLinkButtonClicked:Lkotlin/jvm/functions/Function0;

    iget-object p1, p1, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->onSecondaryLinkButtonClicked:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getActionBarTitle()Ljava/lang/CharSequence;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->actionBarTitle:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getClientUpgradeRequired()Z
    .locals 1

    .line 15
    iget-boolean v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->clientUpgradeRequired:Z

    return v0
.end method

.method public final getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object v0
.end method

.method public final getLinkDebitCardFailed()Z
    .locals 1

    .line 9
    iget-boolean v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->linkDebitCardFailed:Z

    return v0
.end method

.method public final getMessage()Ljava/lang/CharSequence;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->message:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getOnCancelLinkResultClicked()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 16
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->onCancelLinkResultClicked:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnPrimaryLinkButtonClicked()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->onPrimaryLinkButtonClicked:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOnSecondaryLinkButtonClicked()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 18
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->onSecondaryLinkButtonClicked:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getShowLearnMoreButton()Z
    .locals 1

    .line 14
    iget-boolean v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->showLearnMoreButton:Z

    return v0
.end method

.method public final getTitle()Ljava/lang/CharSequence;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->title:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-boolean v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->linkDebitCardFailed:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->actionBarTitle:Ljava/lang/CharSequence;

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->title:Ljava/lang/CharSequence;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->message:Ljava/lang/CharSequence;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->showLearnMoreButton:Z

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :cond_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->clientUpgradeRequired:Z

    if-eqz v2, :cond_6

    goto :goto_4

    :cond_6
    move v1, v2

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->onCancelLinkResultClicked:Lkotlin/jvm/functions/Function0;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_7
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->onPrimaryLinkButtonClicked:Lkotlin/jvm/functions/Function1;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_8
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->onSecondaryLinkButtonClicked:Lkotlin/jvm/functions/Function0;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :cond_9
    add-int/2addr v0, v3

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LinkDebitCardResultScreen(linkDebitCardFailed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->linkDebitCardFailed:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", actionBarTitle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->actionBarTitle:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", glyph="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->title:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->message:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", showLearnMoreButton="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->showLearnMoreButton:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", clientUpgradeRequired="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->clientUpgradeRequired:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", onCancelLinkResultClicked="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->onCancelLinkResultClicked:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onPrimaryLinkButtonClicked="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->onPrimaryLinkButtonClicked:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onSecondaryLinkButtonClicked="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->onSecondaryLinkButtonClicked:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
