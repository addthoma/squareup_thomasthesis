.class final Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$create$1;
.super Ljava/lang/Object;
.source "LinkDebitCardAppUpdateDialogFactory.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory;->create(Landroid/content/Context;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLinkDebitCardAppUpdateDialogFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LinkDebitCardAppUpdateDialogFactory.kt\ncom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$create$1\n*L\n1#1,60:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u001c\u0010\u0003\u001a\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Landroid/app/AlertDialog;",
        "kotlin.jvm.PlatformType",
        "it",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;
    .locals 4

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-static {p1}, Lcom/squareup/workflow/legacy/ScreenKt;->getUnwrapV2Screen(Lcom/squareup/workflow/legacy/Screen;)Lcom/squareup/workflow/legacy/V2Screen;

    move-result-object p1

    check-cast p1, Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogScreen;

    .line 27
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 28
    iget-object v1, p0, Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$create$1;->$context:Landroid/content/Context;

    sget v2, Lcom/squareup/debitcard/R$string;->play_store_intent_uri:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 30
    new-instance v1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    iget-object v2, p0, Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 31
    sget v2, Lcom/squareup/debitcard/R$string;->instant_deposits_app_update_title:I

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    .line 32
    sget v2, Lcom/squareup/debitcard/R$string;->instant_deposits_app_update_message:I

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    .line 34
    iget-object v2, p0, Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/squareup/util/Intents;->isIntentAvailable(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 35
    sget v2, Lcom/squareup/debitcard/R$string;->instant_deposits_app_update_update:I

    new-instance v3, Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$create$1$$special$$inlined$apply$lambda$1;

    invoke-direct {v3, p0, v0, p1}, Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$create$1$$special$$inlined$apply$lambda$1;-><init>(Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$create$1;Landroid/content/Intent;Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogScreen;)V

    check-cast v3, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    .line 39
    sget v2, Lcom/squareup/common/strings/R$string;->cancel:I

    new-instance v3, Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$create$1$$special$$inlined$apply$lambda$2;

    invoke-direct {v3, p0, v0, p1}, Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$create$1$$special$$inlined$apply$lambda$2;-><init>(Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$create$1;Landroid/content/Intent;Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogScreen;)V

    check-cast v3, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    goto :goto_0

    .line 43
    :cond_0
    sget v2, Lcom/squareup/common/strings/R$string;->ok:I

    new-instance v3, Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$create$1$$special$$inlined$apply$lambda$3;

    invoke-direct {v3, p0, v0, p1}, Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$create$1$$special$$inlined$apply$lambda$3;-><init>(Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$create$1;Landroid/content/Intent;Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogScreen;)V

    check-cast v3, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    .line 48
    :goto_0
    new-instance v0, Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$create$1$2;

    invoke-direct {v0, p1}, Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$create$1$2;-><init>(Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogScreen;)V

    check-cast v0, Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 51
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$create$1;->apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method
