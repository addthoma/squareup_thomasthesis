.class final Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkCard$1;
.super Ljava/lang/Object;
.source "RealLinkDebitCardWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->linkCard(Lcom/squareup/protos/client/bills/CardData;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;",
        "debitCardSettingsState",
        "Lcom/squareup/debitcard/DebitCardSettings$State;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkCard$1;->this$0:Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/debitcard/DebitCardSettings$State;)Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;
    .locals 3

    const-string v0, "debitCardSettingsState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 234
    iget-object v0, p1, Lcom/squareup/debitcard/DebitCardSettings$State;->linkCardState:Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;

    sget-object v1, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    .line 237
    iget-object v0, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkCard$1;->this$0:Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;

    invoke-static {v0, p1, v1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->access$onFailure(Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;Lcom/squareup/debitcard/DebitCardSettings$State;Z)Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;

    move-result-object p1

    goto :goto_0

    .line 236
    :cond_0
    iget-object p1, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkCard$1;->this$0:Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;

    invoke-static {p1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->access$onSuccess(Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;)Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;

    move-result-object p1

    goto :goto_0

    .line 235
    :cond_1
    iget-object p1, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkCard$1;->this$0:Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;

    invoke-static {p1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->access$onPending(Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;)Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 53
    check-cast p1, Lcom/squareup/debitcard/DebitCardSettings$State;

    invoke-virtual {p0, p1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkCard$1;->apply(Lcom/squareup/debitcard/DebitCardSettings$State;)Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;

    move-result-object p1

    return-object p1
.end method
