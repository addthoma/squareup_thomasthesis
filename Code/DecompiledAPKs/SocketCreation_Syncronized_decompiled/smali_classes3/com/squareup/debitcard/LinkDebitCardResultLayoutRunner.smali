.class public final Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;
.super Ljava/lang/Object;
.source "LinkDebitCardResultLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/debitcard/LinkDebitCardResultScreen;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u001c2\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001cB\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0002H\u0002J\u0008\u0010\u0013\u001a\u00020\u0011H\u0002J\u0010\u0010\u0014\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0002H\u0002J\u0010\u0010\u0015\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0002H\u0002J\u0010\u0010\u0016\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0002H\u0002J\u0010\u0010\u0017\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0002H\u0002J\u0018\u0010\u0018\u001a\u00020\u00112\u0006\u0010\u0019\u001a\u00020\u00022\u0006\u0010\u001a\u001a\u00020\u001bH\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/debitcard/LinkDebitCardResultScreen;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "glyphMessage",
        "Lcom/squareup/marin/widgets/MarinGlyphMessage;",
        "learnMoreButton",
        "Lcom/squareup/marketfont/MarketButton;",
        "messageContainer",
        "okButton",
        "progressBar",
        "Landroid/widget/ProgressBar;",
        "onHasResult",
        "",
        "screen",
        "onLoading",
        "onScreen",
        "progressComplete",
        "setUpActionBar",
        "showGlyphMessage",
        "showRendering",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner$Companion;


# instance fields
.field private final actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

.field private final learnMoreButton:Lcom/squareup/marketfont/MarketButton;

.field private final messageContainer:Landroid/view/View;

.field private final okButton:Lcom/squareup/marketfont/MarketButton;

.field private final progressBar:Landroid/widget/ProgressBar;

.field private final view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->Companion:Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->view:Landroid/view/View;

    .line 25
    iget-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->view:Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    const-string v0, "ActionBarView.findIn(view)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 27
    iget-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/debitcard/impl/R$id;->instant_deposits_link_debit_card_progress:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.i\u2026link_debit_card_progress)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/ProgressBar;

    iput-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->progressBar:Landroid/widget/ProgressBar;

    .line 29
    iget-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/debitcard/impl/R$id;->instant_deposits_link_debit_card_glyph_message:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.i\u2026debit_card_glyph_message)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iput-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    .line 31
    iget-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/debitcard/impl/R$id;->instant_deposits_link_debit_card_ok_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.i\u2026ink_debit_card_ok_button)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/marketfont/MarketButton;

    iput-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->okButton:Lcom/squareup/marketfont/MarketButton;

    .line 33
    iget-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/debitcard/impl/R$id;->instant_deposits_link_debit_card_learn_more_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.i\u2026t_card_learn_more_button)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/marketfont/MarketButton;

    iput-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->learnMoreButton:Lcom/squareup/marketfont/MarketButton;

    .line 34
    iget-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/debitcard/impl/R$id;->instant_deposits_message_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.i\u2026posits_message_container)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->messageContainer:Landroid/view/View;

    return-void
.end method

.method private final onHasResult(Lcom/squareup/debitcard/LinkDebitCardResultScreen;)V
    .locals 1

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->setUpActionBar(Lcom/squareup/debitcard/LinkDebitCardResultScreen;)V

    .line 62
    invoke-virtual {p1}, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->getClientUpgradeRequired()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    iget-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->progressBar:Landroid/widget/ProgressBar;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 65
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->showGlyphMessage(Lcom/squareup/debitcard/LinkDebitCardResultScreen;)V

    :goto_0
    return-void
.end method

.method private final onLoading()V
    .locals 2

    .line 54
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->messageContainer:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 55
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->okButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    .line 56
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->progressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method

.method private final onScreen(Lcom/squareup/debitcard/LinkDebitCardResultScreen;)V
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->view:Landroid/view/View;

    new-instance v1, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner$onScreen$1;

    invoke-direct {v1, p1}, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner$onScreen$1;-><init>(Lcom/squareup/debitcard/LinkDebitCardResultScreen;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 46
    invoke-virtual {p1}, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->getActionBarTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    invoke-direct {p0}, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->onLoading()V

    goto :goto_0

    .line 49
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->onHasResult(Lcom/squareup/debitcard/LinkDebitCardResultScreen;)V

    :goto_0
    return-void
.end method

.method private final progressComplete(Lcom/squareup/debitcard/LinkDebitCardResultScreen;)V
    .locals 3

    .line 85
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->progressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 86
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->messageContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 88
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->okButton:Lcom/squareup/marketfont/MarketButton;

    .line 89
    new-instance v2, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner$progressComplete$1;

    invoke-direct {v2, p1}, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner$progressComplete$1;-><init>(Lcom/squareup/debitcard/LinkDebitCardResultScreen;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-static {v2}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v2

    check-cast v2, Landroid/view/View$OnClickListener;

    .line 88
    invoke-virtual {v0, v2}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    invoke-virtual {p1}, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->getShowLearnMoreButton()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->okButton:Lcom/squareup/marketfont/MarketButton;

    sget v2, Lcom/squareup/debitcard/R$string;->instant_deposits_try_another_debit_card:I

    invoke-virtual {v0, v2}, Lcom/squareup/marketfont/MarketButton;->setText(I)V

    goto :goto_0

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->okButton:Lcom/squareup/marketfont/MarketButton;

    sget v2, Lcom/squareup/common/strings/R$string;->ok:I

    invoke-virtual {v0, v2}, Lcom/squareup/marketfont/MarketButton;->setText(I)V

    .line 97
    :goto_0
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->okButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    .line 99
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->learnMoreButton:Lcom/squareup/marketfont/MarketButton;

    .line 100
    new-instance v1, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner$progressComplete$2;

    invoke-direct {v1, p1}, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner$progressComplete$2;-><init>(Lcom/squareup/debitcard/LinkDebitCardResultScreen;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    check-cast v1, Landroid/view/View$OnClickListener;

    .line 99
    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->learnMoreButton:Lcom/squareup/marketfont/MarketButton;

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->getShowLearnMoreButton()Z

    move-result p1

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method private final setUpActionBar(Lcom/squareup/debitcard/LinkDebitCardResultScreen;)V
    .locals 4

    .line 73
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 70
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 71
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1}, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->getActionBarTitle()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 72
    new-instance v2, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner$setUpActionBar$1;

    invoke-direct {v2, p1}, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner$setUpActionBar$1;-><init>(Lcom/squareup/debitcard/LinkDebitCardResultScreen;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 73
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final showGlyphMessage(Lcom/squareup/debitcard/LinkDebitCardResultScreen;)V
    .locals 2

    .line 77
    invoke-direct {p0, p1}, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->progressComplete(Lcom/squareup/debitcard/LinkDebitCardResultScreen;)V

    .line 79
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {p1}, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {p1}, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setTitle(Ljava/lang/CharSequence;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {p1}, Lcom/squareup/debitcard/LinkDebitCardResultScreen;->getMessage()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setMessage(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/debitcard/LinkDebitCardResultScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object p2, p0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->view:Landroid/view/View;

    sget-object v0, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner$showRendering$1;->INSTANCE:Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner$showRendering$1;

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 41
    invoke-direct {p0, p1}, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->onScreen(Lcom/squareup/debitcard/LinkDebitCardResultScreen;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/debitcard/LinkDebitCardResultScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/debitcard/LinkDebitCardResultLayoutRunner;->showRendering(Lcom/squareup/debitcard/LinkDebitCardResultScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
