.class public final Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealLinkDebitCardWorkflow.kt"

# interfaces
.implements Lcom/squareup/debitcard/LinkDebitCardWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;",
        "Lcom/squareup/debitcard/LinkDebitCardState;",
        "Lkotlin/Unit;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/debitcard/LinkDebitCardWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealLinkDebitCardWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealLinkDebitCardWorkflow.kt\ncom/squareup/debitcard/RealLinkDebitCardWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 5 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 6 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,339:1\n32#2,12:340\n85#3:352\n85#3:355\n240#4:353\n240#4:356\n276#5:354\n276#5:357\n149#6,5:358\n149#6,5:363\n149#6,5:368\n149#6,5:373\n149#6,5:378\n149#6,5:383\n*E\n*S KotlinDebug\n*F\n+ 1 RealLinkDebitCardWorkflow.kt\ncom/squareup/debitcard/RealLinkDebitCardWorkflow\n*L\n84#1,12:340\n100#1:352\n108#1:355\n100#1:353\n108#1:356\n100#1:354\n108#1:357\n119#1,5:358\n134#1,5:363\n149#1,5:368\n181#1,5:373\n200#1,5:378\n223#1,5:383\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0090\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0008\u0018\u00002<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u00012\u00020\n:\u0001=B/\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u00a2\u0006\u0002\u0010\u0015J8\u0010\u0016\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00082\u0006\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u00032\u0012\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u001aH\u0002J.\u0010\u001b\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00082\u0018\u0010\u001c\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u001e0\u001dH\u0002J\u0008\u0010\u001f\u001a\u00020\u0004H\u0002J\u001a\u0010 \u001a\u00020\u00032\u0006\u0010\u0017\u001a\u00020\u00022\u0008\u0010!\u001a\u0004\u0018\u00010\"H\u0016J\u0016\u0010#\u001a\u0008\u0012\u0004\u0012\u00020%0$2\u0006\u0010&\u001a\u00020\'H\u0002J6\u0010(\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00082\u0006\u0010\u0017\u001a\u00020\u00022\u0018\u0010\u001c\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u001e0\u001dH\u0002J6\u0010)\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00082\u0018\u0010\u001c\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u001e0\u001d2\u0006\u0010\u0018\u001a\u00020*H\u0002J.\u0010+\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00082\u0018\u0010\u001c\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u001e0\u001dH\u0002J.\u0010,\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00082\u0018\u0010\u001c\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u001e0\u001dH\u0002J6\u0010-\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00082\u0006\u0010\u0018\u001a\u00020%2\u0018\u0010\u001c\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u001e0\u001dH\u0002J.\u0010.\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00082\u0018\u0010\u001c\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u001e0\u001dH\u0002J\u0018\u0010/\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u00022\u0006\u00100\u001a\u00020%H\u0002J\u0010\u00101\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u0002H\u0002J\u0018\u00102\u001a\u00020%2\u0006\u00103\u001a\u0002042\u0006\u00105\u001a\u000206H\u0002J\u0008\u00107\u001a\u00020%H\u0002J\u0008\u00108\u001a\u00020%H\u0002JN\u00109\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u00032\u0012\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u001aH\u0016J\u000e\u0010:\u001a\u0008\u0012\u0004\u0012\u00020%0$H\u0002J\u0010\u0010;\u001a\u00020\"2\u0006\u0010\u0018\u001a\u00020\u0003H\u0016J\u0010\u0010<\u001a\u0002062\u0006\u0010\u0017\u001a\u00020\u0002H\u0002R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006>"
    }
    d2 = {
        "Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;",
        "Lcom/squareup/debitcard/LinkDebitCardState;",
        "",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "Lcom/squareup/debitcard/LinkDebitCardWorkflow;",
        "res",
        "Landroid/content/res/Resources;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "analytics",
        "Lcom/squareup/instantdeposit/InstantDepositAnalytics;",
        "debitCardSettings",
        "Lcom/squareup/debitcard/DebitCardSettings;",
        "browserLauncher",
        "Lcom/squareup/util/BrowserLauncher;",
        "(Landroid/content/res/Resources;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/instantdeposit/InstantDepositAnalytics;Lcom/squareup/debitcard/DebitCardSettings;Lcom/squareup/util/BrowserLauncher;)V",
        "cardScreen",
        "props",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "dialogScreen",
        "sink",
        "Lcom/squareup/workflow/Sink;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "enableInstantDeposits",
        "initialState",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "linkCard",
        "Lio/reactivex/Single;",
        "Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;",
        "cardData",
        "Lcom/squareup/protos/client/bills/CardData;",
        "linkDebitCardEntryScreen",
        "linkDebitCardFailureScreen",
        "Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;",
        "linkDebitCardLoadingScreen",
        "linkDebitCardPendingScreen",
        "linkDebitCardResultScreen",
        "linkDebitCardSuccessScreen",
        "logLinkDebitCard",
        "linkResult",
        "logLinkDebitCardCancel",
        "onFailure",
        "debitCardSettingsState",
        "Lcom/squareup/debitcard/DebitCardSettings$State;",
        "linkDebitCardFailed",
        "",
        "onPending",
        "onSuccess",
        "render",
        "resendEmail",
        "snapshotState",
        "startedFromDepositsApplet",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

.field private final browserLauncher:Lcom/squareup/util/BrowserLauncher;

.field private final debitCardSettings:Lcom/squareup/debitcard/DebitCardSettings;

.field private final res:Landroid/content/res/Resources;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/instantdeposit/InstantDepositAnalytics;Lcom/squareup/debitcard/DebitCardSettings;Lcom/squareup/util/BrowserLauncher;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "debitCardSettings"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "browserLauncher"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->res:Landroid/content/res/Resources;

    iput-object p2, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p3, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    iput-object p4, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->debitCardSettings:Lcom/squareup/debitcard/DebitCardSettings;

    iput-object p5, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    return-void
.end method

.method public static final synthetic access$getBrowserLauncher$p(Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;)Lcom/squareup/util/BrowserLauncher;
    .locals 0

    .line 53
    iget-object p0, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    return-object p0
.end method

.method public static final synthetic access$logLinkDebitCard(Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;)V
    .locals 0

    .line 53
    invoke-direct {p0, p1, p2}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->logLinkDebitCard(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;)V

    return-void
.end method

.method public static final synthetic access$logLinkDebitCardCancel(Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;)V
    .locals 0

    .line 53
    invoke-direct {p0, p1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->logLinkDebitCardCancel(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;)V

    return-void
.end method

.method public static final synthetic access$onFailure(Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;Lcom/squareup/debitcard/DebitCardSettings$State;Z)Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;
    .locals 0

    .line 53
    invoke-direct {p0, p1, p2}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->onFailure(Lcom/squareup/debitcard/DebitCardSettings$State;Z)Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onPending(Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;)Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;
    .locals 0

    .line 53
    invoke-direct {p0}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->onPending()Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onSuccess(Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;)Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;
    .locals 0

    .line 53
    invoke-direct {p0}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->onSuccess()Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;

    move-result-object p0

    return-object p0
.end method

.method private final cardScreen(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;Lcom/squareup/debitcard/LinkDebitCardState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;",
            "Lcom/squareup/debitcard/LinkDebitCardState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/debitcard/LinkDebitCardState;",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    .line 98
    sget-object v0, Lcom/squareup/debitcard/LinkDebitCardState$PrepareToLinkCard;->INSTANCE:Lcom/squareup/debitcard/LinkDebitCardState$PrepareToLinkCard;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->linkDebitCardEntryScreen(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto/16 :goto_0

    .line 99
    :cond_0
    instance-of v0, p2, Lcom/squareup/debitcard/LinkDebitCardState$LinkingCard;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 100
    check-cast p2, Lcom/squareup/debitcard/LinkDebitCardState$LinkingCard;

    invoke-virtual {p2}, Lcom/squareup/debitcard/LinkDebitCardState$LinkingCard;->getCardData()Lcom/squareup/protos/client/bills/CardData;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->linkCard(Lcom/squareup/protos/client/bills/CardData;)Lio/reactivex/Single;

    move-result-object p2

    .line 352
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$cardScreen$$inlined$asWorker$1;

    invoke-direct {v0, p2, v1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$cardScreen$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 353
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p2

    .line 354
    const-class v0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p2}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v3, v1

    check-cast v3, Lcom/squareup/workflow/Worker;

    const/4 v4, 0x0

    .line 100
    new-instance p2, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$cardScreen$1;

    invoke-direct {p2, p0, p1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$cardScreen$1;-><init>(Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;)V

    move-object v5, p2

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v2, p3

    invoke-static/range {v2 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 104
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->linkDebitCardLoadingScreen(Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 106
    :cond_1
    instance-of p1, p2, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;

    if-eqz p1, :cond_2

    check-cast p2, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;

    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p1

    invoke-direct {p0, p2, p1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->linkDebitCardResultScreen(Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 107
    :cond_2
    sget-object p1, Lcom/squareup/debitcard/LinkDebitCardState$ResendingEmail;->INSTANCE:Lcom/squareup/debitcard/LinkDebitCardState$ResendingEmail;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 108
    invoke-direct {p0}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->resendEmail()Lio/reactivex/Single;

    move-result-object p1

    .line 355
    sget-object p2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance p2, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$cardScreen$$inlined$asWorker$2;

    invoke-direct {p2, p1, v1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$cardScreen$$inlined$asWorker$2;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    .line 356
    invoke-static {p2}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 357
    const-class p2, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object p2

    new-instance v0, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v0, p2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/Worker;

    const/4 v3, 0x0

    .line 108
    sget-object p1, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$cardScreen$2;->INSTANCE:Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$cardScreen$2;

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, p3

    invoke-static/range {v1 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 111
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->linkDebitCardLoadingScreen(Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final dialogScreen(Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/debitcard/LinkDebitCardState;",
            "Lkotlin/Unit;",
            ">;>;)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    .line 116
    new-instance v0, Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogScreen;

    .line 117
    new-instance v1, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$dialogScreen$1;

    invoke-direct {v1, p1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$dialogScreen$1;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 118
    new-instance v2, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$dialogScreen$2;

    invoke-direct {v2, p1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$dialogScreen$2;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 116
    invoke-direct {v0, v1, v2}, Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogScreen;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 359
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 360
    const-class v1, Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogScreen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 361
    sget-object v2, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v2}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v2

    .line 359
    invoke-direct {p1, v1, v0, v2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method

.method private final enableInstantDeposits()V
    .locals 2

    .line 286
    iget-object v0, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getInstantDepositsSettings()Lcom/squareup/settings/server/InstantDepositsSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/settings/server/InstantDepositsSettings;->allowInstantDeposit(Z)V

    .line 287
    iget-object v0, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->refresh()V

    return-void
.end method

.method private final linkCard(Lcom/squareup/protos/client/bills/CardData;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/CardData;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;",
            ">;"
        }
    .end annotation

    .line 227
    new-instance v0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;-><init>()V

    .line 228
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->request_uuid(Ljava/lang/String;)Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;

    move-result-object v0

    .line 229
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->card_data(Lcom/squareup/protos/client/bills/CardData;)Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;

    move-result-object p1

    .line 230
    invoke-virtual {p1}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->build()Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;

    move-result-object p1

    .line 232
    iget-object v0, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->debitCardSettings:Lcom/squareup/debitcard/DebitCardSettings;

    const-string v1, "linkCardRequest"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/debitcard/DebitCardSettings;->linkCard(Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;)Lio/reactivex/Single;

    move-result-object p1

    .line 233
    new-instance v0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkCard$1;

    invoke-direct {v0, p0}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkCard$1;-><init>(Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "debitCardSettings.linkCa\u2026  )\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final linkDebitCardEntryScreen(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/debitcard/LinkDebitCardState;",
            "Lkotlin/Unit;",
            ">;>;)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    .line 126
    new-instance v0, Lcom/squareup/debitcard/LinkDebitCardEntryScreen;

    .line 127
    iget-object v1, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    const-string v2, "settings.userSettings"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v1

    const-string v2, "settings.userSettings.countryCode"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    iget-object v2, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getInstantDepositsSettings()Lcom/squareup/settings/server/InstantDepositsSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/settings/server/InstantDepositsSettings;->hasLinkedCard()Z

    move-result v2

    .line 129
    new-instance v3, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardEntryScreen$1;

    invoke-direct {v3, p0, p1, p2}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardEntryScreen$1;-><init>(Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;Lcom/squareup/workflow/Sink;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    .line 133
    new-instance p1, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardEntryScreen$2;

    invoke-direct {p1, p2}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardEntryScreen$2;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    .line 126
    invoke-direct {v0, v1, v2, v3, p1}, Lcom/squareup/debitcard/LinkDebitCardEntryScreen;-><init>(Lcom/squareup/CountryCode;ZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 364
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 365
    const-class p2, Lcom/squareup/debitcard/LinkDebitCardEntryScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string v1, ""

    invoke-static {p2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 366
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 364
    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method

.method private final linkDebitCardFailureScreen(Lcom/squareup/workflow/Sink;Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;)Lcom/squareup/workflow/legacy/Screen;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/debitcard/LinkDebitCardState;",
            "Lkotlin/Unit;",
            ">;>;",
            "Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    .line 207
    new-instance v11, Lcom/squareup/debitcard/LinkDebitCardResultScreen;

    .line 208
    invoke-virtual {p2}, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->getLinkDebitCardFailed()Z

    move-result v1

    .line 209
    invoke-virtual {p2}, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->getTitle()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/lang/CharSequence;

    .line 210
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 211
    invoke-virtual {p2}, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->getTitle()Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/lang/CharSequence;

    .line 212
    invoke-virtual {p2}, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->getMessage()Ljava/lang/String;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/lang/CharSequence;

    .line 213
    invoke-virtual {p2}, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->getCardUnsupported()Z

    move-result v6

    .line 214
    invoke-virtual {p2}, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->getClientUpgradeRequired()Z

    move-result v7

    .line 215
    new-instance p2, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardFailureScreen$1;

    invoke-direct {p2, p1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardFailureScreen$1;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v8, p2

    check-cast v8, Lkotlin/jvm/functions/Function0;

    .line 216
    new-instance p2, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardFailureScreen$2;

    invoke-direct {p2, p1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardFailureScreen$2;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v9, p2

    check-cast v9, Lkotlin/jvm/functions/Function1;

    .line 219
    new-instance p1, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardFailureScreen$3;

    invoke-direct {p1, p0}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardFailureScreen$3;-><init>(Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;)V

    move-object v10, p1

    check-cast v10, Lkotlin/jvm/functions/Function0;

    move-object v0, v11

    .line 207
    invoke-direct/range {v0 .. v10}, Lcom/squareup/debitcard/LinkDebitCardResultScreen;-><init>(ZLjava/lang/CharSequence;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    check-cast v11, Lcom/squareup/workflow/legacy/V2Screen;

    .line 384
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 385
    const-class p2, Lcom/squareup/debitcard/LinkDebitCardResultScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string v0, ""

    invoke-static {p2, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 386
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 384
    invoke-direct {p1, p2, v11, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method

.method private final linkDebitCardLoadingScreen(Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/debitcard/LinkDebitCardState;",
            "Lkotlin/Unit;",
            ">;>;)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    .line 140
    new-instance v13, Lcom/squareup/debitcard/LinkDebitCardResultScreen;

    .line 141
    new-instance v0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardLoadingScreen$1;

    invoke-direct {v0, p1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardLoadingScreen$1;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v8, v0

    check-cast v8, Lkotlin/jvm/functions/Function0;

    .line 142
    new-instance v0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardLoadingScreen$2;

    invoke-direct {v0, p1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardLoadingScreen$2;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v9, v0

    check-cast v9, Lkotlin/jvm/functions/Function1;

    .line 145
    new-instance p1, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardLoadingScreen$3;

    invoke-direct {p1, p0}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardLoadingScreen$3;-><init>(Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;)V

    move-object v10, p1

    check-cast v10, Lkotlin/jvm/functions/Function0;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v11, 0x7f

    const/4 v12, 0x0

    move-object v0, v13

    .line 140
    invoke-direct/range {v0 .. v12}, Lcom/squareup/debitcard/LinkDebitCardResultScreen;-><init>(ZLjava/lang/CharSequence;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v13, Lcom/squareup/workflow/legacy/V2Screen;

    .line 369
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 370
    const-class v0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    .line 371
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 369
    invoke-direct {p1, v0, v13, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method

.method private final linkDebitCardPendingScreen(Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/debitcard/LinkDebitCardState;",
            "Lkotlin/Unit;",
            ">;>;)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    .line 164
    iget-object v0, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->res:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/debitcard/R$string;->instant_deposits_verification_email_sent:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 165
    iget-object v1, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    const-string v2, "settings.userSettings"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getEmail()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    const-string v2, "email"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 166
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v5

    .line 168
    new-instance v0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;

    .line 169
    iget-object v1, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->res:Landroid/content/res/Resources;

    sget v2, Lcom/squareup/debitcard/R$string;->instant_deposits_check_your_inbox:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    const-string v1, "res.getText(R.string.ins\u2026eposits_check_your_inbox)"

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 170
    sget-object v4, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_ENVELOPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string/jumbo v1, "title"

    .line 171
    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 172
    iget-object v1, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->res:Landroid/content/res/Resources;

    sget v2, Lcom/squareup/debitcard/R$string;->instant_deposits_verification_email_sent_message:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    const-string v1, "res.getText(R.string.ins\u2026ation_email_sent_message)"

    invoke-static {v6, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    new-instance v1, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardPendingScreen$1;

    invoke-direct {v1, p1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardPendingScreen$1;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v9, v1

    check-cast v9, Lkotlin/jvm/functions/Function0;

    .line 174
    new-instance v1, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardPendingScreen$2;

    invoke-direct {v1, p1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardPendingScreen$2;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v10, v1

    check-cast v10, Lkotlin/jvm/functions/Function1;

    .line 177
    new-instance p1, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardPendingScreen$3;

    invoke-direct {p1, p0}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardPendingScreen$3;-><init>(Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;)V

    move-object v11, p1

    check-cast v11, Lkotlin/jvm/functions/Function0;

    const/4 v2, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v12, 0x61

    const/4 v13, 0x0

    move-object v1, v0

    .line 168
    invoke-direct/range {v1 .. v13}, Lcom/squareup/debitcard/LinkDebitCardResultScreen;-><init>(ZLjava/lang/CharSequence;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 374
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 375
    const-class v1, Lcom/squareup/debitcard/LinkDebitCardResultScreen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 376
    sget-object v2, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v2}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v2

    .line 374
    invoke-direct {p1, v1, v0, v2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method

.method private final linkDebitCardResultScreen(Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/debitcard/LinkDebitCardState;",
            "Lkotlin/Unit;",
            ">;>;)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    .line 156
    sget-object v0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Pending;->INSTANCE:Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Pending;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p2}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->linkDebitCardPendingScreen(Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 157
    :cond_0
    sget-object v0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Success;->INSTANCE:Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Success;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p2}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->linkDebitCardSuccessScreen(Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 158
    :cond_1
    instance-of v0, p1, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;

    invoke-direct {p0, p2, p1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->linkDebitCardFailureScreen(Lcom/squareup/workflow/Sink;Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final linkDebitCardSuccessScreen(Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/debitcard/LinkDebitCardState;",
            "Lkotlin/Unit;",
            ">;>;)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    .line 187
    new-instance v13, Lcom/squareup/debitcard/LinkDebitCardResultScreen;

    .line 188
    iget-object v0, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->res:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/debitcard/R$string;->instant_deposits_card_linked:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    const-string v0, "res.getText(R.string.instant_deposits_card_linked)"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 189
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 190
    iget-object v1, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->res:Landroid/content/res/Resources;

    sget v4, Lcom/squareup/debitcard/R$string;->instant_deposits_card_linked:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 191
    iget-object v0, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->res:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/debitcard/R$string;->instant_deposits_card_linked_message:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    const-string v0, "res.getText(R.string.ins\u2026sits_card_linked_message)"

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 192
    new-instance v0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardSuccessScreen$1;

    invoke-direct {v0, p1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardSuccessScreen$1;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v8, v0

    check-cast v8, Lkotlin/jvm/functions/Function0;

    .line 193
    new-instance v0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardSuccessScreen$2;

    invoke-direct {v0, p1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardSuccessScreen$2;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v9, v0

    check-cast v9, Lkotlin/jvm/functions/Function1;

    .line 196
    new-instance p1, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardSuccessScreen$3;

    invoke-direct {p1, p0}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$linkDebitCardSuccessScreen$3;-><init>(Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;)V

    move-object v10, p1

    check-cast v10, Lkotlin/jvm/functions/Function0;

    const/4 v1, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v11, 0x61

    const/4 v12, 0x0

    move-object v0, v13

    .line 187
    invoke-direct/range {v0 .. v12}, Lcom/squareup/debitcard/LinkDebitCardResultScreen;-><init>(ZLjava/lang/CharSequence;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v13, Lcom/squareup/workflow/legacy/V2Screen;

    .line 379
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 380
    const-class v0, Lcom/squareup/debitcard/LinkDebitCardResultScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    .line 381
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 379
    invoke-direct {p1, v0, v13, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method

.method private final logLinkDebitCard(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;)V
    .locals 1

    .line 304
    invoke-direct {p0, p1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->startedFromDepositsApplet(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 306
    sget-object p1, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Success;->INSTANCE:Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Success;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 307
    :cond_0
    sget-object p1, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Pending;->INSTANCE:Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Pending;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 308
    :goto_0
    iget-object p1, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    const/4 p2, 0x1

    invoke-interface {p1, p2}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->logBalanceAppletAddDebitCardAttempt(Z)V

    .line 309
    iget-object p1, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {p1}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->logBalanceAppletAddDebitCardSuccess()V

    goto :goto_1

    .line 311
    :cond_1
    instance-of p1, p2, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;

    if-eqz p1, :cond_2

    .line 312
    iget-object p1, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    check-cast p2, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;

    invoke-virtual {p2}, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->getAttemptFailed()Z

    move-result v0

    invoke-interface {p1, v0}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->logBalanceAppletAddDebitCardAttempt(Z)V

    .line 313
    iget-object p1, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-virtual {p2}, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->getMessage()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->logBalanceAppletAddDebitCardFailure(Ljava/lang/String;)V

    :cond_2
    :goto_1
    return-void
.end method

.method private final logLinkDebitCardCancel(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;)V
    .locals 0

    .line 295
    invoke-direct {p0, p1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->startedFromDepositsApplet(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 296
    iget-object p1, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {p1}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->logBalanceAppletLinkDebitCardCancel()V

    :cond_0
    return-void
.end method

.method private final onFailure(Lcom/squareup/debitcard/DebitCardSettings$State;Z)Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;
    .locals 8

    .line 273
    new-instance v7, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;

    .line 274
    iget-object v0, p1, Lcom/squareup/debitcard/DebitCardSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 275
    iget-object v0, p1, Lcom/squareup/debitcard/DebitCardSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object v2

    .line 276
    iget-boolean v3, p1, Lcom/squareup/debitcard/DebitCardSettings$State;->cardUnsupported:Z

    .line 277
    iget-boolean v4, p1, Lcom/squareup/debitcard/DebitCardSettings$State;->clientUpgradeRequired:Z

    .line 278
    iget-boolean v5, p1, Lcom/squareup/debitcard/DebitCardSettings$State;->attemptFailed:Z

    move-object v0, v7

    move v6, p2

    .line 273
    invoke-direct/range {v0 .. v6}, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;-><init>(Ljava/lang/String;Ljava/lang/String;ZZZZ)V

    check-cast v7, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;

    return-object v7
.end method

.method private final onPending()Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;
    .locals 1

    .line 260
    invoke-direct {p0}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->enableInstantDeposits()V

    .line 261
    sget-object v0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Pending;->INSTANCE:Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Pending;

    check-cast v0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;

    return-object v0
.end method

.method private final onSuccess()Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;
    .locals 1

    .line 265
    invoke-direct {p0}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->enableInstantDeposits()V

    .line 266
    sget-object v0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Success;->INSTANCE:Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Success;

    check-cast v0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;

    return-object v0
.end method

.method private final resendEmail()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;",
            ">;"
        }
    .end annotation

    .line 246
    iget-object v0, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->debitCardSettings:Lcom/squareup/debitcard/DebitCardSettings;

    invoke-interface {v0}, Lcom/squareup/debitcard/DebitCardSettings;->resendEmail()Lio/reactivex/Single;

    move-result-object v0

    .line 247
    new-instance v1, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$resendEmail$1;

    invoke-direct {v1, p0}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$resendEmail$1;-><init>(Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "debitCardSettings.resend\u2026  )\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final startedFromDepositsApplet(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;)Z
    .locals 1

    .line 291
    instance-of v0, p1, Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartWithPrepareToLinkCard;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartWithPrepareToLinkCard;

    invoke-virtual {p1}, Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartWithPrepareToLinkCard;->getStartedFrom()Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartedFrom;

    move-result-object p1

    sget-object v0, Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartedFrom;->DEPOSITS_APPLET:Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartedFrom;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/debitcard/LinkDebitCardState;
    .locals 3

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 340
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p2}, Lokio/ByteString;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, v2

    :goto_1
    if-eqz p2, :cond_3

    .line 345
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    const-string v2, "Parcel.obtain()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 346
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 347
    array-length v2, p2

    invoke-virtual {v0, p2, v1, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 348
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 349
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p2, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v2, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 350
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 351
    :cond_3
    check-cast v2, Lcom/squareup/debitcard/LinkDebitCardState;

    if-eqz v2, :cond_4

    goto :goto_2

    .line 86
    :cond_4
    instance-of p2, p1, Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartWithPrepareToLinkCard;

    if-eqz p2, :cond_5

    sget-object p1, Lcom/squareup/debitcard/LinkDebitCardState$PrepareToLinkCard;->INSTANCE:Lcom/squareup/debitcard/LinkDebitCardState$PrepareToLinkCard;

    move-object v2, p1

    check-cast v2, Lcom/squareup/debitcard/LinkDebitCardState;

    goto :goto_2

    .line 87
    :cond_5
    sget-object p2, Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartWithResendingEmail;->INSTANCE:Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartWithResendingEmail;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_6

    sget-object p1, Lcom/squareup/debitcard/LinkDebitCardState$ResendingEmail;->INSTANCE:Lcom/squareup/debitcard/LinkDebitCardState$ResendingEmail;

    move-object v2, p1

    check-cast v2, Lcom/squareup/debitcard/LinkDebitCardState;

    :goto_2
    return-object v2

    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 53
    check-cast p1, Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->initialState(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/debitcard/LinkDebitCardState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 53
    check-cast p1, Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;

    check-cast p2, Lcom/squareup/debitcard/LinkDebitCardState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->render(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;Lcom/squareup/debitcard/LinkDebitCardState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;Lcom/squareup/debitcard/LinkDebitCardState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;",
            "Lcom/squareup/debitcard/LinkDebitCardState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/debitcard/LinkDebitCardState;",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    instance-of v0, p2, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;

    invoke-virtual {v0}, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->getClientUpgradeRequired()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    sget-object v0, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    const/4 v1, 0x2

    new-array v1, v1, [Lkotlin/Pair;

    const/4 v2, 0x0

    .line 72
    sget-object v3, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->cardScreen(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;Lcom/squareup/debitcard/LinkDebitCardState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    invoke-static {v3, p1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p1

    aput-object p1, v1, v2

    const/4 p1, 0x1

    .line 73
    sget-object p2, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p3

    invoke-direct {p0, p3}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->dialogScreen(Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p3

    invoke-static {p2, p3}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p2

    aput-object p2, v1, p1

    .line 71
    invoke-virtual {v0, v1}, Lcom/squareup/container/PosLayering$Companion;->partial([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 76
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->cardScreen(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;Lcom/squareup/debitcard/LinkDebitCardState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    sget-object p2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public snapshotState(Lcom/squareup/debitcard/LinkDebitCardState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 53
    check-cast p1, Lcom/squareup/debitcard/LinkDebitCardState;

    invoke-virtual {p0, p1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->snapshotState(Lcom/squareup/debitcard/LinkDebitCardState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
