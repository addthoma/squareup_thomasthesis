.class final Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge$badge$1;
.super Ljava/lang/Object;
.source "RealNotificationCenterBadge.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge;->badge()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/ObservableSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealNotificationCenterBadge.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealNotificationCenterBadge.kt\ncom/squareup/notificationcenterbadge/RealNotificationCenterBadge$badge$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,62:1\n1550#2,3:63\n704#2:66\n777#2,2:67\n*E\n*S KotlinDebug\n*F\n+ 1 RealNotificationCenterBadge.kt\ncom/squareup/notificationcenterbadge/RealNotificationCenterBadge$badge$1\n*L\n26#1,3:63\n27#1:66\n27#1,2:67\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a*\u0012\u000e\u0008\u0001\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0014\u0012\u000e\u0008\u0001\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeState;",
        "kotlin.jvm.PlatformType",
        "importantResult",
        "Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge;


# direct methods
.method constructor <init>(Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge$badge$1;->this$0:Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;)Lio/reactivex/Observable;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;",
            ")",
            "Lio/reactivex/Observable<",
            "+",
            "Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeState;",
            ">;"
        }
    .end annotation

    const-string v0, "importantResult"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;->getNotifications()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 63
    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 64
    :cond_1
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/notificationcenterdata/Notification;

    .line 26
    iget-object v4, p0, Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge$badge$1;->this$0:Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge;

    invoke-static {v4, v1}, Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge;->access$isCritical$p(Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge;Lcom/squareup/notificationcenterdata/Notification;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    .line 27
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;->getNotifications()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 66
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 67
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_3
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Lcom/squareup/notificationcenterdata/Notification;

    .line 27
    invoke-virtual {v5}, Lcom/squareup/notificationcenterdata/Notification;->getState()Lcom/squareup/notificationcenterdata/Notification$State;

    move-result-object v5

    sget-object v6, Lcom/squareup/notificationcenterdata/Notification$State;->UNREAD:Lcom/squareup/notificationcenterdata/Notification$State;

    if-ne v5, v6, :cond_4

    const/4 v5, 0x1

    goto :goto_2

    :cond_4
    const/4 v5, 0x0

    :goto_2
    if-eqz v5, :cond_3

    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 68
    :cond_5
    check-cast v1, Ljava/util/List;

    if-eqz v0, :cond_6

    .line 30
    sget-object p1, Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeState$ShowCritical;->INSTANCE:Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeState$ShowCritical;

    invoke-static {p1}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    goto :goto_3

    .line 32
    :cond_6
    move-object p1, v1

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/2addr p1, v2

    if-eqz p1, :cond_7

    .line 34
    new-instance p1, Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeState$ShowImportant;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {p1, v0}, Lcom/squareup/notificationcenterbadge/NotificationCenterBadgeState$ShowImportant;-><init>(I)V

    .line 33
    invoke-static {p1}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    goto :goto_3

    .line 38
    :cond_7
    iget-object p1, p0, Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge$badge$1;->this$0:Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge;

    invoke-static {p1}, Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge;->access$getNotificationsRepository$p(Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge;)Lcom/squareup/notificationcenterdata/NotificationsRepository;

    move-result-object p1

    .line 39
    sget-object v0, Lcom/squareup/notificationcenterdata/Notification$Priority$General;->INSTANCE:Lcom/squareup/notificationcenterdata/Notification$Priority$General;

    check-cast v0, Lcom/squareup/notificationcenterdata/Notification$Priority;

    invoke-interface {p1, v0}, Lcom/squareup/notificationcenterdata/NotificationsRepository;->unreadNotificationCount(Lcom/squareup/notificationcenterdata/Notification$Priority;)Lio/reactivex/Observable;

    move-result-object p1

    .line 40
    sget-object v0, Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge$badge$1$1;->INSTANCE:Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge$badge$1$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    :goto_3
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;

    invoke-virtual {p0, p1}, Lcom/squareup/notificationcenterbadge/RealNotificationCenterBadge$badge$1;->apply(Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
