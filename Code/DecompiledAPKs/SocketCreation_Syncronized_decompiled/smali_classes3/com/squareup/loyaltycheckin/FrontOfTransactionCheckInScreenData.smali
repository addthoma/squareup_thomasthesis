.class public abstract Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;
.super Ljava/lang/Object;
.source "FrontOfTransactionCheckInScreenData.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$LoadingScreenData;,
        Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EnterPhoneScreenData;,
        Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$ErrorScreenData;,
        Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$WelcomeScreenData;,
        Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$NewLoyaltyAccountScreenData;,
        Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$RewardCarouselScreenData;,
        Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;,
        Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$RewardRedeemedScreenData;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0008\u0003\u0004\u0005\u0006\u0007\u0008\t\nB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0008\u000b\u000c\r\u000e\u000f\u0010\u0011\u0012\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;",
        "Landroid/os/Parcelable;",
        "()V",
        "EditRewardTierScreenData",
        "EnterPhoneScreenData",
        "ErrorScreenData",
        "LoadingScreenData",
        "NewLoyaltyAccountScreenData",
        "RewardCarouselScreenData",
        "RewardRedeemedScreenData",
        "WelcomeScreenData",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$LoadingScreenData;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EnterPhoneScreenData;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$ErrorScreenData;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$WelcomeScreenData;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$NewLoyaltyAccountScreenData;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$RewardCarouselScreenData;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$RewardRedeemedScreenData;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 7
    invoke-direct {p0}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;-><init>()V

    return-void
.end method
