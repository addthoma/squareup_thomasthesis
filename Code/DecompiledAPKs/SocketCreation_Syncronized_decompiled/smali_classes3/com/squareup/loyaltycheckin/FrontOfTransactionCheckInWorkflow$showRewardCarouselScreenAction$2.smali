.class final Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$showRewardCarouselScreenAction$2;
.super Lkotlin/jvm/internal/Lambda;
.source "FrontOfTransactionCheckInWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->showRewardCarouselScreenAction(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$WelcomeScreen;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_showRewardCarouselScreenAction:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$WelcomeScreen;

.field final synthetic this$0:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$WelcomeScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$showRewardCarouselScreenAction$2;->this$0:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;

    iput-object p2, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$showRewardCarouselScreenAction$2;->$this_showRewardCarouselScreenAction:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$WelcomeScreen;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Mutator;

    invoke-virtual {p0, p1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$showRewardCarouselScreenAction$2;->invoke(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Void;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
            ">;)",
            "Ljava/lang/Void;"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 286
    iget-object v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$showRewardCarouselScreenAction$2;->this$0:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;

    invoke-static {v0}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->access$getAnalytics$p(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    sget-object v1, Lcom/squareup/loyaltycheckin/CheckInAnalyticEvent$ViewRewardCarousel;->INSTANCE:Lcom/squareup/loyaltycheckin/CheckInAnalyticEvent$ViewRewardCarousel;

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 287
    new-instance v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$RewardCarousel;

    iget-object v1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$showRewardCarouselScreenAction$2;->$this_showRewardCarouselScreenAction:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$WelcomeScreen;

    invoke-virtual {v1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$WelcomeScreen;->getData()Lcom/squareup/loyaltycheckin/CheckedInData;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$RewardCarousel;-><init>(Lcom/squareup/loyaltycheckin/CheckedInData;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    const/4 p1, 0x0

    return-object p1
.end method
