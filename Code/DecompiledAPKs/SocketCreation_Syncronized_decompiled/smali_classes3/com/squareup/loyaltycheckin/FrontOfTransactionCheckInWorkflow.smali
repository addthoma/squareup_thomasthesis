.class public final Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "FrontOfTransactionCheckInWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFrontOfTransactionCheckInWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 FrontOfTransactionCheckInWorkflow.kt\ncom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow\n+ 2 Rx2.kt\ncom/squareup/util/rx2/Rx2Kt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 5 Worker.kt\ncom/squareup/workflow/Worker$Companion\n*L\n1#1,315:1\n298#1,4:316\n300#1:320\n302#1,2:326\n298#1,4:328\n300#1:332\n302#1,2:338\n298#1,4:340\n300#1:344\n302#1,2:350\n298#1,4:352\n300#1:356\n302#1,2:362\n298#1,4:364\n300#1:368\n302#1,2:374\n298#1,4:376\n300#1:380\n302#1,2:386\n19#2:321\n19#2:333\n19#2:345\n19#2:357\n19#2:369\n19#2:381\n19#2:394\n41#3:322\n56#3,2:323\n41#3:334\n56#3,2:335\n41#3:346\n56#3,2:347\n41#3:358\n56#3,2:359\n41#3:370\n56#3,2:371\n41#3:382\n56#3,2:383\n85#3:388\n85#3:391\n41#3:395\n56#3,2:396\n276#4:325\n276#4:337\n276#4:349\n276#4:361\n276#4:373\n276#4:385\n276#4:390\n276#4:393\n276#4:398\n240#5:389\n240#5:392\n*E\n*S KotlinDebug\n*F\n+ 1 FrontOfTransactionCheckInWorkflow.kt\ncom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow\n*L\n102#1,4:316\n102#1:320\n102#1,2:326\n109#1,4:328\n109#1:332\n109#1,2:338\n111#1,4:340\n111#1:344\n111#1,2:350\n112#1,4:352\n112#1:356\n112#1,2:362\n151#1,4:364\n151#1:368\n151#1,2:374\n157#1,4:376\n157#1:380\n157#1,2:386\n102#1:321\n109#1:333\n111#1:345\n112#1:357\n151#1:369\n157#1:381\n300#1:394\n102#1:322\n102#1,2:323\n109#1:334\n109#1,2:335\n111#1:346\n111#1,2:347\n112#1:358\n112#1,2:359\n151#1:370\n151#1,2:371\n157#1:382\n157#1,2:383\n205#1:388\n223#1:391\n301#1:395\n301#1,2:396\n102#1:325\n109#1:337\n111#1:349\n112#1:361\n151#1:373\n157#1:385\n205#1:390\n223#1:393\n301#1:398\n205#1:389\n223#1:392\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00be\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u001a\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0001B?\u0008\u0007\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\u0002\u0010\u0014J&\u0010\u0019\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0016j\u0002`\u001b0\u001a2\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J\u0018\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020#H\u0002J\u001a\u0010$\u001a\u00020\u00032\u0006\u0010%\u001a\u00020\u00022\u0008\u0010&\u001a\u0004\u0018\u00010\'H\u0016J&\u0010(\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0016j\u0002`\u001b0\u001a2\u0006\u0010\u001c\u001a\u00020)H\u0002J.\u0010*\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0016j\u0002`\u001b0+2\u0006\u0010\u001c\u001a\u00020)2\u0006\u0010,\u001a\u00020-H\u0002J,\u0010.\u001a\u00020\u00052\u0006\u0010%\u001a\u00020\u00022\u0006\u0010\u001c\u001a\u00020\u00032\u0012\u0010/\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000400H\u0016J\u001e\u00101\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00162\u0008\u0008\u0002\u00102\u001a\u000203H\u0002J\u001c\u00104\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00162\u0006\u00105\u001a\u00020\u001fH\u0002J\u0010\u00106\u001a\u00020\'2\u0006\u0010\u001c\u001a\u00020\u0003H\u0016JK\u00107\u001a\u000208\"\n\u0008\u0000\u00109\u0018\u0001*\u00020:*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004002$\u0008\u0008\u0010;\u001a\u001e\u0012\u0004\u0012\u0002H9\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0016j\u0002`\u001b0<H\u0082\u0008JL\u0010=\u001a\u000208*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004002\u0006\u0010\u001c\u001a\u00020\u00032\u0006\u0010>\u001a\u00020?2\"\u0010;\u001a\u001e\u0012\u0004\u0012\u000208\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0016j\u0002`\u001b0<H\u0002J\u0018\u0010@\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0016*\u00020)H\u0002J \u0010A\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0016*\u00020\u001d2\u0006\u00105\u001a\u00020\u001fH\u0002J\u0018\u0010A\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0016*\u00020BH\u0002R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006C"
    }
    d2 = {
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;",
        "rewardCarouselWorkflow",
        "Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;",
        "loyaltyServiceHelper",
        "Lcom/squareup/loyalty/LoyaltyServiceHelper;",
        "loyaltyFrontOfTransactionEvents",
        "Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;",
        "loyaltySettings",
        "Lcom/squareup/loyalty/LoyaltySettings;",
        "res",
        "Lcom/squareup/util/Res;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "pointsTermsFormatter",
        "Lcom/squareup/loyalty/PointsTermsFormatter;",
        "(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/loyalty/PointsTermsFormatter;)V",
        "cancelPhoneEntryAction",
        "Lcom/squareup/workflow/WorkflowAction;",
        "exitAction",
        "showNewLoyaltyAccount",
        "checkExistingContactAddedToSale",
        "Lcom/squareup/workflow/Worker;",
        "Lcom/squareup/loyaltycheckin/Action;",
        "state",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$CheckingExistingContactAddedToSale;",
        "checkedInDataOf",
        "Lcom/squareup/loyaltycheckin/CheckedInData;",
        "hasLoyalty",
        "Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;",
        "contact",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "lookupPhone",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$LookingUpPhone;",
        "lookupPhoneGetStatus",
        "Lio/reactivex/Single;",
        "response",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingResponse;",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "showEnteringPhone",
        "phoneNumber",
        "",
        "showWelcomeScreenAction",
        "data",
        "snapshotState",
        "onReceivedEvent",
        "",
        "Event",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent;",
        "handler",
        "Lkotlin/Function1;",
        "onTimer",
        "durationMs",
        "",
        "showErrorLookingUpPhone",
        "showRewardCarouselScreenAction",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$WelcomeScreen;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final cancelPhoneEntryAction:Lcom/squareup/workflow/WorkflowAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;"
        }
    .end annotation
.end field

.field private final exitAction:Lcom/squareup/workflow/WorkflowAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltyFrontOfTransactionEvents:Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;

.field private final loyaltyServiceHelper:Lcom/squareup/loyalty/LoyaltyServiceHelper;

.field private final loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

.field private final pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

.field private final res:Lcom/squareup/util/Res;

.field private final rewardCarouselWorkflow:Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;

.field private final showNewLoyaltyAccount:Lcom/squareup/workflow/WorkflowAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/loyalty/PointsTermsFormatter;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "rewardCarouselWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltyServiceHelper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltyFrontOfTransactionEvents"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltySettings"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pointsTermsFormatter"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->rewardCarouselWorkflow:Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;

    iput-object p2, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->loyaltyServiceHelper:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    iput-object p3, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->loyaltyFrontOfTransactionEvents:Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;

    iput-object p4, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    iput-object p5, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->res:Lcom/squareup/util/Res;

    iput-object p6, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p7, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    .line 251
    new-instance p1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$cancelPhoneEntryAction$1;

    invoke-direct {p1, p0}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$cancelPhoneEntryAction$1;-><init>(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    const/4 p2, 0x1

    const/4 p3, 0x0

    invoke-static {p0, p3, p1, p2, p3}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->cancelPhoneEntryAction:Lcom/squareup/workflow/WorkflowAction;

    .line 266
    new-instance p1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$showNewLoyaltyAccount$1;

    invoke-direct {p1, p0}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$showNewLoyaltyAccount$1;-><init>(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, p3, p1, p2, p3}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->showNewLoyaltyAccount:Lcom/squareup/workflow/WorkflowAction;

    .line 291
    sget-object p1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$exitAction$1;->INSTANCE:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$exitAction$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, p3, p1, p2, p3}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->exitAction:Lcom/squareup/workflow/WorkflowAction;

    return-void
.end method

.method public static final synthetic access$checkedInDataOf(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/loyaltycheckin/CheckedInData;
    .locals 0

    .line 60
    invoke-direct {p0, p1, p2}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->checkedInDataOf(Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/loyaltycheckin/CheckedInData;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$getCancelPhoneEntryAction$p(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->cancelPhoneEntryAction:Lcom/squareup/workflow/WorkflowAction;

    return-object p0
.end method

.method public static final synthetic access$getExitAction$p(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->exitAction:Lcom/squareup/workflow/WorkflowAction;

    return-object p0
.end method

.method public static final synthetic access$getLoyaltyFrontOfTransactionEvents$p(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;)Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->loyaltyFrontOfTransactionEvents:Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;

    return-object p0
.end method

.method public static final synthetic access$getShowNewLoyaltyAccount$p(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->showNewLoyaltyAccount:Lcom/squareup/workflow/WorkflowAction;

    return-object p0
.end method

.method public static final synthetic access$lookupPhoneGetStatus(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$LookingUpPhone;Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingResponse;)Lio/reactivex/Single;
    .locals 0

    .line 60
    invoke-direct {p0, p1, p2}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->lookupPhoneGetStatus(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$LookingUpPhone;Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingResponse;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$showEnteringPhone(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->showEnteringPhone(Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$showErrorLookingUpPhone(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$LookingUpPhone;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->showErrorLookingUpPhone(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$LookingUpPhone;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$showRewardCarouselScreenAction(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$CheckingExistingContactAddedToSale;Lcom/squareup/loyaltycheckin/CheckedInData;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 60
    invoke-direct {p0, p1, p2}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->showRewardCarouselScreenAction(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$CheckingExistingContactAddedToSale;Lcom/squareup/loyaltycheckin/CheckedInData;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$showRewardCarouselScreenAction(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$WelcomeScreen;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->showRewardCarouselScreenAction(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$WelcomeScreen;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$showWelcomeScreenAction(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;Lcom/squareup/loyaltycheckin/CheckedInData;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->showWelcomeScreenAction(Lcom/squareup/loyaltycheckin/CheckedInData;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private final checkExistingContactAddedToSale(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$CheckingExistingContactAddedToSale;)Lcom/squareup/workflow/Worker;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$CheckingExistingContactAddedToSale;",
            ")",
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;>;"
        }
    .end annotation

    .line 194
    iget-object v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->loyaltyServiceHelper:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    invoke-virtual {p1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$CheckingExistingContactAddedToSale;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->getLoyaltyStatusForContact(Lcom/squareup/protos/client/rolodex/Contact;)Lio/reactivex/Single;

    move-result-object v0

    .line 195
    new-instance v1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$checkExistingContactAddedToSale$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$checkExistingContactAddedToSale$1;-><init>(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$CheckingExistingContactAddedToSale;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "loyaltyServiceHelper.get\u2026  }\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 388
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$checkExistingContactAddedToSale$$inlined$asWorker$1;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$checkExistingContactAddedToSale$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 389
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 390
    const-class v0, Lcom/squareup/workflow/WorkflowAction;

    sget-object v1, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v2, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v3, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v3

    invoke-virtual {v2, v3}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v1, Lcom/squareup/workflow/Worker;

    return-object v1
.end method

.method private final checkedInDataOf(Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/loyaltycheckin/CheckedInData;
    .locals 2

    .line 245
    new-instance v0, Lcom/squareup/loyaltycheckin/CheckedInData;

    .line 246
    invoke-virtual {p1}, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->getPhoneToken()Ljava/lang/String;

    move-result-object v1

    .line 247
    invoke-virtual {p1}, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->getCurrentPoints()I

    move-result p1

    .line 245
    invoke-direct {v0, v1, p1, p2}, Lcom/squareup/loyaltycheckin/CheckedInData;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/rolodex/Contact;)V

    return-object v0
.end method

.method private final lookupPhone(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$LookingUpPhone;)Lcom/squareup/workflow/Worker;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$LookingUpPhone;",
            ")",
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;>;"
        }
    .end annotation

    .line 215
    iget-object v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->loyaltyServiceHelper:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    invoke-virtual {p1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$LookingUpPhone;->getPhoneNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->getLoyaltyAccountFromMapping(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    .line 216
    new-instance v1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$lookupPhone$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$lookupPhone$1;-><init>(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$LookingUpPhone;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "loyaltyServiceHelper.get\u2026())\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 391
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$lookupPhone$$inlined$asWorker$1;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$lookupPhone$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 392
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 393
    const-class v0, Lcom/squareup/workflow/WorkflowAction;

    sget-object v1, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v2, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v3, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v3

    invoke-virtual {v2, v3}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v1, Lcom/squareup/workflow/Worker;

    return-object v1
.end method

.method private final lookupPhoneGetStatus(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$LookingUpPhone;Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingResponse;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$LookingUpPhone;",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingResponse;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;>;"
        }
    .end annotation

    .line 230
    iget-object p2, p2, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingResponse;->loyalty_account:Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    iget-object p2, p2, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    goto :goto_0

    :cond_0
    move-object p2, v0

    :goto_0
    if-eqz p2, :cond_1

    .line 231
    iget-object v0, p2, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    :cond_1
    if-nez v0, :cond_2

    .line 232
    iget-object p1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->showNewLoyaltyAccount:Lcom/squareup/workflow/WorkflowAction;

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "Single.just(showNewLoyaltyAccount)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 234
    :cond_2
    iget-object v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->loyaltyServiceHelper:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    invoke-virtual {v0, p2}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->getLoyaltyStatusForContact(Lcom/squareup/protos/client/rolodex/Contact;)Lio/reactivex/Single;

    move-result-object v0

    .line 235
    new-instance v1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$lookupPhoneGetStatus$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$lookupPhoneGetStatus$1;-><init>(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$LookingUpPhone;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "loyaltyServiceHelper.get\u2026            }\n          }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    return-object p1
.end method

.method private final synthetic onReceivedEvent(Lcom/squareup/workflow/RenderContext;Lkotlin/jvm/functions/Function1;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Event:",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent;",
            ">(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
            "-",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-TEvent;+",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
            "+",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;>;)V"
        }
    .end annotation

    .line 299
    invoke-static {p0}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->access$getLoyaltyFrontOfTransactionEvents$p(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;)Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;->events$public_release()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Event"

    const/4 v2, 0x4

    .line 394
    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v2, Ljava/lang/Object;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v2, "ofType(T::class.java)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 395
    sget-object v2, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v0

    const-string/jumbo v2, "this.toFlowable(BUFFER)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lorg/reactivestreams/Publisher;

    if-eqz v0, :cond_0

    .line 397
    invoke-static {v0}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    const/4 v2, 0x6

    .line 398
    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const/4 v1, 0x0

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v1, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v4, v2

    check-cast v4, Lcom/squareup/workflow/Worker;

    const/4 v5, 0x0

    const/4 v7, 0x2

    const/4 v8, 0x0

    move-object v3, p1

    move-object v6, p2

    .line 298
    invoke-static/range {v3 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    .line 397
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final onTimer(Lcom/squareup/workflow/RenderContext;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;JLkotlin/jvm/functions/Function1;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
            "-",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
            "J",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lkotlin/Unit;",
            "+",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
            "+",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;>;)V"
        }
    .end annotation

    .line 310
    invoke-virtual {p2}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;->toString()Ljava/lang/String;

    move-result-object p2

    .line 311
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-wide v1, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/Worker$Companion;->timer$default(Lcom/squareup/workflow/Worker$Companion;JLjava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/Worker;

    move-result-object p3

    .line 309
    invoke-interface {p1, p3, p2, p5}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final showEnteringPhone(Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;"
        }
    .end annotation

    .line 256
    new-instance v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$showEnteringPhone$1;

    invoke-direct {v0, p1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$showEnteringPhone$1;-><init>(Ljava/lang/String;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method static synthetic showEnteringPhone$default(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const-string p1, ""

    .line 256
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->showEnteringPhone(Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private final showErrorLookingUpPhone(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$LookingUpPhone;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$LookingUpPhone;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;"
        }
    .end annotation

    .line 272
    new-instance v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$showErrorLookingUpPhone$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$showErrorLookingUpPhone$1;-><init>(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$LookingUpPhone;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final showRewardCarouselScreenAction(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$CheckingExistingContactAddedToSale;Lcom/squareup/loyaltycheckin/CheckedInData;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$CheckingExistingContactAddedToSale;",
            "Lcom/squareup/loyaltycheckin/CheckedInData;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;"
        }
    .end annotation

    .line 279
    new-instance p1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$showRewardCarouselScreenAction$1;

    invoke-direct {p1, p0, p2}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$showRewardCarouselScreenAction$1;-><init>(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;Lcom/squareup/loyaltycheckin/CheckedInData;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    const/4 p2, 0x0

    const/4 v0, 0x1

    invoke-static {p0, p2, p1, v0, p2}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final showRewardCarouselScreenAction(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$WelcomeScreen;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$WelcomeScreen;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;"
        }
    .end annotation

    .line 285
    new-instance v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$showRewardCarouselScreenAction$2;

    invoke-direct {v0, p0, p1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$showRewardCarouselScreenAction$2;-><init>(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$WelcomeScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final showWelcomeScreenAction(Lcom/squareup/loyaltycheckin/CheckedInData;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyaltycheckin/CheckedInData;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;"
        }
    .end annotation

    .line 261
    new-instance v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$showWelcomeScreenAction$1;

    invoke-direct {v0, p1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$showWelcomeScreenAction$1;-><init>(Lcom/squareup/loyaltycheckin/CheckedInData;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;
    .locals 1

    const-string p2, "props"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    invoke-virtual {p1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;->getContactAddedToSale()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 78
    new-instance p2, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$CheckingExistingContactAddedToSale;

    invoke-virtual {p1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;->getContactAddedToSale()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$CheckingExistingContactAddedToSale;-><init>(Lcom/squareup/protos/client/rolodex/Contact;)V

    check-cast p2, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;

    goto :goto_0

    .line 80
    :cond_0
    new-instance p1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$EnteringPhone;

    const/4 p2, 0x1

    const/4 v0, 0x0

    invoke-direct {p1, v0, p2, v0}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$EnteringPhone;-><init>(Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object p2, p1

    check-cast p2, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;

    :goto_0
    return-object p2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->initialState(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
            "-",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;)",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;"
        }
    .end annotation

    move-object/from16 v6, p0

    move-object/from16 v7, p2

    const-string v0, "props"

    move-object/from16 v1, p1

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    move-object/from16 v14, p3

    invoke-static {v14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    iget-object v0, v6, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v0}, Lcom/squareup/loyalty/LoyaltySettings;->isLoyaltyProgramActive()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 96
    instance-of v0, v7, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$CheckingExistingContactAddedToSale;

    if-eqz v0, :cond_0

    .line 97
    move-object v0, v7

    check-cast v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$CheckingExistingContactAddedToSale;

    invoke-direct {v6, v0}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->checkExistingContactAddedToSale(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$CheckingExistingContactAddedToSale;)Lcom/squareup/workflow/Worker;

    move-result-object v8

    const/4 v9, 0x0

    sget-object v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$2;->INSTANCE:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$2;

    move-object v10, v0

    check-cast v10, Lkotlin/jvm/functions/Function1;

    const/4 v11, 0x2

    const/4 v12, 0x0

    move-object/from16 v7, p3

    invoke-static/range {v7 .. v12}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 98
    sget-object v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$LoadingScreenData;->INSTANCE:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$LoadingScreenData;

    check-cast v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;

    return-object v0

    .line 101
    :cond_0
    instance-of v0, v7, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$ErrorCheckingExistingContactAddedToSale;

    const-string v15, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    const-string/jumbo v13, "this.toFlowable(BUFFER)"

    const-string v12, "ofType(T::class.java)"

    if-eqz v0, :cond_2

    .line 102
    new-instance v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$3;

    invoke-direct {v0, v6}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$3;-><init>(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;)V

    move-object v11, v0

    check-cast v11, Lkotlin/jvm/functions/Function1;

    .line 317
    invoke-static/range {p0 .. p0}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->access$getLoyaltyFrontOfTransactionEvents$p(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;)Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;->events$public_release()Lio/reactivex/Observable;

    move-result-object v0

    .line 321
    const-class v1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$Done;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {v0, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 322
    sget-object v1, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v0

    invoke-static {v0, v13}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lorg/reactivestreams/Publisher;

    if-eqz v0, :cond_1

    .line 324
    invoke-static {v0}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 325
    const-class v1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$Done;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v1

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v1, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v9, v2

    check-cast v9, Lcom/squareup/workflow/Worker;

    const/4 v10, 0x0

    const/4 v12, 0x2

    const/4 v13, 0x0

    move-object/from16 v8, p3

    .line 316
    invoke-static/range {v8 .. v13}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 103
    invoke-static {}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflowKt;->getERROR_AUTOCLOSE_MS()J

    move-result-wide v3

    new-instance v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$4;

    invoke-direct {v0, v6}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$4;-><init>(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;)V

    move-object v5, v0

    check-cast v5, Lkotlin/jvm/functions/Function1;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->onTimer(Lcom/squareup/workflow/RenderContext;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;JLkotlin/jvm/functions/Function1;)V

    .line 104
    new-instance v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$ErrorScreenData;

    iget-object v1, v6, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/loyaltycheckin/R$string;->loyalty_check_in_error_check_in:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$ErrorScreenData;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;

    return-object v0

    .line 324
    :cond_1
    new-instance v0, Lkotlin/TypeCastException;

    invoke-direct {v0, v15}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :cond_2
    instance-of v0, v7, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$EnteringPhone;

    if-eqz v0, :cond_6

    .line 108
    invoke-static {}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflowKt;->getPHONE_INACTIVITY_AUTOCLOSE_MS()J

    move-result-wide v3

    new-instance v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$5;

    invoke-direct {v0, v6}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$5;-><init>(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;)V

    move-object v5, v0

    check-cast v5, Lkotlin/jvm/functions/Function1;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->onTimer(Lcom/squareup/workflow/RenderContext;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;JLkotlin/jvm/functions/Function1;)V

    .line 109
    new-instance v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$6;

    invoke-direct {v0, v6}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$6;-><init>(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;)V

    move-object v11, v0

    check-cast v11, Lkotlin/jvm/functions/Function1;

    .line 329
    invoke-static/range {p0 .. p0}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->access$getLoyaltyFrontOfTransactionEvents$p(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;)Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;->events$public_release()Lio/reactivex/Observable;

    move-result-object v0

    .line 333
    const-class v1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$Done;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {v0, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 334
    sget-object v1, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v0

    invoke-static {v0, v13}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lorg/reactivestreams/Publisher;

    if-eqz v0, :cond_5

    .line 336
    invoke-static {v0}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 337
    const-class v1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$Done;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v1

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v1, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v9, v2

    check-cast v9, Lcom/squareup/workflow/Worker;

    const/4 v10, 0x0

    const/4 v0, 0x2

    const/4 v1, 0x0

    move-object/from16 v8, p3

    move-object v5, v12

    move v12, v0

    move-object v3, v13

    move-object v13, v1

    .line 328
    invoke-static/range {v8 .. v13}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 111
    sget-object v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$7;->INSTANCE:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$7;

    move-object v11, v0

    check-cast v11, Lkotlin/jvm/functions/Function1;

    .line 341
    invoke-static/range {p0 .. p0}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->access$getLoyaltyFrontOfTransactionEvents$p(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;)Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;->events$public_release()Lio/reactivex/Observable;

    move-result-object v0

    .line 345
    const-class v1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$PhoneNumberEntered;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {v0, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 346
    sget-object v1, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v0

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lorg/reactivestreams/Publisher;

    if-eqz v0, :cond_4

    .line 348
    invoke-static {v0}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 349
    const-class v1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$PhoneNumberEntered;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v1

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v1, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v9, v2

    check-cast v9, Lcom/squareup/workflow/Worker;

    const/4 v10, 0x0

    const/4 v12, 0x2

    const/4 v13, 0x0

    move-object/from16 v8, p3

    .line 340
    invoke-static/range {v8 .. v13}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 112
    new-instance v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$8;

    invoke-direct {v0, v7}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$8;-><init>(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;)V

    move-object v11, v0

    check-cast v11, Lkotlin/jvm/functions/Function1;

    .line 353
    invoke-static/range {p0 .. p0}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->access$getLoyaltyFrontOfTransactionEvents$p(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;)Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;->events$public_release()Lio/reactivex/Observable;

    move-result-object v0

    .line 357
    const-class v1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$SubmitPhoneNumber;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {v0, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 358
    sget-object v1, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v0

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lorg/reactivestreams/Publisher;

    if-eqz v0, :cond_3

    .line 360
    invoke-static {v0}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 361
    const-class v1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$SubmitPhoneNumber;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v1

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v1, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v9, v2

    check-cast v9, Lcom/squareup/workflow/Worker;

    const/4 v10, 0x0

    const/4 v12, 0x2

    const/4 v13, 0x0

    move-object/from16 v8, p3

    .line 352
    invoke-static/range {v8 .. v13}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 114
    new-instance v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EnterPhoneScreenData;

    .line 115
    move-object v1, v7

    check-cast v1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$EnteringPhone;

    invoke-virtual {v1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$EnteringPhone;->getRawPhoneNumber()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    .line 114
    invoke-direct {v0, v1, v2}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EnterPhoneScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;

    return-object v0

    .line 360
    :cond_3
    new-instance v0, Lkotlin/TypeCastException;

    invoke-direct {v0, v15}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 348
    :cond_4
    new-instance v0, Lkotlin/TypeCastException;

    invoke-direct {v0, v15}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 336
    :cond_5
    new-instance v0, Lkotlin/TypeCastException;

    invoke-direct {v0, v15}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    move-object v5, v12

    move-object v3, v13

    .line 120
    instance-of v0, v7, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$LookingUpPhone;

    if-eqz v0, :cond_7

    .line 121
    move-object v0, v7

    check-cast v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$LookingUpPhone;

    invoke-direct {v6, v0}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->lookupPhone(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$LookingUpPhone;)Lcom/squareup/workflow/Worker;

    move-result-object v8

    const/4 v9, 0x0

    sget-object v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$9;->INSTANCE:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$9;

    move-object v10, v0

    check-cast v10, Lkotlin/jvm/functions/Function1;

    const/4 v11, 0x2

    const/4 v12, 0x0

    move-object/from16 v7, p3

    invoke-static/range {v7 .. v12}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 122
    sget-object v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$LoadingScreenData;->INSTANCE:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$LoadingScreenData;

    check-cast v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;

    return-object v0

    .line 125
    :cond_7
    instance-of v0, v7, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$WelcomeScreen;

    if-eqz v0, :cond_b

    .line 127
    sget-object v0, Lcom/squareup/loyalty/LoyaltyServiceHelper;->Companion:Lcom/squareup/loyalty/LoyaltyServiceHelper$Companion;

    move-object v8, v7

    check-cast v8, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$WelcomeScreen;

    invoke-virtual {v8}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$WelcomeScreen;->getData()Lcom/squareup/loyaltycheckin/CheckedInData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/loyaltycheckin/CheckedInData;->getPoints()I

    move-result v1

    iget-object v2, v6, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v2}, Lcom/squareup/loyalty/LoyaltySettings;->rewardTiers()Ljava/util/List;

    move-result-object v2

    if-nez v2, :cond_8

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_8
    const-string v3, "loyaltySettings.rewardTiers()!!"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/loyalty/LoyaltyServiceHelper$Companion;->getAvailableRewards(ILjava/util/List;)I

    move-result v0

    if-lez v0, :cond_9

    const/4 v0, 0x1

    const/4 v9, 0x1

    goto :goto_0

    :cond_9
    const/4 v0, 0x0

    const/4 v9, 0x0

    .line 129
    :goto_0
    invoke-static {}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflowKt;->getWELCOME_AUTOCLOSE_MS()J

    move-result-wide v3

    new-instance v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$10;

    invoke-direct {v0, v6, v9, v7}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$10;-><init>(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;ZLcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;)V

    move-object v5, v0

    check-cast v5, Lkotlin/jvm/functions/Function1;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->onTimer(Lcom/squareup/workflow/RenderContext;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;JLkotlin/jvm/functions/Function1;)V

    .line 139
    new-instance v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$WelcomeScreenData;

    if-eqz v9, :cond_a

    .line 141
    iget-object v1, v6, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/loyaltycheckin/R$string;->loyalty_check_in_welcome_back_rewards_subtitle:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 143
    :cond_a
    iget-object v1, v6, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    invoke-virtual {v8}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$WelcomeScreen;->getData()Lcom/squareup/loyaltycheckin/CheckedInData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/loyaltycheckin/CheckedInData;->getPoints()I

    move-result v2

    sget v3, Lcom/squareup/loyaltycheckin/R$string;->loyalty_check_in_balance_text:I

    invoke-virtual {v1, v2, v3}, Lcom/squareup/loyalty/PointsTermsFormatter;->points(II)Ljava/lang/String;

    move-result-object v1

    .line 145
    :goto_1
    invoke-static {}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflowKt;->getWELCOME_AUTOCLOSE_MS()J

    move-result-wide v2

    .line 139
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$WelcomeScreenData;-><init>(Ljava/lang/String;J)V

    check-cast v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;

    return-object v0

    .line 149
    :cond_b
    instance-of v0, v7, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$NewLoyaltyAccount;

    if-eqz v0, :cond_d

    .line 150
    invoke-static {}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflowKt;->getCHECKED_IN_INACTIVITY_AUTOCLOSE_MS()J

    move-result-wide v8

    new-instance v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$11;

    invoke-direct {v0, v6}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$11;-><init>(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;)V

    move-object v10, v0

    check-cast v10, Lkotlin/jvm/functions/Function1;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p2

    move-object v11, v3

    move-wide v3, v8

    move-object v8, v5

    move-object v5, v10

    invoke-direct/range {v0 .. v5}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->onTimer(Lcom/squareup/workflow/RenderContext;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;JLkotlin/jvm/functions/Function1;)V

    .line 151
    new-instance v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$12;

    invoke-direct {v0, v6}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$12;-><init>(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;)V

    move-object v10, v0

    check-cast v10, Lkotlin/jvm/functions/Function1;

    .line 365
    invoke-static/range {p0 .. p0}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->access$getLoyaltyFrontOfTransactionEvents$p(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;)Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;->events$public_release()Lio/reactivex/Observable;

    move-result-object v0

    .line 369
    const-class v1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$Done;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {v0, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 370
    sget-object v1, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v0

    invoke-static {v0, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lorg/reactivestreams/Publisher;

    if-eqz v0, :cond_c

    .line 372
    invoke-static {v0}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 373
    const-class v1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$Done;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v1

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v1, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v8, v2

    check-cast v8, Lcom/squareup/workflow/Worker;

    const/4 v9, 0x0

    const/4 v11, 0x2

    const/4 v12, 0x0

    move-object/from16 v7, p3

    .line 364
    invoke-static/range {v7 .. v12}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 153
    sget-object v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$NewLoyaltyAccountScreenData;->INSTANCE:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$NewLoyaltyAccountScreenData;

    check-cast v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;

    return-object v0

    .line 372
    :cond_c
    new-instance v0, Lkotlin/TypeCastException;

    invoke-direct {v0, v15}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    move-object v11, v3

    move-object v8, v5

    .line 156
    instance-of v0, v7, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$ErrorLookingUpPhone;

    if-eqz v0, :cond_f

    .line 157
    new-instance v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$13;

    invoke-direct {v0, v6, v7}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$13;-><init>(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 377
    invoke-static/range {p0 .. p0}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->access$getLoyaltyFrontOfTransactionEvents$p(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;)Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;->events$public_release()Lio/reactivex/Observable;

    move-result-object v1

    .line 381
    const-class v2, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$Done;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {v1, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 382
    sget-object v2, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v1

    invoke-static {v1, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lorg/reactivestreams/Publisher;

    if-eqz v1, :cond_e

    .line 384
    invoke-static {v1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v1

    .line 385
    const-class v2, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$Done;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v2

    new-instance v3, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v3, v2, v1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v9, v3

    check-cast v9, Lcom/squareup/workflow/Worker;

    const/4 v10, 0x0

    const/4 v12, 0x2

    const/4 v13, 0x0

    move-object/from16 v8, p3

    move-object v11, v0

    .line 376
    invoke-static/range {v8 .. v13}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 158
    invoke-static {}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflowKt;->getERROR_AUTOCLOSE_MS()J

    move-result-wide v3

    new-instance v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$14;

    invoke-direct {v0, v6, v7}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$14;-><init>(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;)V

    move-object v5, v0

    check-cast v5, Lkotlin/jvm/functions/Function1;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->onTimer(Lcom/squareup/workflow/RenderContext;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;JLkotlin/jvm/functions/Function1;)V

    .line 159
    new-instance v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$ErrorScreenData;

    iget-object v1, v6, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/loyaltycheckin/R$string;->loyalty_check_in_error_check_in:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$ErrorScreenData;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;

    return-object v0

    .line 384
    :cond_e
    new-instance v0, Lkotlin/TypeCastException;

    invoke-direct {v0, v15}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 162
    :cond_f
    instance-of v0, v7, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$RewardCarousel;

    if-eqz v0, :cond_10

    .line 166
    iget-object v0, v6, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->rewardCarouselWorkflow:Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;

    move-object v8, v0

    check-cast v8, Lcom/squareup/workflow/Workflow;

    .line 167
    new-instance v9, Lcom/squareup/loyaltycheckin/RewardCarouselProps;

    .line 168
    move-object v0, v7

    check-cast v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$RewardCarousel;

    invoke-virtual {v0}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$RewardCarousel;->getData()Lcom/squareup/loyaltycheckin/CheckedInData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/loyaltycheckin/CheckedInData;->getPhoneToken()Ljava/lang/String;

    move-result-object v2

    .line 169
    invoke-virtual {v0}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$RewardCarousel;->getData()Lcom/squareup/loyaltycheckin/CheckedInData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/loyaltycheckin/CheckedInData;->getPoints()I

    move-result v3

    .line 170
    invoke-virtual {v0}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$RewardCarousel;->getData()Lcom/squareup/loyaltycheckin/CheckedInData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/loyaltycheckin/CheckedInData;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    .line 171
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;->getCartRewardState()Lcom/squareup/loyalty/CartRewardState;

    move-result-object v1

    .line 167
    invoke-direct {v9, v2, v3, v0, v1}, Lcom/squareup/loyaltycheckin/RewardCarouselProps;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/loyalty/CartRewardState;)V

    const/4 v10, 0x0

    .line 173
    new-instance v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$15;

    invoke-direct {v0, v6, v7}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$15;-><init>(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;)V

    move-object v11, v0

    check-cast v11, Lkotlin/jvm/functions/Function1;

    const/4 v12, 0x4

    const/4 v13, 0x0

    move-object/from16 v7, p3

    .line 165
    invoke-static/range {v7 .. v13}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;

    return-object v0

    :cond_10
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 91
    :cond_11
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "FrontOfTransactionCheckInWorkflow should never be started if loyalty program isn\'t active"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;

    check-cast p2, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->render(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;

    move-result-object p1

    return-object p1
.end method

.method public snapshotState(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;

    invoke-virtual {p0, p1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow;->snapshotState(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
