.class public abstract Lcom/squareup/loyaltycheckin/impl/noop/wiring/NoopLoyaltyFrontOfTransactionSettingModule;
.super Ljava/lang/Object;
.source "NoopLoyaltyFrontOfTransactionSettingModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a2\u0006\u0002\u0008\u0007J\u0015\u0010\u0008\u001a\u00020\t2\u0006\u0010\u0005\u001a\u00020\nH!\u00a2\u0006\u0002\u0008\u000bJ\u0015\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH!\u00a2\u0006\u0002\u0008\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/loyaltycheckin/impl/noop/wiring/NoopLoyaltyFrontOfTransactionSettingModule;",
        "",
        "()V",
        "bindLoyaltyBuyerCartBannerFormatter",
        "Lcom/squareup/loyaltycheckin/LoyaltyBuyerCartBannerFormatter;",
        "formatter",
        "Lcom/squareup/loyaltycheckin/impl/noop/NoopLoyaltyBuyerCartBannerFormatter;",
        "bindLoyaltyBuyerCartBannerFormatter$impl_noop_wiring_release",
        "bindLoyaltySellerCartBannerFormatter",
        "Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;",
        "Lcom/squareup/loyaltycheckin/impl/noop/NoopLoyaltySellerCartBannerFormatter;",
        "bindLoyaltySellerCartBannerFormatter$impl_noop_wiring_release",
        "provideLoyaltyFrontOfTransactionSetting",
        "Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionSetting;",
        "setting",
        "Lcom/squareup/loyaltycheckin/impl/noop/NoopLoyaltyFrontOfTransactionSetting;",
        "provideLoyaltyFrontOfTransactionSetting$impl_noop_wiring_release",
        "impl-noop-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindLoyaltyBuyerCartBannerFormatter$impl_noop_wiring_release(Lcom/squareup/loyaltycheckin/impl/noop/NoopLoyaltyBuyerCartBannerFormatter;)Lcom/squareup/loyaltycheckin/LoyaltyBuyerCartBannerFormatter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindLoyaltySellerCartBannerFormatter$impl_noop_wiring_release(Lcom/squareup/loyaltycheckin/impl/noop/NoopLoyaltySellerCartBannerFormatter;)Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract provideLoyaltyFrontOfTransactionSetting$impl_noop_wiring_release(Lcom/squareup/loyaltycheckin/impl/noop/NoopLoyaltyFrontOfTransactionSetting;)Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionSetting;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
