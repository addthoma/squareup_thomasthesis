.class final Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$updateQuantityAction$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RewardCarouselWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->updateQuantityAction(Lcom/squareup/loyaltycheckin/RewardCarouselState$EditingRewardTier;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$UpdateRewardTierQuantity;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $event:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$UpdateRewardTierQuantity;

.field final synthetic $this_updateQuantityAction:Lcom/squareup/loyaltycheckin/RewardCarouselState$EditingRewardTier;


# direct methods
.method constructor <init>(Lcom/squareup/loyaltycheckin/RewardCarouselState$EditingRewardTier;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$UpdateRewardTierQuantity;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$updateQuantityAction$1;->$this_updateQuantityAction:Lcom/squareup/loyaltycheckin/RewardCarouselState$EditingRewardTier;

    iput-object p2, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$updateQuantityAction$1;->$event:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$UpdateRewardTierQuantity;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Mutator;

    invoke-virtual {p0, p1}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$updateQuantityAction$1;->invoke(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Void;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
            ">;)",
            "Ljava/lang/Void;"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 205
    iget-object v0, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$updateQuantityAction$1;->$this_updateQuantityAction:Lcom/squareup/loyaltycheckin/RewardCarouselState$EditingRewardTier;

    iget-object v1, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$updateQuantityAction$1;->$event:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$UpdateRewardTierQuantity;

    invoke-virtual {v1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$UpdateRewardTierQuantity;->getQuantity()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {v0, v3, v1, v4, v2}, Lcom/squareup/loyaltycheckin/RewardCarouselState$EditingRewardTier;->copy$default(Lcom/squareup/loyaltycheckin/RewardCarouselState$EditingRewardTier;IIILjava/lang/Object;)Lcom/squareup/loyaltycheckin/RewardCarouselState$EditingRewardTier;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    return-object v2
.end method
