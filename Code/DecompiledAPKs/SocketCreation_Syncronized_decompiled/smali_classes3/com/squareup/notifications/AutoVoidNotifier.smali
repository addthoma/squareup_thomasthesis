.class public interface abstract Lcom/squareup/notifications/AutoVoidNotifier;
.super Ljava/lang/Object;
.source "AutoVoidNotifier.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/notifications/AutoVoidNotifier$NoAutoVoidNotifier;
    }
.end annotation


# virtual methods
.method public abstract notifyDanglingAuthVoidedAfterCrash(Lcom/squareup/protos/common/Money;)V
.end method

.method public abstract notifyDanglingMiryo()V
.end method

.method public abstract notifyVoidOnTimeout(Lcom/squareup/protos/common/Money;)V
.end method
