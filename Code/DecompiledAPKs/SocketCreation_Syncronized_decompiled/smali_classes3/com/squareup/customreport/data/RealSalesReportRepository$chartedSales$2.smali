.class final Lcom/squareup/customreport/data/RealSalesReportRepository$chartedSales$2;
.super Ljava/lang/Object;
.source "RealSalesReportRepository.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/customreport/data/RealSalesReportRepository;->chartedSales(Lcom/squareup/customreport/data/ReportConfig;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012:\u0010\u0003\u001a6\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0006 \u0007*\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00050\u0005\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0006 \u0007*\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00050\u00050\u0004H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/customreport/data/SalesReportRepository$Result;",
        "Lcom/squareup/customreport/data/SalesChartReport;",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
        "kotlin.jvm.PlatformType",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $chartedSalesReportType:Lcom/squareup/customreport/data/SalesReportType$Charted;

.field final synthetic this$0:Lcom/squareup/customreport/data/RealSalesReportRepository;


# direct methods
.method constructor <init>(Lcom/squareup/customreport/data/RealSalesReportRepository;Lcom/squareup/customreport/data/SalesReportType$Charted;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/customreport/data/RealSalesReportRepository$chartedSales$2;->this$0:Lcom/squareup/customreport/data/RealSalesReportRepository;

    iput-object p2, p0, Lcom/squareup/customreport/data/RealSalesReportRepository$chartedSales$2;->$chartedSalesReportType:Lcom/squareup/customreport/data/SalesReportType$Charted;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lkotlin/Pair;)Lcom/squareup/customreport/data/SalesReportRepository$Result;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "+",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
            ">;+",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
            ">;>;)",
            "Lcom/squareup/customreport/data/SalesReportRepository$Result<",
            "Lcom/squareup/customreport/data/SalesChartReport;",
            ">;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    .line 212
    instance-of v1, v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-nez v1, :cond_0

    new-instance p1, Lcom/squareup/customreport/data/SalesReportRepository$Result$Failure;

    invoke-virtual {v0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/customreport/data/SalesReportRepository$Result$Failure;-><init>(Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/customreport/data/SalesReportRepository$Result;

    goto :goto_0

    .line 213
    :cond_0
    instance-of v1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-nez v1, :cond_1

    new-instance v0, Lcom/squareup/customreport/data/SalesReportRepository$Result$Failure;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/customreport/data/SalesReportRepository$Result$Failure;-><init>(Ljava/lang/String;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/customreport/data/SalesReportRepository$Result;

    goto :goto_0

    .line 215
    :cond_1
    check-cast v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {v0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;

    .line 216
    iget-object v1, p0, Lcom/squareup/customreport/data/RealSalesReportRepository$chartedSales$2;->this$0:Lcom/squareup/customreport/data/RealSalesReportRepository;

    invoke-static {v1}, Lcom/squareup/customreport/data/RealSalesReportRepository;->access$getCurrentTimeZone$p(Lcom/squareup/customreport/data/RealSalesReportRepository;)Lcom/squareup/time/CurrentTimeZone;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/time/CurrentTimeZone;->zoneId()Lorg/threeten/bp/ZoneId;

    move-result-object v1

    .line 217
    iget-object v2, p0, Lcom/squareup/customreport/data/RealSalesReportRepository$chartedSales$2;->$chartedSalesReportType:Lcom/squareup/customreport/data/SalesReportType$Charted;

    .line 218
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;

    .line 215
    invoke-static {v0, v1, v2, p1}, Lcom/squareup/customreport/data/util/CustomReportResponsesKt;->toSalesChartReport(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;Lorg/threeten/bp/ZoneId;Lcom/squareup/customreport/data/SalesReportType$Charted;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Lcom/squareup/customreport/data/SalesChartReport;

    move-result-object p1

    .line 220
    new-instance v0, Lcom/squareup/customreport/data/SalesReportRepository$Result$Success;

    invoke-direct {v0, p1}, Lcom/squareup/customreport/data/SalesReportRepository$Result$Success;-><init>(Ljava/lang/Object;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/customreport/data/SalesReportRepository$Result;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 58
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/customreport/data/RealSalesReportRepository$chartedSales$2;->apply(Lkotlin/Pair;)Lcom/squareup/customreport/data/SalesReportRepository$Result;

    move-result-object p1

    return-object p1
.end method
