.class public final Lcom/squareup/customreport/data/util/RangeSelectionsKt;
.super Ljava/lang/Object;
.source "RangeSelections.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u001a\u001a\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0001\u001a\u001a\u0010\u0006\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0001\u00a8\u0006\u0007"
    }
    d2 = {
        "endDate",
        "Lorg/threeten/bp/LocalDate;",
        "Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection;",
        "locale",
        "Ljava/util/Locale;",
        "currentDate",
        "startDate",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final endDate(Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection;Ljava/util/Locale;Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;
    .locals 1

    const-string v0, "$this$endDate"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentDate"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneDay;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneDay;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 43
    :cond_0
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneWeek;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneWeek;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p2, p1}, Lcom/squareup/utilities/threeten/LocalDatesKt;->endOfWeek(Lorg/threeten/bp/LocalDate;Ljava/util/Locale;)Lorg/threeten/bp/LocalDate;

    move-result-object p2

    goto :goto_0

    .line 44
    :cond_1
    sget-object p1, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneMonth;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneMonth;

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-static {p2}, Lcom/squareup/utilities/threeten/LocalDatesKt;->endOfMonth(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;

    move-result-object p2

    goto :goto_0

    .line 45
    :cond_2
    sget-object p1, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$ThreeMonths;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$ThreeMonths;

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-static {p2}, Lcom/squareup/utilities/threeten/LocalDatesKt;->endOfMonth(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;

    move-result-object p2

    goto :goto_0

    .line 46
    :cond_3
    sget-object p1, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneYear;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneYear;

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    invoke-static {p2}, Lcom/squareup/utilities/threeten/LocalDatesKt;->endOfYear(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;

    move-result-object p2

    :goto_0
    return-object p2

    :cond_4
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method public static final startDate(Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection;Ljava/util/Locale;Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;
    .locals 1

    const-string v0, "$this$startDate"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentDate"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneDay;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneDay;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 27
    :cond_0
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneWeek;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneWeek;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p2, p1}, Lcom/squareup/utilities/threeten/LocalDatesKt;->startOfWeek(Lorg/threeten/bp/LocalDate;Ljava/util/Locale;)Lorg/threeten/bp/LocalDate;

    move-result-object p2

    goto :goto_0

    .line 28
    :cond_1
    sget-object p1, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneMonth;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneMonth;

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-static {p2}, Lcom/squareup/utilities/threeten/LocalDatesKt;->startOfMonth(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;

    move-result-object p2

    goto :goto_0

    .line 29
    :cond_2
    sget-object p1, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$ThreeMonths;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$ThreeMonths;

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const-wide/16 p0, 0x2

    invoke-virtual {p2, p0, p1}, Lorg/threeten/bp/LocalDate;->minusMonths(J)Lorg/threeten/bp/LocalDate;

    move-result-object p0

    const-string p1, "currentDate.minusMonths(2)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/squareup/utilities/threeten/LocalDatesKt;->startOfMonth(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;

    move-result-object p2

    goto :goto_0

    .line 30
    :cond_3
    sget-object p1, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneYear;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneYear;

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    invoke-static {p2}, Lcom/squareup/utilities/threeten/LocalDatesKt;->startOfYear(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;

    move-result-object p2

    :goto_0
    return-object p2

    :cond_4
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method
