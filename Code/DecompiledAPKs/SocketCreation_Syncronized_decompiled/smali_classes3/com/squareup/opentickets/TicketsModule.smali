.class public abstract Lcom/squareup/opentickets/TicketsModule;
.super Ljava/lang/Object;
.source "TicketsModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# static fields
.field private static final REMOTE_UNSYNCED_FIXABLE_TICKETS:Ljava/lang/String; = "UNSYNCED_FIXABLE_TICKETS"

.field private static final REMOTE_UNSYNCED_UNFIXABLE_TICKETS:Ljava/lang/String; = "UNSYNCED_UNFIXABLE_TICKETS"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideTicketDatabase(Landroid/app/Application;Ljava/io/File;Lcom/squareup/util/Res;)Lcom/squareup/tickets/TicketStore;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 54
    new-instance v0, Lcom/squareup/tickets/SqliteTicketStore;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/tickets/SqliteTicketStore;-><init>(Landroid/content/Context;Ljava/io/File;Lcom/squareup/util/Res;)V

    return-object v0
.end method

.method static provideTicketsExecutor(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/tickets/TicketStore;Ljava/util/concurrent/Executor;)Lcom/squareup/tickets/TicketsExecutor;
    .locals 1
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/tickets/TicketStore;",
            "Ljava/util/concurrent/Executor;",
            ")",
            "Lcom/squareup/tickets/TicketsExecutor<",
            "Lcom/squareup/tickets/TicketStore;",
            ">;"
        }
    .end annotation

    .line 44
    new-instance v0, Lcom/squareup/tickets/FileThreadTicketsExecutor;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/tickets/FileThreadTicketsExecutor;-><init>(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/tickets/TicketDatabase;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method

.method static provideUnsyncedFixableTickets(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 80
    new-instance v0, Lcom/squareup/settings/LongLocalSetting;

    const-string v1, "UNSYNCED_FIXABLE_TICKETS"

    invoke-direct {v0, p0, v1}, Lcom/squareup/settings/LongLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    return-object v0
.end method

.method static provideUnsyncedUnfixableTickets(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 86
    new-instance v0, Lcom/squareup/settings/LongLocalSetting;

    const-string v1, "UNSYNCED_UNFIXABLE_TICKETS"

    invoke-direct {v0, p0, v1}, Lcom/squareup/settings/LongLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method abstract bindPredefinedTicketsAsJailKeeperService(Lcom/squareup/opentickets/RealPredefinedTickets;)Lcom/squareup/jailkeeper/JailKeeperService;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract bindPredefinedTicketsForLoggedIn(Lcom/squareup/opentickets/RealPredefinedTickets;)Lmortar/Scoped;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract bindTicketListSchedulerAsJailKeeperService(Lcom/squareup/opentickets/RealTicketsListScheduler;)Lcom/squareup/jailkeeper/JailKeeperService;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract bindTicketsListSchedulerForLoggedIn(Lcom/squareup/opentickets/TicketsListScheduler;)Lmortar/Scoped;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract provideAvailableTemplateCountCache(Lcom/squareup/tickets/RealAvailableTemplateCountCache;)Lcom/squareup/opentickets/AvailableTemplateCountCache;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideInternalTickets(Lcom/squareup/tickets/RealTickets;)Lcom/squareup/tickets/Tickets$InternalTickets;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideListScheduler(Lcom/squareup/opentickets/RealTicketsListScheduler;)Lcom/squareup/opentickets/TicketsListScheduler;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract providePredefinedTickets(Lcom/squareup/opentickets/RealPredefinedTickets;)Lcom/squareup/opentickets/PredefinedTickets;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideSweeperManager(Lcom/squareup/opentickets/RealTicketsSweeperManager;)Lcom/squareup/opentickets/TicketsSweeperManager;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideTickets(Lcom/squareup/tickets/RealTickets;)Lcom/squareup/tickets/Tickets;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
