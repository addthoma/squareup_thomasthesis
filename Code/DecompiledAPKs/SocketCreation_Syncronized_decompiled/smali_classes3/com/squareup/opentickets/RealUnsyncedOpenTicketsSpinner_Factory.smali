.class public final Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner_Factory;
.super Ljava/lang/Object;
.source "RealUnsyncedOpenTicketsSpinner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner;",
        ">;"
    }
.end annotation


# instance fields
.field private final spinnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner_Factory;->spinnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;)",
            "Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner_Factory;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/register/widgets/GlassSpinner;)Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner;
    .locals 1

    .line 34
    new-instance v0, Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner;

    invoke-direct {v0, p0}, Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner;-><init>(Lcom/squareup/register/widgets/GlassSpinner;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner_Factory;->spinnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/GlassSpinner;

    invoke-static {v0}, Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner_Factory;->newInstance(Lcom/squareup/register/widgets/GlassSpinner;)Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner_Factory;->get()Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner;

    move-result-object v0

    return-object v0
.end method
