.class public final Lcom/squareup/ms/SharedMinesweeperModule_ProvideMinesweeperTicketFactory;
.super Ljava/lang/Object;
.source "SharedMinesweeperModule_ProvideMinesweeperTicketFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ms/MinesweeperTicket;",
        ">;"
    }
.end annotation


# instance fields
.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final minesweeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ms/SharedMinesweeperModule_ProvideMinesweeperTicketFactory;->contextProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/ms/SharedMinesweeperModule_ProvideMinesweeperTicketFactory;->minesweeperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ms/SharedMinesweeperModule_ProvideMinesweeperTicketFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;)",
            "Lcom/squareup/ms/SharedMinesweeperModule_ProvideMinesweeperTicketFactory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/ms/SharedMinesweeperModule_ProvideMinesweeperTicketFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ms/SharedMinesweeperModule_ProvideMinesweeperTicketFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideMinesweeperTicket(Landroid/app/Application;Ljavax/inject/Provider;)Lcom/squareup/ms/MinesweeperTicket;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;)",
            "Lcom/squareup/ms/MinesweeperTicket;"
        }
    .end annotation

    .line 40
    invoke-static {p0, p1}, Lcom/squareup/ms/SharedMinesweeperModule;->provideMinesweeperTicket(Landroid/app/Application;Ljavax/inject/Provider;)Lcom/squareup/ms/MinesweeperTicket;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ms/MinesweeperTicket;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ms/MinesweeperTicket;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/ms/SharedMinesweeperModule_ProvideMinesweeperTicketFactory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/ms/SharedMinesweeperModule_ProvideMinesweeperTicketFactory;->minesweeperProvider:Ljavax/inject/Provider;

    invoke-static {v0, v1}, Lcom/squareup/ms/SharedMinesweeperModule_ProvideMinesweeperTicketFactory;->provideMinesweeperTicket(Landroid/app/Application;Ljavax/inject/Provider;)Lcom/squareup/ms/MinesweeperTicket;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ms/SharedMinesweeperModule_ProvideMinesweeperTicketFactory;->get()Lcom/squareup/ms/MinesweeperTicket;

    move-result-object v0

    return-object v0
.end method
