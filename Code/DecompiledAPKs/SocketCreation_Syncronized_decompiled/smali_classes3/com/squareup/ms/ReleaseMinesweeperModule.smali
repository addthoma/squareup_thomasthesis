.class public final Lcom/squareup/ms/ReleaseMinesweeperModule;
.super Ljava/lang/Object;
.source "ReleaseMinesweeperModule.kt"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/ms/SharedMinesweeperModule;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007JJ\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0008\u0008\u0001\u0010\u0016\u001a\u00020\u0004H\u0007J\u0012\u0010\u0017\u001a\u00020\u00182\u0008\u0008\u0001\u0010\u0019\u001a\u00020\u001aH\u0007\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/ms/ReleaseMinesweeperModule;",
        "",
        "()V",
        "getMinesweeper",
        "Lcom/squareup/ms/Minesweeper;",
        "msFactory",
        "Lcom/squareup/ms/MsFactory;",
        "getMsFactory",
        "application",
        "Landroid/app/Application;",
        "executorService",
        "Lcom/squareup/ms/MinesweeperExecutorService;",
        "dataListener",
        "Lcom/squareup/ms/Minesweeper$DataListener;",
        "mainThread",
        "Lcom/squareup/thread/executor/MainThread;",
        "libraryLoader",
        "Lcom/squareup/cardreader/loader/LibraryLoader;",
        "minesweeperLogger",
        "Lcom/squareup/ms/Minesweeper$MinesweeperLogger;",
        "ms",
        "Lcom/squareup/ms/Ms;",
        "noopMinesweeper",
        "provideMinesweeperService",
        "Lcom/squareup/ms/MinesweeperService;",
        "serviceCreator",
        "Lcom/squareup/api/ServiceCreator;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ms/ReleaseMinesweeperModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    new-instance v0, Lcom/squareup/ms/ReleaseMinesweeperModule;

    invoke-direct {v0}, Lcom/squareup/ms/ReleaseMinesweeperModule;-><init>()V

    sput-object v0, Lcom/squareup/ms/ReleaseMinesweeperModule;->INSTANCE:Lcom/squareup/ms/ReleaseMinesweeperModule;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final getMinesweeper(Lcom/squareup/ms/MsFactory;)Lcom/squareup/ms/Minesweeper;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "msFactory"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-interface {p0}, Lcom/squareup/ms/MsFactory;->getMinesweeper()Lcom/squareup/ms/Minesweeper;

    move-result-object p0

    const-string v0, "msFactory.minesweeper"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final getMsFactory(Landroid/app/Application;Lcom/squareup/ms/MinesweeperExecutorService;Lcom/squareup/ms/Minesweeper$DataListener;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/loader/LibraryLoader;Lcom/squareup/ms/Minesweeper$MinesweeperLogger;Lcom/squareup/ms/Ms;Lcom/squareup/ms/Minesweeper;)Lcom/squareup/ms/MsFactory;
    .locals 11
    .param p7    # Lcom/squareup/ms/Minesweeper;
        .annotation runtime Lcom/squareup/ms/Noop;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/dagger/AppScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    move-object v0, p1

    const-string v1, "application"

    move-object v3, p0

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "executorService"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "dataListener"

    move-object v4, p2

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "mainThread"

    move-object v7, p3

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "libraryLoader"

    move-object v6, p4

    invoke-static {p4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "minesweeperLogger"

    move-object/from16 v8, p5

    invoke-static {v8, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "ms"

    move-object/from16 v9, p6

    invoke-static {v9, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "noopMinesweeper"

    move-object/from16 v10, p7

    invoke-static {v10, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    new-instance v1, Lcom/squareup/ms/RealMsFactory;

    .line 38
    move-object v5, v0

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    move-object v2, v1

    .line 37
    invoke-direct/range {v2 .. v10}, Lcom/squareup/ms/RealMsFactory;-><init>(Landroid/app/Application;Lcom/squareup/ms/Minesweeper$DataListener;Ljava/util/concurrent/ExecutorService;Lcom/squareup/cardreader/loader/LibraryLoader;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/ms/Minesweeper$MinesweeperLogger;Lcom/squareup/ms/Ms;Lcom/squareup/ms/Minesweeper;)V

    check-cast v1, Lcom/squareup/ms/MsFactory;

    return-object v1
.end method

.method public static final provideMinesweeperService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/ms/MinesweeperService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/dagger/AppScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "serviceCreator"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    const-class v0, Lcom/squareup/ms/MinesweeperService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    const-string v0, "serviceCreator.create(Mi\u2026eeperService::class.java)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Lcom/squareup/ms/MinesweeperService;

    return-object p0
.end method
