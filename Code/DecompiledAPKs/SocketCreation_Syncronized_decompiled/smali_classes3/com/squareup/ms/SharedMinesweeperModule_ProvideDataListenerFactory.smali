.class public final Lcom/squareup/ms/SharedMinesweeperModule_ProvideDataListenerFactory;
.super Ljava/lang/Object;
.source "SharedMinesweeperModule_ProvideDataListenerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ms/Minesweeper$DataListener;",
        ">;"
    }
.end annotation


# instance fields
.field private final minesweeperHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/MinesweeperHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/MinesweeperHelper;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ms/SharedMinesweeperModule_ProvideDataListenerFactory;->minesweeperHelperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ms/SharedMinesweeperModule_ProvideDataListenerFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/MinesweeperHelper;",
            ">;)",
            "Lcom/squareup/ms/SharedMinesweeperModule_ProvideDataListenerFactory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/ms/SharedMinesweeperModule_ProvideDataListenerFactory;

    invoke-direct {v0, p0}, Lcom/squareup/ms/SharedMinesweeperModule_ProvideDataListenerFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideDataListener(Lcom/squareup/ms/MinesweeperHelper;)Lcom/squareup/ms/Minesweeper$DataListener;
    .locals 1

    .line 35
    invoke-static {p0}, Lcom/squareup/ms/SharedMinesweeperModule;->provideDataListener(Lcom/squareup/ms/MinesweeperHelper;)Lcom/squareup/ms/Minesweeper$DataListener;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ms/Minesweeper$DataListener;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ms/Minesweeper$DataListener;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/ms/SharedMinesweeperModule_ProvideDataListenerFactory;->minesweeperHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ms/MinesweeperHelper;

    invoke-static {v0}, Lcom/squareup/ms/SharedMinesweeperModule_ProvideDataListenerFactory;->provideDataListener(Lcom/squareup/ms/MinesweeperHelper;)Lcom/squareup/ms/Minesweeper$DataListener;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ms/SharedMinesweeperModule_ProvideDataListenerFactory;->get()Lcom/squareup/ms/Minesweeper$DataListener;

    move-result-object v0

    return-object v0
.end method
