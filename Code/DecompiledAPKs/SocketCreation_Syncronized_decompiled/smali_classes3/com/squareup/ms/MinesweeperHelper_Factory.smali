.class public final Lcom/squareup/ms/MinesweeperHelper_Factory;
.super Ljava/lang/Object;
.source "MinesweeperHelper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ms/MinesweeperHelper;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final minesweeperServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/MinesweeperService;",
            ">;"
        }
    .end annotation
.end field

.field private final minesweeperTicketProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/MinesweeperTicket;",
            ">;"
        }
    .end annotation
.end field

.field private final ohSnapLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/MinesweeperService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/MinesweeperTicket;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/ms/MinesweeperHelper_Factory;->minesweeperServiceProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/ms/MinesweeperHelper_Factory;->minesweeperTicketProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/ms/MinesweeperHelper_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p4, p0, Lcom/squareup/ms/MinesweeperHelper_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p5, p0, Lcom/squareup/ms/MinesweeperHelper_Factory;->clockProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ms/MinesweeperHelper_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/MinesweeperService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/MinesweeperTicket;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;)",
            "Lcom/squareup/ms/MinesweeperHelper_Factory;"
        }
    .end annotation

    .line 46
    new-instance v6, Lcom/squareup/ms/MinesweeperHelper_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ms/MinesweeperHelper_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/ms/MinesweeperService;Lcom/squareup/ms/MinesweeperTicket;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Clock;)Lcom/squareup/ms/MinesweeperHelper;
    .locals 7

    .line 52
    new-instance v6, Lcom/squareup/ms/MinesweeperHelper;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ms/MinesweeperHelper;-><init>(Lcom/squareup/ms/MinesweeperService;Lcom/squareup/ms/MinesweeperTicket;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Clock;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/ms/MinesweeperHelper;
    .locals 5

    .line 38
    iget-object v0, p0, Lcom/squareup/ms/MinesweeperHelper_Factory;->minesweeperServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ms/MinesweeperService;

    iget-object v1, p0, Lcom/squareup/ms/MinesweeperHelper_Factory;->minesweeperTicketProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ms/MinesweeperTicket;

    iget-object v2, p0, Lcom/squareup/ms/MinesweeperHelper_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/log/OhSnapLogger;

    iget-object v3, p0, Lcom/squareup/ms/MinesweeperHelper_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/analytics/Analytics;

    iget-object v4, p0, Lcom/squareup/ms/MinesweeperHelper_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/util/Clock;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ms/MinesweeperHelper_Factory;->newInstance(Lcom/squareup/ms/MinesweeperService;Lcom/squareup/ms/MinesweeperTicket;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Clock;)Lcom/squareup/ms/MinesweeperHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ms/MinesweeperHelper_Factory;->get()Lcom/squareup/ms/MinesweeperHelper;

    move-result-object v0

    return-object v0
.end method
