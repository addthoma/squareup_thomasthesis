.class public final Lcom/squareup/ms/ReleaseMinesweeperModule_GetMinesweeperFactory;
.super Ljava/lang/Object;
.source "ReleaseMinesweeperModule_GetMinesweeperFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ms/Minesweeper;",
        ">;"
    }
.end annotation


# instance fields
.field private final msFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/MsFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/MsFactory;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/ms/ReleaseMinesweeperModule_GetMinesweeperFactory;->msFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ms/ReleaseMinesweeperModule_GetMinesweeperFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/MsFactory;",
            ">;)",
            "Lcom/squareup/ms/ReleaseMinesweeperModule_GetMinesweeperFactory;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/ms/ReleaseMinesweeperModule_GetMinesweeperFactory;

    invoke-direct {v0, p0}, Lcom/squareup/ms/ReleaseMinesweeperModule_GetMinesweeperFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static getMinesweeper(Lcom/squareup/ms/MsFactory;)Lcom/squareup/ms/Minesweeper;
    .locals 1

    .line 34
    invoke-static {p0}, Lcom/squareup/ms/ReleaseMinesweeperModule;->getMinesweeper(Lcom/squareup/ms/MsFactory;)Lcom/squareup/ms/Minesweeper;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ms/Minesweeper;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ms/Minesweeper;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/ms/ReleaseMinesweeperModule_GetMinesweeperFactory;->msFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ms/MsFactory;

    invoke-static {v0}, Lcom/squareup/ms/ReleaseMinesweeperModule_GetMinesweeperFactory;->getMinesweeper(Lcom/squareup/ms/MsFactory;)Lcom/squareup/ms/Minesweeper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ms/ReleaseMinesweeperModule_GetMinesweeperFactory;->get()Lcom/squareup/ms/Minesweeper;

    move-result-object v0

    return-object v0
.end method
