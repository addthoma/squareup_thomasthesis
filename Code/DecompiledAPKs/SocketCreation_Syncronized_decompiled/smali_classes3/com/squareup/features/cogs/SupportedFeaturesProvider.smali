.class public Lcom/squareup/features/cogs/SupportedFeaturesProvider;
.super Ljava/lang/Object;
.source "SupportedFeaturesProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;
    }
.end annotation


# instance fields
.field public final featuresToSync:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/CatalogFeature;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/CatalogFeature;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/features/cogs/SupportedFeaturesProvider;->featuresToSync:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Ljava/util/List;Lcom/squareup/features/cogs/SupportedFeaturesProvider$1;)V
    .locals 0

    .line 11
    invoke-direct {p0, p1}, Lcom/squareup/features/cogs/SupportedFeaturesProvider;-><init>(Ljava/util/List;)V

    return-void
.end method
