.class public Lcom/squareup/features/cogs/SupportedConnectV2ObjectTypesProvider$Builder;
.super Ljava/lang/Object;
.source "SupportedConnectV2ObjectTypesProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/features/cogs/SupportedConnectV2ObjectTypesProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private featuresToSync:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/features/cogs/SupportedConnectV2ObjectTypesProvider$Builder;->featuresToSync:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addItemOptions(Z)Lcom/squareup/features/cogs/SupportedConnectV2ObjectTypesProvider$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 37
    iget-object p1, p0, Lcom/squareup/features/cogs/SupportedConnectV2ObjectTypesProvider$Builder;->featuresToSync:Ljava/util/List;

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->ITEM_OPTION:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38
    iget-object p1, p0, Lcom/squareup/features/cogs/SupportedConnectV2ObjectTypesProvider$Builder;->featuresToSync:Ljava/util/List;

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->ITEM_OPTION_VAL:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object p0
.end method

.method public addMeasurementUnit(Z)Lcom/squareup/features/cogs/SupportedConnectV2ObjectTypesProvider$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 30
    iget-object p1, p0, Lcom/squareup/features/cogs/SupportedConnectV2ObjectTypesProvider$Builder;->featuresToSync:Ljava/util/List;

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->MEASUREMENT_UNIT:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object p0
.end method

.method public addQuickAmountsOptions(Z)Lcom/squareup/features/cogs/SupportedConnectV2ObjectTypesProvider$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 45
    iget-object p1, p0, Lcom/squareup/features/cogs/SupportedConnectV2ObjectTypesProvider$Builder;->featuresToSync:Ljava/util/List;

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->QUICK_AMOUNTS_SETTINGS:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object p0
.end method

.method public build()Lcom/squareup/features/cogs/SupportedConnectV2ObjectTypesProvider;
    .locals 3

    .line 51
    new-instance v0, Lcom/squareup/features/cogs/SupportedConnectV2ObjectTypesProvider;

    iget-object v1, p0, Lcom/squareup/features/cogs/SupportedConnectV2ObjectTypesProvider$Builder;->featuresToSync:Ljava/util/List;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/squareup/features/cogs/SupportedConnectV2ObjectTypesProvider;-><init>(Ljava/util/List;Lcom/squareup/features/cogs/SupportedConnectV2ObjectTypesProvider$1;)V

    return-object v0
.end method
