.class public Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;
.super Ljava/lang/Object;
.source "SupportedFeaturesProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/features/cogs/SupportedFeaturesProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# static fields
.field private static RESTAURANT_FEATURES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/CatalogFeature;",
            ">;"
        }
    .end annotation
.end field

.field private static RETAIL_FEATURES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/CatalogFeature;",
            ">;"
        }
    .end annotation
.end field

.field private static STANDARD_FEATURES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/CatalogFeature;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private featuresToSync:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/CatalogFeature;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 12

    const/16 v0, 0xc

    new-array v0, v0, [Lcom/squareup/api/sync/CatalogFeature;

    .line 21
    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->ADVANCED_MODIFIERS:Lcom/squareup/api/sync/CatalogFeature;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->APPOINTMENTS_SERVICE:Lcom/squareup/api/sync/CatalogFeature;

    const/4 v3, 0x1

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->COMP_DISCOUNTS:Lcom/squareup/api/sync/CatalogFeature;

    const/4 v4, 0x2

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->CONDITIONAL_TAXES:Lcom/squareup/api/sync/CatalogFeature;

    const/4 v5, 0x3

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->DINING_OPTIONS:Lcom/squareup/api/sync/CatalogFeature;

    const/4 v6, 0x4

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->FLEXIBLE_PAGE_LAYOUTS:Lcom/squareup/api/sync/CatalogFeature;

    const/4 v7, 0x5

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->GIFT_CARDS:Lcom/squareup/api/sync/CatalogFeature;

    const/4 v8, 0x6

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->PREDEFINED_TICKETS:Lcom/squareup/api/sync/CatalogFeature;

    const/4 v9, 0x7

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->VOID_REASONS:Lcom/squareup/api/sync/CatalogFeature;

    const/16 v10, 0x8

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->PRICING_RULES:Lcom/squareup/api/sync/CatalogFeature;

    const/16 v11, 0x9

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->PRODUCTSETS_ALL_PRODUCTS:Lcom/squareup/api/sync/CatalogFeature;

    const/16 v11, 0xa

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->LESSER_EQUAL_VALUE:Lcom/squareup/api/sync/CatalogFeature;

    const/16 v11, 0xb

    aput-object v1, v0, v11

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;->STANDARD_FEATURES:Ljava/util/List;

    new-array v0, v10, [Lcom/squareup/api/sync/CatalogFeature;

    .line 35
    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->FLEXIBLE_PAGE_LAYOUTS:Lcom/squareup/api/sync/CatalogFeature;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->MENU_GROUPS:Lcom/squareup/api/sync/CatalogFeature;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->MENU_GROUP_PAGES:Lcom/squareup/api/sync/CatalogFeature;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->MENU_GROUP_MEMBERSHIPS:Lcom/squareup/api/sync/CatalogFeature;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->FUNCTION_PLACEHOLDER_TYPES:Lcom/squareup/api/sync/CatalogFeature;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->PLACEHOLDER_TYPE_CUSTOM_AMOUNT:Lcom/squareup/api/sync/CatalogFeature;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->SURCHARGES:Lcom/squareup/api/sync/CatalogFeature;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->SURCHARGE_TAXABILITY:Lcom/squareup/api/sync/CatalogFeature;

    aput-object v1, v0, v9

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;->RESTAURANT_FEATURES:Ljava/util/List;

    .line 45
    sget-object v0, Lcom/squareup/api/sync/CatalogFeature;->FAVORITES_LIST:Lcom/squareup/api/sync/CatalogFeature;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;->RETAIL_FEATURES:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    sget-object v1, Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;->STANDARD_FEATURES:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;->featuresToSync:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addCheckoutV2Features(Z)Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 96
    iget-object p1, p0, Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;->featuresToSync:Ljava/util/List;

    sget-object v0, Lcom/squareup/api/sync/CatalogFeature;->MOBILE_PAGE_LAYOUT:Lcom/squareup/api/sync/CatalogFeature;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object p0
.end method

.method public addFractionalQuantityFeature(Z)Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 68
    iget-object p1, p0, Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;->featuresToSync:Ljava/util/List;

    sget-object v0, Lcom/squareup/api/sync/CatalogFeature;->FRACTIONAL_QUANTITIES:Lcom/squareup/api/sync/CatalogFeature;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object p0
.end method

.method public addItemOptionsFeature(Z)Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 75
    iget-object p1, p0, Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;->featuresToSync:Ljava/util/List;

    sget-object v0, Lcom/squareup/api/sync/CatalogFeature;->ITEM_OPTIONS:Lcom/squareup/api/sync/CatalogFeature;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object p0
.end method

.method public addLimitVariationFeature(Z)Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 82
    iget-object p1, p0, Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;->featuresToSync:Ljava/util/List;

    sget-object v0, Lcom/squareup/api/sync/CatalogFeature;->ITEM_VARIATION_LIMIT:Lcom/squareup/api/sync/CatalogFeature;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object p0
.end method

.method public addRestaurantFeatures(Z)Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;
    .locals 2

    .line 54
    iget-object v0, p0, Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;->featuresToSync:Ljava/util/List;

    sget-object v1, Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;->RESTAURANT_FEATURES:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    if-eqz p1, :cond_0

    .line 56
    iget-object p1, p0, Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;->featuresToSync:Ljava/util/List;

    sget-object v0, Lcom/squareup/api/sync/CatalogFeature;->FLOOR_PLANS:Lcom/squareup/api/sync/CatalogFeature;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object p0
.end method

.method public addRetailFeatures()Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;
    .locals 2

    .line 62
    iget-object v0, p0, Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;->featuresToSync:Ljava/util/List;

    sget-object v1, Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;->RETAIL_FEATURES:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-object p0
.end method

.method public addWholePurchaseDiscountsFeature(Z)Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 89
    iget-object p1, p0, Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;->featuresToSync:Ljava/util/List;

    sget-object v0, Lcom/squareup/api/sync/CatalogFeature;->WHOLE_PURCHASE:Lcom/squareup/api/sync/CatalogFeature;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object p0
.end method

.method public build()Lcom/squareup/features/cogs/SupportedFeaturesProvider;
    .locals 3

    .line 102
    new-instance v0, Lcom/squareup/features/cogs/SupportedFeaturesProvider;

    iget-object v1, p0, Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;->featuresToSync:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/squareup/features/cogs/SupportedFeaturesProvider;-><init>(Ljava/util/List;Lcom/squareup/features/cogs/SupportedFeaturesProvider$1;)V

    return-object v0
.end method
