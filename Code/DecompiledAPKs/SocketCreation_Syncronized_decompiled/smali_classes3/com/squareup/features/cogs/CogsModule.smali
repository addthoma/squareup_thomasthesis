.class public Lcom/squareup/features/cogs/CogsModule;
.super Ljava/lang/Object;
.source "CogsModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static realCogs(Lcom/squareup/cogs/CogsService;Lcom/squareup/server/catalog/CatalogConnectV2Service;Lcom/squareup/settings/server/AccountStatusSettings;Lrx/Scheduler;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/persistent/PersistentFactory;Lcom/squareup/FileThreadEnforcer;Ljava/io/File;Ljava/util/concurrent/Executor;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Clock;Landroid/app/Application;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/settings/server/Features;)Lcom/squareup/cogs/RealCogs;
    .locals 22
    .param p3    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p8    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    move-object/from16 v0, p13

    .line 55
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    .line 56
    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object v1

    .line 57
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/settings/server/AccountStatusSettings;->shouldLimitVariations()Z

    move-result v2

    .line 58
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/settings/server/AccountStatusSettings;->shouldSyncItemOptions()Z

    move-result v3

    .line 59
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/settings/server/AccountStatusSettings;->shouldEnableWholePurchaseDiscounts()Z

    move-result v4

    .line 60
    sget-object v5, Lcom/squareup/settings/server/Features$Feature;->QUICK_AMOUNTS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v5}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v5

    .line 61
    sget-object v6, Lcom/squareup/settings/server/Features$Feature;->CHECKOUT_APPLET_V2:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v6}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    .line 62
    new-instance v6, Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;

    invoke-direct {v6}, Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;-><init>()V

    const/4 v7, 0x1

    .line 63
    invoke-virtual {v6, v7}, Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;->addFractionalQuantityFeature(Z)Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;

    move-result-object v6

    .line 64
    invoke-virtual {v6, v2}, Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;->addLimitVariationFeature(Z)Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;

    move-result-object v2

    .line 65
    invoke-virtual {v2, v3}, Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;->addItemOptionsFeature(Z)Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;

    move-result-object v2

    .line 66
    invoke-virtual {v2, v4}, Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;->addWholePurchaseDiscountsFeature(Z)Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;

    move-result-object v2

    .line 67
    invoke-virtual {v2, v0}, Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;->addCheckoutV2Features(Z)Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;

    move-result-object v0

    .line 68
    invoke-virtual {v0}, Lcom/squareup/features/cogs/SupportedFeaturesProvider$Builder;->build()Lcom/squareup/features/cogs/SupportedFeaturesProvider;

    move-result-object v0

    .line 69
    new-instance v2, Lcom/squareup/features/cogs/SupportedConnectV2ObjectTypesProvider$Builder;

    invoke-direct {v2}, Lcom/squareup/features/cogs/SupportedConnectV2ObjectTypesProvider$Builder;-><init>()V

    .line 71
    invoke-virtual {v2, v7}, Lcom/squareup/features/cogs/SupportedConnectV2ObjectTypesProvider$Builder;->addMeasurementUnit(Z)Lcom/squareup/features/cogs/SupportedConnectV2ObjectTypesProvider$Builder;

    move-result-object v2

    .line 72
    invoke-virtual {v2, v3}, Lcom/squareup/features/cogs/SupportedConnectV2ObjectTypesProvider$Builder;->addItemOptions(Z)Lcom/squareup/features/cogs/SupportedConnectV2ObjectTypesProvider$Builder;

    move-result-object v2

    .line 73
    invoke-virtual {v2, v5}, Lcom/squareup/features/cogs/SupportedConnectV2ObjectTypesProvider$Builder;->addQuickAmountsOptions(Z)Lcom/squareup/features/cogs/SupportedConnectV2ObjectTypesProvider$Builder;

    move-result-object v2

    .line 74
    invoke-virtual {v2}, Lcom/squareup/features/cogs/SupportedConnectV2ObjectTypesProvider$Builder;->build()Lcom/squareup/features/cogs/SupportedConnectV2ObjectTypesProvider;

    move-result-object v2

    .line 76
    new-instance v9, Lcom/squareup/features/cogs/ItemsSyncEndpoint;

    move-object/from16 v3, p0

    invoke-direct {v9, v3, v0, v1, v7}, Lcom/squareup/features/cogs/ItemsSyncEndpoint;-><init>(Lcom/squareup/cogs/CogsService;Lcom/squareup/features/cogs/SupportedFeaturesProvider;Ljava/lang/String;Z)V

    .line 78
    new-instance v10, Lcom/squareup/cogs/CatalogConnectV2EndpointImpl;

    move-object/from16 v0, p1

    invoke-direct {v10, v0}, Lcom/squareup/cogs/CatalogConnectV2EndpointImpl;-><init>(Lcom/squareup/server/catalog/CatalogConnectV2Service;)V

    .line 80
    new-instance v0, Lcom/squareup/cogs/RealCatalogAnalytics;

    move-object/from16 v1, p9

    invoke-direct {v0, v1}, Lcom/squareup/cogs/RealCatalogAnalytics;-><init>(Lcom/squareup/analytics/Analytics;)V

    .line 81
    new-instance v1, Lcom/squareup/cogs/RealCatalogLogger;

    invoke-direct {v1}, Lcom/squareup/cogs/RealCatalogLogger;-><init>()V

    .line 82
    invoke-static {v1}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->installLogger(Lcom/squareup/shared/catalog/logging/CatalogLogger;)V

    .line 84
    new-instance v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;

    invoke-direct {v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTable;-><init>()V

    .line 85
    new-instance v3, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable;

    invoke-direct {v3}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable;-><init>()V

    .line 86
    new-instance v4, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader;

    invoke-direct {v4, v3, v1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader;-><init>(Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable;Lcom/squareup/shared/catalog/synthetictables/LibraryTable;)V

    .line 88
    new-instance v5, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    const/4 v6, 0x0

    invoke-direct {v5, v3, v1, v6}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;-><init>(Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable;Lcom/squareup/shared/catalog/synthetictables/LibraryTable;Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable;)V

    .line 90
    new-instance v1, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable;

    invoke-direct {v1}, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable;-><init>()V

    .line 91
    new-instance v3, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader;

    invoke-direct {v3, v1}, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader;-><init>(Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable;)V

    .line 93
    new-instance v1, Lcom/squareup/cogs/CogsBuilder;

    iget-object v2, v2, Lcom/squareup/features/cogs/SupportedConnectV2ObjectTypesProvider;->objectTypesToSync:Ljava/util/List;

    move-object v8, v1

    move-object/from16 v11, p3

    move-object/from16 v12, p4

    move-object/from16 v13, p5

    move-object/from16 v14, p6

    move-object/from16 v15, p7

    move-object/from16 v16, p8

    move-object/from16 v17, v0

    move-object/from16 v18, p10

    move-object/from16 v19, p11

    move-object/from16 v20, p12

    move-object/from16 v21, v2

    invoke-direct/range {v8 .. v21}, Lcom/squareup/cogs/CogsBuilder;-><init>(Lcom/squareup/shared/catalog/CatalogEndpoint;Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;Lrx/Scheduler;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/persistent/PersistentFactory;Lcom/squareup/FileThreadEnforcer;Ljava/io/File;Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/logging/CatalogAnalytics;Lcom/squareup/util/Clock;Landroid/app/Application;Lcom/squareup/badbus/BadEventSink;Ljava/util/List;)V

    .line 96
    invoke-virtual {v1, v4}, Lcom/squareup/cogs/CogsBuilder;->registerSyntheticTableReader(Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;)Lcom/squareup/cogs/CogsBuilder;

    move-result-object v0

    .line 97
    invoke-virtual {v0, v5}, Lcom/squareup/cogs/CogsBuilder;->registerSyntheticTableReader(Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;)Lcom/squareup/cogs/CogsBuilder;

    move-result-object v0

    .line 98
    invoke-virtual {v0, v3}, Lcom/squareup/cogs/CogsBuilder;->registerSyntheticTableReader(Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;)Lcom/squareup/cogs/CogsBuilder;

    move-result-object v0

    .line 99
    invoke-virtual {v0}, Lcom/squareup/cogs/CogsBuilder;->build()Lcom/squareup/cogs/RealCogs;

    move-result-object v0

    return-object v0
.end method
