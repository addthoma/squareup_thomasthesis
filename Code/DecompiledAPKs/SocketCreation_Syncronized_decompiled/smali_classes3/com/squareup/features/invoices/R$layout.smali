.class public final Lcom/squareup/features/invoices/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/features/invoices/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final add_payment_view:I = 0x7f0d004c

.field public static final additional_recipients_view:I = 0x7f0d004e

.field public static final cancel_invoice_view:I = 0x7f0d00ab

.field public static final choose_date_view:I = 0x7f0d00e4

.field public static final confirmation_view:I = 0x7f0d0106

.field public static final custom_date_view:I = 0x7f0d01a8

.field public static final dropdown_item:I = 0x7f0d01ee

.field public static final edit_invoice_1_view:I = 0x7f0d01fb

.field public static final edit_invoice_2_view:I = 0x7f0d01fc

.field public static final edit_invoice_bottom_button:I = 0x7f0d01fd

.field public static final edit_invoice_v2_confirmation_view:I = 0x7f0d01fe

.field public static final edit_invoice_v2_invoice_detail_view:I = 0x7f0d01ff

.field public static final edit_invoice_view:I = 0x7f0d0200

.field public static final edit_payment_request_screen:I = 0x7f0d021b

.field public static final edit_payment_request_v2_screen:I = 0x7f0d021c

.field public static final edit_payment_schedule_screen:I = 0x7f0d021d

.field public static final ends_date_view:I = 0x7f0d025a

.field public static final ends_view:I = 0x7f0d025b

.field public static final estimates_banner:I = 0x7f0d0265

.field public static final image_or_icon_content:I = 0x7f0d02cd

.field public static final invoice_attachment_view:I = 0x7f0d02da

.field public static final invoice_automatic_payment_view:I = 0x7f0d02db

.field public static final invoice_automatic_reminder_edit_view:I = 0x7f0d02dc

.field public static final invoice_automatic_reminders_list_view:I = 0x7f0d02dd

.field public static final invoice_bill_history_view:I = 0x7f0d02de

.field public static final invoice_bottom_sheet_dialog:I = 0x7f0d02e1

.field public static final invoice_choose_date_view:I = 0x7f0d02e3

.field public static final invoice_confirm_button_container:I = 0x7f0d02e5

.field public static final invoice_confirmation:I = 0x7f0d02e6

.field public static final invoice_delivery_method_view:I = 0x7f0d02e9

.field public static final invoice_image_attachment_content:I = 0x7f0d02ed

.field public static final invoice_image_attachment_view:I = 0x7f0d02ee

.field public static final invoice_message_view:I = 0x7f0d02ef

.field public static final invoice_mobile_analytics_row:I = 0x7f0d02f0

.field public static final invoice_null_state:I = 0x7f0d02f1

.field public static final invoice_payment_type_view:I = 0x7f0d02f3

.field public static final invoice_preview:I = 0x7f0d02f4

.field public static final invoice_read_write_detail_view:I = 0x7f0d02f7

.field public static final invoice_state_filter_dropdown_header:I = 0x7f0d02fa

.field public static final invoice_state_filter_layout:I = 0x7f0d02fb

.field public static final invoice_timeline:I = 0x7f0d02fc

.field public static final invoice_upload_confirmation_view:I = 0x7f0d02fe

.field public static final invoices_applet_invoice_filter_master_view:I = 0x7f0d02ff

.field public static final invoices_applet_invoice_history_view:I = 0x7f0d0300

.field public static final invoices_create_invoice_button:I = 0x7f0d0301

.field public static final invoices_item_select_card:I = 0x7f0d0302

.field public static final invoices_list_header_row:I = 0x7f0d0303

.field public static final no_invoice_search_results:I = 0x7f0d0385

.field public static final overflow_view:I = 0x7f0d0419

.field public static final pay_invoice_email_row_view:I = 0x7f0d042e

.field public static final pay_invoice_email_row_view_no_plus:I = 0x7f0d042f

.field public static final pay_invoice_keypad_layout:I = 0x7f0d0430

.field public static final payment_plan_section:I = 0x7f0d043a

.field public static final payment_plan_section_contents:I = 0x7f0d043b

.field public static final payment_request_row:I = 0x7f0d043e

.field public static final payment_request_row_contents:I = 0x7f0d043f

.field public static final preview_attachment_content:I = 0x7f0d0450

.field public static final record_payment_method_view:I = 0x7f0d0474

.field public static final record_payment_view:I = 0x7f0d0475

.field public static final recurring_frequency_view:I = 0x7f0d0476

.field public static final repeat_every_view:I = 0x7f0d047c

.field public static final send_reminder_view:I = 0x7f0d04c5


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
