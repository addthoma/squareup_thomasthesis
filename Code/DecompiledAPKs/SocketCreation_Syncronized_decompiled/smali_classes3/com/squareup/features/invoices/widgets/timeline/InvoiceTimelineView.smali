.class public final Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;
.super Landroid/widget/LinearLayout;
.source "InvoiceTimelineView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;,
        Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceTimelineView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceTimelineView.kt\ncom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,236:1\n1103#2,7:237\n*E\n*S KotlinDebug\n*F\n+ 1 InvoiceTimelineView.kt\ncom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView\n*L\n146#1,7:237\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000  2\u00020\u0001:\u0002 !B\u001b\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006J4\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0013\u001a\u00020\u0011H\u0002J*\u0010\u0014\u001a\u00020\u000b2\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00112\u0006\u0010\u0019\u001a\u00020\rH\u0002J\u0008\u0010\u001a\u001a\u00020\u000bH\u0002J\u0018\u0010\u001b\u001a\u00020\u000b2\u000e\u0010\u001c\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\u001dH\u0002J\u0014\u0010\u001e\u001a\u00020\u000b*\u00020\u001f2\u0006\u0010\u0019\u001a\u00020\rH\u0002R\u000e\u0010\u0007\u001a\u00020\u0001X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;",
        "Landroid/widget/LinearLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "timelineContent",
        "viewAllActivityButton",
        "Lcom/squareup/marketfont/MarketButton;",
        "addEventRow",
        "",
        "title",
        "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;",
        "detailed",
        "",
        "showAllDetailedContent",
        "",
        "subtitle",
        "isLastRow",
        "addTitleRow",
        "titleText",
        "backgroundColor",
        "",
        "eventLineVisibility",
        "invoiceTimelineCta",
        "bindViews",
        "setButtonVisibleOrGone",
        "block",
        "Lkotlin/Function0;",
        "setText",
        "Lcom/squareup/marketfont/MarketTextView;",
        "Companion",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final MAX_EVENT_ROWS:I = 0x2


# instance fields
.field private timelineContent:Landroid/widget/LinearLayout;

.field private viewAllActivityButton:Lcom/squareup/marketfont/MarketButton;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;->Companion:Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Companion;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 52
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 58
    sget p2, Lcom/squareup/features/invoices/widgets/impl/R$layout;->invoice_timeline_view:I

    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {p1, p2, v0}, Landroid/widget/LinearLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p1, 0x1

    .line 59
    invoke-virtual {p0, p1}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;->setOrientation(I)V

    .line 60
    invoke-direct {p0}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;->bindViews()V

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 51
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public static final synthetic access$Companion()Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Companion;
    .locals 1

    sget-object v0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;->Companion:Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Companion;

    return-object v0
.end method

.method public static final synthetic access$addEventRow(Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;Ljava/lang/String;ZLjava/lang/String;Z)V
    .locals 0

    .line 48
    invoke-direct/range {p0 .. p5}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;->addEventRow(Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;Ljava/lang/String;ZLjava/lang/String;Z)V

    return-void
.end method

.method public static final synthetic access$addTitleRow(Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;Ljava/lang/String;IZLcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;)V
    .locals 0

    .line 48
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;->addTitleRow(Ljava/lang/String;IZLcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;)V

    return-void
.end method

.method public static final synthetic access$setButtonVisibleOrGone(Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;Lkotlin/jvm/functions/Function0;)V
    .locals 0

    .line 48
    invoke-direct {p0, p1}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;->setButtonVisibleOrGone(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final addEventRow(Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;Ljava/lang/String;ZLjava/lang/String;Z)V
    .locals 3

    .line 108
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/features/invoices/widgets/impl/R$layout;->invoice_event_row:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/LinearLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 110
    check-cast p2, Ljava/lang/CharSequence;

    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    if-eqz p3, :cond_0

    .line 112
    sget p3, Lcom/squareup/features/invoices/widgets/impl/R$id;->event_detailed_full:I

    invoke-virtual {v0, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    const-string v1, "eventRow.findViewById(R.id.event_detailed_full)"

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p3, Lcom/squareup/marketfont/MarketTextView;

    goto :goto_0

    .line 114
    :cond_0
    sget p3, Lcom/squareup/features/invoices/widgets/impl/R$id;->event_detailed_truncated:I

    invoke-virtual {v0, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    if-eqz p3, :cond_1

    check-cast p3, Lcom/squareup/widgets/ShorteningTextView;

    check-cast p3, Lcom/squareup/marketfont/MarketTextView;

    .line 116
    :goto_0
    invoke-virtual {p3, p2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    const/4 p2, 0x0

    .line 117
    invoke-virtual {p3, p2}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    goto :goto_1

    .line 114
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.widgets.ShorteningTextView"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 120
    :cond_2
    :goto_1
    sget p2, Lcom/squareup/features/invoices/widgets/impl/R$id;->invoice_event_line:I

    invoke-virtual {v0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string p3, "eventRow.findViewById<Vi\u2026(R.id.invoice_event_line)"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    xor-int/lit8 p3, p5, 0x1

    .line 121
    invoke-static {p2, p3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 123
    sget p2, Lcom/squareup/features/invoices/widgets/impl/R$id;->event_title:I

    invoke-virtual {v0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/marketfont/MarketTextView;

    const-string/jumbo p3, "titleView"

    .line 124
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p2, p1}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;->setText(Lcom/squareup/marketfont/MarketTextView;Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;)V

    .line 126
    sget p1, Lcom/squareup/features/invoices/widgets/impl/R$id;->event_subtitle:I

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    const-string p2, "subTitleView"

    .line 127
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p4, Ljava/lang/CharSequence;

    invoke-virtual {p1, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    iget-object p1, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;->timelineContent:Landroid/widget/LinearLayout;

    if-nez p1, :cond_3

    const-string/jumbo p2, "timelineContent"

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method private final addTitleRow(Ljava/lang/String;IZLcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;)V
    .locals 5

    .line 77
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/features/invoices/widgets/impl/R$layout;->invoice_event_row:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/LinearLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 78
    sget v1, Lcom/squareup/features/invoices/widgets/impl/R$id;->event_title:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/marketfont/MarketTextView;

    .line 79
    sget v2, Lcom/squareup/features/invoices/widgets/impl/R$id;->event_subtitle:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/squareup/marketfont/MarketTextView;

    const-string v3, "subTitleView"

    .line 81
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v2, p4}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;->setText(Lcom/squareup/marketfont/MarketTextView;Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;)V

    .line 83
    sget p4, Lcom/squareup/features/invoices/widgets/impl/R$id;->invoice_event_bullet_highlight:I

    invoke-virtual {v0, p4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p4

    .line 84
    sget v2, Lcom/squareup/features/invoices/widgets/impl/R$id;->invoice_event_bullet:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const-string v3, "bulletHighlight"

    .line 85
    invoke-static {p4, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-virtual {p4, v3}, Landroid/view/View;->setVisibility(I)V

    .line 86
    invoke-virtual {p4}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object p4

    invoke-virtual {p4}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object p4

    const-string v3, "null cannot be cast to non-null type android.graphics.drawable.GradientDrawable"

    if-eqz p4, :cond_2

    check-cast p4, Landroid/graphics/drawable/GradientDrawable;

    const/4 v4, 0x1

    invoke-virtual {p4, v4, p2}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    const-string p4, "bullet"

    .line 87
    invoke-static {v2, p4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object p4

    invoke-virtual {p4}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object p4

    if-eqz p4, :cond_1

    check-cast p4, Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {p4, p2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 89
    invoke-virtual {v1, p2}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    const-string/jumbo p2, "titleView"

    .line 90
    invoke-static {v1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object p2, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v1, p2}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 91
    sget p2, Lcom/squareup/features/invoices/widgets/impl/R$id;->invoice_event_line:I

    invoke-virtual {v0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string/jumbo p4, "titleRow.findViewById<Vi\u2026(R.id.invoice_event_line)"

    invoke-static {p2, p4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    invoke-static {p2, p3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 93
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v1, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    iget-object p1, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;->timelineContent:Landroid/widget/LinearLayout;

    if-nez p1, :cond_0

    const-string/jumbo p2, "timelineContent"

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void

    .line 87
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v3}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 86
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v3}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final bindViews()V
    .locals 2

    .line 154
    sget v0, Lcom/squareup/features/invoices/widgets/impl/R$id;->timeline_content:I

    invoke-virtual {p0, v0}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.timeline_content)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;->timelineContent:Landroid/widget/LinearLayout;

    .line 155
    sget v0, Lcom/squareup/features/invoices/widgets/impl/R$id;->view_all_activity:I

    invoke-virtual {p0, v0}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.view_all_activity)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;->viewAllActivityButton:Lcom/squareup/marketfont/MarketButton;

    return-void
.end method

.method private final setButtonVisibleOrGone(Lkotlin/jvm/functions/Function0;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 145
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;->viewAllActivityButton:Lcom/squareup/marketfont/MarketButton;

    const-string/jumbo v1, "viewAllActivityButton"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    if-eqz p1, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    if-eqz p1, :cond_3

    .line 147
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;->viewAllActivityButton:Lcom/squareup/marketfont/MarketButton;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v0, Landroid/view/View;

    .line 237
    new-instance v1, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$setButtonVisibleOrGone$$inlined$let$lambda$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$setButtonVisibleOrGone$$inlined$let$lambda$1;-><init>(Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;Lkotlin/jvm/functions/Function0;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    return-void
.end method

.method private final setText(Lcom/squareup/marketfont/MarketTextView;Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;)V
    .locals 2

    .line 133
    invoke-virtual {p2}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;->toCharSequence()Ljava/lang/CharSequence;

    move-result-object v0

    .line 135
    instance-of v1, p2, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$None;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    instance-of v1, p2, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Disabled;

    if-eqz v1, :cond_1

    :goto_0
    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 136
    :cond_1
    instance-of p2, p2, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Clickable;

    if-eqz p2, :cond_2

    const/4 p2, 0x1

    .line 137
    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setClickable(Z)V

    .line 138
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 139
    sget-object p2, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {p1, v0, p2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    :cond_2
    :goto_1
    return-void
.end method
