.class public abstract Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;
.super Ljava/lang/Object;
.source "InvoiceTimelineCta.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$None;,
        Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Disabled;,
        Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Clickable;,
        Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceTimelineCta.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceTimelineCta.kt\ncom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta\n*L\n1#1,120:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0010\r\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0004\t\n\u000b\u000cB\u0011\u0008\u0002\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0003\r\u000e\u000f\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;",
        "",
        "defaultString",
        "",
        "(Ljava/lang/String;)V",
        "getDefaultString",
        "()Ljava/lang/String;",
        "toCharSequence",
        "",
        "Clickable",
        "Disabled",
        "Factory",
        "None",
        "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$None;",
        "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Disabled;",
        "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Clickable;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final defaultString:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;->defaultString:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getDefaultString()Ljava/lang/String;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;->defaultString:Ljava/lang/String;

    return-object v0
.end method

.method public final toCharSequence()Ljava/lang/CharSequence;
    .locals 6

    .line 45
    instance-of v0, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$None;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;->defaultString:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    goto/16 :goto_3

    .line 46
    :cond_0
    instance-of v0, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Disabled;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_3

    .line 47
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;->defaultString:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 48
    move-object v3, v0

    check-cast v3, Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-lez v3, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    .line 49
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0x20

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-object v0, p0

    check-cast v0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Disabled;

    invoke-virtual {v0}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Disabled;->getCtaText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 51
    :cond_2
    move-object v0, p0

    check-cast v0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Disabled;

    invoke-virtual {v0}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Disabled;->getCtaText()Ljava/lang/String;

    move-result-object v0

    .line 48
    :goto_1
    check-cast v0, Ljava/lang/CharSequence;

    goto :goto_3

    .line 54
    :cond_3
    instance-of v0, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Clickable;

    if-eqz v0, :cond_6

    .line 55
    new-instance v0, Landroid/text/SpannableStringBuilder;

    iget-object v3, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;->defaultString:Ljava/lang/String;

    invoke-static {v3}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-direct {v0, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 56
    move-object v3, v0

    check-cast v3, Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-lez v4, :cond_4

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_5

    const-string v1, " "

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 57
    :cond_5
    move-object v1, p0

    check-cast v1, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Clickable;

    invoke-virtual {v1}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Clickable;->getCta()Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;

    move-result-object v4

    iget-object v4, v4, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->text:Ljava/lang/String;

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v0, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 58
    new-instance v4, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$toCharSequence$clickableSpan$1;

    invoke-direct {v4, p0}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$toCharSequence$clickableSpan$1;-><init>(Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;)V

    .line 63
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    invoke-virtual {v1}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Clickable;->getCta()Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->text:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v5, v1

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    invoke-virtual {v0, v4, v5, v1, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    move-object v0, v3

    :goto_3
    return-object v0

    .line 64
    :cond_6
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method
