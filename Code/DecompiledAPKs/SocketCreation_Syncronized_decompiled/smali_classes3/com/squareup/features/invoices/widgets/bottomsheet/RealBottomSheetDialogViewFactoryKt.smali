.class public final Lcom/squareup/features/invoices/widgets/bottomsheet/RealBottomSheetDialogViewFactoryKt;
.super Ljava/lang/Object;
.source "RealBottomSheetDialogViewFactory.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealBottomSheetDialogViewFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealBottomSheetDialogViewFactory.kt\ncom/squareup/features/invoices/widgets/bottomsheet/RealBottomSheetDialogViewFactoryKt\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,77:1\n1103#2,7:78\n*E\n*S KotlinDebug\n*F\n+ 1 RealBottomSheetDialogViewFactory.kt\ncom/squareup/features/invoices/widgets/bottomsheet/RealBottomSheetDialogViewFactoryKt\n*L\n62#1,7:78\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u001c\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0002\u001a\u001c\u0010\u0000\u001a\u00020\u0001*\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0002\u001a\u000c\u0010\u0008\u001a\u00020\u0001*\u00020\u0001H\u0002\u00a8\u0006\t"
    }
    d2 = {
        "render",
        "Landroid/view/View;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;",
        "parent",
        "Landroid/view/ViewGroup;",
        "eventHandler",
        "Lcom/squareup/features/invoices/widgets/EventHandler;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;",
        "withLayoutParams",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$render(Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;Landroid/view/ViewGroup;Lcom/squareup/features/invoices/widgets/EventHandler;)Landroid/view/View;
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/squareup/features/invoices/widgets/bottomsheet/RealBottomSheetDialogViewFactoryKt;->render(Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;Landroid/view/ViewGroup;Lcom/squareup/features/invoices/widgets/EventHandler;)Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$render(Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;Landroid/view/ViewGroup;Lcom/squareup/features/invoices/widgets/EventHandler;)Landroid/view/View;
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/squareup/features/invoices/widgets/bottomsheet/RealBottomSheetDialogViewFactoryKt;->render(Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;Landroid/view/ViewGroup;Lcom/squareup/features/invoices/widgets/EventHandler;)Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$withLayoutParams(Landroid/view/View;)Landroid/view/View;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/features/invoices/widgets/bottomsheet/RealBottomSheetDialogViewFactoryKt;->withLayoutParams(Landroid/view/View;)Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private static final render(Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;Landroid/view/ViewGroup;Lcom/squareup/features/invoices/widgets/EventHandler;)Landroid/view/View;
    .locals 1

    .line 59
    sget v0, Lcom/squareup/features/invoices/widgets/impl/R$layout;->invoice_bottom_dialog_button:I

    .line 58
    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketButton;

    .line 61
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;->getTitle()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    .line 62
    check-cast p1, Landroid/view/View;

    .line 78
    new-instance v0, Lcom/squareup/features/invoices/widgets/bottomsheet/RealBottomSheetDialogViewFactoryKt$render$$inlined$onClickDebounced$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/features/invoices/widgets/bottomsheet/RealBottomSheetDialogViewFactoryKt$render$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;Lcom/squareup/features/invoices/widgets/EventHandler;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p1
.end method

.method private static final render(Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;Landroid/view/ViewGroup;Lcom/squareup/features/invoices/widgets/EventHandler;)Landroid/view/View;
    .locals 1

    .line 44
    sget v0, Lcom/squareup/features/invoices/widgets/impl/R$layout;->invoice_bottom_dialog_confirm_button:I

    .line 43
    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ConfirmButton;

    .line 46
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;->getInitialText()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ConfirmButton;->setInitialText(Ljava/lang/CharSequence;)V

    .line 47
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;->getConfirmText()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ConfirmButton;->setConfirmText(Ljava/lang/CharSequence;)V

    .line 48
    new-instance v0, Lcom/squareup/features/invoices/widgets/bottomsheet/RealBottomSheetDialogViewFactoryKt$render$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/features/invoices/widgets/bottomsheet/RealBottomSheetDialogViewFactoryKt$render$1;-><init>(Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;Lcom/squareup/features/invoices/widgets/EventHandler;)V

    check-cast v0, Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ConfirmButton;->setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V

    .line 51
    check-cast p1, Landroid/view/View;

    return-object p1
.end method

.method private static final withLayoutParams(Landroid/view/View;)Landroid/view/View;
    .locals 3

    .line 69
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 73
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/marin/R$dimen;->marin_gutter_quarter:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 74
    check-cast v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-object p0
.end method
