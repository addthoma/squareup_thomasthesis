.class public final Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$addAttachmentRow$$inlined$apply$lambda$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->addAttachmentRow(Landroid/widget/LinearLayout;Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViews.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Views.kt\ncom/squareup/util/Views$onClickDebounced$1\n+ 2 InvoiceV1SectionRenderer.kt\ncom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer\n*L\n1#1,1322:1\n466#2,2:1323\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006\u00b8\u0006\u0007"
    }
    d2 = {
        "com/squareup/util/Views$onClickDebounced$1",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "view",
        "Landroid/view/View;",
        "public_release",
        "com/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$$special$$inlined$onClickDebounced$5"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $attachment$inlined:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

.field final synthetic $event$inlined:Ljava/lang/Object;

.field final synthetic $onEvent$inlined:Lkotlin/jvm/functions/Function1;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$addAttachmentRow$$inlined$apply$lambda$1;->$event$inlined:Ljava/lang/Object;

    iput-object p2, p0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$addAttachmentRow$$inlined$apply$lambda$1;->$onEvent$inlined:Lkotlin/jvm/functions/Function1;

    iput-object p3, p0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$addAttachmentRow$$inlined$apply$lambda$1;->$attachment$inlined:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    .line 1103
    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1106
    check-cast p1, Lcom/squareup/ui/account/view/SmartLineRow;

    .line 1323
    iget-object p1, p0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$addAttachmentRow$$inlined$apply$lambda$1;->$onEvent$inlined:Lkotlin/jvm/functions/Function1;

    new-instance v0, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$AttachmentClicked;

    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$addAttachmentRow$$inlined$apply$lambda$1;->$event$inlined:Ljava/lang/Object;

    iget-object v2, p0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$addAttachmentRow$$inlined$apply$lambda$1;->$attachment$inlined:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    iget-object v2, v2, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->token:Ljava/lang/String;

    const-string v3, "attachment.token"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1, v2}, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$AttachmentClicked;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
