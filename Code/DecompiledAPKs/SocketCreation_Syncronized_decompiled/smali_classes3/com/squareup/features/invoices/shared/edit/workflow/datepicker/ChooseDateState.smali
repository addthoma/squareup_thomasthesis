.class public abstract Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;
.super Ljava/lang/Object;
.source "ChooseDateState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$SelectingDate;,
        Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$SelectingCustomDate;,
        Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \u00052\u00020\u0001:\u0003\u0005\u0006\u0007B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u0082\u0001\u0002\u0008\t\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;",
        "",
        "()V",
        "takeSnapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "Companion",
        "SelectingCustomDate",
        "SelectingDate",
        "Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$SelectingDate;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$SelectingCustomDate;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;->Companion:Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;-><init>()V

    return-void
.end method


# virtual methods
.method public final takeSnapshot()Lcom/squareup/workflow/Snapshot;
    .locals 2

    .line 37
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$takeSnapshot$1;

    invoke-direct {v1, p0}, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$takeSnapshot$1;-><init>(Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    return-object v0
.end method
