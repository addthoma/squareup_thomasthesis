.class public final Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;
.super Ljava/lang/Object;
.source "EditPaymentRequestsSectionData.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Request"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0013\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B1\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0008\u0010\t\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0002\u0010\u000bJ\u000b\u0010\u0016\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0008H\u00c6\u0003J\u0010\u0010\u001a\u001a\u0004\u0018\u00010\nH\u00c6\u0003\u00a2\u0006\u0002\u0010\u0012JD\u0010\u001b\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\nH\u00c6\u0001\u00a2\u0006\u0002\u0010\u001cJ\u0013\u0010\u001d\u001a\u00020\u001e2\u0008\u0010\u001f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010 \u001a\u00020!H\u00d6\u0001J\t\u0010\"\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\rR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0015\u0010\t\u001a\u0004\u0018\u00010\n\u00a2\u0006\n\n\u0002\u0010\u0013\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;",
        "",
        "name",
        "",
        "formattedAmount",
        "",
        "formattedDueDate",
        "type",
        "Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;",
        "percent",
        "",
        "(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;Ljava/lang/Long;)V",
        "getFormattedAmount",
        "()Ljava/lang/CharSequence;",
        "getFormattedDueDate",
        "getName",
        "()Ljava/lang/String;",
        "getPercent",
        "()Ljava/lang/Long;",
        "Ljava/lang/Long;",
        "getType",
        "()Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;Ljava/lang/Long;)Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final formattedAmount:Ljava/lang/CharSequence;

.field private final formattedDueDate:Ljava/lang/CharSequence;

.field private final name:Ljava/lang/String;

.field private final percent:Ljava/lang/Long;

.field private final type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;Ljava/lang/Long;)V
    .locals 1

    const-string v0, "formattedAmount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "formattedDueDate"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "type"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->formattedAmount:Ljava/lang/CharSequence;

    iput-object p3, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->formattedDueDate:Ljava/lang/CharSequence;

    iput-object p4, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    iput-object p5, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->percent:Ljava/lang/Long;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;Ljava/lang/Long;ILjava/lang/Object;)Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->name:Ljava/lang/String;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->formattedAmount:Ljava/lang/CharSequence;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->formattedDueDate:Ljava/lang/CharSequence;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->percent:Ljava/lang/Long;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->copy(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;Ljava/lang/Long;)Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->formattedAmount:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component3()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->formattedDueDate:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component4()Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;
    .locals 1

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    return-object v0
.end method

.method public final component5()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->percent:Ljava/lang/Long;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;Ljava/lang/Long;)Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;
    .locals 7

    const-string v0, "formattedAmount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "formattedDueDate"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "type"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;Ljava/lang/Long;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->formattedAmount:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->formattedAmount:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->formattedDueDate:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->formattedDueDate:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    iget-object v1, p1, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->percent:Ljava/lang/Long;

    iget-object p1, p1, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->percent:Ljava/lang/Long;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getFormattedAmount()Ljava/lang/CharSequence;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->formattedAmount:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getFormattedDueDate()Ljava/lang/CharSequence;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->formattedDueDate:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final getPercent()Ljava/lang/Long;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->percent:Ljava/lang/Long;

    return-object v0
.end method

.method public final getType()Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->name:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->formattedAmount:Ljava/lang/CharSequence;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->formattedDueDate:Ljava/lang/CharSequence;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->percent:Ljava/lang/Long;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Request(name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", formattedAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->formattedAmount:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", formattedDueDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->formattedDueDate:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", percent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->percent:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
