.class public abstract Lcom/squareup/features/invoices/widgets/V2WidgetsModule;
.super Ljava/lang/Object;
.source "InvoiceWidgetsModule.kt"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/features/invoices/widgets/InvoiceWidgetsModule;
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/features/invoices/widgets/V2WidgetsModule$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\'\u0018\u0000 \u000c2\u00020\u0001:\u0001\u000cB\u0005\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a2\u0006\u0002\u0008\u0007J\u0015\u0010\u0008\u001a\u00020\t2\u0006\u0010\u0005\u001a\u00020\nH!\u00a2\u0006\u0002\u0008\u000b\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/V2WidgetsModule;",
        "",
        "()V",
        "editPaymentRequestRowDataFactory",
        "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;",
        "factory",
        "Lcom/squareup/features/invoices/widgets/paymentrequest/RealEditPaymentRequestRowDataFactory;",
        "editPaymentRequestRowDataFactory$impl_wiring_release",
        "editPaymentRequestsSectionDataFactory",
        "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Factory;",
        "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory;",
        "editPaymentRequestsSectionDataFactory$impl_wiring_release",
        "Companion",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/features/invoices/widgets/V2WidgetsModule$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/features/invoices/widgets/V2WidgetsModule$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/features/invoices/widgets/V2WidgetsModule$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/features/invoices/widgets/V2WidgetsModule;->Companion:Lcom/squareup/features/invoices/widgets/V2WidgetsModule$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract editPaymentRequestRowDataFactory$impl_wiring_release(Lcom/squareup/features/invoices/widgets/paymentrequest/RealEditPaymentRequestRowDataFactory;)Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract editPaymentRequestsSectionDataFactory$impl_wiring_release(Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory;)Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Factory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
