.class Lcom/squareup/eventstream/v1/Es1EventFactory;
.super Ljava/lang/Object;
.source "Es1EventFactory.java"

# interfaces
.implements Lcom/squareup/eventstream/EventFactory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/eventstream/EventFactory<",
        "Lcom/squareup/protos/eventstream/v1/Event;",
        "Lcom/squareup/eventstream/v1/EventStreamEvent;",
        "Lcom/squareup/eventstream/v1/EventStream$CurrentState;",
        ">;"
    }
.end annotation


# instance fields
.field private final application:Lcom/squareup/protos/eventstream/v1/Application;

.field private final current:Lcom/squareup/eventstream/v1/EventStream$CurrentState;

.field private final dates:Lcom/squareup/eventstream/v1/DateTimeFactory;

.field private final deviceBuilder:Lcom/squareup/protos/eventstream/v1/Device$Builder;

.field private final display:Landroid/view/Display;

.field private final displayMetrics:Landroid/util/DisplayMetrics;

.field private final jsonSerializer:Lcom/squareup/eventstream/v1/Es1JsonSerializer;

.field private final os:Lcom/squareup/protos/eventstream/v1/OperatingSystem;

.field private final session:Lcom/squareup/protos/eventstream/v1/Session;

.field private final userAgent:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/eventstream/v1/Es1JsonSerializer;Ljava/lang/String;Lcom/squareup/protos/eventstream/v1/Application;Landroid/view/Display;Lcom/squareup/protos/eventstream/v1/OperatingSystem;Lcom/squareup/protos/eventstream/v1/Session;Lcom/squareup/protos/eventstream/v1/Device$Builder;)V
    .locals 1

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Lcom/squareup/eventstream/v1/DateTimeFactory;

    invoke-direct {v0}, Lcom/squareup/eventstream/v1/DateTimeFactory;-><init>()V

    iput-object v0, p0, Lcom/squareup/eventstream/v1/Es1EventFactory;->dates:Lcom/squareup/eventstream/v1/DateTimeFactory;

    .line 37
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/squareup/eventstream/v1/Es1EventFactory;->displayMetrics:Landroid/util/DisplayMetrics;

    .line 48
    new-instance v0, Lcom/squareup/eventstream/v1/EventStream$CurrentState;

    invoke-direct {v0}, Lcom/squareup/eventstream/v1/EventStream$CurrentState;-><init>()V

    iput-object v0, p0, Lcom/squareup/eventstream/v1/Es1EventFactory;->current:Lcom/squareup/eventstream/v1/EventStream$CurrentState;

    .line 52
    iput-object p1, p0, Lcom/squareup/eventstream/v1/Es1EventFactory;->jsonSerializer:Lcom/squareup/eventstream/v1/Es1JsonSerializer;

    .line 53
    iput-object p2, p0, Lcom/squareup/eventstream/v1/Es1EventFactory;->userAgent:Ljava/lang/String;

    .line 54
    iput-object p3, p0, Lcom/squareup/eventstream/v1/Es1EventFactory;->application:Lcom/squareup/protos/eventstream/v1/Application;

    .line 55
    iput-object p4, p0, Lcom/squareup/eventstream/v1/Es1EventFactory;->display:Landroid/view/Display;

    .line 56
    iput-object p5, p0, Lcom/squareup/eventstream/v1/Es1EventFactory;->os:Lcom/squareup/protos/eventstream/v1/OperatingSystem;

    .line 57
    iput-object p6, p0, Lcom/squareup/eventstream/v1/Es1EventFactory;->session:Lcom/squareup/protos/eventstream/v1/Session;

    .line 58
    iput-object p7, p0, Lcom/squareup/eventstream/v1/Es1EventFactory;->deviceBuilder:Lcom/squareup/protos/eventstream/v1/Device$Builder;

    return-void
.end method

.method static create(Landroid/content/Context;Ljava/lang/String;Lcom/squareup/eventstream/BaseEventstream$BuildType;Lcom/squareup/eventstream/v1/Es1JsonSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/eventstream/v1/Es1EventFactory;
    .locals 10

    move-object v0, p0

    const-string/jumbo v1, "window"

    .line 188
    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 189
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    .line 191
    new-instance v1, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;-><init>()V

    move-object v2, p4

    .line 192
    invoke-virtual {v1, p4}, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;->version_name(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;

    move-result-object v1

    move-object v2, p5

    .line 193
    invoke-virtual {v1, p5}, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;->version_code(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;

    move-result-object v1

    move-object/from16 v2, p6

    .line 194
    invoke-virtual {v1, v2}, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;->build_id(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;

    move-result-object v1

    .line 195
    invoke-virtual {v1}, Lcom/squareup/protos/eventstream/v1/Application$Version$Builder;->build()Lcom/squareup/protos/eventstream/v1/Application$Version;

    move-result-object v1

    .line 196
    new-instance v2, Lcom/squareup/protos/eventstream/v1/Application$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/eventstream/v1/Application$Builder;-><init>()V

    move-object v3, p2

    iget-object v3, v3, Lcom/squareup/eventstream/BaseEventstream$BuildType;->value:Ljava/lang/String;

    .line 197
    invoke-virtual {v2, v3}, Lcom/squareup/protos/eventstream/v1/Application$Builder;->build_type(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Application$Builder;

    move-result-object v2

    .line 198
    invoke-virtual {v2, v1}, Lcom/squareup/protos/eventstream/v1/Application$Builder;->version(Lcom/squareup/protos/eventstream/v1/Application$Version;)Lcom/squareup/protos/eventstream/v1/Application$Builder;

    move-result-object v1

    move-object v2, p1

    .line 199
    invoke-virtual {v1, p1}, Lcom/squareup/protos/eventstream/v1/Application$Builder;->type(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Application$Builder;

    move-result-object v1

    .line 200
    invoke-virtual {v1}, Lcom/squareup/protos/eventstream/v1/Application$Builder;->build()Lcom/squareup/protos/eventstream/v1/Application;

    move-result-object v5

    .line 202
    new-instance v1, Lcom/squareup/protos/eventstream/v1/OperatingSystem$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/eventstream/v1/OperatingSystem$Builder;-><init>()V

    const-string v2, "ANDROID"

    .line 203
    invoke-virtual {v1, v2}, Lcom/squareup/protos/eventstream/v1/OperatingSystem$Builder;->type(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/OperatingSystem$Builder;

    move-result-object v1

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    .line 204
    invoke-virtual {v1, v2}, Lcom/squareup/protos/eventstream/v1/OperatingSystem$Builder;->version(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/OperatingSystem$Builder;

    move-result-object v1

    .line 205
    invoke-virtual {v1}, Lcom/squareup/protos/eventstream/v1/OperatingSystem$Builder;->build()Lcom/squareup/protos/eventstream/v1/OperatingSystem;

    move-result-object v7

    .line 208
    new-instance v1, Lcom/squareup/protos/eventstream/v1/Session$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/eventstream/v1/Session$Builder;-><init>()V

    move-object/from16 v2, p9

    .line 209
    invoke-virtual {v1, v2}, Lcom/squareup/protos/eventstream/v1/Session$Builder;->token(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Session$Builder;

    move-result-object v1

    .line 210
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/eventstream/v1/Session$Builder;->start_time_millis(Ljava/lang/Long;)Lcom/squareup/protos/eventstream/v1/Session$Builder;

    move-result-object v1

    .line 211
    invoke-virtual {v1}, Lcom/squareup/protos/eventstream/v1/Session$Builder;->build()Lcom/squareup/protos/eventstream/v1/Session;

    move-result-object v8

    const-string v1, "phone"

    .line 214
    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 215
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "android_id"

    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 216
    new-instance v3, Lcom/squareup/protos/eventstream/v1/Device$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/eventstream/v1/Device$Builder;-><init>()V

    .line 217
    invoke-virtual {v3, v2}, Lcom/squareup/protos/eventstream/v1/Device$Builder;->android_id(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Device$Builder;

    move-result-object v2

    sget-object v3, Landroid/os/Build;->BRAND:Ljava/lang/String;

    .line 218
    invoke-virtual {v2, v3}, Lcom/squareup/protos/eventstream/v1/Device$Builder;->brand(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Device$Builder;

    move-result-object v2

    move-object/from16 v3, p7

    .line 219
    invoke-virtual {v2, v3}, Lcom/squareup/protos/eventstream/v1/Device$Builder;->installation_id(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Device$Builder;

    move-result-object v2

    sget-object v3, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    .line 220
    invoke-virtual {v2, v3}, Lcom/squareup/protos/eventstream/v1/Device$Builder;->manufacturer(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Device$Builder;

    move-result-object v2

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 221
    invoke-virtual {v2, v3}, Lcom/squareup/protos/eventstream/v1/Device$Builder;->model(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Device$Builder;

    move-result-object v2

    .line 222
    invoke-static {v1}, Lcom/squareup/eventstream/v1/Es1EventFactory;->networkOperator(Landroid/telephony/TelephonyManager;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/protos/eventstream/v1/Device$Builder;->network_carrier(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Device$Builder;

    move-result-object v2

    .line 223
    invoke-static {p0, v1}, Lcom/squareup/eventstream/v1/Es1EventFactory;->createSim(Landroid/content/Context;Landroid/telephony/TelephonyManager;)Lcom/squareup/protos/eventstream/v1/Sim;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/squareup/protos/eventstream/v1/Device$Builder;->sim(Lcom/squareup/protos/eventstream/v1/Sim;)Lcom/squareup/protos/eventstream/v1/Device$Builder;

    move-result-object v9

    .line 225
    new-instance v0, Lcom/squareup/eventstream/v1/Es1EventFactory;

    move-object v2, v0

    move-object v3, p3

    move-object/from16 v4, p8

    invoke-direct/range {v2 .. v9}, Lcom/squareup/eventstream/v1/Es1EventFactory;-><init>(Lcom/squareup/eventstream/v1/Es1JsonSerializer;Ljava/lang/String;Lcom/squareup/protos/eventstream/v1/Application;Landroid/view/Display;Lcom/squareup/protos/eventstream/v1/OperatingSystem;Lcom/squareup/protos/eventstream/v1/Session;Lcom/squareup/protos/eventstream/v1/Device$Builder;)V

    return-object v0
.end method

.method private create(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/eventstream/v1/Event$Builder;
    .locals 1

    .line 100
    new-instance v0, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;-><init>()V

    .line 101
    invoke-virtual {v0, p3}, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->recorded_timestamp(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;

    move-result-object p3

    .line 102
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->uuid(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;

    move-result-object p3

    const-string v0, "android/eventstream"

    .line 103
    invoke-virtual {p3, v0}, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->library_name(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;

    move-result-object p3

    const-string/jumbo v0, "unspecified"

    .line 104
    invoke-virtual {p3, v0}, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->library_version(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;

    move-result-object p3

    .line 105
    invoke-virtual {p3}, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->build()Lcom/squareup/protos/eventstream/v1/EventMetadata;

    move-result-object p3

    .line 107
    new-instance v0, Lcom/squareup/protos/eventstream/v1/Event$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/eventstream/v1/Event$Builder;-><init>()V

    invoke-virtual {v0, p1}, Lcom/squareup/protos/eventstream/v1/Event$Builder;->event_name(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Event$Builder;

    move-result-object p1

    .line 108
    invoke-virtual {p1, p2}, Lcom/squareup/protos/eventstream/v1/Event$Builder;->event_value(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Event$Builder;

    move-result-object p1

    .line 109
    invoke-virtual {p1, p3}, Lcom/squareup/protos/eventstream/v1/Event$Builder;->event_metadata(Lcom/squareup/protos/eventstream/v1/EventMetadata;)Lcom/squareup/protos/eventstream/v1/Event$Builder;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/eventstream/v1/Es1EventFactory;->session:Lcom/squareup/protos/eventstream/v1/Session;

    .line 110
    invoke-virtual {p1, p2}, Lcom/squareup/protos/eventstream/v1/Event$Builder;->session(Lcom/squareup/protos/eventstream/v1/Session;)Lcom/squareup/protos/eventstream/v1/Event$Builder;

    move-result-object p1

    return-object p1
.end method

.method private static createCoordinates(Landroid/location/Location;)Lcom/squareup/protos/common/location/Coordinates;
    .locals 3

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 151
    :cond_0
    new-instance v0, Lcom/squareup/protos/common/location/Coordinates$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/location/Coordinates$Builder;-><init>()V

    .line 152
    invoke-virtual {p0}, Landroid/location/Location;->getAltitude()D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/location/Coordinates$Builder;->altitude(Ljava/lang/Double;)Lcom/squareup/protos/common/location/Coordinates$Builder;

    move-result-object v0

    .line 153
    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/location/Coordinates$Builder;->geographic_accuracy(Ljava/lang/Double;)Lcom/squareup/protos/common/location/Coordinates$Builder;

    move-result-object v0

    .line 154
    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/location/Coordinates$Builder;->latitude(Ljava/lang/Double;)Lcom/squareup/protos/common/location/Coordinates$Builder;

    move-result-object v0

    .line 155
    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/location/Coordinates$Builder;->longitude(Ljava/lang/Double;)Lcom/squareup/protos/common/location/Coordinates$Builder;

    move-result-object v0

    .line 156
    invoke-virtual {p0}, Landroid/location/Location;->getBearing()F

    move-result v1

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/location/Coordinates$Builder;->heading(Ljava/lang/Double;)Lcom/squareup/protos/common/location/Coordinates$Builder;

    move-result-object v0

    .line 157
    invoke-virtual {p0}, Landroid/location/Location;->getSpeed()F

    move-result p0

    float-to-double v1, p0

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/common/location/Coordinates$Builder;->speed(Ljava/lang/Double;)Lcom/squareup/protos/common/location/Coordinates$Builder;

    move-result-object p0

    .line 158
    invoke-virtual {p0}, Lcom/squareup/protos/common/location/Coordinates$Builder;->build()Lcom/squareup/protos/common/location/Coordinates;

    move-result-object p0

    return-object p0
.end method

.method static createSim(Landroid/content/Context;Landroid/telephony/TelephonyManager;)Lcom/squareup/protos/eventstream/v1/Sim;
    .locals 1

    const-string v0, "android.permission.READ_PHONE_STATE"

    .line 230
    invoke-static {p0, v0}, Landroidx/core/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    .line 231
    :goto_0
    invoke-static {p1, p0}, Lcom/squareup/eventstream/v1/Es1EventFactory;->createSim(Landroid/telephony/TelephonyManager;Z)Lcom/squareup/protos/eventstream/v1/Sim;

    move-result-object p0

    return-object p0
.end method

.method static createSim(Landroid/telephony/TelephonyManager;Z)Lcom/squareup/protos/eventstream/v1/Sim;
    .locals 5

    .line 236
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 237
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    :cond_0
    const/4 v2, 0x0

    const/4 v3, 0x3

    .line 239
    :try_start_0
    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 240
    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz p1, :cond_1

    .line 246
    :try_start_1
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getSimSerialNumber()Ljava/lang/String;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 250
    :try_start_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "error getting sim serial: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    move-object p1, v1

    .line 254
    :goto_0
    new-instance v3, Lcom/squareup/protos/eventstream/v1/Sim$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/eventstream/v1/Sim$Builder;-><init>()V

    .line 255
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/protos/eventstream/v1/Sim$Builder;->country_iso(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Sim$Builder;

    move-result-object v3

    .line 256
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/squareup/protos/eventstream/v1/Sim$Builder;->mcc(Ljava/lang/Integer;)Lcom/squareup/protos/eventstream/v1/Sim$Builder;

    move-result-object v2

    .line 257
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/squareup/protos/eventstream/v1/Sim$Builder;->mnc(Ljava/lang/Integer;)Lcom/squareup/protos/eventstream/v1/Sim$Builder;

    move-result-object v0

    .line 258
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getSimOperatorName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/eventstream/v1/Sim$Builder;->operator_name(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Sim$Builder;

    move-result-object p0

    .line 259
    invoke-virtual {p0, p1}, Lcom/squareup/protos/eventstream/v1/Sim$Builder;->serial_number(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Sim$Builder;

    move-result-object p0

    .line 260
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Sim$Builder;->build()Lcom/squareup/protos/eventstream/v1/Sim;

    move-result-object p0
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1

    return-object p0

    :catch_1
    :cond_2
    :goto_1
    return-object v1
.end method

.method private declared-synchronized createSource(Landroid/location/Location;Ljava/util/Locale;Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Source;
    .locals 3

    monitor-enter p0

    .line 115
    :try_start_0
    iget-object v0, p0, Lcom/squareup/eventstream/v1/Es1EventFactory;->display:Landroid/view/Display;

    iget-object v1, p0, Lcom/squareup/eventstream/v1/Es1EventFactory;->displayMetrics:Landroid/util/DisplayMetrics;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    goto/16 :goto_1

    :catch_0
    nop

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_1
    const-string v1, "int[] cannot be cast to android.app.ActivityThread$ActivityClientRecord"

    .line 120
    invoke-virtual {v0}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 119
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_0
    if-eqz p2, :cond_0

    .line 129
    iget-object v0, p0, Lcom/squareup/eventstream/v1/Es1EventFactory;->deviceBuilder:Lcom/squareup/protos/eventstream/v1/Device$Builder;

    invoke-virtual {p2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/eventstream/v1/Device$Builder;->language(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Device$Builder;

    move-result-object v0

    .line 130
    invoke-virtual {p2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/protos/eventstream/v1/Device$Builder;->locale_country_code(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Device$Builder;

    .line 133
    :cond_0
    iget-object p2, p0, Lcom/squareup/eventstream/v1/Es1EventFactory;->deviceBuilder:Lcom/squareup/protos/eventstream/v1/Device$Builder;

    iget-object v0, p0, Lcom/squareup/eventstream/v1/Es1EventFactory;->displayMetrics:Landroid/util/DisplayMetrics;

    invoke-static {v0}, Lcom/squareup/eventstream/v1/Es1EventFactory;->orientation(Landroid/util/DisplayMetrics;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/protos/eventstream/v1/Device$Builder;->orientation(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Device$Builder;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/eventstream/v1/Es1EventFactory;->displayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 134
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/protos/eventstream/v1/Device$Builder;->screen_width(Ljava/lang/Integer;)Lcom/squareup/protos/eventstream/v1/Device$Builder;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/eventstream/v1/Es1EventFactory;->displayMetrics:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 135
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/protos/eventstream/v1/Device$Builder;->screen_height(Ljava/lang/Integer;)Lcom/squareup/protos/eventstream/v1/Device$Builder;

    move-result-object p2

    .line 136
    invoke-virtual {p2, p3}, Lcom/squareup/protos/eventstream/v1/Device$Builder;->advertising_id(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Device$Builder;

    .line 138
    new-instance p2, Lcom/squareup/protos/eventstream/v1/Source$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/eventstream/v1/Source$Builder;-><init>()V

    iget-object p3, p0, Lcom/squareup/eventstream/v1/Es1EventFactory;->application:Lcom/squareup/protos/eventstream/v1/Application;

    .line 139
    invoke-virtual {p2, p3}, Lcom/squareup/protos/eventstream/v1/Source$Builder;->application(Lcom/squareup/protos/eventstream/v1/Application;)Lcom/squareup/protos/eventstream/v1/Source$Builder;

    move-result-object p2

    .line 140
    invoke-static {p1}, Lcom/squareup/eventstream/v1/Es1EventFactory;->createCoordinates(Landroid/location/Location;)Lcom/squareup/protos/common/location/Coordinates;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/protos/eventstream/v1/Source$Builder;->coordinates(Lcom/squareup/protos/common/location/Coordinates;)Lcom/squareup/protos/eventstream/v1/Source$Builder;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/eventstream/v1/Es1EventFactory;->deviceBuilder:Lcom/squareup/protos/eventstream/v1/Device$Builder;

    .line 141
    invoke-virtual {p2}, Lcom/squareup/protos/eventstream/v1/Device$Builder;->build()Lcom/squareup/protos/eventstream/v1/Device;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/eventstream/v1/Source$Builder;->device(Lcom/squareup/protos/eventstream/v1/Device;)Lcom/squareup/protos/eventstream/v1/Source$Builder;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/eventstream/v1/Es1EventFactory;->os:Lcom/squareup/protos/eventstream/v1/OperatingSystem;

    .line 142
    invoke-virtual {p1, p2}, Lcom/squareup/protos/eventstream/v1/Source$Builder;->os(Lcom/squareup/protos/eventstream/v1/OperatingSystem;)Lcom/squareup/protos/eventstream/v1/Source$Builder;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/eventstream/v1/Es1EventFactory;->userAgent:Ljava/lang/String;

    .line 143
    invoke-virtual {p1, p2}, Lcom/squareup/protos/eventstream/v1/Source$Builder;->user_agent(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Source$Builder;

    move-result-object p1

    .line 144
    invoke-virtual {p1}, Lcom/squareup/protos/eventstream/v1/Source$Builder;->build()Lcom/squareup/protos/eventstream/v1/Source;

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 138
    monitor-exit p0

    return-object p1

    .line 121
    :cond_1
    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    monitor-exit p0

    throw p1
.end method

.method private hasRawData(Lcom/squareup/eventstream/v1/EventStreamEvent;)Z
    .locals 0

    .line 92
    instance-of p1, p1, Lcom/squareup/eventstream/v1/EventStream$NoRawData;

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method static networkOperator(Landroid/telephony/TelephonyManager;)Ljava/lang/String;
    .locals 2

    .line 267
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 268
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 269
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method private static orientation(Landroid/util/DisplayMetrics;)Ljava/lang/String;
    .locals 2

    .line 162
    iget v0, p0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v1, p0, Landroid/util/DisplayMetrics;->widthPixels:I

    if-le v0, v1, :cond_0

    const-string p0, "portrait"

    return-object p0

    .line 164
    :cond_0
    iget v0, p0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget p0, p0, Landroid/util/DisplayMetrics;->widthPixels:I

    if-ge v0, p0, :cond_1

    const-string p0, "landscape"

    return-object p0

    :cond_1
    const-string p0, "square"

    return-object p0
.end method

.method private serializeRawData(Lcom/squareup/eventstream/v1/EventStreamEvent;)Ljava/lang/String;
    .locals 3

    .line 86
    iget-object v0, p0, Lcom/squareup/eventstream/v1/Es1EventFactory;->jsonSerializer:Lcom/squareup/eventstream/v1/Es1JsonSerializer;

    iget-object v1, p0, Lcom/squareup/eventstream/v1/Es1EventFactory;->current:Lcom/squareup/eventstream/v1/EventStream$CurrentState;

    invoke-virtual {v1}, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->getCommonProperties()Ljava/util/Map;

    move-result-object v1

    .line 87
    invoke-direct {p0, p1}, Lcom/squareup/eventstream/v1/Es1EventFactory;->hasRawData(Lcom/squareup/eventstream/v1/EventStreamEvent;)Z

    move-result v2

    .line 86
    invoke-interface {v0, p1, v1, v2}, Lcom/squareup/eventstream/v1/Es1JsonSerializer;->serializeRawData(Lcom/squareup/eventstream/v1/EventStreamEvent;Ljava/util/Map;Z)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public create(Lcom/squareup/eventstream/v1/EventStreamEvent;J)Lcom/squareup/protos/eventstream/v1/Event;
    .locals 2

    .line 63
    iget-object v0, p1, Lcom/squareup/eventstream/v1/EventStreamEvent;->overrideTimestamp:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v0, :cond_0

    iget-object p2, p1, Lcom/squareup/eventstream/v1/EventStreamEvent;->overrideTimestamp:Lcom/squareup/protos/common/time/DateTime;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/eventstream/v1/Es1EventFactory;->dates:Lcom/squareup/eventstream/v1/DateTimeFactory;

    .line 65
    invoke-virtual {v0, p2, p3}, Lcom/squareup/eventstream/v1/DateTimeFactory;->forMillis(J)Lcom/squareup/protos/common/time/DateTime;

    move-result-object p2

    .line 66
    :goto_0
    iget-object p3, p1, Lcom/squareup/eventstream/v1/EventStreamEvent;->name:Lcom/squareup/eventstream/v1/EventStream$Name;

    iget-object p3, p3, Lcom/squareup/eventstream/v1/EventStream$Name;->loggingName:Ljava/lang/String;

    iget-object v0, p1, Lcom/squareup/eventstream/v1/EventStreamEvent;->value:Ljava/lang/String;

    .line 67
    invoke-direct {p0, p3, v0, p2}, Lcom/squareup/eventstream/v1/Es1EventFactory;->create(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/eventstream/v1/Event$Builder;

    move-result-object p2

    .line 69
    iget-object p3, p1, Lcom/squareup/eventstream/v1/EventStreamEvent;->overrideSubject:Lcom/squareup/protos/eventstream/v1/Subject;

    if-eqz p3, :cond_1

    iget-object p3, p1, Lcom/squareup/eventstream/v1/EventStreamEvent;->overrideSubject:Lcom/squareup/protos/eventstream/v1/Subject;

    goto :goto_1

    :cond_1
    iget-object p3, p0, Lcom/squareup/eventstream/v1/Es1EventFactory;->current:Lcom/squareup/eventstream/v1/EventStream$CurrentState;

    iget-object p3, p3, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->subject:Lcom/squareup/protos/eventstream/v1/Subject;

    .line 71
    :goto_1
    invoke-virtual {p2, p3}, Lcom/squareup/protos/eventstream/v1/Event$Builder;->subject(Lcom/squareup/protos/eventstream/v1/Subject;)Lcom/squareup/protos/eventstream/v1/Event$Builder;

    .line 72
    iget-object p3, p0, Lcom/squareup/eventstream/v1/Es1EventFactory;->current:Lcom/squareup/eventstream/v1/EventStream$CurrentState;

    iget-object p3, p3, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->location:Landroid/location/Location;

    iget-object v0, p0, Lcom/squareup/eventstream/v1/Es1EventFactory;->current:Lcom/squareup/eventstream/v1/EventStream$CurrentState;

    iget-object v0, v0, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->locale:Ljava/util/Locale;

    iget-object v1, p0, Lcom/squareup/eventstream/v1/Es1EventFactory;->current:Lcom/squareup/eventstream/v1/EventStream$CurrentState;

    iget-object v1, v1, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->advertisingId:Ljava/lang/String;

    invoke-direct {p0, p3, v0, v1}, Lcom/squareup/eventstream/v1/Es1EventFactory;->createSource(Landroid/location/Location;Ljava/util/Locale;Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Source;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/protos/eventstream/v1/Event$Builder;->source(Lcom/squareup/protos/eventstream/v1/Source;)Lcom/squareup/protos/eventstream/v1/Event$Builder;

    .line 73
    new-instance p3, Lcom/squareup/protos/eventstream/v1/Data$Builder;

    invoke-direct {p3}, Lcom/squareup/protos/eventstream/v1/Data$Builder;-><init>()V

    .line 74
    invoke-direct {p0, p1}, Lcom/squareup/eventstream/v1/Es1EventFactory;->serializeRawData(Lcom/squareup/eventstream/v1/EventStreamEvent;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/squareup/protos/eventstream/v1/Data$Builder;->raw_data(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Data$Builder;

    move-result-object p3

    iget-object p1, p1, Lcom/squareup/eventstream/v1/EventStreamEvent;->rawBytes:Lokio/ByteString;

    .line 75
    invoke-virtual {p3, p1}, Lcom/squareup/protos/eventstream/v1/Data$Builder;->raw_bytes(Lokio/ByteString;)Lcom/squareup/protos/eventstream/v1/Data$Builder;

    move-result-object p1

    .line 76
    invoke-virtual {p1}, Lcom/squareup/protos/eventstream/v1/Data$Builder;->build()Lcom/squareup/protos/eventstream/v1/Data;

    move-result-object p1

    .line 73
    invoke-virtual {p2, p1}, Lcom/squareup/protos/eventstream/v1/Event$Builder;->data(Lcom/squareup/protos/eventstream/v1/Data;)Lcom/squareup/protos/eventstream/v1/Event$Builder;

    .line 78
    invoke-virtual {p2}, Lcom/squareup/protos/eventstream/v1/Event$Builder;->build()Lcom/squareup/protos/eventstream/v1/Event;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic create(Ljava/lang/Object;J)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/eventstream/v1/Es1EventFactory;->create(Lcom/squareup/eventstream/v1/EventStreamEvent;J)Lcom/squareup/protos/eventstream/v1/Event;

    move-result-object p1

    return-object p1
.end method

.method public state()Lcom/squareup/eventstream/v1/EventStream$CurrentState;
    .locals 1

    .line 172
    iget-object v0, p0, Lcom/squareup/eventstream/v1/Es1EventFactory;->current:Lcom/squareup/eventstream/v1/EventStream$CurrentState;

    return-object v0
.end method

.method public bridge synthetic state()Ljava/lang/Object;
    .locals 1

    .line 35
    invoke-virtual {p0}, Lcom/squareup/eventstream/v1/Es1EventFactory;->state()Lcom/squareup/eventstream/v1/EventStream$CurrentState;

    move-result-object v0

    return-object v0
.end method
