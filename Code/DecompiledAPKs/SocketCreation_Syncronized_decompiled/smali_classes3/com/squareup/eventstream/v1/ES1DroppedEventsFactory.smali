.class Lcom/squareup/eventstream/v1/ES1DroppedEventsFactory;
.super Ljava/lang/Object;
.source "ES1DroppedEventsFactory.java"

# interfaces
.implements Lcom/squareup/eventstream/DroppedEventsFactory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/eventstream/DroppedEventsFactory<",
        "Lcom/squareup/protos/eventstream/v1/Event;",
        ">;"
    }
.end annotation


# instance fields
.field private final eventFactory:Lcom/squareup/eventstream/EventFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/eventstream/EventFactory<",
            "Lcom/squareup/protos/eventstream/v1/Event;",
            "Lcom/squareup/eventstream/v1/EventStreamEvent;",
            "Lcom/squareup/eventstream/v1/EventStream$CurrentState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/eventstream/EventFactory;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/eventstream/EventFactory<",
            "Lcom/squareup/protos/eventstream/v1/Event;",
            "Lcom/squareup/eventstream/v1/EventStreamEvent;",
            "Lcom/squareup/eventstream/v1/EventStream$CurrentState;",
            ">;)V"
        }
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/eventstream/v1/ES1DroppedEventsFactory;->eventFactory:Lcom/squareup/eventstream/EventFactory;

    return-void
.end method


# virtual methods
.method public createDroppedEventsEvent(Ljava/util/Map;I)Lcom/squareup/protos/eventstream/v1/Event;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/squareup/eventstream/DroppedEventCounter$DropType;",
            "Ljava/lang/Integer;",
            ">;I)",
            "Lcom/squareup/protos/eventstream/v1/Event;"
        }
    .end annotation

    .line 20
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 21
    iget-object v2, p0, Lcom/squareup/eventstream/v1/ES1DroppedEventsFactory;->eventFactory:Lcom/squareup/eventstream/EventFactory;

    new-instance v3, Lcom/squareup/eventstream/v1/DroppedEventsV1Event;

    invoke-direct {v3, p1, p2}, Lcom/squareup/eventstream/v1/DroppedEventsV1Event;-><init>(Ljava/util/Map;I)V

    invoke-interface {v2, v3, v0, v1}, Lcom/squareup/eventstream/EventFactory;->create(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/eventstream/v1/Event;

    return-object p1
.end method

.method public bridge synthetic createDroppedEventsEvent(Ljava/util/Map;I)Ljava/lang/Object;
    .locals 0

    .line 9
    invoke-virtual {p0, p1, p2}, Lcom/squareup/eventstream/v1/ES1DroppedEventsFactory;->createDroppedEventsEvent(Ljava/util/Map;I)Lcom/squareup/protos/eventstream/v1/Event;

    move-result-object p1

    return-object p1
.end method
