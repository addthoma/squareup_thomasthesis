.class public interface abstract Lcom/squareup/eventstream/v2/Es2JsonSerializer;
.super Ljava/lang/Object;
.source "Es2JsonSerializer.java"


# virtual methods
.method public abstract serializeJsonData(Lcom/squareup/eventstream/v2/AppEvent;Ljava/util/Map;Lcom/squareup/eventstream/v2/AndroidEvent;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/eventstream/v2/AppEvent;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/eventstream/v2/AndroidEvent;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation
.end method
