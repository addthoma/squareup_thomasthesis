.class final Lcom/squareup/eventstream/EventQueue$peekBlocking$1;
.super Ljava/lang/Object;
.source "EventQueue.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/eventstream/EventQueue;->peekBlocking(I)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Ljava/util/List<",
        "+TT;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0003\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u0001H\u0002H\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u0001H\u0002H\u0002\u0018\u00010\u00010\u0001\"\u0004\u0008\u0000\u0010\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "T",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $max:I

.field final synthetic this$0:Lcom/squareup/eventstream/EventQueue;


# direct methods
.method constructor <init>(Lcom/squareup/eventstream/EventQueue;I)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/eventstream/EventQueue$peekBlocking$1;->this$0:Lcom/squareup/eventstream/EventQueue;

    iput p2, p0, Lcom/squareup/eventstream/EventQueue$peekBlocking$1;->$max:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/eventstream/EventQueue$peekBlocking$1;->call()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final call()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 91
    :try_start_0
    iget-object v0, p0, Lcom/squareup/eventstream/EventQueue$peekBlocking$1;->this$0:Lcom/squareup/eventstream/EventQueue;

    invoke-static {v0}, Lcom/squareup/eventstream/EventQueue;->access$getQueue$p(Lcom/squareup/eventstream/EventQueue;)Lcom/squareup/tape/FileObjectQueue;

    move-result-object v0

    iget v1, p0, Lcom/squareup/eventstream/EventQueue$peekBlocking$1;->$max:I

    invoke-virtual {v0, v1}, Lcom/squareup/tape/FileObjectQueue;->peek(I)Ljava/util/List;

    move-result-object v0
    :try_end_0
    .catch Lcom/squareup/tape/FileException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 93
    iget-object v1, p0, Lcom/squareup/eventstream/EventQueue$peekBlocking$1;->this$0:Lcom/squareup/eventstream/EventQueue;

    invoke-static {v1, v0}, Lcom/squareup/eventstream/EventQueue;->access$handleQueueFailure(Lcom/squareup/eventstream/EventQueue;Lcom/squareup/tape/FileException;)V

    .line 94
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
