.class public final Lcom/squareup/eventstream/BaseEventstream$LoggingProcessor;
.super Ljava/lang/Object;
.source "BaseEventstream.kt"

# interfaces
.implements Lcom/squareup/eventstream/EventBatchUploader;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/eventstream/BaseEventstream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1c
    name = "LoggingProcessor"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/eventstream/EventBatchUploader<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\u0008\u0004\u0018\u0000*\u0004\u0008\u0003\u0010\u00012\u0008\u0012\u0004\u0012\u0002H\u00010\u0002B#\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00028\u00030\u0002\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0016\u0010\t\u001a\u00020\n2\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00028\u00030\u000cH\u0016R\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00028\u00030\u0002X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/eventstream/BaseEventstream$LoggingProcessor;",
        "T",
        "Lcom/squareup/eventstream/EventBatchUploader;",
        "batchUploader",
        "log",
        "Lcom/squareup/eventstream/EventStreamLog;",
        "loggingName",
        "",
        "(Lcom/squareup/eventstream/EventBatchUploader;Lcom/squareup/eventstream/EventStreamLog;Ljava/lang/String;)V",
        "upload",
        "Lcom/squareup/eventstream/EventBatchUploader$Result;",
        "events",
        "",
        "eventstream-common_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final batchUploader:Lcom/squareup/eventstream/EventBatchUploader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/eventstream/EventBatchUploader<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final log:Lcom/squareup/eventstream/EventStreamLog;

.field private final loggingName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/eventstream/EventBatchUploader;Lcom/squareup/eventstream/EventStreamLog;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/eventstream/EventBatchUploader<",
            "TT;>;",
            "Lcom/squareup/eventstream/EventStreamLog;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string v0, "batchUploader"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "log"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loggingName"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/eventstream/BaseEventstream$LoggingProcessor;->batchUploader:Lcom/squareup/eventstream/EventBatchUploader;

    iput-object p2, p0, Lcom/squareup/eventstream/BaseEventstream$LoggingProcessor;->log:Lcom/squareup/eventstream/EventStreamLog;

    iput-object p3, p0, Lcom/squareup/eventstream/BaseEventstream$LoggingProcessor;->loggingName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public upload(Ljava/util/List;)Lcom/squareup/eventstream/EventBatchUploader$Result;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+TT;>;)",
            "Lcom/squareup/eventstream/EventBatchUploader$Result;"
        }
    .end annotation

    const-string v0, "events"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 84
    iget-object v1, p0, Lcom/squareup/eventstream/BaseEventstream$LoggingProcessor;->log:Lcom/squareup/eventstream/EventStreamLog;

    const/4 v2, 0x2

    new-array v3, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/squareup/eventstream/BaseEventstream$LoggingProcessor;->loggingName:Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v6, 0x1

    aput-object v4, v3, v6

    const-string v4, "ES: %s processing %s items"

    invoke-interface {v1, v4, v3}, Lcom/squareup/eventstream/EventStreamLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 86
    iget-object v1, p0, Lcom/squareup/eventstream/BaseEventstream$LoggingProcessor;->batchUploader:Lcom/squareup/eventstream/EventBatchUploader;

    invoke-interface {v1, p1}, Lcom/squareup/eventstream/EventBatchUploader;->upload(Ljava/util/List;)Lcom/squareup/eventstream/EventBatchUploader$Result;

    move-result-object p1

    .line 88
    sget-object v1, Lcom/squareup/eventstream/BaseEventstream$LoggingProcessor$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/eventstream/EventBatchUploader$Result;->ordinal()I

    move-result v3

    aget v1, v1, v3

    if-eq v1, v6, :cond_1

    if-eq v1, v2, :cond_0

    goto :goto_0

    .line 90
    :cond_0
    iget-object v1, p0, Lcom/squareup/eventstream/BaseEventstream$LoggingProcessor;->log:Lcom/squareup/eventstream/EventStreamLog;

    new-array v2, v2, [Ljava/lang/Object;

    .line 91
    iget-object v3, p0, Lcom/squareup/eventstream/BaseEventstream$LoggingProcessor;->loggingName:Ljava/lang/String;

    aput-object v3, v2, v5

    .line 92
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v6

    const-string v0, "ES: %s cleaning up after processing failure; removing %s items"

    .line 90
    invoke-interface {v1, v0, v2}, Lcom/squareup/eventstream/EventStreamLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 89
    :cond_1
    iget-object v0, p0, Lcom/squareup/eventstream/BaseEventstream$LoggingProcessor;->log:Lcom/squareup/eventstream/EventStreamLog;

    new-array v1, v6, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/squareup/eventstream/BaseEventstream$LoggingProcessor;->loggingName:Ljava/lang/String;

    aput-object v2, v1, v5

    const-string v2, "ES: %s queue processing encountered an error."

    invoke-interface {v0, v2, v1}, Lcom/squareup/eventstream/EventStreamLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-object p1
.end method
