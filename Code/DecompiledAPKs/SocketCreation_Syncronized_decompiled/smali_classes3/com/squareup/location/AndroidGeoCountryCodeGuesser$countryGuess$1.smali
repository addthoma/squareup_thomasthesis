.class final Lcom/squareup/location/AndroidGeoCountryCodeGuesser$countryGuess$1;
.super Ljava/lang/Object;
.source "AndroidGeoCountryCodeGuesser.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/location/AndroidGeoCountryCodeGuesser;->getCountryGuess()Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0014\u0010\u0002\u001a\u0010\u0012\u0004\u0012\u00020\u0001\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/location/CountryGuesser$Result;",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/CountryCode;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/location/AndroidGeoCountryCodeGuesser$countryGuess$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/location/AndroidGeoCountryCodeGuesser$countryGuess$1;

    invoke-direct {v0}, Lcom/squareup/location/AndroidGeoCountryCodeGuesser$countryGuess$1;-><init>()V

    sput-object v0, Lcom/squareup/location/AndroidGeoCountryCodeGuesser$countryGuess$1;->INSTANCE:Lcom/squareup/location/AndroidGeoCountryCodeGuesser$countryGuess$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lkotlin/Pair;)Lcom/squareup/location/CountryGuesser$Result;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "+",
            "Lcom/squareup/location/CountryGuesser$Result;",
            "+",
            "Lcom/squareup/CountryCode;",
            ">;)",
            "Lcom/squareup/location/CountryGuesser$Result;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/location/CountryGuesser$Result;

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 25
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/location/AndroidGeoCountryCodeGuesser$countryGuess$1;->apply(Lkotlin/Pair;)Lcom/squareup/location/CountryGuesser$Result;

    move-result-object p1

    return-object p1
.end method
