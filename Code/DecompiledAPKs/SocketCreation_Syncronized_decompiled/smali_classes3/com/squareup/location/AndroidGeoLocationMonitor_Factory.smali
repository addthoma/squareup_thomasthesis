.class public final Lcom/squareup/location/AndroidGeoLocationMonitor_Factory;
.super Ljava/lang/Object;
.source "AndroidGeoLocationMonitor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/location/AndroidGeoLocationMonitor;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/location/LocationManager;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/core/location/comparer/LocationComparer;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/location/LocationManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/core/location/comparer/LocationComparer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/location/AndroidGeoLocationMonitor_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/location/AndroidGeoLocationMonitor_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/location/AndroidGeoLocationMonitor_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/location/AndroidGeoLocationMonitor_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/location/AndroidGeoLocationMonitor_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 42
    iput-object p6, p0, Lcom/squareup/location/AndroidGeoLocationMonitor_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 43
    iput-object p7, p0, Lcom/squareup/location/AndroidGeoLocationMonitor_Factory;->arg6Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/location/AndroidGeoLocationMonitor_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/location/LocationManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/core/location/comparer/LocationComparer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;)",
            "Lcom/squareup/location/AndroidGeoLocationMonitor_Factory;"
        }
    .end annotation

    .line 55
    new-instance v8, Lcom/squareup/location/AndroidGeoLocationMonitor_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/location/AndroidGeoLocationMonitor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Landroid/app/Application;Landroid/location/LocationManager;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/core/location/comparer/LocationComparer;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Clock;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lcom/squareup/location/AndroidGeoLocationMonitor;
    .locals 9

    .line 60
    new-instance v8, Lcom/squareup/location/AndroidGeoLocationMonitor;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/location/AndroidGeoLocationMonitor;-><init>(Landroid/app/Application;Landroid/location/LocationManager;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/core/location/comparer/LocationComparer;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Clock;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/location/AndroidGeoLocationMonitor;
    .locals 8

    .line 48
    iget-object v0, p0, Lcom/squareup/location/AndroidGeoLocationMonitor_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/app/Application;

    iget-object v0, p0, Lcom/squareup/location/AndroidGeoLocationMonitor_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/location/LocationManager;

    iget-object v0, p0, Lcom/squareup/location/AndroidGeoLocationMonitor_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/location/AndroidGeoLocationMonitor_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/core/location/comparer/LocationComparer;

    iget-object v0, p0, Lcom/squareup/location/AndroidGeoLocationMonitor_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/location/AndroidGeoLocationMonitor_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/util/Clock;

    iget-object v0, p0, Lcom/squareup/location/AndroidGeoLocationMonitor_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-static/range {v1 .. v7}, Lcom/squareup/location/AndroidGeoLocationMonitor_Factory;->newInstance(Landroid/app/Application;Landroid/location/LocationManager;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/core/location/comparer/LocationComparer;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Clock;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lcom/squareup/location/AndroidGeoLocationMonitor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/location/AndroidGeoLocationMonitor_Factory;->get()Lcom/squareup/location/AndroidGeoLocationMonitor;

    move-result-object v0

    return-object v0
.end method
