.class public final Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;
.super Ljava/lang/Object;
.source "LibraryListEntryHandler.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/librarylist/LibraryListEntryHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CogsSelectedEntryData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000h\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u001d\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0095\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0010\u0008\u001a\u0004\u0018\u00010\t\u0012\u0012\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00020\r0\u000b\u0012\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f\u0012\u000c\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u000f\u0012\u0018\u0010\u0013\u001a\u0014\u0012\u0004\u0012\u00020\u0014\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00150\u000f0\u000b\u0012\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00020\u00170\u000b\u0012\u000c\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u000f\u00a2\u0006\u0002\u0010\u001aJ\t\u0010+\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010,\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u000fH\u00c6\u0003J\u000b\u0010-\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\t\u0010.\u001a\u00020\u0007H\u00c6\u0003J\u000b\u0010/\u001a\u0004\u0018\u00010\tH\u00c6\u0003J\u0015\u00100\u001a\u000e\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00020\r0\u000bH\u00c6\u0003J\u000f\u00101\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000fH\u00c6\u0003J\u000f\u00102\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u000fH\u00c6\u0003J\u001b\u00103\u001a\u0014\u0012\u0004\u0012\u00020\u0014\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00150\u000f0\u000bH\u00c6\u0003J\u0015\u00104\u001a\u000e\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00020\u00170\u000bH\u00c6\u0003J\u00ad\u0001\u00105\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t2\u0014\u0008\u0002\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00020\r0\u000b2\u000e\u0008\u0002\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f2\u000e\u0008\u0002\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u000f2\u001a\u0008\u0002\u0010\u0013\u001a\u0014\u0012\u0004\u0012\u00020\u0014\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00150\u000f0\u000b2\u0014\u0008\u0002\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00020\u00170\u000b2\u000e\u0008\u0002\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u000fH\u00c6\u0001J\u0013\u00106\u001a\u0002072\u0008\u00108\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u00109\u001a\u00020:H\u00d6\u0001J\t\u0010;\u001a\u00020\u000cH\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001cR\u0013\u0010\u0008\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001eR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010 R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008!\u0010\"R\u0017\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008#\u0010$R\u001d\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00020\r0\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008%\u0010&R#\u0010\u0013\u001a\u0014\u0012\u0004\u0012\u00020\u0014\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00150\u000f0\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\'\u0010&R\u001d\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00020\u00170\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008(\u0010&R\u0017\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008)\u0010$R\u0017\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008*\u0010$\u00a8\u0006<"
    }
    d2 = {
        "Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;",
        "",
        "item",
        "Lcom/squareup/shared/catalog/models/CatalogItem;",
        "itemImageOrNull",
        "Lcom/squareup/shared/catalog/models/CatalogItemImage;",
        "backingDetails",
        "Lcom/squareup/protos/client/bills/Itemization$BackingDetails;",
        "category",
        "Lcom/squareup/api/items/MenuCategory;",
        "measurementUnits",
        "",
        "",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
        "taxes",
        "",
        "Lcom/squareup/shared/catalog/models/CatalogTax;",
        "variations",
        "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
        "modifierLists",
        "Lcom/squareup/shared/catalog/models/CatalogItemModifierList;",
        "Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;",
        "modifierOverrides",
        "Lcom/squareup/shared/catalog/models/ItemModifierOverride;",
        "itemOptions",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
        "(Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/shared/catalog/models/CatalogItemImage;Lcom/squareup/protos/client/bills/Itemization$BackingDetails;Lcom/squareup/api/items/MenuCategory;Ljava/util/Map;Ljava/util/List;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;Ljava/util/List;)V",
        "getBackingDetails",
        "()Lcom/squareup/protos/client/bills/Itemization$BackingDetails;",
        "getCategory",
        "()Lcom/squareup/api/items/MenuCategory;",
        "getItem",
        "()Lcom/squareup/shared/catalog/models/CatalogItem;",
        "getItemImageOrNull",
        "()Lcom/squareup/shared/catalog/models/CatalogItemImage;",
        "getItemOptions",
        "()Ljava/util/List;",
        "getMeasurementUnits",
        "()Ljava/util/Map;",
        "getModifierLists",
        "getModifierOverrides",
        "getTaxes",
        "getVariations",
        "component1",
        "component10",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final backingDetails:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

.field private final category:Lcom/squareup/api/items/MenuCategory;

.field private final item:Lcom/squareup/shared/catalog/models/CatalogItem;

.field private final itemImageOrNull:Lcom/squareup/shared/catalog/models/CatalogItemImage;

.field private final itemOptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;"
        }
    .end annotation
.end field

.field private final measurementUnits:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
            ">;"
        }
    .end annotation
.end field

.field private final modifierLists:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierList;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;",
            ">;>;"
        }
    .end annotation
.end field

.field private final modifierOverrides:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/ItemModifierOverride;",
            ">;"
        }
    .end annotation
.end field

.field private final taxes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTax;",
            ">;"
        }
    .end annotation
.end field

.field private final variations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/shared/catalog/models/CatalogItemImage;Lcom/squareup/protos/client/bills/Itemization$BackingDetails;Lcom/squareup/api/items/MenuCategory;Ljava/util/Map;Ljava/util/List;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "Lcom/squareup/shared/catalog/models/CatalogItemImage;",
            "Lcom/squareup/protos/client/bills/Itemization$BackingDetails;",
            "Lcom/squareup/api/items/MenuCategory;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogTax;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierList;",
            "+",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;",
            ">;>;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/ItemModifierOverride;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;)V"
        }
    .end annotation

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "backingDetails"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "measurementUnits"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "taxes"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "variations"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "modifierLists"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "modifierOverrides"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemOptions"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->item:Lcom/squareup/shared/catalog/models/CatalogItem;

    iput-object p2, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->itemImageOrNull:Lcom/squareup/shared/catalog/models/CatalogItemImage;

    iput-object p3, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->backingDetails:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    iput-object p4, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->category:Lcom/squareup/api/items/MenuCategory;

    iput-object p5, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->measurementUnits:Ljava/util/Map;

    iput-object p6, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->taxes:Ljava/util/List;

    iput-object p7, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->variations:Ljava/util/List;

    iput-object p8, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->modifierLists:Ljava/util/Map;

    iput-object p9, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->modifierOverrides:Ljava/util/Map;

    iput-object p10, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->itemOptions:Ljava/util/List;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/shared/catalog/models/CatalogItemImage;Lcom/squareup/protos/client/bills/Itemization$BackingDetails;Lcom/squareup/api/items/MenuCategory;Ljava/util/Map;Ljava/util/List;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;
    .locals 11

    move-object v0, p0

    move/from16 v1, p11

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->item:Lcom/squareup/shared/catalog/models/CatalogItem;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->itemImageOrNull:Lcom/squareup/shared/catalog/models/CatalogItemImage;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->backingDetails:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->category:Lcom/squareup/api/items/MenuCategory;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->measurementUnits:Ljava/util/Map;

    goto :goto_4

    :cond_4
    move-object/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->taxes:Ljava/util/List;

    goto :goto_5

    :cond_5
    move-object/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->variations:Ljava/util/List;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-object v9, v0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->modifierLists:Ljava/util/Map;

    goto :goto_7

    :cond_7
    move-object/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget-object v10, v0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->modifierOverrides:Ljava/util/Map;

    goto :goto_8

    :cond_8
    move-object/from16 v10, p9

    :goto_8
    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_9

    iget-object v1, v0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->itemOptions:Ljava/util/List;

    goto :goto_9

    :cond_9
    move-object/from16 v1, p10

    :goto_9
    move-object p1, v2

    move-object p2, v3

    move-object p3, v4

    move-object p4, v5

    move-object/from16 p5, v6

    move-object/from16 p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v9

    move-object/from16 p9, v10

    move-object/from16 p10, v1

    invoke-virtual/range {p0 .. p10}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->copy(Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/shared/catalog/models/CatalogItemImage;Lcom/squareup/protos/client/bills/Itemization$BackingDetails;Lcom/squareup/api/items/MenuCategory;Ljava/util/Map;Ljava/util/List;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;Ljava/util/List;)Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Lcom/squareup/shared/catalog/models/CatalogItem;
    .locals 1

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->item:Lcom/squareup/shared/catalog/models/CatalogItem;

    return-object v0
.end method

.method public final component10()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->itemOptions:Ljava/util/List;

    return-object v0
.end method

.method public final component2()Lcom/squareup/shared/catalog/models/CatalogItemImage;
    .locals 1

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->itemImageOrNull:Lcom/squareup/shared/catalog/models/CatalogItemImage;

    return-object v0
.end method

.method public final component3()Lcom/squareup/protos/client/bills/Itemization$BackingDetails;
    .locals 1

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->backingDetails:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    return-object v0
.end method

.method public final component4()Lcom/squareup/api/items/MenuCategory;
    .locals 1

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->category:Lcom/squareup/api/items/MenuCategory;

    return-object v0
.end method

.method public final component5()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->measurementUnits:Ljava/util/Map;

    return-object v0
.end method

.method public final component6()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTax;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->taxes:Ljava/util/List;

    return-object v0
.end method

.method public final component7()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->variations:Ljava/util/List;

    return-object v0
.end method

.method public final component8()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierList;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->modifierLists:Ljava/util/Map;

    return-object v0
.end method

.method public final component9()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/ItemModifierOverride;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->modifierOverrides:Ljava/util/Map;

    return-object v0
.end method

.method public final copy(Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/shared/catalog/models/CatalogItemImage;Lcom/squareup/protos/client/bills/Itemization$BackingDetails;Lcom/squareup/api/items/MenuCategory;Ljava/util/Map;Ljava/util/List;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;Ljava/util/List;)Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "Lcom/squareup/shared/catalog/models/CatalogItemImage;",
            "Lcom/squareup/protos/client/bills/Itemization$BackingDetails;",
            "Lcom/squareup/api/items/MenuCategory;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogTax;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierList;",
            "+",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;",
            ">;>;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/ItemModifierOverride;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;)",
            "Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;"
        }
    .end annotation

    const-string v0, "item"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "backingDetails"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "measurementUnits"

    move-object/from16 v6, p5

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "taxes"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "variations"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "modifierLists"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "modifierOverrides"

    move-object/from16 v10, p9

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemOptions"

    move-object/from16 v11, p10

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;

    move-object v1, v0

    move-object v3, p2

    move-object/from16 v5, p4

    invoke-direct/range {v1 .. v11}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;-><init>(Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/shared/catalog/models/CatalogItemImage;Lcom/squareup/protos/client/bills/Itemization$BackingDetails;Lcom/squareup/api/items/MenuCategory;Ljava/util/Map;Ljava/util/List;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;Ljava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->item:Lcom/squareup/shared/catalog/models/CatalogItem;

    iget-object v1, p1, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->item:Lcom/squareup/shared/catalog/models/CatalogItem;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->itemImageOrNull:Lcom/squareup/shared/catalog/models/CatalogItemImage;

    iget-object v1, p1, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->itemImageOrNull:Lcom/squareup/shared/catalog/models/CatalogItemImage;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->backingDetails:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    iget-object v1, p1, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->backingDetails:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->category:Lcom/squareup/api/items/MenuCategory;

    iget-object v1, p1, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->category:Lcom/squareup/api/items/MenuCategory;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->measurementUnits:Ljava/util/Map;

    iget-object v1, p1, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->measurementUnits:Ljava/util/Map;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->taxes:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->taxes:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->variations:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->variations:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->modifierLists:Ljava/util/Map;

    iget-object v1, p1, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->modifierLists:Ljava/util/Map;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->modifierOverrides:Ljava/util/Map;

    iget-object v1, p1, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->modifierOverrides:Ljava/util/Map;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->itemOptions:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->itemOptions:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBackingDetails()Lcom/squareup/protos/client/bills/Itemization$BackingDetails;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->backingDetails:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    return-object v0
.end method

.method public final getCategory()Lcom/squareup/api/items/MenuCategory;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->category:Lcom/squareup/api/items/MenuCategory;

    return-object v0
.end method

.method public final getItem()Lcom/squareup/shared/catalog/models/CatalogItem;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->item:Lcom/squareup/shared/catalog/models/CatalogItem;

    return-object v0
.end method

.method public final getItemImageOrNull()Lcom/squareup/shared/catalog/models/CatalogItemImage;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->itemImageOrNull:Lcom/squareup/shared/catalog/models/CatalogItemImage;

    return-object v0
.end method

.method public final getItemOptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;"
        }
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->itemOptions:Ljava/util/List;

    return-object v0
.end method

.method public final getMeasurementUnits()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
            ">;"
        }
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->measurementUnits:Ljava/util/Map;

    return-object v0
.end method

.method public final getModifierLists()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierList;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;",
            ">;>;"
        }
    .end annotation

    .line 30
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->modifierLists:Ljava/util/Map;

    return-object v0
.end method

.method public final getModifierOverrides()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/ItemModifierOverride;",
            ">;"
        }
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->modifierOverrides:Ljava/util/Map;

    return-object v0
.end method

.method public final getTaxes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTax;",
            ">;"
        }
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->taxes:Ljava/util/List;

    return-object v0
.end method

.method public final getVariations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;"
        }
    .end annotation

    .line 29
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->variations:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->item:Lcom/squareup/shared/catalog/models/CatalogItem;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->itemImageOrNull:Lcom/squareup/shared/catalog/models/CatalogItemImage;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->backingDetails:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->category:Lcom/squareup/api/items/MenuCategory;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->measurementUnits:Ljava/util/Map;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->taxes:Ljava/util/List;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->variations:Ljava/util/List;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_6
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->modifierLists:Ljava/util/Map;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_7
    const/4 v2, 0x0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->modifierOverrides:Ljava/util/Map;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_8

    :cond_8
    const/4 v2, 0x0

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->itemOptions:Ljava/util/List;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_9
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CogsSelectedEntryData(item="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->item:Lcom/squareup/shared/catalog/models/CatalogItem;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", itemImageOrNull="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->itemImageOrNull:Lcom/squareup/shared/catalog/models/CatalogItemImage;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", backingDetails="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->backingDetails:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", category="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->category:Lcom/squareup/api/items/MenuCategory;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", measurementUnits="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->measurementUnits:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", taxes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->taxes:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", variations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->variations:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", modifierLists="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->modifierLists:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", modifierOverrides="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->modifierOverrides:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", itemOptions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->itemOptions:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
