.class public final Lcom/squareup/librarylist/CheckoutLibraryListStateSaver_Factory;
.super Ljava/lang/Object;
.source "CheckoutLibraryListStateSaver_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/librarylist/CheckoutLibraryListStateSaver;",
        ">;"
    }
.end annotation


# instance fields
.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final holderSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/librarylist/CheckoutLibraryListState;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/librarylist/CheckoutLibraryListState;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/librarylist/CheckoutLibraryListStateSaver_Factory;->holderSettingProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/librarylist/CheckoutLibraryListStateSaver_Factory;->deviceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/librarylist/CheckoutLibraryListStateSaver_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/librarylist/CheckoutLibraryListState;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;)",
            "Lcom/squareup/librarylist/CheckoutLibraryListStateSaver_Factory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/librarylist/CheckoutLibraryListStateSaver_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/librarylist/CheckoutLibraryListStateSaver_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/LocalSetting;Lcom/squareup/util/Device;)Lcom/squareup/librarylist/CheckoutLibraryListStateSaver;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/librarylist/CheckoutLibraryListState;",
            ">;",
            "Lcom/squareup/util/Device;",
            ")",
            "Lcom/squareup/librarylist/CheckoutLibraryListStateSaver;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/librarylist/CheckoutLibraryListStateSaver;

    invoke-direct {v0, p0, p1}, Lcom/squareup/librarylist/CheckoutLibraryListStateSaver;-><init>(Lcom/squareup/settings/LocalSetting;Lcom/squareup/util/Device;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/librarylist/CheckoutLibraryListStateSaver;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListStateSaver_Factory;->holderSettingProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/LocalSetting;

    iget-object v1, p0, Lcom/squareup/librarylist/CheckoutLibraryListStateSaver_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Device;

    invoke-static {v0, v1}, Lcom/squareup/librarylist/CheckoutLibraryListStateSaver_Factory;->newInstance(Lcom/squareup/settings/LocalSetting;Lcom/squareup/util/Device;)Lcom/squareup/librarylist/CheckoutLibraryListStateSaver;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/librarylist/CheckoutLibraryListStateSaver_Factory;->get()Lcom/squareup/librarylist/CheckoutLibraryListStateSaver;

    move-result-object v0

    return-object v0
.end method
