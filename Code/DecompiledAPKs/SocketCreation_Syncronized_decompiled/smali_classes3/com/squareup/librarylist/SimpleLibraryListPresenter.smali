.class public Lcom/squareup/librarylist/SimpleLibraryListPresenter;
.super Lmortar/ViewPresenter;
.source "SimpleLibraryListPresenter.java"

# interfaces
.implements Lcom/squareup/librarylist/LibraryListPresenter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryCategoriesQueryCallback;,
        Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryQueryCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/librarylist/LibraryListView;",
        ">;",
        "Lcom/squareup/librarylist/LibraryListPresenter;"
    }
.end annotation


# static fields
.field private static final ITEM_THRESHOLD_FOR_ITEM_SUGGESTIONS_VISIBILITY:I = 0x2


# instance fields
.field protected adapter:Lcom/squareup/librarylist/CheckoutLibraryListAdapter;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field protected final bus:Lcom/squareup/badbus/BadBus;

.field private final catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

.field private final catalogLocalizer:Lcom/squareup/shared/i18n/Localizer;

.field private categoryQueryCallback:Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryCategoriesQueryCallback;

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field protected final configuration:Lcom/squareup/librarylist/LibraryListConfiguration;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private currentCursorQueryId:Ljava/lang/String;

.field protected cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

.field private final device:Lcom/squareup/util/Device;

.field private final discountBundleFactory:Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;

.field private final discountCursorFactory:Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;

.field private final durationFormatter:Lcom/squareup/text/DurationFormatter;

.field protected final entryHandler:Lcom/squareup/librarylist/LibraryListEntryHandler;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private isLibraryEmpty:Ljava/lang/Boolean;

.field private final itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

.field protected final libraryListAssistant:Lcom/squareup/librarylist/LibraryListAssistant;

.field protected final libraryState:Lcom/squareup/librarylist/LibraryListStateManager;

.field private final percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field private final priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private queryCallback:Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryQueryCallback;

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

.field private final timeZone:Ljava/util/TimeZone;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Ljavax/inject/Provider;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/librarylist/LibraryListStateManager;Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;Lcom/squareup/util/Clock;Lcom/squareup/badbus/BadBus;Lcom/squareup/librarylist/LibraryListEntryHandler;Lcom/squareup/librarylist/LibraryListConfiguration;Lcom/squareup/librarylist/LibraryListAssistant;Lcom/squareup/shared/i18n/Localizer;Lcom/squareup/analytics/Analytics;Lcom/squareup/catalogapi/CatalogIntegrationController;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/DurationFormatter;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            "Lcom/squareup/librarylist/LibraryListStateManager;",
            "Lflow/Flow;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;",
            "Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/librarylist/LibraryListEntryHandler;",
            "Lcom/squareup/librarylist/LibraryListConfiguration;",
            "Lcom/squareup/librarylist/LibraryListAssistant;",
            "Lcom/squareup/shared/i18n/Localizer;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    .line 144
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    move-object v1, p1

    .line 145
    iput-object v1, v0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->res:Lcom/squareup/util/Res;

    move-object v1, p3

    .line 146
    iput-object v1, v0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    move-object v1, p4

    .line 147
    iput-object v1, v0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    move-object v1, p5

    .line 148
    iput-object v1, v0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->percentageFormatter:Lcom/squareup/text/Formatter;

    move-object v1, p6

    .line 149
    iput-object v1, v0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    move-object v1, p7

    .line 150
    iput-object v1, v0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    move-object v1, p8

    .line 151
    iput-object v1, v0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->features:Lcom/squareup/settings/server/Features;

    move-object v1, p9

    .line 152
    iput-object v1, v0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object v1, p10

    .line 153
    iput-object v1, v0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    move-object v1, p11

    .line 154
    iput-object v1, v0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->libraryState:Lcom/squareup/librarylist/LibraryListStateManager;

    move-object v1, p12

    .line 155
    iput-object v1, v0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->flow:Lflow/Flow;

    move-object v1, p13

    .line 156
    iput-object v1, v0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->device:Lcom/squareup/util/Device;

    move-object v1, p2

    .line 157
    iput-object v1, v0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->cogsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 158
    iput-object v1, v0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->discountCursorFactory:Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;

    move-object/from16 v1, p15

    .line 159
    iput-object v1, v0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->discountBundleFactory:Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;

    .line 160
    invoke-interface/range {p16 .. p16}, Lcom/squareup/util/Clock;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->timeZone:Ljava/util/TimeZone;

    move-object/from16 v1, p17

    .line 161
    iput-object v1, v0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->bus:Lcom/squareup/badbus/BadBus;

    move-object/from16 v1, p18

    .line 162
    iput-object v1, v0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->entryHandler:Lcom/squareup/librarylist/LibraryListEntryHandler;

    move-object/from16 v1, p19

    .line 163
    iput-object v1, v0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->configuration:Lcom/squareup/librarylist/LibraryListConfiguration;

    move-object/from16 v1, p20

    .line 164
    iput-object v1, v0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->libraryListAssistant:Lcom/squareup/librarylist/LibraryListAssistant;

    move-object/from16 v1, p21

    .line 165
    iput-object v1, v0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->catalogLocalizer:Lcom/squareup/shared/i18n/Localizer;

    move-object/from16 v1, p22

    .line 166
    iput-object v1, v0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    move-object/from16 v1, p23

    .line 167
    iput-object v1, v0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/librarylist/SimpleLibraryListPresenter;)Ljava/lang/Object;
    .locals 0

    .line 82
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$202(Lcom/squareup/librarylist/SimpleLibraryListPresenter;Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryQueryCallback;)Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryQueryCallback;
    .locals 0

    .line 82
    iput-object p1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->queryCallback:Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryQueryCallback;

    return-object p1
.end method

.method static synthetic access$300(Lcom/squareup/librarylist/SimpleLibraryListPresenter;)Ljava/lang/Object;
    .locals 0

    .line 82
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private buildQueryId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 587
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->libraryState:Lcom/squareup/librarylist/LibraryListStateManager;

    invoke-interface {v0}, Lcom/squareup/librarylist/LibraryListStateManager;->getLibraryFilter()Lcom/squareup/librarylist/LibraryListState$Filter;

    move-result-object v0

    .line 589
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 590
    invoke-direct {p0, p1}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->searchQueryId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 592
    :cond_0
    sget-object p1, Lcom/squareup/librarylist/SimpleLibraryListPresenter$1;->$SwitchMap$com$squareup$librarylist$LibraryListState$Filter:[I

    invoke-virtual {v0}, Lcom/squareup/librarylist/LibraryListState$Filter;->ordinal()I

    move-result v1

    aget p1, p1, v1

    const/4 v1, 0x4

    if-eq p1, v1, :cond_1

    .line 597
    invoke-direct {p0, v0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->filterQueryId(Lcom/squareup/librarylist/LibraryListState$Filter;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 594
    :cond_1
    iget-object p1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->libraryState:Lcom/squareup/librarylist/LibraryListStateManager;

    invoke-interface {p1}, Lcom/squareup/librarylist/LibraryListStateManager;->getCurrentCategoryId()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->singleCategoryQueryId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private cacheListPosition()V
    .locals 1

    .line 629
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 630
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/librarylist/LibraryListView;

    invoke-virtual {v0}, Lcom/squareup/librarylist/LibraryListView;->cacheListPosition()V

    return-void
.end method

.method private cancelQueryCallback()V
    .locals 1

    .line 616
    invoke-direct {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->hasPendingQuery()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 617
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->queryCallback:Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryQueryCallback;

    invoke-virtual {v0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryQueryCallback;->cancel()V

    const/4 v0, 0x0

    .line 618
    iput-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->queryCallback:Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryQueryCallback;

    :cond_0
    return-void
.end method

.method private closeCursor()V
    .locals 1

    .line 623
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 624
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->close()V

    :cond_0
    return-void
.end method

.method private filterQueryId(Lcom/squareup/librarylist/LibraryListState$Filter;)Ljava/lang/String;
    .locals 0

    .line 612
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListState$Filter;->name()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getSafeCursorCount()I
    .locals 1

    .line 286
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 287
    :cond_0
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getCount()I

    move-result v0

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method private hasPendingQuery()Z
    .locals 1

    .line 499
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->queryCallback:Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryQueryCallback;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private hasSearchText()Z
    .locals 1

    .line 395
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->libraryListAssistant:Lcom/squareup/librarylist/LibraryListAssistant;

    invoke-interface {v0}, Lcom/squareup/librarylist/LibraryListAssistant;->getHasSearchText()Z

    move-result v0

    return v0
.end method

.method private isCreateNewItemButton(I)Z
    .locals 1

    .line 362
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->adapter:Lcom/squareup/librarylist/CheckoutLibraryListAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->isCreateNewItemButton(I)Z

    move-result p1

    return p1
.end method

.method private isItemSuggestion(I)Z
    .locals 1

    .line 366
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->adapter:Lcom/squareup/librarylist/CheckoutLibraryListAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->isItemSuggestion(I)Z

    move-result p1

    return p1
.end method

.method private isLibraryEntryEnabled(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)Z
    .locals 3

    const/4 v0, 0x1

    if-nez p1, :cond_0

    return v0

    .line 581
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v1

    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    if-ne v1, v2, :cond_1

    return v0

    .line 583
    :cond_1
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->entryHandler:Lcom/squareup/librarylist/LibraryListEntryHandler;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/squareup/librarylist/LibraryListEntryHandler;->isEntryEnabled(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public static synthetic lambda$vrwvxdHold9bH8_7MUr-MOAzf64(Lcom/squareup/librarylist/SimpleLibraryListPresenter;Lcom/squareup/cogs/CatalogUpdateEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->onCogsUpdate(Lcom/squareup/cogs/CatalogUpdateEvent;)V

    return-void
.end method

.method private onCogsUpdate(Lcom/squareup/cogs/CatalogUpdateEvent;)V
    .locals 3

    .line 634
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->libraryState:Lcom/squareup/librarylist/LibraryListStateManager;

    invoke-interface {v0, p1}, Lcom/squareup/librarylist/LibraryListStateManager;->onCogsUpdate(Lcom/squareup/cogs/CatalogUpdateEvent;)V

    .line 636
    invoke-virtual {p1}, Lcom/squareup/cogs/CatalogUpdateEvent;->isItemSuggestionDeleted()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v1, 0x0

    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DISCOUNT:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_IMAGE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_VARIATION:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    aput-object v2, v0, v1

    .line 637
    invoke-virtual {p1, v0}, Lcom/squareup/cogs/CatalogUpdateEvent;->hasOneOf([Lcom/squareup/shared/catalog/models/CatalogObjectType;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 638
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->forceRefreshLibrary()V

    :cond_1
    return-void
.end method

.method private onCreateNewOrSuggestionOrPlaceholderClicked(I)V
    .locals 2

    .line 339
    invoke-direct {p0, p1}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->isCreateNewItemButton(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 340
    iget-object p1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->entryHandler:Lcom/squareup/librarylist/LibraryListEntryHandler;

    invoke-interface {p1}, Lcom/squareup/librarylist/LibraryListEntryHandler;->createNewLibraryItemClicked()V

    return-void

    .line 343
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->isItemSuggestion(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 344
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->adapter:Lcom/squareup/librarylist/CheckoutLibraryListAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->getItemSuggestion(I)Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;

    move-result-object p1

    .line 345
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->entryHandler:Lcom/squareup/librarylist/LibraryListEntryHandler;

    invoke-interface {v0, p1}, Lcom/squareup/librarylist/LibraryListEntryHandler;->itemSuggestionClicked(Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;)V

    return-void

    .line 349
    :cond_1
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->adapter:Lcom/squareup/librarylist/CheckoutLibraryListAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->getPlaceholder(I)Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    move-result-object p1

    .line 350
    sget-object v0, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->CUSTOM_AMOUNT:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    if-ne p1, v0, :cond_2

    .line 351
    iget-object p1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->entryHandler:Lcom/squareup/librarylist/LibraryListEntryHandler;

    invoke-interface {p1}, Lcom/squareup/librarylist/LibraryListEntryHandler;->customAmountClicked()V

    return-void

    .line 355
    :cond_2
    sget-object v0, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->REWARDS_FLOW:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    if-ne p1, v0, :cond_3

    .line 356
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CHECKOUT_REDEEM_REWARDS:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 358
    :cond_3
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->libraryState:Lcom/squareup/librarylist/LibraryListStateManager;

    iget-object v1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->flow:Lflow/Flow;

    invoke-interface {v0, p1, v1}, Lcom/squareup/librarylist/LibraryListStateManager;->placeholderClicked(Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;Lflow/Flow;)V

    return-void
.end method

.method private openItemsApplet()V
    .locals 1

    .line 391
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->libraryListAssistant:Lcom/squareup/librarylist/LibraryListAssistant;

    invoke-interface {v0}, Lcom/squareup/librarylist/LibraryListAssistant;->openItemsApplet()V

    return-void
.end method

.method private searchLibrary(Ljava/lang/String;)V
    .locals 4

    .line 403
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 404
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/librarylist/log/CheckoutLibrarySearchEvent;

    invoke-direct {v1, p1}, Lcom/squareup/librarylist/log/CheckoutLibrarySearchEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 406
    :cond_0
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isLandscapeLongTablet()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 410
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/librarylist/LibraryListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/librarylist/LibraryListView;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 412
    :cond_1
    invoke-direct {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->cancelQueryCallback()V

    .line 413
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->libraryState:Lcom/squareup/librarylist/LibraryListStateManager;

    invoke-interface {v0}, Lcom/squareup/librarylist/LibraryListStateManager;->getLibraryFilter()Lcom/squareup/librarylist/LibraryListState$Filter;

    move-result-object v0

    .line 414
    iget-object v1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->libraryState:Lcom/squareup/librarylist/LibraryListStateManager;

    invoke-interface {v1}, Lcom/squareup/librarylist/LibraryListStateManager;->getCurrentCategoryId()Ljava/lang/String;

    move-result-object v1

    .line 415
    invoke-direct {p0, p1}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->buildQueryId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 418
    sget-object v3, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_CATEGORIES:Lcom/squareup/librarylist/LibraryListState$Filter;

    if-ne v0, v3, :cond_2

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 419
    new-instance p1, Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryCategoriesQueryCallback;

    invoke-direct {p1, p0, v2}, Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryCategoriesQueryCallback;-><init>(Lcom/squareup/librarylist/SimpleLibraryListPresenter;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->categoryQueryCallback:Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryCategoriesQueryCallback;

    .line 420
    iget-object p1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cogs/Cogs;

    new-instance v0, Lcom/squareup/librarylist/-$$Lambda$SimpleLibraryListPresenter$Pdb8_cA07pK8gVc7uzSIxl76qOU;

    invoke-direct {v0, p0}, Lcom/squareup/librarylist/-$$Lambda$SimpleLibraryListPresenter$Pdb8_cA07pK8gVc7uzSIxl76qOU;-><init>(Lcom/squareup/librarylist/SimpleLibraryListPresenter;)V

    iget-object v1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->categoryQueryCallback:Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryCategoriesQueryCallback;

    invoke-interface {p1, v0, v1}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    goto :goto_0

    .line 427
    :cond_2
    new-instance v3, Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryQueryCallback;

    invoke-direct {v3, p0, v2}, Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryQueryCallback;-><init>(Lcom/squareup/librarylist/SimpleLibraryListPresenter;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->queryCallback:Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryQueryCallback;

    .line 428
    iget-object v2, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cogs/Cogs;

    new-instance v3, Lcom/squareup/librarylist/-$$Lambda$SimpleLibraryListPresenter$qQF6BNS-GtHLP0lt93qzfwvPBAw;

    invoke-direct {v3, p0, p1, v0, v1}, Lcom/squareup/librarylist/-$$Lambda$SimpleLibraryListPresenter$qQF6BNS-GtHLP0lt93qzfwvPBAw;-><init>(Lcom/squareup/librarylist/SimpleLibraryListPresenter;Ljava/lang/String;Lcom/squareup/librarylist/LibraryListState$Filter;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->queryCallback:Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryQueryCallback;

    invoke-interface {v2, v3, p1}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    :goto_0
    return-void
.end method

.method private searchQueryId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 604
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SEARCH-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private shouldHideGiftCardsAndRewards()Z
    .locals 2

    .line 281
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ORDER_ENTRY_LIBRARY_HIDE_GIFTCARDS_REWARDS_IF_NO_ITEMS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 282
    invoke-direct {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->getSafeCursorCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private showEmptyLibrary()V
    .locals 5

    .line 525
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->libraryState:Lcom/squareup/librarylist/LibraryListStateManager;

    invoke-interface {v0}, Lcom/squareup/librarylist/LibraryListStateManager;->getLibraryFilter()Lcom/squareup/librarylist/LibraryListState$Filter;

    move-result-object v0

    .line 526
    iget-object v1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->libraryState:Lcom/squareup/librarylist/LibraryListStateManager;

    invoke-interface {v1}, Lcom/squareup/librarylist/LibraryListStateManager;->getCurrentCategoryName()Ljava/lang/String;

    move-result-object v1

    .line 528
    sget-object v2, Lcom/squareup/librarylist/SimpleLibraryListPresenter$1;->$SwitchMap$com$squareup$librarylist$LibraryListState$Filter:[I

    invoke-virtual {v0}, Lcom/squareup/librarylist/LibraryListState$Filter;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 572
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown library mode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 563
    :pswitch_0
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isLandscapeLongTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 567
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/librarylist/LibraryListView;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_STACK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/librarylist/R$string;->item_library_empty_services_title:I

    .line 568
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 567
    invoke-virtual {v0, v1, v2}, Lcom/squareup/librarylist/LibraryListView;->showEmptyLibraryView(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 564
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/librarylist/R$string;->item_library_empty_note_services_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/librarylist/R$string;->item_library_empty_note_services_message_items_applet:I

    .line 565
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 564
    invoke-direct {p0, v0, v1}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->showNoContent(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 554
    :pswitch_1
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isLandscapeLongTablet()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_1

    .line 558
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/librarylist/LibraryListView;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_STACK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/librarylist/R$string;->item_library_empty_items_title:I

    .line 559
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 558
    invoke-virtual {v0, v1, v2}, Lcom/squareup/librarylist/LibraryListView;->showEmptyLibraryView(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 555
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/librarylist/R$string;->item_library_empty_note_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/librarylist/R$string;->item_library_empty_note_message_items_applet:I

    .line 556
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 555
    invoke-direct {p0, v0, v1}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->showNoContent(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 547
    :pswitch_2
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/librarylist/LibraryListView;

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_STACK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v3, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/librarylist/R$string;->item_library_empty_category_note_title:I

    .line 548
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    const-string v4, "category_name"

    .line 549
    invoke-virtual {v3, v4, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 550
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 551
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 547
    invoke-virtual {v0, v2, v1}, Lcom/squareup/librarylist/LibraryListView;->showEmptyLibraryView(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;)V

    goto :goto_2

    .line 543
    :pswitch_3
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/librarylist/LibraryListView;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_GIFT_CARD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/librarylist/R$string;->item_library_empty_gift_cards_note_title:I

    .line 544
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 543
    invoke-virtual {v0, v1, v2}, Lcom/squareup/librarylist/LibraryListView;->showEmptyLibraryView(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;)V

    goto :goto_2

    .line 539
    :pswitch_4
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/librarylist/LibraryListView;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_TAG:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/librarylist/R$string;->item_library_empty_discounts_note_title:I

    .line 540
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 539
    invoke-virtual {v0, v1, v2}, Lcom/squareup/librarylist/LibraryListView;->showEmptyLibraryView(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;)V

    goto :goto_2

    .line 530
    :pswitch_5
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isLandscapeLongTablet()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 531
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/librarylist/LibraryListView;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_STACK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/librarylist/R$string;->item_library_empty_categories:I

    .line 532
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 531
    invoke-virtual {v0, v1, v2}, Lcom/squareup/librarylist/LibraryListView;->showEmptyLibraryView(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;)V

    goto :goto_2

    .line 534
    :cond_4
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/librarylist/R$string;->item_library_empty_note_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/librarylist/R$string;->item_library_empty_note_message_items_applet:I

    .line 535
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 534
    invoke-direct {p0, v0, v1}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->showNoContent(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private showLibrary()V
    .locals 1

    .line 504
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->adapter:Lcom/squareup/librarylist/CheckoutLibraryListAdapter;

    invoke-virtual {v0}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->hasSearchText()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 505
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/librarylist/LibraryListView;

    invoke-virtual {v0}, Lcom/squareup/librarylist/LibraryListView;->showNoResults()V

    return-void

    .line 510
    :cond_0
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->adapter:Lcom/squareup/librarylist/CheckoutLibraryListAdapter;

    invoke-virtual {v0}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->hasSearchText()Z

    move-result v0

    if-nez v0, :cond_1

    .line 511
    invoke-direct {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->showEmptyLibrary()V

    return-void

    .line 516
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/librarylist/LibraryListView;

    invoke-virtual {v0}, Lcom/squareup/librarylist/LibraryListView;->showLibrary()V

    return-void
.end method

.method private showNoContent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 520
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/librarylist/LibraryListView;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_STACK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->configuration:Lcom/squareup/librarylist/LibraryListConfiguration;

    .line 521
    invoke-interface {v2}, Lcom/squareup/librarylist/LibraryListConfiguration;->getCanShowEditItemsButton()Z

    move-result v2

    .line 520
    invoke-virtual {v0, v1, p1, p2, v2}, Lcom/squareup/librarylist/LibraryListView;->showEmptyLibraryView(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method private singleCategoryQueryId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 608
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/squareup/librarylist/LibraryListState$Filter;->SINGLE_CATEGORY:Lcom/squareup/librarylist/LibraryListState$Filter;

    invoke-virtual {v1}, Lcom/squareup/librarylist/LibraryListState$Filter;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public bridge synthetic dropView(Lcom/squareup/librarylist/LibraryListView;)V
    .locals 0

    .line 82
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method protected forceRefreshLibrary()V
    .locals 1

    .line 482
    invoke-direct {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->hasPendingQuery()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 486
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->hasCursor()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 487
    invoke-direct {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->cacheListPosition()V

    .line 488
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->adapter:Lcom/squareup/librarylist/CheckoutLibraryListAdapter;

    invoke-virtual {v0}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->getCurrentFilter()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->searchLibrary(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v0, ""

    .line 490
    invoke-virtual {p0, v0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->newLibrarySearch(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method protected hasCursor()Z
    .locals 1

    .line 495
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public synthetic lambda$null$2$SimpleLibraryListPresenter(Lcom/squareup/librarylist/LibraryListView$Action;)V
    .locals 3

    .line 187
    instance-of v0, p1, Lcom/squareup/librarylist/LibraryListView$Action$OpenItemsApplet;

    if-eqz v0, :cond_0

    .line 188
    invoke-direct {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->openItemsApplet()V

    goto :goto_0

    .line 189
    :cond_0
    instance-of v0, p1, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;

    if-eqz v0, :cond_1

    .line 190
    check-cast p1, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;

    .line 191
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;->getPosition()I

    move-result p1

    invoke-virtual {p0, v0, p1}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->libraryListItemClicked(Landroid/view/View;I)V

    goto :goto_0

    .line 192
    :cond_1
    instance-of v0, p1, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemLongClicked;

    if-eqz v0, :cond_2

    .line 193
    check-cast p1, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemLongClicked;

    .line 194
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemLongClicked;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemLongClicked;->getPosition()I

    move-result p1

    invoke-virtual {p0, v0, p1}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->libraryListItemLongClicked(Landroid/view/View;I)V

    :goto_0
    return-void

    .line 196
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public synthetic lambda$onEnterScope$0$SimpleLibraryListPresenter(Lcom/squareup/librarylist/CheckoutLibraryListState;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const-string p1, ""

    .line 172
    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->newLibrarySearch(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$1$SimpleLibraryListPresenter(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)Z
    .locals 0

    .line 179
    invoke-direct {p0, p1}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->isLibraryEntryEnabled(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method public synthetic lambda$onLoad$3$SimpleLibraryListPresenter()Lrx/Subscription;
    .locals 2

    .line 186
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/librarylist/LibraryListView;

    invoke-virtual {v0}, Lcom/squareup/librarylist/LibraryListView;->getAction()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/librarylist/-$$Lambda$SimpleLibraryListPresenter$soAVDtGUHb7kPGAGkkjDMtA4WoY;

    invoke-direct {v1, p0}, Lcom/squareup/librarylist/-$$Lambda$SimpleLibraryListPresenter$soAVDtGUHb7kPGAGkkjDMtA4WoY;-><init>(Lcom/squareup/librarylist/SimpleLibraryListPresenter;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$searchLibrary$4$SimpleLibraryListPresenter(Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;
    .locals 2

    .line 421
    const-class v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 422
    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->getSyntheticTableReader(Ljava/lang/Class;)Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 423
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/squareup/api/items/Item$Type;

    .line 424
    invoke-virtual {v0, v1}, Lcom/squareup/settings/server/AccountStatusSettings;->supportedCatalogItemTypesExcluding([Lcom/squareup/api/items/Item$Type;)Ljava/util/List;

    move-result-object v0

    .line 423
    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readAllUsedCategoriesAndEmpty(Ljava/util/List;)Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$searchLibrary$5$SimpleLibraryListPresenter(Ljava/lang/String;Lcom/squareup/librarylist/LibraryListState$Filter;Ljava/lang/String;Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
    .locals 6

    .line 430
    const-class v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 431
    invoke-interface {p4, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->getSyntheticTableReader(Ljava/lang/Class;)Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 432
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    .line 433
    sget-object p1, Lcom/squareup/librarylist/SimpleLibraryListPresenter$1;->$SwitchMap$com$squareup$librarylist$LibraryListState$Filter:[I

    invoke-virtual {p2}, Lcom/squareup/librarylist/LibraryListState$Filter;->ordinal()I

    move-result v1

    aget p1, p1, v1

    packed-switch p1, :pswitch_data_0

    .line 459
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Unknown mode: "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 443
    :pswitch_0
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readAllServices()Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object p1

    goto :goto_0

    .line 439
    :pswitch_1
    iget-object p1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    new-array p2, v2, [Lcom/squareup/api/items/Item$Type;

    sget-object p3, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    aput-object p3, p2, v3

    .line 440
    invoke-virtual {p1, p2}, Lcom/squareup/settings/server/AccountStatusSettings;->supportedCatalogItemTypesExcluding([Lcom/squareup/api/items/Item$Type;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readItemsWithTypes(Ljava/util/List;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object p1

    goto :goto_0

    .line 435
    :pswitch_2
    iget-object p1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    new-array p2, v2, [Lcom/squareup/api/items/Item$Type;

    sget-object p4, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    aput-object p4, p2, v3

    .line 436
    invoke-virtual {p1, p2}, Lcom/squareup/settings/server/AccountStatusSettings;->supportedCatalogItemTypesExcluding([Lcom/squareup/api/items/Item$Type;)Ljava/util/List;

    move-result-object p1

    .line 435
    invoke-virtual {v0, p3, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->findCatalogItemsForCategoryId(Ljava/lang/String;Ljava/util/List;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object p1

    goto :goto_0

    .line 449
    :pswitch_3
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readAllGiftCards()Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object p1

    goto :goto_0

    .line 452
    :pswitch_4
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readAllDiscounts()Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object v2

    .line 453
    iget-object p1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object p2, Lcom/squareup/settings/server/Features$Feature;->USE_PRICING_ENGINE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, p2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 454
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->discountCursorFactory:Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;

    iget-object v3, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->timeZone:Ljava/util/TimeZone;

    iget-object v4, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->discountBundleFactory:Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;

    iget-object v5, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->catalogLocalizer:Lcom/squareup/shared/i18n/Localizer;

    move-object v1, p4

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;->fromDiscountsCursor(Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Ljava/util/TimeZone;Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;Lcom/squareup/shared/i18n/Localizer;)Lcom/squareup/prices/DiscountRulesLibraryCursor;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, v2

    goto :goto_0

    .line 446
    :pswitch_5
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "This case (ALL_CATEGORIES with blank search text) has been handled above."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 462
    :cond_1
    iget-object p2, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    new-array p3, v2, [Lcom/squareup/api/items/Item$Type;

    sget-object p4, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    aput-object p4, p3, v3

    .line 463
    invoke-virtual {p2, p3}, Lcom/squareup/settings/server/AccountStatusSettings;->supportedCatalogItemTypesExcluding([Lcom/squareup/api/items/Item$Type;)Ljava/util/List;

    move-result-object p2

    .line 462
    invoke-virtual {v0, p1, p2, v3}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->wordPrefixSearchForItemsOrDiscountsByName(Ljava/lang/String;Ljava/util/List;Z)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object p1

    :goto_0
    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public libraryListItemClicked(Landroid/view/View;I)V
    .locals 2

    .line 316
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->libraryListAssistant:Lcom/squareup/librarylist/LibraryListAssistant;

    invoke-interface {v0}, Lcom/squareup/librarylist/LibraryListAssistant;->logLibraryListItemClicked()V

    .line 319
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->adapter:Lcom/squareup/librarylist/CheckoutLibraryListAdapter;

    invoke-virtual {v0, p2}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->isEnabled(I)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 321
    :cond_0
    check-cast p1, Lcom/squareup/librarylist/LibraryItemListNohoRow;

    .line 322
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->adapter:Lcom/squareup/librarylist/CheckoutLibraryListAdapter;

    invoke-virtual {v0, p2}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->getItem(I)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v0

    if-nez v0, :cond_1

    .line 326
    invoke-direct {p0, p2}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->onCreateNewOrSuggestionOrPlaceholderClicked(I)V

    goto :goto_0

    .line 327
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object p2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    if-ne p2, v1, :cond_2

    .line 328
    iget-object p1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->libraryState:Lcom/squareup/librarylist/LibraryListStateManager;

    invoke-interface {p1, v0}, Lcom/squareup/librarylist/LibraryListStateManager;->setModeToSingleCategory(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)V

    goto :goto_0

    .line 329
    :cond_2
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object p2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DISCOUNT:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    if-ne p2, v1, :cond_3

    .line 330
    iget-object p2, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->entryHandler:Lcom/squareup/librarylist/LibraryListEntryHandler;

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getItemThumbnail()Landroid/widget/ImageView;

    move-result-object p1

    const/4 v1, 0x1

    invoke-interface {p2, p1, v0, v1}, Lcom/squareup/librarylist/LibraryListEntryHandler;->discountClicked(Landroid/view/View;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Z)V

    .line 331
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/librarylist/LibraryListView;

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListView;->hideKeyboard()V

    goto :goto_0

    .line 332
    :cond_3
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object p2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    if-ne p2, v1, :cond_4

    .line 333
    iget-object p2, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->entryHandler:Lcom/squareup/librarylist/LibraryListEntryHandler;

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getItemThumbnail()Landroid/widget/ImageView;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getSearchFilter()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, p1, v0, v1}, Lcom/squareup/librarylist/LibraryListEntryHandler;->itemClicked(Landroid/widget/ImageView;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Ljava/lang/String;)V

    .line 334
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/librarylist/LibraryListView;

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListView;->hideKeyboard()V

    :cond_4
    :goto_0
    return-void
.end method

.method public libraryListItemLongClicked(Landroid/view/View;I)V
    .locals 3

    .line 371
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->libraryListAssistant:Lcom/squareup/librarylist/LibraryListAssistant;

    invoke-interface {v0}, Lcom/squareup/librarylist/LibraryListAssistant;->logLibraryListItemLongClicked()V

    .line 373
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->configuration:Lcom/squareup/librarylist/LibraryListConfiguration;

    invoke-interface {v0}, Lcom/squareup/librarylist/LibraryListConfiguration;->getCanLongClickOnItem()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 376
    :cond_0
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->adapter:Lcom/squareup/librarylist/CheckoutLibraryListAdapter;

    invoke-virtual {v0, p2}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->getItem(I)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 378
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v1

    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    if-ne v1, v2, :cond_2

    .line 379
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getItemType()Lcom/squareup/api/items/Item$Type;

    move-result-object v1

    sget-object v2, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    if-ne v1, v2, :cond_1

    goto :goto_0

    .line 384
    :cond_1
    check-cast p1, Lcom/squareup/librarylist/LibraryItemListNohoRow;

    .line 385
    iget-object p2, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->entryHandler:Lcom/squareup/librarylist/LibraryListEntryHandler;

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getItemThumbnail()Landroid/widget/ImageView;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    .line 386
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getSearchFilter()Ljava/lang/String;

    move-result-object v1

    .line 385
    invoke-interface {p2, p1, v0, v1}, Lcom/squareup/librarylist/LibraryListEntryHandler;->itemLongClicked(Landroid/widget/ImageView;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Ljava/lang/String;)V

    .line 387
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/librarylist/LibraryListView;

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListView;->hideKeyboard()V

    return-void

    .line 380
    :cond_2
    :goto_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->libraryListItemClicked(Landroid/view/View;I)V

    return-void
.end method

.method public newLibrarySearch(Ljava/lang/String;)V
    .locals 2

    const-string v0, "searchText"

    .line 224
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 225
    invoke-direct {p0, p1}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->buildQueryId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 226
    invoke-direct {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->hasPendingQuery()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->queryCallback:Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryQueryCallback;

    invoke-static {v1}, Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryQueryCallback;->access$000(Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryQueryCallback;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    .line 228
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->hasCursor()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->currentCursorQueryId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 231
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 232
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/librarylist/LibraryListView;

    invoke-virtual {v0}, Lcom/squareup/librarylist/LibraryListView;->resetListPosition()V

    .line 234
    :cond_2
    invoke-direct {p0, p1}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->searchLibrary(Ljava/lang/String;)V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 11

    .line 171
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->libraryState:Lcom/squareup/librarylist/LibraryListStateManager;

    .line 172
    invoke-interface {v0}, Lcom/squareup/librarylist/LibraryListStateManager;->holder()Lrx/Observable;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/librarylist/-$$Lambda$SimpleLibraryListPresenter$7PzAudLCdq_8IafwS9U3a3GhZAw;

    invoke-direct {v1, p0}, Lcom/squareup/librarylist/-$$Lambda$SimpleLibraryListPresenter$7PzAudLCdq_8IafwS9U3a3GhZAw;-><init>(Lcom/squareup/librarylist/SimpleLibraryListPresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 171
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 173
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->bus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/cogs/CatalogUpdateEvent;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/librarylist/-$$Lambda$SimpleLibraryListPresenter$vrwvxdHold9bH8_7MUr-MOAzf64;

    invoke-direct {v1, p0}, Lcom/squareup/librarylist/-$$Lambda$SimpleLibraryListPresenter$vrwvxdHold9bH8_7MUr-MOAzf64;-><init>(Lcom/squareup/librarylist/SimpleLibraryListPresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 176
    new-instance p1, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;

    iget-object v2, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iget-object v3, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v4, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->percentageFormatter:Lcom/squareup/text/Formatter;

    iget-object v5, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    iget-object v6, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    .line 178
    invoke-virtual {v0}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->isTextTileMode()Z

    move-result v7

    new-instance v8, Lcom/squareup/librarylist/-$$Lambda$SimpleLibraryListPresenter$WT3hZPf08X5iWQFeP0DmP80M40c;

    invoke-direct {v8, p0}, Lcom/squareup/librarylist/-$$Lambda$SimpleLibraryListPresenter$WT3hZPf08X5iWQFeP0DmP80M40c;-><init>(Lcom/squareup/librarylist/SimpleLibraryListPresenter;)V

    iget-object v9, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->res:Lcom/squareup/util/Res;

    iget-object v10, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    move-object v1, p1

    invoke-direct/range {v1 .. v10}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;-><init>(Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;ZLcom/squareup/librarylist/CheckoutLibraryListAdapter$ItemDisabledController;Lcom/squareup/util/Res;Lcom/squareup/catalogapi/CatalogIntegrationController;)V

    iput-object p1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->adapter:Lcom/squareup/librarylist/CheckoutLibraryListAdapter;

    return-void
.end method

.method protected onExitScope()V
    .locals 0

    .line 215
    invoke-direct {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->cancelQueryCallback()V

    .line 216
    invoke-direct {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->closeCursor()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 183
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 185
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/librarylist/LibraryListView;

    iget-object v1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->adapter:Lcom/squareup/librarylist/CheckoutLibraryListAdapter;

    invoke-virtual {v0, v1}, Lcom/squareup/librarylist/LibraryListView;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 186
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/squareup/librarylist/-$$Lambda$SimpleLibraryListPresenter$6kVfWC6Hed6kAamzQbxvZbJz3zs;

    invoke-direct {v1, p0}, Lcom/squareup/librarylist/-$$Lambda$SimpleLibraryListPresenter$6kVfWC6Hed6kAamzQbxvZbJz3zs;-><init>(Lcom/squareup/librarylist/SimpleLibraryListPresenter;)V

    invoke-static {v0, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    const-string v0, ""

    if-nez p1, :cond_0

    .line 202
    invoke-virtual {p0, v0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->newLibrarySearch(Ljava/lang/String;)V

    goto :goto_0

    .line 203
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->hasCursor()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 205
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->updateList()V

    .line 206
    invoke-direct {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->showLibrary()V

    goto :goto_0

    .line 207
    :cond_1
    invoke-direct {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->hasPendingQuery()Z

    move-result p1

    if-nez p1, :cond_2

    .line 210
    invoke-virtual {p0, v0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->newLibrarySearch(Ljava/lang/String;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public setCategoryQueryResults(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Ljava/lang/String;Z)V
    .locals 0

    .line 302
    iput-object p2, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->currentCursorQueryId:Ljava/lang/String;

    .line 303
    iput-object p1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    .line 304
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->isLibraryEmpty:Ljava/lang/Boolean;

    .line 305
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->updateList()V

    .line 306
    invoke-direct {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->showLibrary()V

    return-void
.end method

.method public setLibraryQueryResults(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Ljava/lang/String;)V
    .locals 0

    .line 292
    iput-object p1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    const/4 p1, 0x0

    .line 293
    iput-object p1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->isLibraryEmpty:Ljava/lang/Boolean;

    .line 294
    iput-object p2, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->currentCursorQueryId:Ljava/lang/String;

    .line 295
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->updateList()V

    .line 296
    invoke-direct {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->showLibrary()V

    return-void
.end method

.method public setListAdapter(Lcom/squareup/librarylist/CheckoutLibraryListAdapter;)V
    .locals 0

    .line 311
    iput-object p1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->adapter:Lcom/squareup/librarylist/CheckoutLibraryListAdapter;

    return-void
.end method

.method public bridge synthetic takeView(Lcom/squareup/librarylist/LibraryListView;)V
    .locals 0

    .line 82
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public updateList()V
    .locals 5

    .line 238
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 239
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 242
    iget-object v2, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->isLibraryEmpty:Ljava/lang/Boolean;

    if-nez v2, :cond_2

    .line 243
    invoke-direct {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->hasSearchText()Z

    move-result v2

    .line 244
    iget-object v3, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->libraryState:Lcom/squareup/librarylist/LibraryListStateManager;

    invoke-interface {v3}, Lcom/squareup/librarylist/LibraryListStateManager;->getLibraryFilter()Lcom/squareup/librarylist/LibraryListState$Filter;

    move-result-object v3

    sget-object v4, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_ITEMS:Lcom/squareup/librarylist/LibraryListState$Filter;

    if-ne v3, v4, :cond_0

    if-nez v2, :cond_0

    .line 245
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->libraryState:Lcom/squareup/librarylist/LibraryListStateManager;

    .line 246
    invoke-direct {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->shouldHideGiftCardsAndRewards()Z

    move-result v3

    invoke-interface {v0, v3}, Lcom/squareup/librarylist/LibraryListStateManager;->buildAllItemsPlaceholders(Z)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 247
    :cond_0
    iget-object v3, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->libraryState:Lcom/squareup/librarylist/LibraryListStateManager;

    invoke-interface {v3}, Lcom/squareup/librarylist/LibraryListStateManager;->getLibraryFilter()Lcom/squareup/librarylist/LibraryListState$Filter;

    move-result-object v3

    sget-object v4, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_GIFT_CARDS:Lcom/squareup/librarylist/LibraryListState$Filter;

    if-ne v3, v4, :cond_1

    if-nez v2, :cond_1

    .line 250
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->close()V

    .line 251
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->libraryState:Lcom/squareup/librarylist/LibraryListStateManager;

    .line 252
    invoke-direct {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->shouldHideGiftCardsAndRewards()Z

    move-result v3

    invoke-interface {v0, v3}, Lcom/squareup/librarylist/LibraryListStateManager;->buildGiftCardPlaceholder(Z)Ljava/util/List;

    move-result-object v0

    :cond_1
    :goto_0
    if-nez v2, :cond_5

    .line 254
    invoke-direct {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->getSafeCursorCount()I

    move-result v2

    const/4 v3, 0x2

    if-gt v2, v3, :cond_5

    .line 257
    iget-object v1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->configuration:Lcom/squareup/librarylist/LibraryListConfiguration;

    invoke-interface {v1}, Lcom/squareup/librarylist/LibraryListConfiguration;->getTopLevelItemSuggestions()Ljava/util/List;

    move-result-object v1

    goto :goto_3

    .line 260
    :cond_2
    iget-object v2, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->configuration:Lcom/squareup/librarylist/LibraryListConfiguration;

    invoke-interface {v2}, Lcom/squareup/librarylist/LibraryListConfiguration;->getCanAlwaysShowCategoryPlaceholders()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->isLibraryEmpty:Ljava/lang/Boolean;

    .line 261
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v2}, Lcom/squareup/util/Device;->isLandscapeLongTablet()Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    :cond_4
    :goto_1
    const/4 v2, 0x1

    :goto_2
    if-eqz v2, :cond_5

    .line 263
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->libraryState:Lcom/squareup/librarylist/LibraryListStateManager;

    invoke-interface {v0}, Lcom/squareup/librarylist/LibraryListStateManager;->buildCategoryPlaceholders()Ljava/util/List;

    move-result-object v0

    .line 267
    :cond_5
    :goto_3
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->hasView()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 269
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/librarylist/LibraryListView;

    iget-object v3, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->adapter:Lcom/squareup/librarylist/CheckoutLibraryListAdapter;

    invoke-virtual {v2, v3}, Lcom/squareup/librarylist/LibraryListView;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 272
    :cond_6
    iget-object v2, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->ORDER_ENTRY_LIBRARY_CREATE_NEW_ITEM:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    .line 273
    iget-object v3, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->adapter:Lcom/squareup/librarylist/CheckoutLibraryListAdapter;

    iget-object v4, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v3, v4, v0, v1, v2}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->update(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Ljava/util/List;Ljava/util/List;Z)V

    return-void
.end method
