.class public Lcom/squareup/librarylist/LibraryListView;
.super Landroid/widget/LinearLayout;
.source "LibraryListView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/librarylist/LibraryListView$Action;,
        Lcom/squareup/librarylist/LibraryListView$Component;,
        Lcom/squareup/librarylist/LibraryListView$ListPosition;,
        Lcom/squareup/librarylist/LibraryListView$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000~\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0007\u0008\u0016\u0018\u0000 =2\u00020\u0001:\u0004<=>?B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010$\u001a\u00020%H\u0016J\u0006\u0010&\u001a\u00020%J\u0008\u0010\'\u001a\u00020%H\u0014J\u0008\u0010(\u001a\u00020%H\u0014J\u0008\u0010)\u001a\u00020%H\u0014J\u0010\u0010*\u001a\u00020%2\u0006\u0010+\u001a\u00020,H\u0016J\n\u0010-\u001a\u0004\u0018\u00010,H\u0016J\u0008\u0010.\u001a\u00020%H\u0016J\u0012\u0010/\u001a\u00020%2\u0008\u00100\u001a\u0004\u0018\u000101H\u0016J\u0018\u00102\u001a\u00020%2\u0006\u00103\u001a\u0002042\u0006\u00105\u001a\u000206H\u0016J*\u00102\u001a\u00020%2\u0006\u00103\u001a\u0002042\u0006\u00105\u001a\u0002062\u0008\u00107\u001a\u0004\u0018\u0001062\u0006\u00108\u001a\u000209H\u0016J\u0008\u0010:\u001a\u00020%H\u0016J\u0008\u0010;\u001a\u00020%H\u0016R\u001a\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR2\u0010\u000c\u001a&\u0012\u000c\u0012\n \u000e*\u0004\u0018\u00010\t0\t \u000e*\u0012\u0012\u000c\u0012\n \u000e*\u0004\u0018\u00010\t0\t\u0018\u00010\r0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u00020\u00108BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0011\u0010\u0012R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0015\u001a\u00020\u0016X\u0084.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018\"\u0004\u0008\u0019\u0010\u001aR\u000e\u0010\u001b\u001a\u00020\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082.\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u001e\u001a\u00020\u001f8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008 \u0010!\"\u0004\u0008\"\u0010#\u00a8\u0006@"
    }
    d2 = {
        "Lcom/squareup/librarylist/LibraryListView;",
        "Landroid/widget/LinearLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "action",
        "Lrx/Observable;",
        "Lcom/squareup/librarylist/LibraryListView$Action;",
        "getAction",
        "()Lrx/Observable;",
        "actionSubject",
        "Lrx/subjects/PublishSubject;",
        "kotlin.jvm.PlatformType",
        "currentListPosition",
        "Lcom/squareup/librarylist/LibraryListView$ListPosition;",
        "getCurrentListPosition",
        "()Lcom/squareup/librarylist/LibraryListView$ListPosition;",
        "emptyNoteGlyphMessage",
        "Lcom/squareup/marin/widgets/MarinGlyphMessage;",
        "libraryList",
        "Landroid/widget/ListView;",
        "getLibraryList",
        "()Landroid/widget/ListView;",
        "setLibraryList",
        "(Landroid/widget/ListView;)V",
        "listPosition",
        "noSearchResults",
        "Landroid/view/View;",
        "presenter",
        "Lcom/squareup/librarylist/LibraryListPresenter;",
        "getPresenter",
        "()Lcom/squareup/librarylist/LibraryListPresenter;",
        "setPresenter",
        "(Lcom/squareup/librarylist/LibraryListPresenter;)V",
        "cacheListPosition",
        "",
        "hideKeyboard",
        "onAttachedToWindow",
        "onDetachedFromWindow",
        "onFinishInflate",
        "onRestoreInstanceState",
        "bundle",
        "Landroid/os/Parcelable;",
        "onSaveInstanceState",
        "resetListPosition",
        "setListAdapter",
        "adapter",
        "Landroid/widget/ListAdapter;",
        "showEmptyLibraryView",
        "glyph",
        "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
        "title",
        "",
        "message",
        "showEditItemsButton",
        "",
        "showLibrary",
        "showNoResults",
        "Action",
        "Companion",
        "Component",
        "ListPosition",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/librarylist/LibraryListView$Companion;

.field private static final KEY_LIST_OFFSET:Ljava/lang/String; = "offset"

.field private static final KEY_LIST_POSITION:Ljava/lang/String; = "position"


# instance fields
.field private final action:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/squareup/librarylist/LibraryListView$Action;",
            ">;"
        }
    .end annotation
.end field

.field private final actionSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/squareup/librarylist/LibraryListView$Action;",
            ">;"
        }
    .end annotation
.end field

.field private emptyNoteGlyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

.field protected libraryList:Landroid/widget/ListView;

.field private listPosition:Lcom/squareup/librarylist/LibraryListView$ListPosition;

.field private noSearchResults:Landroid/view/View;

.field public presenter:Lcom/squareup/librarylist/LibraryListPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/librarylist/LibraryListView$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/librarylist/LibraryListView$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/librarylist/LibraryListView;->Companion:Lcom/squareup/librarylist/LibraryListView$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    invoke-static {}, Lrx/subjects/PublishSubject;->create()Lrx/subjects/PublishSubject;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/librarylist/LibraryListView;->actionSubject:Lrx/subjects/PublishSubject;

    .line 49
    iget-object p2, p0, Lcom/squareup/librarylist/LibraryListView;->actionSubject:Lrx/subjects/PublishSubject;

    const-string v0, "actionSubject"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lrx/Observable;

    iput-object p2, p0, Lcom/squareup/librarylist/LibraryListView;->action:Lrx/Observable;

    .line 55
    sget-object p2, Lcom/squareup/librarylist/LibraryListView$ListPosition;->Companion:Lcom/squareup/librarylist/LibraryListView$ListPosition$Companion;

    invoke-virtual {p2}, Lcom/squareup/librarylist/LibraryListView$ListPosition$Companion;->getTOP()Lcom/squareup/librarylist/LibraryListView$ListPosition;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/librarylist/LibraryListView;->listPosition:Lcom/squareup/librarylist/LibraryListView$ListPosition;

    .line 69
    const-class p2, Lcom/squareup/librarylist/LibraryListView$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/librarylist/LibraryListView$Component;

    .line 70
    invoke-interface {p1, p0}, Lcom/squareup/librarylist/LibraryListView$Component;->inject(Lcom/squareup/librarylist/LibraryListView;)V

    return-void
.end method

.method public static final synthetic access$getActionSubject$p(Lcom/squareup/librarylist/LibraryListView;)Lrx/subjects/PublishSubject;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/librarylist/LibraryListView;->actionSubject:Lrx/subjects/PublishSubject;

    return-object p0
.end method

.method private final getCurrentListPosition()Lcom/squareup/librarylist/LibraryListView$ListPosition;
    .locals 3

    .line 58
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListView;->libraryList:Landroid/widget/ListView;

    const-string v1, "libraryList"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    .line 59
    iget-object v2, p0, Lcom/squareup/librarylist/LibraryListView;->libraryList:Landroid/widget/ListView;

    if-nez v2, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 60
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v1

    .line 61
    :cond_2
    new-instance v2, Lcom/squareup/librarylist/LibraryListView$ListPosition;

    invoke-direct {v2, v0, v1}, Lcom/squareup/librarylist/LibraryListView$ListPosition;-><init>(II)V

    return-object v2
.end method


# virtual methods
.method public cacheListPosition()V
    .locals 1

    .line 171
    invoke-direct {p0}, Lcom/squareup/librarylist/LibraryListView;->getCurrentListPosition()Lcom/squareup/librarylist/LibraryListView$ListPosition;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/librarylist/LibraryListView;->listPosition:Lcom/squareup/librarylist/LibraryListView$ListPosition;

    return-void
.end method

.method public getAction()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/librarylist/LibraryListView$Action;",
            ">;"
        }
    .end annotation

    .line 49
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListView;->action:Lrx/Observable;

    return-object v0
.end method

.method protected final getLibraryList()Landroid/widget/ListView;
    .locals 2

    .line 51
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListView;->libraryList:Landroid/widget/ListView;

    if-nez v0, :cond_0

    const-string v1, "libraryList"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getPresenter()Lcom/squareup/librarylist/LibraryListPresenter;
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListView;->presenter:Lcom/squareup/librarylist/LibraryListPresenter;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final hideKeyboard()V
    .locals 1

    .line 163
    move-object v0, p0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .line 97
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 98
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListView;->presenter:Lcom/squareup/librarylist/LibraryListPresenter;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0, p0}, Lcom/squareup/librarylist/LibraryListPresenter;->takeView(Lcom/squareup/librarylist/LibraryListView;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 102
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListView;->presenter:Lcom/squareup/librarylist/LibraryListPresenter;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0, p0}, Lcom/squareup/librarylist/LibraryListPresenter;->dropView(Lcom/squareup/librarylist/LibraryListView;)V

    .line 103
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 74
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 76
    move-object v0, p0

    check-cast v0, Landroid/view/View;

    sget v1, Lcom/squareup/librarylist/R$id;->library_list_view:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/squareup/librarylist/LibraryListView;->libraryList:Landroid/widget/ListView;

    .line 77
    sget v1, Lcom/squareup/librarylist/R$id;->no_search_results:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/librarylist/LibraryListView;->noSearchResults:Landroid/view/View;

    .line 78
    sget v1, Lcom/squareup/librarylist/R$id;->library_empty_note_glyph_message:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iput-object v0, p0, Lcom/squareup/librarylist/LibraryListView;->emptyNoteGlyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    .line 81
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListView;->libraryList:Landroid/widget/ListView;

    const-string v1, "libraryList"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v2, Lcom/squareup/librarylist/LibraryListView$onFinishInflate$1;

    invoke-direct {v2, p0}, Lcom/squareup/librarylist/LibraryListView$onFinishInflate$1;-><init>(Lcom/squareup/librarylist/LibraryListView;)V

    check-cast v2, Landroid/widget/AdapterView$OnItemClickListener;

    invoke-static {v2}, Lcom/squareup/debounce/Debouncers;->debounceItemClick(Landroid/widget/AdapterView$OnItemClickListener;)Lcom/squareup/debounce/DebouncedOnItemClickListener;

    move-result-object v2

    check-cast v2, Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 83
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListView;->libraryList:Landroid/widget/ListView;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v1, Lcom/squareup/librarylist/LibraryListView$onFinishInflate$2;

    invoke-direct {v1, p0}, Lcom/squareup/librarylist/LibraryListView$onFinishInflate$2;-><init>(Lcom/squareup/librarylist/LibraryListView;)V

    check-cast v1, Landroid/widget/AdapterView$OnItemLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 88
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListView;->emptyNoteGlyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    const-string v1, "emptyNoteGlyphMessage"

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    sget v2, Lcom/squareup/librarylist/R$string;->library_go_to_items_applet:I

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setButtonText(I)V

    .line 89
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListView;->emptyNoteGlyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    new-instance v1, Lcom/squareup/librarylist/LibraryListView$onFinishInflate$3;

    invoke-direct {v1, p0}, Lcom/squareup/librarylist/LibraryListView$onFinishInflate$3;-><init>(Lcom/squareup/librarylist/LibraryListView;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3

    const-string v0, "bundle"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    instance-of v0, p1, Lcom/squareup/mortar/BundleSavedState;

    if-eqz v0, :cond_0

    .line 183
    check-cast p1, Lcom/squareup/mortar/BundleSavedState;

    iget-object v0, p1, Lcom/squareup/mortar/BundleSavedState;->bundle:Landroid/os/Bundle;

    const-string v1, "position"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 184
    iget-object v1, p1, Lcom/squareup/mortar/BundleSavedState;->bundle:Landroid/os/Bundle;

    const-string v2, "offset"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 185
    new-instance v2, Lcom/squareup/librarylist/LibraryListView$ListPosition;

    invoke-direct {v2, v0, v1}, Lcom/squareup/librarylist/LibraryListView$ListPosition;-><init>(II)V

    iput-object v2, p0, Lcom/squareup/librarylist/LibraryListView;->listPosition:Lcom/squareup/librarylist/LibraryListView$ListPosition;

    .line 186
    invoke-virtual {p1}, Lcom/squareup/mortar/BundleSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object p1

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0

    .line 188
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :goto_0
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .line 175
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 176
    iget-object v1, p0, Lcom/squareup/librarylist/LibraryListView;->listPosition:Lcom/squareup/librarylist/LibraryListView$ListPosition;

    invoke-virtual {v1}, Lcom/squareup/librarylist/LibraryListView$ListPosition;->getIndex()I

    move-result v1

    const-string v2, "position"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 177
    iget-object v1, p0, Lcom/squareup/librarylist/LibraryListView;->listPosition:Lcom/squareup/librarylist/LibraryListView$ListPosition;

    invoke-virtual {v1}, Lcom/squareup/librarylist/LibraryListView$ListPosition;->getOffset()I

    move-result v1

    const-string v2, "offset"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 178
    new-instance v1, Lcom/squareup/mortar/BundleSavedState;

    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/squareup/mortar/BundleSavedState;-><init>(Landroid/os/Parcelable;Landroid/os/Bundle;)V

    check-cast v1, Landroid/os/Parcelable;

    return-object v1
.end method

.method public resetListPosition()V
    .locals 1

    .line 167
    sget-object v0, Lcom/squareup/librarylist/LibraryListView$ListPosition;->Companion:Lcom/squareup/librarylist/LibraryListView$ListPosition$Companion;

    invoke-virtual {v0}, Lcom/squareup/librarylist/LibraryListView$ListPosition$Companion;->getTOP()Lcom/squareup/librarylist/LibraryListView$ListPosition;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/librarylist/LibraryListView;->listPosition:Lcom/squareup/librarylist/LibraryListView$ListPosition;

    return-void
.end method

.method protected final setLibraryList(Landroid/widget/ListView;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    iput-object p1, p0, Lcom/squareup/librarylist/LibraryListView;->libraryList:Landroid/widget/ListView;

    return-void
.end method

.method public setListAdapter(Landroid/widget/ListAdapter;)V
    .locals 2

    .line 107
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListView;->libraryList:Landroid/widget/ListView;

    const-string v1, "libraryList"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eq v0, p1, :cond_2

    .line 108
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListView;->libraryList:Landroid/widget/ListView;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    :cond_2
    return-void
.end method

.method public final setPresenter(Lcom/squareup/librarylist/LibraryListPresenter;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    iput-object p1, p0, Lcom/squareup/librarylist/LibraryListView;->presenter:Lcom/squareup/librarylist/LibraryListPresenter;

    return-void
.end method

.method public showEmptyLibraryView(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;)V
    .locals 2

    const-string v0, "glyph"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "title"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 135
    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/squareup/librarylist/LibraryListView;->showEmptyLibraryView(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method public showEmptyLibraryView(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    const-string v0, "glyph"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "title"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListView;->emptyNoteGlyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    const-string v1, "emptyNoteGlyphMessage"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->hideButton()V

    .line 146
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListView;->emptyNoteGlyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->hideMessage()V

    .line 148
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListView;->emptyNoteGlyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 149
    iget-object p1, p0, Lcom/squareup/librarylist/LibraryListView;->emptyNoteGlyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    if-nez p1, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setTitle(Ljava/lang/CharSequence;)V

    if-eqz p3, :cond_5

    .line 151
    iget-object p1, p0, Lcom/squareup/librarylist/LibraryListView;->emptyNoteGlyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    if-nez p1, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast p3, Ljava/lang/CharSequence;

    invoke-virtual {p1, p3}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setMessage(Ljava/lang/CharSequence;)V

    :cond_5
    if-eqz p4, :cond_7

    .line 154
    iget-object p1, p0, Lcom/squareup/librarylist/LibraryListView;->emptyNoteGlyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    if-nez p1, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->showButton()V

    .line 157
    :cond_7
    iget-object p1, p0, Lcom/squareup/librarylist/LibraryListView;->noSearchResults:Landroid/view/View;

    if-nez p1, :cond_8

    const-string p2, "noSearchResults"

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 158
    iget-object p1, p0, Lcom/squareup/librarylist/LibraryListView;->emptyNoteGlyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    if-nez p1, :cond_9

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    const/4 p3, 0x0

    invoke-virtual {p1, p3}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setVisibility(I)V

    .line 159
    iget-object p1, p0, Lcom/squareup/librarylist/LibraryListView;->libraryList:Landroid/widget/ListView;

    if-nez p1, :cond_a

    const-string p3, "libraryList"

    invoke-static {p3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {p1, p2}, Landroid/widget/ListView;->setVisibility(I)V

    return-void
.end method

.method public showLibrary()V
    .locals 4

    .line 113
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListView;->noSearchResults:Landroid/view/View;

    if-nez v0, :cond_0

    const-string v1, "noSearchResults"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 114
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListView;->emptyNoteGlyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    if-nez v0, :cond_1

    const-string v2, "emptyNoteGlyphMessage"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setVisibility(I)V

    .line 115
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListView;->libraryList:Landroid/widget/ListView;

    const-string v1, "libraryList"

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 116
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListView;->libraryList:Landroid/widget/ListView;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    iget-object v2, p0, Lcom/squareup/librarylist/LibraryListView;->listPosition:Lcom/squareup/librarylist/LibraryListView$ListPosition;

    invoke-virtual {v2}, Lcom/squareup/librarylist/LibraryListView$ListPosition;->getIndex()I

    move-result v2

    iget-object v3, p0, Lcom/squareup/librarylist/LibraryListView;->listPosition:Lcom/squareup/librarylist/LibraryListView$ListPosition;

    invoke-virtual {v3}, Lcom/squareup/librarylist/LibraryListView$ListPosition;->getOffset()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 120
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListView;->libraryList:Landroid/widget/ListView;

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v0}, Landroid/widget/ListView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 121
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListView;->libraryList:Landroid/widget/ListView;

    if-nez v0, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v0}, Landroid/widget/ListView;->clearFocus()V

    :cond_6
    return-void
.end method

.method public showNoResults()V
    .locals 3

    .line 126
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListView;->noSearchResults:Landroid/view/View;

    if-nez v0, :cond_0

    const-string v1, "noSearchResults"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 127
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListView;->emptyNoteGlyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    if-nez v0, :cond_1

    const-string v1, "emptyNoteGlyphMessage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setVisibility(I)V

    .line 128
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListView;->libraryList:Landroid/widget/ListView;

    if-nez v0, :cond_2

    const-string v2, "libraryList"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    return-void
.end method
