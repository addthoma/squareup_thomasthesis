.class public final Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;
.super Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction;
.source "SimpleEntryHandler.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Discount"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\r\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00072\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;",
        "Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction;",
        "sourceView",
        "Landroid/view/View;",
        "libraryEntry",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
        "useDiscountDrawable",
        "",
        "(Landroid/view/View;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Z)V",
        "getLibraryEntry",
        "()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
        "getSourceView",
        "()Landroid/view/View;",
        "getUseDiscountDrawable",
        "()Z",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final libraryEntry:Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

.field private final sourceView:Landroid/view/View;

.field private final useDiscountDrawable:Z


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Z)V
    .locals 1

    const-string v0, "sourceView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "libraryEntry"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 22
    invoke-direct {p0, v0}, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;->sourceView:Landroid/view/View;

    iput-object p2, p0, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;->libraryEntry:Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    iput-boolean p3, p0, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;->useDiscountDrawable:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;Landroid/view/View;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;ZILjava/lang/Object;)Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;->sourceView:Landroid/view/View;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;->libraryEntry:Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-boolean p3, p0, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;->useDiscountDrawable:Z

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;->copy(Landroid/view/View;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Z)Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;->sourceView:Landroid/view/View;

    return-object v0
.end method

.method public final component2()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;
    .locals 1

    iget-object v0, p0, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;->libraryEntry:Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;->useDiscountDrawable:Z

    return v0
.end method

.method public final copy(Landroid/view/View;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Z)Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;
    .locals 1

    const-string v0, "sourceView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "libraryEntry"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;-><init>(Landroid/view/View;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;

    iget-object v0, p0, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;->sourceView:Landroid/view/View;

    iget-object v1, p1, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;->sourceView:Landroid/view/View;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;->libraryEntry:Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    iget-object v1, p1, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;->libraryEntry:Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;->useDiscountDrawable:Z

    iget-boolean p1, p1, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;->useDiscountDrawable:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;->libraryEntry:Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    return-object v0
.end method

.method public final getSourceView()Landroid/view/View;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;->sourceView:Landroid/view/View;

    return-object v0
.end method

.method public final getUseDiscountDrawable()Z
    .locals 1

    .line 21
    iget-boolean v0, p0, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;->useDiscountDrawable:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;->sourceView:Landroid/view/View;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;->libraryEntry:Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;->useDiscountDrawable:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Discount(sourceView="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;->sourceView:Landroid/view/View;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", libraryEntry="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;->libraryEntry:Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", useDiscountDrawable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/librarylist/SimpleEntryHandler$LibraryEntryAction$Discount;->useDiscountDrawable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
