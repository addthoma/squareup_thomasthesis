.class final Lcom/squareup/librarylist/RealLibraryListSearcher$performTopLevelSearch$1;
.super Ljava/lang/Object;
.source "LibraryListSearcher.kt"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/librarylist/RealLibraryListSearcher;->performTopLevelSearch(Lcom/squareup/librarylist/LibraryListState;Ljava/util/List;)Lrx/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/CatalogTask<",
        "Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u000e\u0010\u0003\u001a\n \u0002*\u0004\u0018\u00010\u00040\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;",
        "kotlin.jvm.PlatformType",
        "catalog",
        "Lcom/squareup/shared/catalog/Catalog$Local;",
        "perform"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $topLevelPlaceholders:Ljava/util/List;

.field final synthetic this$0:Lcom/squareup/librarylist/RealLibraryListSearcher;


# direct methods
.method constructor <init>(Lcom/squareup/librarylist/RealLibraryListSearcher;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/librarylist/RealLibraryListSearcher$performTopLevelSearch$1;->this$0:Lcom/squareup/librarylist/RealLibraryListSearcher;

    iput-object p2, p0, Lcom/squareup/librarylist/RealLibraryListSearcher$performTopLevelSearch$1;->$topLevelPlaceholders:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final perform(Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;
    .locals 5

    .line 165
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListSearcher$performTopLevelSearch$1;->this$0:Lcom/squareup/librarylist/RealLibraryListSearcher;

    iget-object v1, p0, Lcom/squareup/librarylist/RealLibraryListSearcher$performTopLevelSearch$1;->$topLevelPlaceholders:Ljava/util/List;

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/librarylist/RealLibraryListSearcher;->typesFor$default(Lcom/squareup/librarylist/RealLibraryListSearcher;Ljava/util/List;ZILjava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 167
    const-class v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    invoke-interface {p1, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->getSyntheticTableReader(Ljava/lang/Class;)Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 168
    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readAllUsedCategoriesAndEmpty(Ljava/util/List;)Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Object;
    .locals 0

    .line 69
    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/RealLibraryListSearcher$performTopLevelSearch$1;->perform(Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;

    move-result-object p1

    return-object p1
.end method
