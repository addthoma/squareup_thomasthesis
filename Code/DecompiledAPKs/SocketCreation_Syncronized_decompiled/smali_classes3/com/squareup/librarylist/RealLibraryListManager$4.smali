.class final Lcom/squareup/librarylist/RealLibraryListManager$4;
.super Ljava/lang/Object;
.source "LibraryListManager.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/librarylist/RealLibraryListManager;-><init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Device;Lcom/squareup/librarylist/LibraryListConfiguration;Lcom/squareup/librarylist/LibraryListSearcher;Lrx/Scheduler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/librarylist/LibraryListResults;",
        "result",
        "Lcom/squareup/librarylist/CatalogQueryResult;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/librarylist/RealLibraryListManager;


# direct methods
.method constructor <init>(Lcom/squareup/librarylist/RealLibraryListManager;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/librarylist/RealLibraryListManager$4;->this$0:Lcom/squareup/librarylist/RealLibraryListManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/librarylist/CatalogQueryResult;)Lcom/squareup/librarylist/LibraryListResults;
    .locals 5

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    instance-of v0, p1, Lcom/squareup/librarylist/CatalogQueryResult$TopLevelResult;

    if-eqz v0, :cond_1

    .line 135
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListManager$4;->this$0:Lcom/squareup/librarylist/RealLibraryListManager;

    move-object v1, p1

    check-cast v1, Lcom/squareup/librarylist/CatalogQueryResult$TopLevelResult;

    invoke-virtual {v1}, Lcom/squareup/librarylist/CatalogQueryResult$TopLevelResult;->isLibraryEmpty()Z

    move-result v2

    invoke-static {v0, v2}, Lcom/squareup/librarylist/RealLibraryListManager;->access$setLibraryEmpty$p(Lcom/squareup/librarylist/RealLibraryListManager;Z)V

    .line 136
    invoke-virtual {v1}, Lcom/squareup/librarylist/CatalogQueryResult$TopLevelResult;->getCategoriesAndEmpty()Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;->categoryCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    .line 140
    iget-object v2, p0, Lcom/squareup/librarylist/RealLibraryListManager$4;->this$0:Lcom/squareup/librarylist/RealLibraryListManager;

    invoke-static {v2}, Lcom/squareup/librarylist/RealLibraryListManager;->access$getConfiguration$p(Lcom/squareup/librarylist/RealLibraryListManager;)Lcom/squareup/librarylist/LibraryListConfiguration;

    move-result-object v2

    invoke-interface {v2}, Lcom/squareup/librarylist/LibraryListConfiguration;->getTopLevelPlaceholders()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 141
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v2

    goto :goto_0

    .line 143
    :cond_0
    iget-object v2, p0, Lcom/squareup/librarylist/RealLibraryListManager$4;->this$0:Lcom/squareup/librarylist/RealLibraryListManager;

    invoke-static {v2}, Lcom/squareup/librarylist/RealLibraryListManager;->access$getConfiguration$p(Lcom/squareup/librarylist/RealLibraryListManager;)Lcom/squareup/librarylist/LibraryListConfiguration;

    move-result-object v2

    invoke-interface {v2}, Lcom/squareup/librarylist/LibraryListConfiguration;->getTopLevelPlaceholders()Ljava/util/List;

    move-result-object v2

    .line 145
    :goto_0
    new-instance v3, Lcom/squareup/librarylist/LibraryListResults;

    .line 146
    invoke-virtual {p1}, Lcom/squareup/librarylist/CatalogQueryResult;->getState()Lcom/squareup/librarylist/LibraryListState;

    move-result-object p1

    const-string v4, "cursor"

    .line 147
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 149
    invoke-virtual {v1}, Lcom/squareup/librarylist/CatalogQueryResult$TopLevelResult;->isLibraryEmpty()Z

    move-result v1

    .line 145
    invoke-direct {v3, p1, v0, v2, v1}, Lcom/squareup/librarylist/LibraryListResults;-><init>(Lcom/squareup/librarylist/LibraryListState;Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Ljava/util/List;Z)V

    goto :goto_1

    .line 152
    :cond_1
    instance-of v0, p1, Lcom/squareup/librarylist/CatalogQueryResult$FilterResult;

    if-eqz v0, :cond_2

    .line 153
    new-instance v3, Lcom/squareup/librarylist/LibraryListResults;

    .line 154
    invoke-virtual {p1}, Lcom/squareup/librarylist/CatalogQueryResult;->getState()Lcom/squareup/librarylist/LibraryListState;

    move-result-object v0

    .line 155
    check-cast p1, Lcom/squareup/librarylist/CatalogQueryResult$FilterResult;

    invoke-virtual {p1}, Lcom/squareup/librarylist/CatalogQueryResult$FilterResult;->getLibraryCursor()Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object p1

    .line 156
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 157
    iget-object v2, p0, Lcom/squareup/librarylist/RealLibraryListManager$4;->this$0:Lcom/squareup/librarylist/RealLibraryListManager;

    invoke-static {v2}, Lcom/squareup/librarylist/RealLibraryListManager;->access$isLibraryEmpty$p(Lcom/squareup/librarylist/RealLibraryListManager;)Z

    move-result v2

    .line 153
    invoke-direct {v3, v0, p1, v1, v2}, Lcom/squareup/librarylist/LibraryListResults;-><init>(Lcom/squareup/librarylist/LibraryListState;Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Ljava/util/List;Z)V

    goto :goto_1

    .line 160
    :cond_2
    instance-of v0, p1, Lcom/squareup/librarylist/CatalogQueryResult$SearchByNameResult;

    if-eqz v0, :cond_3

    .line 161
    new-instance v3, Lcom/squareup/librarylist/LibraryListResults;

    .line 162
    invoke-virtual {p1}, Lcom/squareup/librarylist/CatalogQueryResult;->getState()Lcom/squareup/librarylist/LibraryListState;

    move-result-object v0

    .line 163
    check-cast p1, Lcom/squareup/librarylist/CatalogQueryResult$SearchByNameResult;

    invoke-virtual {p1}, Lcom/squareup/librarylist/CatalogQueryResult$SearchByNameResult;->getLibraryCursor()Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object p1

    .line 164
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 165
    iget-object v2, p0, Lcom/squareup/librarylist/RealLibraryListManager$4;->this$0:Lcom/squareup/librarylist/RealLibraryListManager;

    invoke-static {v2}, Lcom/squareup/librarylist/RealLibraryListManager;->access$isLibraryEmpty$p(Lcom/squareup/librarylist/RealLibraryListManager;)Z

    move-result v2

    .line 161
    invoke-direct {v3, v0, p1, v1, v2}, Lcom/squareup/librarylist/LibraryListResults;-><init>(Lcom/squareup/librarylist/LibraryListState;Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Ljava/util/List;Z)V

    :goto_1
    return-object v3

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 83
    check-cast p1, Lcom/squareup/librarylist/CatalogQueryResult;

    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/RealLibraryListManager$4;->call(Lcom/squareup/librarylist/CatalogQueryResult;)Lcom/squareup/librarylist/LibraryListResults;

    move-result-object p1

    return-object p1
.end method
