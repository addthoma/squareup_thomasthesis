.class public final Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideSimpleLibraryListAssistantFactory;
.super Ljava/lang/Object;
.source "SimpleLibraryListModule_ProvideSimpleLibraryListAssistantFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/librarylist/SimpleLibraryListAssistant;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/librarylist/SimpleLibraryListModule;


# direct methods
.method public constructor <init>(Lcom/squareup/librarylist/SimpleLibraryListModule;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideSimpleLibraryListAssistantFactory;->module:Lcom/squareup/librarylist/SimpleLibraryListModule;

    return-void
.end method

.method public static create(Lcom/squareup/librarylist/SimpleLibraryListModule;)Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideSimpleLibraryListAssistantFactory;
    .locals 1

    .line 30
    new-instance v0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideSimpleLibraryListAssistantFactory;

    invoke-direct {v0, p0}, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideSimpleLibraryListAssistantFactory;-><init>(Lcom/squareup/librarylist/SimpleLibraryListModule;)V

    return-object v0
.end method

.method public static provideSimpleLibraryListAssistant(Lcom/squareup/librarylist/SimpleLibraryListModule;)Lcom/squareup/librarylist/SimpleLibraryListAssistant;
    .locals 1

    .line 35
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListModule;->provideSimpleLibraryListAssistant()Lcom/squareup/librarylist/SimpleLibraryListAssistant;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/librarylist/SimpleLibraryListAssistant;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/librarylist/SimpleLibraryListAssistant;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideSimpleLibraryListAssistantFactory;->module:Lcom/squareup/librarylist/SimpleLibraryListModule;

    invoke-static {v0}, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideSimpleLibraryListAssistantFactory;->provideSimpleLibraryListAssistant(Lcom/squareup/librarylist/SimpleLibraryListModule;)Lcom/squareup/librarylist/SimpleLibraryListAssistant;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideSimpleLibraryListAssistantFactory;->get()Lcom/squareup/librarylist/SimpleLibraryListAssistant;

    move-result-object v0

    return-object v0
.end method
