.class public final Lcom/squareup/librarylist/ItemSuggestionsManager;
.super Ljava/lang/Object;
.source "ItemSuggestionsManager.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/librarylist/ItemSuggestionsManager$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nItemSuggestionsManager.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ItemSuggestionsManager.kt\ncom/squareup/librarylist/ItemSuggestionsManager\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,123:1\n747#2:124\n769#2,2:125\n*E\n*S KotlinDebug\n*F\n+ 1 ItemSuggestionsManager.kt\ncom/squareup/librarylist/ItemSuggestionsManager\n*L\n67#1:124\n67#1,2:125\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0018\u0000 \u001a2\u00020\u0001:\u0001\u001aB=\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0014\u0008\u0001\u0010\u0006\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\t0\u00080\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u000e\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\tJ\u000e\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0013H\u0002J\u000e\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0013H\u0002J\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0013J\u000e\u0010\u0017\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\tJ\u0010\u0010\u0018\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\tH\u0002J\u0018\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0013*\u0008\u0012\u0004\u0012\u00020\u00140\u0013H\u0002R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0006\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\t0\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/librarylist/ItemSuggestionsManager;",
        "",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "userSettings",
        "Lcom/squareup/settings/server/UserSettingsProvider;",
        "savedItemSuggestionsPref",
        "Lcom/f2prateek/rx/preferences2/Preference;",
        "",
        "",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "bus",
        "Lcom/squareup/badbus/BadBus;",
        "(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/UserSettingsProvider;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/analytics/Analytics;Lcom/squareup/badbus/BadBus;)V",
        "deleteItemSuggestion",
        "",
        "suggestionName",
        "getFoodSuggestions",
        "",
        "Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;",
        "getRetailSuggestions",
        "getTopLevelItemSuggestions",
        "markItemSuggestionAsSaved",
        "putItemSuggestionInStore",
        "filterUnsavedSuggestions",
        "Companion",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/librarylist/ItemSuggestionsManager$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final FOOD_MCCS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final RETAIL_MCCS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final bus:Lcom/squareup/badbus/BadBus;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final savedItemSuggestionsPref:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final userSettings:Lcom/squareup/settings/server/UserSettingsProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    new-instance v0, Lcom/squareup/librarylist/ItemSuggestionsManager$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/librarylist/ItemSuggestionsManager$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/librarylist/ItemSuggestionsManager;->Companion:Lcom/squareup/librarylist/ItemSuggestionsManager$Companion;

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Integer;

    const/16 v1, 0x176f

    .line 119
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/16 v1, 0x163b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v3, 0x1

    aput-object v1, v0, v3

    const/16 v1, 0x1609

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v4, 0x2

    aput-object v1, v0, v4

    const/16 v1, 0x15eb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v5, 0x3

    aput-object v1, v0, v5

    const/16 v1, 0x1643

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v6, 0x4

    aput-object v1, v0, v6

    const/16 v1, 0x1517

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v7, 0x5

    aput-object v1, v0, v7

    const/16 v1, 0x1735

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v8, 0x6

    aput-object v1, v0, v8

    const/16 v1, 0x14be

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v8, 0x7

    aput-object v1, v0, v8

    const/16 v1, 0x1664

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v8, 0x8

    aput-object v1, v0, v8

    const/16 v1, 0x140b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v8, 0x9

    aput-object v1, v0, v8

    invoke-static {v0}, Lkotlin/collections/SetsKt;->setOf([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/squareup/librarylist/ItemSuggestionsManager;->RETAIL_MCCS:Ljava/util/Set;

    new-array v0, v7, [Ljava/lang/Integer;

    const/16 v1, 0x16b4

    .line 120
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    const/16 v1, 0x1523

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    const/16 v1, 0x157b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    const/16 v1, 0x16b6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    const/16 v1, 0x1556

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-static {v0}, Lkotlin/collections/SetsKt;->setOf([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/squareup/librarylist/ItemSuggestionsManager;->FOOD_MCCS:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/UserSettingsProvider;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/analytics/Analytics;Lcom/squareup/badbus/BadBus;)V
    .locals 1
    .param p3    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/librarylist/SavedItemSuggestions;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/settings/server/UserSettingsProvider;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/badbus/BadBus;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "userSettings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "savedItemSuggestionsPref"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bus"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/librarylist/ItemSuggestionsManager;->features:Lcom/squareup/settings/server/Features;

    iput-object p2, p0, Lcom/squareup/librarylist/ItemSuggestionsManager;->userSettings:Lcom/squareup/settings/server/UserSettingsProvider;

    iput-object p3, p0, Lcom/squareup/librarylist/ItemSuggestionsManager;->savedItemSuggestionsPref:Lcom/f2prateek/rx/preferences2/Preference;

    iput-object p4, p0, Lcom/squareup/librarylist/ItemSuggestionsManager;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p5, p0, Lcom/squareup/librarylist/ItemSuggestionsManager;->bus:Lcom/squareup/badbus/BadBus;

    return-void
.end method

.method public static final synthetic access$getFOOD_MCCS$cp()Ljava/util/Set;
    .locals 1

    .line 21
    sget-object v0, Lcom/squareup/librarylist/ItemSuggestionsManager;->FOOD_MCCS:Ljava/util/Set;

    return-object v0
.end method

.method public static final synthetic access$getRETAIL_MCCS$cp()Ljava/util/Set;
    .locals 1

    .line 21
    sget-object v0, Lcom/squareup/librarylist/ItemSuggestionsManager;->RETAIL_MCCS:Ljava/util/Set;

    return-object v0
.end method

.method private final filterUnsavedSuggestions(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;",
            ">;"
        }
    .end annotation

    .line 66
    iget-object v0, p0, Lcom/squareup/librarylist/ItemSuggestionsManager;->savedItemSuggestionsPref:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "savedItemSuggestionsPref.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/util/Set;

    .line 67
    check-cast p1, Ljava/lang/Iterable;

    .line 124
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 125
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;

    .line 68
    invoke-virtual {v3}, Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 126
    :cond_1
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method private final getFoodSuggestions()Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;

    .line 97
    new-instance v7, Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;

    .line 101
    iget-object v1, p0, Lcom/squareup/librarylist/ItemSuggestionsManager;->userSettings:Lcom/squareup/settings/server/UserSettingsProvider;

    invoke-interface {v1}, Lcom/squareup/settings/server/UserSettingsProvider;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v6

    const-string/jumbo v2, "\ud83d\udca7"

    const-string v3, "Water"

    const-wide/16 v4, 0x0

    move-object v1, v7

    .line 97
    invoke-direct/range {v1 .. v6}, Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;-><init>(Ljava/lang/String;Ljava/lang/String;JLcom/squareup/protos/common/CurrencyCode;)V

    const/4 v1, 0x0

    aput-object v7, v0, v1

    .line 103
    new-instance v1, Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;

    .line 107
    iget-object v2, p0, Lcom/squareup/librarylist/ItemSuggestionsManager;->userSettings:Lcom/squareup/settings/server/UserSettingsProvider;

    invoke-interface {v2}, Lcom/squareup/settings/server/UserSettingsProvider;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v13

    const-string/jumbo v9, "\ud83c\udf54"

    const-string v10, "Sandwich"

    const-wide/16 v11, 0x0

    move-object v8, v1

    .line 103
    invoke-direct/range {v8 .. v13}, Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;-><init>(Ljava/lang/String;Ljava/lang/String;JLcom/squareup/protos/common/CurrencyCode;)V

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 109
    new-instance v1, Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;

    .line 113
    iget-object v2, p0, Lcom/squareup/librarylist/ItemSuggestionsManager;->userSettings:Lcom/squareup/settings/server/UserSettingsProvider;

    invoke-interface {v2}, Lcom/squareup/settings/server/UserSettingsProvider;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v8

    const-string/jumbo v4, "\ufe0f\u2615"

    const-string v5, "Coffee"

    const-wide/16 v6, 0x0

    move-object v3, v1

    .line 109
    invoke-direct/range {v3 .. v8}, Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;-><init>(Ljava/lang/String;Ljava/lang/String;JLcom/squareup/protos/common/CurrencyCode;)V

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 96
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private final getRetailSuggestions()Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;

    .line 74
    new-instance v7, Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;

    .line 78
    iget-object v1, p0, Lcom/squareup/librarylist/ItemSuggestionsManager;->userSettings:Lcom/squareup/settings/server/UserSettingsProvider;

    invoke-interface {v1}, Lcom/squareup/settings/server/UserSettingsProvider;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v6

    const-string/jumbo v2, "\ud83d\udce6"

    const-string v3, "Shipping"

    const-wide/16 v4, 0x0

    move-object v1, v7

    .line 74
    invoke-direct/range {v1 .. v6}, Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;-><init>(Ljava/lang/String;Ljava/lang/String;JLcom/squareup/protos/common/CurrencyCode;)V

    const/4 v1, 0x0

    aput-object v7, v0, v1

    .line 80
    new-instance v1, Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;

    .line 84
    iget-object v2, p0, Lcom/squareup/librarylist/ItemSuggestionsManager;->userSettings:Lcom/squareup/settings/server/UserSettingsProvider;

    invoke-interface {v2}, Lcom/squareup/settings/server/UserSettingsProvider;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v13

    const-string/jumbo v9, "\ud83d\udc52"

    const-string v10, "Hat"

    const-wide/16 v11, 0x0

    move-object v8, v1

    .line 80
    invoke-direct/range {v8 .. v13}, Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;-><init>(Ljava/lang/String;Ljava/lang/String;JLcom/squareup/protos/common/CurrencyCode;)V

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 86
    new-instance v1, Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;

    .line 90
    iget-object v2, p0, Lcom/squareup/librarylist/ItemSuggestionsManager;->userSettings:Lcom/squareup/settings/server/UserSettingsProvider;

    invoke-interface {v2}, Lcom/squareup/settings/server/UserSettingsProvider;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v8

    const-string/jumbo v4, "\ufe0f\ud83d\udc5a"

    const-string v5, "T-Shirt"

    const-wide/16 v6, 0x0

    move-object v3, v1

    .line 86
    invoke-direct/range {v3 .. v8}, Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;-><init>(Ljava/lang/String;Ljava/lang/String;JLcom/squareup/protos/common/CurrencyCode;)V

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 73
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private final putItemSuggestionInStore(Ljava/lang/String;)V
    .locals 2

    .line 59
    iget-object v0, p0, Lcom/squareup/librarylist/ItemSuggestionsManager;->savedItemSuggestionsPref:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "savedItemSuggestionsPref.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 60
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toMutableSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    .line 61
    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 62
    iget-object p1, p0, Lcom/squareup/librarylist/ItemSuggestionsManager;->savedItemSuggestionsPref:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {p1, v0}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final deleteItemSuggestion(Ljava/lang/String;)V
    .locals 2

    const-string v0, "suggestionName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/librarylist/ItemSuggestionsManager;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ORDER_ENTRY_ITEM_SUGGESTION_DELETE:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 53
    invoke-direct {p0, p1}, Lcom/squareup/librarylist/ItemSuggestionsManager;->putItemSuggestionInStore(Ljava/lang/String;)V

    .line 54
    iget-object p1, p0, Lcom/squareup/librarylist/ItemSuggestionsManager;->bus:Lcom/squareup/badbus/BadBus;

    new-instance v0, Lcom/squareup/cogs/CatalogUpdateEvent;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/squareup/cogs/CatalogUpdateEvent;-><init>(Z)V

    invoke-virtual {p1, v0}, Lcom/squareup/badbus/BadBus;->post(Ljava/lang/Object;)V

    return-void
.end method

.method public final getTopLevelItemSuggestions()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;",
            ">;"
        }
    .end annotation

    .line 35
    iget-object v0, p0, Lcom/squareup/librarylist/ItemSuggestionsManager;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ORDER_ENTRY_LIBRARY_ITEM_SUGGESTIONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 36
    :cond_0
    sget-object v0, Lcom/squareup/librarylist/ItemSuggestionsManager;->RETAIL_MCCS:Ljava/util/Set;

    iget-object v1, p0, Lcom/squareup/librarylist/ItemSuggestionsManager;->userSettings:Lcom/squareup/settings/server/UserSettingsProvider;

    invoke-interface {v1}, Lcom/squareup/settings/server/UserSettingsProvider;->getMcc()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/squareup/librarylist/ItemSuggestionsManager;->getRetailSuggestions()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/librarylist/ItemSuggestionsManager;->filterUnsavedSuggestions(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 37
    :cond_1
    sget-object v0, Lcom/squareup/librarylist/ItemSuggestionsManager;->FOOD_MCCS:Ljava/util/Set;

    iget-object v1, p0, Lcom/squareup/librarylist/ItemSuggestionsManager;->userSettings:Lcom/squareup/settings/server/UserSettingsProvider;

    invoke-interface {v1}, Lcom/squareup/settings/server/UserSettingsProvider;->getMcc()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/squareup/librarylist/ItemSuggestionsManager;->getFoodSuggestions()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/librarylist/ItemSuggestionsManager;->filterUnsavedSuggestions(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 38
    :cond_2
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 40
    :goto_0
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_3

    .line 41
    iget-object v1, p0, Lcom/squareup/librarylist/ItemSuggestionsManager;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v2, Lcom/squareup/analytics/RegisterViewName;->ORDER_ENTRY_ITEM_SUGGESTIONS:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    :cond_3
    return-object v0
.end method

.method public final markItemSuggestionAsSaved(Ljava/lang/String;)V
    .locals 2

    const-string v0, "suggestionName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lcom/squareup/librarylist/ItemSuggestionsManager;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ORDER_ENTRY_ITEM_SUGGESTION_SAVE:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 48
    invoke-direct {p0, p1}, Lcom/squareup/librarylist/ItemSuggestionsManager;->putItemSuggestionInStore(Ljava/lang/String;)V

    return-void
.end method
