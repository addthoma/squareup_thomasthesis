.class public final enum Lcom/squareup/librarylist/LibraryListState$Filter;
.super Ljava/lang/Enum;
.source "LibraryListState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/librarylist/LibraryListState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Filter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/librarylist/LibraryListState$Filter;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0007\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000j\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000f\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/librarylist/LibraryListState$Filter;",
        "",
        "stringResId",
        "",
        "(Ljava/lang/String;II)V",
        "getStringResId",
        "featureController",
        "Lcom/squareup/catalogapi/CatalogIntegrationController;",
        "includeServicesWithItems",
        "",
        "ALL_CATEGORIES",
        "ALL_ITEMS",
        "ALL_SERVICES",
        "ALL_DISCOUNTS",
        "ALL_GIFT_CARDS",
        "SINGLE_CATEGORY",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/librarylist/LibraryListState$Filter;

.field public static final enum ALL_CATEGORIES:Lcom/squareup/librarylist/LibraryListState$Filter;

.field public static final enum ALL_DISCOUNTS:Lcom/squareup/librarylist/LibraryListState$Filter;

.field public static final enum ALL_GIFT_CARDS:Lcom/squareup/librarylist/LibraryListState$Filter;

.field public static final enum ALL_ITEMS:Lcom/squareup/librarylist/LibraryListState$Filter;

.field public static final enum ALL_SERVICES:Lcom/squareup/librarylist/LibraryListState$Filter;

.field public static final enum SINGLE_CATEGORY:Lcom/squareup/librarylist/LibraryListState$Filter;


# instance fields
.field private final stringResId:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/librarylist/LibraryListState$Filter;

    new-instance v1, Lcom/squareup/librarylist/LibraryListState$Filter;

    .line 20
    sget v2, Lcom/squareup/librarylist/R$string;->library:I

    const/4 v3, 0x0

    const-string v4, "ALL_CATEGORIES"

    invoke-direct {v1, v4, v3, v2}, Lcom/squareup/librarylist/LibraryListState$Filter;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_CATEGORIES:Lcom/squareup/librarylist/LibraryListState$Filter;

    aput-object v1, v0, v3

    new-instance v1, Lcom/squareup/librarylist/LibraryListState$Filter;

    .line 21
    sget v2, Lcom/squareup/librarylist/R$string;->item_library_all_items:I

    const/4 v4, 0x1

    const-string v5, "ALL_ITEMS"

    invoke-direct {v1, v5, v4, v2}, Lcom/squareup/librarylist/LibraryListState$Filter;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_ITEMS:Lcom/squareup/librarylist/LibraryListState$Filter;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/librarylist/LibraryListState$Filter;

    .line 22
    sget v2, Lcom/squareup/librarylist/R$string;->item_library_all_services:I

    const/4 v4, 0x2

    const-string v5, "ALL_SERVICES"

    invoke-direct {v1, v5, v4, v2}, Lcom/squareup/librarylist/LibraryListState$Filter;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_SERVICES:Lcom/squareup/librarylist/LibraryListState$Filter;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/librarylist/LibraryListState$Filter;

    .line 23
    sget v2, Lcom/squareup/librarylist/R$string;->item_library_all_discounts:I

    const/4 v4, 0x3

    const-string v5, "ALL_DISCOUNTS"

    invoke-direct {v1, v5, v4, v2}, Lcom/squareup/librarylist/LibraryListState$Filter;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_DISCOUNTS:Lcom/squareup/librarylist/LibraryListState$Filter;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/librarylist/LibraryListState$Filter;

    .line 24
    sget v2, Lcom/squareup/librarylist/R$string;->item_library_all_gift_cards:I

    const/4 v4, 0x4

    const-string v5, "ALL_GIFT_CARDS"

    invoke-direct {v1, v5, v4, v2}, Lcom/squareup/librarylist/LibraryListState$Filter;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_GIFT_CARDS:Lcom/squareup/librarylist/LibraryListState$Filter;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/librarylist/LibraryListState$Filter;

    const/4 v2, 0x5

    const-string v4, "SINGLE_CATEGORY"

    .line 25
    invoke-direct {v1, v4, v2, v3}, Lcom/squareup/librarylist/LibraryListState$Filter;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/librarylist/LibraryListState$Filter;->SINGLE_CATEGORY:Lcom/squareup/librarylist/LibraryListState$Filter;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/librarylist/LibraryListState$Filter;->$VALUES:[Lcom/squareup/librarylist/LibraryListState$Filter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/squareup/librarylist/LibraryListState$Filter;->stringResId:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/librarylist/LibraryListState$Filter;
    .locals 1

    const-class v0, Lcom/squareup/librarylist/LibraryListState$Filter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/librarylist/LibraryListState$Filter;

    return-object p0
.end method

.method public static values()[Lcom/squareup/librarylist/LibraryListState$Filter;
    .locals 1

    sget-object v0, Lcom/squareup/librarylist/LibraryListState$Filter;->$VALUES:[Lcom/squareup/librarylist/LibraryListState$Filter;

    invoke-virtual {v0}, [Lcom/squareup/librarylist/LibraryListState$Filter;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/librarylist/LibraryListState$Filter;

    return-object v0
.end method


# virtual methods
.method public final getStringResId(Lcom/squareup/catalogapi/CatalogIntegrationController;Z)I
    .locals 1

    const-string v0, "featureController"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget v0, p0, Lcom/squareup/librarylist/LibraryListState$Filter;->stringResId:I

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    .line 38
    invoke-interface {p1}, Lcom/squareup/catalogapi/CatalogIntegrationController;->shouldShowPartiallyRolledOutFeature()Z

    move-result p1

    if-eqz p1, :cond_0

    iget p1, p0, Lcom/squareup/librarylist/LibraryListState$Filter;->stringResId:I

    sget p2, Lcom/squareup/librarylist/R$string;->item_library_all_items:I

    if-ne p1, p2, :cond_0

    .line 41
    sget p1, Lcom/squareup/librarylist/R$string;->item_library_all_items_and_services:I

    return p1

    .line 45
    :cond_0
    iget p1, p0, Lcom/squareup/librarylist/LibraryListState$Filter;->stringResId:I

    return p1

    .line 32
    :cond_1
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    .line 33
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryListState$Filter;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " should not need to be displayed as a string."

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 32
    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
