.class final Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$14;
.super Ljava/lang/Object;
.source "LibraryListManager.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/librarylist/RealLibraryListManager;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012F\u0010\u0002\u001aB\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00010\u0001 \u0005* \u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00010\u0001\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/librarylist/LibraryListState;",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/librarylist/RealLibraryListManager$Action$ViewCategory;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$14;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$14;

    invoke-direct {v0}, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$14;-><init>()V

    sput-object v0, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$14;->INSTANCE:Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$14;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lkotlin/Pair;)Lcom/squareup/librarylist/LibraryListState;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lcom/squareup/librarylist/RealLibraryListManager$Action$ViewCategory;",
            "Lcom/squareup/librarylist/LibraryListState;",
            ">;)",
            "Lcom/squareup/librarylist/LibraryListState;"
        }
    .end annotation

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/librarylist/RealLibraryListManager$Action$ViewCategory;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    move-object v1, p1

    check-cast v1, Lcom/squareup/librarylist/LibraryListState;

    .line 254
    invoke-virtual {v0}, Lcom/squareup/librarylist/RealLibraryListManager$Action$ViewCategory;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object p1

    .line 256
    sget-object v2, Lcom/squareup/librarylist/LibraryListState$Filter;->SINGLE_CATEGORY:Lcom/squareup/librarylist/LibraryListState$Filter;

    .line 257
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v4

    .line 258
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getName()Ljava/lang/String;

    move-result-object v5

    .line 259
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getAbbreviation()Ljava/lang/String;

    move-result-object v6

    .line 260
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getColor()Ljava/lang/String;

    move-result-object v7

    const/4 v3, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x2

    const/4 v10, 0x0

    .line 255
    invoke-static/range {v1 .. v10}, Lcom/squareup/librarylist/LibraryListState;->copy$default(Lcom/squareup/librarylist/LibraryListState;Lcom/squareup/librarylist/LibraryListState$Filter;Lcom/squareup/librarylist/LibraryListState$SearchQuery;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Lcom/squareup/librarylist/LibraryListState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 83
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$14;->call(Lkotlin/Pair;)Lcom/squareup/librarylist/LibraryListState;

    move-result-object p1

    return-object p1
.end method
