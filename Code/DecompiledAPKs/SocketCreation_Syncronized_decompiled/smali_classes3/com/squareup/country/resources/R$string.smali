.class public final Lcom/squareup/country/resources/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/country/resources/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final activation_legal_documents_ca:I = 0x7f12004a

.field public static final activation_legal_documents_helper_prompt_ca:I = 0x7f12004b

.field public static final activation_legal_documents_helper_prompt_jp:I = 0x7f12004c

.field public static final activation_legal_documents_helper_prompt_other:I = 0x7f12004d

.field public static final activation_legal_documents_helper_prompt_us:I = 0x7f12004e

.field public static final activation_legal_documents_jp:I = 0x7f12004f

.field public static final activation_legal_documents_other:I = 0x7f120050

.field public static final activation_legal_documents_us:I = 0x7f120051

.field public static final apartment_suite_hint:I = 0x7f1200ce

.field public static final apartment_unit_hint:I = 0x7f1200cf

.field public static final bank_code:I = 0x7f12015b

.field public static final bic:I = 0x7f12016b

.field public static final branch_code:I = 0x7f120191

.field public static final bsb_number:I = 0x7f120194

.field public static final buyer_no_thanks:I = 0x7f120211

.field public static final buyer_receipt_no_receipt:I = 0x7f12024d

.field public static final checkout_flow_email_receipt_disclaimer_au_japan:I = 0x7f120404

.field public static final checkout_flow_email_receipt_disclaimer_canada:I = 0x7f120405

.field public static final checkout_flow_email_receipt_disclaimer_default:I = 0x7f120406

.field public static final checkout_flow_email_receipt_disclaimer_uk:I = 0x7f120407

.field public static final checkout_flow_email_receipt_disclaimer_us:I = 0x7f120408

.field public static final checkout_flow_receipt_disclaimer:I = 0x7f120409

.field public static final checkout_flow_sms_receipt_disclaimer_au_japan:I = 0x7f12040a

.field public static final checkout_flow_sms_receipt_disclaimer_canada:I = 0x7f12040b

.field public static final checkout_flow_sms_receipt_disclaimer_default:I = 0x7f12040c

.field public static final checkout_flow_sms_receipt_disclaimer_uk:I = 0x7f12040d

.field public static final checkout_flow_sms_receipt_disclaimer_us:I = 0x7f12040e

.field public static final country_albania:I = 0x7f1204e6

.field public static final country_algeria:I = 0x7f1204e7

.field public static final country_americansamoa:I = 0x7f1204e8

.field public static final country_angola:I = 0x7f1204e9

.field public static final country_anguilla:I = 0x7f1204ea

.field public static final country_antiguaandbarbuda:I = 0x7f1204eb

.field public static final country_argentina:I = 0x7f1204ec

.field public static final country_armenia:I = 0x7f1204ed

.field public static final country_aruba:I = 0x7f1204ee

.field public static final country_australia:I = 0x7f1204ef

.field public static final country_austria:I = 0x7f1204f0

.field public static final country_azerbaijan:I = 0x7f1204f1

.field public static final country_bahamas:I = 0x7f1204f2

.field public static final country_bahrain:I = 0x7f1204f3

.field public static final country_bangladesh:I = 0x7f1204f4

.field public static final country_barbados:I = 0x7f1204f5

.field public static final country_belarus:I = 0x7f1204f6

.field public static final country_belgium:I = 0x7f1204f7

.field public static final country_belize:I = 0x7f1204f8

.field public static final country_benin:I = 0x7f1204f9

.field public static final country_bermuda:I = 0x7f1204fa

.field public static final country_bhutan:I = 0x7f1204fb

.field public static final country_bolivia_plurinationalstateof:I = 0x7f1204fc

.field public static final country_bosniaandherzegovina:I = 0x7f1204fd

.field public static final country_botswana:I = 0x7f1204fe

.field public static final country_brazil:I = 0x7f1204ff

.field public static final country_bruneidarussalam:I = 0x7f120500

.field public static final country_bulgaria:I = 0x7f120501

.field public static final country_burkinafaso:I = 0x7f120502

.field public static final country_caboverde:I = 0x7f120503

.field public static final country_cambodia:I = 0x7f120504

.field public static final country_cameroon:I = 0x7f120505

.field public static final country_canada:I = 0x7f120506

.field public static final country_caymanislands:I = 0x7f120507

.field public static final country_chad:I = 0x7f120508

.field public static final country_chile:I = 0x7f120509

.field public static final country_china:I = 0x7f12050a

.field public static final country_colombia:I = 0x7f12050b

.field public static final country_congo:I = 0x7f12050c

.field public static final country_costarica:I = 0x7f12050d

.field public static final country_coted_ivoire:I = 0x7f12050e

.field public static final country_croatia:I = 0x7f12050f

.field public static final country_cyprus:I = 0x7f120510

.field public static final country_czechrepublic:I = 0x7f120511

.field public static final country_denmark:I = 0x7f120512

.field public static final country_dominica:I = 0x7f120513

.field public static final country_dominicanrepublic:I = 0x7f120514

.field public static final country_ecuador:I = 0x7f120515

.field public static final country_egypt:I = 0x7f120516

.field public static final country_elsalvador:I = 0x7f120517

.field public static final country_estonia:I = 0x7f120518

.field public static final country_fiji:I = 0x7f120519

.field public static final country_finland:I = 0x7f12051a

.field public static final country_france:I = 0x7f12051b

.field public static final country_gabon:I = 0x7f12051c

.field public static final country_gambia:I = 0x7f12051d

.field public static final country_germany:I = 0x7f12051e

.field public static final country_ghana:I = 0x7f12051f

.field public static final country_greece:I = 0x7f120520

.field public static final country_grenada:I = 0x7f120521

.field public static final country_guam:I = 0x7f120522

.field public static final country_guatemala:I = 0x7f120523

.field public static final country_guinea_bissau:I = 0x7f120524

.field public static final country_guyana:I = 0x7f120525

.field public static final country_haiti:I = 0x7f120526

.field public static final country_honduras:I = 0x7f120527

.field public static final country_hongkong:I = 0x7f120528

.field public static final country_hungary:I = 0x7f120529

.field public static final country_iceland:I = 0x7f12052a

.field public static final country_india:I = 0x7f12052b

.field public static final country_indonesia:I = 0x7f12052c

.field public static final country_ireland:I = 0x7f12052d

.field public static final country_israel:I = 0x7f12052e

.field public static final country_italy:I = 0x7f12052f

.field public static final country_jamaica:I = 0x7f120530

.field public static final country_japan:I = 0x7f120531

.field public static final country_jordan:I = 0x7f120532

.field public static final country_kazakhstan:I = 0x7f120533

.field public static final country_kenya:I = 0x7f120534

.field public static final country_korea_republicof:I = 0x7f120535

.field public static final country_kuwait:I = 0x7f120536

.field public static final country_kyrgyzstan:I = 0x7f120537

.field public static final country_laopeople_sdemocraticrepublic:I = 0x7f120538

.field public static final country_latvia:I = 0x7f120539

.field public static final country_lebanon:I = 0x7f12053a

.field public static final country_lesotho:I = 0x7f12053b

.field public static final country_liberia:I = 0x7f12053c

.field public static final country_libya:I = 0x7f12053d

.field public static final country_liechtenstein:I = 0x7f12053e

.field public static final country_lithuania:I = 0x7f12053f

.field public static final country_luxembourg:I = 0x7f120540

.field public static final country_macao:I = 0x7f120541

.field public static final country_madagascar:I = 0x7f120542

.field public static final country_malawi:I = 0x7f120543

.field public static final country_malaysia:I = 0x7f120544

.field public static final country_mali:I = 0x7f120545

.field public static final country_malta:I = 0x7f120546

.field public static final country_mauritania:I = 0x7f120547

.field public static final country_mauritius:I = 0x7f120548

.field public static final country_mexico:I = 0x7f120549

.field public static final country_micronesia_federatedstatesof:I = 0x7f12054a

.field public static final country_moldova_republicof:I = 0x7f12054b

.field public static final country_mongolia:I = 0x7f12054c

.field public static final country_montserrat:I = 0x7f12054d

.field public static final country_morocco:I = 0x7f12054e

.field public static final country_mozambique:I = 0x7f12054f

.field public static final country_myanmar:I = 0x7f120550

.field public static final country_namibia:I = 0x7f120551

.field public static final country_nepal:I = 0x7f120552

.field public static final country_netherlands:I = 0x7f120553

.field public static final country_newzealand:I = 0x7f120554

.field public static final country_nicaragua:I = 0x7f120555

.field public static final country_niger:I = 0x7f120556

.field public static final country_nigeria:I = 0x7f120557

.field public static final country_northernmarianaislands:I = 0x7f120558

.field public static final country_northmacedonia_therepublicof:I = 0x7f120559

.field public static final country_norway:I = 0x7f12055a

.field public static final country_oman:I = 0x7f12055b

.field public static final country_pakistan:I = 0x7f12055c

.field public static final country_palau:I = 0x7f12055d

.field public static final country_panama:I = 0x7f12055e

.field public static final country_papuanewguinea:I = 0x7f12055f

.field public static final country_paraguay:I = 0x7f120560

.field public static final country_peru:I = 0x7f120561

.field public static final country_philippines:I = 0x7f120562

.field public static final country_poland:I = 0x7f120563

.field public static final country_portugal:I = 0x7f120564

.field public static final country_puertorico:I = 0x7f120565

.field public static final country_qatar:I = 0x7f120566

.field public static final country_romania:I = 0x7f120567

.field public static final country_russianfederation:I = 0x7f120568

.field public static final country_rwanda:I = 0x7f120569

.field public static final country_saintkittsandnevis:I = 0x7f12056a

.field public static final country_saintlucia:I = 0x7f12056b

.field public static final country_saintvincentandthegrenadines:I = 0x7f12056c

.field public static final country_saotomeandprincipe:I = 0x7f12056d

.field public static final country_saudiarabia:I = 0x7f12056e

.field public static final country_senegal:I = 0x7f120572

.field public static final country_serbia:I = 0x7f120573

.field public static final country_seychelles:I = 0x7f120574

.field public static final country_sierraleone:I = 0x7f120575

.field public static final country_singapore:I = 0x7f120576

.field public static final country_slovakia:I = 0x7f120577

.field public static final country_slovenia:I = 0x7f120578

.field public static final country_solomonislands:I = 0x7f120579

.field public static final country_southafrica:I = 0x7f12057a

.field public static final country_spain:I = 0x7f12057b

.field public static final country_srilanka:I = 0x7f12057c

.field public static final country_suriname:I = 0x7f12057d

.field public static final country_swaziland:I = 0x7f12057e

.field public static final country_sweden:I = 0x7f12057f

.field public static final country_switzerland:I = 0x7f120580

.field public static final country_taiwan_provinceofchina:I = 0x7f120581

.field public static final country_tajikistan:I = 0x7f120582

.field public static final country_tanzania_unitedrepublicof:I = 0x7f120583

.field public static final country_thailand:I = 0x7f120584

.field public static final country_togo:I = 0x7f120585

.field public static final country_tonga:I = 0x7f120586

.field public static final country_trinidadandtobago:I = 0x7f120587

.field public static final country_tunisia:I = 0x7f120588

.field public static final country_turkey:I = 0x7f120589

.field public static final country_turkmenistan:I = 0x7f12058a

.field public static final country_turksandcaicosislands:I = 0x7f12058b

.field public static final country_tuvalu:I = 0x7f12058c

.field public static final country_uganda:I = 0x7f12058d

.field public static final country_ukraine:I = 0x7f12058e

.field public static final country_unitedarabemirates:I = 0x7f12058f

.field public static final country_unitedkingdom:I = 0x7f120590

.field public static final country_unitedstates:I = 0x7f120591

.field public static final country_uruguay:I = 0x7f120592

.field public static final country_uzbekistan:I = 0x7f120593

.field public static final country_venezuela_bolivarianrepublicof:I = 0x7f120594

.field public static final country_vietnam:I = 0x7f120595

.field public static final country_virginislands_british:I = 0x7f120596

.field public static final country_virginislands_us:I = 0x7f120597

.field public static final country_yemen:I = 0x7f120598

.field public static final country_zambia:I = 0x7f120599

.field public static final country_zimbabwe:I = 0x7f12059a

.field public static final county_hint:I = 0x7f12059b

.field public static final crm_cardonfile_customer_email_disclaimer:I = 0x7f120622

.field public static final crm_cardonfile_customer_email_disclaimer_uk:I = 0x7f120623

.field public static final crm_cardonfile_verifypostal_message:I = 0x7f12063b

.field public static final crm_cardonfile_verifypostal_message_no_card_name:I = 0x7f12063c

.field public static final crm_cardonfile_verifypostal_title:I = 0x7f12063d

.field public static final crm_cardonfile_verifypostcode_message:I = 0x7f12063e

.field public static final crm_cardonfile_verifypostcode_message_no_card_name:I = 0x7f12063f

.field public static final crm_cardonfile_verifypostcode_message_uk:I = 0x7f120640

.field public static final crm_cardonfile_verifypostcode_message_uk_no_card_name:I = 0x7f120641

.field public static final crm_cardonfile_verifypostcode_title:I = 0x7f120642

.field public static final crm_cardonfile_verifyzip_message:I = 0x7f120643

.field public static final crm_cardonfile_verifyzip_message_no_card_name:I = 0x7f120644

.field public static final crm_cardonfile_verifyzip_title:I = 0x7f120645

.field public static final empty:I = 0x7f120a3a

.field public static final institution_number:I = 0x7f120c40

.field public static final invalid_bank_message:I = 0x7f120c52

.field public static final invalid_branch_message:I = 0x7f120c53

.field public static final invalid_bsb_number_message:I = 0x7f120c54

.field public static final invalid_institution_number_message:I = 0x7f120c5b

.field public static final invalid_routing_message:I = 0x7f120c67

.field public static final invalid_sort_code_message:I = 0x7f120c68

.field public static final invalid_transit_number_message:I = 0x7f120c6b

.field public static final postal_hint:I = 0x7f121458

.field public static final postcode_hint:I = 0x7f12145c

.field public static final province_hint:I = 0x7f1214fa

.field public static final routing_number:I = 0x7f1216ac

.field public static final sort_code:I = 0x7f121832

.field public static final state_hint:I = 0x7f1218c4

.field public static final state_or_territory_hint:I = 0x7f1218c5

.field public static final transit_number:I = 0x7f121a14

.field public static final zip:I = 0x7f121bf8

.field public static final zip_code_hint:I = 0x7f121bf9


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 205
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
