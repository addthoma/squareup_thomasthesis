.class public Lcom/squareup/log/ReaderEventLogger$FirmwareUpdateReaderEvent;
.super Lcom/squareup/log/ReaderEvent;
.source "ReaderEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/ReaderEventLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FirmwareUpdateReaderEvent"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/log/ReaderEventLogger$FirmwareUpdateReaderEvent$Builder;
    }
.end annotation


# instance fields
.field public final firmwareUpdateSessionId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/log/ReaderEventLogger$FirmwareUpdateReaderEvent$Builder;)V
    .locals 0

    .line 1249
    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEvent;-><init>(Lcom/squareup/log/ReaderEvent$Builder;)V

    .line 1250
    iget-object p1, p1, Lcom/squareup/log/ReaderEventLogger$FirmwareUpdateReaderEvent$Builder;->firmwareUpdateSessionId:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$FirmwareUpdateReaderEvent;->firmwareUpdateSessionId:Ljava/lang/String;

    return-void
.end method
