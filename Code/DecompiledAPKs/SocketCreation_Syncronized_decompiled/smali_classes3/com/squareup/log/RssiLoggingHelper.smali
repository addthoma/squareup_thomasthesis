.class public Lcom/squareup/log/RssiLoggingHelper;
.super Ljava/lang/Object;
.source "RssiLoggingHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;,
        Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;,
        Lcom/squareup/log/RssiLoggingHelper$RssiLogRingBuffer;
    }
.end annotation


# static fields
.field public static MAX_BUFFER_SIZE:I = 0xc8


# instance fields
.field bufferMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/log/RssiLoggingHelper$RssiLogRingBuffer;",
            ">;"
        }
    .end annotation
.end field

.field private final clock:Lcom/squareup/util/Clock;

.field private final dateFormatter:Ljava/text/SimpleDateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/Clock;)V
    .locals 2

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/log/RssiLoggingHelper;->clock:Lcom/squareup/util/Clock;

    .line 38
    new-instance p1, Ljava/text/SimpleDateFormat;

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "mm:ss.S"

    invoke-direct {p1, v1, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object p1, p0, Lcom/squareup/log/RssiLoggingHelper;->dateFormatter:Ljava/text/SimpleDateFormat;

    .line 39
    iget-object p1, p0, Lcom/squareup/log/RssiLoggingHelper;->dateFormatter:Ljava/text/SimpleDateFormat;

    const-string v0, "UTC"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 40
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/log/RssiLoggingHelper;->bufferMap:Ljava/util/HashMap;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/log/RssiLoggingHelper;)Lcom/squareup/util/Clock;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/log/RssiLoggingHelper;->clock:Lcom/squareup/util/Clock;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/log/RssiLoggingHelper;)Ljava/text/SimpleDateFormat;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/log/RssiLoggingHelper;->dateFormatter:Ljava/text/SimpleDateFormat;

    return-object p0
.end method


# virtual methods
.method public addMeasurement(Ljava/lang/String;I)V
    .locals 3

    .line 44
    iget-object v0, p0, Lcom/squareup/log/RssiLoggingHelper;->bufferMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/squareup/log/RssiLoggingHelper;->bufferMap:Ljava/util/HashMap;

    new-instance v1, Lcom/squareup/log/RssiLoggingHelper$RssiLogRingBuffer;

    sget v2, Lcom/squareup/log/RssiLoggingHelper;->MAX_BUFFER_SIZE:I

    invoke-direct {v1, p0, v2}, Lcom/squareup/log/RssiLoggingHelper$RssiLogRingBuffer;-><init>(Lcom/squareup/log/RssiLoggingHelper;I)V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/squareup/log/RssiLoggingHelper;->bufferMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/log/RssiLoggingHelper$RssiLogRingBuffer;

    new-instance v0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;

    invoke-direct {v0, p0, p2}, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;-><init>(Lcom/squareup/log/RssiLoggingHelper;I)V

    invoke-virtual {p1, v0}, Lcom/squareup/log/RssiLoggingHelper$RssiLogRingBuffer;->addMeasurement(Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;)V

    return-void
.end method

.method public flush(Ljava/lang/String;)V
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/squareup/log/RssiLoggingHelper;->bufferMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public formatMeasurements(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 71
    sget v0, Lcom/squareup/log/RssiLoggingHelper;->MAX_BUFFER_SIZE:I

    invoke-virtual {p0, p1, v0}, Lcom/squareup/log/RssiLoggingHelper;->formatMeasurements(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public formatMeasurements(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/log/RssiLoggingHelper;->bufferMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/squareup/log/RssiLoggingHelper;->bufferMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/log/RssiLoggingHelper$RssiLogRingBuffer;

    invoke-virtual {p1, p2}, Lcom/squareup/log/RssiLoggingHelper$RssiLogRingBuffer;->toString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getStatistics(Ljava/lang/String;)Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;
    .locals 2

    .line 89
    iget-object v0, p0, Lcom/squareup/log/RssiLoggingHelper;->bufferMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 90
    :cond_0
    new-instance v0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;

    iget-object v1, p0, Lcom/squareup/log/RssiLoggingHelper;->bufferMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/log/RssiLoggingHelper$RssiLogRingBuffer;

    invoke-virtual {p1}, Lcom/squareup/log/RssiLoggingHelper$RssiLogRingBuffer;->getMeasurements()Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public hasMeasurements(Ljava/lang/String;)Z
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/log/RssiLoggingHelper;->bufferMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/squareup/log/RssiLoggingHelper;->bufferMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/log/RssiLoggingHelper$RssiLogRingBuffer;

    invoke-virtual {p1}, Lcom/squareup/log/RssiLoggingHelper$RssiLogRingBuffer;->getMeasurements()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method public remove(Ljava/lang/String;)V
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/log/RssiLoggingHelper;->bufferMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
