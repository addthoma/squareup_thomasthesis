.class public Lcom/squareup/log/BugsnagCrashReporter;
.super Ljava/lang/Object;
.source "BugsnagCrashReporter.java"

# interfaces
.implements Lcom/squareup/log/CrashReporter;


# static fields
.field private static final NATIVE_CRASH_SEVERITY:Lcom/bugsnag/android/Severity;

.field private static final TAB_DEVICE:Ljava/lang/String; = "DEVICE"

.field private static final TAB_MINIDUMP:Ljava/lang/String; = "MINI-DUMP"

.field private static final TAB_MISC:Ljava/lang/String; = "MISC"

.field private static final TAB_MORTAR_SCOPES:Ljava/lang/String; = "MORTAR-SCOPES"

.field private static final TAB_OHSNAP:Ljava/lang/String; = "OH-SNAP"

.field private static final TAB_USER:Ljava/lang/String; = "USER"

.field private static final TAB_VIEW_HIERARCHY:Ljava/lang/String; = "VIEW-HIERARCHY"


# instance fields
.field private final bugsnagClient:Lcom/bugsnag/android/Client;

.field private final eventsRingBuffer:Lcom/squareup/util/RingBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/util/RingBuffer<",
            "Lcom/squareup/log/OhSnapEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    invoke-static {}, Lcom/squareup/util/X2Build;->isSquareDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/bugsnag/android/Severity;->ERROR:Lcom/bugsnag/android/Severity;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/bugsnag/android/Severity;->WARNING:Lcom/bugsnag/android/Severity;

    :goto_0
    sput-object v0, Lcom/squareup/log/BugsnagCrashReporter;->NATIVE_CRASH_SEVERITY:Lcom/bugsnag/android/Severity;

    return-void
.end method

.method public constructor <init>(Landroid/app/Application;)V
    .locals 3

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Lcom/squareup/util/RingBuffer;

    const-class v1, Lcom/squareup/log/OhSnapEvent;

    const/16 v2, 0xc8

    invoke-direct {v0, v1, v2}, Lcom/squareup/util/RingBuffer;-><init>(Ljava/lang/Class;I)V

    iput-object v0, p0, Lcom/squareup/log/BugsnagCrashReporter;->eventsRingBuffer:Lcom/squareup/util/RingBuffer;

    .line 35
    invoke-static {p1}, Lcom/squareup/log/BugsnagCrashReporter;->createBugsnagClient(Landroid/app/Application;)Lcom/bugsnag/android/Client;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/log/BugsnagCrashReporter;->bugsnagClient:Lcom/bugsnag/android/Client;

    .line 36
    new-instance p1, Lcom/squareup/log/OhSnapEvent;

    sget-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->CRASH_REPORTER:Lcom/squareup/log/OhSnapLogger$EventType;

    sget-object v1, Lcom/squareup/log/OhSnapEvent$Orientation;->UNKNOWN:Lcom/squareup/log/OhSnapEvent$Orientation;

    const-string v2, "BugSnagCrashreporter initialized"

    invoke-direct {p1, v0, v1, v2}, Lcom/squareup/log/OhSnapEvent;-><init>(Lcom/squareup/log/OhSnapLogger$EventType;Lcom/squareup/log/OhSnapEvent$Orientation;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/squareup/log/BugsnagCrashReporter;->log(Lcom/squareup/log/OhSnapEvent;)V

    return-void
.end method

.method public static createBugsnagClient(Landroid/app/Application;)Lcom/bugsnag/android/Client;
    .locals 0

    .line 124
    invoke-static {p0}, Lcom/bugsnag/android/HotfixClient;->createBugsnagClient(Landroid/app/Application;)Lcom/bugsnag/android/Client;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public crashThrowable(Ljava/lang/Throwable;)V
    .locals 2

    .line 40
    iget-object v0, p0, Lcom/squareup/log/BugsnagCrashReporter;->bugsnagClient:Lcom/bugsnag/android/Client;

    sget-object v1, Lcom/bugsnag/android/Severity;->ERROR:Lcom/bugsnag/android/Severity;

    invoke-virtual {v0, p1, v1}, Lcom/bugsnag/android/Client;->notify(Ljava/lang/Throwable;Lcom/bugsnag/android/Severity;)V

    return-void
.end method

.method public crashThrowableBlocking(Ljava/lang/Throwable;Ljava/lang/Thread;)V
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/log/BugsnagCrashReporter;->bugsnagClient:Lcom/bugsnag/android/Client;

    invoke-static {v0, p1, p2}, Lcom/bugsnag/android/ClientSpyKt;->cacheAndNotifyUnhandled(Lcom/bugsnag/android/Client;Ljava/lang/Throwable;Ljava/lang/Thread;)V

    return-void
.end method

.method public crashnadoMiniDump(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const-string v1, "native crash: %s (miniDump elided)"

    .line 95
    invoke-static {p1, v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 96
    new-instance v0, Lcom/bugsnag/android/MetaData;

    invoke-direct {v0}, Lcom/bugsnag/android/MetaData;-><init>()V

    const-string v1, "MINI-DUMP"

    .line 97
    invoke-virtual {v0, v1, p2, p3}, Lcom/bugsnag/android/MetaData;->addToTab(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 98
    iget-object p2, p0, Lcom/squareup/log/BugsnagCrashReporter;->bugsnagClient:Lcom/bugsnag/android/Client;

    sget-object p3, Lcom/squareup/log/BugsnagCrashReporter;->NATIVE_CRASH_SEVERITY:Lcom/bugsnag/android/Severity;

    invoke-virtual {p2, p1, p3, v0}, Lcom/bugsnag/android/Client;->notify(Ljava/lang/Throwable;Lcom/bugsnag/android/Severity;Lcom/bugsnag/android/MetaData;)V

    return-void
.end method

.method public getLastEventsAsString()Ljava/lang/String;
    .locals 4

    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 65
    iget-object v1, p0, Lcom/squareup/log/BugsnagCrashReporter;->eventsRingBuffer:Lcom/squareup/util/RingBuffer;

    monitor-enter v1

    .line 66
    :try_start_0
    iget-object v2, p0, Lcom/squareup/log/BugsnagCrashReporter;->eventsRingBuffer:Lcom/squareup/util/RingBuffer;

    invoke-virtual {v2}, Lcom/squareup/util/RingBuffer;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/log/OhSnapEvent;

    .line 67
    invoke-virtual {v3}, Lcom/squareup/log/OhSnapEvent;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 69
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :catchall_0
    move-exception v0

    .line 69
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public log(Lcom/squareup/log/OhSnapEvent;)V
    .locals 5

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 78
    iget-object v1, p1, Lcom/squareup/log/OhSnapEvent;->eventType:Lcom/squareup/log/OhSnapLogger$EventType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p1, Lcom/squareup/log/OhSnapEvent;->message:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "OhSnap %s -> %s "

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/log/BugsnagCrashReporter;->eventsRingBuffer:Lcom/squareup/util/RingBuffer;

    monitor-enter v0

    .line 82
    :try_start_0
    iget-object v1, p0, Lcom/squareup/log/BugsnagCrashReporter;->eventsRingBuffer:Lcom/squareup/util/RingBuffer;

    invoke-virtual {v1, p1}, Lcom/squareup/util/RingBuffer;->addAndReturnDeleted(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/log/OhSnapEvent;

    if-eqz v1, :cond_0

    .line 87
    iget-object v2, p0, Lcom/squareup/log/BugsnagCrashReporter;->bugsnagClient:Lcom/bugsnag/android/Client;

    const-string v3, "OH-SNAP"

    iget-object v1, v1, Lcom/squareup/log/OhSnapEvent;->id:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v1, v4}, Lcom/bugsnag/android/Client;->addToTab(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 89
    :cond_0
    iget-object v1, p0, Lcom/squareup/log/BugsnagCrashReporter;->bugsnagClient:Lcom/bugsnag/android/Client;

    const-string v2, "OH-SNAP"

    iget-object v3, p1, Lcom/squareup/log/OhSnapEvent;->id:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/squareup/log/OhSnapEvent;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, v3, p1}, Lcom/bugsnag/android/Client;->addToTab(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 90
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/log/BugsnagCrashReporter;->bugsnagClient:Lcom/bugsnag/android/Client;

    invoke-virtual {v0, p1, p2, p3}, Lcom/bugsnag/android/Client;->addToTab(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public logToDeviceTab(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 116
    iget-object v0, p0, Lcom/squareup/log/BugsnagCrashReporter;->bugsnagClient:Lcom/bugsnag/android/Client;

    const-string v1, "DEVICE"

    invoke-virtual {v0, v1, p1, p2}, Lcom/bugsnag/android/Client;->addToTab(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 112
    iget-object v0, p0, Lcom/squareup/log/BugsnagCrashReporter;->bugsnagClient:Lcom/bugsnag/android/Client;

    const-string v1, "MISC"

    invoke-virtual {v0, v1, p1, p2}, Lcom/bugsnag/android/Client;->addToTab(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public logToUserTab(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 120
    iget-object v0, p0, Lcom/squareup/log/BugsnagCrashReporter;->bugsnagClient:Lcom/bugsnag/android/Client;

    const-string v1, "USER"

    invoke-virtual {v0, v1, p1, p2}, Lcom/bugsnag/android/Client;->addToTab(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public setMortarScopeHierarchy(Ljava/lang/String;)V
    .locals 3

    .line 52
    iget-object v0, p0, Lcom/squareup/log/BugsnagCrashReporter;->bugsnagClient:Lcom/bugsnag/android/Client;

    const-string v1, "MORTAR-SCOPES"

    const-string v2, "Mortar scope hierarchy"

    invoke-virtual {v0, v1, v2, p1}, Lcom/bugsnag/android/Client;->addToTab(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public setReleaseStage(Ljava/lang/String;)V
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/log/BugsnagCrashReporter;->bugsnagClient:Lcom/bugsnag/android/Client;

    invoke-virtual {v0, p1}, Lcom/bugsnag/android/Client;->setReleaseStage(Ljava/lang/String;)V

    return-void
.end method

.method public setUserIdentifier(Ljava/lang/String;)V
    .locals 2

    .line 48
    iget-object v0, p0, Lcom/squareup/log/BugsnagCrashReporter;->bugsnagClient:Lcom/bugsnag/android/Client;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, v1}, Lcom/bugsnag/android/Client;->setUser(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setViewHierarchy(Ljava/lang/String;)V
    .locals 3

    .line 56
    iget-object v0, p0, Lcom/squareup/log/BugsnagCrashReporter;->bugsnagClient:Lcom/bugsnag/android/Client;

    const-string v1, "VIEW-HIERARCHY"

    const-string v2, "View hierarchy"

    invoke-virtual {v0, v1, v2, p1}, Lcom/bugsnag/android/Client;->addToTab(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public warningThrowable(Ljava/lang/Throwable;)V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Warning Throwable"

    .line 102
    invoke-static {p1, v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 103
    iget-object v0, p0, Lcom/squareup/log/BugsnagCrashReporter;->bugsnagClient:Lcom/bugsnag/android/Client;

    sget-object v1, Lcom/bugsnag/android/Severity;->WARNING:Lcom/bugsnag/android/Severity;

    invoke-virtual {v0, p1, v1}, Lcom/bugsnag/android/Client;->notify(Ljava/lang/Throwable;Lcom/bugsnag/android/Severity;)V

    return-void
.end method

.method public warningThrowable(Ljava/lang/Throwable;Lcom/bugsnag/android/MetaData;)V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Warning Throwable"

    .line 107
    invoke-static {p1, v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 108
    iget-object v0, p0, Lcom/squareup/log/BugsnagCrashReporter;->bugsnagClient:Lcom/bugsnag/android/Client;

    sget-object v1, Lcom/bugsnag/android/Severity;->WARNING:Lcom/bugsnag/android/Severity;

    invoke-virtual {v0, p1, v1, p2}, Lcom/bugsnag/android/Client;->notify(Ljava/lang/Throwable;Lcom/bugsnag/android/Severity;Lcom/bugsnag/android/MetaData;)V

    return-void
.end method
