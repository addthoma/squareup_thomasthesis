.class public Lcom/squareup/log/ReaderEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "ReaderEvent.java"

# interfaces
.implements Lcom/squareup/log/HasPaymentTimings;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/log/ReaderEvent$Builder;
    }
.end annotation


# instance fields
.field public final cardReaderId:Ljava/lang/Integer;

.field public final chargeCycleCount:I

.field public final connectionType:Ljava/lang/String;

.field public final firmwareVersion:Ljava/lang/String;

.field public final hardwareSerialNumber:Ljava/lang/String;

.field public final inCommsRate:Ljava/lang/String;

.field public final outCommsRate:Ljava/lang/String;

.field public final overrideName:Lcom/squareup/eventstream/v1/EventStream$Name;

.field public final overrideValue:Ljava/lang/String;

.field public final transient paymentTimings:Lcom/squareup/cardreader/PaymentTimings;

.field public final readerTypePrefix:Ljava/lang/String;

.field public final sessionId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/log/ReaderEvent$Builder;)V
    .locals 3

    .line 38
    iget-object v0, p1, Lcom/squareup/log/ReaderEvent$Builder;->overrideName:Lcom/squareup/eventstream/v1/EventStream$Name;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p1, Lcom/squareup/log/ReaderEvent$Builder;->readerTypePrefix:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lcom/squareup/log/ReaderEvent$Builder;->overrideValue:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/log/ReaderEvent$Builder;->overrideRawBytes:Lokio/ByteString;

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lokio/ByteString;)V

    .line 41
    iget-object v0, p1, Lcom/squareup/log/ReaderEvent$Builder;->overrideTimestamp:Lcom/squareup/protos/common/time/DateTime;

    iput-object v0, p0, Lcom/squareup/eventstream/v1/EventStreamEvent;->overrideTimestamp:Lcom/squareup/protos/common/time/DateTime;

    .line 43
    iget-object v0, p1, Lcom/squareup/log/ReaderEvent$Builder;->overrideName:Lcom/squareup/eventstream/v1/EventStream$Name;

    iput-object v0, p0, Lcom/squareup/log/ReaderEvent;->overrideName:Lcom/squareup/eventstream/v1/EventStream$Name;

    .line 44
    iget-object v0, p1, Lcom/squareup/log/ReaderEvent$Builder;->readerTypePrefix:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/log/ReaderEvent;->readerTypePrefix:Ljava/lang/String;

    .line 45
    iget-object v0, p1, Lcom/squareup/log/ReaderEvent$Builder;->overrideValue:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/log/ReaderEvent;->overrideValue:Ljava/lang/String;

    .line 46
    iget-object v0, p1, Lcom/squareup/log/ReaderEvent$Builder;->connectionType:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/log/ReaderEvent;->connectionType:Ljava/lang/String;

    .line 47
    iget-object v0, p1, Lcom/squareup/log/ReaderEvent$Builder;->firmwareVersion:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/log/ReaderEvent;->firmwareVersion:Ljava/lang/String;

    .line 48
    iget-object v0, p1, Lcom/squareup/log/ReaderEvent$Builder;->hardwareSerialNumber:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/log/ReaderEvent;->hardwareSerialNumber:Ljava/lang/String;

    .line 49
    iget-object v0, p1, Lcom/squareup/log/ReaderEvent$Builder;->inCommsRate:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/log/ReaderEvent;->inCommsRate:Ljava/lang/String;

    .line 50
    iget-object v0, p1, Lcom/squareup/log/ReaderEvent$Builder;->outCommsRate:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/log/ReaderEvent;->outCommsRate:Ljava/lang/String;

    .line 51
    iget v0, p1, Lcom/squareup/log/ReaderEvent$Builder;->chargeCycleCount:I

    iput v0, p0, Lcom/squareup/log/ReaderEvent;->chargeCycleCount:I

    .line 52
    iget-object v0, p1, Lcom/squareup/log/ReaderEvent$Builder;->sessionId:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/log/ReaderEvent;->sessionId:Ljava/lang/String;

    .line 53
    iget-object v0, p1, Lcom/squareup/log/ReaderEvent$Builder;->paymentTimings:Lcom/squareup/cardreader/PaymentTimings;

    iput-object v0, p0, Lcom/squareup/log/ReaderEvent;->paymentTimings:Lcom/squareup/cardreader/PaymentTimings;

    .line 54
    iget-object v0, p1, Lcom/squareup/log/ReaderEvent$Builder;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    if-eqz v0, :cond_0

    iget-object p1, p1, Lcom/squareup/log/ReaderEvent$Builder;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    iget p1, p1, Lcom/squareup/cardreader/CardReaderId;->id:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-object p1, p0, Lcom/squareup/log/ReaderEvent;->cardReaderId:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public getPaymentTimings()Lcom/squareup/cardreader/PaymentTimings;
    .locals 1

    .line 214
    iget-object v0, p0, Lcom/squareup/log/ReaderEvent;->paymentTimings:Lcom/squareup/cardreader/PaymentTimings;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/squareup/log/ReaderEvent;->readerTypePrefix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/log/ReaderEvent;->connectionType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/log/ReaderEvent;->overrideName:Lcom/squareup/eventstream/v1/EventStream$Name;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/log/ReaderEvent;->overrideValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/log/ReaderEvent;->overrideTimestamp:Lcom/squareup/protos/common/time/DateTime;

    iget-object v1, v1, Lcom/squareup/protos/common/time/DateTime;->instant_usec:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
