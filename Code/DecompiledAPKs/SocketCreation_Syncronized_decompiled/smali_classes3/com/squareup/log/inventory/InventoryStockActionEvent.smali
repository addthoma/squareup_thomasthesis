.class public Lcom/squareup/log/inventory/InventoryStockActionEvent;
.super Lcom/squareup/eventstream/v2/AppEvent;
.source "InventoryStockActionEvent.java"


# static fields
.field private static final CATALOG_NAME:Ljava/lang/String; = "inventory_stock_action"

.field private static final SCREEN_ADJUST_STOCK_ACTION:Ljava/lang/String; = "ADJUST_STOCK_ACTION"

.field private static final SCREEN_EDIT_PRODUCT_FROM_ITEMS_APPLET:Ljava/lang/String; = "EDIT_PRODUCT_FROM_ITEMS_APPLET"

.field private static final SCREEN_RESTOCK_ON_ITEMIZED_REFUND:Ljava/lang/String; = "RESTOCK_ON_ITEMIZED_REFUND"

.field private static final STOCK_ACTION_DAMAGED:Ljava/lang/String; = "DAMAGED"

.field private static final STOCK_ACTION_LOSS:Ljava/lang/String; = "LOSS"

.field private static final STOCK_ACTION_RECEIVED:Ljava/lang/String; = "RECEIVED"

.field private static final STOCK_ACTION_RECOUNT:Ljava/lang/String; = "RECOUNT"

.field private static final STOCK_ACTION_RESTOCK:Ljava/lang/String; = "RESTOCK"

.field private static final STOCK_ACTION_RETURN:Ljava/lang/String; = "RETURN"

.field private static final STOCK_ACTION_THEFT:Ljava/lang/String; = "THEFT"


# instance fields
.field private final inventory_stock_action_item_token:Ljava/lang/String;

.field private final inventory_stock_action_item_variation_token:Ljava/lang/String;

.field private final inventory_stock_action_new_item:Z

.field private final inventory_stock_action_new_variation:Z

.field private final inventory_stock_action_quantity:Ljava/lang/String;

.field private final inventory_stock_action_screen:Ljava/lang/String;

.field private final inventory_stock_action_stock_action:Ljava/lang/String;

.field private final inventory_stock_action_unit_cost:Ljava/lang/Long;

.field private final inventory_stock_action_unit_token:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "inventory_stock_action"

    .line 35
    invoke-direct {p0, v0}, Lcom/squareup/eventstream/v2/AppEvent;-><init>(Ljava/lang/String;)V

    .line 36
    iput-object p1, p0, Lcom/squareup/log/inventory/InventoryStockActionEvent;->inventory_stock_action_stock_action:Ljava/lang/String;

    .line 37
    iput-object p2, p0, Lcom/squareup/log/inventory/InventoryStockActionEvent;->inventory_stock_action_unit_cost:Ljava/lang/Long;

    .line 38
    iput-object p3, p0, Lcom/squareup/log/inventory/InventoryStockActionEvent;->inventory_stock_action_quantity:Ljava/lang/String;

    .line 39
    iput-object p4, p0, Lcom/squareup/log/inventory/InventoryStockActionEvent;->inventory_stock_action_item_token:Ljava/lang/String;

    .line 40
    iput-boolean p5, p0, Lcom/squareup/log/inventory/InventoryStockActionEvent;->inventory_stock_action_new_item:Z

    .line 41
    iput-object p6, p0, Lcom/squareup/log/inventory/InventoryStockActionEvent;->inventory_stock_action_item_variation_token:Ljava/lang/String;

    .line 42
    iput-boolean p7, p0, Lcom/squareup/log/inventory/InventoryStockActionEvent;->inventory_stock_action_new_variation:Z

    .line 43
    iput-object p8, p0, Lcom/squareup/log/inventory/InventoryStockActionEvent;->inventory_stock_action_screen:Ljava/lang/String;

    .line 44
    iput-object p9, p0, Lcom/squareup/log/inventory/InventoryStockActionEvent;->inventory_stock_action_unit_token:Ljava/lang/String;

    return-void
.end method

.method public static adjustInventoryStockCount(Lcom/squareup/protos/client/AdjustVariationInventoryRequest;Ljava/lang/String;)Lcom/squareup/log/inventory/InventoryStockActionEvent;
    .locals 11

    .line 53
    new-instance v10, Lcom/squareup/log/inventory/InventoryStockActionEvent;

    invoke-static {p0}, Lcom/squareup/log/inventory/InventoryStockActionEvent;->stockAction(Lcom/squareup/protos/client/AdjustVariationInventoryRequest;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lcom/squareup/log/inventory/InventoryStockActionEvent;->unitCost(Lcom/squareup/protos/client/AdjustVariationInventoryRequest;)Ljava/lang/Long;

    move-result-object v2

    invoke-static {p0}, Lcom/squareup/log/inventory/InventoryStockActionEvent;->quantity(Lcom/squareup/protos/client/AdjustVariationInventoryRequest;)Ljava/lang/String;

    move-result-object v3

    .line 54
    invoke-static {p0}, Lcom/squareup/log/inventory/InventoryStockActionEvent;->variationToken(Lcom/squareup/protos/client/AdjustVariationInventoryRequest;)Ljava/lang/String;

    move-result-object v6

    .line 55
    invoke-static {p0}, Lcom/squareup/log/inventory/InventoryStockActionEvent;->unitToken(Lcom/squareup/protos/client/AdjustVariationInventoryRequest;)Ljava/lang/String;

    move-result-object v9

    const/4 v5, 0x0

    const/4 v7, 0x0

    const-string v8, "ADJUST_STOCK_ACTION"

    move-object v0, v10

    move-object v4, p1

    invoke-direct/range {v0 .. v9}, Lcom/squareup/log/inventory/InventoryStockActionEvent;-><init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    return-object v10
.end method

.method public static assignInventoryStockCount(Lcom/squareup/protos/client/InventoryAdjustment;Ljava/lang/String;ZZ)Lcom/squareup/log/inventory/InventoryStockActionEvent;
    .locals 11

    .line 63
    iget-object v0, p0, Lcom/squareup/protos/client/InventoryAdjustment;->cost_money:Lcom/squareup/protos/common/Money;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/protos/client/InventoryAdjustment;->cost_money:Lcom/squareup/protos/common/Money;

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    :goto_0
    move-object v3, v0

    .line 64
    new-instance v0, Lcom/squareup/log/inventory/InventoryStockActionEvent;

    iget-object v4, p0, Lcom/squareup/protos/client/InventoryAdjustment;->adjust_quantity_decimal:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/client/InventoryAdjustment;->variation_token:Ljava/lang/String;

    iget-object v10, p0, Lcom/squareup/protos/client/InventoryAdjustment;->unit_token:Ljava/lang/String;

    const-string v2, "RECEIVED"

    const-string v9, "EDIT_PRODUCT_FROM_ITEMS_APPLET"

    move-object v1, v0

    move-object v5, p1

    move v6, p2

    move v8, p3

    invoke-direct/range {v1 .. v10}, Lcom/squareup/log/inventory/InventoryStockActionEvent;-><init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private static quantity(Lcom/squareup/protos/client/AdjustVariationInventoryRequest;)Ljava/lang/String;
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->count:Lcom/squareup/protos/client/InventoryCount;

    if-eqz v0, :cond_0

    .line 107
    iget-object p0, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->count:Lcom/squareup/protos/client/InventoryCount;

    iget-object p0, p0, Lcom/squareup/protos/client/InventoryCount;->current_quantity_decimal:Ljava/lang/String;

    return-object p0

    .line 109
    :cond_0
    iget-object p0, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->adjustment:Lcom/squareup/protos/client/InventoryAdjustment;

    iget-object p0, p0, Lcom/squareup/protos/client/InventoryAdjustment;->adjust_quantity_decimal:Ljava/lang/String;

    return-object p0
.end method

.method public static restockOnItemizedRefund(Lcom/squareup/protos/client/InventoryAdjustment;Ljava/lang/String;)Lcom/squareup/log/inventory/InventoryStockActionEvent;
    .locals 11

    .line 71
    new-instance v10, Lcom/squareup/log/inventory/InventoryStockActionEvent;

    iget-object v3, p0, Lcom/squareup/protos/client/InventoryAdjustment;->adjust_quantity_decimal:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/client/InventoryAdjustment;->variation_token:Ljava/lang/String;

    iget-object v9, p0, Lcom/squareup/protos/client/InventoryAdjustment;->unit_token:Ljava/lang/String;

    const-string v1, "RETURN"

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const-string v8, "RESTOCK_ON_ITEMIZED_REFUND"

    move-object v0, v10

    move-object v4, p1

    invoke-direct/range {v0 .. v9}, Lcom/squareup/log/inventory/InventoryStockActionEvent;-><init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    return-object v10
.end method

.method private static stockAction(Lcom/squareup/protos/client/AdjustVariationInventoryRequest;)Ljava/lang/String;
    .locals 3

    .line 77
    iget-object v0, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->count:Lcom/squareup/protos/client/InventoryCount;

    if-eqz v0, :cond_0

    const-string p0, "RECOUNT"

    return-object p0

    .line 81
    :cond_0
    sget-object v0, Lcom/squareup/log/inventory/InventoryStockActionEvent$1;->$SwitchMap$com$squareup$protos$client$InventoryAdjustmentReason:[I

    iget-object v1, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->adjustment:Lcom/squareup/protos/client/InventoryAdjustment;

    iget-object v1, v1, Lcom/squareup/protos/client/InventoryAdjustment;->reason:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    invoke-virtual {v1}, Lcom/squareup/protos/client/InventoryAdjustmentReason;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_5

    const/4 v1, 0x2

    if-eq v0, v1, :cond_4

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    const-string p0, "THEFT"

    return-object p0

    .line 93
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Do not expect inventory adjustment reason "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p0, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->adjustment:Lcom/squareup/protos/client/InventoryAdjustment;

    iget-object p0, p0, Lcom/squareup/protos/client/InventoryAdjustment;->reason:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const-string p0, "RESTOCK"

    return-object p0

    :cond_3
    const-string p0, "RECEIVED"

    return-object p0

    :cond_4
    const-string p0, "LOSS"

    return-object p0

    :cond_5
    const-string p0, "DAMAGED"

    return-object p0
.end method

.method private static unitCost(Lcom/squareup/protos/client/AdjustVariationInventoryRequest;)Ljava/lang/Long;
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->adjustment:Lcom/squareup/protos/client/InventoryAdjustment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->adjustment:Lcom/squareup/protos/client/InventoryAdjustment;

    iget-object v0, v0, Lcom/squareup/protos/client/InventoryAdjustment;->cost_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    .line 100
    iget-object p0, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->adjustment:Lcom/squareup/protos/client/InventoryAdjustment;

    iget-object p0, p0, Lcom/squareup/protos/client/InventoryAdjustment;->cost_money:Lcom/squareup/protos/common/Money;

    iget-object p0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method private static unitToken(Lcom/squareup/protos/client/AdjustVariationInventoryRequest;)Ljava/lang/String;
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->count:Lcom/squareup/protos/client/InventoryCount;

    if-eqz v0, :cond_0

    .line 123
    iget-object p0, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->count:Lcom/squareup/protos/client/InventoryCount;

    iget-object p0, p0, Lcom/squareup/protos/client/InventoryCount;->unit_token:Ljava/lang/String;

    return-object p0

    .line 125
    :cond_0
    iget-object p0, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->adjustment:Lcom/squareup/protos/client/InventoryAdjustment;

    iget-object p0, p0, Lcom/squareup/protos/client/InventoryAdjustment;->unit_token:Ljava/lang/String;

    return-object p0
.end method

.method private static variationToken(Lcom/squareup/protos/client/AdjustVariationInventoryRequest;)Ljava/lang/String;
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->count:Lcom/squareup/protos/client/InventoryCount;

    if-eqz v0, :cond_0

    .line 115
    iget-object p0, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->count:Lcom/squareup/protos/client/InventoryCount;

    iget-object p0, p0, Lcom/squareup/protos/client/InventoryCount;->variation_token:Ljava/lang/String;

    return-object p0

    .line 117
    :cond_0
    iget-object p0, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->adjustment:Lcom/squareup/protos/client/InventoryAdjustment;

    iget-object p0, p0, Lcom/squareup/protos/client/InventoryAdjustment;->variation_token:Ljava/lang/String;

    return-object p0
.end method
