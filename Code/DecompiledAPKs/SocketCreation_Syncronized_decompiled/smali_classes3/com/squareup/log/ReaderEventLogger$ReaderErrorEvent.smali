.class public Lcom/squareup/log/ReaderEventLogger$ReaderErrorEvent;
.super Lcom/squareup/log/ReaderEvent;
.source "ReaderEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/ReaderEventLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ReaderErrorEvent"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/log/ReaderEventLogger$ReaderErrorEvent$Builder;
    }
.end annotation


# instance fields
.field public final error:Lcom/squareup/cardreader/lcr/CrsReaderError;


# direct methods
.method public constructor <init>(Lcom/squareup/log/ReaderEventLogger$ReaderErrorEvent$Builder;)V
    .locals 0

    .line 1145
    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEvent;-><init>(Lcom/squareup/log/ReaderEvent$Builder;)V

    .line 1146
    iget-object p1, p1, Lcom/squareup/log/ReaderEventLogger$ReaderErrorEvent$Builder;->error:Lcom/squareup/cardreader/lcr/CrsReaderError;

    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$ReaderErrorEvent;->error:Lcom/squareup/cardreader/lcr/CrsReaderError;

    return-void
.end method
