.class public Lcom/squareup/log/deposits/BankLinkingResultEvent;
.super Lcom/squareup/eventstream/v2/AppEvent;
.source "BankLinkingResultEvent.java"


# static fields
.field private static final CATALOG_NAME:Ljava/lang/String; = "deposits_bank_linking_results"


# instance fields
.field private final deposits_bank_linking_results_description:Ljava/lang/String;

.field private final deposits_bank_linking_results_error:Ljava/lang/String;

.field private final deposits_bank_linking_results_is_successful:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 1

    const-string v0, "deposits_bank_linking_results"

    .line 17
    invoke-direct {p0, v0}, Lcom/squareup/eventstream/v2/AppEvent;-><init>(Ljava/lang/String;)V

    .line 18
    iput-object p1, p0, Lcom/squareup/log/deposits/BankLinkingResultEvent;->deposits_bank_linking_results_description:Ljava/lang/String;

    .line 19
    iput-boolean p2, p0, Lcom/squareup/log/deposits/BankLinkingResultEvent;->deposits_bank_linking_results_is_successful:Z

    .line 20
    iput-object p3, p0, Lcom/squareup/log/deposits/BankLinkingResultEvent;->deposits_bank_linking_results_error:Ljava/lang/String;

    return-void
.end method
