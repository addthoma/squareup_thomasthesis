.class public Lcom/squareup/log/cart/TransactionInteractionsLogger;
.super Ljava/lang/Object;
.source "TransactionInteractionsLogger.java"


# static fields
.field public static SAMPLING_RATE:D = 0.2


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final clock:Lcom/squareup/util/Clock;

.field private currentCatalogDiscount:Lcom/squareup/shared/catalog/models/CatalogDiscount;

.field private currentConfigureItemState:Lcom/squareup/configure/item/ConfigureItemState;

.field private currentEventStartedAtMs:J

.field private currentSource:Lcom/squareup/analytics/RegisterViewName;

.field private currentTap:Lcom/squareup/analytics/RegisterTapName;

.field private currentWorkingDiscount:Lcom/squareup/configure/item/WorkingDiscount;

.field private currentWorkingItem:Lcom/squareup/configure/item/WorkingItem;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Clock;Lcom/squareup/payment/Transaction;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    .line 27
    iput-wide v0, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentEventStartedAtMs:J

    .line 37
    iput-object p1, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->analytics:Lcom/squareup/analytics/Analytics;

    .line 38
    iput-object p2, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->clock:Lcom/squareup/util/Clock;

    .line 39
    iput-object p3, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->transaction:Lcom/squareup/payment/Transaction;

    return-void
.end method

.method private getDurationMs()J
    .locals 4

    .line 162
    iget-object v0, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getUptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentEventStartedAtMs:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method private log(Lcom/squareup/log/cart/CartInteractionEvent;)V
    .locals 5

    .line 153
    iget-wide v0, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentEventStartedAtMs:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-ltz v4, :cond_0

    .line 154
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    sget-wide v2, Lcom/squareup/log/cart/TransactionInteractionsLogger;->SAMPLING_RATE:D

    cmpg-double v4, v0, v2

    if-gez v4, :cond_0

    .line 155
    iget-object v0, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 158
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->clear()V

    return-void
.end method

.method private logDiscountEvent(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/api/items/Discount$DiscountType;)V
    .locals 8

    .line 139
    new-instance v7, Lcom/squareup/log/cart/DiscountEvent;

    iget-object v1, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentSource:Lcom/squareup/analytics/RegisterViewName;

    invoke-direct {p0}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->getDurationMs()J

    move-result-wide v3

    iget-object v5, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentTap:Lcom/squareup/analytics/RegisterTapName;

    move-object v0, v7

    move-object v2, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/squareup/log/cart/DiscountEvent;-><init>(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/analytics/RegisterViewName;JLcom/squareup/analytics/RegisterTapName;Lcom/squareup/api/items/Discount$DiscountType;)V

    invoke-direct {p0, v7}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->log(Lcom/squareup/log/cart/CartInteractionEvent;)V

    return-void
.end method

.method private restart(Lcom/squareup/analytics/RegisterViewName;)V
    .locals 2

    .line 67
    invoke-virtual {p0}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->clear()V

    .line 68
    iget-object v0, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getUptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentEventStartedAtMs:J

    .line 69
    iput-object p1, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentSource:Lcom/squareup/analytics/RegisterViewName;

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    const-wide/16 v0, -0x1

    .line 73
    iput-wide v0, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentEventStartedAtMs:J

    const/4 v0, 0x0

    .line 74
    iput-object v0, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentSource:Lcom/squareup/analytics/RegisterViewName;

    .line 75
    iput-object v0, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentTap:Lcom/squareup/analytics/RegisterTapName;

    .line 76
    iput-object v0, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentConfigureItemState:Lcom/squareup/configure/item/ConfigureItemState;

    .line 77
    iput-object v0, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentWorkingItem:Lcom/squareup/configure/item/WorkingItem;

    .line 78
    iput-object v0, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentWorkingDiscount:Lcom/squareup/configure/item/WorkingDiscount;

    .line 79
    iput-object v0, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentCatalogDiscount:Lcom/squareup/shared/catalog/models/CatalogDiscount;

    return-void
.end method

.method public logAllDiscountsEvent(Lcom/squareup/analytics/RegisterViewName;)V
    .locals 8

    .line 143
    new-instance v7, Lcom/squareup/log/cart/AllDiscountsEvent;

    iget-object v1, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentSource:Lcom/squareup/analytics/RegisterViewName;

    invoke-direct {p0}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->getDurationMs()J

    move-result-wide v3

    iget-object v5, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentTap:Lcom/squareup/analytics/RegisterTapName;

    iget-object v0, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->transaction:Lcom/squareup/payment/Transaction;

    .line 144
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v6

    move-object v0, v7

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lcom/squareup/log/cart/AllDiscountsEvent;-><init>(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/analytics/RegisterViewName;JLcom/squareup/analytics/RegisterTapName;I)V

    .line 143
    invoke-direct {p0, v7}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->log(Lcom/squareup/log/cart/CartInteractionEvent;)V

    return-void
.end method

.method public logAllTaxesEvent(Lcom/squareup/analytics/RegisterViewName;)V
    .locals 8

    .line 148
    new-instance v7, Lcom/squareup/log/cart/AllTaxesEvent;

    iget-object v1, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentSource:Lcom/squareup/analytics/RegisterViewName;

    invoke-direct {p0}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->getDurationMs()J

    move-result-wide v3

    iget-object v5, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentTap:Lcom/squareup/analytics/RegisterTapName;

    iget-object v0, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->transaction:Lcom/squareup/payment/Transaction;

    .line 149
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAvailableTaxes()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    move-object v0, v7

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lcom/squareup/log/cart/AllTaxesEvent;-><init>(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/analytics/RegisterViewName;JLcom/squareup/analytics/RegisterTapName;I)V

    .line 148
    invoke-direct {p0, v7}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->log(Lcom/squareup/log/cart/CartInteractionEvent;)V

    return-void
.end method

.method public logDiscountEvent(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/configure/item/WorkingDiscount;)V
    .locals 0

    .line 135
    invoke-virtual {p2}, Lcom/squareup/configure/item/WorkingDiscount;->getDiscountType()Lcom/squareup/api/items/Discount$DiscountType;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->logDiscountEvent(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/api/items/Discount$DiscountType;)V

    return-void
.end method

.method public logDiscountEvent(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/shared/catalog/models/CatalogDiscount;)V
    .locals 0

    .line 131
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getDiscountType()Lcom/squareup/api/items/Discount$DiscountType;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->logDiscountEvent(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/api/items/Discount$DiscountType;)V

    return-void
.end method

.method public logEvent(Lcom/squareup/analytics/RegisterViewName;)V
    .locals 2

    .line 102
    iget-object v0, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentConfigureItemState:Lcom/squareup/configure/item/ConfigureItemState;

    if-eqz v0, :cond_0

    .line 103
    invoke-virtual {p0, p1, v0}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->logItemEvent(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/configure/item/ConfigureItemState;)V

    goto :goto_0

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentWorkingItem:Lcom/squareup/configure/item/WorkingItem;

    if-eqz v0, :cond_1

    .line 105
    invoke-virtual {p0, p1, v0}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->logItemEvent(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/configure/item/WorkingItem;)V

    goto :goto_0

    .line 106
    :cond_1
    iget-object v0, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentWorkingDiscount:Lcom/squareup/configure/item/WorkingDiscount;

    if-eqz v0, :cond_2

    .line 107
    invoke-virtual {p0, p1, v0}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->logDiscountEvent(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/configure/item/WorkingDiscount;)V

    goto :goto_0

    .line 108
    :cond_2
    iget-object v0, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentCatalogDiscount:Lcom/squareup/shared/catalog/models/CatalogDiscount;

    if-eqz v0, :cond_3

    .line 109
    invoke-virtual {p0, p1, v0}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->logDiscountEvent(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/shared/catalog/models/CatalogDiscount;)V

    goto :goto_0

    .line 110
    :cond_3
    iget-object v0, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentSource:Lcom/squareup/analytics/RegisterViewName;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CART_TAXES:Lcom/squareup/analytics/RegisterViewName;

    if-ne v0, v1, :cond_4

    .line 111
    invoke-virtual {p0, p1}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->logAllTaxesEvent(Lcom/squareup/analytics/RegisterViewName;)V

    goto :goto_0

    .line 112
    :cond_4
    iget-object v0, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentSource:Lcom/squareup/analytics/RegisterViewName;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CART_DISCOUNTS:Lcom/squareup/analytics/RegisterViewName;

    if-ne v0, v1, :cond_5

    .line 113
    invoke-virtual {p0, p1}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->logAllDiscountsEvent(Lcom/squareup/analytics/RegisterViewName;)V

    :cond_5
    :goto_0
    return-void
.end method

.method public logItemEvent(Lcom/squareup/analytics/RegisterViewName;II)V
    .locals 9

    .line 126
    new-instance v8, Lcom/squareup/log/cart/ItemEvent;

    iget-object v1, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentSource:Lcom/squareup/analytics/RegisterViewName;

    invoke-direct {p0}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->getDurationMs()J

    move-result-wide v3

    iget-object v5, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentTap:Lcom/squareup/analytics/RegisterTapName;

    move-object v0, v8

    move-object v2, p1

    move v6, p2

    move v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/squareup/log/cart/ItemEvent;-><init>(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/analytics/RegisterViewName;JLcom/squareup/analytics/RegisterTapName;II)V

    invoke-direct {p0, v8}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->log(Lcom/squareup/log/cart/CartInteractionEvent;)V

    return-void
.end method

.method public logItemEvent(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/configure/item/ConfigureItemState;)V
    .locals 1

    .line 122
    invoke-virtual {p2}, Lcom/squareup/configure/item/ConfigureItemState;->getVariations()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p2}, Lcom/squareup/configure/item/ConfigureItemState;->getModifierLists()Ljava/util/SortedMap;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/SortedMap;->size()I

    move-result p2

    invoke-virtual {p0, p1, v0, p2}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->logItemEvent(Lcom/squareup/analytics/RegisterViewName;II)V

    return-void
.end method

.method public logItemEvent(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/configure/item/WorkingItem;)V
    .locals 1

    .line 118
    iget-object v0, p2, Lcom/squareup/configure/item/WorkingItem;->variations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object p2, p2, Lcom/squareup/configure/item/WorkingItem;->modifierLists:Ljava/util/SortedMap;

    invoke-interface {p2}, Ljava/util/SortedMap;->size()I

    move-result p2

    invoke-virtual {p0, p1, v0, p2}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->logItemEvent(Lcom/squareup/analytics/RegisterViewName;II)V

    return-void
.end method

.method public setCurrentCatalogDiscount(Lcom/squareup/shared/catalog/models/CatalogDiscount;)V
    .locals 0

    .line 87
    iput-object p1, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentCatalogDiscount:Lcom/squareup/shared/catalog/models/CatalogDiscount;

    return-void
.end method

.method public setCurrentWorkingItem(Lcom/squareup/configure/item/WorkingItem;)V
    .locals 0

    .line 83
    iput-object p1, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentWorkingItem:Lcom/squareup/configure/item/WorkingItem;

    return-void
.end method

.method public start(Lcom/squareup/analytics/RegisterViewName;)V
    .locals 0

    .line 43
    invoke-direct {p0, p1}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->restart(Lcom/squareup/analytics/RegisterViewName;)V

    return-void
.end method

.method public start(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/analytics/RegisterTapName;)V
    .locals 0

    .line 47
    invoke-direct {p0, p1}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->restart(Lcom/squareup/analytics/RegisterViewName;)V

    .line 48
    iput-object p2, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentTap:Lcom/squareup/analytics/RegisterTapName;

    return-void
.end method

.method public start(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/configure/item/ConfigureItemState;)V
    .locals 0

    .line 52
    invoke-direct {p0, p1}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->restart(Lcom/squareup/analytics/RegisterViewName;)V

    .line 53
    iput-object p2, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentConfigureItemState:Lcom/squareup/configure/item/ConfigureItemState;

    return-void
.end method

.method public start(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/configure/item/WorkingDiscount;)V
    .locals 0

    .line 62
    invoke-direct {p0, p1}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->restart(Lcom/squareup/analytics/RegisterViewName;)V

    .line 63
    iput-object p2, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentWorkingDiscount:Lcom/squareup/configure/item/WorkingDiscount;

    return-void
.end method

.method public start(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/configure/item/WorkingItem;)V
    .locals 0

    .line 57
    invoke-direct {p0, p1}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->restart(Lcom/squareup/analytics/RegisterViewName;)V

    .line 58
    iput-object p2, p0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->currentWorkingItem:Lcom/squareup/configure/item/WorkingItem;

    return-void
.end method
