.class public final Lcom/squareup/log/CrashReportingLogger;
.super Ljava/lang/Object;
.source "CrashReportingLogger.java"

# interfaces
.implements Lcom/squareup/log/OhSnapLogger;
.implements Lcom/squareup/logging/RemoteLogger;


# static fields
.field private static final APP_FILES_DIR:Ljava/lang/String; = "dirAppFiles"

.field private static final APP_TAB:Ljava/lang/String; = "APP"

.field private static final BRAND:Ljava/lang/String; = "brand"

.field private static final BUILD_SHA:Ljava/lang/String; = "buildSha"

.field private static final CPU:Ljava/lang/String; = "cpu"

.field private static final DEVICE_SERIAL_NUMBER:Ljava/lang/String; = "deviceSerialNumber"

.field private static final DIAGONAL_BUCKET:Ljava/lang/String; = "diagonalBucket"

.field private static final ENABLED_FEATURES:Ljava/lang/String; = "enabledFeatures"

.field private static final ENV_DATA_DIR:Ljava/lang/String; = "dirEnvData"

.field private static final HARD_KEYBOARD_HIDDEN:Ljava/lang/String; = "configurationHardKeyboardHidden"

.field private static final INSTALLATION_ID:Ljava/lang/String; = "installationId"

.field public static final IS_INJECTED:Ljava/lang/String; = "isInjected"

.field private static final IS_TABLET:Ljava/lang/String; = "isTablet"

.field private static final KEYBOARD_PACKAGE_NAME:Ljava/lang/String; = "keyboardPackageName"

.field private static final NOT_MAIN_THREAD:Ljava/lang/String; = "Not main thread, unknown."

.field private static final PERMISSION_PREFIX_LENGTH:I = 0x13

.field private static final PROCESS_UNIQUE_ID:Ljava/lang/String; = "processUniqueId"

.field private static final PROCESS_UPTIME:Ljava/lang/String; = "processUptime"

.field private static final PROCESS_UPTIME_MS:Ljava/lang/String; = "processUptimeMs"

.field private static final USER_AGENT:Ljava/lang/String; = "userAgent"

.field private static final USER_COUNTRY_CODE:Ljava/lang/String; = "userCountryCode"

.field private static final USER_SERIAL_NUMBER:Ljava/lang/String; = "userSerialNumber"


# instance fields
.field private final appUpgradeDetector:Lcom/squareup/log/AppUpgradeDetector;

.field private final application:Landroid/app/Application;

.field private final countryCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation
.end field

.field private final device:Lcom/squareup/util/Device;

.field private final deviceSerialNumber:Ljava/lang/String;

.field private final featureFlagsForLogs:Lcom/squareup/log/FeatureFlagsForLogs;

.field private final installationId:Ljava/lang/String;

.field private final mortarScopeHierarchyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final playServicesVersions:Lcom/squareup/firebase/versions/PlayServicesVersions;

.field private final posBuild:Lcom/squareup/util/PosBuild;

.field private final processUniqueId:Lcom/squareup/analytics/ProcessUniqueId;

.field private final reporter:Lcom/squareup/log/CrashReporter;

.field private final resources:Landroid/content/res/Resources;

.field private final startUptimeMs:J

.field private final userAgent:Ljava/lang/String;

.field private final viewHierarchyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Lcom/squareup/log/CrashReporter;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/log/FeatureFlagsForLogs;Lcom/squareup/util/Device;Landroid/app/Application;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/log/AppUpgradeDetector;JLjava/lang/String;Lcom/squareup/util/PosBuild;Lcom/squareup/firebase/versions/PlayServicesVersions;Lcom/squareup/analytics/ProcessUniqueId;Ljavax/inject/Provider;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Lcom/squareup/log/CrashReporter;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/log/FeatureFlagsForLogs;",
            "Lcom/squareup/util/Device;",
            "Landroid/app/Application;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/log/AppUpgradeDetector;",
            "J",
            "Ljava/lang/String;",
            "Lcom/squareup/util/PosBuild;",
            "Lcom/squareup/firebase/versions/PlayServicesVersions;",
            "Lcom/squareup/analytics/ProcessUniqueId;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 165
    iput-object v1, v0, Lcom/squareup/log/CrashReportingLogger;->resources:Landroid/content/res/Resources;

    move-object v1, p2

    .line 166
    iput-object v1, v0, Lcom/squareup/log/CrashReportingLogger;->reporter:Lcom/squareup/log/CrashReporter;

    move-object v1, p4

    .line 167
    iput-object v1, v0, Lcom/squareup/log/CrashReportingLogger;->viewHierarchyProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 168
    iput-object v1, v0, Lcom/squareup/log/CrashReportingLogger;->mortarScopeHierarchyProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 169
    iput-object v1, v0, Lcom/squareup/log/CrashReportingLogger;->featureFlagsForLogs:Lcom/squareup/log/FeatureFlagsForLogs;

    move-object v1, p6

    .line 170
    iput-object v1, v0, Lcom/squareup/log/CrashReportingLogger;->device:Lcom/squareup/util/Device;

    move-object v1, p7

    .line 171
    iput-object v1, v0, Lcom/squareup/log/CrashReportingLogger;->application:Landroid/app/Application;

    move-object v1, p8

    .line 172
    iput-object v1, v0, Lcom/squareup/log/CrashReportingLogger;->installationId:Ljava/lang/String;

    move-object v1, p9

    .line 173
    iput-object v1, v0, Lcom/squareup/log/CrashReportingLogger;->userAgent:Ljava/lang/String;

    move-object v1, p10

    .line 174
    iput-object v1, v0, Lcom/squareup/log/CrashReportingLogger;->appUpgradeDetector:Lcom/squareup/log/AppUpgradeDetector;

    move-wide v1, p11

    .line 175
    iput-wide v1, v0, Lcom/squareup/log/CrashReportingLogger;->startUptimeMs:J

    move-object/from16 v1, p13

    .line 176
    iput-object v1, v0, Lcom/squareup/log/CrashReportingLogger;->deviceSerialNumber:Ljava/lang/String;

    move-object/from16 v1, p14

    .line 177
    iput-object v1, v0, Lcom/squareup/log/CrashReportingLogger;->posBuild:Lcom/squareup/util/PosBuild;

    move-object/from16 v1, p15

    .line 178
    iput-object v1, v0, Lcom/squareup/log/CrashReportingLogger;->playServicesVersions:Lcom/squareup/firebase/versions/PlayServicesVersions;

    move-object/from16 v1, p16

    .line 179
    iput-object v1, v0, Lcom/squareup/log/CrashReportingLogger;->processUniqueId:Lcom/squareup/analytics/ProcessUniqueId;

    move-object/from16 v1, p17

    .line 180
    iput-object v1, v0, Lcom/squareup/log/CrashReportingLogger;->countryCodeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method private static diagonalBucket(Landroid/content/Context;)Ljava/lang/String;
    .locals 5

    const-string/jumbo v0, "window"

    .line 123
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/view/WindowManager;

    .line 124
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 125
    invoke-interface {p0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object p0

    .line 126
    invoke-virtual {p0, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 127
    iget p0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    mul-int p0, p0, v1

    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    mul-int v1, v1, v2

    add-int/2addr p0, v1

    int-to-double v1, p0

    invoke-static {v1, v2}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v1

    iget p0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-double v3, p0

    div-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static getKeyboardPackageName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .line 133
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "default_input_method"

    invoke-static {p0, v0}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 134
    invoke-static {p0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, "Unknown"

    :cond_0
    return-object p0
.end method

.method private static hardKeyboardHidden(Landroid/content/res/Configuration;)Ljava/lang/String;
    .locals 1

    .line 76
    iget p0, p0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    const-string p0, "YES"

    return-object p0

    :cond_1
    const-string p0, "NO"

    return-object p0

    :cond_2
    const-string p0, "UNDEFINED"

    return-object p0
.end method

.method public static logNonInjectedInfo(Landroid/app/Application;Lcom/squareup/log/CrashReporter;Lcom/squareup/util/PosBuild;Lcom/squareup/firebase/versions/PlayServicesVersions;)V
    .locals 5

    .line 259
    invoke-interface {p2}, Lcom/squareup/util/PosBuild;->getGitSha()Ljava/lang/String;

    move-result-object v0

    const-string v1, "buildSha"

    invoke-interface {p1, v1, v0}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    invoke-interface {p2}, Lcom/squareup/util/PosBuild;->getRegisterVersionName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "registerVersionName"

    invoke-interface {p1, v1, v0}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    invoke-interface {p2}, Lcom/squareup/util/PosBuild;->getRegisterVersionCode()I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    const-string v0, "registerVersionCode"

    .line 261
    invoke-interface {p1, v0, p2}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    invoke-interface {p3}, Lcom/squareup/firebase/versions/PlayServicesVersions;->getClientVersion()Ljava/lang/String;

    move-result-object p2

    const-string v0, "playSvcClient"

    invoke-interface {p1, v0, p2}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    invoke-interface {p3}, Lcom/squareup/firebase/versions/PlayServicesVersions;->getInstalledVersion()Ljava/lang/String;

    move-result-object p2

    const-string p3, "playSvcInstalled"

    invoke-interface {p1, p3, p2}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    invoke-virtual {p0}, Landroid/app/Application;->getFilesDir()Ljava/io/File;

    move-result-object p2

    .line 267
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 268
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " writeable? "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/io/File;->canWrite()Z

    move-result p2

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "dirAppFiles"

    .line 267
    invoke-interface {p1, p3, p2}, Lcom/squareup/log/CrashReporter;->logToDeviceTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object p2

    .line 270
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 271
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/io/File;->canWrite()Z

    move-result p2

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "dirEnvData"

    .line 270
    invoke-interface {p1, p3, p2}, Lcom/squareup/log/CrashReporter;->logToDeviceTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object p2

    const-string/jumbo p3, "user"

    .line 274
    invoke-virtual {p0, p3}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Landroid/os/UserManager;

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 277
    invoke-virtual {p3, p2}, Landroid/os/UserManager;->getSerialNumberForUser(Landroid/os/UserHandle;)J

    move-result-wide p2

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :cond_0
    const-string p2, "[unavailable - missing user or manager]"

    :goto_0
    const-string/jumbo p3, "userSerialNumber"

    .line 281
    invoke-interface {p1, p3, p2}, Lcom/squareup/log/CrashReporter;->logToUserTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    .line 285
    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    .line 286
    invoke-static {}, Lcom/squareup/systempermissions/SystemPermission;->values()[Lcom/squareup/systempermissions/SystemPermission;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v1, :cond_2

    aget-object v3, v0, v2

    .line 287
    iget-object v4, v3, Lcom/squareup/systempermissions/SystemPermission;->permissionName:Ljava/lang/String;

    invoke-static {v4}, Lcom/squareup/log/CrashReportingLogger;->trimPermissionName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 288
    invoke-virtual {v3, p0}, Lcom/squareup/systempermissions/SystemPermission;->hasPermission(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 289
    invoke-interface {p2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 291
    :cond_1
    invoke-interface {p3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    const-string v0, "android.permission.ACCESS_COARSE_LOCATION"

    .line 296
    invoke-static {p0, v0}, Lcom/squareup/systempermissions/SystemPermission;->isGranted(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 297
    invoke-static {v0}, Lcom/squareup/log/CrashReportingLogger;->trimPermissionName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-interface {p2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 299
    :cond_3
    invoke-static {v0}, Lcom/squareup/log/CrashReportingLogger;->trimPermissionName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-interface {p3, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 301
    :goto_3
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p2, "APP"

    const-string v0, "permsGranted"

    invoke-interface {p1, p2, v0, p0}, Lcom/squareup/log/CrashReporter;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p3, "permsNotGranted"

    invoke-interface {p1, p2, p3, p0}, Lcom/squareup/log/CrashReporter;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static msDurationToTimeString(J)Ljava/lang/String;
    .locals 12

    .line 89
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p0, p1}, Ljava/util/concurrent/TimeUnit;->toDays(J)J

    move-result-wide v0

    .line 90
    sget-object v2, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    sub-long/2addr p0, v2

    .line 91
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, p0, p1}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v2

    .line 92
    sget-object v4, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    sub-long/2addr p0, v4

    .line 93
    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, p0, p1}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v4

    .line 94
    sget-object v6, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    sub-long/2addr p0, v6

    .line 95
    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6, p0, p1}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v6

    .line 96
    sget-object v8, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v8, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v8

    sub-long/2addr p0, v8

    .line 98
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-wide/16 v9, 0x0

    cmp-long v11, v0, v9

    if-lez v11, :cond_0

    .line 100
    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, "d "

    .line 101
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    cmp-long v0, v2, v9

    if-lez v0, :cond_1

    .line 104
    invoke-virtual {v8, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, "h "

    .line 105
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    cmp-long v0, v4, v9

    if-lez v0, :cond_2

    .line 108
    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, "min "

    .line 109
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    cmp-long v0, v6, v9

    if-lez v0, :cond_3

    .line 112
    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, "s "

    .line 113
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    cmp-long v0, p0, v9

    if-lez v0, :cond_4

    .line 116
    invoke-virtual {v8, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p0, "ms"

    .line 117
    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    :cond_4
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static trimPermissionName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 306
    sget v0, Lcom/squareup/log/CrashReportingLogger;->PERMISSION_PREFIX_LENGTH:I

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public getLastEventsAsString()Ljava/lang/String;
    .locals 1

    .line 252
    iget-object v0, p0, Lcom/squareup/log/CrashReportingLogger;->reporter:Lcom/squareup/log/CrashReporter;

    invoke-interface {v0}, Lcom/squareup/log/CrashReporter;->getLastEventsAsString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOrientation()Lcom/squareup/log/OhSnapEvent$Orientation;
    .locals 2

    .line 326
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->isMainThread()Z

    move-result v0

    if-nez v0, :cond_0

    .line 327
    sget-object v0, Lcom/squareup/log/OhSnapEvent$Orientation;->UNKNOWN:Lcom/squareup/log/OhSnapEvent$Orientation;

    return-object v0

    .line 330
    :cond_0
    iget-object v0, p0, Lcom/squareup/log/CrashReportingLogger;->resources:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 336
    sget-object v0, Lcom/squareup/log/OhSnapEvent$Orientation;->UNKNOWN:Lcom/squareup/log/OhSnapEvent$Orientation;

    return-object v0

    .line 332
    :cond_1
    sget-object v0, Lcom/squareup/log/OhSnapEvent$Orientation;->LANDSCAPE:Lcom/squareup/log/OhSnapEvent$Orientation;

    return-object v0

    .line 334
    :cond_2
    sget-object v0, Lcom/squareup/log/OhSnapEvent$Orientation;->PORTRAIT:Lcom/squareup/log/OhSnapEvent$Orientation;

    return-object v0
.end method

.method public log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V
    .locals 2

    .line 184
    new-instance v0, Lcom/squareup/log/OhSnapEvent;

    invoke-virtual {p0}, Lcom/squareup/log/CrashReportingLogger;->getOrientation()Lcom/squareup/log/OhSnapEvent$Orientation;

    move-result-object v1

    invoke-direct {v0, p1, v1, p2}, Lcom/squareup/log/OhSnapEvent;-><init>(Lcom/squareup/log/OhSnapLogger$EventType;Lcom/squareup/log/OhSnapEvent$Orientation;Ljava/lang/String;)V

    .line 185
    iget-object p1, p0, Lcom/squareup/log/CrashReportingLogger;->reporter:Lcom/squareup/log/CrashReporter;

    invoke-interface {p1, v0}, Lcom/squareup/log/CrashReporter;->log(Lcom/squareup/log/OhSnapEvent;)V

    return-void
.end method

.method public logExtraDataForCrash()V
    .locals 6

    .line 197
    iget-object v0, p0, Lcom/squareup/log/CrashReportingLogger;->application:Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/log/CrashReportingLogger;->reporter:Lcom/squareup/log/CrashReporter;

    iget-object v2, p0, Lcom/squareup/log/CrashReportingLogger;->posBuild:Lcom/squareup/util/PosBuild;

    iget-object v3, p0, Lcom/squareup/log/CrashReportingLogger;->playServicesVersions:Lcom/squareup/firebase/versions/PlayServicesVersions;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/log/CrashReportingLogger;->logNonInjectedInfo(Landroid/app/Application;Lcom/squareup/log/CrashReporter;Lcom/squareup/util/PosBuild;Lcom/squareup/firebase/versions/PlayServicesVersions;)V

    .line 200
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/squareup/log/CrashReportingLogger;->startUptimeMs:J

    sub-long/2addr v0, v2

    .line 202
    iget-object v2, p0, Lcom/squareup/log/CrashReportingLogger;->reporter:Lcom/squareup/log/CrashReporter;

    invoke-static {v0, v1}, Lcom/squareup/log/CrashReportingLogger;->msDurationToTimeString(J)Ljava/lang/String;

    move-result-object v3

    const-string v4, "processUptime"

    invoke-interface {v2, v4, v3}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    iget-object v2, p0, Lcom/squareup/log/CrashReportingLogger;->reporter:Lcom/squareup/log/CrashReporter;

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    const-string v1, "processUptimeMs"

    invoke-interface {v2, v1, v0}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    iget-object v0, p0, Lcom/squareup/log/CrashReportingLogger;->reporter:Lcom/squareup/log/CrashReporter;

    iget-object v1, p0, Lcom/squareup/log/CrashReportingLogger;->userAgent:Ljava/lang/String;

    const-string/jumbo v2, "userAgent"

    invoke-interface {v0, v2, v1}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    iget-object v0, p0, Lcom/squareup/log/CrashReportingLogger;->appUpgradeDetector:Lcom/squareup/log/AppUpgradeDetector;

    iget-object v1, p0, Lcom/squareup/log/CrashReportingLogger;->reporter:Lcom/squareup/log/CrashReporter;

    invoke-virtual {v0, v1}, Lcom/squareup/log/AppUpgradeDetector;->logAppInfo(Lcom/squareup/log/CrashReporter;)V

    .line 210
    iget-object v0, p0, Lcom/squareup/log/CrashReportingLogger;->reporter:Lcom/squareup/log/CrashReporter;

    iget-object v1, p0, Lcom/squareup/log/CrashReportingLogger;->device:Lcom/squareup/util/Device;

    invoke-interface {v1}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    const-string v2, "isTablet"

    invoke-interface {v0, v2, v1}, Lcom/squareup/log/CrashReporter;->logToDeviceTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    iget-object v0, p0, Lcom/squareup/log/CrashReportingLogger;->reporter:Lcom/squareup/log/CrashReporter;

    sget-object v1, Landroid/os/Build;->BRAND:Ljava/lang/String;

    const-string v2, "brand"

    invoke-interface {v0, v2, v1}, Lcom/squareup/log/CrashReporter;->logToDeviceTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    iget-object v0, p0, Lcom/squareup/log/CrashReportingLogger;->reporter:Lcom/squareup/log/CrashReporter;

    sget-object v1, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    const-string v2, "cpu"

    invoke-interface {v0, v2, v1}, Lcom/squareup/log/CrashReporter;->logToDeviceTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    iget-object v0, p0, Lcom/squareup/log/CrashReportingLogger;->reporter:Lcom/squareup/log/CrashReporter;

    iget-object v1, p0, Lcom/squareup/log/CrashReportingLogger;->installationId:Ljava/lang/String;

    const-string v2, "installationId"

    invoke-interface {v0, v2, v1}, Lcom/squareup/log/CrashReporter;->logToDeviceTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    iget-object v0, p0, Lcom/squareup/log/CrashReportingLogger;->reporter:Lcom/squareup/log/CrashReporter;

    iget-object v1, p0, Lcom/squareup/log/CrashReportingLogger;->deviceSerialNumber:Ljava/lang/String;

    const-string v2, "deviceSerialNumber"

    invoke-interface {v0, v2, v1}, Lcom/squareup/log/CrashReporter;->logToDeviceTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    iget-object v0, p0, Lcom/squareup/log/CrashReportingLogger;->reporter:Lcom/squareup/log/CrashReporter;

    iget-object v1, p0, Lcom/squareup/log/CrashReportingLogger;->processUniqueId:Lcom/squareup/analytics/ProcessUniqueId;

    invoke-virtual {v1}, Lcom/squareup/analytics/ProcessUniqueId;->getUniqueId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "processUniqueId"

    invoke-interface {v0, v2, v1}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->isMainThread()Z

    move-result v0

    const-string v1, "diagonalBucket"

    const-string v2, "keyboardPackageName"

    const-string v3, "enabledFeatures"

    if-nez v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/squareup/log/CrashReportingLogger;->reporter:Lcom/squareup/log/CrashReporter;

    const-string v4, "Not main thread, unknown."

    invoke-interface {v0, v3, v4}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    iget-object v0, p0, Lcom/squareup/log/CrashReportingLogger;->reporter:Lcom/squareup/log/CrashReporter;

    invoke-interface {v0, v2, v4}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    iget-object v0, p0, Lcom/squareup/log/CrashReportingLogger;->reporter:Lcom/squareup/log/CrashReporter;

    invoke-interface {v0, v4}, Lcom/squareup/log/CrashReporter;->setViewHierarchy(Ljava/lang/String;)V

    .line 224
    iget-object v0, p0, Lcom/squareup/log/CrashReportingLogger;->reporter:Lcom/squareup/log/CrashReporter;

    invoke-interface {v0, v4}, Lcom/squareup/log/CrashReporter;->setMortarScopeHierarchy(Ljava/lang/String;)V

    .line 225
    iget-object v0, p0, Lcom/squareup/log/CrashReportingLogger;->reporter:Lcom/squareup/log/CrashReporter;

    invoke-interface {v0, v1, v4}, Lcom/squareup/log/CrashReporter;->logToDeviceTab(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 228
    :cond_0
    iget-object v0, p0, Lcom/squareup/log/CrashReportingLogger;->resources:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 229
    invoke-static {v0}, Lcom/squareup/log/CrashReportingLogger;->hardKeyboardHidden(Landroid/content/res/Configuration;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 231
    iget-object v4, p0, Lcom/squareup/log/CrashReportingLogger;->reporter:Lcom/squareup/log/CrashReporter;

    const-string v5, "configurationHardKeyboardHidden"

    invoke-interface {v4, v5, v0}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    :cond_1
    iget-object v0, p0, Lcom/squareup/log/CrashReportingLogger;->reporter:Lcom/squareup/log/CrashReporter;

    iget-object v4, p0, Lcom/squareup/log/CrashReportingLogger;->featureFlagsForLogs:Lcom/squareup/log/FeatureFlagsForLogs;

    invoke-virtual {v4}, Lcom/squareup/log/FeatureFlagsForLogs;->enabledFeatures()Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    iget-object v0, p0, Lcom/squareup/log/CrashReportingLogger;->reporter:Lcom/squareup/log/CrashReporter;

    iget-object v3, p0, Lcom/squareup/log/CrashReportingLogger;->application:Landroid/app/Application;

    invoke-static {v3}, Lcom/squareup/log/CrashReportingLogger;->getKeyboardPackageName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    iget-object v0, p0, Lcom/squareup/log/CrashReportingLogger;->viewHierarchyProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 240
    iget-object v2, p0, Lcom/squareup/log/CrashReportingLogger;->reporter:Lcom/squareup/log/CrashReporter;

    invoke-interface {v2, v0}, Lcom/squareup/log/CrashReporter;->setViewHierarchy(Ljava/lang/String;)V

    .line 243
    :cond_2
    iget-object v0, p0, Lcom/squareup/log/CrashReportingLogger;->reporter:Lcom/squareup/log/CrashReporter;

    iget-object v2, p0, Lcom/squareup/log/CrashReportingLogger;->mortarScopeHierarchyProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v0, v2}, Lcom/squareup/log/CrashReporter;->setMortarScopeHierarchy(Ljava/lang/String;)V

    .line 245
    iget-object v0, p0, Lcom/squareup/log/CrashReportingLogger;->reporter:Lcom/squareup/log/CrashReporter;

    iget-object v2, p0, Lcom/squareup/log/CrashReportingLogger;->application:Landroid/app/Application;

    invoke-static {v2}, Lcom/squareup/log/CrashReportingLogger;->diagonalBucket(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/CrashReporter;->logToDeviceTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    iget-object v0, p0, Lcom/squareup/log/CrashReportingLogger;->reporter:Lcom/squareup/log/CrashReporter;

    iget-object v1, p0, Lcom/squareup/log/CrashReportingLogger;->countryCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/CountryCode;

    invoke-virtual {v1}, Lcom/squareup/CountryCode;->name()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "userCountryCode"

    invoke-interface {v0, v2, v1}, Lcom/squareup/log/CrashReporter;->logToUserTab(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public logFull(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V
    .locals 4

    .line 189
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    add-int/lit16 v2, v1, 0x7d0

    .line 191
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 192
    invoke-virtual {p2, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Lcom/squareup/log/CrashReportingLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    move v1, v2

    goto :goto_0

    :cond_0
    return-void
.end method

.method public w(Ljava/lang/Throwable;)V
    .locals 1

    const/4 v0, 0x0

    .line 310
    invoke-virtual {p0, p1, v0}, Lcom/squareup/log/CrashReportingLogger;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    return-void
.end method

.method public w(Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "throwable"

    .line 316
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 317
    invoke-virtual {p0}, Lcom/squareup/log/CrashReportingLogger;->logExtraDataForCrash()V

    if-eqz p2, :cond_0

    .line 319
    sget-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->LOGGED_EXCEPTION:Lcom/squareup/log/OhSnapLogger$EventType;

    invoke-virtual {p0, v0, p2}, Lcom/squareup/log/CrashReportingLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 322
    :cond_0
    iget-object p2, p0, Lcom/squareup/log/CrashReportingLogger;->reporter:Lcom/squareup/log/CrashReporter;

    invoke-interface {p2, p1}, Lcom/squareup/log/CrashReporter;->warningThrowable(Ljava/lang/Throwable;)V

    return-void
.end method
