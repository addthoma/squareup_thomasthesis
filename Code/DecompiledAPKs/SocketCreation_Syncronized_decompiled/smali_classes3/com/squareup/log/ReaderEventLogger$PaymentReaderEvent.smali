.class Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent;
.super Lcom/squareup/log/ReaderEvent;
.source "ReaderEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/ReaderEventLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PaymentReaderEvent"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent$Builder;
    }
.end annotation


# instance fields
.field public final amountAuthorized:Ljava/lang/Long;

.field public final approvedOffline:Ljava/lang/Boolean;

.field public final brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

.field public final cardAction:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

.field public final cardBrand:Lcom/squareup/Card$Brand;

.field public final paymentResult:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

.field public final present:Ljava/lang/Boolean;

.field public final requestType:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

.field public final standardMessage:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public final startPaymentResult:Lcom/squareup/cardreader/lcr/CrPaymentResult;

.field public final tmnTransactionResult:Lcom/squareup/cardreader/lcr/TmnTransactionResult;

.field public final transactionId:Ljava/lang/String;

.field public final transactionType:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

.field public final willContinuePayment:Ljava/lang/Boolean;


# direct methods
.method private constructor <init>(Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent$Builder;)V
    .locals 1

    .line 1564
    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEvent;-><init>(Lcom/squareup/log/ReaderEvent$Builder;)V

    .line 1565
    iget-object v0, p1, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent$Builder;->event:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;

    iget-object v0, v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->paymentResult:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    iput-object v0, p0, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent;->paymentResult:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    .line 1566
    iget-object v0, p1, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent$Builder;->event:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;

    iget-object v0, v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->standardMessage:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    iput-object v0, p0, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent;->standardMessage:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 1567
    iget-object v0, p1, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent$Builder;->event:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;

    iget-object v0, v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->cardAction:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    iput-object v0, p0, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent;->cardAction:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    .line 1568
    iget-object v0, p1, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent$Builder;->event:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;

    iget-object v0, v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->approvedOffline:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent;->approvedOffline:Ljava/lang/Boolean;

    .line 1569
    iget-object v0, p1, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent$Builder;->event:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;

    iget-object v0, v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->present:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent;->present:Ljava/lang/Boolean;

    .line 1570
    iget-object v0, p1, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent$Builder;->event:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;

    iget-object v0, v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->willContinuePayment:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent;->willContinuePayment:Ljava/lang/Boolean;

    .line 1571
    iget-object v0, p1, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent$Builder;->event:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;

    iget-object v0, v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->cardInfo:Lcom/squareup/cardreader/CardInfo;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent$Builder;->event:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;

    iget-object v0, v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->cardInfo:Lcom/squareup/cardreader/CardInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardInfo;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent;->cardBrand:Lcom/squareup/Card$Brand;

    .line 1572
    iget-object v0, p1, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent$Builder;->event:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;

    iget-object v0, v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->transactionType:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    iput-object v0, p0, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent;->transactionType:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    .line 1573
    iget-object v0, p1, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent$Builder;->event:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;

    iget-object v0, v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->amountAuthorized:Ljava/lang/Long;

    iput-object v0, p0, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent;->amountAuthorized:Ljava/lang/Long;

    .line 1574
    iget-object v0, p1, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent$Builder;->event:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;

    iget-object v0, v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->startPaymentResult:Lcom/squareup/cardreader/lcr/CrPaymentResult;

    iput-object v0, p0, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent;->startPaymentResult:Lcom/squareup/cardreader/lcr/CrPaymentResult;

    .line 1575
    iget-object v0, p1, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent$Builder;->event:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;

    iget-object v0, v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    iput-object v0, p0, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent;->brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    .line 1576
    iget-object v0, p1, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent$Builder;->event:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;

    iget-object v0, v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->requestType:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    iput-object v0, p0, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent;->requestType:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    .line 1577
    iget-object v0, p1, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent$Builder;->event:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;

    iget-object v0, v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->tmnTransactionResult:Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    iput-object v0, p0, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent;->tmnTransactionResult:Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    .line 1578
    iget-object p1, p1, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent$Builder;->event:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;

    iget-object p1, p1, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->transactionId:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent;->transactionId:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent$Builder;Lcom/squareup/log/ReaderEventLogger$1;)V
    .locals 0

    .line 1547
    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent;-><init>(Lcom/squareup/log/ReaderEventLogger$PaymentReaderEvent$Builder;)V

    return-void
.end method
