.class public Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent;
.super Lcom/squareup/log/ReaderEvent;
.source "ReaderEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/ReaderEventLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FirmwareReaderEvent"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;
    }
.end annotation


# instance fields
.field public final event:I

.field public final message:Ljava/lang/String;

.field public final source:I

.field public final timestamp:J


# direct methods
.method public constructor <init>(Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;)V
    .locals 2

    .line 1199
    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEvent;-><init>(Lcom/squareup/log/ReaderEvent$Builder;)V

    .line 1200
    iget-object v0, p1, Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;->message:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent;->message:Ljava/lang/String;

    .line 1201
    iget v0, p1, Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;->source:I

    iput v0, p0, Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent;->source:I

    .line 1202
    iget v0, p1, Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;->event:I

    iput v0, p0, Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent;->event:I

    .line 1203
    iget-wide v0, p1, Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;->timestamp:J

    iput-wide v0, p0, Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent;->timestamp:J

    return-void
.end method
