.class public Lcom/squareup/log/tickets/TicketInCartUi;
.super Lcom/squareup/analytics/event/v1/TimingEvent;
.source "TicketInCartUi.java"


# instance fields
.field private final duration_ms:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .line 12
    sget-object v0, Lcom/squareup/analytics/RegisterTimingName;->OPEN_TICKETS_TICKET_IN_CART_UI:Lcom/squareup/analytics/RegisterTimingName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/TimingEvent;-><init>(Lcom/squareup/analytics/RegisterTimingName;)V

    .line 13
    iput p1, p0, Lcom/squareup/log/tickets/TicketInCartUi;->duration_ms:I

    return-void
.end method
