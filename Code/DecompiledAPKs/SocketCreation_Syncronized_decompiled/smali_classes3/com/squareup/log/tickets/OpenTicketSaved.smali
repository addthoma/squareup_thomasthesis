.class public Lcom/squareup/log/tickets/OpenTicketSaved;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "OpenTicketSaved.java"


# instance fields
.field public final is_saved_to_named_ticket_group:Z

.field public final is_saved_to_template:Z


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)V
    .locals 3

    .line 19
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKET_SAVED:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    .line 20
    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    iput-boolean v2, p0, Lcom/squareup/log/tickets/OpenTicketSaved;->is_saved_to_named_ticket_group:Z

    if-eqz p1, :cond_1

    .line 22
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ticket_template:Lcom/squareup/api/items/TicketTemplate;

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    iput-boolean v0, p0, Lcom/squareup/log/tickets/OpenTicketSaved;->is_saved_to_template:Z

    return-void
.end method

.method public constructor <init>(Lcom/squareup/tickets/OpenTicket;)V
    .locals 0

    .line 15
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getPredefinedTicket()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/log/tickets/OpenTicketSaved;-><init>(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)V

    return-void
.end method
