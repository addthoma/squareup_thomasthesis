.class public Lcom/squareup/log/MainThreadBlockedLogger;
.super Ljava/lang/Object;
.source "MainThreadBlockedLogger.java"

# interfaces
.implements Lmortar/Scoped;
.implements Lcom/squareup/ActivityListener$ResumedPausedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/log/MainThreadBlockedLogger$MainThreadBlockedEvent;
    }
.end annotation


# static fields
.field private static final BLOCKED_THREAD_THRESHOLD_MS:I = 0x1f4

.field static final SAMPLING_RATE:D = 0.1

.field static final TICK_MS:I = 0x3c


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final clock:Lcom/squareup/util/Clock;

.field private lastTickMs:J

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private rootScope:Lmortar/MortarScope;

.field private final tick:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/util/Clock;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/squareup/log/MainThreadBlockedLogger;->analytics:Lcom/squareup/analytics/Analytics;

    .line 53
    iput-object p2, p0, Lcom/squareup/log/MainThreadBlockedLogger;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 54
    iput-object p3, p0, Lcom/squareup/log/MainThreadBlockedLogger;->clock:Lcom/squareup/util/Clock;

    .line 55
    new-instance p1, Lcom/squareup/log/-$$Lambda$MainThreadBlockedLogger$LpH7LjHMkLZc5C9Y17lbd-Xtu5g;

    invoke-direct {p1, p0}, Lcom/squareup/log/-$$Lambda$MainThreadBlockedLogger$LpH7LjHMkLZc5C9Y17lbd-Xtu5g;-><init>(Lcom/squareup/log/MainThreadBlockedLogger;)V

    iput-object p1, p0, Lcom/squareup/log/MainThreadBlockedLogger;->tick:Ljava/lang/Runnable;

    return-void
.end method

.method public static synthetic lambda$LpH7LjHMkLZc5C9Y17lbd-Xtu5g(Lcom/squareup/log/MainThreadBlockedLogger;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/log/MainThreadBlockedLogger;->tick()V

    return-void
.end method

.method private postTick()V
    .locals 4

    .line 77
    iget-object v0, p0, Lcom/squareup/log/MainThreadBlockedLogger;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getUptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/log/MainThreadBlockedLogger;->lastTickMs:J

    .line 78
    iget-object v0, p0, Lcom/squareup/log/MainThreadBlockedLogger;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/log/MainThreadBlockedLogger;->tick:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3c

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private stopTick()V
    .locals 2

    .line 82
    iget-object v0, p0, Lcom/squareup/log/MainThreadBlockedLogger;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/log/MainThreadBlockedLogger;->tick:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    return-void
.end method

.method private tick()V
    .locals 7

    .line 86
    iget-object v0, p0, Lcom/squareup/log/MainThreadBlockedLogger;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getUptimeMillis()J

    move-result-wide v0

    .line 87
    iget-wide v2, p0, Lcom/squareup/log/MainThreadBlockedLogger;->lastTickMs:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3c

    sub-long/2addr v0, v2

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-wide/16 v4, 0x1f4

    cmp-long v6, v0, v4

    if-lez v6, :cond_0

    const/4 v4, 0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    if-eqz v4, :cond_1

    .line 91
    invoke-virtual {p0}, Lcom/squareup/log/MainThreadBlockedLogger;->isSampled()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/squareup/log/MainThreadBlockedLogger;->rootScope:Lmortar/MortarScope;

    if-eqz v4, :cond_1

    .line 92
    invoke-static {v4}, Lmortar/ScopeSpy;->visibleScreens(Lmortar/MortarScope;)Ljava/lang/String;

    move-result-object v4

    .line 93
    iget-object v5, p0, Lcom/squareup/log/MainThreadBlockedLogger;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v6, Lcom/squareup/log/MainThreadBlockedLogger$MainThreadBlockedEvent;

    invoke-direct {v6, v4, v0, v1}, Lcom/squareup/log/MainThreadBlockedLogger$MainThreadBlockedEvent;-><init>(Ljava/lang/String;J)V

    invoke-interface {v5, v6}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    .line 94
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v5, v3

    aput-object v4, v5, v2

    const-string v0, "Main thread blocked for %dms, visible screens: %s"

    invoke-static {v0, v5}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 98
    :cond_1
    invoke-direct {p0}, Lcom/squareup/log/MainThreadBlockedLogger;->postTick()V

    return-void
.end method


# virtual methods
.method isSampled()Z
    .locals 5

    .line 104
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    const-wide v2, 0x3fb999999999999aL    # 0.1

    cmpg-double v4, v0, v2

    if-gez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 59
    iput-object p1, p0, Lcom/squareup/log/MainThreadBlockedLogger;->rootScope:Lmortar/MortarScope;

    return-void
.end method

.method public onExitScope()V
    .locals 1

    const/4 v0, 0x0

    .line 65
    iput-object v0, p0, Lcom/squareup/log/MainThreadBlockedLogger;->rootScope:Lmortar/MortarScope;

    return-void
.end method

.method public onPause()V
    .locals 0

    .line 73
    invoke-direct {p0}, Lcom/squareup/log/MainThreadBlockedLogger;->stopTick()V

    return-void
.end method

.method public onResume()V
    .locals 0

    .line 69
    invoke-direct {p0}, Lcom/squareup/log/MainThreadBlockedLogger;->postTick()V

    return-void
.end method
