.class public final Lcom/squareup/log/itemizedrefund/ItemizedRefundActionEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "ItemizedRefundActionEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001B\u0013\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005R\u0019\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\n\n\u0002\u0010\u0008\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/log/itemizedrefund/ItemizedRefundActionEvent;",
        "Lcom/squareup/eventstream/v1/EventStreamEvent;",
        "item_names",
        "",
        "",
        "([Ljava/lang/String;)V",
        "getItem_names",
        "()[Ljava/lang/String;",
        "[Ljava/lang/String;",
        "feature-analytics_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final item_names:[Ljava/lang/String;


# direct methods
.method public constructor <init>([Ljava/lang/String;)V
    .locals 2

    const-string v0, "item_names"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    const-string v1, "Transaction: Itemized Refund"

    .line 6
    invoke-direct {p0, v0, v1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/log/itemizedrefund/ItemizedRefundActionEvent;->item_names:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getItem_names()[Ljava/lang/String;
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/squareup/log/itemizedrefund/ItemizedRefundActionEvent;->item_names:[Ljava/lang/String;

    return-object v0
.end method
