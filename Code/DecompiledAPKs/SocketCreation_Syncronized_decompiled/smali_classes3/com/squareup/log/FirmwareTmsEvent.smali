.class public final Lcom/squareup/log/FirmwareTmsEvent;
.super Lcom/squareup/log/ReaderEvent;
.source "FirmwareTmsEvent.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/log/FirmwareTmsEvent$Builder;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001:\u0001\u000bB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\t\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u0008\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/log/FirmwareTmsEvent;",
        "Lcom/squareup/log/ReaderEvent;",
        "builder",
        "Lcom/squareup/log/FirmwareTmsEvent$Builder;",
        "(Lcom/squareup/log/FirmwareTmsEvent$Builder;)V",
        "tmsCountryCode",
        "Lcom/squareup/CountryCode;",
        "getTmsCountryCode",
        "()Lcom/squareup/CountryCode;",
        "userCountryCode",
        "getUserCountryCode",
        "Builder",
        "cardreader_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final tmsCountryCode:Lcom/squareup/CountryCode;

.field private final userCountryCode:Lcom/squareup/CountryCode;


# direct methods
.method public constructor <init>(Lcom/squareup/log/FirmwareTmsEvent$Builder;)V
    .locals 1

    const-string v0, "builder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    move-object v0, p1

    check-cast v0, Lcom/squareup/log/ReaderEvent$Builder;

    invoke-direct {p0, v0}, Lcom/squareup/log/ReaderEvent;-><init>(Lcom/squareup/log/ReaderEvent$Builder;)V

    .line 9
    invoke-virtual {p1}, Lcom/squareup/log/FirmwareTmsEvent$Builder;->getTmsCountryCode$cardreader_release()Lcom/squareup/CountryCode;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/log/FirmwareTmsEvent;->tmsCountryCode:Lcom/squareup/CountryCode;

    .line 10
    invoke-virtual {p1}, Lcom/squareup/log/FirmwareTmsEvent$Builder;->getUserCountryCode$cardreader_release()Lcom/squareup/CountryCode;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iput-object p1, p0, Lcom/squareup/log/FirmwareTmsEvent;->userCountryCode:Lcom/squareup/CountryCode;

    return-void
.end method


# virtual methods
.method public final getTmsCountryCode()Lcom/squareup/CountryCode;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/log/FirmwareTmsEvent;->tmsCountryCode:Lcom/squareup/CountryCode;

    return-object v0
.end method

.method public final getUserCountryCode()Lcom/squareup/CountryCode;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/log/FirmwareTmsEvent;->userCountryCode:Lcom/squareup/CountryCode;

    return-object v0
.end method
