.class final Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1;
.super Ljava/lang/Object;
.source "RealConnectedScalesRepository.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository;-><init>(Lcom/squareup/scales/ScaleTracker;Landroid/content/res/Resources;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "Lio/reactivex/Observable<",
        "TT;>;",
        "Lio/reactivex/ObservableSource<",
        "TR;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealConnectedScalesRepository.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealConnectedScalesRepository.kt\ncom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1\n+ 2 RxKotlin.kt\ncom/squareup/util/rx2/Observables\n*L\n1#1,103:1\n57#2,4:104\n*E\n*S KotlinDebug\n*F\n+ 1 RealConnectedScalesRepository.kt\ncom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1\n*L\n71#1,4:104\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u00020\u00012 \u0010\u0004\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0005 \u0006*\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00020\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "",
        "Lcom/squareup/connectedscalesdata/ConnectedScale;",
        "supportedScales",
        "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
        "kotlin.jvm.PlatformType",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository;


# direct methods
.method constructor <init>(Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1;->this$0:Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lio/reactivex/Observable;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
            ">;>;)",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/connectedscalesdata/ConnectedScale;",
            ">;>;"
        }
    .end annotation

    const-string v0, "supportedScales"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    new-instance v0, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1$latestUnits$1;

    invoke-direct {v0, p0}, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1$latestUnits$1;-><init>(Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "supportedScales.switchMa\u2026.toList() }\n            }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    sget-object v1, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 105
    check-cast p1, Lio/reactivex/ObservableSource;

    check-cast v0, Lio/reactivex/ObservableSource;

    .line 106
    new-instance v1, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1$$special$$inlined$combineLatest$1;

    invoke-direct {v1, p0}, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1$$special$$inlined$combineLatest$1;-><init>(Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1;)V

    check-cast v1, Lio/reactivex/functions/BiFunction;

    .line 104
    invoke-static {p1, v0, v1}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "Observable.combineLatest\u2026ineFunction(t1, t2) }\n  )"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Lio/reactivex/Observable;

    invoke-virtual {p0, p1}, Lcom/squareup/connectedscalesdata/RealConnectedScalesRepository$connectedScales$1;->apply(Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
