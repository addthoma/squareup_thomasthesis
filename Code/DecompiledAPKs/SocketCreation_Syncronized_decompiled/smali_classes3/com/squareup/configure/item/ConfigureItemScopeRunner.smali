.class public Lcom/squareup/configure/item/ConfigureItemScopeRunner;
.super Ljava/lang/Object;
.source "ConfigureItemScopeRunner.java"

# interfaces
.implements Lmortar/Scoped;


# instance fields
.field private final availableDiscountsStore:Lcom/squareup/payment/AvailableDiscountsStore;

.field private final currency:Lcom/squareup/protos/common/CurrencyCode;

.field private displayedConfigureItemDetailScreen:Z

.field private final host:Lcom/squareup/configure/item/ConfigureItemHost;

.field private final navigator:Lcom/squareup/configure/item/ConfigureItemNavigator;

.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

.field private final orderItemBundleKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation
.end field

.field private final posContainer:Lcom/squareup/ui/main/PosContainer;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private state:Lcom/squareup/configure/item/ConfigureItemState;

.field private final voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

.field private final workingItemBundleKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/BundleKey;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/configure/item/ConfigureItemHost;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/BundleKey;Lcom/squareup/configure/item/ConfigureItemNavigator;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/payment/AvailableDiscountsStore;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/checkout/CartItem;",
            ">;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/configure/item/ConfigureItemHost;",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingItem;",
            ">;",
            "Lcom/squareup/configure/item/ConfigureItemNavigator;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            "Lcom/squareup/payment/AvailableDiscountsStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 43
    iput-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->displayedConfigureItemDetailScreen:Z

    .line 50
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->currency:Lcom/squareup/protos/common/CurrencyCode;

    .line 51
    iput-object p2, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->orderItemBundleKey:Lcom/squareup/BundleKey;

    .line 52
    iput-object p3, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 53
    iput-object p4, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    .line 54
    iput-object p5, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    .line 55
    iput-object p6, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->workingItemBundleKey:Lcom/squareup/BundleKey;

    .line 56
    iput-object p7, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->navigator:Lcom/squareup/configure/item/ConfigureItemNavigator;

    .line 57
    iput-object p8, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->posContainer:Lcom/squareup/ui/main/PosContainer;

    .line 58
    iput-object p9, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 59
    iput-object p10, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->availableDiscountsStore:Lcom/squareup/payment/AvailableDiscountsStore;

    return-void
.end method

.method private createOrLoadState(Lcom/squareup/configure/item/ConfigureItemScope;)Lcom/squareup/configure/item/ConfigureItemState;
    .locals 8

    .line 138
    iget-boolean v0, p1, Lcom/squareup/configure/item/ConfigureItemScope;->cartItem:Z

    if-eqz v0, :cond_1

    .line 139
    iget v0, p1, Lcom/squareup/configure/item/ConfigureItemScope;->indexToEdit:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 140
    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    iget-object v4, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    iget-object v5, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->currency:Lcom/squareup/protos/common/CurrencyCode;

    iget v6, p1, Lcom/squareup/configure/item/ConfigureItemScope;->indexToEdit:I

    iget-object v7, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->orderItemBundleKey:Lcom/squareup/BundleKey;

    invoke-static/range {v2 .. v7}, Lcom/squareup/configure/item/ConfigureOrderItemState;->loadedOrderItemState(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/configure/item/ConfigureItemHost;Lcom/squareup/protos/common/CurrencyCode;ILcom/squareup/BundleKey;)Lcom/squareup/configure/item/ConfigureOrderItemState;

    move-result-object p1

    return-object p1

    .line 143
    :cond_0
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->currency:Lcom/squareup/protos/common/CurrencyCode;

    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->orderItemBundleKey:Lcom/squareup/BundleKey;

    invoke-static {p1, v0, v1, v2, v3}, Lcom/squareup/configure/item/ConfigureOrderItemState;->emptyOrderItemState(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/configure/item/ConfigureItemHost;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/BundleKey;)Lcom/squareup/configure/item/ConfigureOrderItemState;

    move-result-object p1

    return-object p1

    .line 147
    :cond_1
    iget-object v0, p1, Lcom/squareup/configure/item/ConfigureItemScope;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    if-eqz v0, :cond_2

    .line 148
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->currency:Lcom/squareup/protos/common/CurrencyCode;

    iget-object p1, p1, Lcom/squareup/configure/item/ConfigureItemScope;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->workingItemBundleKey:Lcom/squareup/BundleKey;

    invoke-static {v0, v1, p1, v2}, Lcom/squareup/configure/item/ConfigureWorkingItemState;->loadedWorkingItemState(Lcom/squareup/configure/item/ConfigureItemHost;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/configure/item/WorkingItem;Lcom/squareup/BundleKey;)Lcom/squareup/configure/item/ConfigureWorkingItemState;

    move-result-object p1

    return-object p1

    .line 150
    :cond_2
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->currency:Lcom/squareup/protos/common/CurrencyCode;

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->workingItemBundleKey:Lcom/squareup/BundleKey;

    invoke-static {p1, v0, v1}, Lcom/squareup/configure/item/ConfigureWorkingItemState;->emptyWorkingItemState(Lcom/squareup/configure/item/ConfigureItemHost;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/BundleKey;)Lcom/squareup/configure/item/ConfigureWorkingItemState;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method getAvailableCartTaxes()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation

    .line 111
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->state:Lcom/squareup/configure/item/ConfigureItemState;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getAppliedTaxes()Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    .line 112
    invoke-interface {v1}, Lcom/squareup/configure/item/ConfigureItemHost;->getAvailableTaxes()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->shouldShowInclusiveTaxesInCart()Z

    move-result v2

    .line 111
    invoke-static {v0, v1, v2}, Lcom/squareup/configure/item/ConfigureItemTaxUtils;->getAvailableCartTaxes(Ljava/util/Map;Ljava/util/List;Z)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getState()Lcom/squareup/configure/item/ConfigureItemState;
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->state:Lcom/squareup/configure/item/ConfigureItemState;

    return-object v0
.end method

.method getToggleableDiscountsForItem()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation

    .line 122
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    .line 123
    invoke-interface {v0}, Lcom/squareup/configure/item/ConfigureItemHost;->getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->availableDiscountsStore:Lcom/squareup/payment/AvailableDiscountsStore;

    .line 124
    invoke-virtual {v1}, Lcom/squareup/payment/AvailableDiscountsStore;->getFixedPercentageDiscounts()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->state:Lcom/squareup/configure/item/ConfigureItemState;

    .line 125
    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemState;->getAppliedDiscounts()Ljava/util/Map;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    .line 126
    invoke-virtual {v3}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isCompEnabled()Z

    move-result v3

    iget-object v4, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->availableDiscountsStore:Lcom/squareup/payment/AvailableDiscountsStore;

    .line 127
    invoke-virtual {v4}, Lcom/squareup/payment/AvailableDiscountsStore;->isDiscountApplicationIdEnabled()Z

    move-result v4

    .line 122
    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/configure/item/ConfigureItemDiscountUtils;->toggleableDiscountsForItem(Ljava/util/Map;Ljava/util/List;Ljava/util/Map;ZZ)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public isService()Z
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->state:Lcom/squareup/configure/item/ConfigureItemState;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->isService()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$onEnterScope$0$ConfigureItemScopeRunner(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 67
    instance-of v0, p1, Lcom/squareup/configure/item/ConfigureItemDetailScreen;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/squareup/configure/item/AppointmentConfigureItemDetailScreen;

    if-nez v0, :cond_0

    instance-of p1, p1, Lcom/squareup/configure/item/InvoiceConfigureItemDetailScreen;

    if-eqz p1, :cond_1

    :cond_0
    const/4 p1, 0x1

    .line 71
    iput-boolean p1, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->displayedConfigureItemDetailScreen:Z

    :cond_1
    return-void
.end method

.method public onCancelConfigureItemPriceScreen()V
    .locals 1

    .line 92
    iget-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->displayedConfigureItemDetailScreen:Z

    if-nez v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->clearFlyByAnimationData()V

    .line 94
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->navigator:Lcom/squareup/configure/item/ConfigureItemNavigator;

    invoke-interface {v0}, Lcom/squareup/configure/item/ConfigureItemNavigator;->exit()V

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->navigator:Lcom/squareup/configure/item/ConfigureItemNavigator;

    invoke-interface {v0}, Lcom/squareup/configure/item/ConfigureItemNavigator;->goBack()V

    return-void
.end method

.method public onCommitConfigureItemPriceScreen()V
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->navigator:Lcom/squareup/configure/item/ConfigureItemNavigator;

    invoke-interface {v0}, Lcom/squareup/configure/item/ConfigureItemNavigator;->goBack()V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 63
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemScope;

    invoke-direct {p0, v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->createOrLoadState(Lcom/squareup/configure/item/ConfigureItemScope;)Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->state:Lcom/squareup/configure/item/ConfigureItemState;

    .line 64
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->state:Lcom/squareup/configure/item/ConfigureItemState;

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->posContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-static {v0}, Lcom/squareup/ui/main/PosContainers;->nextScreen(Lcom/squareup/ui/main/PosContainer;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemScopeRunner$-tnEDuBopIicUbnW3yuvBxNknOg;

    invoke-direct {v1, p0}, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemScopeRunner$-tnEDuBopIicUbnW3yuvBxNknOg;-><init>(Lcom/squareup/configure/item/ConfigureItemScopeRunner;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
