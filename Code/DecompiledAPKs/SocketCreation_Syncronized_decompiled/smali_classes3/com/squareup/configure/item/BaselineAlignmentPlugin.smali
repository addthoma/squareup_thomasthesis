.class public final Lcom/squareup/configure/item/BaselineAlignmentPlugin;
.super Ljava/lang/Object;
.source "BaselineAlignmentPlugin.kt"

# interfaces
.implements Lcom/squareup/noho/NohoEditRow$Plugin;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0018\u0010\u0010\u001a\u00020\u000b2\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u00020\u0008X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\t\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/configure/item/BaselineAlignmentPlugin;",
        "Lcom/squareup/noho/NohoEditRow$Plugin;",
        "iconPlugin",
        "Lcom/squareup/noho/IconPlugin;",
        "(Lcom/squareup/noho/IconPlugin;)V",
        "editText",
        "Lcom/squareup/noho/NohoEditRow;",
        "isClickable",
        "",
        "()Z",
        "attach",
        "",
        "measure",
        "Lcom/squareup/noho/NohoEditRow$PluginSize;",
        "editRect",
        "Landroid/graphics/Rect;",
        "onDraw",
        "canvas",
        "Landroid/graphics/Canvas;",
        "drawingInfo",
        "Lcom/squareup/noho/NohoEditRow$DrawingInfo;",
        "configure-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private editText:Lcom/squareup/noho/NohoEditRow;

.field private final iconPlugin:Lcom/squareup/noho/IconPlugin;

.field private final isClickable:Z


# direct methods
.method public constructor <init>(Lcom/squareup/noho/IconPlugin;)V
    .locals 1

    const-string v0, "iconPlugin"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/configure/item/BaselineAlignmentPlugin;->iconPlugin:Lcom/squareup/noho/IconPlugin;

    .line 34
    iget-object p1, p0, Lcom/squareup/configure/item/BaselineAlignmentPlugin;->iconPlugin:Lcom/squareup/noho/IconPlugin;

    invoke-virtual {p1}, Lcom/squareup/noho/IconPlugin;->isClickable()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/configure/item/BaselineAlignmentPlugin;->isClickable:Z

    return-void
.end method


# virtual methods
.method public attach(Lcom/squareup/noho/NohoEditRow;)V
    .locals 1

    const-string v0, "editText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    iput-object p1, p0, Lcom/squareup/configure/item/BaselineAlignmentPlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    return-void
.end method

.method public description(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 1

    const-string v0, "editDescription"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-static {p0, p1}, Lcom/squareup/noho/NohoEditRow$Plugin$DefaultImpls;->description(Lcom/squareup/noho/NohoEditRow$Plugin;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public detach(Lcom/squareup/noho/NohoEditRow;)V
    .locals 1

    const-string v0, "editText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-static {p0, p1}, Lcom/squareup/noho/NohoEditRow$Plugin$DefaultImpls;->detach(Lcom/squareup/noho/NohoEditRow$Plugin;Lcom/squareup/noho/NohoEditRow;)V

    return-void
.end method

.method public focusChanged()V
    .locals 0

    .line 13
    invoke-static {p0}, Lcom/squareup/noho/NohoEditRow$Plugin$DefaultImpls;->focusChanged(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    return-void
.end method

.method public isClickable()Z
    .locals 1

    .line 34
    iget-boolean v0, p0, Lcom/squareup/configure/item/BaselineAlignmentPlugin;->isClickable:Z

    return v0
.end method

.method public measure(Landroid/graphics/Rect;)Lcom/squareup/noho/NohoEditRow$PluginSize;
    .locals 1

    const-string v0, "editRect"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    iget-object v0, p0, Lcom/squareup/configure/item/BaselineAlignmentPlugin;->iconPlugin:Lcom/squareup/noho/IconPlugin;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/IconPlugin;->measure(Landroid/graphics/Rect;)Lcom/squareup/noho/NohoEditRow$PluginSize;

    move-result-object p1

    return-object p1
.end method

.method public onClick()Z
    .locals 1

    .line 13
    invoke-static {p0}, Lcom/squareup/noho/NohoEditRow$Plugin$DefaultImpls;->onClick(Lcom/squareup/noho/NohoEditRow$Plugin;)Z

    move-result v0

    return v0
.end method

.method public onDraw(Landroid/graphics/Canvas;Lcom/squareup/noho/NohoEditRow$DrawingInfo;)V
    .locals 3

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "drawingInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-virtual {p2}, Lcom/squareup/noho/NohoEditRow$DrawingInfo;->getPluginRect()Landroid/graphics/Rect;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/configure/item/BaselineAlignmentPlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    if-nez v1, :cond_0

    const-string v2, "editText"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Lcom/squareup/noho/NohoEditRow;->getBaseline()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 30
    invoke-virtual {p2}, Lcom/squareup/noho/NohoEditRow$DrawingInfo;->getPluginRect()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p2}, Lcom/squareup/noho/NohoEditRow$DrawingInfo;->getPluginRect()Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Lcom/squareup/configure/item/BaselineAlignmentPlugin;->iconPlugin:Lcom/squareup/noho/IconPlugin;

    invoke-virtual {v2}, Lcom/squareup/noho/IconPlugin;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 31
    iget-object v0, p0, Lcom/squareup/configure/item/BaselineAlignmentPlugin;->iconPlugin:Lcom/squareup/noho/IconPlugin;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/noho/IconPlugin;->onDraw(Landroid/graphics/Canvas;Lcom/squareup/noho/NohoEditRow$DrawingInfo;)V

    return-void
.end method
