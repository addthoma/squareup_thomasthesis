.class final Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1$success$1;
.super Ljava/lang/Object;
.source "ConfigureItemDetailScreen.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1;->success()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/protos/client/Employee;",
        "it",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1$success$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1$success$1;

    invoke-direct {v0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1$success$1;-><init>()V

    sput-object v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1$success$1;->INSTANCE:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1$success$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/protos/client/Employee;)Lcom/squareup/util/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/Employee;",
            ")",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/protos/client/Employee;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1359
    sget-object v0, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/util/Optional$Companion;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1355
    check-cast p1, Lcom/squareup/protos/client/Employee;

    invoke-virtual {p0, p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1$success$1;->apply(Lcom/squareup/protos/client/Employee;)Lcom/squareup/util/Optional;

    move-result-object p1

    return-object p1
.end method
