.class public final Lcom/squareup/configure/item/ScaleLogEventKt;
.super Ljava/lang/Object;
.source "ScaleLogEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u001a\u000c\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0002\u001a\u000e\u0010\u0003\u001a\u0004\u0018\u00010\u0004*\u00020\u0002H\u0002\u00a8\u0006\u0005"
    }
    d2 = {
        "toMeasurementMethod",
        "Lcom/squareup/configure/item/MeasurementMethod;",
        "Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;",
        "toMeasurementType",
        "",
        "configure-item_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$toMeasurementMethod(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;)Lcom/squareup/configure/item/MeasurementMethod;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/configure/item/ScaleLogEventKt;->toMeasurementMethod(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;)Lcom/squareup/configure/item/MeasurementMethod;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toMeasurementType(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/configure/item/ScaleLogEventKt;->toMeasurementType(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static final toMeasurementMethod(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;)Lcom/squareup/configure/item/MeasurementMethod;
    .locals 1

    .line 80
    sget-object v0, Lcom/squareup/configure/item/ScaleLogEventKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    .line 82
    sget-object p0, Lcom/squareup/configure/item/MeasurementMethod;->MANUAL:Lcom/squareup/configure/item/MeasurementMethod;

    goto :goto_0

    .line 81
    :cond_0
    sget-object p0, Lcom/squareup/configure/item/MeasurementMethod;->SCALE:Lcom/squareup/configure/item/MeasurementMethod;

    :goto_0
    return-object p0
.end method

.method private static final toMeasurementType(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;)Ljava/lang/String;
    .locals 1

    .line 95
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->READ_FROM_SCALE:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    if-ne p0, v0, :cond_0

    const-string p0, "net"

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method
