.class public Lcom/squareup/configure/item/ConfigureItemScope$ComponentFactory;
.super Ljava/lang/Object;
.source "ConfigureItemScope.java"

# interfaces
.implements Lcom/squareup/ui/WithComponent$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/configure/item/ConfigureItemScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ComponentFactory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Object;
    .locals 1

    .line 118
    check-cast p2, Lcom/squareup/configure/item/ConfigureItemScope;

    .line 119
    new-instance v0, Lcom/squareup/configure/item/ConfigureItemScope$Module;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v0, p2}, Lcom/squareup/configure/item/ConfigureItemScope$Module;-><init>(Lcom/squareup/configure/item/ConfigureItemScope;)V

    .line 120
    const-class p2, Lcom/squareup/configure/item/ConfigureItemScope$ParentComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/configure/item/ConfigureItemScope$ParentComponent;

    invoke-interface {p1, v0}, Lcom/squareup/configure/item/ConfigureItemScope$ParentComponent;->configureItem(Lcom/squareup/configure/item/ConfigureItemScope$Module;)Lcom/squareup/configure/item/ConfigureItemScope$Component;

    move-result-object p1

    return-object p1
.end method
