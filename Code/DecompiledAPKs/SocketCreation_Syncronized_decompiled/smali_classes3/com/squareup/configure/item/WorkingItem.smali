.class public Lcom/squareup/configure/item/WorkingItem;
.super Ljava/lang/Object;
.source "WorkingItem.java"


# instance fields
.field public final appliedDiscounts:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation
.end field

.field private appliedTaxes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation
.end field

.field private final cartLevelDiningOption:Lcom/squareup/checkout/DiningOption;

.field public currentVariablePrice:Lcom/squareup/protos/common/Money;

.field private final defaultTaxes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation
.end field

.field private final employee:Lcom/squareup/protos/client/Employee;

.field private hasHiddenModifier:Z

.field public final imageUrl:Ljava/lang/String;

.field private final isEcomAvailable:Z

.field public final isGiftCard:Z

.field public final itemDescription:Ljava/lang/String;

.field public final itemId:Ljava/lang/String;

.field public itemOptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;"
        }
    .end annotation
.end field

.field public final itemType:Lcom/squareup/api/items/Item$Type;

.field public final merchantObjectCatalogToken:Ljava/lang/String;

.field public final modifierLists:Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/checkout/OrderModifierList;",
            ">;"
        }
    .end annotation
.end field

.field public final name:Ljava/lang/String;

.field public note:Ljava/lang/String;

.field private final orderItemBuilder:Lcom/squareup/checkout/CartItem$Builder;

.field public overridePrice:Lcom/squareup/protos/common/Money;

.field public quantity:Ljava/math/BigDecimal;

.field public quantityEntryType:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

.field private requiresModifierSelection:Z

.field private ruleBasedTaxes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation
.end field

.field private selectedDiningOption:Lcom/squareup/checkout/DiningOption;

.field public final selectedModifiers:Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/checkout/OrderModifier;",
            ">;>;"
        }
    .end annotation
.end field

.field public selectedVariation:Lcom/squareup/checkout/OrderVariation;

.field public final skipConfigureItemDetailScreenSetting:Z

.field private final taxRules:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderTaxRule;",
            ">;"
        }
    .end annotation
.end field

.field private final userEditedTaxIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public variations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/checkout/OrderVariation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/models/CatalogItem;Ljava/lang/String;Lcom/squareup/protos/client/bills/Itemization$BackingDetails;Lcom/squareup/api/items/MenuCategory;Ljava/util/List;Ljava/util/List;Ljava/util/Map;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/checkout/DiningOption;Lcom/squareup/util/Res;Lcom/squareup/protos/client/Employee;Ljava/util/Map;Ljava/util/List;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/Itemization$BackingDetails;",
            "Lcom/squareup/api/items/MenuCategory;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTax;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderTaxRule;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierList;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;",
            ">;>;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/ItemModifierOverride;",
            ">;",
            "Lcom/squareup/checkout/DiningOption;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/protos/client/Employee;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    const/4 v8, 0x0

    .line 158
    invoke-direct/range {v0 .. v16}, Lcom/squareup/configure/item/WorkingItem;-><init>(Lcom/squareup/shared/catalog/models/CatalogItem;Ljava/lang/String;Lcom/squareup/protos/client/bills/Itemization$BackingDetails;Lcom/squareup/api/items/MenuCategory;Ljava/util/List;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/checkout/DiningOption;Lcom/squareup/util/Res;Lcom/squareup/protos/client/Employee;Ljava/util/Map;Ljava/util/List;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/shared/catalog/models/CatalogItem;Ljava/lang/String;Lcom/squareup/protos/client/bills/Itemization$BackingDetails;Lcom/squareup/api/items/MenuCategory;Ljava/util/List;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/checkout/DiningOption;Lcom/squareup/util/Res;Lcom/squareup/protos/client/Employee;Ljava/util/Map;Ljava/util/List;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/Itemization$BackingDetails;",
            "Lcom/squareup/api/items/MenuCategory;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTax;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderTaxRule;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/Itemization$Event;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierList;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;",
            ">;>;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/ItemModifierOverride;",
            ">;",
            "Lcom/squareup/checkout/DiningOption;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/protos/client/Employee;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    move-object/from16 v1, p7

    move-object/from16 v2, p13

    .line 180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v3, 0x0

    .line 111
    iput-boolean v3, v0, Lcom/squareup/configure/item/WorkingItem;->requiresModifierSelection:Z

    .line 131
    sget-object v4, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    iput-object v4, v0, Lcom/squareup/configure/item/WorkingItem;->quantity:Ljava/math/BigDecimal;

    .line 132
    sget-object v4, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->MANUALLY_ENTERED:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    iput-object v4, v0, Lcom/squareup/configure/item/WorkingItem;->quantityEntryType:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    .line 183
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItem;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItem;->getId()Ljava/lang/String;

    move-result-object v4

    :goto_0
    iput-object v4, v0, Lcom/squareup/configure/item/WorkingItem;->itemId:Ljava/lang/String;

    .line 184
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItem;->getName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/squareup/configure/item/WorkingItem;->name:Ljava/lang/String;

    .line 185
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItem;->getDescription()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/squareup/configure/item/WorkingItem;->itemDescription:Ljava/lang/String;

    .line 186
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItem;->isGiftCard()Z

    move-result v4

    iput-boolean v4, v0, Lcom/squareup/configure/item/WorkingItem;->isGiftCard:Z

    .line 187
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItem;->getItemType()Lcom/squareup/api/items/Item$Type;

    move-result-object v4

    iput-object v4, v0, Lcom/squareup/configure/item/WorkingItem;->itemType:Lcom/squareup/api/items/Item$Type;

    move-object v4, p2

    .line 188
    iput-object v4, v0, Lcom/squareup/configure/item/WorkingItem;->imageUrl:Ljava/lang/String;

    move-object/from16 v5, p14

    .line 189
    iput-object v5, v0, Lcom/squareup/configure/item/WorkingItem;->employee:Lcom/squareup/protos/client/Employee;

    .line 190
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItem;->skipsModifierScreen()Z

    move-result v5

    iput-boolean v5, v0, Lcom/squareup/configure/item/WorkingItem;->skipConfigureItemDetailScreenSetting:Z

    move-object/from16 v5, p12

    .line 193
    iput-object v5, v0, Lcom/squareup/configure/item/WorkingItem;->cartLevelDiningOption:Lcom/squareup/checkout/DiningOption;

    move-object/from16 v5, p6

    .line 196
    iput-object v5, v0, Lcom/squareup/configure/item/WorkingItem;->taxRules:Ljava/util/List;

    .line 197
    new-instance v5, Ljava/util/LinkedHashSet;

    invoke-direct {v5}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v5, v0, Lcom/squareup/configure/item/WorkingItem;->userEditedTaxIds:Ljava/util/Set;

    .line 198
    new-instance v5, Ljava/util/LinkedHashMap;

    invoke-direct {v5}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v5, v0, Lcom/squareup/configure/item/WorkingItem;->defaultTaxes:Ljava/util/Map;

    .line 199
    new-instance v5, Ljava/util/LinkedHashMap;

    invoke-direct {v5}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v5, v0, Lcom/squareup/configure/item/WorkingItem;->ruleBasedTaxes:Ljava/util/Map;

    .line 200
    new-instance v5, Ljava/util/LinkedHashMap;

    invoke-direct {v5}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v5, v0, Lcom/squareup/configure/item/WorkingItem;->appliedTaxes:Ljava/util/Map;

    move-object/from16 v5, p5

    .line 201
    invoke-direct {p0, v5}, Lcom/squareup/configure/item/WorkingItem;->initializeTaxes(Ljava/util/List;)V

    .line 204
    new-instance v5, Ljava/util/LinkedHashMap;

    invoke-direct {v5, v1}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    iput-object v5, v0, Lcom/squareup/configure/item/WorkingItem;->appliedDiscounts:Ljava/util/Map;

    .line 208
    iget-object v5, v0, Lcom/squareup/configure/item/WorkingItem;->appliedDiscounts:Ljava/util/Map;

    invoke-static {v5}, Lcom/squareup/checkout/Discounts;->removeDiscountsThatCanBeAppliedToOnlyOneItem(Ljava/util/Map;)V

    .line 211
    invoke-static/range {p10 .. p11}, Lcom/squareup/checkout/OrderModifierList;->modifierListsFrom(Ljava/util/Map;Ljava/util/Map;)Ljava/util/SortedMap;

    move-result-object v5

    iput-object v5, v0, Lcom/squareup/configure/item/WorkingItem;->modifierLists:Ljava/util/SortedMap;

    .line 212
    new-instance v5, Ljava/util/TreeMap;

    invoke-direct {v5}, Ljava/util/TreeMap;-><init>()V

    iput-object v5, v0, Lcom/squareup/configure/item/WorkingItem;->selectedModifiers:Ljava/util/SortedMap;

    .line 214
    iget-object v5, v0, Lcom/squareup/configure/item/WorkingItem;->modifierLists:Ljava/util/SortedMap;

    invoke-interface {v5}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/checkout/OrderModifierList;

    .line 215
    new-instance v7, Ljava/util/TreeMap;

    invoke-direct {v7}, Ljava/util/TreeMap;-><init>()V

    .line 216
    iget-object v8, v6, Lcom/squareup/checkout/OrderModifierList;->modifiers:Ljava/util/SortedMap;

    invoke-interface {v8}, Ljava/util/SortedMap;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_1
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    const/4 v10, 0x1

    if-eqz v9, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    .line 217
    iget-object v11, v6, Lcom/squareup/checkout/OrderModifierList;->modifiers:Ljava/util/SortedMap;

    invoke-interface {v11, v9}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/squareup/checkout/OrderModifier;

    if-nez v11, :cond_2

    goto :goto_2

    .line 220
    :cond_2
    invoke-virtual {v11}, Lcom/squareup/checkout/OrderModifier;->getOnByDefault()Z

    move-result v12

    if-eqz v12, :cond_1

    .line 221
    invoke-interface {v7, v9, v11}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    invoke-virtual {v11}, Lcom/squareup/checkout/OrderModifier;->getHideFromCustomer()Z

    move-result v9

    if-eqz v9, :cond_1

    const/4 v3, 0x1

    goto :goto_2

    .line 227
    :cond_3
    invoke-interface {v7}, Ljava/util/SortedMap;->size()I

    move-result v8

    invoke-virtual {v6}, Lcom/squareup/checkout/OrderModifierList;->getMinSelectedModifiers()I

    move-result v9

    if-ge v8, v9, :cond_4

    .line 228
    iput-boolean v10, v0, Lcom/squareup/configure/item/WorkingItem;->requiresModifierSelection:Z

    .line 230
    :cond_4
    iget-object v8, v0, Lcom/squareup/configure/item/WorkingItem;->selectedModifiers:Ljava/util/SortedMap;

    iget v6, v6, Lcom/squareup/checkout/OrderModifierList;->clientOrdinal:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v8, v6, v7}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 232
    :cond_5
    iput-boolean v3, v0, Lcom/squareup/configure/item/WorkingItem;->hasHiddenModifier:Z

    .line 235
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 236
    invoke-interface/range {p9 .. p9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-object/from16 v7, p15

    .line 238
    invoke-static {v6, v7}, Lcom/squareup/configure/item/WorkingItem;->getCatalogMeasurementUnit(Lcom/squareup/shared/catalog/models/CatalogItemVariation;Ljava/util/Map;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v8

    .line 245
    invoke-static {v2, v8}, Lcom/squareup/quantity/UnitDisplayData;->fromCatalogMeasurementUnit(Lcom/squareup/util/Res;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object v9

    .line 246
    invoke-static {v2, v6}, Lcom/squareup/payment/OrderVariationNames;->displayNameFromCatalogVariation(Lcom/squareup/util/Res;Lcom/squareup/shared/catalog/models/CatalogItemVariation;)Ljava/lang/String;

    move-result-object v10

    .line 244
    invoke-static {v6, v8, v9, v10}, Lcom/squareup/checkout/OrderVariation;->of(Lcom/squareup/shared/catalog/models/CatalogItemVariation;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/quantity/UnitDisplayData;Ljava/lang/String;)Lcom/squareup/checkout/OrderVariation;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 248
    :cond_6
    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/configure/item/WorkingItem;->variations:Ljava/util/List;

    .line 249
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v0, Lcom/squareup/configure/item/WorkingItem;->itemOptions:Ljava/util/List;

    .line 250
    iget-object v2, v0, Lcom/squareup/configure/item/WorkingItem;->itemOptions:Ljava/util/List;

    move-object/from16 v3, p16

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 252
    new-instance v2, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    move-object v3, p1

    invoke-direct {v2, p1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;-><init>(Lcom/squareup/shared/catalog/models/CatalogItem;)V

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->isEcomAvailable()Z

    move-result v2

    iput-boolean v2, v0, Lcom/squareup/configure/item/WorkingItem;->isEcomAvailable:Z

    .line 253
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItem;->getMerchantCatalogObjectToken()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/configure/item/WorkingItem;->merchantObjectCatalogToken:Ljava/lang/String;

    .line 256
    new-instance v2, Lcom/squareup/checkout/CartItem$Builder;

    invoke-direct {v2}, Lcom/squareup/checkout/CartItem$Builder;-><init>()V

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItem;->getColor()Ljava/lang/String;

    move-result-object v5

    .line 257
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItem;->getAbbreviationOrAbbreviatedName()Ljava/lang/String;

    move-result-object v3

    iget-object v6, v0, Lcom/squareup/configure/item/WorkingItem;->itemId:Ljava/lang/String;

    iget-object v7, v0, Lcom/squareup/configure/item/WorkingItem;->name:Ljava/lang/String;

    iget-object v8, v0, Lcom/squareup/configure/item/WorkingItem;->itemDescription:Ljava/lang/String;

    move-object/from16 p9, v2

    move-object/from16 p10, v5

    move-object/from16 p11, v3

    move-object/from16 p12, v6

    move-object/from16 p13, v7

    move-object/from16 p14, v8

    move-object/from16 p15, p2

    .line 256
    invoke-virtual/range {p9 .. p15}, Lcom/squareup/checkout/CartItem$Builder;->of(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v2

    move-object/from16 v3, p3

    .line 259
    invoke-virtual {v2, v3}, Lcom/squareup/checkout/CartItem$Builder;->backingDetails(Lcom/squareup/protos/client/bills/Itemization$BackingDetails;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v2

    move-object/from16 v3, p4

    .line 260
    invoke-virtual {v2, v3}, Lcom/squareup/checkout/CartItem$Builder;->category(Lcom/squareup/api/items/MenuCategory;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/configure/item/WorkingItem;->orderItemBuilder:Lcom/squareup/checkout/CartItem$Builder;

    if-eqz p8, :cond_8

    .line 265
    invoke-interface/range {p8 .. p8}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_7
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 266
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 267
    invoke-interface {v1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 268
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Itemization$Event;

    if-eqz v3, :cond_7

    .line 270
    iget-object v4, v0, Lcom/squareup/configure/item/WorkingItem;->orderItemBuilder:Lcom/squareup/checkout/CartItem$Builder;

    invoke-virtual {v4, v3}, Lcom/squareup/checkout/CartItem$Builder;->addEvent(Lcom/squareup/protos/client/bills/Itemization$Event;)Lcom/squareup/checkout/CartItem$Builder;

    goto :goto_4

    :cond_8
    return-void
.end method

.method private static getCatalogMeasurementUnit(Lcom/squareup/shared/catalog/models/CatalogItemVariation;Ljava/util/Map;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
            ">;)",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;"
        }
    .end annotation

    .line 500
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getCatalogMeasurementUnitToken()Ljava/lang/String;

    move-result-object p0

    .line 503
    invoke-static {p0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 504
    invoke-interface {p1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method private initializeTaxes(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTax;",
            ">;)V"
        }
    .end annotation

    .line 486
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogTax;

    .line 487
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogTax;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 488
    invoke-static {v0}, Lcom/squareup/checkout/Tax$Builder;->from(Lcom/squareup/shared/catalog/models/CatalogTax;)Lcom/squareup/checkout/Tax$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/checkout/Tax$Builder;->build()Lcom/squareup/checkout/Tax;

    move-result-object v0

    .line 489
    iget-object v1, p0, Lcom/squareup/configure/item/WorkingItem;->defaultTaxes:Ljava/util/Map;

    iget-object v2, v0, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 493
    :cond_1
    iget-object p1, p0, Lcom/squareup/configure/item/WorkingItem;->itemId:Ljava/lang/String;

    if-nez p1, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/squareup/configure/item/WorkingItem;->defaultTaxes:Ljava/util/Map;

    iget-object v2, p0, Lcom/squareup/configure/item/WorkingItem;->taxRules:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/configure/item/WorkingItem;->cartLevelDiningOption:Lcom/squareup/checkout/DiningOption;

    .line 494
    invoke-static {p1, v0, v1, v2, v3}, Lcom/squareup/util/ConditionalTaxesHelper;->updateRuleBasedTaxes(Ljava/lang/String;ZLjava/util/Map;Ljava/util/List;Lcom/squareup/checkout/DiningOption;)Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/WorkingItem;->ruleBasedTaxes:Ljava/util/Map;

    .line 495
    iget-object p1, p0, Lcom/squareup/configure/item/WorkingItem;->ruleBasedTaxes:Ljava/util/Map;

    iget-object v0, p0, Lcom/squareup/configure/item/WorkingItem;->appliedTaxes:Ljava/util/Map;

    iget-object v1, p0, Lcom/squareup/configure/item/WorkingItem;->userEditedTaxIds:Ljava/util/Set;

    invoke-static {p1, v0, v1}, Lcom/squareup/util/ConditionalTaxesHelper;->updateAppliedTaxes(Ljava/util/Map;Ljava/util/Map;Ljava/util/Set;)Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/WorkingItem;->appliedTaxes:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public ensureIdPair()Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 324
    iget-object v0, p0, Lcom/squareup/configure/item/WorkingItem;->orderItemBuilder:Lcom/squareup/checkout/CartItem$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem$Builder;->ensureIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    return-object v0
.end method

.method public finish()Lcom/squareup/checkout/CartItem;
    .locals 5

    .line 338
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 340
    iget-object v1, p0, Lcom/squareup/configure/item/WorkingItem;->orderItemBuilder:Lcom/squareup/checkout/CartItem$Builder;

    iget-object v2, p0, Lcom/squareup/configure/item/WorkingItem;->variations:Ljava/util/List;

    .line 341
    invoke-virtual {v1, v2}, Lcom/squareup/checkout/CartItem$Builder;->variations(Ljava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/configure/item/WorkingItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    .line 342
    invoke-virtual {v1, v2}, Lcom/squareup/checkout/CartItem$Builder;->selectedVariation(Lcom/squareup/checkout/OrderVariation;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/configure/item/WorkingItem;->overridePrice:Lcom/squareup/protos/common/Money;

    .line 343
    invoke-virtual {v1, v2}, Lcom/squareup/checkout/CartItem$Builder;->overridePrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/configure/item/WorkingItem;->selectedDiningOption:Lcom/squareup/checkout/DiningOption;

    .line 344
    invoke-virtual {v1, v2}, Lcom/squareup/checkout/CartItem$Builder;->selectedDiningOption(Lcom/squareup/checkout/DiningOption;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/configure/item/WorkingItem;->defaultTaxes:Ljava/util/Map;

    .line 345
    invoke-virtual {v1, v2}, Lcom/squareup/checkout/CartItem$Builder;->defaultTaxes(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/configure/item/WorkingItem;->ruleBasedTaxes:Ljava/util/Map;

    .line 346
    invoke-virtual {v1, v2}, Lcom/squareup/checkout/CartItem$Builder;->ruleBasedTaxes(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/configure/item/WorkingItem;->appliedTaxes:Ljava/util/Map;

    .line 347
    invoke-virtual {v1, v2}, Lcom/squareup/checkout/CartItem$Builder;->appliedTaxes(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/configure/item/WorkingItem;->userEditedTaxIds:Ljava/util/Set;

    .line 348
    invoke-virtual {v1, v2}, Lcom/squareup/checkout/CartItem$Builder;->userEditedTaxIds(Ljava/util/Set;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/configure/item/WorkingItem;->appliedDiscounts:Ljava/util/Map;

    .line 349
    invoke-virtual {v1, v2}, Lcom/squareup/checkout/CartItem$Builder;->appliedDiscounts(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/configure/item/WorkingItem;->note:Ljava/lang/String;

    .line 350
    invoke-virtual {v1, v2}, Lcom/squareup/checkout/CartItem$Builder;->notes(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/configure/item/WorkingItem;->quantity:Ljava/math/BigDecimal;

    .line 351
    invoke-virtual {v1, v2}, Lcom/squareup/checkout/CartItem$Builder;->quantity(Ljava/math/BigDecimal;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/configure/item/WorkingItem;->quantityEntryType:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    .line 352
    invoke-virtual {v1, v2}, Lcom/squareup/checkout/CartItem$Builder;->quantityEntryType(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v1

    .line 353
    invoke-virtual {v1, v0}, Lcom/squareup/checkout/CartItem$Builder;->createdAt(Ljava/util/Date;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/configure/item/WorkingItem;->modifierLists:Ljava/util/SortedMap;

    .line 354
    invoke-virtual {v1, v2}, Lcom/squareup/checkout/CartItem$Builder;->modifierLists(Ljava/util/SortedMap;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/configure/item/WorkingItem;->employee:Lcom/squareup/protos/client/Employee;

    .line 355
    invoke-static {v2, v0}, Lcom/squareup/checkout/util/ItemizationEvents;->creationEvent(Lcom/squareup/protos/client/Employee;Ljava/util/Date;)Lcom/squareup/protos/client/bills/Itemization$Event;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/checkout/CartItem$Builder;->addEvent(Lcom/squareup/protos/client/bills/Itemization$Event;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/configure/item/WorkingItem;->selectedModifiers:Ljava/util/SortedMap;

    .line 356
    invoke-virtual {v0, v1}, Lcom/squareup/checkout/CartItem$Builder;->selectedModifiers(Ljava/util/SortedMap;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/configure/item/WorkingItem;->skipConfigureItemDetailScreenSetting:Z

    .line 357
    invoke-virtual {v0, v1}, Lcom/squareup/checkout/CartItem$Builder;->skipModifierDetailScreen(Z)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/configure/item/WorkingItem;->hasHiddenModifier:Z

    .line 358
    invoke-virtual {v0, v1}, Lcom/squareup/checkout/CartItem$Builder;->hasHiddenModifier(Z)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/configure/item/WorkingItem;->itemOptions:Ljava/util/List;

    .line 359
    invoke-virtual {v0, v1}, Lcom/squareup/checkout/CartItem$Builder;->itemOptions(Ljava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/configure/item/WorkingItem;->isEcomAvailable:Z

    .line 360
    invoke-virtual {v0, v1}, Lcom/squareup/checkout/CartItem$Builder;->isEcomAvailable(Z)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/configure/item/WorkingItem;->merchantObjectCatalogToken:Ljava/lang/String;

    .line 361
    invoke-virtual {v0, v1}, Lcom/squareup/checkout/CartItem$Builder;->merchantCatalogObjectToken(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 363
    invoke-virtual {p0}, Lcom/squareup/configure/item/WorkingItem;->isCustomAmountItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 364
    iget-object v0, p0, Lcom/squareup/configure/item/WorkingItem;->orderItemBuilder:Lcom/squareup/checkout/CartItem$Builder;

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;->CUSTOM_AMOUNT:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    invoke-virtual {v0, v1}, Lcom/squareup/checkout/CartItem$Builder;->backingType(Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;)Lcom/squareup/checkout/CartItem$Builder;

    .line 367
    :cond_0
    iget-object v0, p0, Lcom/squareup/configure/item/WorkingItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->isVariablePriced()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 368
    iget-object v0, p0, Lcom/squareup/configure/item/WorkingItem;->orderItemBuilder:Lcom/squareup/checkout/CartItem$Builder;

    iget-object v1, p0, Lcom/squareup/configure/item/WorkingItem;->currentVariablePrice:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/checkout/CartItem$Builder;->variablePrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/CartItem$Builder;

    .line 371
    :cond_1
    iget-object v0, p0, Lcom/squareup/configure/item/WorkingItem;->itemType:Lcom/squareup/api/items/Item$Type;

    sget-object v1, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    if-ne v0, v1, :cond_2

    .line 374
    iget-object v0, p0, Lcom/squareup/configure/item/WorkingItem;->orderItemBuilder:Lcom/squareup/checkout/CartItem$Builder;

    new-instance v1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;-><init>()V

    new-instance v2, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;-><init>()V

    iget-object v3, p0, Lcom/squareup/configure/item/WorkingItem;->itemId:Ljava/lang/String;

    .line 377
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->service_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/configure/item/WorkingItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    .line 378
    invoke-virtual {v3}, Lcom/squareup/checkout/OrderVariation;->getDuration()Lorg/threeten/bp/Duration;

    move-result-object v3

    invoke-virtual {v3}, Lorg/threeten/bp/Duration;->toMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->service_duration(Ljava/lang/Long;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/configure/item/WorkingItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    .line 379
    invoke-virtual {v3}, Lcom/squareup/checkout/OrderVariation;->getItemVariation()Lcom/squareup/api/items/ItemVariation;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/api/items/ItemVariation;->pricing_type:Lcom/squareup/api/items/PricingType;

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->pricing_type(Lcom/squareup/api/items/PricingType;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/configure/item/WorkingItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    .line 380
    invoke-virtual {v3}, Lcom/squareup/checkout/OrderVariation;->getTransitionTime()Lorg/threeten/bp/Duration;

    move-result-object v3

    invoke-virtual {v3}, Lorg/threeten/bp/Duration;->toMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->transition_time_duration(Ljava/lang/Long;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/configure/item/WorkingItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    .line 381
    invoke-virtual {v3}, Lcom/squareup/checkout/OrderVariation;->getIntermissions()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->intermissions(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;

    move-result-object v2

    const/4 v3, 0x0

    .line 382
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;

    move-result-object v2

    .line 383
    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    move-result-object v2

    .line 375
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->appointments_service_details(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;

    move-result-object v1

    .line 384
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    move-result-object v1

    .line 385
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    .line 374
    invoke-virtual {v0, v1, v2}, Lcom/squareup/checkout/CartItem$Builder;->featureDetails(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;Ljava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    .line 389
    :cond_2
    iget-object v0, p0, Lcom/squareup/configure/item/WorkingItem;->orderItemBuilder:Lcom/squareup/checkout/CartItem$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object v0

    return-object v0
.end method

.method public finishWithOnlyFixedPrice()Lcom/squareup/checkout/CartItem;
    .locals 1

    .line 315
    invoke-virtual {p0}, Lcom/squareup/configure/item/WorkingItem;->selectOnlyVariation()V

    .line 316
    invoke-virtual {p0}, Lcom/squareup/configure/item/WorkingItem;->finish()Lcom/squareup/checkout/CartItem;

    move-result-object v0

    return-object v0
.end method

.method public getAppliedTaxes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation

    .line 410
    iget-object v0, p0, Lcom/squareup/configure/item/WorkingItem;->appliedTaxes:Ljava/util/Map;

    return-object v0
.end method

.method public getQuantity()Ljava/math/BigDecimal;
    .locals 1

    .line 282
    iget-object v0, p0, Lcom/squareup/configure/item/WorkingItem;->quantity:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getQuantityEntryType()Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;
    .locals 1

    .line 290
    iget-object v0, p0, Lcom/squareup/configure/item/WorkingItem;->quantityEntryType:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    return-object v0
.end method

.method public getRequiresModifierSelection()Z
    .locals 1

    .line 471
    iget-boolean v0, p0, Lcom/squareup/configure/item/WorkingItem;->requiresModifierSelection:Z

    return v0
.end method

.method getRuleBasedTaxes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation

    .line 402
    iget-object v0, p0, Lcom/squareup/configure/item/WorkingItem;->ruleBasedTaxes:Ljava/util/Map;

    return-object v0
.end method

.method public getSelectedDiningOption()Lcom/squareup/checkout/DiningOption;
    .locals 1

    .line 414
    iget-object v0, p0, Lcom/squareup/configure/item/WorkingItem;->selectedDiningOption:Lcom/squareup/checkout/DiningOption;

    return-object v0
.end method

.method public getUserEditedTaxIds()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 406
    iget-object v0, p0, Lcom/squareup/configure/item/WorkingItem;->userEditedTaxIds:Ljava/util/Set;

    return-object v0
.end method

.method public hasTaxRules()Z
    .locals 1

    .line 278
    iget-object v0, p0, Lcom/squareup/configure/item/WorkingItem;->taxRules:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public isCustomAmountItem()Z
    .locals 2

    .line 479
    iget-object v0, p0, Lcom/squareup/configure/item/WorkingItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    if-eqz v0, :cond_0

    sget-object v1, Lcom/squareup/checkout/OrderVariation;->CUSTOM_ITEM_VARIATION:Lcom/squareup/checkout/OrderVariation;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public selectOnlyVariation()V
    .locals 1

    const/4 v0, 0x0

    .line 298
    invoke-virtual {p0, v0}, Lcom/squareup/configure/item/WorkingItem;->selectVariation(I)V

    return-void
.end method

.method public selectVariation(I)V
    .locals 1

    .line 302
    iget-object v0, p0, Lcom/squareup/configure/item/WorkingItem;->variations:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkout/OrderVariation;

    iput-object p1, p0, Lcom/squareup/configure/item/WorkingItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    .line 303
    iget-object p1, p0, Lcom/squareup/configure/item/WorkingItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->getQuantityUnit()Lcom/squareup/orders/model/Order$QuantityUnit;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/checkout/OrderVariation;->setQuantityUnitOverride(Lcom/squareup/orders/model/Order$QuantityUnit;)V

    .line 305
    iget-object p1, p0, Lcom/squareup/configure/item/WorkingItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 306
    sget-object p1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    iput-object p1, p0, Lcom/squareup/configure/item/WorkingItem;->quantity:Ljava/math/BigDecimal;

    :cond_0
    return-void
.end method

.method public setDiscountApplied(Lcom/squareup/checkout/Discount;Z)V
    .locals 1

    const/4 v0, 0x0

    .line 427
    invoke-virtual {p0, p1, p2, v0}, Lcom/squareup/configure/item/WorkingItem;->setDiscountApplied(Lcom/squareup/checkout/Discount;ZLcom/squareup/protos/client/Employee;)V

    return-void
.end method

.method public setDiscountApplied(Lcom/squareup/checkout/Discount;ZLcom/squareup/protos/client/Employee;)V
    .locals 1

    if-eqz p2, :cond_0

    .line 432
    iget-object p2, p0, Lcom/squareup/configure/item/WorkingItem;->appliedDiscounts:Ljava/util/Map;

    iget-object v0, p1, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-interface {p2, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p3, :cond_1

    .line 434
    iget-object p2, p0, Lcom/squareup/configure/item/WorkingItem;->orderItemBuilder:Lcom/squareup/checkout/CartItem$Builder;

    invoke-virtual {p1}, Lcom/squareup/checkout/Discount;->id()Ljava/lang/String;

    move-result-object p1

    invoke-static {p3, p1}, Lcom/squareup/checkout/util/ItemizationEvents;->discountAddEvent(Lcom/squareup/protos/client/Employee;Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Event;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/checkout/CartItem$Builder;->addEvent(Lcom/squareup/protos/client/bills/Itemization$Event;)Lcom/squareup/checkout/CartItem$Builder;

    goto :goto_0

    .line 437
    :cond_0
    iget-object p2, p0, Lcom/squareup/configure/item/WorkingItem;->appliedDiscounts:Ljava/util/Map;

    iget-object v0, p1, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-interface {p2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p3, :cond_1

    .line 439
    iget-object p2, p0, Lcom/squareup/configure/item/WorkingItem;->orderItemBuilder:Lcom/squareup/checkout/CartItem$Builder;

    invoke-virtual {p1}, Lcom/squareup/checkout/Discount;->id()Ljava/lang/String;

    move-result-object p1

    invoke-static {p3, p1}, Lcom/squareup/checkout/util/ItemizationEvents;->discountRemoveEvent(Lcom/squareup/protos/client/Employee;Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Event;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/checkout/CartItem$Builder;->addEvent(Lcom/squareup/protos/client/bills/Itemization$Event;)Lcom/squareup/checkout/CartItem$Builder;

    :cond_1
    :goto_0
    return-void
.end method

.method public setGiftCardDetails(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;)Lcom/squareup/configure/item/WorkingItem;
    .locals 1

    .line 333
    iget-object v0, p0, Lcom/squareup/configure/item/WorkingItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {v0, p1}, Lcom/squareup/checkout/OrderVariation;->setGiftCardDetails(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;)V

    return-object p0
.end method

.method public setHasHiddenModifier(Z)V
    .locals 0

    .line 475
    iput-boolean p1, p0, Lcom/squareup/configure/item/WorkingItem;->hasHiddenModifier:Z

    return-void
.end method

.method public setIdPair(Lcom/squareup/protos/client/IdPair;)V
    .locals 1

    .line 320
    iget-object v0, p0, Lcom/squareup/configure/item/WorkingItem;->orderItemBuilder:Lcom/squareup/checkout/CartItem$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/checkout/CartItem$Builder;->idPair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/checkout/CartItem$Builder;

    return-void
.end method

.method public setOrderItemName(Ljava/lang/String;)Lcom/squareup/configure/item/WorkingItem;
    .locals 1

    .line 328
    iget-object v0, p0, Lcom/squareup/configure/item/WorkingItem;->orderItemBuilder:Lcom/squareup/checkout/CartItem$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/checkout/CartItem$Builder;->itemName(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    return-object p0
.end method

.method public setQuantity(Ljava/math/BigDecimal;)V
    .locals 0

    .line 286
    iput-object p1, p0, Lcom/squareup/configure/item/WorkingItem;->quantity:Ljava/math/BigDecimal;

    return-void
.end method

.method public setQuantityEntryType(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;)V
    .locals 0

    .line 294
    iput-object p1, p0, Lcom/squareup/configure/item/WorkingItem;->quantityEntryType:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    return-void
.end method

.method public setSelectedDiningOption(Lcom/squareup/checkout/DiningOption;)V
    .locals 4

    const-string v0, "selected item-level dining option"

    .line 445
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 446
    iget-object v0, p0, Lcom/squareup/configure/item/WorkingItem;->selectedDiningOption:Lcom/squareup/checkout/DiningOption;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/configure/item/WorkingItem;->cartLevelDiningOption:Lcom/squareup/checkout/DiningOption;

    .line 447
    invoke-virtual {p1, v0}, Lcom/squareup/checkout/DiningOption;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    .line 448
    :cond_0
    invoke-virtual {p1, v0}, Lcom/squareup/checkout/DiningOption;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 449
    :goto_0
    iput-object p1, p0, Lcom/squareup/configure/item/WorkingItem;->selectedDiningOption:Lcom/squareup/checkout/DiningOption;

    .line 450
    iget-object v0, p0, Lcom/squareup/configure/item/WorkingItem;->taxRules:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz v1, :cond_2

    iget-boolean v0, p0, Lcom/squareup/configure/item/WorkingItem;->isGiftCard:Z

    if-nez v0, :cond_2

    .line 451
    iget-object v0, p0, Lcom/squareup/configure/item/WorkingItem;->itemId:Ljava/lang/String;

    .line 452
    invoke-virtual {p0}, Lcom/squareup/configure/item/WorkingItem;->isCustomAmountItem()Z

    move-result v1

    iget-object v2, p0, Lcom/squareup/configure/item/WorkingItem;->defaultTaxes:Ljava/util/Map;

    iget-object v3, p0, Lcom/squareup/configure/item/WorkingItem;->taxRules:Ljava/util/List;

    .line 451
    invoke-static {v0, v1, v2, v3, p1}, Lcom/squareup/util/ConditionalTaxesHelper;->updateRuleBasedTaxes(Ljava/lang/String;ZLjava/util/Map;Ljava/util/List;Lcom/squareup/checkout/DiningOption;)Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/WorkingItem;->ruleBasedTaxes:Ljava/util/Map;

    .line 454
    iget-object p1, p0, Lcom/squareup/configure/item/WorkingItem;->ruleBasedTaxes:Ljava/util/Map;

    iget-object v0, p0, Lcom/squareup/configure/item/WorkingItem;->appliedTaxes:Ljava/util/Map;

    iget-object v1, p0, Lcom/squareup/configure/item/WorkingItem;->userEditedTaxIds:Ljava/util/Set;

    invoke-static {p1, v0, v1}, Lcom/squareup/util/ConditionalTaxesHelper;->updateAppliedTaxes(Ljava/util/Map;Ljava/util/Map;Ljava/util/Set;)Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/WorkingItem;->appliedTaxes:Ljava/util/Map;

    :cond_2
    return-void
.end method

.method public setTaxApplied(Lcom/squareup/checkout/Tax;Z)V
    .locals 1

    if-eqz p2, :cond_0

    .line 419
    iget-object p2, p0, Lcom/squareup/configure/item/WorkingItem;->appliedTaxes:Ljava/util/Map;

    iget-object v0, p1, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {p2, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 421
    :cond_0
    iget-object p2, p0, Lcom/squareup/configure/item/WorkingItem;->appliedTaxes:Ljava/util/Map;

    iget-object v0, p1, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {p2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 423
    :goto_0
    iget-object p2, p0, Lcom/squareup/configure/item/WorkingItem;->userEditedTaxIds:Ljava/util/Set;

    iget-object p1, p1, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {p2, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public setZeroCurrentAmount(Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 2

    const-wide/16 v0, 0x0

    .line 311
    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/WorkingItem;->currentVariablePrice:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method public skipsConfigureItemDetailScreen()Z
    .locals 2

    .line 465
    iget-boolean v0, p0, Lcom/squareup/configure/item/WorkingItem;->skipConfigureItemDetailScreenSetting:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/configure/item/WorkingItem;->requiresModifierSelection:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/configure/item/WorkingItem;->variations:Ljava/util/List;

    .line 467
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public total()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 393
    iget-object v0, p0, Lcom/squareup/configure/item/WorkingItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    if-eqz v0, :cond_1

    .line 394
    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->isVariablePriced()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/configure/item/WorkingItem;->currentVariablePrice:Lcom/squareup/protos/common/Money;

    if-nez v0, :cond_0

    goto :goto_0

    .line 397
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/configure/item/WorkingItem;->finish()Lcom/squareup/checkout/CartItem;

    move-result-object v0

    .line 398
    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->total()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method
