.class public Lcom/squareup/configure/item/ConfigureItemVoidScreen$Presenter;
.super Lcom/squareup/configure/item/VoidCompPresenter;
.source "ConfigureItemVoidScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/configure/item/ConfigureItemVoidScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/configure/item/VoidCompPresenter<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final navigator:Lcom/squareup/configure/item/ConfigureItemNavigator;

.field private final scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

.field private screen:Lcom/squareup/configure/item/ConfigureItemVoidScreen;


# direct methods
.method constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/configure/item/ConfigureItemNavigator;Lcom/squareup/util/Res;Lcom/squareup/configure/item/ConfigureItemScopeRunner;Lcom/squareup/tickets/voidcomp/VoidReasonsCache;Lcom/squareup/permissions/EmployeeManagement;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 56
    invoke-direct {p0, p3, p5}, Lcom/squareup/configure/item/VoidCompPresenter;-><init>(Lcom/squareup/util/Res;Lcom/squareup/tickets/voidcomp/CogsCache;)V

    .line 57
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemVoidScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 58
    iput-object p2, p0, Lcom/squareup/configure/item/ConfigureItemVoidScreen$Presenter;->navigator:Lcom/squareup/configure/item/ConfigureItemNavigator;

    .line 59
    iput-object p4, p0, Lcom/squareup/configure/item/ConfigureItemVoidScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    .line 60
    iput-object p6, p0, Lcom/squareup/configure/item/ConfigureItemVoidScreen$Presenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    return-void
.end method


# virtual methods
.method protected getCancelButtonTextResourceId()I
    .locals 1

    .line 87
    sget v0, Lcom/squareup/configure/item/R$string;->void_item:I

    return v0
.end method

.method protected getPrimaryButtonTextResourceId()I
    .locals 1

    .line 91
    sget v0, Lcom/squareup/configure/item/R$string;->void_initial:I

    return v0
.end method

.method protected getReasonHeaderTextResourceId()I
    .locals 1

    .line 95
    sget v0, Lcom/squareup/configure/item/R$string;->void_uppercase_reason:I

    return v0
.end method

.method public synthetic lambda$null$0$ConfigureItemVoidScreen$Presenter(Lcom/squareup/util/Optional;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 74
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemVoidScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    .line 75
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemVoidScreen$Presenter;->getCheckedReason()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1}, Lcom/squareup/util/Optional;->getValueOrNull()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/Employee;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/configure/item/ConfigureItemState;->voidItem(Ljava/lang/String;Lcom/squareup/protos/client/Employee;)V

    .line 76
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemVoidScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->VOID_ITEMIZATION_VOIDED:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 77
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemVoidScreen$Presenter;->navigator:Lcom/squareup/configure/item/ConfigureItemNavigator;

    invoke-interface {p1}, Lcom/squareup/configure/item/ConfigureItemNavigator;->exit()V

    return-void
.end method

.method public synthetic lambda$onPrimaryClicked$1$ConfigureItemVoidScreen$Presenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemVoidScreen$Presenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemVoidScreen$Presenter;->screen:Lcom/squareup/configure/item/ConfigureItemVoidScreen;

    invoke-static {v1}, Lcom/squareup/configure/item/ConfigureItemVoidScreen;->access$000(Lcom/squareup/configure/item/ConfigureItemVoidScreen;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/permissions/EmployeeManagement;->oneEmployeeProtoByToken(Ljava/lang/String;)Lio/reactivex/Maybe;

    move-result-object v0

    sget-object v1, Lcom/squareup/configure/item/-$$Lambda$Dy0PHuYjqCY7wofLHWfV9mlLNt0;->INSTANCE:Lcom/squareup/configure/item/-$$Lambda$Dy0PHuYjqCY7wofLHWfV9mlLNt0;

    .line 71
    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object v0

    .line 72
    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->toSingle(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemVoidScreen$Presenter$iooY5m7Nqlrg6MDHBeZndaSlC-I;

    invoke-direct {v1, p0}, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemVoidScreen$Presenter$iooY5m7Nqlrg6MDHBeZndaSlC-I;-><init>(Lcom/squareup/configure/item/ConfigureItemVoidScreen$Presenter;)V

    .line 73
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public onCancelClicked()V
    .locals 2

    .line 82
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemVoidScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->VOID_ITEMIZATION_CANCELED:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 83
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemVoidScreen$Presenter;->navigator:Lcom/squareup/configure/item/ConfigureItemNavigator;

    invoke-interface {v0}, Lcom/squareup/configure/item/ConfigureItemNavigator;->goBack()V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 64
    invoke-super {p0, p1}, Lcom/squareup/configure/item/VoidCompPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 65
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/configure/item/ConfigureItemVoidScreen;

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemVoidScreen$Presenter;->screen:Lcom/squareup/configure/item/ConfigureItemVoidScreen;

    return-void
.end method

.method public onPrimaryClicked()V
    .locals 2

    .line 69
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemVoidScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemVoidScreen$Presenter$A49LK_FpQT2HbT_bXQ0qqpYoKQ0;

    invoke-direct {v1, p0}, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemVoidScreen$Presenter$A49LK_FpQT2HbT_bXQ0qqpYoKQ0;-><init>(Lcom/squareup/configure/item/ConfigureItemVoidScreen$Presenter;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
