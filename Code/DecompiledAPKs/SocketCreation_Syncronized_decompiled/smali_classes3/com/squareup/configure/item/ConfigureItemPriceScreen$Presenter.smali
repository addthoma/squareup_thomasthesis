.class Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "ConfigureItemPriceScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/configure/item/ConfigureItemPriceScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter$PriceEntryKeypadListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/configure/item/ConfigureItemPriceView;",
        ">;"
    }
.end annotation


# static fields
.field private static final EDITING_AMOUNT_KEY:Ljava/lang/String; = "EDITING_AMOUNT_KEY"

.field private static final INITIAL_AMOUNT_KEY:Ljava/lang/String; = "INITIAL_AMOUNT_KEY"

.field private static final USER_INTERACTED_KEY:Ljava/lang/String; = "USER_INTERACTED_KEY"


# instance fields
.field private final actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final currency:Lcom/squareup/protos/common/CurrencyCode;

.field private final host:Lcom/squareup/configure/item/ConfigureItemHost;

.field private initialAmount:Lcom/squareup/protos/common/Money;

.field protected listener:Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter$PriceEntryKeypadListener;

.field private newAmount:Lcom/squareup/protos/common/Money;

.field private final perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private final res:Lcom/squareup/util/Res;

.field private final scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field userHasInteracted:Z

.field private final vibrator:Landroid/os/Vibrator;


# direct methods
.method constructor <init>(Lcom/squareup/configure/item/ConfigureItemScopeRunner;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/util/Res;Landroid/os/Vibrator;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/configure/item/ConfigureItemHost;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 78
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 79
    iput-object p2, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 80
    iput-object p3, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->currency:Lcom/squareup/protos/common/CurrencyCode;

    .line 81
    iput-object p4, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 82
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    .line 83
    iput-object p5, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 84
    iput-object p6, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->vibrator:Landroid/os/Vibrator;

    .line 85
    iput-object p7, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 86
    iput-object p8, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;)Ljava/lang/Object;
    .locals 0

    .line 55
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;)Lcom/squareup/protos/common/Money;
    .locals 0

    .line 55
    iget-object p0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->newAmount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method static synthetic access$102(Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;
    .locals 0

    .line 55
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->newAmount:Lcom/squareup/protos/common/Money;

    return-object p1
.end method

.method static synthetic access$200(Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;)Lcom/squareup/protos/common/CurrencyCode;
    .locals 0

    .line 55
    iget-object p0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->currency:Lcom/squareup/protos/common/CurrencyCode;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;)Lcom/squareup/configure/item/ConfigureItemScopeRunner;
    .locals 0

    .line 55
    iget-object p0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;)Lcom/squareup/settings/server/AccountStatusSettings;
    .locals 0

    .line 55
    iget-object p0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;)Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 0

    .line 55
    iget-object p0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;)Lcom/squareup/protos/common/Money;
    .locals 0

    .line 55
    iget-object p0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->initialAmount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method


# virtual methods
.method displayPrice(Lcom/squareup/protos/common/Money;Z)V
    .locals 2

    .line 138
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    .line 139
    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemState;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/quantity/PerUnitFormatter;->format(Lcom/squareup/protos/common/Money;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 140
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemPriceView;

    invoke-virtual {v0, p1}, Lcom/squareup/configure/item/ConfigureItemPriceView;->setAmountText(Ljava/lang/CharSequence;)V

    if-eqz p2, :cond_0

    .line 141
    sget p1, Lcom/squareup/marin/R$color;->marin_light_gray:I

    goto :goto_0

    :cond_0
    sget p1, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    .line 144
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/configure/item/ConfigureItemPriceView;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->res:Lcom/squareup/util/Res;

    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->getColor(I)I

    move-result p1

    invoke-virtual {p2, p1}, Lcom/squareup/configure/item/ConfigureItemPriceView;->setAmountColor(I)V

    return-void
.end method

.method public onCancelSelected()V
    .locals 1

    .line 174
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->isVariablePriced()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->revertToPreviousVariationAndVariablePrice()V

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->onCancelConfigureItemPriceScreen()V

    return-void
.end method

.method public onCommitSelected()V
    .locals 3

    .line 148
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    iget-boolean v0, v0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonEnabled:Z

    if-nez v0, :cond_0

    return-void

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CONFIGURE_ITEM_PRICE:Lcom/squareup/analytics/RegisterViewName;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/squareup/configure/item/ConfigureItemHost;->onCommit(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/configure/item/ConfigureItemState;)V

    .line 150
    iget-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->userHasInteracted:Z

    if-eqz v0, :cond_2

    .line 151
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->isService()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    .line 152
    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    .line 153
    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->isFixedPriced()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 154
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->newAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/configure/item/ConfigureItemState;->setOverridePrice(Lcom/squareup/protos/common/Money;)V

    goto :goto_0

    .line 156
    :cond_1
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->newAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/configure/item/ConfigureItemState;->setCurrentVariablePrice(Lcom/squareup/protos/common/Money;)V

    .line 160
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getPreviousVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    .line 161
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v1

    if-eq v0, v1, :cond_3

    .line 166
    invoke-static {v0, v1}, Lcom/squareup/configure/item/ConfigureItemScreensKt;->shouldClearQuantity(Lcom/squareup/checkout/OrderVariation;Lcom/squareup/checkout/OrderVariation;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 167
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/squareup/configure/item/ConfigureItemState;->setQuantity(Ljava/math/BigDecimal;)V

    .line 170
    :cond_3
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->onCommitConfigureItemPriceScreen()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 5

    .line 90
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 92
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v3, "selectedVariation must be non-null before entering ConfigureItemPriceScreen"

    invoke-static {v0, v3}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    if-eqz p1, :cond_1

    const-string v0, "USER_INTERACTED_KEY"

    .line 96
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->userHasInteracted:Z

    const-string v0, "EDITING_AMOUNT_KEY"

    .line 97
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->currency:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v3, v4, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->newAmount:Lcom/squareup/protos/common/Money;

    const-string v0, "INITIAL_AMOUNT_KEY"

    .line 98
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->currency:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v3, v4, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->initialAmount:Lcom/squareup/protos/common/Money;

    goto :goto_1

    .line 100
    :cond_1
    iput-boolean v2, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->userHasInteracted:Z

    const-wide/16 v3, 0x0

    .line 101
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->currency:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v3, v4, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->newAmount:Lcom/squareup/protos/common/Money;

    .line 102
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->isVariablePriced()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 103
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    .line 104
    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemState;->getCurrentVariablePrice()Lcom/squareup/protos/common/Money;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->currency:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v3, v4, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->initialAmount:Lcom/squareup/protos/common/Money;

    goto :goto_1

    .line 106
    :cond_2
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemState;->getOverridePrice()Lcom/squareup/protos/common/Money;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->currency:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v3, v4, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->initialAmount:Lcom/squareup/protos/common/Money;

    .line 110
    :goto_1
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v3}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/configure/item/ConfigureItemState;->getVariations()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v1, :cond_3

    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/configure/item/R$string;->set_price:I

    .line 111
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :cond_3
    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    .line 112
    invoke-virtual {v3}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/configure/item/ConfigureItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/checkout/OrderVariation;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    .line 110
    :goto_2
    invoke-virtual {p1, v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 113
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    new-instance v0, Lcom/squareup/configure/item/-$$Lambda$P86WOtMJ7GhivQa8q5A_dU9rvek;

    invoke-direct {v0, p0}, Lcom/squareup/configure/item/-$$Lambda$P86WOtMJ7GhivQa8q5A_dU9rvek;-><init>(Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 114
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/strings/R$string;->save:I

    invoke-interface {v0, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 115
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    new-instance v0, Lcom/squareup/configure/item/-$$Lambda$ga8A9RUDZVVRYXM6zCM-XoU8hF8;

    invoke-direct {v0, p0}, Lcom/squareup/configure/item/-$$Lambda$ga8A9RUDZVVRYXM6zCM-XoU8hF8;-><init>(Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->showPrimaryButton(Ljava/lang/Runnable;)V

    .line 117
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemState;->isGiftCard()Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 118
    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getGiftCardSettings()Lcom/squareup/settings/server/GiftCardSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/settings/server/GiftCardSettings;->getGiftCardActivationMaximum()J

    move-result-wide v3

    goto :goto_3

    :cond_4
    sget-wide v3, Lcom/squareup/money/MaxMoneyScrubber;->MAX_MONEY:J

    .line 120
    :goto_3
    new-instance p1, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter$PriceEntryKeypadListener;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->vibrator:Landroid/os/Vibrator;

    invoke-direct {p1, p0, v0, v3, v4}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter$PriceEntryKeypadListener;-><init>(Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;Landroid/os/Vibrator;J)V

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->listener:Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter$PriceEntryKeypadListener;

    .line 121
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/configure/item/ConfigureItemPriceView;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->listener:Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter$PriceEntryKeypadListener;

    invoke-virtual {p1, v0}, Lcom/squareup/configure/item/ConfigureItemPriceView;->setListener(Lcom/squareup/padlock/Padlock$OnKeyPressListener;)V

    .line 123
    iget-boolean p1, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->userHasInteracted:Z

    if-eqz p1, :cond_5

    .line 124
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->newAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {p0, p1, v2}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->displayPrice(Lcom/squareup/protos/common/Money;Z)V

    goto :goto_4

    .line 126
    :cond_5
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->initialAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {p0, p1, v1}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->displayPrice(Lcom/squareup/protos/common/Money;Z)V

    :goto_4
    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 3

    .line 131
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onSave(Landroid/os/Bundle;)V

    .line 132
    iget-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->userHasInteracted:Z

    const-string v1, "USER_INTERACTED_KEY"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 133
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->newAmount:Lcom/squareup/protos/common/Money;

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-string v2, "EDITING_AMOUNT_KEY"

    invoke-virtual {p1, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 134
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->initialAmount:Lcom/squareup/protos/common/Money;

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-string v2, "INITIAL_AMOUNT_KEY"

    invoke-virtual {p1, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    return-void
.end method

.method protected onStartVisualTransition()V
    .locals 3

    .line 182
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CONFIGURE_ITEM_PRICE:Lcom/squareup/analytics/RegisterViewName;

    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/configure/item/ConfigureItemHost;->onStartVisualTransition(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/configure/item/ConfigureItemState;)V

    return-void
.end method
