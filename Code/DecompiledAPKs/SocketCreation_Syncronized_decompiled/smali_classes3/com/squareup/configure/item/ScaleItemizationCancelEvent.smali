.class public final Lcom/squareup/configure/item/ScaleItemizationCancelEvent;
.super Lcom/squareup/configure/item/ScaleLogEvent;
.source "ScaleLogEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u001b\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/configure/item/ScaleItemizationCancelEvent;",
        "Lcom/squareup/configure/item/ScaleLogEvent;",
        "quantityEntryType",
        "Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;",
        "hardwareScale",
        "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
        "(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;Lcom/squareup/scales/ScaleTracker$HardwareScale;)V",
        "measure_method",
        "Lcom/squareup/configure/item/MeasurementMethod;",
        "getMeasure_method",
        "()Lcom/squareup/configure/item/MeasurementMethod;",
        "configure-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final measure_method:Lcom/squareup/configure/item/MeasurementMethod;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;Lcom/squareup/scales/ScaleTracker$HardwareScale;)V
    .locals 3

    .line 60
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->TAP:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SCALE_ITEMIZATION_CANCEL:Lcom/squareup/analytics/RegisterTapName;

    iget-object v1, v1, Lcom/squareup/analytics/RegisterTapName;->value:Ljava/lang/String;

    const-string v2, "SCALE_ITEMIZATION_CANCEL.value"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, p2, v2}, Lcom/squareup/configure/item/ScaleLogEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lcom/squareup/scales/ScaleTracker$HardwareScale;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    if-eqz p1, :cond_0

    .line 61
    invoke-static {p1}, Lcom/squareup/configure/item/ScaleLogEventKt;->access$toMeasurementMethod(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;)Lcom/squareup/configure/item/MeasurementMethod;

    move-result-object v2

    :cond_0
    iput-object v2, p0, Lcom/squareup/configure/item/ScaleItemizationCancelEvent;->measure_method:Lcom/squareup/configure/item/MeasurementMethod;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;Lcom/squareup/scales/ScaleTracker$HardwareScale;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 59
    check-cast p2, Lcom/squareup/scales/ScaleTracker$HardwareScale;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/configure/item/ScaleItemizationCancelEvent;-><init>(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;Lcom/squareup/scales/ScaleTracker$HardwareScale;)V

    return-void
.end method


# virtual methods
.method public final getMeasure_method()Lcom/squareup/configure/item/MeasurementMethod;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/configure/item/ScaleItemizationCancelEvent;->measure_method:Lcom/squareup/configure/item/MeasurementMethod;

    return-object v0
.end method
