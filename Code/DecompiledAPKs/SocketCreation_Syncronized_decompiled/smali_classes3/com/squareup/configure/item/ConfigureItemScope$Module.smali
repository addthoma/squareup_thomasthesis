.class public Lcom/squareup/configure/item/ConfigureItemScope$Module;
.super Ljava/lang/Object;
.source "ConfigureItemScope.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/configure/item/ConfigureItemScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Module"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/configure/item/ConfigureItemScope;


# direct methods
.method public constructor <init>(Lcom/squareup/configure/item/ConfigureItemScope;)V
    .locals 0

    .line 108
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemScope$Module;->this$0:Lcom/squareup/configure/item/ConfigureItemScope;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method provideConfigureItemNavigator(Lcom/squareup/util/Device;Lflow/Flow;)Lcom/squareup/configure/item/ConfigureItemNavigator;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 111
    new-instance v0, Lcom/squareup/configure/item/ConfigureItemScope$RealConfigureItemNavigator;

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemScope$Module;->this$0:Lcom/squareup/configure/item/ConfigureItemScope;

    invoke-direct {v0, p1, p2, v1}, Lcom/squareup/configure/item/ConfigureItemScope$RealConfigureItemNavigator;-><init>(Lcom/squareup/util/Device;Lflow/Flow;Lcom/squareup/configure/item/ConfigureItemScope;)V

    return-object v0
.end method
