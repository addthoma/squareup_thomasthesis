.class public interface abstract Lcom/squareup/depositschedule/DepositScheduleService;
.super Ljava/lang/Object;
.source "DepositScheduleService.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006H\'J\u0018\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\tH\'\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/depositschedule/DepositScheduleService;",
        "",
        "getDepositSchedule",
        "Lcom/squareup/server/AcceptedResponse;",
        "Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;",
        "request",
        "Lcom/squareup/protos/client/depositsettings/GetDepositScheduleRequest;",
        "setDepositSchedule",
        "Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;",
        "Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getDepositSchedule(Lcom/squareup/protos/client/depositsettings/GetDepositScheduleRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/client/depositsettings/GetDepositScheduleRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/depositsettings/GetDepositScheduleRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/deposit-settings/get-deposit-schedule"
    .end annotation
.end method

.method public abstract setDepositSchedule(Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/deposit-settings/set-deposit-schedule"
    .end annotation
.end method
