.class public final Lcom/squareup/glyph/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/glyph/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final glyph_font_size:I = 0x7f070169

.field public static final glyph_logo_image:I = 0x7f07016a

.field public static final glyph_logo_image_text_size:I = 0x7f07016b

.field public static final glyph_logotype_font_size:I = 0x7f07016c

.field public static final glyph_logotype_font_size_world:I = 0x7f07016d

.field public static final glyph_logotype_height:I = 0x7f07016e

.field public static final glyph_logotype_height_world:I = 0x7f07016f

.field public static final glyph_logotype_width_en:I = 0x7f070170

.field public static final glyph_logotype_width_en_world:I = 0x7f070171

.field public static final glyph_logotype_width_es:I = 0x7f070172

.field public static final glyph_logotype_width_es_world:I = 0x7f070173

.field public static final glyph_logotype_width_fr:I = 0x7f070174

.field public static final glyph_logotype_width_fr_world:I = 0x7f070175

.field public static final glyph_logotype_width_ja:I = 0x7f070176

.field public static final glyph_logotype_width_ja_world:I = 0x7f070177

.field public static final glyph_scaling:I = 0x7f070178

.field public static final glyph_shadow_dx:I = 0x7f070179

.field public static final glyph_shadow_dx_45_angle:I = 0x7f07017a

.field public static final glyph_shadow_dy:I = 0x7f07017b

.field public static final glyph_shadow_radius:I = 0x7f07017c

.field public static final glyph_size_circle:I = 0x7f07017d

.field public static final glyph_stroke_width:I = 0x7f07017e

.field public static final glyph_vertical_caret:I = 0x7f07017f


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
