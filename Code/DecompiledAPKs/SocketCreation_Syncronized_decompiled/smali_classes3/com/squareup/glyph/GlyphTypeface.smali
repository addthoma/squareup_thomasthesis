.class public Lcom/squareup/glyph/GlyphTypeface;
.super Ljava/lang/Object;
.source "GlyphTypeface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/glyph/GlyphTypeface$LayeredTileDrawable;,
        Lcom/squareup/glyph/GlyphTypeface$Glyph;,
        Lcom/squareup/glyph/GlyphTypeface$Size;
    }
.end annotation


# static fields
.field private static final ICON_FONT_PATH:Ljava/lang/String; = "fonts/square_glyphs.ttf"

.field private static iconFont:Landroid/graphics/Typeface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static asBitmap(Landroid/content/res/Resources;Lcom/squareup/glyph/GlyphTypeface$Glyph;I)Landroid/graphics/Bitmap;
    .locals 3

    .line 115
    new-instance v0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;-><init>(Landroid/content/res/Resources;)V

    .line 116
    invoke-virtual {v0, p2}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->colorId(I)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object p0

    .line 117
    invoke-virtual {p0, p1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object p0

    .line 118
    invoke-virtual {p0}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->build()Lcom/squareup/glyph/SquareGlyphDrawable;

    move-result-object p0

    .line 120
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result p1

    .line 121
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result p2

    .line 122
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 123
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v2, 0x0

    .line 126
    invoke-virtual {p0, v2, v2, p1, p2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 127
    invoke-virtual {p0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    return-object v0
.end method

.method public static buildGlyphWithBackground(Landroid/content/res/Resources;ILcom/squareup/glyph/GlyphTypeface$Glyph;IZ)Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 77
    invoke-static {p0, p1, p2, p4}, Lcom/squareup/glyph/GlyphTypeface;->buildShadowedGlyph(Landroid/content/res/Resources;ILcom/squareup/glyph/GlyphTypeface$Glyph;Z)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    .line 78
    new-instance p2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {p2, p3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    const/4 p3, 0x0

    .line 79
    invoke-virtual {p2, p3, p3, p1, p1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 80
    new-instance p4, Lcom/squareup/glyph/GlyphTypeface$LayeredTileDrawable;

    const/4 v0, 0x2

    new-array v0, v0, [Landroid/graphics/drawable/Drawable;

    aput-object p2, v0, p3

    const/4 p2, 0x1

    aput-object p0, v0, p2

    const/4 p0, 0x0

    invoke-direct {p4, p1, v0, p0}, Lcom/squareup/glyph/GlyphTypeface$LayeredTileDrawable;-><init>(I[Landroid/graphics/drawable/Drawable;Lcom/squareup/glyph/GlyphTypeface$1;)V

    return-object p4
.end method

.method public static buildLayeredGlyphs(Landroid/content/res/Resources;Lcom/squareup/glyph/GlyphTypeface$Glyph;ILcom/squareup/glyph/GlyphTypeface$Glyph;I)Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 103
    new-instance v0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;-><init>(Landroid/content/res/Resources;)V

    .line 104
    invoke-virtual {v0, p2}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->colorId(I)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object p2

    .line 105
    invoke-virtual {p2, p1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object p1

    .line 106
    invoke-virtual {p1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->build()Lcom/squareup/glyph/SquareGlyphDrawable;

    move-result-object p1

    .line 107
    new-instance p2, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    invoke-direct {p2, p0}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;-><init>(Landroid/content/res/Resources;)V

    .line 108
    invoke-virtual {p2, p4}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->colorId(I)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object p0

    .line 109
    invoke-virtual {p0, p3}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object p0

    .line 110
    invoke-virtual {p0}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->build()Lcom/squareup/glyph/SquareGlyphDrawable;

    move-result-object p0

    .line 111
    new-instance p2, Landroid/graphics/drawable/LayerDrawable;

    const/4 p3, 0x2

    new-array p3, p3, [Landroid/graphics/drawable/Drawable;

    const/4 p4, 0x0

    aput-object p1, p3, p4

    const/4 p1, 0x1

    aput-object p0, p3, p1

    invoke-direct {p2, p3}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    return-object p2
.end method

.method public static buildShadowedGlyph(Landroid/content/res/Resources;ILcom/squareup/glyph/GlyphTypeface$Glyph;Z)Landroid/graphics/drawable/Drawable;
    .locals 3

    .line 85
    new-instance v0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;-><init>(Landroid/content/res/Resources;)V

    sget v1, Lcom/squareup/glyph/R$color;->glyph_background:I

    .line 86
    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->colorId(I)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object v0

    .line 87
    invoke-virtual {v0, p2}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object p2

    if-eqz p3, :cond_0

    .line 89
    sget p3, Lcom/squareup/glyph/R$dimen;->glyph_shadow_radius:I

    invoke-virtual {p0, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p3

    .line 90
    sget v0, Lcom/squareup/glyph/R$dimen;->glyph_shadow_dx:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 91
    sget v1, Lcom/squareup/glyph/R$dimen;->glyph_shadow_dy:I

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 92
    sget v2, Lcom/squareup/glyph/R$color;->glyph_shadow:I

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p0

    int-to-float v0, v0

    int-to-float v1, v1

    .line 93
    invoke-virtual {p2, p3, v0, v1, p0}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->shadow(IFFI)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    .line 95
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->build()Lcom/squareup/glyph/SquareGlyphDrawable;

    move-result-object p0

    const/4 p2, 0x0

    .line 97
    invoke-virtual {p0, p2, p2, p1, p1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    return-object p0
.end method

.method public static getGlyphFont(Landroid/content/res/Resources;)Landroid/graphics/Typeface;
    .locals 3

    .line 44
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface;->iconFont:Landroid/graphics/Typeface;

    if-nez v0, :cond_0

    sget v0, Lcom/squareup/glyph/R$bool;->load_overriding_glyph_font:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    new-instance v0, Ljava/io/File;

    sget-object v1, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    .line 46
    invoke-static {v1}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    const-string v2, "square_glyphs.ttf"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 48
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Overriding glyph font"

    .line 49
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 50
    invoke-static {v0}, Landroid/graphics/Typeface;->createFromFile(Ljava/io/File;)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface;->iconFont:Landroid/graphics/Typeface;

    .line 53
    :cond_0
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface;->iconFont:Landroid/graphics/Typeface;

    if-nez v0, :cond_1

    .line 54
    invoke-virtual {p0}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object p0

    const-string v0, "fonts/square_glyphs.ttf"

    invoke-static {p0, v0}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object p0

    sput-object p0, Lcom/squareup/glyph/GlyphTypeface;->iconFont:Landroid/graphics/Typeface;

    .line 56
    :cond_1
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface;->iconFont:Landroid/graphics/Typeface;

    return-object p0
.end method

.method public static getGlyphFont(Lcom/squareup/util/Res;)Landroid/graphics/Typeface;
    .locals 3

    .line 60
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface;->iconFont:Landroid/graphics/Typeface;

    if-nez v0, :cond_0

    sget v0, Lcom/squareup/glyph/R$bool;->load_overriding_glyph_font:I

    invoke-interface {p0, v0}, Lcom/squareup/util/Res;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    new-instance v0, Ljava/io/File;

    sget-object v1, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    .line 62
    invoke-static {v1}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    const-string v2, "square_glyphs.ttf"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 64
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Overriding glyph font"

    .line 65
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 66
    invoke-static {v0}, Landroid/graphics/Typeface;->createFromFile(Ljava/io/File;)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface;->iconFont:Landroid/graphics/Typeface;

    .line 69
    :cond_0
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface;->iconFont:Landroid/graphics/Typeface;

    if-nez v0, :cond_1

    .line 70
    invoke-interface {p0}, Lcom/squareup/util/Res;->getAssets()Landroid/content/res/AssetManager;

    move-result-object p0

    const-string v0, "fonts/square_glyphs.ttf"

    invoke-static {p0, v0}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object p0

    sput-object p0, Lcom/squareup/glyph/GlyphTypeface;->iconFont:Landroid/graphics/Typeface;

    .line 72
    :cond_1
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface;->iconFont:Landroid/graphics/Typeface;

    return-object p0
.end method
