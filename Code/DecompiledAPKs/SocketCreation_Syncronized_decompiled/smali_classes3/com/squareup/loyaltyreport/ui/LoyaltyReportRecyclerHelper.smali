.class public final Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper;
.super Ljava/lang/Object;
.source "LoyaltyReportRecyclerHelper.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLoyaltyReportRecyclerHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LoyaltyReportRecyclerHelper.kt\ncom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper\n+ 2 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 3 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 4 RecyclerEdges.kt\ncom/squareup/noho/dsl/RecyclerEdgesKt\n+ 5 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 6 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec\n*L\n1#1,143:1\n49#2:144\n50#2,3:150\n53#2:239\n599#3,4:145\n601#3:149\n43#4,2:153\n310#5,3:155\n313#5,3:164\n310#5,3:167\n313#5,3:176\n310#5,3:179\n313#5,3:188\n310#5,3:191\n313#5,3:200\n310#5,3:203\n313#5,3:212\n310#5,3:215\n313#5,3:224\n355#5,3:227\n358#5,3:236\n35#6,6:158\n35#6,6:170\n35#6,6:182\n35#6,6:194\n35#6,6:206\n35#6,6:218\n35#6,6:230\n*E\n*S KotlinDebug\n*F\n+ 1 LoyaltyReportRecyclerHelper.kt\ncom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper\n*L\n33#1:144\n33#1,3:150\n33#1:239\n33#1,4:145\n33#1:149\n33#1,2:153\n33#1,3:155\n33#1,3:164\n33#1,3:167\n33#1,3:176\n33#1,3:179\n33#1,3:188\n33#1,3:191\n33#1,3:200\n33#1,3:203\n33#1,3:212\n33#1,3:215\n33#1,3:224\n33#1,3:227\n33#1,3:236\n33#1,6:158\n33#1,6:170\n33#1,6:182\n33#1,6:194\n33#1,6:206\n33#1,6:218\n33#1,6:230\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00010\u00082\u0006\u0010\t\u001a\u00020\nJ\u0014\u0010\u000b\u001a\u00020\u000c*\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper;",
        "",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Res;)V",
        "adoptFor",
        "Lcom/squareup/cycler/Recycler;",
        "recyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "setBold",
        "",
        "Landroid/widget/TextView;",
        "titleValue",
        "Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$TitleValue;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "recyclerFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    iput-object p2, p0, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper;)Lcom/squareup/util/Res;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$setBold(Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper;Landroid/widget/TextView;Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$TitleValue;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper;->setBold(Landroid/widget/TextView;Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$TitleValue;)V

    return-void
.end method

.method private final setBold(Landroid/widget/TextView;Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$TitleValue;)V
    .locals 0

    .line 136
    invoke-virtual {p2}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$TitleValue;->isBold()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 137
    sget-object p2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    goto :goto_0

    .line 139
    :cond_0
    sget-object p2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    .line 136
    :goto_0
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    return-void
.end method


# virtual methods
.method public final adoptFor(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/recyclerview/widget/RecyclerView;",
            ")",
            "Lcom/squareup/cycler/Recycler<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    const-string v0, "recyclerView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    .line 144
    sget-object v1, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 145
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 146
    new-instance v1, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v1}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 150
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 151
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 153
    new-instance v0, Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    invoke-direct {v0}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;-><init>()V

    const/4 v2, 0x0

    .line 38
    invoke-virtual {v0, v2}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->setDefault(I)V

    .line 39
    check-cast v0, Lcom/squareup/cycler/ExtensionSpec;

    .line 153
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->extension(Lcom/squareup/cycler/ExtensionSpec;)V

    .line 156
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$row$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 42
    sget v2, Lcom/squareup/loyaltyreport/impl/R$layout;->date_range_title:I

    .line 158
    new-instance v3, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$create$1;

    invoke-direct {v3, v2}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$create$1;-><init>(I)V

    check-cast v3, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 156
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 155
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 168
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$row$2;->INSTANCE:Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$row$2;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 54
    move-object v2, v0

    check-cast v2, Lcom/squareup/cycler/Recycler$RowSpec;

    const/16 v3, 0x8

    invoke-static {v2, v3}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->setEdges(Lcom/squareup/cycler/Recycler$RowSpec;I)V

    .line 55
    sget v3, Lcom/squareup/loyaltyreport/impl/R$layout;->section_header:I

    .line 170
    new-instance v4, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$create$2;

    invoke-direct {v4, v3}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$create$2;-><init>(I)V

    check-cast v4, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v4}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 167
    invoke-virtual {v1, v2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 180
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$row$3;->INSTANCE:Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$row$3;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 63
    move-object v2, v0

    check-cast v2, Lcom/squareup/cycler/Recycler$RowSpec;

    const/16 v3, 0xa

    invoke-static {v2, v3}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->setEdges(Lcom/squareup/cycler/Recycler$RowSpec;I)V

    .line 64
    sget v4, Lcom/squareup/loyaltyreport/impl/R$layout;->summary:I

    .line 182
    new-instance v5, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$create$3;

    invoke-direct {v5, v4}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$create$3;-><init>(I)V

    check-cast v5, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v5}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 179
    invoke-virtual {v1, v2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 192
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$row$4;->INSTANCE:Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$row$4;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 81
    sget v2, Lcom/squareup/loyaltyreport/impl/R$layout;->view_in_dashboard:I

    .line 194
    new-instance v4, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$create$4;

    invoke-direct {v4, v2}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$create$4;-><init>(I)V

    check-cast v4, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v4}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 192
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 191
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 204
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$row$5;->INSTANCE:Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$row$5;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 91
    move-object v2, v0

    check-cast v2, Lcom/squareup/cycler/Recycler$RowSpec;

    invoke-static {v2, v3}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->setEdges(Lcom/squareup/cycler/Recycler$RowSpec;I)V

    .line 92
    sget v4, Lcom/squareup/loyaltyreport/impl/R$layout;->title_value:I

    .line 206
    new-instance v5, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$adoptFor$$inlined$adopt$lambda$1;

    invoke-direct {v5, v4, p0}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$adoptFor$$inlined$adopt$lambda$1;-><init>(ILcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper;)V

    check-cast v5, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v5}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 203
    invoke-virtual {v1, v2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 216
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$row$6;->INSTANCE:Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$row$6;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 108
    move-object v2, v0

    check-cast v2, Lcom/squareup/cycler/Recycler$RowSpec;

    invoke-static {v2, v3}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->setEdges(Lcom/squareup/cycler/Recycler$RowSpec;I)V

    .line 109
    sget v3, Lcom/squareup/loyaltyreport/impl/R$layout;->icon_initials_title_value:I

    .line 218
    new-instance v4, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$adoptFor$$inlined$adopt$lambda$2;

    invoke-direct {v4, v3, p0}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$adoptFor$$inlined$adopt$lambda$2;-><init>(ILcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper;)V

    check-cast v4, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v4}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 215
    invoke-virtual {v1, v2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 228
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$extraItem$1;->INSTANCE:Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$extraItem$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 130
    sget v2, Lcom/squareup/loyaltyreport/impl/R$layout;->loyalty_report_space:I

    .line 230
    new-instance v3, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$create$7;

    invoke-direct {v3, v2}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$create$7;-><init>(I)V

    check-cast v3, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 228
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 227
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->extraItem(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 148
    invoke-virtual {v1, p1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    return-object p1

    .line 145
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
