.class public final Lcom/squareup/loyaltyreport/impl/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loyaltyreport/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final date_range_tooltip:I = 0x7f1207d1

.field public static final loyalty_report_average_spend:I = 0x7f120f4d

.field public static final loyalty_report_average_visits:I = 0x7f120f4e

.field public static final loyalty_report_customer_visits_plural:I = 0x7f120f4f

.field public static final loyalty_report_customer_visits_singular:I = 0x7f120f50

.field public static final loyalty_report_customize_report:I = 0x7f120f51

.field public static final loyalty_report_date_pattern:I = 0x7f120f52

.field public static final loyalty_report_date_range:I = 0x7f120f53

.field public static final loyalty_report_loyalty_customers:I = 0x7f120f54

.field public static final loyalty_report_network_error_button:I = 0x7f120f55

.field public static final loyalty_report_network_error_message:I = 0x7f120f56

.field public static final loyalty_report_network_error_title:I = 0x7f120f57

.field public static final loyalty_report_non_loyalty_customers:I = 0x7f120f58

.field public static final loyalty_report_not_available:I = 0x7f120f59

.field public static final loyalty_report_not_available_shorthand:I = 0x7f120f5a

.field public static final loyalty_report_rewards_redeemed:I = 0x7f120f5b

.field public static final loyalty_report_save:I = 0x7f120f5c

.field public static final loyalty_report_selector_last_month:I = 0x7f120f5d

.field public static final loyalty_report_selector_last_year:I = 0x7f120f5e

.field public static final loyalty_report_selector_this_month:I = 0x7f120f5f

.field public static final loyalty_report_selector_this_year:I = 0x7f120f60

.field public static final loyalty_report_summary:I = 0x7f120f61

.field public static final loyalty_report_title:I = 0x7f120f62

.field public static final loyalty_report_top_customers:I = 0x7f120f63

.field public static final loyalty_report_total:I = 0x7f120f64

.field public static final loyalty_report_view_dashboard:I = 0x7f120f65

.field public static final loyalty_report_view_dashboard_details:I = 0x7f120f66

.field public static final loyalty_report_view_dashboard_link:I = 0x7f120f67


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
