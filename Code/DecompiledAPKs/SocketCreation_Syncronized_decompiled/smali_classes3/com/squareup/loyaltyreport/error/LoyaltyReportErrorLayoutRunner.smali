.class public final Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorLayoutRunner;
.super Ljava/lang/Object;
.source "LoyaltyReportErrorLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorLayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorScreen;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u000f2\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u000fB\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0018\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u00022\u0006\u0010\r\u001a\u00020\u000eH\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorScreen;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "messageView",
        "Lcom/squareup/noho/NohoMessageView;",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorLayoutRunner$Companion;


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final messageView:Lcom/squareup/noho/NohoMessageView;

.field private final view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorLayoutRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorLayoutRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorLayoutRunner;->Companion:Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorLayoutRunner$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorLayoutRunner;->view:Landroid/view/View;

    .line 20
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object v0, p0, Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 22
    iget-object p1, p0, Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/loyaltyreport/impl/R$id;->loyalty_report_error_message_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.l\u2026eport_error_message_view)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/noho/NohoMessageView;

    iput-object p1, p0, Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorLayoutRunner;->messageView:Lcom/squareup/noho/NohoMessageView;

    .line 27
    iget-object p1, p0, Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 25
    new-instance v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 26
    new-instance v1, Lcom/squareup/resources/ResourceString;

    sget v2, Lcom/squareup/loyaltyreport/impl/R$string;->loyalty_report_title:I

    invoke-direct {v1, v2}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 27
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    iget-object p2, p0, Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorLayoutRunner$showRendering$1;-><init>(Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 35
    iget-object p2, p0, Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorLayoutRunner;->messageView:Lcom/squareup/noho/NohoMessageView;

    .line 36
    new-instance v0, Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorLayoutRunner$showRendering$2;

    invoke-direct {v0, p1}, Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorLayoutRunner$showRendering$2;-><init>(Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorScreen;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    const-string v0, "Debouncers.debounceRunna\u2026dering.onRetryPressed() }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-virtual {p2, p1}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 16
    check-cast p1, Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorLayoutRunner;->showRendering(Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
