.class public abstract Lcom/squareup/loyaltyreport/LoyaltyReportState;
.super Ljava/lang/Object;
.source "LoyaltyReportState.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loyaltyreport/LoyaltyReportState$Loading;,
        Lcom/squareup/loyaltyreport/LoyaltyReportState$RenderReport;,
        Lcom/squareup/loyaltyreport/LoyaltyReportState$RenderDateSelector;,
        Lcom/squareup/loyaltyreport/LoyaltyReportState$RenderError;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0004\u0007\u0008\t\nB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0004\u000b\u000c\r\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/loyaltyreport/LoyaltyReportState;",
        "Landroid/os/Parcelable;",
        "selectedDateRange",
        "Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;",
        "(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)V",
        "getSelectedDateRange",
        "()Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;",
        "Loading",
        "RenderDateSelector",
        "RenderError",
        "RenderReport",
        "Lcom/squareup/loyaltyreport/LoyaltyReportState$Loading;",
        "Lcom/squareup/loyaltyreport/LoyaltyReportState$RenderReport;",
        "Lcom/squareup/loyaltyreport/LoyaltyReportState$RenderDateSelector;",
        "Lcom/squareup/loyaltyreport/LoyaltyReportState$RenderError;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final selectedDateRange:Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;


# direct methods
.method private constructor <init>(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/loyaltyreport/LoyaltyReportState;->selectedDateRange:Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 8
    invoke-direct {p0, p1}, Lcom/squareup/loyaltyreport/LoyaltyReportState;-><init>(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)V

    return-void
.end method


# virtual methods
.method public getSelectedDateRange()Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/loyaltyreport/LoyaltyReportState;->selectedDateRange:Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;

    return-object v0
.end method
