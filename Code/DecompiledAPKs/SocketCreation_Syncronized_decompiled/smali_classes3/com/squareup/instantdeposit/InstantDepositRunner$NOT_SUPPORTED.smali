.class public final Lcom/squareup/instantdeposit/InstantDepositRunner$NOT_SUPPORTED;
.super Ljava/lang/Object;
.source "InstantDepositRunner.kt"

# interfaces
.implements Lcom/squareup/instantdeposit/InstantDepositRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/instantdeposit/InstantDepositRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NOT_SUPPORTED"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInstantDepositRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InstantDepositRunner.kt\ncom/squareup/instantdeposit/InstantDepositRunner$NOT_SUPPORTED\n+ 2 Objects.kt\ncom/squareup/util/Objects\n*L\n1#1,163:1\n151#2,2:164\n*E\n*S KotlinDebug\n*F\n+ 1 InstantDepositRunner.kt\ncom/squareup/instantdeposit/InstantDepositRunner$NOT_SUPPORTED\n*L\n161#1,2:164\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0017\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0097\u0001J\u0011\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0005H\u0096\u0001J\u000f\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cH\u0097\u0001J\u000f\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004H\u0097\u0001J\t\u0010\u000f\u001a\u00020\rH\u0096\u0001J\u000f\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u000cH\u0097\u0001\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/instantdeposit/InstantDepositRunner$NOT_SUPPORTED;",
        "Lcom/squareup/instantdeposit/InstantDepositRunner;",
        "()V",
        "checkIfEligibleForInstantDeposit",
        "Lio/reactivex/Single;",
        "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
        "includeRecentActivity",
        "",
        "instantDepositHint",
        "",
        "snapshot",
        "onInstantDepositMade",
        "Lio/reactivex/Observable;",
        "",
        "sendInstantDeposit",
        "setIsConfirmingInstantTransfer",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$INSTANCE:Lcom/squareup/instantdeposit/InstantDepositRunner$NOT_SUPPORTED;


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/instantdeposit/InstantDepositRunner;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 161
    new-instance v0, Lcom/squareup/instantdeposit/InstantDepositRunner$NOT_SUPPORTED;

    invoke-direct {v0}, Lcom/squareup/instantdeposit/InstantDepositRunner$NOT_SUPPORTED;-><init>()V

    sput-object v0, Lcom/squareup/instantdeposit/InstantDepositRunner$NOT_SUPPORTED;->$$INSTANCE:Lcom/squareup/instantdeposit/InstantDepositRunner$NOT_SUPPORTED;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 164
    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    .line 165
    const-class v2, Lcom/squareup/instantdeposit/InstantDepositRunner;

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-static {v2, v1, v3, v4, v0}, Lcom/squareup/util/Objects;->allMethodsThrowUnsupportedOperation$default(Ljava/lang/Class;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/instantdeposit/InstantDepositRunner;

    iput-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$NOT_SUPPORTED;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositRunner;

    return-void
.end method


# virtual methods
.method public checkIfEligibleForInstantDeposit(Z)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$NOT_SUPPORTED;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositRunner;

    invoke-interface {v0, p1}, Lcom/squareup/instantdeposit/InstantDepositRunner;->checkIfEligibleForInstantDeposit(Z)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public instantDepositHint(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Ljava/lang/CharSequence;
    .locals 1

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$NOT_SUPPORTED;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositRunner;

    invoke-interface {v0, p1}, Lcom/squareup/instantdeposit/InstantDepositRunner;->instantDepositHint(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method public onInstantDepositMade()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$NOT_SUPPORTED;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositRunner;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositRunner;->onInstantDepositMade()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public sendInstantDeposit()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$NOT_SUPPORTED;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositRunner;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositRunner;->sendInstantDeposit()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public setIsConfirmingInstantTransfer()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$NOT_SUPPORTED;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositRunner;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositRunner;->setIsConfirmingInstantTransfer()V

    return-void
.end method

.method public snapshot()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositRunner$NOT_SUPPORTED;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositRunner;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositRunner;->snapshot()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
