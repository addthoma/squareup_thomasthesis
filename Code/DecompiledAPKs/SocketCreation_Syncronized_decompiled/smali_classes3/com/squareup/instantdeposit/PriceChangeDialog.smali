.class public final Lcom/squareup/instantdeposit/PriceChangeDialog;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "PriceChangeDialog.kt"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/instantdeposit/PriceChangeDialog$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/instantdeposit/PriceChangeDialog$Runner;,
        Lcom/squareup/instantdeposit/PriceChangeDialog$ParentComponent;,
        Lcom/squareup/instantdeposit/PriceChangeDialog$Factory;,
        Lcom/squareup/instantdeposit/PriceChangeDialog$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPriceChangeDialog.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PriceChangeDialog.kt\ncom/squareup/instantdeposit/PriceChangeDialog\n+ 2 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,59:1\n24#2,4:60\n*E\n*S KotlinDebug\n*F\n+ 1 PriceChangeDialog.kt\ncom/squareup/instantdeposit/PriceChangeDialog\n*L\n54#1,4:60\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0005\u0008\u0007\u0018\u0000 \r2\u00020\u0001:\u0004\r\u000e\u000f\u0010B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0001\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u0004\u001a\u00020\u0006H\u0016J\u0018\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016R\u0011\u0010\u0002\u001a\u00020\u0001\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0004\u0010\u0005\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/instantdeposit/PriceChangeDialog;",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "parentKey",
        "(Lcom/squareup/ui/main/RegisterTreeKey;)V",
        "getParentKey",
        "()Lcom/squareup/ui/main/RegisterTreeKey;",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "",
        "Companion",
        "Factory",
        "ParentComponent",
        "Runner",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/instantdeposit/PriceChangeDialog;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/instantdeposit/PriceChangeDialog$Companion;


# instance fields
.field private final parentKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/instantdeposit/PriceChangeDialog$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/instantdeposit/PriceChangeDialog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/instantdeposit/PriceChangeDialog;->Companion:Lcom/squareup/instantdeposit/PriceChangeDialog$Companion;

    .line 60
    new-instance v0, Lcom/squareup/instantdeposit/PriceChangeDialog$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/instantdeposit/PriceChangeDialog$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 63
    sput-object v0, Lcom/squareup/instantdeposit/PriceChangeDialog;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 1

    const-string v0, "parentKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/instantdeposit/PriceChangeDialog;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method


# virtual methods
.method public final getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/instantdeposit/PriceChangeDialog;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/instantdeposit/PriceChangeDialog;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/RegisterTreeKey;->writeToParcel(Landroid/os/Parcel;I)V

    .line 50
    iget-object v0, p0, Lcom/squareup/instantdeposit/PriceChangeDialog;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
