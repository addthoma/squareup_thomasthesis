.class public final enum Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;
.super Ljava/lang/Enum;
.source "OrderHubAlertsFrequency.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency$Converter;,
        Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\u0008\u0086\u0001\u0018\u0000 \u000f2\u0008\u0012\u0004\u0012\u00020\u00000\u0001:\u0002\u000f\u0010B\u0017\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000e\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;",
        "",
        "interval",
        "",
        "timeUnit",
        "Ljava/util/concurrent/TimeUnit;",
        "(Ljava/lang/String;IJLjava/util/concurrent/TimeUnit;)V",
        "getInterval",
        "()J",
        "getTimeUnit",
        "()Ljava/util/concurrent/TimeUnit;",
        "IMMEDIATELY",
        "MINUTES_5",
        "MINUTES_15",
        "MINUTES_30",
        "Companion",
        "Converter",
        "settings_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

.field public static final Companion:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency$Companion;

.field private static final DEFAULT:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

.field public static final enum IMMEDIATELY:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

.field public static final enum MINUTES_15:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

.field public static final enum MINUTES_30:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

.field public static final enum MINUTES_5:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;


# instance fields
.field private final interval:J

.field private final timeUnit:Ljava/util/concurrent/TimeUnit;


# direct methods
.method static constructor <clinit>()V
    .locals 14

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    new-instance v7, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    .line 12
    sget-object v6, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-string v2, "IMMEDIATELY"

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;-><init>(Ljava/lang/String;IJLjava/util/concurrent/TimeUnit;)V

    sput-object v7, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;->IMMEDIATELY:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    const/4 v1, 0x0

    aput-object v7, v0, v1

    new-instance v1, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    .line 13
    sget-object v13, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-string v9, "MINUTES_5"

    const/4 v10, 0x1

    const-wide/16 v11, 0x5

    move-object v8, v1

    invoke-direct/range {v8 .. v13}, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;-><init>(Ljava/lang/String;IJLjava/util/concurrent/TimeUnit;)V

    sput-object v1, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;->MINUTES_5:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    .line 14
    sget-object v8, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-string v4, "MINUTES_15"

    const/4 v5, 0x2

    const-wide/16 v6, 0xf

    move-object v3, v1

    invoke-direct/range {v3 .. v8}, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;-><init>(Ljava/lang/String;IJLjava/util/concurrent/TimeUnit;)V

    sput-object v1, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;->MINUTES_15:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    .line 15
    sget-object v8, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-string v4, "MINUTES_30"

    const/4 v5, 0x3

    const-wide/16 v6, 0x1e

    move-object v3, v1

    invoke-direct/range {v3 .. v8}, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;-><init>(Ljava/lang/String;IJLjava/util/concurrent/TimeUnit;)V

    sput-object v1, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;->MINUTES_30:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;->$VALUES:[Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    new-instance v0, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;->Companion:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency$Companion;

    .line 18
    sget-object v0, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;->IMMEDIATELY:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    sput-object v0, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;->DEFAULT:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IJLjava/util/concurrent/TimeUnit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")V"
        }
    .end annotation

    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-wide p3, p0, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;->interval:J

    iput-object p5, p0, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;->timeUnit:Ljava/util/concurrent/TimeUnit;

    return-void
.end method

.method public static final synthetic access$getDEFAULT$cp()Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;->DEFAULT:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;
    .locals 1

    const-class v0, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    return-object p0
.end method

.method public static values()[Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;
    .locals 1

    sget-object v0, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;->$VALUES:[Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    invoke-virtual {v0}, [Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    return-object v0
.end method


# virtual methods
.method public final getInterval()J
    .locals 2

    .line 11
    iget-wide v0, p0, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;->interval:J

    return-wide v0
.end method

.method public final getTimeUnit()Ljava/util/concurrent/TimeUnit;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;->timeUnit:Ljava/util/concurrent/TimeUnit;

    return-object v0
.end method
