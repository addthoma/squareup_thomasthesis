.class final Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$createRecycler$1$2$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "CheckoutLinkListScreenLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->createRecycler(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Integer;",
        "Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "",
        "indicator",
        "Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$createRecycler$1$2$1$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$createRecycler$1$2$1$1;

    invoke-direct {v0}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$createRecycler$1$2$1$1;-><init>()V

    sput-object v0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$createRecycler$1$2$1$1;->INSTANCE:Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$createRecycler$1$2$1$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 50
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p2, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$createRecycler$1$2$1$1;->invoke(ILcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ILcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;)V
    .locals 0

    const-string p1, "indicator"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    invoke-virtual {p2}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;->getCalledLoadNext()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    .line 123
    invoke-virtual {p2, p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;->setCalledLoadNext(Z)V

    .line 124
    invoke-virtual {p2}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;->getLoadNext()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    :cond_0
    return-void
.end method
