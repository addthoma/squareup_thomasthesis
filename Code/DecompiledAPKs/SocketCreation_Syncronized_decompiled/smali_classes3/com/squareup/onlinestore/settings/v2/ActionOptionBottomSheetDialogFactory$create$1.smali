.class final Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$create$1;
.super Ljava/lang/Object;
.source "ActionOptionBottomSheetDialogFactory.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory;->create(Landroid/content/Context;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nActionOptionBottomSheetDialogFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ActionOptionBottomSheetDialogFactory.kt\ncom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$create$1\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,91:1\n1103#2,7:92\n1103#2,7:99\n1103#2,7:106\n1103#2,7:113\n1103#2,7:120\n*E\n*S KotlinDebug\n*F\n+ 1 ActionOptionBottomSheetDialogFactory.kt\ncom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$create$1\n*L\n37#1,7:92\n43#1,7:99\n43#1,7:106\n60#1,7:113\n66#1,7:120\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0016\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003j\u0002`\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/google/android/material/bottomsheet/BottomSheetDialog;",
        "screen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetScreenData;",
        "",
        "Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogScreen;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/workflow/legacy/Screen;)Lcom/google/android/material/bottomsheet/BottomSheetDialog;
    .locals 4

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/Screen;->component1()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetScreenData;

    .line 27
    new-instance v0, Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;-><init>(Landroid/content/Context;)V

    .line 30
    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$create$1;->$context:Landroid/content/Context;

    sget v2, Lcom/squareup/onlinestore/settings/impl/R$layout;->online_checkout_settings_action_bottom_sheet_dialog:I

    const/4 v3, 0x0

    .line 29
    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 32
    invoke-virtual {v0, v1}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->setContentView(Landroid/view/View;)V

    .line 34
    invoke-virtual {v0}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->getBehavior()Lcom/google/android/material/bottomsheet/BottomSheetBehavior;

    move-result-object v2

    const-string v3, "dialog.behavior"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->setState(I)V

    const-string/jumbo v2, "view"

    .line 36
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v2, Lcom/squareup/onlinestore/settings/impl/R$id;->edit_button:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    .line 92
    new-instance v3, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$create$1$$special$$inlined$onClickDebounced$1;

    invoke-direct {v3, v0, p1}, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$create$1$$special$$inlined$onClickDebounced$1;-><init>(Lcom/google/android/material/bottomsheet/BottomSheetDialog;Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetScreenData;)V

    check-cast v3, Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 42
    sget v2, Lcom/squareup/onlinestore/settings/impl/R$id;->deactivate_button:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    .line 43
    check-cast v2, Lcom/squareup/noho/NohoButton;

    .line 44
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetScreenData;->isLinkEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 45
    sget v3, Lcom/squareup/onlinestore/settings/impl/R$string;->deactivate:I

    invoke-virtual {v2, v3}, Lcom/squareup/noho/NohoButton;->setText(I)V

    .line 46
    check-cast v2, Landroid/view/View;

    .line 99
    new-instance v3, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$create$1$$special$$inlined$apply$lambda$1;

    invoke-direct {v3, p1, v0}, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$create$1$$special$$inlined$apply$lambda$1;-><init>(Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetScreenData;Lcom/google/android/material/bottomsheet/BottomSheetDialog;)V

    check-cast v3, Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 51
    :cond_0
    sget v3, Lcom/squareup/onlinestore/settings/impl/R$string;->activate:I

    invoke-virtual {v2, v3}, Lcom/squareup/noho/NohoButton;->setText(I)V

    .line 52
    check-cast v2, Landroid/view/View;

    .line 106
    new-instance v3, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$create$1$$special$$inlined$apply$lambda$2;

    invoke-direct {v3, p1, v0}, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$create$1$$special$$inlined$apply$lambda$2;-><init>(Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetScreenData;Lcom/google/android/material/bottomsheet/BottomSheetDialog;)V

    check-cast v3, Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    :goto_0
    sget v2, Lcom/squareup/onlinestore/settings/impl/R$id;->delete_button:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    .line 113
    new-instance v3, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$create$1$$special$$inlined$onClickDebounced$4;

    invoke-direct {v3, v0, p1}, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$create$1$$special$$inlined$onClickDebounced$4;-><init>(Lcom/google/android/material/bottomsheet/BottomSheetDialog;Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetScreenData;)V

    check-cast v3, Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    sget v2, Lcom/squareup/onlinestore/settings/impl/R$id;->dismiss_button:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    .line 120
    new-instance v2, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$create$1$$special$$inlined$onClickDebounced$5;

    invoke-direct {v2, v0, p1}, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$create$1$$special$$inlined$onClickDebounced$5;-><init>(Lcom/google/android/material/bottomsheet/BottomSheetDialog;Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetScreenData;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$create$1$5;

    invoke-direct {v1, p1}, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$create$1$5;-><init>(Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetScreenData;)V

    check-cast v1, Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$create$1;->apply(Lcom/squareup/workflow/legacy/Screen;)Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    move-result-object p1

    return-object p1
.end method
