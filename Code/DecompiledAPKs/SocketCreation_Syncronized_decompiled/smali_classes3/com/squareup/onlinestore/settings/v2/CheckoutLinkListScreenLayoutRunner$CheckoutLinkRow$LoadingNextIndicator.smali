.class public final Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;
.super Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow;
.source "CheckoutLinkListScreenLayoutRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LoadingNextIndicator"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\u000e\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J#\u0010\u0010\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0001J\u0013\u0010\u0011\u001a\u00020\u00032\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u00d6\u0003J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0008\u0010\t\"\u0004\u0008\n\u0010\u000bR\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\r\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;",
        "Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow;",
        "calledLoadNext",
        "",
        "loadNext",
        "Lkotlin/Function0;",
        "",
        "(ZLkotlin/jvm/functions/Function0;)V",
        "getCalledLoadNext",
        "()Z",
        "setCalledLoadNext",
        "(Z)V",
        "getLoadNext",
        "()Lkotlin/jvm/functions/Function0;",
        "component1",
        "component2",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private calledLoadNext:Z

.field private final loadNext:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ZLkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "loadNext"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 303
    invoke-direct {p0, v0}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;->calledLoadNext:Z

    iput-object p2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;->loadNext:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public synthetic constructor <init>(ZLkotlin/jvm/functions/Function0;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x1

    if-eqz p3, :cond_0

    const/4 p1, 0x0

    .line 301
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;-><init>(ZLkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;ZLkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-boolean p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;->calledLoadNext:Z

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;->loadNext:Lkotlin/jvm/functions/Function0;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;->copy(ZLkotlin/jvm/functions/Function0;)Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;->calledLoadNext:Z

    return v0
.end method

.method public final component2()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;->loadNext:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final copy(ZLkotlin/jvm/functions/Function0;)Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;"
        }
    .end annotation

    const-string v0, "loadNext"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;

    invoke-direct {v0, p1, p2}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;-><init>(ZLkotlin/jvm/functions/Function0;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;

    iget-boolean v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;->calledLoadNext:Z

    iget-boolean v1, p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;->calledLoadNext:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;->loadNext:Lkotlin/jvm/functions/Function0;

    iget-object p1, p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;->loadNext:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCalledLoadNext()Z
    .locals 1

    .line 301
    iget-boolean v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;->calledLoadNext:Z

    return v0
.end method

.method public final getLoadNext()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 302
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;->loadNext:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    iget-boolean v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;->calledLoadNext:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;->loadNext:Lkotlin/jvm/functions/Function0;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public final setCalledLoadNext(Z)V
    .locals 0

    .line 301
    iput-boolean p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;->calledLoadNext:Z

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LoadingNextIndicator(calledLoadNext="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;->calledLoadNext:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", loadNext="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;->loadNext:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
