.class public final Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflowViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "EnableCheckoutLinksWorkflowViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflowViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "()V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflowViewFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 5
    new-instance v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflowViewFactory;

    invoke-direct {v0}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflowViewFactory;-><init>()V

    sput-object v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflowViewFactory;->INSTANCE:Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflowViewFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 6
    sget-object v1, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->Companion:Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner$Companion;

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
