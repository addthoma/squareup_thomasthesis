.class public final Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;
.super Ljava/lang/Object;
.source "CheckoutLinkScreenLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner$Factory;,
        Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner$Binding;,
        Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCheckoutLinkScreenLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CheckoutLinkScreenLayoutRunner.kt\ncom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,154:1\n1103#2,7:155\n1103#2,7:162\n*E\n*S KotlinDebug\n*F\n+ 1 CheckoutLinkScreenLayoutRunner.kt\ncom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner\n*L\n87#1,7:155\n97#1,7:162\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 \u001c2\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0003\u001b\u001c\u001dB\u001b\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0002H\u0002J\u0018\u0010\u0018\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u0019\u001a\u00020\u001aH\u0016R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;",
        "view",
        "Landroid/view/View;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "(Landroid/view/View;Lcom/squareup/text/Formatter;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "amount",
        "Lcom/squareup/noho/NohoLabel;",
        "checkoutLinkUrl",
        "Lcom/squareup/noho/NohoRow;",
        "helpText",
        "Lcom/squareup/widgets/MessageView;",
        "payLinkName",
        "qrCodeView",
        "Landroid/widget/ImageView;",
        "shareLink",
        "setHelpText",
        "",
        "rendering",
        "showRendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Binding",
        "Companion",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final CHECKOUT_LINK_URL_MAX_LINES:I = 0x2

.field private static final CHECKOUT_LINK_URL_WIDTH_PERCENT:F = 0.8f

.field public static final Companion:Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner$Companion;


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final amount:Lcom/squareup/noho/NohoLabel;

.field private final checkoutLinkUrl:Lcom/squareup/noho/NohoRow;

.field private final helpText:Lcom/squareup/widgets/MessageView;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final payLinkName:Lcom/squareup/noho/NohoLabel;

.field private final qrCodeView:Landroid/widget/ImageView;

.field private final shareLink:Landroid/view/View;

.field private final view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->Companion:Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;Lcom/squareup/text/Formatter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->view:Landroid/view/View;

    iput-object p2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 39
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object p2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 41
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/onlinestore/settings/impl/R$id;->name:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->payLinkName:Lcom/squareup/noho/NohoLabel;

    .line 42
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/onlinestore/settings/impl/R$id;->amount:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->amount:Lcom/squareup/noho/NohoLabel;

    .line 43
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/onlinestore/settings/impl/R$id;->qr_code:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->qrCodeView:Landroid/widget/ImageView;

    .line 44
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/onlinestore/settings/impl/R$id;->share_checkoutlink:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->shareLink:Landroid/view/View;

    .line 45
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/onlinestore/settings/impl/R$id;->checkoutlink_url:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoRow;

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->checkoutLinkUrl:Lcom/squareup/noho/NohoRow;

    .line 46
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/onlinestore/settings/impl/R$id;->checkoutlink_help_text:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->helpText:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method public static final synthetic access$getView$p(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;)Landroid/view/View;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->view:Landroid/view/View;

    return-object p0
.end method

.method private final setHelpText(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;)V
    .locals 3

    .line 103
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 104
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;->getCheckoutLinkInfo()Lcom/squareup/onlinestore/settings/v2/CheckoutLinkInfo;

    move-result-object v1

    .line 105
    instance-of v2, v1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkInfo$CustomAmountLinkInfo;

    if-eqz v2, :cond_2

    .line 106
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;->getCheckoutLinkInfo()Lcom/squareup/onlinestore/settings/v2/CheckoutLinkInfo;

    move-result-object v1

    check-cast v1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkInfo$CustomAmountLinkInfo;

    invoke-virtual {v1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkInfo$CustomAmountLinkInfo;->getEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 107
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;->getQrCodeBitmap()Landroid/graphics/Bitmap;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 108
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->helpText:Lcom/squareup/widgets/MessageView;

    sget v1, Lcom/squareup/onlinestore/settings/impl/R$string;->online_checkout_settings_checkout_link_help_text:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 111
    :cond_0
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->helpText:Lcom/squareup/widgets/MessageView;

    sget v1, Lcom/squareup/onlinestore/settings/impl/R$string;->online_checkout_settings_checkout_link_help_text_no_qr_code:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 114
    :cond_1
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->helpText:Lcom/squareup/widgets/MessageView;

    sget v1, Lcom/squareup/onlinestore/settings/impl/R$string;->checkout_link_screen_deactivated_msg:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 117
    :cond_2
    instance-of v1, v1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkInfo$DonationLinkInfo;

    if-eqz v1, :cond_5

    .line 118
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;->getCheckoutLinkInfo()Lcom/squareup/onlinestore/settings/v2/CheckoutLinkInfo;

    move-result-object v1

    check-cast v1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkInfo$DonationLinkInfo;

    invoke-virtual {v1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkInfo$DonationLinkInfo;->getEnabled()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 119
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;->getQrCodeBitmap()Landroid/graphics/Bitmap;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 121
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->helpText:Lcom/squareup/widgets/MessageView;

    sget v1, Lcom/squareup/onlinestore/settings/impl/R$string;->online_checkout_settings_donation_checkout_link_help_text:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 124
    :cond_3
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->helpText:Lcom/squareup/widgets/MessageView;

    .line 125
    sget v1, Lcom/squareup/onlinestore/settings/impl/R$string;->online_checkout_settings_donation_checkout_link_help_text_no_qr_code:I

    .line 124
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 129
    :cond_4
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->helpText:Lcom/squareup/widgets/MessageView;

    sget v1, Lcom/squareup/onlinestore/settings/impl/R$string;->checkout_link_screen_deactivated_msg:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    :goto_0
    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 8

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    iget-object p2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 63
    iget-object p2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 53
    new-instance v7, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v7}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 55
    new-instance v0, Lcom/squareup/resources/ResourceString;

    sget v1, Lcom/squareup/onlinestore/settings/impl/R$string;->online_checkout_settings_title:I

    invoke-direct {v0, v1}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v0, Lcom/squareup/resources/TextModel;

    invoke-virtual {v7, v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 57
    sget-object v1, Lcom/squareup/noho/NohoActionButtonStyle;->SECONDARY:Lcom/squareup/noho/NohoActionButtonStyle;

    .line 58
    new-instance v0, Lcom/squareup/resources/ResourceString;

    sget v2, Lcom/squareup/onlinestore/settings/impl/R$string;->online_checkout_settings_checkout_link_actions:I

    invoke-direct {v0, v2}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    move-object v2, v0

    check-cast v2, Lcom/squareup/resources/TextModel;

    .line 59
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;->getOnClickActionButton()Lkotlin/jvm/functions/Function0;

    move-result-object v4

    const/4 v3, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, v7

    .line 56
    invoke-static/range {v0 .. v6}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionButton$default(Lcom/squareup/noho/NohoActionBar$Config$Builder;Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 61
    sget-object v0, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 63
    invoke-virtual {v7}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 65
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;->getCheckoutLinkInfo()Lcom/squareup/onlinestore/settings/v2/CheckoutLinkInfo;

    move-result-object p2

    .line 66
    instance-of v0, p2, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkInfo$CustomAmountLinkInfo;

    if-eqz v0, :cond_0

    .line 67
    iget-object p2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->payLinkName:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;->getCheckoutLinkInfo()Lcom/squareup/onlinestore/settings/v2/CheckoutLinkInfo;

    move-result-object v0

    check-cast v0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkInfo$CustomAmountLinkInfo;

    invoke-virtual {v0}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkInfo$CustomAmountLinkInfo;->getName()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 68
    iget-object p2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->amount:Lcom/squareup/noho/NohoLabel;

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;->getCheckoutLinkInfo()Lcom/squareup/onlinestore/settings/v2/CheckoutLinkInfo;

    move-result-object v1

    check-cast v1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkInfo$CustomAmountLinkInfo;

    invoke-virtual {v1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkInfo$CustomAmountLinkInfo;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 69
    iget-object p2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->amount:Lcom/squareup/noho/NohoLabel;

    check-cast p2, Landroid/view/View;

    invoke-static {p2}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    goto :goto_0

    .line 71
    :cond_0
    instance-of p2, p2, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkInfo$DonationLinkInfo;

    if-eqz p2, :cond_1

    .line 72
    iget-object p2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->payLinkName:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;->getCheckoutLinkInfo()Lcom/squareup/onlinestore/settings/v2/CheckoutLinkInfo;

    move-result-object v0

    check-cast v0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkInfo$DonationLinkInfo;

    invoke-virtual {v0}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkInfo$DonationLinkInfo;->getName()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 73
    iget-object p2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->amount:Lcom/squareup/noho/NohoLabel;

    check-cast p2, Landroid/view/View;

    invoke-static {p2}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 77
    :cond_1
    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->setHelpText(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;)V

    .line 79
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;->getQrCodeBitmap()Landroid/graphics/Bitmap;

    move-result-object p2

    if-eqz p2, :cond_2

    .line 80
    iget-object p2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->qrCodeView:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;->getQrCodeBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 81
    iget-object p2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->qrCodeView:Landroid/widget/ImageView;

    check-cast p2, Landroid/view/View;

    invoke-static {p2}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    goto :goto_1

    .line 84
    :cond_2
    iget-object p2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->qrCodeView:Landroid/widget/ImageView;

    check-cast p2, Landroid/view/View;

    invoke-static {p2}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 87
    :goto_1
    iget-object p2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->checkoutLinkUrl:Lcom/squareup/noho/NohoRow;

    .line 88
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;->getCheckoutLinkUrl()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    const v0, 0x3f4ccccd    # 0.8f

    .line 89
    invoke-static {p2, v0}, Lcom/squareup/noho/NohoRowUtilsKt;->constrainValueSize(Lcom/squareup/noho/NohoRow;F)V

    const/4 v0, 0x2

    .line 90
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {p2, v0, v1}, Lcom/squareup/noho/NohoRowUtilsKt;->setMaxLinesForValue(Lcom/squareup/noho/NohoRow;ILandroid/text/TextUtils$TruncateAt;)V

    const v0, 0x800005

    .line 91
    invoke-static {p2, v0}, Lcom/squareup/noho/NohoRowUtilsKt;->setHorizontalGravityForValue(Lcom/squareup/noho/NohoRow;I)V

    .line 92
    check-cast p2, Landroid/view/View;

    .line 155
    new-instance v0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner$showRendering$$inlined$with$lambda$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner$showRendering$$inlined$with$lambda$1;-><init>(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    iget-object p2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->shareLink:Landroid/view/View;

    .line 162
    new-instance v0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner$showRendering$$inlined$onClickDebounced$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner$showRendering$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner;->showRendering(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
