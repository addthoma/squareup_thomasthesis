.class public final Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$logCreatePayWorker$1;
.super Lcom/squareup/workflow/LifecycleWorker;
.source "CreateLinkWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;-><init>(Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0011\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016\u00a8\u0006\u0004"
    }
    d2 = {
        "com/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$logCreatePayWorker$1",
        "Lcom/squareup/workflow/LifecycleWorker;",
        "onStarted",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 48
    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$logCreatePayWorker$1;->this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;

    invoke-direct {p0}, Lcom/squareup/workflow/LifecycleWorker;-><init>()V

    return-void
.end method


# virtual methods
.method public onStarted()V
    .locals 3

    .line 50
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$logCreatePayWorker$1;->this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;

    invoke-static {v0}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;->access$getAnalytics$p(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;)Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    move-result-object v0

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEvent;

    sget-object v2, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;->CREATE_PAY:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;

    invoke-direct {v1, v2}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEvent;-><init>(Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;)V

    check-cast v1, Lcom/squareup/onlinestore/analytics/OnlineStoreViewEvent;

    invoke-interface {v0, v1}, Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;->logView(Lcom/squareup/onlinestore/analytics/OnlineStoreViewEvent;)V

    return-void
.end method
