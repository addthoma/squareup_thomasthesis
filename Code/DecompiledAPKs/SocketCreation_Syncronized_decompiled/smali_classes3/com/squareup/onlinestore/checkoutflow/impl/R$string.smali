.class public final Lcom/squareup/onlinestore/checkoutflow/impl/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onlinestore/checkoutflow/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final online_checkout_buylink_url:I = 0x7f12112a

.field public static final online_checkout_buylink_url_staging:I = 0x7f12112b

.field public static final online_checkout_checkout_link:I = 0x7f12112c

.field public static final online_checkout_checkout_link_help_text:I = 0x7f12112d

.field public static final online_checkout_checkout_link_help_text_no_qr_code:I = 0x7f12112e

.field public static final online_checkout_checkout_link_label:I = 0x7f12112f

.field public static final online_checkout_checkout_link_share_link:I = 0x7f121130

.field public static final online_checkout_clipboard_label:I = 0x7f121131

.field public static final online_checkout_copied_toast_message:I = 0x7f121132

.field public static final online_checkout_error_screen_message:I = 0x7f121135

.field public static final online_checkout_error_screen_retry_text:I = 0x7f121136

.field public static final online_checkout_error_screen_title:I = 0x7f121137

.field public static final online_checkout_feature_not_available_msg:I = 0x7f121138

.field public static final online_checkout_get_link_name_is_required:I = 0x7f121139

.field public static final online_checkout_get_link_name_over_255:I = 0x7f12113a

.field public static final online_checkout_link_qr_code_content_desc:I = 0x7f12113b

.field public static final online_checkout_new_sale:I = 0x7f12113d

.field public static final online_checkout_pay_link_already_exists_error_msg:I = 0x7f12113e

.field public static final online_checkout_pay_link_get_link_label:I = 0x7f12113f

.field public static final online_checkout_pay_link_name_hint:I = 0x7f121140

.field public static final online_checkout_pay_link_name_label:I = 0x7f121141

.field public static final online_checkout_paylink_url:I = 0x7f121142

.field public static final online_checkout_paylink_url_staging:I = 0x7f121143

.field public static final online_checkout_share_sheet_subject:I = 0x7f12115a

.field public static final online_checkout_share_sheet_title:I = 0x7f12115b

.field public static final online_checkout_title:I = 0x7f12115c

.field public static final online_checkout_title_item_unavailable:I = 0x7f12115d

.field public static final online_checkout_title_items_only:I = 0x7f12115e

.field public static final online_checkout_title_no_split_payments:I = 0x7f12115f

.field public static final online_checkout_title_online_only:I = 0x7f121160

.field public static final online_checkout_title_single_items_only:I = 0x7f121161

.field public static final online_checkout_title_with_money:I = 0x7f121162


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
