.class final Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$1$6;
.super Ljava/lang/Object;
.source "RealOnlineCheckoutBuyLinkWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$1;->apply(Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/MaybeSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0004\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0002H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Maybe;",
        "",
        "kotlin.jvm.PlatformType",
        "it",
        "apply",
        "(Ljava/lang/Boolean;)Lio/reactivex/Maybe;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $result:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;

.field final synthetic this$0:Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$1;


# direct methods
.method constructor <init>(Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$1;Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$1$6;->this$0:Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$1;

    iput-object p2, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$1$6;->$result:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Boolean;)Lio/reactivex/Maybe;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/reactivex/Maybe<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 243
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$1$6;->this$0:Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$1;

    iget-object p1, p1, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;

    invoke-static {p1}, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->access$getCheckoutLinksRepository$p(Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    move-result-object p1

    .line 244
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$1$6;->$result:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;

    check-cast v0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult$Success;

    invoke-virtual {v0}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult$Success;->getJsonWebToken()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$1$6;->this$0:Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$1;

    iget-object v1, v1, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$1;->$merchantCatalogObjectToken:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;->enableEcomAvailableForItem(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    .line 245
    invoke-virtual {p1}, Lio/reactivex/Single;->toMaybe()Lio/reactivex/Maybe;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 67
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$1$6;->apply(Ljava/lang/Boolean;)Lio/reactivex/Maybe;

    move-result-object p1

    return-object p1
.end method
