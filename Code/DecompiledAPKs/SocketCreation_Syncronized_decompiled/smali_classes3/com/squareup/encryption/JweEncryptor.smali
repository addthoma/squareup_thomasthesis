.class public Lcom/squareup/encryption/JweEncryptor;
.super Lcom/squareup/encryption/AbstractCryptoPrimitive;
.source "JweEncryptor.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/encryption/AbstractCryptoPrimitive<",
        "TK;>;"
    }
.end annotation


# static fields
.field protected static final AES_CBC:Ljava/lang/String; = "AES/CBC/PKCS5Padding"

.field private static final ASCII:Ljava/nio/charset/Charset;

.field private static final AUTH_TAG_BYTES:I = 0x10

.field private static final BASE64_FLAGS:I = 0xb

.field private static final HEADER_VALUES:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final HMAC_SHA256:Ljava/lang/String; = "HmacSHA256"

.field private static final KEY_ID_FIELD:Ljava/lang/String; = "kid"

.field protected static final RSA_PKCS15:Ljava/lang/String; = "RSA/ECB/PKCS1Padding"


# instance fields
.field private additionalHeaders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final keyId:Ljava/lang/String;

.field private final publicKey:Ljava/security/interfaces/RSAPublicKey;

.field private final secureRandom:Ljava/security/SecureRandom;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/squareup/encryption/JweEncryptor;->HEADER_VALUES:Ljava/util/Map;

    .line 52
    sget-object v0, Lcom/squareup/encryption/JweEncryptor;->HEADER_VALUES:Ljava/util/Map;

    const-string v1, "alg"

    const-string v2, "RSA1_5"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lcom/squareup/encryption/JweEncryptor;->HEADER_VALUES:Ljava/util/Map;

    const-string v1, "enc"

    const-string v2, "A128CBC-HS256"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/squareup/encryption/JweEncryptor;->HEADER_VALUES:Ljava/util/Map;

    const-string/jumbo v1, "zip"

    const-string v2, "DEF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "US-ASCII"

    .line 58
    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lcom/squareup/encryption/JweEncryptor;->ASCII:Ljava/nio/charset/Charset;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/encryption/JweEncryptor;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/encryption/JweEncryptor<",
            "TK;>;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 87
    invoke-direct {p0, p1}, Lcom/squareup/encryption/AbstractCryptoPrimitive;-><init>(Lcom/squareup/encryption/AbstractCryptoPrimitive;)V

    .line 65
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/encryption/JweEncryptor;->additionalHeaders:Ljava/util/Map;

    .line 89
    iget-object v0, p1, Lcom/squareup/encryption/JweEncryptor;->publicKey:Ljava/security/interfaces/RSAPublicKey;

    iput-object v0, p0, Lcom/squareup/encryption/JweEncryptor;->publicKey:Ljava/security/interfaces/RSAPublicKey;

    .line 90
    iget-object v0, p1, Lcom/squareup/encryption/JweEncryptor;->secureRandom:Ljava/security/SecureRandom;

    iput-object v0, p0, Lcom/squareup/encryption/JweEncryptor;->secureRandom:Ljava/security/SecureRandom;

    .line 91
    iget-object v0, p1, Lcom/squareup/encryption/JweEncryptor;->keyId:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/encryption/JweEncryptor;->keyId:Ljava/lang/String;

    .line 93
    iget-object v0, p0, Lcom/squareup/encryption/JweEncryptor;->additionalHeaders:Ljava/util/Map;

    iget-object p1, p1, Lcom/squareup/encryption/JweEncryptor;->additionalHeaders:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 94
    iget-object p1, p0, Lcom/squareup/encryption/JweEncryptor;->additionalHeaders:Ljava/util/Map;

    invoke-interface {p1, p2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Lcom/squareup/encryption/CryptoKeyAdapter;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Lcom/squareup/encryption/CryptoKeyAdapter<",
            "TK;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .line 69
    invoke-direct {p0, p1, p2}, Lcom/squareup/encryption/AbstractCryptoPrimitive;-><init>(Ljava/lang/Object;Lcom/squareup/encryption/CryptoKeyAdapter;)V

    .line 65
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/encryption/JweEncryptor;->additionalHeaders:Ljava/util/Map;

    .line 72
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-interface {p2, p1}, Lcom/squareup/encryption/CryptoKeyAdapter;->getRawKey(Ljava/lang/Object;)[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    const-string v1, "X.509"

    .line 73
    invoke-static {v1}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v1

    .line 74
    invoke-virtual {v1, v0}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    .line 75
    invoke-virtual {v0}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v0

    check-cast v0, Ljava/security/interfaces/RSAPublicKey;

    iput-object v0, p0, Lcom/squareup/encryption/JweEncryptor;->publicKey:Ljava/security/interfaces/RSAPublicKey;

    .line 78
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    iput-object v0, p0, Lcom/squareup/encryption/JweEncryptor;->secureRandom:Ljava/security/SecureRandom;

    .line 80
    invoke-interface {p2, p1}, Lcom/squareup/encryption/CryptoKeyAdapter;->getKeyId(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/encryption/JweEncryptor;->keyId:Ljava/lang/String;

    return-void
.end method

.method private aesEncrypt([B[B[B)[B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    :try_start_0
    const-string v0, "AES/CBC/PKCS5Padding"

    .line 261
    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 262
    new-instance v1, Ljavax/crypto/spec/SecretKeySpec;

    const-string v2, "AES"

    invoke-direct {v1, p1, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    const/4 p1, 0x1

    .line 263
    new-instance v2, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v2, p2}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    invoke-virtual {v0, p1, v1, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 264
    invoke-virtual {v0, p3}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object p1
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    goto :goto_0

    :catch_1
    move-exception p1

    goto :goto_0

    :catch_2
    move-exception p1

    goto :goto_0

    :catch_3
    move-exception p1

    goto :goto_0

    :catch_4
    move-exception p1

    .line 268
    :goto_0
    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method

.method private buildHeader()[B
    .locals 4

    .line 201
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 204
    :try_start_0
    iget-object v1, p0, Lcom/squareup/encryption/JweEncryptor;->additionalHeaders:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 205
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    .line 207
    :cond_0
    sget-object v1, Lcom/squareup/encryption/JweEncryptor;->HEADER_VALUES:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 208
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_1

    .line 210
    :cond_1
    iget-object v1, p0, Lcom/squareup/encryption/JweEncryptor;->keyId:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, "kid"

    .line 211
    iget-object v2, p0, Lcom/squareup/encryption/JweEncryptor;->keyId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 218
    :cond_2
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/squareup/encryption/JweEncryptor;->ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    .line 215
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static deflate([B)[B
    .locals 5

    .line 223
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 226
    new-instance v1, Ljava/util/zip/DeflaterOutputStream;

    new-instance v2, Ljava/util/zip/Deflater;

    const/4 v3, -0x1

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Ljava/util/zip/Deflater;-><init>(IZ)V

    invoke-direct {v1, v0, v2}, Ljava/util/zip/DeflaterOutputStream;-><init>(Ljava/io/OutputStream;Ljava/util/zip/Deflater;)V

    .line 231
    :try_start_0
    invoke-virtual {v1, p0}, Ljava/util/zip/DeflaterOutputStream;->write([B)V

    .line 232
    invoke-virtual {v1}, Ljava/util/zip/DeflaterOutputStream;->finish()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 237
    invoke-static {v1}, Lcom/squareup/util/Streams;->closeQuietly(Ljava/io/Closeable;)V

    .line 240
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p0

    return-object p0

    :catchall_0
    move-exception p0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 235
    :try_start_1
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 237
    :goto_0
    invoke-static {v1}, Lcom/squareup/util/Streams;->closeQuietly(Ljava/io/Closeable;)V

    .line 238
    throw p0
.end method

.method private hmacSha256([B[B)[B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    :try_start_0
    const-string v0, "HmacSHA256"

    .line 275
    invoke-static {v0}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v0

    .line 276
    new-instance v1, Ljavax/crypto/spec/SecretKeySpec;

    const-string v2, "HMAC"

    invoke-direct {v1, p1, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 277
    invoke-virtual {v0, v1}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V

    .line 278
    invoke-virtual {v0, p2}, Ljavax/crypto/Mac;->doFinal([B)[B

    move-result-object p1

    const/16 p2, 0x10

    .line 279
    invoke-static {p2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object p1
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 282
    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method

.method private rsaEncrypt(Ljava/security/interfaces/RSAPublicKey;[B)[B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    :try_start_0
    const-string v0, "RSA/ECB/PKCS1Padding"

    .line 247
    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    const/4 v1, 0x1

    .line 248
    iget-object v2, p0, Lcom/squareup/encryption/JweEncryptor;->secureRandom:Ljava/security/SecureRandom;

    invoke-virtual {v0, v1, p1, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/SecureRandom;)V

    .line 249
    invoke-virtual {v0, p2}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object p1
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    goto :goto_0

    :catch_1
    move-exception p1

    goto :goto_0

    :catch_2
    move-exception p1

    goto :goto_0

    :catch_3
    move-exception p1

    .line 253
    :goto_0
    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method

.method private static zeroOutBytes([B)V
    .locals 3

    if-nez p0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 290
    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_1

    .line 291
    aput-byte v0, p0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method protected doCompute([B)Lcom/squareup/encryption/CryptoResult;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Lcom/squareup/encryption/CryptoResult<",
            "TK;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    .line 107
    invoke-direct {p0}, Lcom/squareup/encryption/JweEncryptor;->buildHeader()[B

    move-result-object v0

    const/16 v1, 0x10

    new-array v2, v1, [B

    new-array v3, v1, [B

    new-array v1, v1, [B

    .line 114
    iget-object v4, p0, Lcom/squareup/encryption/JweEncryptor;->secureRandom:Ljava/security/SecureRandom;

    invoke-virtual {v4, v2}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 115
    iget-object v4, p0, Lcom/squareup/encryption/JweEncryptor;->secureRandom:Ljava/security/SecureRandom;

    invoke-virtual {v4, v3}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 116
    iget-object v4, p0, Lcom/squareup/encryption/JweEncryptor;->secureRandom:Ljava/security/SecureRandom;

    invoke-virtual {v4, v1}, Ljava/security/SecureRandom;->nextBytes([B)V

    const/16 v4, 0x20

    .line 118
    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 119
    invoke-virtual {v4, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 120
    invoke-virtual {v4, v3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 123
    iget-object v5, p0, Lcom/squareup/encryption/JweEncryptor;->publicKey:Ljava/security/interfaces/RSAPublicKey;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    invoke-direct {p0, v5, v4}, Lcom/squareup/encryption/JweEncryptor;->rsaEncrypt(Ljava/security/interfaces/RSAPublicKey;[B)[B

    move-result-object v4

    .line 126
    invoke-static {p1}, Lcom/squareup/encryption/JweEncryptor;->deflate([B)[B

    move-result-object p1

    invoke-direct {p0, v3, v2, p1}, Lcom/squareup/encryption/JweEncryptor;->aesEncrypt([B[B[B)[B

    move-result-object p1

    const/16 v5, 0xb

    .line 129
    invoke-static {v0, v5}, Landroid/util/Base64;->encode([BI)[B

    move-result-object v6

    .line 131
    array-length v7, v6

    array-length v8, v2

    add-int/2addr v7, v8

    array-length v8, p1

    add-int/2addr v7, v8

    add-int/lit8 v7, v7, 0x8

    .line 134
    invoke-static {v7}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v7

    .line 135
    invoke-virtual {v7, v6}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    move-result-object v7

    .line 136
    invoke-virtual {v7, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    move-result-object v7

    .line 137
    invoke-virtual {v7, p1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    move-result-object v7

    array-length v8, v6

    mul-int/lit8 v8, v8, 0x8

    int-to-long v8, v8

    .line 138
    invoke-virtual {v7, v8, v9}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    move-result-object v7

    .line 141
    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v7

    invoke-direct {p0, v1, v7}, Lcom/squareup/encryption/JweEncryptor;->hmacSha256([B[B)[B

    move-result-object v7

    .line 145
    array-length v8, v0

    array-length v9, v4

    add-int/2addr v8, v9

    array-length v9, v2

    add-int/2addr v8, v9

    array-length v9, p1

    add-int/2addr v8, v9

    array-length v9, v7

    add-int/2addr v8, v9

    add-int/lit8 v8, v8, 0x8

    .line 152
    new-instance v9, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v9, v8}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 153
    :try_start_0
    new-instance v8, Lcom/squareup/encryption/Base64OutputStream;

    invoke-direct {v8, v9, v5}, Lcom/squareup/encryption/Base64OutputStream;-><init>(Ljava/io/OutputStream;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 156
    :try_start_1
    invoke-virtual {v8, v0}, Lcom/squareup/encryption/Base64OutputStream;->writeFinal([B)V

    const/16 v0, 0x2e

    .line 157
    invoke-virtual {v9, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 159
    invoke-virtual {v8, v4}, Lcom/squareup/encryption/Base64OutputStream;->writeFinal([B)V

    .line 160
    invoke-virtual {v9, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 162
    invoke-virtual {v8, v2}, Lcom/squareup/encryption/Base64OutputStream;->writeFinal([B)V

    .line 163
    invoke-virtual {v9, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 165
    invoke-virtual {v8, p1}, Lcom/squareup/encryption/Base64OutputStream;->writeFinal([B)V

    .line 166
    invoke-virtual {v9, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 168
    invoke-virtual {v8, v7}, Lcom/squareup/encryption/Base64OutputStream;->writeFinal([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 169
    :try_start_2
    invoke-virtual {v8}, Lcom/squareup/encryption/Base64OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 172
    invoke-static {v9}, Lcom/squareup/util/Streams;->closeQuietly(Ljava/io/Closeable;)V

    .line 175
    invoke-static {v2}, Lcom/squareup/encryption/JweEncryptor;->zeroOutBytes([B)V

    .line 176
    invoke-static {v3}, Lcom/squareup/encryption/JweEncryptor;->zeroOutBytes([B)V

    .line 177
    invoke-static {v1}, Lcom/squareup/encryption/JweEncryptor;->zeroOutBytes([B)V

    .line 178
    invoke-static {p1}, Lcom/squareup/encryption/JweEncryptor;->zeroOutBytes([B)V

    .line 179
    invoke-static {v4}, Lcom/squareup/encryption/JweEncryptor;->zeroOutBytes([B)V

    .line 180
    invoke-static {v7}, Lcom/squareup/encryption/JweEncryptor;->zeroOutBytes([B)V

    .line 181
    invoke-static {v6}, Lcom/squareup/encryption/JweEncryptor;->zeroOutBytes([B)V

    .line 184
    new-instance p1, Lcom/squareup/encryption/CryptoResult;

    invoke-virtual {p0}, Lcom/squareup/encryption/JweEncryptor;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v9}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-direct {p1, v0, v1}, Lcom/squareup/encryption/CryptoResult;-><init>(Ljava/lang/Object;[B)V

    return-object p1

    :catchall_0
    move-exception v0

    .line 153
    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v5

    .line 169
    :try_start_4
    invoke-virtual {v8}, Lcom/squareup/encryption/Base64OutputStream;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_0

    :catchall_2
    move-exception v8

    :try_start_5
    invoke-virtual {v0, v8}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    :goto_0
    throw v5
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    :catchall_3
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 170
    :try_start_6
    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 172
    :goto_1
    invoke-static {v9}, Lcom/squareup/util/Streams;->closeQuietly(Ljava/io/Closeable;)V

    .line 175
    invoke-static {v2}, Lcom/squareup/encryption/JweEncryptor;->zeroOutBytes([B)V

    .line 176
    invoke-static {v3}, Lcom/squareup/encryption/JweEncryptor;->zeroOutBytes([B)V

    .line 177
    invoke-static {v1}, Lcom/squareup/encryption/JweEncryptor;->zeroOutBytes([B)V

    .line 178
    invoke-static {p1}, Lcom/squareup/encryption/JweEncryptor;->zeroOutBytes([B)V

    .line 179
    invoke-static {v4}, Lcom/squareup/encryption/JweEncryptor;->zeroOutBytes([B)V

    .line 180
    invoke-static {v7}, Lcom/squareup/encryption/JweEncryptor;->zeroOutBytes([B)V

    .line 181
    invoke-static {v6}, Lcom/squareup/encryption/JweEncryptor;->zeroOutBytes([B)V

    .line 182
    throw v0
.end method

.method public withAdditionalHeaders(Ljava/util/Map;)Lcom/squareup/encryption/JweEncryptor;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/encryption/JweEncryptor<",
            "TK;>;"
        }
    .end annotation

    .line 196
    new-instance v0, Lcom/squareup/encryption/JweEncryptor;

    invoke-direct {v0, p0, p1}, Lcom/squareup/encryption/JweEncryptor;-><init>(Lcom/squareup/encryption/JweEncryptor;Ljava/util/Map;)V

    return-object v0
.end method
