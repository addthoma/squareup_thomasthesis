.class public final Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;
.super Ljava/lang/Object;
.source "SelectCorrectedAddressCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J$\u0010\u0007\u001a\u00020\u00082\u001c\u0010\t\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00020\r0\u000bj\u0002`\u000e0\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;",
        "",
        "configuration",
        "Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;",
        "spinner",
        "Lcom/squareup/register/widgets/GlassSpinner;",
        "(Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;Lcom/squareup/register/widgets/GlassSpinner;)V",
        "create",
        "Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData;",
        "Lcom/squareup/mailorder/OrderEvent$CorrectedAddressEvent;",
        "Lcom/squareup/mailorder/SelectCorrectedAddressScreen;",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final configuration:Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;

.field private final spinner:Lcom/squareup/register/widgets/GlassSpinner;


# direct methods
.method public constructor <init>(Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;Lcom/squareup/register/widgets/GlassSpinner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "configuration"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "spinner"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;->configuration:Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;

    iput-object p2, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;->spinner:Lcom/squareup/register/widgets/GlassSpinner;

    return-void
.end method


# virtual methods
.method public final create(Lio/reactivex/Observable;)Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData;",
            "Lcom/squareup/mailorder/OrderEvent$CorrectedAddressEvent;",
            ">;>;)",
            "Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    new-instance v0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;

    iget-object v1, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;->configuration:Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;

    iget-object v2, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;->spinner:Lcom/squareup/register/widgets/GlassSpinner;

    const/4 v3, 0x0

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;Lcom/squareup/register/widgets/GlassSpinner;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
