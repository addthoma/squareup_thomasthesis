.class public final Lcom/squareup/mailorder/OrderServiceHelperKt;
.super Ljava/lang/Object;
.source "OrderServiceHelper.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderServiceHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderServiceHelper.kt\ncom/squareup/mailorder/OrderServiceHelperKt\n*L\n1#1,276:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u000c\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0000\u00a8\u0006\u0003"
    }
    d2 = {
        "toContactInfo",
        "Lcom/squareup/mailorder/ContactInfo;",
        "Lcom/squareup/server/account/MerchantProfileResponse;",
        "mail-order_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final toContactInfo(Lcom/squareup/server/account/MerchantProfileResponse;)Lcom/squareup/mailorder/ContactInfo;
    .locals 9

    const-string v0, "$this$toContactInfo"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-virtual {p0}, Lcom/squareup/server/account/MerchantProfileResponse;->isSuccessful()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 40
    iget-object v0, p0, Lcom/squareup/server/account/MerchantProfileResponse;->entity:Lcom/squareup/server/account/MerchantProfileResponse$Entity;

    iget-object v0, v0, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->name:Ljava/lang/String;

    const-string v1, ""

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    iget-object v2, p0, Lcom/squareup/server/account/MerchantProfileResponse;->entity:Lcom/squareup/server/account/MerchantProfileResponse$Entity;

    iget-object v2, v2, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->phone:Ljava/lang/String;

    if-eqz v2, :cond_1

    move-object v1, v2

    :cond_1
    sget-object v2, Lcom/squareup/address/Address;->Companion:Lcom/squareup/address/Address$Companion;

    iget-object p0, p0, Lcom/squareup/server/account/MerchantProfileResponse;->entity:Lcom/squareup/server/account/MerchantProfileResponse$Entity;

    invoke-virtual {v2, p0}, Lcom/squareup/address/Address$Companion;->from(Lcom/squareup/server/account/MerchantProfileResponse$Entity;)Lcom/squareup/address/Address;

    move-result-object p0

    new-instance v2, Lcom/squareup/mailorder/ContactInfo;

    invoke-direct {v2, v0, v1, p0}, Lcom/squareup/mailorder/ContactInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/address/Address;)V

    move-object p0, v2

    goto :goto_1

    .line 42
    :cond_2
    new-instance p0, Lcom/squareup/mailorder/ContactInfo;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x7

    const/4 v8, 0x0

    move-object v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/squareup/mailorder/ContactInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/address/Address;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :goto_1
    return-object p0
.end method
