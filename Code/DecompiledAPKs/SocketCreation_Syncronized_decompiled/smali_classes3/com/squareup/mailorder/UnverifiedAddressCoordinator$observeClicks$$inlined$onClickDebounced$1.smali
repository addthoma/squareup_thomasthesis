.class public final Lcom/squareup/mailorder/UnverifiedAddressCoordinator$observeClicks$$inlined$onClickDebounced$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/mailorder/UnverifiedAddressCoordinator;->observeClicks(Lcom/squareup/workflow/legacy/Screen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViews.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Views.kt\ncom/squareup/util/Views$onClickDebounced$1\n+ 2 UnverifiedAddressCoordinator.kt\ncom/squareup/mailorder/UnverifiedAddressCoordinator\n*L\n1#1,1322:1\n120#2,6:1323\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006\u00b8\u0006\u0000"
    }
    d2 = {
        "com/squareup/util/Views$onClickDebounced$1",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "view",
        "Landroid/view/View;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen$inlined:Lcom/squareup/workflow/legacy/Screen;


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/mailorder/UnverifiedAddressCoordinator$observeClicks$$inlined$onClickDebounced$1;->$screen$inlined:Lcom/squareup/workflow/legacy/Screen;

    .line 1103
    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1106
    check-cast p1, Lcom/squareup/noho/NohoButton;

    .line 1323
    iget-object p1, p0, Lcom/squareup/mailorder/UnverifiedAddressCoordinator$observeClicks$$inlined$onClickDebounced$1;->$screen$inlined:Lcom/squareup/workflow/legacy/Screen;

    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    instance-of v0, p1, Lcom/squareup/mailorder/UnverifiedAddress$ScreenData$ConfirmAddress;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    check-cast p1, Lcom/squareup/mailorder/UnverifiedAddress$ScreenData$ConfirmAddress;

    if-eqz p1, :cond_1

    .line 1324
    iget-object v0, p0, Lcom/squareup/mailorder/UnverifiedAddressCoordinator$observeClicks$$inlined$onClickDebounced$1;->$screen$inlined:Lcom/squareup/workflow/legacy/Screen;

    iget-object v0, v0, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 1325
    new-instance v1, Lcom/squareup/mailorder/OrderEvent$UnverifiedAddressEvent$SubmitUnverifiedAddress;

    invoke-virtual {p1}, Lcom/squareup/mailorder/UnverifiedAddress$ScreenData$ConfirmAddress;->getContactInfo()Lcom/squareup/mailorder/ContactInfo;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/mailorder/UnverifiedAddress$ScreenData$ConfirmAddress;->getAddress()Lcom/squareup/address/Address;

    move-result-object p1

    invoke-direct {v1, v2, p1}, Lcom/squareup/mailorder/OrderEvent$UnverifiedAddressEvent$SubmitUnverifiedAddress;-><init>(Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/address/Address;)V

    .line 1324
    invoke-interface {v0, v1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method
