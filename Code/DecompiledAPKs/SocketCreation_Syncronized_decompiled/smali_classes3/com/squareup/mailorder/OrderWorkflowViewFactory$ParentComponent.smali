.class public interface abstract Lcom/squareup/mailorder/OrderWorkflowViewFactory$ParentComponent;
.super Ljava/lang/Object;
.source "OrderWorkflowViewFactory.kt"

# interfaces
.implements Lcom/squareup/address/AddressLayout$Component;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/mailorder/OrderWorkflowViewFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ParentComponent"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001\u00a8\u0006\u0002"
    }
    d2 = {
        "Lcom/squareup/mailorder/OrderWorkflowViewFactory$ParentComponent;",
        "Lcom/squareup/address/AddressLayout$Component;",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation
