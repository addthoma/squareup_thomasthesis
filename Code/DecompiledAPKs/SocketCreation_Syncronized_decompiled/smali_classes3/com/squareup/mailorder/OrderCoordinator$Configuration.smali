.class public final Lcom/squareup/mailorder/OrderCoordinator$Configuration;
.super Ljava/lang/Object;
.source "OrderCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/mailorder/OrderCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Configuration"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\r\u0018\u00002\u00020\u0001BE\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0006\u0012\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u0012\u0008\u0008\u0003\u0010\t\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\n\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\n\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\rR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0011\u0010\t\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\r\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/mailorder/OrderCoordinator$Configuration;",
        "",
        "orderButtonLabel",
        "",
        "actionBarTitle",
        "showName",
        "",
        "glyph",
        "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
        "title",
        "message",
        "(IIZLcom/squareup/glyph/GlyphTypeface$Glyph;II)V",
        "getActionBarTitle",
        "()I",
        "getGlyph",
        "()Lcom/squareup/glyph/GlyphTypeface$Glyph;",
        "getMessage",
        "getOrderButtonLabel",
        "getShowName",
        "()Z",
        "getTitle",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBarTitle:I

.field private final glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field private final message:I

.field private final orderButtonLabel:I

.field private final showName:Z

.field private final title:I


# direct methods
.method public constructor <init>(III)V
    .locals 9

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0x1c

    const/4 v8, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v6, p3

    invoke-direct/range {v0 .. v8}, Lcom/squareup/mailorder/OrderCoordinator$Configuration;-><init>(IIZLcom/squareup/glyph/GlyphTypeface$Glyph;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(IIZI)V
    .locals 9

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0x18

    const/4 v8, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v6, p4

    invoke-direct/range {v0 .. v8}, Lcom/squareup/mailorder/OrderCoordinator$Configuration;-><init>(IIZLcom/squareup/glyph/GlyphTypeface$Glyph;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(IIZLcom/squareup/glyph/GlyphTypeface$Glyph;I)V
    .locals 9

    const/4 v5, 0x0

    const/16 v7, 0x10

    const/4 v8, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v6, p5

    invoke-direct/range {v0 .. v8}, Lcom/squareup/mailorder/OrderCoordinator$Configuration;-><init>(IIZLcom/squareup/glyph/GlyphTypeface$Glyph;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(IIZLcom/squareup/glyph/GlyphTypeface$Glyph;II)V
    .locals 0

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/mailorder/OrderCoordinator$Configuration;->orderButtonLabel:I

    iput p2, p0, Lcom/squareup/mailorder/OrderCoordinator$Configuration;->actionBarTitle:I

    iput-boolean p3, p0, Lcom/squareup/mailorder/OrderCoordinator$Configuration;->showName:Z

    iput-object p4, p0, Lcom/squareup/mailorder/OrderCoordinator$Configuration;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iput p5, p0, Lcom/squareup/mailorder/OrderCoordinator$Configuration;->title:I

    iput p6, p0, Lcom/squareup/mailorder/OrderCoordinator$Configuration;->message:I

    return-void
.end method

.method public synthetic constructor <init>(IIZLcom/squareup/glyph/GlyphTypeface$Glyph;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p8, p7, 0x4

    if-eqz p8, :cond_0

    const/4 p3, 0x0

    const/4 v3, 0x0

    goto :goto_0

    :cond_0
    move v3, p3

    :goto_0
    and-int/lit8 p3, p7, 0x8

    if-eqz p3, :cond_1

    const/4 p3, 0x0

    .line 95
    move-object p4, p3

    check-cast p4, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    :cond_1
    move-object v4, p4

    and-int/lit8 p3, p7, 0x10

    if-eqz p3, :cond_2

    const/4 p5, -0x1

    const/4 v5, -0x1

    goto :goto_1

    :cond_2
    move v5, p5

    :goto_1
    move-object v0, p0

    move v1, p1

    move v2, p2

    move v6, p6

    .line 96
    invoke-direct/range {v0 .. v6}, Lcom/squareup/mailorder/OrderCoordinator$Configuration;-><init>(IIZLcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    return-void
.end method


# virtual methods
.method public final getActionBarTitle()I
    .locals 1

    .line 93
    iget v0, p0, Lcom/squareup/mailorder/OrderCoordinator$Configuration;->actionBarTitle:I

    return v0
.end method

.method public final getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/squareup/mailorder/OrderCoordinator$Configuration;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object v0
.end method

.method public final getMessage()I
    .locals 1

    .line 97
    iget v0, p0, Lcom/squareup/mailorder/OrderCoordinator$Configuration;->message:I

    return v0
.end method

.method public final getOrderButtonLabel()I
    .locals 1

    .line 92
    iget v0, p0, Lcom/squareup/mailorder/OrderCoordinator$Configuration;->orderButtonLabel:I

    return v0
.end method

.method public final getShowName()Z
    .locals 1

    .line 94
    iget-boolean v0, p0, Lcom/squareup/mailorder/OrderCoordinator$Configuration;->showName:Z

    return v0
.end method

.method public final getTitle()I
    .locals 1

    .line 96
    iget v0, p0, Lcom/squareup/mailorder/OrderCoordinator$Configuration;->title:I

    return v0
.end method
