.class public final Lcom/squareup/mailorder/OrderServiceHelper_Factory;
.super Ljava/lang/Object;
.source "OrderServiceHelper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/mailorder/OrderServiceHelper;",
        ">;"
    }
.end annotation


# instance fields
.field private final endpointsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;",
            ">;"
        }
    .end annotation
.end field

.field private final messageFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final profileUpdaterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantprofile/MerchantProfileUpdater;",
            ">;"
        }
    .end annotation
.end field

.field private final resourcesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantprofile/MerchantProfileUpdater;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/mailorder/OrderServiceHelper_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p2, p0, Lcom/squareup/mailorder/OrderServiceHelper_Factory;->profileUpdaterProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p3, p0, Lcom/squareup/mailorder/OrderServiceHelper_Factory;->resourcesProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p4, p0, Lcom/squareup/mailorder/OrderServiceHelper_Factory;->messageFactoryProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p5, p0, Lcom/squareup/mailorder/OrderServiceHelper_Factory;->endpointsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/mailorder/OrderServiceHelper_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantprofile/MerchantProfileUpdater;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;",
            ">;)",
            "Lcom/squareup/mailorder/OrderServiceHelper_Factory;"
        }
    .end annotation

    .line 50
    new-instance v6, Lcom/squareup/mailorder/OrderServiceHelper_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/mailorder/OrderServiceHelper_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/merchantprofile/MerchantProfileUpdater;Landroid/content/res/Resources;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;)Lcom/squareup/mailorder/OrderServiceHelper;
    .locals 7

    .line 56
    new-instance v6, Lcom/squareup/mailorder/OrderServiceHelper;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/mailorder/OrderServiceHelper;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/merchantprofile/MerchantProfileUpdater;Landroid/content/res/Resources;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/mailorder/OrderServiceHelper;
    .locals 5

    .line 43
    iget-object v0, p0, Lcom/squareup/mailorder/OrderServiceHelper_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, p0, Lcom/squareup/mailorder/OrderServiceHelper_Factory;->profileUpdaterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/merchantprofile/MerchantProfileUpdater;

    iget-object v2, p0, Lcom/squareup/mailorder/OrderServiceHelper_Factory;->resourcesProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    iget-object v3, p0, Lcom/squareup/mailorder/OrderServiceHelper_Factory;->messageFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/receiving/FailureMessageFactory;

    iget-object v4, p0, Lcom/squareup/mailorder/OrderServiceHelper_Factory;->endpointsProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/mailorder/OrderServiceHelper_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/merchantprofile/MerchantProfileUpdater;Landroid/content/res/Resources;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;)Lcom/squareup/mailorder/OrderServiceHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderServiceHelper_Factory;->get()Lcom/squareup/mailorder/OrderServiceHelper;

    move-result-object v0

    return-object v0
.end method
