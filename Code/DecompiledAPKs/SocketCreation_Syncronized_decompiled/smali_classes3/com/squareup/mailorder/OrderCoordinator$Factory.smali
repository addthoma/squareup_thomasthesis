.class public final Lcom/squareup/mailorder/OrderCoordinator$Factory;
.super Ljava/lang/Object;
.source "OrderCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/mailorder/OrderCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B)\u0008\u0001\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ$\u0010\u000b\u001a\u00020\u000c2\u001c\u0010\r\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00110\u000fj\u0002`\u00120\u000eR\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/mailorder/OrderCoordinator$Factory;",
        "",
        "scrubber",
        "Lcom/squareup/text/InsertingScrubber;",
        "res",
        "Lcom/squareup/util/Res;",
        "spinner",
        "Lcom/squareup/register/widgets/GlassSpinner;",
        "configuration",
        "Lcom/squareup/mailorder/OrderCoordinator$Configuration;",
        "(Lcom/squareup/text/InsertingScrubber;Lcom/squareup/util/Res;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/mailorder/OrderCoordinator$Configuration;)V",
        "create",
        "Lcom/squareup/mailorder/OrderCoordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/mailorder/Order$ScreenData;",
        "Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent;",
        "Lcom/squareup/mailorder/OrderScreen;",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final configuration:Lcom/squareup/mailorder/OrderCoordinator$Configuration;

.field private final res:Lcom/squareup/util/Res;

.field private final scrubber:Lcom/squareup/text/InsertingScrubber;

.field private final spinner:Lcom/squareup/register/widgets/GlassSpinner;


# direct methods
.method public constructor <init>(Lcom/squareup/text/InsertingScrubber;Lcom/squareup/util/Res;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/mailorder/OrderCoordinator$Configuration;)V
    .locals 1
    .param p1    # Lcom/squareup/text/InsertingScrubber;
        .annotation runtime Lcom/squareup/text/InsertingScrubber$PhoneNumber;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "scrubber"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "spinner"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configuration"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/mailorder/OrderCoordinator$Factory;->scrubber:Lcom/squareup/text/InsertingScrubber;

    iput-object p2, p0, Lcom/squareup/mailorder/OrderCoordinator$Factory;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/mailorder/OrderCoordinator$Factory;->spinner:Lcom/squareup/register/widgets/GlassSpinner;

    iput-object p4, p0, Lcom/squareup/mailorder/OrderCoordinator$Factory;->configuration:Lcom/squareup/mailorder/OrderCoordinator$Configuration;

    return-void
.end method


# virtual methods
.method public final create(Lio/reactivex/Observable;)Lcom/squareup/mailorder/OrderCoordinator;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/mailorder/Order$ScreenData;",
            "Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent;",
            ">;>;)",
            "Lcom/squareup/mailorder/OrderCoordinator;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    new-instance v0, Lcom/squareup/mailorder/OrderCoordinator;

    .line 75
    iget-object v2, p0, Lcom/squareup/mailorder/OrderCoordinator$Factory;->scrubber:Lcom/squareup/text/InsertingScrubber;

    iget-object v3, p0, Lcom/squareup/mailorder/OrderCoordinator$Factory;->res:Lcom/squareup/util/Res;

    iget-object v4, p0, Lcom/squareup/mailorder/OrderCoordinator$Factory;->spinner:Lcom/squareup/register/widgets/GlassSpinner;

    iget-object v5, p0, Lcom/squareup/mailorder/OrderCoordinator$Factory;->configuration:Lcom/squareup/mailorder/OrderCoordinator$Configuration;

    const/4 v7, 0x0

    move-object v1, v0

    move-object v6, p1

    .line 74
    invoke-direct/range {v1 .. v7}, Lcom/squareup/mailorder/OrderCoordinator;-><init>(Lcom/squareup/text/InsertingScrubber;Lcom/squareup/util/Res;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/mailorder/OrderCoordinator$Configuration;Lio/reactivex/Observable;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
