.class public final Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;
.super Ljava/lang/Object;
.source "MailOrderAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/mailorder/MailOrderAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static logShippingDetailsContinueClick(Lcom/squareup/mailorder/MailOrderAnalytics;)V
    .locals 0

    return-void
.end method

.method public static logShippingDetailsCorrectedAddressAlertScreen(Lcom/squareup/mailorder/MailOrderAnalytics;)V
    .locals 0

    return-void
.end method

.method public static logShippingDetailsCorrectedAddressOriginalSubmitted(Lcom/squareup/mailorder/MailOrderAnalytics;)V
    .locals 0

    return-void
.end method

.method public static logShippingDetailsCorrectedAddressRecommendedSubmitted(Lcom/squareup/mailorder/MailOrderAnalytics;)V
    .locals 0

    return-void
.end method

.method public static logShippingDetailsCorrectedAddressScreen(Lcom/squareup/mailorder/MailOrderAnalytics;)V
    .locals 0

    return-void
.end method

.method public static logShippingDetailsMissingInfoErrorScreen(Lcom/squareup/mailorder/MailOrderAnalytics;)V
    .locals 0

    return-void
.end method

.method public static logShippingDetailsScreen(Lcom/squareup/mailorder/MailOrderAnalytics;)V
    .locals 0

    return-void
.end method

.method public static logShippingDetailsServerErrorScreen(Lcom/squareup/mailorder/MailOrderAnalytics;)V
    .locals 0

    return-void
.end method

.method public static logShippingDetailsUnverifiedAddressErrorScreen(Lcom/squareup/mailorder/MailOrderAnalytics;)V
    .locals 0

    return-void
.end method

.method public static logShippingDetailsUnverifiedAddressProceedClick(Lcom/squareup/mailorder/MailOrderAnalytics;)V
    .locals 0

    return-void
.end method

.method public static logShippingDetailsUnverifiedAddressReenterClick(Lcom/squareup/mailorder/MailOrderAnalytics;)V
    .locals 0

    return-void
.end method

.method public static logShippingDetailsUnverifiedAddressScreen(Lcom/squareup/mailorder/MailOrderAnalytics;)V
    .locals 0

    return-void
.end method

.method public static logShippingOrderConfirmedScreen(Lcom/squareup/mailorder/MailOrderAnalytics;)V
    .locals 0

    return-void
.end method

.method public static logShippingOrderConfirmedWithSignatureOnly(Lcom/squareup/mailorder/MailOrderAnalytics;)V
    .locals 0

    return-void
.end method

.method public static logShippingOrderConfirmedWithStampsAndSignature(Lcom/squareup/mailorder/MailOrderAnalytics;)V
    .locals 0

    return-void
.end method

.method public static logShippingOrderConfirmedWithStampsOnly(Lcom/squareup/mailorder/MailOrderAnalytics;)V
    .locals 0

    return-void
.end method

.method public static logShippingOrderErrorScreen(Lcom/squareup/mailorder/MailOrderAnalytics;)V
    .locals 0

    return-void
.end method
