.class public final Lcom/squareup/notificationcenter/applet/NotificationCenterAppletScopeRunner;
.super Ljava/lang/Object;
.source "NotificationCenterAppletScopeRunner.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/notificationcenter/applet/NotificationCenterAppletScope;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/notificationcenter/applet/NotificationCenterAppletScopeRunner;",
        "Lmortar/Scoped;",
        "applet",
        "Lcom/squareup/notificationcenter/applet/NotificationCenterApplet;",
        "(Lcom/squareup/notificationcenter/applet/NotificationCenterApplet;)V",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final applet:Lcom/squareup/notificationcenter/applet/NotificationCenterApplet;


# direct methods
.method public constructor <init>(Lcom/squareup/notificationcenter/applet/NotificationCenterApplet;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "applet"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/notificationcenter/applet/NotificationCenterAppletScopeRunner;->applet:Lcom/squareup/notificationcenter/applet/NotificationCenterApplet;

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    iget-object p1, p0, Lcom/squareup/notificationcenter/applet/NotificationCenterAppletScopeRunner;->applet:Lcom/squareup/notificationcenter/applet/NotificationCenterApplet;

    invoke-virtual {p1}, Lcom/squareup/notificationcenter/applet/NotificationCenterApplet;->select()V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
