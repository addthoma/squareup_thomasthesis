.class public final Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt;
.super Ljava/lang/Object;
.source "NotificationCenterRecyclerFactory.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNotificationCenterRecyclerFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NotificationCenterRecyclerFactory.kt\ncom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt\n+ 2 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec\n*L\n1#1,81:1\n35#2,6:82\n35#2,6:88\n35#2,6:94\n*E\n*S KotlinDebug\n*F\n+ 1 NotificationCenterRecyclerFactory.kt\ncom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt\n*L\n36#1,6:82\n65#1,6:88\n79#1,6:94\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u001e\u0010\u0000\u001a\u00020\u0001*\u0014\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0002H\u0002\u001a\u001e\u0010\u0006\u001a\u00020\u0001*\u0014\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00050\u0002H\u0002\u001a\u001e\u0010\u0008\u001a\u00020\u0001*\u0014\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u00050\u0002H\u0002\u00a8\u0006\n"
    }
    d2 = {
        "createNotificationRow",
        "",
        "Lcom/squareup/cycler/StandardRowSpec;",
        "Lcom/squareup/notificationcenter/ui/NotificationCenterRow;",
        "Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;",
        "Landroid/view/ViewGroup;",
        "createPaddingRow",
        "Lcom/squareup/notificationcenter/ui/NotificationCenterRow$PaddingRow;",
        "createWarningBannerRow",
        "Lcom/squareup/notificationcenter/ui/NotificationCenterRow$WarningBannerRow;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$createNotificationRow(Lcom/squareup/cycler/StandardRowSpec;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt;->createNotificationRow(Lcom/squareup/cycler/StandardRowSpec;)V

    return-void
.end method

.method public static final synthetic access$createPaddingRow(Lcom/squareup/cycler/StandardRowSpec;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt;->createPaddingRow(Lcom/squareup/cycler/StandardRowSpec;)V

    return-void
.end method

.method public static final synthetic access$createWarningBannerRow(Lcom/squareup/cycler/StandardRowSpec;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt;->createWarningBannerRow(Lcom/squareup/cycler/StandardRowSpec;)V

    return-void
.end method

.method private static final createNotificationRow(Lcom/squareup/cycler/StandardRowSpec;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/StandardRowSpec<",
            "Lcom/squareup/notificationcenter/ui/NotificationCenterRow;",
            "Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;",
            "Landroid/view/ViewGroup;",
            ">;)V"
        }
    .end annotation

    .line 36
    sget v0, Lcom/squareup/notificationcenter/impl/R$layout;->notification_row:I

    .line 82
    new-instance v1, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createNotificationRow$$inlined$create$1;

    invoke-direct {v1, v0}, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createNotificationRow$$inlined$create$1;-><init>(I)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p0, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method private static final createPaddingRow(Lcom/squareup/cycler/StandardRowSpec;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/StandardRowSpec<",
            "Lcom/squareup/notificationcenter/ui/NotificationCenterRow;",
            "Lcom/squareup/notificationcenter/ui/NotificationCenterRow$PaddingRow;",
            "Landroid/view/ViewGroup;",
            ">;)V"
        }
    .end annotation

    .line 79
    sget v0, Lcom/squareup/notificationcenter/impl/R$layout;->notification_center_padding_row:I

    .line 94
    new-instance v1, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createPaddingRow$$inlined$create$1;

    invoke-direct {v1, v0}, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createPaddingRow$$inlined$create$1;-><init>(I)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p0, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method private static final createWarningBannerRow(Lcom/squareup/cycler/StandardRowSpec;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/StandardRowSpec<",
            "Lcom/squareup/notificationcenter/ui/NotificationCenterRow;",
            "Lcom/squareup/notificationcenter/ui/NotificationCenterRow$WarningBannerRow;",
            "Landroid/view/ViewGroup;",
            ">;)V"
        }
    .end annotation

    .line 65
    sget v0, Lcom/squareup/notificationcenter/impl/R$layout;->warning_banner_row:I

    .line 88
    new-instance v1, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createWarningBannerRow$$inlined$create$1;

    invoke-direct {v1, v0}, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createWarningBannerRow$$inlined$create$1;-><init>(I)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p0, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method
