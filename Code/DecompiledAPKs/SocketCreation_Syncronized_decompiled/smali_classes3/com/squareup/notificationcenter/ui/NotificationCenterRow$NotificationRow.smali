.class public final Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;
.super Lcom/squareup/notificationcenter/ui/NotificationCenterRow;
.source "NotificationCenterRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/notificationcenter/ui/NotificationCenterRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NotificationRow"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0016\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u00002\u00020\u0001BV\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006\u0012\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0006\u0012\u0008\u0008\u0001\u0010\u0008\u001a\u00020\u0006\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0019\u0010\u000b\u001a\u0015\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u000c\u00a2\u0006\u0002\u0008\u000f\u00a2\u0006\u0002\u0010\u0010J\t\u0010\u001c\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001d\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001e\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u001f\u001a\u00020\u0006H\u00c6\u0003J\t\u0010 \u001a\u00020\u0006H\u00c6\u0003J\t\u0010!\u001a\u00020\nH\u00c6\u0003J\u001c\u0010\"\u001a\u0015\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u000c\u00a2\u0006\u0002\u0008\u000fH\u00c6\u0003Jb\u0010#\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0003\u0010\u0007\u001a\u00020\u00062\u0008\u0008\u0003\u0010\u0008\u001a\u00020\u00062\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\u001b\u0008\u0002\u0010\u000b\u001a\u0015\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u000c\u00a2\u0006\u0002\u0008\u000fH\u00c6\u0001J\u0013\u0010$\u001a\u00020\n2\u0008\u0010%\u001a\u0004\u0018\u00010&H\u00d6\u0003J\t\u0010\'\u001a\u00020\u0006H\u00d6\u0001J\t\u0010(\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0008\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\u0007\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0014R$\u0010\u000b\u001a\u0015\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u000c\u00a2\u0006\u0002\u0008\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0014R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u0012\u00a8\u0006)"
    }
    d2 = {
        "Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;",
        "Lcom/squareup/notificationcenter/ui/NotificationCenterRow;",
        "title",
        "",
        "content",
        "textColor",
        "",
        "dotDrawable",
        "dotContentDescription",
        "showOpenInBrowserIcon",
        "",
        "onClickHandler",
        "Lkotlin/Function1;",
        "Landroid/view/View;",
        "",
        "Lkotlin/ExtensionFunctionType;",
        "(Ljava/lang/String;Ljava/lang/String;IIIZLkotlin/jvm/functions/Function1;)V",
        "getContent",
        "()Ljava/lang/String;",
        "getDotContentDescription",
        "()I",
        "getDotDrawable",
        "getOnClickHandler",
        "()Lkotlin/jvm/functions/Function1;",
        "getShowOpenInBrowserIcon",
        "()Z",
        "getTextColor",
        "getTitle",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final content:Ljava/lang/String;

.field private final dotContentDescription:I

.field private final dotDrawable:I

.field private final onClickHandler:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final showOpenInBrowserIcon:Z

.field private final textColor:I

.field private final title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IIIZLkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IIIZ",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "content"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 18
    invoke-direct {p0, v0}, Lcom/squareup/notificationcenter/ui/NotificationCenterRow;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->title:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->content:Ljava/lang/String;

    iput p3, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->textColor:I

    iput p4, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->dotDrawable:I

    iput p5, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->dotContentDescription:I

    iput-boolean p6, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->showOpenInBrowserIcon:Z

    iput-object p7, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->onClickHandler:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;Ljava/lang/String;Ljava/lang/String;IIIZLkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-object p1, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->title:Ljava/lang/String;

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-object p2, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->content:Ljava/lang/String;

    :cond_1
    move-object p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget p3, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->textColor:I

    :cond_2
    move v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget p4, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->dotDrawable:I

    :cond_3
    move v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget p5, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->dotContentDescription:I

    :cond_4
    move v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget-boolean p6, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->showOpenInBrowserIcon:Z

    :cond_5
    move v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    iget-object p7, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->onClickHandler:Lkotlin/jvm/functions/Function1;

    :cond_6
    move-object v4, p7

    move-object p2, p0

    move-object p3, p1

    move-object p4, p9

    move p5, v0

    move p6, v1

    move p7, v2

    move p8, v3

    move-object p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->copy(Ljava/lang/String;Ljava/lang/String;IIIZLkotlin/jvm/functions/Function1;)Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->title:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->content:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->textColor:I

    return v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->dotDrawable:I

    return v0
.end method

.method public final component5()I
    .locals 1

    iget v0, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->dotContentDescription:I

    return v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->showOpenInBrowserIcon:Z

    return v0
.end method

.method public final component7()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->onClickHandler:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;IIIZLkotlin/jvm/functions/Function1;)Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IIIZ",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;"
        }
    .end annotation

    const-string/jumbo v0, "title"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "content"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;

    move-object v1, v0

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v1 .. v8}, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;-><init>(Ljava/lang/String;Ljava/lang/String;IIIZLkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;

    iget-object v0, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->title:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->title:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->content:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->content:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->textColor:I

    iget v1, p1, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->textColor:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->dotDrawable:I

    iget v1, p1, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->dotDrawable:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->dotContentDescription:I

    iget v1, p1, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->dotContentDescription:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->showOpenInBrowserIcon:Z

    iget-boolean v1, p1, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->showOpenInBrowserIcon:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->onClickHandler:Lkotlin/jvm/functions/Function1;

    iget-object p1, p1, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->onClickHandler:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getContent()Ljava/lang/String;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->content:Ljava/lang/String;

    return-object v0
.end method

.method public final getDotContentDescription()I
    .locals 1

    .line 15
    iget v0, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->dotContentDescription:I

    return v0
.end method

.method public final getDotDrawable()I
    .locals 1

    .line 14
    iget v0, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->dotDrawable:I

    return v0
.end method

.method public final getOnClickHandler()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->onClickHandler:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getShowOpenInBrowserIcon()Z
    .locals 1

    .line 16
    iget-boolean v0, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->showOpenInBrowserIcon:Z

    return v0
.end method

.method public final getTextColor()I
    .locals 1

    .line 13
    iget v0, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->textColor:I

    return v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->title:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->title:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->content:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->textColor:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->dotDrawable:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->dotContentDescription:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->showOpenInBrowserIcon:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->onClickHandler:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NotificationRow(title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", content="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->content:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", textColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->textColor:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", dotDrawable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->dotDrawable:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", dotContentDescription="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->dotContentDescription:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", showOpenInBrowserIcon="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->showOpenInBrowserIcon:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", onClickHandler="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->onClickHandler:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
