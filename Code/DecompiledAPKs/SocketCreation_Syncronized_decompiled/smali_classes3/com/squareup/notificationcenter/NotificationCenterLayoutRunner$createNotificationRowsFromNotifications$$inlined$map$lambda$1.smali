.class final Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$createNotificationRowsFromNotifications$$inlined$map$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "NotificationCenterLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->createNotificationRowsFromNotifications(Ljava/util/List;Lkotlin/jvm/functions/Function1;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroid/view/View;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0003\u00a8\u0006\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "Landroid/view/View;",
        "invoke",
        "com/squareup/notificationcenter/NotificationCenterLayoutRunner$createNotificationRowsFromNotifications$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $it:Lcom/squareup/notificationcenterdata/Notification;

.field final synthetic $onNotificationClicked$inlined:Lkotlin/jvm/functions/Function1;


# direct methods
.method constructor <init>(Lcom/squareup/notificationcenterdata/Notification;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$createNotificationRowsFromNotifications$$inlined$map$lambda$1;->$it:Lcom/squareup/notificationcenterdata/Notification;

    iput-object p2, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$createNotificationRowsFromNotifications$$inlined$map$lambda$1;->$onNotificationClicked$inlined:Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 44
    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$createNotificationRowsFromNotifications$$inlined$map$lambda$1;->invoke(Landroid/view/View;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Landroid/view/View;)V
    .locals 1

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 183
    iget-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$createNotificationRowsFromNotifications$$inlined$map$lambda$1;->$onNotificationClicked$inlined:Lkotlin/jvm/functions/Function1;

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$createNotificationRowsFromNotifications$$inlined$map$lambda$1;->$it:Lcom/squareup/notificationcenterdata/Notification;

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
