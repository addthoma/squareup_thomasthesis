.class public abstract Lcom/squareup/notificationcenter/NotificationCenterCommonModule;
.super Ljava/lang/Object;
.source "NotificationCenterCommonModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH!J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH!J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H!J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H!J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0011\u001a\u00020\u0019H!\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/notificationcenter/NotificationCenterCommonModule;",
        "",
        "()V",
        "bindNotificationCenterAnalytics",
        "Lcom/squareup/notificationcenter/NotificationCenterAnalytics;",
        "analytics",
        "Lcom/squareup/notificationcenter/RealNotificationCenterAnalytics;",
        "bindNotificationCenterScreens",
        "Lcom/squareup/notificationcenter/NotificationCenterScreens;",
        "notificationCenterScreens",
        "Lcom/squareup/notificationcenter/RealNotificationCenterScreens;",
        "bindNotificationCenterViewFactory",
        "Lcom/squareup/notificationcenter/NotificationCenterViewFactory;",
        "viewFactory",
        "Lcom/squareup/notificationcenter/RealNotificationCenterViewFactory;",
        "bindNotificationCenterWorkflow",
        "Lcom/squareup/notificationcenter/NotificationCenterWorkflow;",
        "realNotificationCenterWorkflow",
        "Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;",
        "bindNotificationHandler",
        "Lcom/squareup/notificationcenter/NotificationHandler;",
        "notificationHandler",
        "Lcom/squareup/notificationcenter/RealNotificationHandler;",
        "bindOpenInBrowserDialogWorkflow",
        "Lcom/squareup/notificationcenter/OpenInBrowserDialogWorkflow;",
        "Lcom/squareup/notificationcenter/RealOpenInBrowserDialogWorkflow;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindNotificationCenterAnalytics(Lcom/squareup/notificationcenter/RealNotificationCenterAnalytics;)Lcom/squareup/notificationcenter/NotificationCenterAnalytics;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindNotificationCenterScreens(Lcom/squareup/notificationcenter/RealNotificationCenterScreens;)Lcom/squareup/notificationcenter/NotificationCenterScreens;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindNotificationCenterViewFactory(Lcom/squareup/notificationcenter/RealNotificationCenterViewFactory;)Lcom/squareup/notificationcenter/NotificationCenterViewFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindNotificationCenterWorkflow(Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;)Lcom/squareup/notificationcenter/NotificationCenterWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindNotificationHandler(Lcom/squareup/notificationcenter/RealNotificationHandler;)Lcom/squareup/notificationcenter/NotificationHandler;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindOpenInBrowserDialogWorkflow(Lcom/squareup/notificationcenter/RealOpenInBrowserDialogWorkflow;)Lcom/squareup/notificationcenter/OpenInBrowserDialogWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
