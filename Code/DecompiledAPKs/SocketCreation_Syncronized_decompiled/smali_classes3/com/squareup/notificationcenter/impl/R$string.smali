.class public final Lcom/squareup/notificationcenter/impl/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/notificationcenter/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final account_tab:I = 0x7f12003c

.field public static final network_failure_message_description:I = 0x7f12105f

.field public static final network_failure_message_title:I = 0x7f121060

.field public static final no_account_notifications_view_message:I = 0x7f12107b

.field public static final no_account_notifications_view_title:I = 0x7f12107c

.field public static final no_whats_new_notifications_view_message:I = 0x7f121092

.field public static final no_whats_new_notifications_view_title:I = 0x7f121093

.field public static final notification_center_applet_name:I = 0x7f1210a9

.field public static final notification_open_in_browser_content_description:I = 0x7f1210b4

.field public static final open_in_browser_dialog_dismiss_button:I = 0x7f121165

.field public static final open_in_browser_dialog_unhandled_body:I = 0x7f121166

.field public static final open_in_browser_dialog_view_button:I = 0x7f121167

.field public static final read_notification_dot_content_description:I = 0x7f121564

.field public static final unread_notification_dot_content_description:I = 0x7f121aef

.field public static final whats_new_tab:I = 0x7f121be3


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
