.class public final Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory_Factory_Factory;
.super Ljava/lang/Object;
.source "OpenInBrowserDialogFactory_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory_Factory_Factory;->arg0Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory_Factory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;)",
            "Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory_Factory_Factory;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory_Factory_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory_Factory_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/util/Device;)Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$Factory;
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$Factory;

    invoke-direct {v0, p0}, Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$Factory;-><init>(Lcom/squareup/util/Device;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$Factory;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory_Factory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Device;

    invoke-static {v0}, Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory_Factory_Factory;->newInstance(Lcom/squareup/util/Device;)Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory_Factory_Factory;->get()Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$Factory;

    move-result-object v0

    return-object v0
.end method
