.class public final Lcom/squareup/notificationcenter/RealNotificationCenterAnalyticsKt;
.super Ljava/lang/Object;
.source "RealNotificationCenterAnalytics.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\"\u0018\u0010\u0005\u001a\u00020\u0001*\u00020\u00068BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008\"\u0018\u0010\t\u001a\u00020\u0001*\u00020\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u0004\u00a8\u0006\u000b"
    }
    d2 = {
        "sourceString",
        "",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "getSourceString",
        "(Lcom/squareup/notificationcenterdata/Notification;)Ljava/lang/String;",
        "string",
        "Lcom/squareup/notificationcenter/NotificationCenterTab;",
        "getString",
        "(Lcom/squareup/notificationcenter/NotificationCenterTab;)Ljava/lang/String;",
        "tabString",
        "getTabString",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$getSourceString$p(Lcom/squareup/notificationcenterdata/Notification;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/notificationcenter/RealNotificationCenterAnalyticsKt;->getSourceString(Lcom/squareup/notificationcenterdata/Notification;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getString$p(Lcom/squareup/notificationcenter/NotificationCenterTab;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/notificationcenter/RealNotificationCenterAnalyticsKt;->getString(Lcom/squareup/notificationcenter/NotificationCenterTab;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getTabString$p(Lcom/squareup/notificationcenterdata/Notification;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/notificationcenter/RealNotificationCenterAnalyticsKt;->getTabString(Lcom/squareup/notificationcenterdata/Notification;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static final getSourceString(Lcom/squareup/notificationcenterdata/Notification;)Ljava/lang/String;
    .locals 1

    .line 235
    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/Notification;->getSource()Lcom/squareup/notificationcenterdata/Notification$Source;

    move-result-object p0

    sget-object v0, Lcom/squareup/notificationcenter/RealNotificationCenterAnalyticsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/Notification$Source;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-ne p0, v0, :cond_0

    const-string p0, "remote"

    goto :goto_0

    .line 237
    :cond_0
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    :cond_1
    const-string p0, "local"

    :goto_0
    return-object p0
.end method

.method private static final getString(Lcom/squareup/notificationcenter/NotificationCenterTab;)Ljava/lang/String;
    .locals 1

    .line 224
    instance-of v0, p0, Lcom/squareup/notificationcenter/NotificationCenterTab$AccountTab;

    if-eqz v0, :cond_0

    const-string p0, "account"

    goto :goto_0

    .line 225
    :cond_0
    instance-of p0, p0, Lcom/squareup/notificationcenter/NotificationCenterTab$WhatsNewTab;

    if-eqz p0, :cond_1

    const-string p0, "product"

    :goto_0
    return-object p0

    :cond_1
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method private static final getTabString(Lcom/squareup/notificationcenterdata/Notification;)Ljava/lang/String;
    .locals 1

    .line 229
    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/Notification;->getPriority()Lcom/squareup/notificationcenterdata/Notification$Priority;

    move-result-object p0

    .line 230
    instance-of v0, p0, Lcom/squareup/notificationcenterdata/Notification$Priority$Important;

    if-eqz v0, :cond_0

    const-string p0, "account"

    goto :goto_0

    .line 231
    :cond_0
    instance-of p0, p0, Lcom/squareup/notificationcenterdata/Notification$Priority$General;

    if-eqz p0, :cond_1

    const-string p0, "product"

    :goto_0
    return-object p0

    :cond_1
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method
