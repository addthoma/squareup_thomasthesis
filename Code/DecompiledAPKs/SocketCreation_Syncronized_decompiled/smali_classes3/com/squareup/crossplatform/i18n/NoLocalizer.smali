.class public Lcom/squareup/crossplatform/i18n/NoLocalizer;
.super Ljava/lang/Object;
.source "NoLocalizer.java"

# interfaces
.implements Lcom/squareup/shared/i18n/Localizer;


# direct methods
.method constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public dateTime(Ljava/lang/String;Lcom/squareup/shared/i18n/Localizer$DateTimeType;)Ljava/lang/String;
    .locals 0

    .line 15
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public money(Lcom/squareup/protos/common/Money;Lcom/squareup/shared/i18n/Localizer$MoneyType;)Ljava/lang/String;
    .locals 0

    .line 19
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public percentage(Ljava/lang/String;Lcom/squareup/shared/i18n/Localizer$PercentageType;)Ljava/lang/String;
    .locals 0

    .line 23
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public simpleString(Lcom/squareup/shared/i18n/Key;)Ljava/lang/String;
    .locals 0

    .line 31
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;
    .locals 0

    .line 27
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method
