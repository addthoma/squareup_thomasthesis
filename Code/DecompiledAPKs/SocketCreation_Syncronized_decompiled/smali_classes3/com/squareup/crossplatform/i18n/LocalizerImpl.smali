.class public Lcom/squareup/crossplatform/i18n/LocalizerImpl;
.super Ljava/lang/Object;
.source "LocalizerImpl.java"

# interfaces
.implements Lcom/squareup/shared/i18n/Localizer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/crossplatform/i18n/LocalizerImpl$BaseLocalizedStringBuilder;
    }
.end annotation


# static fields
.field static final ISO_8601:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final discountPercentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field private final longDateFormat:Ljava/text/DateFormat;

.field private final mediumDateFormat:Ljava/text/DateFormat;

.field private final mediumDateFormatNoYear:Ljava/text/DateFormat;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final packageName:Ljava/lang/String;

.field private final percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field private final realCentsMoneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final shortDateFormat:Ljava/text/DateFormat;

.field private final shortDateFormatNoYear:Ljava/text/DateFormat;

.field private final shortMoneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final taxPercentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field private final timeFormat:Ljava/text/DateFormat;

.field private final wholeNumberPercentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 158
    new-instance v0, Lcom/squareup/crossplatform/i18n/LocalizerImpl$1;

    invoke-direct {v0}, Lcom/squareup/crossplatform/i18n/LocalizerImpl$1;-><init>()V

    sput-object v0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->ISO_8601:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/Res;Ljava/lang/String;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Ljava/lang/String;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p1, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->res:Lcom/squareup/util/Res;

    .line 77
    iput-object p2, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->packageName:Ljava/lang/String;

    .line 78
    iput-object p3, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->shortDateFormat:Ljava/text/DateFormat;

    .line 79
    iput-object p4, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->shortDateFormatNoYear:Ljava/text/DateFormat;

    .line 80
    iput-object p5, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->mediumDateFormat:Ljava/text/DateFormat;

    .line 81
    iput-object p6, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->mediumDateFormatNoYear:Ljava/text/DateFormat;

    .line 82
    iput-object p7, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->longDateFormat:Ljava/text/DateFormat;

    .line 83
    iput-object p8, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->timeFormat:Ljava/text/DateFormat;

    .line 84
    iput-object p9, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->percentageFormatter:Lcom/squareup/text/Formatter;

    .line 85
    iput-object p10, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->discountPercentageFormatter:Lcom/squareup/text/Formatter;

    .line 86
    iput-object p11, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->wholeNumberPercentageFormatter:Lcom/squareup/text/Formatter;

    .line 87
    iput-object p12, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->taxPercentageFormatter:Lcom/squareup/text/Formatter;

    .line 88
    iput-object p13, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 89
    iput-object p14, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->shortMoneyFormatter:Lcom/squareup/text/Formatter;

    .line 90
    iput-object p15, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->realCentsMoneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method private static parseIso8601Date(Ljava/lang/String;)Ljava/util/Date;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    const-string v0, "Z"

    const-string v1, "-0000"

    .line 166
    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 169
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x3

    .line 170
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x2

    if-le v1, v2, :cond_0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x3a

    if-ne v1, v2, :cond_0

    .line 171
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    .line 172
    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    .line 173
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 172
    invoke-virtual {p0, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 176
    :cond_0
    sget-object v0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->ISO_8601:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/DateFormat;

    invoke-virtual {v0, p0}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public dateTime(Ljava/lang/String;Lcom/squareup/shared/i18n/Localizer$DateTimeType;)Ljava/lang/String;
    .locals 2

    .line 96
    :try_start_0
    invoke-static {p1}, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->parseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    sget-object v0, Lcom/squareup/crossplatform/i18n/LocalizerImpl$2;->$SwitchMap$com$squareup$shared$i18n$Localizer$DateTimeType:[I

    invoke-virtual {p2}, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 115
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unsupported DateTimeType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 113
    :pswitch_0
    iget-object p2, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->timeFormat:Ljava/text/DateFormat;

    invoke-virtual {p2, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 111
    :pswitch_1
    iget-object p2, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->longDateFormat:Ljava/text/DateFormat;

    invoke-virtual {p2, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 109
    :pswitch_2
    iget-object p2, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->mediumDateFormatNoYear:Ljava/text/DateFormat;

    invoke-virtual {p2, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 107
    :pswitch_3
    iget-object p2, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->mediumDateFormat:Ljava/text/DateFormat;

    invoke-virtual {p2, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 105
    :pswitch_4
    iget-object p2, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->shortDateFormatNoYear:Ljava/text/DateFormat;

    invoke-virtual {p2, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 103
    :pswitch_5
    iget-object p2, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->shortDateFormat:Ljava/text/DateFormat;

    invoke-virtual {p2, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :catch_0
    move-exception p1

    .line 98
    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public money(Lcom/squareup/protos/common/Money;Lcom/squareup/shared/i18n/Localizer$MoneyType;)Ljava/lang/String;
    .locals 2

    .line 146
    sget-object v0, Lcom/squareup/crossplatform/i18n/LocalizerImpl$2;->$SwitchMap$com$squareup$shared$i18n$Localizer$MoneyType:[I

    invoke-virtual {p2}, Lcom/squareup/shared/i18n/Localizer$MoneyType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 152
    iget-object p2, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->realCentsMoneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {p2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 154
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unsupported MoneyType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/squareup/shared/i18n/Localizer$MoneyType;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 150
    :cond_1
    iget-object p2, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->shortMoneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {p2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 148
    :cond_2
    iget-object p2, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {p2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public percentage(Ljava/lang/String;Lcom/squareup/shared/i18n/Localizer$PercentageType;)Ljava/lang/String;
    .locals 2

    .line 120
    invoke-static {p1}, Lcom/squareup/util/Percentage;->fromString(Ljava/lang/String;)Lcom/squareup/util/Percentage;

    move-result-object p1

    .line 121
    sget-object v0, Lcom/squareup/crossplatform/i18n/LocalizerImpl$2;->$SwitchMap$com$squareup$shared$i18n$Localizer$PercentageType:[I

    invoke-virtual {p2}, Lcom/squareup/shared/i18n/Localizer$PercentageType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 129
    iget-object p2, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->wholeNumberPercentageFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {p2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 131
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unsupported PercentageType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/squareup/shared/i18n/Localizer$PercentageType;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 127
    :cond_1
    iget-object p2, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->discountPercentageFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {p2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 125
    :cond_2
    iget-object p2, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->taxPercentageFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {p2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 123
    :cond_3
    iget-object p2, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->percentageFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {p2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public simpleString(Lcom/squareup/shared/i18n/Key;)Ljava/lang/String;
    .locals 0

    .line 142
    invoke-virtual {p0, p1}, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->format()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;
    .locals 3

    .line 136
    iget-object v0, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->res:Lcom/squareup/util/Res;

    invoke-interface {v0}, Lcom/squareup/util/Res;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/shared/i18n/Key;->androidKey()Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->packageName:Ljava/lang/String;

    const-string v2, "string"

    invoke-virtual {v0, p1, v2, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    .line 137
    iget-object v0, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl;->res:Lcom/squareup/util/Res;

    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 138
    new-instance v0, Lcom/squareup/crossplatform/i18n/LocalizerImpl$BaseLocalizedStringBuilder;

    invoke-direct {v0, p1}, Lcom/squareup/crossplatform/i18n/LocalizerImpl$BaseLocalizedStringBuilder;-><init>(Lcom/squareup/phrase/Phrase;)V

    return-object v0
.end method
