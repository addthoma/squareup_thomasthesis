.class public final Lcom/squareup/feetutorial/RatesTourView_MembersInjector;
.super Ljava/lang/Object;
.source "RatesTourView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/feetutorial/RatesTourView;",
        ">;"
    }
.end annotation


# instance fields
.field private final currencyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final picassoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Picasso;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feetutorial/RatesTourScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field

.field private final rateFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/RateFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final shorterMoneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feetutorial/RatesTourScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/RateFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Picasso;",
            ">;)V"
        }
    .end annotation

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/feetutorial/RatesTourView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p2, p0, Lcom/squareup/feetutorial/RatesTourView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p3, p0, Lcom/squareup/feetutorial/RatesTourView_MembersInjector;->shorterMoneyFormatterProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p4, p0, Lcom/squareup/feetutorial/RatesTourView_MembersInjector;->rateFormatterProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p5, p0, Lcom/squareup/feetutorial/RatesTourView_MembersInjector;->currencyProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p6, p0, Lcom/squareup/feetutorial/RatesTourView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p7, p0, Lcom/squareup/feetutorial/RatesTourView_MembersInjector;->picassoProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feetutorial/RatesTourScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/RateFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Picasso;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/feetutorial/RatesTourView;",
            ">;"
        }
    .end annotation

    .line 53
    new-instance v8, Lcom/squareup/feetutorial/RatesTourView_MembersInjector;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/feetutorial/RatesTourView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static injectCurrency(Lcom/squareup/feetutorial/RatesTourView;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 0

    .line 90
    iput-object p1, p0, Lcom/squareup/feetutorial/RatesTourView;->currency:Lcom/squareup/protos/common/CurrencyCode;

    return-void
.end method

.method public static injectDevice(Lcom/squareup/feetutorial/RatesTourView;Lcom/squareup/util/Device;)V
    .locals 0

    .line 73
    iput-object p1, p0, Lcom/squareup/feetutorial/RatesTourView;->device:Lcom/squareup/util/Device;

    return-void
.end method

.method public static injectPicasso(Lcom/squareup/feetutorial/RatesTourView;Lcom/squareup/picasso/Picasso;)V
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/squareup/feetutorial/RatesTourView;->picasso:Lcom/squareup/picasso/Picasso;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/feetutorial/RatesTourView;Ljava/lang/Object;)V
    .locals 0

    .line 68
    check-cast p1, Lcom/squareup/feetutorial/RatesTourScreen$Presenter;

    iput-object p1, p0, Lcom/squareup/feetutorial/RatesTourView;->presenter:Lcom/squareup/feetutorial/RatesTourScreen$Presenter;

    return-void
.end method

.method public static injectRateFormatter(Lcom/squareup/feetutorial/RatesTourView;Lcom/squareup/text/RateFormatter;)V
    .locals 0

    .line 85
    iput-object p1, p0, Lcom/squareup/feetutorial/RatesTourView;->rateFormatter:Lcom/squareup/text/RateFormatter;

    return-void
.end method

.method public static injectRes(Lcom/squareup/feetutorial/RatesTourView;Lcom/squareup/util/Res;)V
    .locals 0

    .line 95
    iput-object p1, p0, Lcom/squareup/feetutorial/RatesTourView;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public static injectShorterMoneyFormatter(Lcom/squareup/feetutorial/RatesTourView;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/feetutorial/RatesTourView;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .line 80
    iput-object p1, p0, Lcom/squareup/feetutorial/RatesTourView;->shorterMoneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/feetutorial/RatesTourView;)V
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/feetutorial/RatesTourView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/feetutorial/RatesTourView_MembersInjector;->injectPresenter(Lcom/squareup/feetutorial/RatesTourView;Ljava/lang/Object;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/feetutorial/RatesTourView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Device;

    invoke-static {p1, v0}, Lcom/squareup/feetutorial/RatesTourView_MembersInjector;->injectDevice(Lcom/squareup/feetutorial/RatesTourView;Lcom/squareup/util/Device;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/feetutorial/RatesTourView_MembersInjector;->shorterMoneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    invoke-static {p1, v0}, Lcom/squareup/feetutorial/RatesTourView_MembersInjector;->injectShorterMoneyFormatter(Lcom/squareup/feetutorial/RatesTourView;Lcom/squareup/text/Formatter;)V

    .line 60
    iget-object v0, p0, Lcom/squareup/feetutorial/RatesTourView_MembersInjector;->rateFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/RateFormatter;

    invoke-static {p1, v0}, Lcom/squareup/feetutorial/RatesTourView_MembersInjector;->injectRateFormatter(Lcom/squareup/feetutorial/RatesTourView;Lcom/squareup/text/RateFormatter;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/feetutorial/RatesTourView_MembersInjector;->currencyProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p1, v0}, Lcom/squareup/feetutorial/RatesTourView_MembersInjector;->injectCurrency(Lcom/squareup/feetutorial/RatesTourView;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/feetutorial/RatesTourView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/feetutorial/RatesTourView_MembersInjector;->injectRes(Lcom/squareup/feetutorial/RatesTourView;Lcom/squareup/util/Res;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/feetutorial/RatesTourView_MembersInjector;->picassoProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/picasso/Picasso;

    invoke-static {p1, v0}, Lcom/squareup/feetutorial/RatesTourView_MembersInjector;->injectPicasso(Lcom/squareup/feetutorial/RatesTourView;Lcom/squareup/picasso/Picasso;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 16
    check-cast p1, Lcom/squareup/feetutorial/RatesTourView;

    invoke-virtual {p0, p1}, Lcom/squareup/feetutorial/RatesTourView_MembersInjector;->injectMembers(Lcom/squareup/feetutorial/RatesTourView;)V

    return-void
.end method
