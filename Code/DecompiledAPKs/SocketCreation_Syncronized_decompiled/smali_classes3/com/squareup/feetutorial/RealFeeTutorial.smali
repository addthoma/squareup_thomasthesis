.class public final Lcom/squareup/feetutorial/RealFeeTutorial;
.super Ljava/lang/Object;
.source "RealFeeTutorial.kt"

# interfaces
.implements Lcom/squareup/feetutorial/FeeTutorial;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealFeeTutorial.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealFeeTutorial.kt\ncom/squareup/feetutorial/RealFeeTutorial\n+ 2 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n*L\n1#1,32:1\n10554#2,2:33\n*E\n*S KotlinDebug\n*F\n+ 1 RealFeeTutorial.kt\ncom/squareup/feetutorial/RealFeeTutorial\n*L\n30#1,2:33\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u000c\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0002\u0010\tJ\u0008\u0010\u0017\u001a\u00020\u0018H\u0016J%\u0010\u0019\u001a\u00020\u000b*\u00020\u00032\u0012\u0010\u0002\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u001b0\u001a\"\u00020\u001bH\u0002\u00a2\u0006\u0002\u0010\u001cR\u0014\u0010\n\u001a\u00020\u000b8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u001b\u0010\u0010\u001a\u00020\u00088BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0013\u0010\u0014\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/feetutorial/RealFeeTutorial;",
        "Lcom/squareup/feetutorial/FeeTutorial;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "lazyFlow",
        "Ldagger/Lazy;",
        "Lflow/Flow;",
        "(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Ldagger/Lazy;)V",
        "canShow",
        "",
        "getCanShow",
        "()Z",
        "getFeatures",
        "()Lcom/squareup/settings/server/Features;",
        "flow",
        "getFlow",
        "()Lflow/Flow;",
        "flow$delegate",
        "Ldagger/Lazy;",
        "getSettings",
        "()Lcom/squareup/settings/server/AccountStatusSettings;",
        "activate",
        "",
        "allEnabled",
        "",
        "Lcom/squareup/settings/server/Features$Feature;",
        "(Lcom/squareup/settings/server/Features;[Lcom/squareup/settings/server/Features$Feature;)Z",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow$delegate:Ldagger/Lazy;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    const-class v2, Lcom/squareup/feetutorial/RealFeeTutorial;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "flow"

    const-string v4, "getFlow()Lflow/Flow;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/feetutorial/RealFeeTutorial;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Ldagger/Lazy;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lazyFlow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/feetutorial/RealFeeTutorial;->features:Lcom/squareup/settings/server/Features;

    iput-object p2, p0, Lcom/squareup/feetutorial/RealFeeTutorial;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 19
    iput-object p3, p0, Lcom/squareup/feetutorial/RealFeeTutorial;->flow$delegate:Ldagger/Lazy;

    return-void
.end method

.method private final varargs allEnabled(Lcom/squareup/settings/server/Features;[Lcom/squareup/settings/server/Features$Feature;)Z
    .locals 4

    .line 33
    array-length v0, p2

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    aget-object v3, p2, v2

    .line 30
    invoke-interface {p1, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v3

    if-nez v3, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    :goto_1
    return v1
.end method

.method private final getFlow()Lflow/Flow;
    .locals 3

    iget-object v0, p0, Lcom/squareup/feetutorial/RealFeeTutorial;->flow$delegate:Ldagger/Lazy;

    sget-object v1, Lcom/squareup/feetutorial/RealFeeTutorial;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lcom/squareup/dagger/LazysKt;->getValue(Ldagger/Lazy;Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    return-object v0
.end method


# virtual methods
.method public activate()V
    .locals 2

    .line 26
    invoke-virtual {p0}, Lcom/squareup/feetutorial/RealFeeTutorial;->getCanShow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27
    invoke-direct {p0}, Lcom/squareup/feetutorial/RealFeeTutorial;->getFlow()Lflow/Flow;

    move-result-object v0

    sget-object v1, Lcom/squareup/feetutorial/RatesTourScreen;->INSTANCE:Lcom/squareup/feetutorial/RatesTourScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 26
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Activated when canShow is false"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public getCanShow()Z
    .locals 5

    .line 22
    iget-object v0, p0, Lcom/squareup/feetutorial/RealFeeTutorial;->features:Lcom/squareup/settings/server/Features;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/squareup/settings/server/Features$Feature;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->FEE_TRANSPARENCY:Lcom/squareup/settings/server/Features$Feature;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->FEE_TRANSPARENCY_TUTORIAL:Lcom/squareup/settings/server/Features$Feature;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    invoke-direct {p0, v0, v1}, Lcom/squareup/feetutorial/RealFeeTutorial;->allEnabled(Lcom/squareup/settings/server/Features;[Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 23
    iget-object v0, p0, Lcom/squareup/feetutorial/RealFeeTutorial;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-static {v0}, Lcom/squareup/feetutorial/RateCategory;->categories(Lcom/squareup/settings/server/AccountStatusSettings;)Ljava/util/List;

    move-result-object v0

    const-string v1, "RateCategory.categories(settings)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v4

    if-eqz v0, :cond_0

    const/4 v3, 0x1

    :cond_0
    return v3
.end method

.method public final getFeatures()Lcom/squareup/settings/server/Features;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/feetutorial/RealFeeTutorial;->features:Lcom/squareup/settings/server/Features;

    return-object v0
.end method

.method public final getSettings()Lcom/squareup/settings/server/AccountStatusSettings;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/feetutorial/RealFeeTutorial;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-object v0
.end method
