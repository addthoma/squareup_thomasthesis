.class public abstract Lcom/squareup/firebase/common/NoFirebaseModule;
.super Ljava/lang/Object;
.source "NoFirebaseModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindFirebase(Lcom/squareup/firebase/common/MockFirebase;)Lcom/squareup/firebase/common/Firebase;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
