.class public final Lcom/squareup/firebase/fcm/FcmPushServiceAvailability;
.super Ljava/lang/Object;
.source "FcmPushServiceAvailability.kt"

# interfaces
.implements Lcom/squareup/push/PushServiceAvailability;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0008\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/firebase/fcm/FcmPushServiceAvailability;",
        "Lcom/squareup/push/PushServiceAvailability;",
        "firebase",
        "Lcom/squareup/firebase/common/Firebase;",
        "(Lcom/squareup/firebase/common/Firebase;)V",
        "isAvailable",
        "",
        "fcm_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final firebase:Lcom/squareup/firebase/common/Firebase;


# direct methods
.method public constructor <init>(Lcom/squareup/firebase/common/Firebase;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "firebase"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/firebase/fcm/FcmPushServiceAvailability;->firebase:Lcom/squareup/firebase/common/Firebase;

    return-void
.end method


# virtual methods
.method public isAvailable()Z
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/firebase/fcm/FcmPushServiceAvailability;->firebase:Lcom/squareup/firebase/common/Firebase;

    invoke-interface {v0}, Lcom/squareup/firebase/common/Firebase;->isAvailable()Z

    move-result v0

    return v0
.end method
