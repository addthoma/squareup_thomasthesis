.class public final Lcom/squareup/firebase/fcm/SharedFcmModule_ProvideFirebaseInstanceIdFactory;
.super Ljava/lang/Object;
.source "SharedFcmModule_ProvideFirebaseInstanceIdFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/google/firebase/iid/FirebaseInstanceId;",
        ">;"
    }
.end annotation


# instance fields
.field private final firebaseAppProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/firebase/FirebaseApp;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/google/firebase/FirebaseApp;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/firebase/fcm/SharedFcmModule_ProvideFirebaseInstanceIdFactory;->firebaseAppProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/firebase/fcm/SharedFcmModule_ProvideFirebaseInstanceIdFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/google/firebase/FirebaseApp;",
            ">;)",
            "Lcom/squareup/firebase/fcm/SharedFcmModule_ProvideFirebaseInstanceIdFactory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/firebase/fcm/SharedFcmModule_ProvideFirebaseInstanceIdFactory;

    invoke-direct {v0, p0}, Lcom/squareup/firebase/fcm/SharedFcmModule_ProvideFirebaseInstanceIdFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideFirebaseInstanceId(Lcom/google/firebase/FirebaseApp;)Lcom/google/firebase/iid/FirebaseInstanceId;
    .locals 1

    .line 37
    invoke-static {p0}, Lcom/squareup/firebase/fcm/SharedFcmModule;->provideFirebaseInstanceId(Lcom/google/firebase/FirebaseApp;)Lcom/google/firebase/iid/FirebaseInstanceId;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/google/firebase/iid/FirebaseInstanceId;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/google/firebase/iid/FirebaseInstanceId;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/firebase/fcm/SharedFcmModule_ProvideFirebaseInstanceIdFactory;->firebaseAppProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/firebase/FirebaseApp;

    invoke-static {v0}, Lcom/squareup/firebase/fcm/SharedFcmModule_ProvideFirebaseInstanceIdFactory;->provideFirebaseInstanceId(Lcom/google/firebase/FirebaseApp;)Lcom/google/firebase/iid/FirebaseInstanceId;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/firebase/fcm/SharedFcmModule_ProvideFirebaseInstanceIdFactory;->get()Lcom/google/firebase/iid/FirebaseInstanceId;

    move-result-object v0

    return-object v0
.end method
