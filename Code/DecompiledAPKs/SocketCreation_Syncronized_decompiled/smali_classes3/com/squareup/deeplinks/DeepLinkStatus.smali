.class public final enum Lcom/squareup/deeplinks/DeepLinkStatus;
.super Ljava/lang/Enum;
.source "DeepLinkStatus.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/deeplinks/DeepLinkStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/deeplinks/DeepLinkStatus;

.field public static final enum INACCESSIBLE:Lcom/squareup/deeplinks/DeepLinkStatus;

.field public static final enum KNOWN:Lcom/squareup/deeplinks/DeepLinkStatus;

.field public static final enum UNRECOGNIZED:Lcom/squareup/deeplinks/DeepLinkStatus;

.field public static final enum WAITING_FOR_BRANCH:Lcom/squareup/deeplinks/DeepLinkStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 5
    new-instance v0, Lcom/squareup/deeplinks/DeepLinkStatus;

    const/4 v1, 0x0

    const-string v2, "KNOWN"

    invoke-direct {v0, v2, v1}, Lcom/squareup/deeplinks/DeepLinkStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->KNOWN:Lcom/squareup/deeplinks/DeepLinkStatus;

    .line 7
    new-instance v0, Lcom/squareup/deeplinks/DeepLinkStatus;

    const/4 v2, 0x1

    const-string v3, "UNRECOGNIZED"

    invoke-direct {v0, v3, v2}, Lcom/squareup/deeplinks/DeepLinkStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->UNRECOGNIZED:Lcom/squareup/deeplinks/DeepLinkStatus;

    .line 9
    new-instance v0, Lcom/squareup/deeplinks/DeepLinkStatus;

    const/4 v3, 0x2

    const-string v4, "WAITING_FOR_BRANCH"

    invoke-direct {v0, v4, v3}, Lcom/squareup/deeplinks/DeepLinkStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->WAITING_FOR_BRANCH:Lcom/squareup/deeplinks/DeepLinkStatus;

    .line 11
    new-instance v0, Lcom/squareup/deeplinks/DeepLinkStatus;

    const/4 v4, 0x3

    const-string v5, "INACCESSIBLE"

    invoke-direct {v0, v5, v4}, Lcom/squareup/deeplinks/DeepLinkStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->INACCESSIBLE:Lcom/squareup/deeplinks/DeepLinkStatus;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/deeplinks/DeepLinkStatus;

    .line 3
    sget-object v5, Lcom/squareup/deeplinks/DeepLinkStatus;->KNOWN:Lcom/squareup/deeplinks/DeepLinkStatus;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/deeplinks/DeepLinkStatus;->UNRECOGNIZED:Lcom/squareup/deeplinks/DeepLinkStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/deeplinks/DeepLinkStatus;->WAITING_FOR_BRANCH:Lcom/squareup/deeplinks/DeepLinkStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/deeplinks/DeepLinkStatus;->INACCESSIBLE:Lcom/squareup/deeplinks/DeepLinkStatus;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->$VALUES:[Lcom/squareup/deeplinks/DeepLinkStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/deeplinks/DeepLinkStatus;
    .locals 1

    .line 3
    const-class v0, Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/deeplinks/DeepLinkStatus;

    return-object p0
.end method

.method public static values()[Lcom/squareup/deeplinks/DeepLinkStatus;
    .locals 1

    .line 3
    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->$VALUES:[Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-virtual {v0}, [Lcom/squareup/deeplinks/DeepLinkStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/deeplinks/DeepLinkStatus;

    return-object v0
.end method
