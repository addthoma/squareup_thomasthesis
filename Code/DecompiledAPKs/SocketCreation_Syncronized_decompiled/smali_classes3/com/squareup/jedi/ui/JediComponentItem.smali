.class public Lcom/squareup/jedi/ui/JediComponentItem;
.super Ljava/lang/Object;
.source "JediComponentItem.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field protected static final ACTIONABLE_KEY:Ljava/lang/String; = "actionable"

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/jedi/ui/JediComponentItem;",
            ">;"
        }
    .end annotation
.end field

.field protected static final EMPHASIS_KEY:Ljava/lang/String; = "emphasis"

.field protected static final ERROR_STATE_KEY:Ljava/lang/String; = "error_state"

.field protected static final IS_HTML_KEY:Ljava/lang/String; = "is_html"

.field protected static final LABEL_KEY:Ljava/lang/String; = "label"

.field protected static final LINK_PANEL_TOKEN_KEY:Ljava/lang/String; = "link_panel_token"

.field protected static final MIN_LENGTH_KEY:Ljava/lang/String; = "min_length"

.field protected static final MULTILINE_KEY:Ljava/lang/String; = "multiline"

.field protected static final NEXT_PANEL_VIEW_KEY:Ljava/lang/String; = "next_panel_view"

.field protected static final PLACEHOLDER_KEY:Ljava/lang/String; = "placeholder"

.field protected static final REQUIRED_KEY:Ljava/lang/String; = "required"

.field protected static final STATE_KEY:Ljava/lang/String; = "state"

.field protected static final SUBLABEL_KEY:Ljava/lang/String; = "sub_label"

.field protected static final TEXT_KEY:Ljava/lang/String; = "text"

.field protected static final TITLE_KEY:Ljava/lang/String; = "title"

.field protected static final TYPE_KEY:Ljava/lang/String; = "type"

.field protected static final URL_KEY:Ljava/lang/String; = "url"

.field protected static final VALIDATE_KEY:Ljava/lang/String; = "validate"

.field protected static final VALUE_KEY:Ljava/lang/String; = "value_label"

.field protected static final VALUE_TYPE:Ljava/lang/String; = "value_type"


# instance fields
.field public final component:Lcom/squareup/protos/jedi/service/Component;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 84
    new-instance v0, Lcom/squareup/jedi/ui/JediComponentItem$1;

    invoke-direct {v0}, Lcom/squareup/jedi/ui/JediComponentItem$1;-><init>()V

    sput-object v0, Lcom/squareup/jedi/ui/JediComponentItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/jedi/service/Component;)V
    .locals 0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/jedi/ui/JediComponentItem;->component:Lcom/squareup/protos/jedi/service/Component;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    if-eqz p1, :cond_2

    .line 47
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_1

    goto :goto_0

    .line 48
    :cond_1
    check-cast p1, Lcom/squareup/jedi/ui/JediComponentItem;

    .line 49
    iget-object v0, p0, Lcom/squareup/jedi/ui/JediComponentItem;->component:Lcom/squareup/protos/jedi/service/Component;

    iget-object p1, p1, Lcom/squareup/jedi/ui/JediComponentItem;->component:Lcom/squareup/protos/jedi/service/Component;

    invoke-static {v0, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_2
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method protected getBooleanParameterOrDefault(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 1

    .line 61
    sget-object v0, Lcom/squareup/jedi/ui/-$$Lambda$lp08LRTaU10V2uF4q1zi-C3uZFA;->INSTANCE:Lcom/squareup/jedi/ui/-$$Lambda$lp08LRTaU10V2uF4q1zi-C3uZFA;

    invoke-virtual {p0, p1, p2, v0}, Lcom/squareup/jedi/ui/JediComponentItem;->getParameterOrDefault(Ljava/lang/String;Ljava/lang/Object;Lrx/functions/Func1;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    return-object p1
.end method

.method protected getParameterOrDefault(Ljava/lang/String;Ljava/lang/Object;Lrx/functions/Func1;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "TT;",
            "Lrx/functions/Func1<",
            "Ljava/lang/String;",
            "TT;>;)TT;"
        }
    .end annotation

    .line 66
    iget-object v0, p0, Lcom/squareup/jedi/ui/JediComponentItem;->component:Lcom/squareup/protos/jedi/service/Component;

    iget-object v0, v0, Lcom/squareup/protos/jedi/service/Component;->parameters:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 67
    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 68
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-interface {p3, p1}, Lrx/functions/Func1;->call(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    return-object p1

    :cond_0
    return-object p2
.end method

.method protected getStringParameterOrEmpty(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 57
    sget-object v0, Lcom/squareup/jedi/ui/-$$Lambda$VEEDUYKkKgh681BQQlSd4cgtJkM;->INSTANCE:Lcom/squareup/jedi/ui/-$$Lambda$VEEDUYKkKgh681BQQlSd4cgtJkM;

    const-string v1, ""

    invoke-virtual {p0, p1, v1, v0}, Lcom/squareup/jedi/ui/JediComponentItem;->getParameterOrDefault(Ljava/lang/String;Ljava/lang/Object;Lrx/functions/Func1;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 53
    iget-object v1, p0, Lcom/squareup/jedi/ui/JediComponentItem;->component:Lcom/squareup/protos/jedi/service/Component;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 81
    iget-object p2, p0, Lcom/squareup/jedi/ui/JediComponentItem;->component:Lcom/squareup/protos/jedi/service/Component;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    return-void
.end method
