.class Lcom/squareup/jedi/ui/JediComponentsAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "JediComponentsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/jedi/ui/JediComponentItemViewHolder<",
        "+",
        "Lcom/squareup/jedi/ui/JediComponentItem;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final UNSUPPORTED_VIEW_TYPE:I = -0x1


# instance fields
.field private final components:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/jedi/ui/JediComponentItem;",
            ">;"
        }
    .end annotation
.end field

.field private final inputHandler:Lcom/squareup/jedi/JediComponentInputHandler;

.field private final jediPanelView:Lcom/squareup/jedi/ui/JediPanelView;

.field private final screenData:Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;


# direct methods
.method constructor <init>(Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Lcom/squareup/jedi/JediComponentInputHandler;Lcom/squareup/jedi/ui/JediPanelView;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/jedi/ui/JediComponentsAdapter;->screenData:Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    .line 39
    iput-object p2, p0, Lcom/squareup/jedi/ui/JediComponentsAdapter;->inputHandler:Lcom/squareup/jedi/JediComponentInputHandler;

    .line 40
    iput-object p3, p0, Lcom/squareup/jedi/ui/JediComponentsAdapter;->jediPanelView:Lcom/squareup/jedi/ui/JediPanelView;

    .line 41
    invoke-virtual {p1}, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->getSearchResults()Ljava/util/List;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 42
    invoke-virtual {p1}, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->getSearchResults()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/jedi/ui/JediComponentsAdapter;->components:Ljava/util/List;

    goto :goto_0

    .line 44
    :cond_0
    iget-object p1, p1, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->components:Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/jedi/ui/JediComponentsAdapter;->components:Ljava/util/List;

    :goto_0
    return-void
.end method

.method private bindComponent(Lcom/squareup/jedi/ui/JediComponentItemViewHolder;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/jedi/ui/JediComponentItem;",
            ">(",
            "Lcom/squareup/jedi/ui/JediComponentItemViewHolder<",
            "TT;>;I)V"
        }
    .end annotation

    .line 75
    iget-object v0, p0, Lcom/squareup/jedi/ui/JediComponentsAdapter;->components:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/jedi/ui/JediComponentItem;

    .line 76
    iget-object v0, p0, Lcom/squareup/jedi/ui/JediComponentsAdapter;->screenData:Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    iget-object v1, p0, Lcom/squareup/jedi/ui/JediComponentsAdapter;->inputHandler:Lcom/squareup/jedi/JediComponentInputHandler;

    invoke-virtual {p1, p2, v0, v1}, Lcom/squareup/jedi/ui/JediComponentItemViewHolder;->onBind(Lcom/squareup/jedi/ui/JediComponentItem;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V

    return-void
.end method

.method private createComponentItemViewHolder(Lcom/squareup/protos/jedi/service/ComponentKind;Landroid/view/ViewGroup;)Lcom/squareup/jedi/ui/JediComponentItemViewHolder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/jedi/service/ComponentKind;",
            "Landroid/view/ViewGroup;",
            ")",
            "Lcom/squareup/jedi/ui/JediComponentItemViewHolder<",
            "+",
            "Lcom/squareup/jedi/ui/JediComponentItem;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 82
    sget-object v0, Lcom/squareup/jedi/ui/JediComponentsAdapter$1;->$SwitchMap$com$squareup$protos$jedi$service$ComponentKind:[I

    invoke-virtual {p1}, Lcom/squareup/protos/jedi/service/ComponentKind;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 100
    :pswitch_0
    new-instance p1, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItemViewHolder;

    invoke-direct {p1, p2}, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItemViewHolder;-><init>(Landroid/view/ViewGroup;)V

    return-object p1

    .line 98
    :pswitch_1
    new-instance p1, Lcom/squareup/jedi/ui/components/JediSectionHeaderComponentItemViewHolder;

    invoke-direct {p1, p2}, Lcom/squareup/jedi/ui/components/JediSectionHeaderComponentItemViewHolder;-><init>(Landroid/view/ViewGroup;)V

    return-object p1

    .line 96
    :pswitch_2
    new-instance p1, Lcom/squareup/jedi/ui/components/JediRowComponentItemViewHolder;

    iget-object v0, p0, Lcom/squareup/jedi/ui/JediComponentsAdapter;->jediPanelView:Lcom/squareup/jedi/ui/JediPanelView;

    invoke-direct {p1, p2, v0}, Lcom/squareup/jedi/ui/components/JediRowComponentItemViewHolder;-><init>(Landroid/view/ViewGroup;Lcom/squareup/jedi/ui/JediPanelView;)V

    return-object p1

    .line 94
    :pswitch_3
    new-instance p1, Lcom/squareup/jedi/ui/components/JediParagraphComponentItemViewHolder;

    invoke-direct {p1, p2}, Lcom/squareup/jedi/ui/components/JediParagraphComponentItemViewHolder;-><init>(Landroid/view/ViewGroup;)V

    return-object p1

    .line 92
    :pswitch_4
    new-instance p1, Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItemViewHolder;

    invoke-direct {p1, p2}, Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItemViewHolder;-><init>(Landroid/view/ViewGroup;)V

    return-object p1

    .line 90
    :pswitch_5
    new-instance p1, Lcom/squareup/jedi/ui/components/JediInputConfirmationComponentItemViewHolder;

    invoke-direct {p1, p2}, Lcom/squareup/jedi/ui/components/JediInputConfirmationComponentItemViewHolder;-><init>(Landroid/view/ViewGroup;)V

    return-object p1

    .line 88
    :pswitch_6
    new-instance p1, Lcom/squareup/jedi/ui/components/JediIconComponentItemViewHolder;

    invoke-direct {p1, p2}, Lcom/squareup/jedi/ui/components/JediIconComponentItemViewHolder;-><init>(Landroid/view/ViewGroup;)V

    return-object p1

    .line 86
    :pswitch_7
    new-instance p1, Lcom/squareup/jedi/ui/components/JediHeadlineComponentItemViewHolder;

    invoke-direct {p1, p2}, Lcom/squareup/jedi/ui/components/JediHeadlineComponentItemViewHolder;-><init>(Landroid/view/ViewGroup;)V

    return-object p1

    .line 84
    :pswitch_8
    new-instance p1, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItemViewHolder;

    invoke-direct {p1, p2}, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItemViewHolder;-><init>(Landroid/view/ViewGroup;)V

    return-object p1

    .line 106
    :cond_0
    :goto_0
    new-instance p1, Lcom/squareup/jedi/ui/components/UnsupportedJediComponentItemViewHolder;

    invoke-direct {p1, p2}, Lcom/squareup/jedi/ui/components/UnsupportedJediComponentItemViewHolder;-><init>(Landroid/view/ViewGroup;)V

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/jedi/ui/JediComponentsAdapter;->components:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/jedi/ui/JediComponentsAdapter;->components:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/jedi/ui/JediComponentItem;

    iget-object p1, p1, Lcom/squareup/jedi/ui/JediComponentItem;->component:Lcom/squareup/protos/jedi/service/Component;

    iget-object p1, p1, Lcom/squareup/protos/jedi/service/Component;->kind:Lcom/squareup/protos/jedi/service/ComponentKind;

    if-eqz p1, :cond_0

    .line 67
    invoke-virtual {p1}, Lcom/squareup/protos/jedi/service/ComponentKind;->getValue()I

    move-result p1

    return p1

    :cond_0
    const/4 p1, -0x1

    return p1
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 26
    check-cast p1, Lcom/squareup/jedi/ui/JediComponentItemViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/jedi/ui/JediComponentsAdapter;->onBindViewHolder(Lcom/squareup/jedi/ui/JediComponentItemViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/jedi/ui/JediComponentItemViewHolder;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/jedi/ui/JediComponentItemViewHolder<",
            "+",
            "Lcom/squareup/jedi/ui/JediComponentItem;",
            ">;I)V"
        }
    .end annotation

    .line 57
    invoke-direct {p0, p1, p2}, Lcom/squareup/jedi/ui/JediComponentsAdapter;->bindComponent(Lcom/squareup/jedi/ui/JediComponentItemViewHolder;I)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 26
    invoke-virtual {p0, p1, p2}, Lcom/squareup/jedi/ui/JediComponentsAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/jedi/ui/JediComponentItemViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/jedi/ui/JediComponentItemViewHolder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Lcom/squareup/jedi/ui/JediComponentItemViewHolder<",
            "+",
            "Lcom/squareup/jedi/ui/JediComponentItem;",
            ">;"
        }
    .end annotation

    .line 50
    invoke-static {p2}, Lcom/squareup/protos/jedi/service/ComponentKind;->fromValue(I)Lcom/squareup/protos/jedi/service/ComponentKind;

    move-result-object p2

    .line 51
    invoke-direct {p0, p2, p1}, Lcom/squareup/jedi/ui/JediComponentsAdapter;->createComponentItemViewHolder(Lcom/squareup/protos/jedi/service/ComponentKind;Landroid/view/ViewGroup;)Lcom/squareup/jedi/ui/JediComponentItemViewHolder;

    move-result-object p1

    return-object p1
.end method
