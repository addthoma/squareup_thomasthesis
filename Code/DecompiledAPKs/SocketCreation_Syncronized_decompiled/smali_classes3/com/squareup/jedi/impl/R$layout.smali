.class public final Lcom/squareup/jedi/impl/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/jedi/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final jedi_banner_view:I = 0x7f0d030a

.field public static final jedi_button_base_component_view:I = 0x7f0d030b

.field public static final jedi_headline_component_view:I = 0x7f0d030c

.field public static final jedi_help_section:I = 0x7f0d030d

.field public static final jedi_icon_component_view:I = 0x7f0d030e

.field public static final jedi_input_confirmation_component_view:I = 0x7f0d030f

.field public static final jedi_instant_answer_component_view:I = 0x7f0d0310

.field public static final jedi_panel_recycler_view:I = 0x7f0d0311

.field public static final jedi_panel_view:I = 0x7f0d0312

.field public static final jedi_paragraph_component_view:I = 0x7f0d0313

.field public static final jedi_row_component_view:I = 0x7f0d0314

.field public static final jedi_section_header_component_view:I = 0x7f0d0315

.field public static final jedi_text_field_component_view:I = 0x7f0d0316

.field public static final jedi_unsupported_component_view:I = 0x7f0d0317


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
