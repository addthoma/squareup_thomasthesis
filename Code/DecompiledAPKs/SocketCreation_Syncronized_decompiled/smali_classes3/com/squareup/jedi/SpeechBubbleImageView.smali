.class public final Lcom/squareup/jedi/SpeechBubbleImageView;
.super Landroidx/appcompat/widget/AppCompatImageView;
.source "SpeechBubbleImageView.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\t\u001a\u00020\nH\u0002J\u0008\u0010\u000b\u001a\u00020\nH\u0002J\u000e\u0010\u000c\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\u000eR\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/jedi/SpeechBubbleImageView;",
        "Landroidx/appcompat/widget/AppCompatImageView;",
        "context",
        "Landroid/content/Context;",
        "resources",
        "Landroid/content/res/Resources;",
        "(Landroid/content/Context;Landroid/content/res/Resources;)V",
        "notificationDrawable",
        "Lcom/squareup/noho/NotificationDrawable;",
        "displayNotificationIndicator",
        "",
        "hideNotificationIndicator",
        "updateNotificationCount",
        "count",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private notificationDrawable:Lcom/squareup/noho/NotificationDrawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;)V
    .locals 8

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resources"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0, p1}, Landroidx/appcompat/widget/AppCompatImageView;-><init>(Landroid/content/Context;)V

    .line 20
    sget v0, Lcom/squareup/jedi/impl/R$id;->messaging_notification_bubble:I

    invoke-virtual {p0, v0}, Lcom/squareup/jedi/SpeechBubbleImageView;->setId(I)V

    .line 22
    new-instance v0, Lcom/squareup/noho/NotificationDrawable;

    sget v3, Lcom/squareup/jedi/impl/R$drawable;->speech_bubble_selector:I

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    move-object v1, v0

    move-object v2, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/noho/NotificationDrawable;-><init>(Landroid/content/Context;IIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v0, p0, Lcom/squareup/jedi/SpeechBubbleImageView;->notificationDrawable:Lcom/squareup/noho/NotificationDrawable;

    .line 23
    iget-object p1, p0, Lcom/squareup/jedi/SpeechBubbleImageView;->notificationDrawable:Lcom/squareup/noho/NotificationDrawable;

    check-cast p1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, p1}, Lcom/squareup/jedi/SpeechBubbleImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 25
    sget p1, Lcom/squareup/marin/R$dimen;->marin_action_bar_button_padding:I

    invoke-virtual {p2, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    const/4 p2, 0x0

    .line 26
    invoke-virtual {p0, p2, p2, p1, p2}, Lcom/squareup/jedi/SpeechBubbleImageView;->setPadding(IIII)V

    return-void
.end method

.method private final displayNotificationIndicator()V
    .locals 2

    .line 39
    iget-object v0, p0, Lcom/squareup/jedi/SpeechBubbleImageView;->notificationDrawable:Lcom/squareup/noho/NotificationDrawable;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NotificationDrawable;->setText(Ljava/lang/String;)V

    return-void
.end method

.method private final hideNotificationIndicator()V
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/jedi/SpeechBubbleImageView;->notificationDrawable:Lcom/squareup/noho/NotificationDrawable;

    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NotificationDrawable;->setText(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final updateNotificationCount(I)V
    .locals 0

    if-lez p1, :cond_0

    .line 31
    invoke-direct {p0}, Lcom/squareup/jedi/SpeechBubbleImageView;->displayNotificationIndicator()V

    goto :goto_0

    .line 33
    :cond_0
    invoke-direct {p0}, Lcom/squareup/jedi/SpeechBubbleImageView;->hideNotificationIndicator()V

    :goto_0
    return-void
.end method
