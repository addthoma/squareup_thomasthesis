.class final Lcom/squareup/jedi/RealJediWorkflow$legacyWorkflowScreen$4;
.super Lkotlin/jvm/internal/Lambda;
.source "RealJediWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/jedi/RealJediWorkflow;->legacyWorkflowScreen(Lcom/squareup/workflow/Sink;Lcom/squareup/jedi/JediWorkflowState;)Lcom/squareup/workflow/legacy/Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/String;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealJediWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealJediWorkflow.kt\ncom/squareup/jedi/RealJediWorkflow$legacyWorkflowScreen$4\n*L\n1#1,179:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "text",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $loading:Z

.field final synthetic $sink:Lcom/squareup/workflow/Sink;


# direct methods
.method constructor <init>(ZLcom/squareup/workflow/Sink;)V
    .locals 0

    iput-boolean p1, p0, Lcom/squareup/jedi/RealJediWorkflow$legacyWorkflowScreen$4;->$loading:Z

    iput-object p2, p0, Lcom/squareup/jedi/RealJediWorkflow$legacyWorkflowScreen$4;->$sink:Lcom/squareup/workflow/Sink;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/jedi/RealJediWorkflow$legacyWorkflowScreen$4;->invoke(Ljava/lang/String;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/String;)V
    .locals 2

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 162
    iget-boolean v0, p0, Lcom/squareup/jedi/RealJediWorkflow$legacyWorkflowScreen$4;->$loading:Z

    if-nez v0, :cond_1

    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 163
    iget-object v0, p0, Lcom/squareup/jedi/RealJediWorkflow$legacyWorkflowScreen$4;->$sink:Lcom/squareup/workflow/Sink;

    new-instance v1, Lcom/squareup/jedi/JediWorkflowAction$SearchJedi;

    invoke-direct {v1, p1}, Lcom/squareup/jedi/JediWorkflowAction$SearchJedi;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method
