.class public final Lcom/squareup/comms/protos/seller/EnteringSaveCustomer;
.super Lcom/squareup/wire/AndroidMessage;
.source "EnteringSaveCustomer.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/protos/seller/EnteringSaveCustomer$Builder;,
        Lcom/squareup/comms/protos/seller/EnteringSaveCustomer$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/comms/protos/seller/EnteringSaveCustomer;",
        "Lcom/squareup/comms/protos/seller/EnteringSaveCustomer$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u0000 \u00112\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0010\u0011B\u000f\u0012\u0008\u0008\u0002\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0003\u001a\u00020\u0004J\u0013\u0010\u0007\u001a\u00020\u00082\u0008\u0010\t\u001a\u0004\u0018\u00010\nH\u0096\u0002J\u0008\u0010\u000b\u001a\u00020\u000cH\u0016J\u0008\u0010\r\u001a\u00020\u0002H\u0016J\u0008\u0010\u000e\u001a\u00020\u000fH\u0016\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/EnteringSaveCustomer;",
        "Lcom/squareup/wire/AndroidMessage;",
        "Lcom/squareup/comms/protos/seller/EnteringSaveCustomer$Builder;",
        "unknownFields",
        "Lokio/ByteString;",
        "(Lokio/ByteString;)V",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "newBuilder",
        "toString",
        "",
        "Builder",
        "Companion",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/comms/protos/seller/EnteringSaveCustomer;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/comms/protos/seller/EnteringSaveCustomer;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/comms/protos/seller/EnteringSaveCustomer$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/comms/protos/seller/EnteringSaveCustomer$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/seller/EnteringSaveCustomer$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/comms/protos/seller/EnteringSaveCustomer;->Companion:Lcom/squareup/comms/protos/seller/EnteringSaveCustomer$Companion;

    .line 52
    new-instance v0, Lcom/squareup/comms/protos/seller/EnteringSaveCustomer$Companion$ADAPTER$1;

    .line 53
    sget-object v1, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    .line 54
    const-class v2, Lcom/squareup/comms/protos/seller/EnteringSaveCustomer;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/comms/protos/seller/EnteringSaveCustomer$Companion$ADAPTER$1;-><init>(Lcom/squareup/wire/FieldEncoding;Lkotlin/reflect/KClass;)V

    check-cast v0, Lcom/squareup/wire/ProtoAdapter;

    sput-object v0, Lcom/squareup/comms/protos/seller/EnteringSaveCustomer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 76
    sget-object v0, Lcom/squareup/wire/AndroidMessage;->Companion:Lcom/squareup/wire/AndroidMessage$Companion;

    sget-object v1, Lcom/squareup/comms/protos/seller/EnteringSaveCustomer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/AndroidMessage$Companion;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/comms/protos/seller/EnteringSaveCustomer;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, v0}, Lcom/squareup/comms/protos/seller/EnteringSaveCustomer;-><init>(Lokio/ByteString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;)V
    .locals 1

    const-string/jumbo v0, "unknownFields"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    sget-object v0, Lcom/squareup/comms/protos/seller/EnteringSaveCustomer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p1}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    return-void
.end method

.method public synthetic constructor <init>(Lokio/ByteString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 23
    sget-object p1, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/comms/protos/seller/EnteringSaveCustomer;-><init>(Lokio/ByteString;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/comms/protos/seller/EnteringSaveCustomer;Lokio/ByteString;ILjava/lang/Object;)Lcom/squareup/comms/protos/seller/EnteringSaveCustomer;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 41
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/EnteringSaveCustomer;->unknownFields()Lokio/ByteString;

    move-result-object p1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/comms/protos/seller/EnteringSaveCustomer;->copy(Lokio/ByteString;)Lcom/squareup/comms/protos/seller/EnteringSaveCustomer;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final copy(Lokio/ByteString;)Lcom/squareup/comms/protos/seller/EnteringSaveCustomer;
    .locals 1

    const-string/jumbo v0, "unknownFields"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    new-instance v0, Lcom/squareup/comms/protos/seller/EnteringSaveCustomer;

    invoke-direct {v0, p1}, Lcom/squareup/comms/protos/seller/EnteringSaveCustomer;-><init>(Lokio/ByteString;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 32
    move-object v0, p0

    check-cast v0, Lcom/squareup/comms/protos/seller/EnteringSaveCustomer;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 33
    :cond_0
    instance-of v0, p1, Lcom/squareup/comms/protos/seller/EnteringSaveCustomer;

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return p1

    .line 34
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/EnteringSaveCustomer;->unknownFields()Lokio/ByteString;

    move-result-object v0

    check-cast p1, Lcom/squareup/comms/protos/seller/EnteringSaveCustomer;

    invoke-virtual {p1}, Lcom/squareup/comms/protos/seller/EnteringSaveCustomer;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public hashCode()I
    .locals 1

    .line 37
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/EnteringSaveCustomer;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    return v0
.end method

.method public newBuilder()Lcom/squareup/comms/protos/seller/EnteringSaveCustomer$Builder;
    .locals 2

    .line 26
    new-instance v0, Lcom/squareup/comms/protos/seller/EnteringSaveCustomer$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/seller/EnteringSaveCustomer$Builder;-><init>()V

    .line 27
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/EnteringSaveCustomer;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/seller/EnteringSaveCustomer$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/EnteringSaveCustomer;->newBuilder()Lcom/squareup/comms/protos/seller/EnteringSaveCustomer$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    const-string v0, "EnteringSaveCustomer{}"

    return-object v0
.end method
