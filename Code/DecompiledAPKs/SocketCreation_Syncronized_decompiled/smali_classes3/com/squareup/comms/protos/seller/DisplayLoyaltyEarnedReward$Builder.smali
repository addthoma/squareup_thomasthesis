.class public final Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DisplayLoyaltyEarnedReward.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;",
        "Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u0016\u001a\u00020\u0002H\u0016J\u000e\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005J\u0015\u0010\u0007\u001a\u00020\u00002\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0017J\u000e\u0010\u0008\u001a\u00020\u00002\u0006\u0010\u0008\u001a\u00020\u0005J\u000e\u0010\t\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u0005J\u000e\u0010\n\u001a\u00020\u00002\u0006\u0010\n\u001a\u00020\u000bJ\u000e\u0010\r\u001a\u00020\u00002\u0006\u0010\r\u001a\u00020\u000bJ\u0010\u0010\u000e\u001a\u00020\u00002\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000fJ\u000e\u0010\u0010\u001a\u00020\u00002\u0006\u0010\u0010\u001a\u00020\u0011J\u0015\u0010\u0012\u001a\u00020\u00002\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010\u0018J\u000e\u0010\u0013\u001a\u00020\u00002\u0006\u0010\u0013\u001a\u00020\u000bJ\u000e\u0010\u0014\u001a\u00020\u00002\u0006\u0010\u0014\u001a\u00020\u000bJ\u000e\u0010\u0015\u001a\u00020\u00002\u0006\u0010\u0015\u001a\u00020\u000fR\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\u0008\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\t\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\n\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u000cR\u0016\u0010\r\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u000cR\u0014\u0010\u000e\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u00118\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0012\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u000cR\u0016\u0010\u0013\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u000cR\u0016\u0010\u0014\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u000cR\u0014\u0010\u0015\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;",
        "()V",
        "dark_theme",
        "",
        "Ljava/lang/Boolean;",
        "is_cash_integration_enabled",
        "is_enrolled",
        "is_newly_enrolled",
        "new_eligible_reward_tiers",
        "",
        "Ljava/lang/Integer;",
        "new_points",
        "phone_token",
        "",
        "points_terminology",
        "Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;",
        "timeout_duration_millis",
        "total_eligible_reward_tiers",
        "total_points",
        "unit_name",
        "build",
        "(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;",
        "(Ljava/lang/Integer;)Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public dark_theme:Ljava/lang/Boolean;

.field public is_cash_integration_enabled:Ljava/lang/Boolean;

.field public is_enrolled:Ljava/lang/Boolean;

.field public is_newly_enrolled:Ljava/lang/Boolean;

.field public new_eligible_reward_tiers:Ljava/lang/Integer;

.field public new_points:Ljava/lang/Integer;

.field public phone_token:Ljava/lang/String;

.field public points_terminology:Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;

.field public timeout_duration_millis:Ljava/lang/Integer;

.field public total_eligible_reward_tiers:Ljava/lang/Integer;

.field public total_points:Ljava/lang/Integer;

.field public unit_name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 218
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;
    .locals 19

    move-object/from16 v0, p0

    .line 325
    new-instance v15, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;

    .line 326
    iget-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->new_points:Ljava/lang/Integer;

    const/4 v3, 0x0

    const/4 v4, 0x2

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 327
    iget-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->total_points:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 328
    iget-object v7, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->points_terminology:Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;

    if-eqz v7, :cond_6

    .line 330
    iget-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->new_eligible_reward_tiers:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 332
    iget-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->total_eligible_reward_tiers:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 334
    iget-object v10, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->unit_name:Ljava/lang/String;

    if-eqz v10, :cond_3

    .line 335
    iget-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->is_enrolled:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    .line 336
    iget-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->is_newly_enrolled:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    .line 338
    iget-object v13, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->phone_token:Ljava/lang/String;

    .line 339
    iget-object v14, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->timeout_duration_millis:Ljava/lang/Integer;

    .line 340
    iget-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->is_cash_integration_enabled:Ljava/lang/Boolean;

    .line 341
    iget-object v2, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->dark_theme:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v16

    .line 342
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v17

    move-object/from16 v18, v1

    move-object v1, v15

    move v2, v5

    move v3, v6

    move-object v4, v7

    move v5, v8

    move v6, v9

    move-object v7, v10

    move v8, v11

    move v9, v12

    move-object v10, v13

    move-object v11, v14

    move-object/from16 v12, v18

    move/from16 v13, v16

    move-object/from16 v14, v17

    .line 325
    invoke-direct/range {v1 .. v14}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;-><init>(IILcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;IILjava/lang/String;ZZLjava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;ZLokio/ByteString;)V

    return-object v15

    :cond_0
    new-array v1, v4, [Ljava/lang/Object;

    aput-object v2, v1, v3

    const-string v2, "dark_theme"

    const/4 v5, 0x1

    aput-object v2, v1, v5

    .line 341
    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_1
    const/4 v5, 0x1

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v1, v2, v3

    const-string v1, "is_newly_enrolled"

    aput-object v1, v2, v5

    .line 336
    invoke-static {v2}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_2
    const/4 v5, 0x1

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v1, v2, v3

    const-string v1, "is_enrolled"

    aput-object v1, v2, v5

    .line 335
    invoke-static {v2}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_3
    const/4 v5, 0x1

    new-array v1, v4, [Ljava/lang/Object;

    aput-object v10, v1, v3

    const-string/jumbo v2, "unit_name"

    aput-object v2, v1, v5

    .line 334
    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_4
    const/4 v5, 0x1

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v1, v2, v3

    const-string/jumbo v1, "total_eligible_reward_tiers"

    aput-object v1, v2, v5

    .line 333
    invoke-static {v2}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_5
    const/4 v5, 0x1

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v1, v2, v3

    const-string v1, "new_eligible_reward_tiers"

    aput-object v1, v2, v5

    .line 331
    invoke-static {v2}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_6
    const/4 v5, 0x1

    new-array v1, v4, [Ljava/lang/Object;

    aput-object v7, v1, v3

    const-string v2, "points_terminology"

    aput-object v2, v1, v5

    .line 328
    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_7
    const/4 v5, 0x1

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v1, v2, v3

    const-string/jumbo v1, "total_points"

    aput-object v1, v2, v5

    .line 327
    invoke-static {v2}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_8
    const/4 v5, 0x1

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v1, v2, v3

    const-string v1, "new_points"

    aput-object v1, v2, v5

    .line 326
    invoke-static {v2}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    throw v1
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 218
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->build()Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final dark_theme(Z)Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;
    .locals 0

    .line 321
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->dark_theme:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final is_cash_integration_enabled(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;
    .locals 0

    .line 316
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->is_cash_integration_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final is_enrolled(Z)Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;
    .locals 0

    .line 290
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->is_enrolled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final is_newly_enrolled(Z)Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;
    .locals 0

    .line 295
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->is_newly_enrolled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final new_eligible_reward_tiers(I)Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;
    .locals 0

    .line 275
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->new_eligible_reward_tiers:Ljava/lang/Integer;

    return-object p0
.end method

.method public final new_points(I)Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;
    .locals 0

    .line 256
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->new_points:Ljava/lang/Integer;

    return-object p0
.end method

.method public final phone_token(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;
    .locals 0

    .line 300
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->phone_token:Ljava/lang/String;

    return-object p0
.end method

.method public final points_terminology(Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;)Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;
    .locals 1

    const-string v0, "points_terminology"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 270
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->points_terminology:Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;

    return-object p0
.end method

.method public final timeout_duration_millis(Ljava/lang/Integer;)Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;
    .locals 0

    .line 308
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->timeout_duration_millis:Ljava/lang/Integer;

    return-object p0
.end method

.method public final total_eligible_reward_tiers(I)Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;
    .locals 0

    .line 280
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->total_eligible_reward_tiers:Ljava/lang/Integer;

    return-object p0
.end method

.method public final total_points(I)Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;
    .locals 0

    .line 264
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->total_points:Ljava/lang/Integer;

    return-object p0
.end method

.method public final unit_name(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;
    .locals 1

    const-string/jumbo v0, "unit_name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 285
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->unit_name:Ljava/lang/String;

    return-object p0
.end method
