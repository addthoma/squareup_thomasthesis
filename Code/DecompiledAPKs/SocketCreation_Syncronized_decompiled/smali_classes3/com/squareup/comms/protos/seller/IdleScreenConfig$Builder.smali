.class public final Lcom/squareup/comms/protos/seller/IdleScreenConfig$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "IdleScreenConfig.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/seller/IdleScreenConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/seller/IdleScreenConfig;",
        "Lcom/squareup/comms/protos/seller/IdleScreenConfig$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0005\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u000c\u001a\u00020\u0002H\u0016J\u000e\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005J\u000e\u0010\u0007\u001a\u00020\u00002\u0006\u0010\u0007\u001a\u00020\u0008J\u000e\u0010\n\u001a\u00020\u00002\u0006\u0010\n\u001a\u00020\u0008J\u000e\u0010\u000b\u001a\u00020\u00002\u0006\u0010\u000b\u001a\u00020\u0005R\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\u00088\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\tR\u0016\u0010\n\u001a\u0004\u0018\u00010\u00088\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\tR\u0016\u0010\u000b\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/IdleScreenConfig$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/seller/IdleScreenConfig;",
        "()V",
        "seconds_per_image",
        "",
        "Ljava/lang/Integer;",
        "show_business_name",
        "",
        "Ljava/lang/Boolean;",
        "show_profile_image",
        "slideshow_tap_index",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public seconds_per_image:Ljava/lang/Integer;

.field public show_business_name:Ljava/lang/Boolean;

.field public show_profile_image:Ljava/lang/Boolean;

.field public slideshow_tap_index:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 110
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/seller/IdleScreenConfig;
    .locals 10

    .line 146
    new-instance v6, Lcom/squareup/comms/protos/seller/IdleScreenConfig;

    .line 147
    iget-object v0, p0, Lcom/squareup/comms/protos/seller/IdleScreenConfig$Builder;->show_business_name:Ljava/lang/Boolean;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 149
    iget-object v0, p0, Lcom/squareup/comms/protos/seller/IdleScreenConfig$Builder;->show_profile_image:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 151
    iget-object v0, p0, Lcom/squareup/comms/protos/seller/IdleScreenConfig$Builder;->seconds_per_image:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 153
    iget-object v0, p0, Lcom/squareup/comms/protos/seller/IdleScreenConfig$Builder;->slideshow_tap_index:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 155
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/IdleScreenConfig$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v6

    move v1, v4

    move v2, v5

    move v3, v7

    move v4, v8

    move-object v5, v9

    .line 146
    invoke-direct/range {v0 .. v5}, Lcom/squareup/comms/protos/seller/IdleScreenConfig;-><init>(ZZIILokio/ByteString;)V

    return-object v6

    :cond_0
    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v2

    const-string v0, "slideshow_tap_index"

    aput-object v0, v3, v1

    .line 153
    invoke-static {v3}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v2

    const-string v0, "seconds_per_image"

    aput-object v0, v3, v1

    .line 151
    invoke-static {v3}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_2
    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v2

    const-string v0, "show_profile_image"

    aput-object v0, v3, v1

    .line 149
    invoke-static {v3}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_3
    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v2

    const-string v0, "show_business_name"

    aput-object v0, v3, v1

    .line 147
    invoke-static {v3}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 110
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/IdleScreenConfig$Builder;->build()Lcom/squareup/comms/protos/seller/IdleScreenConfig;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final seconds_per_image(I)Lcom/squareup/comms/protos/seller/IdleScreenConfig$Builder;
    .locals 0

    .line 134
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/IdleScreenConfig$Builder;->seconds_per_image:Ljava/lang/Integer;

    return-object p0
.end method

.method public final show_business_name(Z)Lcom/squareup/comms/protos/seller/IdleScreenConfig$Builder;
    .locals 0

    .line 124
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/IdleScreenConfig$Builder;->show_business_name:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final show_profile_image(Z)Lcom/squareup/comms/protos/seller/IdleScreenConfig$Builder;
    .locals 0

    .line 129
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/IdleScreenConfig$Builder;->show_profile_image:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final slideshow_tap_index(I)Lcom/squareup/comms/protos/seller/IdleScreenConfig$Builder;
    .locals 0

    .line 142
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/IdleScreenConfig$Builder;->slideshow_tap_index:Ljava/lang/Integer;

    return-object p0
.end method
