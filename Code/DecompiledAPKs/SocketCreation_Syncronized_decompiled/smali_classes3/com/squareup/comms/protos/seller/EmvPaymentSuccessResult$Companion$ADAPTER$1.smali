.class public final Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Companion$ADAPTER$1;
.super Lcom/squareup/wire/ProtoAdapter;
.source "EmvPaymentSuccessResult.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEmvPaymentSuccessResult.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EmvPaymentSuccessResult.kt\ncom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Companion$ADAPTER$1\n+ 2 ProtoReader.kt\ncom/squareup/wire/ProtoReader\n*L\n1#1,266:1\n415#2,7:267\n*E\n*S KotlinDebug\n*F\n+ 1 EmvPaymentSuccessResult.kt\ncom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Companion$ADAPTER$1\n*L\n233#1,7:267\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000-\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0018\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\n\u001a\u00020\u0002H\u0016J\u0010\u0010\r\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\u0002H\u0016\u00a8\u0006\u000e"
    }
    d2 = {
        "com/squareup/comms/protos/seller/EmvPaymentSuccessResult$Companion$ADAPTER$1",
        "Lcom/squareup/wire/ProtoAdapter;",
        "Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;",
        "decode",
        "reader",
        "Lcom/squareup/wire/ProtoReader;",
        "encode",
        "",
        "writer",
        "Lcom/squareup/wire/ProtoWriter;",
        "value",
        "encodedSize",
        "",
        "redact",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/wire/FieldEncoding;Lkotlin/reflect/KClass;)V
    .locals 0

    .line 206
    invoke-direct {p0, p1, p2}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Lkotlin/reflect/KClass;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;
    .locals 16

    move-object/from16 v0, p1

    const-string v1, "reader"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 228
    move-object v2, v1

    check-cast v2, Lokio/ByteString;

    .line 229
    move-object v3, v1

    check-cast v3, Lcom/squareup/comms/protos/seller/TipRequirements;

    .line 230
    move-object v4, v1

    check-cast v4, Lcom/squareup/comms/protos/seller/SignatureRequirements;

    .line 231
    move-object v5, v1

    check-cast v5, Lcom/squareup/comms/protos/seller/ReceiptOptions;

    .line 232
    check-cast v1, Ljava/lang/Long;

    .line 267
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v6

    move-object v9, v2

    move-object v10, v3

    move-object v11, v4

    move-object v12, v5

    .line 269
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v2

    const/4 v3, -0x1

    const/4 v4, 0x2

    const/4 v5, 0x1

    if-ne v2, v3, :cond_1

    .line 273
    invoke-virtual {v0, v6, v7}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object v15

    .line 243
    new-instance v0, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;

    if-eqz v1, :cond_0

    .line 248
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v13

    move-object v8, v0

    .line 243
    invoke-direct/range {v8 .. v15}, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;-><init>(Lokio/ByteString;Lcom/squareup/comms/protos/seller/TipRequirements;Lcom/squareup/comms/protos/seller/SignatureRequirements;Lcom/squareup/comms/protos/seller/ReceiptOptions;JLokio/ByteString;)V

    return-object v0

    :cond_0
    new-array v0, v4, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "amount_covered"

    aput-object v1, v0, v5

    .line 248
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    if-eq v2, v5, :cond_6

    if-eq v2, v4, :cond_5

    const/4 v3, 0x3

    if-eq v2, v3, :cond_4

    const/4 v3, 0x4

    if-eq v2, v3, :cond_3

    const/4 v3, 0x5

    if-eq v2, v3, :cond_2

    .line 240
    invoke-virtual {v0, v2}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 239
    :cond_2
    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v1, v0}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    goto :goto_0

    .line 238
    :cond_3
    sget-object v2, Lcom/squareup/comms/protos/seller/ReceiptOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v2, v0}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/comms/protos/seller/ReceiptOptions;

    move-object v12, v2

    goto :goto_0

    .line 237
    :cond_4
    sget-object v2, Lcom/squareup/comms/protos/seller/SignatureRequirements;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v2, v0}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/comms/protos/seller/SignatureRequirements;

    move-object v11, v2

    goto :goto_0

    .line 236
    :cond_5
    sget-object v2, Lcom/squareup/comms/protos/seller/TipRequirements;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v2, v0}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/comms/protos/seller/TipRequirements;

    move-object v10, v2

    goto :goto_0

    .line 235
    :cond_6
    sget-object v2, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v2, v0}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lokio/ByteString;

    move-object v9, v2

    goto :goto_0
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0

    .line 205
    invoke-virtual {p0, p1}, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Companion$ADAPTER$1;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;)V
    .locals 3

    const-string/jumbo v0, "writer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "value"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 219
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->arpc:Lokio/ByteString;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 220
    sget-object v0, Lcom/squareup/comms/protos/seller/TipRequirements;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->tip_requirements:Lcom/squareup/comms/protos/seller/TipRequirements;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 221
    sget-object v0, Lcom/squareup/comms/protos/seller/SignatureRequirements;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->signature_requirements:Lcom/squareup/comms/protos/seller/SignatureRequirements;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 222
    sget-object v0, Lcom/squareup/comms/protos/seller/ReceiptOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 223
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-wide v1, p2, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->amount_covered:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 224
    invoke-virtual {p2}, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0

    .line 205
    check-cast p2, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Companion$ADAPTER$1;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;)I
    .locals 4

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 211
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->arpc:Lokio/ByteString;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 212
    sget-object v1, Lcom/squareup/comms/protos/seller/TipRequirements;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->tip_requirements:Lcom/squareup/comms/protos/seller/TipRequirements;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 213
    sget-object v1, Lcom/squareup/comms/protos/seller/SignatureRequirements;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->signature_requirements:Lcom/squareup/comms/protos/seller/SignatureRequirements;

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 214
    sget-object v1, Lcom/squareup/comms/protos/seller/ReceiptOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    const/4 v3, 0x4

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 215
    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-wide v2, p1, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->amount_covered:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x5

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 216
    invoke-virtual {p1}, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 205
    check-cast p1, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;

    invoke-virtual {p0, p1}, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Companion$ADAPTER$1;->encodedSize(Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;)Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;
    .locals 12

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 255
    iget-object v0, p1, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->tip_requirements:Lcom/squareup/comms/protos/seller/TipRequirements;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    sget-object v2, Lcom/squareup/comms/protos/seller/TipRequirements;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v2, v0}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/comms/protos/seller/TipRequirements;

    move-object v4, v0

    goto :goto_0

    :cond_0
    move-object v4, v1

    :goto_0
    const/4 v5, 0x0

    .line 257
    iget-object v0, p1, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    if-eqz v0, :cond_1

    sget-object v1, Lcom/squareup/comms/protos/seller/ReceiptOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v1, v0}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/comms/protos/seller/ReceiptOptions;

    :cond_1
    move-object v6, v1

    const-wide/16 v7, 0x0

    .line 258
    sget-object v9, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    const/16 v10, 0x11

    const/4 v11, 0x0

    const/4 v3, 0x0

    move-object v2, p1

    .line 254
    invoke-static/range {v2 .. v11}, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;->copy$default(Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;Lokio/ByteString;Lcom/squareup/comms/protos/seller/TipRequirements;Lcom/squareup/comms/protos/seller/SignatureRequirements;Lcom/squareup/comms/protos/seller/ReceiptOptions;JLokio/ByteString;ILjava/lang/Object;)Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 205
    check-cast p1, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;

    invoke-virtual {p0, p1}, Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult$Companion$ADAPTER$1;->redact(Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;)Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;

    move-result-object p1

    return-object p1
.end method
