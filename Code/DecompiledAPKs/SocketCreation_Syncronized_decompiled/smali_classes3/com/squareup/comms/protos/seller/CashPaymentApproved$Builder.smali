.class public final Lcom/squareup/comms/protos/seller/CashPaymentApproved$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CashPaymentApproved.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/seller/CashPaymentApproved;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/seller/CashPaymentApproved;",
        "Lcom/squareup/comms/protos/seller/CashPaymentApproved$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u000e\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005J\u0008\u0010\t\u001a\u00020\u0002H\u0016J\u0010\u0010\u0007\u001a\u00020\u00002\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008R\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00088\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/CashPaymentApproved$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/seller/CashPaymentApproved;",
        "()V",
        "amount_covered",
        "",
        "Ljava/lang/Long;",
        "receipt_options",
        "Lcom/squareup/comms/protos/seller/ReceiptOptions;",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public amount_covered:Ljava/lang/Long;

.field public receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 88
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public final amount_covered(J)Lcom/squareup/comms/protos/seller/CashPaymentApproved$Builder;
    .locals 0

    .line 107
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/CashPaymentApproved$Builder;->amount_covered:Ljava/lang/Long;

    return-object p0
.end method

.method public build()Lcom/squareup/comms/protos/seller/CashPaymentApproved;
    .locals 5

    .line 111
    new-instance v0, Lcom/squareup/comms/protos/seller/CashPaymentApproved;

    .line 112
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/CashPaymentApproved$Builder;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    .line 113
    iget-object v2, p0, Lcom/squareup/comms/protos/seller/CashPaymentApproved$Builder;->amount_covered:Ljava/lang/Long;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 115
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/CashPaymentApproved$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    .line 111
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/comms/protos/seller/CashPaymentApproved;-><init>(Lcom/squareup/comms/protos/seller/ReceiptOptions;JLokio/ByteString;)V

    return-object v0

    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "amount_covered"

    aput-object v2, v0, v1

    .line 113
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 88
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/CashPaymentApproved$Builder;->build()Lcom/squareup/comms/protos/seller/CashPaymentApproved;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final receipt_options(Lcom/squareup/comms/protos/seller/ReceiptOptions;)Lcom/squareup/comms/protos/seller/CashPaymentApproved$Builder;
    .locals 0

    .line 99
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/CashPaymentApproved$Builder;->receipt_options:Lcom/squareup/comms/protos/seller/ReceiptOptions;

    return-object p0
.end method
