.class public final Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReceiptOptions.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/seller/ReceiptOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/seller/ReceiptOptions;",
        "Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0010\t\n\u0002\u0008\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u000e\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005J\u0015\u0010\u0007\u001a\u00020\u00002\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0013J\u0008\u0010\u0014\u001a\u00020\u0002H\u0016J\u0015\u0010\u0008\u001a\u00020\u00002\u0008\u0010\u0008\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0013J\u0015\u0010\t\u001a\u00020\u00002\u0008\u0010\t\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0013J\u0010\u0010\n\u001a\u00020\u00002\u0008\u0010\n\u001a\u0004\u0018\u00010\u000bJ\u0010\u0010\u000c\u001a\u00020\u00002\u0008\u0010\u000c\u001a\u0004\u0018\u00010\u000bJ\u0015\u0010\r\u001a\u00020\u00002\u0008\u0010\r\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0013J\u0010\u0010\u000e\u001a\u00020\u00002\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000bJ\u0015\u0010\u000f\u001a\u00020\u00002\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0010\u00a2\u0006\u0002\u0010\u0015J\u000e\u0010\u0012\u001a\u00020\u00002\u0006\u0010\u0012\u001a\u00020\u0005R\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\u0008\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\t\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0014\u0010\n\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000c\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\r\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0014\u0010\u000e\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000f\u001a\u0004\u0018\u00010\u00108\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0011R\u0016\u0010\u0012\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/seller/ReceiptOptions;",
        "()V",
        "automatically_print_receipt_to_sign_or_tip",
        "",
        "Ljava/lang/Boolean;",
        "automatically_send_email_receipt",
        "can_print",
        "can_sms",
        "default_email",
        "",
        "default_sms",
        "fast_checkout",
        "order_name",
        "remaining_balance",
        "",
        "Ljava/lang/Long;",
        "skip_receipt_screen",
        "(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;",
        "build",
        "(Ljava/lang/Long;)Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public automatically_print_receipt_to_sign_or_tip:Ljava/lang/Boolean;

.field public automatically_send_email_receipt:Ljava/lang/Boolean;

.field public can_print:Ljava/lang/Boolean;

.field public can_sms:Ljava/lang/Boolean;

.field public default_email:Ljava/lang/String;

.field public default_sms:Ljava/lang/String;

.field public fast_checkout:Ljava/lang/Boolean;

.field public order_name:Ljava/lang/String;

.field public remaining_balance:Ljava/lang/Long;

.field public skip_receipt_screen:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 207
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public final automatically_print_receipt_to_sign_or_tip(Z)Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;
    .locals 0

    .line 299
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;->automatically_print_receipt_to_sign_or_tip:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final automatically_send_email_receipt(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;
    .locals 0

    .line 307
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;->automatically_send_email_receipt:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/comms/protos/seller/ReceiptOptions;
    .locals 17

    move-object/from16 v0, p0

    .line 319
    new-instance v13, Lcom/squareup/comms/protos/seller/ReceiptOptions;

    .line 320
    iget-object v1, v0, Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;->skip_receipt_screen:Ljava/lang/Boolean;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x2

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 322
    iget-object v6, v0, Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;->can_print:Ljava/lang/Boolean;

    .line 323
    iget-object v7, v0, Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;->can_sms:Ljava/lang/Boolean;

    .line 324
    iget-object v8, v0, Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;->default_sms:Ljava/lang/String;

    .line 325
    iget-object v9, v0, Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;->default_email:Ljava/lang/String;

    .line 326
    iget-object v10, v0, Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;->remaining_balance:Ljava/lang/Long;

    .line 327
    iget-object v11, v0, Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;->order_name:Ljava/lang/String;

    .line 328
    iget-object v1, v0, Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;->automatically_print_receipt_to_sign_or_tip:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    .line 331
    iget-object v14, v0, Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;->automatically_send_email_receipt:Ljava/lang/Boolean;

    .line 332
    iget-object v15, v0, Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;->fast_checkout:Ljava/lang/Boolean;

    .line 333
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v16

    move-object v1, v13

    move v2, v5

    move-object v3, v6

    move-object v4, v7

    move-object v5, v8

    move-object v6, v9

    move-object v7, v10

    move-object v8, v11

    move v9, v12

    move-object v10, v14

    move-object v11, v15

    move-object/from16 v12, v16

    .line 319
    invoke-direct/range {v1 .. v12}, Lcom/squareup/comms/protos/seller/ReceiptOptions;-><init>(ZLjava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;ZLjava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v13

    :cond_0
    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v3

    const-string v1, "automatically_print_receipt_to_sign_or_tip"

    aput-object v1, v4, v2

    .line 329
    invoke-static {v4}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_1
    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v3

    const-string v1, "skip_receipt_screen"

    aput-object v1, v4, v2

    .line 320
    invoke-static {v4}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    throw v1
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 207
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;->build()Lcom/squareup/comms/protos/seller/ReceiptOptions;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final can_print(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;
    .locals 0

    .line 250
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;->can_print:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final can_sms(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;
    .locals 0

    .line 258
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;->can_sms:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final default_email(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;
    .locals 0

    .line 274
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;->default_email:Ljava/lang/String;

    return-object p0
.end method

.method public final default_sms(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;
    .locals 0

    .line 266
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;->default_sms:Ljava/lang/String;

    return-object p0
.end method

.method public final fast_checkout(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;
    .locals 0

    .line 315
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;->fast_checkout:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final order_name(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;
    .locals 0

    .line 290
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;->order_name:Ljava/lang/String;

    return-object p0
.end method

.method public final remaining_balance(Ljava/lang/Long;)Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;
    .locals 0

    .line 282
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;->remaining_balance:Ljava/lang/Long;

    return-object p0
.end method

.method public final skip_receipt_screen(Z)Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;
    .locals 0

    .line 242
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/ReceiptOptions$Builder;->skip_receipt_screen:Ljava/lang/Boolean;

    return-object p0
.end method
