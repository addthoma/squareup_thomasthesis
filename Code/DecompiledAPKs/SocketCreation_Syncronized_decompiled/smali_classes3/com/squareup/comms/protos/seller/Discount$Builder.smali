.class public final Lcom/squareup/comms/protos/seller/Discount$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Discount.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/seller/Discount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/seller/Discount;",
        "Lcom/squareup/comms/protos/seller/Discount$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u0007\u001a\u00020\u0002H\u0016J\u0010\u0010\u0004\u001a\u00020\u00002\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005J\u0010\u0010\u0006\u001a\u00020\u00002\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0005R\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/Discount$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/seller/Discount;",
        "()V",
        "id",
        "",
        "name",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public id:Ljava/lang/String;

.field public name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 76
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/seller/Discount;
    .locals 4

    .line 93
    new-instance v0, Lcom/squareup/comms/protos/seller/Discount;

    .line 94
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/Discount$Builder;->id:Ljava/lang/String;

    .line 95
    iget-object v2, p0, Lcom/squareup/comms/protos/seller/Discount$Builder;->name:Ljava/lang/String;

    .line 96
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/Discount$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    .line 93
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/comms/protos/seller/Discount;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 76
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/Discount$Builder;->build()Lcom/squareup/comms/protos/seller/Discount;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final id(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/Discount$Builder;
    .locals 0

    .line 84
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/Discount$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public final name(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/Discount$Builder;
    .locals 0

    .line 89
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/Discount$Builder;->name:Ljava/lang/String;

    return-object p0
.end method
