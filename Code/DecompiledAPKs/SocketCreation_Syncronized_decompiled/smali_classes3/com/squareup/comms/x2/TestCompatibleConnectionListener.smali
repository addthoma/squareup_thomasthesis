.class public Lcom/squareup/comms/x2/TestCompatibleConnectionListener;
.super Ljava/lang/Object;
.source "TestCompatibleConnectionListener.java"

# interfaces
.implements Lcom/squareup/comms/x2/CompatibleConnectionListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/squareup/wire/Message;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/comms/x2/CompatibleConnectionListener<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public onCompatible:I

.field public onCompatibleConnectionLost:I

.field public onIncompatible:I

.field public onIncompatibleConnectionLost:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 9
    iput v0, p0, Lcom/squareup/comms/x2/TestCompatibleConnectionListener;->onIncompatible:I

    .line 10
    iput v0, p0, Lcom/squareup/comms/x2/TestCompatibleConnectionListener;->onIncompatibleConnectionLost:I

    .line 11
    iput v0, p0, Lcom/squareup/comms/x2/TestCompatibleConnectionListener;->onCompatible:I

    .line 12
    iput v0, p0, Lcom/squareup/comms/x2/TestCompatibleConnectionListener;->onCompatibleConnectionLost:I

    return-void
.end method


# virtual methods
.method public onCompatibleConnectionEstablished(Lcom/squareup/comms/RemoteBusConnection;Lcom/squareup/wire/Message;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/comms/RemoteBusConnection;",
            "TT;)V"
        }
    .end annotation

    .line 23
    iget p1, p0, Lcom/squareup/comms/x2/TestCompatibleConnectionListener;->onCompatible:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/squareup/comms/x2/TestCompatibleConnectionListener;->onCompatible:I

    return-void
.end method

.method public onCompatibleConnectionLost()V
    .locals 1

    .line 27
    iget v0, p0, Lcom/squareup/comms/x2/TestCompatibleConnectionListener;->onCompatibleConnectionLost:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/comms/x2/TestCompatibleConnectionListener;->onCompatibleConnectionLost:I

    return-void
.end method

.method public onIncompatibleConnectionEstablished()V
    .locals 1

    .line 15
    iget v0, p0, Lcom/squareup/comms/x2/TestCompatibleConnectionListener;->onIncompatible:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/comms/x2/TestCompatibleConnectionListener;->onIncompatible:I

    return-void
.end method

.method public onIncompatibleConnectionLost()V
    .locals 1

    .line 19
    iget v0, p0, Lcom/squareup/comms/x2/TestCompatibleConnectionListener;->onIncompatibleConnectionLost:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/comms/x2/TestCompatibleConnectionListener;->onIncompatibleConnectionLost:I

    return-void
.end method
