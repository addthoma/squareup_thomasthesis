.class final Lcom/squareup/comms/x2/X2HealthChecker;
.super Ljava/lang/Object;
.source "X2HealthChecker.java"

# interfaces
.implements Lcom/squareup/comms/HealthChecker;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/x2/X2HealthChecker$HealthCheckRunnable;
    }
.end annotation


# static fields
.field private static final HEALTH_CHECK_INTERVAL_MS:I = 0xfa0


# instance fields
.field private final healthCheckIntervalMs:I

.field private healthState:Lcom/squareup/comms/x2/X2HealthState;

.field private final ioThread:Lcom/squareup/comms/common/IoThread;


# direct methods
.method constructor <init>(Lcom/squareup/comms/common/IoThread;)V
    .locals 1

    const/16 v0, 0xfa0

    .line 27
    invoke-direct {p0, p1, v0}, Lcom/squareup/comms/x2/X2HealthChecker;-><init>(Lcom/squareup/comms/common/IoThread;I)V

    return-void
.end method

.method constructor <init>(Lcom/squareup/comms/common/IoThread;I)V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/comms/x2/X2HealthChecker;->ioThread:Lcom/squareup/comms/common/IoThread;

    .line 32
    iput p2, p0, Lcom/squareup/comms/x2/X2HealthChecker;->healthCheckIntervalMs:I

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/comms/x2/X2HealthChecker;)I
    .locals 0

    .line 19
    iget p0, p0, Lcom/squareup/comms/x2/X2HealthChecker;->healthCheckIntervalMs:I

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/comms/x2/X2HealthChecker;)Lcom/squareup/comms/common/IoThread;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/squareup/comms/x2/X2HealthChecker;->ioThread:Lcom/squareup/comms/common/IoThread;

    return-object p0
.end method


# virtual methods
.method public start(Lcom/squareup/comms/MessagePoster;Lcom/squareup/comms/HealthChecker$Callback;)V
    .locals 2

    .line 54
    new-instance v0, Lcom/squareup/comms/x2/X2HealthState;

    invoke-direct {v0, p2}, Lcom/squareup/comms/x2/X2HealthState;-><init>(Lcom/squareup/comms/HealthChecker$Callback;)V

    iput-object v0, p0, Lcom/squareup/comms/x2/X2HealthChecker;->healthState:Lcom/squareup/comms/x2/X2HealthState;

    .line 55
    iget-object p2, p0, Lcom/squareup/comms/x2/X2HealthChecker;->ioThread:Lcom/squareup/comms/common/IoThread;

    new-instance v0, Lcom/squareup/comms/x2/X2HealthChecker$HealthCheckRunnable;

    iget-object v1, p0, Lcom/squareup/comms/x2/X2HealthChecker;->healthState:Lcom/squareup/comms/x2/X2HealthState;

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/comms/x2/X2HealthChecker$HealthCheckRunnable;-><init>(Lcom/squareup/comms/x2/X2HealthChecker;Lcom/squareup/comms/MessagePoster;Lcom/squareup/comms/x2/X2HealthState;)V

    invoke-virtual {p2, v0}, Lcom/squareup/comms/common/IoThread;->post(Ljava/lang/Runnable;)V

    return-void
.end method

.method public stop()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "HealthChecker stop"

    .line 59
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 60
    iget-object v0, p0, Lcom/squareup/comms/x2/X2HealthChecker;->ioThread:Lcom/squareup/comms/common/IoThread;

    invoke-virtual {v0}, Lcom/squareup/comms/common/IoThread;->assertIoThread()V

    .line 61
    iget-object v0, p0, Lcom/squareup/comms/x2/X2HealthChecker;->healthState:Lcom/squareup/comms/x2/X2HealthState;

    if-eqz v0, :cond_0

    .line 62
    invoke-virtual {v0}, Lcom/squareup/comms/x2/X2HealthState;->stop()V

    const/4 v0, 0x0

    .line 63
    iput-object v0, p0, Lcom/squareup/comms/x2/X2HealthChecker;->healthState:Lcom/squareup/comms/x2/X2HealthState;

    :cond_0
    return-void
.end method

.method public tryConsumeMessage(Lcom/squareup/comms/MessagePoster;Lcom/squareup/wire/Message;)Z
    .locals 2

    .line 36
    iget-object v0, p0, Lcom/squareup/comms/x2/X2HealthChecker;->ioThread:Lcom/squareup/comms/common/IoThread;

    invoke-virtual {v0}, Lcom/squareup/comms/common/IoThread;->assertIoThread()V

    .line 37
    instance-of v0, p2, Lcom/squareup/comms/protos/common/HealthCheckRequest;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 38
    new-instance p2, Lcom/squareup/comms/protos/common/HealthCheckResponse;

    invoke-direct {p2}, Lcom/squareup/comms/protos/common/HealthCheckResponse;-><init>()V

    invoke-interface {p1, p2}, Lcom/squareup/comms/MessagePoster;->post(Lcom/squareup/wire/Message;)V

    return v1

    .line 42
    :cond_0
    instance-of p1, p2, Lcom/squareup/comms/protos/common/HealthCheckResponse;

    if-eqz p1, :cond_1

    .line 43
    iget-object p1, p0, Lcom/squareup/comms/x2/X2HealthChecker;->healthState:Lcom/squareup/comms/x2/X2HealthState;

    const-string p2, "healthState"

    invoke-static {p1, p2}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 44
    iget-object p1, p0, Lcom/squareup/comms/x2/X2HealthChecker;->healthState:Lcom/squareup/comms/x2/X2HealthState;

    invoke-virtual {p1}, Lcom/squareup/comms/x2/X2HealthState;->isStopped()Z

    move-result p1

    xor-int/2addr p1, v1

    invoke-static {p1}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 46
    iget-object p1, p0, Lcom/squareup/comms/x2/X2HealthChecker;->healthState:Lcom/squareup/comms/x2/X2HealthState;

    invoke-virtual {p1}, Lcom/squareup/comms/x2/X2HealthState;->onResponseReceived()V

    return v1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method
