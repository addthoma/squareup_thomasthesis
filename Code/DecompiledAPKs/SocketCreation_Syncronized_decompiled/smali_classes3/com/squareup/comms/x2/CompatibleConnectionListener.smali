.class public interface abstract Lcom/squareup/comms/x2/CompatibleConnectionListener;
.super Ljava/lang/Object;
.source "CompatibleConnectionListener.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Remote:",
        "Lcom/squareup/wire/Message;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract onCompatibleConnectionEstablished(Lcom/squareup/comms/RemoteBusConnection;Lcom/squareup/wire/Message;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/comms/RemoteBusConnection;",
            "TRemote;)V"
        }
    .end annotation
.end method

.method public abstract onCompatibleConnectionLost()V
.end method

.method public abstract onIncompatibleConnectionEstablished()V
.end method

.method public abstract onIncompatibleConnectionLost()V
.end method
