.class public final Lcom/squareup/itemsapplet/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/itemsapplet/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final advanced_modifier_instruction:I = 0x7f0a01a7

.field public static final banner:I = 0x7f0a0217

.field public static final category_name:I = 0x7f0a02f7

.field public static final category_row_delete:I = 0x7f0a02f8

.field public static final category_row_name:I = 0x7f0a02f9

.field public static final checkbox:I = 0x7f0a0318

.field public static final delete_button:I = 0x7f0a0561

.field public static final delete_message:I = 0x7f0a0563

.field public static final detail_list_view:I = 0x7f0a0599

.field public static final detail_list_wrapper:I = 0x7f0a059a

.field public static final detail_progress:I = 0x7f0a059b

.field public static final detail_search:I = 0x7f0a059c

.field public static final detail_searchable_list:I = 0x7f0a059d

.field public static final detail_searchable_list_convert_item_button:I = 0x7f0a059e

.field public static final detail_searchable_list_create_button:I = 0x7f0a059f

.field public static final detail_searchable_list_create_button_container:I = 0x7f0a05a0

.field public static final detail_searchable_list_create_from_search_button:I = 0x7f0a05a1

.field public static final detail_searchable_list_empty_view:I = 0x7f0a05a2

.field public static final detail_searchable_list_empty_view_subtitle:I = 0x7f0a05a3

.field public static final detail_searchable_list_empty_view_title:I = 0x7f0a05a4

.field public static final detail_searchable_list_loading_progress:I = 0x7f0a05a5

.field public static final detail_searchable_list_no_results_group:I = 0x7f0a05a6

.field public static final detail_searchable_list_recycler_view:I = 0x7f0a05a7

.field public static final detail_searchable_list_search_bar:I = 0x7f0a05a8

.field public static final detail_searchable_list_unit_number_limit_help_text_under_create_button:I = 0x7f0a05a9

.field public static final detail_searchable_list_unit_number_limit_help_text_under_create_from_search_button:I = 0x7f0a05aa

.field public static final detail_searchable_list_view_add_button:I = 0x7f0a05ab

.field public static final draggable_detail_list_view:I = 0x7f0a05f0

.field public static final edit_category_label_color_grid:I = 0x7f0a0613

.field public static final edit_category_label_edit_label_color_header:I = 0x7f0a0614

.field public static final edit_category_name:I = 0x7f0a0615

.field public static final edit_category_static_top_content_tile:I = 0x7f0a0616

.field public static final edit_category_title:I = 0x7f0a0617

.field public static final edit_category_top_image_tile:I = 0x7f0a0618

.field public static final edit_category_top_text_tile:I = 0x7f0a0619

.field public static final edit_category_view:I = 0x7f0a061a

.field public static final image_tile_help_text:I = 0x7f0a0826

.field public static final modifier_card_progress:I = 0x7f0a09e4

.field public static final modifier_option_delete:I = 0x7f0a09e7

.field public static final modifier_option_drag_handle:I = 0x7f0a09e8

.field public static final modifier_option_name:I = 0x7f0a09e9

.field public static final modifier_option_price:I = 0x7f0a09ea

.field public static final modifier_set_apply_to_items:I = 0x7f0a09eb

.field public static final modifiers_label:I = 0x7f0a09ec

.field public static final name:I = 0x7f0a0a0c

.field public static final null_state_container:I = 0x7f0a0a77

.field public static final null_state_subtitle:I = 0x7f0a0a7a

.field public static final null_state_title:I = 0x7f0a0a7b

.field public static final overlay:I = 0x7f0a0b77

.field public static final recycler:I = 0x7f0a0d1f

.field public static final recycler_view:I = 0x7f0a0d21

.field public static final stable_action_bar:I = 0x7f0a0f1c

.field public static final thumbnail:I = 0x7f0a0fab


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
