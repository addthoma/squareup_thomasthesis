.class public final Lcom/squareup/itemsapplet/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/itemsapplet/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final convert_items:I = 0x7f1204d9

.field public static final create_unit_button_text:I = 0x7f1205ec

.field public static final detail_searchable_list_no_search_results:I = 0x7f120830

.field public static final discounts_hint_url:I = 0x7f12088e

.field public static final items_applet_all_items_title:I = 0x7f120e47

.field public static final items_applet_all_services_title:I = 0x7f120e48

.field public static final items_applet_categories_null_message:I = 0x7f120e49

.field public static final items_applet_categories_null_title:I = 0x7f120e4a

.field public static final items_applet_categories_title:I = 0x7f120e4b

.field public static final items_applet_category_message:I = 0x7f120e4c

.field public static final items_applet_create_category:I = 0x7f120e4d

.field public static final items_applet_detail_searchable_list_empty_view_message:I = 0x7f120e4e

.field public static final items_applet_detail_searchable_list_empty_view_title_for_units:I = 0x7f120e4f

.field public static final items_applet_discount_too_complicated_dialog_button:I = 0x7f120e50

.field public static final items_applet_discount_too_complicated_dialog_message:I = 0x7f120e51

.field public static final items_applet_discount_too_complicated_dialog_title:I = 0x7f120e52

.field public static final items_applet_discounts_null_message:I = 0x7f120e53

.field public static final items_applet_discounts_null_title:I = 0x7f120e54

.field public static final items_applet_discounts_title:I = 0x7f120e55

.field public static final items_applet_edit_category:I = 0x7f120e56

.field public static final items_applet_edit_modifier_set:I = 0x7f120e57

.field public static final items_applet_items_null_message:I = 0x7f120e58

.field public static final items_applet_items_null_title:I = 0x7f120e59

.field public static final items_applet_modifiers_null_message:I = 0x7f120e5a

.field public static final items_applet_modifiers_null_title:I = 0x7f120e5b

.field public static final items_applet_modifiers_title:I = 0x7f120e5c

.field public static final items_applet_no_categories:I = 0x7f120e5d

.field public static final items_applet_no_discounts:I = 0x7f120e5e

.field public static final items_applet_no_gift_cards:I = 0x7f120e5f

.field public static final items_applet_no_items:I = 0x7f120e60

.field public static final items_applet_no_items_in_category:I = 0x7f120e61

.field public static final items_applet_no_modifier_sets:I = 0x7f120e62

.field public static final items_applet_search_categories:I = 0x7f120e63

.field public static final items_applet_search_category:I = 0x7f120e64

.field public static final items_applet_search_discounts:I = 0x7f120e65

.field public static final items_applet_search_modifier_sets:I = 0x7f120e66

.field public static final items_applet_search_units:I = 0x7f120e67

.field public static final items_applet_services_null_message:I = 0x7f120e68

.field public static final items_applet_services_null_title:I = 0x7f120e69

.field public static final items_applet_setup_item_grid:I = 0x7f120e6a

.field public static final items_applet_units_title:I = 0x7f120e6b

.field public static final items_applet_uppercase_modifiers:I = 0x7f120e6c

.field public static final modifier_count_plural:I = 0x7f121000

.field public static final modifier_count_single:I = 0x7f121001

.field public static final modifier_count_zero:I = 0x7f121002

.field public static final modifier_new_hint:I = 0x7f121008

.field public static final modifier_required_warning_message:I = 0x7f121014

.field public static final modifier_required_warning_title:I = 0x7f121015

.field public static final modifiers_fixed_null_title:I = 0x7f12101e

.field public static final unit_detail_searchable_list_custom_label:I = 0x7f121ae9


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
