.class public final Lcom/squareup/itemsapplet/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/itemsapplet/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final all_items_or_services_detail_view:I = 0x7f0d0053

.field public static final categories_list_view:I = 0x7f0d00d6

.field public static final category_assignment_item_row:I = 0x7f0d00d7

.field public static final category_detail_view:I = 0x7f0d00d8

.field public static final category_edit_list_row:I = 0x7f0d00da

.field public static final detail_searchable_list:I = 0x7f0d01d1

.field public static final detail_searchable_list_create_button_row:I = 0x7f0d01d2

.field public static final detail_searchable_list_empty_view:I = 0x7f0d01d3

.field public static final detail_searchable_list_no_search_results_row:I = 0x7f0d01d4

.field public static final detail_searchable_list_view_content:I = 0x7f0d01d5

.field public static final detail_searchable_list_view_draggable_list:I = 0x7f0d01d6

.field public static final edit_category_label_view:I = 0x7f0d01f2

.field public static final edit_category_static_top_content:I = 0x7f0d01f3

.field public static final edit_category_static_top_content_tile:I = 0x7f0d01f4

.field public static final edit_category_view:I = 0x7f0d01f5

.field public static final edit_modifier_set_main_view:I = 0x7f0d0217

.field public static final edit_modifier_set_static_content_top:I = 0x7f0d0218

.field public static final items_applet_category_list_row:I = 0x7f0d0306

.field public static final items_applet_delete_button_row:I = 0x7f0d0307

.field public static final modifier_assignment:I = 0x7f0d035b

.field public static final modifier_option_row:I = 0x7f0d035c

.field public static final modifiers_detail_view:I = 0x7f0d035d

.field public static final unit_detail_view:I = 0x7f0d057c


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
