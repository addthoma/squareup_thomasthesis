.class abstract Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;
.super Ljava/lang/Object;
.source "OnboardingNotificationsSource.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/OnboardingNotificationsSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "NotificationType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$CompleteSignupInApp;,
        Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$CompleteSignupOnWeb;,
        Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$LinkBankAccountInApp;,
        Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$LinkBankAccountOnWeb;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00082\u0018\u00002\u00020\u0001:\u0004\u0015\u0016\u0017\u0018B3\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\r\u0082\u0001\u0004\u0019\u001a\u001b\u001c\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;",
        "",
        "id",
        "",
        "title",
        "",
        "content",
        "displayType",
        "Lcom/squareup/notificationcenterdata/Notification$DisplayType;",
        "messageType",
        "Lcom/squareup/communications/Message$Type;",
        "(Ljava/lang/String;IILcom/squareup/notificationcenterdata/Notification$DisplayType;Lcom/squareup/communications/Message$Type;)V",
        "getContent",
        "()I",
        "getDisplayType",
        "()Lcom/squareup/notificationcenterdata/Notification$DisplayType;",
        "getId",
        "()Ljava/lang/String;",
        "getMessageType",
        "()Lcom/squareup/communications/Message$Type;",
        "getTitle",
        "CompleteSignupInApp",
        "CompleteSignupOnWeb",
        "LinkBankAccountInApp",
        "LinkBankAccountOnWeb",
        "Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$CompleteSignupInApp;",
        "Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$CompleteSignupOnWeb;",
        "Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$LinkBankAccountInApp;",
        "Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$LinkBankAccountOnWeb;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final content:I

.field private final displayType:Lcom/squareup/notificationcenterdata/Notification$DisplayType;

.field private final id:Ljava/lang/String;

.field private final messageType:Lcom/squareup/communications/Message$Type;

.field private final title:I


# direct methods
.method private constructor <init>(Ljava/lang/String;IILcom/squareup/notificationcenterdata/Notification$DisplayType;Lcom/squareup/communications/Message$Type;)V
    .locals 0

    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;->id:Ljava/lang/String;

    iput p2, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;->title:I

    iput p3, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;->content:I

    iput-object p4, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;->displayType:Lcom/squareup/notificationcenterdata/Notification$DisplayType;

    iput-object p5, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;->messageType:Lcom/squareup/communications/Message$Type;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;IILcom/squareup/notificationcenterdata/Notification$DisplayType;Lcom/squareup/communications/Message$Type;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 144
    invoke-direct/range {p0 .. p5}, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;-><init>(Ljava/lang/String;IILcom/squareup/notificationcenterdata/Notification$DisplayType;Lcom/squareup/communications/Message$Type;)V

    return-void
.end method


# virtual methods
.method public final getContent()I
    .locals 1

    .line 147
    iget v0, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;->content:I

    return v0
.end method

.method public final getDisplayType()Lcom/squareup/notificationcenterdata/Notification$DisplayType;
    .locals 1

    .line 148
    iget-object v0, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;->displayType:Lcom/squareup/notificationcenterdata/Notification$DisplayType;

    return-object v0
.end method

.method public final getId()Ljava/lang/String;
    .locals 1

    .line 145
    iget-object v0, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final getMessageType()Lcom/squareup/communications/Message$Type;
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;->messageType:Lcom/squareup/communications/Message$Type;

    return-object v0
.end method

.method public final getTitle()I
    .locals 1

    .line 146
    iget v0, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;->title:I

    return v0
.end method
