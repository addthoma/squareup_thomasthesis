.class public final Lcom/squareup/onboarding/OnboardingFlowKt;
.super Ljava/lang/Object;
.source "OnboardingFlow.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\"\u0011\u0010\u0000\u001a\u00020\u0001\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\u0003\"\u0011\u0010\u0004\u001a\u00020\u0001\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0003\u00a8\u0006\u0006"
    }
    d2 = {
        "LINK_BANK_ACCOUNT",
        "Lcom/squareup/onboarding/OnboardingFlow;",
        "getLINK_BANK_ACCOUNT",
        "()Lcom/squareup/onboarding/OnboardingFlow;",
        "POS_ONBOARDING",
        "getPOS_ONBOARDING",
        "onboarding_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final LINK_BANK_ACCOUNT:Lcom/squareup/onboarding/OnboardingFlow;

.field private static final POS_ONBOARDING:Lcom/squareup/onboarding/OnboardingFlow;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 9
    new-instance v0, Lcom/squareup/onboarding/OnboardingFlow;

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    const-string v4, "pos_onboarding"

    invoke-direct {v0, v4, v3, v2, v1}, Lcom/squareup/onboarding/OnboardingFlow;-><init>(Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/onboarding/OnboardingFlowKt;->POS_ONBOARDING:Lcom/squareup/onboarding/OnboardingFlow;

    .line 14
    new-instance v0, Lcom/squareup/onboarding/OnboardingFlow;

    const-string v4, "link_bank_account"

    invoke-direct {v0, v4, v3, v2, v1}, Lcom/squareup/onboarding/OnboardingFlow;-><init>(Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/onboarding/OnboardingFlowKt;->LINK_BANK_ACCOUNT:Lcom/squareup/onboarding/OnboardingFlow;

    return-void
.end method

.method public static final getLINK_BANK_ACCOUNT()Lcom/squareup/onboarding/OnboardingFlow;
    .locals 1

    .line 14
    sget-object v0, Lcom/squareup/onboarding/OnboardingFlowKt;->LINK_BANK_ACCOUNT:Lcom/squareup/onboarding/OnboardingFlow;

    return-object v0
.end method

.method public static final getPOS_ONBOARDING()Lcom/squareup/onboarding/OnboardingFlow;
    .locals 1

    .line 9
    sget-object v0, Lcom/squareup/onboarding/OnboardingFlowKt;->POS_ONBOARDING:Lcom/squareup/onboarding/OnboardingFlow;

    return-object v0
.end method
