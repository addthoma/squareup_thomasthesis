.class final Lcom/squareup/onboarding/RealOnboardingType$variant$1;
.super Ljava/lang/Object;
.source "RealOnboardingType.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/RealOnboardingType;->variant()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/onboarding/OnboardingType$Variant;",
        "serverDrivenFeatureEnabled",
        "",
        "apply",
        "(Ljava/lang/Boolean;)Lcom/squareup/onboarding/OnboardingType$Variant;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/onboarding/RealOnboardingType;


# direct methods
.method constructor <init>(Lcom/squareup/onboarding/RealOnboardingType;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onboarding/RealOnboardingType$variant$1;->this$0:Lcom/squareup/onboarding/RealOnboardingType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Boolean;)Lcom/squareup/onboarding/OnboardingType$Variant;
    .locals 1

    const-string v0, "serverDrivenFeatureEnabled"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/onboarding/OnboardingType$Variant;->SERVER_DRIVEN:Lcom/squareup/onboarding/OnboardingType$Variant;

    goto :goto_0

    .line 28
    :cond_0
    iget-object p1, p0, Lcom/squareup/onboarding/RealOnboardingType$variant$1;->this$0:Lcom/squareup/onboarding/RealOnboardingType;

    invoke-static {p1}, Lcom/squareup/onboarding/RealOnboardingType;->access$getSettings$p(Lcom/squareup/onboarding/RealOnboardingType;)Lcom/squareup/settings/server/AccountStatusSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getOnboardingSettings()Lcom/squareup/settings/server/OnboardingSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/settings/server/OnboardingSettings;->shouldActivateOnTheWeb()Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/onboarding/OnboardingType$Variant;->WEB:Lcom/squareup/onboarding/OnboardingType$Variant;

    goto :goto_0

    .line 29
    :cond_1
    sget-object p1, Lcom/squareup/onboarding/OnboardingType$Variant;->LEGACY_NATIVE:Lcom/squareup/onboarding/OnboardingType$Variant;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 13
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/RealOnboardingType$variant$1;->apply(Ljava/lang/Boolean;)Lcom/squareup/onboarding/OnboardingType$Variant;

    move-result-object p1

    return-object p1
.end method
