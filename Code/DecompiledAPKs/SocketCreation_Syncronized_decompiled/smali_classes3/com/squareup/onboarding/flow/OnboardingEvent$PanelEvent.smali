.class public abstract Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;
.super Lcom/squareup/onboarding/flow/OnboardingEvent;
.source "OnboardingEvent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/flow/OnboardingEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "PanelEvent"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Back;,
        Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Exit;,
        Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Finish;,
        Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Skip;,
        Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Timeout;,
        Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Submit;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0006\u000c\r\u000e\u000f\u0010\u0011B\u001f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u001a\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000b\u0082\u0001\u0006\u0012\u0013\u0014\u0015\u0016\u0017\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;",
        "Lcom/squareup/onboarding/flow/OnboardingEvent;",
        "actionName",
        "",
        "outputs",
        "",
        "Lcom/squareup/protos/client/onboard/Output;",
        "(Ljava/lang/String;Ljava/util/List;)V",
        "getActionName",
        "()Ljava/lang/String;",
        "getOutputs",
        "()Ljava/util/List;",
        "Back",
        "Exit",
        "Finish",
        "Skip",
        "Submit",
        "Timeout",
        "Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Back;",
        "Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Exit;",
        "Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Finish;",
        "Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Skip;",
        "Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Timeout;",
        "Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Submit;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionName:Ljava/lang/String;

.field private final outputs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/onboard/Output;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/onboard/Output;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 22
    invoke-direct {p0, v0}, Lcom/squareup/onboarding/flow/OnboardingEvent;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;->actionName:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;->outputs:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    .line 21
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p2

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;-><init>(Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/util/List;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1, p2}, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;-><init>(Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public getActionName()Ljava/lang/String;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;->actionName:Ljava/lang/String;

    return-object v0
.end method

.method public getOutputs()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/onboard/Output;",
            ">;"
        }
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;->outputs:Ljava/util/List;

    return-object v0
.end method
