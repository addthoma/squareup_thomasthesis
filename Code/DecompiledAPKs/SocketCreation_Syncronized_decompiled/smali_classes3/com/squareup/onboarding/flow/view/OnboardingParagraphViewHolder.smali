.class public final Lcom/squareup/onboarding/flow/view/OnboardingParagraphViewHolder;
.super Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;
.source "OnboardingParagraphViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder<",
        "Lcom/squareup/onboarding/flow/data/OnboardingParagraphItem;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingParagraphViewHolder.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingParagraphViewHolder.kt\ncom/squareup/onboarding/flow/view/OnboardingParagraphViewHolder\n*L\n1#1,35:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0002H\u0014R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/view/OnboardingParagraphViewHolder;",
        "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;",
        "Lcom/squareup/onboarding/flow/data/OnboardingParagraphItem;",
        "inputHandler",
        "Lcom/squareup/onboarding/flow/OnboardingInputHandler;",
        "parent",
        "Landroid/view/ViewGroup;",
        "(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V",
        "header",
        "Lcom/squareup/marketfont/MarketTextView;",
        "htmlField",
        "Lcom/squareup/widgets/MessageView;",
        "onBindComponent",
        "",
        "component",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final header:Lcom/squareup/marketfont/MarketTextView;

.field private final htmlField:Lcom/squareup/widgets/MessageView;


# direct methods
.method public constructor <init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V
    .locals 1

    const-string v0, "inputHandler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    sget v0, Lcom/squareup/onboarding/flow/R$layout;->onboarding_component_paragraph:I

    .line 16
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;-><init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;I)V

    .line 19
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingParagraphViewHolder;->itemView:Landroid/view/View;

    const-string p2, "itemView"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v0, Lcom/squareup/onboarding/flow/R$id;->paragraph_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingParagraphViewHolder;->header:Lcom/squareup/marketfont/MarketTextView;

    .line 20
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingParagraphViewHolder;->itemView:Landroid/view/View;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget p2, Lcom/squareup/onboarding/flow/R$id;->paragraph_text:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingParagraphViewHolder;->htmlField:Lcom/squareup/widgets/MessageView;

    return-void
.end method


# virtual methods
.method public bridge synthetic onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;)V
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/onboarding/flow/data/OnboardingParagraphItem;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingParagraphViewHolder;->onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingParagraphItem;)V

    return-void
.end method

.method protected onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingParagraphItem;)V
    .locals 4

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingParagraphViewHolder;->header:Lcom/squareup/marketfont/MarketTextView;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingParagraphItem;->label()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setTextAndVisibility(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 25
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingParagraphViewHolder;->htmlField:Lcom/squareup/widgets/MessageView;

    .line 26
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingParagraphItem;->style()Lcom/squareup/onboarding/flow/data/ParagraphStyle;

    move-result-object v1

    .line 27
    invoke-virtual {v0}, Lcom/squareup/widgets/MessageView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/onboarding/flow/data/ParagraphStyle;->getPaddingRes()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 29
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingParagraphItem;->html()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 30
    invoke-virtual {v1}, Lcom/squareup/onboarding/flow/data/ParagraphStyle;->getBackgroundRes()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setBackgroundResource(I)V

    .line 31
    invoke-virtual {v0, v2, v2, v2, v2}, Lcom/squareup/widgets/MessageView;->setPadding(IIII)V

    return-void
.end method
