.class public abstract Lcom/squareup/onboarding/flow/OnboardingState;
.super Ljava/lang/Object;
.source "OnboardingState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onboarding/flow/OnboardingState$Waiting;,
        Lcom/squareup/onboarding/flow/OnboardingState$LoadingFirst;,
        Lcom/squareup/onboarding/flow/OnboardingState$LoadingNext;,
        Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;,
        Lcom/squareup/onboarding/flow/OnboardingState$ShowingError;,
        Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0006\u0007\u0008\t\n\u000b\u000cB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0006\r\u000e\u000f\u0010\u0011\u0012\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/OnboardingState;",
        "",
        "()V",
        "history",
        "Lcom/squareup/onboarding/flow/PanelHistory;",
        "getHistory",
        "()Lcom/squareup/onboarding/flow/PanelHistory;",
        "LoadingFirst",
        "LoadingNext",
        "ShowingError",
        "ShowingExitDialog",
        "ShowingPanel",
        "Waiting",
        "Lcom/squareup/onboarding/flow/OnboardingState$Waiting;",
        "Lcom/squareup/onboarding/flow/OnboardingState$LoadingFirst;",
        "Lcom/squareup/onboarding/flow/OnboardingState$LoadingNext;",
        "Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;",
        "Lcom/squareup/onboarding/flow/OnboardingState$ShowingError;",
        "Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 61
    invoke-direct {p0}, Lcom/squareup/onboarding/flow/OnboardingState;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getHistory()Lcom/squareup/onboarding/flow/PanelHistory;
.end method
