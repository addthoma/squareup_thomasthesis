.class final Lcom/squareup/onboarding/flow/OnboardingReactor$reactionFromState$1;
.super Ljava/lang/Object;
.source "OnboardingReactor.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/flow/OnboardingReactor;->reactionFromState(Lcom/squareup/server/StatusResponse;Lcom/squareup/onboarding/flow/OnboardingState;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001\"\u0012\u0008\u0000\u0010\u0004*\u000c\u0012\u0004\u0012\u0002H\u0004\u0012\u0002\u0008\u00030\u00052\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u0002H\u00040\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "Lcom/squareup/onboarding/flow/OnboardingState;",
        "Lcom/squareup/onboarding/OnboardingWorkflowResult;",
        "T",
        "Lcom/squareup/wire/Message;",
        "successOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $fallbackSessionToken:Ljava/lang/String;

.field final synthetic $payloadParser:Lkotlin/jvm/functions/Function1;

.field final synthetic $state:Lcom/squareup/onboarding/flow/OnboardingState;

.field final synthetic this$0:Lcom/squareup/onboarding/flow/OnboardingReactor;


# direct methods
.method constructor <init>(Lcom/squareup/onboarding/flow/OnboardingReactor;Lkotlin/jvm/functions/Function1;Lcom/squareup/onboarding/flow/OnboardingState;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingReactor$reactionFromState$1;->this$0:Lcom/squareup/onboarding/flow/OnboardingReactor;

    iput-object p2, p0, Lcom/squareup/onboarding/flow/OnboardingReactor$reactionFromState$1;->$payloadParser:Lkotlin/jvm/functions/Function1;

    iput-object p3, p0, Lcom/squareup/onboarding/flow/OnboardingReactor$reactionFromState$1;->$state:Lcom/squareup/onboarding/flow/OnboardingState;

    iput-object p4, p0, Lcom/squareup/onboarding/flow/OnboardingReactor$reactionFromState$1;->$fallbackSessionToken:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "+TT;>;)",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/onboarding/flow/OnboardingState;",
            "Lcom/squareup/onboarding/OnboardingWorkflowResult;",
            ">;"
        }
    .end annotation

    const-string v0, "successOrFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 216
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingReactor$reactionFromState$1;->this$0:Lcom/squareup/onboarding/flow/OnboardingReactor;

    iget-object v1, p0, Lcom/squareup/onboarding/flow/OnboardingReactor$reactionFromState$1;->$payloadParser:Lkotlin/jvm/functions/Function1;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    invoke-interface {v1, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/onboarding/flow/OnboardingReactor$ResponsePayload;

    iget-object v1, p0, Lcom/squareup/onboarding/flow/OnboardingReactor$reactionFromState$1;->$state:Lcom/squareup/onboarding/flow/OnboardingState;

    invoke-virtual {v1}, Lcom/squareup/onboarding/flow/OnboardingState;->getHistory()Lcom/squareup/onboarding/flow/PanelHistory;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/squareup/onboarding/flow/OnboardingReactor;->access$screenDataFromSuccess(Lcom/squareup/onboarding/flow/OnboardingReactor;Lcom/squareup/onboarding/flow/OnboardingReactor$ResponsePayload;Lcom/squareup/onboarding/flow/PanelHistory;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p1

    goto :goto_0

    .line 219
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_1

    .line 220
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingReactor$reactionFromState$1;->this$0:Lcom/squareup/onboarding/flow/OnboardingReactor;

    .line 221
    iget-object v1, p0, Lcom/squareup/onboarding/flow/OnboardingReactor$reactionFromState$1;->$fallbackSessionToken:Ljava/lang/String;

    .line 222
    invoke-static {v0}, Lcom/squareup/onboarding/flow/OnboardingReactor;->access$getMessages$p(Lcom/squareup/onboarding/flow/OnboardingReactor;)Lcom/squareup/receiving/FailureMessageFactory;

    move-result-object v2

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    sget v3, Lcom/squareup/onboarding/flow/R$string;->onboarding_panel_error_title:I

    new-instance v4, Lcom/squareup/onboarding/flow/OnboardingReactor$reactionFromState$1$1;

    invoke-direct {v4, p0}, Lcom/squareup/onboarding/flow/OnboardingReactor$reactionFromState$1$1;-><init>(Lcom/squareup/onboarding/flow/OnboardingReactor$reactionFromState$1;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v2, p1, v3, v4}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 225
    iget-object v2, p0, Lcom/squareup/onboarding/flow/OnboardingReactor$reactionFromState$1;->$state:Lcom/squareup/onboarding/flow/OnboardingState;

    invoke-virtual {v2}, Lcom/squareup/onboarding/flow/OnboardingState;->getHistory()Lcom/squareup/onboarding/flow/PanelHistory;

    move-result-object v2

    .line 220
    invoke-static {v0, v1, p1, v2}, Lcom/squareup/onboarding/flow/OnboardingReactor;->access$screenDataFromError(Lcom/squareup/onboarding/flow/OnboardingReactor;Ljava/lang/String;Lcom/squareup/receiving/FailureMessage;Lcom/squareup/onboarding/flow/PanelHistory;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 71
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/OnboardingReactor$reactionFromState$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p1

    return-object p1
.end method
