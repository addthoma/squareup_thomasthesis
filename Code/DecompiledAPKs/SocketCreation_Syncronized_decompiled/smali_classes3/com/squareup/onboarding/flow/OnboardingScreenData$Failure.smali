.class public final Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;
.super Lcom/squareup/onboarding/flow/OnboardingScreenData;
.source "OnboardingScreenData.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/flow/OnboardingScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Failure"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0005H\u00c6\u0003J\'\u0010\u0010\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u00d6\u0003J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001J\t\u0010\u0017\u001a\u00020\u0005H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\tR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;",
        "Lcom/squareup/onboarding/flow/OnboardingScreenData;",
        "panel",
        "Lcom/squareup/protos/client/onboard/Panel;",
        "errorTitle",
        "",
        "errorMessage",
        "(Lcom/squareup/protos/client/onboard/Panel;Ljava/lang/String;Ljava/lang/String;)V",
        "getErrorMessage",
        "()Ljava/lang/String;",
        "getErrorTitle",
        "getPanel",
        "()Lcom/squareup/protos/client/onboard/Panel;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final errorMessage:Ljava/lang/String;

.field private final errorTitle:Ljava/lang/String;

.field private final panel:Lcom/squareup/protos/client/onboard/Panel;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/onboard/Panel;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "panel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorTitle"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorMessage"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 33
    invoke-direct {p0, v0}, Lcom/squareup/onboarding/flow/OnboardingScreenData;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;->panel:Lcom/squareup/protos/client/onboard/Panel;

    iput-object p2, p0, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;->errorTitle:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;->errorMessage:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;Lcom/squareup/protos/client/onboard/Panel;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;->getPanel()Lcom/squareup/protos/client/onboard/Panel;

    move-result-object p1

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;->errorTitle:Ljava/lang/String;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;->errorMessage:Ljava/lang/String;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;->copy(Lcom/squareup/protos/client/onboard/Panel;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/protos/client/onboard/Panel;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;->getPanel()Lcom/squareup/protos/client/onboard/Panel;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;->errorTitle:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;->errorMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Lcom/squareup/protos/client/onboard/Panel;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;
    .locals 1

    const-string v0, "panel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorTitle"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorMessage"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;-><init>(Lcom/squareup/protos/client/onboard/Panel;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;

    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;->getPanel()Lcom/squareup/protos/client/onboard/Panel;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;->getPanel()Lcom/squareup/protos/client/onboard/Panel;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;->errorTitle:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;->errorTitle:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;->errorMessage:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;->errorMessage:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getErrorMessage()Ljava/lang/String;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;->errorMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final getErrorTitle()Ljava/lang/String;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;->errorTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getPanel()Lcom/squareup/protos/client/onboard/Panel;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;->panel:Lcom/squareup/protos/client/onboard/Panel;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;->getPanel()Lcom/squareup/protos/client/onboard/Panel;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;->errorTitle:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;->errorMessage:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Failure(panel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;->getPanel()Lcom/squareup/protos/client/onboard/Panel;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", errorTitle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;->errorTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", errorMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;->errorMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
