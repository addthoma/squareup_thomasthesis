.class final Lcom/squareup/onboarding/flow/OnboardingPanelView$onAttachedToWindow$1;
.super Lkotlin/jvm/internal/Lambda;
.source "OnboardingPanelView.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/flow/OnboardingPanelView;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lio/reactivex/disposables/Disposable;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/disposables/Disposable;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/onboarding/flow/OnboardingPanelView;


# direct methods
.method constructor <init>(Lcom/squareup/onboarding/flow/OnboardingPanelView;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView$onAttachedToWindow$1;->this$0:Lcom/squareup/onboarding/flow/OnboardingPanelView;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 156
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView$onAttachedToWindow$1;->this$0:Lcom/squareup/onboarding/flow/OnboardingPanelView;

    invoke-static {v0}, Lcom/squareup/onboarding/flow/OnboardingPanelView;->access$getPanelState$p(Lcom/squareup/onboarding/flow/OnboardingPanelView;)Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    new-instance v1, Lcom/squareup/onboarding/flow/OnboardingPanelView$onAttachedToWindow$1$1;

    invoke-direct {v1, p0}, Lcom/squareup/onboarding/flow/OnboardingPanelView$onAttachedToWindow$1$1;-><init>(Lcom/squareup/onboarding/flow/OnboardingPanelView$onAttachedToWindow$1;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "panelState.subscribe { s\u2026enData.panel.name\n      }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 64
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/OnboardingPanelView$onAttachedToWindow$1;->invoke()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method
