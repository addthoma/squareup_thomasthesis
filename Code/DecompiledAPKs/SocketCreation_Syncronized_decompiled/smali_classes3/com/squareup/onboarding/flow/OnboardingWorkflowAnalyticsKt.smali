.class public final Lcom/squareup/onboarding/flow/OnboardingWorkflowAnalyticsKt;
.super Ljava/lang/Object;
.source "OnboardingWorkflowAnalytics.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0010\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0002\u00a8\u0006\u0004"
    }
    d2 = {
        "fromResult",
        "",
        "result",
        "Lcom/squareup/onboarding/OnboardingWorkflowResult;",
        "onboarding_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$fromResult(Lcom/squareup/onboarding/OnboardingWorkflowResult;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/onboarding/flow/OnboardingWorkflowAnalyticsKt;->fromResult(Lcom/squareup/onboarding/OnboardingWorkflowResult;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static final fromResult(Lcom/squareup/onboarding/OnboardingWorkflowResult;)Ljava/lang/String;
    .locals 1

    .line 36
    sget-object v0, Lcom/squareup/onboarding/OnboardingWorkflowResult$Finished;->INSTANCE:Lcom/squareup/onboarding/OnboardingWorkflowResult$Finished;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, "Completed"

    goto :goto_0

    .line 37
    :cond_0
    sget-object v0, Lcom/squareup/onboarding/OnboardingWorkflowResult$Errored;->INSTANCE:Lcom/squareup/onboarding/OnboardingWorkflowResult$Errored;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string p0, "Error"

    goto :goto_0

    .line 38
    :cond_1
    sget-object v0, Lcom/squareup/onboarding/OnboardingWorkflowResult$Cancelled;->INSTANCE:Lcom/squareup/onboarding/OnboardingWorkflowResult$Cancelled;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string p0, "Cancelled"

    goto :goto_0

    .line 39
    :cond_2
    sget-object v0, Lcom/squareup/onboarding/OnboardingWorkflowResult$Unsupported;->INSTANCE:Lcom/squareup/onboarding/OnboardingWorkflowResult$Unsupported;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    const-string p0, "Unsupported Client"

    :goto_0
    return-object p0

    :cond_3
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method
