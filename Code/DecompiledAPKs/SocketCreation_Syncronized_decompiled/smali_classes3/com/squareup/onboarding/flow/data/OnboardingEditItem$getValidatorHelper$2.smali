.class final Lcom/squareup/onboarding/flow/data/OnboardingEditItem$getValidatorHelper$2;
.super Lkotlin/jvm/internal/Lambda;
.source "OnboardingEditItem.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/flow/data/OnboardingEditItem;->getValidatorHelper(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;)Lcom/squareup/onboarding/flow/data/OnboardingValidator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/text/Regex;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lkotlin/text/Regex;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $outputs:Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;


# direct methods
.method constructor <init>(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onboarding/flow/data/OnboardingEditItem$getValidatorHelper$2;->$outputs:Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 13
    check-cast p1, Lkotlin/text/Regex;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/data/OnboardingEditItem$getValidatorHelper$2;->invoke(Lkotlin/text/Regex;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lkotlin/text/Regex;)Z
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/squareup/onboarding/flow/data/OnboardingEditItem$getValidatorHelper$2;->$outputs:Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;

    const-string v1, "text"

    invoke-virtual {v0, v1}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;->get(Ljava/lang/String;)Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;->matchesRegex(Lkotlin/text/Regex;)Z

    move-result p1

    return p1
.end method
