.class final Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "OnboardingMultiSelectListViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "MultiSelectAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$RowHolder;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingMultiSelectListViewHolder.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingMultiSelectListViewHolder.kt\ncom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter\n+ 2 Views.kt\ncom/squareup/util/Views\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,129:1\n1103#2,7:130\n1360#3:137\n1429#3,3:138\n1360#3:141\n1429#3,3:142\n704#3:145\n777#3,2:146\n*E\n*S KotlinDebug\n*F\n+ 1 OnboardingMultiSelectListViewHolder.kt\ncom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter\n*L\n73#1,7:130\n90#1:137\n90#1,3:138\n61#1:141\n61#1,3:142\n62#1:145\n62#1,2:146\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010#\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u001e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0008\u0010\u0011\u001a\u00020\u000cH\u0016J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u000cH\u0016J\u0008\u0010\u0015\u001a\u00020\u0016H\u0002J\u0018\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u0018\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u000cH\u0016J\u0018\u0010\u0019\u001a\u00020\u00022\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u000cH\u0016J\u0010\u0010\u001d\u001a\u00020\u00162\u0006\u0010\u0018\u001a\u00020\u0002H\u0002J\u000e\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020\n0\u001fH\u0002J\u0018\u0010 \u001a\u00020\u00162\u0006\u0010\u0014\u001a\u00020\u000c2\u0006\u0010!\u001a\u00020\"H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\u000f\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n0\u00100\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;",
        "Landroidx/recyclerview/widget/RecyclerView$Adapter;",
        "Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$RowHolder;",
        "component",
        "Lcom/squareup/onboarding/flow/data/OnboardingMultiSelectListItem;",
        "inputHandler",
        "Lcom/squareup/onboarding/flow/OnboardingInputHandler;",
        "(Lcom/squareup/onboarding/flow/data/OnboardingMultiSelectListItem;Lcom/squareup/onboarding/flow/OnboardingInputHandler;)V",
        "keys",
        "",
        "",
        "maxSelectable",
        "",
        "selectedRows",
        "",
        "values",
        "Lkotlin/Pair;",
        "getItemCount",
        "getItemId",
        "",
        "position",
        "notifySelectedKeysChanged",
        "",
        "onBindViewHolder",
        "holder",
        "onCreateViewHolder",
        "parent",
        "Landroid/view/ViewGroup;",
        "viewType",
        "onRowTapped",
        "selectedKeys",
        "",
        "setRowSelected",
        "selected",
        "",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final component:Lcom/squareup/onboarding/flow/data/OnboardingMultiSelectListItem;

.field private final inputHandler:Lcom/squareup/onboarding/flow/OnboardingInputHandler;

.field private final keys:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final maxSelectable:I

.field private final selectedRows:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final values:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lkotlin/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/onboarding/flow/data/OnboardingMultiSelectListItem;Lcom/squareup/onboarding/flow/OnboardingInputHandler;)V
    .locals 3

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "inputHandler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->component:Lcom/squareup/onboarding/flow/data/OnboardingMultiSelectListItem;

    iput-object p2, p0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->inputHandler:Lcom/squareup/onboarding/flow/OnboardingInputHandler;

    .line 51
    new-instance p1, Ljava/util/LinkedHashSet;

    invoke-direct {p1}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast p1, Ljava/util/Set;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->selectedRows:Ljava/util/Set;

    .line 52
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->component:Lcom/squareup/onboarding/flow/data/OnboardingMultiSelectListItem;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingMultiSelectListItem;->keys()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->keys:Ljava/util/List;

    .line 53
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->component:Lcom/squareup/onboarding/flow/data/OnboardingMultiSelectListItem;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingMultiSelectListItem;->values()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->values:Ljava/util/List;

    .line 54
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->component:Lcom/squareup/onboarding/flow/data/OnboardingMultiSelectListItem;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingMultiSelectListItem;->maxSelectable()I

    move-result p1

    iput p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->maxSelectable:I

    const/4 p1, 0x1

    .line 57
    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->setHasStableIds(Z)V

    .line 60
    iget-object p2, p0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->component:Lcom/squareup/onboarding/flow/data/OnboardingMultiSelectListItem;

    invoke-virtual {p2}, Lcom/squareup/onboarding/flow/data/OnboardingMultiSelectListItem;->keysSelectedByDefault()Ljava/util/List;

    move-result-object p2

    check-cast p2, Ljava/lang/Iterable;

    .line 141
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p2, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 142
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 143
    check-cast v1, Ljava/lang/String;

    .line 61
    iget-object v2, p0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->keys:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 144
    :cond_0
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 145
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    check-cast p2, Ljava/util/Collection;

    .line 146
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    if-ltz v2, :cond_2

    const/4 v2, 0x1

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    if-eqz v2, :cond_1

    .line 62
    invoke-interface {p2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 147
    :cond_3
    check-cast p2, Ljava/util/List;

    check-cast p2, Ljava/lang/Iterable;

    .line 63
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->selectedRows:Ljava/util/Set;

    check-cast p1, Ljava/util/Collection;

    invoke-static {p2, p1}, Lkotlin/collections/CollectionsKt;->toCollection(Ljava/lang/Iterable;Ljava/util/Collection;)Ljava/util/Collection;

    .line 65
    invoke-direct {p0}, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->notifySelectedKeysChanged()V

    return-void
.end method

.method public static final synthetic access$onRowTapped(Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$RowHolder;)V
    .locals 0

    .line 47
    invoke-direct {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->onRowTapped(Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$RowHolder;)V

    return-void
.end method

.method private final notifySelectedKeysChanged()V
    .locals 3

    .line 119
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->inputHandler:Lcom/squareup/onboarding/flow/OnboardingInputHandler;

    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->component:Lcom/squareup/onboarding/flow/data/OnboardingMultiSelectListItem;

    invoke-direct {p0}, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->selectedKeys()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/onboarding/flow/data/OnboardingMultiSelectListItem;->keysOutput(Ljava/util/Collection;)Lcom/squareup/protos/client/onboard/Output;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/onboarding/flow/OnboardingInputHandler;->onOutput(Lcom/squareup/protos/client/onboard/Output;)V

    return-void
.end method

.method private final onRowTapped(Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$RowHolder;)V
    .locals 2

    .line 93
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->selectedRows:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$RowHolder;->getAdapterPosition()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$RowHolder;->getAdapterPosition()I

    move-result p1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->setRowSelected(IZ)V

    goto :goto_0

    .line 99
    :cond_0
    iget v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->maxSelectable:I

    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->selectedRows:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 100
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$RowHolder;->getAdapterPosition()I

    move-result p1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->setRowSelected(IZ)V

    :cond_1
    :goto_0
    return-void
.end method

.method private final selectedKeys()Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 90
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->selectedRows:Ljava/util/Set;

    check-cast v0, Ljava/lang/Iterable;

    .line 137
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 138
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 139
    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    .line 90
    iget-object v3, p0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->keys:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 140
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    return-object v1
.end method

.method private final setRowSelected(IZ)V
    .locals 1

    if-eqz p2, :cond_0

    .line 110
    iget-object p2, p0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->selectedRows:Ljava/util/Set;

    check-cast p2, Ljava/util/Collection;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 112
    :cond_0
    iget-object p2, p0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->selectedRows:Ljava/util/Set;

    check-cast p2, Ljava/util/Collection;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 114
    :goto_0
    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->notifyItemChanged(I)V

    .line 115
    invoke-direct {p0}, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->notifySelectedKeysChanged()V

    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 2

    .line 88
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->keys:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->values:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public getItemId(I)J
    .locals 2

    .line 86
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->keys:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p1

    int-to-long v0, p1

    return-wide v0
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 47
    check-cast p1, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$RowHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->onBindViewHolder(Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$RowHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$RowHolder;I)V
    .locals 2

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$RowHolder;->getTextView()Lcom/squareup/noho/NohoCheckableRow;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->values:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/Pair;

    invoke-virtual {v1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 82
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$RowHolder;->getTextView()Lcom/squareup/noho/NohoCheckableRow;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->values:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/Pair;

    invoke-virtual {v1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableRow;->setDescription(Ljava/lang/CharSequence;)V

    .line 83
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$RowHolder;->getTextView()Lcom/squareup/noho/NohoCheckableRow;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->selectedRows:Ljava/util/Set;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 47
    invoke-virtual {p0, p1, p2}, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$RowHolder;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$RowHolder;
    .locals 1

    const-string p2, "parent"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    new-instance p2, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$RowHolder;

    sget v0, Lcom/squareup/onboarding/flow/R$layout;->onboarding_component_multi_list_item:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$RowHolder;-><init>(Landroid/view/View;)V

    .line 73
    invoke-virtual {p2}, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$RowHolder;->getTextView()Lcom/squareup/noho/NohoCheckableRow;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    .line 130
    new-instance v0, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter$onCreateViewHolder$$inlined$onClickDebounced$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter$onCreateViewHolder$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$MultiSelectAdapter;Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder$RowHolder;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p2
.end method
