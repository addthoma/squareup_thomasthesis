.class public abstract Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;
.super Ljava/lang/Object;
.source "OnboardingComponentItem.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;,
        Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$Statement;,
        Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingComponentItem.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingComponentItem.kt\ncom/squareup/onboarding/flow/data/OnboardingComponentItem\n*L\n1#1,131:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000j\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0010\u001e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0006\u0008&\u0018\u00002\u00020\u0001:\u0003123B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u000c2\u0006\u0010\u0019\u001a\u00020\u0017H\u0004J%\u0010\u001a\u001a\n \u001c*\u0004\u0018\u00010\u001b0\u001b2\u0006\u0010\u001d\u001a\u00020\u000c2\u0006\u0010\u001e\u001a\u00020\u0017H\u0000\u00a2\u0006\u0002\u0008\u001fJ%\u0010\u001a\u001a\n \u001c*\u0004\u0018\u00010\u001b0\u001b2\u0006\u0010\u001d\u001a\u00020\u000c2\u0006\u0010\u001e\u001a\u00020 H\u0000\u00a2\u0006\u0002\u0008\u001fJ%\u0010\u001a\u001a\n \u001c*\u0004\u0018\u00010\u001b0\u001b2\u0006\u0010\u001d\u001a\u00020\u000c2\u0006\u0010\u001e\u001a\u00020\u000cH\u0000\u00a2\u0006\u0002\u0008\u001fJ+\u0010\u001a\u001a\n \u001c*\u0004\u0018\u00010\u001b0\u001b2\u0006\u0010\u001d\u001a\u00020\u000c2\u000c\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020\u000c0!H\u0000\u00a2\u0006\u0002\u0008\u001fJ\u0010\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020%H\u0014J\u0018\u0010&\u001a\u00020 2\u0006\u0010\u0018\u001a\u00020\u000c2\u0006\u0010\u0019\u001a\u00020 H\u0004J1\u0010\'\u001a\n \u001c*\u0004\u0018\u00010\u001b0\u001b2\u0006\u0010\u001d\u001a\u00020\u000c2\u0017\u0010(\u001a\u0013\u0012\u0004\u0012\u00020*\u0012\u0004\u0012\u00020+0)\u00a2\u0006\u0002\u0008,H\u0002J&\u0010-\u001a\u0008\u0012\u0004\u0012\u00020\u000c0.2\u0006\u0010\u0018\u001a\u00020\u000c2\u000e\u0008\u0002\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u000c0.H\u0004J\u001a\u0010/\u001a\u00020\u000c2\u0006\u0010\u0018\u001a\u00020\u000c2\u0008\u0008\u0002\u0010\u0019\u001a\u00020\u000cH\u0004J\u0014\u00100\u001a\u00020#2\u000c\u0010$\u001a\u0008\u0012\u0004\u0012\u00020\u001b0.R\u0014\u0010\u0002\u001a\u00020\u0003X\u0084\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0014\u0010\u0004\u001a\u00020\u0005X\u0084\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u001b\u0010\u000b\u001a\u00020\u000c8FX\u0086\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u000f\u0010\u0010\u001a\u0004\u0008\r\u0010\u000eR\u001d\u0010\u0011\u001a\u0004\u0018\u00010\u00128FX\u0086\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0015\u0010\u0010\u001a\u0004\u0008\u0013\u0010\u0014\u00a8\u00064"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;",
        "",
        "component",
        "Lcom/squareup/protos/client/onboard/Component;",
        "componentData",
        "Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;",
        "(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;)V",
        "getComponent",
        "()Lcom/squareup/protos/client/onboard/Component;",
        "getComponentData",
        "()Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;",
        "name",
        "",
        "getName",
        "()Ljava/lang/String;",
        "name$delegate",
        "Lkotlin/properties/ReadOnlyProperty;",
        "type",
        "Lcom/squareup/protos/client/onboard/ComponentType;",
        "getType",
        "()Lcom/squareup/protos/client/onboard/ComponentType;",
        "type$delegate",
        "booleanProperty",
        "",
        "key",
        "defaultValue",
        "createOutput",
        "Lcom/squareup/protos/client/onboard/Output;",
        "kotlin.jvm.PlatformType",
        "outputName",
        "value",
        "createOutput$onboarding_release",
        "",
        "",
        "getValidatorHelper",
        "Lcom/squareup/onboarding/flow/data/OnboardingValidator;",
        "outputs",
        "Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;",
        "intProperty",
        "output",
        "block",
        "Lkotlin/Function1;",
        "Lcom/squareup/protos/client/onboard/Output$Builder;",
        "",
        "Lkotlin/ExtensionFunctionType;",
        "stringListProperty",
        "",
        "stringProperty",
        "validator",
        "OutputMap",
        "OutputStatement",
        "Statement",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final component:Lcom/squareup/protos/client/onboard/Component;

.field private final componentData:Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;

.field private final name$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final type$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-class v0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;

    const/4 v1, 0x2

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lkotlin/jvm/internal/PropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, "name"

    const-string v5, "getName()Ljava/lang/String;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/PropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const-string/jumbo v3, "type"

    const-string v4, "getType()Lcom/squareup/protos/client/onboard/ComponentType;"

    invoke-direct {v2, v0, v3, v4}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aput-object v0, v1, v2

    sput-object v1, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;)V
    .locals 1

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "componentData"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->component:Lcom/squareup/protos/client/onboard/Component;

    iput-object p2, p0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->componentData:Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;

    .line 21
    new-instance p1, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$name$2;

    iget-object p2, p0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->component:Lcom/squareup/protos/client/onboard/Component;

    invoke-direct {p1, p2}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$name$2;-><init>(Lcom/squareup/protos/client/onboard/Component;)V

    invoke-static {p1}, Lcom/squareup/util/DelegatesKt;->alias(Lkotlin/reflect/KProperty0;)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->name$delegate:Lkotlin/properties/ReadOnlyProperty;

    .line 24
    new-instance p1, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$type$2;

    iget-object p2, p0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->component:Lcom/squareup/protos/client/onboard/Component;

    invoke-direct {p1, p2}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$type$2;-><init>(Lcom/squareup/protos/client/onboard/Component;)V

    invoke-static {p1}, Lcom/squareup/util/DelegatesKt;->alias(Lkotlin/reflect/KProperty0;)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->type$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    .line 17
    sget-object p2, Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;->Companion:Lcom/squareup/onboarding/flow/PanelMemory$ForComponent$Companion;

    invoke-virtual {p2}, Lcom/squareup/onboarding/flow/PanelMemory$ForComponent$Companion;->getEMPTY()Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;

    move-result-object p2

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;-><init>(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;)V

    return-void
.end method

.method private final output(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/protos/client/onboard/Output;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/protos/client/onboard/Output$Builder;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/protos/client/onboard/Output;"
        }
    .end annotation

    .line 49
    new-instance v0, Lcom/squareup/protos/client/onboard/Output$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/onboard/Output$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/onboard/Output$Builder;->component_name(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/onboard/Output$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output$Builder;

    move-result-object p1

    invoke-interface {p2, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/squareup/protos/client/onboard/Output$Builder;->build()Lcom/squareup/protos/client/onboard/Output;

    move-result-object p1

    return-object p1
.end method

.method public static synthetic stringListProperty$default(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;Ljava/lang/String;Ljava/util/List;ILjava/lang/Object;)Ljava/util/List;
    .locals 0

    if-nez p4, :cond_1

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    .line 53
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p2

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->stringListProperty(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0

    .line 0
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: stringListProperty"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static synthetic stringProperty$default(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;
    .locals 0

    if-nez p4, :cond_1

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const-string p2, ""

    .line 58
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->stringProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 0
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: stringProperty"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method protected final booleanProperty(Ljava/lang/String;Z)Z
    .locals 1

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->component:Lcom/squareup/protos/client/onboard/Component;

    iget-object v0, v0, Lcom/squareup/protos/client/onboard/Component;->properties:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/onboard/PropertyMapEntry;

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/client/onboard/PropertyMapEntry;->boolean_value:Ljava/lang/Boolean;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    :cond_0
    return p2
.end method

.method public final createOutput$onboarding_release(Ljava/lang/String;I)Lcom/squareup/protos/client/onboard/Output;
    .locals 1

    const-string v0, "outputName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    new-instance v0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$createOutput$3;

    invoke-direct {v0, p2}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$createOutput$3;-><init>(I)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, p1, v0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->output(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/protos/client/onboard/Output;

    move-result-object p1

    return-object p1
.end method

.method public final createOutput$onboarding_release(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;
    .locals 1

    const-string v0, "outputName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "value"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    new-instance v0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$createOutput$2;

    invoke-direct {v0, p2}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$createOutput$2;-><init>(Ljava/lang/String;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, p1, v0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->output(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/protos/client/onboard/Output;

    move-result-object p1

    return-object p1
.end method

.method public final createOutput$onboarding_release(Ljava/lang/String;Ljava/util/Collection;)Lcom/squareup/protos/client/onboard/Output;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/onboard/Output;"
        }
    .end annotation

    const-string v0, "outputName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "value"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    new-instance v0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$createOutput$1;

    invoke-direct {v0, p2}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$createOutput$1;-><init>(Ljava/util/Collection;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, p1, v0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->output(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/protos/client/onboard/Output;

    move-result-object p1

    return-object p1
.end method

.method public final createOutput$onboarding_release(Ljava/lang/String;Z)Lcom/squareup/protos/client/onboard/Output;
    .locals 1

    const-string v0, "outputName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    new-instance v0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$createOutput$4;

    invoke-direct {v0, p2}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$createOutput$4;-><init>(Z)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, p1, v0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->output(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/protos/client/onboard/Output;

    move-result-object p1

    return-object p1
.end method

.method protected final getComponent()Lcom/squareup/protos/client/onboard/Component;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->component:Lcom/squareup/protos/client/onboard/Component;

    return-object v0
.end method

.method protected final getComponentData()Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->componentData:Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->name$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getType()Lcom/squareup/protos/client/onboard/ComponentType;
    .locals 3

    iget-object v0, p0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->type$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/onboard/ComponentType;

    return-object v0
.end method

.method protected getValidatorHelper(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;)Lcom/squareup/onboarding/flow/data/OnboardingValidator;
    .locals 8

    const-string v0, "outputs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    new-instance p1, Lcom/squareup/onboarding/flow/data/OnboardingValidator;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xf

    const/4 v7, 0x0

    move-object v1, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/onboarding/flow/data/OnboardingValidator;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p1
.end method

.method protected final intProperty(Ljava/lang/String;I)I
    .locals 1

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->component:Lcom/squareup/protos/client/onboard/Component;

    iget-object v0, v0, Lcom/squareup/protos/client/onboard/Component;->properties:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/onboard/PropertyMapEntry;

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/client/onboard/PropertyMapEntry;->integer_value:Ljava/lang/Integer;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p2

    :cond_0
    return p2
.end method

.method protected final stringListProperty(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultValue"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->component:Lcom/squareup/protos/client/onboard/Component;

    iget-object v0, v0, Lcom/squareup/protos/client/onboard/Component;->properties:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/onboard/PropertyMapEntry;

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/client/onboard/PropertyMapEntry;->string_value:Ljava/lang/String;

    if-eqz p1, :cond_0

    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    const-string p1, "\t"

    filled-new-array {p1}, [Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lkotlin/text/StringsKt;->split$default(Ljava/lang/CharSequence;[Ljava/lang/String;ZIILjava/lang/Object;)Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    move-object p1, p2

    :goto_0
    return-object p1
.end method

.method protected final stringProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultValue"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->component:Lcom/squareup/protos/client/onboard/Component;

    iget-object v0, v0, Lcom/squareup/protos/client/onboard/Component;->properties:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/onboard/PropertyMapEntry;

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/client/onboard/PropertyMapEntry;->string_value:Ljava/lang/String;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    move-object p1, p2

    :goto_0
    return-object p1
.end method

.method public final validator(Ljava/util/List;)Lcom/squareup/onboarding/flow/data/OnboardingValidator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/onboard/Output;",
            ">;)",
            "Lcom/squareup/onboarding/flow/data/OnboardingValidator;"
        }
    .end annotation

    const-string v0, "outputs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    new-instance v0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;

    invoke-direct {v0, p1}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;-><init>(Ljava/util/List;)V

    invoke-virtual {p0, v0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->getValidatorHelper(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;)Lcom/squareup/onboarding/flow/data/OnboardingValidator;

    move-result-object p1

    return-object p1
.end method
