.class final Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor$onEnterScope$6;
.super Lkotlin/jvm/internal/Lambda;
.source "OnboardingWorkflowMonitor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
        "+",
        "Lcom/squareup/server/activation/ActivationUrl;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012*\u0010\u0002\u001a&\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004 \u0005*\u0012\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/server/activation/ActivationUrl;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;


# direct methods
.method constructor <init>(Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor$onEnterScope$6;->this$0:Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 38
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor$onEnterScope$6;->invoke(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "+",
            "Lcom/squareup/server/activation/ActivationUrl;",
            ">;)V"
        }
    .end annotation

    .line 84
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor$onEnterScope$6;->this$0:Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;

    invoke-static {v0}, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->access$getBrowserLauncher$p(Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;)Lcom/squareup/util/BrowserLauncher;

    move-result-object v0

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/server/activation/ActivationUrl;

    iget-object p1, p1, Lcom/squareup/server/activation/ActivationUrl;->activation_url:Ljava/lang/String;

    invoke-interface {v0, p1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    .line 87
    :cond_0
    iget-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor$onEnterScope$6;->this$0:Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;

    invoke-static {p1}, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->access$getContainer$p(Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;)Lcom/squareup/ui/main/PosContainer;

    move-result-object p1

    sget-object v0, Lcom/squareup/onboarding/OnboardingWorkflowRunner;->Companion:Lcom/squareup/onboarding/OnboardingWorkflowRunner$Companion;

    invoke-virtual {v0}, Lcom/squareup/onboarding/OnboardingWorkflowRunner$Companion;->getNAME()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/ui/main/PosContainer;->goBackPastWorkflow(Ljava/lang/String;)V

    return-void
.end method
