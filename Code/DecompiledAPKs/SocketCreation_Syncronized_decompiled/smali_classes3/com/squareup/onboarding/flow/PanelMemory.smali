.class public final Lcom/squareup/onboarding/flow/PanelMemory;
.super Ljava/lang/Object;
.source "PanelMemory.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;,
        Lcom/squareup/onboarding/flow/PanelMemory$ReadWriteForComponent;,
        Lcom/squareup/onboarding/flow/PanelMemory$UnmodifiableForComponent;,
        Lcom/squareup/onboarding/flow/PanelMemory$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPanelMemory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PanelMemory.kt\ncom/squareup/onboarding/flow/PanelMemory\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Maps.kt\nkotlin/collections/MapsKt__MapsKt\n*L\n1#1,163:1\n1265#2,12:164\n1642#2,2:176\n1642#2,2:185\n347#3,7:178\n*E\n*S KotlinDebug\n*F\n+ 1 PanelMemory.kt\ncom/squareup/onboarding/flow/PanelMemory\n*L\n40#1,12:164\n49#1,2:176\n28#1,2:185\n54#1,7:178\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u0000 \u00192\u00020\u0001:\u0004\u0019\u001a\u001b\u001cB\u0015\u0012\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u0006\u0010\n\u001a\u00020\u000bJ\u0008\u0010\u000c\u001a\u00020\rH\u0016J\u0011\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0008H\u0086\u0002J\u0010\u0010\u0011\u001a\u00020\t2\u0006\u0010\u0010\u001a\u00020\u0008H\u0002J\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0012J\u0011\u0010\u0013\u001a\u00020\u000b2\u0006\u0010\u0014\u001a\u00020\u0004H\u0086\u0002J\u0018\u0010\u0015\u001a\u00020\u000b2\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\rH\u0016R\u001a\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\t0\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/PanelMemory;",
        "Landroid/os/Parcelable;",
        "outputs",
        "",
        "Lcom/squareup/protos/client/onboard/Output;",
        "(Ljava/util/Collection;)V",
        "components",
        "",
        "",
        "Lcom/squareup/onboarding/flow/PanelMemory$ReadWriteForComponent;",
        "clearOutputs",
        "",
        "describeContents",
        "",
        "get",
        "Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;",
        "componentName",
        "getOrCreate",
        "",
        "plusAssign",
        "output",
        "writeToParcel",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "Companion",
        "ForComponent",
        "ReadWriteForComponent",
        "UnmodifiableForComponent",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/onboarding/flow/PanelMemory;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/onboarding/flow/PanelMemory$Companion;


# instance fields
.field private final components:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/onboarding/flow/PanelMemory$ReadWriteForComponent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/onboarding/flow/PanelMemory$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/onboarding/flow/PanelMemory$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/onboarding/flow/PanelMemory;->Companion:Lcom/squareup/onboarding/flow/PanelMemory$Companion;

    .line 119
    new-instance v0, Lcom/squareup/onboarding/flow/PanelMemory$Companion$CREATOR$1;

    invoke-direct {v0}, Lcom/squareup/onboarding/flow/PanelMemory$Companion$CREATOR$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/onboarding/flow/PanelMemory;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, v0}, Lcom/squareup/onboarding/flow/PanelMemory;-><init>(Ljava/util/Collection;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/protos/client/onboard/Output;",
            ">;)V"
        }
    .end annotation

    const-string v0, "outputs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcom/squareup/onboarding/flow/PanelMemory;->components:Ljava/util/Map;

    .line 28
    check-cast p1, Ljava/lang/Iterable;

    .line 185
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/onboard/Output;

    .line 28
    invoke-virtual {p0, v0}, Lcom/squareup/onboarding/flow/PanelMemory;->plusAssign(Lcom/squareup/protos/client/onboard/Output;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/Collection;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 21
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/onboarding/flow/PanelMemory;-><init>(Ljava/util/Collection;)V

    return-void
.end method

.method private final getOrCreate(Ljava/lang/String;)Lcom/squareup/onboarding/flow/PanelMemory$ReadWriteForComponent;
    .locals 2

    .line 54
    iget-object v0, p0, Lcom/squareup/onboarding/flow/PanelMemory;->components:Ljava/util/Map;

    .line 178
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 180
    new-instance v1, Lcom/squareup/onboarding/flow/PanelMemory$ReadWriteForComponent;

    .line 54
    invoke-direct {v1}, Lcom/squareup/onboarding/flow/PanelMemory$ReadWriteForComponent;-><init>()V

    .line 181
    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    :cond_0
    check-cast v1, Lcom/squareup/onboarding/flow/PanelMemory$ReadWriteForComponent;

    return-object v1
.end method


# virtual methods
.method public final clearOutputs()V
    .locals 2

    .line 49
    iget-object v0, p0, Lcom/squareup/onboarding/flow/PanelMemory;->components:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 176
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/onboarding/flow/PanelMemory$ReadWriteForComponent;

    .line 49
    invoke-virtual {v1}, Lcom/squareup/onboarding/flow/PanelMemory$ReadWriteForComponent;->clearAllOutputs()V

    goto :goto_0

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/squareup/onboarding/flow/PanelMemory;->components:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final get(Ljava/lang/String;)Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;
    .locals 1

    const-string v0, "componentName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0, p1}, Lcom/squareup/onboarding/flow/PanelMemory;->getOrCreate(Ljava/lang/String;)Lcom/squareup/onboarding/flow/PanelMemory$ReadWriteForComponent;

    move-result-object p1

    check-cast p1, Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;

    return-object p1
.end method

.method public final outputs()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/onboard/Output;",
            ">;"
        }
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/squareup/onboarding/flow/PanelMemory;->components:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 164
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 171
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 172
    check-cast v2, Lcom/squareup/onboarding/flow/PanelMemory$ReadWriteForComponent;

    .line 40
    invoke-virtual {v2}, Lcom/squareup/onboarding/flow/PanelMemory$ReadWriteForComponent;->outputs()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 173
    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0

    .line 175
    :cond_0
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public final plusAssign(Lcom/squareup/protos/client/onboard/Output;)V
    .locals 2

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    iget-object v0, p1, Lcom/squareup/protos/client/onboard/Output;->component_name:Ljava/lang/String;

    const-string v1, "output.component_name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/squareup/onboarding/flow/PanelMemory;->getOrCreate(Ljava/lang/String;)Lcom/squareup/onboarding/flow/PanelMemory$ReadWriteForComponent;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/onboarding/flow/PanelMemory$ReadWriteForComponent;->addOutput(Lcom/squareup/protos/client/onboard/Output;)V

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/PanelMemory;->outputs()Ljava/util/List;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/squareup/util/Parcels;->writeProtoMessages(Landroid/os/Parcel;Ljava/util/List;)V

    return-void
.end method
