.class public final Lcom/squareup/onboarding/flow/data/OnboardingValidatorTypes;
.super Ljava/lang/Object;
.source "OnboardingValidatorTypes.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/data/OnboardingValidatorTypes;",
        "",
        "()V",
        "PRESENT",
        "",
        "REGEX",
        "VALID",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/onboarding/flow/data/OnboardingValidatorTypes;

.field public static final PRESENT:Ljava/lang/String; = "present"

.field public static final REGEX:Ljava/lang/String; = "regex"

.field public static final VALID:Ljava/lang/String; = "valid"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 12
    new-instance v0, Lcom/squareup/onboarding/flow/data/OnboardingValidatorTypes;

    invoke-direct {v0}, Lcom/squareup/onboarding/flow/data/OnboardingValidatorTypes;-><init>()V

    sput-object v0, Lcom/squareup/onboarding/flow/data/OnboardingValidatorTypes;->INSTANCE:Lcom/squareup/onboarding/flow/data/OnboardingValidatorTypes;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
