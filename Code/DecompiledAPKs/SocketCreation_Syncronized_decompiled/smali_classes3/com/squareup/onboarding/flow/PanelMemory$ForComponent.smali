.class public interface abstract Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;
.super Ljava/lang/Object;
.source "PanelMemory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/flow/PanelMemory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ForComponent"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onboarding/flow/PanelMemory$ForComponent$DefaultImpls;,
        Lcom/squareup/onboarding/flow/PanelMemory$ForComponent$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0008\u0003\u0008f\u0018\u0000 \u000f2\u00020\u0001:\u0001\u000fJ\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u00a6\u0002J\u0018\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0008\u001a\u00020\u0007H\u0016J\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0008\u001a\u00020\nH\u0016J\u001a\u0010\u000b\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u0005H\u0016J&\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u00050\r2\u0006\u0010\u0004\u001a\u00020\u00052\u000e\u0008\u0002\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00050\rH\u0016J\u000e\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00030\rH&\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;",
        "",
        "get",
        "Lcom/squareup/protos/client/onboard/Output;",
        "outputName",
        "",
        "getBoolean",
        "",
        "defaultValue",
        "getInt",
        "",
        "getString",
        "getStringList",
        "",
        "outputs",
        "Companion",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/onboarding/flow/PanelMemory$ForComponent$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/onboarding/flow/PanelMemory$ForComponent$Companion;->$$INSTANCE:Lcom/squareup/onboarding/flow/PanelMemory$ForComponent$Companion;

    sput-object v0, Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;->Companion:Lcom/squareup/onboarding/flow/PanelMemory$ForComponent$Companion;

    return-void
.end method


# virtual methods
.method public abstract get(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;
.end method

.method public abstract getBoolean(Ljava/lang/String;Z)Z
.end method

.method public abstract getInt(Ljava/lang/String;I)I
.end method

.method public abstract getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getStringList(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract outputs()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/onboard/Output;",
            ">;"
        }
    .end annotation
.end method
