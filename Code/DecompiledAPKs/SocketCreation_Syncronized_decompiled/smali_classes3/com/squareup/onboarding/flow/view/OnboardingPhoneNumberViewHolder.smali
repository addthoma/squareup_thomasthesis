.class public final Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;
.super Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;
.source "OnboardingPhoneNumberViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder<",
        "Lcom/squareup/onboarding/flow/data/OnboardingPhoneNumberItem;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingPhoneNumberViewHolder.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingPhoneNumberViewHolder.kt\ncom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,67:1\n66#2:68\n*E\n*S KotlinDebug\n*F\n+ 1 OnboardingPhoneNumberViewHolder.kt\ncom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder\n*L\n38#1:68\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u0002H\u0014J\u0008\u0010\u001f\u001a\u00020\u001dH\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u000c\u001a\u00020\r8\u0000@\u0000X\u0081.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\"\u0004\u0008\u0010\u0010\u0011R$\u0010\u0012\u001a\u00020\u00138\u0000@\u0000X\u0081.\u00a2\u0006\u0014\n\u0000\u0012\u0004\u0008\u0014\u0010\u0015\u001a\u0004\u0008\u0016\u0010\u0017\"\u0004\u0008\u0018\u0010\u0019R\u0010\u0010\u001a\u001a\u0004\u0018\u00010\u001bX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;",
        "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;",
        "Lcom/squareup/onboarding/flow/data/OnboardingPhoneNumberItem;",
        "inputHandler",
        "Lcom/squareup/onboarding/flow/OnboardingInputHandler;",
        "parent",
        "Landroid/view/ViewGroup;",
        "(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V",
        "field",
        "Lcom/squareup/noho/NohoEditText;",
        "header",
        "Lcom/squareup/marketfont/MarketTextView;",
        "phoneHelper",
        "Lcom/squareup/text/PhoneNumberHelper;",
        "getPhoneHelper$onboarding_release",
        "()Lcom/squareup/text/PhoneNumberHelper;",
        "setPhoneHelper$onboarding_release",
        "(Lcom/squareup/text/PhoneNumberHelper;)V",
        "phoneScrubber",
        "Lcom/squareup/text/InsertingScrubber;",
        "phoneScrubber$annotations",
        "()V",
        "getPhoneScrubber$onboarding_release",
        "()Lcom/squareup/text/InsertingScrubber;",
        "setPhoneScrubber$onboarding_release",
        "(Lcom/squareup/text/InsertingScrubber;)V",
        "valueWatcher",
        "Landroid/text/TextWatcher;",
        "onBindComponent",
        "",
        "component",
        "onHighlightError",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final field:Lcom/squareup/noho/NohoEditText;

.field private final header:Lcom/squareup/marketfont/MarketTextView;

.field public phoneHelper:Lcom/squareup/text/PhoneNumberHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public phoneScrubber:Lcom/squareup/text/InsertingScrubber;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private valueWatcher:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V
    .locals 2

    const-string v0, "inputHandler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    sget v0, Lcom/squareup/onboarding/flow/R$layout;->onboarding_component_text_field:I

    .line 26
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;-><init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;I)V

    .line 29
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;->itemView:Landroid/view/View;

    const-string p2, "itemView"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v0, Lcom/squareup/onboarding/flow/R$id;->onboarding_text_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;->header:Lcom/squareup/marketfont/MarketTextView;

    .line 30
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;->itemView:Landroid/view/View;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget p2, Lcom/squareup/onboarding/flow/R$id;->onboarding_text_field:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoEditText;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;->field:Lcom/squareup/noho/NohoEditText;

    .line 38
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 68
    const-class p2, Lcom/squareup/onboarding/flow/OnboardingWorkflowParentComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->componentInParent(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/onboarding/flow/OnboardingWorkflowParentComponent;

    .line 39
    invoke-interface {p1, p0}, Lcom/squareup/onboarding/flow/OnboardingWorkflowParentComponent;->inject(Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;)V

    .line 41
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;->field:Lcom/squareup/noho/NohoEditText;

    const/4 p2, 0x3

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoEditText;->setInputType(I)V

    .line 42
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;->field:Lcom/squareup/noho/NohoEditText;

    new-instance p2, Lcom/squareup/text/ScrubbingTextWatcher;

    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;->phoneScrubber:Lcom/squareup/text/InsertingScrubber;

    if-nez v0, :cond_0

    const-string v1, "phoneScrubber"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;->field:Lcom/squareup/noho/NohoEditText;

    check-cast v1, Lcom/squareup/text/HasSelectableText;

    invoke-direct {p2, v0, v1}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V

    check-cast p2, Landroid/text/TextWatcher;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public static synthetic phoneScrubber$annotations()V
    .locals 0
    .annotation runtime Lcom/squareup/text/InsertingScrubber$PhoneNumber;
    .end annotation

    return-void
.end method


# virtual methods
.method public final getPhoneHelper$onboarding_release()Lcom/squareup/text/PhoneNumberHelper;
    .locals 2

    .line 35
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;->phoneHelper:Lcom/squareup/text/PhoneNumberHelper;

    if-nez v0, :cond_0

    const-string v1, "phoneHelper"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getPhoneScrubber$onboarding_release()Lcom/squareup/text/InsertingScrubber;
    .locals 2

    .line 34
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;->phoneScrubber:Lcom/squareup/text/InsertingScrubber;

    if-nez v0, :cond_0

    const-string v1, "phoneScrubber"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public bridge synthetic onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;)V
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/onboarding/flow/data/OnboardingPhoneNumberItem;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;->onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingPhoneNumberItem;)V

    return-void
.end method

.method protected onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingPhoneNumberItem;)V
    .locals 2

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;->header:Lcom/squareup/marketfont/MarketTextView;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingPhoneNumberItem;->label()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setTextAndVisibility(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;->field:Lcom/squareup/noho/NohoEditText;

    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;->valueWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 49
    new-instance v0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder$onBindComponent$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder$onBindComponent$1;-><init>(Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;Lcom/squareup/onboarding/flow/data/OnboardingPhoneNumberItem;)V

    check-cast v0, Landroid/text/TextWatcher;

    iput-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;->valueWatcher:Landroid/text/TextWatcher;

    .line 56
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;->field:Lcom/squareup/noho/NohoEditText;

    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;->valueWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;->field:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingPhoneNumberItem;->hint()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;->field:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingPhoneNumberItem;->defaultPhoneNumber()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onHighlightError()V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;->field:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditText;->requestFocus()Z

    .line 64
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;->field:Lcom/squareup/noho/NohoEditText;

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {p0, v0}, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;->setSelectionEnd(Landroid/widget/EditText;)V

    return-void
.end method

.method public final setPhoneHelper$onboarding_release(Lcom/squareup/text/PhoneNumberHelper;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;->phoneHelper:Lcom/squareup/text/PhoneNumberHelper;

    return-void
.end method

.method public final setPhoneScrubber$onboarding_release(Lcom/squareup/text/InsertingScrubber;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;->phoneScrubber:Lcom/squareup/text/InsertingScrubber;

    return-void
.end method
