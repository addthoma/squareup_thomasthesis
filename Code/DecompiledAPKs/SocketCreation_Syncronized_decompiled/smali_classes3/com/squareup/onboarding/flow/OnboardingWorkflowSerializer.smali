.class public final Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;
.super Ljava/lang/Object;
.source "OnboardingWorkflowSerializer.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingWorkflowSerializer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingWorkflowSerializer.kt\ncom/squareup/onboarding/flow/OnboardingWorkflowSerializer\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n+ 3 BuffersProtos.kt\ncom/squareup/workflow/BuffersProtos\n+ 4 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,108:1\n180#2:109\n165#2:110\n158#2,3:114\n161#2:119\n56#3:111\n56#3:112\n56#3:113\n56#3:120\n1591#4,2:117\n*E\n*S KotlinDebug\n*F\n+ 1 OnboardingWorkflowSerializer.kt\ncom/squareup/onboarding/flow/OnboardingWorkflowSerializer\n*L\n29#1:109\n31#1:110\n49#1,3:114\n49#1:119\n31#1:111\n31#1:112\n31#1:113\n94#1:120\n49#1,2:117\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0008J\u000e\u0010\t\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u0004J\u000c\u0010\u000b\u001a\u00020\u000c*\u00020\u0008H\u0002J\u000c\u0010\r\u001a\u00020\u000e*\u00020\u0008H\u0002J\u000c\u0010\u000f\u001a\u00020\u0010*\u00020\u0008H\u0002J\u0014\u0010\u0011\u001a\u00020\u0012*\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u000cH\u0002J\u0014\u0010\u0015\u001a\u00020\u0012*\u00020\u00132\u0006\u0010\n\u001a\u00020\u000eH\u0002J\u0014\u0010\u0016\u001a\u00020\u0012*\u00020\u00132\u0006\u0010\u0017\u001a\u00020\u0010H\u0002J\u0014\u0010\u0018\u001a\u00020\u0013*\u00020\u00132\u0006\u0010\n\u001a\u00020\u0004H\u0002\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;",
        "",
        "()V",
        "readState",
        "Lcom/squareup/onboarding/flow/OnboardingState;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "source",
        "Lokio/BufferedSource;",
        "snapshotState",
        "state",
        "readComponentError",
        "Lcom/squareup/onboarding/flow/PanelError;",
        "readNestedShowingPanel",
        "Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;",
        "readPanelScreen",
        "Lcom/squareup/onboarding/flow/PanelScreen;",
        "writeComponentError",
        "",
        "Lokio/BufferedSink;",
        "error",
        "writeNestedShowingPanel",
        "writePanelScreen",
        "screen",
        "writeState",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;

    invoke-direct {v0}, Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;-><init>()V

    sput-object v0, Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic access$writeState(Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;Lokio/BufferedSink;Lcom/squareup/onboarding/flow/OnboardingState;)Lokio/BufferedSink;
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2}, Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;->writeState(Lokio/BufferedSink;Lcom/squareup/onboarding/flow/OnboardingState;)Lokio/BufferedSink;

    move-result-object p0

    return-object p0
.end method

.method private final readComponentError(Lokio/BufferedSource;)Lcom/squareup/onboarding/flow/PanelError;
    .locals 2

    .line 103
    new-instance v0, Lcom/squareup/onboarding/flow/PanelError;

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/onboarding/flow/PanelError;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private final readNestedShowingPanel(Lokio/BufferedSource;)Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;
    .locals 1

    .line 84
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readByteStringWithLength(Lokio/BufferedSource;)Lokio/ByteString;

    move-result-object p1

    .line 85
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p1}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    .line 86
    check-cast v0, Lokio/BufferedSource;

    invoke-virtual {p0, v0}, Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;->readState(Lokio/BufferedSource;)Lcom/squareup/onboarding/flow/OnboardingState;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;

    return-object p1

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.onboarding.flow.OnboardingState.ShowingPanel"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final readPanelScreen(Lokio/BufferedSource;)Lcom/squareup/onboarding/flow/PanelScreen;
    .locals 2

    .line 120
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v1, Lcom/squareup/protos/client/onboard/Panel;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/onboard/Panel;

    .line 94
    new-instance v0, Lcom/squareup/onboarding/flow/PanelScreen;

    invoke-direct {v0, p1}, Lcom/squareup/onboarding/flow/PanelScreen;-><init>(Lcom/squareup/protos/client/onboard/Panel;)V

    return-object v0
.end method

.method private final writeComponentError(Lokio/BufferedSink;Lcom/squareup/onboarding/flow/PanelError;)V
    .locals 1

    .line 98
    invoke-virtual {p2}, Lcom/squareup/onboarding/flow/PanelError;->getComponentName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 99
    invoke-virtual {p2}, Lcom/squareup/onboarding/flow/PanelError;->getErrorMessage()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    return-void
.end method

.method private final writeNestedShowingPanel(Lokio/BufferedSink;Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;)V
    .locals 3

    .line 78
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    sget-object v1, Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;

    move-object v2, v0

    check-cast v2, Lokio/BufferedSink;

    check-cast p2, Lcom/squareup/onboarding/flow/OnboardingState;

    invoke-direct {v1, v2, p2}, Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;->writeState(Lokio/BufferedSink;Lcom/squareup/onboarding/flow/OnboardingState;)Lokio/BufferedSink;

    .line 79
    invoke-virtual {v0}, Lokio/Buffer;->readByteString()Lokio/ByteString;

    move-result-object p2

    .line 80
    invoke-static {p1, p2}, Lcom/squareup/workflow/SnapshotKt;->writeByteStringWithLength(Lokio/BufferedSink;Lokio/ByteString;)Lokio/BufferedSink;

    return-void
.end method

.method private final writePanelScreen(Lokio/BufferedSink;Lcom/squareup/onboarding/flow/PanelScreen;)V
    .locals 0

    .line 90
    invoke-virtual {p2}, Lcom/squareup/onboarding/flow/PanelScreen;->getPanel()Lcom/squareup/protos/client/onboard/Panel;

    move-result-object p2

    check-cast p2, Lcom/squareup/wire/Message;

    invoke-static {p1, p2}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    return-void
.end method

.method private final writeState(Lokio/BufferedSink;Lcom/squareup/onboarding/flow/OnboardingState;)Lokio/BufferedSink;
    .locals 3

    .line 50
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "state::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 52
    invoke-virtual {p2}, Lcom/squareup/onboarding/flow/OnboardingState;->getHistory()Lcom/squareup/onboarding/flow/PanelHistory;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    .line 115
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {p1, v1}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 116
    check-cast v0, Ljava/lang/Iterable;

    .line 117
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 116
    check-cast v1, Lcom/squareup/onboarding/flow/PanelScreen;

    .line 52
    sget-object v2, Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;

    invoke-direct {v2, p1, v1}, Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;->writePanelScreen(Lokio/BufferedSink;Lcom/squareup/onboarding/flow/PanelScreen;)V

    goto :goto_0

    .line 55
    :cond_0
    instance-of v0, p2, Lcom/squareup/onboarding/flow/OnboardingState$LoadingFirst;

    if-eqz v0, :cond_1

    .line 56
    check-cast p2, Lcom/squareup/onboarding/flow/OnboardingState$LoadingFirst;

    invoke-virtual {p2}, Lcom/squareup/onboarding/flow/OnboardingState$LoadingFirst;->getRequest()Lcom/squareup/protos/client/onboard/StartSessionRequest;

    move-result-object p2

    check-cast p2, Lcom/squareup/wire/Message;

    invoke-static {p1, p2}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    goto :goto_1

    .line 58
    :cond_1
    instance-of v0, p2, Lcom/squareup/onboarding/flow/OnboardingState$LoadingNext;

    if-eqz v0, :cond_2

    .line 59
    check-cast p2, Lcom/squareup/onboarding/flow/OnboardingState$LoadingNext;

    invoke-virtual {p2}, Lcom/squareup/onboarding/flow/OnboardingState$LoadingNext;->getRequest()Lcom/squareup/protos/client/onboard/NextRequest;

    move-result-object p2

    check-cast p2, Lcom/squareup/wire/Message;

    invoke-static {p1, p2}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    goto :goto_1

    .line 61
    :cond_2
    instance-of v0, p2, Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;

    if-eqz v0, :cond_3

    .line 62
    check-cast p2, Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;

    invoke-virtual {p2}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;->getSessionToken()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 63
    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;

    invoke-virtual {p2}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;->getError()Lcom/squareup/onboarding/flow/PanelError;

    move-result-object p2

    invoke-direct {v0, p1, p2}, Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;->writeComponentError(Lokio/BufferedSink;Lcom/squareup/onboarding/flow/PanelError;)V

    goto :goto_1

    .line 65
    :cond_3
    instance-of v0, p2, Lcom/squareup/onboarding/flow/OnboardingState$ShowingError;

    if-eqz v0, :cond_4

    .line 66
    check-cast p2, Lcom/squareup/onboarding/flow/OnboardingState$ShowingError;

    invoke-virtual {p2}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingError;->getSessionToken()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 67
    invoke-virtual {p2}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingError;->getErrorTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 68
    invoke-virtual {p2}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingError;->getErrorMessage()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    goto :goto_1

    .line 70
    :cond_4
    instance-of v0, p2, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;

    if-eqz v0, :cond_5

    .line 71
    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;

    check-cast p2, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;

    invoke-virtual {p2}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;->getContext()Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;->writeNestedShowingPanel(Lokio/BufferedSink;Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;)V

    .line 72
    invoke-virtual {p2}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;->getExitDialog()Lcom/squareup/protos/client/onboard/Dialog;

    move-result-object p2

    check-cast p2, Lcom/squareup/wire/Message;

    invoke-static {p1, p2}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    :cond_5
    :goto_1
    return-object p1
.end method


# virtual methods
.method public final readState(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/onboarding/flow/OnboardingState;
    .locals 1

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-virtual {p1}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    .line 109
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p1}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p1

    check-cast p1, Lokio/BufferedSource;

    .line 29
    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;

    invoke-virtual {v0, p1}, Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;->readState(Lokio/BufferedSource;)Lcom/squareup/onboarding/flow/OnboardingState;

    move-result-object p1

    return-object p1
.end method

.method public final readState(Lokio/BufferedSource;)Lcom/squareup/onboarding/flow/OnboardingState;
    .locals 5

    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v1, "Class.forName(readUtf8WithLength())"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/jvm/JvmClassMappingKt;->getKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    .line 110
    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_0

    .line 35
    sget-object v4, Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;

    invoke-direct {v4, p1}, Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;->readPanelScreen(Lokio/BufferedSource;)Lcom/squareup/onboarding/flow/PanelScreen;

    move-result-object v4

    .line 110
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    check-cast v2, Ljava/util/List;

    .line 35
    new-instance v1, Lcom/squareup/onboarding/flow/PanelHistory;

    invoke-direct {v1, v2}, Lcom/squareup/onboarding/flow/PanelHistory;-><init>(Ljava/util/List;)V

    .line 38
    const-class v2, Lcom/squareup/onboarding/flow/OnboardingState$LoadingFirst;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 111
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v2, Lcom/squareup/protos/client/onboard/StartSessionRequest;

    invoke-virtual {v0, v2}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/onboard/StartSessionRequest;

    .line 38
    new-instance v0, Lcom/squareup/onboarding/flow/OnboardingState$LoadingFirst;

    invoke-direct {v0, v1, p1}, Lcom/squareup/onboarding/flow/OnboardingState$LoadingFirst;-><init>(Lcom/squareup/onboarding/flow/PanelHistory;Lcom/squareup/protos/client/onboard/StartSessionRequest;)V

    check-cast v0, Lcom/squareup/onboarding/flow/OnboardingState;

    goto/16 :goto_1

    .line 39
    :cond_1
    const-class v2, Lcom/squareup/onboarding/flow/OnboardingState$LoadingNext;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 112
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v2, Lcom/squareup/protos/client/onboard/NextRequest;

    invoke-virtual {v0, v2}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/onboard/NextRequest;

    .line 39
    new-instance v0, Lcom/squareup/onboarding/flow/OnboardingState$LoadingNext;

    invoke-direct {v0, v1, p1}, Lcom/squareup/onboarding/flow/OnboardingState$LoadingNext;-><init>(Lcom/squareup/onboarding/flow/PanelHistory;Lcom/squareup/protos/client/onboard/NextRequest;)V

    check-cast v0, Lcom/squareup/onboarding/flow/OnboardingState;

    goto :goto_1

    .line 40
    :cond_2
    const-class v2, Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    new-instance v0, Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;

    invoke-direct {v3, p1}, Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;->readComponentError(Lokio/BufferedSource;)Lcom/squareup/onboarding/flow/PanelError;

    move-result-object p1

    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;-><init>(Lcom/squareup/onboarding/flow/PanelHistory;Ljava/lang/String;Lcom/squareup/onboarding/flow/PanelError;)V

    check-cast v0, Lcom/squareup/onboarding/flow/OnboardingState;

    goto :goto_1

    .line 41
    :cond_3
    const-class v2, Lcom/squareup/onboarding/flow/OnboardingState$ShowingError;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    new-instance v0, Lcom/squareup/onboarding/flow/OnboardingState$ShowingError;

    .line 42
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object p1

    .line 41
    invoke-direct {v0, v1, v2, v3, p1}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingError;-><init>(Lcom/squareup/onboarding/flow/PanelHistory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/onboarding/flow/OnboardingState;

    goto :goto_1

    .line 44
    :cond_4
    const-class v1, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;

    invoke-direct {v0, p1}, Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;->readNestedShowingPanel(Lokio/BufferedSource;)Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;

    move-result-object v0

    .line 113
    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v2, Lcom/squareup/protos/client/onboard/Dialog;

    invoke-virtual {v1, v2}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/onboard/Dialog;

    .line 44
    new-instance v1, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;

    invoke-direct {v1, v0, p1}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;-><init>(Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;Lcom/squareup/protos/client/onboard/Dialog;)V

    move-object v0, v1

    check-cast v0, Lcom/squareup/onboarding/flow/OnboardingState;

    :goto_1
    return-object v0

    .line 45
    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid OnboardingState class: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final snapshotState(Lcom/squareup/onboarding/flow/OnboardingState;)Lcom/squareup/workflow/Snapshot;
    .locals 2

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer$snapshotState$1;

    invoke-direct {v1, p1}, Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer$snapshotState$1;-><init>(Lcom/squareup/onboarding/flow/OnboardingState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
