.class public final Lcom/squareup/onboarding/flow/OnboardingReactor_Factory;
.super Ljava/lang/Object;
.source "OnboardingReactor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/onboarding/flow/OnboardingReactor;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final messagesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final onboardingServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/onboard/OnboardingService;",
            ">;"
        }
    .end annotation
.end field

.field private final panelVerifierProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/flow/PanelVerifier;",
            ">;"
        }
    .end annotation
.end field

.field private final secureScopeManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/secure/SecureScopeManager;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final uniqueProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Unique;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/onboard/OnboardingService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/flow/PanelVerifier;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Unique;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/secure/SecureScopeManager;",
            ">;)V"
        }
    .end annotation

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingReactor_Factory;->onboardingServiceProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p2, p0, Lcom/squareup/onboarding/flow/OnboardingReactor_Factory;->messagesProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p3, p0, Lcom/squareup/onboarding/flow/OnboardingReactor_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p4, p0, Lcom/squareup/onboarding/flow/OnboardingReactor_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p5, p0, Lcom/squareup/onboarding/flow/OnboardingReactor_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p6, p0, Lcom/squareup/onboarding/flow/OnboardingReactor_Factory;->localeProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p7, p0, Lcom/squareup/onboarding/flow/OnboardingReactor_Factory;->panelVerifierProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p8, p0, Lcom/squareup/onboarding/flow/OnboardingReactor_Factory;->uniqueProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p9, p0, Lcom/squareup/onboarding/flow/OnboardingReactor_Factory;->secureScopeManagerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/onboarding/flow/OnboardingReactor_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/onboard/OnboardingService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/flow/PanelVerifier;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Unique;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/secure/SecureScopeManager;",
            ">;)",
            "Lcom/squareup/onboarding/flow/OnboardingReactor_Factory;"
        }
    .end annotation

    .line 71
    new-instance v10, Lcom/squareup/onboarding/flow/OnboardingReactor_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/onboarding/flow/OnboardingReactor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Lcom/squareup/server/onboard/OnboardingService;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/settings/server/AccountStatusSettings;Lio/reactivex/Scheduler;Lcom/squareup/analytics/Analytics;Ljavax/inject/Provider;Lcom/squareup/onboarding/flow/PanelVerifier;Lcom/squareup/util/Unique;Lcom/squareup/secure/SecureScopeManager;)Lcom/squareup/onboarding/flow/OnboardingReactor;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/onboard/OnboardingService;",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/analytics/Analytics;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/onboarding/flow/PanelVerifier;",
            "Lcom/squareup/util/Unique;",
            "Lcom/squareup/secure/SecureScopeManager;",
            ")",
            "Lcom/squareup/onboarding/flow/OnboardingReactor;"
        }
    .end annotation

    .line 78
    new-instance v10, Lcom/squareup/onboarding/flow/OnboardingReactor;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/onboarding/flow/OnboardingReactor;-><init>(Lcom/squareup/server/onboard/OnboardingService;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/settings/server/AccountStatusSettings;Lio/reactivex/Scheduler;Lcom/squareup/analytics/Analytics;Ljavax/inject/Provider;Lcom/squareup/onboarding/flow/PanelVerifier;Lcom/squareup/util/Unique;Lcom/squareup/secure/SecureScopeManager;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/onboarding/flow/OnboardingReactor;
    .locals 10

    .line 61
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingReactor_Factory;->onboardingServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/server/onboard/OnboardingService;

    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingReactor_Factory;->messagesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/receiving/FailureMessageFactory;

    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingReactor_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingReactor_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingReactor_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/analytics/Analytics;

    iget-object v6, p0, Lcom/squareup/onboarding/flow/OnboardingReactor_Factory;->localeProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingReactor_Factory;->panelVerifierProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/onboarding/flow/PanelVerifier;

    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingReactor_Factory;->uniqueProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/util/Unique;

    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingReactor_Factory;->secureScopeManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/secure/SecureScopeManager;

    invoke-static/range {v1 .. v9}, Lcom/squareup/onboarding/flow/OnboardingReactor_Factory;->newInstance(Lcom/squareup/server/onboard/OnboardingService;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/settings/server/AccountStatusSettings;Lio/reactivex/Scheduler;Lcom/squareup/analytics/Analytics;Ljavax/inject/Provider;Lcom/squareup/onboarding/flow/PanelVerifier;Lcom/squareup/util/Unique;Lcom/squareup/secure/SecureScopeManager;)Lcom/squareup/onboarding/flow/OnboardingReactor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/OnboardingReactor_Factory;->get()Lcom/squareup/onboarding/flow/OnboardingReactor;

    move-result-object v0

    return-object v0
.end method
