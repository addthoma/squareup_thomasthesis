.class public final Lcom/squareup/onboarding/flow/view/OnboardingImageViewHolder;
.super Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;
.source "OnboardingImageViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder<",
        "Lcom/squareup/onboarding/flow/data/OnboardingImageItem;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingImageViewHolder.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingImageViewHolder.kt\ncom/squareup/onboarding/flow/view/OnboardingImageViewHolder\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,47:1\n66#2:48\n*E\n*S KotlinDebug\n*F\n+ 1 OnboardingImageViewHolder.kt\ncom/squareup/onboarding/flow/view/OnboardingImageViewHolder\n*L\n27#1:48\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0002H\u0014R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\n\u001a\u00020\u000b8\u0000@\u0000X\u0081.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000c\u0010\r\"\u0004\u0008\u000e\u0010\u000f\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/view/OnboardingImageViewHolder;",
        "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;",
        "Lcom/squareup/onboarding/flow/data/OnboardingImageItem;",
        "inputHandler",
        "Lcom/squareup/onboarding/flow/OnboardingInputHandler;",
        "parent",
        "Landroid/view/ViewGroup;",
        "(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V",
        "imageView",
        "Landroid/widget/ImageView;",
        "picasso",
        "Lcom/squareup/picasso/Picasso;",
        "getPicasso$onboarding_release",
        "()Lcom/squareup/picasso/Picasso;",
        "setPicasso$onboarding_release",
        "(Lcom/squareup/picasso/Picasso;)V",
        "onBindComponent",
        "",
        "component",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final imageView:Landroid/widget/ImageView;

.field public picasso:Lcom/squareup/picasso/Picasso;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V
    .locals 1

    const-string v0, "inputHandler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    sget v0, Lcom/squareup/onboarding/flow/R$layout;->onboarding_component_image:I

    .line 19
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;-><init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;I)V

    .line 24
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingImageViewHolder;->itemView:Landroid/view/View;

    const-string p2, "itemView"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget p2, Lcom/squareup/onboarding/flow/R$id;->onboarding_image:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingImageViewHolder;->imageView:Landroid/widget/ImageView;

    .line 27
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/view/OnboardingImageViewHolder;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 48
    const-class p2, Lcom/squareup/onboarding/flow/OnboardingWorkflowParentComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->componentInParent(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/onboarding/flow/OnboardingWorkflowParentComponent;

    .line 28
    invoke-interface {p1, p0}, Lcom/squareup/onboarding/flow/OnboardingWorkflowParentComponent;->inject(Lcom/squareup/onboarding/flow/view/OnboardingImageViewHolder;)V

    return-void
.end method


# virtual methods
.method public final getPicasso$onboarding_release()Lcom/squareup/picasso/Picasso;
    .locals 2

    .line 22
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingImageViewHolder;->picasso:Lcom/squareup/picasso/Picasso;

    if-nez v0, :cond_0

    const-string v1, "picasso"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public bridge synthetic onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;)V
    .locals 0

    .line 16
    check-cast p1, Lcom/squareup/onboarding/flow/data/OnboardingImageItem;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingImageViewHolder;->onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingImageItem;)V

    return-void
.end method

.method protected onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingImageItem;)V
    .locals 3

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingImageItem;->url()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 33
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingImageViewHolder;->imageView:Landroid/widget/ImageView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void

    .line 37
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingImageItem;->imageSize()Lcom/squareup/onboarding/flow/data/ImageSize;

    move-result-object v0

    .line 38
    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingImageViewHolder;->picasso:Lcom/squareup/picasso/Picasso;

    if-nez v1, :cond_2

    const-string v2, "picasso"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingImageItem;->url()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    .line 40
    sget-object v1, Lcom/squareup/onboarding/flow/data/ImageSize;->ORIGINAL:Lcom/squareup/onboarding/flow/data/ImageSize;

    if-eq v0, v1, :cond_3

    .line 41
    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/data/ImageSize;->getSizeResId()I

    move-result v1

    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/data/ImageSize;->getSizeResId()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Lcom/squareup/picasso/RequestCreator;->resizeDimen(II)Lcom/squareup/picasso/RequestCreator;

    .line 44
    :cond_3
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingImageViewHolder;->imageView:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    return-void
.end method

.method public final setPicasso$onboarding_release(Lcom/squareup/picasso/Picasso;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingImageViewHolder;->picasso:Lcom/squareup/picasso/Picasso;

    return-void
.end method
