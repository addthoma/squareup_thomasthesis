.class public final Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;
.super Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;
.source "OnboardingAddressViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder<",
        "Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingAddressViewHolder.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingAddressViewHolder.kt\ncom/squareup/onboarding/flow/view/OnboardingAddressViewHolder\n*L\n1#1,125:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u001a\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0002J\u0006\u0010\u0015\u001a\u00020\u0016J\u0010\u0010\u0017\u001a\u00020\u00162\u0006\u0010\n\u001a\u00020\u0002H\u0014J\u0008\u0010\u0018\u001a\u00020\u0016H\u0016J5\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u0002H\u001b0\u001a\"\u0008\u0008\u0000\u0010\u001b*\u00020\u001c*\u00020\t2\u0017\u0010\u001d\u001a\u0013\u0012\u0004\u0012\u00020\u001f\u0012\u0004\u0012\u0002H\u001b0\u001e\u00a2\u0006\u0002\u0008 H\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u0002X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;",
        "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;",
        "Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;",
        "inputHandler",
        "Lcom/squareup/onboarding/flow/OnboardingInputHandler;",
        "parent",
        "Landroid/view/ViewGroup;",
        "(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V",
        "addressLayout",
        "Lcom/squareup/address/AddressLayout;",
        "component",
        "disposables",
        "Lio/reactivex/disposables/CompositeDisposable;",
        "header",
        "Lcom/squareup/marketfont/MarketTextView;",
        "checkPostalCode",
        "",
        "postalCode",
        "",
        "countryCode",
        "Lcom/squareup/CountryCode;",
        "observeAddressChanges",
        "",
        "onBindComponent",
        "onHighlightError",
        "field",
        "Lio/reactivex/Observable;",
        "T",
        "",
        "fieldExtractor",
        "Lkotlin/Function1;",
        "Lcom/squareup/address/Address;",
        "Lkotlin/ExtensionFunctionType;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final addressLayout:Lcom/squareup/address/AddressLayout;

.field private component:Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;

.field private final disposables:Lio/reactivex/disposables/CompositeDisposable;

.field private final header:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method public constructor <init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V
    .locals 1

    const-string v0, "inputHandler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    sget v0, Lcom/squareup/onboarding/flow/R$layout;->onboarding_component_address:I

    .line 27
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;-><init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;I)V

    .line 30
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->itemView:Landroid/view/View;

    const-string p2, "itemView"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v0, Lcom/squareup/onboarding/flow/R$id;->onboarding_address_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->header:Lcom/squareup/marketfont/MarketTextView;

    .line 31
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->itemView:Landroid/view/View;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget p2, Lcom/squareup/onboarding/flow/R$id;->onboarding_address:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/address/AddressLayout;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->addressLayout:Lcom/squareup/address/AddressLayout;

    .line 33
    new-instance p1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 38
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->addressLayout:Lcom/squareup/address/AddressLayout;

    new-instance p2, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$1;

    invoke-direct {p2, p0}, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$1;-><init>(Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;)V

    check-cast p2, Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {p1, p2}, Lcom/squareup/address/AddressLayout;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    return-void
.end method

.method public static final synthetic access$checkPostalCode(Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;Ljava/lang/String;Lcom/squareup/CountryCode;)Z
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2}, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->checkPostalCode(Ljava/lang/String;Lcom/squareup/CountryCode;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$getAddressLayout$p(Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;)Lcom/squareup/address/AddressLayout;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->addressLayout:Lcom/squareup/address/AddressLayout;

    return-object p0
.end method

.method public static final synthetic access$getDisposables$p(Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;)Lio/reactivex/disposables/CompositeDisposable;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    return-object p0
.end method

.method private final checkPostalCode(Ljava/lang/String;Lcom/squareup/CountryCode;)Z
    .locals 2

    const/4 v0, 0x1

    if-nez p2, :cond_0

    goto :goto_0

    .line 112
    :cond_0
    sget-object v1, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p2}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p2

    aget p2, v1, p2

    if-eq p2, v0, :cond_2

    const/4 v1, 0x2

    if-eq p2, v1, :cond_1

    goto :goto_0

    .line 114
    :cond_1
    invoke-static {p1}, Lcom/squareup/text/PostalCodes;->isUsZipCode(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 113
    :cond_2
    invoke-static {p1}, Lcom/squareup/text/PostalCodes;->isCaPostalCode(Ljava/lang/String;)Z

    move-result v0

    :goto_0
    return v0
.end method

.method private final field(Lcom/squareup/address/AddressLayout;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/address/AddressLayout;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/address/Address;",
            "+TT;>;)",
            "Lio/reactivex/Observable<",
            "TT;>;"
        }
    .end annotation

    .line 120
    invoke-virtual {p1}, Lcom/squareup/address/AddressLayout;->address()Lio/reactivex/Observable;

    move-result-object p1

    if-eqz p2, :cond_0

    .line 121
    new-instance v0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$sam$io_reactivex_functions_Function$0;

    invoke-direct {v0, p2}, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$sam$io_reactivex_functions_Function$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object p2, v0

    :cond_0
    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 122
    invoke-virtual {p1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "address()\n        .map(f\u2026  .distinctUntilChanged()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public final observeAddressChanges()V
    .locals 10

    .line 60
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->component:Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;

    if-eqz v0, :cond_0

    .line 62
    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v1}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 64
    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v2, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->addressLayout:Lcom/squareup/address/AddressLayout;

    sget-object v3, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$observeAddressChanges$1;->INSTANCE:Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$observeAddressChanges$1;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v2, v3}, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->field(Lcom/squareup/address/AddressLayout;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;

    move-result-object v2

    .line 65
    new-instance v3, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$observeAddressChanges$2;

    invoke-direct {v3, p0, v0}, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$observeAddressChanges$2;-><init>(Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;)V

    check-cast v3, Lio/reactivex/functions/Consumer;

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v2

    const-string v3, "addressLayout.field { st\u2026onent.streetOutput(it)) }"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-static {v1, v2}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    .line 67
    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v2, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->addressLayout:Lcom/squareup/address/AddressLayout;

    sget-object v3, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$observeAddressChanges$3;->INSTANCE:Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$observeAddressChanges$3;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v2, v3}, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->field(Lcom/squareup/address/AddressLayout;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;

    move-result-object v2

    .line 68
    new-instance v3, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$observeAddressChanges$4;

    invoke-direct {v3, p0, v0}, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$observeAddressChanges$4;-><init>(Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;)V

    check-cast v3, Lio/reactivex/functions/Consumer;

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v2

    const-string v3, "addressLayout.field { ap\u2026nt.apartmentOutput(it)) }"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-static {v1, v2}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    .line 70
    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v2, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->addressLayout:Lcom/squareup/address/AddressLayout;

    sget-object v3, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$observeAddressChanges$5;->INSTANCE:Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$observeAddressChanges$5;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v2, v3}, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->field(Lcom/squareup/address/AddressLayout;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;

    move-result-object v2

    .line 71
    new-instance v3, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$observeAddressChanges$6;

    invoke-direct {v3, p0, v0}, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$observeAddressChanges$6;-><init>(Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;)V

    check-cast v3, Lio/reactivex/functions/Consumer;

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v2

    const-string v3, "addressLayout.field { ci\u2026mponent.cityOutput(it)) }"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    invoke-static {v1, v2}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    .line 73
    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v2, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->addressLayout:Lcom/squareup/address/AddressLayout;

    sget-object v3, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$observeAddressChanges$7;->INSTANCE:Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$observeAddressChanges$7;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v2, v3}, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->field(Lcom/squareup/address/AddressLayout;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;

    move-result-object v2

    .line 74
    new-instance v3, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$observeAddressChanges$8;

    invoke-direct {v3, p0, v0}, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$observeAddressChanges$8;-><init>(Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;)V

    check-cast v3, Lio/reactivex/functions/Consumer;

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v2

    const-string v3, "addressLayout.field { st\u2026ponent.stateOutput(it)) }"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-static {v1, v2}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    .line 76
    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v2, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->addressLayout:Lcom/squareup/address/AddressLayout;

    sget-object v3, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$observeAddressChanges$9;->INSTANCE:Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$observeAddressChanges$9;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v2, v3}, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->field(Lcom/squareup/address/AddressLayout;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;

    move-result-object v2

    .line 77
    new-instance v3, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$observeAddressChanges$10;

    invoke-direct {v3, p0, v0}, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder$observeAddressChanges$10;-><init>(Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;)V

    check-cast v3, Lio/reactivex/functions/Consumer;

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v2

    const-string v3, "addressLayout.field { po\u2026y))\n          )\n        }"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    invoke-static {v1, v2}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    .line 84
    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->addressLayout:Lcom/squareup/address/AddressLayout;

    new-instance v9, Lcom/squareup/address/Address;

    .line 85
    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;->defaultStreet()Ljava/lang/String;

    move-result-object v3

    .line 86
    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;->defaultApartment()Ljava/lang/String;

    move-result-object v4

    .line 87
    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;->defaultCity()Ljava/lang/String;

    move-result-object v5

    .line 88
    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;->defaultState()Ljava/lang/String;

    move-result-object v6

    .line 89
    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;->defaultPostalCode()Ljava/lang/String;

    move-result-object v7

    .line 90
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->addressLayout:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v0}, Lcom/squareup/address/AddressLayout;->getCountry()Lcom/squareup/CountryCode;

    move-result-object v8

    move-object v2, v9

    .line 84
    invoke-direct/range {v2 .. v8}, Lcom/squareup/address/Address;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/CountryCode;)V

    invoke-virtual {v1, v9}, Lcom/squareup/address/AddressLayout;->setAddress(Lcom/squareup/address/Address;)V

    :cond_0
    return-void
.end method

.method protected onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;)V
    .locals 1

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->component:Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;

    .line 52
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->header:Lcom/squareup/marketfont/MarketTextView;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;->label()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setTextAndVisibility(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 54
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->addressLayout:Lcom/squareup/address/AddressLayout;

    invoke-virtual {p1}, Lcom/squareup/address/AddressLayout;->isAttachedToWindow()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 55
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->observeAddressChanges()V

    :cond_0
    return-void
.end method

.method public bridge synthetic onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;)V
    .locals 0

    .line 24
    check-cast p1, Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;)V

    return-void
.end method

.method public onHighlightError()V
    .locals 2

    .line 95
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->addressLayout:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v0}, Lcom/squareup/address/AddressLayout;->getEmptyField()Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 98
    invoke-virtual {v0}, Landroid/widget/TextView;->performClick()Z

    move-result v1

    if-nez v1, :cond_1

    .line 99
    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    .line 100
    instance-of v1, v0, Landroid/widget/EditText;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Landroid/widget/EditText;

    if-eqz v0, :cond_1

    invoke-virtual {p0, v0}, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;->setSelectionEnd(Landroid/widget/EditText;)V

    :cond_1
    return-void
.end method
