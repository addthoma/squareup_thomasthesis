.class public final Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;
.super Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;
.source "OnboardingEditViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder<",
        "Lcom/squareup/onboarding/flow/data/OnboardingEditItem;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingEditViewHolder.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingEditViewHolder.kt\ncom/squareup/onboarding/flow/view/OnboardingEditViewHolder\n+ 2 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n+ 3 ArraysJVM.kt\nkotlin/collections/ArraysKt__ArraysJVMKt\n*L\n1#1,85:1\n10725#2,2:86\n3862#2:88\n3940#2,2:89\n37#3,2:91\n*E\n*S KotlinDebug\n*F\n+ 1 OnboardingEditViewHolder.kt\ncom/squareup/onboarding/flow/view/OnboardingEditViewHolder\n*L\n74#1,2:86\n80#1:88\n80#1,2:89\n81#1,2:91\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0008\u0010\u000f\u001a\u00020\u0010H\u0002J\u0010\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0012\u001a\u00020\u0002H\u0014J\u0008\u0010\u0013\u001a\u00020\u0010H\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;",
        "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;",
        "Lcom/squareup/onboarding/flow/data/OnboardingEditItem;",
        "inputHandler",
        "Lcom/squareup/onboarding/flow/OnboardingInputHandler;",
        "parent",
        "Landroid/view/ViewGroup;",
        "(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V",
        "field",
        "Lcom/squareup/noho/NohoEditText;",
        "formatWatcher",
        "Landroid/text/TextWatcher;",
        "header",
        "Lcom/squareup/marketfont/MarketTextView;",
        "valueWatcher",
        "maybeFilterInputFilters",
        "",
        "onBindComponent",
        "component",
        "onHighlightError",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final field:Lcom/squareup/noho/NohoEditText;

.field private formatWatcher:Landroid/text/TextWatcher;

.field private final header:Lcom/squareup/marketfont/MarketTextView;

.field private valueWatcher:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V
    .locals 1

    const-string v0, "inputHandler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    sget v0, Lcom/squareup/onboarding/flow/R$layout;->onboarding_component_text_field:I

    .line 22
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;-><init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;I)V

    .line 26
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;->itemView:Landroid/view/View;

    const-string p2, "itemView"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v0, Lcom/squareup/onboarding/flow/R$id;->onboarding_text_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;->header:Lcom/squareup/marketfont/MarketTextView;

    .line 27
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;->itemView:Landroid/view/View;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget p2, Lcom/squareup/onboarding/flow/R$id;->onboarding_text_field:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoEditText;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;->field:Lcom/squareup/noho/NohoEditText;

    .line 33
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;->field:Lcom/squareup/noho/NohoEditText;

    new-instance p2, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder$1;

    invoke-direct {p2, p0}, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder$1;-><init>(Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;)V

    check-cast p2, Landroid/text/TextWatcher;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public static final synthetic access$maybeFilterInputFilters(Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;->maybeFilterInputFilters()V

    return-void
.end method

.method private final maybeFilterInputFilters()V
    .locals 8

    .line 72
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;->field:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_5

    const-string v1, "field.text ?: return"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-interface {v0}, Landroid/text/Editable;->getFilters()[Landroid/text/InputFilter;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 74
    iget-object v2, p0, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;->formatWatcher:Landroid/text/TextWatcher;

    if-eqz v2, :cond_5

    .line 86
    array-length v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_1

    aget-object v5, v1, v4

    .line 74
    instance-of v5, v5, Landroid/text/method/DigitsKeyListener;

    if-eqz v5, :cond_0

    const/4 v2, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_5

    .line 88
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 89
    array-length v4, v1

    const/4 v5, 0x0

    :goto_2
    if-ge v5, v4, :cond_3

    aget-object v6, v1, v5

    .line 80
    instance-of v7, v6, Landroid/text/method/DigitsKeyListener;

    if-nez v7, :cond_2

    invoke-interface {v2, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 90
    :cond_3
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/util/Collection;

    new-array v1, v3, [Landroid/text/InputFilter;

    .line 92
    invoke-interface {v2, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_4

    check-cast v1, [Landroid/text/InputFilter;

    invoke-interface {v0, v1}, Landroid/text/Editable;->setFilters([Landroid/text/InputFilter;)V

    goto :goto_3

    :cond_4
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    :goto_3
    return-void
.end method


# virtual methods
.method public bridge synthetic onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;)V
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/onboarding/flow/data/OnboardingEditItem;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;->onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingEditItem;)V

    return-void
.end method

.method protected onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingEditItem;)V
    .locals 5

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;->header:Lcom/squareup/marketfont/MarketTextView;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingEditItem;->label()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setTextAndVisibility(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 43
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;->field:Lcom/squareup/noho/NohoEditText;

    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;->valueWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 44
    new-instance v0, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder$onBindComponent$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder$onBindComponent$1;-><init>(Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;Lcom/squareup/onboarding/flow/data/OnboardingEditItem;)V

    check-cast v0, Landroid/text/TextWatcher;

    iput-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;->valueWatcher:Landroid/text/TextWatcher;

    .line 49
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;->field:Lcom/squareup/noho/NohoEditText;

    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;->valueWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;->field:Lcom/squareup/noho/NohoEditText;

    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;->formatWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 52
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingEditItem;->format()Ljava/lang/String;

    move-result-object v0

    .line 53
    move-object v1, v0

    check-cast v1, Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    .line 54
    new-instance v1, Lcom/squareup/text/ScrubbingTextWatcher;

    new-instance v4, Lcom/squareup/text/PatternScrubber;

    invoke-direct {v4, v0}, Lcom/squareup/text/PatternScrubber;-><init>(Ljava/lang/String;)V

    check-cast v4, Lcom/squareup/text/Scrubber;

    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;->field:Lcom/squareup/noho/NohoEditText;

    check-cast v0, Lcom/squareup/text/HasSelectableText;

    invoke-direct {v1, v4, v0}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/Scrubber;Lcom/squareup/text/HasSelectableText;)V

    check-cast v1, Landroid/text/TextWatcher;

    iput-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;->formatWatcher:Landroid/text/TextWatcher;

    .line 55
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;->field:Lcom/squareup/noho/NohoEditText;

    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;->formatWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 58
    :cond_1
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;->field:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingEditItem;->hint()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;->field:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingEditItem;->inputFlags()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->setInputType(I)V

    .line 60
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;->field:Lcom/squareup/noho/NohoEditText;

    new-array v1, v2, [Landroid/text/InputFilter$LengthFilter;

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingEditItem;->maxLength()I

    move-result v4

    invoke-direct {v2, v4}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v1, v3

    check-cast v1, [Landroid/text/InputFilter;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;->field:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingEditItem;->defaultValue()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditText;->setText(Ljava/lang/CharSequence;)V

    .line 63
    invoke-direct {p0}, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;->maybeFilterInputFilters()V

    return-void
.end method

.method public onHighlightError()V
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;->field:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditText;->requestFocus()Z

    .line 68
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;->field:Lcom/squareup/noho/NohoEditText;

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {p0, v0}, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;->setSelectionEnd(Landroid/widget/EditText;)V

    return-void
.end method
