.class public final Lcom/squareup/onboarding/BannerButtonResolver_Factory;
.super Ljava/lang/Object;
.source "BannerButtonResolver_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/onboarding/BannerButtonResolver;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final bankLinkingStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankLinkingStarter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankLinkingStarter;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/onboarding/BannerButtonResolver_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/onboarding/BannerButtonResolver_Factory;->bankLinkingStarterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/onboarding/BannerButtonResolver_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankLinkingStarter;",
            ">;)",
            "Lcom/squareup/onboarding/BannerButtonResolver_Factory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/onboarding/BannerButtonResolver_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/onboarding/BannerButtonResolver_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/banklinking/BankLinkingStarter;)Lcom/squareup/onboarding/BannerButtonResolver;
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/onboarding/BannerButtonResolver;

    invoke-direct {v0, p0, p1}, Lcom/squareup/onboarding/BannerButtonResolver;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/banklinking/BankLinkingStarter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/onboarding/BannerButtonResolver;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/onboarding/BannerButtonResolver_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, p0, Lcom/squareup/onboarding/BannerButtonResolver_Factory;->bankLinkingStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/banklinking/BankLinkingStarter;

    invoke-static {v0, v1}, Lcom/squareup/onboarding/BannerButtonResolver_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/banklinking/BankLinkingStarter;)Lcom/squareup/onboarding/BannerButtonResolver;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/onboarding/BannerButtonResolver_Factory;->get()Lcom/squareup/onboarding/BannerButtonResolver;

    move-result-object v0

    return-object v0
.end method
