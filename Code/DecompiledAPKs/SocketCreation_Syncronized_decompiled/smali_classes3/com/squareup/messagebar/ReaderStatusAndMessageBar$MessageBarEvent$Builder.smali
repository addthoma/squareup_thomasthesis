.class final Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;
.super Ljava/lang/Object;
.source "ReaderStatusAndMessageBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Builder"
.end annotation


# instance fields
.field private barAnimationType:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

.field private batteryLevel:Lcom/squareup/cardreader/BatteryLevel;

.field private colorId:I

.field private ignoreColorId:Z

.field private message:Ljava/lang/CharSequence;

.field private messageAnimationType:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 739
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 740
    iput-boolean v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->ignoreColorId:Z

    .line 742
    sget-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;->IGNORE_MESSAGE:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    iput-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->messageAnimationType:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    .line 743
    sget-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;->IGNORE_BAR:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    iput-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->barAnimationType:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    .line 744
    sget-object v0, Lcom/squareup/messagebar/ReaderMessageBarView;->HIDDEN_BATTERY_LEVEL:Lcom/squareup/cardreader/BatteryLevel;

    iput-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->batteryLevel:Lcom/squareup/cardreader/BatteryLevel;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;)V
    .locals 0

    .line 730
    invoke-direct {p0}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;)I
    .locals 0

    .line 730
    iget p0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->colorId:I

    return p0
.end method

.method static synthetic access$200(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;)Z
    .locals 0

    .line 730
    iget-boolean p0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->ignoreColorId:Z

    return p0
.end method

.method static synthetic access$300(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;
    .locals 0

    .line 730
    iget-object p0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->messageAnimationType:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;)Ljava/lang/CharSequence;
    .locals 0

    .line 730
    iget-object p0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->message:Ljava/lang/CharSequence;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;
    .locals 0

    .line 730
    iget-object p0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->barAnimationType:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;)Lcom/squareup/cardreader/BatteryLevel;
    .locals 0

    .line 730
    iget-object p0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->batteryLevel:Lcom/squareup/cardreader/BatteryLevel;

    return-object p0
.end method


# virtual methods
.method public barAnimationType(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;
    .locals 0

    .line 794
    iput-object p1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->barAnimationType:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    return-object p0
.end method

.method public build()Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;
    .locals 2

    .line 799
    new-instance v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;-><init>(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;)V

    return-object v0
.end method

.method public colorId(I)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;
    .locals 0

    .line 748
    iput p1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->colorId:I

    return-object p0
.end method

.method public messageAnimationType(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;
    .locals 0

    .line 789
    iput-object p1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->messageAnimationType:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    return-object p0
.end method

.method public setBatteryLevel(Lcom/squareup/cardreader/BatteryLevel;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;
    .locals 0

    .line 784
    iput-object p1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->batteryLevel:Lcom/squareup/cardreader/BatteryLevel;

    return-object p0
.end method

.method public setColorId(I)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 753
    invoke-virtual {p0, p1, v0}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setColorId(IZ)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setColorId(IZ)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;
    .locals 0

    .line 757
    iput-boolean p2, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->ignoreColorId:Z

    .line 758
    iput p1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->colorId:I

    return-object p0
.end method

.method public setMessage(Lcom/squareup/phrase/Phrase;I)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;
    .locals 1

    const-string v0, "percent"

    .line 774
    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setMessage(Lcom/squareup/phrase/Phrase;Ljava/lang/CharSequence;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;
    .locals 1

    const-string v0, "reader"

    .line 770
    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setMessage(Lcom/squareup/phrase/Phrase;Ljava/lang/CharSequence;I)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;
    .locals 1

    const-string v0, "reader"

    .line 780
    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const-string p2, "percent"

    invoke-virtual {p1, p2, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 779
    invoke-virtual {p0, p1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setMessage(Ljava/lang/CharSequence;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;
    .locals 0

    .line 763
    iput-object p1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->message:Ljava/lang/CharSequence;

    .line 764
    sget-object p1, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;->CYCLE_IN_FROM_TOP:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    iput-object p1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->messageAnimationType:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    .line 765
    sget-object p1, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;->CYCLE_IN_IF_GONE:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    iput-object p1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->barAnimationType:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    return-object p0
.end method
