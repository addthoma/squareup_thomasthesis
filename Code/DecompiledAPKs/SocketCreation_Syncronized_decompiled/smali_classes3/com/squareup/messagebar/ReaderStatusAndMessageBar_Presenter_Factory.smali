.class public final Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;
.super Ljava/lang/Object;
.source "ReaderStatusAndMessageBar_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final badBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderHubProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderOracleProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderScreensGatewayProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/ReaderStatusMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final dippedCardTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final mainContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final offlineModeMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryScreenStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final statusBarHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/StatusBarHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/ReaderStatusMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/StatusBarHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;)V"
        }
    .end annotation

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;->dippedCardTrackerProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p2, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;->cardReaderOracleProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p3, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p4, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;->badBusProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p5, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p6, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p7, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;->cardReaderScreensGatewayProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p8, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;->statusBarHelperProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p9, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p10, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p11, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    .line 78
    iput-object p12, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    .line 79
    iput-object p13, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/ReaderStatusMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/StatusBarHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;)",
            "Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;"
        }
    .end annotation

    .line 99
    new-instance v14, Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v14
.end method

.method public static newInstance(Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/badbus/BadBus;Lcom/squareup/util/Res;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/ui/StatusBarHelper;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/cardreader/CardReaderHub;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;
    .locals 15

    .line 108
    new-instance v14, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;-><init>(Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/badbus/BadBus;Lcom/squareup/util/Res;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/ui/StatusBarHelper;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/cardreader/CardReaderHub;)V

    return-object v14
.end method


# virtual methods
.method public get()Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;
    .locals 14

    .line 84
    iget-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;->dippedCardTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/cardreader/DippedCardTracker;

    iget-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;->cardReaderOracleProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    iget-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/orderentry/OrderEntryScreenState;

    iget-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;->badBusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/badbus/BadBus;

    iget-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/main/PosContainer;

    iget-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;->cardReaderScreensGatewayProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/ui/main/ReaderStatusMonitor;

    iget-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;->statusBarHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/ui/StatusBarHelper;

    iget-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/payment/OfflineModeMonitor;

    iget-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/squareup/cardreader/CardReaderHub;

    invoke-static/range {v1 .. v13}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;->newInstance(Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/badbus/BadBus;Lcom/squareup/util/Res;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/ui/StatusBarHelper;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/cardreader/CardReaderHub;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar_Presenter_Factory;->get()Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;

    move-result-object v0

    return-object v0
.end method
