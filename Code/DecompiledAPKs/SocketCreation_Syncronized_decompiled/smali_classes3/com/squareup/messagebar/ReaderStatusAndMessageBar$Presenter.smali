.class public Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;
.super Lmortar/ViewPresenter;
.source "ReaderStatusAndMessageBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/messagebar/ReaderStatusAndMessageBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/messagebar/ReaderMessageBarView;",
        ">;"
    }
.end annotation


# static fields
.field private static final ASSET_WILL_REBOOT_PERCENT:I = 0x5a

.field private static final NEARLY_DONE_PERCENT:I = 0x63


# instance fields
.field private final badBus:Lcom/squareup/badbus/BadBus;

.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field private final cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

.field private final cardReaderScreensGateway:Lcom/squareup/ui/main/ReaderStatusMonitor;

.field private final cardSwipedRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

.field private final internetState:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/connectivity/InternetState;",
            ">;"
        }
    .end annotation
.end field

.field private final mainContainer:Lcom/squareup/ui/main/PosContainer;

.field private final offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

.field private oneReaderConnectedAndMissingMicPermission:Z

.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final statusBarHelper:Lcom/squareup/ui/StatusBarHelper;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/badbus/BadBus;Lcom/squareup/util/Res;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/ui/StatusBarHelper;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/cardreader/CardReaderHub;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 108
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    const/4 v0, 0x0

    .line 98
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->cardSwipedRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 109
    iput-object p1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    .line 110
    iput-object p2, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    .line 111
    iput-object p3, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 112
    iput-object p4, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->badBus:Lcom/squareup/badbus/BadBus;

    .line 113
    iput-object p5, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    .line 114
    iput-object p6, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    .line 115
    iput-object p7, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->cardReaderScreensGateway:Lcom/squareup/ui/main/ReaderStatusMonitor;

    .line 116
    iput-object p8, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->statusBarHelper:Lcom/squareup/ui/StatusBarHelper;

    .line 117
    iput-object p9, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 118
    iput-object p10, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 119
    invoke-interface {p11}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->internetState:Lio/reactivex/Observable;

    .line 120
    iput-object p12, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    .line 121
    iput-object p13, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    .line 123
    iput-boolean v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->oneReaderConnectedAndMissingMicPermission:Z

    return-void
.end method

.method private applyReaderReadyModifications(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;ZZZ)V
    .locals 0

    if-eqz p2, :cond_1

    if-eqz p4, :cond_0

    .line 331
    iget-object p2, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/messagebar/R$string;->messagebar_reader_ready_card_inserted_press_charge:I

    .line 333
    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 332
    invoke-virtual {p1, p2}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    move-result-object p1

    sget-object p2, Lcom/squareup/messagebar/ReaderMessageBarView;->HIDDEN_BATTERY_LEVEL:Lcom/squareup/cardreader/BatteryLevel;

    .line 334
    invoke-virtual {p1, p2}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setBatteryLevel(Lcom/squareup/cardreader/BatteryLevel;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    goto :goto_0

    .line 336
    :cond_0
    iget-object p2, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/messagebar/R$string;->messagebar_reader_ready_card_inserted:I

    .line 337
    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    move-result-object p1

    sget-object p2, Lcom/squareup/messagebar/ReaderMessageBarView;->HIDDEN_BATTERY_LEVEL:Lcom/squareup/cardreader/BatteryLevel;

    .line 338
    invoke-virtual {p1, p2}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setBatteryLevel(Lcom/squareup/cardreader/BatteryLevel;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    goto :goto_0

    :cond_1
    if-eqz p3, :cond_3

    if-eqz p4, :cond_2

    .line 342
    iget-object p2, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/messagebar/R$string;->messagebar_reader_ready_card_swiped_press_charge:I

    .line 343
    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    move-result-object p1

    sget-object p2, Lcom/squareup/messagebar/ReaderMessageBarView;->HIDDEN_BATTERY_LEVEL:Lcom/squareup/cardreader/BatteryLevel;

    .line 344
    invoke-virtual {p1, p2}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setBatteryLevel(Lcom/squareup/cardreader/BatteryLevel;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    goto :goto_0

    .line 346
    :cond_2
    iget-object p2, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/messagebar/R$string;->messagebar_reader_ready_card_swiped:I

    .line 347
    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    move-result-object p1

    sget-object p2, Lcom/squareup/messagebar/ReaderMessageBarView;->HIDDEN_BATTERY_LEVEL:Lcom/squareup/cardreader/BatteryLevel;

    .line 348
    invoke-virtual {p1, p2}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setBatteryLevel(Lcom/squareup/cardreader/BatteryLevel;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    :cond_3
    :goto_0
    return-void
.end method

.method private getMessageForMultipleReaders(ILcom/squareup/ui/settings/paymentdevices/ReaderState$Level;Z)Ljava/lang/CharSequence;
    .locals 3

    .line 271
    sget-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Level:[I

    invoke-virtual {p2}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->ordinal()I

    move-result p2

    aget p2, v0, p2

    const-string v0, "count"

    const/4 v1, 0x1

    packed-switch p2, :pswitch_data_0

    .line 321
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Invalid reader event level, can\'t construct status bar message"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 319
    :pswitch_0
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/messagebar/R$string;->messagebar_reader_unavailable_and_tap:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_1
    if-eqz p3, :cond_1

    const-string p2, "reader"

    if-ne p1, v1, :cond_0

    .line 300
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/messagebar/R$string;->messagebar_reader_ready_low_battery_single:I

    invoke-interface {p1, p3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object p3, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/messagebar/R$string;->smart_reader_anonymous_name:I

    .line 301
    invoke-interface {p3, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 302
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    .line 304
    :cond_0
    iget-object p3, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/messagebar/R$string;->messagebar_reader_ready_low_battery_plural:I

    invoke-interface {p3, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p3

    iget-object v1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/messagebar/R$string;->smart_reader_anonymous_name:I

    .line 305
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, p2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 306
    invoke-virtual {p2, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 307
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    :cond_1
    if-ne p1, v1, :cond_2

    .line 311
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/messagebar/R$string;->messagebar_readers_ready_single:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 313
    :cond_2
    iget-object p2, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/messagebar/R$string;->messagebar_readers_ready_plural:I

    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 314
    invoke-virtual {p2, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 315
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    :pswitch_2
    if-ne p1, v1, :cond_3

    .line 291
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/messagebar/R$string;->messagebar_readers_connecting_single:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 293
    :cond_3
    iget-object p2, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/messagebar/R$string;->messagebar_readers_connecting_plural:I

    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 294
    invoke-virtual {p2, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 295
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    :pswitch_3
    if-ne p1, v1, :cond_4

    .line 283
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/messagebar/R$string;->messagebar_readers_not_ready_single:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 285
    :cond_4
    iget-object p2, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/messagebar/R$string;->messagebar_readers_not_ready_plural:I

    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 286
    invoke-virtual {p2, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 287
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    :pswitch_4
    if-ne p1, v1, :cond_5

    .line 274
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/messagebar/R$string;->messagebar_readers_updating_single:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 276
    :cond_5
    iget-object p2, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/messagebar/R$string;->messagebar_readers_updating_plural:I

    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 277
    invoke-virtual {p2, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 278
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getMessageForSingleOrMultipleReaders(Ljava/util/Collection;Z)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
            ">;Z)",
            "Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;"
        }
    .end annotation

    .line 523
    iget-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasCard()Z

    move-result v0

    .line 526
    invoke-static {}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->values()[Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    move-result-object v1

    array-length v1, v1

    .line 527
    new-array v1, v1, [I

    .line 531
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    const/4 v8, 0x1

    if-eqz v7, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    .line 532
    iget-object v9, v7, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    sget-object v10, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_BATTERY_LOW:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    if-ne v9, v10, :cond_0

    const/4 v6, 0x1

    .line 535
    :cond_0
    iget-object v9, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    invoke-virtual {v9}, Lcom/squareup/cardreader/DippedCardTracker;->mustReinsertDippedCard()Z

    move-result v9

    if-nez v9, :cond_1

    .line 536
    iget-object v9, v7, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    if-eqz v9, :cond_1

    .line 537
    iget-object v9, v7, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v9}, Lcom/squareup/cardreader/CardReaderInfo;->isCardPresent()Z

    move-result v9

    or-int/2addr v5, v9

    .line 541
    :cond_1
    iget-object v7, v7, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    iget-object v7, v7, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->level:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    invoke-virtual {v7}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->ordinal()I

    move-result v7

    aget v9, v1, v7

    add-int/2addr v9, v8

    aput v9, v1, v7

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 545
    :cond_2
    sget-object v2, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_DISCONNECTED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    invoke-virtual {v2}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->ordinal()I

    move-result v2

    aget v2, v1, v2

    sub-int v2, v4, v2

    .line 548
    invoke-static {}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;->constructHideEvent()Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    move-result-object v7

    if-nez v4, :cond_3

    if-eqz v0, :cond_e

    .line 554
    sget p1, Lcom/squareup/messagebar/R$color;->message_bar_background_ready:I

    .line 555
    invoke-virtual {v7, p1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setColorId(I)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    .line 556
    invoke-direct {p0, v7, v5, v0, p2}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->applyReaderReadyModifications(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;ZZZ)V

    goto/16 :goto_4

    :cond_3
    const/4 v4, 0x0

    if-ne v2, v8, :cond_7

    .line 564
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    .line 565
    iget-object v2, v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    iget-object v2, v2, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->level:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    sget-object v3, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_DISCONNECTED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    if-eq v2, v3, :cond_4

    goto :goto_1

    :cond_5
    move-object v1, v4

    :goto_1
    if-eqz v1, :cond_e

    .line 572
    invoke-virtual {p0, v1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->getMessageForSingleReader(Lcom/squareup/ui/settings/paymentdevices/ReaderState;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    move-result-object v7

    .line 574
    iget-object p1, v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    iget-object p1, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->level:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    sget-object v2, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_UPDATING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    if-ne p1, v2, :cond_6

    .line 575
    sget-object p1, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;->SHOW_IMMEDIATELY:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    .line 576
    invoke-virtual {v7, p1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->messageAnimationType(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    .line 579
    :cond_6
    iget-object p1, v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    iget-object p1, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->level:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_READY:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    if-ne p1, v1, :cond_e

    .line 580
    invoke-direct {p0, v7, v5, v0, p2}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->applyReaderReadyModifications(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;ZZZ)V

    goto :goto_4

    .line 587
    :cond_7
    invoke-static {}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->values()[Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    move-result-object v2

    array-length v9, v2

    const/4 v10, 0x0

    :goto_2
    if-ge v10, v9, :cond_e

    aget-object v11, v2, v10

    .line 588
    invoke-virtual {v11}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->ordinal()I

    move-result v12

    aget v12, v1, v12

    if-nez v12, :cond_8

    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 601
    :cond_8
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v2, 0x0

    :cond_9
    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_b

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    .line 602
    iget-object v9, v7, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    iget-object v9, v9, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->level:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    if-eq v9, v11, :cond_a

    goto :goto_3

    .line 605
    :cond_a
    iget-object v7, v7, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    sget-object v9, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_SS_RENEWING_SESSION:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    if-ne v7, v9, :cond_9

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 610
    :cond_b
    new-instance p1, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    invoke-direct {p1, v4}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;-><init>(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;)V

    if-ne v12, v2, :cond_c

    const/4 v3, 0x1

    .line 611
    :cond_c
    invoke-static {v11, v3}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->getStatusBarColor(Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;Z)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setColorId(I)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    move-result-object p1

    .line 614
    invoke-virtual {v11}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->ordinal()I

    move-result v2

    aget v1, v1, v2

    invoke-direct {p0, v1, v11, v6}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->getMessageForMultipleReaders(ILcom/squareup/ui/settings/paymentdevices/ReaderState$Level;Z)Ljava/lang/CharSequence;

    move-result-object v1

    .line 613
    invoke-virtual {p1, v1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    move-result-object p1

    sget-object v1, Lcom/squareup/messagebar/ReaderMessageBarView;->HIDDEN_BATTERY_LEVEL:Lcom/squareup/cardreader/BatteryLevel;

    .line 616
    invoke-virtual {p1, v1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setBatteryLevel(Lcom/squareup/cardreader/BatteryLevel;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    move-result-object v7

    .line 619
    sget-object p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_UPDATING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    if-ne v11, p1, :cond_d

    .line 620
    sget-object p1, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;->SHOW_IMMEDIATELY:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    .line 621
    invoke-virtual {v7, p1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->messageAnimationType(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    .line 624
    :cond_d
    sget-object p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_READY:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    if-ne v11, p1, :cond_e

    .line 625
    invoke-direct {p0, v7, v5, v0, p2}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->applyReaderReadyModifications(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;ZZZ)V

    :cond_e
    :goto_4
    return-object v7
.end method

.method private getOfflineColor()I
    .locals 1

    .line 386
    iget-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStoreAndForwardSettings()Lcom/squareup/settings/server/StoreAndForwardSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/StoreAndForwardSettings;->isStoreAndForwardEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/squareup/messagebar/R$color;->message_bar_background_ready:I

    goto :goto_0

    :cond_0
    sget v0, Lcom/squareup/messagebar/R$color;->message_bar_background_offline_mode:I

    :goto_0
    return v0
.end method

.method private getOfflineString()Ljava/lang/String;
    .locals 2

    .line 380
    iget-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getStoreAndForwardSettings()Lcom/squareup/settings/server/StoreAndForwardSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/StoreAndForwardSettings;->isStoreAndForwardEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Lcom/squareup/cardreader/ui/R$string;->messagebar_reader_offline_swipe_only:I

    goto :goto_0

    :cond_0
    sget v1, Lcom/squareup/messagebar/R$string;->messagebar_reader_offline:I

    :goto_0
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getStatusBarColor(Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;Z)I
    .locals 2

    .line 359
    sget-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Level:[I

    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 375
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown ReaderState.Level: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 373
    :pswitch_0
    sget p0, Lcom/squareup/messagebar/R$color;->message_bar_background_attention:I

    return p0

    .line 371
    :pswitch_1
    sget p0, Lcom/squareup/messagebar/R$color;->message_bar_background_ready:I

    return p0

    :pswitch_2
    if-eqz p1, :cond_0

    .line 367
    sget p0, Lcom/squareup/messagebar/R$color;->message_bar_background_ready:I

    return p0

    .line 369
    :cond_0
    sget p0, Lcom/squareup/messagebar/R$color;->message_bar_background_attention:I

    return p0

    .line 364
    :pswitch_3
    sget p0, Lcom/squareup/messagebar/R$color;->message_bar_background_attention:I

    return p0

    .line 361
    :pswitch_4
    sget p0, Lcom/squareup/messagebar/R$color;->message_bar_background_attention:I

    return p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static getStatusBarColor(Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;)I
    .locals 2

    .line 354
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->level:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_SS_RENEWING_SESSION:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    if-ne p0, v1, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {v0, p0}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->getStatusBarColor(Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;Z)I

    move-result p0

    return p0
.end method

.method public static synthetic lambda$bAWnX0rC2yQllvnw6d2BahHya3w(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;Lflow/History;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->shouldHideMessageBarForHistory(Lflow/History;)Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$subscribeTheMessageAndStatusBarView$2(Ljava/util/Collection;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 400
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 401
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    .line 403
    iget-object v2, v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    if-nez v2, :cond_1

    .line 404
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 407
    :cond_1
    iget-object v2, v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->isSmartReader()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    sget-object v3, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_MIC_PERMISSION_MISSING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    if-ne v2, v3, :cond_0

    .line 409
    :cond_2
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    return-object v0
.end method

.method static synthetic lambda$subscribeTheMessageAndStatusBarView$4(Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 432
    instance-of p0, p0, Lcom/squareup/orderentry/HasChargeButton;

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method private shouldHideMessageBar()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 485
    iget-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-interface {v0}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/messagebar/-$$Lambda$ReaderStatusAndMessageBar$Presenter$bAWnX0rC2yQllvnw6d2BahHya3w;

    invoke-direct {v1, p0}, Lcom/squareup/messagebar/-$$Lambda$ReaderStatusAndMessageBar$Presenter$bAWnX0rC2yQllvnw6d2BahHya3w;-><init>(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;)V

    .line 486
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 487
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method private shouldHideMessageBarForHistory(Lflow/History;)Z
    .locals 1

    .line 491
    invoke-virtual {p1}, Lflow/History;->framesFromTop()Ljava/lang/Iterable;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    .line 493
    invoke-direct {p0, v0}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->shouldHideMessageBarForPath(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method private shouldHideMessageBarForPath(Lcom/squareup/container/ContainerTreeKey;)Z
    .locals 1

    .line 503
    const-class v0, Lcom/squareup/container/spot/ModalBodyScreen;

    invoke-virtual {p1, v0}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 508
    :cond_0
    instance-of v0, p1, Lcom/squareup/container/layer/MayHideStatusBar;

    if-eqz v0, :cond_1

    .line 509
    check-cast p1, Lcom/squareup/container/layer/MayHideStatusBar;

    invoke-interface {p1}, Lcom/squareup/container/layer/MayHideStatusBar;->hideStatusBar()Z

    move-result p1

    return p1

    .line 513
    :cond_1
    const-class v0, Lcom/squareup/container/layer/FullSheet;

    invoke-static {p1, v0}, Lcom/squareup/util/Objects;->isAnnotated(Ljava/lang/Object;Ljava/lang/Class;)Z

    move-result p1

    return p1
.end method

.method private showEventInMessageBar(Lcom/squareup/messagebar/ReaderMessageBarView;Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;)V
    .locals 5

    .line 637
    iget-boolean v0, p2, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;->ignoreColorId:Z

    if-nez v0, :cond_0

    .line 638
    iget v0, p2, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;->colorId:I

    invoke-virtual {p1, v0}, Lcom/squareup/messagebar/ReaderMessageBarView;->setMessageBarColor(I)V

    .line 641
    :cond_0
    iget-object v0, p2, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;->barAnimationType:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    sget-object v1, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;->CYCLE_IN_IF_GONE:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    if-ne v0, v1, :cond_1

    iget-object v0, p2, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;->messageAnimationType:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    sget-object v1, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;->CYCLE_IN_FROM_TOP:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    if-ne v0, v1, :cond_1

    .line 643
    iget-object v0, p2, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;->message:Ljava/lang/CharSequence;

    iget-object p2, p2, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;->batteryLevel:Lcom/squareup/cardreader/BatteryLevel;

    invoke-virtual {p1, v0, p2}, Lcom/squareup/messagebar/ReaderMessageBarView;->showBarShowMessageAnimated(Ljava/lang/CharSequence;Lcom/squareup/cardreader/BatteryLevel;)V

    return-void

    .line 647
    :cond_1
    sget-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$messagebar$ReaderStatusAndMessageBar$BarAnimationType:[I

    iget-object v1, p2, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;->barAnimationType:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    invoke-virtual {v1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x3

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eq v0, v3, :cond_5

    if-eq v0, v2, :cond_4

    if-eq v0, v1, :cond_3

    const/4 v4, 0x4

    if-ne v0, v4, :cond_2

    .line 662
    invoke-virtual {p1}, Lcom/squareup/messagebar/ReaderMessageBarView;->hideBar()V

    goto :goto_0

    .line 665
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Specified invalid message bar animation type"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 659
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/messagebar/ReaderMessageBarView;->showBar()V

    goto :goto_0

    .line 656
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/messagebar/ReaderMessageBarView;->hideBarImmediately()V

    .line 668
    :cond_5
    :goto_0
    sget-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$messagebar$ReaderStatusAndMessageBar$MessageAnimationType:[I

    iget-object v4, p2, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;->messageAnimationType:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    invoke-virtual {v4}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;->ordinal()I

    move-result v4

    aget v0, v0, v4

    if-eq v0, v3, :cond_8

    if-eq v0, v2, :cond_7

    if-ne v0, v1, :cond_6

    .line 675
    iget-object v0, p2, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;->message:Ljava/lang/CharSequence;

    iget-object p2, p2, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;->batteryLevel:Lcom/squareup/cardreader/BatteryLevel;

    invoke-virtual {p1, v0, p2}, Lcom/squareup/messagebar/ReaderMessageBarView;->showMessageAnimated(Ljava/lang/CharSequence;Lcom/squareup/cardreader/BatteryLevel;)V

    goto :goto_1

    .line 678
    :cond_6
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Specified invalid message animation type"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 672
    :cond_7
    iget-object v0, p2, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;->message:Ljava/lang/CharSequence;

    iget-object p2, p2, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;->batteryLevel:Lcom/squareup/cardreader/BatteryLevel;

    invoke-virtual {p1, v0, p2}, Lcom/squareup/messagebar/ReaderMessageBarView;->showMessageImmediately(Ljava/lang/CharSequence;Lcom/squareup/cardreader/BatteryLevel;)V

    :cond_8
    :goto_1
    return-void
.end method

.method private subscribeTheMessageAndStatusBarView(Lcom/squareup/messagebar/ReaderMessageBarView;)Lio/reactivex/disposables/Disposable;
    .locals 10

    .line 396
    iget-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    .line 397
    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerStates()Lrx/Observable;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/messagebar/-$$Lambda$ReaderStatusAndMessageBar$Presenter$t0jJM2fh7PAROiei6N5V5nRH7eg;->INSTANCE:Lcom/squareup/messagebar/-$$Lambda$ReaderStatusAndMessageBar$Presenter$t0jJM2fh7PAROiei6N5V5nRH7eg;

    .line 399
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/messagebar/-$$Lambda$ReaderStatusAndMessageBar$Presenter$E5Odsp77hvVpHeA40Ge2NT0veLA;

    invoke-direct {v1, p0}, Lcom/squareup/messagebar/-$$Lambda$ReaderStatusAndMessageBar$Presenter$E5Odsp77hvVpHeA40Ge2NT0veLA;-><init>(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;)V

    .line 414
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    iget-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 427
    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->interactionMode()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->SALE:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    .line 428
    invoke-interface {v0}, Lcom/squareup/payment/OfflineModeMonitor;->offlineMode()Lio/reactivex/Observable;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->internetState:Lio/reactivex/Observable;

    .line 430
    invoke-direct {p0}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->shouldHideMessageBar()Lio/reactivex/Observable;

    move-result-object v6

    iget-object v7, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->cardSwipedRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    .line 432
    invoke-static {v0}, Lcom/squareup/ui/main/PosContainers;->nextScreen(Lcom/squareup/ui/main/PosContainer;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/messagebar/-$$Lambda$ReaderStatusAndMessageBar$Presenter$l7DpnHihv8u4A4pwaRwBL5IPY_k;->INSTANCE:Lcom/squareup/messagebar/-$$Lambda$ReaderStatusAndMessageBar$Presenter$l7DpnHihv8u4A4pwaRwBL5IPY_k;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 433
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v8

    new-instance v9, Lcom/squareup/messagebar/-$$Lambda$ReaderStatusAndMessageBar$Presenter$TrJ6LlIrVkM8wWftYE96PRbb9q4;

    invoke-direct {v9, p0}, Lcom/squareup/messagebar/-$$Lambda$ReaderStatusAndMessageBar$Presenter$TrJ6LlIrVkM8wWftYE96PRbb9q4;-><init>(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;)V

    .line 396
    invoke-static/range {v2 .. v9}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function7;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/messagebar/-$$Lambda$ReaderStatusAndMessageBar$Presenter$XQDGFzh-CIStecae0sK8GcVz4iE;

    invoke-direct {v1, p0, p1}, Lcom/squareup/messagebar/-$$Lambda$ReaderStatusAndMessageBar$Presenter$XQDGFzh-CIStecae0sK8GcVz4iE;-><init>(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;Lcom/squareup/messagebar/ReaderMessageBarView;)V

    .line 476
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method getMessageForSingleReader(Lcom/squareup/ui/settings/paymentdevices/ReaderState;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;
    .locals 4

    .line 147
    new-instance v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;-><init>(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;)V

    iget-object v1, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    .line 148
    invoke-static {v1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->getStatusBarColor(Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setColorId(I)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/messagebar/ReaderMessageBarView;->HIDDEN_BATTERY_LEVEL:Lcom/squareup/cardreader/BatteryLevel;

    .line 149
    invoke-virtual {v0, v1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setBatteryLevel(Lcom/squareup/cardreader/BatteryLevel;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    move-result-object v0

    .line 151
    sget-object v1, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    iget-object v2, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v2}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/16 v2, 0x63

    packed-switch v1, :pswitch_data_0

    .line 262
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to handle reader event: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    .line 263
    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 258
    :pswitch_0
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/messagebar/R$string;->messagebar_reader_unavailable_and_tap:I

    .line 259
    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    goto/16 :goto_0

    .line 251
    :pswitch_1
    iget-object v1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/messagebar/R$string;->messagebar_reader_ready:I

    .line 252
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/messagebar/R$string;->smart_reader_anonymous_name:I

    .line 253
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 252
    invoke-virtual {v0, v1, v2}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setMessage(Lcom/squareup/phrase/Phrase;Ljava/lang/CharSequence;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    move-result-object v1

    iget-object p1, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 254
    invoke-static {p1}, Lcom/squareup/cardreader/BatteryLevel;->forCardReaderInfo(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/cardreader/BatteryLevel;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setBatteryLevel(Lcom/squareup/cardreader/BatteryLevel;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    goto/16 :goto_0

    .line 248
    :pswitch_2
    invoke-direct {p0}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->getOfflineString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    goto/16 :goto_0

    .line 242
    :pswitch_3
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/messagebar/R$string;->messagebar_reader_ready_low_battery_single:I

    .line 243
    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/messagebar/R$string;->smart_reader_anonymous_name:I

    .line 244
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 243
    invoke-virtual {v0, p1, v1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setMessage(Lcom/squareup/phrase/Phrase;Ljava/lang/CharSequence;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    goto/16 :goto_0

    .line 237
    :pswitch_4
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/messagebar/R$string;->messagebar_reader_ss_connecting:I

    .line 238
    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    goto/16 :goto_0

    .line 233
    :pswitch_5
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/messagebar/R$string;->messagebar_reader_connecting:I

    .line 234
    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    goto/16 :goto_0

    .line 224
    :pswitch_6
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/messagebar/R$string;->messagebar_reader_not_ready:I

    .line 225
    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/messagebar/R$string;->smart_reader_anonymous_name:I

    .line 226
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 225
    invoke-virtual {v0, p1, v1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setMessage(Lcom/squareup/phrase/Phrase;Ljava/lang/CharSequence;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    goto/16 :goto_0

    .line 216
    :pswitch_7
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/messagebar/R$string;->key_injection_failed:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    goto/16 :goto_0

    .line 211
    :pswitch_8
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/messagebar/R$string;->messagebar_missing_microphone_permission:I

    .line 212
    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    goto/16 :goto_0

    .line 189
    :pswitch_9
    instance-of v1, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;

    if-eqz v1, :cond_0

    .line 190
    check-cast p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;

    .line 195
    iget p1, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;->totalPercentComplete:I

    .line 196
    invoke-static {v2, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 198
    iget-object v1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/cardreader/ui/R$string;->messagebar_reader_fwup_blocking_progress:I

    .line 199
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/messagebar/R$string;->smart_reader_anonymous_name:I

    .line 201
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 199
    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setMessage(Lcom/squareup/phrase/Phrase;Ljava/lang/CharSequence;I)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    goto/16 :goto_0

    .line 203
    :cond_0
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/messagebar/R$string;->messagebar_reader_fwup_blocking_progress_no_details:I

    .line 205
    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/messagebar/R$string;->smart_reader_anonymous_name:I

    .line 206
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 204
    invoke-virtual {v0, p1, v1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setMessage(Lcom/squareup/phrase/Phrase;Ljava/lang/CharSequence;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    goto/16 :goto_0

    .line 164
    :pswitch_a
    instance-of v1, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;

    if-eqz v1, :cond_2

    .line 165
    check-cast p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;

    .line 166
    iget v1, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;->totalPercentComplete:I

    .line 167
    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 169
    iget p1, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;->currentPercentComplete:I

    const/16 v2, 0x5a

    if-gt p1, v2, :cond_1

    .line 170
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/cardreader/ui/R$string;->messagebar_reader_fwup_blocking_progress:I

    .line 171
    invoke-interface {p1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v2, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/messagebar/R$string;->smart_reader_anonymous_name:I

    .line 173
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 171
    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setMessage(Lcom/squareup/phrase/Phrase;Ljava/lang/CharSequence;I)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    goto :goto_0

    .line 176
    :cond_1
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/messagebar/R$string;->messagebar_reader_fwup_rebooting_will_reboot:I

    .line 177
    invoke-interface {p1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v2, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/messagebar/R$string;->smart_reader_anonymous_name:I

    .line 178
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 177
    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setMessage(Lcom/squareup/phrase/Phrase;Ljava/lang/CharSequence;I)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    goto :goto_0

    .line 182
    :cond_2
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/messagebar/R$string;->messagebar_reader_fwup_blocking_progress_no_details:I

    .line 184
    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/messagebar/R$string;->smart_reader_anonymous_name:I

    .line 185
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 183
    invoke-virtual {v0, p1, v1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setMessage(Lcom/squareup/phrase/Phrase;Ljava/lang/CharSequence;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    goto :goto_0

    .line 159
    :pswitch_b
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/messagebar/R$string;->messagebar_reader_fwup_rebooting:I

    .line 160
    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/messagebar/R$string;->smart_reader_anonymous_name:I

    .line 161
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 160
    invoke-virtual {v0, p1, v1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setMessage(Lcom/squareup/phrase/Phrase;Ljava/lang/CharSequence;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    goto :goto_0

    .line 156
    :pswitch_c
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/messagebar/R$string;->key_injection_started:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    goto :goto_0

    .line 153
    :pswitch_d
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/messagebar/R$string;->messagebar_reader_checking_manifest_updates:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    :goto_0
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public synthetic lambda$onEnterScope$0$ReaderStatusAndMessageBar$Presenter(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 127
    invoke-virtual {p0}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->onCartChanged()V

    return-void
.end method

.method public synthetic lambda$onLoad$1$ReaderStatusAndMessageBar$Presenter(Lcom/squareup/messagebar/ReaderMessageBarView;)Lio/reactivex/disposables/Disposable;
    .locals 0

    .line 137
    invoke-direct {p0, p1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->subscribeTheMessageAndStatusBarView(Lcom/squareup/messagebar/ReaderMessageBarView;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$subscribeTheMessageAndStatusBarView$3$ReaderStatusAndMessageBar$Presenter(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 415
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 417
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    .line 418
    iget-object v3, v2, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    if-eqz v3, :cond_0

    iget-object v3, v2, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v3}, Lcom/squareup/cardreader/CardReaderInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 419
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 423
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v1, v3, :cond_2

    .line 424
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    iget-object v0, v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_MIC_PERMISSION_MISSING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    if-ne v0, v1, :cond_2

    const/4 v2, 0x1

    :cond_2
    iput-boolean v2, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->oneReaderConnectedAndMissingMicPermission:Z

    return-object p1
.end method

.method public synthetic lambda$subscribeTheMessageAndStatusBarView$5$ReaderStatusAndMessageBar$Presenter(Ljava/util/List;Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;Ljava/lang/Boolean;Lcom/squareup/connectivity/InternetState;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 438
    invoke-virtual {p5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p5

    if-eqz p5, :cond_0

    .line 439
    new-instance p1, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;-><init>(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;)V

    sget p2, Lcom/squareup/messagebar/R$color;->message_bar_background_ready:I

    .line 440
    invoke-virtual {p1, p2}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setColorId(I)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    move-result-object p1

    sget-object p2, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;->HIDE_IMMEDIATELY:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    .line 441
    invoke-virtual {p1, p2}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->barAnimationType(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    move-result-object p1

    .line 442
    invoke-virtual {p1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->build()Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;

    move-result-object p1

    return-object p1

    .line 447
    :cond_0
    invoke-virtual {p7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p5

    if-eqz p5, :cond_1

    iget-object p5, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p5}, Lcom/squareup/payment/Transaction;->paymentIsBelowMinimum()Z

    move-result p5

    if-nez p5, :cond_1

    const/4 p5, 0x1

    goto :goto_0

    :cond_1
    const/4 p5, 0x0

    .line 446
    :goto_0
    invoke-direct {p0, p1, p5}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->getMessageForSingleOrMultipleReaders(Ljava/util/Collection;Z)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    move-result-object p1

    .line 451
    sget-object p5, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->EDIT:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    if-ne p5, p2, :cond_2

    .line 452
    sget p2, Lcom/squareup/messagebar/R$color;->message_bar_background_ready:I

    invoke-static {p1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->access$100(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;)I

    move-result p5

    if-ne p2, p5, :cond_2

    .line 453
    invoke-static {p1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->access$200(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;)Z

    move-result p2

    if-nez p2, :cond_2

    .line 454
    sget p2, Lcom/squareup/messagebar/R$color;->message_bar_background_editing:I

    invoke-virtual {p1, p2}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setColorId(I)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    .line 459
    :cond_2
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_4

    .line 460
    iget-object p2, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->res:Lcom/squareup/util/Res;

    iget-object p3, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    .line 461
    invoke-virtual {p3}, Lcom/squareup/cardreader/CardReaderHub;->hasCardReader()Z

    move-result p3

    if-eqz p3, :cond_3

    sget p3, Lcom/squareup/cardreader/ui/R$string;->messagebar_reader_offline_swipe_only:I

    goto :goto_1

    :cond_3
    sget p3, Lcom/squareup/messagebar/R$string;->messagebar_reader_offline_mode:I

    :goto_1
    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    move-result-object p2

    sget p3, Lcom/squareup/messagebar/R$color;->message_bar_background_offline_mode:I

    .line 464
    invoke-virtual {p2, p3}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setColorId(I)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    move-result-object p2

    sget-object p3, Lcom/squareup/messagebar/ReaderMessageBarView;->HIDDEN_BATTERY_LEVEL:Lcom/squareup/cardreader/BatteryLevel;

    .line 465
    invoke-virtual {p2, p3}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setBatteryLevel(Lcom/squareup/cardreader/BatteryLevel;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    goto :goto_2

    .line 466
    :cond_4
    sget-object p2, Lcom/squareup/connectivity/InternetState;->NOT_CONNECTED:Lcom/squareup/connectivity/InternetState;

    if-ne p4, p2, :cond_5

    .line 467
    invoke-static {p1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->access$300(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    move-result-object p2

    sget-object p3, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;->IGNORE_MESSAGE:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    if-eq p2, p3, :cond_5

    .line 469
    invoke-direct {p0}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->getOfflineString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    move-result-object p2

    .line 470
    invoke-direct {p0}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->getOfflineColor()I

    move-result p3

    invoke-virtual {p2, p3}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setColorId(I)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    move-result-object p2

    sget-object p3, Lcom/squareup/messagebar/ReaderMessageBarView;->HIDDEN_BATTERY_LEVEL:Lcom/squareup/cardreader/BatteryLevel;

    .line 471
    invoke-virtual {p2, p3}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setBatteryLevel(Lcom/squareup/cardreader/BatteryLevel;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    .line 474
    :cond_5
    :goto_2
    invoke-virtual {p1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->build()Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$subscribeTheMessageAndStatusBarView$6$ReaderStatusAndMessageBar$Presenter(Lcom/squareup/messagebar/ReaderMessageBarView;Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 477
    iget-boolean v0, p2, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;->ignoreColorId:Z

    if-nez v0, :cond_0

    .line 478
    iget-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->statusBarHelper:Lcom/squareup/ui/StatusBarHelper;

    iget v1, p2, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;->colorId:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/StatusBarHelper;->setStatusBarColor(I)V

    .line 480
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->showEventInMessageBar(Lcom/squareup/messagebar/ReaderMessageBarView;Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;)V

    return-void
.end method

.method public onBarClicked()V
    .locals 1

    .line 683
    iget-boolean v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->oneReaderConnectedAndMissingMicPermission:Z

    if-eqz v0, :cond_0

    .line 684
    iget-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->cardReaderScreensGateway:Lcom/squareup/ui/main/ReaderStatusMonitor;

    invoke-virtual {v0}, Lcom/squareup/ui/main/ReaderStatusMonitor;->showAudioPermissionCard()V

    goto :goto_0

    .line 686
    :cond_0
    iget-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->cardReaderScreensGateway:Lcom/squareup/ui/main/ReaderStatusMonitor;

    invoke-virtual {v0}, Lcom/squareup/ui/main/ReaderStatusMonitor;->showCardReaderCard()V

    :goto_0
    return-void
.end method

.method onCartChanged()V
    .locals 2

    .line 143
    iget-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->cardSwipedRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->hasCard()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 127
    iget-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->badBus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/messagebar/-$$Lambda$ReaderStatusAndMessageBar$Presenter$qttolI8Ma-9zKVxCb5EhqMFSI-4;

    invoke-direct {v1, p0}, Lcom/squareup/messagebar/-$$Lambda$ReaderStatusAndMessageBar$Presenter$qttolI8Ma-9zKVxCb5EhqMFSI-4;-><init>(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 131
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 132
    invoke-virtual {p0}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/messagebar/ReaderMessageBarView;

    .line 135
    iget-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->statusBarHelper:Lcom/squareup/ui/StatusBarHelper;

    invoke-virtual {v0}, Lcom/squareup/ui/StatusBarHelper;->getStatusBarHeight()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/messagebar/ReaderMessageBarView;->initialize(I)V

    .line 137
    new-instance v0, Lcom/squareup/messagebar/-$$Lambda$ReaderStatusAndMessageBar$Presenter$kM0IS4bF1J5N1OXJNlaOCIfoOE0;

    invoke-direct {v0, p0, p1}, Lcom/squareup/messagebar/-$$Lambda$ReaderStatusAndMessageBar$Presenter$kM0IS4bF1J5N1OXJNlaOCIfoOE0;-><init>(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;Lcom/squareup/messagebar/ReaderMessageBarView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
