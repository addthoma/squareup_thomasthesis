.class public Lcom/squareup/messagebar/ReaderMessageBarView;
.super Landroid/widget/FrameLayout;
.source "ReaderMessageBarView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/messagebar/ReaderMessageBarView$BatteryPositionAdjustment;,
        Lcom/squareup/messagebar/ReaderMessageBarView$Component;
    }
.end annotation


# static fields
.field private static final ANIMATION_DURATION_MS:I = 0x258

.field public static final HIDDEN_BATTERY_LEVEL:Lcom/squareup/cardreader/BatteryLevel;

.field private static final OPAQUE_ALPHA:F = 1.0f

.field private static final TRANSPARENT_ALPHA:F


# instance fields
.field private currentBatteryLevel:Lcom/squareup/cardreader/BatteryLevel;

.field private currentTextView:Landroid/widget/TextView;

.field private isTextAnimating:Z

.field private offscreenBottomYPosition:I

.field private offscreenTopYPosition:I

.field private onscreenYPosition:I

.field presenter:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private previousTextView:Landroid/widget/TextView;

.field private viewHeight:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 36
    sget-object v0, Lcom/squareup/cardreader/BatteryLevel;->UNKNOWN:Lcom/squareup/cardreader/BatteryLevel;

    sput-object v0, Lcom/squareup/messagebar/ReaderMessageBarView;->HIDDEN_BATTERY_LEVEL:Lcom/squareup/cardreader/BatteryLevel;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 54
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    const-class p2, Lcom/squareup/messagebar/ReaderMessageBarView$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/messagebar/ReaderMessageBarView$Component;

    invoke-interface {p1, p0}, Lcom/squareup/messagebar/ReaderMessageBarView$Component;->inject(Lcom/squareup/messagebar/ReaderMessageBarView;)V

    .line 57
    sget-object p1, Lcom/squareup/messagebar/ReaderMessageBarView;->HIDDEN_BATTERY_LEVEL:Lcom/squareup/cardreader/BatteryLevel;

    iput-object p1, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->currentBatteryLevel:Lcom/squareup/cardreader/BatteryLevel;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/messagebar/ReaderMessageBarView;)I
    .locals 0

    .line 32
    iget p0, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->offscreenTopYPosition:I

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/messagebar/ReaderMessageBarView;)Landroid/widget/TextView;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->previousTextView:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$202(Lcom/squareup/messagebar/ReaderMessageBarView;Z)Z
    .locals 0

    .line 32
    iput-boolean p1, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->isTextAnimating:Z

    return p1
.end method

.method private bindViews()V
    .locals 1

    .line 284
    sget v0, Lcom/squareup/messagebar/R$id;->reader_message_bar_previous_text_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->previousTextView:Landroid/widget/TextView;

    .line 285
    sget v0, Lcom/squareup/messagebar/R$id;->reader_message_bar_current_text_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->currentTextView:Landroid/widget/TextView;

    return-void
.end method

.method private isVisible()Z
    .locals 1

    .line 228
    invoke-virtual {p0}, Lcom/squareup/messagebar/ReaderMessageBarView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private setMessage(Ljava/lang/CharSequence;Lcom/squareup/cardreader/BatteryLevel;Landroid/widget/TextView;)V
    .locals 4

    .line 238
    sget-object v0, Lcom/squareup/messagebar/ReaderMessageBarView;->HIDDEN_BATTERY_LEVEL:Lcom/squareup/cardreader/BatteryLevel;

    const/4 v1, 0x0

    if-eq p2, v0, :cond_0

    .line 239
    invoke-virtual {p0}, Lcom/squareup/messagebar/ReaderMessageBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 240
    new-instance v2, Lcom/squareup/messagebar/ReaderMessageBarView$BatteryPositionAdjustment;

    sget v3, Lcom/squareup/marin/R$color;->marin_white:I

    .line 241
    invoke-static {p2, v3, v0}, Lcom/squareup/cardreader/BatteryLevelResources;->buildBatteryDrawableTiny(Lcom/squareup/cardreader/BatteryLevel;ILandroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    sget v3, Lcom/squareup/messagebar/R$dimen;->reader_message_bar_battery_vertical_offset:I

    .line 242
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    invoke-direct {v2, p2, v0}, Lcom/squareup/messagebar/ReaderMessageBarView$BatteryPositionAdjustment;-><init>(Landroid/graphics/drawable/Drawable;I)V

    goto :goto_0

    :cond_0
    move-object v2, v1

    .line 244
    :goto_0
    invoke-virtual {p3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 245
    invoke-virtual {p3, v1, v1, v2, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method private setMessageOnCurrentTextView(Ljava/lang/CharSequence;Lcom/squareup/cardreader/BatteryLevel;)V
    .locals 1

    .line 232
    iput-object p2, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->currentBatteryLevel:Lcom/squareup/cardreader/BatteryLevel;

    .line 233
    iget-object p2, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->currentBatteryLevel:Lcom/squareup/cardreader/BatteryLevel;

    iget-object v0, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->currentTextView:Landroid/widget/TextView;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/messagebar/ReaderMessageBarView;->setMessage(Ljava/lang/CharSequence;Lcom/squareup/cardreader/BatteryLevel;Landroid/widget/TextView;)V

    return-void
.end method

.method private showBar(Ljava/lang/CharSequence;Lcom/squareup/cardreader/BatteryLevel;)V
    .locals 2

    const/4 v0, 0x0

    .line 125
    invoke-virtual {p0, v0}, Lcom/squareup/messagebar/ReaderMessageBarView;->setVisibility(I)V

    .line 126
    iget-object v0, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->currentTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 127
    iget-object v0, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->currentTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->onscreenYPosition:I

    int-to-float v1, v1

    .line 128
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 129
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    .line 130
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    .line 131
    invoke-direct {p0, p1, p2}, Lcom/squareup/messagebar/ReaderMessageBarView;->setMessageOnCurrentTextView(Ljava/lang/CharSequence;Lcom/squareup/cardreader/BatteryLevel;)V

    return-void
.end method


# virtual methods
.method protected getCurrentMessage()Ljava/lang/CharSequence;
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->currentTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method hideBar()V
    .locals 3

    .line 148
    iget-object v0, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->currentTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 149
    iget-object v0, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->currentTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    .line 150
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0x258

    .line 151
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->offscreenTopYPosition:I

    int-to-float v1, v1

    .line 152
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    const/16 v0, 0x8

    .line 153
    invoke-virtual {p0, v0}, Lcom/squareup/messagebar/ReaderMessageBarView;->setVisibility(I)V

    return-void
.end method

.method hideBarImmediately()V
    .locals 2

    .line 109
    iget-object v0, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->currentTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 110
    iget-object v0, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->currentTextView:Landroid/widget/TextView;

    iget v1, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->offscreenTopYPosition:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTranslationY(F)V

    const/16 v0, 0x8

    .line 111
    invoke-virtual {p0, v0}, Lcom/squareup/messagebar/ReaderMessageBarView;->setVisibility(I)V

    return-void
.end method

.method initialize(I)V
    .locals 2

    .line 83
    invoke-virtual {p0}, Lcom/squareup/messagebar/ReaderMessageBarView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, -0x1

    .line 84
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 85
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 86
    invoke-virtual {p0, v0}, Lcom/squareup/messagebar/ReaderMessageBarView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 87
    invoke-virtual {p0, p1}, Lcom/squareup/messagebar/ReaderMessageBarView;->setMinimumHeight(I)V

    .line 88
    iget-object v0, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->currentTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setMinimumHeight(I)V

    .line 89
    iget-object v0, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->previousTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setMinimumHeight(I)V

    .line 90
    iput p1, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->viewHeight:I

    const/4 p1, 0x0

    .line 91
    iput p1, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->onscreenYPosition:I

    .line 92
    iget v0, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->viewHeight:I

    neg-int v1, v0

    iput v1, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->offscreenTopYPosition:I

    .line 93
    iput v0, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->offscreenBottomYPosition:I

    .line 96
    iput-boolean p1, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->isTextAnimating:Z

    .line 98
    iget p1, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->onscreenYPosition:I

    int-to-float p1, p1

    invoke-virtual {p0, p1}, Lcom/squareup/messagebar/ReaderMessageBarView;->setTranslationY(F)V

    .line 100
    invoke-virtual {p0}, Lcom/squareup/messagebar/ReaderMessageBarView;->hideBarImmediately()V

    .line 101
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->currentTextView:Landroid/widget/TextView;

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setAlpha(F)V

    .line 103
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->previousTextView:Landroid/widget/TextView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 104
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->previousTextView:Landroid/widget/TextView;

    iget v0, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->offscreenTopYPosition:I

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTranslationY(F)V

    .line 105
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->previousTextView:Landroid/widget/TextView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setAlpha(F)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 61
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 62
    invoke-direct {p0}, Lcom/squareup/messagebar/ReaderMessageBarView;->bindViews()V

    .line 63
    new-instance v0, Lcom/squareup/messagebar/ReaderMessageBarView$1;

    invoke-direct {v0, p0}, Lcom/squareup/messagebar/ReaderMessageBarView$1;-><init>(Lcom/squareup/messagebar/ReaderMessageBarView;)V

    invoke-virtual {p0, v0}, Lcom/squareup/messagebar/ReaderMessageBarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->presenter:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->presenter:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->dropView(Ljava/lang/Object;)V

    .line 73
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method setMessageBarColor(I)V
    .locals 0

    .line 116
    invoke-virtual {p0, p1}, Lcom/squareup/messagebar/ReaderMessageBarView;->setBackgroundResource(I)V

    return-void
.end method

.method showBar()V
    .locals 2

    .line 144
    invoke-virtual {p0}, Lcom/squareup/messagebar/ReaderMessageBarView;->getCurrentMessage()Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->currentBatteryLevel:Lcom/squareup/cardreader/BatteryLevel;

    invoke-virtual {p0, v0, v1}, Lcom/squareup/messagebar/ReaderMessageBarView;->showBarShowMessageAnimated(Ljava/lang/CharSequence;Lcom/squareup/cardreader/BatteryLevel;)V

    return-void
.end method

.method showBarShowMessageAnimated(Ljava/lang/CharSequence;Lcom/squareup/cardreader/BatteryLevel;)V
    .locals 1

    .line 135
    invoke-direct {p0}, Lcom/squareup/messagebar/ReaderMessageBarView;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 136
    invoke-direct {p0, p1, p2}, Lcom/squareup/messagebar/ReaderMessageBarView;->showBar(Ljava/lang/CharSequence;Lcom/squareup/cardreader/BatteryLevel;)V

    return-void

    .line 140
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/messagebar/ReaderMessageBarView;->showMessageAnimated(Ljava/lang/CharSequence;Lcom/squareup/cardreader/BatteryLevel;)V

    return-void
.end method

.method showMessageAnimated(Ljava/lang/CharSequence;Lcom/squareup/cardreader/BatteryLevel;)V
    .locals 4

    .line 170
    iget-object v0, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->currentTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 175
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->isTextAnimating:Z

    if-eqz v0, :cond_1

    .line 176
    invoke-direct {p0, p1, p2}, Lcom/squareup/messagebar/ReaderMessageBarView;->setMessageOnCurrentTextView(Ljava/lang/CharSequence;Lcom/squareup/cardreader/BatteryLevel;)V

    return-void

    :cond_1
    const/4 v0, 0x1

    .line 180
    iput-boolean v0, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->isTextAnimating:Z

    .line 183
    iget-object v0, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->currentTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->currentBatteryLevel:Lcom/squareup/cardreader/BatteryLevel;

    iget-object v2, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->previousTextView:Landroid/widget/TextView;

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/messagebar/ReaderMessageBarView;->setMessage(Ljava/lang/CharSequence;Lcom/squareup/cardreader/BatteryLevel;Landroid/widget/TextView;)V

    .line 184
    invoke-direct {p0, p1, p2}, Lcom/squareup/messagebar/ReaderMessageBarView;->setMessageOnCurrentTextView(Ljava/lang/CharSequence;Lcom/squareup/cardreader/BatteryLevel;)V

    .line 187
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->previousTextView:Landroid/widget/TextView;

    iget p2, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->onscreenYPosition:I

    int-to-float p2, p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTranslationY(F)V

    .line 188
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->previousTextView:Landroid/widget/TextView;

    const/high16 p2, 0x3f800000    # 1.0f

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setAlpha(F)V

    .line 189
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->previousTextView:Landroid/widget/TextView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 191
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->previousTextView:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    .line 192
    invoke-virtual {p1, v0}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    const-wide/16 v0, 0x258

    .line 193
    invoke-virtual {p1, v0, v1}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    const/4 v2, 0x0

    .line 194
    invoke-virtual {p1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    iget v3, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->offscreenBottomYPosition:I

    int-to-float v3, v3

    .line 195
    invoke-virtual {p1, v3}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    new-instance v3, Lcom/squareup/messagebar/ReaderMessageBarView$2;

    invoke-direct {v3, p0}, Lcom/squareup/messagebar/ReaderMessageBarView$2;-><init>(Lcom/squareup/messagebar/ReaderMessageBarView;)V

    .line 196
    invoke-virtual {p1, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 205
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->currentTextView:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 206
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->currentTextView:Landroid/widget/TextView;

    iget v3, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->offscreenTopYPosition:I

    int-to-float v3, v3

    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setTranslationY(F)V

    .line 207
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->currentTextView:Landroid/widget/TextView;

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setAlpha(F)V

    .line 209
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->currentTextView:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    .line 210
    invoke-virtual {p1, v2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 211
    invoke-virtual {p1, v0, v1}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 212
    invoke-virtual {p1, p2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    iget p2, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->onscreenYPosition:I

    int-to-float p2, p2

    .line 213
    invoke-virtual {p1, p2}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    new-instance p2, Lcom/squareup/messagebar/ReaderMessageBarView$3;

    invoke-direct {p2, p0}, Lcom/squareup/messagebar/ReaderMessageBarView$3;-><init>(Lcom/squareup/messagebar/ReaderMessageBarView;)V

    .line 214
    invoke-virtual {p1, p2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    return-void
.end method

.method showMessageImmediately(Ljava/lang/CharSequence;Lcom/squareup/cardreader/BatteryLevel;)V
    .locals 3

    .line 158
    sget-object v0, Lcom/squareup/messagebar/ReaderMessageBarView;->HIDDEN_BATTERY_LEVEL:Lcom/squareup/cardreader/BatteryLevel;

    iget-object v1, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->previousTextView:Landroid/widget/TextView;

    const-string v2, ""

    invoke-direct {p0, v2, v0, v1}, Lcom/squareup/messagebar/ReaderMessageBarView;->setMessage(Ljava/lang/CharSequence;Lcom/squareup/cardreader/BatteryLevel;Landroid/widget/TextView;)V

    .line 159
    iget-object v0, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->previousTextView:Landroid/widget/TextView;

    iget v1, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->offscreenTopYPosition:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTranslationY(F)V

    .line 162
    invoke-direct {p0, p1, p2}, Lcom/squareup/messagebar/ReaderMessageBarView;->setMessageOnCurrentTextView(Ljava/lang/CharSequence;Lcom/squareup/cardreader/BatteryLevel;)V

    .line 163
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->currentTextView:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 164
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->currentTextView:Landroid/widget/TextView;

    iget p2, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->onscreenYPosition:I

    int-to-float p2, p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTranslationY(F)V

    .line 165
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderMessageBarView;->currentTextView:Landroid/widget/TextView;

    const/high16 p2, 0x3f800000    # 1.0f

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setAlpha(F)V

    return-void
.end method
