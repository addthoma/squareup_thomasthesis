.class public final Lcom/squareup/messagebar/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/messagebar/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final key_injection_failed:I = 0x7f120e79

.field public static final key_injection_started:I = 0x7f120e7a

.field public static final messagebar_missing_microphone_permission:I = 0x7f120fc6

.field public static final messagebar_reader_checking_manifest_updates:I = 0x7f120fc7

.field public static final messagebar_reader_connecting:I = 0x7f120fc8

.field public static final messagebar_reader_fwup_blocking_progress_no_details:I = 0x7f120fca

.field public static final messagebar_reader_fwup_rebooting:I = 0x7f120fcb

.field public static final messagebar_reader_fwup_rebooting_will_reboot:I = 0x7f120fcc

.field public static final messagebar_reader_not_ready:I = 0x7f120fcd

.field public static final messagebar_reader_not_ready_and_tap:I = 0x7f120fce

.field public static final messagebar_reader_offline:I = 0x7f120fcf

.field public static final messagebar_reader_offline_mode:I = 0x7f120fd0

.field public static final messagebar_reader_ready:I = 0x7f120fd2

.field public static final messagebar_reader_ready_card_inserted:I = 0x7f120fd3

.field public static final messagebar_reader_ready_card_inserted_press_charge:I = 0x7f120fd4

.field public static final messagebar_reader_ready_card_swiped:I = 0x7f120fd5

.field public static final messagebar_reader_ready_card_swiped_press_charge:I = 0x7f120fd6

.field public static final messagebar_reader_ready_low_battery_plural:I = 0x7f120fd7

.field public static final messagebar_reader_ready_low_battery_single:I = 0x7f120fd8

.field public static final messagebar_reader_ss_connecting:I = 0x7f120fd9

.field public static final messagebar_reader_unavailable:I = 0x7f120fda

.field public static final messagebar_reader_unavailable_and_tap:I = 0x7f120fdb

.field public static final messagebar_readers_connecting_plural:I = 0x7f120fdc

.field public static final messagebar_readers_connecting_single:I = 0x7f120fdd

.field public static final messagebar_readers_not_ready_plural:I = 0x7f120fde

.field public static final messagebar_readers_not_ready_single:I = 0x7f120fdf

.field public static final messagebar_readers_ready_plural:I = 0x7f120fe0

.field public static final messagebar_readers_ready_single:I = 0x7f120fe1

.field public static final messagebar_readers_updating_plural:I = 0x7f120fe2

.field public static final messagebar_readers_updating_single:I = 0x7f120fe3

.field public static final smart_reader_anonymous_name:I = 0x7f121824

.field public static final smart_reader_chip_name:I = 0x7f121825

.field public static final smart_reader_updating:I = 0x7f121827


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
