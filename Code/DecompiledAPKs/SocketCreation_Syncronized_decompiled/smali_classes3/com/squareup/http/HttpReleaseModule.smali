.class public final Lcom/squareup/http/HttpReleaseModule;
.super Ljava/lang/Object;
.source "HttpReleaseModule.kt"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/http/HttpModule;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J#\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0011\u0010\u0007\u001a\r\u0012\t\u0012\u00070\t\u00a2\u0006\u0002\u0008\n0\u0008H\u0001\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/http/HttpReleaseModule;",
        "",
        "()V",
        "provideOkHttpClientBuilder",
        "Lokhttp3/OkHttpClient$Builder;",
        "application",
        "Landroid/app/Application;",
        "interceptors",
        "",
        "Lokhttp3/Interceptor;",
        "Lkotlin/jvm/JvmSuppressWildcards;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/http/HttpReleaseModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 11
    new-instance v0, Lcom/squareup/http/HttpReleaseModule;

    invoke-direct {v0}, Lcom/squareup/http/HttpReleaseModule;-><init>()V

    sput-object v0, Lcom/squareup/http/HttpReleaseModule;->INSTANCE:Lcom/squareup/http/HttpReleaseModule;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final provideOkHttpClientBuilder(Landroid/app/Application;Ljava/util/Set;)Lokhttp3/OkHttpClient$Builder;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Ljava/util/Set<",
            "Lokhttp3/Interceptor;",
            ">;)",
            "Lokhttp3/OkHttpClient$Builder;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "application"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interceptors"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-static {p0, p1}, Lcom/squareup/http/HttpClientBuilder;->createOkHttpClient(Landroid/app/Application;Ljava/util/Set;)Lokhttp3/OkHttpClient$Builder;

    move-result-object p0

    const-string p1, "createOkHttpClient(application, interceptors)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
