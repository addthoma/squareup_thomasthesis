.class public final Lcom/squareup/http/useragent/UserAgentModule_ProvideUserAgentFactory;
.super Ljava/lang/Object;
.source "UserAgentModule_ProvideUserAgentFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final userAgentProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/useragent/UserAgentProvider;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/useragent/UserAgentProvider;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/http/useragent/UserAgentModule_ProvideUserAgentFactory;->userAgentProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/http/useragent/UserAgentModule_ProvideUserAgentFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/useragent/UserAgentProvider;",
            ">;)",
            "Lcom/squareup/http/useragent/UserAgentModule_ProvideUserAgentFactory;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/http/useragent/UserAgentModule_ProvideUserAgentFactory;

    invoke-direct {v0, p0}, Lcom/squareup/http/useragent/UserAgentModule_ProvideUserAgentFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideUserAgent(Lcom/squareup/http/useragent/UserAgentProvider;)Ljava/lang/String;
    .locals 1

    .line 34
    invoke-static {p0}, Lcom/squareup/http/useragent/UserAgentModule;->provideUserAgent(Lcom/squareup/http/useragent/UserAgentProvider;)Ljava/lang/String;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/http/useragent/UserAgentModule_ProvideUserAgentFactory;->get()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/lang/String;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/http/useragent/UserAgentModule_ProvideUserAgentFactory;->userAgentProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/http/useragent/UserAgentProvider;

    invoke-static {v0}, Lcom/squareup/http/useragent/UserAgentModule_ProvideUserAgentFactory;->provideUserAgent(Lcom/squareup/http/useragent/UserAgentProvider;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
