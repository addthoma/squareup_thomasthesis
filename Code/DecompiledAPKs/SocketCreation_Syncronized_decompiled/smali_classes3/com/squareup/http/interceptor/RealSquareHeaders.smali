.class public Lcom/squareup/http/interceptor/RealSquareHeaders;
.super Ljava/lang/Object;
.source "RealSquareHeaders.java"

# interfaces
.implements Lcom/squareup/http/SquareHeaders;


# static fields
.field private static final UNPRINTABLE_REPLACEMENT:Ljava/lang/String; = "?"


# instance fields
.field private final adIdCache:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final androidIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final androidSerialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final application:Landroid/app/Application;

.field private final clock:Lcom/squareup/util/Clock;

.field final connectivityProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/net/ConnectivityManager;",
            ">;"
        }
    .end annotation
.end field

.field final deviceId:Lcom/squareup/settings/DeviceIdProvider;

.field private final installationIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final locationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation
.end field

.field private final macAddressProvider:Lcom/squareup/http/interceptor/MacAddressProvider;

.field private md5Mac:Ljava/lang/String;

.field private final onboardRedirectSettings:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final persistentDeviceName:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private sha1Mac:Ljava/lang/String;

.field private sim:Ljava/lang/String;

.field private simWithoutSub:Ljava/lang/String;

.field private final speleoIdGenerator:Lcom/squareup/http/interceptor/SpeleoIdGenerator;

.field private subscriberId:Ljava/lang/String;

.field private final telephony:Lcom/squareup/http/interceptor/Telephony;

.field final telephonyManager:Landroid/telephony/TelephonyManager;

.field private final userAgent:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Landroid/app/Application;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/http/interceptor/MacAddressProvider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Landroid/telephony/TelephonyManager;Lcom/squareup/http/interceptor/Telephony;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/settings/DeviceIdProvider;Lcom/squareup/util/Clock;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/http/interceptor/SpeleoIdGenerator;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/http/interceptor/MacAddressProvider;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;",
            "Landroid/telephony/TelephonyManager;",
            "Lcom/squareup/http/interceptor/Telephony;",
            "Ljavax/inject/Provider<",
            "Landroid/net/ConnectivityManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/settings/DeviceIdProvider;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/http/interceptor/SpeleoIdGenerator;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 89
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders;->application:Landroid/app/Application;

    move-object v1, p2

    .line 90
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders;->androidIdProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 91
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders;->androidSerialProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 92
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders;->installationIdProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 93
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders;->macAddressProvider:Lcom/squareup/http/interceptor/MacAddressProvider;

    move-object v1, p6

    .line 94
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders;->persistentDeviceName:Ljavax/inject/Provider;

    move-object v1, p7

    .line 95
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders;->userAgent:Ljavax/inject/Provider;

    move-object v1, p8

    .line 96
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders;->locationProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 97
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders;->telephonyManager:Landroid/telephony/TelephonyManager;

    move-object v1, p10

    .line 98
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders;->telephony:Lcom/squareup/http/interceptor/Telephony;

    move-object v1, p11

    .line 99
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders;->connectivityProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 100
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders;->localeProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 101
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders;->deviceId:Lcom/squareup/settings/DeviceIdProvider;

    move-object/from16 v1, p14

    .line 102
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders;->clock:Lcom/squareup/util/Clock;

    move-object/from16 v1, p15

    .line 103
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders;->onboardRedirectSettings:Lcom/squareup/settings/LocalSetting;

    move-object/from16 v1, p16

    .line 104
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders;->adIdCache:Lcom/squareup/settings/LocalSetting;

    move-object/from16 v1, p17

    .line 105
    iput-object v1, v0, Lcom/squareup/http/interceptor/RealSquareHeaders;->speleoIdGenerator:Lcom/squareup/http/interceptor/SpeleoIdGenerator;

    return-void
.end method

.method private static addIfNotNull(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Header;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    if-eqz p2, :cond_0

    .line 231
    new-instance v0, Lcom/squareup/protos/common/Header$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/Header$Builder;-><init>()V

    invoke-virtual {v0, p1}, Lcom/squareup/protos/common/Header$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/common/Header$Builder;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/squareup/protos/common/Header$Builder;->value(Ljava/lang/String;)Lcom/squareup/protos/common/Header$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/protos/common/Header$Builder;->build()Lcom/squareup/protos/common/Header;

    move-result-object p1

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private checkMacAddress()V
    .locals 2

    .line 221
    iget-object v0, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->md5Mac:Ljava/lang/String;

    if-eqz v0, :cond_0

    return-void

    .line 223
    :cond_0
    iget-object v0, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->macAddressProvider:Lcom/squareup/http/interceptor/MacAddressProvider;

    invoke-virtual {v0}, Lcom/squareup/http/interceptor/MacAddressProvider;->getMacAddress()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 225
    invoke-static {v0}, Lcom/squareup/util/Strings;->createMD5Hash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->md5Mac:Ljava/lang/String;

    .line 226
    invoke-static {v0}, Lcom/squareup/util/Strings;->createSHA1Hash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->sha1Mac:Ljava/lang/String;

    :cond_1
    return-void
.end method

.method public static getHeaderString(Landroid/location/Location;)Ljava/lang/String;
    .locals 4

    if-nez p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 109
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    invoke-virtual {p0}, Landroid/location/Location;->getAltitude()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v1, " epu="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " hdn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    invoke-virtual {p0}, Landroid/location/Location;->getBearing()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " spd="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    invoke-virtual {p0}, Landroid/location/Location;->getSpeed()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " utc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    invoke-virtual {p0}, Landroid/location/Location;->getTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0
.end method


# virtual methods
.method public createMutableHeaderList()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Header;",
            ">;"
        }
    .end annotation

    .line 128
    iget-object v0, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->installationIdProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 130
    invoke-direct {p0}, Lcom/squareup/http/interceptor/RealSquareHeaders;->checkMacAddress()V

    .line 132
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 133
    iget-object v2, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->userAgent:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "User-Agent"

    invoke-static {v1, v3, v2}, Lcom/squareup/http/interceptor/RealSquareHeaders;->addIfNotNull(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    iget-object v2, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Locale;

    invoke-static {v2}, Lcom/squareup/util/Locales;->getAcceptLanguage(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Accept-Language"

    invoke-static {v1, v3, v2}, Lcom/squareup/http/interceptor/RealSquareHeaders;->addIfNotNull(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    iget-object v2, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->clock:Lcom/squareup/util/Clock;

    invoke-static {v2}, Lcom/squareup/util/Times;->asTimeZoneString(Lcom/squareup/util/Clock;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Time-Zone"

    invoke-static {v1, v3, v2}, Lcom/squareup/http/interceptor/RealSquareHeaders;->addIfNotNull(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    iget-object v2, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->androidIdProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "X-Android-ID"

    invoke-static {v1, v3, v2}, Lcom/squareup/http/interceptor/RealSquareHeaders;->addIfNotNull(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "X-Device-Installation-ID"

    .line 138
    invoke-static {v1, v2, v0}, Lcom/squareup/http/interceptor/RealSquareHeaders;->addIfNotNull(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "X-Device-Vendor-ID"

    .line 139
    invoke-static {v1, v2, v0}, Lcom/squareup/http/interceptor/RealSquareHeaders;->addIfNotNull(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    iget-object v0, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->md5Mac:Ljava/lang/String;

    const-string v2, "X-Device-Hash-MAC"

    invoke-static {v1, v2, v0}, Lcom/squareup/http/interceptor/RealSquareHeaders;->addIfNotNull(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    iget-object v0, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->sha1Mac:Ljava/lang/String;

    const-string v2, "X-Device-Hash-SHA1-MAC"

    invoke-static {v1, v2, v0}, Lcom/squareup/http/interceptor/RealSquareHeaders;->addIfNotNull(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    invoke-virtual {p0}, Lcom/squareup/http/interceptor/RealSquareHeaders;->getEncodedSubscriberId()Ljava/lang/String;

    move-result-object v0

    const-string v2, "X-Subscriber-ID"

    invoke-static {v1, v2, v0}, Lcom/squareup/http/interceptor/RealSquareHeaders;->addIfNotNull(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    iget-object v0, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->androidSerialProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "X-Android-Serial"

    invoke-static {v1, v2, v0}, Lcom/squareup/http/interceptor/RealSquareHeaders;->addIfNotNull(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->locationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    invoke-static {v0}, Lcom/squareup/http/interceptor/RealSquareHeaders;->getHeaderString(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "Geo-Position"

    invoke-static {v1, v2, v0}, Lcom/squareup/http/interceptor/RealSquareHeaders;->addIfNotNull(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->onboardRedirectSettings:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "X-Anonymized-User-Token"

    invoke-static {v1, v2, v0}, Lcom/squareup/http/interceptor/RealSquareHeaders;->addIfNotNull(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    iget-object v0, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->adIdCache:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "X-Device-Advertising-ID"

    invoke-static {v1, v2, v0}, Lcom/squareup/http/interceptor/RealSquareHeaders;->addIfNotNull(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->speleoIdGenerator:Lcom/squareup/http/interceptor/SpeleoIdGenerator;

    invoke-virtual {v0}, Lcom/squareup/http/interceptor/SpeleoIdGenerator;->next()Ljava/lang/String;

    move-result-object v0

    const-string v2, "X-Speleo-Trace-Id"

    invoke-static {v1, v2, v0}, Lcom/squareup/http/interceptor/RealSquareHeaders;->addIfNotNull(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    .line 129
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "null installationId"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public getCellLocation()Ljava/lang/String;
    .locals 4

    .line 238
    sget-object v0, Lcom/squareup/systempermissions/SystemPermission;->LOCATION:Lcom/squareup/systempermissions/SystemPermission;

    iget-object v1, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->application:Landroid/app/Application;

    invoke-virtual {v0, v1}, Lcom/squareup/systempermissions/SystemPermission;->hasPermission(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 240
    :cond_0
    iget-object v0, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    move-result-object v0

    .line 241
    instance-of v2, v0, Landroid/telephony/gsm/GsmCellLocation;

    if-eqz v2, :cond_1

    .line 242
    check-cast v0, Landroid/telephony/gsm/GsmCellLocation;

    .line 243
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GSM; lac="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", cid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 244
    :cond_1
    instance-of v2, v0, Landroid/telephony/cdma/CdmaCellLocation;

    if-eqz v2, :cond_2

    .line 245
    check-cast v0, Landroid/telephony/cdma/CdmaCellLocation;

    .line 246
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CDMA; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    invoke-virtual {v0}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationLatitude()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    invoke-virtual {v0}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationLongitude()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    invoke-virtual {v0}, Landroid/telephony/cdma/CdmaCellLocation;->getSystemId()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 250
    invoke-virtual {v0}, Landroid/telephony/cdma/CdmaCellLocation;->getNetworkId()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    return-object v1
.end method

.method getEncodedSim()Ljava/lang/String;
    .locals 4

    .line 182
    iget-object v0, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->sim:Ljava/lang/String;

    if-eqz v0, :cond_0

    return-object v0

    .line 186
    :cond_0
    sget-object v0, Lcom/squareup/systempermissions/SystemPermission;->PHONE:Lcom/squareup/systempermissions/SystemPermission;

    iget-object v1, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->application:Landroid/app/Application;

    invoke-virtual {v0, v1}, Lcom/squareup/systempermissions/SystemPermission;->hasPermission(Landroid/content/Context;)Z

    move-result v0

    const-string v1, " "

    const-string v2, "; "

    if-eqz v0, :cond_1

    .line 189
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 190
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 191
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimOperatorName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v1}, Landroid/net/Uri;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 192
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimSerialNumber()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->sim:Ljava/lang/String;

    const/4 v0, 0x0

    .line 193
    iput-object v0, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->simWithoutSub:Ljava/lang/String;

    .line 194
    iget-object v0, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->sim:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    nop

    .line 201
    :cond_1
    iget-object v0, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->simWithoutSub:Ljava/lang/String;

    if-eqz v0, :cond_2

    return-object v0

    .line 205
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 206
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 207
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimOperatorName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Landroid/net/Uri;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "; no_permission"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->simWithoutSub:Ljava/lang/String;

    .line 210
    iget-object v0, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->simWithoutSub:Ljava/lang/String;

    return-object v0
.end method

.method getEncodedSubscriberId()Ljava/lang/String;
    .locals 2

    .line 164
    iget-object v0, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->subscriberId:Ljava/lang/String;

    if-nez v0, :cond_0

    sget-object v0, Lcom/squareup/systempermissions/SystemPermission;->PHONE:Lcom/squareup/systempermissions/SystemPermission;

    iget-object v1, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->application:Landroid/app/Application;

    invoke-virtual {v0, v1}, Lcom/squareup/systempermissions/SystemPermission;->hasPermission(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    :try_start_0
    iget-object v0, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->subscriberId:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 173
    :catch_0
    :cond_0
    iget-object v0, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->subscriberId:Ljava/lang/String;

    return-object v0
.end method

.method sanitizeDeviceName()Ljava/lang/String;
    .locals 2

    .line 156
    iget-object v0, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->persistentDeviceName:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->removeAsciiControlCharacters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "?"

    .line 157
    invoke-static {v0, v1}, Lcom/squareup/util/Strings;->sanitizeTextToAsciiLetters(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method sanitizeNetworkOperator()Ljava/lang/String;
    .locals 2

    .line 152
    iget-object v0, p0, Lcom/squareup/http/interceptor/RealSquareHeaders;->telephony:Lcom/squareup/http/interceptor/Telephony;

    invoke-virtual {v0}, Lcom/squareup/http/interceptor/Telephony;->getNetworkOperator()Ljava/lang/String;

    move-result-object v0

    const-string v1, "?"

    invoke-static {v0, v1}, Lcom/squareup/util/Strings;->sanitizeTextToAsciiLetters(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
