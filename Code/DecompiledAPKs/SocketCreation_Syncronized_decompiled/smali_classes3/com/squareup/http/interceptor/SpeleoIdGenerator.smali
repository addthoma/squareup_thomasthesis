.class Lcom/squareup/http/interceptor/SpeleoIdGenerator;
.super Ljava/lang/Object;
.source "SpeleoIdGenerator.java"


# instance fields
.field private final random:Ljava/util/Random;


# direct methods
.method constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    iput-object v0, p0, Lcom/squareup/http/interceptor/SpeleoIdGenerator;->random:Ljava/util/Random;

    return-void
.end method


# virtual methods
.method next()Ljava/lang/String;
    .locals 2

    .line 19
    iget-object v0, p0, Lcom/squareup/http/interceptor/SpeleoIdGenerator;->random:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/http/interceptor/LongCodec;->encodeToString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
