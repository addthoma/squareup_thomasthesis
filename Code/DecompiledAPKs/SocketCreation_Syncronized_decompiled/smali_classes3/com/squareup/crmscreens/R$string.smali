.class public final Lcom/squareup/crmscreens/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/crmscreens/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final crm_activity_list_header_v2:I = 0x7f1205f3

.field public static final crm_add_coupon:I = 0x7f1205f9

.field public static final crm_add_coupon_amount:I = 0x7f1205fa

.field public static final crm_add_coupon_disclaimer:I = 0x7f1205fb

.field public static final crm_add_from_address_book:I = 0x7f120603

.field public static final crm_add_note_hint:I = 0x7f120604

.field public static final crm_address_header:I = 0x7f12060f

.field public static final crm_adjust_points_add:I = 0x7f120610

.field public static final crm_adjust_points_remove:I = 0x7f120611

.field public static final crm_adjust_points_title:I = 0x7f120612

.field public static final crm_adjust_punches_error:I = 0x7f120613

.field public static final crm_all_notes_title:I = 0x7f120614

.field public static final crm_birthday_header:I = 0x7f120618

.field public static final crm_birthday_picker_provide_year:I = 0x7f120619

.field public static final crm_cancel:I = 0x7f12061d

.field public static final crm_cardonfile_unlink_failed:I = 0x7f120636

.field public static final crm_cardonfile_unlink_loading:I = 0x7f120637

.field public static final crm_cardonfile_unlink_success:I = 0x7f120638

.field public static final crm_comment_negative_feedback:I = 0x7f12064b

.field public static final crm_comment_positive_feedback:I = 0x7f12064c

.field public static final crm_company_hint:I = 0x7f12064d

.field public static final crm_confirm_customer_deletion_one_details:I = 0x7f120650

.field public static final crm_confirm_customer_deletion_one_details_loyalty_sms:I = 0x7f120651

.field public static final crm_conversation_loading_error:I = 0x7f120660

.field public static final crm_conversation_no_response_error:I = 0x7f120661

.field public static final crm_coupons_and_rewards_manage_action:I = 0x7f120667

.field public static final crm_coupons_and_rewards_manage_title:I = 0x7f120668

.field public static final crm_coupons_and_rewards_view_all_title:I = 0x7f120669

.field public static final crm_coupons_and_rewards_void_action:I = 0x7f12066a

.field public static final crm_coupons_and_rewards_void_error:I = 0x7f12066e

.field public static final crm_coupons_and_rewards_void_result_plural:I = 0x7f12066f

.field public static final crm_coupons_and_rewards_void_result_single:I = 0x7f120670

.field public static final crm_coupons_and_rewards_void_voiding:I = 0x7f120671

.field public static final crm_create_group_title:I = 0x7f120677

.field public static final crm_create_note_title:I = 0x7f12067d

.field public static final crm_delete:I = 0x7f12069a

.field public static final crm_delete_confirmation_title:I = 0x7f12069b

.field public static final crm_delete_note_label:I = 0x7f12069f

.field public static final crm_delete_note_label_confirm:I = 0x7f1206a0

.field public static final crm_email_hint:I = 0x7f1206bd

.field public static final crm_first_name_hint:I = 0x7f1206ce

.field public static final crm_frequent_items_heading:I = 0x7f1206cf

.field public static final crm_frequent_items_last_year:I = 0x7f1206d1

.field public static final crm_frequent_items_subtitle_format:I = 0x7f1206d3

.field public static final crm_frequent_items_subtitle_purchase_plural:I = 0x7f1206d4

.field public static final crm_frequent_items_subtitle_purchase_singular:I = 0x7f1206d5

.field public static final crm_frequent_items_uncategorized:I = 0x7f1206d6

.field public static final crm_group_saving_error:I = 0x7f1206d9

.field public static final crm_groups_hint:I = 0x7f1206df

.field public static final crm_last_name_hint:I = 0x7f1206e8

.field public static final crm_loyalty_adjust_status_add:I = 0x7f1206e9

.field public static final crm_loyalty_adjust_status_add_reason_one:I = 0x7f1206ea

.field public static final crm_loyalty_adjust_status_add_reason_title:I = 0x7f1206eb

.field public static final crm_loyalty_adjust_status_add_reason_two:I = 0x7f1206ec

.field public static final crm_loyalty_adjust_status_remove:I = 0x7f1206ed

.field public static final crm_loyalty_adjust_status_remove_reason_one:I = 0x7f1206ee

.field public static final crm_loyalty_adjust_status_remove_reason_three:I = 0x7f1206ef

.field public static final crm_loyalty_adjust_status_remove_reason_title:I = 0x7f1206f0

.field public static final crm_loyalty_adjust_status_remove_reason_two:I = 0x7f1206f1

.field public static final crm_loyalty_adjust_status_sms_disclaimer:I = 0x7f1206f2

.field public static final crm_loyalty_delete_account_body:I = 0x7f1206f3

.field public static final crm_loyalty_delete_account_body_sms_warning:I = 0x7f1206f4

.field public static final crm_loyalty_delete_account_title:I = 0x7f1206f6

.field public static final crm_loyalty_deleting_account:I = 0x7f1206f7

.field public static final crm_loyalty_deleting_failure:I = 0x7f1206f8

.field public static final crm_loyalty_deleting_success:I = 0x7f1206f9

.field public static final crm_loyalty_program_section_all_loyalty_rewards:I = 0x7f1206ff

.field public static final crm_loyalty_program_section_view_expiring_points_title:I = 0x7f12070a

.field public static final crm_loyalty_program_section_view_expiring_points_value:I = 0x7f12070b

.field public static final crm_loyalty_redeem_row_action:I = 0x7f12070c

.field public static final crm_no_comment_negative_feedback:I = 0x7f12071c

.field public static final crm_no_comment_positive_feedback:I = 0x7f12071d

.field public static final crm_note_char_max:I = 0x7f12071f

.field public static final crm_note_creator_timestamp:I = 0x7f120720

.field public static final crm_note_deleting_error:I = 0x7f120721

.field public static final crm_note_header:I = 0x7f120722

.field public static final crm_note_saving_error:I = 0x7f120723

.field public static final crm_other_information_edit_uppercase:I = 0x7f120726

.field public static final crm_personal_information_header_uppercase:I = 0x7f120727

.field public static final crm_phone_hint:I = 0x7f120728

.field public static final crm_points_add:I = 0x7f120729

.field public static final crm_points_balance:I = 0x7f12072a

.field public static final crm_points_current_unadjusted_balance:I = 0x7f12072b

.field public static final crm_points_earned_format:I = 0x7f12072c

.field public static final crm_points_negative_adjusted_balance:I = 0x7f12072d

.field public static final crm_points_positive_adjusted_balance:I = 0x7f12072e

.field public static final crm_points_remove:I = 0x7f12072f

.field public static final crm_reference_id_hint:I = 0x7f120749

.field public static final crm_reminder:I = 0x7f12074a

.field public static final crm_reminder_none:I = 0x7f12074b

.field public static final crm_reminder_one_day:I = 0x7f12074c

.field public static final crm_reminder_one_month:I = 0x7f12074d

.field public static final crm_reminder_one_week:I = 0x7f12074e

.field public static final crm_reminder_remove:I = 0x7f12074f

.field public static final crm_reminder_remove_confirm:I = 0x7f120750

.field public static final crm_reminder_tomorrow:I = 0x7f120752

.field public static final crm_send_message_error:I = 0x7f120773

.field public static final crm_send_message_hint:I = 0x7f120774

.field public static final crm_send_message_send:I = 0x7f120776

.field public static final crm_send_message_title:I = 0x7f120777

.field public static final crm_send_message_warning:I = 0x7f120778

.field public static final crm_view_note_title:I = 0x7f120785

.field public static final feedback_url:I = 0x7f120aba

.field public static final loyalty_url:I = 0x7f120f89

.field public static final prod_base_url:I = 0x7f1214f9

.field public static final remove:I = 0x7f121668

.field public static final square_relative_url:I = 0x7f12189e

.field public static final staging_base_url:I = 0x7f1218ab

.field public static final transactions_url:I = 0x7f1219f8


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
