.class public Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;
.super Lcom/squareup/experiments/ExperimentProfile;
.source "BranPaymentPromptVariationsExperiment.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$Behavior;,
        Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0016\u0018\u0000 \r2\u00020\u0001:\u0002\u000c\rB\u001b\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0006\u0010\u0008\u001a\u00020\tJ\u000e\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\t0\u000bH\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;",
        "Lcom/squareup/experiments/ExperimentProfile;",
        "storage",
        "Ldagger/Lazy;",
        "Lcom/squareup/experiments/ExperimentStorage;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Ldagger/Lazy;Lcom/squareup/settings/server/Features;)V",
        "getBranPaymentPromptVariationsBehavior",
        "Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$Behavior;",
        "onPaymentPromptVariationBehavior",
        "Lio/reactivex/Observable;",
        "Behavior",
        "Companion",
        "experiments_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final CLASSIC:Lcom/squareup/server/ExperimentsResponse$Bucket;

.field public static final Companion:Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$Companion;

.field private static final DEFAULT_CONFIGURATION:Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;

.field private static final STATIC:Lcom/squareup/server/ExperimentsResponse$Bucket;

.field private static final VIDEO:Lcom/squareup/server/ExperimentsResponse$Bucket;


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;->Companion:Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$Companion;

    .line 48
    new-instance v0, Lcom/squareup/server/ExperimentsResponse$Bucket;

    const/16 v1, 0x21

    const/16 v2, 0xc69

    const/16 v3, 0x1efd

    const-string v4, "CLASSIC"

    invoke-direct {v0, v2, v3, v4, v1}, Lcom/squareup/server/ExperimentsResponse$Bucket;-><init>(IILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;->CLASSIC:Lcom/squareup/server/ExperimentsResponse$Bucket;

    .line 49
    new-instance v0, Lcom/squareup/server/ExperimentsResponse$Bucket;

    const/16 v3, 0x1efe

    const-string v4, "VIDEO"

    invoke-direct {v0, v2, v3, v4, v1}, Lcom/squareup/server/ExperimentsResponse$Bucket;-><init>(IILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;->VIDEO:Lcom/squareup/server/ExperimentsResponse$Bucket;

    .line 50
    new-instance v0, Lcom/squareup/server/ExperimentsResponse$Bucket;

    const/16 v3, 0x1eff

    const-string v4, "STATIC"

    invoke-direct {v0, v2, v3, v4, v1}, Lcom/squareup/server/ExperimentsResponse$Bucket;-><init>(IILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;->STATIC:Lcom/squareup/server/ExperimentsResponse$Bucket;

    .line 52
    new-instance v0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;

    invoke-direct {v0}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;-><init>()V

    .line 53
    sget-object v1, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;->CLASSIC:Lcom/squareup/server/ExperimentsResponse$Bucket;

    invoke-virtual {v0, v1}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->addBucket(Lcom/squareup/server/ExperimentsResponse$Bucket;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;

    move-result-object v0

    .line 54
    sget-object v1, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;->VIDEO:Lcom/squareup/server/ExperimentsResponse$Bucket;

    invoke-virtual {v0, v1}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->addBucket(Lcom/squareup/server/ExperimentsResponse$Bucket;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;

    move-result-object v0

    .line 55
    sget-object v1, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;->STATIC:Lcom/squareup/server/ExperimentsResponse$Bucket;

    invoke-virtual {v0, v1}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->addBucket(Lcom/squareup/server/ExperimentsResponse$Bucket;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;

    move-result-object v0

    const-string v1, "Test different Bran payment prompts to find what works best for customers."

    .line 56
    invoke-virtual {v0, v1}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->setDescription(Ljava/lang/String;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;

    move-result-object v0

    .line 57
    invoke-virtual {v0, v2}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->setId(I)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;

    move-result-object v0

    const-string v1, "bran_payment_prompt_variations"

    .line 58
    invoke-virtual {v0, v1}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->setName(Ljava/lang/String;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;

    move-result-object v0

    const-string v1, "RUNNING"

    .line 59
    invoke-virtual {v0, v1}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->setStatus(Ljava/lang/String;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;

    move-result-object v0

    .line 60
    new-instance v1, Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;

    const-wide/16 v2, 0x0

    invoke-direct {v1, v2, v3}, Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;-><init>(J)V

    invoke-virtual {v0, v1}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->setUpdatedAt(Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;

    move-result-object v0

    const-string v1, "3"

    .line 61
    invoke-virtual {v0, v1}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->setVersion(Ljava/lang/String;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;

    move-result-object v0

    .line 62
    invoke-virtual {v0}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->build()Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;

    move-result-object v0

    const-string v1, "ExperimentConfig.Builder\u2026ion(\"3\")\n        .build()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;->DEFAULT_CONFIGURATION:Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;

    return-void
.end method

.method public constructor <init>(Ldagger/Lazy;Lcom/squareup/settings/server/Features;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/experiments/ExperimentStorage;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            ")V"
        }
    .end annotation

    const-string v0, "storage"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    sget-object v0, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$Behavior;->CLASSIC:Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$Behavior;

    invoke-virtual {v0}, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$Behavior;->name()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;->DEFAULT_CONFIGURATION:Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/experiments/ExperimentProfile;-><init>(Ldagger/Lazy;Ljava/lang/String;Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;)V

    iput-object p2, p0, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static final synthetic access$getDEFAULT_CONFIGURATION$cp()Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;
    .locals 1

    .line 18
    sget-object v0, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;->DEFAULT_CONFIGURATION:Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;

    return-object v0
.end method

.method private final onPaymentPromptVariationBehavior()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$Behavior;",
            ">;"
        }
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_BRAN_PAYMENT_PROMPT_VARIATIONS_EXPERIMENT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->featureEnabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Observable;

    move-result-object v0

    .line 32
    new-instance v1, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$onPaymentPromptVariationBehavior$1;

    invoke-direct {v1, p0}, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$onPaymentPromptVariationBehavior$1;-><init>(Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "features.featureEnabled(\u2026IC)\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final getBranPaymentPromptVariationsBehavior()Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$Behavior;
    .locals 2

    .line 25
    invoke-direct {p0}, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;->onPaymentPromptVariationBehavior()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$getBranPaymentPromptVariationsBehavior$1;->INSTANCE:Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$getBranPaymentPromptVariationsBehavior$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "onPaymentPromptVariation\u2026turn { Behavior.CLASSIC }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    sget-object v1, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$Behavior;->CLASSIC:Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$Behavior;

    .line 24
    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2BlockingSupport;->getValueOrDefault(Lio/reactivex/Observable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "getValueOrDefault(\n     \u2026   Behavior.CLASSIC\n    )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$Behavior;

    return-object v0
.end method
