.class final enum Lcom/squareup/fsm/Rule$Phase;
.super Ljava/lang/Enum;
.source "Rule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/fsm/Rule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "Phase"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/fsm/Rule$Phase;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/fsm/Rule$Phase;

.field public static final enum ENTRY:Lcom/squareup/fsm/Rule$Phase;

.field public static final enum EXIT:Lcom/squareup/fsm/Rule$Phase;

.field public static final enum GUARDED:Lcom/squareup/fsm/Rule$Phase;

.field public static final enum UNGUARDED:Lcom/squareup/fsm/Rule$Phase;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 17
    new-instance v0, Lcom/squareup/fsm/Rule$Phase;

    const/4 v1, 0x0

    const-string v2, "GUARDED"

    invoke-direct {v0, v2, v1}, Lcom/squareup/fsm/Rule$Phase;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/fsm/Rule$Phase;->GUARDED:Lcom/squareup/fsm/Rule$Phase;

    .line 20
    new-instance v0, Lcom/squareup/fsm/Rule$Phase;

    const/4 v2, 0x1

    const-string v3, "UNGUARDED"

    invoke-direct {v0, v3, v2}, Lcom/squareup/fsm/Rule$Phase;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/fsm/Rule$Phase;->UNGUARDED:Lcom/squareup/fsm/Rule$Phase;

    .line 23
    new-instance v0, Lcom/squareup/fsm/Rule$Phase;

    const/4 v3, 0x2

    const-string v4, "ENTRY"

    invoke-direct {v0, v4, v3}, Lcom/squareup/fsm/Rule$Phase;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/fsm/Rule$Phase;->ENTRY:Lcom/squareup/fsm/Rule$Phase;

    .line 26
    new-instance v0, Lcom/squareup/fsm/Rule$Phase;

    const/4 v4, 0x3

    const-string v5, "EXIT"

    invoke-direct {v0, v5, v4}, Lcom/squareup/fsm/Rule$Phase;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/fsm/Rule$Phase;->EXIT:Lcom/squareup/fsm/Rule$Phase;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/fsm/Rule$Phase;

    .line 15
    sget-object v5, Lcom/squareup/fsm/Rule$Phase;->GUARDED:Lcom/squareup/fsm/Rule$Phase;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/fsm/Rule$Phase;->UNGUARDED:Lcom/squareup/fsm/Rule$Phase;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/fsm/Rule$Phase;->ENTRY:Lcom/squareup/fsm/Rule$Phase;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/fsm/Rule$Phase;->EXIT:Lcom/squareup/fsm/Rule$Phase;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/fsm/Rule$Phase;->$VALUES:[Lcom/squareup/fsm/Rule$Phase;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/fsm/Rule$Phase;
    .locals 1

    .line 15
    const-class v0, Lcom/squareup/fsm/Rule$Phase;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/fsm/Rule$Phase;

    return-object p0
.end method

.method public static values()[Lcom/squareup/fsm/Rule$Phase;
    .locals 1

    .line 15
    sget-object v0, Lcom/squareup/fsm/Rule$Phase;->$VALUES:[Lcom/squareup/fsm/Rule$Phase;

    invoke-virtual {v0}, [Lcom/squareup/fsm/Rule$Phase;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/fsm/Rule$Phase;

    return-object v0
.end method
