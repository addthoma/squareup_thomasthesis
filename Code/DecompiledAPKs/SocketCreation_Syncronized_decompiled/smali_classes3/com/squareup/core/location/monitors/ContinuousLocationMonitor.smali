.class public interface abstract Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;
.super Ljava/lang/Object;
.source "ContinuousLocationMonitor.java"

# interfaces
.implements Lcom/squareup/core/location/monitors/LocationMonitor;
.implements Lmortar/Scoped;


# static fields
.field public static final MAX_LOCATION_AGE:J = 0x202fbf000L


# virtual methods
.method public abstract isRunning()Z
.end method

.method public abstract onLocation()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation
.end method

.method public abstract pause()V
.end method

.method public abstract setInterval(Lcom/squareup/core/location/monitors/LocationInterval;)V
.end method

.method public abstract start()V
.end method
