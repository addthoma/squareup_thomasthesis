.class public Lcom/squareup/core/location/providers/GpsProvider;
.super Ljava/lang/Object;
.source "GpsProvider.java"

# interfaces
.implements Lcom/squareup/core/location/providers/LocationProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/core/location/providers/GpsProvider$WrongLocationTimestampEvent;,
        Lcom/squareup/core/location/providers/GpsProvider$InternalGpsListener;
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final available:Z

.field private final clock:Lcom/squareup/util/Clock;

.field private final context:Landroid/content/Context;

.field private externalListener:Landroid/location/LocationListener;

.field private final internalListener:Lcom/squareup/core/location/providers/GpsProvider$InternalGpsListener;

.field private final locationManager:Landroid/location/LocationManager;

.field private loggedInvalidTimestamp:Z

.field private final nmeaListener:Lcom/squareup/core/location/providers/NMEAEventListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/location/LocationManager;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Clock;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 1

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 30
    iput-boolean v0, p0, Lcom/squareup/core/location/providers/GpsProvider;->loggedInvalidTimestamp:Z

    .line 40
    iput-object p1, p0, Lcom/squareup/core/location/providers/GpsProvider;->context:Landroid/content/Context;

    .line 41
    iput-object p2, p0, Lcom/squareup/core/location/providers/GpsProvider;->locationManager:Landroid/location/LocationManager;

    .line 42
    invoke-virtual {p2}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object p1

    const-string p2, "gps"

    invoke-interface {p1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/core/location/providers/GpsProvider;->available:Z

    .line 43
    new-instance p1, Lcom/squareup/core/location/providers/GpsProvider$InternalGpsListener;

    const/4 p2, 0x0

    invoke-direct {p1, p0, p2}, Lcom/squareup/core/location/providers/GpsProvider$InternalGpsListener;-><init>(Lcom/squareup/core/location/providers/GpsProvider;Lcom/squareup/core/location/providers/GpsProvider$1;)V

    iput-object p1, p0, Lcom/squareup/core/location/providers/GpsProvider;->internalListener:Lcom/squareup/core/location/providers/GpsProvider$InternalGpsListener;

    .line 44
    new-instance p1, Lcom/squareup/core/location/providers/NMEAEventListener;

    iget-object p2, p0, Lcom/squareup/core/location/providers/GpsProvider;->internalListener:Lcom/squareup/core/location/providers/GpsProvider$InternalGpsListener;

    invoke-direct {p1, p2, p5}, Lcom/squareup/core/location/providers/NMEAEventListener;-><init>(Lcom/squareup/core/location/providers/NMEAEventListener$Listener;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    iput-object p1, p0, Lcom/squareup/core/location/providers/GpsProvider;->nmeaListener:Lcom/squareup/core/location/providers/NMEAEventListener;

    .line 45
    iput-object p3, p0, Lcom/squareup/core/location/providers/GpsProvider;->analytics:Lcom/squareup/analytics/Analytics;

    .line 46
    iput-object p4, p0, Lcom/squareup/core/location/providers/GpsProvider;->clock:Lcom/squareup/util/Clock;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/core/location/providers/GpsProvider;)Z
    .locals 0

    .line 21
    iget-boolean p0, p0, Lcom/squareup/core/location/providers/GpsProvider;->loggedInvalidTimestamp:Z

    return p0
.end method

.method static synthetic access$102(Lcom/squareup/core/location/providers/GpsProvider;Z)Z
    .locals 0

    .line 21
    iput-boolean p1, p0, Lcom/squareup/core/location/providers/GpsProvider;->loggedInvalidTimestamp:Z

    return p1
.end method

.method static synthetic access$300(Lcom/squareup/core/location/providers/GpsProvider;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/core/location/providers/GpsProvider;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/core/location/providers/GpsProvider;)Landroid/location/LocationListener;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/core/location/providers/GpsProvider;->externalListener:Landroid/location/LocationListener;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/core/location/providers/GpsProvider;)Lcom/squareup/util/Clock;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/core/location/providers/GpsProvider;->clock:Lcom/squareup/util/Clock;

    return-object p0
.end method


# virtual methods
.method public getLastKnownLocation()Landroid/location/Location;
    .locals 2

    .line 51
    iget-boolean v0, p0, Lcom/squareup/core/location/providers/GpsProvider;->available:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/systempermissions/SystemPermission;->LOCATION:Lcom/squareup/systempermissions/SystemPermission;

    iget-object v1, p0, Lcom/squareup/core/location/providers/GpsProvider;->context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/squareup/systempermissions/SystemPermission;->hasPermission(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/core/location/providers/GpsProvider;->locationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    .line 52
    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public removeUpdates()V
    .locals 2

    .line 80
    sget-object v0, Lcom/squareup/systempermissions/SystemPermission;->LOCATION:Lcom/squareup/systempermissions/SystemPermission;

    iget-object v1, p0, Lcom/squareup/core/location/providers/GpsProvider;->context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/squareup/systempermissions/SystemPermission;->hasPermission(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/squareup/core/location/providers/GpsProvider;->locationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/squareup/core/location/providers/GpsProvider;->nmeaListener:Lcom/squareup/core/location/providers/NMEAEventListener;

    invoke-static {v0, v1}, Lcom/squareup/core/location/providers/LocationManagerKt;->removeNmeaListener(Landroid/location/LocationManager;Lcom/squareup/core/location/providers/NMEAEventListener;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/core/location/providers/GpsProvider;->locationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/squareup/core/location/providers/GpsProvider;->internalListener:Lcom/squareup/core/location/providers/GpsProvider$InternalGpsListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    return-void
.end method

.method public requestLocationUpdate(Landroid/location/LocationListener;J)V
    .locals 1

    const/4 v0, 0x0

    .line 75
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/squareup/core/location/providers/GpsProvider;->requestLocationUpdate(Landroid/location/LocationListener;JF)V

    return-void
.end method

.method public requestLocationUpdate(Landroid/location/LocationListener;JF)V
    .locals 7

    .line 59
    iget-boolean v0, p0, Lcom/squareup/core/location/providers/GpsProvider;->available:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/systempermissions/SystemPermission;->LOCATION:Lcom/squareup/systempermissions/SystemPermission;

    iget-object v1, p0, Lcom/squareup/core/location/providers/GpsProvider;->context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/squareup/systempermissions/SystemPermission;->hasPermission(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    if-eqz p1, :cond_1

    .line 66
    iput-object p1, p0, Lcom/squareup/core/location/providers/GpsProvider;->externalListener:Landroid/location/LocationListener;

    .line 68
    iget-object p1, p0, Lcom/squareup/core/location/providers/GpsProvider;->locationManager:Landroid/location/LocationManager;

    iget-object v0, p0, Lcom/squareup/core/location/providers/GpsProvider;->nmeaListener:Lcom/squareup/core/location/providers/NMEAEventListener;

    invoke-static {p1, v0}, Lcom/squareup/core/location/providers/LocationManagerKt;->addNmeaListener(Landroid/location/LocationManager;Lcom/squareup/core/location/providers/NMEAEventListener;)V

    .line 69
    iget-object v1, p0, Lcom/squareup/core/location/providers/GpsProvider;->locationManager:Landroid/location/LocationManager;

    iget-object v6, p0, Lcom/squareup/core/location/providers/GpsProvider;->internalListener:Lcom/squareup/core/location/providers/GpsProvider$InternalGpsListener;

    const-string v2, "gps"

    move-wide v3, p2

    move v5, p4

    invoke-virtual/range {v1 .. v6}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    return-void

    .line 64
    :cond_1
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "externalListener must not be null!"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    :goto_0
    return-void
.end method
