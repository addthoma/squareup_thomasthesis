.class public Lcom/squareup/core/location/constraint/CompositeLocationConstraint;
.super Ljava/lang/Object;
.source "CompositeLocationConstraint.java"

# interfaces
.implements Lcom/squareup/core/location/constraint/LocationConstraint;


# instance fields
.field private final constraints:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/core/location/constraint/LocationConstraint;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>([Lcom/squareup/core/location/constraint/LocationConstraint;)V
    .locals 1

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/core/location/constraint/CompositeLocationConstraint;->constraints:Ljava/util/List;

    .line 13
    iget-object v0, p0, Lcom/squareup/core/location/constraint/CompositeLocationConstraint;->constraints:Ljava/util/List;

    invoke-static {v0, p1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public isSatisfied(Landroid/location/Location;)Z
    .locals 2

    .line 17
    iget-object v0, p0, Lcom/squareup/core/location/constraint/CompositeLocationConstraint;->constraints:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/core/location/constraint/LocationConstraint;

    .line 18
    invoke-interface {v1, p1}, Lcom/squareup/core/location/constraint/LocationConstraint;->isSatisfied(Landroid/location/Location;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_1
    const/4 p1, 0x1

    return p1
.end method
