.class public Lcom/squareup/core/location/constraint/AccuracyConstraint;
.super Ljava/lang/Object;
.source "AccuracyConstraint.java"

# interfaces
.implements Lcom/squareup/core/location/constraint/LocationConstraint;


# instance fields
.field private final minAccuracy:F


# direct methods
.method public constructor <init>(F)V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput p1, p0, Lcom/squareup/core/location/constraint/AccuracyConstraint;->minAccuracy:F

    return-void
.end method


# virtual methods
.method public isSatisfied(Landroid/location/Location;)Z
    .locals 1

    .line 15
    invoke-virtual {p1}, Landroid/location/Location;->hasAccuracy()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result p1

    iget v0, p0, Lcom/squareup/core/location/constraint/AccuracyConstraint;->minAccuracy:F

    cmpg-float p1, p1, v0

    if-gez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
