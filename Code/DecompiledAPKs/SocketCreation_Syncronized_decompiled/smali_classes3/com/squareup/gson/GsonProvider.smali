.class public Lcom/squareup/gson/GsonProvider;
.super Ljava/lang/Object;
.source "GsonProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/gson/GsonProvider$JediInputKindAdapter;,
        Lcom/squareup/gson/GsonProvider$JediComponentKindAdapter;,
        Lcom/squareup/gson/GsonProvider$UriAdapter;,
        Lcom/squareup/gson/GsonProvider$PercentageTypeAdapter;,
        Lcom/squareup/gson/GsonProvider$FileAdapter;,
        Lcom/squareup/gson/GsonProvider$Iso8601DateTypeAdapter;,
        Lcom/squareup/gson/GsonProvider$LowercaseEnumTypeAdapterFactory;,
        Lcom/squareup/gson/GsonProvider$Holder;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static gson()Lcom/google/gson/Gson;
    .locals 1

    .line 36
    sget-object v0, Lcom/squareup/gson/GsonProvider$Holder;->GSON:Lcom/google/gson/Gson;

    return-object v0
.end method

.method public static gsonBuilder()Lcom/google/gson/GsonBuilder;
    .locals 4

    .line 44
    new-instance v0, Lcom/google/gson/GsonBuilder;

    invoke-direct {v0}, Lcom/google/gson/GsonBuilder;-><init>()V

    new-instance v1, Lcom/squareup/gson/GsonProvider$LowercaseEnumTypeAdapterFactory;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/squareup/gson/GsonProvider$LowercaseEnumTypeAdapterFactory;-><init>(Lcom/squareup/gson/GsonProvider$1;)V

    .line 45
    invoke-virtual {v0, v1}, Lcom/google/gson/GsonBuilder;->registerTypeAdapterFactory(Lcom/google/gson/TypeAdapterFactory;)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    const-class v1, Ljava/util/Date;

    new-instance v3, Lcom/squareup/gson/GsonProvider$Iso8601DateTypeAdapter;

    invoke-direct {v3}, Lcom/squareup/gson/GsonProvider$Iso8601DateTypeAdapter;-><init>()V

    .line 46
    invoke-virtual {v3}, Lcom/squareup/gson/GsonProvider$Iso8601DateTypeAdapter;->nullSafe()Lcom/google/gson/TypeAdapter;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    const-class v1, Ljava/io/File;

    new-instance v3, Lcom/squareup/gson/GsonProvider$FileAdapter;

    invoke-direct {v3, v2}, Lcom/squareup/gson/GsonProvider$FileAdapter;-><init>(Lcom/squareup/gson/GsonProvider$1;)V

    .line 47
    invoke-virtual {v3}, Lcom/squareup/gson/GsonProvider$FileAdapter;->nullSafe()Lcom/google/gson/TypeAdapter;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    const-class v1, Lcom/squareup/util/Percentage;

    new-instance v3, Lcom/squareup/gson/GsonProvider$PercentageTypeAdapter;

    invoke-direct {v3, v2}, Lcom/squareup/gson/GsonProvider$PercentageTypeAdapter;-><init>(Lcom/squareup/gson/GsonProvider$1;)V

    .line 48
    invoke-virtual {v3}, Lcom/squareup/gson/GsonProvider$PercentageTypeAdapter;->nullSafe()Lcom/google/gson/TypeAdapter;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    const-class v1, Landroid/net/Uri;

    new-instance v3, Lcom/squareup/gson/GsonProvider$UriAdapter;

    invoke-direct {v3, v2}, Lcom/squareup/gson/GsonProvider$UriAdapter;-><init>(Lcom/squareup/gson/GsonProvider$1;)V

    .line 49
    invoke-virtual {v3}, Lcom/squareup/gson/GsonProvider$UriAdapter;->nullSafe()Lcom/google/gson/TypeAdapter;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    const-class v1, Lcom/squareup/protos/jedi/service/ComponentKind;

    new-instance v3, Lcom/squareup/gson/GsonProvider$JediComponentKindAdapter;

    invoke-direct {v3, v2}, Lcom/squareup/gson/GsonProvider$JediComponentKindAdapter;-><init>(Lcom/squareup/gson/GsonProvider$1;)V

    .line 50
    invoke-virtual {v3}, Lcom/squareup/gson/GsonProvider$JediComponentKindAdapter;->nullSafe()Lcom/google/gson/TypeAdapter;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    const-class v1, Lcom/squareup/protos/jedi/service/InputKind;

    new-instance v3, Lcom/squareup/gson/GsonProvider$JediInputKindAdapter;

    invoke-direct {v3, v2}, Lcom/squareup/gson/GsonProvider$JediInputKindAdapter;-><init>(Lcom/squareup/gson/GsonProvider$1;)V

    .line 51
    invoke-virtual {v3}, Lcom/squareup/gson/GsonProvider$JediInputKindAdapter;->nullSafe()Lcom/google/gson/TypeAdapter;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->disableHtmlEscaping()Lcom/google/gson/GsonBuilder;

    move-result-object v0

    return-object v0
.end method
