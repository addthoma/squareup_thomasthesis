.class public final Lcom/squareup/gson/GsonModule_ProvideWireGsonFactory;
.super Ljava/lang/Object;
.source "GsonModule_ProvideWireGsonFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/gson/GsonModule_ProvideWireGsonFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/google/gson/Gson;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/gson/GsonModule_ProvideWireGsonFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/gson/GsonModule_ProvideWireGsonFactory$InstanceHolder;->access$000()Lcom/squareup/gson/GsonModule_ProvideWireGsonFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideWireGson()Lcom/google/gson/Gson;
    .locals 2

    .line 27
    sget-object v0, Lcom/squareup/gson/GsonModule;->INSTANCE:Lcom/squareup/gson/GsonModule;

    invoke-virtual {v0}, Lcom/squareup/gson/GsonModule;->provideWireGson()Lcom/google/gson/Gson;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gson/Gson;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/google/gson/Gson;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/gson/GsonModule_ProvideWireGsonFactory;->provideWireGson()Lcom/google/gson/Gson;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/gson/GsonModule_ProvideWireGsonFactory;->get()Lcom/google/gson/Gson;

    move-result-object v0

    return-object v0
.end method
