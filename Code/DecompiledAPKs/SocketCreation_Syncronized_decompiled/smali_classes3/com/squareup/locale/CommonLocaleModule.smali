.class public abstract Lcom/squareup/locale/CommonLocaleModule;
.super Ljava/lang/Object;
.source "CommonLocaleModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindsLocaleChangedNotifier(Lcom/squareup/locale/RealLocaleChangedNotifier;)Lcom/squareup/locale/LocaleChangedNotifier;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
