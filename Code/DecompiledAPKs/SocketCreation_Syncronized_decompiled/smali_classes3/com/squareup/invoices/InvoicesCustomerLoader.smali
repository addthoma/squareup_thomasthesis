.class public Lcom/squareup/invoices/InvoicesCustomerLoader;
.super Lcom/squareup/datafetch/Rx1AbstractLoader;
.source "InvoicesCustomerLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/InvoicesCustomerLoader$SharedScope;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/datafetch/Rx1AbstractLoader<",
        "Ljava/lang/String;",
        "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
        ">;"
    }
.end annotation


# instance fields
.field private final contactToken:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;


# direct methods
.method public constructor <init>(Lcom/squareup/connectivity/ConnectivityMonitor;Lrx/Scheduler;Lcom/squareup/invoices/ClientInvoiceServiceHelper;)V
    .locals 0
    .param p2    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 39
    invoke-direct {p0, p1, p2}, Lcom/squareup/datafetch/Rx1AbstractLoader;-><init>(Lcom/squareup/connectivity/ConnectivityMonitor;Lrx/Scheduler;)V

    .line 32
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/InvoicesCustomerLoader;->contactToken:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 40
    iput-object p3, p0, Lcom/squareup/invoices/InvoicesCustomerLoader;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    return-void
.end method

.method static synthetic lambda$fetch$0(Ljava/lang/String;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;Lcom/squareup/protos/client/invoice/ListInvoicesResponse;)Lcom/squareup/datafetch/Rx1AbstractLoader$Response;
    .locals 3

    .line 63
    iget-object v0, p2, Lcom/squareup/protos/client/invoice/ListInvoicesResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/squareup/protos/client/invoice/ListInvoicesResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    sget-object v1, Lcom/squareup/protos/client/Status;->DEFAULT_SUCCESS:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/ListInvoicesResponse;->invoice:Ljava/util/List;

    iget-object p2, p2, Lcom/squareup/protos/client/invoice/ListInvoicesResponse;->paging_key:Ljava/lang/String;

    invoke-direct {v0, p0, p1, v1, p2}, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;-><init>(Ljava/lang/Object;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;Ljava/util/List;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;

    new-instance p2, Lcom/squareup/datafetch/LoaderError$ThrowableError;

    new-instance v1, Ljava/lang/Throwable;

    const-string v2, "Request failed."

    invoke-direct {v1, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-direct {p2, v1}, Lcom/squareup/datafetch/LoaderError$ThrowableError;-><init>(Ljava/lang/Throwable;)V

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;-><init>(Ljava/lang/Object;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;Lcom/squareup/datafetch/LoaderError;)V

    :goto_0
    return-object v0
.end method


# virtual methods
.method protected bridge synthetic fetch(Ljava/lang/Object;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;Lrx/functions/Action0;)Lrx/Observable;
    .locals 0

    .line 26
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/invoices/InvoicesCustomerLoader;->fetch(Ljava/lang/String;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method protected fetch(Ljava/lang/String;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;Lrx/functions/Action0;)Lrx/Observable;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;",
            "Lrx/functions/Action0;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$Response<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
            ">;>;"
        }
    .end annotation

    .line 60
    iget-object v0, p0, Lcom/squareup/invoices/InvoicesCustomerLoader;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    iget-object v2, p2, Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;->pagingKey:Ljava/lang/String;

    iget-object v1, p2, Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;->pageSize:Ljava/lang/Integer;

    .line 61
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sget-object v4, Lcom/squareup/protos/client/invoice/StateFilter;->ALL_SENT:Lcom/squareup/protos/client/invoice/StateFilter;

    const/4 v1, 0x0

    const/4 v6, 0x0

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->list(Ljava/lang/String;Ljava/lang/String;ILcom/squareup/protos/client/invoice/StateFilter;Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;

    move-result-object v0

    .line 62
    invoke-virtual {v0, p3}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p3

    new-instance v0, Lcom/squareup/invoices/-$$Lambda$InvoicesCustomerLoader$qxyz9T8cbJYkknsznwM0mq9xoJU;

    invoke-direct {v0, p1, p2}, Lcom/squareup/invoices/-$$Lambda$InvoicesCustomerLoader$qxyz9T8cbJYkknsznwM0mq9xoJU;-><init>(Ljava/lang/String;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;)V

    .line 63
    invoke-virtual {p3, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method protected input()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 55
    iget-object v0, p0, Lcom/squareup/invoices/InvoicesCustomerLoader;->contactToken:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public refresh()V
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/squareup/invoices/InvoicesCustomerLoader;->contactToken:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const-string v1, "contactToken must be set."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 51
    invoke-super {p0}, Lcom/squareup/datafetch/Rx1AbstractLoader;->refresh()V

    return-void
.end method

.method public setContactToken(Ljava/lang/String;)V
    .locals 2

    .line 44
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const-string v1, "contactToken cannot be blank"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 45
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 46
    iget-object v0, p0, Lcom/squareup/invoices/InvoicesCustomerLoader;->contactToken:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
