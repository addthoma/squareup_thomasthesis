.class public final Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "AddItemToInvoiceEvent.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent$Events;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u0000 \u00102\u00020\u0001:\u0001\u0010B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\u0008\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;",
        "Lcom/squareup/eventstream/v1/EventStreamEvent;",
        "itemId",
        "",
        "(Ljava/lang/String;)V",
        "getItemId",
        "()Ljava/lang/String;",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "Events",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ADD_DISCOUNT_EVENT:Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;

.field public static final ADD_EDIT_CUSTOM_AMOUNT_EVENT:Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;

.field private static final CUSTOM_AMOUNT_EVENT_ID:Ljava/lang/String; = "Custom Amount"

.field private static final DISCOUNT_EVENT_ID:Ljava/lang/String; = "Discount"

.field public static final Events:Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent$Events;

.field public static final NO_ITEM_ID:Ljava/lang/String; = "No Item ID"


# instance fields
.field private final itemId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent$Events;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent$Events;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;->Events:Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent$Events;

    .line 23
    new-instance v0, Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;

    const-string v1, "Custom Amount"

    invoke-direct {v0, v1}, Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;->ADD_EDIT_CUSTOM_AMOUNT_EVENT:Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;

    .line 27
    new-instance v0, Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;

    const-string v1, "Discount"

    invoke-direct {v0, v1}, Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;->ADD_DISCOUNT_EVENT:Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    const-string v0, "itemId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    const-string v1, "Invoices: Add Item"

    invoke-direct {p0, v0, v1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;->itemId:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;->itemId:Ljava/lang/String;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;->copy(Ljava/lang/String;)Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;->itemId:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;)Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;
    .locals 1

    const-string v0, "itemId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;

    iget-object v0, p0, Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;->itemId:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;->itemId:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getItemId()Ljava/lang/String;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;->itemId:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;->itemId:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AddItemToInvoiceEvent(itemId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;->itemId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
