.class public final Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;
.super Ljava/lang/Object;
.source "InvoiceDefaultMessageUpdater.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c2\u0006\u0010\u000e\u001a\u00020\u000fR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;",
        "",
        "invoiceServiceHelper",
        "Lcom/squareup/invoices/ClientInvoiceServiceHelper;",
        "standardReceiver",
        "Lcom/squareup/receiving/StandardReceiver;",
        "failureMessageFactory",
        "Lcom/squareup/receiving/FailureMessageFactory;",
        "invoiceUnitCache",
        "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
        "(Lcom/squareup/invoices/ClientInvoiceServiceHelper;Lcom/squareup/receiving/StandardReceiver;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;)V",
        "update",
        "Lio/reactivex/Single;",
        "Lcom/squareup/invoices/DefaultMessageUpdatedResult;",
        "message",
        "",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

.field private final invoiceServiceHelper:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

.field private final invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

.field private final standardReceiver:Lcom/squareup/receiving/StandardReceiver;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/ClientInvoiceServiceHelper;Lcom/squareup/receiving/StandardReceiver;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "invoiceServiceHelper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "standardReceiver"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "failureMessageFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceUnitCache"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;->invoiceServiceHelper:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    iput-object p2, p0, Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    iput-object p3, p0, Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    iput-object p4, p0, Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;->invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    return-void
.end method

.method public static final synthetic access$getFailureMessageFactory$p(Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;)Lcom/squareup/receiving/FailureMessageFactory;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    return-object p0
.end method

.method public static final synthetic access$getInvoiceUnitCache$p(Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;)Lcom/squareup/invoicesappletapi/InvoiceUnitCache;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;->invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    return-object p0
.end method


# virtual methods
.method public final update(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/invoices/DefaultMessageUpdatedResult;",
            ">;"
        }
    .end annotation

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    new-instance v0, Lcom/squareup/protos/client/invoice/UnitMetadata$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/UnitMetadata$Builder;-><init>()V

    .line 31
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/UnitMetadata$Builder;->default_message(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/UnitMetadata$Builder;

    move-result-object v0

    .line 32
    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/UnitMetadata$Builder;->build()Lcom/squareup/protos/client/invoice/UnitMetadata;

    move-result-object v0

    .line 34
    iget-object v1, p0, Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;->invoiceServiceHelper:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    invoke-virtual {v1, v0}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->updateUnitMetadata(Lcom/squareup/protos/client/invoice/UnitMetadata;)Lrx/Observable;

    move-result-object v0

    .line 35
    iget-object v1, p0, Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    sget-object v2, Lcom/squareup/invoices/InvoiceDefaultMessageUpdater$update$1;->INSTANCE:Lcom/squareup/invoices/InvoiceDefaultMessageUpdater$update$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, v2}, Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    const-string v1, "invoiceServiceHelper.upd\u2026il { it.status.success })"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 37
    invoke-virtual {v0}, Lio/reactivex/Observable;->singleOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 38
    new-instance v1, Lcom/squareup/invoices/InvoiceDefaultMessageUpdater$update$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/InvoiceDefaultMessageUpdater$update$2;-><init>(Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    .line 39
    new-instance v0, Lcom/squareup/invoices/InvoiceDefaultMessageUpdater$update$3;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/InvoiceDefaultMessageUpdater$update$3;-><init>(Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "invoiceServiceHelper.upd\u2026  }\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
