.class public abstract Lcom/squareup/invoices/InvoicesAppletModule;
.super Ljava/lang/Object;
.source "InvoicesAppletModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideShowEstimatesBannerPreference(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .annotation runtime Lcom/squareup/invoices/qualifier/ShowEstimatesBanner;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 36
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "invoices-show-estimates-banner"

    invoke-virtual {p0, v1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method abstract bindChooseDateDataConverter(Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateDataConverter;)Lcom/squareup/invoices/workflow/edit/ChooseDateDataConverter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindInvoiceClientActionLinkTranslator(Lcom/squareup/invoices/ui/InvoicesClientActionLinkTranslator;)Lcom/squareup/clientactiontranslation/ClientActionTranslator;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract bindInvoicesApplet(Lcom/squareup/invoices/ui/RealInvoicesApplet;)Lcom/squareup/invoicesappletapi/InvoicesApplet;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideInvoicesAppletRunner(Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;)Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
