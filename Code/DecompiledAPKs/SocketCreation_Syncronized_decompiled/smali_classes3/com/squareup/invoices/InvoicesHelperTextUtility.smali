.class public Lcom/squareup/invoices/InvoicesHelperTextUtility;
.super Ljava/lang/Object;
.source "InvoicesHelperTextUtility.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static formatEndDate(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/CharSequence;
    .locals 5

    .line 248
    sget-object v0, Lcom/squareup/invoices/InvoicesHelperTextUtility$1;->$SwitchMap$com$squareup$invoices$workflow$edit$RecurrenceInterval$IntervalUnit:[I

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->getFrequency()Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->getUnit()Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x4

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eq v0, v3, :cond_2

    if-eq v0, v2, :cond_3

    const/4 v4, 0x3

    if-eq v0, v4, :cond_1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x2

    goto :goto_0

    :cond_2
    const/4 v1, 0x5

    .line 263
    :cond_3
    :goto_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    const-string v4, "MMM d, yyyy"

    invoke-direct {v0, v4, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 264
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->getRecurrenceEnd()Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;

    move-result-object v2

    .line 265
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->getFrequency()Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    move-result-object p0

    .line 266
    instance-of v4, v2, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AfterCount;

    if-eqz v4, :cond_4

    .line 268
    check-cast v2, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AfterCount;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AfterCount;->getCount()I

    move-result v2

    sub-int/2addr v2, v3

    .line 269
    invoke-static {p1}, Lcom/squareup/util/ProtoDates;->calendarFromYmd(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Calendar;

    move-result-object p1

    .line 270
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->getInterval()I

    move-result p0

    mul-int v2, v2, p0

    invoke-virtual {p1, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 271
    invoke-virtual {p1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 273
    :cond_4
    check-cast v2, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AtDate;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AtDate;->getDate()Ljava/util/Date;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static formatStartDate(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/CharSequence;
    .locals 3

    .line 241
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    const-string v2, "MMM d, yyyy"

    invoke-direct {v0, v2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 242
    invoke-static {p0}, Lcom/squareup/util/ProtoDates;->getDateForYearMonthDay(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Date;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static formatWithSmallDollar(Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .line 44
    invoke-interface {p0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p0

    const/4 p1, 0x0

    .line 47
    invoke-interface {p0, p1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v1, 0x24

    if-ne v0, v1, :cond_0

    .line 48
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 49
    new-instance p0, Lcom/squareup/widgets/TopAlignRelativeSizeSpan;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-direct {p0, v1}, Lcom/squareup/widgets/TopAlignRelativeSizeSpan;-><init>(F)V

    const/4 v1, 0x1

    const/16 v2, 0x21

    invoke-virtual {v0, p0, p1, v1, v2}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    return-object v0

    :cond_0
    return-object p0
.end method

.method public static getRecurringPeriodLongText(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Lcom/squareup/util/Res;Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/CharSequence;
    .locals 9

    if-nez p0, :cond_0

    const-string p0, ""

    return-object p0

    .line 120
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->getFrequency()Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->getUnit()Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    move-result-object v0

    .line 121
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->getFrequency()Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->getInterval()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_1

    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    .line 123
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->getRecurrenceEnd()Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;

    move-result-object v4

    sget-object v5, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$Never;->INSTANCE:Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$Never;

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    xor-int/2addr v4, v2

    .line 125
    sget v5, Lcom/squareup/common/invoices/R$string;->invoice_recurring_period_daily_plural_ending:I

    invoke-interface {p1, v5}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v5

    .line 127
    invoke-static {p2}, Lcom/squareup/util/ProtoDates;->getDateForYearMonthDay(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Date;

    move-result-object v6

    .line 129
    sget-object v7, Lcom/squareup/invoices/InvoicesHelperTextUtility$1;->$SwitchMap$com$squareup$invoices$workflow$edit$RecurrenceInterval$IntervalUnit:[I

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->ordinal()I

    move-result v0

    aget v0, v7, v0

    const-string v7, "end_date"

    const-string v8, "count"

    if-eq v0, v2, :cond_12

    const/4 v2, 0x2

    if-eq v0, v2, :cond_e

    const/4 v2, 0x3

    if-eq v0, v2, :cond_6

    const/4 v2, 0x4

    if-eq v0, v2, :cond_2

    goto/16 :goto_c

    :cond_2
    if-eqz v4, :cond_4

    if-eqz v3, :cond_3

    .line 217
    sget v0, Lcom/squareup/common/invoices/R$string;->invoice_recurring_period_yearly_plural_ending:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 219
    invoke-virtual {p1, v8, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    goto :goto_1

    .line 221
    :cond_3
    sget v0, Lcom/squareup/common/invoices/R$string;->invoice_recurring_period_yearly_ending:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 223
    :goto_1
    invoke-static {p0, p2}, Lcom/squareup/invoices/InvoicesHelperTextUtility;->formatEndDate(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/CharSequence;

    move-result-object p0

    invoke-virtual {p1, v7, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-object v5, p1

    goto :goto_3

    :cond_4
    if-eqz v3, :cond_5

    .line 226
    sget p0, Lcom/squareup/common/invoices/R$string;->invoice_recurring_period_yearly_plural:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 227
    invoke-virtual {p0, v8, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    goto :goto_2

    .line 229
    :cond_5
    sget p0, Lcom/squareup/common/invoices/R$string;->invoice_recurring_period_yearly:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    :goto_2
    move-object v5, p0

    .line 232
    :goto_3
    new-instance p0, Ljava/text/SimpleDateFormat;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p1

    const-string v0, "MMMM d"

    invoke-direct {p0, v0, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 233
    invoke-virtual {p0, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    const-string p1, "month_and_day"

    invoke-virtual {v5, p1, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    goto/16 :goto_c

    .line 171
    :cond_6
    invoke-static {p2}, Lcom/squareup/invoices/InvoicesHelperTextUtility;->isScheduledAtEndOfMonth(Lcom/squareup/protos/common/time/YearMonthDay;)Z

    move-result v0

    if-eqz v0, :cond_a

    if-eqz v4, :cond_8

    if-eqz v3, :cond_7

    .line 174
    sget v0, Lcom/squareup/common/invoices/R$string;->invoice_recurring_period_monthly_plural_end_of_month_ending:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 176
    invoke-virtual {p1, v8, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    goto :goto_4

    .line 178
    :cond_7
    sget v0, Lcom/squareup/common/invoices/R$string;->invoice_recurring_period_monthly_end_of_month_ending:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    :goto_4
    move-object v5, p1

    .line 181
    invoke-static {p0, p2}, Lcom/squareup/invoices/InvoicesHelperTextUtility;->formatEndDate(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/CharSequence;

    move-result-object p0

    invoke-virtual {v5, v7, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    goto/16 :goto_c

    :cond_8
    if-eqz v3, :cond_9

    .line 184
    sget p0, Lcom/squareup/common/invoices/R$string;->invoice_recurring_period_monthly_plural_end_of_month:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v5

    .line 186
    invoke-virtual {v5, v8, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    goto/16 :goto_c

    .line 188
    :cond_9
    sget p0, Lcom/squareup/common/invoices/R$string;->invoice_recurring_period_monthly_end_of_month:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v5

    goto/16 :goto_c

    :cond_a
    if-eqz v4, :cond_c

    if-eqz v3, :cond_b

    .line 195
    sget v0, Lcom/squareup/common/invoices/R$string;->invoice_recurring_period_monthly_plural_ending:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 197
    invoke-virtual {p1, v8, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    goto :goto_5

    .line 199
    :cond_b
    sget v0, Lcom/squareup/common/invoices/R$string;->invoice_recurring_period_monthly_ending:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 201
    :goto_5
    invoke-static {p0, p2}, Lcom/squareup/invoices/InvoicesHelperTextUtility;->formatEndDate(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/CharSequence;

    move-result-object p0

    invoke-virtual {p1, v7, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-object v5, p1

    goto :goto_7

    :cond_c
    if-eqz v3, :cond_d

    .line 204
    sget p0, Lcom/squareup/common/invoices/R$string;->invoice_recurring_period_monthly_plural:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 205
    invoke-virtual {p0, v8, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    goto :goto_6

    .line 207
    :cond_d
    sget p0, Lcom/squareup/common/invoices/R$string;->invoice_recurring_period_monthly:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    :goto_6
    move-object v5, p0

    .line 210
    :goto_7
    new-instance p0, Ljava/text/SimpleDateFormat;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p1

    const-string v0, "d"

    invoke-direct {p0, v0, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 211
    invoke-virtual {p0, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    const-string p1, "day_of_month"

    invoke-virtual {v5, p1, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    goto/16 :goto_c

    :cond_e
    if-eqz v4, :cond_10

    if-eqz v3, :cond_f

    .line 152
    sget v0, Lcom/squareup/common/invoices/R$string;->invoice_recurring_period_weekly_plural_ending:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 154
    invoke-virtual {p1, v8, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    goto :goto_8

    .line 156
    :cond_f
    sget v0, Lcom/squareup/common/invoices/R$string;->invoice_recurring_period_weekly_ending:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 158
    :goto_8
    invoke-static {p0, p2}, Lcom/squareup/invoices/InvoicesHelperTextUtility;->formatEndDate(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/CharSequence;

    move-result-object p0

    invoke-virtual {p1, v7, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-object v5, p1

    goto :goto_a

    :cond_10
    if-eqz v3, :cond_11

    .line 161
    sget p0, Lcom/squareup/common/invoices/R$string;->invoice_recurring_period_weekly_plural:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 162
    invoke-virtual {p0, v8, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    goto :goto_9

    .line 164
    :cond_11
    sget p0, Lcom/squareup/common/invoices/R$string;->invoice_recurring_period_weekly:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    :goto_9
    move-object v5, p0

    .line 167
    :goto_a
    new-instance p0, Ljava/text/SimpleDateFormat;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p1

    const-string v0, "EEEE"

    invoke-direct {p0, v0, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 168
    invoke-virtual {p0, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    const-string p1, "day_of_week"

    invoke-virtual {v5, p1, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    goto :goto_c

    :cond_12
    if-eqz v4, :cond_14

    if-eqz v3, :cond_13

    .line 133
    sget v0, Lcom/squareup/common/invoices/R$string;->invoice_recurring_period_daily_plural_ending:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 135
    invoke-virtual {p1, v8, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    goto :goto_b

    .line 137
    :cond_13
    sget v0, Lcom/squareup/common/invoices/R$string;->invoice_recurring_period_daily_ending:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    :goto_b
    move-object v5, p1

    .line 139
    invoke-static {p0, p2}, Lcom/squareup/invoices/InvoicesHelperTextUtility;->formatEndDate(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/CharSequence;

    move-result-object p0

    invoke-virtual {v5, v7, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    goto :goto_c

    :cond_14
    if-eqz v3, :cond_15

    .line 142
    sget p0, Lcom/squareup/common/invoices/R$string;->invoice_recurring_period_daily_plural:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v5

    .line 143
    invoke-virtual {v5, v8, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    goto :goto_c

    .line 145
    :cond_15
    sget p0, Lcom/squareup/common/invoices/R$string;->invoice_recurring_period_daily:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v5

    .line 236
    :goto_c
    invoke-static {p2}, Lcom/squareup/invoices/InvoicesHelperTextUtility;->formatStartDate(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/CharSequence;

    move-result-object p0

    const-string p1, "start_date"

    invoke-virtual {v5, p1, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    .line 237
    invoke-virtual {v5}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0
.end method

.method public static getRecurringPeriodShortText(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Lcom/squareup/util/Res;Ljava/util/Date;Z)Ljava/lang/CharSequence;
    .locals 4

    .line 58
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 59
    invoke-virtual {v0, p2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    if-eqz p3, :cond_0

    .line 62
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->getRecurrenceEnd()Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;

    move-result-object p2

    instance-of p2, p2, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$Never;

    if-nez p2, :cond_0

    .line 63
    sget p2, Lcom/squareup/common/invoices/R$string;->recurring_ending_on_date:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 64
    invoke-static {v0}, Lcom/squareup/util/ProtoDates;->calendarToYmd(Ljava/util/Calendar;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p3

    invoke-static {p0, p3}, Lcom/squareup/invoices/InvoicesHelperTextUtility;->formatEndDate(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/CharSequence;

    move-result-object p3

    const-string v0, "date"

    invoke-virtual {p2, v0, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 65
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    goto :goto_0

    :cond_0
    const-string p2, "."

    .line 68
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->getFrequency()Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->getInterval()I

    move-result p3

    const/4 v0, 0x1

    if-le p3, v0, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    .line 71
    :goto_1
    sget v2, Lcom/squareup/common/invoices/R$string;->invoice_recurring_period_daily:I

    invoke-interface {p1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 73
    sget-object v3, Lcom/squareup/invoices/InvoicesHelperTextUtility$1;->$SwitchMap$com$squareup$invoices$workflow$edit$RecurrenceInterval$IntervalUnit:[I

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->getFrequency()Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->getUnit()Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->ordinal()I

    move-result p0

    aget p0, v3, p0

    const-string v3, "count"

    if-eq p0, v0, :cond_8

    const/4 v0, 0x2

    if-eq p0, v0, :cond_6

    const/4 v0, 0x3

    if-eq p0, v0, :cond_4

    const/4 v0, 0x4

    if-eq p0, v0, :cond_2

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_3

    .line 103
    sget p0, Lcom/squareup/common/invoices/R$string;->invoice_recurring_period_yearly_plural_short:I

    .line 104
    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 105
    invoke-virtual {p0, v3, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    goto :goto_2

    .line 107
    :cond_3
    sget p0, Lcom/squareup/common/invoices/R$string;->invoice_recurring_period_yearly_short:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    goto :goto_2

    :cond_4
    if-eqz v1, :cond_5

    .line 94
    sget p0, Lcom/squareup/common/invoices/R$string;->invoice_recurring_period_monthly_plural_short:I

    .line 95
    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 96
    invoke-virtual {p0, v3, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    goto :goto_2

    .line 98
    :cond_5
    sget p0, Lcom/squareup/common/invoices/R$string;->invoice_recurring_period_monthly_short:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    goto :goto_2

    :cond_6
    if-eqz v1, :cond_7

    .line 85
    sget p0, Lcom/squareup/common/invoices/R$string;->invoice_recurring_period_weekly_plural_short:I

    .line 86
    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 87
    invoke-virtual {p0, v3, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    goto :goto_2

    .line 89
    :cond_7
    sget p0, Lcom/squareup/common/invoices/R$string;->invoice_recurring_period_weekly_short:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    goto :goto_2

    :cond_8
    if-eqz v1, :cond_9

    .line 76
    sget p0, Lcom/squareup/common/invoices/R$string;->invoice_recurring_period_daily_plural_short:I

    .line 77
    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 78
    invoke-virtual {p0, v3, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    goto :goto_2

    .line 80
    :cond_9
    sget p0, Lcom/squareup/common/invoices/R$string;->invoice_recurring_period_daily_short:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    :goto_2
    const-string p0, "end_phrase"

    .line 112
    invoke-virtual {v2, p0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static isScheduledAtEndOfMonth(Lcom/squareup/protos/common/time/YearMonthDay;)Z
    .locals 1

    .line 277
    iget-object p0, p0, Lcom/squareup/protos/common/time/YearMonthDay;->day_of_month:Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    const/16 v0, 0x1c

    if-le p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method
