.class final Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onSendInvoiceClicked$2;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunnerV2.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->onSendInvoiceClicked()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;",
        "experimentShowing",
        "",
        "apply",
        "(Ljava/lang/Boolean;)Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onSendInvoiceClicked$2;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Boolean;)Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;
    .locals 1

    const-string v0, "experimentShowing"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 641
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 642
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onSendInvoiceClicked$2;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getSetupGuideDialogScreenDataFactory$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;

    move-result-object p1

    .line 643
    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$SetupPaymentsDialogKey;->SEND_INVOICE:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$SetupPaymentsDialogKey;

    .line 642
    invoke-virtual {p1, v0}, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;->forSetupPaymentsPromptWithNoIDVSendingEnabled(Ljava/lang/Object;)Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;

    move-result-object p1

    goto :goto_0

    .line 646
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onSendInvoiceClicked$2;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getSetupGuideDialogScreenDataFactory$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;

    move-result-object p1

    .line 647
    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$SetupPaymentsDialogKey;->SEND_INVOICE:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$SetupPaymentsDialogKey;

    .line 646
    invoke-virtual {p1, v0}, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;->forSetupPaymentsPromptWhenSendingInvoice(Ljava/lang/Object;)Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 236
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onSendInvoiceClicked$2;->apply(Ljava/lang/Boolean;)Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;

    move-result-object p1

    return-object p1
.end method
