.class final Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$sendInvoiceSingle$3;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunnerV2.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->sendInvoiceSingle(Lcom/squareup/util/Optional;Lcom/squareup/protos/client/invoice/Invoice;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/invoices/editv2/service/SendInvoiceResponse;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditInvoiceScopeRunnerV2.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditInvoiceScopeRunnerV2.kt\ncom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$sendInvoiceSingle$3\n*L\n1#1,1749:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "response",
        "Lcom/squareup/invoices/editv2/service/SendInvoiceResponse;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $optionalRule:Lcom/squareup/util/Optional;

.field final synthetic $preparedInvoice:Lcom/squareup/protos/client/invoice/Invoice;

.field final synthetic this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/util/Optional;Lcom/squareup/protos/client/invoice/Invoice;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$sendInvoiceSingle$3;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    iput-object p2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$sendInvoiceSingle$3;->$optionalRule:Lcom/squareup/util/Optional;

    iput-object p3, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$sendInvoiceSingle$3;->$preparedInvoice:Lcom/squareup/protos/client/invoice/Invoice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/invoices/editv2/service/SendInvoiceResponse;)V
    .locals 2

    .line 943
    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/service/SendInvoiceResponse;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$sendInvoiceSingle$3;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getWorkingInvoiceEditor$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$sendInvoiceSingle$3;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getEditInvoiceContext$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->set(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)V

    .line 944
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$sendInvoiceSingle$3;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$sendInvoiceSingle$3;->$optionalRule:Lcom/squareup/util/Optional;

    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$sendInvoiceSingle$3;->$preparedInvoice:Lcom/squareup/protos/client/invoice/Invoice;

    invoke-static {p1, v0, v1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$logSend(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/util/Optional;Lcom/squareup/protos/client/invoice/Invoice;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 236
    check-cast p1, Lcom/squareup/invoices/editv2/service/SendInvoiceResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$sendInvoiceSingle$3;->accept(Lcom/squareup/invoices/editv2/service/SendInvoiceResponse;)V

    return-void
.end method
