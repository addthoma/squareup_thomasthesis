.class public final Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunnerV2.kt"

# interfaces
.implements Lmortar/bundler/Bundler;
.implements Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen$Runner;
.implements Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Screen$Runner;
.implements Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;
.implements Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$Runner;
.implements Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog$Runner;
.implements Lcom/squareup/setupguide/SetupPaymentsDialog$Runner;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$SetupPaymentsDialogKey;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditInvoiceScopeRunnerV2.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditInvoiceScopeRunnerV2.kt\ncom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2\n+ 2 Flows.kt\ncom/squareup/container/Flows\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,1749:1\n57#2:1750\n57#2:1751\n57#2:1752\n1360#3:1753\n1429#3,3:1754\n1360#3:1757\n1429#3,3:1758\n*E\n*S KotlinDebug\n*F\n+ 1 EditInvoiceScopeRunnerV2.kt\ncom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2\n*L\n585#1:1750\n793#1:1751\n998#1:1752\n1168#1:1753\n1168#1,3:1754\n1176#1:1757\n1176#1,3:1758\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0084\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000e\n\u0002\u0008\r\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0012\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u00052\u00020\u00062\u00020\u0007:\u0002\u0095\u0002B\u00b1\u0002\u0008\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u0012\u0006\u0010\u001a\u001a\u00020\u001b\u0012\u0006\u0010\u001c\u001a\u00020\u001d\u0012\u0006\u0010\u001e\u001a\u00020\u001f\u0012\u0006\u0010 \u001a\u00020!\u0012\u0006\u0010\"\u001a\u00020#\u0012\u0006\u0010$\u001a\u00020%\u0012\u0006\u0010&\u001a\u00020\'\u0012\u0006\u0010(\u001a\u00020)\u0012\u0006\u0010*\u001a\u00020+\u0012\u0006\u0010,\u001a\u00020-\u0012\u0006\u0010.\u001a\u00020/\u0012\u0006\u00100\u001a\u000201\u0012\u0006\u00102\u001a\u000203\u0012\u0006\u00104\u001a\u000205\u0012\u0006\u00106\u001a\u000207\u0012\u0006\u00108\u001a\u000209\u0012\u0006\u0010:\u001a\u00020;\u0012\u0006\u0010<\u001a\u00020=\u0012\u0006\u0010>\u001a\u00020?\u0012\u0006\u0010@\u001a\u00020A\u0012\u0006\u0010B\u001a\u00020C\u0012\u0006\u0010D\u001a\u00020E\u0012\u0006\u0010F\u001a\u00020G\u0012\u0006\u0010H\u001a\u00020I\u0012\u0008\u0008\u0001\u0010J\u001a\u00020K\u0012\u0006\u0010L\u001a\u00020M\u0012\u0006\u0010N\u001a\u00020O\u0012\u0006\u0010P\u001a\u00020Q\u00a2\u0006\u0002\u0010RJ\u0008\u0010d\u001a\u00020eH\u0002J\u0008\u0010f\u001a\u00020eH\u0002J\u0008\u0010g\u001a\u00020eH\u0002J\u0008\u0010h\u001a\u00020eH\u0002J\u0008\u0010i\u001a\u00020eH\u0002J\u0006\u0010j\u001a\u00020eJ\u000e\u0010k\u001a\u00020e2\u0006\u0010l\u001a\u00020UJ\u0008\u0010m\u001a\u00020eH\u0016J\u000e\u0010W\u001a\u0008\u0012\u0004\u0012\u00020X0nH\u0016J\u0008\u0010o\u001a\u00020eH\u0016J\u0008\u0010p\u001a\u00020eH\u0002J\u0008\u0010q\u001a\u00020eH\u0002J\u0010\u0010r\u001a\u00020e2\u0006\u0010s\u001a\u00020tH\u0002J\u000e\u0010u\u001a\u0008\u0012\u0004\u0012\u00020v0nH\u0016J\u000e\u0010w\u001a\u0008\u0012\u0004\u0012\u00020x0nH\u0016J\u0008\u0010y\u001a\u00020eH\u0002J\u0008\u0010z\u001a\u00020eH\u0002J\u0008\u0010{\u001a\u00020eH\u0002J\u0008\u0010|\u001a\u00020eH\u0002J\u0008\u0010}\u001a\u00020eH\u0002J\u0008\u0010~\u001a\u00020\u007fH\u0016J\t\u0010\u0080\u0001\u001a\u00020eH\u0016J\u0012\u0010\u0081\u0001\u001a\u00020e2\u0007\u0010\u0082\u0001\u001a\u00020UH\u0016J\t\u0010\u0083\u0001\u001a\u00020eH\u0016J\t\u0010\u0084\u0001\u001a\u00020eH\u0016J\t\u0010\u0085\u0001\u001a\u00020eH\u0016J\u0007\u0010\u0086\u0001\u001a\u00020eJ\u0012\u0010\u0087\u0001\u001a\u00020e2\u0007\u0010\u0088\u0001\u001a\u00020UH\u0002J\t\u0010\u0089\u0001\u001a\u00020eH\u0016J\t\u0010\u008a\u0001\u001a\u00020eH\u0002J\u0013\u0010\u008b\u0001\u001a\u00020e2\u0008\u0010\u008c\u0001\u001a\u00030\u008d\u0001H\u0002J\u0013\u0010\u008e\u0001\u001a\u00020e2\u0008\u0010\u008c\u0001\u001a\u00030\u008d\u0001H\u0002J\u0013\u0010\u008f\u0001\u001a\u00020e2\u0008\u0010\u0090\u0001\u001a\u00030\u0091\u0001H\u0002J!\u0010\u0092\u0001\u001a\u00020e2\u0008\u0010\u0090\u0001\u001a\u00030\u0091\u00012\u000c\u0008\u0002\u0010\u0093\u0001\u001a\u0005\u0018\u00010\u0094\u0001H\u0002J\u0013\u0010\u0095\u0001\u001a\u00020e2\u0008\u0010\u0096\u0001\u001a\u00030\u0097\u0001H\u0002J\u0013\u0010\u0098\u0001\u001a\u00020e2\u0008\u0010\u0099\u0001\u001a\u00030\u009a\u0001H\u0002J\u0013\u0010\u009b\u0001\u001a\u00020e2\u0008\u0010\u009c\u0001\u001a\u00030\u009d\u0001H\u0002J\u0013\u0010\u009e\u0001\u001a\u00020e2\u0008\u0010\u0099\u0001\u001a\u00030\u009f\u0001H\u0002J\u0013\u0010\u00a0\u0001\u001a\u00020e2\u0008\u0010\u00a1\u0001\u001a\u00030\u00a2\u0001H\u0002J\u0013\u0010\u00a3\u0001\u001a\u00020e2\u0008\u0010\u0099\u0001\u001a\u00030\u00a4\u0001H\u0002J\u0013\u0010\u00a5\u0001\u001a\u00020e2\u0008\u0010\u0099\u0001\u001a\u00030\u00a6\u0001H\u0002J\u0013\u0010\u00a7\u0001\u001a\u00020e2\u0008\u0010\u00a8\u0001\u001a\u00030\u00a9\u0001H\u0002J\u0013\u0010\u00aa\u0001\u001a\u00020e2\u0008\u0010\u0099\u0001\u001a\u00030\u00ab\u0001H\u0002J\u0013\u0010\u00ac\u0001\u001a\u00020e2\u0008\u0010\u00ad\u0001\u001a\u00030\u00ae\u0001H\u0002J\u0013\u0010\u00af\u0001\u001a\u00020e2\u0008\u0010\u00b0\u0001\u001a\u00030\u00b1\u0001H\u0002J\u0013\u0010\u00b2\u0001\u001a\u00020e2\u0008\u0010\u00b3\u0001\u001a\u00030\u00b4\u0001H\u0002J\u001d\u0010\u00b5\u0001\u001a\u00020e2\u0008\u0010\u00b6\u0001\u001a\u00030\u00b7\u00012\u0008\u0010\u0099\u0001\u001a\u00030\u00b8\u0001H\u0002J\u0013\u0010\u00b9\u0001\u001a\u00020e2\u0008\u0010\u0099\u0001\u001a\u00030\u00ba\u0001H\u0002J\u0013\u0010\u00bb\u0001\u001a\u00020e2\u0008\u0010\u0099\u0001\u001a\u00030\u00bc\u0001H\u0002J\u0013\u0010\u00bd\u0001\u001a\u00020e2\u0008\u0010\u00be\u0001\u001a\u00030\u00bf\u0001H\u0002J\u0013\u0010\u00c0\u0001\u001a\u00020e2\u0008\u0010\u00c1\u0001\u001a\u00030\u00c2\u0001H\u0002J\u0013\u0010\u00c3\u0001\u001a\u00020e2\u0008\u0010\u00c4\u0001\u001a\u00030\u00c5\u0001H\u0002J\u0013\u0010\u00c6\u0001\u001a\u00020e2\u0008\u0010\u00c7\u0001\u001a\u00030\u00c8\u0001H\u0002J\u0013\u0010\u00c9\u0001\u001a\u00020e2\u0008\u0010\u00c7\u0001\u001a\u00030\u00ca\u0001H\u0002J\u0013\u0010\u00cb\u0001\u001a\u00020e2\u0008\u0010\u00c7\u0001\u001a\u00030\u00cc\u0001H\u0002J\u0013\u0010\u00cd\u0001\u001a\u00020e2\u0008\u0010\u00ce\u0001\u001a\u00030\u00cf\u0001H\u0002J\u0013\u0010\u00d0\u0001\u001a\u00020e2\u0008\u0010\u00ce\u0001\u001a\u00030\u00cf\u0001H\u0002J\u0013\u0010\u00d1\u0001\u001a\u00020e2\u0008\u0010\u00ce\u0001\u001a\u00030\u00cf\u0001H\u0002J\u001c\u0010\u00d2\u0001\u001a\u00020e2\u0008\u0010\u00d3\u0001\u001a\u00030\u00d4\u00012\u0007\u0010\u00d5\u0001\u001a\u00020UH\u0002J\t\u0010\u00d6\u0001\u001a\u00020eH\u0016J\u0013\u0010\u00d7\u0001\u001a\u00020e2\u0008\u0010\u00d8\u0001\u001a\u00030\u00d9\u0001H\u0016J\u0013\u0010\u00da\u0001\u001a\u00020e2\u0008\u0010\u00db\u0001\u001a\u00030\u00dc\u0001H\u0016J\t\u0010\u00dd\u0001\u001a\u00020eH\u0016J\u0015\u0010\u00de\u0001\u001a\u00020e2\n\u0010\u00df\u0001\u001a\u0005\u0018\u00010\u00e0\u0001H\u0016J\u0013\u0010\u00e1\u0001\u001a\u00020e2\u0008\u0010\u00e2\u0001\u001a\u00030\u00e0\u0001H\u0016J\t\u0010\u00e3\u0001\u001a\u00020eH\u0016J\t\u0010\u00e4\u0001\u001a\u00020eH\u0016J\u0013\u0010\u00e5\u0001\u001a\u00020e2\u0008\u0010\u00c7\u0001\u001a\u00030\u00e6\u0001H\u0016J\u0013\u0010\u00e7\u0001\u001a\u00020e2\u0008\u0010\u00c7\u0001\u001a\u00030\u00e6\u0001H\u0016J\u0010\u0010\u00e8\u0001\u001a\t\u0012\u0005\u0012\u00030\u00e9\u00010nH\u0016J\t\u0010\u00ea\u0001\u001a\u00020eH\u0002J\u0007\u0010\u00eb\u0001\u001a\u00020eJ\u0011\u0010\u00ec\u0001\u001a\n\u0012\u0005\u0012\u00030\u008d\u00010\u00ed\u0001H\u0002J\u0007\u0010\u00ee\u0001\u001a\u00020eJ,\u0010\u00ef\u0001\u001a\n\u0012\u0005\u0012\u00030\u0091\u00010\u00ed\u00012\u000f\u0010\u00f0\u0001\u001a\n\u0012\u0005\u0012\u00030\u00f2\u00010\u00f1\u00012\u0008\u0010\u00f3\u0001\u001a\u00030\u00d4\u0001H\u0002J\u000f\u0010\u00f4\u0001\u001a\u0008\u0012\u0004\u0012\u00020c0nH\u0016J\t\u0010\u00f5\u0001\u001a\u00020UH\u0002J\t\u0010\u00f6\u0001\u001a\u00020eH\u0016J\u0013\u0010\u00f7\u0001\u001a\u00020e2\u0008\u0010\u008c\u0001\u001a\u00030\u008d\u0001H\u0002J\'\u0010\u00f8\u0001\u001a\u00020e2\u0008\u0010\u00f9\u0001\u001a\u00030\u00fa\u00012\u0008\u0010\u00fb\u0001\u001a\u00030\u00fc\u00012\u0008\u0010\u00fd\u0001\u001a\u00030\u00fe\u0001H\u0016J\u001d\u0010\u00ff\u0001\u001a\u00020e2\u0008\u0010\u0080\u0002\u001a\u00030\u0081\u00022\u0008\u0010\u00fd\u0001\u001a\u00030\u00fe\u0001H\u0016J&\u0010\u0082\u0002\u001a\u00020e2\u0008\u0010\u0080\u0002\u001a\u00030\u0081\u00022\u0007\u0010\u0083\u0002\u001a\u00020U2\u0008\u0010\u00fd\u0001\u001a\u00030\u00fe\u0001H\u0016J\u0013\u0010\u0084\u0002\u001a\u00020e2\u0008\u0010\u0085\u0002\u001a\u00030\u00b7\u0001H\u0016J\u0012\u0010\u0086\u0002\u001a\u00020e2\u0007\u0010\u0087\u0002\u001a\u00020UH\u0002J\u0012\u0010\u0088\u0002\u001a\u00020e2\u0007\u0010\u0089\u0002\u001a\u00020UH\u0002J\t\u0010\u0088\u0001\u001a\u00020eH\u0002J\u0012\u0010\u008a\u0002\u001a\u00020e2\u0007\u0010\u008b\u0002\u001a\u00020UH\u0002J\u0012\u0010\u008c\u0002\u001a\u00020e2\u0007\u0010\u008d\u0002\u001a\u00020UH\u0002J\t\u0010\u008e\u0002\u001a\u00020eH\u0002J$\u0010\u008f\u0002\u001a\u00020U2\u000f\u0010\u00f0\u0001\u001a\n\u0012\u0005\u0012\u00030\u00f2\u00010\u00f1\u00012\u0008\u0010\u00d3\u0001\u001a\u00030\u00d4\u0001H\u0002J\u0017\u0010\u0090\u0002\u001a\u00020e*\u00020E2\u0008\u0010\u00d3\u0001\u001a\u00030\u00d4\u0001H\u0002J\u001f\u0010\u0091\u0002\u001a\u00020e*\n\u0012\u0005\u0012\u00030\u00f2\u00010\u00f1\u00012\u0008\u0010\u00d3\u0001\u001a\u00030\u00d4\u0001H\u0002J\r\u0010\u0092\u0002\u001a\u00020e*\u00020EH\u0002J!\u0010\u0093\u0002\u001a\u00020e*\u00030\u0094\u00022\u0008\u0010\u00d3\u0001\u001a\u00030\u00d4\u00012\u0007\u0010\u00d5\u0001\u001a\u00020UH\u0002R\u000e\u0010H\u001a\u00020IX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010D\u001a\u00020EX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010S\u001a\u0010\u0012\u000c\u0012\n V*\u0004\u0018\u00010U0U0TX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010P\u001a\u00020QX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010:\u001a\u00020;X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010W\u001a\u0010\u0012\u000c\u0012\n V*\u0004\u0018\u00010X0X0TX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00100\u001a\u000201X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010.\u001a\u00020/X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010Y\u001a\u00020ZX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010[\u001a\u00020\\X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010]\u001a\u00020^X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010_\u001a\u00020`X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020%X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010L\u001a\u00020MX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010a\u001a\u00020UX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010*\u001a\u00020+X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00108\u001a\u000209X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00104\u001a\u000205X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\'X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010@\u001a\u00020AX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020#X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010>\u001a\u00020?X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010B\u001a\u00020CX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010J\u001a\u00020KX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00106\u001a\u000207X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010F\u001a\u00020GX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010(\u001a\u00020)X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010<\u001a\u00020=X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010,\u001a\u00020-X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010b\u001a\u0010\u0012\u000c\u0012\n V*\u0004\u0018\u00010c0c0TX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00102\u001a\u000203X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010N\u001a\u00020OX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0096\u0002"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;",
        "Lmortar/bundler/Bundler;",
        "Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen$Runner;",
        "Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Screen$Runner;",
        "Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;",
        "Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$Runner;",
        "Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog$Runner;",
        "Lcom/squareup/setupguide/SetupPaymentsDialog$Runner;",
        "editInvoice1ScreenDataFactory",
        "Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;",
        "editInvoice2ScreenDataFactory",
        "Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;",
        "editInvoiceOverflowDialogDataFactory",
        "Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$Factory;",
        "setupGuideDialogScreenDataFactory",
        "Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;",
        "workingInvoiceEditor",
        "Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;",
        "chooseCustomerFlow",
        "Lcom/squareup/ui/crm/ChooseCustomerFlow;",
        "workingOrderEditor",
        "Lcom/squareup/invoices/order/WorkingOrderEditor;",
        "flow",
        "Lflow/Flow;",
        "x2SellerScreenRunner",
        "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
        "workingRecurrenceRuleEditor",
        "Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;",
        "editInvoiceV2ServiceHelper",
        "Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;",
        "confirmationScreenDataFactory",
        "Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;",
        "invoicesAppletRunner",
        "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
        "invoiceUnitCache",
        "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
        "failureMessageFactory",
        "Lcom/squareup/receiving/FailureMessageFactory;",
        "invoicePreparer",
        "Lcom/squareup/invoices/editv2/service/InvoicePreparer;",
        "recurringHelper",
        "Lcom/squareup/invoices/editv2/recurring/RecurringHelper;",
        "instrumentsStore",
        "Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;",
        "res",
        "Lcom/squareup/util/Res;",
        "currentTime",
        "Lcom/squareup/time/CurrentTime;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "shareLinkMessageFactory",
        "Lcom/squareup/invoices/ui/edit/InvoiceShareLinkMessageFactory;",
        "invoiceFileValidator",
        "Lcom/squareup/invoices/image/InvoiceFileValidator;",
        "keypadRunner",
        "Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;",
        "invoiceFileHelper",
        "Lcom/squareup/invoices/image/InvoiceFileHelper;",
        "chooseDateInfoFactory",
        "Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;",
        "reminderInfoFactory",
        "Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;",
        "invoiceUrlHelper",
        "Lcom/squareup/invoices/InvoiceUrlHelper;",
        "invoiceShareUrlLauncher",
        "Lcom/squareup/url/InvoiceShareUrlLauncher;",
        "invoiceValidator",
        "Lcom/squareup/invoices/editv2/validation/InvoiceValidator;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "onboardingDiverter",
        "Lcom/squareup/onboarding/OnboardingDiverter;",
        "accountStatusSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "iposSkipOnboardingExperiment",
        "Lcom/squareup/experiments/ExperimentProfile;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "buyerFlowStarter",
        "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
        "(Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$Factory;Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;Lcom/squareup/ui/crm/ChooseCustomerFlow;Lcom/squareup/invoices/order/WorkingOrderEditor;Lflow/Flow;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/invoices/editv2/service/InvoicePreparer;Lcom/squareup/invoices/editv2/recurring/RecurringHelper;Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;Lcom/squareup/util/Res;Lcom/squareup/time/CurrentTime;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/invoices/ui/edit/InvoiceShareLinkMessageFactory;Lcom/squareup/invoices/image/InvoiceFileValidator;Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;Lcom/squareup/invoices/image/InvoiceFileHelper;Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;Lcom/squareup/invoices/InvoiceUrlHelper;Lcom/squareup/url/InvoiceShareUrlLauncher;Lcom/squareup/invoices/editv2/validation/InvoiceValidator;Lcom/squareup/analytics/Analytics;Lcom/squareup/onboarding/OnboardingDiverter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/experiments/ExperimentProfile;Lcom/squareup/settings/server/Features;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/buyer/BuyerFlowStarter;)V",
        "busy",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "confirmationScreenData",
        "Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;",
        "disposables",
        "Lio/reactivex/disposables/CompositeDisposable;",
        "editInvoiceContext",
        "Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;",
        "editInvoiceScopeV2",
        "Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;",
        "editInvoiceWorkflowRunner",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;",
        "inCheckout",
        "setupGuideDialogScreenData",
        "Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;",
        "addAdditionalRecipients",
        "",
        "addAttachment",
        "addCustomer",
        "addLineItem",
        "addPaymentSchedule",
        "attemptSendInvoice",
        "cancelConfigureItemCard",
        "isWorkingItem",
        "closeScreen",
        "Lio/reactivex/Observable;",
        "copyLink",
        "deleteDraft",
        "editAutomaticPayments",
        "editDate",
        "dateType",
        "Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType;",
        "editInvoice1ScreenData",
        "Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData;",
        "editInvoice2ScreenData",
        "Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;",
        "editInvoiceDetails",
        "editInvoiceMethod",
        "editPaymentSchedule",
        "editRecurrenceRule",
        "editReminders",
        "getMortarBundleKey",
        "",
        "goBackFrom2Screen",
        "goBackFromConfirmationScreen",
        "success",
        "goBackFromItemSelect",
        "goBackFromOverflowDialog",
        "goBackFromSetupPaymentsDialog",
        "goBackToEditInvoice1Screen",
        "goBackToEditInvoice2Screen",
        "updatePreview",
        "goToEditInvoice2Screen",
        "goToRecurringEndOfMonthDialog",
        "goToSaveDraftConfirmation",
        "response",
        "Lcom/squareup/invoices/editv2/service/SaveDraftResponse;",
        "goToSaveDraftInEdit",
        "goToSendInvoiceConfirmation",
        "sendInvoiceResponse",
        "Lcom/squareup/invoices/editv2/service/SendInvoiceResponse;",
        "goToSendInvoiceConfirmationInEdit",
        "instrumentSummary",
        "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
        "goToValidationErrorDialog",
        "validationResult",
        "Lcom/squareup/invoices/editv2/validation/ValidationResult$InvalidWithMessage;",
        "handleAttachmentResult",
        "result",
        "Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult;",
        "handleAttachmentRowClicked",
        "attachmentClicked",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$AttachmentClicked;",
        "handleAutomaticPaymentsResult",
        "Lcom/squareup/invoices/workflow/edit/EditAutomaticPaymentsResult;",
        "handleAutomaticRemindersOutput",
        "output",
        "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;",
        "handleCustomerChosen",
        "Lcom/squareup/ui/crm/ChooseCustomerFlow$Result;",
        "handleDeliveryMethodResult",
        "Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;",
        "handleDueDateResult",
        "dueDateResult",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$DueDateResult;",
        "handleEditInvoiceWorkflowResult",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult;",
        "handleInvoiceDetailsResult",
        "detailsWorkflowResult",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsResult;",
        "handleLineItemClicked",
        "lineItemClicked",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$LineItemClicked;",
        "handlePaymentRequestClicked",
        "paymentRequestClicked",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$PaymentRequestClicked;",
        "handlePaymentRequestResult",
        "index",
        "",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult;",
        "handlePaymentScheduleResult",
        "Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult;",
        "handleRecipientResults",
        "Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult;",
        "handleRecurringResult",
        "recurringOutput",
        "Lcom/squareup/invoices/workflow/edit/RecurringScheduleWorkflowOutput;",
        "handleScheduledDateResult",
        "scheduledDateResult",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$ScheduledDateResult;",
        "handleSimpleEvent",
        "simpleViewEvent",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$Simple;",
        "handleSimpleEventEditInvoice1Screen",
        "key",
        "Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;",
        "handleSimpleEventEditInvoice2Screen",
        "Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;",
        "handleSimpleEventOverflowDialogScreen",
        "Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$EventKey;",
        "handleToggleEvent",
        "toggleEvent",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$ToggleClicked;",
        "handleToggleEventEditInvoice1Screen",
        "handleToggleEventEditInvoice2Screen",
        "launchBuyerFlow",
        "invoice",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "isSaveDraft",
        "moreOptionsPressed",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onEvent",
        "event",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
        "onExitScope",
        "onLoad",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onSave",
        "outState",
        "onSaveDraftClicked",
        "onSendInvoiceClicked",
        "onSetupPaymentsDialogPrimaryClicked",
        "",
        "onSetupPaymentsDialogSecondaryClicked",
        "overflowDialogScreenData",
        "Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData;",
        "requestDeposit",
        "saveDraft",
        "saveDraftSingle",
        "Lio/reactivex/Single;",
        "sendInvoice",
        "sendInvoiceSingle",
        "optionalRule",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceRule;",
        "preparedInvoice",
        "setupPaymentsDialogScreenData",
        "shouldCompleteOnboardingSetUp",
        "showOverflowDialog",
        "showSaveDraftFailure",
        "startEditingDiscount",
        "workingDiscount",
        "Lcom/squareup/configure/item/WorkingDiscount;",
        "info",
        "Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;",
        "parentPath",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "startEditingItemVariablePrice",
        "workingItem",
        "Lcom/squareup/configure/item/WorkingItem;",
        "startEditingItemWithModifiers",
        "showPriceScreenFirst",
        "startEditingOrder",
        "pos",
        "updateAllowAutoPayments",
        "allowAutoPaymentsEnabled",
        "updateBuyerCofEnabled",
        "buyerCofEnabled",
        "updateRequestShippingAddress",
        "requestShippingAddress",
        "updateRequestTipping",
        "requestTipping",
        "viewCustomer",
        "willSeriesRecurEndOfMonth",
        "logInvoiceSentAction",
        "logSend",
        "logSeriesScheduled",
        "populateFromInvoice",
        "Lcom/squareup/payment/InvoicePayment;",
        "SetupPaymentsDialogKey",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

.field private final chooseCustomerFlow:Lcom/squareup/ui/crm/ChooseCustomerFlow;

.field private final chooseDateInfoFactory:Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;

.field private final confirmationScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;",
            ">;"
        }
    .end annotation
.end field

.field private final confirmationScreenDataFactory:Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final currentTime:Lcom/squareup/time/CurrentTime;

.field private final disposables:Lio/reactivex/disposables/CompositeDisposable;

.field private final editInvoice1ScreenDataFactory:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;

.field private final editInvoice2ScreenDataFactory:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;

.field private editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

.field private final editInvoiceOverflowDialogDataFactory:Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$Factory;

.field private editInvoiceScopeV2:Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

.field private final editInvoiceV2ServiceHelper:Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;

.field private editInvoiceWorkflowRunner:Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

.field private final failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private inCheckout:Z

.field private final instrumentsStore:Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;

.field private final invoiceFileHelper:Lcom/squareup/invoices/image/InvoiceFileHelper;

.field private final invoiceFileValidator:Lcom/squareup/invoices/image/InvoiceFileValidator;

.field private final invoicePreparer:Lcom/squareup/invoices/editv2/service/InvoicePreparer;

.field private final invoiceShareUrlLauncher:Lcom/squareup/url/InvoiceShareUrlLauncher;

.field private final invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

.field private final invoiceUrlHelper:Lcom/squareup/invoices/InvoiceUrlHelper;

.field private final invoiceValidator:Lcom/squareup/invoices/editv2/validation/InvoiceValidator;

.field private final invoicesAppletRunner:Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

.field private final iposSkipOnboardingExperiment:Lcom/squareup/experiments/ExperimentProfile;

.field private final keypadRunner:Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;

.field private final onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

.field private final recurringHelper:Lcom/squareup/invoices/editv2/recurring/RecurringHelper;

.field private final reminderInfoFactory:Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;

.field private final res:Lcom/squareup/util/Res;

.field private final setupGuideDialogScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;",
            ">;"
        }
    .end annotation
.end field

.field private final setupGuideDialogScreenDataFactory:Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;

.field private final shareLinkMessageFactory:Lcom/squareup/invoices/ui/edit/InvoiceShareLinkMessageFactory;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

.field private final workingOrderEditor:Lcom/squareup/invoices/order/WorkingOrderEditor;

.field private final workingRecurrenceRuleEditor:Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;

.field private final x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$Factory;Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;Lcom/squareup/ui/crm/ChooseCustomerFlow;Lcom/squareup/invoices/order/WorkingOrderEditor;Lflow/Flow;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/invoices/editv2/service/InvoicePreparer;Lcom/squareup/invoices/editv2/recurring/RecurringHelper;Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;Lcom/squareup/util/Res;Lcom/squareup/time/CurrentTime;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/invoices/ui/edit/InvoiceShareLinkMessageFactory;Lcom/squareup/invoices/image/InvoiceFileValidator;Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;Lcom/squareup/invoices/image/InvoiceFileHelper;Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;Lcom/squareup/invoices/InvoiceUrlHelper;Lcom/squareup/url/InvoiceShareUrlLauncher;Lcom/squareup/invoices/editv2/validation/InvoiceValidator;Lcom/squareup/analytics/Analytics;Lcom/squareup/onboarding/OnboardingDiverter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/experiments/ExperimentProfile;Lcom/squareup/settings/server/Features;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/buyer/BuyerFlowStarter;)V
    .locals 16
    .param p34    # Lcom/squareup/experiments/ExperimentProfile;
        .annotation runtime Lcom/squareup/experiments/IposSkipOnboardingExperiment;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move-object/from16 v0, p16

    const-string v0, "editInvoice1ScreenDataFactory"

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editInvoice2ScreenDataFactory"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editInvoiceOverflowDialogDataFactory"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "setupGuideDialogScreenDataFactory"

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workingInvoiceEditor"

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chooseCustomerFlow"

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workingOrderEditor"

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flow"

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "x2SellerScreenRunner"

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workingRecurrenceRuleEditor"

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editInvoiceV2ServiceHelper"

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "confirmationScreenDataFactory"

    invoke-static {v12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoicesAppletRunner"

    invoke-static {v13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceUnitCache"

    invoke-static {v14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "failureMessageFactory"

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoicePreparer"

    move-object/from16 v15, p16

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recurringHelper"

    move-object/from16 v15, p17

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "instrumentsStore"

    move-object/from16 v15, p18

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    move-object/from16 v15, p19

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentTime"

    move-object/from16 v15, p20

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    move-object/from16 v15, p21

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "shareLinkMessageFactory"

    move-object/from16 v15, p22

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceFileValidator"

    move-object/from16 v15, p23

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "keypadRunner"

    move-object/from16 v15, p24

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceFileHelper"

    move-object/from16 v15, p25

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chooseDateInfoFactory"

    move-object/from16 v15, p26

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reminderInfoFactory"

    move-object/from16 v15, p27

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceUrlHelper"

    move-object/from16 v15, p28

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceShareUrlLauncher"

    move-object/from16 v15, p29

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceValidator"

    move-object/from16 v15, p30

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    move-object/from16 v15, p31

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onboardingDiverter"

    move-object/from16 v15, p32

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountStatusSettings"

    move-object/from16 v15, p33

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "iposSkipOnboardingExperiment"

    move-object/from16 v15, p34

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    move-object/from16 v15, p35

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transaction"

    move-object/from16 v15, p36

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerFlowStarter"

    move-object/from16 v15, p37

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 237
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v15, p16

    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoice1ScreenDataFactory:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;

    iput-object v2, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoice2ScreenDataFactory:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;

    iput-object v3, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceOverflowDialogDataFactory:Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$Factory;

    iput-object v4, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->setupGuideDialogScreenDataFactory:Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;

    iput-object v5, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    iput-object v6, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->chooseCustomerFlow:Lcom/squareup/ui/crm/ChooseCustomerFlow;

    iput-object v7, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingOrderEditor:Lcom/squareup/invoices/order/WorkingOrderEditor;

    iput-object v8, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->flow:Lflow/Flow;

    iput-object v9, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iput-object v10, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingRecurrenceRuleEditor:Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;

    iput-object v11, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceV2ServiceHelper:Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;

    iput-object v12, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->confirmationScreenDataFactory:Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;

    iput-object v13, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->invoicesAppletRunner:Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    iput-object v14, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    move-object/from16 v1, p15

    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    iput-object v15, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->invoicePreparer:Lcom/squareup/invoices/editv2/service/InvoicePreparer;

    move-object/from16 v1, p17

    move-object/from16 v2, p18

    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->recurringHelper:Lcom/squareup/invoices/editv2/recurring/RecurringHelper;

    iput-object v2, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->instrumentsStore:Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;

    move-object/from16 v1, p19

    move-object/from16 v2, p20

    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->res:Lcom/squareup/util/Res;

    iput-object v2, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->currentTime:Lcom/squareup/time/CurrentTime;

    move-object/from16 v1, p21

    move-object/from16 v2, p22

    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object v2, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->shareLinkMessageFactory:Lcom/squareup/invoices/ui/edit/InvoiceShareLinkMessageFactory;

    move-object/from16 v1, p23

    move-object/from16 v2, p24

    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->invoiceFileValidator:Lcom/squareup/invoices/image/InvoiceFileValidator;

    iput-object v2, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->keypadRunner:Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;

    move-object/from16 v1, p25

    move-object/from16 v2, p26

    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->invoiceFileHelper:Lcom/squareup/invoices/image/InvoiceFileHelper;

    iput-object v2, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->chooseDateInfoFactory:Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;

    move-object/from16 v1, p27

    move-object/from16 v2, p28

    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->reminderInfoFactory:Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;

    iput-object v2, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->invoiceUrlHelper:Lcom/squareup/invoices/InvoiceUrlHelper;

    move-object/from16 v1, p29

    move-object/from16 v2, p30

    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->invoiceShareUrlLauncher:Lcom/squareup/url/InvoiceShareUrlLauncher;

    iput-object v2, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->invoiceValidator:Lcom/squareup/invoices/editv2/validation/InvoiceValidator;

    move-object/from16 v1, p31

    move-object/from16 v2, p32

    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object v2, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    move-object/from16 v1, p33

    move-object/from16 v2, p34

    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object v2, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->iposSkipOnboardingExperiment:Lcom/squareup/experiments/ExperimentProfile;

    move-object/from16 v1, p35

    move-object/from16 v2, p36

    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->features:Lcom/squareup/settings/server/Features;

    iput-object v2, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->transaction:Lcom/squareup/payment/Transaction;

    move-object/from16 v1, p37

    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

    .line 282
    new-instance v1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    const/4 v1, 0x0

    .line 284
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    const-string v2, "BehaviorRelay.createDefault(false)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 286
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    const-string v2, "BehaviorRelay.create<Edi\u2026ationScreen.ScreenData>()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->confirmationScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 287
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    const-string v2, "BehaviorRelay.create<Set\u2026mentsDialog.ScreenData>()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->setupGuideDialogScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$getBusy$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getChooseDateInfoFactory$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;
    .locals 0

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->chooseDateInfoFactory:Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;

    return-object p0
.end method

.method public static final synthetic access$getConfirmationScreenData$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->confirmationScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getConfirmationScreenDataFactory$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;
    .locals 0

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->confirmationScreenDataFactory:Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;

    return-object p0
.end method

.method public static final synthetic access$getCurrencyCode$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/protos/common/CurrencyCode;
    .locals 0

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-object p0
.end method

.method public static final synthetic access$getCurrentTime$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/time/CurrentTime;
    .locals 0

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->currentTime:Lcom/squareup/time/CurrentTime;

    return-object p0
.end method

.method public static final synthetic access$getEditInvoice1ScreenDataFactory$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;
    .locals 0

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoice1ScreenDataFactory:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;

    return-object p0
.end method

.method public static final synthetic access$getEditInvoice2ScreenDataFactory$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;
    .locals 0

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoice2ScreenDataFactory:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;

    return-object p0
.end method

.method public static final synthetic access$getEditInvoiceContext$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;
    .locals 1

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    if-nez p0, :cond_0

    const-string v0, "editInvoiceContext"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getEditInvoiceScopeV2$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;
    .locals 1

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceScopeV2:Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    if-nez p0, :cond_0

    const-string v0, "editInvoiceScopeV2"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getEditInvoiceV2ServiceHelper$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;
    .locals 0

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceV2ServiceHelper:Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;

    return-object p0
.end method

.method public static final synthetic access$getEditInvoiceWorkflowRunner$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;
    .locals 1

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceWorkflowRunner:Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

    if-nez p0, :cond_0

    const-string v0, "editInvoiceWorkflowRunner"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getFailureMessageFactory$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/receiving/FailureMessageFactory;
    .locals 0

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    return-object p0
.end method

.method public static final synthetic access$getFeatures$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/settings/server/Features;
    .locals 0

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->features:Lcom/squareup/settings/server/Features;

    return-object p0
.end method

.method public static final synthetic access$getFlow$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lflow/Flow;
    .locals 0

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->flow:Lflow/Flow;

    return-object p0
.end method

.method public static final synthetic access$getInCheckout$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Z
    .locals 0

    .line 236
    iget-boolean p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->inCheckout:Z

    return p0
.end method

.method public static final synthetic access$getInstrumentsStore$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;
    .locals 0

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->instrumentsStore:Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;

    return-object p0
.end method

.method public static final synthetic access$getInvoiceFileHelper$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/image/InvoiceFileHelper;
    .locals 0

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->invoiceFileHelper:Lcom/squareup/invoices/image/InvoiceFileHelper;

    return-object p0
.end method

.method public static final synthetic access$getInvoiceFileValidator$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/image/InvoiceFileValidator;
    .locals 0

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->invoiceFileValidator:Lcom/squareup/invoices/image/InvoiceFileValidator;

    return-object p0
.end method

.method public static final synthetic access$getInvoicePreparer$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/editv2/service/InvoicePreparer;
    .locals 0

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->invoicePreparer:Lcom/squareup/invoices/editv2/service/InvoicePreparer;

    return-object p0
.end method

.method public static final synthetic access$getInvoiceShareUrlLauncher$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/url/InvoiceShareUrlLauncher;
    .locals 0

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->invoiceShareUrlLauncher:Lcom/squareup/url/InvoiceShareUrlLauncher;

    return-object p0
.end method

.method public static final synthetic access$getInvoiceUnitCache$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoicesappletapi/InvoiceUnitCache;
    .locals 0

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    return-object p0
.end method

.method public static final synthetic access$getInvoiceUrlHelper$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/InvoiceUrlHelper;
    .locals 0

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->invoiceUrlHelper:Lcom/squareup/invoices/InvoiceUrlHelper;

    return-object p0
.end method

.method public static final synthetic access$getInvoiceValidator$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/editv2/validation/InvoiceValidator;
    .locals 0

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->invoiceValidator:Lcom/squareup/invoices/editv2/validation/InvoiceValidator;

    return-object p0
.end method

.method public static final synthetic access$getIposSkipOnboardingExperiment$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/experiments/ExperimentProfile;
    .locals 0

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->iposSkipOnboardingExperiment:Lcom/squareup/experiments/ExperimentProfile;

    return-object p0
.end method

.method public static final synthetic access$getOnboardingDiverter$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/onboarding/OnboardingDiverter;
    .locals 0

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    return-object p0
.end method

.method public static final synthetic access$getReminderInfoFactory$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;
    .locals 0

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->reminderInfoFactory:Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/util/Res;
    .locals 0

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$getSetupGuideDialogScreenData$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->setupGuideDialogScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getSetupGuideDialogScreenDataFactory$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;
    .locals 0

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->setupGuideDialogScreenDataFactory:Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;

    return-object p0
.end method

.method public static final synthetic access$getShareLinkMessageFactory$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/ui/edit/InvoiceShareLinkMessageFactory;
    .locals 0

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->shareLinkMessageFactory:Lcom/squareup/invoices/ui/edit/InvoiceShareLinkMessageFactory;

    return-object p0
.end method

.method public static final synthetic access$getWorkingInvoiceEditor$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;
    .locals 0

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    return-object p0
.end method

.method public static final synthetic access$getWorkingOrderEditor$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/order/WorkingOrderEditor;
    .locals 0

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingOrderEditor:Lcom/squareup/invoices/order/WorkingOrderEditor;

    return-object p0
.end method

.method public static final synthetic access$getWorkingRecurrenceRuleEditor$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;
    .locals 0

    .line 236
    iget-object p0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingRecurrenceRuleEditor:Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;

    return-object p0
.end method

.method public static final synthetic access$goToRecurringEndOfMonthDialog(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V
    .locals 0

    .line 236
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goToRecurringEndOfMonthDialog()V

    return-void
.end method

.method public static final synthetic access$goToSaveDraftConfirmation(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/invoices/editv2/service/SaveDraftResponse;)V
    .locals 0

    .line 236
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goToSaveDraftConfirmation(Lcom/squareup/invoices/editv2/service/SaveDraftResponse;)V

    return-void
.end method

.method public static final synthetic access$goToSendInvoiceConfirmation(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/invoices/editv2/service/SendInvoiceResponse;)V
    .locals 0

    .line 236
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goToSendInvoiceConfirmation(Lcom/squareup/invoices/editv2/service/SendInvoiceResponse;)V

    return-void
.end method

.method public static final synthetic access$goToSendInvoiceConfirmationInEdit(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/invoices/editv2/service/SendInvoiceResponse;Lcom/squareup/protos/client/instruments/InstrumentSummary;)V
    .locals 0

    .line 236
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goToSendInvoiceConfirmationInEdit(Lcom/squareup/invoices/editv2/service/SendInvoiceResponse;Lcom/squareup/protos/client/instruments/InstrumentSummary;)V

    return-void
.end method

.method public static final synthetic access$goToValidationErrorDialog(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/invoices/editv2/validation/ValidationResult$InvalidWithMessage;)V
    .locals 0

    .line 236
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goToValidationErrorDialog(Lcom/squareup/invoices/editv2/validation/ValidationResult$InvalidWithMessage;)V

    return-void
.end method

.method public static final synthetic access$handleCustomerChosen(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/ui/crm/ChooseCustomerFlow$Result;)V
    .locals 0

    .line 236
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->handleCustomerChosen(Lcom/squareup/ui/crm/ChooseCustomerFlow$Result;)V

    return-void
.end method

.method public static final synthetic access$handleEditInvoiceWorkflowResult(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/invoices/workflow/edit/EditInvoiceResult;)V
    .locals 0

    .line 236
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->handleEditInvoiceWorkflowResult(Lcom/squareup/invoices/workflow/edit/EditInvoiceResult;)V

    return-void
.end method

.method public static final synthetic access$logSend(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/util/Optional;Lcom/squareup/protos/client/invoice/Invoice;)V
    .locals 0

    .line 236
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->logSend(Lcom/squareup/util/Optional;Lcom/squareup/protos/client/invoice/Invoice;)V

    return-void
.end method

.method public static final synthetic access$saveDraftSingle(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lio/reactivex/Single;
    .locals 0

    .line 236
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->saveDraftSingle()Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$sendInvoiceSingle(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/util/Optional;Lcom/squareup/protos/client/invoice/Invoice;)Lio/reactivex/Single;
    .locals 0

    .line 236
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->sendInvoiceSingle(Lcom/squareup/util/Optional;Lcom/squareup/protos/client/invoice/Invoice;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$setEditInvoiceContext$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)V
    .locals 0

    .line 236
    iput-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    return-void
.end method

.method public static final synthetic access$setEditInvoiceScopeV2$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;)V
    .locals 0

    .line 236
    iput-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceScopeV2:Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    return-void
.end method

.method public static final synthetic access$setEditInvoiceWorkflowRunner$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;)V
    .locals 0

    .line 236
    iput-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceWorkflowRunner:Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

    return-void
.end method

.method public static final synthetic access$setInCheckout$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Z)V
    .locals 0

    .line 236
    iput-boolean p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->inCheckout:Z

    return-void
.end method

.method public static final synthetic access$shouldCompleteOnboardingSetUp(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Z
    .locals 0

    .line 236
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->shouldCompleteOnboardingSetUp()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$showSaveDraftFailure(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/invoices/editv2/service/SaveDraftResponse;)V
    .locals 0

    .line 236
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->showSaveDraftFailure(Lcom/squareup/invoices/editv2/service/SaveDraftResponse;)V

    return-void
.end method

.method public static final synthetic access$willSeriesRecurEndOfMonth(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/util/Optional;Lcom/squareup/protos/client/invoice/Invoice;)Z
    .locals 0

    .line 236
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->willSeriesRecurEndOfMonth(Lcom/squareup/util/Optional;Lcom/squareup/protos/client/invoice/Invoice;)Z

    move-result p0

    return p0
.end method

.method private final addAdditionalRecipients()V
    .locals 2

    .line 1523
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->workingInvoice()Lio/reactivex/Observable;

    move-result-object v0

    .line 1524
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 1525
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$addAdditionalRecipients$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$addAdditionalRecipients$1;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string/jumbo v1, "workingInvoiceEditor.wor\u2026ecipient_email)\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1528
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {v0, v1}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    return-void
.end method

.method private final addAttachment()V
    .locals 2

    .line 1532
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->workingInvoice()Lio/reactivex/Observable;

    move-result-object v0

    .line 1533
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 1534
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$addAttachment$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$addAttachment$1;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 1538
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$addAttachment$2;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$addAttachment$2;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 1561
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$addAttachment$3;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$addAttachment$3;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string/jumbo v1, "workingInvoiceEditor.wor\u2026  )\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1591
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {v0, v1}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    return-void
.end method

.method private final addCustomer()V
    .locals 9

    .line 1047
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->flow:Lflow/Flow;

    .line 1048
    sget-object v1, Lcom/squareup/ui/crm/ChooseCustomerFlow;->Companion:Lcom/squareup/ui/crm/ChooseCustomerFlow$Companion;

    .line 1049
    iget-object v2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceScopeV2:Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    if-nez v2, :cond_0

    const-string v3, "editInvoiceScopeV2"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v2, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 1050
    sget-object v3, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;->EDIT_INVOICE:Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    .line 1051
    sget v4, Lcom/squareup/crmchoosecustomer/R$string;->crm_add_customer_to_invoice_title:I

    .line 1052
    sget-object v5, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;->REQUIRE_FIRST_LAST_EMAIL:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

    .line 1053
    sget-object v6, Lcom/squareup/ui/crm/flow/CrmScopeType;->ADD_CUSTOMER_TO_INVOICE:Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 1048
    invoke-virtual/range {v1 .. v8}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Companion;->start(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;ILcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;Lcom/squareup/ui/crm/flow/CrmScopeType;ZZ)Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v1

    .line 1047
    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private final addLineItem()V
    .locals 4

    .line 1251
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/invoices/edit/items/ItemSelectScreen;

    iget-object v2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceScopeV2:Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    if-nez v2, :cond_0

    const-string v3, "editInvoiceScopeV2"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v2, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v1, v2}, Lcom/squareup/invoices/edit/items/ItemSelectScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private final addPaymentSchedule()V
    .locals 3

    .line 1488
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/invoices/analytics/AddPaymentScheduleClicked;->INSTANCE:Lcom/squareup/invoices/analytics/AddPaymentScheduleClicked;

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 1489
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 1490
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->workingInvoice()Lio/reactivex/Observable;

    move-result-object v1

    .line 1491
    iget-object v2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    invoke-static {v2}, Lcom/squareup/invoices/util/InvoiceReminderUtilsKt;->reminderDefaults(Lcom/squareup/invoicesappletapi/InvoiceUnitCache;)Lio/reactivex/Observable;

    move-result-object v2

    .line 1489
    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 1493
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 1494
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$addPaymentSchedule$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$addPaymentSchedule$1;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "Observables.combineLates\u2026  )\n          )\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1502
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {v0, v1}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    return-void
.end method

.method private final deleteDraft()V
    .locals 3

    .line 606
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_DELETE_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 607
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 608
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->workingInvoice()Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$deleteDraft$1;->INSTANCE:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$deleteDraft$1;

    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    const-string/jumbo v2, "workingInvoiceEditor.wor\u2026oice().map { it.id_pair }"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 609
    iget-object v2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingRecurrenceRuleEditor:Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;

    invoke-virtual {v2}, Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;->rule()Lio/reactivex/Observable;

    move-result-object v2

    .line 607
    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 611
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 612
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$deleteDraft$2;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$deleteDraft$2;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 617
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$deleteDraft$3;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$deleteDraft$3;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "Observables.combineLates\u2026nvoiceScopeV2))\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 623
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {v0, v1}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    return-void
.end method

.method private final editAutomaticPayments()V
    .locals 2

    .line 1477
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->workingInvoice()Lio/reactivex/Observable;

    move-result-object v0

    .line 1478
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 1479
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editAutomaticPayments$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editAutomaticPayments$1;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string/jumbo v1, "workingInvoiceEditor.wor\u2026led\n          )\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1484
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {v0, v1}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    return-void
.end method

.method private final editDate(Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType;)V
    .locals 4

    .line 1409
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 1410
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->workingInvoice()Lio/reactivex/Observable;

    move-result-object v1

    .line 1411
    iget-object v2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingRecurrenceRuleEditor:Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;

    invoke-virtual {v2}, Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;->rule()Lio/reactivex/Observable;

    move-result-object v2

    sget-object v3, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editDate$1;->INSTANCE:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editDate$1;

    check-cast v3, Lio/reactivex/functions/Function;

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    const-string/jumbo v3, "workingRecurrenceRuleEdi\u2026le().map { it.isPresent }"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1409
    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 1413
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 1414
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editDate$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editDate$2;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const-string v0, "Observables.combineLates\u2026Info, dateType)\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1424
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {p1, v0}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    return-void
.end method

.method private final editInvoiceDetails()V
    .locals 2

    .line 1428
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->workingInvoice()Lio/reactivex/Observable;

    move-result-object v0

    .line 1429
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 1430
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoiceDetails$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoiceDetails$1;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string/jumbo v1, "workingInvoiceEditor.wor\u2026d()\n          )\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1440
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {v0, v1}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    return-void
.end method

.method private final editInvoiceMethod()V
    .locals 2

    .line 1386
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    .line 1387
    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->workingInvoice()Lio/reactivex/Observable;

    move-result-object v0

    .line 1388
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 1389
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoiceMethod$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoiceMethod$1;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 1395
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoiceMethod$2;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoiceMethod$2;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string/jumbo v1, "workingInvoiceEditor\n   \u2026n()\n          )\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1405
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {v0, v1}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    return-void
.end method

.method private final editPaymentSchedule()V
    .locals 3

    .line 1506
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 1507
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->workingInvoice()Lio/reactivex/Observable;

    move-result-object v1

    .line 1508
    iget-object v2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    invoke-static {v2}, Lcom/squareup/invoices/util/InvoiceReminderUtilsKt;->reminderDefaults(Lcom/squareup/invoicesappletapi/InvoiceUnitCache;)Lio/reactivex/Observable;

    move-result-object v2

    .line 1506
    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 1510
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 1511
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editPaymentSchedule$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editPaymentSchedule$1;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "Observables.combineLates\u2026  )\n          )\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1519
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {v0, v1}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    return-void
.end method

.method private final editRecurrenceRule()V
    .locals 3

    .line 1368
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 1369
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->workingInvoice()Lio/reactivex/Observable;

    move-result-object v1

    .line 1370
    iget-object v2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingRecurrenceRuleEditor:Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;

    invoke-virtual {v2}, Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;->rule()Lio/reactivex/Observable;

    move-result-object v2

    .line 1368
    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 1372
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 1373
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editRecurrenceRule$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editRecurrenceRule$1;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "Observables.combineLates\u2026  )\n          )\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1382
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {v0, v1}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    return-void
.end method

.method private final editReminders()V
    .locals 6

    .line 1444
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 1445
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingRecurrenceRuleEditor:Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;->rule()Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editReminders$1;->INSTANCE:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editReminders$1;

    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    const-string/jumbo v2, "workingRecurrenceRuleEdi\u2026le().map { it.isPresent }"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1446
    iget-object v2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {v2}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->workingInvoice()Lio/reactivex/Observable;

    move-result-object v2

    .line 1447
    iget-object v3, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    invoke-interface {v3}, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;->unitMetadataDisplayDetails()Lio/reactivex/Observable;

    move-result-object v3

    .line 1448
    sget-object v4, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editReminders$2;->INSTANCE:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editReminders$2;

    check-cast v4, Lio/reactivex/functions/Function;

    invoke-virtual {v3, v4}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v3

    const-string v4, "invoiceUnitCache.unitMet\u2026matic_reminder_settings }"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1449
    iget-object v4, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    invoke-interface {v4}, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;->invoiceDefaultsList()Lio/reactivex/Observable;

    move-result-object v4

    sget-object v5, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editReminders$3;->INSTANCE:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editReminders$3;

    check-cast v5, Lio/reactivex/functions/Function;

    invoke-virtual {v4, v5}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v4

    const-string v5, "invoiceUnitCache.invoice\u2026reminder_config\n        }"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1444
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 1453
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 1454
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editReminders$4;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editReminders$4;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "Observables.combineLates\u2026ngs\n          )\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1473
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {v0, v1}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    return-void
.end method

.method private final goBackToEditInvoice2Screen(Z)V
    .locals 4

    .line 774
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Screen;

    iget-object v2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceScopeV2:Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    if-nez v2, :cond_0

    const-string v3, "editInvoiceScopeV2"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {v1, v2}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Screen;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    if-eqz p1, :cond_2

    .line 775
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    if-nez p1, :cond_1

    const-string v0, "editInvoiceContext"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-static {p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2Kt;->access$canPreview(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 776
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->updatePreview()V

    :cond_2
    return-void
.end method

.method private final goToRecurringEndOfMonthDialog()V
    .locals 4

    .line 1364
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/invoices/editv2/recurring/RecurringEndOfMonthDialog;

    iget-object v2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceScopeV2:Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    if-nez v2, :cond_0

    const-string v3, "editInvoiceScopeV2"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {v1, v2}, Lcom/squareup/invoices/editv2/recurring/RecurringEndOfMonthDialog;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private final goToSaveDraftConfirmation(Lcom/squareup/invoices/editv2/service/SaveDraftResponse;)V
    .locals 1

    .line 1257
    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/service/SaveDraftResponse;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->inCheckout:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/service/SaveDraftResponse;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object p1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->launchBuyerFlow(Lcom/squareup/protos/client/invoice/Invoice;Z)V

    goto :goto_0

    .line 1258
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goToSaveDraftInEdit(Lcom/squareup/invoices/editv2/service/SaveDraftResponse;)V

    :goto_0
    return-void
.end method

.method private final goToSaveDraftInEdit(Lcom/squareup/invoices/editv2/service/SaveDraftResponse;)V
    .locals 3

    .line 1294
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->confirmationScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 1295
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->confirmationScreenDataFactory:Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;

    invoke-virtual {v1, p1}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->fromSaveDraftResponse(Lcom/squareup/invoices/editv2/service/SaveDraftResponse;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    move-result-object p1

    .line 1294
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 1297
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen;

    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceScopeV2:Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    if-nez v1, :cond_0

    const-string v2, "editInvoiceScopeV2"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {v0, v1}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private final goToSendInvoiceConfirmation(Lcom/squareup/invoices/editv2/service/SendInvoiceResponse;)V
    .locals 4

    .line 1266
    instance-of v0, p1, Lcom/squareup/invoices/editv2/service/SendInvoiceResponse$Recurring;

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    invoke-static {p0, p1, v2, v1, v2}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goToSendInvoiceConfirmationInEdit$default(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/invoices/editv2/service/SendInvoiceResponse;Lcom/squareup/protos/client/instruments/InstrumentSummary;ILjava/lang/Object;)V

    goto :goto_0

    .line 1268
    :cond_0
    instance-of v0, p1, Lcom/squareup/invoices/editv2/service/SendInvoiceResponse$SingleInvoice;

    if-eqz v0, :cond_3

    .line 1269
    move-object v0, p1

    check-cast v0, Lcom/squareup/invoices/editv2/service/SendInvoiceResponse$SingleInvoice;

    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/service/SendInvoiceResponse$SingleInvoice;->getSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object v0

    .line 1271
    instance-of v3, v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v3, :cond_1

    .line 1272
    invoke-static {p0, p1, v2, v1, v2}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goToSendInvoiceConfirmationInEdit$default(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/invoices/editv2/service/SendInvoiceResponse;Lcom/squareup/protos/client/instruments/InstrumentSummary;ILjava/lang/Object;)V

    goto :goto_0

    .line 1274
    :cond_1
    instance-of v1, v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v1, :cond_3

    .line 1275
    check-cast v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {v0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceResponse;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceResponse;->invoice:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    .line 1277
    iget-boolean v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->inCheckout:Z

    const-string v2, "invoice"

    if-eqz v1, :cond_2

    .line 1278
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    invoke-direct {p0, v0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->launchBuyerFlow(Lcom/squareup/protos/client/invoice/Invoice;Z)V

    goto :goto_0

    .line 1280
    :cond_2
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->instrumentsStore:Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;->findInstrument(Lcom/squareup/protos/client/invoice/Invoice;)Lio/reactivex/Single;

    move-result-object v0

    .line 1281
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$goToSendInvoiceConfirmation$$inlined$let$lambda$1;

    invoke-direct {v1, p1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$goToSendInvoiceConfirmation$$inlined$let$lambda$1;-><init>(Lcom/squareup/invoices/editv2/service/SendInvoiceResponse;Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const-string v0, "instrumentsStore.findIns\u2026ll)\n                    }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1284
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {p1, v0}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    :cond_3
    :goto_0
    return-void
.end method

.method private final goToSendInvoiceConfirmationInEdit(Lcom/squareup/invoices/editv2/service/SendInvoiceResponse;Lcom/squareup/protos/client/instruments/InstrumentSummary;)V
    .locals 4

    .line 1304
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->confirmationScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 1305
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->confirmationScreenDataFactory:Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;

    .line 1307
    iget-object v2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    const-string v3, "editInvoiceContext"

    if-nez v2, :cond_0

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v2}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isNew()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    if-nez v2, :cond_1

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v2}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isDraft()Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    .line 1305
    :goto_0
    invoke-virtual {v1, p1, v2, p2}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->fromSendInvoiceResponse(Lcom/squareup/invoices/editv2/service/SendInvoiceResponse;ZLcom/squareup/protos/client/instruments/InstrumentSummary;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    move-result-object p1

    .line 1304
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 1311
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->flow:Lflow/Flow;

    new-instance p2, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceScopeV2:Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    if-nez v0, :cond_3

    const-string v1, "editInvoiceScopeV2"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-direct {p2, v0}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;)V

    invoke-virtual {p1, p2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic goToSendInvoiceConfirmationInEdit$default(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/invoices/editv2/service/SendInvoiceResponse;Lcom/squareup/protos/client/instruments/InstrumentSummary;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 1302
    check-cast p2, Lcom/squareup/protos/client/instruments/InstrumentSummary;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goToSendInvoiceConfirmationInEdit(Lcom/squareup/invoices/editv2/service/SendInvoiceResponse;Lcom/squareup/protos/client/instruments/InstrumentSummary;)V

    return-void
.end method

.method private final goToValidationErrorDialog(Lcom/squareup/invoices/editv2/validation/ValidationResult$InvalidWithMessage;)V
    .locals 5

    .line 1353
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->flow:Lflow/Flow;

    .line 1354
    new-instance v1, Lcom/squareup/register/widgets/WarningDialogScreen;

    .line 1355
    new-instance v2, Lcom/squareup/widgets/warning/WarningStrings;

    .line 1356
    iget-object v3, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/common/strings/R$string;->error_default:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1357
    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/validation/ValidationResult$InvalidWithMessage;->getErrorMessage()Ljava/lang/String;

    move-result-object p1

    .line 1355
    invoke-direct {v2, v3, p1}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/widgets/warning/Warning;

    .line 1354
    invoke-direct {v1, v2}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    .line 1353
    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private final handleAttachmentResult(Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult;)V
    .locals 2

    .line 1193
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult$Uploaded;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult$Uploaded;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult$Uploaded;->getAttachmentMetadata()Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->addFileAttachment(Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)V

    goto :goto_0

    .line 1194
    :cond_0
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult$Updated;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    .line 1195
    check-cast p1, Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult$Updated;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult$Updated;->getAttachmentToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult$Updated;->getUpdatedTitle()Ljava/lang/String;

    move-result-object p1

    .line 1194
    invoke-virtual {v0, v1, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->updateFileAttachment(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1197
    :cond_1
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult$Removed;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult$Removed;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult$Removed;->getAttachmentToken()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->removeFileAttachment(Ljava/lang/String;)V

    :cond_2
    :goto_0
    return-void
.end method

.method private final handleAttachmentRowClicked(Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$AttachmentClicked;)V
    .locals 2

    .line 1601
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$AttachmentClicked;->getKey()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;

    sget-object v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$4:[I

    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1603
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->workingInvoice()Lio/reactivex/Observable;

    move-result-object v0

    .line 1604
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 1605
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handleAttachmentRowClicked$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handleAttachmentRowClicked$1;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$AttachmentClicked;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const-string/jumbo v0, "workingInvoiceEditor.wor\u2026          )\n            }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1623
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {p1, v0}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    return-void

    .line 1625
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Invalid attachment clicked view event key."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 1601
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.invoices.editv2.secondscreen.EditInvoice2ScreenData.EventKey"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final handleAutomaticPaymentsResult(Lcom/squareup/invoices/workflow/edit/EditAutomaticPaymentsResult;)V
    .locals 0

    .line 1147
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditAutomaticPaymentsResult;->getAllowAutoPayments()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->updateAllowAutoPayments(Z)V

    return-void
.end method

.method private final handleAutomaticRemindersOutput(Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;)V
    .locals 2

    .line 1166
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;->isConfig()Z

    move-result v0

    const/16 v1, 0xa

    if-eqz v0, :cond_2

    .line 1167
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;->getReminders()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 1753
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 1754
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 1755
    check-cast v1, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    if-eqz v1, :cond_0

    .line 1168
    check-cast v1, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;

    invoke-static {v1}, Lcom/squareup/invoices/workflow/edit/InvoiceReminderKt;->toProto(Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;)Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.invoices.workflow.edit.InvoiceReminder.Config"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 1756
    :cond_1
    check-cast v0, Ljava/util/List;

    .line 1170
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {p1, v0}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->updateAutomaticRemindersConfig(Ljava/util/List;)V

    goto :goto_2

    .line 1175
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;->getReminders()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 1757
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 1758
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 1759
    check-cast v1, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    if-eqz v1, :cond_3

    .line 1176
    check-cast v1, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;

    invoke-static {v1}, Lcom/squareup/invoices/workflow/edit/InvoiceReminderKt;->toProto(Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.invoices.workflow.edit.InvoiceReminder.Instance"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 1760
    :cond_4
    check-cast v0, Ljava/util/List;

    .line 1178
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {p1, v0}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->updateAutomaticRemindersInstances(Ljava/util/List;)V

    :goto_2
    return-void
.end method

.method private final handleCustomerChosen(Lcom/squareup/ui/crm/ChooseCustomerFlow$Result;)V
    .locals 5

    .line 880
    instance-of v0, p1, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;

    if-eqz v0, :cond_1

    .line 881
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingOrderEditor:Lcom/squareup/invoices/order/WorkingOrderEditor;

    check-cast p1, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/order/WorkingOrderEditor;->setCustomer(Lcom/squareup/protos/client/rolodex/Contact;)V

    .line 882
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->analytics:Lcom/squareup/analytics/Analytics;

    .line 883
    new-instance v1, Lcom/squareup/invoices/analytics/AddCustomerToInvoiceEvent;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    const-string v2, "No contact token"

    :goto_0
    invoke-direct {v1, v2}, Lcom/squareup/invoices/analytics/AddCustomerToInvoiceEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 882
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 885
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->flow:Lflow/Flow;

    sget-object v1, Lflow/Direction;->BACKWARD:Lflow/Direction;

    const/4 v2, 0x2

    new-array v2, v2, [Lkotlin/jvm/functions/Function1;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->getHistoryFuncForConfirmation()Lkotlin/jvm/functions/Function1;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p1}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->getHistoryFuncForBackout()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->editHistory(Lflow/Flow;Lflow/Direction;[Lkotlin/jvm/functions/Function1;)V

    .line 888
    :cond_1
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {p1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->dismissSaveCustomer()Z

    return-void
.end method

.method private final handleDeliveryMethodResult(Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;)V
    .locals 2

    .line 1151
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;->getPaymentMethod()Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->updateMethod(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;)V

    .line 1152
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;->getInstrumentToken()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->updateInstrumentToken(Ljava/lang/String;)V

    return-void
.end method

.method private final handleDueDateResult(Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$DueDateResult;)V
    .locals 2

    .line 1125
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    .line 1126
    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->workingInvoice()Lio/reactivex/Observable;

    move-result-object v0

    .line 1127
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 1128
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handleDueDateResult$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handleDueDateResult$1;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$DueDateResult;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const-string/jumbo v0, "workingInvoiceEditor\n   \u2026  )\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1143
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {p1, v0}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    return-void
.end method

.method private final handleEditInvoiceWorkflowResult(Lcom/squareup/invoices/workflow/edit/EditInvoiceResult;)V
    .locals 3

    .line 801
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$ScheduledDateResult;

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 802
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$ScheduledDateResult;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->handleScheduledDateResult(Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$ScheduledDateResult;)V

    .line 803
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->features:Lcom/squareup/settings/server/Features;

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_FLOW_V2_WIDGETS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 804
    invoke-direct {p0, v1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goBackToEditInvoice2Screen(Z)V

    goto/16 :goto_0

    .line 806
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goBackToEditInvoice1Screen()V

    goto/16 :goto_0

    .line 809
    :cond_1
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$DueDateResult;

    if-eqz v0, :cond_2

    .line 810
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$DueDateResult;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->handleDueDateResult(Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$DueDateResult;)V

    .line 811
    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goBackToEditInvoice1Screen()V

    goto/16 :goto_0

    .line 813
    :cond_2
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$DeliveryResult;

    const/4 v2, 0x0

    if-eqz v0, :cond_4

    .line 814
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$DeliveryResult;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$DeliveryResult;->getResult()Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->handleDeliveryMethodResult(Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;)V

    .line 815
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->features:Lcom/squareup/settings/server/Features;

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_FLOW_V2_WIDGETS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 817
    invoke-direct {p0, v2}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goBackToEditInvoice2Screen(Z)V

    goto/16 :goto_0

    .line 819
    :cond_3
    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goBackToEditInvoice1Screen()V

    goto/16 :goto_0

    .line 822
    :cond_4
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$RecurringResult;

    if-eqz v0, :cond_5

    .line 823
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$RecurringResult;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$RecurringResult;->getResult()Lcom/squareup/invoices/workflow/edit/RecurringScheduleWorkflowOutput;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->handleRecurringResult(Lcom/squareup/invoices/workflow/edit/RecurringScheduleWorkflowOutput;)V

    .line 824
    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goBackToEditInvoice1Screen()V

    goto/16 :goto_0

    .line 826
    :cond_5
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$PaymentRequestResult;

    if-eqz v0, :cond_6

    .line 827
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$PaymentRequestResult;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$PaymentRequestResult;->getIndex()I

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$PaymentRequestResult;->getEditPaymentRequestResult()Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->handlePaymentRequestResult(ILcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult;)V

    .line 828
    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goBackToEditInvoice1Screen()V

    goto/16 :goto_0

    .line 830
    :cond_6
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$PaymentScheduleResult;

    if-eqz v0, :cond_7

    .line 831
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$PaymentScheduleResult;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$PaymentScheduleResult;->getEditPaymentScheduleResult()Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->handlePaymentScheduleResult(Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult;)V

    .line 832
    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goBackToEditInvoice1Screen()V

    goto/16 :goto_0

    .line 834
    :cond_7
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$AutomaticPaymentsResult;

    if-eqz v0, :cond_8

    .line 835
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$AutomaticPaymentsResult;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$AutomaticPaymentsResult;->getAutomaticPaymentsResult()Lcom/squareup/invoices/workflow/edit/EditAutomaticPaymentsResult;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->handleAutomaticPaymentsResult(Lcom/squareup/invoices/workflow/edit/EditAutomaticPaymentsResult;)V

    .line 836
    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goBackToEditInvoice1Screen()V

    goto/16 :goto_0

    .line 838
    :cond_8
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$InvoiceDetailsResult;

    if-eqz v0, :cond_a

    .line 839
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$InvoiceDetailsResult;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$InvoiceDetailsResult;->getEditInvoiceDetailsResult()Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsResult;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->handleInvoiceDetailsResult(Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsResult;)V

    .line 840
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_FLOW_V2_WIDGETS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 841
    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goBackToEditInvoice1Screen()V

    goto :goto_0

    .line 843
    :cond_9
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$InvoiceDetailsResult;->getEditInvoiceDetailsResult()Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsResult;

    move-result-object p1

    instance-of p1, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsResult$Canceled;

    xor-int/2addr p1, v1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goBackToEditInvoice2Screen(Z)V

    goto :goto_0

    .line 846
    :cond_a
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$AutoRemindersResult;

    if-eqz v0, :cond_b

    .line 847
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$AutoRemindersResult;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$AutoRemindersResult;->getOutput()Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->handleAutomaticRemindersOutput(Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;)V

    .line 849
    invoke-direct {p0, v2}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goBackToEditInvoice2Screen(Z)V

    goto :goto_0

    .line 853
    :cond_b
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$RecipientsResult;

    if-eqz v0, :cond_d

    .line 854
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$RecipientsResult;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$RecipientsResult;->getAdditionalRecipientsResult()Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->handleRecipientResults(Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult;)V

    .line 855
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_FLOW_V2_WIDGETS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 856
    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goBackToEditInvoice1Screen()V

    goto :goto_0

    .line 859
    :cond_c
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$RecipientsResult;->getAdditionalRecipientsResult()Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult;

    move-result-object p1

    instance-of p1, p1, Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult$Canceled;

    xor-int/2addr p1, v1

    .line 858
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goBackToEditInvoice2Screen(Z)V

    goto :goto_0

    .line 863
    :cond_d
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$AttachmentResult;

    if-eqz v0, :cond_f

    .line 864
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$AttachmentResult;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$AttachmentResult;->getInvoiceAttachmentResult()Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->handleAttachmentResult(Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult;)V

    .line 865
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_FLOW_V2_WIDGETS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 866
    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goBackToEditInvoice1Screen()V

    goto :goto_0

    .line 869
    :cond_e
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$AttachmentResult;->getInvoiceAttachmentResult()Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult;

    move-result-object p1

    instance-of p1, p1, Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult$Canceled;

    xor-int/2addr p1, v1

    .line 868
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goBackToEditInvoice2Screen(Z)V

    :goto_0
    return-void

    .line 873
    :cond_f
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Unknown result type."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final handleInvoiceDetailsResult(Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsResult;)V
    .locals 2

    .line 1156
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsResult$Saved;

    if-eqz v0, :cond_0

    .line 1157
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsResult$Saved;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsResult$Saved;->getInvoiceDetailsInfo()Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;

    move-result-object p1

    .line 1158
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->updateInvoiceTitle(Ljava/lang/String;)V

    .line 1159
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->updateInvoiceId(Ljava/lang/String;)V

    .line 1160
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->updateInvoiceMessage(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private final handleLineItemClicked(Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$LineItemClicked;)V
    .locals 2

    .line 1673
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$LineItemClicked;->getKey()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    sget-object v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$6:[I

    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1674
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$LineItemClicked;->getIndex()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->startEditingOrder(I)V

    return-void

    .line 1675
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Invalid line item clicked view event key."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 1673
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.invoices.editv2.firstscreen.EditInvoice1ScreenData.EventKey"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final handlePaymentRequestClicked(Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$PaymentRequestClicked;)V
    .locals 2

    .line 1650
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$PaymentRequestClicked;->getKey()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    check-cast v0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    sget-object v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$5:[I

    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 1668
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Invalid payment request clicked view event key."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 1652
    :cond_1
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$PaymentRequestClicked;->getIndex()I

    move-result p1

    .line 1653
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->workingInvoice()Lio/reactivex/Observable;

    move-result-object v0

    .line 1654
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 1655
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handlePaymentRequestClicked$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handlePaymentRequestClicked$1;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;I)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const-string/jumbo v0, "workingInvoiceEditor.wor\u2026          )\n            }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1666
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {p1, v0}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    return-void

    .line 1650
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.invoices.editv2.firstscreen.EditInvoice1ScreenData.EventKey"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final handlePaymentRequestResult(ILcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult;)V
    .locals 1

    .line 1236
    instance-of v0, p2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult$Saved;

    if-eqz v0, :cond_1

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 1238
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    check-cast p2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult$Saved;

    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult$Saved;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->addPaymentRequest(Lcom/squareup/protos/client/invoice/PaymentRequest;)V

    goto :goto_0

    .line 1240
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    check-cast p2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult$Saved;

    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult$Saved;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->updatePaymentRequest(ILcom/squareup/protos/client/invoice/PaymentRequest;)V

    goto :goto_0

    .line 1243
    :cond_1
    instance-of v0, p2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult$Removed;

    if-eqz v0, :cond_2

    iget-object p2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {p2, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->removePaymentRequest(I)V

    goto :goto_0

    .line 1244
    :cond_2
    instance-of p1, p2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult$Cancelled;

    :goto_0
    return-void
.end method

.method private final handlePaymentScheduleResult(Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult;)V
    .locals 4

    .line 1205
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult$Saved;

    if-eqz v0, :cond_0

    .line 1206
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 1207
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    .line 1208
    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->workingInvoice()Lio/reactivex/Observable;

    move-result-object v1

    .line 1209
    iget-object v2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    invoke-interface {v2}, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;->invoiceDefaultsList()Lio/reactivex/Observable;

    move-result-object v2

    sget-object v3, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handlePaymentScheduleResult$1;->INSTANCE:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handlePaymentScheduleResult$1;

    check-cast v3, Lio/reactivex/functions/Function;

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    const-string v3, "invoiceUnitCache.invoice\u2026tomatic_reminder_config }"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1206
    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 1211
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 1212
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handlePaymentScheduleResult$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handlePaymentScheduleResult$2;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const-string v0, "Observables.combineLates\u2026          }\n            }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1226
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {p1, v0}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    :cond_0
    return-void
.end method

.method private final handleRecipientResults(Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult;)V
    .locals 1

    .line 1186
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult$Saved;

    if-eqz v0, :cond_0

    .line 1187
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult$Saved;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult$Saved;->getAdditionalRecipients()Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->updateRecipients(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method private final handleRecurringResult(Lcom/squareup/invoices/workflow/edit/RecurringScheduleWorkflowOutput;)V
    .locals 4

    .line 1085
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 1086
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->workingInvoice()Lio/reactivex/Observable;

    move-result-object v1

    .line 1087
    iget-object v2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingRecurrenceRuleEditor:Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;

    invoke-virtual {v2}, Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;->rule()Lio/reactivex/Observable;

    move-result-object v2

    sget-object v3, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handleRecurringResult$1;->INSTANCE:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handleRecurringResult$1;

    check-cast v3, Lio/reactivex/functions/Function;

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    const-string/jumbo v3, "workingRecurrenceRuleEdi\u2026le().map { it.isPresent }"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1085
    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 1089
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 1090
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handleRecurringResult$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handleRecurringResult$2;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/invoices/workflow/edit/RecurringScheduleWorkflowOutput;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const-string v0, "Observables.combineLates\u2026et(rRuleOutput)\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1113
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {p1, v0}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    return-void
.end method

.method private final handleScheduledDateResult(Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$ScheduledDateResult;)V
    .locals 1

    .line 1119
    sget-object v0, Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;->Converter:Lcom/squareup/invoices/workflow/edit/ChooseDateOutput$Converter;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$ScheduledDateResult;->getResult()Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/workflow/edit/ChooseDateOutput$Converter;->selectedDateFromOutput(Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    .line 1121
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->updateScheduledAt(Lcom/squareup/protos/common/time/YearMonthDay;)V

    return-void
.end method

.method private final handleSimpleEvent(Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$Simple;)V
    .locals 1

    .line 964
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$Simple;->getKey()Ljava/lang/Object;

    move-result-object p1

    .line 965
    instance-of v0, p1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->handleSimpleEventEditInvoice1Screen(Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;)V

    goto :goto_0

    .line 966
    :cond_0
    instance-of v0, p1, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->handleSimpleEventEditInvoice2Screen(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;)V

    goto :goto_0

    .line 967
    :cond_1
    instance-of v0, p1, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$EventKey;

    if-eqz v0, :cond_2

    .line 968
    check-cast p1, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$EventKey;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->handleSimpleEventOverflowDialogScreen(Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$EventKey;)V

    :cond_2
    :goto_0
    return-void
.end method

.method private final handleSimpleEventEditInvoice1Screen(Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;)V
    .locals 1

    .line 973
    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 984
    :pswitch_0
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editPaymentSchedule()V

    goto :goto_0

    .line 983
    :pswitch_1
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->addPaymentSchedule()V

    goto :goto_0

    .line 982
    :pswitch_2
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editAutomaticPayments()V

    goto :goto_0

    .line 981
    :pswitch_3
    sget-object p1, Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType$Due;->INSTANCE:Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType$Due;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editDate(Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType;)V

    goto :goto_0

    .line 980
    :pswitch_4
    sget-object p1, Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType$Scheduled;->INSTANCE:Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType$Scheduled;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editDate(Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType;)V

    goto :goto_0

    .line 979
    :pswitch_5
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->requestDeposit()V

    goto :goto_0

    .line 978
    :pswitch_6
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceMethod()V

    goto :goto_0

    .line 977
    :pswitch_7
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editRecurrenceRule()V

    goto :goto_0

    .line 976
    :pswitch_8
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->addLineItem()V

    goto :goto_0

    .line 975
    :pswitch_9
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->viewCustomer()V

    goto :goto_0

    .line 974
    :pswitch_a
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->addCustomer()V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private final handleSimpleEventEditInvoice2Screen(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;)V
    .locals 1

    .line 989
    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 993
    :cond_0
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->addAttachment()V

    goto :goto_0

    .line 992
    :cond_1
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->addAdditionalRecipients()V

    goto :goto_0

    .line 991
    :cond_2
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editReminders()V

    goto :goto_0

    .line 990
    :cond_3
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceDetails()V

    :goto_0
    return-void
.end method

.method private final handleSimpleEventOverflowDialogScreen(Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$EventKey;)V
    .locals 2

    .line 998
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->flow:Lflow/Flow;

    .line 1752
    const-class v1, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    .line 999
    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$EventKey;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 1001
    :cond_0
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->deleteDraft()V

    goto :goto_0

    .line 1000
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->onSaveDraftClicked()V

    :goto_0
    return-void
.end method

.method private final handleToggleEvent(Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$ToggleClicked;)V
    .locals 2

    .line 1006
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$ToggleClicked;->getKey()Ljava/lang/Object;

    move-result-object v0

    .line 1007
    instance-of v1, v0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    if-eqz v1, :cond_0

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->handleToggleEventEditInvoice1Screen(Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$ToggleClicked;)V

    goto :goto_0

    .line 1008
    :cond_0
    instance-of v0, v0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->handleToggleEventEditInvoice2Screen(Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$ToggleClicked;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private final handleToggleEventEditInvoice1Screen(Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$ToggleClicked;)V
    .locals 2

    .line 1013
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$ToggleClicked;->getKey()Ljava/lang/Object;

    move-result-object v0

    .line 1014
    sget-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->REQUEST_SHIPPING_ADDRESS_TOGGLED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$ToggleClicked;->getChecked()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->updateRequestShippingAddress(Z)V

    goto :goto_0

    .line 1015
    :cond_0
    sget-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->REQUEST_A_TIP_TOGGLED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    if-ne v0, v1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$ToggleClicked;->getChecked()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->updateRequestTipping(Z)V

    goto :goto_0

    .line 1016
    :cond_1
    sget-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->AUTO_PAYMENTS_TOGGLED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    if-ne v0, v1, :cond_2

    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$ToggleClicked;->getChecked()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->updateAllowAutoPayments(Z)V

    :goto_0
    return-void

    .line 1017
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " has the wrong key."

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final handleToggleEventEditInvoice2Screen(Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$ToggleClicked;)V
    .locals 2

    .line 1022
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$ToggleClicked;->getKey()Ljava/lang/Object;

    move-result-object v0

    .line 1023
    sget-object v1, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;->BUYER_COF_TOGGLED:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$ToggleClicked;->getChecked()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->updateBuyerCofEnabled(Z)V

    return-void

    .line 1024
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " has the wrong key."

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final launchBuyerFlow(Lcom/squareup/protos/client/invoice/Invoice;Z)V
    .locals 2

    .line 1318
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireInvoicePayment()Lcom/squareup/payment/InvoicePayment;

    move-result-object v0

    const-string/jumbo v1, "transaction.requireInvoicePayment()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1319
    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->populateFromInvoice(Lcom/squareup/payment/InvoicePayment;Lcom/squareup/protos/client/invoice/Invoice;Z)V

    .line 1320
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

    const/4 p2, 0x0

    invoke-interface {p1, p2}, Lcom/squareup/ui/buyer/BuyerFlowStarter;->startBuyerFlowRecreatingSellerFlow(Z)V

    return-void
.end method

.method private final logInvoiceSentAction(Lcom/squareup/analytics/Analytics;Lcom/squareup/protos/client/invoice/Invoice;)V
    .locals 2

    .line 1693
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    const-string v1, "editInvoiceContext"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isNew()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isDraft()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1694
    :cond_2
    invoke-static {p2}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->SHARE_LINK:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-ne v0, v1, :cond_3

    .line 1695
    sget-object p2, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_CREATE_SHARE_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, p2}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    goto :goto_1

    .line 1698
    :cond_3
    iget-object p2, p2, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-static {p2, v0}, Lcom/squareup/invoices/InvoiceDatesKt;->isBeforeOrEqualToday(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/time/CurrentTime;)Z

    move-result p2

    if-eqz p2, :cond_4

    .line 1699
    sget-object p2, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_SCHEDULE_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    goto :goto_0

    .line 1701
    :cond_4
    sget-object p2, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_SEND_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    .line 1697
    :goto_0
    invoke-interface {p1, p2}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    :cond_5
    :goto_1
    return-void
.end method

.method private final logSend(Lcom/squareup/util/Optional;Lcom/squareup/protos/client/invoice/Invoice;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/invoices/workflow/edit/RecurrenceRule;",
            ">;",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            ")V"
        }
    .end annotation

    .line 1681
    sget-object v0, Lcom/squareup/util/Optional$Empty;->INSTANCE:Lcom/squareup/util/Optional$Empty;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->logInvoiceSentAction(Lcom/squareup/analytics/Analytics;Lcom/squareup/protos/client/invoice/Invoice;)V

    goto :goto_0

    .line 1682
    :cond_0
    instance-of p1, p1, Lcom/squareup/util/Optional$Present;

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->logSeriesScheduled(Lcom/squareup/analytics/Analytics;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private final logSeriesScheduled(Lcom/squareup/analytics/Analytics;)V
    .locals 2

    .line 1687
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    if-nez v0, :cond_0

    const-string v1, "editInvoiceContext"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isEditingNonDraftSeries()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1688
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_SEND_RECURRING_SERIES:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    :cond_1
    return-void
.end method

.method private final populateFromInvoice(Lcom/squareup/payment/InvoicePayment;Lcom/squareup/protos/client/invoice/Invoice;Z)V
    .locals 2

    .line 1327
    iget-object v0, p2, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/payment/InvoicePayment;->setBuyerName(Ljava/lang/String;)V

    .line 1328
    iget-object v0, p2, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_email:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/payment/InvoicePayment;->setEmail(Ljava/lang/String;)V

    .line 1329
    iget-object v0, p2, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {p1, v0}, Lcom/squareup/payment/InvoicePayment;->setIdPair(Lcom/squareup/protos/client/IdPair;)V

    const/4 v0, 0x1

    if-eqz p3, :cond_0

    .line 1332
    invoke-virtual {p1, v0}, Lcom/squareup/payment/InvoicePayment;->setDraft(Z)V

    goto :goto_0

    .line 1334
    :cond_0
    iget-object p3, p2, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-static {p3, v1}, Lcom/squareup/invoices/InvoiceDatesKt;->isToday(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/time/CurrentTime;)Z

    move-result p3

    if-eqz p3, :cond_3

    .line 1335
    invoke-static {p2}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object p3

    .line 1336
    sget-object v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$3:[I

    invoke-virtual {p3}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->ordinal()I

    move-result p3

    aget p3, v1, p3

    if-eq p3, v0, :cond_2

    const/4 p2, 0x2

    if-eq p3, p2, :cond_1

    goto :goto_0

    .line 1342
    :cond_1
    invoke-virtual {p1, v0}, Lcom/squareup/payment/InvoicePayment;->setShareLinkInvoice(Z)V

    goto :goto_0

    .line 1338
    :cond_2
    iget-object p3, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->instrumentsStore:Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;

    invoke-virtual {p3, p2}, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;->findInstrument(Lcom/squareup/protos/client/invoice/Invoice;)Lio/reactivex/Single;

    move-result-object p2

    .line 1339
    new-instance p3, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$populateFromInvoice$1;

    invoke-direct {p3, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$populateFromInvoice$1;-><init>(Lcom/squareup/payment/InvoicePayment;)V

    check-cast p3, Lio/reactivex/functions/Consumer;

    invoke-virtual {p2, p3}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const-string p2, "instrumentsStore.findIns\u2026e, summary.valueOrNull) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1340
    iget-object p2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {p1, p2}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    goto :goto_0

    .line 1347
    :cond_3
    invoke-virtual {p1, v0}, Lcom/squareup/payment/InvoicePayment;->setScheduled(Z)V

    :goto_0
    return-void
.end method

.method private final requestDeposit()V
    .locals 2

    .line 1630
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->workingInvoice()Lio/reactivex/Observable;

    move-result-object v0

    .line 1631
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 1632
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$requestDeposit$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$requestDeposit$1;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string/jumbo v1, "workingInvoiceEditor.wor\u2026EST\n          )\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1646
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {v0, v1}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    return-void
.end method

.method private final saveDraftSingle()Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/invoices/editv2/service/SaveDraftResponse;",
            ">;"
        }
    .end annotation

    .line 897
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 898
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->workingInvoice()Lio/reactivex/Observable;

    move-result-object v1

    .line 899
    iget-object v2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingRecurrenceRuleEditor:Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;

    invoke-virtual {v2}, Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;->rule()Lio/reactivex/Observable;

    move-result-object v2

    .line 897
    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 901
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 902
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$saveDraftSingle$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$saveDraftSingle$1;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 911
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$saveDraftSingle$2;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$saveDraftSingle$2;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v0

    .line 928
    sget-object v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$saveDraftSingle$3;->INSTANCE:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$saveDraftSingle$3;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "Observables.combineLates\u2026        .map { it.first }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final sendInvoiceSingle(Lcom/squareup/util/Optional;Lcom/squareup/protos/client/invoice/Invoice;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/invoices/workflow/edit/RecurrenceRule;",
            ">;",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/invoices/editv2/service/SendInvoiceResponse;",
            ">;"
        }
    .end annotation

    .line 939
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceV2ServiceHelper:Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;->sendOrSchedule(Lcom/squareup/util/Optional;Lcom/squareup/protos/client/invoice/Invoice;)Lio/reactivex/Single;

    move-result-object v0

    .line 940
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$sendInvoiceSingle$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$sendInvoiceSingle$1;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v0

    .line 941
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$sendInvoiceSingle$2;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$sendInvoiceSingle$2;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Action;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doAfterTerminate(Lio/reactivex/functions/Action;)Lio/reactivex/Single;

    move-result-object v0

    .line 942
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$sendInvoiceSingle$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$sendInvoiceSingle$3;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/util/Optional;Lcom/squareup/protos/client/invoice/Invoice;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "editInvoiceV2ServiceHelp\u2026reparedInvoice)\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final shouldCompleteOnboardingSetUp()Z
    .locals 1

    .line 516
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getOnboardingSettings()Lcom/squareup/settings/server/OnboardingSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/OnboardingSettings;->acceptsCards()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private final showSaveDraftFailure(Lcom/squareup/invoices/editv2/service/SaveDraftResponse;)V
    .locals 4

    .line 1595
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    invoke-static {v0, p1}, Lcom/squareup/invoices/editv2/service/SaveDraftResponseKt;->format(Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/invoices/editv2/service/SaveDraftResponse;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 1596
    new-instance v0, Lcom/squareup/widgets/warning/WarningStrings;

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1597
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/register/widgets/WarningDialogScreen;

    check-cast v0, Lcom/squareup/widgets/warning/Warning;

    const/4 v2, 0x0

    const-string v3, "cannot save draft"

    invoke-direct {v1, v0, v2, v3}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;ZLjava/lang/String;)V

    invoke-virtual {p1, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private final updateAllowAutoPayments(Z)V
    .locals 1

    .line 1073
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->updateAutomaticPayments(Z)V

    return-void
.end method

.method private final updateBuyerCofEnabled(Z)V
    .locals 1

    .line 1069
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->updateBuyerCofEnabled(Z)V

    return-void
.end method

.method private final updatePreview()V
    .locals 2

    .line 781
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->saveDraftSingle()Lio/reactivex/Single;

    move-result-object v0

    .line 782
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$updatePreview$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$updatePreview$1;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "saveDraftSingle()\n      \u2026\"))\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 789
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {v0, v1}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    return-void
.end method

.method private final updateRequestShippingAddress(Z)V
    .locals 1

    .line 1061
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->updateRequestShippingAddress(Z)V

    return-void
.end method

.method private final updateRequestTipping(Z)V
    .locals 1

    .line 1065
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->updateRequestTipping(Z)V

    return-void
.end method

.method private final viewCustomer()V
    .locals 2

    .line 1029
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingOrderEditor:Lcom/squareup/invoices/order/WorkingOrderEditor;

    invoke-virtual {v0}, Lcom/squareup/invoices/order/WorkingOrderEditor;->workingOrder()Lio/reactivex/Observable;

    move-result-object v0

    .line 1030
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 1031
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$viewCustomer$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$viewCustomer$1;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string/jumbo v1, "workingOrderEditor.worki\u2026  }\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1043
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {v0, v1}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    return-void
.end method

.method private final willSeriesRecurEndOfMonth(Lcom/squareup/util/Optional;Lcom/squareup/protos/client/invoice/Invoice;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/invoices/workflow/edit/RecurrenceRule;",
            ">;",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            ")Z"
        }
    .end annotation

    .line 958
    invoke-virtual {p1}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->recurringHelper:Lcom/squareup/invoices/editv2/recurring/RecurringHelper;

    .line 959
    invoke-virtual {p1}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    .line 958
    invoke-virtual {v0, p2, p1}, Lcom/squareup/invoices/editv2/recurring/RecurringHelper;->willSeriesRecurEndOfMonth(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public final attemptSendInvoice()V
    .locals 8

    .line 668
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 669
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->workingInvoice()Lio/reactivex/Observable;

    move-result-object v1

    .line 670
    iget-object v2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingRecurrenceRuleEditor:Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;

    invoke-virtual {v2}, Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;->rule()Lio/reactivex/Observable;

    move-result-object v2

    .line 668
    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 672
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 673
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$attemptSendInvoice$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$attemptSendInvoice$1;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMapMaybe(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object v2

    const-string v0, "Observables.combineLates\u2026oice).toMaybe()\n        }"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 682
    new-instance v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$attemptSendInvoice$2;

    move-object v1, p0

    check-cast v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-direct {v0, v1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$attemptSendInvoice$2;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    move-object v3, v0

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 683
    new-instance v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$attemptSendInvoice$3;

    invoke-direct {v0, v1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$attemptSendInvoice$3;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    move-object v5, v0

    check-cast v5, Lkotlin/jvm/functions/Function0;

    const/4 v4, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x0

    .line 681
    invoke-static/range {v2 .. v7}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2Kt;->subscribe$default(Lio/reactivex/Maybe;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 685
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {v0, v1}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    return-void
.end method

.method public final cancelConfigureItemCard(Z)V
    .locals 3

    if-eqz p1, :cond_1

    .line 479
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/invoices/edit/items/ItemSelectScreen;

    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceScopeV2:Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    if-nez v1, :cond_0

    const-string v2, "editInvoiceScopeV2"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v1, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v0, v1}, Lcom/squareup/invoices/edit/items/ItemSelectScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 481
    :cond_1
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingOrderEditor:Lcom/squareup/invoices/order/WorkingOrderEditor;

    .line 482
    invoke-virtual {p1}, Lcom/squareup/invoices/order/WorkingOrderEditor;->workingOrder()Lio/reactivex/Observable;

    move-result-object p1

    .line 483
    invoke-virtual {p1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object p1

    .line 484
    new-instance v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$cancelConfigureItemCard$1;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$cancelConfigureItemCard$1;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v0, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const-string/jumbo v0, "workingOrderEditor\n     \u2026            }\n          }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 493
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {p1, v0}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    :goto_0
    return-void
.end method

.method public closeScreen()V
    .locals 2

    .line 705
    iget-boolean v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->inCheckout:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->transaction:Lcom/squareup/payment/Transaction;

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Transaction;->dropPayment(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 706
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public confirmationScreenData()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;",
            ">;"
        }
    .end annotation

    .line 710
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->confirmationScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method public copyLink()V
    .locals 2

    .line 743
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    .line 744
    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->workingInvoice()Lio/reactivex/Observable;

    move-result-object v0

    .line 745
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 746
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$copyLink$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$copyLink$1;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string/jumbo v1, "workingInvoiceEditor\n   \u2026PLET_COPY_LINK)\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 752
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {v0, v1}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    return-void
.end method

.method public editInvoice1ScreenData()Lio/reactivex/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData;",
            ">;"
        }
    .end annotation

    .line 364
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 365
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->workingInvoice()Lio/reactivex/Observable;

    move-result-object v1

    .line 366
    iget-object v2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingRecurrenceRuleEditor:Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;

    invoke-virtual {v2}, Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;->rule()Lio/reactivex/Observable;

    move-result-object v2

    .line 367
    iget-object v3, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v3, Lio/reactivex/Observable;

    .line 364
    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 369
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoice1ScreenData$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoice1ScreenData$1;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 376
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoice1ScreenData$2;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoice1ScreenData$2;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observables.combineLates\u2026out\n          )\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public editInvoice2ScreenData()Lio/reactivex/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;",
            ">;"
        }
    .end annotation

    .line 718
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 719
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->workingInvoice()Lio/reactivex/Observable;

    move-result-object v1

    .line 720
    iget-object v2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingRecurrenceRuleEditor:Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;

    invoke-virtual {v2}, Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;->rule()Lio/reactivex/Observable;

    move-result-object v2

    .line 721
    iget-object v3, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v3, Lio/reactivex/Observable;

    .line 718
    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 723
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoice2ScreenData$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoice2ScreenData$1;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 730
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoice2ScreenData$2;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoice2ScreenData$2;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observables.combineLates\u2026ull\n          )\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    const-string v0, "EditInvoiceScopeRunnerV2"

    return-object v0
.end method

.method public goBackFrom2Screen()V
    .locals 0

    .line 739
    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goBackToEditInvoice1Screen()V

    return-void
.end method

.method public goBackFromConfirmationScreen(Z)V
    .locals 0

    if-eqz p1, :cond_0

    .line 714
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->invoicesAppletRunner:Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    invoke-interface {p1}, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;->cancelInvoiceConfirmation()V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->flow:Lflow/Flow;

    invoke-virtual {p1}, Lflow/Flow;->goBack()Z

    :goto_0
    return-void
.end method

.method public goBackFromItemSelect()V
    .locals 0

    .line 498
    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goBackToEditInvoice1Screen()V

    return-void
.end method

.method public goBackFromOverflowDialog()V
    .locals 2

    .line 793
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->flow:Lflow/Flow;

    .line 1751
    const-class v1, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public goBackFromSetupPaymentsDialog()V
    .locals 2

    .line 585
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->flow:Lflow/Flow;

    .line 1750
    const-class v1, Lcom/squareup/setupguide/SetupPaymentsDialog;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public final goBackToEditInvoice1Screen()V
    .locals 4

    .line 770
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen;

    iget-object v2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceScopeV2:Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    if-nez v2, :cond_0

    const-string v3, "editInvoiceScopeV2"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {v1, v2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public goToEditInvoice2Screen()V
    .locals 4

    .line 385
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    const-string v1, "editInvoiceContext"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isNew()Z

    move-result v0

    const-string/jumbo v2, "workingInvoiceEditor.wor\u2026            }\n          }"

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isDraft()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 423
    :cond_2
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->workingInvoice()Lio/reactivex/Observable;

    move-result-object v0

    .line 424
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 425
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$goToEditInvoice2Screen$4;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$goToEditInvoice2Screen$4;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 426
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$goToEditInvoice2Screen$5;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$goToEditInvoice2Screen$5;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 433
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {v0, v1}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    goto :goto_1

    .line 388
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->workingInvoice()Lio/reactivex/Observable;

    move-result-object v0

    .line 389
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 390
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$goToEditInvoice2Screen$1;

    iget-object v3, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->invoiceValidator:Lcom/squareup/invoices/editv2/validation/InvoiceValidator;

    invoke-direct {v1, v3}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$goToEditInvoice2Screen$1;-><init>(Lcom/squareup/invoices/editv2/validation/InvoiceValidator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v3, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2Kt$sam$io_reactivex_functions_Function$0;

    invoke-direct {v3, v1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2Kt$sam$io_reactivex_functions_Function$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v3, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v3}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 391
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$goToEditInvoice2Screen$2;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$goToEditInvoice2Screen$2;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 400
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$goToEditInvoice2Screen$3;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$goToEditInvoice2Screen$3;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 421
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {v0, v1}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    :goto_1
    return-void
.end method

.method public moreOptionsPressed()V
    .locals 2

    .line 756
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_SHARE_LINK:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 757
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    .line 758
    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->workingInvoice()Lio/reactivex/Observable;

    move-result-object v0

    .line 759
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 760
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$moreOptionsPressed$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$moreOptionsPressed$1;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string/jumbo v1, "workingInvoiceEditor\n   \u2026ICE\n          )\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 766
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {v0, v1}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 295
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    const-string v1, "RegisterTreeKey.get(scope)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    iput-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceScopeV2:Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    .line 296
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceScopeV2:Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    const-string v1, "editInvoiceScopeV2"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;->getEditInvoiceContext()Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    .line 297
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceScopeV2:Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;->getInCheckout()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->inCheckout:Z

    .line 299
    iget-boolean v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->inCheckout:Z

    const-string v1, "editInvoiceContext"

    if-eqz v0, :cond_2

    .line 301
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    sget-object v2, Lcom/squareup/comms/protos/common/TenderType;->INVOICE:Lcom/squareup/comms/protos/common/TenderType;

    invoke-interface {v0, v2}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->configuringTender(Lcom/squareup/comms/protos/common/TenderType;)Z

    .line 302
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    iget-object v2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0, v2}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->loadFromTransaction(Lcom/squareup/payment/Transaction;)V

    goto :goto_0

    .line 304
    :cond_2
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    iget-object v2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    if-nez v2, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v0, v2}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->init(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)V

    .line 306
    :goto_0
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingRecurrenceRuleEditor:Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;

    iget-object v2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    if-nez v2, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v2}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->getInitialRecurrenceRule()Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;->set(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)V

    .line 308
    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunnerKt;->getEditInvoiceWorkflowRunner(Lmortar/MortarScope;)Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceWorkflowRunner:Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

    .line 310
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceWorkflowRunner:Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

    const-string v1, "editInvoiceWorkflowRunner"

    if-nez v0, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-interface {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    .line 311
    new-instance v2, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onEnterScope$1;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onEnterScope$1;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v2}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 313
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceWorkflowRunner:Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

    if-nez v0, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-interface {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    .line 314
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onEnterScope$2;

    move-object v2, p0

    check-cast v2, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-direct {v1, v2}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onEnterScope$2;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 316
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->chooseCustomerFlow:Lcom/squareup/ui/crm/ChooseCustomerFlow;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/ChooseCustomerFlow;->getResults()Lrx/Observable;

    move-result-object v0

    .line 317
    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 318
    sget-object v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onEnterScope$3;->INSTANCE:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onEnterScope$3;

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "chooseCustomerFlow.resul\u2026sultKey == EDIT_INVOICE }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 319
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onEnterScope$4;

    invoke-direct {v1, v2}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onEnterScope$4;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 321
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->keypadRunner:Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;->events()Lio/reactivex/Observable;

    move-result-object v0

    .line 322
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onEnterScope$5;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onEnterScope$5;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 336
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->analytics:Lcom/squareup/analytics/Analytics;

    .line 337
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_FLOW_V2_WIDGETS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 338
    sget-object v0, Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent$NewWidgets;->INSTANCE:Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent$NewWidgets;

    check-cast v0, Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent;

    goto :goto_1

    .line 340
    :cond_7
    sget-object v0, Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent$OldWidgets;->INSTANCE:Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent$OldWidgets;

    check-cast v0, Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent;

    .line 337
    :goto_1
    check-cast v0, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 336
    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public onEvent(Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;)V
    .locals 1

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 507
    instance-of v0, p1, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$Simple;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$Simple;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->handleSimpleEvent(Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$Simple;)V

    goto :goto_0

    .line 508
    :cond_0
    instance-of v0, p1, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$ToggleClicked;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$ToggleClicked;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->handleToggleEvent(Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$ToggleClicked;)V

    goto :goto_0

    .line 509
    :cond_1
    instance-of v0, p1, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$AttachmentClicked;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$AttachmentClicked;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->handleAttachmentRowClicked(Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$AttachmentClicked;)V

    goto :goto_0

    .line 510
    :cond_2
    instance-of v0, p1, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$LineItemClicked;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$LineItemClicked;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->handleLineItemClicked(Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$LineItemClicked;)V

    goto :goto_0

    .line 511
    :cond_3
    instance-of v0, p1, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$PaymentRequestClicked;

    if-eqz v0, :cond_4

    check-cast p1, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$PaymentRequestClicked;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->handlePaymentRequestClicked(Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$PaymentRequestClicked;)V

    :cond_4
    :goto_0
    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 360
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 3

    if-eqz p1, :cond_1

    .line 347
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    if-nez v1, :cond_0

    const-string v2, "editInvoiceContext"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0, p1, v1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2SerializersKt;->restoreFromBundle(Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;Landroid/os/Bundle;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)V

    .line 348
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingRecurrenceRuleEditor:Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;

    invoke-static {v0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2SerializersKt;->restoreFromBundle(Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;Landroid/os/Bundle;)V

    :cond_1
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 1

    const-string v0, "outState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 355
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-static {v0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2SerializersKt;->writeInvoiceToBundle(Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;Landroid/os/Bundle;)V

    .line 356
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingRecurrenceRuleEditor:Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;

    invoke-static {v0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2SerializersKt;->writeRuleToBundle(Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;Landroid/os/Bundle;)V

    return-void
.end method

.method public onSaveDraftClicked()V
    .locals 0

    .line 578
    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->saveDraft()V

    return-void
.end method

.method public onSendInvoiceClicked()V
    .locals 3

    .line 631
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->shouldCompleteOnboardingSetUp()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 632
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->IPOS_NO_IDV_EXPERIMENT_ROLLOUT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->featureEnabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Observable;

    move-result-object v0

    .line 633
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onSendInvoiceClicked$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onSendInvoiceClicked$1;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 640
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onSendInvoiceClicked$2;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onSendInvoiceClicked$2;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 651
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    .line 652
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 653
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onSendInvoiceClicked$3;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onSendInvoiceClicked$3;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "features.featureEnabled(\u2026oiceScopeV2))\n          }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 657
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {v0, v1}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    goto :goto_0

    .line 659
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->attemptSendInvoice()V

    :goto_0
    return-void
.end method

.method public onSetupPaymentsDialogPrimaryClicked(Ljava/lang/Object;)V
    .locals 2

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 521
    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$SetupPaymentsDialogKey;->SAVE_DRAFT:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$SetupPaymentsDialogKey;

    if-ne p1, v0, :cond_0

    .line 522
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/invoices/analytics/SetupPaymentDialogSetupPaymentTapped;

    const-string v1, "Save Invoice"

    invoke-direct {v0, v1}, Lcom/squareup/invoices/analytics/SetupPaymentDialogSetupPaymentTapped;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 523
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    sget-object v0, Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;->RESTART:Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;

    invoke-interface {p1, v0}, Lcom/squareup/onboarding/OnboardingDiverter;->maybeDivertToOnboardingOrBank(Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;)V

    goto :goto_0

    .line 525
    :cond_0
    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$SetupPaymentsDialogKey;->SEND_INVOICE:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$SetupPaymentsDialogKey;

    if-ne p1, v0, :cond_1

    .line 526
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/invoices/analytics/SetupPaymentDialogSetupPaymentTapped;

    const-string v1, "Send Invoice"

    invoke-direct {v0, v1}, Lcom/squareup/invoices/analytics/SetupPaymentDialogSetupPaymentTapped;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 527
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->saveDraftSingle()Lio/reactivex/Single;

    move-result-object p1

    .line 528
    new-instance v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onSetupPaymentsDialogPrimaryClicked$1;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onSetupPaymentsDialogPrimaryClicked$1;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v0, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    :cond_1
    :goto_0
    return-void
.end method

.method public onSetupPaymentsDialogSecondaryClicked(Ljava/lang/Object;)V
    .locals 3

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 541
    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$SetupPaymentsDialogKey;->SAVE_DRAFT:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$SetupPaymentsDialogKey;

    if-ne p1, v0, :cond_1

    .line 542
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/invoices/analytics/SetupPaymentDialogLaterTapped;

    const-string v1, "Save Invoice"

    invoke-direct {v0, v1}, Lcom/squareup/invoices/analytics/SetupPaymentDialogLaterTapped;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 543
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->confirmationScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 544
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->confirmationScreenDataFactory:Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;

    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->createSaveDraftSuccess()Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    move-result-object v0

    .line 543
    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 546
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen;

    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceScopeV2:Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    if-nez v1, :cond_0

    const-string v2, "editInvoiceScopeV2"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {v0, v1}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 548
    :cond_1
    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$SetupPaymentsDialogKey;->SEND_INVOICE:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$SetupPaymentsDialogKey;

    if-ne p1, v0, :cond_2

    .line 549
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->features:Lcom/squareup/settings/server/Features;

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->IPOS_NO_IDV_EXPERIMENT_ROLLOUT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->featureEnabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Observable;

    move-result-object p1

    .line 550
    new-instance v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onSetupPaymentsDialogSecondaryClicked$1;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onSetupPaymentsDialogSecondaryClicked$1;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 557
    invoke-virtual {p1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object p1

    .line 558
    new-instance v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onSetupPaymentsDialogSecondaryClicked$2;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onSetupPaymentsDialogSecondaryClicked$2;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v0, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const-string v0, "features.featureEnabled(\u2026          }\n            }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 568
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {p1, v0}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public overflowDialogScreenData()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData;",
            ">;"
        }
    .end annotation

    .line 797
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceOverflowDialogDataFactory:Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$Factory;

    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    if-nez v1, :cond_0

    const-string v2, "editInvoiceContext"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, v1}, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$Factory;->create(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public final saveDraft()V
    .locals 2

    .line 590
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_SAVE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 591
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->saveDraftSingle()Lio/reactivex/Single;

    move-result-object v0

    .line 592
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$saveDraft$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$saveDraft$1;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "saveDraftSingle()\n      \u2026se)\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 602
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {v0, v1}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    return-void
.end method

.method public final sendInvoice()V
    .locals 3

    .line 690
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 691
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingInvoiceEditor:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->workingInvoice()Lio/reactivex/Observable;

    move-result-object v1

    .line 692
    iget-object v2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->workingRecurrenceRuleEditor:Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;

    invoke-virtual {v2}, Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;->rule()Lio/reactivex/Observable;

    move-result-object v2

    .line 690
    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 694
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 695
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$sendInvoice$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$sendInvoice$1;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 700
    new-instance v1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$sendInvoice$2;

    move-object v2, p0

    check-cast v2, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-direct {v1, v2}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$sendInvoice$2;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2Kt$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2Kt$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "Observables.combineLates\u2026oSendInvoiceConfirmation)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 701
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {v0, v1}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    return-void
.end method

.method public setupPaymentsDialogScreenData()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;",
            ">;"
        }
    .end annotation

    .line 582
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->setupGuideDialogScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method public showOverflowDialog()V
    .locals 4

    .line 502
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog;

    iget-object v2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceScopeV2:Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    if-nez v2, :cond_0

    const-string v3, "editInvoiceScopeV2"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {v1, v2}, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public startEditingDiscount(Lcom/squareup/configure/item/WorkingDiscount;Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 1

    const-string/jumbo v0, "workingDiscount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "info"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parentPath"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 473
    iget-object p3, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->keypadRunner:Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;

    invoke-virtual {p3, p1, p2}, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;->setupToEditDiscount(Lcom/squareup/configure/item/WorkingDiscount;Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;)V

    .line 474
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->flow:Lflow/Flow;

    new-instance p2, Lcom/squareup/orderentry/KeypadEntryScreen;

    iget-object p3, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceScopeV2:Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    if-nez p3, :cond_0

    const-string v0, "editInvoiceScopeV2"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p3, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {p2, p3}, Lcom/squareup/orderentry/KeypadEntryScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p1, p2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public startEditingItemVariablePrice(Lcom/squareup/configure/item/WorkingItem;Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 2

    const-string/jumbo v0, "workingItem"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parentPath"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 441
    iget-object p2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->keypadRunner:Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;

    .line 442
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/configure/item/R$string;->add:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 441
    invoke-virtual {p2, p1, v0}, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;->setupToEditItemVariablePrice(Lcom/squareup/configure/item/WorkingItem;Ljava/lang/String;)V

    .line 444
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->flow:Lflow/Flow;

    new-instance p2, Lcom/squareup/orderentry/KeypadEntryScreen;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceScopeV2:Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    if-nez v0, :cond_0

    const-string v1, "editInvoiceScopeV2"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {p2, v0}, Lcom/squareup/orderentry/KeypadEntryScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p1, p2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public startEditingItemWithModifiers(Lcom/squareup/configure/item/WorkingItem;ZLcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 3

    const-string/jumbo v0, "workingItem"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parentPath"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 452
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->keypadRunner:Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;->setupToEditItem(Lcom/squareup/configure/item/WorkingItem;)V

    .line 453
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/container/CalculatedKey;

    new-instance v2, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$startEditingItemWithModifiers$1;

    invoke-direct {v2, p3, p1, p2}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$startEditingItemWithModifiers$1;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/configure/item/WorkingItem;Z)V

    check-cast v2, Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;

    invoke-direct {v1, v2}, Lcom/squareup/container/CalculatedKey;-><init>(Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public startEditingOrder(I)V
    .locals 4

    .line 465
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/configure/item/InvoiceConfigureItemDetailScreen;

    iget-object v2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoiceScopeV2:Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    if-nez v2, :cond_0

    const-string v3, "editInvoiceScopeV2"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v2, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v1, v2, p1}, Lcom/squareup/configure/item/InvoiceConfigureItemDetailScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;I)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
