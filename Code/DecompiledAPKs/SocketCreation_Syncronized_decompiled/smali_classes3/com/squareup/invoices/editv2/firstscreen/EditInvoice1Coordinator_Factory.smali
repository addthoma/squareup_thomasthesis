.class public final Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator_Factory;
.super Ljava/lang/Object;
.source "EditInvoice1Coordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final glassSpinnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceSectionContainerViewFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen$Runner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator_Factory;->invoiceSectionContainerViewFactoryProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator_Factory;->glassSpinnerProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator_Factory;"
        }
    .end annotation

    .line 45
    new-instance v0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen$Runner;Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerViewFactory;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/settings/server/Features;)Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;
    .locals 1

    .line 51
    new-instance v0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;-><init>(Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen$Runner;Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerViewFactory;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;
    .locals 4

    .line 38
    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen$Runner;

    iget-object v1, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator_Factory;->invoiceSectionContainerViewFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerViewFactory;

    iget-object v2, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator_Factory;->glassSpinnerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/register/widgets/GlassSpinner;

    iget-object v3, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator_Factory;->newInstance(Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen$Runner;Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerViewFactory;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/settings/server/Features;)Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator_Factory;->get()Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;

    move-result-object v0

    return-object v0
.end method
