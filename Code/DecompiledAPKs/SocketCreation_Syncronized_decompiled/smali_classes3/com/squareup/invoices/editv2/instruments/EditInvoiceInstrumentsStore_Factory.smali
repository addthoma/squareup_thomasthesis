.class public final Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore_Factory;
.super Ljava/lang/Object;
.source "EditInvoiceInstrumentsStore_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;",
        ">;"
    }
.end annotation


# instance fields
.field private final rolodexProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore_Factory;->rolodexProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;)",
            "Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore_Factory;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/crm/RolodexServiceHelper;)Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;
    .locals 1

    .line 34
    new-instance v0, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;-><init>(Lcom/squareup/crm/RolodexServiceHelper;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore_Factory;->rolodexProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/crm/RolodexServiceHelper;

    invoke-static {v0}, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore_Factory;->newInstance(Lcom/squareup/crm/RolodexServiceHelper;)Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore_Factory;->get()Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;

    move-result-object v0

    return-object v0
.end method
