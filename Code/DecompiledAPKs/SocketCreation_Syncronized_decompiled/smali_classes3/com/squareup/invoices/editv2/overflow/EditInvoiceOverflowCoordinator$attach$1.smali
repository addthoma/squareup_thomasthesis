.class final Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator$attach$1;
.super Ljava/lang/Object;
.source "EditInvoiceOverflowCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator$attach$1;->this$0:Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData;)V
    .locals 3

    .line 29
    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData;->getButtonData()Ljava/util/List;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator$attach$1;->this$0:Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;

    invoke-static {v0}, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;->access$getButtonContainer$p(Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;)Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator$attach$1;->this$0:Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;

    invoke-static {v1}, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;->access$getFactory$p(Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;)Lcom/squareup/features/invoices/widgets/bottomsheet/BottomSheetDialogViewFactory;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator$attach$1;->this$0:Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;

    invoke-static {v2}, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;->access$getRunner$p(Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;)Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog$Runner;

    move-result-object v2

    check-cast v2, Lcom/squareup/features/invoices/widgets/EventHandler;

    invoke-static {p1, v0, v1, v2}, Lcom/squareup/features/invoices/widgets/bottomsheet/BottomSheetDialogViewFactoryKt;->setOn(Ljava/util/List;Landroid/view/ViewGroup;Lcom/squareup/features/invoices/widgets/bottomsheet/BottomSheetDialogViewFactory;Lcom/squareup/features/invoices/widgets/EventHandler;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 15
    check-cast p1, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator$attach$1;->accept(Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData;)V

    return-void
.end method
