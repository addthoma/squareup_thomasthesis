.class final Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore$instruments$2;
.super Ljava/lang/Object;
.source "EditInvoiceInstrumentsStore.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;->instruments(Ljava/lang/String;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Ljava/util/List<",
        "+",
        "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u001a\u0010\u0002\u001a\u0016\u0012\u0004\u0012\u00020\u0004 \u0005*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $contactToken:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore$instruments$2;->this$0:Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;

    iput-object p2, p0, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore$instruments$2;->$contactToken:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 27
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore$instruments$2;->accept(Ljava/util/List;)V

    return-void
.end method

.method public final accept(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
            ">;)V"
        }
    .end annotation

    .line 71
    iget-object v0, p0, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore$instruments$2;->this$0:Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;

    invoke-static {v0}, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;->access$getInstrumentsForContact$p(Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/editv2/instruments/InstrumentsForContact;

    iget-object v2, p0, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore$instruments$2;->$contactToken:Ljava/lang/String;

    const-string v3, "it"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v2, p1}, Lcom/squareup/invoices/editv2/instruments/InstrumentsForContact;-><init>(Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
