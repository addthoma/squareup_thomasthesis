.class final Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$goToEditInvoice2Screen$3;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunnerV2.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goToEditInvoice2Screen()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/invoices/editv2/SaveDraftResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "result",
        "Lcom/squareup/invoices/editv2/SaveDraftResult;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$goToEditInvoice2Screen$3;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/invoices/editv2/SaveDraftResult;)V
    .locals 5

    .line 402
    instance-of v0, p1, Lcom/squareup/invoices/editv2/SaveDraftResult$ValidationError;

    if-eqz v0, :cond_0

    .line 403
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$goToEditInvoice2Screen$3;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getFlow$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lflow/Flow;

    move-result-object v0

    .line 404
    new-instance v1, Lcom/squareup/register/widgets/WarningDialogScreen;

    .line 405
    new-instance v2, Lcom/squareup/widgets/warning/WarningStrings;

    .line 406
    iget-object v3, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$goToEditInvoice2Screen$3;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v3}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getRes$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/util/Res;

    move-result-object v3

    sget v4, Lcom/squareup/common/strings/R$string;->error_default:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 407
    check-cast p1, Lcom/squareup/invoices/editv2/SaveDraftResult$ValidationError;

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/SaveDraftResult$ValidationError;->getValidationResult()Lcom/squareup/invoices/editv2/validation/ValidationResult$InvalidWithMessage;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/validation/ValidationResult$InvalidWithMessage;->getErrorMessage()Ljava/lang/String;

    move-result-object p1

    .line 405
    invoke-direct {v2, v3, p1}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/widgets/warning/Warning;

    .line 404
    invoke-direct {v1, v2}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    .line 403
    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 412
    :cond_0
    instance-of v0, p1, Lcom/squareup/invoices/editv2/SaveDraftResult$DraftSaved;

    if-eqz v0, :cond_2

    .line 413
    check-cast p1, Lcom/squareup/invoices/editv2/SaveDraftResult$DraftSaved;

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/SaveDraftResult$DraftSaved;->getResponse()Lcom/squareup/invoices/editv2/service/SaveDraftResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/service/SaveDraftResponse;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 414
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$goToEditInvoice2Screen$3;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getFlow$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lflow/Flow;

    move-result-object p1

    new-instance v0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Screen;

    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$goToEditInvoice2Screen$3;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getEditInvoiceScopeV2$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Screen;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 416
    :cond_1
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$goToEditInvoice2Screen$3;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/SaveDraftResult$DraftSaved;->getResponse()Lcom/squareup/invoices/editv2/service/SaveDraftResponse;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$showSaveDraftFailure(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/invoices/editv2/service/SaveDraftResponse;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 236
    check-cast p1, Lcom/squareup/invoices/editv2/SaveDraftResult;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$goToEditInvoice2Screen$3;->accept(Lcom/squareup/invoices/editv2/SaveDraftResult;)V

    return-void
.end method
