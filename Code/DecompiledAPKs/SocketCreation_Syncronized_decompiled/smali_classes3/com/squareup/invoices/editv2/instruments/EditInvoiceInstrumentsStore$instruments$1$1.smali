.class final Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore$instruments$1$1;
.super Ljava/lang/Object;
.source "EditInvoiceInstrumentsStore.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore$instruments$1;->apply(Lcom/squareup/invoices/editv2/instruments/InstrumentsForContact;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditInvoiceInstrumentsStore.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditInvoiceInstrumentsStore.kt\ncom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore$instruments$1$1\n*L\n1#1,114:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
        "kotlin.jvm.PlatformType",
        "successOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/rolodex/GetContactResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore$instruments$1$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore$instruments$1$1;

    invoke-direct {v0}, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore$instruments$1$1;-><init>()V

    sput-object v0, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore$instruments$1$1;->INSTANCE:Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore$instruments$1$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore$instruments$1$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/GetContactResponse;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
            ">;"
        }
    .end annotation

    const-string v0, "successOrFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    goto :goto_1

    .line 63
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_3

    .line 64
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/GetContactResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/GetContactResponse;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    if-eqz p1, :cond_1

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact;->instruments_on_file:Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;

    if-eqz p1, :cond_1

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;->instrument:Ljava/util/List;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_2

    goto :goto_1

    :cond_2
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    :goto_1
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
