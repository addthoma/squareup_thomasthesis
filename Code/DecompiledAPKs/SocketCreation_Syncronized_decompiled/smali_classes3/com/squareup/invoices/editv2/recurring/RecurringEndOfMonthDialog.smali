.class public final Lcom/squareup/invoices/editv2/recurring/RecurringEndOfMonthDialog;
.super Lcom/squareup/invoices/editv2/InEditInvoiceScopeV2;
.source "RecurringEndOfMonthDialog.kt"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/invoices/editv2/recurring/RecurringEndOfMonthDialog$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/editv2/recurring/RecurringEndOfMonthDialog$Factory;,
        Lcom/squareup/invoices/editv2/recurring/RecurringEndOfMonthDialog$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRecurringEndOfMonthDialog.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RecurringEndOfMonthDialog.kt\ncom/squareup/invoices/editv2/recurring/RecurringEndOfMonthDialog\n+ 2 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,58:1\n24#2,4:59\n*E\n*S KotlinDebug\n*F\n+ 1 RecurringEndOfMonthDialog.kt\ncom/squareup/invoices/editv2/recurring/RecurringEndOfMonthDialog\n*L\n51#1,4:59\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0007\u0018\u0000 \u000b2\u00020\u0001:\u0002\u000b\u000cB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0014\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/recurring/RecurringEndOfMonthDialog;",
        "Lcom/squareup/invoices/editv2/InEditInvoiceScopeV2;",
        "editInvoiceScopeV2",
        "Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;",
        "(Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;)V",
        "doWriteToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "",
        "Companion",
        "Factory",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/invoices/editv2/recurring/RecurringEndOfMonthDialog;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/invoices/editv2/recurring/RecurringEndOfMonthDialog$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/editv2/recurring/RecurringEndOfMonthDialog$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/editv2/recurring/RecurringEndOfMonthDialog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/editv2/recurring/RecurringEndOfMonthDialog;->Companion:Lcom/squareup/invoices/editv2/recurring/RecurringEndOfMonthDialog$Companion;

    .line 59
    new-instance v0, Lcom/squareup/invoices/editv2/recurring/RecurringEndOfMonthDialog$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/invoices/editv2/recurring/RecurringEndOfMonthDialog$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 62
    sput-object v0, Lcom/squareup/invoices/editv2/recurring/RecurringEndOfMonthDialog;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;)V
    .locals 1

    const-string v0, "editInvoiceScopeV2"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/InEditInvoiceScopeV2;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;)V

    return-void
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-super {p0, p1, p2}, Lcom/squareup/invoices/editv2/InEditInvoiceScopeV2;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 47
    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/recurring/RecurringEndOfMonthDialog;->getEditInvoiceScopeV2()Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
