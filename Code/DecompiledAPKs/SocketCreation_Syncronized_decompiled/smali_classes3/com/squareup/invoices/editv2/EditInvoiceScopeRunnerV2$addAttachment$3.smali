.class final Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$addAttachment$3;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunnerV2.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->addAttachment()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/invoices/editv2/AddAttachmentPreparationResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "addAttachmentPreparationResult",
        "Lcom/squareup/invoices/editv2/AddAttachmentPreparationResult;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$addAttachment$3;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/invoices/editv2/AddAttachmentPreparationResult;)V
    .locals 7

    .line 1563
    instance-of v0, p1, Lcom/squareup/invoices/editv2/AddAttachmentPreparationResult$ValidationError;

    if-eqz v0, :cond_0

    .line 1564
    check-cast p1, Lcom/squareup/invoices/editv2/AddAttachmentPreparationResult$ValidationError;

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/AddAttachmentPreparationResult$ValidationError;->getValidationResult()Lcom/squareup/invoices/image/FileValidationResult$Failure;

    move-result-object p1

    .line 1565
    new-instance v0, Lcom/squareup/widgets/warning/WarningStrings;

    invoke-virtual {p1}, Lcom/squareup/invoices/image/FileValidationResult$Failure;->getErrorTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/invoices/image/FileValidationResult$Failure;->getErrorBody()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1566
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$addAttachment$3;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getFlow$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lflow/Flow;

    move-result-object p1

    new-instance v1, Lcom/squareup/register/widgets/WarningDialogScreen;

    check-cast v0, Lcom/squareup/widgets/warning/Warning;

    const/4 v2, 0x0

    const-string v3, "file attachment count limit"

    invoke-direct {v1, v0, v2, v3}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;ZLjava/lang/String;)V

    invoke-virtual {p1, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 1568
    :cond_0
    instance-of v0, p1, Lcom/squareup/invoices/editv2/AddAttachmentPreparationResult$SaveDraftSuccess;

    if-eqz v0, :cond_1

    .line 1569
    check-cast p1, Lcom/squareup/invoices/editv2/AddAttachmentPreparationResult$SaveDraftSuccess;

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/AddAttachmentPreparationResult$SaveDraftSuccess;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object p1

    .line 1570
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$addAttachment$3;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getEditInvoiceWorkflowRunner$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

    move-result-object v0

    .line 1573
    new-instance v1, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Upload;

    .line 1574
    new-instance v2, Lcom/squareup/invoices/workflow/edit/InvoiceTokenType$Invoice;

    .line 1575
    iget-object v3, p1, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, v3, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    const-string v4, "invoice.id_pair.server_id"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1574
    invoke-direct {v2, v3}, Lcom/squareup/invoices/workflow/edit/InvoiceTokenType$Invoice;-><init>(Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    .line 1577
    iget-object v3, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$addAttachment$3;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v3}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getInvoiceFileHelper$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/image/InvoiceFileHelper;

    move-result-object v3

    .line 1578
    iget-object v4, p1, Lcom/squareup/protos/client/invoice/Invoice;->attachment:Ljava/util/List;

    sget v5, Lcom/squareup/features/invoices/R$string;->invoice_attachment_default_name:I

    .line 1577
    invoke-virtual {v3, v4, v5}, Lcom/squareup/invoices/image/InvoiceFileHelper;->nextFileAttachmentDefaultTitle(Ljava/util/List;I)Ljava/lang/String;

    move-result-object v3

    .line 1580
    new-instance v4, Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;

    .line 1581
    invoke-static {p1}, Lcom/squareup/invoices/image/InvoiceFileHelperKt;->getTotalSizeBytesUploaded(Lcom/squareup/protos/client/invoice/Invoice;)J

    move-result-wide v5

    .line 1580
    invoke-direct {v4, v5, v6}, Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;-><init>(J)V

    .line 1573
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Upload;-><init>(Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;)V

    check-cast v1, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo;

    .line 1570
    invoke-interface {v0, v1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;->startInvoiceAttachment(Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo;)V

    goto :goto_0

    .line 1586
    :cond_1
    instance-of v0, p1, Lcom/squareup/invoices/editv2/AddAttachmentPreparationResult$SaveDraftFailure;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$addAttachment$3;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    .line 1587
    check-cast p1, Lcom/squareup/invoices/editv2/AddAttachmentPreparationResult$SaveDraftFailure;

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/AddAttachmentPreparationResult$SaveDraftFailure;->getSaveDraftResponse()Lcom/squareup/invoices/editv2/service/SaveDraftResponse;

    move-result-object p1

    .line 1586
    invoke-static {v0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$showSaveDraftFailure(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/invoices/editv2/service/SaveDraftResponse;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 236
    check-cast p1, Lcom/squareup/invoices/editv2/AddAttachmentPreparationResult;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$addAttachment$3;->accept(Lcom/squareup/invoices/editv2/AddAttachmentPreparationResult;)V

    return-void
.end method
