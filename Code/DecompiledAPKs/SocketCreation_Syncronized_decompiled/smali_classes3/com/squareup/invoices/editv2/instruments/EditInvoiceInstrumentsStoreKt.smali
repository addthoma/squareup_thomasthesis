.class public final Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStoreKt;
.super Ljava/lang/Object;
.source "EditInvoiceInstrumentsStore.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0002"
    }
    d2 = {
        "EMPTY",
        "Lcom/squareup/invoices/editv2/instruments/InstrumentsForContact;",
        "invoices-hairball_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final EMPTY:Lcom/squareup/invoices/editv2/instruments/InstrumentsForContact;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 113
    new-instance v0, Lcom/squareup/invoices/editv2/instruments/InstrumentsForContact;

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v2, v1}, Lcom/squareup/invoices/editv2/instruments/InstrumentsForContact;-><init>(Ljava/lang/String;Ljava/util/List;)V

    sput-object v0, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStoreKt;->EMPTY:Lcom/squareup/invoices/editv2/instruments/InstrumentsForContact;

    return-void
.end method

.method public static final synthetic access$getEMPTY$p()Lcom/squareup/invoices/editv2/instruments/InstrumentsForContact;
    .locals 1

    .line 1
    sget-object v0, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStoreKt;->EMPTY:Lcom/squareup/invoices/editv2/instruments/InstrumentsForContact;

    return-object v0
.end method
