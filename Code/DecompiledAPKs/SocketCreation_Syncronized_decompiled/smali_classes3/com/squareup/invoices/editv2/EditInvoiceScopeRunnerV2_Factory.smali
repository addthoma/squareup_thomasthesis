.class public final Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunnerV2_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerFlowStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final chooseCustomerFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/ChooseCustomerFlow;",
            ">;"
        }
    .end annotation
.end field

.field private final chooseDateInfoFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final confirmationScreenDataFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final currentTimeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;"
        }
    .end annotation
.end field

.field private final editInvoice1ScreenDataFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final editInvoice2ScreenDataFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final editInvoiceOverflowDialogDataFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final editInvoiceV2ServiceHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final failureMessageFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final instrumentsStoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceFileHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/InvoiceFileHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceFileValidatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/InvoiceFileValidator;",
            ">;"
        }
    .end annotation
.end field

.field private final invoicePreparerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/service/InvoicePreparer;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceShareUrlLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/url/InvoiceShareUrlLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceUnitCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceUrlHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/InvoiceUrlHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceValidatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/validation/InvoiceValidator;",
            ">;"
        }
    .end annotation
.end field

.field private final invoicesAppletRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final iposSkipOnboardingExperimentProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/ExperimentProfile;",
            ">;"
        }
    .end annotation
.end field

.field private final keypadRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final onboardingDiverterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            ">;"
        }
    .end annotation
.end field

.field private final recurringHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/recurring/RecurringHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final reminderInfoFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final setupGuideDialogScreenDataFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final shareLinkMessageFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/edit/InvoiceShareLinkMessageFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final workingInvoiceEditorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;",
            ">;"
        }
    .end annotation
.end field

.field private final workingOrderEditorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/order/WorkingOrderEditor;",
            ">;"
        }
    .end annotation
.end field

.field private final workingRecurrenceRuleEditorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;",
            ">;"
        }
    .end annotation
.end field

.field private final x2SellerScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/ChooseCustomerFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/order/WorkingOrderEditor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/service/InvoicePreparer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/recurring/RecurringHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/edit/InvoiceShareLinkMessageFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/InvoiceFileValidator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/InvoiceFileHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/InvoiceUrlHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/url/InvoiceShareUrlLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/validation/InvoiceValidator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/ExperimentProfile;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 158
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->editInvoice1ScreenDataFactoryProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 159
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->editInvoice2ScreenDataFactoryProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 160
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->editInvoiceOverflowDialogDataFactoryProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 161
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->setupGuideDialogScreenDataFactoryProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 162
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->workingInvoiceEditorProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 163
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->chooseCustomerFlowProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 164
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->workingOrderEditorProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 165
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->flowProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 166
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->x2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 167
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->workingRecurrenceRuleEditorProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 168
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->editInvoiceV2ServiceHelperProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 169
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->confirmationScreenDataFactoryProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 170
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->invoicesAppletRunnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 171
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->invoiceUnitCacheProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 172
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->failureMessageFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 173
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->invoicePreparerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 174
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->recurringHelperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 175
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->instrumentsStoreProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 176
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->resProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 177
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->currentTimeProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 178
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 179
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->shareLinkMessageFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 180
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->invoiceFileValidatorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p24

    .line 181
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->keypadRunnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p25

    .line 182
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->invoiceFileHelperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p26

    .line 183
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->chooseDateInfoFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p27

    .line 184
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->reminderInfoFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p28

    .line 185
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->invoiceUrlHelperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p29

    .line 186
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->invoiceShareUrlLauncherProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p30

    .line 187
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->invoiceValidatorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p31

    .line 188
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p32

    .line 189
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->onboardingDiverterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p33

    .line 190
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p34

    .line 191
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->iposSkipOnboardingExperimentProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p35

    .line 192
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p36

    .line 193
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p37

    .line 194
    iput-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->buyerFlowStarterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;
    .locals 39
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/ChooseCustomerFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/order/WorkingOrderEditor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/service/InvoicePreparer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/recurring/RecurringHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/edit/InvoiceShareLinkMessageFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/InvoiceFileValidator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/InvoiceFileHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/InvoiceUrlHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/url/InvoiceShareUrlLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/validation/InvoiceValidator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/ExperimentProfile;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            ">;)",
            "Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    move-object/from16 v30, p29

    move-object/from16 v31, p30

    move-object/from16 v32, p31

    move-object/from16 v33, p32

    move-object/from16 v34, p33

    move-object/from16 v35, p34

    move-object/from16 v36, p35

    move-object/from16 v37, p36

    .line 235
    new-instance v38, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;

    move-object/from16 v0, v38

    invoke-direct/range {v0 .. v37}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v38
.end method

.method public static newInstance(Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$Factory;Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;Lcom/squareup/ui/crm/ChooseCustomerFlow;Lcom/squareup/invoices/order/WorkingOrderEditor;Lflow/Flow;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/invoices/editv2/service/InvoicePreparer;Lcom/squareup/invoices/editv2/recurring/RecurringHelper;Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;Lcom/squareup/util/Res;Lcom/squareup/time/CurrentTime;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/invoices/ui/edit/InvoiceShareLinkMessageFactory;Lcom/squareup/invoices/image/InvoiceFileValidator;Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;Lcom/squareup/invoices/image/InvoiceFileHelper;Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;Lcom/squareup/invoices/InvoiceUrlHelper;Lcom/squareup/url/InvoiceShareUrlLauncher;Lcom/squareup/invoices/editv2/validation/InvoiceValidator;Lcom/squareup/analytics/Analytics;Lcom/squareup/onboarding/OnboardingDiverter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/experiments/ExperimentProfile;Lcom/squareup/settings/server/Features;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/buyer/BuyerFlowStarter;)Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;
    .locals 39

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    move-object/from16 v30, p29

    move-object/from16 v31, p30

    move-object/from16 v32, p31

    move-object/from16 v33, p32

    move-object/from16 v34, p33

    move-object/from16 v35, p34

    move-object/from16 v36, p35

    move-object/from16 v37, p36

    .line 261
    new-instance v38, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    move-object/from16 v0, v38

    invoke-direct/range {v0 .. v37}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;-><init>(Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$Factory;Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;Lcom/squareup/ui/crm/ChooseCustomerFlow;Lcom/squareup/invoices/order/WorkingOrderEditor;Lflow/Flow;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/invoices/editv2/service/InvoicePreparer;Lcom/squareup/invoices/editv2/recurring/RecurringHelper;Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;Lcom/squareup/util/Res;Lcom/squareup/time/CurrentTime;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/invoices/ui/edit/InvoiceShareLinkMessageFactory;Lcom/squareup/invoices/image/InvoiceFileValidator;Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;Lcom/squareup/invoices/image/InvoiceFileHelper;Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;Lcom/squareup/invoices/InvoiceUrlHelper;Lcom/squareup/url/InvoiceShareUrlLauncher;Lcom/squareup/invoices/editv2/validation/InvoiceValidator;Lcom/squareup/analytics/Analytics;Lcom/squareup/onboarding/OnboardingDiverter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/experiments/ExperimentProfile;Lcom/squareup/settings/server/Features;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/buyer/BuyerFlowStarter;)V

    return-object v38
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;
    .locals 39

    move-object/from16 v0, p0

    .line 199
    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->editInvoice1ScreenDataFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->editInvoice2ScreenDataFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->editInvoiceOverflowDialogDataFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$Factory;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->setupGuideDialogScreenDataFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->workingInvoiceEditorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->chooseCustomerFlowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/ui/crm/ChooseCustomerFlow;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->workingOrderEditorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/invoices/order/WorkingOrderEditor;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->x2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->workingRecurrenceRuleEditorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->editInvoiceV2ServiceHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->confirmationScreenDataFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->invoicesAppletRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->invoiceUnitCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->failureMessageFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/receiving/FailureMessageFactory;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->invoicePreparerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/invoices/editv2/service/InvoicePreparer;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->recurringHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/invoices/editv2/recurring/RecurringHelper;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->instrumentsStoreProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->currentTimeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/time/CurrentTime;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->shareLinkMessageFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/invoices/ui/edit/InvoiceShareLinkMessageFactory;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->invoiceFileValidatorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v24, v1

    check-cast v24, Lcom/squareup/invoices/image/InvoiceFileValidator;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->keypadRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v25, v1

    check-cast v25, Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->invoiceFileHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v26, v1

    check-cast v26, Lcom/squareup/invoices/image/InvoiceFileHelper;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->chooseDateInfoFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v27, v1

    check-cast v27, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->reminderInfoFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v28, v1

    check-cast v28, Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->invoiceUrlHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v29, v1

    check-cast v29, Lcom/squareup/invoices/InvoiceUrlHelper;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->invoiceShareUrlLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v30, v1

    check-cast v30, Lcom/squareup/url/InvoiceShareUrlLauncher;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->invoiceValidatorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v31, v1

    check-cast v31, Lcom/squareup/invoices/editv2/validation/InvoiceValidator;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v32, v1

    check-cast v32, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->onboardingDiverterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v33, v1

    check-cast v33, Lcom/squareup/onboarding/OnboardingDiverter;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v34, v1

    check-cast v34, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->iposSkipOnboardingExperimentProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v35, v1

    check-cast v35, Lcom/squareup/experiments/ExperimentProfile;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v36, v1

    check-cast v36, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v37, v1

    check-cast v37, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->buyerFlowStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v38, v1

    check-cast v38, Lcom/squareup/ui/buyer/BuyerFlowStarter;

    invoke-static/range {v2 .. v38}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->newInstance(Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$Factory;Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;Lcom/squareup/ui/crm/ChooseCustomerFlow;Lcom/squareup/invoices/order/WorkingOrderEditor;Lflow/Flow;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/invoices/editv2/service/InvoicePreparer;Lcom/squareup/invoices/editv2/recurring/RecurringHelper;Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;Lcom/squareup/util/Res;Lcom/squareup/time/CurrentTime;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/invoices/ui/edit/InvoiceShareLinkMessageFactory;Lcom/squareup/invoices/image/InvoiceFileValidator;Lcom/squareup/invoices/keypad/InvoiceKeypadRunner;Lcom/squareup/invoices/image/InvoiceFileHelper;Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;Lcom/squareup/invoices/InvoiceUrlHelper;Lcom/squareup/url/InvoiceShareUrlLauncher;Lcom/squareup/invoices/editv2/validation/InvoiceValidator;Lcom/squareup/analytics/Analytics;Lcom/squareup/onboarding/OnboardingDiverter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/experiments/ExperimentProfile;Lcom/squareup/settings/server/Features;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/buyer/BuyerFlowStarter;)Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 42
    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2_Factory;->get()Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    move-result-object v0

    return-object v0
.end method
