.class public final Lcom/squareup/invoices/editv2/validation/InvoiceValidator;
.super Ljava/lang/Object;
.source "InvoiceValidator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000e\u0008\u0001\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ \u0010\n\u001a\n \u000c*\u0004\u0018\u00010\u000b0\u000b2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u000e\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014J\u0010\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0016\u001a\u00020\u0006H\u0002J\u0012\u0010\u0017\u001a\u00020\u00122\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0002J\u0015\u0010\u001a\u001a\u00020\u0012*\u00020\u00122\u0006\u0010\u001b\u001a\u00020\u0012H\u0082\u0004R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/validation/InvoiceValidator;",
        "",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)V",
        "formatMoney",
        "",
        "kotlin.jvm.PlatformType",
        "amount",
        "",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "validate",
        "Lcom/squareup/invoices/editv2/validation/ValidationResult;",
        "invoice",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "validateAmount",
        "amountDue",
        "validateCustomer",
        "payer",
        "Lcom/squareup/protos/client/invoice/InvoiceContact;",
        "and",
        "other",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)V
    .locals 1
    .param p2    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/money/Shorter;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "settings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/editv2/validation/InvoiceValidator;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p2, p0, Lcom/squareup/invoices/editv2/validation/InvoiceValidator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p3, p0, Lcom/squareup/invoices/editv2/validation/InvoiceValidator;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method private final and(Lcom/squareup/invoices/editv2/validation/ValidationResult;Lcom/squareup/invoices/editv2/validation/ValidationResult;)Lcom/squareup/invoices/editv2/validation/ValidationResult;
    .locals 1

    .line 80
    instance-of v0, p1, Lcom/squareup/invoices/editv2/validation/ValidationResult$Valid;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    move-object p1, p2

    :goto_0
    return-object p1
.end method

.method private final formatMoney(JLcom/squareup/protos/common/CurrencyCode;)Ljava/lang/CharSequence;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/invoices/editv2/validation/InvoiceValidator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-static {p1, p2, p3}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method private final validateAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/invoices/editv2/validation/ValidationResult;
    .locals 9

    .line 48
    iget-object v0, p0, Lcom/squareup/invoices/editv2/validation/InvoiceValidator;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    const-string v1, "settings.paymentSettings"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->getTransactionMinimum()J

    move-result-wide v2

    .line 49
    iget-object v0, p0, Lcom/squareup/invoices/editv2/validation/InvoiceValidator;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->getTransactionMaximum()J

    move-result-wide v0

    .line 51
    iget-object v4, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-string v6, "amountDue.currency_code"

    const-string v7, "amount"

    cmp-long v8, v4, v2

    if-gez v8, :cond_0

    new-instance v0, Lcom/squareup/invoices/editv2/validation/ValidationResult$InvalidWithMessage$AmountOutOfRange;

    .line 52
    iget-object v1, p0, Lcom/squareup/invoices/editv2/validation/InvoiceValidator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/features/invoices/R$string;->payment_type_below_minimum_invoice:I

    invoke-interface {v1, v4}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 53
    iget-object p1, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v2, v3, p1}, Lcom/squareup/invoices/editv2/validation/InvoiceValidator;->formatMoney(JLcom/squareup/protos/common/CurrencyCode;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v1, v7, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 54
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 55
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 51
    invoke-direct {v0, p1}, Lcom/squareup/invoices/editv2/validation/ValidationResult$InvalidWithMessage$AmountOutOfRange;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/invoices/editv2/validation/ValidationResult;

    goto :goto_0

    .line 57
    :cond_0
    iget-object v2, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v4, v2, v0

    if-lez v4, :cond_1

    new-instance v2, Lcom/squareup/invoices/editv2/validation/ValidationResult$InvalidWithMessage$AmountOutOfRange;

    .line 58
    iget-object v3, p0, Lcom/squareup/invoices/editv2/validation/InvoiceValidator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/features/invoices/R$string;->payment_type_above_maximum_invoice:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    .line 59
    iget-object p1, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/invoices/editv2/validation/InvoiceValidator;->formatMoney(JLcom/squareup/protos/common/CurrencyCode;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v3, v7, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 60
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 61
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 57
    invoke-direct {v2, p1}, Lcom/squareup/invoices/editv2/validation/ValidationResult$InvalidWithMessage$AmountOutOfRange;-><init>(Ljava/lang/String;)V

    move-object v0, v2

    check-cast v0, Lcom/squareup/invoices/editv2/validation/ValidationResult;

    goto :goto_0

    .line 63
    :cond_1
    sget-object p1, Lcom/squareup/invoices/editv2/validation/ValidationResult$Valid;->INSTANCE:Lcom/squareup/invoices/editv2/validation/ValidationResult$Valid;

    move-object v0, p1

    check-cast v0, Lcom/squareup/invoices/editv2/validation/ValidationResult;

    :goto_0
    return-object v0
.end method

.method private final validateCustomer(Lcom/squareup/protos/client/invoice/InvoiceContact;)Lcom/squareup/invoices/editv2/validation/ValidationResult;
    .locals 2

    if-nez p1, :cond_0

    .line 34
    new-instance p1, Lcom/squareup/invoices/editv2/validation/ValidationResult$InvalidWithMessage$InvalidCustomer;

    .line 35
    iget-object v0, p0, Lcom/squareup/invoices/editv2/validation/InvoiceValidator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_validation_error_no_customer:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 34
    invoke-direct {p1, v0}, Lcom/squareup/invoices/editv2/validation/ValidationResult$InvalidWithMessage$InvalidCustomer;-><init>(Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/invoices/editv2/validation/ValidationResult;

    goto :goto_0

    .line 37
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_name:Ljava/lang/String;

    if-nez v0, :cond_1

    new-instance p1, Lcom/squareup/invoices/editv2/validation/ValidationResult$InvalidWithMessage$InvalidCustomer;

    .line 38
    iget-object v0, p0, Lcom/squareup/invoices/editv2/validation/InvoiceValidator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_validation_error_no_customer_name:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 37
    invoke-direct {p1, v0}, Lcom/squareup/invoices/editv2/validation/ValidationResult$InvalidWithMessage$InvalidCustomer;-><init>(Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/invoices/editv2/validation/ValidationResult;

    goto :goto_0

    .line 40
    :cond_1
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_email:Ljava/lang/String;

    if-nez p1, :cond_2

    new-instance p1, Lcom/squareup/invoices/editv2/validation/ValidationResult$InvalidWithMessage$InvalidCustomer;

    .line 41
    iget-object v0, p0, Lcom/squareup/invoices/editv2/validation/InvoiceValidator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_validation_error_no_email:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 40
    invoke-direct {p1, v0}, Lcom/squareup/invoices/editv2/validation/ValidationResult$InvalidWithMessage$InvalidCustomer;-><init>(Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/invoices/editv2/validation/ValidationResult;

    goto :goto_0

    .line 43
    :cond_2
    sget-object p1, Lcom/squareup/invoices/editv2/validation/ValidationResult$Valid;->INSTANCE:Lcom/squareup/invoices/editv2/validation/ValidationResult$Valid;

    check-cast p1, Lcom/squareup/invoices/editv2/validation/ValidationResult;

    :goto_0
    return-object p1
.end method


# virtual methods
.method public final validate(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/invoices/editv2/validation/ValidationResult;
    .locals 2

    const-string v0, "invoice"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    invoke-direct {p0, v0}, Lcom/squareup/invoices/editv2/validation/InvoiceValidator;->validateCustomer(Lcom/squareup/protos/client/invoice/InvoiceContact;)Lcom/squareup/invoices/editv2/validation/ValidationResult;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    const-string v1, "invoice.cart.amounts.total_money"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/validation/InvoiceValidator;->validateAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/invoices/editv2/validation/ValidationResult;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/invoices/editv2/validation/InvoiceValidator;->and(Lcom/squareup/invoices/editv2/validation/ValidationResult;Lcom/squareup/invoices/editv2/validation/ValidationResult;)Lcom/squareup/invoices/editv2/validation/ValidationResult;

    move-result-object p1

    return-object p1
.end method
