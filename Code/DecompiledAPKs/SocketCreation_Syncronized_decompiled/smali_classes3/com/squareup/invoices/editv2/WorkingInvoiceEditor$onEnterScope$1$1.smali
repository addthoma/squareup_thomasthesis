.class final Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$onEnterScope$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "WorkingInvoiceEditor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$onEnterScope$1;->accept(Lcom/squareup/payment/Order;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/client/invoice/Invoice$Builder;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/protos/client/invoice/Invoice$Builder;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $order:Lcom/squareup/payment/Order;


# direct methods
.method constructor <init>(Lcom/squareup/payment/Order;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$onEnterScope$1$1;->$order:Lcom/squareup/payment/Order;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$onEnterScope$1$1;->invoke(Lcom/squareup/protos/client/invoice/Invoice$Builder;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/protos/client/invoice/Invoice$Builder;)V
    .locals 4

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$onEnterScope$1$1;->$order:Lcom/squareup/payment/Order;

    const-string v1, "order"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/squareup/payment/Order;->getCartProtoForInvoice(Lcom/squareup/protos/common/Money;Z)Lcom/squareup/protos/client/bills/Cart;

    move-result-object v0

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 67
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$onEnterScope$1$1;->$order:Lcom/squareup/payment/Order;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/squareup/invoices/Invoices;->toInvoiceContact(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/protos/client/invoice/InvoiceContact;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-object v0, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    return-void
.end method
