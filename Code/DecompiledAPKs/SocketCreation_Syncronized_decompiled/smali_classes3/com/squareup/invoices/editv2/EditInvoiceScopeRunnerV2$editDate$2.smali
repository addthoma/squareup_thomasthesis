.class final Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editDate$2;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunnerV2.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editDate(Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "+",
        "Ljava/lang/Boolean;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u000126\u0010\u0002\u001a2\u0012\u0004\u0012\u00020\u0004\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005 \u0006*\u0018\u0012\u0004\u0012\u00020\u0004\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $dateType:Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType;

.field final synthetic this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editDate$2;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    iput-object p2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editDate$2;->$dateType:Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 236
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editDate$2;->accept(Lkotlin/Pair;)V

    return-void
.end method

.method public final accept(Lkotlin/Pair;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/Invoice;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    .line 1415
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editDate$2;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getChooseDateInfoFactory$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;

    move-result-object v1

    .line 1416
    iget-object v2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editDate$2;->$dateType:Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType;

    .line 1417
    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/Invoice;->newBuilder()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    const-string/jumbo v3, "workingInvoice.newBuilder()"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1418
    iget-object v3, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editDate$2;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v3}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getEditInvoiceContext$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v3

    const-string v4, "isRecurring"

    .line 1419
    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    .line 1415
    invoke-virtual {v1, v2, v0, v3, p1}, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->createForInvoice(Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType;Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/invoices/DisplayDetails;Z)Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;

    move-result-object p1

    .line 1422
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editDate$2;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getEditInvoiceWorkflowRunner$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editDate$2;->$dateType:Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType;

    check-cast v1, Lcom/squareup/invoices/workflow/edit/ChooseDateType;

    invoke-interface {v0, p1, v1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;->startDateChooser(Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;Lcom/squareup/invoices/workflow/edit/ChooseDateType;)V

    return-void
.end method
