.class final Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$cancelConfigureItemCard$1;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunnerV2.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->cancelConfigureItemCard(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/payment/Order;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "order",
        "Lcom/squareup/payment/Order;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$cancelConfigureItemCard$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/payment/Order;)V
    .locals 2

    .line 486
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->peekUninterestingItem()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 487
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$cancelConfigureItemCard$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getFlow$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lflow/Flow;

    move-result-object p1

    new-instance v0, Lcom/squareup/invoices/edit/items/ItemSelectScreen;

    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$cancelConfigureItemCard$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getEditInvoiceScopeV2$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v0, v1}, Lcom/squareup/invoices/edit/items/ItemSelectScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 488
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$cancelConfigureItemCard$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getWorkingOrderEditor$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/order/WorkingOrderEditor;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/invoices/order/WorkingOrderEditor;->popUninterestingItem()V

    goto :goto_0

    .line 490
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$cancelConfigureItemCard$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goBackFromItemSelect()V

    :goto_0
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 236
    check-cast p1, Lcom/squareup/payment/Order;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$cancelConfigureItemCard$1;->accept(Lcom/squareup/payment/Order;)V

    return-void
.end method
