.class public final Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$Factory;
.super Ljava/lang/Object;
.source "EditInvoiceOverflowDialogData.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0008\u001a\u00020\tR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$Factory;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/util/Res;)V",
        "create",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData;",
        "editInvoiceContext",
        "Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$Factory;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public final create(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Lio/reactivex/Observable;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData;",
            ">;"
        }
    .end annotation

    const-string v0, "editInvoiceContext"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isDraft()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 29
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isEditingSeries()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 30
    sget p1, Lcom/squareup/features/invoices/R$string;->invoice_edit_delete_draft_recurring_confirm:I

    goto :goto_0

    .line 32
    :cond_0
    sget p1, Lcom/squareup/features/invoices/R$string;->invoice_edit_delete_draft_confirm:I

    :goto_0
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/features/invoices/widgets/SectionElement;

    const/4 v1, 0x0

    .line 36
    new-instance v8, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    .line 37
    iget-object v2, p0, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$Factory;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/features/invoices/R$string;->invoice_edit_save_and_close:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 38
    sget-object v4, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$EventKey;->SAVE_AS_DRAFT:Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$EventKey;

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v2, v8

    .line 36
    invoke-direct/range {v2 .. v7}, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v8, Lcom/squareup/features/invoices/widgets/SectionElement;

    aput-object v8, v0, v1

    const/4 v1, 0x1

    .line 40
    new-instance v2, Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;

    .line 41
    iget-object v3, p0, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$Factory;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/features/invoices/R$string;->invoice_edit_delete_draft:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 42
    iget-object v4, p0, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$Factory;->res:Lcom/squareup/util/Res;

    invoke-interface {v4, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 43
    sget-object v4, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$EventKey;->DELETE_DRAFT:Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$EventKey;

    .line 40
    invoke-direct {v2, v3, p1, v4}, Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    check-cast v2, Lcom/squareup/features/invoices/widgets/SectionElement;

    aput-object v2, v0, v1

    .line 35
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    goto :goto_1

    .line 47
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isNew()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 48
    new-instance p1, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$Factory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_edit_save_as_draft:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$EventKey;->SAVE_AS_DRAFT:Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$EventKey;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 47
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    goto :goto_1

    .line 50
    :cond_2
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    .line 52
    :goto_1
    new-instance v0, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData;-><init>(Ljava/util/List;)V

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "Observable.just(EditInvo\u2026owDialogData(screenData))"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
