.class public final Lcom/squareup/invoices/PaymentRequestDisplayState$Factory;
.super Ljava/lang/Object;
.source "PaymentRequestDisplayState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/PaymentRequestDisplayState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/invoices/PaymentRequestDisplayState$Factory;",
        "",
        "clock",
        "Lcom/squareup/util/Clock;",
        "(Lcom/squareup/util/Clock;)V",
        "fromPaymentRequestReadOnlyInfo",
        "Lcom/squareup/invoices/PaymentRequestDisplayState;",
        "paymentRequestReadOnlyInfo",
        "Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;",
        "invoices_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final clock:Lcom/squareup/util/Clock;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Clock;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "clock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/PaymentRequestDisplayState$Factory;->clock:Lcom/squareup/util/Clock;

    return-void
.end method


# virtual methods
.method public final fromPaymentRequestReadOnlyInfo(Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;)Lcom/squareup/invoices/PaymentRequestDisplayState;
    .locals 3

    const-string v0, "paymentRequestReadOnlyInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-virtual {p1}, Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;->getCompletedAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 64
    invoke-virtual {p1}, Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;->getRequestedAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 65
    invoke-virtual {p1}, Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;->getDueDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    .line 68
    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->greaterThanOrEqualTo(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object p1, Lcom/squareup/invoices/PaymentRequestDisplayState$Paid;->INSTANCE:Lcom/squareup/invoices/PaymentRequestDisplayState$Paid;

    check-cast p1, Lcom/squareup/invoices/PaymentRequestDisplayState;

    goto :goto_0

    .line 69
    :cond_0
    iget-object v2, p0, Lcom/squareup/invoices/PaymentRequestDisplayState$Factory;->clock:Lcom/squareup/util/Clock;

    invoke-static {p1, v2}, Lcom/squareup/invoices/InvoiceDateUtility;->beforeToday(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/util/Clock;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 70
    invoke-static {v1, v0}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 71
    new-instance v1, Lcom/squareup/invoices/PaymentRequestDisplayState$Overdue;

    const-string v2, "overdueAmount"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v0, p1}, Lcom/squareup/invoices/PaymentRequestDisplayState$Overdue;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    move-object p1, v1

    check-cast p1, Lcom/squareup/invoices/PaymentRequestDisplayState;

    goto :goto_0

    .line 73
    :cond_1
    invoke-static {v0}, Lcom/squareup/money/MoneyMath;->isZero(Lcom/squareup/protos/common/Money;)Z

    move-result p1

    if-eqz p1, :cond_2

    new-instance p1, Lcom/squareup/invoices/PaymentRequestDisplayState$Unpaid;

    invoke-direct {p1, v1}, Lcom/squareup/invoices/PaymentRequestDisplayState$Unpaid;-><init>(Lcom/squareup/protos/common/Money;)V

    check-cast p1, Lcom/squareup/invoices/PaymentRequestDisplayState;

    goto :goto_0

    .line 74
    :cond_2
    invoke-static {v1, v0}, Lcom/squareup/money/MoneyMath;->greaterThan(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p1

    if-eqz p1, :cond_3

    new-instance p1, Lcom/squareup/invoices/PaymentRequestDisplayState$PartiallyPaid;

    .line 76
    invoke-static {v1, v0}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    const-string v2, "MoneyMath.subtract(reque\u2026dAmount, completedAmount)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-direct {p1, v0, v1}, Lcom/squareup/invoices/PaymentRequestDisplayState$PartiallyPaid;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    check-cast p1, Lcom/squareup/invoices/PaymentRequestDisplayState;

    goto :goto_0

    .line 78
    :cond_3
    sget-object p1, Lcom/squareup/invoices/PaymentRequestDisplayState$Unknown;->INSTANCE:Lcom/squareup/invoices/PaymentRequestDisplayState$Unknown;

    check-cast p1, Lcom/squareup/invoices/PaymentRequestDisplayState;

    :goto_0
    return-object p1
.end method
