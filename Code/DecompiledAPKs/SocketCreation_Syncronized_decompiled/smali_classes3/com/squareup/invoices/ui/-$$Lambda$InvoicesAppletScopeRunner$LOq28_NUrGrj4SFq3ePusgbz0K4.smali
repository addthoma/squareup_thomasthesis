.class public final synthetic Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$LOq28_NUrGrj4SFq3ePusgbz0K4;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/BiConsumer;


# instance fields
.field private final synthetic f$0:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

.field private final synthetic f$1:Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResult$Saved;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResult$Saved;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$LOq28_NUrGrj4SFq3ePusgbz0K4;->f$0:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    iput-object p2, p0, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$LOq28_NUrGrj4SFq3ePusgbz0K4;->f$1:Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResult$Saved;

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$LOq28_NUrGrj4SFq3ePusgbz0K4;->f$0:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    iget-object v1, p0, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$LOq28_NUrGrj4SFq3ePusgbz0K4;->f$1:Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResult$Saved;

    check-cast p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    check-cast p2, Lcom/squareup/protos/client/rolodex/Contact;

    invoke-virtual {v0, v1, p1, p2}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->lambda$handleResult$35$InvoicesAppletScopeRunner(Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResult$Saved;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/client/rolodex/Contact;)V

    return-void
.end method
