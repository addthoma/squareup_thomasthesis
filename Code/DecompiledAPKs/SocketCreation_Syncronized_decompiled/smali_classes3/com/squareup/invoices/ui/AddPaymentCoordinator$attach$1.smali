.class final Lcom/squareup/invoices/ui/AddPaymentCoordinator$attach$1;
.super Ljava/lang/Object;
.source "AddPaymentCoordinator.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/ui/AddPaymentCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/invoices/ui/AddPaymentCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/ui/AddPaymentCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/ui/AddPaymentCoordinator$attach$1;->this$0:Lcom/squareup/invoices/ui/AddPaymentCoordinator;

    iput-object p2, p0, Lcom/squareup/invoices/ui/AddPaymentCoordinator$attach$1;->$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData;)V
    .locals 5

    .line 35
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData;->getSectionDataList()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;

    .line 36
    iget-object v1, p0, Lcom/squareup/invoices/ui/AddPaymentCoordinator$attach$1;->this$0:Lcom/squareup/invoices/ui/AddPaymentCoordinator;

    invoke-static {v1}, Lcom/squareup/invoices/ui/AddPaymentCoordinator;->access$getPaymentTypeContainer$p(Lcom/squareup/invoices/ui/AddPaymentCoordinator;)Landroid/widget/LinearLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/ui/AddPaymentCoordinator$attach$1;->this$0:Lcom/squareup/invoices/ui/AddPaymentCoordinator;

    iget-object v3, p0, Lcom/squareup/invoices/ui/AddPaymentCoordinator$attach$1;->$view:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string/jumbo v4, "view.context"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2, v0, v3}, Lcom/squareup/invoices/ui/AddPaymentCoordinator;->access$createSectionView(Lcom/squareup/invoices/ui/AddPaymentCoordinator;Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;Landroid/content/Context;)Lcom/squareup/invoices/ui/InvoicePaymentTypeView;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/AddPaymentCoordinator$attach$1;->call(Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData;)V

    return-void
.end method
