.class Lcom/squareup/invoices/ui/InvoiceFilterMasterView$SectionsAdapter;
.super Landroid/widget/BaseAdapter;
.source "InvoiceFilterMasterView.java"

# interfaces
.implements Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/InvoiceFilterMasterView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SectionsAdapter"
.end annotation


# static fields
.field private static final INVOICE_ROW_TYPE:I = 0x0

.field private static final SERIES_ROW_TYPE:I = 0x1


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/ui/InvoiceFilterMasterView;


# direct methods
.method private constructor <init>(Lcom/squareup/invoices/ui/InvoiceFilterMasterView;)V
    .locals 0

    .line 77
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView$SectionsAdapter;->this$0:Lcom/squareup/invoices/ui/InvoiceFilterMasterView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/invoices/ui/InvoiceFilterMasterView;Lcom/squareup/invoices/ui/InvoiceFilterMasterView$1;)V
    .locals 0

    .line 77
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoiceFilterMasterView$SectionsAdapter;-><init>(Lcom/squareup/invoices/ui/InvoiceFilterMasterView;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView$SectionsAdapter;->this$0:Lcom/squareup/invoices/ui/InvoiceFilterMasterView;

    iget-object v0, v0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->presenter:Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;->visibleFilters:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getHeaderId(I)J
    .locals 3

    .line 127
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoiceFilterMasterView$SectionsAdapter;->getItemViewType(I)I

    move-result p1

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 132
    sget p1, Lcom/squareup/features/invoices/R$string;->titlecase_recurring_series:I

    :goto_0
    int-to-long v0, p1

    return-wide v0

    .line 135
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected row type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 130
    :cond_1
    sget p1, Lcom/squareup/features/invoices/R$string;->titlecase_invoices:I

    goto :goto_0
.end method

.method public getHeaderView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView$SectionsAdapter;->this$0:Lcom/squareup/invoices/ui/InvoiceFilterMasterView;

    iget-object v0, v0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->presenter:Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;->isReccuringEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 104
    new-instance p1, Landroid/view/View;

    iget-object p2, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView$SectionsAdapter;->this$0:Lcom/squareup/invoices/ui/InvoiceFilterMasterView;

    invoke-virtual {p2}, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    return-object p1

    .line 106
    :cond_0
    instance-of v0, p2, Landroid/widget/TextView;

    if-nez v0, :cond_1

    sget p2, Lcom/squareup/pos/container/R$layout;->applet_header_sidebar:I

    .line 107
    invoke-static {p2, p3}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_1
    check-cast p2, Landroid/widget/TextView;

    const/4 p3, 0x0

    .line 109
    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 110
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoiceFilterMasterView$SectionsAdapter;->getItemViewType(I)I

    move-result p1

    if-eqz p1, :cond_3

    const/4 p3, 0x1

    if-ne p1, p3, :cond_2

    .line 116
    sget p1, Lcom/squareup/features/invoices/R$string;->titlecase_recurring_series:I

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 120
    :cond_2
    new-instance p2, Ljava/lang/IllegalStateException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unexpected row type: "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 113
    :cond_3
    sget p1, Lcom/squareup/features/invoices/R$string;->titlecase_invoices:I

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    return-object p2
.end method

.method public getItem(I)Lcom/squareup/invoices/ui/GenericListFilter;
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView$SectionsAdapter;->this$0:Lcom/squareup/invoices/ui/InvoiceFilterMasterView;

    iget-object v0, v0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->presenter:Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;->visibleFilters:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/ui/GenericListFilter;

    return-object p1
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 0

    .line 77
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoiceFilterMasterView$SectionsAdapter;->getItem(I)Lcom/squareup/invoices/ui/GenericListFilter;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 0

    .line 94
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoiceFilterMasterView$SectionsAdapter;->getItem(I)Lcom/squareup/invoices/ui/GenericListFilter;

    move-result-object p1

    .line 95
    invoke-interface {p1}, Lcom/squareup/invoices/ui/GenericListFilter;->isRecurringListFilter()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    const/4 p1, 0x1

    return p1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .line 140
    move-object v0, p2

    check-cast v0, Lcom/squareup/ui/account/view/SmartLineRow;

    if-nez p2, :cond_0

    .line 142
    sget p2, Lcom/squareup/pos/container/R$layout;->applet_sidebar_row:I

    invoke-static {p2, p3}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    move-object v0, p2

    check-cast v0, Lcom/squareup/ui/account/view/SmartLineRow;

    .line 144
    :cond_0
    iget-object p2, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView$SectionsAdapter;->this$0:Lcom/squareup/invoices/ui/InvoiceFilterMasterView;

    iget-object p2, p2, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->presenter:Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;

    iget-object p2, p2, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;->visibleFilters:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/invoices/ui/GenericListFilter;

    .line 145
    iget-object p3, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView$SectionsAdapter;->this$0:Lcom/squareup/invoices/ui/InvoiceFilterMasterView;

    iget-object p3, p3, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->presenter:Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;

    invoke-virtual {p3, p1}, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;->isFilterSelected(I)Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setActivated(Z)V

    .line 147
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView$SectionsAdapter;->this$0:Lcom/squareup/invoices/ui/InvoiceFilterMasterView;

    .line 148
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-interface {p2}, Lcom/squareup/invoices/ui/GenericListFilter;->getLabel()I

    move-result p3

    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 147
    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 152
    invoke-interface {p2}, Lcom/squareup/invoices/ui/GenericListFilter;->getTitle()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setTag(Ljava/lang/Object;)V

    const/4 p1, 0x0

    .line 153
    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    return-object v0
.end method
