.class public Lcom/squareup/invoices/ui/InvoiceLoader;
.super Lcom/squareup/datafetch/Rx1AbstractLoader;
.source "InvoiceLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/ui/InvoiceLoader$Input;,
        Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/datafetch/Rx1AbstractLoader<",
        "Lcom/squareup/invoices/ui/InvoiceLoader$Input;",
        "Lcom/squareup/invoices/DisplayDetails;",
        ">;"
    }
.end annotation


# static fields
.field static final SEARCH_DELAY_MS:J = 0xc8L


# instance fields
.field private final invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

.field private final query:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/util/Optional<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final results:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$Results<",
            "Lcom/squareup/invoices/ui/InvoiceLoader$Input;",
            "Lcom/squareup/invoices/DisplayDetails;",
            ">;>;"
        }
    .end annotation
.end field

.field private final stateFilterParam:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/connectivity/ConnectivityMonitor;Lrx/Scheduler;Lcom/squareup/invoices/ClientInvoiceServiceHelper;)V
    .locals 0
    .param p2    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 139
    invoke-direct {p0, p1, p2}, Lcom/squareup/datafetch/Rx1AbstractLoader;-><init>(Lcom/squareup/connectivity/ConnectivityMonitor;Lrx/Scheduler;)V

    .line 53
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceLoader;->results:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 56
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceLoader;->query:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 57
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceLoader;->stateFilterParam:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 140
    iput-object p3, p0, Lcom/squareup/invoices/ui/InvoiceLoader;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    return-void
.end method

.method static synthetic lambda$fetch$2(Lcom/squareup/invoices/ui/InvoiceLoader$Input;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;Lcom/squareup/protos/client/invoice/ListRecurringSeriesResponse;)Lcom/squareup/datafetch/Rx1AbstractLoader$Response;
    .locals 3

    .line 182
    iget-object v0, p2, Lcom/squareup/protos/client/invoice/ListRecurringSeriesResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/squareup/protos/client/invoice/ListRecurringSeriesResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    sget-object v1, Lcom/squareup/protos/client/Status;->DEFAULT_SUCCESS:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/ListRecurringSeriesResponse;->recurring_invoice:Ljava/util/List;

    .line 183
    invoke-static {v1}, Lcom/squareup/invoices/ui/InvoiceLoader;->wrapSeriesList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iget-object p2, p2, Lcom/squareup/protos/client/invoice/ListRecurringSeriesResponse;->paging_key:Ljava/lang/String;

    invoke-direct {v0, p0, p1, v1, p2}, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;-><init>(Ljava/lang/Object;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;Ljava/util/List;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;

    new-instance p2, Lcom/squareup/datafetch/LoaderError$ThrowableError;

    new-instance v1, Ljava/lang/Throwable;

    const-string v2, "Request failed."

    invoke-direct {v1, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-direct {p2, v1}, Lcom/squareup/datafetch/LoaderError$ThrowableError;-><init>(Ljava/lang/Throwable;)V

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;-><init>(Ljava/lang/Object;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;Lcom/squareup/datafetch/LoaderError;)V

    :goto_0
    return-object v0
.end method

.method static synthetic lambda$fetch$3(Lcom/squareup/invoices/ui/InvoiceLoader$Input;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;Lcom/squareup/protos/client/invoice/ListInvoicesResponse;)Lcom/squareup/datafetch/Rx1AbstractLoader$Response;
    .locals 3

    .line 193
    iget-object v0, p2, Lcom/squareup/protos/client/invoice/ListInvoicesResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/squareup/protos/client/invoice/ListInvoicesResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    sget-object v1, Lcom/squareup/protos/client/Status;->DEFAULT_SUCCESS:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/ListInvoicesResponse;->invoice:Ljava/util/List;

    .line 194
    invoke-static {v1}, Lcom/squareup/invoices/ui/InvoiceLoader;->wrapInvoiceList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iget-object p2, p2, Lcom/squareup/protos/client/invoice/ListInvoicesResponse;->paging_key:Ljava/lang/String;

    invoke-direct {v0, p0, p1, v1, p2}, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;-><init>(Ljava/lang/Object;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;Ljava/util/List;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;

    new-instance p2, Lcom/squareup/datafetch/LoaderError$ThrowableError;

    new-instance v1, Ljava/lang/Throwable;

    const-string v2, "Request failed."

    invoke-direct {v1, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-direct {p2, v1}, Lcom/squareup/datafetch/LoaderError$ThrowableError;-><init>(Ljava/lang/Throwable;)V

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;-><init>(Ljava/lang/Object;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;Lcom/squareup/datafetch/LoaderError;)V

    :goto_0
    return-object v0
.end method

.method static synthetic lambda$input$1(Lcom/squareup/util/Optional;Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;)Lcom/squareup/invoices/ui/InvoiceLoader$Input;
    .locals 1

    .line 167
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceLoader$Input;

    invoke-virtual {p0}, Lcom/squareup/util/Optional;->getValueOrNull()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/ui/InvoiceLoader$Input;-><init>(Ljava/lang/String;Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;)V

    return-object v0
.end method

.method public static wrapInvoiceList(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/DisplayDetails;",
            ">;"
        }
    .end annotation

    .line 233
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 234
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    .line 235
    new-instance v2, Lcom/squareup/invoices/DisplayDetails$Invoice;

    invoke-direct {v2, v1}, Lcom/squareup/invoices/DisplayDetails$Invoice;-><init>(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static wrapSeriesList(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/DisplayDetails;",
            ">;"
        }
    .end annotation

    .line 243
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 244
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    .line 245
    new-instance v2, Lcom/squareup/invoices/DisplayDetails$Recurring;

    invoke-direct {v2, v1}, Lcom/squareup/invoices/DisplayDetails$Recurring;-><init>(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method


# virtual methods
.method protected fetch(Lcom/squareup/invoices/ui/InvoiceLoader$Input;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;Lrx/functions/Action0;)Lrx/Observable;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/ui/InvoiceLoader$Input;",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;",
            "Lrx/functions/Action0;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$Response<",
            "Lcom/squareup/invoices/ui/InvoiceLoader$Input;",
            "Lcom/squareup/invoices/DisplayDetails;",
            ">;>;"
        }
    .end annotation

    .line 173
    iget-object v0, p2, Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;->pagingKey:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceLoader;->results:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 178
    :cond_0
    iget-object v0, p1, Lcom/squareup/invoices/ui/InvoiceLoader$Input;->stateFilterInput:Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;->isRecurring()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 179
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceLoader;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    iget-object v1, p1, Lcom/squareup/invoices/ui/InvoiceLoader$Input;->query:Ljava/lang/String;

    iget-object v2, p2, Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;->pagingKey:Ljava/lang/String;

    iget-object v3, p2, Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;->pageSize:Ljava/lang/Integer;

    .line 180
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v4, p1, Lcom/squareup/invoices/ui/InvoiceLoader$Input;->stateFilterInput:Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;

    iget-object v4, v4, Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;->seriesStateFilter:Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;

    .line 179
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->listRecurringSeries(Ljava/lang/String;Ljava/lang/String;ILcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;)Lrx/Observable;

    move-result-object v0

    .line 181
    invoke-virtual {v0, p3}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p3

    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceLoader$J-Q3B-2ahBoW69UoSHr841fABos;

    invoke-direct {v0, p1, p2}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceLoader$J-Q3B-2ahBoW69UoSHr841fABos;-><init>(Lcom/squareup/invoices/ui/InvoiceLoader$Input;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;)V

    .line 182
    invoke-virtual {p3, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    return-object p1

    .line 188
    :cond_1
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceLoader;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    iget-object v1, p1, Lcom/squareup/invoices/ui/InvoiceLoader$Input;->query:Ljava/lang/String;

    iget-object v2, p2, Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;->pagingKey:Ljava/lang/String;

    iget-object v3, p2, Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;->pageSize:Ljava/lang/Integer;

    .line 189
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v4, p1, Lcom/squareup/invoices/ui/InvoiceLoader$Input;->stateFilterInput:Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;

    iget-object v4, v4, Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;->invoiceStateFilter:Lcom/squareup/protos/client/invoice/StateFilter;

    const/4 v5, 0x0

    iget-object v6, p1, Lcom/squareup/invoices/ui/InvoiceLoader$Input;->stateFilterInput:Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;

    iget-object v6, v6, Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;->parentSeriesToken:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->list(Ljava/lang/String;Ljava/lang/String;ILcom/squareup/protos/client/invoice/StateFilter;Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;

    move-result-object v0

    .line 192
    invoke-virtual {v0, p3}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p3

    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceLoader$yHdQ6Fcgei4K94UyEkixJNLp1xc;

    invoke-direct {v0, p1, p2}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceLoader$yHdQ6Fcgei4K94UyEkixJNLp1xc;-><init>(Lcom/squareup/invoices/ui/InvoiceLoader$Input;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;)V

    .line 193
    invoke-virtual {p3, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fetch(Ljava/lang/Object;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;Lrx/functions/Action0;)Lrx/Observable;
    .locals 0

    .line 45
    check-cast p1, Lcom/squareup/invoices/ui/InvoiceLoader$Input;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/invoices/ui/InvoiceLoader;->fetch(Lcom/squareup/invoices/ui/InvoiceLoader$Input;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getQuery()Ljava/lang/String;
    .locals 1

    .line 202
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceLoader;->query:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Optional;

    invoke-virtual {v0}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getStateFilter()Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;
    .locals 1

    .line 207
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceLoader;->stateFilterParam:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;

    return-object v0
.end method

.method protected input()Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/invoices/ui/InvoiceLoader$Input;",
            ">;"
        }
    .end annotation

    .line 155
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceLoader;->query:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 156
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceLoader$I-_li3GbbPj6rtcdS240QXiEf2U;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceLoader$I-_li3GbbPj6rtcdS240QXiEf2U;-><init>(Lcom/squareup/invoices/ui/InvoiceLoader;)V

    .line 157
    invoke-virtual {v0, v1}, Lrx/Observable;->debounce(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceLoader;->stateFilterParam:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 165
    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceLoader$Xf5AxCKWmJ3sNwLRYAnomKOsrME;->INSTANCE:Lcom/squareup/invoices/ui/-$$Lambda$InvoiceLoader$Xf5AxCKWmJ3sNwLRYAnomKOsrME;

    .line 155
    invoke-static {v0, v1, v2}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$input$0$InvoiceLoader(Lcom/squareup/util/Optional;)Lrx/Observable;
    .locals 4

    .line 158
    invoke-virtual {p1}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 159
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    const-wide/16 v0, 0xc8

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lcom/squareup/invoices/ui/InvoiceLoader;->mainScheduler:Lrx/Scheduler;

    .line 160
    invoke-virtual {p1, v0, v1, v2, v3}, Lrx/Observable;->delay(JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    return-object p1

    .line 162
    :cond_0
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 144
    invoke-super {p0, p1}, Lcom/squareup/datafetch/Rx1AbstractLoader;->onEnterScope(Lmortar/MortarScope;)V

    .line 147
    invoke-super {p0}, Lcom/squareup/datafetch/Rx1AbstractLoader;->results()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceLoader;->results:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method public results()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$Results<",
            "Lcom/squareup/invoices/ui/InvoiceLoader$Input;",
            "Lcom/squareup/invoices/DisplayDetails;",
            ">;>;"
        }
    .end annotation

    .line 151
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceLoader;->results:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method public setInvoiceStateFilter(Lcom/squareup/protos/client/invoice/StateFilter;)V
    .locals 1

    .line 226
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 227
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceLoader;->stateFilterParam:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {p1}, Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;->createSingleListStateFilterInput(Lcom/squareup/protos/client/invoice/StateFilter;)Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public setParentRecurringListFilter(Lcom/squareup/invoices/ui/ParentRecurringSeriesListFilter;)V
    .locals 1

    .line 216
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 217
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceLoader;->stateFilterParam:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/ParentRecurringSeriesListFilter;->getToken()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;->createParentSeriesTokenInput(Ljava/lang/String;)Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public setQuery(Ljava/lang/String;)V
    .locals 1

    .line 211
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 212
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceLoader;->query:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {p1}, Lcom/squareup/util/Optional;->ofNullable(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public setSeriesStateFilter(Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;)V
    .locals 1

    .line 221
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 222
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceLoader;->stateFilterParam:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {p1}, Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;->createRecurringListStateFilterInput(Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;)Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
