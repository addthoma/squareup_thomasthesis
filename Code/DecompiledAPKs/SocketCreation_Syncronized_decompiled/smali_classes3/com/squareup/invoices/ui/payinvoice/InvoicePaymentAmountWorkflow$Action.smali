.class public abstract Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action;
.super Ljava/lang/Object;
.source "InvoicePaymentAmountWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action$Cancel;,
        Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action$OtherAmountSaved;,
        Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action$OtherAmountCanceled;,
        Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action$ErrorCanceled;,
        Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action$OptionSelected;,
        Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action$SaveAmount;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResult;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoicePaymentAmountWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoicePaymentAmountWorkflow.kt\ncom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,289:1\n310#2,7:290\n*E\n*S KotlinDebug\n*F\n+ 1 InvoicePaymentAmountWorkflow.kt\ncom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action\n*L\n87#1,7:290\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0006\u0007\u0008\t\n\u000b\u000cB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u0003*\u0008\u0012\u0004\u0012\u00020\u00020\u0006H\u0016\u0082\u0001\u0006\r\u000e\u000f\u0010\u0011\u0012\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResult;",
        "()V",
        "apply",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "Cancel",
        "ErrorCanceled",
        "OptionSelected",
        "OtherAmountCanceled",
        "OtherAmountSaved",
        "SaveAmount",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action$Cancel;",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action$OtherAmountSaved;",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action$OtherAmountCanceled;",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action$ErrorCanceled;",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action$OptionSelected;",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action$SaveAmount;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 55
    invoke-direct {p0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResult;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;",
            ">;)",
            "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResult;"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    instance-of v0, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action$OptionSelected;

    const-string v1, "null cannot be cast to non-null type com.squareup.invoices.ui.payinvoice.InvoicePaymentAmountState.PaymentAmountOptions"

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    .line 71
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$PaymentAmountOptions;

    .line 72
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$PaymentAmountOptions;->getAmountTypes()Ljava/util/List;

    move-result-object v1

    move-object v3, p0

    check-cast v3, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action$OptionSelected;

    invoke-virtual {v3}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action$OptionSelected;->getIndex()I

    move-result v4

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType;

    .line 75
    instance-of v1, v1, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType$OtherAmount;

    if-eqz v1, :cond_0

    .line 76
    new-instance v1, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;

    .line 77
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$PaymentAmountOptions;->getAmountTypes()Ljava/util/List;

    move-result-object v3

    .line 78
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$PaymentAmountOptions;->getSelectedIndex()I

    move-result v0

    .line 76
    invoke-direct {v1, v3, v0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;-><init>(Ljava/util/List;I)V

    check-cast v1, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;

    goto :goto_0

    .line 81
    :cond_0
    invoke-virtual {v3}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action$OptionSelected;->getIndex()I

    move-result v1

    const/4 v3, 0x1

    invoke-static {v0, v2, v1, v3, v2}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$PaymentAmountOptions;->copy$default(Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$PaymentAmountOptions;Ljava/util/List;IILjava/lang/Object;)Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$PaymentAmountOptions;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;

    .line 74
    :goto_0
    invoke-virtual {p1, v1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_4

    .line 71
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 85
    :cond_2
    instance-of v0, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action$OtherAmountSaved;

    const-string v3, "null cannot be cast to non-null type com.squareup.invoices.ui.payinvoice.InvoicePaymentAmountState.OtherAmountDialog"

    if-eqz v0, :cond_6

    .line 86
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    check-cast v0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;

    .line 87
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;->getAmountTypes()Ljava/util/List;

    move-result-object v1

    const/4 v3, 0x0

    .line 291
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 292
    check-cast v4, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType;

    .line 87
    instance-of v4, v4, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType$OtherAmount;

    if-eqz v4, :cond_3

    goto :goto_2

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    const/4 v3, -0x1

    .line 89
    :goto_2
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;->getAmountTypes()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    .line 91
    new-instance v1, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType$OtherAmount;

    move-object v4, p0

    check-cast v4, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action$OtherAmountSaved;

    invoke-virtual {v4}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action$OtherAmountSaved;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v4

    invoke-direct {v1, v4}, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType$OtherAmount;-><init>(Lcom/squareup/protos/common/Money;)V

    invoke-interface {v0, v3, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 94
    new-instance v1, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$PaymentAmountOptions;

    invoke-direct {v1, v0, v3}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$PaymentAmountOptions;-><init>(Ljava/util/List;I)V

    invoke-virtual {p1, v1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_4

    .line 86
    :cond_5
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v3}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 97
    :cond_6
    sget-object v0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action$OtherAmountCanceled;->INSTANCE:Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action$OtherAmountCanceled;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 98
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_8

    check-cast v0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;

    .line 99
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;->getAmountTypes()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowKt;->access$getOtherAmountAndIndex(Ljava/util/List;)Lkotlin/Pair;

    move-result-object v1

    invoke-virtual {v1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType$OtherAmount;

    invoke-virtual {v1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    .line 103
    invoke-virtual {v3}, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType$OtherAmount;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v3

    if-nez v3, :cond_7

    .line 104
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$OtherAmountDialog;->getPreviousIndex()I

    move-result v1

    .line 109
    :cond_7
    new-instance v0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$PaymentAmountOptions;

    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;

    invoke-virtual {v3}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;->getAmountTypes()Ljava/util/List;

    move-result-object v3

    invoke-direct {v0, v3, v1}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$PaymentAmountOptions;-><init>(Ljava/util/List;I)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_4

    .line 98
    :cond_8
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v3}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 112
    :cond_9
    sget-object v0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action$ErrorCanceled;->INSTANCE:Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action$ErrorCanceled;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 113
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_a

    check-cast v0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$ErrorDialog;

    .line 114
    new-instance v1, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$PaymentAmountOptions;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$ErrorDialog;->getAmountTypes()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$ErrorDialog;->getSelectedIndex()I

    move-result v0

    invoke-direct {v1, v3, v0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$PaymentAmountOptions;-><init>(Ljava/util/List;I)V

    invoke-virtual {p1, v1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_4

    .line 113
    :cond_a
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.invoices.ui.payinvoice.InvoicePaymentAmountState.ErrorDialog"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 117
    :cond_b
    sget-object v0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action$SaveAmount;->INSTANCE:Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action$SaveAmount;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 118
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_f

    check-cast v0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$PaymentAmountOptions;

    .line 119
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$PaymentAmountOptions;->getAmountTypes()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$PaymentAmountOptions;->getSelectedIndex()I

    move-result v3

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType;

    .line 120
    invoke-virtual {v1}, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    if-eqz v1, :cond_e

    .line 123
    iget-object v3, v1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-nez v3, :cond_c

    goto :goto_3

    :cond_c
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-nez v7, :cond_d

    .line 124
    new-instance v1, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$ErrorDialog;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$PaymentAmountOptions;->getAmountTypes()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$PaymentAmountOptions;->getSelectedIndex()I

    move-result v0

    invoke-direct {v1, v3, v0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState$ErrorDialog;-><init>(Ljava/util/List;I)V

    invoke-virtual {p1, v1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_4

    .line 126
    :cond_d
    :goto_3
    new-instance p1, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResult$Saved;

    invoke-direct {p1, v1}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResult$Saved;-><init>(Lcom/squareup/protos/common/Money;)V

    check-cast p1, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResult;

    return-object p1

    .line 120
    :cond_e
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Cannot save amount with null value."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 118
    :cond_f
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 130
    :cond_10
    sget-object p1, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action$Cancel;->INSTANCE:Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action$Cancel;

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_11

    .line 131
    sget-object p1, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResult$Canceled;->INSTANCE:Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResult$Canceled;

    check-cast p1, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResult;

    return-object p1

    :cond_11
    :goto_4
    return-object v2
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 55
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResult;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountState;",
            "-",
            "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Updater;)V

    return-void
.end method
