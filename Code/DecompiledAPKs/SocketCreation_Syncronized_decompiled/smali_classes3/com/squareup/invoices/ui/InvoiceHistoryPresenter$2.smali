.class synthetic Lcom/squareup/invoices/ui/InvoiceHistoryPresenter$2;
.super Ljava/lang/Object;
.source "InvoiceHistoryPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$util$Times$RelativeDate:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 319
    invoke-static {}, Lcom/squareup/util/Times$RelativeDate;->values()[Lcom/squareup/util/Times$RelativeDate;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter$2;->$SwitchMap$com$squareup$util$Times$RelativeDate:[I

    :try_start_0
    sget-object v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter$2;->$SwitchMap$com$squareup$util$Times$RelativeDate:[I

    sget-object v1, Lcom/squareup/util/Times$RelativeDate;->TODAY:Lcom/squareup/util/Times$RelativeDate;

    invoke-virtual {v1}, Lcom/squareup/util/Times$RelativeDate;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter$2;->$SwitchMap$com$squareup$util$Times$RelativeDate:[I

    sget-object v1, Lcom/squareup/util/Times$RelativeDate;->YESTERDAY:Lcom/squareup/util/Times$RelativeDate;

    invoke-virtual {v1}, Lcom/squareup/util/Times$RelativeDate;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    return-void
.end method
