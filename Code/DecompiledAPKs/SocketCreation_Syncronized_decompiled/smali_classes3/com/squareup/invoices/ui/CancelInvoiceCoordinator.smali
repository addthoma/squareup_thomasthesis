.class public final Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "CancelInvoiceCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0010\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\u0010\u0010\u0016\u001a\u00020\u00122\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/invoices/ui/CancelInvoiceScreen$Runner;",
        "res",
        "Lcom/squareup/util/Res;",
        "glassSpinner",
        "Lcom/squareup/register/widgets/GlassSpinner;",
        "(Lcom/squareup/invoices/ui/CancelInvoiceScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/register/widgets/GlassSpinner;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "cancelInvoiceButton",
        "Lcom/squareup/marketfont/MarketButton;",
        "confirmationMessageView",
        "Lcom/squareup/widgets/MessageView;",
        "notifyRecipientToggle",
        "Lcom/squareup/widgets/list/ToggleButtonRow;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "configureActionBar",
        "title",
        "",
        "spinnerData",
        "Lcom/squareup/register/widgets/GlassSpinnerState;",
        "screenData",
        "Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private cancelInvoiceButton:Lcom/squareup/marketfont/MarketButton;

.field private confirmationMessageView:Lcom/squareup/widgets/MessageView;

.field private final glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

.field private notifyRecipientToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/invoices/ui/CancelInvoiceScreen$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/ui/CancelInvoiceScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/register/widgets/GlassSpinner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "glassSpinner"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;->runner:Lcom/squareup/invoices/ui/CancelInvoiceScreen$Runner;

    iput-object p2, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    return-void
.end method

.method public static final synthetic access$configureActionBar(Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;Ljava/lang/String;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;->configureActionBar(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$getCancelInvoiceButton$p(Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;)Lcom/squareup/marketfont/MarketButton;
    .locals 1

    .line 25
    iget-object p0, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;->cancelInvoiceButton:Lcom/squareup/marketfont/MarketButton;

    if-nez p0, :cond_0

    const-string v0, "cancelInvoiceButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getConfirmationMessageView$p(Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;)Lcom/squareup/widgets/MessageView;
    .locals 1

    .line 25
    iget-object p0, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;->confirmationMessageView:Lcom/squareup/widgets/MessageView;

    if-nez p0, :cond_0

    const-string v0, "confirmationMessageView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getNotifyRecipientToggle$p(Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;)Lcom/squareup/widgets/list/ToggleButtonRow;
    .locals 1

    .line 25
    iget-object p0, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;->notifyRecipientToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    if-nez p0, :cond_0

    const-string v0, "notifyRecipientToggle"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;)Lcom/squareup/invoices/ui/CancelInvoiceScreen$Runner;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;->runner:Lcom/squareup/invoices/ui/CancelInvoiceScreen$Runner;

    return-object p0
.end method

.method public static final synthetic access$setCancelInvoiceButton$p(Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;Lcom/squareup/marketfont/MarketButton;)V
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;->cancelInvoiceButton:Lcom/squareup/marketfont/MarketButton;

    return-void
.end method

.method public static final synthetic access$setConfirmationMessageView$p(Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;Lcom/squareup/widgets/MessageView;)V
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;->confirmationMessageView:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method public static final synthetic access$setNotifyRecipientToggle$p(Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;Lcom/squareup/widgets/list/ToggleButtonRow;)V
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;->notifyRecipientToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    return-void
.end method

.method public static final synthetic access$spinnerData(Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;->spinnerData(Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p0

    return-object p0
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 69
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 70
    sget v0, Lcom/squareup/features/invoices/R$id;->confirmation_text:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;->confirmationMessageView:Lcom/squareup/widgets/MessageView;

    .line 71
    sget v0, Lcom/squareup/features/invoices/R$id;->notify_recipients:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;->notifyRecipientToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 72
    sget v0, Lcom/squareup/features/invoices/R$id;->cancel_invoice_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketButton;

    iput-object p1, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;->cancelInvoiceButton:Lcom/squareup/marketfont/MarketButton;

    return-void
.end method

.method private final configureActionBar(Ljava/lang/String;)V
    .locals 4

    .line 76
    iget-object v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    const-string v1, "actionBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v2, "actionBar.presenter"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 77
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v3, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    const/4 v3, 0x1

    .line 78
    invoke-virtual {p1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 79
    new-instance v3, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator$configureActionBar$1;

    invoke-direct {v3, p0}, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator$configureActionBar$1;-><init>(Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {p1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 81
    iget-object p1, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez p1, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final spinnerData(Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 4

    .line 65
    sget-object v0, Lcom/squareup/register/widgets/GlassSpinnerState;->Factory:Lcom/squareup/register/widgets/GlassSpinnerState$Factory;

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->isBusy()Z

    move-result p1

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, p1, v1, v2, v3}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->showNonDebouncedSpinner$default(Lcom/squareup/register/widgets/GlassSpinnerState$Factory;ZIILjava/lang/Object;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 39
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;->bindViews(Landroid/view/View;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;->notifyRecipientToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    if-nez v0, :cond_0

    const-string v1, "notifyRecipientToggle"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 43
    iget-object v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/GlassSpinner;->showOrHideSpinner(Landroid/content/Context;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "glassSpinner.showOrHideSpinner(view.context)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-static {v0, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnDetach(Lrx/Subscription;Landroid/view/View;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;->runner:Lcom/squareup/invoices/ui/CancelInvoiceScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Runner;->cancelInvoiceScreenData()Lio/reactivex/Observable;

    move-result-object v0

    .line 47
    iget-object v1, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    new-instance v2, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator$attach$1;

    move-object v3, p0

    check-cast v3, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;

    invoke-direct {v2, v3}, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator$attach$1;-><init>(Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    new-instance v3, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator$sam$rx_functions_Func1$0;

    invoke-direct {v3, v2}, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator$sam$rx_functions_Func1$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v3, Lrx/functions/Func1;

    invoke-virtual {v1, v3}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransform(Lrx/functions/Func1;)Lrx/Observable$Transformer;

    move-result-object v1

    const-string v2, "glassSpinner.spinnerTransform(::spinnerData)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v1, v2}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Transformer(Lrx/Observable$Transformer;Lio/reactivex/BackpressureStrategy;)Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 48
    new-instance v1, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator$attach$2;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator$attach$2;-><init>(Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "runner.cancelInvoiceScre\u2026el)\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
