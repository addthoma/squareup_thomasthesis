.class public abstract Lcom/squareup/invoices/ui/InvoicesAppletScope$Module;
.super Ljava/lang/Object;
.source "InvoicesAppletScope.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/features/invoices/widgets/V1WidgetsModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/InvoicesAppletScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideAddPaymentScreenRunner(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)Lcom/squareup/invoices/ui/AddPaymentScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideBottomDialogRunner(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)Lcom/squareup/invoices/ui/InvoiceActionBottomDialog$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideCancelInvoiceRunner(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)Lcom/squareup/invoices/ui/CancelInvoiceScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideConfirmationRunner(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideFileAttachmentResultHandler(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentResultHandler;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract providePaymentAmountResultHandler(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResultHandler;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideTimelineRunner(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)Lcom/squareup/invoices/InvoiceTimelineScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideTransactionsHistoryRefundHelper(Lcom/squareup/invoices/RealInvoicesTransactionsHistoryRefundHelper;)Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
