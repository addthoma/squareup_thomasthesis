.class public final Lcom/squareup/invoices/ui/InvoiceHistoryScreen;
.super Lcom/squareup/invoices/ui/InInvoicesAppletScope;
.source "InvoiceHistoryScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/invoices/ui/InvoiceHistoryScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/ui/InvoiceHistoryScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/invoices/ui/InvoiceHistoryScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/invoices/ui/InvoiceHistoryScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceHistoryScreen;

    invoke-direct {v0}, Lcom/squareup/invoices/ui/InvoiceHistoryScreen;-><init>()V

    sput-object v0, Lcom/squareup/invoices/ui/InvoiceHistoryScreen;->INSTANCE:Lcom/squareup/invoices/ui/InvoiceHistoryScreen;

    .line 54
    sget-object v0, Lcom/squareup/invoices/ui/InvoiceHistoryScreen;->INSTANCE:Lcom/squareup/invoices/ui/InvoiceHistoryScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/invoices/ui/InvoiceHistoryScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InInvoicesAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 37
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->INVOICES_LIST:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 33
    sget-object p1, Lcom/squareup/container/spot/Spots;->HERE:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 29
    sget v0, Lcom/squareup/features/invoices/R$layout;->invoices_applet_invoice_history_view:I

    return v0
.end method
