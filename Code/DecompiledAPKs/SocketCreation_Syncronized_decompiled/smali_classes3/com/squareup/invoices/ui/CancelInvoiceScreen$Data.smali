.class public final Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;
.super Ljava/lang/Object;
.source "CancelInvoiceScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/CancelInvoiceScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Data"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0017\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0008\u0086\u0008\u0018\u00002\u00020\u0001BA\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\u0007\u0012\u0006\u0010\t\u001a\u00020\u0007\u0012\u0008\u0008\u0002\u0010\n\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0014\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0007H\u00c6\u0003JO\u0010\u001b\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00072\u0008\u0008\u0002\u0010\t\u001a\u00020\u00072\u0008\u0008\u0002\u0010\n\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u001c\u001a\u00020\u00072\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001e\u001a\u00020\u001fH\u00d6\u0001J\t\u0010 \u001a\u00020\u0003H\u00d6\u0001J\u000e\u0010!\u001a\u00020\u00002\u0006\u0010\"\u001a\u00020\u0007R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\t\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\rR\u0011\u0010\n\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000fR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u000fR\u0011\u0010\u0008\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\r\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;",
        "",
        "title",
        "",
        "buttonText",
        "helperText",
        "issueRefundAfterCancel",
        "",
        "showNotifyRecipients",
        "defaultNotifyRecipients",
        "isBusy",
        "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZ)V",
        "getButtonText",
        "()Ljava/lang/String;",
        "getDefaultNotifyRecipients",
        "()Z",
        "getHelperText",
        "getIssueRefundAfterCancel",
        "getShowNotifyRecipients",
        "getTitle",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "updateIsBusy",
        "busy",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final buttonText:Ljava/lang/String;

.field private final defaultNotifyRecipients:Z

.field private final helperText:Ljava/lang/String;

.field private final isBusy:Z

.field private final issueRefundAfterCancel:Z

.field private final showNotifyRecipients:Z

.field private final title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZ)V
    .locals 1

    const-string/jumbo v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buttonText"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "helperText"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->title:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->buttonText:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->helperText:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->issueRefundAfterCancel:Z

    iput-boolean p5, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->showNotifyRecipients:Z

    iput-boolean p6, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->defaultNotifyRecipients:Z

    iput-boolean p7, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->isBusy:Z

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 10

    and-int/lit8 v0, p8, 0x8

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 v6, 0x0

    goto :goto_0

    :cond_0
    move v6, p4

    :goto_0
    and-int/lit8 v0, p8, 0x40

    if-eqz v0, :cond_1

    const/4 v9, 0x0

    goto :goto_1

    :cond_1
    move/from16 v9, p7

    :goto_1
    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v7, p5

    move/from16 v8, p6

    .line 63
    invoke-direct/range {v2 .. v9}, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZ)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZILjava/lang/Object;)Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->title:Ljava/lang/String;

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-object p2, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->buttonText:Ljava/lang/String;

    :cond_1
    move-object p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->helperText:Ljava/lang/String;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->issueRefundAfterCancel:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->showNotifyRecipients:Z

    :cond_4
    move v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget-boolean p6, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->defaultNotifyRecipients:Z

    :cond_5
    move v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    iget-boolean p7, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->isBusy:Z

    :cond_6
    move v4, p7

    move-object p2, p0

    move-object p3, p1

    move-object p4, p9

    move-object p5, v0

    move p6, v1

    move p7, v2

    move p8, v3

    move p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->copy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZ)Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->title:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->buttonText:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->helperText:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->issueRefundAfterCancel:Z

    return v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->showNotifyRecipients:Z

    return v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->defaultNotifyRecipients:Z

    return v0
.end method

.method public final component7()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->isBusy:Z

    return v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZ)Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;
    .locals 9

    const-string/jumbo v0, "title"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buttonText"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "helperText"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;

    move-object v1, v0

    move v5, p4

    move v6, p5

    move v7, p6

    move/from16 v8, p7

    invoke-direct/range {v1 .. v8}, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;

    iget-object v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->title:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->title:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->buttonText:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->buttonText:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->helperText:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->helperText:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->issueRefundAfterCancel:Z

    iget-boolean v1, p1, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->issueRefundAfterCancel:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->showNotifyRecipients:Z

    iget-boolean v1, p1, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->showNotifyRecipients:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->defaultNotifyRecipients:Z

    iget-boolean v1, p1, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->defaultNotifyRecipients:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->isBusy:Z

    iget-boolean p1, p1, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->isBusy:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getButtonText()Ljava/lang/String;
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->buttonText:Ljava/lang/String;

    return-object v0
.end method

.method public final getDefaultNotifyRecipients()Z
    .locals 1

    .line 62
    iget-boolean v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->defaultNotifyRecipients:Z

    return v0
.end method

.method public final getHelperText()Ljava/lang/String;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->helperText:Ljava/lang/String;

    return-object v0
.end method

.method public final getIssueRefundAfterCancel()Z
    .locals 1

    .line 60
    iget-boolean v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->issueRefundAfterCancel:Z

    return v0
.end method

.method public final getShowNotifyRecipients()Z
    .locals 1

    .line 61
    iget-boolean v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->showNotifyRecipients:Z

    return v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->title:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->title:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->buttonText:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->helperText:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->issueRefundAfterCancel:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->showNotifyRecipients:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :cond_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->defaultNotifyRecipients:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :cond_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->isBusy:Z

    if-eqz v1, :cond_6

    const/4 v1, 0x1

    :cond_6
    add-int/2addr v0, v1

    return v0
.end method

.method public final isBusy()Z
    .locals 1

    .line 63
    iget-boolean v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->isBusy:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Data(title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", buttonText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->buttonText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", helperText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->helperText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", issueRefundAfterCancel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->issueRefundAfterCancel:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showNotifyRecipients="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->showNotifyRecipients:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", defaultNotifyRecipients="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->defaultNotifyRecipients:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isBusy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->isBusy:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final updateIsBusy(Z)Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;
    .locals 10

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0x3f

    const/4 v9, 0x0

    move-object v0, p0

    move v7, p1

    .line 65
    invoke-static/range {v0 .. v9}, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->copy$default(Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZILjava/lang/Object;)Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;

    move-result-object p1

    return-object p1
.end method
