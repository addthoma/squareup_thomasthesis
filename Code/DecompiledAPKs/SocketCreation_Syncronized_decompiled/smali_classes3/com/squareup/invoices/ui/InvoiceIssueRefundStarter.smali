.class public Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;
.super Ljava/lang/Object;
.source "InvoiceIssueRefundStarter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter$WhenAccessGranted;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0008\u0016\u0018\u00002\u00020\u0001:\u0001\rB\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0008\u0010\u000b\u001a\u00020\u000cH\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;",
        "",
        "flow",
        "Lflow/Flow;",
        "invoiceBillLoader",
        "Lcom/squareup/invoices/ui/InvoiceBillLoader;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "permissionGatekeeper",
        "Lcom/squareup/permissions/PermissionGatekeeper;",
        "(Lflow/Flow;Lcom/squareup/invoices/ui/InvoiceBillLoader;Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/PermissionGatekeeper;)V",
        "showIssueRefundScreen",
        "",
        "WhenAccessGranted",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private final invoiceBillLoader:Lcom/squareup/invoices/ui/InvoiceBillLoader;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;


# direct methods
.method public constructor <init>(Lflow/Flow;Lcom/squareup/invoices/ui/InvoiceBillLoader;Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/PermissionGatekeeper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceBillLoader"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "permissionGatekeeper"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;->flow:Lflow/Flow;

    iput-object p2, p0, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;->invoiceBillLoader:Lcom/squareup/invoices/ui/InvoiceBillLoader;

    iput-object p3, p0, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;->features:Lcom/squareup/settings/server/Features;

    iput-object p4, p0, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    return-void
.end method

.method public static final synthetic access$getFlow$p(Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;)Lflow/Flow;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;->flow:Lflow/Flow;

    return-object p0
.end method

.method public static final synthetic access$getInvoiceBillLoader$p(Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;)Lcom/squareup/invoices/ui/InvoiceBillLoader;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;->invoiceBillLoader:Lcom/squareup/invoices/ui/InvoiceBillLoader;

    return-object p0
.end method


# virtual methods
.method public showIssueRefundScreen()V
    .locals 3

    .line 24
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;->invoiceBillLoader:Lcom/squareup/invoices/ui/InvoiceBillLoader;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceBillLoader;->getBillHistory()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    const-string v1, "bill"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 26
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->ISSUE_REFUNDS:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter$WhenAccessGranted;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter$WhenAccessGranted;-><init>(Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;)V

    check-cast v2, Lcom/squareup/permissions/PermissionGatekeeper$When;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method
