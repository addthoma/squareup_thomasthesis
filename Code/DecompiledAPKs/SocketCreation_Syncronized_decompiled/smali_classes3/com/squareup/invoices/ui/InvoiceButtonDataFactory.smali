.class public final Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;
.super Ljava/lang/Object;
.source "InvoiceButtonDataFactory.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/ui/InvoiceButtonDataFactory$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceButtonDataFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceButtonDataFactory.kt\ncom/squareup/invoices/ui/InvoiceButtonDataFactory\n*L\n1#1,327:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000b\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0018\u0000 42\u00020\u0001:\u00014B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u001e\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020!0 H\u0002J\u001e\u0010\"\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020!0 H\u0002J\u0016\u0010#\u001a\u0008\u0012\u0004\u0012\u00020!0$2\u0006\u0010\u001d\u001a\u00020\u001eH\u0007J\u0016\u0010#\u001a\u0008\u0012\u0004\u0012\u00020!0$2\u0006\u0010%\u001a\u00020&H\u0007J\u0014\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020!0$2\u0006\u0010\u001d\u001a\u00020\u001eJ\u0014\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020!0$2\u0006\u0010%\u001a\u00020&J\u0014\u0010(\u001a\u0008\u0012\u0004\u0012\u00020!0$2\u0006\u0010\u001d\u001a\u00020\u001eJ\u0014\u0010(\u001a\u0008\u0012\u0004\u0012\u00020!0$2\u0006\u0010%\u001a\u00020&J\u001e\u0010)\u001a\u00020\u001c2\u0006\u0010\u0006\u001a\u00020\u00072\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020!0 H\u0002J&\u0010*\u001a\u00020\u001c2\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u001d\u001a\u00020\u001e2\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020!0 H\u0002J&\u0010+\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u0008\u001a\u00020\t2\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020!0 H\u0002J\u001e\u0010,\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020!0 H\u0002J\u001e\u0010-\u001a\u00020\u001c2\u0006\u0010\u0004\u001a\u00020\u00052\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020!0 H\u0002J\u001e\u0010.\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020!0 H\u0002J\u0018\u0010/\u001a\u0008\u0012\u0004\u0012\u00020!0$*\u0008\u0012\u0004\u0012\u00020!0$H\u0002J\u0018\u00100\u001a\u0008\u0012\u0004\u0012\u00020!0$*\u0008\u0012\u0004\u0012\u00020!0$H\u0002J\"\u00101\u001a\u00020\u001c*\u0008\u0012\u0004\u0012\u00020!0 2\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u00102\u001a\u000203H\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00065"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "invoiceUrlHelper",
        "Lcom/squareup/invoices/InvoiceUrlHelper;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "(Lcom/squareup/util/Res;Lcom/squareup/invoices/InvoiceUrlHelper;Lcom/squareup/settings/server/Features;Lcom/squareup/protos/common/CurrencyCode;)V",
        "archiveInvoice",
        "Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;",
        "cancel",
        "deleteDraft",
        "Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;",
        "deleteSeriesDraft",
        "downloadInvoice",
        "duplicate",
        "endSeries",
        "issueRefund",
        "more",
        "sendReminder",
        "share",
        "unarchiveInvoice",
        "viewInvoicesInSeries",
        "viewTransaction",
        "addCancelButtonData",
        "",
        "invoiceDisplayDetails",
        "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
        "buttonsList",
        "",
        "Lcom/squareup/features/invoices/widgets/SectionElement;",
        "addDeleteDraftButtonData",
        "createButtonDataList",
        "",
        "recurringSeriesDisplayDetails",
        "Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;",
        "createForBottomDialog",
        "createForDetailScreen",
        "maybeAddDownloadPdf",
        "maybeAddDuplicateButtonData",
        "maybeAddIssueRefundButtonData",
        "maybeAddSendReminderButtonData",
        "maybeAddShareButtonData",
        "maybeAddViewTransactionButtonData",
        "forBottomDialog",
        "forDetailScreen",
        "maybeAddArchiveOrUnarchive",
        "isArchived",
        "",
        "Companion",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final BUTTON_LIMIT:I = 0x3

.field public static final Companion:Lcom/squareup/invoices/ui/InvoiceButtonDataFactory$Companion;


# instance fields
.field private final archiveInvoice:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

.field private final cancel:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final deleteDraft:Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;

.field private final deleteSeriesDraft:Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;

.field private final downloadInvoice:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

.field private final duplicate:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

.field private final endSeries:Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final invoiceUrlHelper:Lcom/squareup/invoices/InvoiceUrlHelper;

.field private final issueRefund:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

.field private final more:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

.field private final sendReminder:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

.field private final share:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

.field private final unarchiveInvoice:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

.field private final viewInvoicesInSeries:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

.field private final viewTransaction:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->Companion:Lcom/squareup/invoices/ui/InvoiceButtonDataFactory$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/invoices/InvoiceUrlHelper;Lcom/squareup/settings/server/Features;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 18
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    const-string v5, "res"

    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "invoiceUrlHelper"

    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "features"

    invoke-static {v3, v5}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "currencyCode"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput-object v2, v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->invoiceUrlHelper:Lcom/squareup/invoices/InvoiceUrlHelper;

    iput-object v3, v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->features:Lcom/squareup/settings/server/Features;

    iput-object v4, v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 69
    new-instance v2, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    sget v3, Lcom/squareup/features/invoices/R$string;->issue_refund:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->ISSUE_REFUND:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    const/4 v9, 0x0

    const/4 v10, 0x4

    const/4 v11, 0x0

    move-object v6, v2

    invoke-direct/range {v6 .. v11}, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v2, v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->issueRefund:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    .line 70
    new-instance v2, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    sget v3, Lcom/squareup/common/invoices/R$string;->share:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v13

    sget-object v14, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->SHARE:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    const/4 v15, 0x0

    const/16 v16, 0x4

    const/16 v17, 0x0

    move-object v12, v2

    invoke-direct/range {v12 .. v17}, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v2, v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->share:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    .line 71
    new-instance v2, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    .line 72
    sget v3, Lcom/squareup/common/invoices/R$string;->invoice_detail_send_reminder:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 73
    sget-object v5, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->SEND_REMINDER:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v3, v2

    .line 71
    invoke-direct/range {v3 .. v8}, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v2, v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->sendReminder:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    .line 76
    new-instance v2, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    .line 77
    sget v3, Lcom/squareup/common/invoices/R$string;->invoice_detail_view_transaction:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 78
    sget-object v11, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->VIEW_TRANSACTION:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    const/4 v12, 0x0

    const/4 v13, 0x4

    const/4 v14, 0x0

    move-object v9, v2

    .line 76
    invoke-direct/range {v9 .. v14}, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v2, v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->viewTransaction:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    .line 80
    new-instance v2, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    .line 81
    sget v3, Lcom/squareup/common/invoices/R$string;->invoice_detail_duplicate:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->DUPLICATE:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    move-object v3, v2

    .line 80
    invoke-direct/range {v3 .. v8}, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v2, v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->duplicate:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    .line 83
    new-instance v2, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    sget v3, Lcom/squareup/features/invoices/R$string;->invoice_edit_cancel:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v10

    sget-object v11, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->CANCEL:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    move-object v9, v2

    invoke-direct/range {v9 .. v14}, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v2, v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->cancel:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    .line 85
    new-instance v2, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    .line 86
    sget v3, Lcom/squareup/common/invoices/R$string;->invoice_detail_view_invoices_in_series:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 87
    sget-object v5, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->VIEW_IN_SERIES:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    move-object v3, v2

    .line 85
    invoke-direct/range {v3 .. v8}, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v2, v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->viewInvoicesInSeries:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    .line 89
    new-instance v2, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    sget v3, Lcom/squareup/common/invoices/R$string;->more:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v10

    sget-object v11, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->MORE:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    move-object v9, v2

    invoke-direct/range {v9 .. v14}, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v2, v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->more:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    .line 90
    new-instance v2, Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;

    .line 91
    sget v3, Lcom/squareup/features/invoices/R$string;->invoice_detail_end_series:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 92
    sget v4, Lcom/squareup/features/invoices/R$string;->invoice_detail_end_series_confirmation:I

    invoke-interface {v1, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 93
    sget-object v5, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->END_SERIES:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    .line 90
    invoke-direct {v2, v3, v4, v5}, Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v2, v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->endSeries:Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;

    .line 96
    new-instance v2, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    sget v3, Lcom/squareup/features/invoices/R$string;->invoice_detail_download_invoice:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->DOWNLOAD_INVOICE:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    const/4 v9, 0x0

    const/4 v10, 0x4

    const/4 v11, 0x0

    move-object v6, v2

    invoke-direct/range {v6 .. v11}, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v2, v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->downloadInvoice:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    .line 99
    new-instance v2, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    sget v3, Lcom/squareup/features/invoices/R$string;->archive_invoice:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v13

    sget-object v14, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->ARCHIVE:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    move-object v12, v2

    invoke-direct/range {v12 .. v17}, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v2, v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->archiveInvoice:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    .line 102
    new-instance v2, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    sget v3, Lcom/squareup/features/invoices/R$string;->unarchive_invoice:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->UNARCHIVE:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v3, v2

    invoke-direct/range {v3 .. v8}, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v2, v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->unarchiveInvoice:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    .line 104
    new-instance v2, Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;

    .line 105
    sget v3, Lcom/squareup/features/invoices/R$string;->invoice_detail_delete_draft:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 106
    sget v4, Lcom/squareup/features/invoices/R$string;->invoice_detail_delete_draft_confirmation:I

    invoke-interface {v1, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 107
    sget-object v5, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->DELETE_DRAFT:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    .line 104
    invoke-direct {v2, v3, v4, v5}, Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v2, v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->deleteDraft:Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;

    .line 109
    new-instance v2, Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;

    .line 110
    sget v3, Lcom/squareup/features/invoices/R$string;->invoice_detail_delete_draft:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 111
    sget v4, Lcom/squareup/features/invoices/R$string;->invoice_detail_delete_draft_recurring_confirmation:I

    invoke-interface {v1, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 112
    sget-object v4, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->DELETE_DRAFT:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    .line 109
    invoke-direct {v2, v3, v1, v4}, Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v2, v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->deleteSeriesDraft:Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;

    return-void
.end method

.method private final addCancelButtonData(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/SectionElement;",
            ">;)V"
        }
    .end annotation

    .line 255
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->cancel:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private final addDeleteDraftButtonData(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/SectionElement;",
            ">;)V"
        }
    .end annotation

    .line 262
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->deleteDraft:Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private final forBottomDialog(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/features/invoices/widgets/SectionElement;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/SectionElement;",
            ">;"
        }
    .end annotation

    .line 145
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    .line 146
    check-cast p1, Ljava/lang/Iterable;

    const/4 v0, 0x2

    invoke-static {p1, v0}, Lkotlin/collections/CollectionsKt;->drop(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object p1

    goto :goto_0

    .line 148
    :cond_0
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final forDetailScreen(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/features/invoices/widgets/SectionElement;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/SectionElement;",
            ">;"
        }
    .end annotation

    .line 132
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x3

    if-le v0, v1, :cond_1

    .line 133
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x1

    if-gt v1, v2, :cond_0

    .line 135
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 137
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->more:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    move-object p1, v0

    check-cast p1, Ljava/util/List;

    :cond_1
    return-object p1
.end method

.method private final maybeAddArchiveOrUnarchive(Ljava/util/List;Lcom/squareup/settings/server/Features;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/SectionElement;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            "Z)V"
        }
    .end annotation

    .line 318
    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_ARCHIVE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p2, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p2

    if-eqz p2, :cond_1

    if-eqz p3, :cond_0

    .line 320
    iget-object p2, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->unarchiveInvoice:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 322
    :cond_0
    iget-object p2, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->archiveInvoice:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_0
    return-void
.end method

.method private final maybeAddDownloadPdf(Lcom/squareup/settings/server/Features;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/SectionElement;",
            ">;)V"
        }
    .end annotation

    .line 309
    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_DOWNLOAD_INVOICE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 310
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->downloadInvoice:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private final maybeAddDuplicateButtonData(Lcom/squareup/settings/server/Features;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/SectionElement;",
            ">;)V"
        }
    .end annotation

    .line 279
    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->INVOICES_PAYMENT_REQUEST_EDITING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 280
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->duplicate:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    invoke-interface {p3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 281
    :cond_0
    iget-object p1, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    const/4 p2, 0x1

    if-gt p1, p2, :cond_1

    .line 282
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->duplicate:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    invoke-interface {p3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_0
    return-void
.end method

.method private final maybeAddIssueRefundButtonData(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/common/CurrencyCode;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/SectionElement;",
            ">;)V"
        }
    .end annotation

    .line 246
    invoke-static {p1, p2}, Lcom/squareup/invoices/Invoices;->isPartiallyPaid(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/common/CurrencyCode;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 247
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->issueRefund:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    invoke-interface {p3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private final maybeAddSendReminderButtonData(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/SectionElement;",
            ">;)V"
        }
    .end annotation

    .line 290
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    const-string v0, "invoiceDisplayDetails.invoice"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object p1

    sget-object v0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->SHARE_LINK:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-eq p1, v0, :cond_0

    .line 292
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->sendReminder:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private final maybeAddShareButtonData(Lcom/squareup/invoices/InvoiceUrlHelper;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/InvoiceUrlHelper;",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/SectionElement;",
            ">;)V"
        }
    .end annotation

    .line 300
    invoke-virtual {p1}, Lcom/squareup/invoices/InvoiceUrlHelper;->canShareInvoiceUrl()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 301
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->share:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private final maybeAddViewTransactionButtonData(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/SectionElement;",
            ">;)V"
        }
    .end annotation

    .line 269
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz p1, :cond_0

    .line 270
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->viewTransaction:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public final createButtonDataList(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/SectionElement;",
            ">;"
        }
    .end annotation

    const-string v0, "invoiceDisplayDetails"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 158
    iget-object v1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    invoke-static {v1}, Lcom/squareup/invoices/InvoiceDisplayState;->forInvoiceDisplayState(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)Lcom/squareup/invoices/InvoiceDisplayState;

    move-result-object v1

    if-nez v1, :cond_0

    goto/16 :goto_0

    .line 159
    :cond_0
    sget-object v2, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v1}, Lcom/squareup/invoices/InvoiceDisplayState;->ordinal()I

    move-result v1

    aget v1, v2, v1

    const-string v2, "invoiceDisplayDetails.is_archived"

    packed-switch v1, :pswitch_data_0

    goto/16 :goto_0

    .line 212
    :pswitch_0
    invoke-direct {p0, p1, v0}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->addDeleteDraftButtonData(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Ljava/util/List;)V

    goto/16 :goto_0

    .line 207
    :pswitch_1
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->features:Lcom/squareup/settings/server/Features;

    invoke-direct {p0, v1, p1, v0}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->maybeAddDuplicateButtonData(Lcom/squareup/settings/server/Features;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Ljava/util/List;)V

    .line 208
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->features:Lcom/squareup/settings/server/Features;

    invoke-direct {p0, v1, v0}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->maybeAddDownloadPdf(Lcom/squareup/settings/server/Features;Ljava/util/List;)V

    .line 209
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->features:Lcom/squareup/settings/server/Features;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->is_archived:Ljava/lang/Boolean;

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->maybeAddArchiveOrUnarchive(Ljava/util/List;Lcom/squareup/settings/server/Features;Z)V

    goto/16 :goto_0

    .line 201
    :pswitch_2
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->features:Lcom/squareup/settings/server/Features;

    invoke-direct {p0, v1, p1, v0}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->maybeAddDuplicateButtonData(Lcom/squareup/settings/server/Features;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Ljava/util/List;)V

    .line 202
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {p0, p1, v1, v0}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->maybeAddIssueRefundButtonData(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/common/CurrencyCode;Ljava/util/List;)V

    .line 203
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->features:Lcom/squareup/settings/server/Features;

    invoke-direct {p0, v1, v0}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->maybeAddDownloadPdf(Lcom/squareup/settings/server/Features;Ljava/util/List;)V

    .line 204
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->features:Lcom/squareup/settings/server/Features;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->is_archived:Ljava/lang/Boolean;

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->maybeAddArchiveOrUnarchive(Ljava/util/List;Lcom/squareup/settings/server/Features;Z)V

    goto/16 :goto_0

    .line 197
    :pswitch_3
    invoke-direct {p0, p1, v0}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->addCancelButtonData(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Ljava/util/List;)V

    .line 198
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->features:Lcom/squareup/settings/server/Features;

    invoke-direct {p0, p1, v0}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->maybeAddDownloadPdf(Lcom/squareup/settings/server/Features;Ljava/util/List;)V

    goto/16 :goto_0

    .line 188
    :pswitch_4
    invoke-direct {p0, p1, v0}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->maybeAddSendReminderButtonData(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Ljava/util/List;)V

    .line 189
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->features:Lcom/squareup/settings/server/Features;

    invoke-direct {p0, v1, p1, v0}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->maybeAddDuplicateButtonData(Lcom/squareup/settings/server/Features;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Ljava/util/List;)V

    .line 190
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->invoiceUrlHelper:Lcom/squareup/invoices/InvoiceUrlHelper;

    invoke-direct {p0, v1, v0}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->maybeAddShareButtonData(Lcom/squareup/invoices/InvoiceUrlHelper;Ljava/util/List;)V

    .line 191
    invoke-direct {p0, p1, v0}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->addCancelButtonData(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Ljava/util/List;)V

    .line 192
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {p0, p1, v1, v0}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->maybeAddIssueRefundButtonData(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/common/CurrencyCode;Ljava/util/List;)V

    .line 193
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->features:Lcom/squareup/settings/server/Features;

    invoke-direct {p0, v1, v0}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->maybeAddDownloadPdf(Lcom/squareup/settings/server/Features;Ljava/util/List;)V

    .line 194
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->features:Lcom/squareup/settings/server/Features;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->is_archived:Ljava/lang/Boolean;

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->maybeAddArchiveOrUnarchive(Ljava/util/List;Lcom/squareup/settings/server/Features;Z)V

    goto :goto_0

    .line 181
    :pswitch_5
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->invoiceUrlHelper:Lcom/squareup/invoices/InvoiceUrlHelper;

    invoke-direct {p0, v1, v0}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->maybeAddShareButtonData(Lcom/squareup/invoices/InvoiceUrlHelper;Ljava/util/List;)V

    .line 182
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->features:Lcom/squareup/settings/server/Features;

    invoke-direct {p0, v1, p1, v0}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->maybeAddDuplicateButtonData(Lcom/squareup/settings/server/Features;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Ljava/util/List;)V

    .line 183
    invoke-direct {p0, p1, v0}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->addCancelButtonData(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Ljava/util/List;)V

    .line 184
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->features:Lcom/squareup/settings/server/Features;

    invoke-direct {p0, v1, v0}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->maybeAddDownloadPdf(Lcom/squareup/settings/server/Features;Ljava/util/List;)V

    .line 185
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->features:Lcom/squareup/settings/server/Features;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->is_archived:Ljava/lang/Boolean;

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->maybeAddArchiveOrUnarchive(Ljava/util/List;Lcom/squareup/settings/server/Features;Z)V

    goto :goto_0

    .line 176
    :pswitch_6
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->features:Lcom/squareup/settings/server/Features;

    invoke-direct {p0, v1, p1, v0}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->maybeAddDuplicateButtonData(Lcom/squareup/settings/server/Features;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Ljava/util/List;)V

    .line 177
    invoke-direct {p0, p1, v0}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->addCancelButtonData(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Ljava/util/List;)V

    .line 178
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->features:Lcom/squareup/settings/server/Features;

    invoke-direct {p0, p1, v0}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->maybeAddDownloadPdf(Lcom/squareup/settings/server/Features;Ljava/util/List;)V

    goto :goto_0

    .line 167
    :pswitch_7
    invoke-direct {p0, p1, v0}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->maybeAddSendReminderButtonData(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Ljava/util/List;)V

    .line 168
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->features:Lcom/squareup/settings/server/Features;

    invoke-direct {p0, v1, p1, v0}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->maybeAddDuplicateButtonData(Lcom/squareup/settings/server/Features;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Ljava/util/List;)V

    .line 169
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->invoiceUrlHelper:Lcom/squareup/invoices/InvoiceUrlHelper;

    invoke-direct {p0, v1, v0}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->maybeAddShareButtonData(Lcom/squareup/invoices/InvoiceUrlHelper;Ljava/util/List;)V

    .line 170
    invoke-direct {p0, p1, v0}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->addCancelButtonData(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Ljava/util/List;)V

    .line 171
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {p0, p1, v1, v0}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->maybeAddIssueRefundButtonData(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/common/CurrencyCode;Ljava/util/List;)V

    .line 172
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->features:Lcom/squareup/settings/server/Features;

    invoke-direct {p0, v1, v0}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->maybeAddDownloadPdf(Lcom/squareup/settings/server/Features;Ljava/util/List;)V

    .line 173
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->features:Lcom/squareup/settings/server/Features;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->is_archived:Ljava/lang/Boolean;

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->maybeAddArchiveOrUnarchive(Ljava/util/List;Lcom/squareup/settings/server/Features;Z)V

    goto :goto_0

    .line 161
    :pswitch_8
    invoke-direct {p0, p1, v0}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->maybeAddViewTransactionButtonData(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Ljava/util/List;)V

    .line 162
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->features:Lcom/squareup/settings/server/Features;

    invoke-direct {p0, v1, p1, v0}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->maybeAddDuplicateButtonData(Lcom/squareup/settings/server/Features;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Ljava/util/List;)V

    .line 163
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->features:Lcom/squareup/settings/server/Features;

    invoke-direct {p0, v1, v0}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->maybeAddDownloadPdf(Lcom/squareup/settings/server/Features;Ljava/util/List;)V

    .line 164
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->features:Lcom/squareup/settings/server/Features;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->is_archived:Ljava/lang/Boolean;

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->maybeAddArchiveOrUnarchive(Ljava/util/List;Lcom/squareup/settings/server/Features;Z)V

    :goto_0
    :pswitch_9
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_8
        :pswitch_7
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method

.method public final createButtonDataList(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/SectionElement;",
            ">;"
        }
    .end annotation

    const-string v0, "recurringSeriesDisplayDetails"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 225
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 226
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->ordinal()I

    move-result p1

    aget p1, v1, p1

    const/4 v1, 0x1

    if-eq p1, v1, :cond_2

    const/4 v1, 0x2

    if-eq p1, v1, :cond_1

    .line 235
    :goto_0
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->viewInvoicesInSeries:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 231
    :cond_1
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->endSeries:Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 232
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->viewInvoicesInSeries:Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 228
    :cond_2
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->deleteSeriesDraft:Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    return-object v0
.end method

.method public final createForBottomDialog(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/SectionElement;",
            ">;"
        }
    .end annotation

    const-string v0, "invoiceDisplayDetails"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->createButtonDataList(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->forBottomDialog(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final createForBottomDialog(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/SectionElement;",
            ">;"
        }
    .end annotation

    const-string v0, "recurringSeriesDisplayDetails"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->createButtonDataList(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->forBottomDialog(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final createForDetailScreen(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/SectionElement;",
            ">;"
        }
    .end annotation

    const-string v0, "invoiceDisplayDetails"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->createButtonDataList(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->forDetailScreen(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final createForDetailScreen(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/SectionElement;",
            ">;"
        }
    .end annotation

    const-string v0, "recurringSeriesDisplayDetails"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->createButtonDataList(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory;->forDetailScreen(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
