.class Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "BillHistoryDetailScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/BillHistoryDetailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/invoices/ui/BillHistoryDetailView;",
        ">;"
    }
.end annotation


# static fields
.field private static final INVOICE_BILL_ERRORS_KEY:Ljava/lang/String; = "invoice-bill-history-detail-view"


# instance fields
.field private billToken:Ljava/lang/String;

.field private final controller:Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Controller;

.field private final errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private screen:Lcom/squareup/invoices/ui/BillHistoryDetailScreen;

.field private title:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Controller;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Controller;",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 105
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 106
    iput-object p1, p0, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 107
    iput-object p2, p0, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;->controller:Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Controller;

    .line 108
    iput-object p4, p0, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 109
    iput-object p3, p0, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    const/4 p1, 0x1

    .line 111
    invoke-virtual {p3, p1}, Lcom/squareup/ui/ErrorsBarPresenter;->setMaxMessages(I)V

    return-void
.end method

.method private handleBillLoadedFailure(Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Failed;)V
    .locals 2

    .line 174
    iget-object v0, p0, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Failed;->getFailureTitle()Ljava/lang/String;

    move-result-object p1

    const-string v1, "invoice-bill-history-detail-view"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private handleBillLoadedSuccess(Lcom/squareup/invoices/ui/BillHistoryDetailView;Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Loaded;)V
    .locals 3

    .line 178
    invoke-virtual {p2}, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Loaded;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p2

    .line 179
    iget-object v0, p0, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-static {p2, v0, v1}, Lcom/squareup/billhistory/Bills;->formatTitleOf(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 180
    iget-object v1, p0, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/squareup/ui/ErrorsBarPresenter;->removeError(Ljava/lang/String;)Z

    .line 181
    invoke-direct {p0, v0}, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;->updateTitle(Ljava/lang/String;)V

    .line 182
    invoke-virtual {p1, p2}, Lcom/squareup/invoices/ui/BillHistoryDetailView;->show(Lcom/squareup/billhistory/model/BillHistory;)V

    return-void
.end method

.method private updateTitle(Ljava/lang/String;)V
    .locals 2

    .line 186
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/BillHistoryDetailView;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/BillHistoryDetailView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    .line 187
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 188
    invoke-virtual {v1, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 189
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$null$0$BillHistoryDetailScreen$Presenter(Lcom/squareup/invoices/ui/BillHistoryDetailView;Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 138
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/invoices/ui/BillHistoryDetailView;->showProgress(Z)V

    .line 139
    instance-of v0, p2, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Failed;

    if-eqz v0, :cond_0

    .line 140
    check-cast p2, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Failed;

    invoke-direct {p0, p2}, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;->handleBillLoadedFailure(Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Failed;)V

    goto :goto_0

    .line 141
    :cond_0
    instance-of v0, p2, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Loaded;

    if-eqz v0, :cond_1

    .line 142
    check-cast p2, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Loaded;

    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;->handleBillLoadedSuccess(Lcom/squareup/invoices/ui/BillHistoryDetailView;Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Loaded;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public synthetic lambda$null$2$BillHistoryDetailScreen$Presenter(Lkotlin/Unit;)V
    .locals 0

    .line 149
    iget-object p1, p0, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;->controller:Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Controller;

    invoke-interface {p1}, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Controller;->showIssueRefundScreen()V

    return-void
.end method

.method public synthetic lambda$null$4$BillHistoryDetailScreen$Presenter(Lkotlin/Unit;)V
    .locals 0

    .line 154
    iget-object p1, p0, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;->controller:Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Controller;

    invoke-interface {p1}, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Controller;->showIssueReceiptScreen()V

    return-void
.end method

.method public synthetic lambda$null$6$BillHistoryDetailScreen$Presenter(Lcom/squareup/invoices/ui/BillHistoryDetailView;Lkotlin/Unit;)V
    .locals 0

    .line 160
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/BillHistoryDetailView;->temporarilySetReprintTicketButtonDisabled()V

    .line 161
    iget-object p1, p0, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;->controller:Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Controller;

    invoke-interface {p1}, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Controller;->reprintTicket()V

    return-void
.end method

.method public synthetic lambda$null$8$BillHistoryDetailScreen$Presenter(Lcom/squareup/invoices/ui/BillHistoryDetailView;Lkotlin/Unit;)V
    .locals 0

    .line 168
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/BillHistoryDetailView;->temporarilySetPrintGiftReceiptButtonDisabled()V

    .line 169
    iget-object p1, p0, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;->controller:Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Controller;

    invoke-interface {p1}, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Controller;->startPrintGiftReceiptFlow()V

    return-void
.end method

.method public synthetic lambda$onLoad$1$BillHistoryDetailScreen$Presenter(Lcom/squareup/invoices/ui/BillHistoryDetailView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 136
    iget-object v0, p0, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;->controller:Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Controller;

    iget-object v1, p0, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;->billToken:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Controller;->billFromBillToken(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$BillHistoryDetailScreen$Presenter$HFHRWqAvoPmTE03Jn67SwfyvCoo;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$BillHistoryDetailScreen$Presenter$HFHRWqAvoPmTE03Jn67SwfyvCoo;-><init>(Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;Lcom/squareup/invoices/ui/BillHistoryDetailView;)V

    .line 137
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$3$BillHistoryDetailScreen$Presenter(Lcom/squareup/invoices/ui/BillHistoryDetailView;)Lrx/Subscription;
    .locals 1

    .line 148
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/BillHistoryDetailView;->onRefundButtonClicked()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$BillHistoryDetailScreen$Presenter$uWNMVv4vmyEtqwYr5TeRmmG-Op8;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/ui/-$$Lambda$BillHistoryDetailScreen$Presenter$uWNMVv4vmyEtqwYr5TeRmmG-Op8;-><init>(Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;)V

    .line 149
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$5$BillHistoryDetailScreen$Presenter(Lcom/squareup/invoices/ui/BillHistoryDetailView;)Lrx/Subscription;
    .locals 1

    .line 153
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/BillHistoryDetailView;->onReceiptButtonClicked()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$BillHistoryDetailScreen$Presenter$PPuD0TUVstAixMs2WYu-N4YSGSs;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/ui/-$$Lambda$BillHistoryDetailScreen$Presenter$PPuD0TUVstAixMs2WYu-N4YSGSs;-><init>(Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;)V

    .line 154
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$7$BillHistoryDetailScreen$Presenter(Lcom/squareup/invoices/ui/BillHistoryDetailView;)Lrx/Subscription;
    .locals 2

    .line 158
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/BillHistoryDetailView;->onReprintButtonClicked()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$BillHistoryDetailScreen$Presenter$pUPEM4hiHDYwTkaSYrw0yX7dfpA;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$BillHistoryDetailScreen$Presenter$pUPEM4hiHDYwTkaSYrw0yX7dfpA;-><init>(Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;Lcom/squareup/invoices/ui/BillHistoryDetailView;)V

    .line 159
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$9$BillHistoryDetailScreen$Presenter(Lcom/squareup/invoices/ui/BillHistoryDetailView;)Lrx/Subscription;
    .locals 2

    .line 166
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/BillHistoryDetailView;->onPrintGiftReceiptButtonClicked()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$BillHistoryDetailScreen$Presenter$vFHVHBY-8DZY_SvPkrwFOYU6rMQ;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$BillHistoryDetailScreen$Presenter$vFHVHBY-8DZY_SvPkrwFOYU6rMQ;-><init>(Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;Lcom/squareup/invoices/ui/BillHistoryDetailView;)V

    .line 167
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 115
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 116
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/ui/BillHistoryDetailScreen;

    iput-object p1, p0, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;->screen:Lcom/squareup/invoices/ui/BillHistoryDetailScreen;

    .line 118
    iget-object p1, p0, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;->screen:Lcom/squareup/invoices/ui/BillHistoryDetailScreen;

    invoke-static {p1}, Lcom/squareup/invoices/ui/BillHistoryDetailScreen;->access$000(Lcom/squareup/invoices/ui/BillHistoryDetailScreen;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;->title:Ljava/lang/String;

    .line 119
    iget-object p1, p0, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;->screen:Lcom/squareup/invoices/ui/BillHistoryDetailScreen;

    invoke-static {p1}, Lcom/squareup/invoices/ui/BillHistoryDetailScreen;->access$100(Lcom/squareup/invoices/ui/BillHistoryDetailScreen;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;->billToken:Ljava/lang/String;

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 123
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 124
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/ui/BillHistoryDetailView;

    .line 126
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/BillHistoryDetailView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v3, 0x0

    .line 127
    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;->title:Ljava/lang/String;

    .line 128
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;->controller:Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Controller;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/invoices/ui/-$$Lambda$q3aMvK1sWmXoxLbc2QR1jxRDY1E;

    invoke-direct {v3, v2}, Lcom/squareup/invoices/ui/-$$Lambda$q3aMvK1sWmXoxLbc2QR1jxRDY1E;-><init>(Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Controller;)V

    .line 129
    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 130
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 126
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 132
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/invoices/ui/BillHistoryDetailView;->showProgress(Z)V

    .line 135
    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$BillHistoryDetailScreen$Presenter$XD4MhJho-rkcvGinCKg1WXwxxYc;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$BillHistoryDetailScreen$Presenter$XD4MhJho-rkcvGinCKg1WXwxxYc;-><init>(Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;Lcom/squareup/invoices/ui/BillHistoryDetailView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 147
    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$BillHistoryDetailScreen$Presenter$imNqvHWpUO9RFLKiirFT4U-0Q-8;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$BillHistoryDetailScreen$Presenter$imNqvHWpUO9RFLKiirFT4U-0Q-8;-><init>(Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;Lcom/squareup/invoices/ui/BillHistoryDetailView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 152
    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$BillHistoryDetailScreen$Presenter$ggK47Z9E19eAz85BEt67IlNW__Y;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$BillHistoryDetailScreen$Presenter$ggK47Z9E19eAz85BEt67IlNW__Y;-><init>(Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;Lcom/squareup/invoices/ui/BillHistoryDetailView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 157
    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$BillHistoryDetailScreen$Presenter$2Ep2B4PC85rY7ufypLer6PiG3LI;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$BillHistoryDetailScreen$Presenter$2Ep2B4PC85rY7ufypLer6PiG3LI;-><init>(Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;Lcom/squareup/invoices/ui/BillHistoryDetailView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 165
    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$BillHistoryDetailScreen$Presenter$4ih-ATZWWxQKkqb_KSo2y2hZo7w;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$BillHistoryDetailScreen$Presenter$4ih-ATZWWxQKkqb_KSo2y2hZo7w;-><init>(Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;Lcom/squareup/invoices/ui/BillHistoryDetailView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
