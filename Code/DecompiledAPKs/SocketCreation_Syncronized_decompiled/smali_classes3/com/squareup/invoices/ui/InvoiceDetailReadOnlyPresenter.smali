.class public Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;
.super Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;
.source "InvoiceDetailReadOnlyPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter<",
        "Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyView;",
        ">;"
    }
.end annotation


# instance fields
.field appletRunner:Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

.field scopeRunner:Lcom/squareup/invoices/ui/CrmInvoiceListRunner;

.field screen:Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen;


# direct methods
.method constructor <init>(Lcom/squareup/ui/cart/CartEntryViewsFactory;Lcom/squareup/invoices/ui/CrmInvoiceListRunner;Lcom/squareup/util/Res;Lflow/Flow;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/util/Clock;Lcom/squareup/settings/server/Features;Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/cart/CartEntryViewsFactory;",
            "Lcom/squareup/invoices/ui/CrmInvoiceListRunner;",
            "Lcom/squareup/util/Res;",
            "Lflow/Flow;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v9, p0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    .line 46
    invoke-direct/range {v0 .. v8}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;-><init>(Lcom/squareup/ui/cart/CartEntryViewsFactory;Lcom/squareup/util/Res;Lflow/Flow;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/util/Clock;Lcom/squareup/settings/server/Features;)V

    move-object v0, p2

    .line 48
    iput-object v0, v9, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;->scopeRunner:Lcom/squareup/invoices/ui/CrmInvoiceListRunner;

    move-object/from16 v0, p10

    .line 49
    iput-object v0, v9, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;->appletRunner:Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    return-void
.end method

.method private updateActionBar(Lcom/squareup/protos/client/invoice/Invoice;Ljava/lang/String;)V
    .locals 3

    .line 77
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;->createActionBarConfigForInvoice(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 79
    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailReadOnlyPresenter$3V2kluWce8gWpImconxDRuOpw4Y;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailReadOnlyPresenter$3V2kluWce8gWpImconxDRuOpw4Y;-><init>(Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;)V

    .line 80
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/invoices/R$string;->invoice_detail_show_full_invoice:I

    .line 84
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailReadOnlyPresenter$oL5uPCKYTTSNN0o9q4ER5mG78CE;

    invoke-direct {v1, p0, p2}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailReadOnlyPresenter$oL5uPCKYTTSNN0o9q4ER5mG78CE;-><init>(Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;Ljava/lang/String;)V

    .line 86
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 88
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;->getView()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyView;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyView;->setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$null$0$InvoiceDetailReadOnlyPresenter(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Ljava/lang/Boolean;
    .locals 1

    .line 66
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;->screen:Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen;->getInvoiceId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$1$InvoiceDetailReadOnlyPresenter(Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyView;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V
    .locals 1

    .line 68
    sget-object v0, Lcom/squareup/invoices/ListState;->SHOW_INVOICES:Lcom/squareup/invoices/ListState;

    invoke-virtual {p1, v0}, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyView;->changeListState(Lcom/squareup/invoices/ListState;)V

    .line 69
    iget-object p1, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object v0, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;->updateActionBar(Lcom/squareup/protos/client/invoice/Invoice;Ljava/lang/String;)V

    .line 70
    iget-object p1, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;->updateSharedInvoiceTemplateSections(Lcom/squareup/protos/client/invoice/Invoice;)V

    .line 71
    invoke-virtual {p0, p2}, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;->updateInvoiceDisplayDetailsSections(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    .line 72
    iget-object p1, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;->populateItems(Lcom/squareup/protos/client/bills/Cart;)V

    return-void
.end method

.method public synthetic lambda$onLoad$2$InvoiceDetailReadOnlyPresenter(Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyView;)Lrx/Subscription;
    .locals 2

    .line 64
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;->scopeRunner:Lcom/squareup/invoices/ui/CrmInvoiceListRunner;

    invoke-interface {v0}, Lcom/squareup/invoices/ui/CrmInvoiceListRunner;->getCurrentInvoiceDisplayDetails()Lrx/Observable;

    move-result-object v0

    .line 65
    invoke-static {}, Lcom/squareup/util/OptionalExtensionsRx1Kt;->mapIfPresent()Lrx/Observable$Transformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailReadOnlyPresenter$dkTEe91MPzOL4O0t9VqEWGJzmug;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailReadOnlyPresenter$dkTEe91MPzOL4O0t9VqEWGJzmug;-><init>(Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;)V

    .line 66
    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailReadOnlyPresenter$PsWSLDcVDp6NiJ7X8yNOv4-BOWM;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailReadOnlyPresenter$PsWSLDcVDp6NiJ7X8yNOv4-BOWM;-><init>(Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyView;)V

    .line 67
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$updateActionBar$3$InvoiceDetailReadOnlyPresenter()V
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;->scopeRunner:Lcom/squareup/invoices/ui/CrmInvoiceListRunner;

    invoke-interface {v0}, Lcom/squareup/invoices/ui/CrmInvoiceListRunner;->clearCurrentInvoice()V

    .line 82
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public synthetic lambda$updateActionBar$4$InvoiceDetailReadOnlyPresenter(Ljava/lang/String;)V
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;->appletRunner:Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    invoke-interface {v0, p1}, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;->viewFullInvoiceDetail(Ljava/lang/String;)V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 53
    invoke-super {p0, p1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 54
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen;

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;->screen:Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen;

    .line 56
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;->scopeRunner:Lcom/squareup/invoices/ui/CrmInvoiceListRunner;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;->screen:Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen;->getInvoiceId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/invoices/ui/CrmInvoiceListRunner;->setCurrentInvoiceId(Ljava/lang/String;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 60
    invoke-super {p0, p1}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 61
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyView;

    .line 63
    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailReadOnlyPresenter$hTxJMaeOBNI8xNrMlE1162bTXAY;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailReadOnlyPresenter$hTxJMaeOBNI8xNrMlE1162bTXAY;-><init>(Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
