.class public final Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;
.super Ljava/lang/Object;
.source "InvoiceHistoryPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final actionBarNavigationHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final appletSelectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;"
        }
    .end annotation
.end field

.field private final cacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            ">;"
        }
    .end annotation
.end field

.field private final dateFormatProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final filterDropDownPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final scopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final showEstimatesBannerPreferenceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;)V"
        }
    .end annotation

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p2, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->actionBarNavigationHelperProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p3, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p4, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p5, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->dateFormatProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p6, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->invoiceLoaderProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p7, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->localeProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p8, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->filterDropDownPresenterProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p9, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 78
    iput-object p10, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 79
    iput-object p11, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 80
    iput-object p12, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 81
    iput-object p13, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->cacheProvider:Ljavax/inject/Provider;

    .line 82
    iput-object p14, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->appletSelectionProvider:Ljavax/inject/Provider;

    .line 83
    iput-object p15, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->showEstimatesBannerPreferenceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;)",
            "Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;"
        }
    .end annotation

    .line 102
    new-instance v16, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    invoke-direct/range {v0 .. v15}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v16
.end method

.method public static newInstance(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/invoices/ui/InvoiceLoader;Ljavax/inject/Provider;Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lflow/Flow;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/applet/AppletSelection;Lcom/f2prateek/rx/preferences2/Preference;)Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/invoices/ui/InvoiceLoader;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/analytics/Analytics;",
            "Lflow/Flow;",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            "Lcom/squareup/applet/AppletSelection;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;"
        }
    .end annotation

    .line 111
    new-instance v16, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    invoke-direct/range {v0 .. v15}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/invoices/ui/InvoiceLoader;Ljavax/inject/Provider;Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lflow/Flow;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/applet/AppletSelection;Lcom/f2prateek/rx/preferences2/Preference;)V

    return-object v16
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;
    .locals 17

    move-object/from16 v0, p0

    .line 88
    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->actionBarNavigationHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/applet/ActionBarNavigationHelper;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/text/Formatter;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->dateFormatProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Ljava/text/DateFormat;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->invoiceLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/invoices/ui/InvoiceLoader;

    iget-object v8, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->localeProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->filterDropDownPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/util/Device;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->cacheProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->appletSelectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/applet/AppletSelection;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->showEstimatesBannerPreferenceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/f2prateek/rx/preferences2/Preference;

    invoke-static/range {v2 .. v16}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->newInstance(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/invoices/ui/InvoiceLoader;Ljavax/inject/Provider;Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lflow/Flow;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/applet/AppletSelection;Lcom/f2prateek/rx/preferences2/Preference;)Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter_Factory;->get()Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    move-result-object v0

    return-object v0
.end method
