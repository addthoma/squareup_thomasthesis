.class public Lcom/squareup/invoices/ui/InvoiceFilterMasterView;
.super Landroid/widget/LinearLayout;
.source "InvoiceFilterMasterView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/ui/InvoiceFilterMasterView$SectionsAdapter;
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private adapter:Lcom/squareup/invoices/ui/InvoiceFilterMasterView$SectionsAdapter;

.field appletSelection:Lcom/squareup/applet/AppletSelection;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field badgePresenter:Lcom/squareup/applet/BadgePresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field presenter:Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private sectionList:Lcom/squareup/stickylistheaders/StickyListHeadersListView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->getContext()Landroid/content/Context;

    move-result-object p1

    const-class p2, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Component;->inject(Lcom/squareup/invoices/ui/InvoiceFilterMasterView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 64
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 65
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_filter_listview:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->sectionList:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    return-void
.end method


# virtual methods
.method public createAdapter()V
    .locals 2

    .line 69
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView$SectionsAdapter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/invoices/ui/InvoiceFilterMasterView$SectionsAdapter;-><init>(Lcom/squareup/invoices/ui/InvoiceFilterMasterView;Lcom/squareup/invoices/ui/InvoiceFilterMasterView$1;)V

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->adapter:Lcom/squareup/invoices/ui/InvoiceFilterMasterView$SectionsAdapter;

    .line 70
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->sectionList:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->adapter:Lcom/squareup/invoices/ui/InvoiceFilterMasterView$SectionsAdapter;

    invoke-virtual {v0, v1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->setAdapter(Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;)V

    return-void
.end method

.method public synthetic lambda$null$0$InvoiceFilterMasterView(Lcom/squareup/applet/Applet;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 49
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    .line 50
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/features/invoices/R$string;->invoice_history_title:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 49
    invoke-interface {v0, v1, v2, p1}, Lcom/squareup/applet/ActionBarNavigationHelper;->makeActionBarForAppletActivationScreen(Lcom/squareup/marin/widgets/MarinActionBar;Ljava/lang/String;Lcom/squareup/applet/Applet;)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$1$InvoiceFilterMasterView()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->appletSelection:Lcom/squareup/applet/AppletSelection;

    invoke-interface {v0}, Lcom/squareup/applet/AppletSelection;->selectedApplet()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceFilterMasterView$CZshZOUrTQY_rKl46AdCBNqBxdU;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceFilterMasterView$CZshZOUrTQY_rKl46AdCBNqBxdU;-><init>(Lcom/squareup/invoices/ui/InvoiceFilterMasterView;)V

    .line 48
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onAttachedToWindow$2$InvoiceFilterMasterView(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 53
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->presenter:Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;

    invoke-virtual {p1, p3}, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;->onSectionClicked(I)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .line 42
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 44
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->bindViews()V

    .line 45
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0, v1}, Lcom/squareup/applet/BadgePresenter;->takeView(Ljava/lang/Object;)V

    .line 47
    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceFilterMasterView$k6_JM6r7b0CT1IzagBgBNY2XFhs;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceFilterMasterView$k6_JM6r7b0CT1IzagBgBNY2XFhs;-><init>(Lcom/squareup/invoices/ui/InvoiceFilterMasterView;)V

    invoke-static {p0, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->sectionList:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceFilterMasterView$Vf0yrDZB4ko9MGN3zgyL52UkxtM;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceFilterMasterView$Vf0yrDZB4ko9MGN3zgyL52UkxtM;-><init>(Lcom/squareup/invoices/ui/InvoiceFilterMasterView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->presenter:Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 58
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0, v1}, Lcom/squareup/applet/BadgePresenter;->dropView(Lcom/squareup/marin/widgets/Badgeable;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->presenter:Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 60
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public refreshAdapter()V
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceFilterMasterView;->adapter:Lcom/squareup/invoices/ui/InvoiceFilterMasterView$SectionsAdapter;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceFilterMasterView$SectionsAdapter;->notifyDataSetChanged()V

    return-void
.end method
