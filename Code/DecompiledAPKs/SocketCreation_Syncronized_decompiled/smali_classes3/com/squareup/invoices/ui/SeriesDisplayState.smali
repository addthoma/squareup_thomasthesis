.class public final enum Lcom/squareup/invoices/ui/SeriesDisplayState;
.super Ljava/lang/Enum;
.source "SeriesDisplayState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/invoices/ui/SeriesDisplayState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/invoices/ui/SeriesDisplayState;

.field public static final enum ACTIVE:Lcom/squareup/invoices/ui/SeriesDisplayState;

.field public static final enum DRAFT:Lcom/squareup/invoices/ui/SeriesDisplayState;

.field public static final enum ENDED:Lcom/squareup/invoices/ui/SeriesDisplayState;

.field public static final enum UNKONWN:Lcom/squareup/invoices/ui/SeriesDisplayState;


# instance fields
.field private colorId:I

.field private displayState:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

.field private titleId:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 12
    new-instance v6, Lcom/squareup/invoices/ui/SeriesDisplayState;

    sget v3, Lcom/squareup/features/invoices/R$string;->recurring_state_filter_unknown:I

    sget-object v5, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->UNUSED:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    const-string v1, "UNKONWN"

    const/4 v2, 0x0

    const/4 v4, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/invoices/ui/SeriesDisplayState;-><init>(Ljava/lang/String;IIILcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;)V

    sput-object v6, Lcom/squareup/invoices/ui/SeriesDisplayState;->UNKONWN:Lcom/squareup/invoices/ui/SeriesDisplayState;

    .line 13
    new-instance v0, Lcom/squareup/invoices/ui/SeriesDisplayState;

    sget v10, Lcom/squareup/features/invoices/R$string;->recurring_state_filter_draft_short:I

    sget v11, Lcom/squareup/marin/R$color;->marin_blue:I

    sget-object v12, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->DRAFT:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    const-string v8, "DRAFT"

    const/4 v9, 0x1

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/invoices/ui/SeriesDisplayState;-><init>(Ljava/lang/String;IIILcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;)V

    sput-object v0, Lcom/squareup/invoices/ui/SeriesDisplayState;->DRAFT:Lcom/squareup/invoices/ui/SeriesDisplayState;

    .line 15
    new-instance v0, Lcom/squareup/invoices/ui/SeriesDisplayState;

    sget v4, Lcom/squareup/features/invoices/R$string;->recurring_state_filter_active_short:I

    sget v5, Lcom/squareup/marin/R$color;->marin_green:I

    sget-object v6, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->ACTIVE:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    const-string v2, "ACTIVE"

    const/4 v3, 0x2

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/invoices/ui/SeriesDisplayState;-><init>(Ljava/lang/String;IIILcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;)V

    sput-object v0, Lcom/squareup/invoices/ui/SeriesDisplayState;->ACTIVE:Lcom/squareup/invoices/ui/SeriesDisplayState;

    .line 17
    new-instance v0, Lcom/squareup/invoices/ui/SeriesDisplayState;

    sget v10, Lcom/squareup/features/invoices/R$string;->recurring_state_filter_inactive_short:I

    sget-object v12, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->ENDED:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    const-string v8, "ENDED"

    const/4 v9, 0x3

    const/4 v11, 0x0

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/invoices/ui/SeriesDisplayState;-><init>(Ljava/lang/String;IIILcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;)V

    sput-object v0, Lcom/squareup/invoices/ui/SeriesDisplayState;->ENDED:Lcom/squareup/invoices/ui/SeriesDisplayState;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/invoices/ui/SeriesDisplayState;

    .line 11
    sget-object v1, Lcom/squareup/invoices/ui/SeriesDisplayState;->UNKONWN:Lcom/squareup/invoices/ui/SeriesDisplayState;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/invoices/ui/SeriesDisplayState;->DRAFT:Lcom/squareup/invoices/ui/SeriesDisplayState;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/invoices/ui/SeriesDisplayState;->ACTIVE:Lcom/squareup/invoices/ui/SeriesDisplayState;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/invoices/ui/SeriesDisplayState;->ENDED:Lcom/squareup/invoices/ui/SeriesDisplayState;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/invoices/ui/SeriesDisplayState;->$VALUES:[Lcom/squareup/invoices/ui/SeriesDisplayState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIILcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;",
            ")V"
        }
    .end annotation

    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 32
    iput p3, p0, Lcom/squareup/invoices/ui/SeriesDisplayState;->titleId:I

    .line 33
    iput-object p5, p0, Lcom/squareup/invoices/ui/SeriesDisplayState;->displayState:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    if-nez p4, :cond_0

    .line 35
    sget p4, Lcom/squareup/marin/R$color;->marin_medium_gray:I

    :cond_0
    iput p4, p0, Lcom/squareup/invoices/ui/SeriesDisplayState;->colorId:I

    return-void
.end method

.method public static forSeriesDisplayState(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;)Lcom/squareup/invoices/ui/SeriesDisplayState;
    .locals 1

    .line 39
    sget-object v0, Lcom/squareup/invoices/ui/SeriesDisplayState$1;->$SwitchMap$com$squareup$protos$client$invoice$RecurringSeriesDisplayDetails$DisplayState:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    .line 47
    sget-object p0, Lcom/squareup/invoices/ui/SeriesDisplayState;->UNKONWN:Lcom/squareup/invoices/ui/SeriesDisplayState;

    return-object p0

    .line 45
    :cond_0
    sget-object p0, Lcom/squareup/invoices/ui/SeriesDisplayState;->ENDED:Lcom/squareup/invoices/ui/SeriesDisplayState;

    return-object p0

    .line 43
    :cond_1
    sget-object p0, Lcom/squareup/invoices/ui/SeriesDisplayState;->ACTIVE:Lcom/squareup/invoices/ui/SeriesDisplayState;

    return-object p0

    .line 41
    :cond_2
    sget-object p0, Lcom/squareup/invoices/ui/SeriesDisplayState;->DRAFT:Lcom/squareup/invoices/ui/SeriesDisplayState;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/invoices/ui/SeriesDisplayState;
    .locals 1

    .line 11
    const-class v0, Lcom/squareup/invoices/ui/SeriesDisplayState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/invoices/ui/SeriesDisplayState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/invoices/ui/SeriesDisplayState;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/invoices/ui/SeriesDisplayState;->$VALUES:[Lcom/squareup/invoices/ui/SeriesDisplayState;

    invoke-virtual {v0}, [Lcom/squareup/invoices/ui/SeriesDisplayState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/invoices/ui/SeriesDisplayState;

    return-object v0
.end method


# virtual methods
.method public getDisplayColor()I
    .locals 1

    .line 24
    iget v0, p0, Lcom/squareup/invoices/ui/SeriesDisplayState;->colorId:I

    return v0
.end method

.method public getTitle()I
    .locals 1

    .line 28
    iget v0, p0, Lcom/squareup/invoices/ui/SeriesDisplayState;->titleId:I

    return v0
.end method
