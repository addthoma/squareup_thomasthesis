.class public final Lcom/squareup/invoices/ui/InvoiceToDeepLink;
.super Ljava/lang/Object;
.source "InvoiceToDeepLink.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00060\tJ\u0016\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fR(\u0010\u0003\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0006 \u0007*\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00050\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/InvoiceToDeepLink;",
        "",
        "()V",
        "deepLinkInvoiceInfo",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/invoices/ui/DeepLinkInvoiceInfo;",
        "kotlin.jvm.PlatformType",
        "deepLinkInfo",
        "Lio/reactivex/Observable;",
        "setInfo",
        "",
        "token",
        "",
        "strategy",
        "Lcom/squareup/invoices/ui/InvoiceLoadingStrategy;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final deepLinkInvoiceInfo:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/invoices/ui/DeepLinkInvoiceInfo;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    sget-object v0, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    invoke-virtual {v0}, Lcom/squareup/util/Optional$Companion;->empty()Lcom/squareup/util/Optional;

    move-result-object v0

    invoke-static {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    const-string v1, "BehaviorRelay.createDefa\u2026eInfo>>(Optional.empty())"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoiceToDeepLink;->deepLinkInvoiceInfo:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$getDeepLinkInvoiceInfo$p(Lcom/squareup/invoices/ui/InvoiceToDeepLink;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/invoices/ui/InvoiceToDeepLink;->deepLinkInvoiceInfo:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method


# virtual methods
.method public final deepLinkInfo()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/ui/DeepLinkInvoiceInfo;",
            ">;"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceToDeepLink;->deepLinkInvoiceInfo:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 27
    sget-object v1, Lcom/squareup/invoices/ui/InvoiceToDeepLink$deepLinkInfo$1;->INSTANCE:Lcom/squareup/invoices/ui/InvoiceToDeepLink$deepLinkInfo$1;

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 28
    sget-object v1, Lcom/squareup/invoices/ui/InvoiceToDeepLink$deepLinkInfo$2;->INSTANCE:Lcom/squareup/invoices/ui/InvoiceToDeepLink$deepLinkInfo$2;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 29
    new-instance v1, Lcom/squareup/invoices/ui/InvoiceToDeepLink$deepLinkInfo$3;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/InvoiceToDeepLink$deepLinkInfo$3;-><init>(Lcom/squareup/invoices/ui/InvoiceToDeepLink;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "deepLinkInvoiceInfo\n    \u2026ccept(Optional.empty()) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final setInfo(Ljava/lang/String;Lcom/squareup/invoices/ui/InvoiceLoadingStrategy;)V
    .locals 3

    const-string/jumbo v0, "token"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "strategy"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceToDeepLink;->deepLinkInvoiceInfo:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    new-instance v2, Lcom/squareup/invoices/ui/DeepLinkInvoiceInfo;

    invoke-direct {v2, p1, p2}, Lcom/squareup/invoices/ui/DeepLinkInvoiceInfo;-><init>(Ljava/lang/String;Lcom/squareup/invoices/ui/InvoiceLoadingStrategy;)V

    invoke-virtual {v1, v2}, Lcom/squareup/util/Optional$Companion;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
