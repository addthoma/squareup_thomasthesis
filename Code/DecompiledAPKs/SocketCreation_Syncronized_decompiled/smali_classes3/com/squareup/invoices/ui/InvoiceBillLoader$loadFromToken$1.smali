.class final Lcom/squareup/invoices/ui/InvoiceBillLoader$loadFromToken$1;
.super Ljava/lang/Object;
.source "InvoiceBillLoader.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/ui/InvoiceBillLoader;->loadFromToken(Ljava/lang/String;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceBillLoader.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceBillLoader.kt\ncom/squareup/invoices/ui/InvoiceBillLoader$loadFromToken$1\n*L\n1#1,157:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0014\u0010\u0002\u001a\u0010\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState;",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;",
        "kotlin.jvm.PlatformType",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/ui/InvoiceBillLoader;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/ui/InvoiceBillLoader;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader$loadFromToken$1;->this$0:Lcom/squareup/invoices/ui/InvoiceBillLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;",
            ">;)",
            "Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_1

    .line 64
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;->bill_family:Ljava/util/List;

    const-string v1, "it.response.bill_family"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 65
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Loaded;

    .line 67
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;->bill_family:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;

    .line 68
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader$loadFromToken$1;->this$0:Lcom/squareup/invoices/ui/InvoiceBillLoader;

    invoke-static {v1}, Lcom/squareup/invoices/ui/InvoiceBillLoader;->access$getRes$p(Lcom/squareup/invoices/ui/InvoiceBillLoader;)Lcom/squareup/util/Res;

    move-result-object v1

    .line 69
    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader$loadFromToken$1;->this$0:Lcom/squareup/invoices/ui/InvoiceBillLoader;

    invoke-static {v2}, Lcom/squareup/invoices/ui/InvoiceBillLoader;->access$getVoidCompSettings$p(Lcom/squareup/invoices/ui/InvoiceBillLoader;)Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isVoidCompAllowed()Z

    move-result v2

    .line 70
    iget-object v3, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader$loadFromToken$1;->this$0:Lcom/squareup/invoices/ui/InvoiceBillLoader;

    invoke-static {v3}, Lcom/squareup/invoices/ui/InvoiceBillLoader;->access$getFeatures$p(Lcom/squareup/invoices/ui/InvoiceBillLoader;)Lcom/squareup/settings/server/Features;

    move-result-object v3

    sget-object v4, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v3, v4}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v3

    .line 66
    invoke-static {p1, v1, v2, v3}, Lcom/squareup/billhistory/Bills;->toBill(Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;Lcom/squareup/util/Res;ZZ)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    const-string/jumbo v1, "toBill(\n                \u2026NS)\n                    )"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-direct {v0, p1}, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Loaded;-><init>(Lcom/squareup/billhistory/model/BillHistory;)V

    check-cast v0, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState;

    goto :goto_0

    .line 74
    :cond_0
    new-instance p1, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Failed;

    .line 75
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader$loadFromToken$1;->this$0:Lcom/squareup/invoices/ui/InvoiceBillLoader;

    invoke-static {v0}, Lcom/squareup/invoices/ui/InvoiceBillLoader;->access$getRes$p(Lcom/squareup/invoices/ui/InvoiceBillLoader;)Lcom/squareup/util/Res;

    move-result-object v0

    sget v1, Lcom/squareup/common/strings/R$string;->failed:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 76
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader$loadFromToken$1;->this$0:Lcom/squareup/invoices/ui/InvoiceBillLoader;

    invoke-static {v1}, Lcom/squareup/invoices/ui/InvoiceBillLoader;->access$getRes$p(Lcom/squareup/invoices/ui/InvoiceBillLoader;)Lcom/squareup/util/Res;

    move-result-object v1

    sget v2, Lcom/squareup/common/strings/R$string;->server_error_message:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 74
    invoke-direct {p1, v0, v1}, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Failed;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p1

    check-cast v0, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState;

    goto :goto_0

    .line 80
    :cond_1
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_2

    .line 82
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader$loadFromToken$1;->this$0:Lcom/squareup/invoices/ui/InvoiceBillLoader;

    invoke-static {v0}, Lcom/squareup/invoices/ui/InvoiceBillLoader;->access$getFailureMessageFactory$p(Lcom/squareup/invoices/ui/InvoiceBillLoader;)Lcom/squareup/receiving/FailureMessageFactory;

    move-result-object v0

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    sget v1, Lcom/squareup/common/strings/R$string;->failed:I

    sget-object v2, Lcom/squareup/invoices/ui/InvoiceBillLoader$loadFromToken$1$failureMessage$1;->INSTANCE:Lcom/squareup/invoices/ui/InvoiceBillLoader$loadFromToken$1$failureMessage$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 85
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Failed;

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Failed;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState;

    :goto_0
    return-object v0

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 36
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoiceBillLoader$loadFromToken$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState;

    move-result-object p1

    return-object p1
.end method
