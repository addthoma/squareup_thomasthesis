.class final Lcom/squareup/invoices/image/FileViewer$viewFileExternally$1;
.super Ljava/lang/Object;
.source "FileViewer.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/image/FileViewer;->viewFileExternally(Ljava/io/File;Ljava/lang/String;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "optionalFile",
        "Lcom/squareup/util/Optional;",
        "Ljava/io/File;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $mimeType:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/invoices/image/FileViewer;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/image/FileViewer;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/image/FileViewer$viewFileExternally$1;->this$0:Lcom/squareup/invoices/image/FileViewer;

    iput-object p2, p0, Lcom/squareup/invoices/image/FileViewer$viewFileExternally$1;->$mimeType:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/util/Optional;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/image/FileViewer$viewFileExternally$1;->apply(Lcom/squareup/util/Optional;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final apply(Lcom/squareup/util/Optional;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Optional<",
            "+",
            "Ljava/io/File;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "optionalFile"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    sget-object v0, Lcom/squareup/util/Optional$Empty;->INSTANCE:Lcom/squareup/util/Optional$Empty;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 58
    :cond_0
    instance-of v0, p1, Lcom/squareup/util/Optional$Present;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/invoices/image/FileViewer$viewFileExternally$1;->this$0:Lcom/squareup/invoices/image/FileViewer;

    invoke-virtual {p1}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/io/File;

    iget-object v1, p0, Lcom/squareup/invoices/image/FileViewer$viewFileExternally$1;->$mimeType:Ljava/lang/String;

    invoke-static {v0, p1, v1}, Lcom/squareup/invoices/image/FileViewer;->access$launchViewer(Lcom/squareup/invoices/image/FileViewer;Ljava/io/File;Ljava/lang/String;)Z

    move-result p1

    :goto_0
    return p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
