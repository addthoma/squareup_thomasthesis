.class public final Lcom/squareup/invoices/image/InvoiceFileAttachmentServiceHelper;
.super Ljava/lang/Object;
.source "InvoiceFileAttachmentServiceHelper.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\"\u0010\u0005\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00080\u00070\u00062\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\nJ.\u0010\u000c\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\r0\u00070\u00062\u0006\u0010\t\u001a\u00020\n2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/invoices/image/InvoiceFileAttachmentServiceHelper;",
        "",
        "service",
        "Lcom/squareup/server/invoices/InvoiceFileAttachmentService;",
        "(Lcom/squareup/server/invoices/InvoiceFileAttachmentService;)V",
        "downloadFile",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lokhttp3/ResponseBody;",
        "invoiceToken",
        "",
        "attachmentToken",
        "uploadFile",
        "Lcom/squareup/server/invoices/InvoiceFileUploadResponse;",
        "file",
        "Ljava/io/File;",
        "mediaType",
        "Lokhttp3/MediaType;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final service:Lcom/squareup/server/invoices/InvoiceFileAttachmentService;


# direct methods
.method public constructor <init>(Lcom/squareup/server/invoices/InvoiceFileAttachmentService;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "service"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/image/InvoiceFileAttachmentServiceHelper;->service:Lcom/squareup/server/invoices/InvoiceFileAttachmentService;

    return-void
.end method


# virtual methods
.method public final downloadFile(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lokhttp3/ResponseBody;",
            ">;>;"
        }
    .end annotation

    const-string v0, "invoiceToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attachmentToken"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/squareup/invoices/image/InvoiceFileAttachmentServiceHelper;->service:Lcom/squareup/server/invoices/InvoiceFileAttachmentService;

    .line 40
    invoke-interface {v0, p1, p2}, Lcom/squareup/server/invoices/InvoiceFileAttachmentService;->download(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    .line 41
    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public final uploadFile(Ljava/lang/String;Ljava/io/File;Lokhttp3/MediaType;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/io/File;",
            "Lokhttp3/MediaType;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/invoices/InvoiceFileUploadResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "invoiceToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 25
    check-cast v0, Lokhttp3/MultipartBody$Part;

    if-eqz p2, :cond_0

    .line 27
    sget-object v0, Lokhttp3/RequestBody;->Companion:Lokhttp3/RequestBody$Companion;

    const-string v1, "mediaType"

    invoke-static {p3, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lokhttp3/MediaType;

    invoke-virtual {v0, p3, p2}, Lokhttp3/RequestBody$Companion;->create(Lokhttp3/MediaType;Ljava/io/File;)Lokhttp3/RequestBody;

    move-result-object p3

    .line 28
    sget-object v0, Lokhttp3/MultipartBody$Part;->Companion:Lokhttp3/MultipartBody$Part$Companion;

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p2

    const-string v1, "file"

    invoke-virtual {v0, v1, p2, p3}, Lokhttp3/MultipartBody$Part$Companion;->createFormData(Ljava/lang/String;Ljava/lang/String;Lokhttp3/RequestBody;)Lokhttp3/MultipartBody$Part;

    move-result-object v0

    .line 31
    :cond_0
    iget-object p2, p0, Lcom/squareup/invoices/image/InvoiceFileAttachmentServiceHelper;->service:Lcom/squareup/server/invoices/InvoiceFileAttachmentService;

    const-string p3, ""

    invoke-interface {p2, p1, v0, p3}, Lcom/squareup/server/invoices/InvoiceFileAttachmentService;->upload(Ljava/lang/String;Lokhttp3/MultipartBody$Part;Ljava/lang/String;)Lcom/squareup/server/SimpleStandardResponse;

    move-result-object p1

    .line 32
    invoke-virtual {p1}, Lcom/squareup/server/SimpleStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
