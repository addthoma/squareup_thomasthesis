.class public final Lcom/squareup/invoices/image/FileViewer;
.super Ljava/lang/Object;
.source "FileViewer.kt"

# interfaces
.implements Lcom/squareup/ui/ActivityDelegate;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/image/FileViewer$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFileViewer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 FileViewer.kt\ncom/squareup/invoices/image/FileViewer\n*L\n1#1,113:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u0000 \u001d2\u00020\u0001:\u0001\u001dB\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0018\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\t\u001a\u00020\nH\u0016J\u0010\u0010\u0013\u001a\u00020\u00122\u0006\u0010\t\u001a\u00020\nH\u0016J\u001c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u00152\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010J$\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u00152\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0019\u001a\u00020\u0010J\u0016\u0010\u001a\u001a\u00020\u000c*\u0004\u0018\u00010\n2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/invoices/image/FileViewer;",
        "Lcom/squareup/ui/ActivityDelegate;",
        "intentAvailabilityManager",
        "Lcom/squareup/util/IntentAvailabilityManager;",
        "invoiceFileHelper",
        "Lcom/squareup/invoices/image/InvoiceFileHelper;",
        "fileViewingIntentCreator",
        "Lcom/squareup/invoices/image/ExternalFileViewingIntentCreator;",
        "(Lcom/squareup/util/IntentAvailabilityManager;Lcom/squareup/invoices/image/InvoiceFileHelper;Lcom/squareup/invoices/image/ExternalFileViewingIntentCreator;)V",
        "activity",
        "Landroid/app/Activity;",
        "launchViewer",
        "",
        "file",
        "Ljava/io/File;",
        "mimeType",
        "",
        "onCreate",
        "",
        "onDestroy",
        "viewFileExternally",
        "Lio/reactivex/Single;",
        "viewUriExternally",
        "uri",
        "Landroid/net/Uri;",
        "extension",
        "startActivity",
        "intent",
        "Landroid/content/Intent;",
        "Companion",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/image/FileViewer$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final DEFAULT_EXTERNAL_INVOICE_ATTACHMENT_NAME:Ljava/lang/String; = "InvoiceAttachmentExternal"


# instance fields
.field private activity:Landroid/app/Activity;

.field private final fileViewingIntentCreator:Lcom/squareup/invoices/image/ExternalFileViewingIntentCreator;

.field private final intentAvailabilityManager:Lcom/squareup/util/IntentAvailabilityManager;

.field private final invoiceFileHelper:Lcom/squareup/invoices/image/InvoiceFileHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/image/FileViewer$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/image/FileViewer$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/image/FileViewer;->Companion:Lcom/squareup/invoices/image/FileViewer$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/IntentAvailabilityManager;Lcom/squareup/invoices/image/InvoiceFileHelper;Lcom/squareup/invoices/image/ExternalFileViewingIntentCreator;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "intentAvailabilityManager"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceFileHelper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fileViewingIntentCreator"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/image/FileViewer;->intentAvailabilityManager:Lcom/squareup/util/IntentAvailabilityManager;

    iput-object p2, p0, Lcom/squareup/invoices/image/FileViewer;->invoiceFileHelper:Lcom/squareup/invoices/image/InvoiceFileHelper;

    iput-object p3, p0, Lcom/squareup/invoices/image/FileViewer;->fileViewingIntentCreator:Lcom/squareup/invoices/image/ExternalFileViewingIntentCreator;

    return-void
.end method

.method public static final synthetic access$launchViewer(Lcom/squareup/invoices/image/FileViewer;Ljava/io/File;Ljava/lang/String;)Z
    .locals 0

    .line 20
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/image/FileViewer;->launchViewer(Ljava/io/File;Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method private final launchViewer(Ljava/io/File;Ljava/lang/String;)Z
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/invoices/image/FileViewer;->fileViewingIntentCreator:Lcom/squareup/invoices/image/ExternalFileViewingIntentCreator;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/invoices/image/ExternalFileViewingIntentCreator;->createChooserIntent(Ljava/io/File;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    .line 94
    iget-object p2, p0, Lcom/squareup/invoices/image/FileViewer;->intentAvailabilityManager:Lcom/squareup/util/IntentAvailabilityManager;

    invoke-interface {p2, p1}, Lcom/squareup/util/IntentAvailabilityManager;->isAvailable(Landroid/content/Intent;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 95
    iget-object p2, p0, Lcom/squareup/invoices/image/FileViewer;->activity:Landroid/app/Activity;

    invoke-direct {p0, p2, p1}, Lcom/squareup/invoices/image/FileViewer;->startActivity(Landroid/app/Activity;Landroid/content/Intent;)Z

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final startActivity(Landroid/app/Activity;Landroid/content/Intent;)Z
    .locals 0

    if-eqz p1, :cond_0

    .line 104
    invoke-virtual {p1, p2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public onCreate(Landroid/app/Activity;)V
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    iput-object p1, p0, Lcom/squareup/invoices/image/FileViewer;->activity:Landroid/app/Activity;

    return-void
.end method

.method public onDestroy(Landroid/app/Activity;)V
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lcom/squareup/invoices/image/FileViewer;->activity:Landroid/app/Activity;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 35
    check-cast p1, Landroid/app/Activity;

    iput-object p1, p0, Lcom/squareup/invoices/image/FileViewer;->activity:Landroid/app/Activity;

    :cond_0
    return-void
.end method

.method public final viewFileExternally(Ljava/io/File;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "file"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mimeType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/invoices/image/FileViewer;->invoiceFileHelper:Lcom/squareup/invoices/image/InvoiceFileHelper;

    const-string v1, "InvoiceAttachmentExternal"

    invoke-virtual {v0, p1, v1}, Lcom/squareup/invoices/image/InvoiceFileHelper;->copyToExternalFile(Ljava/io/File;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    .line 55
    new-instance v0, Lcom/squareup/invoices/image/FileViewer$viewFileExternally$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/invoices/image/FileViewer$viewFileExternally$1;-><init>(Lcom/squareup/invoices/image/FileViewer;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "invoiceFileHelper.copyTo\u2026pe)\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final viewUriExternally(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string/jumbo v0, "uri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mimeType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "extension"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    iget-object v0, p0, Lcom/squareup/invoices/image/FileViewer;->invoiceFileHelper:Lcom/squareup/invoices/image/InvoiceFileHelper;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p3, v1}, Lcom/squareup/invoices/image/InvoiceFileHelper;->createFileFromUri(Landroid/net/Uri;Ljava/lang/String;Z)Lio/reactivex/Single;

    move-result-object p1

    .line 80
    new-instance p3, Lcom/squareup/invoices/image/FileViewer$viewUriExternally$1;

    invoke-direct {p3, p0, p2}, Lcom/squareup/invoices/image/FileViewer$viewUriExternally$1;-><init>(Lcom/squareup/invoices/image/FileViewer;Ljava/lang/String;)V

    check-cast p3, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p3}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "invoiceFileHelper.create\u2026lse\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
