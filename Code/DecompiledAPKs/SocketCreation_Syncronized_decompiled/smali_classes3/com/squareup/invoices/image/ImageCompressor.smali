.class public final Lcom/squareup/invoices/image/ImageCompressor;
.super Ljava/lang/Object;
.source "ImageCompressor.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/image/ImageCompressor$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u00102\u00020\u0001:\u0001\u0010B-\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0014\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b2\u0006\u0010\r\u001a\u00020\u000eJ\u000c\u0010\u000f\u001a\u00020\u0006*\u00020\u000eH\u0002R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/invoices/image/ImageCompressor;",
        "",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "fileThreadScheduler",
        "tempPhotoDir",
        "Ljava/io/File;",
        "application",
        "Landroid/app/Application;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Ljava/io/File;Landroid/app/Application;)V",
        "compress",
        "Lio/reactivex/Single;",
        "Lcom/squareup/invoices/image/CompressionResult;",
        "uri",
        "Landroid/net/Uri;",
        "compressToFile",
        "Companion",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final COMPRESSION_QUALITY:I = 0x46

.field public static final Companion:Lcom/squareup/invoices/image/ImageCompressor$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field private final application:Landroid/app/Application;

.field private final fileThreadScheduler:Lio/reactivex/Scheduler;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final tempPhotoDir:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/image/ImageCompressor$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/image/ImageCompressor$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/image/ImageCompressor;->Companion:Lcom/squareup/invoices/image/ImageCompressor$Companion;

    return-void
.end method

.method public constructor <init>(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Ljava/io/File;Landroid/app/Application;)V
    .locals 1
    .param p1    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .param p3    # Ljava/io/File;
        .annotation runtime Lcom/squareup/util/TempPhotoDir;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "mainScheduler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fileThreadScheduler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tempPhotoDir"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "application"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/image/ImageCompressor;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p2, p0, Lcom/squareup/invoices/image/ImageCompressor;->fileThreadScheduler:Lio/reactivex/Scheduler;

    iput-object p3, p0, Lcom/squareup/invoices/image/ImageCompressor;->tempPhotoDir:Ljava/io/File;

    iput-object p4, p0, Lcom/squareup/invoices/image/ImageCompressor;->application:Landroid/app/Application;

    return-void
.end method

.method public static final synthetic access$compressToFile(Lcom/squareup/invoices/image/ImageCompressor;Landroid/net/Uri;)Ljava/io/File;
    .locals 0

    .line 22
    invoke-direct {p0, p1}, Lcom/squareup/invoices/image/ImageCompressor;->compressToFile(Landroid/net/Uri;)Ljava/io/File;

    move-result-object p0

    return-object p0
.end method

.method private final compressToFile(Landroid/net/Uri;)Ljava/io/File;
    .locals 6

    .line 49
    iget-object v0, p0, Lcom/squareup/invoices/image/ImageCompressor;->tempPhotoDir:Ljava/io/File;

    const-string v1, "TEMP"

    const-string v2, ".jpg"

    invoke-static {v1, v2, v0}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    const-string v1, "File.createTempFile(\"TEMP\", \".jpg\", tempPhotoDir)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 51
    iget-object v2, p0, Lcom/squareup/invoices/image/ImageCompressor;->application:Landroid/app/Application;

    invoke-virtual {v2}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object p1

    .line 53
    invoke-static {p1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 54
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    check-cast v4, Ljava/io/OutputStream;

    const/16 v5, 0x46

    invoke-virtual {v2, v3, v5, v4}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 56
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    if-eqz p1, :cond_0

    .line 57
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    :cond_0
    return-object v0
.end method


# virtual methods
.method public final compress(Landroid/net/Uri;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/invoices/image/CompressionResult;",
            ">;"
        }
    .end annotation

    const-string/jumbo v0, "uri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    new-instance v0, Lcom/squareup/invoices/image/ImageCompressor$compress$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/image/ImageCompressor$compress$1;-><init>(Lcom/squareup/invoices/image/ImageCompressor;Landroid/net/Uri;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Single;

    move-result-object p1

    .line 40
    sget-object v0, Lcom/squareup/invoices/image/ImageCompressor$compress$2;->INSTANCE:Lcom/squareup/invoices/image/ImageCompressor$compress$2;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 41
    iget-object v0, p0, Lcom/squareup/invoices/image/ImageCompressor;->fileThreadScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    .line 42
    iget-object v0, p0, Lcom/squareup/invoices/image/ImageCompressor;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.fromCallable<Comp\u2026.observeOn(mainScheduler)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
