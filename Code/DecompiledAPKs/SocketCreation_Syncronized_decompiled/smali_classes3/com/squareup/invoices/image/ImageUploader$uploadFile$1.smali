.class final Lcom/squareup/invoices/image/ImageUploader$uploadFile$1;
.super Ljava/lang/Object;
.source "ImageUploader.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/image/ImageUploader;->uploadFile(Ljava/io/File;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0012\u0012\u000e\u0008\u0001\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/invoices/image/FileUploadResult;",
        "kotlin.jvm.PlatformType",
        "validationResult",
        "Lcom/squareup/invoices/image/FileValidationResult;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $file:Ljava/io/File;

.field final synthetic $fileName:Ljava/lang/String;

.field final synthetic $invoiceTokenType:Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

.field final synthetic this$0:Lcom/squareup/invoices/image/ImageUploader;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/image/ImageUploader;Ljava/io/File;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/image/ImageUploader$uploadFile$1;->this$0:Lcom/squareup/invoices/image/ImageUploader;

    iput-object p2, p0, Lcom/squareup/invoices/image/ImageUploader$uploadFile$1;->$file:Ljava/io/File;

    iput-object p3, p0, Lcom/squareup/invoices/image/ImageUploader$uploadFile$1;->$fileName:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/invoices/image/ImageUploader$uploadFile$1;->$invoiceTokenType:Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/invoices/image/FileValidationResult;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/image/FileValidationResult;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/invoices/image/FileUploadResult;",
            ">;"
        }
    .end annotation

    const-string/jumbo v0, "validationResult"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    sget-object v0, Lcom/squareup/invoices/image/FileValidationResult$Success;->INSTANCE:Lcom/squareup/invoices/image/FileValidationResult$Success;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/image/ImageUploader$uploadFile$1;->this$0:Lcom/squareup/invoices/image/ImageUploader;

    iget-object v0, p0, Lcom/squareup/invoices/image/ImageUploader$uploadFile$1;->$file:Ljava/io/File;

    iget-object v1, p0, Lcom/squareup/invoices/image/ImageUploader$uploadFile$1;->$fileName:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/invoices/image/ImageUploader$uploadFile$1;->$invoiceTokenType:Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    invoke-static {p1, v0, v1, v2}, Lcom/squareup/invoices/image/ImageUploader;->access$uploadFileToInvoice(Lcom/squareup/invoices/image/ImageUploader;Ljava/io/File;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 91
    :cond_0
    instance-of v0, p1, Lcom/squareup/invoices/image/FileValidationResult$Failure;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/invoices/image/ImageUploader$uploadFile$1;->this$0:Lcom/squareup/invoices/image/ImageUploader;

    check-cast p1, Lcom/squareup/invoices/image/FileValidationResult$Failure;

    invoke-static {v0, p1}, Lcom/squareup/invoices/image/ImageUploader;->access$toUploadFailureResult(Lcom/squareup/invoices/image/ImageUploader;Lcom/squareup/invoices/image/FileValidationResult$Failure;)Lcom/squareup/invoices/image/FileUploadResult$Failure;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(validationRe\u2026.toUploadFailureResult())"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 29
    check-cast p1, Lcom/squareup/invoices/image/FileValidationResult;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/image/ImageUploader$uploadFile$1;->apply(Lcom/squareup/invoices/image/FileValidationResult;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
