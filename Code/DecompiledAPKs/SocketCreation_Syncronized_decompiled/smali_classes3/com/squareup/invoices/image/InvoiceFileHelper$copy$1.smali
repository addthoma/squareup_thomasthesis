.class final Lcom/squareup/invoices/image/InvoiceFileHelper$copy$1;
.super Ljava/lang/Object;
.source "InvoiceFileHelper.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/image/InvoiceFileHelper;->copy(Ljava/io/File;Ljava/lang/String;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/util/Optional;",
        "Ljava/io/File;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $file:Ljava/io/File;

.field final synthetic $name:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/invoices/image/InvoiceFileHelper;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/image/InvoiceFileHelper;Ljava/lang/String;Ljava/io/File;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/image/InvoiceFileHelper$copy$1;->this$0:Lcom/squareup/invoices/image/InvoiceFileHelper;

    iput-object p2, p0, Lcom/squareup/invoices/image/InvoiceFileHelper$copy$1;->$name:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/invoices/image/InvoiceFileHelper$copy$1;->$file:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Lcom/squareup/util/Optional;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/util/Optional<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .line 57
    new-instance v1, Ljava/io/File;

    iget-object v0, p0, Lcom/squareup/invoices/image/InvoiceFileHelper$copy$1;->this$0:Lcom/squareup/invoices/image/InvoiceFileHelper;

    invoke-static {v0}, Lcom/squareup/invoices/image/InvoiceFileHelper;->access$getTempPhotoDir$p(Lcom/squareup/invoices/image/InvoiceFileHelper;)Ljava/io/File;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/invoices/image/InvoiceFileHelper$copy$1;->$name:Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 58
    sget-object v6, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    iget-object v0, p0, Lcom/squareup/invoices/image/InvoiceFileHelper$copy$1;->$file:Ljava/io/File;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lkotlin/io/FilesKt;->copyTo$default(Ljava/io/File;Ljava/io/File;ZIILjava/lang/Object;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/squareup/util/Optional$Companion;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .line 36
    invoke-virtual {p0}, Lcom/squareup/invoices/image/InvoiceFileHelper$copy$1;->call()Lcom/squareup/util/Optional;

    move-result-object v0

    return-object v0
.end method
