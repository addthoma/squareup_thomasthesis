.class final Lcom/squareup/invoices/image/AttachmentFileViewer$viewAttachment$1$1;
.super Ljava/lang/Object;
.source "AttachmentFileViewer.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/image/AttachmentFileViewer$viewAttachment$1;->apply(Lcom/squareup/invoices/image/FileDownloadResult;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "kotlin.jvm.PlatformType",
        "accept",
        "(Ljava/lang/Boolean;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $downloadResult:Lcom/squareup/invoices/image/FileDownloadResult;

.field final synthetic this$0:Lcom/squareup/invoices/image/AttachmentFileViewer$viewAttachment$1;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/image/AttachmentFileViewer$viewAttachment$1;Lcom/squareup/invoices/image/FileDownloadResult;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/image/AttachmentFileViewer$viewAttachment$1$1;->this$0:Lcom/squareup/invoices/image/AttachmentFileViewer$viewAttachment$1;

    iput-object p2, p0, Lcom/squareup/invoices/image/AttachmentFileViewer$viewAttachment$1$1;->$downloadResult:Lcom/squareup/invoices/image/FileDownloadResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Boolean;)V
    .locals 1

    .line 32
    iget-object p1, p0, Lcom/squareup/invoices/image/AttachmentFileViewer$viewAttachment$1$1;->this$0:Lcom/squareup/invoices/image/AttachmentFileViewer$viewAttachment$1;

    iget-object p1, p1, Lcom/squareup/invoices/image/AttachmentFileViewer$viewAttachment$1;->this$0:Lcom/squareup/invoices/image/AttachmentFileViewer;

    invoke-static {p1}, Lcom/squareup/invoices/image/AttachmentFileViewer;->access$getInvoiceFileHelper$p(Lcom/squareup/invoices/image/AttachmentFileViewer;)Lcom/squareup/invoices/image/InvoiceFileHelper;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/image/AttachmentFileViewer$viewAttachment$1$1;->$downloadResult:Lcom/squareup/invoices/image/FileDownloadResult;

    check-cast v0, Lcom/squareup/invoices/image/FileDownloadResult$Success;

    invoke-virtual {v0}, Lcom/squareup/invoices/image/FileDownloadResult$Success;->getFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/invoices/image/InvoiceFileHelper;->delete(Ljava/io/File;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 10
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/image/AttachmentFileViewer$viewAttachment$1$1;->accept(Ljava/lang/Boolean;)V

    return-void
.end method
