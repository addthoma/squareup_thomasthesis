.class public Lcom/squareup/invoices/ClientInvoiceServiceHelper;
.super Ljava/lang/Object;
.source "ClientInvoiceServiceHelper.java"


# instance fields
.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

.field private final mainScheduler:Lrx/Scheduler;

.field private final service:Lcom/squareup/server/invoices/ClientInvoiceService;

.field private final standardReceiver:Lcom/squareup/receiving/StandardReceiver;


# direct methods
.method constructor <init>(Lcom/squareup/server/invoices/ClientInvoiceService;Lrx/Scheduler;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/receiving/StandardReceiver;)V
    .locals 0
    .param p2    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput-object p1, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->service:Lcom/squareup/server/invoices/ClientInvoiceService;

    .line 87
    iput-object p2, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->mainScheduler:Lrx/Scheduler;

    .line 88
    iput-object p3, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 89
    iput-object p4, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    return-void
.end method

.method static synthetic lambda$archiveInvoice$7(Lcom/squareup/protos/client/invoice/ArchiveInvoiceResponse;)Ljava/lang/Boolean;
    .locals 0

    .line 321
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/ArchiveInvoiceResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p0, p0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic lambda$deleteDraft$1(Lcom/squareup/protos/client/invoice/DeleteDraftResponse;)Ljava/lang/Boolean;
    .locals 0

    .line 169
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/DeleteDraftResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p0, p0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic lambda$deleteDraftSeries$2(Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesResponse;)Ljava/lang/Boolean;
    .locals 0

    .line 180
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p0, p0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic lambda$endSeries$3(Lcom/squareup/protos/client/invoice/EndRecurringSeriesResponse;)Ljava/lang/Boolean;
    .locals 0

    .line 248
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/EndRecurringSeriesResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p0, p0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic lambda$getAllNotificationSettings$5(Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsResponse;)Ljava/lang/Boolean;
    .locals 0

    .line 300
    iget-object p0, p0, Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsResponse;->status:Lcom/squareup/protos/invoice/v2/common/Status;

    iget-object p0, p0, Lcom/squareup/protos/invoice/v2/common/Status;->success:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic lambda$recordPayment$4(Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandResponse;)Ljava/lang/Boolean;
    .locals 0

    .line 291
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p0, p0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic lambda$sendReminder$0(Lcom/squareup/protos/client/invoice/SendInvoiceReminderResponse;)Ljava/lang/Boolean;
    .locals 0

    .line 149
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/SendInvoiceReminderResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p0, p0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic lambda$unarchiveInvoice$8(Lcom/squareup/protos/client/invoice/UnarchiveInvoiceResponse;)Ljava/lang/Boolean;
    .locals 0

    .line 333
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/UnarchiveInvoiceResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p0, p0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic lambda$updateNotificationSettings$6(Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsResponse;)Ljava/lang/Boolean;
    .locals 0

    .line 310
    iget-object p0, p0, Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsResponse;->status:Lcom/squareup/protos/invoice/v2/common/Status;

    iget-object p0, p0, Lcom/squareup/protos/invoice/v2/common/Status;->success:Ljava/lang/Boolean;

    return-object p0
.end method


# virtual methods
.method public archiveInvoice(Lcom/squareup/protos/client/invoice/Invoice;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/invoice/ArchiveInvoiceResponse;",
            ">;>;"
        }
    .end annotation

    .line 315
    new-instance v0, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$Builder;-><init>()V

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    .line 316
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$Builder;->id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->version:Ljava/lang/String;

    .line 317
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$Builder;->version(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$Builder;

    move-result-object p1

    .line 318
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$Builder;->build()Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;

    move-result-object p1

    .line 319
    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->service:Lcom/squareup/server/invoices/ClientInvoiceService;

    invoke-interface {v0, p1}, Lcom/squareup/server/invoices/ClientInvoiceService;->archiveInvoice(Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->mainScheduler:Lrx/Scheduler;

    .line 320
    invoke-virtual {p1, v0}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    sget-object v1, Lcom/squareup/invoices/-$$Lambda$ClientInvoiceServiceHelper$yp8yUnsyKH6ObrsG4ml3qO4Ls6U;->INSTANCE:Lcom/squareup/invoices/-$$Lambda$ClientInvoiceServiceHelper$yp8yUnsyKH6ObrsG4ml3qO4Ls6U;

    .line 321
    invoke-static {v0, v1}, Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p1

    .line 322
    invoke-virtual {p1}, Lrx/Observable;->toSingle()Lrx/Single;

    move-result-object p1

    .line 319
    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Single(Lrx/Single;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public cancel(Lcom/squareup/protos/client/IdPair;Z)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/IdPair;",
            "Z)",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/CancelInvoiceResponse;",
            ">;"
        }
    .end annotation

    .line 153
    new-instance v0, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest$Builder;-><init>()V

    .line 154
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest$Builder;->id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/invoice/CancelInvoiceRequest$Builder;

    move-result-object p1

    .line 155
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest$Builder;->send_email_to_recipients(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/CancelInvoiceRequest$Builder;

    move-result-object p1

    .line 156
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest$Builder;->build()Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;

    move-result-object p1

    .line 158
    iget-object p2, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->service:Lcom/squareup/server/invoices/ClientInvoiceService;

    invoke-interface {p2, p1}, Lcom/squareup/server/invoices/ClientInvoiceService;->cancel(Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;)Lrx/Observable;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->mainScheduler:Lrx/Scheduler;

    invoke-virtual {p1, p2}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public clone(Lcom/squareup/protos/client/invoice/Invoice;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/CloneInvoiceResponse;",
            ">;"
        }
    .end annotation

    .line 124
    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->service:Lcom/squareup/server/invoices/ClientInvoiceService;

    new-instance v1, Lcom/squareup/protos/client/invoice/CloneInvoiceRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/invoice/CloneInvoiceRequest$Builder;-><init>()V

    .line 126
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/invoice/CloneInvoiceRequest$Builder;->invoice(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/CloneInvoiceRequest$Builder;

    move-result-object p1

    .line 127
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/CloneInvoiceRequest$Builder;->build()Lcom/squareup/protos/client/invoice/CloneInvoiceRequest;

    move-result-object p1

    .line 124
    invoke-interface {v0, p1}, Lcom/squareup/server/invoices/ClientInvoiceService;->clone(Lcom/squareup/protos/client/invoice/CloneInvoiceRequest;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->mainScheduler:Lrx/Scheduler;

    .line 128
    invoke-virtual {p1, v0}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public deleteDraft(Lcom/squareup/protos/client/IdPair;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/IdPair;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/invoice/DeleteDraftResponse;",
            ">;>;"
        }
    .end annotation

    .line 163
    new-instance v0, Lcom/squareup/protos/client/invoice/DeleteDraftRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/DeleteDraftRequest$Builder;-><init>()V

    .line 164
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/DeleteDraftRequest$Builder;->id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/invoice/DeleteDraftRequest$Builder;

    move-result-object p1

    .line 165
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/DeleteDraftRequest$Builder;->build()Lcom/squareup/protos/client/invoice/DeleteDraftRequest;

    move-result-object p1

    .line 167
    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->service:Lcom/squareup/server/invoices/ClientInvoiceService;

    invoke-interface {v0, p1}, Lcom/squareup/server/invoices/ClientInvoiceService;->deleteDraft(Lcom/squareup/protos/client/invoice/DeleteDraftRequest;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->mainScheduler:Lrx/Scheduler;

    .line 168
    invoke-virtual {p1, v0}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    sget-object v1, Lcom/squareup/invoices/-$$Lambda$ClientInvoiceServiceHelper$WTL_XIWkCcJy8Xw3LkDuZC_rups;->INSTANCE:Lcom/squareup/invoices/-$$Lambda$ClientInvoiceServiceHelper$WTL_XIWkCcJy8Xw3LkDuZC_rups;

    .line 169
    invoke-static {v0, v1}, Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public deleteDraftSeries(Lcom/squareup/protos/client/IdPair;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/IdPair;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesResponse;",
            ">;>;"
        }
    .end annotation

    .line 174
    new-instance v0, Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesRequest$Builder;-><init>()V

    .line 175
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesRequest$Builder;->id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesRequest$Builder;

    move-result-object p1

    .line 176
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesRequest$Builder;->build()Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesRequest;

    move-result-object p1

    .line 178
    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->service:Lcom/squareup/server/invoices/ClientInvoiceService;

    invoke-interface {v0, p1}, Lcom/squareup/server/invoices/ClientInvoiceService;->deleteDraftSeries(Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesRequest;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->mainScheduler:Lrx/Scheduler;

    .line 179
    invoke-virtual {p1, v0}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    sget-object v1, Lcom/squareup/invoices/-$$Lambda$ClientInvoiceServiceHelper$_U9Hj_JG1wunBa2ObeZ8ISVX-Go;->INSTANCE:Lcom/squareup/invoices/-$$Lambda$ClientInvoiceServiceHelper$_U9Hj_JG1wunBa2ObeZ8ISVX-Go;

    .line 180
    invoke-static {v0, v1}, Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public endSeries(Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/invoice/EndRecurringSeriesResponse;",
            ">;>;"
        }
    .end annotation

    .line 241
    new-instance v0, Lcom/squareup/protos/client/invoice/EndRecurringSeriesRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/EndRecurringSeriesRequest$Builder;-><init>()V

    .line 242
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/EndRecurringSeriesRequest$Builder;->token(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/EndRecurringSeriesRequest$Builder;

    move-result-object p1

    .line 243
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/invoice/EndRecurringSeriesRequest$Builder;->version(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/EndRecurringSeriesRequest$Builder;

    move-result-object p1

    .line 244
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/EndRecurringSeriesRequest$Builder;->build()Lcom/squareup/protos/client/invoice/EndRecurringSeriesRequest;

    move-result-object p1

    .line 246
    iget-object p2, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->service:Lcom/squareup/server/invoices/ClientInvoiceService;

    invoke-interface {p2, p1}, Lcom/squareup/server/invoices/ClientInvoiceService;->endRecurring(Lcom/squareup/protos/client/invoice/EndRecurringSeriesRequest;)Lrx/Observable;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->mainScheduler:Lrx/Scheduler;

    .line 247
    invoke-virtual {p1, p2}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    sget-object v0, Lcom/squareup/invoices/-$$Lambda$ClientInvoiceServiceHelper$d85amgaVJzsbOJi5gMTid97xyvQ;->INSTANCE:Lcom/squareup/invoices/-$$Lambda$ClientInvoiceServiceHelper$d85amgaVJzsbOJi5gMTid97xyvQ;

    .line 248
    invoke-static {p2, v0}, Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public get(Ljava/lang/String;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/GetInvoiceResponse;",
            ">;"
        }
    .end annotation

    .line 108
    new-instance v0, Lcom/squareup/protos/client/invoice/GetInvoiceRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/GetInvoiceRequest$Builder;-><init>()V

    new-instance v1, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    .line 109
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/IdPair$Builder;->server_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/GetInvoiceRequest$Builder;->id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/invoice/GetInvoiceRequest$Builder;

    move-result-object p1

    .line 110
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/GetInvoiceRequest$Builder;->build()Lcom/squareup/protos/client/invoice/GetInvoiceRequest;

    move-result-object p1

    .line 112
    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->service:Lcom/squareup/server/invoices/ClientInvoiceService;

    invoke-interface {v0, p1}, Lcom/squareup/server/invoices/ClientInvoiceService;->get(Lcom/squareup/protos/client/invoice/GetInvoiceRequest;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->mainScheduler:Lrx/Scheduler;

    invoke-virtual {p1, v0}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getAllNotificationSettings()Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsResponse;",
            ">;>;"
        }
    .end annotation

    .line 296
    new-instance v0, Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsRequest$Builder;-><init>()V

    .line 297
    invoke-virtual {v0}, Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsRequest$Builder;->build()Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsRequest;

    move-result-object v0

    .line 298
    iget-object v1, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->service:Lcom/squareup/server/invoices/ClientInvoiceService;

    invoke-interface {v1, v0}, Lcom/squareup/server/invoices/ClientInvoiceService;->getAllNotificationSettings(Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsRequest;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->mainScheduler:Lrx/Scheduler;

    .line 299
    invoke-virtual {v0, v1}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    sget-object v2, Lcom/squareup/invoices/-$$Lambda$ClientInvoiceServiceHelper$U7g1UdBWRuD-NJau-zCBGOLhvq4;->INSTANCE:Lcom/squareup/invoices/-$$Lambda$ClientInvoiceServiceHelper$U7g1UdBWRuD-NJau-zCBGOLhvq4;

    .line 300
    invoke-static {v1, v2}, Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getMetrics(Ljava/lang/String;Ljava/util/List;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;",
            ">;)",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/GetMetricsResponse;",
            ">;"
        }
    .end annotation

    .line 252
    new-instance v0, Lcom/squareup/protos/client/invoice/GetMetricsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$Builder;-><init>()V

    .line 253
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$Builder;->contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/GetMetricsRequest$Builder;

    move-result-object p1

    .line 254
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$Builder;->metric_query(Ljava/util/List;)Lcom/squareup/protos/client/invoice/GetMetricsRequest$Builder;

    move-result-object p1

    .line 255
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$Builder;->build()Lcom/squareup/protos/client/invoice/GetMetricsRequest;

    move-result-object p1

    .line 256
    iget-object p2, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->service:Lcom/squareup/server/invoices/ClientInvoiceService;

    invoke-interface {p2, p1}, Lcom/squareup/server/invoices/ClientInvoiceService;->getMetrics(Lcom/squareup/protos/client/invoice/GetMetricsRequest;)Lrx/Observable;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->mainScheduler:Lrx/Scheduler;

    invoke-virtual {p1, p2}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getMetrics(Ljava/util/List;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;",
            ">;)",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/GetMetricsResponse;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 260
    invoke-virtual {p0, v0, p1}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->getMetrics(Ljava/lang/String;Ljava/util/List;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getRecurringSeries(Ljava/lang/String;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/GetRecurringSeriesResponse;",
            ">;"
        }
    .end annotation

    .line 232
    new-instance v0, Lcom/squareup/protos/client/invoice/GetRecurringSeriesRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/GetRecurringSeriesRequest$Builder;-><init>()V

    .line 233
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/GetRecurringSeriesRequest$Builder;->token(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/GetRecurringSeriesRequest$Builder;

    move-result-object p1

    .line 234
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/GetRecurringSeriesRequest$Builder;->build()Lcom/squareup/protos/client/invoice/GetRecurringSeriesRequest;

    move-result-object p1

    .line 236
    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->service:Lcom/squareup/server/invoices/ClientInvoiceService;

    invoke-interface {v0, p1}, Lcom/squareup/server/invoices/ClientInvoiceService;->getRecurring(Lcom/squareup/protos/client/invoice/GetRecurringSeriesRequest;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->mainScheduler:Lrx/Scheduler;

    invoke-virtual {p1, v0}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getUnitMetadata()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;",
            ">;"
        }
    .end annotation

    .line 184
    new-instance v0, Lcom/squareup/protos/client/invoice/GetUnitMetadataRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/GetUnitMetadataRequest$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/GetUnitMetadataRequest$Builder;->build()Lcom/squareup/protos/client/invoice/GetUnitMetadataRequest;

    move-result-object v0

    .line 185
    iget-object v1, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->service:Lcom/squareup/server/invoices/ClientInvoiceService;

    invoke-interface {v1, v0}, Lcom/squareup/server/invoices/ClientInvoiceService;->getUnitMetadata(Lcom/squareup/protos/client/invoice/GetUnitMetadataRequest;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->mainScheduler:Lrx/Scheduler;

    invoke-virtual {v0, v1}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getUnitSettings()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;",
            ">;"
        }
    .end annotation

    .line 264
    new-instance v0, Lcom/squareup/protos/client/invoice/GetUnitSettingsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/GetUnitSettingsRequest$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/GetUnitSettingsRequest$Builder;->build()Lcom/squareup/protos/client/invoice/GetUnitSettingsRequest;

    move-result-object v0

    .line 265
    iget-object v1, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->service:Lcom/squareup/server/invoices/ClientInvoiceService;

    invoke-interface {v1, v0}, Lcom/squareup/server/invoices/ClientInvoiceService;->getUnitSettings(Lcom/squareup/protos/client/invoice/GetUnitSettingsRequest;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->mainScheduler:Lrx/Scheduler;

    invoke-virtual {v0, v1}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public isOffline()Z
    .locals 1

    .line 338
    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-interface {v0}, Lcom/squareup/connectivity/ConnectivityMonitor;->isConnected()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public list(Ljava/lang/String;Ljava/lang/String;ILcom/squareup/protos/client/invoice/StateFilter;Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Lcom/squareup/protos/client/invoice/StateFilter;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/ListInvoicesResponse;",
            ">;"
        }
    .end annotation

    .line 94
    new-instance v0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;-><init>()V

    .line 95
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->query(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;

    move-result-object p1

    .line 96
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->paging_key(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;

    move-result-object p1

    .line 97
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->limit(Ljava/lang/Integer;)Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;

    move-result-object p1

    const/4 p2, 0x1

    .line 98
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->sort_ascending(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;

    move-result-object p1

    .line 99
    invoke-virtual {p1, p4}, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->state_filter(Lcom/squareup/protos/client/invoice/StateFilter;)Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;

    move-result-object p1

    .line 100
    invoke-virtual {p1, p5}, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;

    move-result-object p1

    .line 101
    invoke-virtual {p1, p6}, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->parent_series_token(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;

    move-result-object p1

    .line 102
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->build()Lcom/squareup/protos/client/invoice/ListInvoicesRequest;

    move-result-object p1

    .line 104
    iget-object p2, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->service:Lcom/squareup/server/invoices/ClientInvoiceService;

    invoke-interface {p2, p1}, Lcom/squareup/server/invoices/ClientInvoiceService;->list(Lcom/squareup/protos/client/invoice/ListInvoicesRequest;)Lrx/Observable;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->mainScheduler:Lrx/Scheduler;

    invoke-virtual {p1, p2}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public listRecurringSeries(Ljava/lang/String;Ljava/lang/String;ILcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/ListRecurringSeriesResponse;",
            ">;"
        }
    .end annotation

    .line 221
    new-instance v0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;-><init>()V

    .line 222
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->query(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;

    move-result-object p1

    .line 223
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->paging_key(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;

    move-result-object p1

    .line 224
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->limit(Ljava/lang/Integer;)Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;

    move-result-object p1

    .line 225
    invoke-virtual {p1, p4}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->state_filter(Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;)Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;

    move-result-object p1

    .line 226
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->build()Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;

    move-result-object p1

    .line 228
    iget-object p2, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->service:Lcom/squareup/server/invoices/ClientInvoiceService;

    invoke-interface {p2, p1}, Lcom/squareup/server/invoices/ClientInvoiceService;->listRecurring(Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;)Lrx/Observable;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->mainScheduler:Lrx/Scheduler;

    invoke-virtual {p1, p2}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public recordPayment(Lcom/squareup/invoices/RecordPaymentInfo;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/RecordPaymentInfo;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandResponse;",
            ">;>;"
        }
    .end annotation

    .line 278
    new-instance v0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;-><init>()V

    new-instance v1, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    .line 280
    invoke-virtual {p1}, Lcom/squareup/invoices/RecordPaymentInfo;->getInvoiceToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/IdPair$Builder;->server_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object v1

    .line 281
    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    .line 279
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;->id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;

    move-result-object v0

    .line 282
    invoke-virtual {p1}, Lcom/squareup/invoices/RecordPaymentInfo;->getPaymentMethod()Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;->getTenderType()Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;->tender_type(Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;)Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;

    move-result-object v0

    .line 283
    invoke-virtual {p1}, Lcom/squareup/invoices/RecordPaymentInfo;->getNote()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;->tender_note(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;

    move-result-object v0

    .line 284
    invoke-virtual {p1}, Lcom/squareup/invoices/RecordPaymentInfo;->getSendReceipt()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;->send_email_to_recipients(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;

    move-result-object v0

    .line 285
    invoke-virtual {p1}, Lcom/squareup/invoices/RecordPaymentInfo;->getInvoiceVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;->version(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;

    move-result-object v0

    .line 286
    invoke-virtual {p1}, Lcom/squareup/invoices/RecordPaymentInfo;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;->payment_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;

    move-result-object p1

    .line 287
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;->build()Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;

    move-result-object p1

    .line 289
    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->service:Lcom/squareup/server/invoices/ClientInvoiceService;

    invoke-interface {v0, p1}, Lcom/squareup/server/invoices/ClientInvoiceService;->recordPayment(Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->mainScheduler:Lrx/Scheduler;

    .line 290
    invoke-virtual {p1, v0}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    sget-object v1, Lcom/squareup/invoices/-$$Lambda$ClientInvoiceServiceHelper$fh75jLYUz780-UaH0BIl8y0uB3g;->INSTANCE:Lcom/squareup/invoices/-$$Lambda$ClientInvoiceServiceHelper$fh75jLYUz780-UaH0BIl8y0uB3g;

    .line 291
    invoke-static {v0, v1}, Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public saveDraftInvoice(Lcom/squareup/protos/client/invoice/Invoice;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/SaveDraftInvoiceResponse;",
            ">;"
        }
    .end annotation

    .line 132
    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->service:Lcom/squareup/server/invoices/ClientInvoiceService;

    new-instance v1, Lcom/squareup/protos/client/invoice/SaveDraftInvoiceRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/invoice/SaveDraftInvoiceRequest$Builder;-><init>()V

    .line 134
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/invoice/SaveDraftInvoiceRequest$Builder;->invoice(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/SaveDraftInvoiceRequest$Builder;

    move-result-object p1

    .line 135
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/SaveDraftInvoiceRequest$Builder;->build()Lcom/squareup/protos/client/invoice/SaveDraftInvoiceRequest;

    move-result-object p1

    .line 132
    invoke-interface {v0, p1}, Lcom/squareup/server/invoices/ClientInvoiceService;->saveDraftInvoice(Lcom/squareup/protos/client/invoice/SaveDraftInvoiceRequest;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->mainScheduler:Lrx/Scheduler;

    .line 136
    invoke-virtual {p1, v0}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public saveRecurringDraft(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            "Lcom/squareup/invoices/workflow/edit/RecurrenceRule;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesResponse;",
            ">;"
        }
    .end annotation

    .line 197
    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->service:Lcom/squareup/server/invoices/ClientInvoiceService;

    new-instance v1, Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesRequest$Builder;-><init>()V

    new-instance v2, Lcom/squareup/protos/client/invoice/RecurringSeries$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/invoice/RecurringSeries$Builder;-><init>()V

    .line 200
    invoke-virtual {v2, p1}, Lcom/squareup/protos/client/invoice/RecurringSeries$Builder;->template(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/RecurringSeries$Builder;

    move-result-object v2

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 201
    invoke-virtual {p2, p1}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->toRecurringSchedule(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/RecurringSchedule;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/squareup/protos/client/invoice/RecurringSeries$Builder;->recurrence_schedule(Lcom/squareup/protos/client/invoice/RecurringSchedule;)Lcom/squareup/protos/client/invoice/RecurringSeries$Builder;

    move-result-object p1

    .line 202
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/RecurringSeries$Builder;->build()Lcom/squareup/protos/client/invoice/RecurringSeries;

    move-result-object p1

    .line 199
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesRequest$Builder;->recurring_invoice(Lcom/squareup/protos/client/invoice/RecurringSeries;)Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesRequest$Builder;

    move-result-object p1

    .line 203
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesRequest$Builder;->build()Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesRequest;

    move-result-object p1

    .line 197
    invoke-interface {v0, p1}, Lcom/squareup/server/invoices/ClientInvoiceService;->saveRecurringDraft(Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesRequest;)Lrx/Observable;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->mainScheduler:Lrx/Scheduler;

    .line 204
    invoke-virtual {p1, p2}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public scheduleRecurringSeries(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/protos/client/invoice/RecurringSchedule;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            "Lcom/squareup/protos/client/invoice/RecurringSchedule;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesResponse;",
            ">;"
        }
    .end annotation

    .line 209
    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->service:Lcom/squareup/server/invoices/ClientInvoiceService;

    new-instance v1, Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesRequest$Builder;-><init>()V

    new-instance v2, Lcom/squareup/protos/client/invoice/RecurringSeries$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/invoice/RecurringSeries$Builder;-><init>()V

    .line 212
    invoke-virtual {v2, p1}, Lcom/squareup/protos/client/invoice/RecurringSeries$Builder;->template(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/RecurringSeries$Builder;

    move-result-object p1

    .line 213
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/invoice/RecurringSeries$Builder;->recurrence_schedule(Lcom/squareup/protos/client/invoice/RecurringSchedule;)Lcom/squareup/protos/client/invoice/RecurringSeries$Builder;

    move-result-object p1

    .line 214
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/RecurringSeries$Builder;->build()Lcom/squareup/protos/client/invoice/RecurringSeries;

    move-result-object p1

    .line 211
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesRequest$Builder;->recurring_invoice(Lcom/squareup/protos/client/invoice/RecurringSeries;)Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesRequest$Builder;

    move-result-object p1

    .line 215
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesRequest$Builder;->build()Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesRequest;

    move-result-object p1

    .line 209
    invoke-interface {v0, p1}, Lcom/squareup/server/invoices/ClientInvoiceService;->scheduleRecurring(Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesRequest;)Lrx/Observable;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->mainScheduler:Lrx/Scheduler;

    .line 216
    invoke-virtual {p1, p2}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public sendOrSchedule(Lcom/squareup/protos/client/invoice/Invoice;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceResponse;",
            ">;"
        }
    .end annotation

    .line 116
    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->service:Lcom/squareup/server/invoices/ClientInvoiceService;

    new-instance v1, Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceRequest$Builder;-><init>()V

    .line 118
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceRequest$Builder;->invoice(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceRequest$Builder;

    move-result-object p1

    .line 119
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceRequest$Builder;->build()Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceRequest;

    move-result-object p1

    .line 116
    invoke-interface {v0, p1}, Lcom/squareup/server/invoices/ClientInvoiceService;->sendOrSchedule(Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceRequest;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->mainScheduler:Lrx/Scheduler;

    .line 120
    invoke-virtual {p1, v0}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public sendReminder(Lcom/squareup/protos/client/IdPair;Ljava/lang/String;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/IdPair;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/invoice/SendInvoiceReminderResponse;",
            ">;>;"
        }
    .end annotation

    .line 142
    new-instance v0, Lcom/squareup/protos/client/invoice/SendInvoiceReminderRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/SendInvoiceReminderRequest$Builder;-><init>()V

    .line 143
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/SendInvoiceReminderRequest$Builder;->id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/invoice/SendInvoiceReminderRequest$Builder;

    move-result-object p1

    .line 144
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/invoice/SendInvoiceReminderRequest$Builder;->reminder_message(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/SendInvoiceReminderRequest$Builder;

    move-result-object p1

    .line 145
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/SendInvoiceReminderRequest$Builder;->build()Lcom/squareup/protos/client/invoice/SendInvoiceReminderRequest;

    move-result-object p1

    .line 147
    iget-object p2, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->service:Lcom/squareup/server/invoices/ClientInvoiceService;

    invoke-interface {p2, p1}, Lcom/squareup/server/invoices/ClientInvoiceService;->sendReminder(Lcom/squareup/protos/client/invoice/SendInvoiceReminderRequest;)Lrx/Observable;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->mainScheduler:Lrx/Scheduler;

    .line 148
    invoke-virtual {p1, p2}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    sget-object v0, Lcom/squareup/invoices/-$$Lambda$ClientInvoiceServiceHelper$I4WTg4gIUGjkIeszbAP7sdGkbmQ;->INSTANCE:Lcom/squareup/invoices/-$$Lambda$ClientInvoiceServiceHelper$I4WTg4gIUGjkIeszbAP7sdGkbmQ;

    .line 149
    invoke-static {p2, v0}, Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public submitFeedback(ILjava/lang/String;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/feedback/CreateFeedbackResponse;",
            ">;"
        }
    .end annotation

    .line 269
    new-instance v0, Lcom/squareup/protos/client/feedback/CreateFeedbackRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/feedback/CreateFeedbackRequest$Builder;-><init>()V

    .line 270
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/feedback/CreateFeedbackRequest$Builder;->rating(Ljava/lang/Integer;)Lcom/squareup/protos/client/feedback/CreateFeedbackRequest$Builder;

    move-result-object p1

    .line 271
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/feedback/CreateFeedbackRequest$Builder;->feedback(Ljava/lang/String;)Lcom/squareup/protos/client/feedback/CreateFeedbackRequest$Builder;

    move-result-object p1

    .line 272
    invoke-virtual {p1}, Lcom/squareup/protos/client/feedback/CreateFeedbackRequest$Builder;->build()Lcom/squareup/protos/client/feedback/CreateFeedbackRequest;

    move-result-object p1

    .line 273
    iget-object p2, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->service:Lcom/squareup/server/invoices/ClientInvoiceService;

    invoke-interface {p2, p1}, Lcom/squareup/server/invoices/ClientInvoiceService;->submitFeedback(Lcom/squareup/protos/client/feedback/CreateFeedbackRequest;)Lrx/Observable;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->mainScheduler:Lrx/Scheduler;

    invoke-virtual {p1, p2}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public unarchiveInvoice(Lcom/squareup/protos/client/invoice/Invoice;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/invoice/UnarchiveInvoiceResponse;",
            ">;>;"
        }
    .end annotation

    .line 327
    new-instance v0, Lcom/squareup/protos/client/invoice/UnarchiveInvoiceRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/UnarchiveInvoiceRequest$Builder;-><init>()V

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    .line 328
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/UnarchiveInvoiceRequest$Builder;->id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/invoice/UnarchiveInvoiceRequest$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->version:Ljava/lang/String;

    .line 329
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/UnarchiveInvoiceRequest$Builder;->version(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/UnarchiveInvoiceRequest$Builder;

    move-result-object p1

    .line 330
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/UnarchiveInvoiceRequest$Builder;->build()Lcom/squareup/protos/client/invoice/UnarchiveInvoiceRequest;

    move-result-object p1

    .line 331
    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->service:Lcom/squareup/server/invoices/ClientInvoiceService;

    invoke-interface {v0, p1}, Lcom/squareup/server/invoices/ClientInvoiceService;->unarchiveInvoice(Lcom/squareup/protos/client/invoice/UnarchiveInvoiceRequest;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->mainScheduler:Lrx/Scheduler;

    .line 332
    invoke-virtual {p1, v0}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    sget-object v1, Lcom/squareup/invoices/-$$Lambda$ClientInvoiceServiceHelper$bzoPiLz5tQaZ3fMIraObcG4HjCY;->INSTANCE:Lcom/squareup/invoices/-$$Lambda$ClientInvoiceServiceHelper$bzoPiLz5tQaZ3fMIraObcG4HjCY;

    .line 333
    invoke-static {v0, v1}, Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p1

    .line 334
    invoke-virtual {p1}, Lrx/Observable;->toSingle()Lrx/Single;

    move-result-object p1

    .line 331
    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Single(Lrx/Single;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public updateNotificationSettings(Ljava/util/List;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/invoice/v2/notifications/NotificationSettingUpdate;",
            ">;)",
            "Lrx/Observable<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsResponse;",
            ">;>;"
        }
    .end annotation

    .line 305
    new-instance v0, Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsRequest$Builder;-><init>()V

    .line 306
    invoke-virtual {v0, p1}, Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsRequest$Builder;->setting_update(Ljava/util/List;)Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsRequest$Builder;

    move-result-object p1

    .line 307
    invoke-virtual {p1}, Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsRequest$Builder;->build()Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsRequest;

    move-result-object p1

    .line 308
    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->service:Lcom/squareup/server/invoices/ClientInvoiceService;

    invoke-interface {v0, p1}, Lcom/squareup/server/invoices/ClientInvoiceService;->updateNotificationSettings(Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsRequest;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->mainScheduler:Lrx/Scheduler;

    .line 309
    invoke-virtual {p1, v0}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    sget-object v1, Lcom/squareup/invoices/-$$Lambda$ClientInvoiceServiceHelper$T2w84LK6PG22bFaBQRslPLaksXo;->INSTANCE:Lcom/squareup/invoices/-$$Lambda$ClientInvoiceServiceHelper$T2w84LK6PG22bFaBQRslPLaksXo;

    .line 310
    invoke-static {v0, v1}, Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public updateUnitMetadata(Lcom/squareup/protos/client/invoice/UnitMetadata;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/UnitMetadata;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/UpdateUnitMetadataResponse;",
            ">;"
        }
    .end annotation

    .line 189
    new-instance v0, Lcom/squareup/protos/client/invoice/UpdateUnitMetadataRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/UpdateUnitMetadataRequest$Builder;-><init>()V

    .line 190
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/UpdateUnitMetadataRequest$Builder;->metadata(Lcom/squareup/protos/client/invoice/UnitMetadata;)Lcom/squareup/protos/client/invoice/UpdateUnitMetadataRequest$Builder;

    move-result-object p1

    .line 191
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/UpdateUnitMetadataRequest$Builder;->build()Lcom/squareup/protos/client/invoice/UpdateUnitMetadataRequest;

    move-result-object p1

    .line 192
    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->service:Lcom/squareup/server/invoices/ClientInvoiceService;

    invoke-interface {v0, p1}, Lcom/squareup/server/invoices/ClientInvoiceService;->updateUnitMetadata(Lcom/squareup/protos/client/invoice/UpdateUnitMetadataRequest;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->mainScheduler:Lrx/Scheduler;

    invoke-virtual {p1, v0}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
