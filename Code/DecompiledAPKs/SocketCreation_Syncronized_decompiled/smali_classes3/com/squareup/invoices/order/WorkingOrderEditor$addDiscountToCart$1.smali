.class final Lcom/squareup/invoices/order/WorkingOrderEditor$addDiscountToCart$1;
.super Lkotlin/jvm/internal/Lambda;
.source "WorkingOrderEditor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/order/WorkingOrderEditor;->addDiscountToCart(Lcom/squareup/checkout/Discount;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/payment/Order;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/payment/Order;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $discount:Lcom/squareup/checkout/Discount;


# direct methods
.method constructor <init>(Lcom/squareup/checkout/Discount;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/order/WorkingOrderEditor$addDiscountToCart$1;->$discount:Lcom/squareup/checkout/Discount;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Lcom/squareup/payment/Order;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/order/WorkingOrderEditor$addDiscountToCart$1;->invoke(Lcom/squareup/payment/Order;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/payment/Order;)V
    .locals 1

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/squareup/invoices/order/WorkingOrderEditor$addDiscountToCart$1;->$discount:Lcom/squareup/checkout/Discount;

    invoke-virtual {p1, v0}, Lcom/squareup/payment/Order;->addDiscount(Lcom/squareup/checkout/Discount;)V

    return-void
.end method
