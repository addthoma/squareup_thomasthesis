.class public final Lcom/squareup/invoices/InvoiceUrlHelper;
.super Ljava/lang/Object;
.source "InvoiceUrlHelper.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/InvoiceUrlHelper$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 \u00152\u00020\u0001:\u0001\u0015B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0006\u0010\u000b\u001a\u00020\u000cJ\u0016\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012J\u0016\u0010\u0013\u001a\u00020\u00102\u0006\u0010\u0014\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/invoices/InvoiceUrlHelper;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "server",
        "Lcom/squareup/http/Server;",
        "application",
        "Landroid/app/Application;",
        "intentAvailabilityManager",
        "Lcom/squareup/util/IntentAvailabilityManager;",
        "(Lcom/squareup/util/Res;Lcom/squareup/http/Server;Landroid/app/Application;Lcom/squareup/util/IntentAvailabilityManager;)V",
        "canShareInvoiceUrl",
        "",
        "copyUrlToClipboard",
        "",
        "invoiceServerId",
        "",
        "urlType",
        "Lcom/squareup/url/UrlType;",
        "invoiceUrl",
        "serverId",
        "Companion",
        "invoices_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/InvoiceUrlHelper$Companion;


# instance fields
.field private final application:Landroid/app/Application;

.field private final intentAvailabilityManager:Lcom/squareup/util/IntentAvailabilityManager;

.field private final res:Lcom/squareup/util/Res;

.field private final server:Lcom/squareup/http/Server;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/InvoiceUrlHelper$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/InvoiceUrlHelper$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/InvoiceUrlHelper;->Companion:Lcom/squareup/invoices/InvoiceUrlHelper$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/http/Server;Landroid/app/Application;Lcom/squareup/util/IntentAvailabilityManager;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "server"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "application"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "intentAvailabilityManager"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/InvoiceUrlHelper;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/invoices/InvoiceUrlHelper;->server:Lcom/squareup/http/Server;

    iput-object p3, p0, Lcom/squareup/invoices/InvoiceUrlHelper;->application:Landroid/app/Application;

    iput-object p4, p0, Lcom/squareup/invoices/InvoiceUrlHelper;->intentAvailabilityManager:Lcom/squareup/util/IntentAvailabilityManager;

    return-void
.end method

.method public static final shareInvoiceIntent()Landroid/content/Intent;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/invoices/InvoiceUrlHelper;->Companion:Lcom/squareup/invoices/InvoiceUrlHelper$Companion;

    invoke-virtual {v0}, Lcom/squareup/invoices/InvoiceUrlHelper$Companion;->shareInvoiceIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final canShareInvoiceUrl()Z
    .locals 2

    .line 61
    iget-object v0, p0, Lcom/squareup/invoices/InvoiceUrlHelper;->intentAvailabilityManager:Lcom/squareup/util/IntentAvailabilityManager;

    sget-object v1, Lcom/squareup/invoices/InvoiceUrlHelper;->Companion:Lcom/squareup/invoices/InvoiceUrlHelper$Companion;

    invoke-virtual {v1}, Lcom/squareup/invoices/InvoiceUrlHelper$Companion;->shareInvoiceIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/util/IntentAvailabilityManager;->isAvailable(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method public final copyUrlToClipboard(Ljava/lang/String;Lcom/squareup/url/UrlType;)V
    .locals 1

    const-string v0, "invoiceServerId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "urlType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/InvoiceUrlHelper;->invoiceUrl(Ljava/lang/String;Lcom/squareup/url/UrlType;)Ljava/lang/String;

    move-result-object p1

    .line 72
    iget-object p2, p0, Lcom/squareup/invoices/InvoiceUrlHelper;->application:Landroid/app/Application;

    const-string v0, "clipboard"

    invoke-virtual {p2, v0}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    if-eqz p2, :cond_0

    check-cast p2, Landroid/content/ClipboardManager;

    const-string v0, "invoice url"

    .line 73
    check-cast v0, Ljava/lang/CharSequence;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {v0, p1}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object p1

    .line 74
    invoke-virtual {p2, p1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    return-void

    .line 72
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type android.content.ClipboardManager"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final invoiceUrl(Ljava/lang/String;Lcom/squareup/url/UrlType;)Ljava/lang/String;
    .locals 2

    const-string v0, "serverId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "urlType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/invoices/InvoiceUrlHelper;->server:Lcom/squareup/http/Server;

    invoke-virtual {v0}, Lcom/squareup/http/Server;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/http/Endpoints;->getServerFromUrl(Ljava/lang/String;)Lcom/squareup/http/Endpoints$Server;

    move-result-object v0

    .line 43
    sget-object v1, Lcom/squareup/http/Endpoints$Server;->PRODUCTION:Lcom/squareup/http/Endpoints$Server;

    if-ne v0, v1, :cond_0

    .line 44
    iget-object v0, p0, Lcom/squareup/invoices/InvoiceUrlHelper;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/invoices/R$string;->invoice_prod_url_pay_page:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/InvoiceUrlHelper;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/invoices/R$string;->invoice_staging_url_pay_page:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 48
    :goto_0
    sget-object v1, Lcom/squareup/invoices/InvoiceUrlHelper$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p2}, Lcom/squareup/url/UrlType;->ordinal()I

    move-result p2

    aget p2, v1, p2

    const/4 v1, 0x1

    if-eq p2, v1, :cond_2

    const/4 v1, 0x2

    if-eq p2, v1, :cond_1

    const/4 v1, 0x3

    goto :goto_1

    .line 50
    :cond_1
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "estimate/"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 49
    :cond_2
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "series/"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 55
    :goto_1
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
