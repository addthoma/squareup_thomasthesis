.class public final Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial_Factory;
.super Ljava/lang/Object;
.source "FirstInvoiceTutorial_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;",
        ">;"
    }
.end annotation


# instance fields
.field private final badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;"
        }
    .end annotation
.end field

.field private final cacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final firstInvoiceTutorialViewedProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final helpBadgeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpBadge;",
            ">;"
        }
    .end annotation
.end field

.field private final invoicesAppletRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpBadge;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;)V"
        }
    .end annotation

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial_Factory;->presenterProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p2, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial_Factory;->invoicesAppletRunnerProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p3, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial_Factory;->cacheProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p4, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial_Factory;->resProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p5, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p6, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial_Factory;->helpBadgeProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p7, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial_Factory;->firstInvoiceTutorialViewedProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p8, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial_Factory;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial_Factory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpBadge;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;)",
            "Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial_Factory;"
        }
    .end annotation

    .line 69
    new-instance v9, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial_Factory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static newInstance(Lcom/squareup/register/tutorial/TutorialPresenter;Ldagger/Lazy;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/help/HelpBadge;Lcom/squareup/settings/LocalSetting;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            "Ldagger/Lazy<",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ">;",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/ui/help/HelpBadge;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ")",
            "Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;"
        }
    .end annotation

    .line 76
    new-instance v9, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;-><init>(Lcom/squareup/register/tutorial/TutorialPresenter;Ldagger/Lazy;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/help/HelpBadge;Lcom/squareup/settings/LocalSetting;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)V

    return-object v9
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;
    .locals 9

    .line 60
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial_Factory;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/register/tutorial/TutorialPresenter;

    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial_Factory;->invoicesAppletRunnerProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v2

    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial_Factory;->cacheProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial_Factory;->helpBadgeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/help/HelpBadge;

    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial_Factory;->firstInvoiceTutorialViewedProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/settings/LocalSetting;

    iget-object v0, p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial_Factory;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    invoke-static/range {v1 .. v8}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial_Factory;->newInstance(Lcom/squareup/register/tutorial/TutorialPresenter;Ldagger/Lazy;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/help/HelpBadge;Lcom/squareup/settings/LocalSetting;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial_Factory;->get()Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;

    move-result-object v0

    return-object v0
.end method
