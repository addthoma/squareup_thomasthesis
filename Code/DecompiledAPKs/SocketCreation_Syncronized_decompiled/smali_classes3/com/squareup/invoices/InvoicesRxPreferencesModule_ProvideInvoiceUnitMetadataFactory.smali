.class public final Lcom/squareup/invoices/InvoicesRxPreferencesModule_ProvideInvoiceUnitMetadataFactory;
.super Ljava/lang/Object;
.source "InvoicesRxPreferencesModule_ProvideInvoiceUnitMetadataFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/f2prateek/rx/preferences2/Preference<",
        "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final preferencesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/invoices/InvoicesRxPreferencesModule_ProvideInvoiceUnitMetadataFactory;->preferencesProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/invoices/InvoicesRxPreferencesModule_ProvideInvoiceUnitMetadataFactory;->resProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/invoices/InvoicesRxPreferencesModule_ProvideInvoiceUnitMetadataFactory;->currencyCodeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/InvoicesRxPreferencesModule_ProvideInvoiceUnitMetadataFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)",
            "Lcom/squareup/invoices/InvoicesRxPreferencesModule_ProvideInvoiceUnitMetadataFactory;"
        }
    .end annotation

    .line 44
    new-instance v0, Lcom/squareup/invoices/InvoicesRxPreferencesModule_ProvideInvoiceUnitMetadataFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/invoices/InvoicesRxPreferencesModule_ProvideInvoiceUnitMetadataFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideInvoiceUnitMetadata(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/squareup/util/Res;Lcom/squareup/protos/common/CurrencyCode;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;",
            ">;"
        }
    .end annotation

    .line 49
    invoke-static {p0, p1, p2}, Lcom/squareup/invoices/InvoicesRxPreferencesModule;->provideInvoiceUnitMetadata(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/squareup/util/Res;Lcom/squareup/protos/common/CurrencyCode;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/f2prateek/rx/preferences2/Preference;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/f2prateek/rx/preferences2/Preference;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;",
            ">;"
        }
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/squareup/invoices/InvoicesRxPreferencesModule_ProvideInvoiceUnitMetadataFactory;->preferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    iget-object v1, p0, Lcom/squareup/invoices/InvoicesRxPreferencesModule_ProvideInvoiceUnitMetadataFactory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/invoices/InvoicesRxPreferencesModule_ProvideInvoiceUnitMetadataFactory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, v2}, Lcom/squareup/invoices/InvoicesRxPreferencesModule_ProvideInvoiceUnitMetadataFactory;->provideInvoiceUnitMetadata(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/squareup/util/Res;Lcom/squareup/protos/common/CurrencyCode;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/invoices/InvoicesRxPreferencesModule_ProvideInvoiceUnitMetadataFactory;->get()Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    return-object v0
.end method
