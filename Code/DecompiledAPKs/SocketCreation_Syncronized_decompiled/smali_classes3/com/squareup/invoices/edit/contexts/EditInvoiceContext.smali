.class public abstract Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;
.super Ljava/lang/Object;
.source "EditInvoiceContext.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewSingleInvoice;,
        Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewRecurringSeries;,
        Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;,
        Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftSingleInvoice;,
        Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditRecurringSeries;,
        Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftRecurringSeries;,
        Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DuplicatedSingleInvoice;,
        Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\\\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0011\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 !2\u00020\u0001:\u0008\u001c\u001d\u001e\u001f !\"#B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J&\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cJ\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eJ\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0010J\u0006\u0010\u0011\u001a\u00020\u0012J\u0006\u0010\u0013\u001a\u00020\u0012J\u0006\u0010\u0014\u001a\u00020\u0012J\u0006\u0010\u0015\u001a\u00020\u0012J\u0006\u0010\u0016\u001a\u00020\u0012J\u0006\u0010\u0017\u001a\u00020\u0012J\u0006\u0010\u0018\u001a\u00020\u0012J\u0006\u0010\u0019\u001a\u00020\u0012J\u0006\u0010\u001a\u001a\u00020\u0012J\u0006\u0010\u001b\u001a\u00020\u0012\u0082\u0001\u0007$%&\'()*\u00a8\u0006+"
    }
    d2 = {
        "Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;",
        "",
        "()V",
        "constructInitialTemplateToEdit",
        "Lcom/squareup/protos/client/invoice/Invoice$Builder;",
        "creationContext",
        "Lcom/squareup/invoices/edit/contexts/InvoiceCreationContext;",
        "defaultMessage",
        "",
        "invoiceDefaults",
        "Lcom/squareup/protos/client/invoice/InvoiceDefaults;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "getCurrentDisplayDetails",
        "Lcom/squareup/invoices/DisplayDetails;",
        "getInitialRecurrenceRule",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceRule;",
        "isDraft",
        "",
        "isDuplicateInvoice",
        "isEditingDraftSeries",
        "isEditingNonDraftSentOrSharedSingleInvoice",
        "isEditingNonDraftSeries",
        "isEditingNonDraftSingleInvoice",
        "isEditingSeries",
        "isEditingSingleInvoice",
        "isEditingSingleInvoiceInRecurringSeries",
        "isNew",
        "DraftRecurringSeries",
        "DraftSingleInvoice",
        "DuplicatedSingleInvoice",
        "EditRecurringSeries",
        "EditSingleInvoice",
        "Factory",
        "NewRecurringSeries",
        "NewSingleInvoice",
        "Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewSingleInvoice;",
        "Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewRecurringSeries;",
        "Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;",
        "Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftSingleInvoice;",
        "Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditRecurringSeries;",
        "Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftRecurringSeries;",
        "Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DuplicatedSingleInvoice;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Factory:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$Factory;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$Factory;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$Factory;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->Factory:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$Factory;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;-><init>()V

    return-void
.end method

.method public static final duplicateSingleInvoiceContext(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->Factory:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$Factory;

    invoke-virtual {v0, p0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$Factory;->duplicateSingleInvoiceContext(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    move-result-object p0

    return-object p0
.end method

.method public static final editExistingInvoiceContext(Lcom/squareup/invoices/DisplayDetails;)Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->Factory:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$Factory;

    invoke-virtual {v0, p0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$Factory;->editExistingInvoiceContext(Lcom/squareup/invoices/DisplayDetails;)Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    move-result-object p0

    return-object p0
.end method

.method public static final newRecurringSeriesContext()Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewRecurringSeries;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->Factory:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$Factory;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$Factory;->newRecurringSeriesContext()Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewRecurringSeries;

    move-result-object v0

    return-object v0
.end method

.method public static final newSingleInvoiceContext()Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewSingleInvoice;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->Factory:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$Factory;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$Factory;->newSingleInvoiceContext()Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewSingleInvoice;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final constructInitialTemplateToEdit(Lcom/squareup/invoices/edit/contexts/InvoiceCreationContext;Ljava/lang/String;Lcom/squareup/protos/client/invoice/InvoiceDefaults;Lcom/squareup/settings/server/Features;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 1

    const-string v0, "creationContext"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultMessage"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceDefaults"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    sget-object v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewSingleInvoice;->INSTANCE:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewSingleInvoice;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewRecurringSeries;->INSTANCE:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewRecurringSeries;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 118
    :goto_0
    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_INVOICE_DEFAULTS_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p4, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p4

    if-eqz p4, :cond_1

    .line 119
    invoke-static {p3, p1}, Lcom/squareup/invoices/edit/contexts/InvoiceCreationContextKt;->constructNewTemplate(Lcom/squareup/protos/client/invoice/InvoiceDefaults;Lcom/squareup/invoices/edit/contexts/InvoiceCreationContext;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p1

    goto :goto_1

    .line 121
    :cond_1
    invoke-static {p1, p2}, Lcom/squareup/invoices/edit/contexts/InvoiceCreationContextKt;->constructNewTemplate(Lcom/squareup/invoices/edit/contexts/InvoiceCreationContext;Ljava/lang/String;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p1

    goto :goto_1

    .line 124
    :cond_2
    instance-of p2, p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;

    const-string p3, "invoiceDisplayDetails.invoice.newBuilder()"

    if-eqz p2, :cond_3

    move-object p1, p0

    check-cast p1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;

    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;->getInvoiceDisplayDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/Invoice;->newBuilder()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p1

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 125
    :cond_3
    instance-of p2, p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftSingleInvoice;

    if-eqz p2, :cond_4

    move-object p1, p0

    check-cast p1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftSingleInvoice;

    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftSingleInvoice;->getInvoiceDisplayDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/Invoice;->newBuilder()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p1

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 126
    :cond_4
    instance-of p2, p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditRecurringSeries;

    if-eqz p2, :cond_5

    .line 128
    move-object p1, p0

    check-cast p1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditRecurringSeries;

    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditRecurringSeries;->getRecurringSeriesDisplayDetails()Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    move-result-object p1

    .line 127
    invoke-static {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContextKt;->access$createInvoiceTemplateFromRecurringSeriesDisplayDetails(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p1

    goto :goto_1

    .line 131
    :cond_5
    instance-of p2, p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftRecurringSeries;

    if-eqz p2, :cond_6

    .line 133
    move-object p1, p0

    check-cast p1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftRecurringSeries;

    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftRecurringSeries;->getRecurringSeriesDisplayDetails()Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    move-result-object p1

    .line 132
    invoke-static {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContextKt;->access$createInvoiceTemplateFromRecurringSeriesDisplayDetails(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p1

    goto :goto_1

    .line 136
    :cond_6
    instance-of p2, p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DuplicatedSingleInvoice;

    if-eqz p2, :cond_7

    .line 138
    move-object p2, p0

    check-cast p2, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DuplicatedSingleInvoice;

    invoke-virtual {p2}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DuplicatedSingleInvoice;->getInvoiceDisplayDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p2

    .line 137
    invoke-static {p2, p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContextKt;->access$createInvoiceTemplateFromDuplicatedInvoice(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/invoices/edit/contexts/InvoiceCreationContext;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p1

    :goto_1
    return-object p1

    :cond_7
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public final getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;
    .locals 2

    .line 144
    sget-object v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewSingleInvoice;->INSTANCE:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewSingleInvoice;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewRecurringSeries;->INSTANCE:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewRecurringSeries;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_0
    const/4 v0, 0x0

    goto :goto_1

    .line 145
    :cond_1
    instance-of v0, p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    move-object v1, p0

    check-cast v1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;

    invoke-virtual {v1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;->getInvoiceDisplayDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/invoices/DisplayDetails$Invoice;-><init>(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    check-cast v0, Lcom/squareup/invoices/DisplayDetails;

    goto :goto_1

    .line 146
    :cond_2
    instance-of v0, p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftSingleInvoice;

    if-eqz v0, :cond_3

    new-instance v0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    move-object v1, p0

    check-cast v1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftSingleInvoice;

    invoke-virtual {v1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftSingleInvoice;->getInvoiceDisplayDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/invoices/DisplayDetails$Invoice;-><init>(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    check-cast v0, Lcom/squareup/invoices/DisplayDetails;

    goto :goto_1

    .line 147
    :cond_3
    instance-of v0, p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditRecurringSeries;

    if-eqz v0, :cond_4

    new-instance v0, Lcom/squareup/invoices/DisplayDetails$Recurring;

    .line 148
    move-object v1, p0

    check-cast v1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditRecurringSeries;

    invoke-virtual {v1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditRecurringSeries;->getRecurringSeriesDisplayDetails()Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    move-result-object v1

    .line 147
    invoke-direct {v0, v1}, Lcom/squareup/invoices/DisplayDetails$Recurring;-><init>(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)V

    check-cast v0, Lcom/squareup/invoices/DisplayDetails;

    goto :goto_1

    .line 150
    :cond_4
    instance-of v0, p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftRecurringSeries;

    if-eqz v0, :cond_5

    new-instance v0, Lcom/squareup/invoices/DisplayDetails$Recurring;

    .line 151
    move-object v1, p0

    check-cast v1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftRecurringSeries;

    invoke-virtual {v1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftRecurringSeries;->getRecurringSeriesDisplayDetails()Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    move-result-object v1

    .line 150
    invoke-direct {v0, v1}, Lcom/squareup/invoices/DisplayDetails$Recurring;-><init>(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)V

    check-cast v0, Lcom/squareup/invoices/DisplayDetails;

    goto :goto_1

    .line 153
    :cond_5
    instance-of v0, p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DuplicatedSingleInvoice;

    if-eqz v0, :cond_6

    new-instance v0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    .line 154
    move-object v1, p0

    check-cast v1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DuplicatedSingleInvoice;

    invoke-virtual {v1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DuplicatedSingleInvoice;->getInvoiceDisplayDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object v1

    .line 153
    invoke-direct {v0, v1}, Lcom/squareup/invoices/DisplayDetails$Invoice;-><init>(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    check-cast v0, Lcom/squareup/invoices/DisplayDetails;

    :goto_1
    return-object v0

    :cond_6
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public final getInitialRecurrenceRule()Lcom/squareup/invoices/workflow/edit/RecurrenceRule;
    .locals 1

    .line 91
    instance-of v0, p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewSingleInvoice;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 92
    :cond_0
    instance-of v0, p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;

    if-eqz v0, :cond_1

    goto :goto_0

    .line 93
    :cond_1
    instance-of v0, p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftSingleInvoice;

    if-eqz v0, :cond_2

    goto :goto_0

    .line 94
    :cond_2
    instance-of v0, p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DuplicatedSingleInvoice;

    if-eqz v0, :cond_3

    :goto_0
    const/4 v0, 0x0

    goto :goto_1

    .line 95
    :cond_3
    instance-of v0, p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewRecurringSeries;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->Companion:Lcom/squareup/invoices/workflow/edit/RecurrenceRule$Companion;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule$Companion;->defaultRecurrenceRule()Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object v0

    goto :goto_1

    .line 96
    :cond_4
    instance-of v0, p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditRecurringSeries;

    if-eqz v0, :cond_5

    .line 98
    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditRecurringSeries;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditRecurringSeries;->getRecurringSeriesDisplayDetails()Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    move-result-object v0

    .line 97
    invoke-static {v0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContextKt;->access$getRRuleFromRecurringSeriesDisplayDetails(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object v0

    goto :goto_1

    .line 101
    :cond_5
    instance-of v0, p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftRecurringSeries;

    if-eqz v0, :cond_6

    .line 103
    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftRecurringSeries;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftRecurringSeries;->getRecurringSeriesDisplayDetails()Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    move-result-object v0

    .line 102
    invoke-static {v0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContextKt;->access$getRRuleFromRecurringSeriesDisplayDetails(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_6
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public final isDraft()Z
    .locals 1

    .line 88
    instance-of v0, p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftSingleInvoice;

    if-nez v0, :cond_1

    instance-of v0, p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftRecurringSeries;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public final isDuplicateInvoice()Z
    .locals 1

    .line 71
    instance-of v0, p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DuplicatedSingleInvoice;

    return v0
.end method

.method public final isEditingDraftSeries()Z
    .locals 1

    .line 69
    instance-of v0, p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftRecurringSeries;

    return v0
.end method

.method public final isEditingNonDraftSentOrSharedSingleInvoice()Z
    .locals 2

    .line 74
    instance-of v0, p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;

    if-eqz v0, :cond_0

    .line 75
    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;->getInvoiceDisplayDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    invoke-static {v0}, Lcom/squareup/invoices/InvoiceDisplayState;->forInvoiceDisplayState(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)Lcom/squareup/invoices/InvoiceDisplayState;

    move-result-object v0

    const-string v1, "forInvoiceDisplayState(i\u2026layDetails.display_state)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/invoices/InvoiceDisplayState;->isSentOrShared()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isEditingNonDraftSeries()Z
    .locals 1

    .line 65
    instance-of v0, p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditRecurringSeries;

    return v0
.end method

.method public final isEditingNonDraftSingleInvoice()Z
    .locals 1

    .line 67
    instance-of v0, p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;

    return v0
.end method

.method public final isEditingSeries()Z
    .locals 1

    .line 63
    instance-of v0, p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditRecurringSeries;

    if-nez v0, :cond_1

    instance-of v0, p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftRecurringSeries;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public final isEditingSingleInvoice()Z
    .locals 1

    .line 61
    instance-of v0, p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;

    if-nez v0, :cond_1

    instance-of v0, p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftSingleInvoice;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public final isEditingSingleInvoiceInRecurringSeries()Z
    .locals 1

    .line 81
    instance-of v0, p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;->getInvoiceDisplayDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->series_details:Lcom/squareup/protos/client/invoice/SeriesDetails;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isNew()Z
    .locals 1

    .line 84
    sget-object v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewSingleInvoice;->INSTANCE:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewSingleInvoice;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewRecurringSeries;->INSTANCE:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewRecurringSeries;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    instance-of v0, p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DuplicatedSingleInvoice;

    if-eqz v0, :cond_2

    :goto_0
    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    return v0
.end method
