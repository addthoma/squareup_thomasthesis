.class public final Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator_Factory;
.super Ljava/lang/Object;
.source "InvoicePreviewCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final invoiceTutorialRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/InvoiceTutorialRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Runner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/InvoiceTutorialRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator_Factory;->invoiceTutorialRunnerProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/InvoiceTutorialRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator_Factory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Runner;Lcom/squareup/register/tutorial/InvoiceTutorialRunner;Lcom/squareup/util/Res;)Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;
    .locals 1

    .line 44
    new-instance v0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;-><init>(Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Runner;Lcom/squareup/register/tutorial/InvoiceTutorialRunner;Lcom/squareup/util/Res;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;
    .locals 3

    .line 33
    iget-object v0, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Runner;

    iget-object v1, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator_Factory;->invoiceTutorialRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    iget-object v2, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Res;

    invoke-static {v0, v1, v2}, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator_Factory;->newInstance(Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Runner;Lcom/squareup/register/tutorial/InvoiceTutorialRunner;Lcom/squareup/util/Res;)Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator_Factory;->get()Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;

    move-result-object v0

    return-object v0
.end method
