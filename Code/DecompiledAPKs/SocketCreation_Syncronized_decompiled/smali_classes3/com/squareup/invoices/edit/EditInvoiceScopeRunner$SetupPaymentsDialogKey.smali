.class public final enum Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;
.super Ljava/lang/Enum;
.source "EditInvoiceScopeRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SetupPaymentsDialogKey"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;

.field public static final enum SAVE_DRAFT:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;

.field public static final enum SEND_INVOICE:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 2104
    new-instance v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;

    const/4 v1, 0x0

    const-string v2, "SAVE_DRAFT"

    invoke-direct {v0, v2, v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;->SAVE_DRAFT:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;

    .line 2105
    new-instance v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;

    const/4 v2, 0x1

    const-string v3, "SEND_INVOICE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;->SEND_INVOICE:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;

    .line 2103
    sget-object v3, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;->SAVE_DRAFT:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;->SEND_INVOICE:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;->$VALUES:[Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 2103
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;
    .locals 1

    .line 2103
    const-class v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;

    return-object p0
.end method

.method public static values()[Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;
    .locals 1

    .line 2103
    sget-object v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;->$VALUES:[Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;

    invoke-virtual {v0}, [Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;

    return-object v0
.end method
