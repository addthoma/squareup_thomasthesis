.class public final Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner;
.super Ljava/lang/Object;
.source "EditInvoiceLoadingScreenRunner.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/invoices/edit/EditInvoiceLoadingScreen;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016J\u0008\u0010\r\u001a\u00020\nH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner;",
        "Lmortar/Scoped;",
        "invoiceUnitCache",
        "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
        "flow",
        "Lflow/Flow;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lflow/Flow;Lcom/squareup/util/Res;)V",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final flow:Lflow/Flow;

.field private final invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lflow/Flow;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "invoiceUnitCache"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner;->invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    iput-object p2, p0, Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner;->flow:Lflow/Flow;

    iput-object p3, p0, Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public static final synthetic access$getFlow$p(Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner;)Lflow/Flow;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner;->flow:Lflow/Flow;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner;)Lcom/squareup/util/Res;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner;->res:Lcom/squareup/util/Res;

    return-object p0
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/edit/EditInvoiceLoadingScreen;

    .line 27
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner;->invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    .line 28
    invoke-interface {v1}, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;->forceGetSettings()Lio/reactivex/Single;

    move-result-object v1

    .line 29
    new-instance v2, Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner$onEnterScope$1;

    invoke-direct {v2, p0, v0}, Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner$onEnterScope$1;-><init>(Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner;Lcom/squareup/invoices/edit/EditInvoiceLoadingScreen;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "invoiceUnitCache\n       \u2026)\n            )\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
