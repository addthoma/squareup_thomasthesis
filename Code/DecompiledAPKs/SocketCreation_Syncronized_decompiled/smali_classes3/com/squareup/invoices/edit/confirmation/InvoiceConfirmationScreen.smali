.class public Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationScreen;
.super Lcom/squareup/invoices/edit/InEditInvoiceScope;
.source "InvoiceConfirmationScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final action:Lcom/squareup/invoices/edit/InvoiceAction;

.field public final errorDescription:Ljava/lang/CharSequence;

.field public final errorTitle:Ljava/lang/CharSequence;

.field public final isResponseSuccess:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 44
    sget-object v0, Lcom/squareup/invoices/edit/confirmation/-$$Lambda$InvoiceConfirmationScreen$gdz4e8UKK9fuALNo38FjECinN5M;->INSTANCE:Lcom/squareup/invoices/edit/confirmation/-$$Lambda$InvoiceConfirmationScreen$gdz4e8UKK9fuALNo38FjECinN5M;

    .line 45
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/invoices/edit/EditInvoiceScope;Lcom/squareup/invoices/edit/InvoiceAction;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/InEditInvoiceScope;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScope;)V

    .line 26
    iput-object p2, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationScreen;->action:Lcom/squareup/invoices/edit/InvoiceAction;

    .line 27
    iput-boolean p3, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationScreen;->isResponseSuccess:Z

    .line 28
    iput-object p4, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationScreen;->errorTitle:Ljava/lang/CharSequence;

    .line 29
    iput-object p5, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationScreen;->errorDescription:Ljava/lang/CharSequence;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationScreen;
    .locals 7

    .line 46
    const-class v0, Lcom/squareup/invoices/edit/EditInvoiceScope;

    .line 47
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/invoices/edit/EditInvoiceScope;

    .line 48
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/invoices/edit/InvoiceAction;->valueOf(Ljava/lang/String;)Lcom/squareup/invoices/edit/InvoiceAction;

    move-result-object v3

    .line 49
    new-instance p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationScreen;

    const/4 v4, 0x0

    const-string v5, ""

    const-string v6, ""

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationScreen;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScope;Lcom/squareup/invoices/edit/InvoiceAction;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-object p0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 39
    invoke-super {p0, p1, p2}, Lcom/squareup/invoices/edit/InEditInvoiceScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 40
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationScreen;->editInvoicePath:Lcom/squareup/invoices/edit/EditInvoiceScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 41
    iget-object p2, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationScreen;->action:Lcom/squareup/invoices/edit/InvoiceAction;

    invoke-virtual {p2}, Lcom/squareup/invoices/edit/InvoiceAction;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 53
    sget v0, Lcom/squareup/features/invoices/R$layout;->invoice_confirmation:I

    return v0
.end method
