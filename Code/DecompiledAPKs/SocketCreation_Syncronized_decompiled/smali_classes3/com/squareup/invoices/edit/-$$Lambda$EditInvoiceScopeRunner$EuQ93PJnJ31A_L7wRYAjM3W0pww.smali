.class public final synthetic Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$EuQ93PJnJ31A_L7wRYAjM3W0pww;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/main/RegisterTreeKey;

.field private final synthetic f$1:Lcom/squareup/configure/item/WorkingItem;

.field private final synthetic f$2:Z


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/configure/item/WorkingItem;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$EuQ93PJnJ31A_L7wRYAjM3W0pww;->f$0:Lcom/squareup/ui/main/RegisterTreeKey;

    iput-object p2, p0, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$EuQ93PJnJ31A_L7wRYAjM3W0pww;->f$1:Lcom/squareup/configure/item/WorkingItem;

    iput-boolean p3, p0, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$EuQ93PJnJ31A_L7wRYAjM3W0pww;->f$2:Z

    return-void
.end method


# virtual methods
.method public final call(Lflow/History;)Lcom/squareup/container/Command;
    .locals 3

    iget-object v0, p0, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$EuQ93PJnJ31A_L7wRYAjM3W0pww;->f$0:Lcom/squareup/ui/main/RegisterTreeKey;

    iget-object v1, p0, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$EuQ93PJnJ31A_L7wRYAjM3W0pww;->f$1:Lcom/squareup/configure/item/WorkingItem;

    iget-boolean v2, p0, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$EuQ93PJnJ31A_L7wRYAjM3W0pww;->f$2:Z

    invoke-static {v0, v1, v2, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->lambda$startEditingItemWithModifiers$13(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/configure/item/WorkingItem;ZLflow/History;)Lcom/squareup/container/Command;

    move-result-object p1

    return-object p1
.end method
