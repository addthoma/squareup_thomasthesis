.class public interface abstract Lcom/squareup/invoices/edit/EditInvoiceScope$Component;
.super Ljava/lang/Object;
.source "EditInvoiceScope.java"

# interfaces
.implements Lcom/squareup/configure/item/ConfigureItemScope$ParentComponent;
.implements Lcom/squareup/ui/crm/flow/CrmScope$ParentViewCustomerFromServicesComponent;
.implements Lcom/squareup/ui/crm/ChooseCustomerScope$ParentComponent;
.implements Lcom/squareup/invoices/edit/items/ItemSelectScreen$ParentComponent;
.implements Lcom/squareup/orderentry/KeypadEntryScreen$ParentComponent;
.implements Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$ParentComponent;
.implements Lcom/squareup/ui/tender/DaysOfWeekView$ParentComponent;
.implements Lcom/squareup/setupguide/SetupPaymentsDialog$ParentComponent;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/invoices/edit/EditInvoiceScope$EditInvoiceScopeModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/edit/EditInvoiceScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract automaticPaymentCoordinator()Lcom/squareup/invoices/edit/automaticpayments/AutomaticPaymentCoordinator;
.end method

.method public abstract editInvoiceScopeRunner()Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;
.end method

.method public abstract editInvoiceScreen()Lcom/squareup/invoices/edit/EditInvoiceScreen$Component;
.end method

.method public abstract invoiceConfirmationScreen()Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationScreen$Component;
.end method
