.class public Lcom/squareup/invoices/edit/RecurringMonthlyDialog;
.super Lcom/squareup/invoices/edit/InEditInvoiceScope;
.source "RecurringMonthlyDialog.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/invoices/edit/RecurringMonthlyDialog$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/edit/RecurringMonthlyDialog$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/invoices/edit/RecurringMonthlyDialog;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final action:Lcom/squareup/invoices/edit/InvoiceAction;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 59
    sget-object v0, Lcom/squareup/invoices/edit/-$$Lambda$RecurringMonthlyDialog$oOBVOwe5bgV9N_bDe6me3cRCcTk;->INSTANCE:Lcom/squareup/invoices/edit/-$$Lambda$RecurringMonthlyDialog$oOBVOwe5bgV9N_bDe6me3cRCcTk;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/invoices/edit/RecurringMonthlyDialog;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/invoices/edit/EditInvoiceScope;Lcom/squareup/invoices/edit/InvoiceAction;)V
    .locals 0

    .line 22
    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/InEditInvoiceScope;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScope;)V

    .line 23
    iput-object p2, p0, Lcom/squareup/invoices/edit/RecurringMonthlyDialog;->action:Lcom/squareup/invoices/edit/InvoiceAction;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/invoices/edit/RecurringMonthlyDialog;
    .locals 2

    .line 60
    const-class v0, Lcom/squareup/invoices/edit/EditInvoiceScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/edit/EditInvoiceScope;

    .line 61
    invoke-static {}, Lcom/squareup/invoices/edit/InvoiceAction;->values()[Lcom/squareup/invoices/edit/InvoiceAction;

    move-result-object v1

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    aget-object p0, v1, p0

    .line 63
    new-instance v1, Lcom/squareup/invoices/edit/RecurringMonthlyDialog;

    invoke-direct {v1, v0, p0}, Lcom/squareup/invoices/edit/RecurringMonthlyDialog;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScope;Lcom/squareup/invoices/edit/InvoiceAction;)V

    return-object v1
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 54
    invoke-super {p0, p1, p2}, Lcom/squareup/invoices/edit/InEditInvoiceScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 55
    iget-object v0, p0, Lcom/squareup/invoices/edit/RecurringMonthlyDialog;->editInvoicePath:Lcom/squareup/invoices/edit/EditInvoiceScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 56
    iget-object p2, p0, Lcom/squareup/invoices/edit/RecurringMonthlyDialog;->action:Lcom/squareup/invoices/edit/InvoiceAction;

    invoke-virtual {p2}, Lcom/squareup/invoices/edit/InvoiceAction;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public getInvoiceAction()Lcom/squareup/invoices/edit/InvoiceAction;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/invoices/edit/RecurringMonthlyDialog;->action:Lcom/squareup/invoices/edit/InvoiceAction;

    return-object v0
.end method
