.class public abstract Lcom/squareup/invoices/DisplayDetails;
.super Ljava/lang/Object;
.source "DisplayDetails.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/DisplayDetails$Invoice;,
        Lcom/squareup/invoices/DisplayDetails$Recurring;,
        Lcom/squareup/invoices/DisplayDetails$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \u001a2\u00020\u0001:\u0003\u001a\u001b\u001cB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0013\u001a\u00020\u0014H\u0016J\u0018\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0014H\u0016R\u0011\u0010\u0003\u001a\u00020\u00048F\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0011\u0010\u0007\u001a\u00020\u00088F\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u000b\u001a\u00020\u000c8F\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\rR\u0011\u0010\u000e\u001a\u00020\u000c8F\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\rR\u0011\u0010\u000f\u001a\u00020\u00108F\u00a2\u0006\u0006\u001a\u0004\u0008\u0011\u0010\u0012\u0082\u0001\u0002\u001d\u001e\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/invoices/DisplayDetails;",
        "Landroid/os/Parcelable;",
        "()V",
        "id",
        "",
        "getId",
        "()Ljava/lang/String;",
        "invoice",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "getInvoice",
        "()Lcom/squareup/protos/client/invoice/Invoice;",
        "isDraft",
        "",
        "()Z",
        "isRecurring",
        "sortDate",
        "Lcom/squareup/protos/client/ISO8601Date;",
        "getSortDate",
        "()Lcom/squareup/protos/client/ISO8601Date;",
        "describeContents",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "i",
        "Companion",
        "Invoice",
        "Recurring",
        "Lcom/squareup/invoices/DisplayDetails$Invoice;",
        "Lcom/squareup/invoices/DisplayDetails$Recurring;",
        "invoices-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/invoices/DisplayDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/invoices/DisplayDetails$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/DisplayDetails$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/DisplayDetails$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/DisplayDetails;->Companion:Lcom/squareup/invoices/DisplayDetails$Companion;

    .line 73
    new-instance v0, Lcom/squareup/invoices/DisplayDetails$Companion$CREATOR$1;

    invoke-direct {v0}, Lcom/squareup/invoices/DisplayDetails$Companion$CREATOR$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/invoices/DisplayDetails;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/squareup/invoices/DisplayDetails;-><init>()V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getId()Ljava/lang/String;
    .locals 2

    .line 52
    instance-of v0, p0, Lcom/squareup/invoices/DisplayDetails$Recurring;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/DisplayDetails$Recurring;

    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails$Recurring;->getDetails()Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->recurring_series_template:Lcom/squareup/protos/client/invoice/RecurringSeries;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/RecurringSeries;->template:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    const-string v1, "details.recurring_series\u2026emplate.id_pair.server_id"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 53
    :cond_0
    instance-of v0, p0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    if-eqz v0, :cond_1

    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    const-string v1, "details.invoice.id_pair.server_id"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public final getInvoice()Lcom/squareup/protos/client/invoice/Invoice;
    .locals 2

    .line 40
    instance-of v0, p0, Lcom/squareup/invoices/DisplayDetails$Recurring;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/DisplayDetails$Recurring;

    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails$Recurring;->getDetails()Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->recurring_series_template:Lcom/squareup/protos/client/invoice/RecurringSeries;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/RecurringSeries;->template:Lcom/squareup/protos/client/invoice/Invoice;

    const-string v1, "details.recurring_series_template.template"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 41
    :cond_0
    instance-of v0, p0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    if-eqz v0, :cond_1

    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    const-string v1, "details.invoice"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public final getSortDate()Lcom/squareup/protos/client/ISO8601Date;
    .locals 2

    .line 46
    instance-of v0, p0, Lcom/squareup/invoices/DisplayDetails$Recurring;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/DisplayDetails$Recurring;

    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails$Recurring;->getDetails()Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    const-string v1, "details.created_at"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :cond_0
    instance-of v0, p0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    if-eqz v0, :cond_1

    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    const-string v1, "details.sort_date"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public final isDraft()Z
    .locals 4

    .line 34
    instance-of v0, p0, Lcom/squareup/invoices/DisplayDetails$Recurring;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/DisplayDetails$Recurring;

    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails$Recurring;->getDetails()Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    sget-object v3, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->DRAFT:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    if-ne v0, v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 35
    :cond_1
    instance-of v0, p0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    if-eqz v0, :cond_2

    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    sget-object v3, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->DRAFT:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    if-ne v0, v3, :cond_0

    :goto_0
    return v1

    :cond_2
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public final isRecurring()Z
    .locals 1

    .line 30
    instance-of v0, p0, Lcom/squareup/invoices/DisplayDetails$Recurring;

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 66
    instance-of p2, p0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    if-eqz p2, :cond_0

    move-object p2, p0

    check-cast p2, Lcom/squareup/invoices/DisplayDetails$Invoice;

    invoke-virtual {p2}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p2

    check-cast p2, Lcom/squareup/wire/Message;

    invoke-static {p1, p2}, Lcom/squareup/util/Parcels;->writeProtoMessage(Landroid/os/Parcel;Lcom/squareup/wire/Message;)V

    goto :goto_0

    .line 67
    :cond_0
    instance-of p2, p0, Lcom/squareup/invoices/DisplayDetails$Recurring;

    if-eqz p2, :cond_1

    move-object p2, p0

    check-cast p2, Lcom/squareup/invoices/DisplayDetails$Recurring;

    invoke-virtual {p2}, Lcom/squareup/invoices/DisplayDetails$Recurring;->getDetails()Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    move-result-object p2

    check-cast p2, Lcom/squareup/wire/Message;

    invoke-static {p1, p2}, Lcom/squareup/util/Parcels;->writeProtoMessage(Landroid/os/Parcel;Lcom/squareup/wire/Message;)V

    :cond_1
    :goto_0
    return-void
.end method
