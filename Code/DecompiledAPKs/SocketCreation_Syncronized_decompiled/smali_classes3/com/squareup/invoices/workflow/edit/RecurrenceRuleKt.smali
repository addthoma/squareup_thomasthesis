.class public final Lcom/squareup/invoices/workflow/edit/RecurrenceRuleKt;
.super Ljava/lang/Object;
.source "RecurrenceRule.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRecurrenceRule.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RecurrenceRule.kt\ncom/squareup/invoices/workflow/edit/RecurrenceRuleKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,242:1\n1642#2,2:243\n*E\n*S KotlinDebug\n*F\n+ 1 RecurrenceRule.kt\ncom/squareup/invoices/workflow/edit/RecurrenceRuleKt\n*L\n53#1,2:243\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u000e\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003\u001a\u0018\u0010\u0004\u001a\u00020\u00012\u0006\u0010\u0005\u001a\u00020\u00062\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u00a8\u0006\t"
    }
    d2 = {
        "fromRecurringSchedule",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceRule;",
        "schedule",
        "Lcom/squareup/protos/client/invoice/RecurringSchedule;",
        "fromRfc5545",
        "rfcString",
        "",
        "untilDate",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "invoices-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final fromRecurringSchedule(Lcom/squareup/protos/client/invoice/RecurringSchedule;)Lcom/squareup/invoices/workflow/edit/RecurrenceRule;
    .locals 2

    const-string v0, "schedule"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/squareup/protos/client/invoice/RecurringSchedule;->recurrence_rule:Ljava/lang/String;

    const-string v1, "schedule.recurrence_rule"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/RecurringSchedule;->end_date:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-static {v0, p0}, Lcom/squareup/invoices/workflow/edit/RecurrenceRuleKt;->fromRfc5545(Ljava/lang/String;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object p0

    return-object p0
.end method

.method public static final fromRfc5545(Ljava/lang/String;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/invoices/workflow/edit/RecurrenceRule;
    .locals 16

    move-object/from16 v0, p0

    const-string v1, "rfcString"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    sget-object v1, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->DAYS:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    .line 50
    sget-object v2, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$Never;->INSTANCE:Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$Never;

    check-cast v2, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;

    .line 52
    move-object v3, v0

    check-cast v3, Ljava/lang/CharSequence;

    const/4 v0, 0x1

    new-array v4, v0, [C

    const/4 v9, 0x0

    const/16 v5, 0x3b

    aput-char v5, v4, v9

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x6

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lkotlin/text/StringsKt;->split$default(Ljava/lang/CharSequence;[CZIILjava/lang/Object;)Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    .line 243
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v4, v2

    const/4 v2, 0x1

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 54
    move-object v10, v5

    check-cast v10, Ljava/lang/CharSequence;

    const/4 v13, 0x2

    const/4 v12, 0x0

    new-array v11, v0, [C

    const/16 v5, 0x3d

    aput-char v5, v11, v9

    const/4 v14, 0x2

    const/4 v15, 0x0

    invoke-static/range {v10 .. v15}, Lkotlin/text/StringsKt;->split$default(Ljava/lang/CharSequence;[CZIILjava/lang/Object;)Ljava/util/List;

    move-result-object v5

    .line 55
    invoke-interface {v5, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v7

    const v8, 0x210e78

    if-eq v7, v8, :cond_3

    const v8, 0x3d558ef

    if-eq v7, v8, :cond_2

    const v8, 0x50a5d4c5

    if-eq v7, v8, :cond_1

    goto :goto_0

    :cond_1
    const-string v7, "INTERVAL"

    .line 56
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    goto :goto_0

    :cond_2
    const-string v7, "COUNT"

    .line 58
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    new-instance v5, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AfterCount;

    invoke-direct {v5, v4}, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AfterCount;-><init>(I)V

    check-cast v5, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;

    move-object v4, v5

    goto :goto_0

    :cond_3
    const-string v7, "FREQ"

    .line 57
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    sget-object v1, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->Companion:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit$Companion;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v1, v5}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit$Companion;->fromRfc(Ljava/lang/String;)Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    move-result-object v1

    if-eqz v1, :cond_4

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_5
    if-eqz p1, :cond_6

    .line 65
    new-instance v0, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AtDate;

    invoke-static/range {p1 .. p1}, Lcom/squareup/util/ProtoDates;->getDateForYearMonthDay(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Date;

    move-result-object v3

    const-string v4, "getDateForYearMonthDay(untilDate)"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v3}, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AtDate;-><init>(Ljava/util/Date;)V

    move-object v4, v0

    check-cast v4, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;

    .line 67
    :cond_6
    new-instance v0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    new-instance v3, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    invoke-direct {v3, v2, v1}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;-><init>(ILcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;)V

    invoke-direct {v0, v3, v4}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;-><init>(Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;)V

    return-object v0
.end method
