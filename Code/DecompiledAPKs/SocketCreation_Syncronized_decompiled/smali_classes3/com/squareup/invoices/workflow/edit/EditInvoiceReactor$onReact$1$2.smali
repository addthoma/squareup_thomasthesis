.class final Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$2;
.super Lkotlin/jvm/internal/Lambda;
.source "EditInvoiceReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartDeliveryMethod;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;",
        "it",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartDeliveryMethod;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$2;

    invoke-direct {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$2;-><init>()V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$2;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$2;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartDeliveryMethod;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartDeliveryMethod;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    .line 113
    new-instance v8, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;

    .line 114
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartDeliveryMethod;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 115
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartDeliveryMethod;->getInstruments()Lio/reactivex/Observable;

    move-result-object v3

    .line 116
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartDeliveryMethod;->getLoadingInstruments()Lio/reactivex/Observable;

    move-result-object v4

    .line 117
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartDeliveryMethod;->getCurrentPaymentMethod()Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v5

    .line 118
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartDeliveryMethod;->getShareLinkMessage()Ljava/lang/CharSequence;

    move-result-object v6

    .line 119
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartDeliveryMethod;->getCurrentInstrumentToken()Ljava/lang/String;

    move-result-object v7

    move-object v1, v8

    .line 113
    invoke-direct/range {v1 .. v7}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lio/reactivex/Observable;Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Ljava/lang/CharSequence;Ljava/lang/String;)V

    .line 112
    invoke-direct {v0, v8}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 78
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartDeliveryMethod;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$2;->invoke(Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartDeliveryMethod;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
