.class public final Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$asWorkflow$1;
.super Ljava/lang/Object;
.source "InvoiceFileAttachmentReactor.kt"

# interfaces
.implements Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileAttachmentsWorkflow;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->asWorkflow()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileAttachmentsWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00001\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001JH\u0010\u0002\u001aB\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0006\u0012&\u0012$\u0012\u0004\u0012\u00020\u0008\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\tj\u0002`\n0\u0007j\u0008\u0012\u0004\u0012\u00020\u0008`\u000b0\u0003H\u0016\u00a8\u0006\u000c"
    }
    d2 = {
        "com/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$asWorkflow$1",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileAttachmentsWorkflow;",
        "asStatefulWorkflow",
        "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
        "Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 113
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$asWorkflow$1;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic asStatefulWorkflow()Lcom/squareup/workflow/StatefulWorkflow;
    .locals 1

    .line 113
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$asWorkflow$1;->asStatefulWorkflow()Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/StatefulWorkflow;

    return-object v0
.end method

.method public asStatefulWorkflow()Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;",
            "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
            "Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation

    .line 115
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$asWorkflow$1;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;

    .line 116
    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentRenderer;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentRenderer;

    check-cast v1, Lcom/squareup/workflow/legacy/Renderer;

    .line 117
    sget-object v2, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$asWorkflow$1$asStatefulWorkflow$1;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$asWorkflow$1$asStatefulWorkflow$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 118
    sget-object v3, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$asWorkflow$1$asStatefulWorkflow$2;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$asWorkflow$1$asStatefulWorkflow$2;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 115
    invoke-static {v0, v1, v2, v3}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapterKt;->asV2Workflow(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;Lcom/squareup/workflow/legacy/Renderer;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;

    move-result-object v0

    return-object v0
.end method
