.class public final Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory_Factory;
.super Ljava/lang/Object;
.source "RealEditDetailsViewFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final editInvoiceDetailsScreenFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator$Factory;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory_Factory;->editInvoiceDetailsScreenFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator$Factory;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory_Factory;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator$Factory;)Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory;
    .locals 1

    .line 35
    new-instance v0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory;-><init>(Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator$Factory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory_Factory;->editInvoiceDetailsScreenFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator$Factory;

    invoke-static {v0}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory_Factory;->newInstance(Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator$Factory;)Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory_Factory;->get()Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory;

    move-result-object v0

    return-object v0
.end method
