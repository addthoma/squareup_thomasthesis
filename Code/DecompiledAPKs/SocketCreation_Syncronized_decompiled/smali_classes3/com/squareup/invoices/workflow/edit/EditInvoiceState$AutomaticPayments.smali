.class public final Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;
.super Lcom/squareup/invoices/workflow/edit/EditInvoiceState;
.source "EditInvoiceState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/EditInvoiceState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AutomaticPayments"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u000f\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B{\u0012t\u0010\u0005\u001ap\u0012(\u0012&\u0012\u0004\u0012\u00020\u0003\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\t\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000b0\u00080\u0007\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000c0\u0006j6\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000c\u0012&\u0012$\u0012\u0004\u0012\u00020\t\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000b0\u0008j\u0008\u0012\u0004\u0012\u00020\t`\u000e`\r\u00a2\u0006\u0002\u0010\u000fJw\u0010\u0012\u001ap\u0012(\u0012&\u0012\u0004\u0012\u00020\u0003\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\t\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000b0\u00080\u0007\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000c0\u0006j6\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000c\u0012&\u0012$\u0012\u0004\u0012\u00020\t\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000b0\u0008j\u0008\u0012\u0004\u0012\u00020\t`\u000e`\rH\u00c6\u0003J\u0081\u0001\u0010\u0013\u001a\u00020\u00002v\u0008\u0002\u0010\u0005\u001ap\u0012(\u0012&\u0012\u0004\u0012\u00020\u0003\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\t\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000b0\u00080\u0007\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000c0\u0006j6\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000c\u0012&\u0012$\u0012\u0004\u0012\u00020\t\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000b0\u0008j\u0008\u0012\u0004\u0012\u00020\t`\u000e`\rH\u00c6\u0001J\u0013\u0010\u0014\u001a\u00020\u00032\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001R\u007f\u0010\u0005\u001ap\u0012(\u0012&\u0012\u0004\u0012\u00020\u0003\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\t\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000b0\u00080\u0007\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000c0\u0006j6\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000c\u0012&\u0012$\u0012\u0004\u0012\u00020\t\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000b0\u0008j\u0008\u0012\u0004\u0012\u00020\t`\u000e`\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState;",
        "allowAutoPayments",
        "",
        "(Z)V",
        "editAutomaticPaymentsWorkflow",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Handle;",
        "Lcom/squareup/workflow/legacyintegration/LegacyState;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/invoices/workflow/edit/EditAutomaticPaymentsResult;",
        "Lcom/squareup/workflow/legacyintegration/LegacyHandle;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V",
        "getEditAutomaticPaymentsWorkflow",
        "()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;",
        "component1",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final editAutomaticPaymentsWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Ljava/lang/Boolean;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/invoices/workflow/edit/EditAutomaticPaymentsResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Ljava/lang/Boolean;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;-",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/invoices/workflow/edit/EditAutomaticPaymentsResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "editAutomaticPaymentsWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 181
    invoke-direct {p0, v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;->editAutomaticPaymentsWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    .line 183
    sget-object v0, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsWorkflow;->Companion:Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsWorkflow$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsWorkflow$Companion;->legacyHandleFromInput(Z)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    .line 182
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;->editAutomaticPaymentsWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;->copy(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Ljava/lang/Boolean;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/invoices/workflow/edit/EditAutomaticPaymentsResult;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;->editAutomaticPaymentsWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    return-object v0
.end method

.method public final copy(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Ljava/lang/Boolean;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;-",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/invoices/workflow/edit/EditAutomaticPaymentsResult;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;"
        }
    .end annotation

    const-string v0, "editAutomaticPaymentsWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;->editAutomaticPaymentsWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    iget-object p1, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;->editAutomaticPaymentsWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getEditAutomaticPaymentsWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Ljava/lang/Boolean;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/invoices/workflow/edit/EditAutomaticPaymentsResult;",
            ">;"
        }
    .end annotation

    .line 179
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;->editAutomaticPaymentsWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;->editAutomaticPaymentsWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AutomaticPayments(editAutomaticPaymentsWorkflow="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;->editAutomaticPaymentsWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
