.class public final Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;
.super Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action;
.source "EditPaymentRequestV2Workflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UpdateDueDate"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005J\t\u0010\t\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\n\u001a\u00020\u0003H\u00c6\u0003J\u001d\u0010\u000b\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u000c\u001a\u00020\r2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u00d6\u0003J\t\u0010\u0010\u001a\u00020\u0011H\u00d6\u0001J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0007\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action;",
        "dueDate",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "firstSentDate",
        "(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)V",
        "getDueDate",
        "()Lcom/squareup/protos/common/time/YearMonthDay;",
        "getFirstSentDate",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final dueDate:Lcom/squareup/protos/common/time/YearMonthDay;

.field private final firstSentDate:Lcom/squareup/protos/common/time/YearMonthDay;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)V
    .locals 1

    const-string v0, "dueDate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "firstSentDate"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 116
    invoke-direct {p0, v0}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;->dueDate:Lcom/squareup/protos/common/time/YearMonthDay;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;->firstSentDate:Lcom/squareup/protos/common/time/YearMonthDay;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;->dueDate:Lcom/squareup/protos/common/time/YearMonthDay;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;->firstSentDate:Lcom/squareup/protos/common/time/YearMonthDay;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;->copy(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;->dueDate:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;->firstSentDate:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object v0
.end method

.method public final copy(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;
    .locals 1

    const-string v0, "dueDate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "firstSentDate"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;

    invoke-direct {v0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;-><init>(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;->dueDate:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;->dueDate:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;->firstSentDate:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object p1, p1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;->firstSentDate:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDueDate()Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;->dueDate:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object v0
.end method

.method public final getFirstSentDate()Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;->firstSentDate:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;->dueDate:Lcom/squareup/protos/common/time/YearMonthDay;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;->firstSentDate:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UpdateDueDate(dueDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;->dueDate:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", firstSentDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action$UpdateDueDate;->firstSentDate:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
