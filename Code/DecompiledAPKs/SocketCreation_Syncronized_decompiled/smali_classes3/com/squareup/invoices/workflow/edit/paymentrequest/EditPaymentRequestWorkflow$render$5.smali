.class final Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$5;
.super Lkotlin/jvm/internal/Lambda;
.source "EditPaymentRequestWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;->render(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;",
        "+",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult;",
        "it",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$5;->$state:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 215
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult$Succeeded;

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 216
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult$Saved;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$5;->$state:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;->getPaymentRequestInfo()Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;->getPaymentRequestType()Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult$Saved;-><init>(Lcom/squareup/protos/client/invoice/PaymentRequest;)V

    .line 215
    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 218
    :cond_0
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult$Failed;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 219
    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$ShowingValidationError;

    .line 220
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$5;->$state:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;->getPaymentRequestInfo()Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    move-result-object v2

    new-instance v3, Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorInfo;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult$Failed;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult$Failed;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult$Failed;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v3, v4, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    invoke-direct {v1, v2, v3}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$ShowingValidationError;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorInfo;)V

    const/4 p1, 0x2

    const/4 v2, 0x0

    .line 218
    invoke-static {v0, v1, v2, p1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 69
    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$5;->invoke(Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
