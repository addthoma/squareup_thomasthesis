.class final Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator$attach$1;
.super Ljava/lang/Object;
.source "EditInvoiceDetailsScreen.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;",
        "Lcom/squareup/register/widgets/GlassSpinnerState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/register/widgets/GlassSpinnerState;",
        "it",
        "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator$attach$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator$attach$1;

    invoke-direct {v0}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator$attach$1;-><init>()V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator$attach$1;->INSTANCE:Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator$attach$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 4

    .line 115
    sget-object v0, Lcom/squareup/register/widgets/GlassSpinnerState;->Factory:Lcom/squareup/register/widgets/GlassSpinnerState$Factory;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->getShowProgress()Z

    move-result p1

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, p1, v1, v2, v3}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->showNonDebouncedSpinner$default(Lcom/squareup/register/widgets/GlassSpinnerState$Factory;ZIILjava/lang/Object;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 90
    check-cast p1, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsCoordinator$attach$1;->call(Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    return-object p1
.end method
