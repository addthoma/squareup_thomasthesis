.class public final Lcom/squareup/invoices/workflow/edit/CustomDateInfo;
.super Ljava/lang/Object;
.source "ChooseDateInfo.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0015\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B;\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\u0008\u0012\u0006\u0010\n\u001a\u00020\u0008\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\t\u0010\u0018\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0008H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u0008H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u0008H\u00c6\u0003J\t\u0010\u001d\u001a\u00020\u000cH\u00c6\u0003JK\u0010\u001e\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\u00082\u0008\u0008\u0002\u0010\n\u001a\u00020\u00082\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000cH\u00c6\u0001J\u0013\u0010\u001f\u001a\u00020\u000c2\u0008\u0010 \u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010!\u001a\u00020\"H\u00d6\u0001J\t\u0010#\u001a\u00020\u0003H\u00d6\u0001R\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\n\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\t\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0011R\u0011\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0011R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/CustomDateInfo;",
        "",
        "title",
        "",
        "dateOptions",
        "",
        "Lcom/squareup/invoices/workflow/edit/DateOption;",
        "startDate",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "selectedDate",
        "maxDate",
        "shouldShowActualDate",
        "",
        "(Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Z)V",
        "getDateOptions",
        "()Ljava/util/List;",
        "getMaxDate",
        "()Lcom/squareup/protos/common/time/YearMonthDay;",
        "getSelectedDate",
        "getShouldShowActualDate",
        "()Z",
        "getStartDate",
        "getTitle",
        "()Ljava/lang/String;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "invoices-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final dateOptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/invoices/workflow/edit/DateOption;",
            ">;"
        }
    .end annotation
.end field

.field private final maxDate:Lcom/squareup/protos/common/time/YearMonthDay;

.field private final selectedDate:Lcom/squareup/protos/common/time/YearMonthDay;

.field private final shouldShowActualDate:Z

.field private final startDate:Lcom/squareup/protos/common/time/YearMonthDay;

.field private final title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/invoices/workflow/edit/DateOption;",
            ">;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            "Z)V"
        }
    .end annotation

    const-string/jumbo v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateOptions"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "startDate"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedDate"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "maxDate"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->title:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->dateOptions:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->startDate:Lcom/squareup/protos/common/time/YearMonthDay;

    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->selectedDate:Lcom/squareup/protos/common/time/YearMonthDay;

    iput-object p5, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->maxDate:Lcom/squareup/protos/common/time/YearMonthDay;

    iput-boolean p6, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->shouldShowActualDate:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/workflow/edit/CustomDateInfo;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;ZILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/CustomDateInfo;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->title:Ljava/lang/String;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->dateOptions:Ljava/util/List;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->startDate:Lcom/squareup/protos/common/time/YearMonthDay;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->selectedDate:Lcom/squareup/protos/common/time/YearMonthDay;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->maxDate:Lcom/squareup/protos/common/time/YearMonthDay;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-boolean p6, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->shouldShowActualDate:Z

    :cond_5
    move v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->copy(Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Z)Lcom/squareup/invoices/workflow/edit/CustomDateInfo;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->title:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/workflow/edit/DateOption;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->dateOptions:Ljava/util/List;

    return-object v0
.end method

.method public final component3()Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->startDate:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object v0
.end method

.method public final component4()Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->selectedDate:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object v0
.end method

.method public final component5()Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->maxDate:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->shouldShowActualDate:Z

    return v0
.end method

.method public final copy(Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Z)Lcom/squareup/invoices/workflow/edit/CustomDateInfo;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/invoices/workflow/edit/DateOption;",
            ">;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            "Z)",
            "Lcom/squareup/invoices/workflow/edit/CustomDateInfo;"
        }
    .end annotation

    const-string/jumbo v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateOptions"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "startDate"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedDate"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "maxDate"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;-><init>(Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->title:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->title:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->dateOptions:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->dateOptions:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->startDate:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->startDate:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->selectedDate:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->selectedDate:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->maxDate:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->maxDate:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->shouldShowActualDate:Z

    iget-boolean p1, p1, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->shouldShowActualDate:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDateOptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/workflow/edit/DateOption;",
            ">;"
        }
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->dateOptions:Ljava/util/List;

    return-object v0
.end method

.method public final getMaxDate()Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->maxDate:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object v0
.end method

.method public final getSelectedDate()Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->selectedDate:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object v0
.end method

.method public final getShouldShowActualDate()Z
    .locals 1

    .line 43
    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->shouldShowActualDate:Z

    return v0
.end method

.method public final getStartDate()Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->startDate:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->title:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->title:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->dateOptions:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->startDate:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->selectedDate:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->maxDate:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->shouldShowActualDate:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CustomDateInfo(title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", dateOptions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->dateOptions:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", startDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->startDate:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", selectedDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->selectedDate:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", maxDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->maxDate:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", shouldShowActualDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->shouldShowActualDate:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
