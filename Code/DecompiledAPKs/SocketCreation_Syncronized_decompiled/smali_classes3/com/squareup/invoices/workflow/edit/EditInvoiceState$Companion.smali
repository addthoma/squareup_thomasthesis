.class public final Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Companion;
.super Ljava/lang/Object;
.source "EditInvoiceState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/EditInvoiceState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditInvoiceState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditInvoiceState.kt\ncom/squareup/invoices/workflow/edit/EditInvoiceState$Companion\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n+ 3 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n*L\n1#1,312:1\n180#2:313\n32#3,12:314\n*E\n*S KotlinDebug\n*F\n+ 1 EditInvoiceState.kt\ncom/squareup/invoices/workflow/edit/EditInvoiceState$Companion\n*L\n237#1:313\n237#1,12:314\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0017\u0010\u0003\u001a\u00020\u00042\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0000\u00a2\u0006\u0002\u0008\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Companion;",
        "",
        "()V",
        "fromSnapshot",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "fromSnapshot$invoices_hairball_release",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 233
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromSnapshot$invoices_hairball_release(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/invoices/workflow/edit/EditInvoiceState;
    .locals 4

    if-eqz p1, :cond_12

    .line 237
    invoke-virtual {p1}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    if-eqz p1, :cond_12

    .line 313
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p1}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p1

    check-cast p1, Lokio/BufferedSource;

    .line 238
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 239
    const-class v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Initializing;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Initializing;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Initializing;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState;

    goto/16 :goto_2

    .line 240
    :cond_0
    const-class v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 241
    sget-object v0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;->Companion:Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Companion;

    invoke-interface {p1}, Lokio/BufferedSource;->readByteString()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Companion;->fromByteString(Lokio/ByteString;)Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    move-result-object p1

    .line 242
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;

    sget-object v1, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;->Companion:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$Companion;

    invoke-virtual {v1, p1}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$Companion;->handle(Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState;

    goto/16 :goto_2

    .line 244
    :cond_1
    const-class v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Recurring;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 245
    sget-object v0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;->Companion:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Companion;

    invoke-interface {p1}, Lokio/BufferedSource;->readByteString()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Companion;->fromByteString(Lokio/ByteString;)Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;

    move-result-object p1

    .line 246
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Recurring;

    sget-object v1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor;->Companion:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$Companion;

    invoke-virtual {v1, p1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$Companion;->handle(Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Recurring;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState;

    goto/16 :goto_2

    .line 248
    :cond_2
    const-class v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingSendDate;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 249
    sget-object v0, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;->Companion:Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$Companion;

    invoke-interface {p1}, Lokio/BufferedSource;->readByteString()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$Companion;->fromByteString(Lokio/ByteString;)Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;

    move-result-object p1

    .line 250
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingSendDate;

    sget-object v1, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;->Companion:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$Companion;

    invoke-virtual {v1, p1}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$Companion;->handle(Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingSendDate;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState;

    goto/16 :goto_2

    .line 252
    :cond_3
    const-class v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingDueDate;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 253
    sget-object v0, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;->Companion:Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$Companion;

    invoke-interface {p1}, Lokio/BufferedSource;->readByteString()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$Companion;->fromByteString(Lokio/ByteString;)Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;

    move-result-object p1

    .line 254
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingDueDate;

    sget-object v1, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;->Companion:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$Companion;

    invoke-virtual {v1, p1}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$Companion;->handle(Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingDueDate;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState;

    goto/16 :goto_2

    .line 256
    :cond_4
    const-class v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingExpiryDate;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 257
    sget-object v0, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;->Companion:Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$Companion;

    invoke-interface {p1}, Lokio/BufferedSource;->readByteString()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$Companion;->fromByteString(Lokio/ByteString;)Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;

    move-result-object p1

    .line 258
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingExpiryDate;

    sget-object v1, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;->Companion:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$Companion;

    invoke-virtual {v1, p1}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$Companion;->handle(Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingExpiryDate;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState;

    goto/16 :goto_2

    .line 260
    :cond_5
    const-class v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 261
    sget-object v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;->Companion:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$Companion;

    invoke-interface {p1}, Lokio/BufferedSource;->readByteString()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$Companion;->fromByteString(Lokio/ByteString;)Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;

    move-result-object p1

    .line 262
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;

    sget-object v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;->Companion:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$Companion;

    invoke-virtual {v1, p1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$Companion;->handle(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState;

    goto/16 :goto_2

    .line 264
    :cond_6
    const-class v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceAttachment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 265
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    .line 266
    invoke-interface {p1}, Lokio/BufferedSource;->readByteString()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/workflow/Snapshot$Companion;->of(Lokio/ByteString;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    .line 314
    invoke-virtual {p1}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_7

    const/4 v0, 0x1

    goto :goto_0

    :cond_7
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x0

    if-eqz v0, :cond_8

    goto :goto_1

    :cond_8
    move-object p1, v2

    :goto_1
    if-eqz p1, :cond_a

    .line 319
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    const-string v2, "Parcel.obtain()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 320
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 321
    array-length v2, p1

    invoke-virtual {v0, p1, v1, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 322
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 323
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    if-nez v2, :cond_9

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_9
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 324
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    :cond_a
    if-nez v2, :cond_b

    .line 325
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 265
    :cond_b
    check-cast v2, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    .line 268
    new-instance p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceAttachment;

    sget-object v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->Companion:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$Companion;

    invoke-virtual {v0, v2}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$Companion;->handle(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceAttachment;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V

    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState;

    goto/16 :goto_2

    .line 270
    :cond_c
    const-class v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceDetails;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 271
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceDetails;

    .line 272
    sget-object v1, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow;->Companion:Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow$Companion;

    .line 273
    sget-object v2, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    invoke-interface {p1}, Lokio/BufferedSource;->readByteString()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/squareup/workflow/Snapshot$Companion;->of(Lokio/ByteString;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    .line 272
    invoke-virtual {v1, p1}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow$Companion;->legacyHandleFromSnapshot(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    .line 271
    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceDetails;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState;

    goto/16 :goto_2

    .line 277
    :cond_d
    const-class v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 278
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;

    .line 279
    sget-object v1, Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow;->Companion:Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow$Companion;

    .line 280
    sget-object v2, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    invoke-interface {p1}, Lokio/BufferedSource;->readByteString()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/squareup/workflow/Snapshot$Companion;->of(Lokio/ByteString;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    .line 279
    invoke-virtual {v1, p1}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow$Companion;->legacyHandleFromSnapshot(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    .line 278
    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState;

    goto :goto_2

    .line 284
    :cond_e
    const-class v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutoReminders;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 285
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutoReminders;

    .line 286
    sget-object v1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->Companion:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Companion;

    .line 287
    sget-object v2, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    invoke-interface {p1}, Lokio/BufferedSource;->readByteString()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/squareup/workflow/Snapshot$Companion;->of(Lokio/ByteString;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    .line 286
    invoke-virtual {v1, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Companion;->legacyHandleFromSnapshot(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    .line 285
    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutoReminders;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState;

    goto :goto_2

    .line 291
    :cond_f
    const-class v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentRequest;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 292
    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result v0

    .line 293
    new-instance v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentRequest;

    .line 294
    sget-object v2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;->Companion:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$Companion;

    .line 295
    sget-object v3, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    invoke-interface {p1}, Lokio/BufferedSource;->readByteString()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v3, p1}, Lcom/squareup/workflow/Snapshot$Companion;->of(Lokio/ByteString;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    .line 294
    invoke-virtual {v2, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$Companion;->legacyHandleFromSnapshot(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    .line 293
    invoke-direct {v1, v0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentRequest;-><init>(ILcom/squareup/workflow/legacy/WorkflowPool$Handle;)V

    move-object p1, v1

    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState;

    goto :goto_2

    .line 299
    :cond_10
    const-class v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentSchedule;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 300
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentSchedule;

    .line 301
    sget-object v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;->Companion:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Companion;

    .line 302
    sget-object v2, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    invoke-interface {p1}, Lokio/BufferedSource;->readByteString()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/squareup/workflow/Snapshot$Companion;->of(Lokio/ByteString;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    .line 301
    invoke-virtual {v1, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Companion;->legacyHandleFromSnapshot(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    .line 300
    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentSchedule;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState;

    :goto_2
    if-eqz p1, :cond_12

    goto :goto_3

    .line 306
    :cond_11
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 308
    :cond_12
    sget-object p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Initializing;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Initializing;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState;

    :goto_3
    return-object p1
.end method
