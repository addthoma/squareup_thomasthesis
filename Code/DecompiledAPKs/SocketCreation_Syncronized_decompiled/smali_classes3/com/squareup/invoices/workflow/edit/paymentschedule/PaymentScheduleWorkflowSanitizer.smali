.class public final Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleWorkflowSanitizer;
.super Ljava/lang/Object;
.source "PaymentScheduleWorkflowSanitizer.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPaymentScheduleWorkflowSanitizer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PaymentScheduleWorkflowSanitizer.kt\ncom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleWorkflowSanitizer\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 ProtosPure.kt\ncom/squareup/util/ProtosPure\n*L\n1#1,52:1\n704#2:53\n777#2,2:54\n950#2:56\n950#2:57\n1370#2:58\n1401#2,3:59\n1404#2:65\n132#3,3:62\n*E\n*S KotlinDebug\n*F\n+ 1 PaymentScheduleWorkflowSanitizer.kt\ncom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleWorkflowSanitizer\n*L\n27#1:53\n27#1,2:54\n28#1:56\n35#1:57\n42#1:58\n42#1,3:59\n42#1:65\n42#1,3:62\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\"\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0007\u001a\u00020\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleWorkflowSanitizer;",
        "",
        "()V",
        "sanitize",
        "",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "paymentRequests",
        "isTippingEnabled",
        "",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleWorkflowSanitizer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 10
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleWorkflowSanitizer;

    invoke-direct {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleWorkflowSanitizer;-><init>()V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleWorkflowSanitizer;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleWorkflowSanitizer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final sanitize(Ljava/util/List;Z)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;Z)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;"
        }
    .end annotation

    const-string v0, "paymentRequests"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    sget-object v0, Lcom/squareup/invoices/PaymentRequestsConfig;->Companion:Lcom/squareup/invoices/PaymentRequestsConfig$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/PaymentRequestsConfig$Companion;->fromPaymentRequests(Ljava/util/List;)Lcom/squareup/invoices/PaymentRequestsConfig;

    move-result-object v0

    .line 24
    sget-object v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleWorkflowSanitizer$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/invoices/PaymentRequestsConfig;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    const/4 v3, 0x2

    if-eq v0, v3, :cond_0

    goto :goto_1

    .line 35
    :cond_0
    check-cast p1, Ljava/lang/Iterable;

    .line 57
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleWorkflowSanitizer$sanitize$$inlined$sortedBy$2;

    invoke-direct {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleWorkflowSanitizer$sanitize$$inlined$sortedBy$2;-><init>()V

    check-cast v0, Ljava/util/Comparator;

    invoke-static {p1, v0}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p1

    goto :goto_1

    .line 26
    :cond_1
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 27
    check-cast p1, Ljava/lang/Iterable;

    .line 53
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    check-cast v3, Ljava/util/Collection;

    .line 54
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 27
    invoke-static {v5}, Lcom/squareup/invoices/PaymentRequestsKt;->isInstallment(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 55
    :cond_3
    check-cast v3, Ljava/util/List;

    check-cast v3, Ljava/lang/Iterable;

    .line 56
    new-instance p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleWorkflowSanitizer$sanitize$$inlined$sortedBy$1;

    invoke-direct {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleWorkflowSanitizer$sanitize$$inlined$sortedBy$1;-><init>()V

    check-cast p1, Ljava/util/Comparator;

    invoke-static {v3, p1}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p1

    .line 29
    check-cast p1, Ljava/util/Collection;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p1

    .line 31
    invoke-interface {p1, v1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :goto_1
    if-eqz p2, :cond_8

    .line 42
    move-object p2, p1

    check-cast p2, Ljava/lang/Iterable;

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {p2, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 60
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    const/4 v3, 0x0

    :goto_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    add-int/lit8 v5, v3, 0x1

    if-gez v3, :cond_4

    .line 61
    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_4
    check-cast v4, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 43
    check-cast v4, Lcom/squareup/wire/Message;

    .line 63
    invoke-virtual {v4}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object v4

    if-eqz v4, :cond_6

    move-object v6, v4

    check-cast v6, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    .line 44
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->getLastIndex(Ljava/util/List;)I

    move-result v7

    if-ne v3, v7, :cond_5

    const/4 v3, 0x1

    goto :goto_3

    :cond_5
    const/4 v3, 0x0

    :goto_3
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v6, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->tipping_enabled:Ljava/lang/Boolean;

    .line 64
    invoke-virtual {v4}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 45
    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v3, v5

    goto :goto_2

    .line 63
    :cond_6
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type B"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 65
    :cond_7
    move-object p1, v0

    check-cast p1, Ljava/util/List;

    :cond_8
    return-object p1
.end method
