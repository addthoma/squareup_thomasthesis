.class final Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory$from$balanceSplitString$2;
.super Lkotlin/jvm/internal/Lambda;
.source "EditPaymentScheduleScreen.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;->from(Ljava/util/List;Lcom/squareup/protos/common/Money;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "+",
        "Lcom/squareup/protos/common/Money;",
        ">;",
        "Ljava/lang/CharSequence;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "kotlin.jvm.PlatformType",
        "it",
        "Lkotlin/Pair;",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "Lcom/squareup/protos/common/Money;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory$from$balanceSplitString$2;->this$0:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lkotlin/Pair;)Ljava/lang/CharSequence;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            "Lcom/squareup/protos/common/Money;",
            ">;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory$from$balanceSplitString$2;->this$0:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;

    invoke-static {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;->access$getMoneyFormatter$p(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;)Lcom/squareup/text/Formatter;

    move-result-object v0

    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v0, "moneyFormatter.format(it.second)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 74
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory$from$balanceSplitString$2;->invoke(Lkotlin/Pair;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method
