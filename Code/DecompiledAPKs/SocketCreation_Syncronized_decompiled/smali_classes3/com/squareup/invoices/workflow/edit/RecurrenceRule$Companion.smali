.class public final Lcom/squareup/invoices/workflow/edit/RecurrenceRule$Companion;
.super Ljava/lang/Object;
.source "RecurrenceRule.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/RecurrenceRule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRecurrenceRule.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RecurrenceRule.kt\ncom/squareup/invoices/workflow/edit/RecurrenceRule$Companion\n+ 2 BuffersProtos.kt\ncom/squareup/workflow/BuffersProtos\n*L\n1#1,242:1\n56#2:243\n*E\n*S KotlinDebug\n*F\n+ 1 RecurrenceRule.kt\ncom/squareup/invoices/workflow/edit/RecurrenceRule$Companion\n*L\n149#1:243\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0007J\u0010\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/RecurrenceRule$Companion;",
        "",
        "()V",
        "defaultRecurrenceRule",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceRule;",
        "fromByteString",
        "str",
        "Lokio/ByteString;",
        "invoices-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 144
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final defaultRecurrenceRule()Lcom/squareup/invoices/workflow/edit/RecurrenceRule;
    .locals 4
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 161
    new-instance v0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    sget-object v2, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->MONTHS:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    const/4 v3, 0x1

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;-><init>(ILcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;)V

    sget-object v2, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$Never;->INSTANCE:Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$Never;

    check-cast v2, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;

    invoke-direct {v0, v1, v2}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;-><init>(Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;)V

    return-object v0
.end method

.method public final fromByteString(Lokio/ByteString;)Lcom/squareup/invoices/workflow/edit/RecurrenceRule;
    .locals 6
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "str"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    .line 148
    invoke-virtual {v0, p1}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p1

    .line 150
    move-object v0, p1

    check-cast v0, Lokio/BufferedSource;

    invoke-static {v0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v1

    .line 151
    invoke-virtual {p1}, Lokio/Buffer;->size()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long p1, v2, v4

    if-lez p1, :cond_0

    .line 243
    sget-object p1, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v2, Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {p1, v2}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/common/time/YearMonthDay;

    .line 152
    invoke-static {v1, p1}, Lcom/squareup/invoices/workflow/edit/RecurrenceRuleKt;->fromRfc5545(Ljava/lang/String;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 154
    invoke-static {v1, p1}, Lcom/squareup/invoices/workflow/edit/RecurrenceRuleKt;->fromRfc5545(Ljava/lang/String;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object p1

    :goto_0
    return-object p1
.end method
