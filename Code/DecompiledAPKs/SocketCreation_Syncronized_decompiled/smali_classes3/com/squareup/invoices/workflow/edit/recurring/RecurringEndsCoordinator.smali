.class public final Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "RecurringEndsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRecurringEndsCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RecurringEndsCoordinator.kt\ncom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator\n+ 2 MarketSpan.kt\ncom/squareup/marketfont/MarketSpanKt\n*L\n1#1,176:1\n17#2,2:177\n*E\n*S KotlinDebug\n*F\n+ 1 RecurringEndsCoordinator.kt\ncom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator\n*L\n121#1,2:177\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008a\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001/B5\u0008\u0002\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 H\u0016J\u0010\u0010!\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 H\u0002J\u001e\u0010\"\u001a\u00020#2\u000c\u0010$\u001a\u0008\u0012\u0004\u0012\u00020\u00060%2\u0006\u0010\n\u001a\u00020&H\u0002J\u001e\u0010\'\u001a\u00020\u001e2\u000c\u0010$\u001a\u0008\u0012\u0004\u0012\u00020\u00060%2\u0006\u0010(\u001a\u00020)H\u0002J(\u0010*\u001a\u00020\u001e2\u0016\u0010+\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00072\u0006\u0010\u001f\u001a\u00020 H\u0002J\u0010\u0010,\u001a\u00020)2\u0006\u0010-\u001a\u00020.H\u0002R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u00060"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
        "Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsScreen;",
        "dateFormat",
        "Ljava/text/DateFormat;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lio/reactivex/Observable;Ljava/text/DateFormat;Lcom/squareup/util/Res;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "chooseEnds",
        "Lcom/squareup/widgets/CheckableGroup;",
        "dateOption",
        "Lcom/squareup/marketfont/MarketCheckedTextView;",
        "endHelper",
        "Lcom/squareup/widgets/MessageView;",
        "neverOption",
        "numberOption",
        "numberRow",
        "Lcom/squareup/register/widgets/list/EditQuantityRow;",
        "startDate",
        "Ljava/util/Date;",
        "suppressCheck",
        "",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "getActionBarConfig",
        "Lcom/squareup/marin/widgets/MarinActionBar$Config;",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "Landroid/content/res/Resources;",
        "optionSelected",
        "optionId",
        "",
        "update",
        "screen",
        "viewIdForEndOption",
        "end",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;",
        "Factory",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private chooseEnds:Lcom/squareup/widgets/CheckableGroup;

.field private final dateFormat:Ljava/text/DateFormat;

.field private dateOption:Lcom/squareup/marketfont/MarketCheckedTextView;

.field private endHelper:Lcom/squareup/widgets/MessageView;

.field private neverOption:Lcom/squareup/marketfont/MarketCheckedTextView;

.field private numberOption:Lcom/squareup/marketfont/MarketCheckedTextView;

.field private numberRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

.field private final res:Lcom/squareup/util/Res;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private startDate:Ljava/util/Date;

.field private suppressCheck:Z


# direct methods
.method private constructor <init>(Lio/reactivex/Observable;Ljava/text/DateFormat;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
            ">;>;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .line 42
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->dateFormat:Ljava/text/DateFormat;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public synthetic constructor <init>(Lio/reactivex/Observable;Ljava/text/DateFormat;Lcom/squareup/util/Res;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;-><init>(Lio/reactivex/Observable;Ljava/text/DateFormat;Lcom/squareup/util/Res;)V

    return-void
.end method

.method public static final synthetic access$getDateOption$p(Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;)Lcom/squareup/marketfont/MarketCheckedTextView;
    .locals 1

    .line 38
    iget-object p0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->dateOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez p0, :cond_0

    const-string v0, "dateOption"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getSuppressCheck$p(Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;)Z
    .locals 0

    .line 38
    iget-boolean p0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->suppressCheck:Z

    return p0
.end method

.method public static final synthetic access$optionSelected(Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;I)V
    .locals 0

    .line 38
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->optionSelected(Lcom/squareup/workflow/legacy/WorkflowInput;I)V

    return-void
.end method

.method public static final synthetic access$setDateOption$p(Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;Lcom/squareup/marketfont/MarketCheckedTextView;)V
    .locals 0

    .line 38
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->dateOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    return-void
.end method

.method public static final synthetic access$setSuppressCheck$p(Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;Z)V
    .locals 0

    .line 38
    iput-boolean p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->suppressCheck:Z

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->update(Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 166
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string/jumbo v1, "view.findById<ActionBarV\u2026n_bar)\n        .presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 168
    sget v0, Lcom/squareup/features/invoices/R$id;->ends_options:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/CheckableGroup;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->chooseEnds:Lcom/squareup/widgets/CheckableGroup;

    .line 169
    sget v0, Lcom/squareup/features/invoices/R$id;->ends_never:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketCheckedTextView;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->neverOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    .line 170
    sget v0, Lcom/squareup/features/invoices/R$id;->ends_set_date:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketCheckedTextView;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->dateOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    .line 171
    sget v0, Lcom/squareup/features/invoices/R$id;->ends_number:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketCheckedTextView;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->numberOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    .line 172
    sget v0, Lcom/squareup/features/invoices/R$id;->choose_days:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/list/EditQuantityRow;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->numberRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    .line 173
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_end_helper:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->endHelper:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method private final getActionBarConfig(Lcom/squareup/workflow/legacy/WorkflowInput;Landroid/content/res/Resources;)Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
            ">;",
            "Landroid/content/res/Resources;",
            ")",
            "Lcom/squareup/marin/widgets/MarinActionBar$Config;"
        }
    .end annotation

    .line 139
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 140
    sget v1, Lcom/squareup/features/invoices/R$string;->end:I

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v0, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p2

    const/4 v0, 0x1

    .line 141
    invoke-virtual {p2, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p2

    .line 142
    new-instance v0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator$getActionBarConfig$builder$1;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator$getActionBarConfig$builder$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p2, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 144
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    const-string p2, "builder.build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final optionSelected(Lcom/squareup/workflow/legacy/WorkflowInput;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
            ">;I)V"
        }
    .end annotation

    .line 152
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->neverOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez v0, :cond_0

    const-string v1, "neverOption"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marketfont/MarketCheckedTextView;->getId()I

    move-result v0

    if-ne p2, v0, :cond_1

    sget-object p2, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$NeverClicked;->INSTANCE:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$NeverClicked;

    invoke-interface {p1, p2}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    goto :goto_0

    .line 153
    :cond_1
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->dateOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez v0, :cond_2

    const-string v1, "dateOption"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0}, Lcom/squareup/marketfont/MarketCheckedTextView;->getId()I

    move-result v0

    if-ne p2, v0, :cond_3

    sget-object p2, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$DateClicked;->INSTANCE:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$DateClicked;

    invoke-interface {p1, p2}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    goto :goto_0

    .line 154
    :cond_3
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->numberOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez v0, :cond_4

    const-string v1, "numberOption"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v0}, Lcom/squareup/marketfont/MarketCheckedTextView;->getId()I

    move-result v0

    if-ne p2, v0, :cond_5

    sget-object p2, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$NumberClicked;->INSTANCE:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$NumberClicked;

    invoke-interface {p1, p2}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    :cond_5
    :goto_0
    return-void
.end method

.method private final update(Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .line 78
    iget-object v0, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;->getRecurrenceRule()Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object v0

    if-nez v0, :cond_0

    .line 79
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->getRecurrenceEnd()Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;

    move-result-object v1

    .line 80
    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 82
    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v3, :cond_1

    const-string v4, "actionBar"

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    iget-object v4, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    const-string v5, "resources"

    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v4, v2}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->getActionBarConfig(Lcom/squareup/workflow/legacy/WorkflowInput;Landroid/content/res/Resources;)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 84
    new-instance v3, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator$update$1;

    invoke-direct {v3, p1}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator$update$1;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v3}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 86
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->chooseEnds:Lcom/squareup/widgets/CheckableGroup;

    const-string v3, "chooseEnds"

    if-nez p2, :cond_2

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    new-instance v4, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator$update$2;

    invoke-direct {v4, p0, p1}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator$update$2;-><init>(Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v4, Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;

    invoke-virtual {p2, v4}, Lcom/squareup/widgets/CheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;)V

    .line 92
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->chooseEnds:Lcom/squareup/widgets/CheckableGroup;

    if-nez p2, :cond_3

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    new-instance v4, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator$update$3;

    invoke-direct {v4, p0, p1}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator$update$3;-><init>(Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v4, Lcom/squareup/widgets/CheckableGroup$OnCheckedClickListener;

    invoke-virtual {p2, v4}, Lcom/squareup/widgets/CheckableGroup;->setOnCheckedClickListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedClickListener;)V

    .line 99
    iget-object p2, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p2, Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;

    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;->getStartDate()Ljava/util/Date;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->startDate:Ljava/util/Date;

    const/4 p2, 0x1

    .line 100
    iput-boolean p2, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->suppressCheck:Z

    .line 101
    iget-object v4, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->chooseEnds:Lcom/squareup/widgets/CheckableGroup;

    if-nez v4, :cond_4

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-direct {p0, v1}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->viewIdForEndOption(Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;)I

    move-result v3

    invoke-virtual {v4, v3}, Lcom/squareup/widgets/CheckableGroup;->check(I)V

    .line 102
    instance-of v3, v1, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AfterCount;

    const/4 v4, 0x0

    const-string v5, "numberRow"

    if-eqz v3, :cond_7

    .line 103
    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->numberRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    if-nez v3, :cond_5

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v3, v4}, Lcom/squareup/register/widgets/list/EditQuantityRow;->setVisibility(I)V

    .line 104
    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->numberRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    if-nez v3, :cond_6

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    move-object v6, v1

    check-cast v6, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AfterCount;

    invoke-virtual {v6}, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AfterCount;->getCount()I

    move-result v6

    invoke-virtual {v3, v6}, Lcom/squareup/register/widgets/list/EditQuantityRow;->setValue(I)V

    goto :goto_0

    .line 106
    :cond_7
    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->numberRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    if-nez v3, :cond_8

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    const/16 v6, 0x8

    invoke-virtual {v3, v6}, Lcom/squareup/register/widgets/list/EditQuantityRow;->setVisibility(I)V

    .line 109
    :goto_0
    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->numberRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    if-nez v3, :cond_9

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    new-instance v5, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator$update$4;

    invoke-direct {v5, p1}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator$update$4;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v5, Lcom/squareup/register/widgets/list/EditQuantityRow$OnQuantityChangedListener;

    invoke-virtual {v3, v5}, Lcom/squareup/register/widgets/list/EditQuantityRow;->setOnQuantityChangedListener(Lcom/squareup/register/widgets/list/EditQuantityRow$OnQuantityChangedListener;)V

    .line 115
    new-instance p1, Landroid/text/SpannableStringBuilder;

    invoke-direct {p1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 116
    sget v3, Lcom/squareup/features/invoices/R$string;->on_a_set_date:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {p1, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 117
    instance-of v3, v1, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AtDate;

    const-string v5, "dateOption"

    if-eqz v3, :cond_b

    .line 118
    new-instance v3, Landroid/text/SpannableString;

    .line 119
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->dateFormat:Ljava/text/DateFormat;

    check-cast v1, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AtDate;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AtDate;->getDate()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 118
    invoke-direct {v3, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v3, Landroid/text/Spannable;

    .line 121
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->dateOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez v1, :cond_a

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {v1}, Lcom/squareup/marketfont/MarketCheckedTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v6, "dateOption.context"

    invoke-static {v1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v6, Lcom/squareup/marketfont/MarketFont$Weight;->LIGHT:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 178
    new-instance v7, Lcom/squareup/fonts/FontSpan;

    invoke-static {v6, v4}, Lcom/squareup/marketfont/MarketFont$Weight;->resourceIdFor(Lcom/squareup/marketfont/MarketFont$Weight;I)I

    move-result v6

    invoke-direct {v7, v1, v6}, Lcom/squareup/fonts/FontSpan;-><init>(Landroid/content/Context;I)V

    check-cast v7, Landroid/text/style/CharacterStyle;

    .line 121
    invoke-static {v3, v7}, Lcom/squareup/text/Spannables;->span(Landroid/text/Spannable;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v1

    .line 123
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    .line 124
    sget v6, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 123
    invoke-direct {v3, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    check-cast v3, Landroid/text/style/CharacterStyle;

    .line 122
    invoke-static {v1, v3}, Lcom/squareup/text/Spannables;->span(Landroid/text/Spannable;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v1

    .line 127
    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 129
    :cond_b
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->dateOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez v1, :cond_c

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v1, p1}, Lcom/squareup/marketfont/MarketCheckedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->endHelper:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_d

    const-string v1, "endHelper"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->res:Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->startDate:Ljava/util/Date;

    if-nez v2, :cond_e

    const-string v3, "startDate"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_e
    invoke-static {v0, v1, v2, p2}, Lcom/squareup/invoices/InvoicesHelperTextUtility;->getRecurringPeriodShortText(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Lcom/squareup/util/Res;Ljava/util/Date;Z)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iput-boolean v4, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->suppressCheck:Z

    return-void
.end method

.method private final viewIdForEndOption(Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;)I
    .locals 1

    .line 159
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$Never;

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->neverOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez p1, :cond_0

    const-string v0, "neverOption"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/marketfont/MarketCheckedTextView;->getId()I

    move-result p1

    goto :goto_0

    .line 160
    :cond_1
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AtDate;

    if-eqz v0, :cond_3

    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->dateOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez p1, :cond_2

    const-string v0, "dateOption"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1}, Lcom/squareup/marketfont/MarketCheckedTextView;->getId()I

    move-result p1

    goto :goto_0

    .line 161
    :cond_3
    instance-of p1, p1, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AfterCount;

    if-eqz p1, :cond_5

    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->numberOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez p1, :cond_4

    const-string v0, "numberOption"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p1}, Lcom/squareup/marketfont/MarketCheckedTextView;->getId()I

    move-result p1

    goto :goto_0

    :cond_5
    const/4 p1, -0x1

    :goto_0
    return p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 70
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->bindViews(Landroid/view/View;)V

    .line 71
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator$attach$1;-><init>(Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
