.class public final Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory;
.super Lcom/squareup/workflow/CompoundWorkflowViewFactory;
.source "EditInvoiceViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B?\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory;",
        "Lcom/squareup/workflow/CompoundWorkflowViewFactory;",
        "editInvoiceScreensViewFactory",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory;",
        "editPaymentRequestViewFactory",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestViewFactory;",
        "editPaymentScheduleViewFactory",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleViewFactory;",
        "realDeliveryMethodViewFactory",
        "Lcom/squareup/invoices/workflow/edit/delivery/RealDeliveryMethodViewFactory;",
        "realFileAttachmentsViewFactory",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory;",
        "realAdditionalRecipientsViewFactory",
        "Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsViewFactory;",
        "realEditDetailsViewFactory",
        "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory;",
        "(Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory;Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestViewFactory;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleViewFactory;Lcom/squareup/invoices/workflow/edit/delivery/RealDeliveryMethodViewFactory;Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory;Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsViewFactory;Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory;)V",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory;Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestViewFactory;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleViewFactory;Lcom/squareup/invoices/workflow/edit/delivery/RealDeliveryMethodViewFactory;Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory;Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsViewFactory;Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "editInvoiceScreensViewFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editPaymentRequestViewFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editPaymentScheduleViewFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "realDeliveryMethodViewFactory"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "realFileAttachmentsViewFactory"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "realAdditionalRecipientsViewFactory"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "realEditDetailsViewFactory"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/workflow/WorkflowViewFactory;

    .line 46
    check-cast p1, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 47
    check-cast p2, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 p1, 0x1

    aput-object p2, v0, p1

    .line 48
    check-cast p3, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 p1, 0x2

    aput-object p3, v0, p1

    .line 49
    check-cast p4, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 p1, 0x3

    aput-object p4, v0, p1

    .line 50
    check-cast p5, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 p1, 0x4

    aput-object p5, v0, p1

    .line 51
    check-cast p6, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 p1, 0x5

    aput-object p6, v0, p1

    .line 52
    check-cast p7, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 p1, 0x6

    aput-object p7, v0, p1

    .line 45
    invoke-direct {p0, v0}, Lcom/squareup/workflow/CompoundWorkflowViewFactory;-><init>([Lcom/squareup/workflow/WorkflowViewFactory;)V

    return-void
.end method
