.class public final Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "DeliveryMethodCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$DefaultDeliveryMethods;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDeliveryMethodCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DeliveryMethodCoordinator.kt\ncom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,174:1\n1651#2,3:175\n*E\n*S KotlinDebug\n*F\n+ 1 DeliveryMethodCoordinator.kt\ncom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator\n*L\n118#1,3:175\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001:\u00012B#\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u001e\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001b2\u000c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u001e0\u001dH\u0002J\u0010\u0010\u001f\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0010\u0010 \u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\u001e\u0010!\u001a\u00020\u00192\u000c\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020\u00060#2\u0006\u0010$\u001a\u00020%H\u0002J\u0018\u0010&\u001a\u00020\u00192\u0006\u0010\'\u001a\u00020(2\u0006\u0010)\u001a\u00020%H\u0002J&\u0010*\u001a\u00020\u00192\u0006\u0010+\u001a\u00020\n2\u000c\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020\u00060#2\u0006\u0010,\u001a\u00020-H\u0002J\u0010\u0010.\u001a\u00020\u00192\u0006\u0010/\u001a\u00020\u0017H\u0002J&\u00100\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u00101\u001a\u00020\u00052\u000c\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020\u00060#H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000R\'\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u00063"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;",
        "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent;",
        "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodScreen;",
        "(Lio/reactivex/Observable;)V",
        "actionBarView",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "deliveryMethodOptions",
        "Lcom/squareup/widgets/CheckableGroup;",
        "emailOption",
        "Lcom/squareup/marketfont/MarketCheckedTextView;",
        "manualOption",
        "messageView",
        "Lcom/squareup/widgets/MessageView;",
        "progressBar",
        "Landroid/widget/ProgressBar;",
        "getScreens",
        "()Lio/reactivex/Observable;",
        "suppressCheckChangeEvent",
        "",
        "addInstrumentOptions",
        "",
        "view",
        "Landroid/view/View;",
        "instruments",
        "",
        "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
        "attach",
        "bindViews",
        "onCheck",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "index",
        "",
        "setCheckedOption",
        "paymentMethod",
        "Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;",
        "instrumentIndex",
        "setupActionbar",
        "actionBar",
        "title",
        "",
        "showLoading",
        "loading",
        "update",
        "state",
        "DefaultDeliveryMethods",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

.field private deliveryMethodOptions:Lcom/squareup/widgets/CheckableGroup;

.field private emailOption:Lcom/squareup/marketfont/MarketCheckedTextView;

.field private manualOption:Lcom/squareup/marketfont/MarketCheckedTextView;

.field private messageView:Lcom/squareup/widgets/MessageView;

.field private progressBar:Landroid/widget/ProgressBar;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;",
            "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private suppressCheckChangeEvent:Z


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;",
            "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$getSuppressCheckChangeEvent$p(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;)Z
    .locals 0

    .line 34
    iget-boolean p0, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->suppressCheckChangeEvent:Z

    return p0
.end method

.method public static final synthetic access$onCheck(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;I)V
    .locals 0

    .line 34
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->onCheck(Lcom/squareup/workflow/legacy/WorkflowInput;I)V

    return-void
.end method

.method public static final synthetic access$setSuppressCheckChangeEvent$p(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;Z)V
    .locals 0

    .line 34
    iput-boolean p1, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->suppressCheckChangeEvent:Z

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;Landroid/view/View;Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->update(Landroid/view/View;Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method private final addInstrumentOptions(Landroid/view/View;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
            ">;)V"
        }
    .end annotation

    .line 115
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 116
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 118
    check-cast p2, Ljava/lang/Iterable;

    .line 176
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    add-int/lit8 v4, v2, 0x1

    if-gez v2, :cond_0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_0
    check-cast v3, Lcom/squareup/protos/client/instruments/InstrumentSummary;

    .line 119
    iget-object v3, v3, Lcom/squareup/protos/client/instruments/InstrumentSummary;->card:Lcom/squareup/protos/client/instruments/CardSummary;

    .line 120
    sget v5, Lcom/squareup/features/invoices/R$string;->invoice_charge_cof:I

    invoke-static {p1, v5}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v5

    .line 121
    iget-object v6, v3, Lcom/squareup/protos/client/instruments/CardSummary;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object v6, v6, Lcom/squareup/protos/client/bills/CardTender$Card;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v6}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->toString()Ljava/lang/String;

    move-result-object v6

    check-cast v6, Ljava/lang/CharSequence;

    const-string v7, "card_brand"

    invoke-virtual {v5, v7, v6}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v5

    .line 122
    iget-object v3, v3, Lcom/squareup/protos/client/instruments/CardSummary;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/CardTender$Card;->pan_suffix:Ljava/lang/String;

    check-cast v3, Ljava/lang/CharSequence;

    const-string v6, "pan"

    invoke-virtual {v5, v6, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    .line 123
    invoke-virtual {v3}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v3

    .line 125
    iget-object v5, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->deliveryMethodOptions:Lcom/squareup/widgets/CheckableGroup;

    if-nez v5, :cond_1

    const-string v6, "deliveryMethodOptions"

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 126
    :cond_1
    invoke-static {}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$DefaultDeliveryMethods;->values()[Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$DefaultDeliveryMethods;

    move-result-object v6

    array-length v6, v6

    add-int/2addr v2, v6

    .line 124
    invoke-static {v0, v5, v2, v3, v1}, Lcom/squareup/ui/CheckableGroups;->addCheckableRow(Landroid/view/LayoutInflater;Lcom/squareup/widgets/CheckableGroup;ILjava/lang/CharSequence;Z)V

    move v2, v4

    goto :goto_0

    :cond_2
    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 166
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    .line 167
    sget v0, Lcom/squareup/features/invoices/R$id;->delivery_method_options:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/CheckableGroup;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->deliveryMethodOptions:Lcom/squareup/widgets/CheckableGroup;

    .line 168
    sget v0, Lcom/squareup/features/invoices/R$id;->email_option:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketCheckedTextView;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->emailOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    .line 169
    sget v0, Lcom/squareup/features/invoices/R$id;->manual_option:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketCheckedTextView;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->manualOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    .line 170
    sget v0, Lcom/squareup/features/invoices/R$id;->message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->messageView:Lcom/squareup/widgets/MessageView;

    .line 171
    sget v0, Lcom/squareup/features/invoices/R$id;->loading_instruments:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ProgressBar;

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->progressBar:Landroid/widget/ProgressBar;

    return-void
.end method

.method private final onCheck(Lcom/squareup/workflow/legacy/WorkflowInput;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent;",
            ">;I)V"
        }
    .end annotation

    .line 136
    sget-object v0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$DefaultDeliveryMethods;->EMAIL:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$DefaultDeliveryMethods;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$DefaultDeliveryMethods;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_0

    sget-object p2, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent$EmailChecked;->INSTANCE:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent$EmailChecked;

    invoke-interface {p1, p2}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    goto :goto_0

    .line 137
    :cond_0
    sget-object v0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$DefaultDeliveryMethods;->SHARE_LINK:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$DefaultDeliveryMethods;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$DefaultDeliveryMethods;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_1

    sget-object p2, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent$ShareLinkChecked;->INSTANCE:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent$ShareLinkChecked;

    invoke-interface {p1, p2}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    goto :goto_0

    .line 138
    :cond_1
    new-instance v0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent$InstrumentChecked;

    invoke-static {}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$DefaultDeliveryMethods;->values()[Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$DefaultDeliveryMethods;

    move-result-object v1

    array-length v1, v1

    sub-int/2addr p2, v1

    invoke-direct {v0, p2}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent$InstrumentChecked;-><init>(I)V

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method private final setCheckedOption(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;I)V
    .locals 2

    const/4 v0, 0x1

    .line 147
    iput-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->suppressCheckChangeEvent:Z

    .line 148
    sget-object v1, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->ordinal()I

    move-result p1

    aget p1, v1, p1

    const-string v1, "deliveryMethodOptions"

    if-eq p1, v0, :cond_4

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 151
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->deliveryMethodOptions:Lcom/squareup/widgets/CheckableGroup;

    if-nez p1, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 152
    :cond_0
    invoke-static {}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$DefaultDeliveryMethods;->values()[Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$DefaultDeliveryMethods;

    move-result-object v0

    array-length v0, v0

    add-int/2addr p2, v0

    .line 151
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/CheckableGroup;->check(I)V

    goto :goto_0

    .line 154
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 150
    :cond_2
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->deliveryMethodOptions:Lcom/squareup/widgets/CheckableGroup;

    if-nez p1, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    sget-object p2, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$DefaultDeliveryMethods;->SHARE_LINK:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$DefaultDeliveryMethods;

    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$DefaultDeliveryMethods;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/CheckableGroup;->check(I)V

    goto :goto_0

    .line 149
    :cond_4
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->deliveryMethodOptions:Lcom/squareup/widgets/CheckableGroup;

    if-nez p1, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    sget-object p2, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$DefaultDeliveryMethods;->EMAIL:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$DefaultDeliveryMethods;

    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$DefaultDeliveryMethods;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/CheckableGroup;->check(I)V

    :goto_0
    const/4 p1, 0x0

    .line 156
    iput-boolean p1, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->suppressCheckChangeEvent:Z

    return-void
.end method

.method private final setupActionbar(Lcom/squareup/marin/widgets/ActionBarView;Lcom/squareup/workflow/legacy/WorkflowInput;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/marin/widgets/ActionBarView;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 108
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    const-string v0, "actionBar.presenter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 105
    check-cast p3, Ljava/lang/CharSequence;

    invoke-virtual {v0, p3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p3

    const/4 v0, 0x1

    .line 106
    invoke-virtual {p3, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p3

    .line 107
    new-instance v0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$setupActionbar$1;

    invoke-direct {v0, p2}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$setupActionbar$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p3, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p2

    .line 108
    invoke-virtual {p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final showLoading(Z)V
    .locals 2

    .line 160
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->progressBar:Landroid/widget/ProgressBar;

    if-nez v0, :cond_0

    const-string v1, "progressBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 161
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->messageView:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_1

    const-string v1, "messageView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/View;

    xor-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 162
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->deliveryMethodOptions:Lcom/squareup/widgets/CheckableGroup;

    if-nez v0, :cond_2

    const-string v1, "deliveryMethodOptions"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v0, Landroid/view/View;

    xor-int/lit8 p1, p1, 0x1

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent;",
            ">;)V"
        }
    .end annotation

    .line 67
    new-instance v0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$update$1;

    invoke-direct {v0, p3}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$update$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 69
    instance-of v0, p2, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Loading;

    const-string v1, "actionBarView"

    if-eqz v0, :cond_1

    .line 70
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez p1, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p2, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Loading;

    invoke-virtual {p2}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Loading;->getTitle()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p1, p3, p2}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->setupActionbar(Lcom/squareup/marin/widgets/ActionBarView;Lcom/squareup/workflow/legacy/WorkflowInput;Ljava/lang/String;)V

    const/4 p1, 0x1

    .line 71
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->showLoading(Z)V

    goto/16 :goto_1

    .line 73
    :cond_1
    instance-of v0, p2, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;

    if-eqz v0, :cond_a

    .line 74
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast p2, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;

    invoke-virtual {p2}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, p3, v1}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->setupActionbar(Lcom/squareup/marin/widgets/ActionBarView;Lcom/squareup/workflow/legacy/WorkflowInput;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 75
    invoke-direct {p0, v0}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->showLoading(Z)V

    .line 76
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->messageView:Lcom/squareup/widgets/MessageView;

    if-nez v1, :cond_3

    const-string v2, "messageView"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p2}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;->getMessage()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    invoke-virtual {p2}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;->getInstrumentsChanged()Z

    move-result v1

    const-string v2, "deliveryMethodOptions"

    if-eqz v1, :cond_8

    .line 79
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->deliveryMethodOptions:Lcom/squareup/widgets/CheckableGroup;

    if-nez v1, :cond_4

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v1}, Lcom/squareup/widgets/CheckableGroup;->getChildCount()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_7

    .line 80
    invoke-static {}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$DefaultDeliveryMethods;->values()[Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$DefaultDeliveryMethods;

    move-result-object v3

    array-length v3, v3

    if-lt v0, v3, :cond_6

    .line 82
    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->deliveryMethodOptions:Lcom/squareup/widgets/CheckableGroup;

    if-nez v3, :cond_5

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v3, v0}, Lcom/squareup/widgets/CheckableGroup;->removeViewAt(I)V

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 85
    :cond_7
    invoke-virtual {p2}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;->getInstruments()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->addInstrumentOptions(Landroid/view/View;Ljava/util/List;)V

    .line 88
    :cond_8
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->deliveryMethodOptions:Lcom/squareup/widgets/CheckableGroup;

    if-nez p1, :cond_9

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    new-instance v0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$update$2;

    invoke-direct {v0, p0, p3}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$update$2;-><init>(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/CheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;)V

    .line 94
    invoke-virtual {p2}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;->getPaymentMethod()Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object p1

    invoke-virtual {p2}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;->getSelectedInstrumentIndex()I

    move-result p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->setCheckedOption(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;I)V

    :cond_a
    :goto_1
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->bindViews(Landroid/view/View;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->emailOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez v0, :cond_0

    const-string v1, "emailOption"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget-object v1, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$DefaultDeliveryMethods;->EMAIL:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$DefaultDeliveryMethods;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$DefaultDeliveryMethods;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketCheckedTextView;->setId(I)V

    .line 56
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->manualOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez v0, :cond_1

    const-string v1, "manualOption"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget-object v1, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$DefaultDeliveryMethods;->SHARE_LINK:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$DefaultDeliveryMethods;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$DefaultDeliveryMethods;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketCheckedTextView;->setId(I)V

    .line 58
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator$attach$1;-><init>(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens.subscribe { upda\u2026, it.data, it.workflow) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method

.method public final getScreens()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;",
            "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent;",
            ">;>;"
        }
    .end annotation

    .line 35
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodCoordinator;->screens:Lio/reactivex/Observable;

    return-object v0
.end method
