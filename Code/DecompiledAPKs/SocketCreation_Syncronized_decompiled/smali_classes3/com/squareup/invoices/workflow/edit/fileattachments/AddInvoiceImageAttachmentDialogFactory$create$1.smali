.class final Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory$create$1;
.super Ljava/lang/Object;
.source "AddInvoiceImageAttachmentDialogFactory.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory;->create(Landroid/content/Context;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0016\u0010\u0003\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Landroid/app/AlertDialog;",
        "kotlin.jvm.PlatformType",
        "screen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogScreen;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Landroid/content/Context;

.field final synthetic $fileAttachmentSources:[Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;[Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory$create$1;->$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory$create$1;->$fileAttachmentSources:[Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lkotlin/Unit;",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
            ">;)",
            "Landroid/app/AlertDialog;"
        }
    .end annotation

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 50
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 51
    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_add_attachment:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 52
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory$create$1;->$fileAttachmentSources:[Ljava/lang/String;

    check-cast v1, [Ljava/lang/CharSequence;

    new-instance v2, Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory$create$1$1;

    invoke-direct {v2, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory$create$1$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 59
    sget v1, Lcom/squareup/common/strings/R$string;->cancel:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 60
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 61
    new-instance v1, Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory$create$1$2;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory$create$1$2;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 62
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory$create$1;->apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method
