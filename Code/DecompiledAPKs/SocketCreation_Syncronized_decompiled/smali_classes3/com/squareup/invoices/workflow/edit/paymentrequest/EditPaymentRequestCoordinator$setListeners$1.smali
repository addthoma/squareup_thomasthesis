.class final Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$setListeners$1;
.super Ljava/lang/Object;
.source "EditPaymentRequestCoordinator.kt"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->setListeners(Lkotlin/jvm/functions/Function1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Landroid/widget/RadioGroup;",
        "kotlin.jvm.PlatformType",
        "checkedId",
        "",
        "onCheckedChanged"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $eventHandler:Lkotlin/jvm/functions/Function1;


# direct methods
.method constructor <init>(Lkotlin/jvm/functions/Function1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$setListeners$1;->$eventHandler:Lkotlin/jvm/functions/Function1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 1

    .line 152
    sget p1, Lcom/squareup/features/invoices/R$id;->percentage_button:I

    if-ne p2, p1, :cond_0

    .line 153
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$setListeners$1;->$eventHandler:Lkotlin/jvm/functions/Function1;

    new-instance p2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen$Event$AmountTypeChanged;

    sget-object v0, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->PERCENTAGE_DEPOSIT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    invoke-direct {p2, v0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen$Event$AmountTypeChanged;-><init>(Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;)V

    invoke-interface {p1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 155
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$setListeners$1;->$eventHandler:Lkotlin/jvm/functions/Function1;

    new-instance p2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen$Event$AmountTypeChanged;

    sget-object v0, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->FIXED_DEPOSIT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    invoke-direct {p2, v0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen$Event$AmountTypeChanged;-><init>(Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;)V

    invoke-interface {p1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void
.end method
