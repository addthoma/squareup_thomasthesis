.class public final Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor_Factory;
.super Ljava/lang/Object;
.source "ChooseDateReactor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;",
        ">;"
    }
.end annotation


# instance fields
.field private final chooseDateDataConverterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/ChooseDateDataConverter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/ChooseDateDataConverter;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor_Factory;->chooseDateDataConverterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/ChooseDateDataConverter;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor_Factory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/invoices/workflow/edit/ChooseDateDataConverter;)Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;
    .locals 1

    .line 35
    new-instance v0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;-><init>(Lcom/squareup/invoices/workflow/edit/ChooseDateDataConverter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor_Factory;->chooseDateDataConverterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/ChooseDateDataConverter;

    invoke-static {v0}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor_Factory;->newInstance(Lcom/squareup/invoices/workflow/edit/ChooseDateDataConverter;)Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor_Factory;->get()Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;

    move-result-object v0

    return-object v0
.end method
