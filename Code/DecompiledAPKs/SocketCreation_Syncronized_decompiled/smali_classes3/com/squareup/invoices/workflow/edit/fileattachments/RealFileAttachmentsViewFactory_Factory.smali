.class public final Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory_Factory;
.super Ljava/lang/Object;
.source "RealFileAttachmentsViewFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final addInvoiceImageAttachmentDialogFactoryFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceImageCoordinatorFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$Factory;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory_Factory;->addInvoiceImageAttachmentDialogFactoryFactoryProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory_Factory;->invoiceImageCoordinatorFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$Factory;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory$Factory;Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$Factory;)Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory;
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory$Factory;Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$Factory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory_Factory;->addInvoiceImageAttachmentDialogFactoryFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory$Factory;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory_Factory;->invoiceImageCoordinatorFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$Factory;

    invoke-static {v0, v1}, Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory_Factory;->newInstance(Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory$Factory;Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$Factory;)Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory_Factory;->get()Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory;

    move-result-object v0

    return-object v0
.end method
