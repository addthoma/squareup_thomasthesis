.class public final Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;
.super Ljava/lang/Object;
.source "EditPaymentScheduleScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditPaymentScheduleScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditPaymentScheduleScreen.kt\ncom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,217:1\n1847#2,3:218\n1577#2,4:221\n704#2:225\n777#2,2:226\n215#2,2:228\n704#2:230\n777#2,2:231\n1360#2:233\n1429#2,3:234\n1866#2,7:237\n704#2:244\n777#2,2:245\n1360#2:247\n1429#2,3:248\n1866#2,7:251\n*E\n*S KotlinDebug\n*F\n+ 1 EditPaymentScheduleScreen.kt\ncom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory\n*L\n83#1,3:218\n87#1,4:221\n89#1:225\n89#1,2:226\n93#1,2:228\n122#1:230\n122#1,2:231\n123#1:233\n123#1,3:234\n124#1,7:237\n128#1:244\n128#1,2:245\n129#1:247\n129#1,3:248\n130#1,7:251\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0000\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u000e\u0008\u0001\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0003\u00a2\u0006\u0002\u0010\u0007J\u001c\u0010\u0008\u001a\u00020\t2\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b2\u0006\u0010\r\u001a\u00020\u0004J\u0012\u0010\u000e\u001a\u00020\u0004*\u0008\u0012\u0004\u0012\u00020\u000c0\u000bH\u0002J\u0012\u0010\u000f\u001a\u00020\u0010*\u0008\u0012\u0004\u0012\u00020\u000c0\u000bH\u0002R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;",
        "",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "percentageFormatter",
        "Lcom/squareup/util/Percentage;",
        "(Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)V",
        "from",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;",
        "paymentRequests",
        "",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "invoiceAmount",
        "totalInstallmentMoney",
        "totalInstallmentPercentage",
        "",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)V
    .locals 1
    .param p2    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/text/ForPercentage;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "moneyFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "percentageFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;->percentageFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method public static final synthetic access$getMoneyFormatter$p(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;)Lcom/squareup/text/Formatter;
    .locals 0

    .line 74
    iget-object p0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-object p0
.end method

.method private final totalInstallmentMoney(Ljava/util/List;)Lcom/squareup/protos/common/Money;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;)",
            "Lcom/squareup/protos/common/Money;"
        }
    .end annotation

    .line 128
    check-cast p1, Ljava/lang/Iterable;

    .line 244
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 245
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 128
    invoke-static {v2}, Lcom/squareup/invoices/PaymentRequestsKt;->isInstallment(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 246
    :cond_1
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 247
    new-instance p1, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {p1, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p1, Ljava/util/Collection;

    .line 248
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 249
    check-cast v1, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 129
    iget-object v1, v1, Lcom/squareup/protos/client/invoice/PaymentRequest;->fixed_amount:Lcom/squareup/protos/common/Money;

    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 250
    :cond_2
    check-cast p1, Ljava/util/List;

    check-cast p1, Ljava/lang/Iterable;

    .line 251
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    .line 252
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 253
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 254
    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 255
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/common/Money;

    check-cast v0, Lcom/squareup/protos/common/Money;

    .line 130
    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_2

    :cond_3
    const-string p1, "filter { it.isInstallmen\u2026neyMath.sum(acc, money) }"

    .line 257
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/protos/common/Money;

    return-object v0

    .line 252
    :cond_4
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Empty collection can\'t be reduced."

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final totalInstallmentPercentage(Ljava/util/List;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;)I"
        }
    .end annotation

    .line 122
    check-cast p1, Ljava/lang/Iterable;

    .line 230
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 231
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 122
    invoke-static {v2}, Lcom/squareup/invoices/PaymentRequestsKt;->isInstallment(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 232
    :cond_1
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 233
    new-instance p1, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {p1, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p1, Ljava/util/Collection;

    .line 234
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 235
    check-cast v1, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 123
    iget-object v1, v1, Lcom/squareup/protos/client/invoice/PaymentRequest;->percentage_amount:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    long-to-int v2, v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 236
    :cond_2
    check-cast p1, Ljava/util/List;

    check-cast p1, Ljava/lang/Iterable;

    .line 237
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    .line 238
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 239
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 240
    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 241
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    add-int/2addr v0, v1

    .line 124
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_2

    .line 243
    :cond_3
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result p1

    return p1

    .line 238
    :cond_4
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Empty collection can\'t be reduced."

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method


# virtual methods
.method public final from(Ljava/util/List;Lcom/squareup/protos/common/Money;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "paymentRequests"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "invoiceAmount"

    move-object/from16 v3, p2

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    move-object v2, v1

    check-cast v2, Ljava/lang/Iterable;

    .line 218
    instance-of v4, v2, Ljava/util/Collection;

    const/4 v5, 0x0

    const/4 v6, 0x1

    if-eqz v4, :cond_1

    move-object v7, v2

    check-cast v7, Ljava/util/Collection;

    invoke-interface {v7}, Ljava/util/Collection;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_1

    :cond_0
    const/4 v7, 0x1

    goto :goto_0

    .line 219
    :cond_1
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 83
    invoke-static {v8}, Lcom/squareup/invoices/PaymentRequestsKt;->isInstallment(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v7, 0x0

    :goto_0
    if-nez v7, :cond_f

    if-eqz v4, :cond_3

    .line 221
    move-object v4, v2

    check-cast v4, Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v9, 0x0

    goto :goto_2

    .line 223
    :cond_3
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    const/4 v7, 0x0

    :cond_4
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 87
    invoke-static {v8}, Lcom/squareup/invoices/PaymentRequestsKt;->isInstallment(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v8

    if-eqz v8, :cond_4

    add-int/lit8 v7, v7, 0x1

    if-gez v7, :cond_4

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwCountOverflow()V

    goto :goto_1

    :cond_5
    move v9, v7

    .line 88
    :goto_2
    invoke-static/range {p1 .. p2}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateAmounts(Ljava/util/List;Lcom/squareup/protos/common/Money;)Ljava/util/List;

    move-result-object v4

    if-nez v4, :cond_6

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_6
    check-cast v4, Ljava/lang/Iterable;

    .line 225
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    check-cast v7, Ljava/util/Collection;

    .line 226
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_7
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    move-object v10, v8

    check-cast v10, Lkotlin/Pair;

    .line 89
    invoke-virtual {v10}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/squareup/protos/client/invoice/PaymentRequest;

    invoke-static {v10}, Lcom/squareup/invoices/PaymentRequestsKt;->isInstallment(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v10

    if-eqz v10, :cond_7

    invoke-interface {v7, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 227
    :cond_8
    check-cast v7, Ljava/util/List;

    move-object v10, v7

    check-cast v10, Ljava/lang/Iterable;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    .line 90
    new-instance v4, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory$from$balanceSplitString$2;

    invoke-direct {v4, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory$from$balanceSplitString$2;-><init>(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;)V

    move-object/from16 v16, v4

    check-cast v16, Lkotlin/jvm/functions/Function1;

    const/16 v17, 0x1f

    const/16 v18, 0x0

    invoke-static/range {v10 .. v18}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 91
    invoke-static/range {p1 .. p2}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateBalanceAmount(Ljava/util/List;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 228
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 93
    invoke-static {v4}, Lcom/squareup/invoices/PaymentRequestsKt;->isInstallment(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 229
    iget-object v2, v4, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    sget-object v4, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->PERCENTAGE_INSTALLMENT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-ne v2, v4, :cond_a

    const/4 v2, 0x1

    goto :goto_4

    :cond_a
    const/4 v2, 0x0

    :goto_4
    if-eqz v2, :cond_c

    .line 97
    invoke-direct/range {p0 .. p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;->totalInstallmentPercentage(Ljava/util/List;)I

    move-result v1

    .line 98
    new-instance v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;

    .line 100
    iget-object v4, v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;->percentageFormatter:Lcom/squareup/text/Formatter;

    .line 101
    sget-object v7, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    invoke-virtual {v7, v1}, Lcom/squareup/util/Percentage$Companion;->fromInt(I)Lcom/squareup/util/Percentage;

    move-result-object v7

    .line 100
    invoke-interface {v4, v7}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 102
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    .line 103
    iget-object v4, v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v4, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    const/16 v3, 0x64

    if-gt v1, v3, :cond_b

    const/4 v13, 0x1

    goto :goto_5

    :cond_b
    const/4 v13, 0x0

    :goto_5
    const/4 v14, 0x1

    move-object v8, v2

    .line 98
    invoke-direct/range {v8 .. v14}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    goto :goto_7

    .line 109
    :cond_c
    invoke-direct/range {p0 .. p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;->totalInstallmentMoney(Ljava/util/List;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 110
    new-instance v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;

    .line 112
    iget-object v4, v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v4, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    .line 113
    iget-object v4, v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v4, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    .line 115
    invoke-static {v1, v3}, Lcom/squareup/money/MoneyMathOperatorsKt;->compareTo(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)I

    move-result v1

    if-gtz v1, :cond_d

    const/4 v13, 0x1

    goto :goto_6

    :cond_d
    const/4 v13, 0x0

    :goto_6
    const/4 v14, 0x0

    move-object v8, v2

    .line 110
    invoke-direct/range {v8 .. v14}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$BalanceSplitHelperTextData;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    :goto_7
    return-object v2

    .line 229
    :cond_e
    new-instance v1, Ljava/util/NoSuchElementException;

    const-string v2, "Collection contains no element matching the predicate."

    invoke-direct {v1, v2}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 84
    :cond_f
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Must contain installments."

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1
.end method
