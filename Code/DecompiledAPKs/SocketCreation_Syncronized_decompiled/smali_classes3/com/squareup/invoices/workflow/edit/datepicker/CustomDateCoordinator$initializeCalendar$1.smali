.class public final Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator$initializeCalendar$1;
.super Ljava/lang/Object;
.source "CustomDateCoordinator.kt"

# interfaces
.implements Lcom/squareup/timessquare/CalendarPickerView$OnDateSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator;->initializeCalendar(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/workflow/legacy/WorkflowInput;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0012\u0010\u0006\u001a\u00020\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "com/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator$initializeCalendar$1",
        "Lcom/squareup/timessquare/CalendarPickerView$OnDateSelectedListener;",
        "onDateSelected",
        "",
        "date",
        "Ljava/util/Date;",
        "onDateUnselected",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $input:Lcom/squareup/workflow/legacy/WorkflowInput;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator$initializeCalendar$1;->$input:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDateSelected(Ljava/util/Date;)V
    .locals 3

    const-string v0, "date"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator$initializeCalendar$1;->$input:Lcom/squareup/workflow/legacy/WorkflowInput;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent$CustomDateSelected;

    invoke-static {p1}, Lcom/squareup/util/ProtoDates;->dateToYmd(Ljava/util/Date;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    const-string v2, "dateToYmd(date)"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, p1}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent$CustomDateSelected;-><init>(Lcom/squareup/protos/common/time/YearMonthDay;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method public onDateUnselected(Ljava/util/Date;)V
    .locals 0

    return-void
.end method
