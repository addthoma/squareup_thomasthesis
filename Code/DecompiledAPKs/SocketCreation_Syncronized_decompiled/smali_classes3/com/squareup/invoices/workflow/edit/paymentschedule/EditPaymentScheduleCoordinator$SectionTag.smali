.class final enum Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;
.super Ljava/lang/Enum;
.source "EditPaymentScheduleCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "SectionTag"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\u0008\u0082\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;",
        "",
        "tag",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "getTag",
        "()Ljava/lang/String;",
        "DEPOSIT",
        "BALANCE",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;

.field public static final enum BALANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;

.field public static final enum DEPOSIT:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;


# instance fields
.field private final tag:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;

    const/4 v2, 0x0

    const-string v3, "DEPOSIT"

    const-string v4, "payment-plan-deposit-section"

    .line 302
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;->DEPOSIT:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;

    const/4 v2, 0x1

    const-string v3, "BALANCE"

    const-string v4, "payment-plan-balance-section"

    .line 303
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;->BALANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;->$VALUES:[Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 301
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;->tag:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;
    .locals 1

    const-class v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;

    return-object p0
.end method

.method public static values()[Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;
    .locals 1

    sget-object v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;->$VALUES:[Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;

    invoke-virtual {v0}, [Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;

    return-object v0
.end method


# virtual methods
.method public final getTag()Ljava/lang/String;
    .locals 1

    .line 301
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$SectionTag;->tag:Ljava/lang/String;

    return-object v0
.end method
