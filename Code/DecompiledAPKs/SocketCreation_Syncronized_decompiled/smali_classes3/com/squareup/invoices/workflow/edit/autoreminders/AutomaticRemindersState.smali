.class public abstract Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;
.super Ljava/lang/Object;
.source "AutomaticRemindersState.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;,
        Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;,
        Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$ToggleOffWarning;,
        Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$DuplicateDaysError;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0004\u0011\u0012\u0013\u0014B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0012\u0010\u0007\u001a\u00020\u0008X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\nR\u0012\u0010\u000b\u001a\u00020\u000cX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000eR\u0012\u0010\u000f\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0006\u0082\u0001\u0004\u0015\u0016\u0017\u0018\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;",
        "Landroid/os/Parcelable;",
        "()V",
        "defaultListInfo",
        "Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;",
        "getDefaultListInfo",
        "()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;",
        "reminderSettings",
        "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;",
        "getReminderSettings",
        "()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;",
        "remindersEnabled",
        "",
        "getRemindersEnabled",
        "()Z",
        "remindersListInfo",
        "getRemindersListInfo",
        "DuplicateDaysError",
        "EditReminder",
        "RemindersList",
        "ToggleOffWarning",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$ToggleOffWarning;",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$DuplicateDaysError;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 10
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getDefaultListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;
.end method

.method public abstract getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;
.end method

.method public abstract getRemindersEnabled()Z
.end method

.method public abstract getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;
.end method
