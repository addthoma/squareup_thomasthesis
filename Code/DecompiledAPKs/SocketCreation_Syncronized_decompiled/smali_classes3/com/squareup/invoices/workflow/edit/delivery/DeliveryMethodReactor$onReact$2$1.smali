.class final Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2$1;
.super Lkotlin/jvm/internal/Lambda;
.source "DeliveryMethodReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent$BackPressed;",
        "Lcom/squareup/workflow/legacy/FinishWith<",
        "+",
        "Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/FinishWith;",
        "Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;",
        "it",
        "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent$BackPressed;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2$1;->this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent$BackPressed;)Lcom/squareup/workflow/legacy/FinishWith;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent$BackPressed;",
            ")",
            "Lcom/squareup/workflow/legacy/FinishWith<",
            "Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    new-instance p1, Lcom/squareup/workflow/legacy/FinishWith;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2$1;->this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;

    iget-object v0, v0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;->this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2$1;->this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;

    iget-object v1, v1, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    invoke-static {v0, v1}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;->access$toResult(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;)Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/FinishWith;-><init>(Ljava/lang/Object;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent$BackPressed;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2$1;->invoke(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent$BackPressed;)Lcom/squareup/workflow/legacy/FinishWith;

    move-result-object p1

    return-object p1
.end method
