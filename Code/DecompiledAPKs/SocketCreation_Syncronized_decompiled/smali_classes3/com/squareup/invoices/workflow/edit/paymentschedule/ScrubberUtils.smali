.class public final Lcom/squareup/invoices/workflow/edit/paymentschedule/ScrubberUtils;
.super Ljava/lang/Object;
.source "ScrubberUtils.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nScrubberUtils.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ScrubberUtils.kt\ncom/squareup/invoices/workflow/edit/paymentschedule/ScrubberUtils\n*L\n1#1,73:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\u0018\u00002\u00020\u0001B5\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u0016\u0010\u0014\u001a\u00020\n2\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\nJ\u000e\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u0015\u001a\u00020\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/ScrubberUtils;",
        "",
        "priceLocaleHelper",
        "Lcom/squareup/money/PriceLocaleHelper;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "unitFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)V",
        "maxMoneyScrubber",
        "Lcom/squareup/money/MaxMoneyScrubber;",
        "perUnitScrubber",
        "Lcom/squareup/money/PerUnitScrubber;",
        "percentageScrubber",
        "Lcom/squareup/text/PercentageWholeUnitNoDecimalAmountScrubber;",
        "scrubAndExtractMoney",
        "string",
        "",
        "moneyMax",
        "scrubAndExtractPercentage",
        "",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final maxMoneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

.field private final perUnitScrubber:Lcom/squareup/money/PerUnitScrubber;

.field private final percentageScrubber:Lcom/squareup/text/PercentageWholeUnitNoDecimalAmountScrubber;

.field private final priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;


# direct methods
.method public constructor <init>(Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/money/PriceLocaleHelper;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "priceLocaleHelper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unitFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/ScrubberUtils;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/ScrubberUtils;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 31
    new-instance p1, Lcom/squareup/text/PercentageWholeUnitNoDecimalAmountScrubber;

    sget-object p2, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->BLANKABLE:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    invoke-direct {p1, p5, p2}, Lcom/squareup/text/PercentageWholeUnitNoDecimalAmountScrubber;-><init>(Lcom/squareup/util/Res;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/ScrubberUtils;->percentageScrubber:Lcom/squareup/text/PercentageWholeUnitNoDecimalAmountScrubber;

    .line 32
    new-instance p1, Lcom/squareup/money/PerUnitScrubber;

    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/ScrubberUtils;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    sget-object p5, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->BLANKABLE:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    const-string v0, ""

    invoke-direct {p1, p2, p3, p5, v0}, Lcom/squareup/money/PerUnitScrubber;-><init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/ScrubberUtils;->perUnitScrubber:Lcom/squareup/money/PerUnitScrubber;

    .line 34
    new-instance p1, Lcom/squareup/money/MaxMoneyScrubber;

    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/ScrubberUtils;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    check-cast p2, Lcom/squareup/money/MoneyExtractor;

    iget-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/ScrubberUtils;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v0, 0x0

    invoke-static {v0, v1, p3}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p3

    invoke-direct {p1, p2, p4, p3}, Lcom/squareup/money/MaxMoneyScrubber;-><init>(Lcom/squareup/money/MoneyExtractor;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/ScrubberUtils;->maxMoneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

    return-void
.end method


# virtual methods
.method public final scrubAndExtractMoney(Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;
    .locals 1

    const-string v0, "string"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyMax"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/ScrubberUtils;->maxMoneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

    invoke-virtual {v0, p2}, Lcom/squareup/money/MaxMoneyScrubber;->setMax(Lcom/squareup/protos/common/Money;)V

    .line 60
    new-instance p2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    invoke-direct {p2, p1}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;)V

    .line 61
    new-instance p1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    const-string v0, ""

    invoke-direct {p1, v0}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/ScrubberUtils;->perUnitScrubber:Lcom/squareup/money/PerUnitScrubber;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/money/PerUnitScrubber;->scrub(Lcom/squareup/text/SelectableTextScrubber$SelectableText;Lcom/squareup/text/SelectableTextScrubber$SelectableText;)Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    move-result-object p1

    .line 64
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/ScrubberUtils;->maxMoneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

    iget-object p1, p1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    invoke-virtual {p2, p1}, Lcom/squareup/money/MaxMoneyScrubber;->scrub(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 66
    check-cast p1, Ljava/lang/CharSequence;

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p2

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p2, 0x1

    :goto_1
    if-eqz p2, :cond_2

    const-wide/16 p1, 0x0

    .line 67
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/ScrubberUtils;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p1, p2, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_2

    .line 69
    :cond_2
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/ScrubberUtils;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    check-cast p2, Lcom/squareup/money/MoneyExtractor;

    invoke-static {p2, p1}, Lcom/squareup/money/MoneyExtractorKt;->requireMoney(Lcom/squareup/money/MoneyExtractor;Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    :goto_2
    return-object p1
.end method

.method public final scrubAndExtractPercentage(Ljava/lang/String;)J
    .locals 2

    const-string v0, "string"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    new-instance v0, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    invoke-direct {v0, p1}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;)V

    .line 42
    new-instance p1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    const-string v1, ""

    invoke-direct {p1, v1}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;)V

    .line 44
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/ScrubberUtils;->percentageScrubber:Lcom/squareup/text/PercentageWholeUnitNoDecimalAmountScrubber;

    invoke-virtual {v1, p1, v0}, Lcom/squareup/text/PercentageWholeUnitNoDecimalAmountScrubber;->scrub(Lcom/squareup/text/SelectableTextScrubber$SelectableText;Lcom/squareup/text/SelectableTextScrubber$SelectableText;)Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    move-result-object p1

    .line 46
    iget-object p1, p1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    sget-object v0, Lcom/squareup/util/Percentage;->ZERO:Lcom/squareup/util/Percentage;

    .line 45
    invoke-static {p1, v0}, Lcom/squareup/util/Numbers;->parseFormattedPercentage(Ljava/lang/String;Lcom/squareup/util/Percentage;)Lcom/squareup/util/Percentage;

    move-result-object p1

    if-eqz p1, :cond_0

    const-wide/16 v0, 0x64

    .line 47
    invoke-virtual {p1, v0, v1}, Lcom/squareup/util/Percentage;->percentOf(J)J

    move-result-wide v0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0
.end method
