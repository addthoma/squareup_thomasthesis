.class final Lcom/squareup/feedback/AppFeedbackScopeRunner$submitFeedbackAndRating$4$failureMessage$1;
.super Lkotlin/jvm/internal/Lambda;
.source "AppFeedbackScopeRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/feedback/AppFeedbackScopeRunner$submitFeedbackAndRating$4;->call(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/feedback/AppFeedbackResponse;",
        "Lcom/squareup/receiving/FailureMessage$Parts;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/receiving/FailureMessage$Parts;",
        "r",
        "Lcom/squareup/feedback/AppFeedbackResponse;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/feedback/AppFeedbackScopeRunner$submitFeedbackAndRating$4$failureMessage$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/feedback/AppFeedbackScopeRunner$submitFeedbackAndRating$4$failureMessage$1;

    invoke-direct {v0}, Lcom/squareup/feedback/AppFeedbackScopeRunner$submitFeedbackAndRating$4$failureMessage$1;-><init>()V

    sput-object v0, Lcom/squareup/feedback/AppFeedbackScopeRunner$submitFeedbackAndRating$4$failureMessage$1;->INSTANCE:Lcom/squareup/feedback/AppFeedbackScopeRunner$submitFeedbackAndRating$4$failureMessage$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/feedback/AppFeedbackResponse;)Lcom/squareup/receiving/FailureMessage$Parts;
    .locals 1

    .line 76
    new-instance v0, Lcom/squareup/receiving/FailureMessage$Parts;

    invoke-interface {p1}, Lcom/squareup/feedback/AppFeedbackResponse;->getStatus()Lcom/squareup/protos/client/Status;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/receiving/FailureMessage$Parts;-><init>(Lcom/squareup/protos/client/Status;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 26
    check-cast p1, Lcom/squareup/feedback/AppFeedbackResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/feedback/AppFeedbackScopeRunner$submitFeedbackAndRating$4$failureMessage$1;->invoke(Lcom/squareup/feedback/AppFeedbackResponse;)Lcom/squareup/receiving/FailureMessage$Parts;

    move-result-object p1

    return-object p1
.end method
