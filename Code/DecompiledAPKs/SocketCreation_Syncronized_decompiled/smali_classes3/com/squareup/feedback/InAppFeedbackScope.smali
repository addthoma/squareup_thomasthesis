.class public abstract Lcom/squareup/feedback/InAppFeedbackScope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "InAppFeedbackScope.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0000\n\u0000\u0008&\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/feedback/InAppFeedbackScope;",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "parent",
        "Lcom/squareup/feedback/AppFeedbackScope;",
        "(Lcom/squareup/feedback/AppFeedbackScope;)V",
        "getParent",
        "()Lcom/squareup/feedback/AppFeedbackScope;",
        "getParentKey",
        "",
        "feedback_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final parent:Lcom/squareup/feedback/AppFeedbackScope;


# direct methods
.method public constructor <init>(Lcom/squareup/feedback/AppFeedbackScope;)V
    .locals 1

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/feedback/InAppFeedbackScope;->parent:Lcom/squareup/feedback/AppFeedbackScope;

    return-void
.end method


# virtual methods
.method public final getParent()Lcom/squareup/feedback/AppFeedbackScope;
    .locals 1

    .line 5
    iget-object v0, p0, Lcom/squareup/feedback/InAppFeedbackScope;->parent:Lcom/squareup/feedback/AppFeedbackScope;

    return-object v0
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/squareup/feedback/InAppFeedbackScope;->parent:Lcom/squareup/feedback/AppFeedbackScope;

    return-object v0
.end method
